/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtConcateMember.c
 *
 * Created Date: May 8, 2014
 *
 * Description : VCG member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtConcateMemberInternal.h"
#include "AtVcgInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtConcateMember)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/* Override */
static tAtObjectMethods          m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtConcateMemberMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsVcatVcg(AtConcateGroup self)
    {
    if (AtConcateGroupConcatTypeGet(self) != cAtConcateGroupTypeVcat)
        return cAtFalse;

    return AtVcgLcasIsEnabled((AtVcg)self) ? cAtFalse : cAtTrue;
    }

static eAtLcasMemberSourceState SourceStateGet(AtConcateMember self)
    {
    AtUnused(self);
    return cAtLcasMemberSourceStateInvl;
    }

static eAtLcasMemberSinkState SinkStateGet(AtConcateMember self)
    {
    AtUnused(self);
    return cAtLcasMemberSinkStateInvl;
    }

static eAtModuleConcateRet SourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    AtUnused(self);
    AtUnused(sourceSequence);
    return cAtError;
    }

static uint32 SourceSequenceGet(AtConcateMember self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtModuleConcateRet SinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    AtUnused(self);
    AtUnused(sinkSequence);
    return cAtError;
    }

static uint32 SinkExpectedSequenceGet(AtConcateMember self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 SinkSequenceGet(AtConcateMember self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 SourceMstGet(AtConcateMember self)
    {
    return AtVcgSourceMstGet((AtVcg)AtConcateMemberConcateGroupGet(self), self);
    }

static eAtLcasMemberCtrl SourceControlGet(AtConcateMember self)
    {
    /* Sub class must know */
    AtUnused(self);
    return cAtLcasMemberCtrlInvl;
    }

static uint32 SinkMstGet(AtConcateMember self)
    {
    return AtVcgSinkMstGet((AtVcg)AtConcateMemberConcateGroupGet(self), self);
    }

static eAtLcasMemberCtrl SinkControlGet(AtConcateMember self)
    {
    /* Concrete must know */
    AtUnused(self);
    return cAtLcasMemberCtrlInvl;
    }

static AtChannel SinkChannelGet(AtConcateMember self)
    {
    return self->sinkChannel;
    }

static AtChannel SourceChannelGet(AtConcateMember self)
    {
    return self->sourceChannel;
    }

static eAtModuleConcateRet SourceInit(AtConcateMember self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtModuleConcateRet SinkInit(AtConcateMember self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AllInterruptMaskClear(AtConcateMember self,
                                    uint32 (*IntrMaskGet)(AtConcateMember),
                                    uint32 (*IntrMaskSet)(AtConcateMember, uint32, uint32))
    {
    uint32 mask = IntrMaskGet(self);
    return IntrMaskSet(self, mask, 0);
    }

static eAtRet SourceHardwareCleanup(AtConcateMember self)
    {
    eAtRet ret = cAtOk;

    ret |= AllInterruptMaskClear(self,
                                 mMethodsGet(self)->SourceInterruptMaskGet,
                                 mMethodsGet(self)->SourceInterruptMaskSet);
    /* More thing to clean-up here */

    return ret;
    }

static eAtRet SinkHardwareCleanup(AtConcateMember self)
    {
    eAtRet ret = cAtOk;

    ret |= AllInterruptMaskClear(self,
                                 mMethodsGet(self)->SinkInterruptMaskGet,
                                 mMethodsGet(self)->SinkInterruptMaskSet);
    /* More thing to clean-up here */

    return ret;
    }

static eAtRet SourceInterruptMaskSet(AtConcateMember self, uint32 interruptMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(interruptMask);
    AtUnused(enableMask);
    /* Let subclass implement */
    return cAtError;
    }

static uint32 SourceInterruptMaskGet(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SourceSupportedInterruptMasks(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SourceDefectGet(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SourceDefectHistoryGet(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SourceDefectHistoryClear(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static void SourceInterruptProcess(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    }

static eAtRet SinkInterruptMaskSet(AtConcateMember self, uint32 interruptMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(interruptMask);
    AtUnused(enableMask);
    /* Let subclass implement */
    return cAtError;
    }

static uint32 SinkInterruptMaskGet(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SinkSupportedInterruptMasks(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SinkDefectGet(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SinkDefectHistoryGet(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static uint32 SinkDefectHistoryClear(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return 0;
    }

static void SinkInterruptProcess(AtConcateMember self)
    {
    AtUnused(self);
    /* Let subclass determine */
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    return ret;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "member";
    }

static const char *IdString(AtChannel self)
    {
    AtChannel channel = AtConcateMemberChannelGet((AtConcateMember)self);
    static char description[64];

    if (channel == NULL)
        return m_AtChannelMethods->IdString(self);

    AtSprintf(description, "%s", AtObjectToString((AtObject)channel));
    return description;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtConcateMember member = (AtConcateMember)self;
    uint32 defects = 0;

    if (AtConcateMemberSinkChannelGet(member))
        defects |= mMethodsGet(member)->SinkDefectGet(member);

    if (AtConcateMemberSourceChannelGet(member))
        defects |= mMethodsGet(member)->SourceDefectGet(member);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtConcateMember member = (AtConcateMember)self;
    uint32 defects = 0;

    if (AtConcateMemberSinkChannelGet(member))
        defects |= mMethodsGet(member)->SinkDefectHistoryGet(member);

    if (AtConcateMemberSourceChannelGet(member))
        defects |= mMethodsGet(member)->SourceDefectHistoryGet(member);

    return defects;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtConcateMember member = (AtConcateMember)self;
    uint32 defects = 0;

    if (AtConcateMemberSinkChannelGet(member))
        defects |= mMethodsGet(member)->SinkDefectHistoryClear(member);

    if (AtConcateMemberSourceChannelGet(member))
        defects |= mMethodsGet(member)->SourceDefectHistoryClear(member);

    return defects;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtConcateMember member = (AtConcateMember)self;
    eAtRet ret = cAtOk;

    if (AtConcateMemberSinkChannelGet(member))
        ret |= mMethodsGet(member)->SinkInterruptMaskSet(member, defectMask, enableMask);

    if (AtConcateMemberSourceChannelGet(member))
        ret |= mMethodsGet(member)->SourceInterruptMaskSet(member, defectMask, enableMask);

    return ret;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtConcateMember member = (AtConcateMember)self;
    uint32 defects = 0;

    if (AtConcateMemberSinkChannelGet(member))
        defects |= mMethodsGet(member)->SinkInterruptMaskGet(member);

    if (AtConcateMemberSourceChannelGet(member))
        defects |= mMethodsGet(member)->SourceInterruptMaskGet(member);

    return defects;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtConcateMember member = (AtConcateMember)self;
    uint32 defects = 0;

    if (AtConcateMemberSinkChannelGet(member))
        defects |= mMethodsGet(member)->SinkSupportedInterruptMasks(member);

    if (AtConcateMemberSourceChannelGet(member))
        defects |= mMethodsGet(member)->SourceSupportedInterruptMasks(member);

    return defects;
    }

static eAtRet DoSinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (!IsVcatVcg(AtConcateMemberConcateGroupGet(self)))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->SinkExpectedSequenceSet(self, sinkSequence);
    }

static eAtModuleConcateRet DoSourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (!IsVcatVcg(AtConcateMemberConcateGroupGet(self)))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->SourceSequenceSet(self, sourceSequence);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtConcateMember);
    }

static void MethodsInit(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SourceInit);
        mMethodOverride(m_methods, SourceStateGet);
        mMethodOverride(m_methods, SinkStateGet);
        mMethodOverride(m_methods, SourceSequenceSet);
        mMethodOverride(m_methods, SourceSequenceGet);
        mMethodOverride(m_methods, SinkInit);
        mMethodOverride(m_methods, SinkExpectedSequenceSet);
        mMethodOverride(m_methods, SinkExpectedSequenceGet);
        mMethodOverride(m_methods, SinkSequenceGet);
        mMethodOverride(m_methods, SourceMstGet);
        mMethodOverride(m_methods, SourceControlGet);
        mMethodOverride(m_methods, SinkMstGet);
        mMethodOverride(m_methods, SinkControlGet);
        mMethodOverride(m_methods, SinkChannelGet);
        mMethodOverride(m_methods, SourceChannelGet);
        mMethodOverride(m_methods, SourceHardwareCleanup);
        mMethodOverride(m_methods, SinkHardwareCleanup);
        mMethodOverride(m_methods, SourceInterruptMaskSet);
        mMethodOverride(m_methods, SourceInterruptMaskGet);
        mMethodOverride(m_methods, SourceSupportedInterruptMasks);
        mMethodOverride(m_methods, SourceDefectGet);
        mMethodOverride(m_methods, SourceDefectHistoryGet);
        mMethodOverride(m_methods, SourceDefectHistoryClear);
        mMethodOverride(m_methods, SourceInterruptProcess);
        mMethodOverride(m_methods, SinkInterruptMaskSet);
        mMethodOverride(m_methods, SinkInterruptMaskGet);
        mMethodOverride(m_methods, SinkSupportedInterruptMasks);
        mMethodOverride(m_methods, SinkDefectGet);
        mMethodOverride(m_methods, SinkDefectHistoryGet);
        mMethodOverride(m_methods, SinkDefectHistoryClear);
        mMethodOverride(m_methods, SinkInterruptProcess);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtConcateMember object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(group);
    mEncodeObjectDescription(sourceChannel);
    mEncodeObjectDescription(sinkChannel);
    mEncodeUInt(sinkAdded);
    mEncodeUInt(sourceAdded);
    mEncodeObjectDescription(channel);
    }

static void OverrideAtObject(AtConcateMember self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtConcateMember self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

AtConcateMember AtConcateMemberObjectInit(AtConcateMember self, AtConcateGroup group, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, AtChannelIdGet(channel), AtChannelModuleGet((AtChannel)group)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->group   = group;
    self->channel = channel;

    return self;
    }

void AtConcateMemberSinkAdd(AtConcateMember self)
    {
    if (self)
        self->sinkAdded = cAtTrue;
    }

void AtConcateMemberSinkRemove(AtConcateMember self)
    {
    if (self)
        self->sinkAdded = cAtFalse;
    }

eBool AtConcateMemberSinkIsAdded(AtConcateMember self)
    {
    return (eBool)(self ? self->sinkAdded : cAtFalse);
    }

void AtConcateMemberSourceAdd(AtConcateMember self)
    {
    if (self)
        self->sourceAdded = cAtTrue;
    }

void AtConcateMemberSourceRemove(AtConcateMember self)
    {
    if (self)
        self->sourceAdded = cAtFalse;
    }

eBool AtConcateMemberSourceIsAdded(AtConcateMember self)
    {
    return (eBool)(self ? self->sourceAdded : cAtFalse);
    }

void AtConcateMemberSourceChannelSet(AtConcateMember self, AtChannel channel)
    {
    if (self)
        self->sourceChannel = channel;
    }

void AtConcateMemberSinkChannelSet(AtConcateMember self, AtChannel channel)
    {
    if (self)
        self->sinkChannel = channel;
    }

/*
 * Initialize member at source side
 *
 * @param self This member
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateMemberSourceInit(AtConcateMember self)
    {
    if (self)
        return mMethodsGet(self)->SourceInit(self);
    return cAtErrorNullPointer;
    }

/*
 * Initialize member at sink side
 *
 * @param self This member
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateMemberSinkInit(AtConcateMember self)
    {
    if (self)
        return mMethodsGet(self)->SinkInit(self);
    return cAtErrorNullPointer;
    }

eAtRet AtConcateMemberSourceHardwareCleanup(AtConcateMember self)
    {
    if (self)
        return mMethodsGet(self)->SourceHardwareCleanup(self);
    return cAtErrorNullPointer;
    }

eAtRet AtConcateMemberSinkHardwareCleanup(AtConcateMember self)
    {
    if (self)
        return mMethodsGet(self)->SinkHardwareCleanup(self);
    return cAtErrorNullPointer;
    }

void AtConcateMemberSourceInterruptProcess(AtConcateMember self)
    {
    if (self)
        mMethodsGet(self)->SourceInterruptProcess(self);
    }

void AtConcateMemberSinkInterruptProcess(AtConcateMember self)
    {
    if (self)
        mMethodsGet(self)->SinkInterruptProcess(self);
    }

/**
* @addtogroup AtConcateMember
* @{
*/

/**
 * Get source member state
 *
 * @param self This member
 *
 * @return Source member state @ref eAtLcasMemberSourceState "LCAS source member state"
 */
eAtLcasMemberSourceState AtConcateMemberSourceStateGet(AtConcateMember self)
    {
    mAttributeGet(SourceStateGet, eAtLcasMemberSourceState, cAtLcasMemberSourceStateInvl);
    }

/**
 * Get sink member state
 *
 * @param self This member
 *
 * @return Sink member state @ref eAtLcasMemberSinkState "LCAS sink member state"
 */
eAtLcasMemberSinkState AtConcateMemberSinkStateGet(AtConcateMember self)
    {
    mAttributeGet(SinkStateGet, eAtLcasMemberSinkState, cAtLcasMemberSinkStateInvl);
    }

/**
 * Set source sequence number
 *
 * @param self This member
 * @param sourceSequence The source sequence number
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateMemberSourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    mOneParamWrapCall(sourceSequence, eAtModuleConcateRet, DoSourceSequenceSet(self, sourceSequence));
    }

/**
 * Get source sequence number
 *
 * @param self This member
 *
 * @return Source sequence number
 */
uint32 AtConcateMemberSourceSequenceGet(AtConcateMember self)
    {
    mAttributeGet(SourceSequenceGet, uint32, cInvalidUint32);
    }

/**
 * Set sink expected sequence number
 *
 * @param self This member
 * @param sinkSequence The sink expected sequence number
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateMemberSinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    mOneParamWrapCall(sinkSequence, eAtModuleConcateRet, DoSinkExpectedSequenceSet(self, sinkSequence));
    }

/**
 * Get expected sink sequence number
 *
 * @param self This member
 *
 * @return Expected sink sequence
 */
uint32 AtConcateMemberSinkExpectedSequenceGet(AtConcateMember self)
    {
    mAttributeGet(SinkExpectedSequenceGet, uint32, cInvalidUint32);
    }

/**
 * Get sink sequence number
 *
 * @param self This member
 *
 * @return Sink sequence number
 */
uint32 AtConcateMemberSinkSequenceGet(AtConcateMember self)
    {
    mAttributeGet(SinkSequenceGet, uint32, cInvalidUint32);
    }

/**
 * Get source member status (MST)
 *
 * @param self This member
 *
 * @return Source member status
 */
uint32 AtConcateMemberSourceMstGet(AtConcateMember self)
    {
    mAttributeGet(SourceMstGet, uint32, cInvalidUint32);
    }

/**
 * Get source control word
 *
 * @param self This member
 *
 * @return Source control word @ref eAtLcasMemberCtrl "LCAS control words"
 */
eAtLcasMemberCtrl AtConcateMemberSourceControlGet(AtConcateMember self)
    {
    mAttributeGet(SourceControlGet, uint32, cAtLcasMemberCtrlInvl);
    }

/**
 * Get the TDM channel that is bound to this member at source side
 *
 * @param self This member
 *
 * @return Bound TDM channel at source side
 */
AtChannel AtConcateMemberSourceChannelGet(AtConcateMember self)
    {
    mAttributeGet(SourceChannelGet, AtChannel, NULL);
    }

/**
 * Get sink member status (MST)
 *
 * @param self This member
 *
 * @return Sink member status
 */
uint32 AtConcateMemberSinkMstGet(AtConcateMember self)
    {
    mAttributeGet(SinkMstGet, uint32, cInvalidUint32);
    }

/**
 * Get sink control word
 *
 * @param self This member
 *
 * @return Sink control word @ref eAtLcasMemberCtrl "LCAS control words"
 */
eAtLcasMemberCtrl AtConcateMemberSinkControlGet(AtConcateMember self)
    {
    mAttributeGet(SinkControlGet, eAtLcasMemberCtrl, cAtLcasMemberCtrlInvl);
    }

/**
 * Get the TDM channel that is bound to this member
 *
 * @param self This member
 *
 * @return Bound TDM channel
 */
AtChannel AtConcateMemberChannelGet(AtConcateMember self)
    {
    mDirectAttributeGet(channel, AtChannel, NULL);
    }

/**
 * Get the TDM channel that is bound to this member at sink side
 *
 * @param self This member
 *
 * @return Bound TDM channel at sink side
 */
AtChannel AtConcateMemberSinkChannelGet(AtConcateMember self)
    {
    mAttributeGet(SinkChannelGet, AtChannel, NULL);
    }

/**
 * Get concatenation group which the member belongs to
 *
 * @param self This member
 *
 * @return The concatenation group which it is provisioned to
 */
AtConcateGroup AtConcateMemberConcateGroupGet(AtConcateMember self)
    {
    mDirectAttributeGet(group, AtConcateGroup, NULL);
    }

/**
 *@}
 */

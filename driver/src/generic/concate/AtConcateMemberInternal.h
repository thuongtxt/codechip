/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtConcateMemberInternal.h
 * 
 * Created Date: May 8, 2014
 *
 * Description : VCG member
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONCATEMEMBERINTERNAL_H_
#define _ATCONCATEMEMBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtConcateMember.h"
#include "AtConcateGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtConcateMemberMethods
    {
    eAtModuleConcateRet (*SourceInit)(AtConcateMember self);
    eAtLcasMemberSourceState (*SourceStateGet)(AtConcateMember self);
    eAtModuleConcateRet (*SourceSequenceSet)(AtConcateMember self, uint32 sourceSequence);
    uint32 (*SourceSequenceGet)(AtConcateMember self);
    uint32 (*SourceMstGet)(AtConcateMember self);
    eAtLcasMemberCtrl (*SourceControlGet)(AtConcateMember self);

    eAtModuleConcateRet (*SinkInit)(AtConcateMember self);
    eAtLcasMemberSinkState (*SinkStateGet)(AtConcateMember self);
    eAtModuleConcateRet (*SinkExpectedSequenceSet)(AtConcateMember self, uint32 sinkSequence);
    uint32 (*SinkExpectedSequenceGet)(AtConcateMember self);
    uint32 (*SinkSequenceGet)(AtConcateMember self);
    uint32 (*SinkMstGet)(AtConcateMember self);
    eAtLcasMemberCtrl (*SinkControlGet)(AtConcateMember self);

    AtChannel (*SinkChannelGet)(AtConcateMember self);
    AtChannel (*SourceChannelGet)(AtConcateMember self);

    /* Source interrupt */
    eAtRet (*SourceInterruptMaskSet)(AtConcateMember self, uint32 interruptMask, uint32 enableMask);
    uint32 (*SourceInterruptMaskGet)(AtConcateMember self);
    uint32 (*SourceSupportedInterruptMasks)(AtConcateMember self);
    uint32 (*SourceDefectGet)(AtConcateMember self);
    uint32 (*SourceDefectHistoryGet)(AtConcateMember self);
    uint32 (*SourceDefectHistoryClear)(AtConcateMember self);
    void (*SourceInterruptProcess)(AtConcateMember self);

    /* Sink interrupt */
    eAtRet (*SinkInterruptMaskSet)(AtConcateMember self, uint32 interruptMask, uint32 enableMask);
    uint32 (*SinkInterruptMaskGet)(AtConcateMember self);
    uint32 (*SinkSupportedInterruptMasks)(AtConcateMember self);
    uint32 (*SinkDefectGet)(AtConcateMember self);
    uint32 (*SinkDefectHistoryGet)(AtConcateMember self);
    uint32 (*SinkDefectHistoryClear)(AtConcateMember self);
    void (*SinkInterruptProcess)(AtConcateMember self);

    /* Internal */
    eAtRet (*SourceHardwareCleanup)(AtConcateMember self);
    eAtRet (*SinkHardwareCleanup)(AtConcateMember self);
    }tAtConcateMemberMethods;

typedef struct tAtConcateMember
    {
    tAtChannel super;
    const tAtConcateMemberMethods *methods;

    /* Private Data */
    AtConcateGroup group;
    AtChannel sourceChannel;
    AtChannel sinkChannel;
    eBool sinkAdded;
    eBool sourceAdded;

    /* TODO: Deprecate this */
    AtChannel channel;
    }tAtConcateMember;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateMember AtConcateMemberObjectInit(AtConcateMember self, AtConcateGroup group, AtChannel channel);

/* For setup */
eAtModuleConcateRet AtConcateMemberSourceInit(AtConcateMember self);
eAtModuleConcateRet AtConcateMemberSinkInit(AtConcateMember self);

/* For tear-down */
eAtRet AtConcateMemberSourceHardwareCleanup(AtConcateMember self);
eAtRet AtConcateMemberSinkHardwareCleanup(AtConcateMember self);

/* Interrupt */
void AtConcateMemberSourceInterruptProcess(AtConcateMember self);
void AtConcateMemberSinkInterruptProcess(AtConcateMember self);

/* Access private data */
void AtConcateMemberSinkAdd(AtConcateMember self);
void AtConcateMemberSinkRemove(AtConcateMember self);
eBool AtConcateMemberSinkIsAdded(AtConcateMember self);
void AtConcateMemberSourceAdd(AtConcateMember self);
void AtConcateMemberSourceRemove(AtConcateMember self);
eBool AtConcateMemberSourceIsAdded(AtConcateMember self);

void AtConcateMemberSourceChannelSet(AtConcateMember self, AtChannel channel);
void AtConcateMemberSinkChannelSet(AtConcateMember self, AtChannel channel);

#endif /* _ATCONCATEMEMBERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : AtCrossConnect.c
 *
 * Created Date: Aug 1, 2014
 *
 * Description : Cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../common/AtChannelInternal.h"
#include "AtCrossConnectInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtCrossConnectMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ConnectCheck(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    AtChannel oldSource;

    AtUnused(self);

    /* At least two of channels must be not NULL */
    if ((source == NULL) || (dest == NULL))
        return cAtErrorNullPointer;

    /* Connection may be busy */
    oldSource = AtChannelSourceGet(dest);
    if (oldSource && (oldSource != source))
        return cAtModuleXcErrorConnectionBusy;

    return cAtOk;
    }

static eAtModuleXcRet ChannelConnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtRet ret;

    /* Make sure that these two channels can be connected */
    ret = mMethodsGet(self)->ConnectCheck(self, source, dest);
    if (ret != cAtOk)
        return ret;

    /* Connect already exists, just do nothing */
    if (source == AtCrossConnectSourceChannelGet(self, dest))
        return cAtOk;

    /* Connect them */
    ret |= mMethodsGet(self)->HwConnect(self, source, dest);

    /* Let's the two channels determine how to save source and destination
     * references */
    ret |= AtChannelDestinationAdd(source, dest);
    ret |= AtChannelSourceSet(dest, source);

    return ret;
    }

static eAtModuleXcRet ChannelDisconnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtRet ret = cAtOk;
    AtChannel currentSource;

    /* Two of channels must be valid */
    if ((source == NULL) || (dest == NULL))
        return cAtErrorNullPointer;

    /* Already disconnect, just do nothing */
    currentSource = AtChannelSourceGet(dest);
    if (currentSource == NULL)
        return cAtOk;

    /* Connection must exist */
    if (currentSource != source)
        return cAtModuleXcErrorConnectionNotExist;

    /* Disconnect them */
    ret |= mMethodsGet(self)->HwDisconnect(self, dest);

    /* Let's the two channels determine how to release source and destination
     * references */
    ret |= AtChannelDestinationRemove(source, dest);
    ret |= AtChannelSourceSet(dest, NULL);

    return ret;
    }

static uint32 NumDestChannelsGet(AtCrossConnect self, AtChannel source)
    {
    AtUnused(self);
    return AtListLengthGet(AtChannelDestinationChannels(source));
    }

static AtChannel DestChannelGetByIndex(AtCrossConnect self, AtChannel source, uint32 destIndex)
    {
    AtList destChannels = AtChannelDestinationChannels(source);
    AtUnused(self);
    return (AtChannel)AtListObjectGet(destChannels, destIndex);
    }

static AtChannel SourceChannelGet(AtCrossConnect self, AtChannel dest)
    {
    AtUnused(self);
    return AtChannelSourceGet(dest);
    }

static eAtRet AllConnectionsDelete(AtCrossConnect self)
    {
    /* Concrete must know */
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static const char *DescriptionBuild(AtObject self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "%s", AtCrossConnectTypeString((AtCrossConnect)self));
    return buffer;
    }

static const char *TypeString(AtCrossConnect self)
    {
    AtUnused(self);
    return "xc";
    }

static eAtRet HwConnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    AtUnused(self);
    AtUnused(source);
    AtUnused(dest);
    return cAtErrorNotImplemented;
    }

static eAtRet HwDisconnect(AtCrossConnect self, AtChannel dest)
    {
    AtUnused(self);
    AtUnused(dest);
    return cAtErrorNotImplemented;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtCrossConnect object = (AtCrossConnect)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    }

static eBool IsBlockingCrossConnect(AtCrossConnect self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HwIsConnected(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    AtUnused(self);
    AtUnused(sourceVc);
    AtUnused(destVc);
    return cAtFalse;
    }

static eAtRet VcDisconnectAllConnections(AtCrossConnect self, AtSdhChannel vc)
    {
    eAtRet ret = cAtOk;
    AtChannel channel = (AtChannel)vc;
    AtChannel source;

    /* Disconnect source connection */
    source = AtCrossConnectSourceChannelGet(self, channel);
    if (source)
        {
        ret = AtCrossConnectChannelDisconnect(self, source, channel);
        if (ret != cAtOk)
            return ret;
        }

    /* Disconnect all destinations */
    while (AtCrossConnectNumDestChannelsGet(self, channel) > 0)
        {
        AtChannel dest = AtCrossConnectDestChannelGetByIndex(self, channel, 0);
        ret = AtCrossConnectChannelDisconnect(self, channel, dest);
        if (ret != cAtOk)
            return ret;
        }

    return ret;
    }

static void ChannelConnectDisconnectApiLogStart(AtCrossConnect self, AtChannel source, AtChannel dest,
                                                const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *sourceStr, *destStr;

        AtDriverLogLock();

        /* Build object descriptions */
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);
        sourceStr = buffer;
        destStr = buffer + (bufferSize / 2);
        AtSnprintf(sourceStr, (bufferSize / 2), "%s", AtObjectToString((AtObject)source));
        AtSnprintf(destStr, (bufferSize / 2), "%s", AtObjectToString((AtObject)dest));

        /* Log the call */
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                                file, line, "API: %s([%s], %s, %s)\r\n",
                                                function,
                                                AtObjectToString((AtObject)self), sourceStr, destStr);
        AtDriverLogUnLock();
        }
    }

static void OverrideAtObject(AtCrossConnect self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, DescriptionBuild);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtCrossConnect self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtCrossConnect self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, ChannelConnect);
        mMethodOverride(m_methods, ChannelDisconnect);
        mMethodOverride(m_methods, NumDestChannelsGet);
        mMethodOverride(m_methods, DestChannelGetByIndex);
        mMethodOverride(m_methods, SourceChannelGet);
        mMethodOverride(m_methods, AllConnectionsDelete);
        mMethodOverride(m_methods, HwConnect);
        mMethodOverride(m_methods, HwDisconnect);
        mMethodOverride(m_methods, ConnectCheck);
        mMethodOverride(m_methods, IsBlockingCrossConnect);
        mMethodOverride(m_methods, HwIsConnected);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCrossConnect);
    }

AtCrossConnect AtCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->module = module;

    return self;
    }

eAtRet AtCrossConnectHwConnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    if (self)
        return mMethodsGet(self)->HwConnect(self, source, dest);
    return cAtErrorNullPointer;
    }

eAtRet AtCrossConnectHwDisconnect(AtCrossConnect self, AtChannel dest)
    {
    if (self)
        return mMethodsGet(self)->HwDisconnect(self, dest);
    return cAtErrorNullPointer;
    }

eBool AtCrossConnectHwIsConnected(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    if (self)
        return mMethodsGet(self)->HwIsConnected(self, sourceVc, destVc);
    return cAtFalse;
    }

eAtRet AtCrossConnectVcDisconnectAllConnections(AtCrossConnect self, AtSdhChannel vc)
    {
    if (self)
        return VcDisconnectAllConnections(self, vc);
    return cAtErrorObjectNotExist;
    }

/**
 * @addtogroup AtCrossConnect
 * @{
 */

/**
 * Get cross-connect type description
 *
 * @param self This cross-connect engine
 *
 * @return cross-connect type description
 */
const char *AtCrossConnectTypeString(AtCrossConnect self)
    {
    if (self)
        return mMethodsGet(self)->TypeString(self);
    return NULL;
    }

/**
 * Get XC module
 *
 * @param self This cross-connect engine
 *
 * @return AT Module
 */
AtModule AtCrossConnectModuleGet(AtCrossConnect self)
    {
    if (self)
        return (AtModule)self->module;
    return NULL;
    }

/**
 * Create a connection from source and destination channels
 *
 * @param self This cross-connect engine
 * @param source Source channel
 * @param dest Destination channel
 *
 * @return AT return code
 */
eAtModuleXcRet AtCrossConnectChannelConnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtModuleXcRet ret = cAtErrorObjectNotExist;

    ChannelConnectDisconnectApiLogStart(self, source, dest, AtSourceLocationNone, AtFunction);
    if (self)
        ret = mMethodsGet(self)->ChannelConnect(self, source, dest);
    AtDriverApiLogStop();
    
    return ret;
    }

/**
 * Delete a connection from source and destination channels
 *
 * @param self This cross-connect engine
 * @param source Source channel
 * @param dest Destination channel
 *
 * @return AT return code
 */
eAtModuleXcRet AtCrossConnectChannelDisconnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtModuleXcRet ret = cAtErrorObjectNotExist;

    ChannelConnectDisconnectApiLogStart(self, source, dest, AtSourceLocationNone, AtFunction);
    if (self)
        ret = mMethodsGet(self)->ChannelDisconnect(self, source, dest);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get number of destination channels that the input source channel is connected to
 *
 * @param self This cross-connect engine
 * @param source Source channel
 *
 * @return Number of destination channels that the input source channel is connected to
 * @see AtCrossConnectDestChannelGetByIndex()
 */
uint32 AtCrossConnectNumDestChannelsGet(AtCrossConnect self, AtChannel source)
    {
    if (self)
        return mMethodsGet(self)->NumDestChannelsGet(self, source);
    return 0;
    }

/**
 * Get destination channel by index in list of destination channels that the
 * source channel is connected to
 *
 * @param self This cross-connect engine
 * @param source Source channel
 * @param destIndex Index in list of destination channels that the source channel is connected to.
 *
 * @return Destination channel or NULL if the index is invalid.
 */
AtChannel AtCrossConnectDestChannelGetByIndex(AtCrossConnect self, AtChannel source, uint32 destIndex)
    {
    if (self)
        return mMethodsGet(self)->DestChannelGetByIndex(self, source, destIndex);
    return NULL;
    }

/**
 * Get the source channel that is connected to the input destination channel
 *
 * @param self This cross-connect engine
 * @param dest Destination channel
 *
 * @return The source channel that is connected to the input destination channel.
 *         Or NULL if there is no such channel.
 */
AtChannel AtCrossConnectSourceChannelGet(AtCrossConnect self, AtChannel dest)
    {
    if (self)
        return mMethodsGet(self)->SourceChannelGet(self, dest);
    return NULL;
    }

/**
 * Delete all of connections
 *
 * @param self This cross-connect engine
 *
 * @return AT return code
 */
eAtRet AtCrossConnectAllConnectionsDelete(AtCrossConnect self)
    {
    mNoParamCall(AllConnectionsDelete, eAtRet, cAtErrorNullPointer);
    }

/**
 * @}
 */

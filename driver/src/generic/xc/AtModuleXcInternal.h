/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtModuleXcInternal.h
 * 
 * Created Date: May 9, 2014
 *
 * Description : XC module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEXCINTERNAL_H_
#define _ATMODULEXCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtModuleInternal.h"
#include "AtModuleXc.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleXcMethods
    {
    AtCrossConnectDs0 (*Ds0CrossConnectGet)(AtModuleXc self);
    AtCrossConnect (*De1CrossConnectGet)(AtModuleXc self);
    AtCrossConnect (*De3CrossConnectGet)(AtModuleXc self);
    AtCrossConnect (*VcCrossConnectGet)(AtModuleXc self);

    /* Internal methods */
    AtCrossConnect (*VcCrossConnectObjectCreate)(AtModuleXc self);
    }tAtModuleXcMethods;

typedef struct tAtModuleXc
    {
    tAtModule super;
    const tAtModuleXcMethods *methods;

    /* Private data */
    AtCrossConnect vcCrossConnect;
    }tAtModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXc AtModuleXcObjectInit(AtModuleXc self, AtDevice device);

#endif /* _ATMODULEXCINTERNAL_H_ */


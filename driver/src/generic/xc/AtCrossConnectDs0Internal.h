/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtCrossConnectDs0Internal.h
 * 
 * Created Date: Feb 7, 2015
 *
 * Description : DS0 cross-connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCROSSCONNECTDS0INTERNAL_H_
#define _ATCROSSCONNECTDS0INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCrossConnectDs0.h"
#include "AtCrossConnectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCrossConnectDs0Methods
    {
    eAtModuleXcRet (*Ds0Connect)(AtCrossConnectDs0 self,
                                 AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                 AtPdhDe1 destDe1, uint32 destTimeslot);
    eAtModuleXcRet (*Ds0Disconnect)(AtCrossConnectDs0 self,
                                    AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                    AtPdhDe1 destDe1, uint32 destTimeslot);

    uint32 (*Ds0NumDestChannelsGet)(AtCrossConnectDs0 self, AtPdhDe1 sourceDe1, uint8 timeslotId);
    AtPdhDe1 (*Ds0DestChannelGetByIndex)(AtCrossConnectDs0 self,
                                         AtPdhDe1 sourceDe1, uint32 timeslotId, uint32 destIndex,
                                         uint8 *destTimeslot);
    AtPdhDe1 (*Ds0SourceChannelGet)(AtCrossConnectDs0 self, AtPdhDe1 destDe1, uint8 destTimeslot, uint8 *sourceTimeslot);
    }tAtCrossConnectDs0Methods;

typedef struct tAtCrossConnectDs0
    {
    tAtObject super;
    const tAtCrossConnectDs0Methods *methods;

    /* Private data */
    AtModuleXc module;
    }tAtCrossConnectDs0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnectDs0 AtCrossConnectDs0ObjectInit(AtCrossConnectDs0 self, AtModuleXc module);

#ifdef __cplusplus
}
#endif
#endif /* _ATCROSSCONNECTDS0INTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : AtCrossConnectDs0.c
 *
 * Created Date: Feb 7, 2015
 *
 * Description : DS0 cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCrossConnectDs0Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtCrossConnectDs0Methods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleXcRet Ds0Connect(AtCrossConnectDs0 self,
                                 AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                 AtPdhDe1 destDe1, uint32 destTimeslot)
    {
	AtUnused(destTimeslot);
	AtUnused(destDe1);
	AtUnused(sourceTimeslot);
	AtUnused(sourceDe1);
	AtUnused(self);
    /* Concrete must know */
    return cAtErrorNotImplemented;
    }

static eAtModuleXcRet Ds0Disconnect(AtCrossConnectDs0 self,
                                    AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                    AtPdhDe1 destDe1, uint32 destTimeslot)
    {
	AtUnused(destTimeslot);
	AtUnused(destDe1);
	AtUnused(sourceTimeslot);
	AtUnused(sourceDe1);
	AtUnused(self);
    /* Concrete must know */
    return cAtErrorNotImplemented;
    }

static uint32 Ds0NumDestChannelsGet(AtCrossConnectDs0 self, AtPdhDe1 sourceDe1, uint8 timeslotId)
    {
	AtUnused(timeslotId);
	AtUnused(sourceDe1);
	AtUnused(self);
    /* Concrete must know */
    return 0;
    }

static AtPdhDe1 Ds0DestChannelGetByIndex(AtCrossConnectDs0 self,
                                     AtPdhDe1 sourceDe1, uint32 timeslotId, uint32 destIndex,
                                     uint8 *destTimeslot)
    {
	AtUnused(destTimeslot);
	AtUnused(destIndex);
	AtUnused(timeslotId);
	AtUnused(sourceDe1);
	AtUnused(self);
    /* Concrete must know */
    return NULL;
    }

static AtPdhDe1 Ds0SourceChannelGet(AtCrossConnectDs0 self, AtPdhDe1 destDe1, uint8 destTimeslot, uint8 *sourceTimeslot)
    {
	AtUnused(sourceTimeslot);
	AtUnused(destTimeslot);
	AtUnused(destDe1);
	AtUnused(self);
    /* Concrete must know */
    return NULL;
    }

static void MethodsInit(AtCrossConnectDs0 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Ds0Connect);
        mMethodOverride(m_methods, Ds0Disconnect);
        mMethodOverride(m_methods, Ds0NumDestChannelsGet);
        mMethodOverride(m_methods, Ds0DestChannelGetByIndex);
        mMethodOverride(m_methods, Ds0SourceChannelGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCrossConnectDs0);
    }

AtCrossConnectDs0 AtCrossConnectDs0ObjectInit(AtCrossConnectDs0 self, AtModuleXc module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);

    /* Save private data */
    self->module = module;

    return self;
    }

/**
 * @addtogroup AtCrossConnectDs0
 * @{
 */

/**
 * Connect two DS0 timeslots
 *
 * @param self This cross-connect
 * @param sourceDe1 Source DS1/E1
 * @param sourceTimeslot Source DS0 timeslot
 * @param destDe1 Destination DS1/E1
 * @param destTimeslot Destination DS0 timeslot
 *
 * @return AT return code
 */
eAtModuleXcRet AtCrossConnectDs0Connect(AtCrossConnectDs0 self,
                                        AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                        AtPdhDe1 destDe1, uint32 destTimeslot)
    {
    if (self)
        return mMethodsGet(self)->Ds0Connect(self, sourceDe1, sourceTimeslot, destDe1, destTimeslot);
    return cAtError;
    }

/**
 * Disconnect two DS0 timeslots
 *
 * @param self This cross-connect
 * @param sourceDe1 Source DS1/E1
 * @param sourceTimeslot Source DS0 timeslot
 * @param destDe1 Destination DS1/E1
 * @param destTimeslot Destination DS0 timeslot
 *
 * @return AT return code
 */
eAtModuleXcRet AtCrossConnectDs0Disconnect(AtCrossConnectDs0 self,
                                           AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                           AtPdhDe1 destDe1, uint32 destTimeslot)
    {
    if (self)
        return mMethodsGet(self)->Ds0Disconnect(self, sourceDe1, sourceTimeslot, destDe1, destTimeslot);
    return cAtError;
    }

/**
 * Get number of destination DS0s that the input source DS0 is connected to
 *
 * @param self This cross-connect
 * @param sourceDe1 Source DS1/E1
 * @param sourceTimeslotId Source DS0 timeslot
 *
 * @return Number of destination DS0s
 */
uint32 AtCrossConnectDs0NumDestChannelsGet(AtCrossConnectDs0 self, AtPdhDe1 sourceDe1, uint8 sourceTimeslotId)
    {
    if (self)
        return mMethodsGet(self)->Ds0NumDestChannelsGet(self, sourceDe1, sourceTimeslotId);
    return 0;
    }

/**
 * Get destination DS0 by index in list of destination channels that the source
 * DS0 is connected to
 *
 * @param self This cross-connect
 * @param sourceDe1 Source DS1/E1
 * @param sourceTimeslotId Source DS0 timeslot
 * @param destIndex Index in list of destination channels that the source channel is connected to.
 * @param [out] destTimeslot Destination timeslot
 *
 * @return DS1/E1 containing destination timeslot.
 */
AtPdhDe1 AtCrossConnectDs0DestChannelGetByIndex(AtCrossConnectDs0 self,
                                                AtPdhDe1 sourceDe1, uint32 sourceTimeslotId, uint32 destIndex,
                                                uint8 *destTimeslot)
    {
    if (self)
        return mMethodsGet(self)->Ds0DestChannelGetByIndex(self, sourceDe1, sourceTimeslotId, destIndex, destTimeslot);
    return NULL;
    }

/**
 * Get the source DS0 that is connected to the input destination DS0
 *
 * @param self This cross-connect
 * @param destDe1 Destination DS0
 * @param destTimeslot Destination timeslot.
 * @param [out] sourceTimeslot The source DS0 that is connected to the input destination DS0.
 *
 * @return DS1/E1 containing source timeslot.
 */
AtPdhDe1 AtCrossConnectDs0SourceChannelGet(AtCrossConnectDs0 self, AtPdhDe1 destDe1, uint8 destTimeslot, uint8 *sourceTimeslot)
    {
    if (self)
        return mMethodsGet(self)->Ds0SourceChannelGet(self, destDe1, destTimeslot, sourceTimeslot);
    return 0;
    }

/**
 * @}
 */

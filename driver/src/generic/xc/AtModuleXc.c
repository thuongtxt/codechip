/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : AtModuleXc.c
 *
 * Created Date: May 9, 2014
 *
 * Description : XC Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleXcInternal.h"
#include "AtCrossConnect.h"

/*--------------------------- Define -----------------------------------------*/
#define mModuleIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleXc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleXcMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "xc";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModuleXc xcModule = (AtModuleXc)self;
    AtOsal osal = AtSharedDriverOsalGet();
    static char capacityDescription[32];

    mMethodsGet(osal)->MemInit(osal, capacityDescription, 0, sizeof(capacityDescription));
    if (AtModuleXcDe1CrossConnectGet(xcModule))
        AtStrcat(capacityDescription, "de1 ");
    if (AtModuleXcDe3CrossConnectGet(xcModule))
        AtStrcat(capacityDescription, "de3 ");
    if (AtModuleXcVcCrossConnectGet(xcModule))
        AtStrcat(capacityDescription, "vc ");
    if (AtModuleXcDs0CrossConnectGet(xcModule))
        AtStrcat(capacityDescription, "ds0 ");

    return capacityDescription;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->vcCrossConnect);
    mThis(self)->vcCrossConnect = NULL;

    m_AtObjectMethods->Delete(self);
    }

static AtCrossConnect De1CrossConnectGet(AtModuleXc self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtCrossConnect De3CrossConnectGet(AtModuleXc self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtCrossConnect VcCrossConnectGet(AtModuleXc self)
    {
    return self->vcCrossConnect;
    }

static AtCrossConnectDs0 Ds0CrossConnectGet(AtModuleXc self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtRet Init(AtModule self)
    {
    AtCrossConnect crossConnect;
    AtModuleXc xcModule = (AtModuleXc)self;
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    crossConnect = AtModuleXcDe1CrossConnectGet(xcModule);
    if (crossConnect)
        AtCrossConnectAllConnectionsDelete(crossConnect);

    crossConnect = AtModuleXcDe3CrossConnectGet(xcModule);
    if (crossConnect)
        AtCrossConnectAllConnectionsDelete(crossConnect);

    crossConnect = AtModuleXcVcCrossConnectGet(xcModule);
    if (crossConnect)
        AtCrossConnectAllConnectionsDelete(crossConnect);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (mThis(self)->vcCrossConnect == NULL)
        mThis(self)->vcCrossConnect = mMethodsGet(mThis(self))->VcCrossConnectObjectCreate(mThis(self));

    return cAtOk;
    }

static AtCrossConnect VcCrossConnectObjectCreate(AtModuleXc self)
    {
    AtUnused(self);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleXc object = (AtModuleXc)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(vcCrossConnect);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleXc);
    }

static void MethodsInit(AtModuleXc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Ds0CrossConnectGet);
        mMethodOverride(m_methods, De1CrossConnectGet);
        mMethodOverride(m_methods, De3CrossConnectGet);
        mMethodOverride(m_methods, VcCrossConnectGet);
        mMethodOverride(m_methods, VcCrossConnectObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModuleXc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleXc self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleXc self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModuleXc AtModuleXcObjectInit(AtModuleXc self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleXc, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModuleXc
 * @{
 */

/**
 * Get cross-connect controller to connect/disconnect two @ref AtPdhDe1 "DE1s".
 *
 * @param self This module
 *
 * @return Cross-connect controller.
 */
AtCrossConnect AtModuleXcDe1CrossConnectGet(AtModuleXc self)
    {
    mNoParamObjectGet(De1CrossConnectGet, AtCrossConnect);
    }

/**
 * Get cross-connect controller to connect/disconnect two @ref AtPdhDe3 "DE3s".
 *
 * @param self This module
 *
 * @return Cross-connect controller.
 */
AtCrossConnect AtModuleXcDe3CrossConnectGet(AtModuleXc self)
    {
    mNoParamObjectGet(De3CrossConnectGet, AtCrossConnect);
    }

/**
 * Get cross-connect controller to connect/disconnect two @ref AtSdhVc "SDH VCs".
 *
 * @param self This module
 *
 * @return Cross-connect controller.
 */
AtCrossConnect AtModuleXcVcCrossConnectGet(AtModuleXc self)
    {
    mNoParamObjectGet(VcCrossConnectGet, AtCrossConnect);
    }

/**
 * Get cross-connect controller to connect/disconnect two DS0 timeslots.
 *
 * @param self This module
 *
 * @return Cross-connect controller.
 */
AtCrossConnectDs0 AtModuleXcDs0CrossConnectGet(AtModuleXc self)
    {
    mNoParamObjectGet(Ds0CrossConnectGet, AtCrossConnectDs0);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtCrossConnectInternal.h
 * 
 * Created Date: Aug 1, 2014
 *
 * Description : Cross-connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCROSSCONNECTINTERNAL_H_
#define _ATCROSSCONNECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtCrossConnect.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCrossConnectMethods
    {
    const char *(*TypeString)(AtCrossConnect self);
    eAtModuleXcRet (*ChannelConnect)(AtCrossConnect self, AtChannel source, AtChannel dest);
    eAtModuleXcRet (*ChannelDisconnect)(AtCrossConnect self, AtChannel source, AtChannel dest);
    uint32 (*NumDestChannelsGet)(AtCrossConnect self, AtChannel source);
    AtChannel (*DestChannelGetByIndex)(AtCrossConnect self, AtChannel source, uint32 destIndex);
    AtChannel (*SourceChannelGet)(AtCrossConnect self, AtChannel dest);
    eAtRet (*AllConnectionsDelete)(AtCrossConnect self);

    /* Internal */
    eAtRet (*HwConnect)(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc);
    eAtRet (*HwDisconnect)(AtCrossConnect self, AtChannel dest);
    eBool (*HwIsConnected)(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc);
    eAtRet (*ConnectCheck)(AtCrossConnect self, AtChannel source, AtChannel dest);
    eBool (*IsBlockingCrossConnect)(AtCrossConnect self);
    }tAtCrossConnectMethods;

typedef struct tAtCrossConnect
    {
    tAtObject super;
    const tAtCrossConnectMethods *methods;

    /* Private data */
    AtModuleXc module;
    }tAtCrossConnect;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnect AtCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module);

eAtRet AtCrossConnectHwConnect(AtCrossConnect self, AtChannel source, AtChannel dest);
eAtRet AtCrossConnectHwDisconnect(AtCrossConnect self, AtChannel dest);
eBool AtCrossConnectHwIsConnected(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc);
eAtRet AtCrossConnectVcDisconnectAllConnections(AtCrossConnect self, AtSdhChannel vc);

#endif /* _ATCROSSCONNECTINTERNAL_H_ */


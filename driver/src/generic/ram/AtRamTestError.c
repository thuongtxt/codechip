/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtRamTestError.c
 *
 * Created Date: Mar 10, 2015
 *
 * Description : Ram test error implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRamTestErrorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtRamTestErrorMethods m_methods;
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *Description(AtRamTestError self)
    {
    static char string[64];

    AtSprintf(string, "Write 0x%08X to address 0x%08X but get 0x%08X", self->expectedValue, self->errorAddress, self->actualValue);
    return string;
    }

/* Initialize implementation structure */
static void MethodsInit(AtRamTestError self)
    {
    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Description);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof (tAtRamTestError);
    }

AtRamTestError AtRamTestErrorObjectInit(AtRamTestError self, uint32 errorAddress, uint32 expectedValue, uint32 actualValue)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->errorAddress  = errorAddress;
    self->expectedValue = expectedValue;
    self->actualValue   = actualValue;

    return self;
    }

AtRamTestError AtRamTestErrorNew(uint32 errorAddress, uint32 expectedValue, uint32 actualValue)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRamTestError newError = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newError == NULL)
        return NULL;

    /* Construct it */
    return AtRamTestErrorObjectInit(newError, errorAddress, expectedValue, actualValue);
    }

/**
 * @addtogroup AtRam
 * @{
 */

/**
 * Return description of an error
 *
 * @param self This Error
 * @return Error description string
 */
const char* AtRamTestErrorDescriptionGet(AtRamTestError self)
    {
    if (self)
        return mMethodsGet(self)->Description(self);

    return NULL;
    }

/**
 * Get error address
 *
 * @param self This error
 * @return Address that error happened
 */
uint32 AtRamTestErrorAddressGet(AtRamTestError self)
    {
    if (self)
        return self->errorAddress;

    return 0xCAFECAFE;
    }

/**
 * Get expected value
 *
 * @param self This error
 * @return Expected value
 */
uint32 AtRamTestErrorExpectedValueGet(AtRamTestError self)
    {
    if (self)
        return self->expectedValue;

    return 0xCAFECAFE;
    }

/**
 * Get actual gotten value
 *
 * @param self This error
 * @return Actual gotten value
 */
uint32 AtRamTestErrorActualValueGet(AtRamTestError self)
    {
    if (self)
        return self->actualValue;

    return 0xCAFECAFE;
    }

/** @} */

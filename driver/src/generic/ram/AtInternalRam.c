/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtInternalRam.c
 *
 * Created Date: Dec 4, 2015
 *
 * Description : Internal RAM implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDeviceInternal.h"
#include "../man/AtModuleInternal.h"
#include "AtInternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtInternalRamMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ParityMonitorEnable(AtInternalRam self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool ParityMonitorIsEnabled(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet EccMonitorEnable(AtInternalRam self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool EccMonitorIsEnabled(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool EccMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet CrcMonitorEnable(AtInternalRam self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool CrcMonitorIsEnabled(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CrcMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ErrorForce(AtInternalRam self, uint32 errors)
    {
    AtUnused(self);
    AtUnused(errors);
    return cAtErrorModeNotSupport;
    }

static eAtRet ErrorUnForce(AtInternalRam self, uint32 errors)
    {
    AtUnused(self);
    AtUnused(errors);
    return cAtErrorModeNotSupport;
    }

static uint32 ForcedErrorsGet(AtInternalRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ErrorHistoryGet(AtInternalRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ErrorHistoryClear(AtInternalRam self)
    {
    AtUnused(self);
    return 0;
    }

static const char* Description(AtInternalRam self)
    {
    AtUnused(self);
    return "\0";
    }

static eBool IsReserved(AtInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet(self);
    return AtModuleInternalRamIsReserved(module, self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtInternalRam object = (AtInternalRam)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(phyModule);
    mEncodeUInt(ramId);
    mEncodeUInt(localId);
    }

static AtErrorGenerator ErrorGeneratorGet(AtInternalRam self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0x0;
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0x0;
    }

static const char* Name(AtInternalRam self)
    {
    AtUnused(self);
    return "Internal RAM";
    }

static void StatusClear(AtInternalRam self)
    {
    AtInternalRamErrorHistoryClear(self);
    }

static const char *ToString(AtObject self)
    {
    return AtInternalRamDescription((AtInternalRam)self);
    }

static eBool CrcMonitorCanEnable(AtInternalRam self, eBool enable)
    {
    /* Let concrete determine, so far, all of them can be enabled/disabled */
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eBool EccMonitorCanEnable(AtInternalRam self, eBool enable)
    {
    /* Let concrete determine, so far, all of them can be enabled/disabled */
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static void OverrideAtObject(AtInternalRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ParityMonitorEnable);
        mMethodOverride(m_methods, ParityMonitorIsEnabled);
        mMethodOverride(m_methods, ParityMonitorIsSupported);
        mMethodOverride(m_methods, EccMonitorEnable);
        mMethodOverride(m_methods, EccMonitorIsEnabled);
        mMethodOverride(m_methods, EccMonitorIsSupported);
        mMethodOverride(m_methods, EccMonitorCanEnable);
        mMethodOverride(m_methods, CrcMonitorEnable);
        mMethodOverride(m_methods, CrcMonitorIsEnabled);
        mMethodOverride(m_methods, CrcMonitorIsSupported);
        mMethodOverride(m_methods, CrcMonitorCanEnable);
        mMethodOverride(m_methods, ErrorForce);
        mMethodOverride(m_methods, ErrorUnForce);
        mMethodOverride(m_methods, ForcedErrorsGet);
        mMethodOverride(m_methods, ForcableErrorsGet);
        mMethodOverride(m_methods, ErrorHistoryClear);
        mMethodOverride(m_methods, ErrorHistoryGet);
        mMethodOverride(m_methods, Description);
        mMethodOverride(m_methods, IsReserved);
        mMethodOverride(m_methods, ErrorGeneratorGet);
        mMethodOverride(m_methods, CounterTypeIsSupported);
        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, Name);
        mMethodOverride(m_methods, StatusClear);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtInternalRam);
    }

AtInternalRam AtInternalRamObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private attributes */
    self->localId = localId;
    self->ramId = ramId;
    self->phyModule = phyModule;

    return self;
    }

AtModule AtInternalRamPhyModuleGet(AtInternalRam self)
    {
    if (self)
        return self->phyModule;
    return NULL;
    }

uint32 AtInternalRamLocalIdGet(AtInternalRam self)
    {
    if (self)
        return self->localId;
    return cBit31_0;
    }

const char* AtInternalRamName(AtInternalRam self)
    {
    if (self)
        return mMethodsGet(self)->Name(self);
    return NULL;
    }

void AtInternalRamStatusClear(AtInternalRam self)
    {
    if (self)
        mMethodsGet(self)->StatusClear(self);
    }

/*
 * Check if CRC monitoring can be enabled/disabled
 *
 * @param self This RAM
 * @param enable Testing
 *               - cAtTrue to check if CRC monitoring can be enabled
 *               - cAtFalse to check if CRC monitoring can be disabled
 *
 * @retval cAtTrue if it can be enabled/disabled
 * @retval cAtFalse if it can not be enabled/disabled
 */
eBool AtInternalRamCrcMonitorCanEnable(AtInternalRam self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CrcMonitorCanEnable(self, enable);
    return cAtFalse;
    }

eBool AtInternalRamEccMonitorCanEnable(AtInternalRam self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->EccMonitorCanEnable(self, enable);
    return cAtFalse;
    }

/**
 * @addtogroup AtInternalRam
 * @{
 */

/**
 * Enable parity monitoring on internal RAM
 *
 * @param self This internal RAM
 * @param enable Enable/disable parity monitoring.
 *
 * @return AT return code.
 */
eAtRet AtInternalRamParityMonitorEnable(AtInternalRam self, eBool enable)
    {
    mNumericalAttributeSet(ParityMonitorEnable, enable);
    }

/**
 * Check if parity monitoring is enabled on internal RAM
 *
 * @param self This internal RAM
 *
 * @retval cAtTrue Parity monitoring is enabled.
 * @retval cAtFalse Parity monitoring is disabled.
 */
eBool AtInternalRamParityMonitorIsEnabled(AtInternalRam self)
    {
    mAttributeGet(ParityMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if parity monitoring is supported on internal RAM
 *
 * @param self This internal RAM
 *
 * @retval cAtTrue Parity monitoring is supported.
 * @retval cAtFalse Parity monitoring is unsupported.
 */
eBool AtInternalRamParityMonitorIsSupported(AtInternalRam self)
    {
    mAttributeGet(ParityMonitorIsSupported, eBool, cAtFalse);
    }

/**
 * Enable ECC monitoring on internal RAM
 *
 * @param self This internal RAM
 * @param enable Enable/disable ECC monitoring.
 *
 * @return AT return code.
 */
eAtRet AtInternalRamEccMonitorEnable(AtInternalRam self, eBool enable)
    {
    mNumericalAttributeSet(EccMonitorEnable, enable);
    }

/**
 * Check if ECC monitoring is enabled on internal RAM
 *
 * @param self This internal RAM
 *
 * @retval cAtTrue ECC monitoring is enabled.
 * @retval cAtFalse ECC monitoring is disabled.
 */
eBool AtInternalRamEccMonitorIsEnabled(AtInternalRam self)
    {
    mAttributeGet(EccMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if ECC monitoring is supported on internal RAM
 *
 * @param self This internal RAM
 *
 * @retval cAtTrue ECC monitoring is supported.
 * @retval cAtFalse ECC monitoring is not supported.
 */
eBool AtInternalRamEccMonitorIsSupported(AtInternalRam self)
    {
    mAttributeGet(EccMonitorIsSupported, eBool, cAtFalse);
    }

/**
 * Enable CRC monitoring on internal RAM
 *
 * @param self This internal RAM
 * @param enable Enable/disable CRC monitoring.
 *
 * @return AT return code.
 */
eAtRet AtInternalRamCrcMonitorEnable(AtInternalRam self, eBool enable)
    {
    mNumericalAttributeSet(CrcMonitorEnable, enable);
    }

/**
 * Check if CRC monitoring is enabled on internal RAM
 *
 * @param self This internal RAM
 *
 * @retval cAtTrue CRC monitoring is enabled.
 * @retval cAtFalse CRC monitoring is disabled.
 */
eBool AtInternalRamCrcMonitorIsEnabled(AtInternalRam self)
    {
    mAttributeGet(CrcMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if CRC monitoring is supported on internal RAM
 *
 * @param self This internal RAM
 *
 * @retval cAtTrue CRC monitoring is supported.
 * @retval cAtFalse CRC monitoring is unsupported.
 */
eBool AtInternalRamCrcMonitorIsSupported(AtInternalRam self)
    {
    mAttributeGet(CrcMonitorIsSupported, eBool, cAtFalse);
    }

/**
 * Force error on internal RAM
 *
 * @param self This internal RAM
 * @param errors Errors to be forced @ref eAtRamAlarm "RAM errors."
 *
 * @return AT return code
 */
eAtRet AtInternalRamErrorForce(AtInternalRam self, uint32 errors)
    {
    mNumericalAttributeSet(ErrorForce, errors);
    }

/**
 * Unforce error on internal RAM
 *
 * @param self This internal RAM
 * @param errors Errors to be unforced @ref eAtRamAlarm "RAM errors."
 *
 * @return AT return code
 */
eAtRet AtInternalRamErrorUnForce(AtInternalRam self, uint32 errors)
    {
    mNumericalAttributeSet(ErrorUnForce, errors);
    }

/**
 * Get forced error on internal RAM
 *
 * @param self This internal RAM
 *
 * @return Forced errors @ref eAtRamAlarm "RAM errors."
 */
uint32 AtInternalRamForcedErrorsGet(AtInternalRam self)
    {
    mAttributeGet(ForcedErrorsGet, uint32, 0);
    }

/**
 * Get forcable error on internal RAM
 *
 * @param self This internal RAM
 *
 * @return Forced errors @ref eAtRamAlarm "RAM errors."
 */
uint32 AtInternalRamForcableErrorsGet(AtInternalRam self)
    {
    mAttributeGet(ForcableErrorsGet, uint32, 0);
    }

/**
 * Get error history on internal RAM
 *
 * @param self This internal RAM
 *
 * @return Error history @ref eAtRamAlarm "RAM errors."
 */
uint32 AtInternalRamErrorHistoryGet(AtInternalRam self)
    {
    mAttributeGet(ErrorHistoryGet, uint32, 0);
    }

/**
 * Get and clear error history on internal RAM
 *
 * @param self This internal RAM
 *
 * @return Error history @ref eAtRamAlarm "RAM errors."
 */
uint32 AtInternalRamErrorHistoryClear(AtInternalRam self)
    {
    mAttributeGet(ErrorHistoryClear, uint32, 0);
    }

/**
 * Get internal RAM description
 *
 * @param self This internal RAM
 *
 * @return Description string.
 */
const char* AtInternalRamDescription(AtInternalRam self)
    {
    mAttributeGet(Description, const char*, NULL);
    }

/**
 * Get internal RAM identifier
 *
 * @param self This internal RAM
 *
 * @return 32-bit number identifier.
 */
uint32 AtInternalRamIdGet(AtInternalRam self)
    {
    if (self)
        return self->ramId;
    return cBit31_0;
    }

/**
 * Get RAM module from internal RAM
 *
 * @param self This internal RAM
 *
 * @return RAM module @ref AtModuleRam "RAM module"
 */
AtModuleRam AtInternalRamModuleRamGet(AtInternalRam self)
    {
    if (self)
        return (AtModuleRam)AtDeviceModuleGet(AtModuleDeviceGet(self->phyModule), cAtModuleRam);
    return NULL;
    }

/**
 * Get error generator
 *
 * @param self This internal RAM
 *
 * @return Error generator
 */
AtErrorGenerator AtInternalRamErrorGeneratorGet(AtInternalRam self)
    {
    mAttributeGet(ErrorGeneratorGet, AtErrorGenerator, NULL);
    }

/**
 * Check if Counter type is supported or not
 *
 * @param self This internal RAM
 * @param counterType ref eAtRamCounterType "RAM counter type."
 *
 * @return cAtTrue if it is supported
 */
eBool  AtInternalRamCounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterTypeIsSupported(self, counterType);
    return cAtFalse;
    }

/**
 * Get counters error on internal RAM only
 *
 * @param self This internal RAM
 * @param counterType ref eAtRamCounterType "RAM counter type."
 *
 * @return number accumulation of this counter type
 */
uint32 AtInternalRamCounterGet(AtInternalRam self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterGet(self, counterType);
    return 0x0;
    }

/**
 * Get counters error on internal RAM, then clear
 *
 * @param self This internal RAM
 * @param counterType ref eAtRamCounterType "RAM counter type."
 *
 * @return number accumulation of this counter type
 */
uint32 AtInternalRamCounterClear(AtInternalRam self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterClear(self, counterType);
    return 0x0;
    }

/**
 * Check if RAM is reserved
 *
 * @param self This RAM
 *
 * @retval cAtTrue if it is reserved
 * @retval cAtFalse if it is not reserved
 *
 * @note Accessing status of reserved RAM will have undetermined value returned
 */
eBool AtInternalRamIsReserved(AtInternalRam self)
    {
    if (self)
        return mMethodsGet(self)->IsReserved(self);
    return cAtFalse;
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtModuleRam.c
 *
 * Created Date: Jan 31, 2013
 *
 * Description : RAM module generic implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleRamInternal.h"
#include "AtRamInternal.h"
#include "AtDevice.h"
#include "AtInternalRamInternal.h"
#include "../man/AtDeviceInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleIsValid(self) (self == NULL) ? cAtFalse : cAtTrue
#define mThis(self) ((AtModuleRam)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleRamMethods m_methods;

static tAtModuleMethods m_AtModuleOverride;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DeleteRams(AtRam **rams, uint8 numRams)
    {
    uint8 i;
    AtOsal osal = AtSharedDriverOsalGet();

    if (*rams == NULL)
        return;

    if (numRams == 0)
        return;

    /* Delete each RAM */
    for (i = 0; i < numRams; i++)
        AtObjectDelete((AtObject)((*rams)[i]));

    /* And this memory */
    mMethodsGet(osal)->MemFree(osal, *rams);
    *rams = NULL;
    }

static eBool ShouldInitRam(AtModuleRam self)
    {
    AtModule module = (AtModule)self;

    if (AtDeviceIpCoreHalGet(AtModuleDeviceGet(module), 0) == NULL)
        return cAtFalse;

    if (AtDeviceTestbenchIsEnabled(AtModuleDeviceGet(module)))
        return cAtFalse;

    return cAtTrue;
    }

static void CreateRams(AtModuleRam module,
                       AtRam **rams, uint8 numRams,
                       AtRam (*ramCreateFunc)(AtModuleRam self, AtIpCore core, uint8 ramId),
                       AtIpCore (*coreOfRamFunc)(AtModuleRam self, uint8 ramId),
                       eBool (*ramIsUsedFunc)(AtModuleRam self, uint8 ramId))
    {
    uint8 i;
    AtOsal osal = AtSharedDriverOsalGet();
    eBool canInit;

    /* No RAMs need to be created */
    if (numRams == 0)
        return;

    /* Create each RAM */
    canInit = ShouldInitRam(module);
    *rams = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtRam) * numRams);
    for (i = 0; i < numRams; i++)
        {
        AtRam newRam = ramCreateFunc(module, coreOfRamFunc(module, i), i);
        eBool ramIsUsed = cAtTrue;

        (*rams)[i] = newRam;

        if (ramIsUsedFunc)
            ramIsUsed = ramIsUsedFunc(module, i);

        if (canInit && ramIsUsed)
            AtRamInit(newRam);
        }
    }

static eBool DdrIdIsValid(AtModuleRam self, uint8 ddrId)
    {
    return (ddrId < AtModuleRamNumDdrGet(self)) ? cAtTrue : cAtFalse;
    }

static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
	AtUnused(ddrId);
	AtUnused(core);
	AtUnused(self);
    /* Let sub class do */
    return NULL;
    }

static uint8 NumDdrGet(AtModuleRam self)
    {
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static void DeleteAllDdrs(AtModuleRam self)
    {
    DeleteRams(&(self->ddrs), self->numDdrs);
    }

static void CreateAllDdrs(AtModuleRam self)
    {
    DeleteRams(&(self->ddrs), self->numDdrs);

    self->numDdrs = AtModuleRamNumDdrGet((AtModuleRam)self);
    CreateRams(self, &(self->ddrs),
               self->numDdrs,
               mMethodsGet(self)->DdrCreate,
               mMethodsGet(self)->CoreOfDdr,
               mMethodsGet(self)->DdrIsUsed);
    }

static AtIpCore DefaultCore(AtModuleRam self)
    {
    AtModule module = (AtModule)self;
    return AtDeviceIpCoreGet(AtModuleDeviceGet(module), AtModuleDefaultCoreGet(module));
    }

static AtIpCore CoreOfDdr(AtModuleRam self, uint8 ddrId)
    {
	AtUnused(ddrId);
	return DefaultCore(self);
    }

static eBool ZbtIdIsValid(AtModuleRam self, uint8 ramId)
    {
    return (ramId < AtModuleRamNumZbtGet(self)) ? cAtTrue : cAtFalse;
    }

static AtRam ZbtCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
	AtUnused(ddrId);
	AtUnused(core);
	AtUnused(self);
    /* Let sub class do */
    return NULL;
    }

static uint8 NumZbtGet(AtModuleRam self)
    {
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static void DeleteAllZbts(AtModuleRam self)
    {
    DeleteRams(&(self->zbts), self->numZbts);
    }

static void CreateAllZbts(AtModuleRam self)
    {
    DeleteRams(&(self->zbts), self->numZbts);

    self->numZbts = AtModuleRamNumZbtGet((AtModuleRam)self);
    CreateRams(self, &(self->zbts),
               self->numZbts,
               mMethodsGet(self)->ZbtCreate, mMethodsGet(self)->CoreOfZbt, NULL);
    }

static AtIpCore CoreOfZbt(AtModuleRam self, uint8 ramId)
    {
    AtUnused(ramId);
    return DefaultCore(self);
    }

static void DeleteAllQdrs(AtModuleRam self)
    {
    DeleteRams(&(self->qdrs), self->numQdrs);
    }

static void CreateAllQdrs(AtModuleRam self)
    {
    DeleteRams(&(self->qdrs), self->numQdrs);

    self->numQdrs = AtModuleRamNumQdrGet((AtModuleRam)self);
    CreateRams(self, &(self->qdrs),
               self->numQdrs,
               mMethodsGet(self)->QdrCreate, mMethodsGet(self)->CoreOfQdr,
               NULL);
    }

static uint8 NumQdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 0;
    }

static AtRam QdrCreate(AtModuleRam self, AtIpCore core, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(core);
    AtUnused(qdrId);
    return NULL;
    }

static AtIpCore CoreOfQdr(AtModuleRam self, uint8 qdrId)
    {
    AtUnused(qdrId);
    return DefaultCore(self);
    }

static eBool QdrIdIsValid(AtModuleRam self, uint8 ramId)
    {
    return (ramId < AtModuleRamNumQdrGet(self)) ? cAtTrue : cAtFalse;
    }

static void DeleteAllRams(AtModuleRam self)
    {
    DeleteAllDdrs(self);
    DeleteAllZbts(self);
    DeleteAllQdrs(self);
    }

static void CreateAllRams(AtModuleRam self)
    {
    CreateAllDdrs(self);
    CreateAllZbts(self);
    CreateAllQdrs(self);
    }

static eAtRet Setup(AtModule self)
    {
    /* Let's the super do first */
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    CreateAllRams((AtModuleRam)self);

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    /* Let super do first */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    CreateAllRams((AtModuleRam)self);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    DeleteAllRams((AtModuleRam)self);

    m_AtObjectMethods->Delete(self);
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "ram";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModuleRam moduleRam = (AtModuleRam)self;
    static char string[128];

    AtSnprintf(string, sizeof(string) - 1, "ddrs: %d, zbts: %d, qdrs: %d, internal ram: %d",
               AtModuleRamNumDdrGet(moduleRam),
               AtModuleRamNumZbtGet(moduleRam),
               AtModuleRamNumQdrGet(moduleRam),
               AtModuleRamNumInternalRams(moduleRam));

    return string;
    }

static uint32 NumInternalRams(AtModuleRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DdrUserClock(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 0;
    }

static const char *DdrTypeString(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return "DDR";
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 25;
    }

static uint32 DdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 16;
    }

static uint32 QdrUserClock(AtModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(qdrId);
    return 0;
    }

static AtInternalRam InternalRamGet(AtModuleRam self, uint32 ramId)
    {
    AtUnused(self);
    AtUnused(ramId);
    return NULL;
    }

static uint32 StartInternalRamIdOfModuleGet(AtModuleRam self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return 0;
    }

static AtModuleEventListenerWrapper EventListenerWrapperObjectInit(AtModuleEventListenerWrapper wrapper, AtModule module, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRamEventListenerWrapper thisWrapper = (AtModuleRamEventListenerWrapper)wrapper;

    mMethodsGet(osal)->MemInit(osal, wrapper, 0, sizeof(tAtModuleRamEventListenerWrapper));

    AtModuleEventListenerWrapperObjectInit(wrapper, module, listener, userData);
    mMethodsGet(osal)->MemCpy(osal, &(thisWrapper->listener), listener, sizeof(tAtModuleRamEventListener));

    return wrapper;
    }

static AtModuleEventListenerWrapper EventListenerWrapperNew(AtModule self, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEventListenerWrapper newListener = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtModuleRamEventListenerWrapper));

    if (newListener)
        return EventListenerWrapperObjectInit(newListener, self, listener, userData);

    return NULL;
    }

static AtModuleEventListenerWrapper ListenerWrapperCreate(AtModule self, void* listener, void* userData)
    {
    return EventListenerWrapperNew(self, listener, userData);
    }

static eBool EventListenersAreIdentical(AtModule self, AtModuleEventListenerWrapper registeredListener, void* listener)
    {
    tAtModuleRamEventListener *input = (tAtModuleRamEventListener *)listener;
    AtModuleRamEventListenerWrapper local = (AtModuleRamEventListenerWrapper)registeredListener;
    AtUnused(self);

    if (input->ErrorNotify != local->listener.ErrorNotify)
        return cAtFalse;

    return cAtTrue;
    }

static void EventListenerCall(AtModuleRamEventListenerWrapper self, AtInternalRam ram, uint32 errors)
    {
    if (self->listener.ErrorNotify)
        self->listener.ErrorNotify(ram, errors, ((AtModuleEventListenerWrapper)self)->userData);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleRam object = (AtModuleRam)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(ddrs, object->numDdrs);
    mEncodeObjects(qdrs, object->numQdrs);
    mEncodeObjects(zbts, object->numZbts);

    mEncodeUInt(ddrErrorLatchingEnabled);
    mEncodeUInt(qdrErrorLatchingEnabled);
    }

static eAtRet DdrErrorLatchingEnable(AtModuleRam self, eBool enabled)
    {
    self->ddrErrorLatchingEnabled = enabled;
    return cAtOk;
    }

static eAtRet QdrErrorLatchingEnable(AtModuleRam self, eBool enabled)
    {
    self->qdrErrorLatchingEnabled = enabled;
    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevInfo,   "\r\n");
    AtPrintc(cSevInfo,   "* Error latching logic:\r\n");
    AtPrintc(cSevNormal, "  - DDR error latching: %s\r\n", mThis(self)->ddrErrorLatchingEnabled ? "enabled" : "disabled");
    AtPrintc(cSevNormal, "  - QDR error latching: %s\r\n", mThis(self)->qdrErrorLatchingEnabled ? "enabled" : "disabled");

    return cAtOk;
    }

static eBool DdrIsUsed(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return cAtTrue;
    }

static void InternalRamsStatusClear(AtModuleRam self)
    {
    uint32 numRams = AtModuleRamNumInternalRams(self);
    uint32 ram_i;

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        if (AtDeviceRamIdIsValid(AtModuleDeviceGet((AtModule)self), ram_i))
            {
            AtInternalRam ram = AtModuleRamInternalRamGet(self, ram_i);
            if (!AtInternalRamIsReserved(ram))
                AtInternalRamStatusClear(ram);
            }
        }
    }

static void StatusClear(AtModule self)
    {
    m_AtModuleMethods->StatusClear(self);
    InternalRamsStatusClear(mThis(self));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleRam);
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, ListenerWrapperCreate);
        mMethodOverride(m_AtModuleOverride, EventListenersAreIdentical);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* DDRs */
        mMethodOverride(m_methods, NumDdrGet);
        mMethodOverride(m_methods, DdrCreate);
        mMethodOverride(m_methods, CoreOfDdr);
        mMethodOverride(m_methods, DdrAddressBusSizeGet);
        mMethodOverride(m_methods, DdrDataBusSizeGet);
        mMethodOverride(m_methods, DdrIsUsed);

        /* ZBTs */
        mMethodOverride(m_methods, NumZbtGet);
        mMethodOverride(m_methods, ZbtCreate);
        mMethodOverride(m_methods, CoreOfZbt);

        /* QDRs */
        mMethodOverride(m_methods, NumQdrGet);
        mMethodOverride(m_methods, QdrCreate);
        mMethodOverride(m_methods, CoreOfQdr);
        mMethodOverride(m_methods, QdrUserClock);

        /* Internal RAMs */
        mMethodOverride(m_methods, NumInternalRams);
        mMethodOverride(m_methods, InternalRamGet);
        mMethodOverride(m_methods, StartInternalRamIdOfModuleGet);

        /* Internal methods */
        mMethodOverride(m_methods, DdrUserClock);
        mMethodOverride(m_methods, DdrTypeString);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleRam AtModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleRam, device) == NULL)
        return NULL;

    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint32 AtModuleRamDdrUserClock(AtModuleRam self, uint8 ddrId)
    {
    if (self)
        return mMethodsGet(self)->DdrUserClock(self, ddrId);
    return 0;
    }

const char *AtModuleRamDdrTypeString(AtModuleRam self, uint8 ddrId)
    {
    if (self)
        return mMethodsGet(self)->DdrTypeString(self, ddrId);
    return NULL;
    }

uint32 AtModuleRamDdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    if (self)
        return mMethodsGet(self)->DdrAddressBusSizeGet(self, ddrId);
    return 0;
    }

uint32 AtModuleRamDdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    if (self)
        return mMethodsGet(self)->DdrDataBusSizeGet(self, ddrId);
    return 0;
    }

uint32 AtModuleRamQdrUserClock(AtModuleRam self, uint8 qdrId)
    {
    if (self)
        return mMethodsGet(self)->QdrUserClock(self, qdrId);
    return 0;
    }

eBool AtModuleRamDdrCalibAreGood(AtModuleRam self)
    {
    uint8 numDdrs = AtModuleRamNumDdrGet(self);
    uint8 ddr_i;
    eBool good = cAtTrue;

    for (ddr_i = 0; ddr_i < numDdrs; ddr_i++)
        {
        AtRam ddr = AtModuleRamDdrGet(self, ddr_i);
        eAtRet ret = AtRamInitStatusGet(ddr);
        if (ret != cAtOk)
            {
            AtRamLog(ddr,
                     cAtLogLevelCritical, AtSourceLocation,
                     "Init fail with ret = %s\r\n",
                     AtRet2String(ret));
            good = cAtFalse;
            }
        }

    return good;
    }

void AtModuleRamAllEventListenersCall(AtModuleRam self, AtInternalRam ram, uint32 errors)
    {
    AtModule module = (AtModule)self;
    AtModuleRamEventListenerWrapper aListener;
    AtIterator iterator;

    if ((module == NULL) || (module->allEventListeners == NULL))
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(module->allEventListeners);
    while((aListener = (AtModuleRamEventListenerWrapper)AtIteratorNext(iterator)) != NULL)
        EventListenerCall(aListener, ram, errors);

    AtObjectDelete((AtObject)iterator);
    }

uint32 AtModuleRamStartInternalRamIdOfModuleGet(AtModuleRam self, uint32 moduleId)
    {
    if (self)
        return mMethodsGet(self)->StartInternalRamIdOfModuleGet(self, moduleId);

    return 0;
    }

eAtRet AtModuleRamDdrErrorLatchingEnable(AtModuleRam self, eBool enabled)
    {
    if (self)
        return DdrErrorLatchingEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool AtModuleRamDdrErrorLatchingIsEnabled(AtModuleRam self)
    {
    return (eBool)(self ? self->ddrErrorLatchingEnabled : cAtFalse);
    }

eAtRet AtModuleRamQdrErrorLatchingEnable(AtModuleRam self, eBool enabled)
    {
    if (self)
        return QdrErrorLatchingEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool AtModuleRamQdrErrorLatchingIsEnabled(AtModuleRam self)
    {
    return (eBool)(self ? self->qdrErrorLatchingEnabled : cAtFalse);
    }

/**
 * @addtogroup AtModuleRam
 * @{
 */

/**
 * Get number of DDRs that this module manages
 *
 * @param self This module
 *
 * @return Number of DDRs
 */
uint8 AtModuleRamNumDdrGet(AtModuleRam self)
    {
    mAttributeGet(NumDdrGet, uint8, 0);
    }

/**
 * Get DDR
 *
 * @param self This module
 * @param ddrId DDR ID
 *
 * @return DDR object or NULL if ID is invalid
 */
AtRam AtModuleRamDdrGet(AtModuleRam self, uint8 ddrId)
    {
    if (!mModuleIsValid(self))
    	return NULL;

    if (DdrIdIsValid(self, ddrId) && self->ddrs)
        return self->ddrs[ddrId];

    return NULL;
    }

/**
 * Get number of ZBTs that this module manages
 *
 * @param self This module
 *
 * @return Number of ZBTs
 */
uint8 AtModuleRamNumZbtGet(AtModuleRam self)
    {
    mAttributeGet(NumZbtGet, uint8, 0);
    }

/**
 * Get ZBT
 *
 * @param self This module
 * @param zbtId ZBT ID
 *
 * @return ZBT object or NULL if ID is invalid
 */
AtRam AtModuleRamZbtGet(AtModuleRam self, uint8 zbtId)
    {
    if (!mModuleIsValid(self))
    	return NULL;

    if (ZbtIdIsValid(self, zbtId) && self->zbts)
        return self->zbts[zbtId];

    return NULL;
    }

/**
 * Get number of QDRs that this module manages
 *
 * @param self This module
 *
 * @return Number of QDRs
 */
uint8 AtModuleRamNumQdrGet(AtModuleRam self)
    {
    mAttributeGet(NumQdrGet, uint8, 0);
    }

/**
 * Get QDR
 *
 * @param self This module
 * @param qdrId QDR ID
 *
 * @return QDR object or NULL if ID is invalid
 */
AtRam AtModuleRamQdrGet(AtModuleRam self, uint8 qdrId)
    {
    if (!mModuleIsValid(self))
        return NULL;

    if (QdrIdIsValid(self, qdrId) && self->qdrs)
        return self->qdrs[qdrId];

    return NULL;
    }

/**
 * Get number of internal RAMs that device may use
 *
 * @param self This module
 *
 * @return Number of internal RAMs
 */
uint32 AtModuleRamNumInternalRams(AtModuleRam self)
    {
    mAttributeGet(NumInternalRams, uint32, 0);
    }

/**
 * Get Internal RAM
 *
 * @param self This module
 * @param ramId Internal RAM ID
 *
 * @return Internal RAM object or NULL if ID is invalid
 */
AtInternalRam AtModuleRamInternalRamGet(AtModuleRam self, uint32 ramId)
    {
    mOneParamAttributeGet(InternalRamGet, ramId, AtInternalRam, NULL);
    }


/**
 * @}
 */

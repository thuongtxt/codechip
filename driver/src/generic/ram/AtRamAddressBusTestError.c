/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ram
 *
 * File        : AtRamAddressBusTestError.c
 *
 * Created Date: Mar 11, 2015
 *
 * Description : RAM address bug test error implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRamTestErrorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtRamAddressBusTestError)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtRamAddressBusTestError * AtRamAddressBusTestError;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtRamTestErrorMethods m_AtRamTestErrorOverride;

/* Save super implementation */
static const tAtRamTestErrorMethods *m_AtRamTestErrorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *Description(AtRamTestError self)
    {
    static char string[64];

    AtSprintf(string, "Affected by writing 0x%08X to address 0x%08X", mThis(self)->causedPattern, mThis(self)->causedAddress);
    return string;
    }

static void OverrideAtRamTestError(AtRamTestError self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtRamTestErrorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamTestErrorOverride, m_AtRamTestErrorMethods, sizeof(m_AtRamTestErrorOverride));

        mMethodOverride(m_AtRamTestErrorOverride, Description);
        }

    mMethodsGet(self) = &m_AtRamTestErrorOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof (tAtRamAddressBusTestError);
    }

static AtRamTestError ObjectInit(AtRamTestError self,
                                 uint32 errorAddress,
                                 uint32 expectedValue,
                                 uint32 actualValue,
                                 uint32 causedAddress,
                                 uint32 causedPattern)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtRamTestErrorObjectInit(self, errorAddress, expectedValue, actualValue) == NULL)
        return NULL;

    OverrideAtRamTestError(self);
    m_methodsInit = 1;

    /* Override */
    mThis(self)->causedAddress = causedAddress;
    mThis(self)->causedPattern = causedPattern;

    return self;
    }

AtRamTestError AtRamAddressBusTestErrorNew(uint32 errorAddress,
                                           uint32 expectedValue,
                                           uint32 actualValue,
                                           uint32 causedAddress,
                                           uint32 causedPattern)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRamTestError newError = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newError == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newError, errorAddress, expectedValue, actualValue, causedAddress, causedPattern);
    }

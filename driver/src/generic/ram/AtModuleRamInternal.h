/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtModuleRamInternal.h
 * 
 * Created Date: Jan 31, 2013
 *
 * Description : RAM module class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULERAMINTERNAL_H_
#define _ATMODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* RAM module methods */
typedef struct tAtModuleRamMethods
    {
    /* DDR management */
    uint8 (*NumDdrGet)(AtModuleRam self);
    AtRam (*DdrCreate)(AtModuleRam self, AtIpCore core, uint8 ddrId);
    AtIpCore (*CoreOfDdr)(AtModuleRam self, uint8 ddrId);
    uint32 (*DdrAddressBusSizeGet)(AtModuleRam self, uint8 ddrId);
    uint32 (*DdrDataBusSizeGet)(AtModuleRam self, uint8 ddrId);
    eBool (*DdrIsUsed)(AtModuleRam self, uint8 ddrId);

    /* ZBT management */
    uint8 (*NumZbtGet)(AtModuleRam self);
    AtRam (*ZbtCreate)(AtModuleRam self, AtIpCore core, uint8 zbtId);
    AtIpCore (*CoreOfZbt)(AtModuleRam self, uint8 zbtId);

    /* QDR management */
    uint8 (*NumQdrGet)(AtModuleRam self);
    AtRam (*QdrCreate)(AtModuleRam self, AtIpCore core, uint8 qdrId);
    AtIpCore (*CoreOfQdr)(AtModuleRam self, uint8 qdrId);

    /* For internal RAMs */
    uint32 (*NumInternalRams)(AtModuleRam self);
    AtInternalRam (*InternalRamGet)(AtModuleRam self, uint32 ramId);
    uint32 (*StartInternalRamIdOfModuleGet)(AtModuleRam self, uint32 moduleId);

    /* Internal */
    uint32 (*DdrUserClock)(AtModuleRam self, uint8 ddrId);
    const char *(*DdrTypeString)(AtModuleRam self, uint8 ddrId);
    uint32 (*QdrUserClock)(AtModuleRam self, uint8 qdrId);
    }tAtModuleRamMethods;

/* RAM module class representation */
typedef struct tAtModuleRam
    {
    tAtModule super;
    const tAtModuleRamMethods *methods;

    /* Private data */
    AtRam *ddrs;   /* List of DDRs that this module manage */
    uint8 numDdrs;

    AtRam *zbts;   /* List of ZBTs that this module manage */
    uint8 numZbts;

    AtRam *qdrs;   /* List of QDRs that this module manage */
    uint8 numQdrs;

    /* Error latching enabling. */
    eBool ddrErrorLatchingEnabled;
    eBool qdrErrorLatchingEnabled;
    }tAtModuleRam;

typedef struct tAtModuleRamEventListenerWrapper * AtModuleRamEventListenerWrapper;
typedef struct tAtModuleRamEventListenerWrapper
    {
    tAtModuleEventListenerWrapper super;
    tAtModuleRamEventListener listener;
    }tAtModuleRamEventListenerWrapper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam AtModuleRamObjectInit(AtModuleRam self, AtDevice device);

uint32 AtModuleRamDdrUserClock(AtModuleRam self, uint8 ddrId);
const char *AtModuleRamDdrTypeString(AtModuleRam self, uint8 ddrId);
uint32 AtModuleRamQdrUserClock(AtModuleRam self, uint8 qdrId);

uint32 AtModuleRamDdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId);
uint32 AtModuleRamDdrDataBusSizeGet(AtModuleRam self, uint8 ddrId);
eBool AtModuleRamDdrCalibAreGood(AtModuleRam self);

void AtModuleRamAllEventListenersCall(AtModuleRam self, AtInternalRam ram, uint32 errors);

uint32 AtModuleRamStartInternalRamIdOfModuleGet(AtModuleRam self, uint32 moduleId);

eAtRet AtModuleRamDdrErrorLatchingEnable(AtModuleRam self, eBool enabled);
eBool  AtModuleRamDdrErrorLatchingIsEnabled(AtModuleRam self);
eAtRet AtModuleRamQdrErrorLatchingEnable(AtModuleRam self, eBool enabled);
eBool  AtModuleRamQdrErrorLatchingIsEnabled(AtModuleRam self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULERAMINTERNAL_H_ */


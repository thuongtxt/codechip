/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtDdr.c
 *
 * Created Date: Feb 21, 2013
 *
 * Description : DDR generic class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRamInternal.h"
#include "AtDdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods m_AtRamOverride;

/* Save super implementation */
static const tAtRamMethods *m_AtRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ErrorLatchingSupported(AtRam self)
    {
    if (AtModuleRamDdrErrorLatchingIsEnabled(AtRamModuleGet(self)))
        return cAtTrue;

    return m_AtRamMethods->ErrorLatchingSupported(self);
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, ErrorLatchingSupported);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDdr);
    }

AtRam AtDdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtRamObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtZbtInternal.h
 * 
 * Created Date: Feb 21, 2013
 *
 * Description : ZBT generic class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATZBTINTERNAL_H_
#define _ATZBTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtZbt
    {
    tAtRam super;

    /* Private data */
    }tAtZbt;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtRam AtZbtObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _ATZBTINTERNAL_H_ */


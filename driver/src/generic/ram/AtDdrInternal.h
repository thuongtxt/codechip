/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtDdrInternal.h
 * 
 * Created Date: Feb 21, 2013
 *
 * Description : DDR generic class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDDRINTERNAL_H_
#define _ATDDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDdr
    {
    tAtRam super;

    /* Private data */
    }tAtDdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtRam AtDdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _ATDDRINTERNAL_H_ */


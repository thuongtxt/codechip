/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtRam.c
 *
 * Created Date: Jan 31, 2013
 *
 * Description : RAM generic source code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
/*
 * External RAM test explanation
 * -----------------------------------
 *
 * NOTATION:
 *     - Walking 1 address/pattern: (0x1, 0x2, 0x4, 0x8...)
 *     - Walking 0 address/pattern: (0xFFFFFE, 0xFFFFFD, 0xFFFFFB, 0xFFFFF7...)
 *     - All 1 address: 0xFFFFFF
 *
 * 1. Address bus test
 * ========================
 *
 * PATTERN = 0xAAAAAAAA
 * ANTI-PATTERN = 0x55555555
 *
 * Step 1: Check if address bits stuck high
 * - Write PATTERN to all 'walking 1 addresses'
 * - Write ANTI-PATTERN to address 0x0 and re-check all 'walking 1 addresses' if it is affected
 *
 * Step 2: Check if address bit stuck low or shorted
 * - Write PATTERN to address 0x0 and all 'walking 1 addresses'
 * - On each 'walking 1 address' write ANTI-PATTERN and check: whether value of address 0x0 is affected,
 *   and whether all other 'walking 1 addresses' are affected.
 *
 * Step 3: Check if address bits are shorted:
 * - Write PATTERN to a 'all 1 address' (0xFFFFFF)
 * - On each 'walking 0 address', write ANTI-PATTERN and check if value of 'all 1 address' is affected.
 *
 * 2. Data bus test with one input address
 * =======================================
 * Step 1. For all 'walking 1 patterns", write to address then read to compare
 * Step 2. For all 'walking 0 patterns", write to address then read to compare
 *
 * 3. Memory test with input start address and end address, apply March-C algorithm
 * =======================================================================================
 *
 * PATTERN = 0xAAAAAAAA
 * ANTI-PATTERN = 0x55555555
 *
 * Step 1. Fill PATTERN from start address to end address
 * Step 2. Increase address from start address to end address. On each address, read to check with expected value is PATTERN, then write ANTI-PATTERN
 * Step 3. Increase address from start address to end address. On each address, read to check with expected value is ANTI-PATTERN written by step 2, then write PATTERN
 * Step 4. Decrease address from end address to start address. On each address, read to check with expected value is PATTERN written by step 3, then write ANTI-PATTERN
 * Step 5. Decrease address from end address to start address. On each address, read to check with expected value is ANTI-PATTERN written by step 4, then write PATTERN
 * Step 6. Increase address from start address to end address. On each address, read to check with expected value is PATTERN written by step 5
 */

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtRamInternal.h"
#include "AtRamTestErrorInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAllOne         0xFFFFFFFF
#define cInvalidValue   cAllOne
#define cDefaultPattern 0xAAAAAAAA
#define cAntiPattern    0x55555555

#define cAddressIncrease 1
#define cAddressDecrease 0

/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(self, ret)                                              \
    do{                                                                        \
        eAtRet _ret = ErrorCheckAndLog(self, ret, AtSourceLocation);         \
        if (_ret != cAtOk)                                                     \
            return _ret;                                                       \
    }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtRamMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ErrorCheckAndLog(AtRam self, eAtRet ret, const char *file, uint32 line)
    {
    if (ret != cAtOk)
        AtRamLog(self, cAtLogLevelCritical, file, line, "ret = %s\r\n", AtRet2String(ret));
    return ret;
    }

static eBool RamIsValid(AtRam self)
    {
    return (self != NULL) ? cAtTrue : cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtRam);
    }

static eAtModuleRamRet Init(AtRam self)
    {
    self->addressBusIsTested = cAtFalse;
    self->addressBusIsGood   = cAtFalse;
    self->dataBusIsTested    = cAtFalse;
    self->dataBusIsGood      = cAtFalse;
    self->memoryIsTested     = cAtFalse;
    self->memoryIsGood       = cAtFalse;

    return cAtOk;
    }

static uint32 Read(AtRam self, uint32 address)
    {
    AtHal hal = AtIpCoreHalGet(AtRamIpCoreGet(self));
    uint32 value = AtHalRead(hal, address);
    return value;
    }

static void Write(AtRam self, uint32 address, uint32 value)
    {
    AtHal hal = AtIpCoreHalGet(AtRamIpCoreGet(self));
    AtHalWrite(hal, address, value);
    }

static uint32 BusMask(uint32 busSize)
    {
    return ~(((uint32)0xFFFFFFFF) << busSize);
    }

static uint32 AddressBusSizeGet(AtRam self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DataBusSizeGet(AtRam self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModuleRamRet TestDurationSet(AtRam self, uint32 durationInMs)
    {
	AtUnused(durationInMs);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 TestDurationGet(AtRam self)
    {
	AtUnused(self);
    return 0;
    }

static void ErrorListFlush(AtList list)
    {
    AtObject error;

    while ((error = AtListObjectRemoveAtIndex(list, 0)) != NULL)
        AtObjectDelete(error);
    }

static void DataBusTestErrorListAdd(AtList errorList, uint32 address, uint32 expectedValue, uint32 actualValue)
    {
    AtRamTestError newError;
    if (errorList == NULL)
        return;

    newError = AtRamTestErrorNew(address, expectedValue, actualValue);
    if (newError)
        AtListObjectAdd(errorList, (AtObject)newError);
    }

static void AddressBusTestErrorListAdd(AtList errorList,
                                       uint32 address,
                                       uint32 expectedValue,
                                       uint32 actualValue,
                                       uint32 causedAddress,
                                       uint32 causedPattern)
    {
    AtRamTestError newError;
    if (errorList == NULL)
        return;

    newError = AtRamAddressBusTestErrorNew(address, expectedValue, actualValue, causedAddress, causedPattern);
    if (newError)
        AtListObjectAdd(errorList, (AtObject)newError);
    }

static void AddressBusErrorsListReset(AtRam self)
    {
    ErrorListFlush(self->addressBusTestErrors);

    /* Also restart iterator by deleting the current iterator so that new one will be created */
    AtObjectDelete((AtObject)(self->addressBusErrorsIterator));
    }

static uint32 CellMemorySize(AtRam self)
    {
    return mMethodsGet(self)->CellSizeGet(self) * sizeof(uint32);
    }

static uint32 *AllocateCellContent(AtRam self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 cellMemorySize = CellMemorySize(self);
    uint32 *cellContent = mMethodsGet(osal)->MemAlloc(osal, cellMemorySize);

    if (cellContent)
        mMethodsGet(osal)->MemInit(osal, cellContent, 0, cellMemorySize);

    return cellContent;
    }

static uint32 *CellContent1Get(AtRam self)
    {
    if (self->cellContent1)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, self->cellContent1, 0, CellMemorySize(self));
        return self->cellContent1;
        }

    self->cellContent1 = AllocateCellContent(self);
    return self->cellContent1;
    }

static uint32 *CellContent2Get(AtRam self)
    {
    if (self->cellContent2)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, self->cellContent2, 0, CellMemorySize(self));
        return self->cellContent2;
        }

    self->cellContent2 = AllocateCellContent(self);
    return self->cellContent2;
    }

static uint32 AddressBusSize(AtRam self)
    {
    /* If developer configure this, just use this setting */
    if (self->addressBusSize)
        return self->addressBusSize;

    return mMethodsGet(self)->AddressBusSizeGet(self);
    }

static uint32 DataBusSize(AtRam self)
    {
    /* If developer configure this, just use this setting */
    if (self->dataBusSize)
        return self->dataBusSize;

    return mMethodsGet(self)->DataBusSizeGet(self);
    }

static uint32 MaxNumDwordsToDisplayCellValue(AtRam self)
    {
    return (sizeof(self->buffer) / (8 + 1)); /* Plus 1 for the separator */
    }

static char *CellValueStringBuild(AtRam self, const uint32 *cellValues, char *buffer, uint32 bufferSize)
    {
    uint32 maxNumDwordsToDisplay = MaxNumDwordsToDisplayCellValue(self);
    uint32 cellSize = AtRamCellSizeGet(self);
    int32 numDwordsToDisplay = (int32)mMin(cellSize, maxNumDwordsToDisplay);
    int32 dword_i;

    AtOsalMemInit(buffer, 0, bufferSize);
    for (dword_i = numDwordsToDisplay - 1; dword_i >= 0; dword_i--)
        {
        if (dword_i == (numDwordsToDisplay - 1))
            AtSprintf(buffer, "%08x", cellValues[dword_i]);
        else
            AtSprintf(buffer, "%s.%08x", buffer, cellValues[dword_i]);
        }

    return buffer;
    }

static const char *CellValueString(AtRam self, const uint32 *cellValues)
    {
    return CellValueStringBuild(self, cellValues, self->buffer, sizeof(self->buffer));
    }

static eAtRet CellWriteWithLog(AtRam self, uint32 address, const uint32 *value)
    {
    eAtRet ret = mMethodsGet(self)->CellWrite(self, address, value);

    if (ret != cAtOk)
        {
        AtRamLog(self, cAtLogLevelCritical,
                 AtSourceLocation,
                 "Write address 0x%08x, value %s fail, ret = %s\r\n",
                 address, CellValueString(self, value), AtRet2String(ret));
        }

    return ret;
    }

static eAtRet CellReadWithLog(AtRam self, uint32 address, uint32 *value)
    {
    eAtRet ret = mMethodsGet(self)->CellRead(self, address, value);

    if (ret != cAtOk)
        {
        AtRamLog(self, cAtLogLevelCritical,
                 AtSourceLocation,
                 "Read address 0x%08x fail, ret = %s\r\n",
                 address, AtRet2String(ret));
        }

    return ret;
    }

static eAtModuleRamRet AddressBusTest(AtRam self)
    {
    uint32 data;
    uint32 antiData;
    uint32 address, testAddress;
    uint32 *value = CellContent1Get(self);
    eBool disableRamAccessAfterTesting = cAtFalse;
    uint32 addressBusMask;

    if (value == NULL)
        return cAtErrorRsrcNoAvail;

    /* Cleanup for testing */
    addressBusMask = BusMask(AddressBusSize(self));
    AddressBusErrorsListReset(self);

    if (!AtRamAccessIsEnabled(self))
        {
        AtRamAccessEnable(self, cAtTrue);
        disableRamAccessAfterTesting = cAtTrue;
        }

    /* Setup data */
    data     = cDefaultPattern;
    antiData = ~data;

    /*
     * Walking 1: Check if address bits stuck high
     * Write data to 'walking 1 addresses' (0x1, 0x2, 0x4, 0x8...),
     * write anti-data to address 0x0 and re-check 'walking 1 address' to see if it is affected
     */
    for (address = 1; (address & addressBusMask) != 0; address <<= 1)
        {
        value[0] = data;
        mSuccessAssert(self, CellWriteWithLog(self, address, value));
        }

    value[0] = antiData;
    mSuccessAssert(self, CellWriteWithLog(self, 0x0, value));

    /* Make sure that other registers still keep their values */
    for (address = 1; (address & addressBusMask) != 0; address <<= 1)
        {
        mSuccessAssert(self, CellReadWithLog(self, address, value));
        if (value[0] != data)
            AddressBusTestErrorListAdd(self->addressBusTestErrors, address, data, value[0], 0x0, antiData);
        }

    /*
     * Walking 1: Check if address bit stuck low or shorted
     * Write data to address 0x0
     * Write anti-data to a 'walking 1 address',
     * Check whether value of address 0x0 is affected or not.
     * Moreover, Check whether all other 'walking 1 addresses' are affected or not
     */
    value[0] = data;
    mSuccessAssert(self, CellWriteWithLog(self, 0x0, value));

    for (testAddress = 1; (testAddress & addressBusMask) != 0; testAddress <<= 1)
        {
        value[0] = antiData;
        mSuccessAssert(self, CellWriteWithLog(self, testAddress, value));

        /* Check address 0x0 */
        mSuccessAssert(self, CellReadWithLog(self, 0x0, value));

        if (value[0] != data)
            AddressBusTestErrorListAdd(self->addressBusTestErrors, 0x0, data, value[0], testAddress, antiData);

        /* Check if address bits are shorted */
        for (address = 1; (address & addressBusMask) != 0; address <<= 1)
            {
            mSuccessAssert(self, CellReadWithLog(self, address, value));

            if ((value[0] != data) && (address != testAddress))
                AddressBusTestErrorListAdd(self->addressBusTestErrors, address, data, value[0], testAddress, antiData);
            }

        /* Restore value of tested address */
        value[0] = data;
        mSuccessAssert(self, CellWriteWithLog(self, testAddress, value));
        }

    /*
     * Walking 0: Check if address bits are shorted:
     * Write data to 'all 1 address' (last address),
     * Write anti-data to 'walking 0 addresses'
     * Check whether value of last address is affected or not
     */
    value[0] = data;
    mSuccessAssert(self, CellWriteWithLog(self, addressBusMask, value));

    for (address = 1; (address & addressBusMask) != 0; address <<= 1)
        {
        value[0] = antiData;
        mSuccessAssert(self, CellWriteWithLog(self, (~address), value));

        /* Check last address */
        mSuccessAssert(self, CellReadWithLog(self, addressBusMask, value));

        if (value[0] != data)
            AddressBusTestErrorListAdd(self->addressBusTestErrors, addressBusMask, data, value[0], (~address), antiData);
        }

    /* If testing is enabled by this API, disable it */
    if (disableRamAccessAfterTesting)
        AtRamAccessEnable(self, cAtFalse);

    return (AtRamDataBusTestErrorCount(self) == 0) ? cAtOk : cAtErrorMemoryTestFail;
    }

static eBool ReadValueIsMatchWithPattern(AtRam self, uint32 *readVal, uint32 *pattern, uint8 *dwordIndex)
    {
    uint32 busSize = DataBusSize(self);
    uint32 maskOfLast;

    *dwordIndex = 0;
    while (busSize > 32)
        {
        if (*readVal != *pattern)
            return cAtFalse;

        busSize -= 32;
        readVal++;
        pattern++;
        *dwordIndex = (uint8)(*dwordIndex + 1);
        }

    maskOfLast = BusMask(busSize);
    if ((*readVal & maskOfLast) != (*pattern & maskOfLast))
        return cAtFalse;

    return cAtTrue;
    }

static uint8 Walking1PatternGenerate(AtRam self, uint32 *pattern, uint32 *bitPosition)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memSize = mMethodsGet(self)->CellSizeGet(self) * sizeof(uint32);
    uint32 dataBusSize = DataBusSize(self);
    uint32 dwordIndex;

    if (*bitPosition == 0)
        mMethodsGet(osal)->MemInit(osal, pattern, 0, memSize);
    else
        {
        if (*bitPosition > dataBusSize)
            {
            mMethodsGet(osal)->MemInit(osal, pattern, 0, memSize);
            return 0;
            }

        dwordIndex = (uint8)((*bitPosition - 1) / 32);
        if ((*bitPosition % 32) == 1)
            {
            pattern[dwordIndex] = 1;

            if (dwordIndex > 0)
                pattern[dwordIndex - 1] = 0;
            }
        else
            pattern[dwordIndex] <<= 1;
        }

    (*bitPosition)++;
    return 1;
    }

static uint8 Walking0PatternGenerate(AtRam self, uint32 *pattern, uint32 *bitPosition)
    {
    uint8 i;

    for (i = 0; i < mMethodsGet(self)->CellSizeGet(self); i++)
        pattern[i] = ~pattern[i];

    if (Walking1PatternGenerate(self, pattern, bitPosition) == 0)
        return 0;

    for (i = 0; i < mMethodsGet(self)->CellSizeGet(self); i++)
        pattern[i] = ~pattern[i];

    return 1;
    }

static void DataBusErrorsListReset(AtRam self)
    {
    ErrorListFlush(self->dataBusTestErrors);

    /* Also restart iterator by deleting the current iterator so that new one will be created */
    AtObjectDelete((AtObject)(self->dataBusErrorsIterator));
    }

static uint32 NumBitsForLastDword(AtRam self)
    {
    uint32 numUsedBits   = AtRamCellSizeInBits(self);
    uint32 allBits       = AtRamCellSizeGet(self) * 32;
    uint32 numUnusedBits = allBits - numUsedBits;
    return (32 - numUnusedBits);
    }

static uint32 LastDwordMask(AtRam self)
    {
    uint32 numBits = NumBitsForLastDword(self);
    if (numBits == 0)
        return cBit31_0;
    return ~(cBit31_0 << numBits);
    }

static eAtModuleRamRet DataBusTest(AtRam self, uint32 address)
    {
    eBool disableRamAccessAfterTesting;
    uint32 bitPosition = 0;
    uint8 dwIndex;
    uint32 *pattern = CellContent2Get(self);
    uint32 *value   = CellContent1Get(self);
    AtOsal osal = AtSharedDriverOsalGet();

    if ((value == NULL) || (pattern == NULL))
        return cAtErrorRsrcNoAvail;

    /* Setup */
    disableRamAccessAfterTesting = cAtFalse;
    DataBusErrorsListReset(self);
    if (!AtRamAccessIsEnabled(self))
        {
        AtRamAccessEnable(self, cAtTrue);
        disableRamAccessAfterTesting = cAtTrue;
        }

    bitPosition = 0;
    while (Walking1PatternGenerate(self, pattern, &bitPosition))
        {
        /* Write pattern then */
        mSuccessAssert(self, CellWriteWithLog(self, address, pattern));

        /* Read to check */
        mSuccessAssert(self, CellReadWithLog(self, address, value));
        if (!ReadValueIsMatchWithPattern(self, value, pattern, &dwIndex))
            DataBusTestErrorListAdd(self->dataBusTestErrors, address, pattern[dwIndex], value[dwIndex]);
        }

    bitPosition = 1;
    mMethodsGet(osal)->MemInit(osal, pattern, 0xFF, CellMemorySize(self));
    pattern[AtRamCellSizeGet(self) - 1] &= LastDwordMask(self);
    while (Walking0PatternGenerate(self, pattern, &bitPosition))
        {
        /* Write pattern */
        mSuccessAssert(self, CellWriteWithLog(self, address, pattern));

        /* Read to check */
        mSuccessAssert(self, CellReadWithLog(self, address, value));
        if (!ReadValueIsMatchWithPattern(self, value, pattern, &dwIndex))
            DataBusTestErrorListAdd(self->dataBusTestErrors, address, pattern[dwIndex], value[dwIndex]);
        }

    /* If testing is enabled by this API, disable it */
    if (disableRamAccessAfterTesting)
        AtRamAccessEnable(self, cAtFalse);

    return (AtRamDataBusTestErrorCount(self) == 0) ? cAtOk : cAtErrorMemoryTestFail;
    }

static eBool AddressIncrease(uint32 currentAddress, uint32 startAddress, uint32 endAddress, uint32 *nextAddress)
    {
	AtUnused(startAddress);
    if (currentAddress == endAddress)
        return cAtFalse;

    *nextAddress = currentAddress + 1;
    return cAtTrue;
    }

static eBool AddressDecrease(uint32 currentAddress, uint32 startAddress, uint32 endAddress, uint32 *nextAddress)
    {
	AtUnused(endAddress);
    if (currentAddress == startAddress)
        return cAtFalse;

    *nextAddress = currentAddress - 1;
    return cAtTrue;
    }

static eBool NextAddress(uint32 currentAddress, uint32 startAddress, uint32 endAddress, uint32 *nextAddress, uint8 increaseMode)
    {
    if (increaseMode == cAddressIncrease)
        return AddressIncrease(currentAddress, startAddress, endAddress, nextAddress);
    if (increaseMode == cAddressDecrease)
        return AddressDecrease(currentAddress, startAddress, endAddress, nextAddress);

    return cAtFalse;
    }

static eAtModuleRamRet MemoryReadWriteCheck(AtRam self,
										    eBool increaseAddress,
										    uint32 expectedReadData,
										    uint32 writeData,
										    uint32 startAddress,
										    uint32 endAddress,
										    uint32 *firstErrorAddress,
										    uint32 *errorCount)
    {
    eAtModuleRamRet ret = cAtOk;
    uint32 dword_i;
    uint32 address;
    uint32 *readValue  = CellContent1Get(self);
    uint32 *writeValue = CellContent2Get(self);
    AtOsal osal;
    uint32 errCount = 0;
    tAtOsalCurTime previousTime;
    eBool finish = cAtFalse;
    uint32 cellSize;
    uint32 lastDwordMask = LastDwordMask(self);

    /* Need memory */
    osal = AtSharedDriverOsalGet();
    if ((readValue == NULL) || (writeValue == NULL))
        return cAtErrorRsrcNoAvail;

    /* Setup */
    cellSize = AtRamCellSizeGet(self);
    for (dword_i = 0; dword_i < cellSize; dword_i++)
        writeValue[dword_i] = writeData;
    writeValue[cellSize - 1] &= lastDwordMask;

    mMethodsGet(osal)->CurTimeGet(osal, &previousTime);

    address = (increaseAddress) ? startAddress : endAddress;
    while (!finish)
        {
        mSuccessAssert(self, AtRamCellRead(self, address, readValue));

        for (dword_i = 0; dword_i < cellSize; dword_i++)
            {
            eBool isLast = (dword_i == (cellSize - 1)) ? cAtTrue : cAtFalse;
            uint32 expected = expectedReadData;
            if (isLast)
                expected = expectedReadData & lastDwordMask;

            if (readValue[dword_i] == expected)
                continue;

            AtRamLog(self, cAtLogLevelCritical, AtSourceLocation,
                     "Address 0x%08x, dword#%u, expect 0x%08x but got 0x%08x\r\n",
                     address, dword_i, expectedReadData, readValue[dword_i]);

            /* Save first error address */
            if (errCount == 0)
                *firstErrorAddress = address;

            errCount++;
            break;
            }

        mSuccessAssert(self, AtRamCellWrite(self, address, writeValue));

        /* Next address */
        if (!NextAddress(address, startAddress, endAddress, &address, increaseAddress))
            {
        	finish = cAtTrue;
        	break;
            }

        /* Give CPU a rest if this jobs take long time */
        AtRamCpuRest(osal, &previousTime);
        }

    *errorCount = errCount;

    return ret;
    }

static eAtModuleRamRet MemoryReadCheck(AtRam self,
									   eBool addressIsIncreased,
									   uint32 expectedData,
									   uint32 startAddress,
									   uint32 endAddress,
									   uint32 *firstErrorAddress,
									   uint32 *errorCount)
    {
    eAtModuleRamRet ret = cAtOk;
    uint32 dword_i;
    uint32 address;
    uint32 *readValue = CellContent1Get(self);
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 errCount = 0;
    tAtOsalCurTime previousTime;
    eBool finish = cAtFalse;
    uint32 cellSize;
    uint32 lastDwordMask;

    if (readValue == NULL)
        return cAtErrorRsrcNoAvail;

    /* Setup */
    address = addressIsIncreased ? startAddress : endAddress;
    mMethodsGet(osal)->CurTimeGet(osal, &previousTime);

    /* Start testing */
    cellSize = mMethodsGet(self)->CellSizeGet(self);
    lastDwordMask = LastDwordMask(self);
    while (!finish)
        {
        mSuccessAssert(self, CellReadWithLog(self, address, readValue));

        for (dword_i = 0; dword_i < cellSize; dword_i++)
            {
            eBool isLast = (dword_i == (cellSize - 1));
            uint32 expected = isLast ? (expectedData & lastDwordMask) : expectedData;

            if (readValue[dword_i] == expected)
                continue;

            AtRamLog(self, cAtLogLevelCritical, AtSourceLocation,
                     "Address 0x%08x, dword#%u, expect 0x%08x but got 0x%08x\r\n",
                     address, dword_i, expectedData, readValue[dword_i]);

            /* Save first error */
            if (errCount == 0)
                *firstErrorAddress = address;

            errCount++;
            break;
            }

        /* Next address */
        if (!NextAddress(address, startAddress, endAddress, &address, addressIsIncreased))
            {
        	finish = cAtTrue;
        	break;
            }

        /* Give CPU a rest if this jobs take long time */
        AtRamCpuRest(osal, &previousTime);
        }

    *errorCount = errCount;

    return ret;
    }

/*
 * Fill all memory locations in RAM with data
 * @param self
 * @param addressIsIncreased set to cAtTrue to fill from 0 to 0xFFFFFFFF, else fill reversely.
 * @param data
 * @return
 */
static eAtModuleRamRet MemoryFill(AtRam self, eBool increaseAddress, uint32 data, uint32 startAddress, uint32 endAddress)
    {
    uint32 i;
    uint32 address;
    uint32 *value = CellContent1Get(self);
    AtOsal osal;
    tAtOsalCurTime previousTime;
    uint32 cellSize;

    /* Need memory */
    osal = AtSharedDriverOsalGet();
    if (value == NULL)
        return cAtErrorRsrcNoAvail;

    /* Setup */
    cellSize = mMethodsGet(self)->CellSizeGet(self);
    for (i = 0; i < cellSize; i++)
        value[i] = data;
    value[cellSize - 1] &= LastDwordMask(self);

    /* Although this memory is filled with a pattern, but it is required to
     * separate address increasing and decreasing. So, do not try to merge the
     * two for loops to one */
    /* Increase writing */
    if (increaseAddress)
        {
        mMethodsGet(osal)->CurTimeGet(osal, &previousTime);
        for (address = startAddress; address <= endAddress; address++)
            {
            mSuccessAssert(self, CellWriteWithLog(self, address, value));
            if (address == endAddress)
                break;
            AtRamCpuRest(osal, &previousTime);
            }
        }

    /* Decrease writing */
    else
        {
        mMethodsGet(osal)->CurTimeGet(osal, &previousTime);
        for (address = endAddress; address >= startAddress; address--)
            {
            mSuccessAssert(self, CellWriteWithLog(self, address, value));
            if (address == startAddress)
                break;
            AtRamCpuRest(osal, &previousTime);
            }
        }

    return cAtOk;
    }

static eAtModuleRamRet MemoryFillAndLog(AtRam self, eBool addressIsIncreased, uint32 data, uint32 startAddress, uint32 endAddress)
    {
    eAtRet ret = cAtOk;

    AtRamLog(self, cAtLogLevelInfo, AtSourceLocation,
             "Increase address from 0x%08x to 0x%08x and fill all registers with value 0x%08x\r\n",
             startAddress, endAddress, data);

    ret = mMethodsGet(self)->MemoryFill(self, addressIsIncreased, data, startAddress, endAddress);

    if (ret != cAtOk)
        AtRamLog(self, cAtLogLevelCritical, AtSourceLocation, "MemoryFillAndLog fail, ret = %s\r\n", AtRet2String(ret));

    return ret;
    }

static eAtModuleRamRet MemoryReadWriteCheckAndLog(AtRam self,
                                                  eBool addressIsIncreased,
                                                  uint32 expectedReadData,
                                                  uint32 writeData,
                                                  uint32 startAddress,
                                                  uint32 endAddress,
                                                  uint32 *firstErrorAddress,
                                                  uint32 *memTestErrorCount)
    {
    eAtRet ret = cAtOk;

    AtRamLog(self, cAtLogLevelInfo, AtSourceLocation,
             "%s address from 0x%08x to 0x%08x, expect all registers have value 0x%08x, then write 0x%08x to all of them.\r\n",
             addressIsIncreased ? "Increase" : "Decrease",
             addressIsIncreased ? startAddress : endAddress,
             addressIsIncreased ? endAddress : startAddress,
             expectedReadData, writeData);

    ret = mMethodsGet(self)->MemoryReadWriteCheck(self,
                                                  addressIsIncreased,
                                                  expectedReadData,
                                                  writeData,
                                                  startAddress,
                                                  endAddress,
                                                  firstErrorAddress,
                                                  memTestErrorCount);
    if (*memTestErrorCount)
        {
        AtRamLog(self, cAtLogLevelCritical, AtSourceLocation,
                 "MemoryReadWriteCheck: error counter = %u, address: 0x%08x\r\n",
                 *memTestErrorCount,
                 *firstErrorAddress);
        }

    return ret;
    }

static eAtModuleRamRet MemoryReadCheckAndLog(AtRam self,
                                             eBool addressIsIncreased,
                                             uint32 expectedData,
                                             uint32 startAddress,
                                             uint32 endAddress,
                                             uint32 *firstErrorAddress,
                                             uint32 *memTestErrorCount)
    {
    eAtRet ret = cAtOk;
    eBool disableAccessAfterTesting = cAtFalse;

    if (!AtRamAccessIsEnabled(self))
        {
        AtRamAccessEnable(self, cAtTrue);
        disableAccessAfterTesting = cAtTrue;
        }

    AtRamLog(self, cAtLogLevelInfo, AtSourceLocation,
             "%s address from 0x%08x to 0x%08x, expect all registers have value 0x%08x\r\n",
             addressIsIncreased ? "Increase" : "Decrease",
             addressIsIncreased ? startAddress : endAddress,
             addressIsIncreased ? endAddress : startAddress,
             expectedData);

    ret = mMethodsGet(self)->MemoryReadCheck(self,
                                             addressIsIncreased,
                                             expectedData,
                                             startAddress,
                                             endAddress,
                                             firstErrorAddress,
                                             memTestErrorCount);

    if (*memTestErrorCount)
        {
        AtRamLog(self, cAtLogLevelCritical, AtSourceLocation,
                 "MemoryReadWriteCheck: error counter = %u, address: 0x%08x\r\n",
                 *memTestErrorCount,
                 *firstErrorAddress);
        }

    /* If testing is enabled by this API, disable it */
    if (disableAccessAfterTesting)
        AtRamAccessEnable(self, cAtFalse);

    return ret;
    }

/*
 * March C- algorithm
 * - Notation:
 *     + r0: read a 0 from a memory location
 *     + r1: read a 1 from a memory location
 *     + w0: write a 0 to a memory location
 *     + w1: write a 1 to a memory location
 *     + ==>  : increasing memory addressing
 *     + <==  : decreasing memory addressing
 *     + <==> : either increasing or decreasing
 * - Algorithm: <==> (w0); ==> (r0, w1); ==> (r1, w0); <== (r0, w1); <== (r1, w0); <==> (r0)
 * @param self
 * @param errorAddress
 * @param pattern1
 * @param pattern2
 * @return
 */
static eAtModuleRamRet MarchC(AtRam self,
                              uint32 data0,
                              uint32 data1,
                              uint32 startAddress,
                              uint32 endAddress,
                              uint32 *firstErrorAddress,
                              uint32 *errorCount)
    {
	eAtModuleRamRet ret = cAtOk;
    uint32 errorAddress;
    uint32 errCount = 0;

    *errorCount = 0;
    *firstErrorAddress = 0;

    /* <==> (w0) */
    ret = MemoryFillAndLog(self, cAtTrue, data0, startAddress, endAddress);
    if (ret != cAtOk)
        return ret;

    /* ==> (r0, w1) */
    ret = MemoryReadWriteCheckAndLog(self, cAtTrue, data0, data1, startAddress, endAddress, &errorAddress, &errCount);
    if ((*errorCount == 0) && (errCount != 0))
        *firstErrorAddress = errorAddress;
    *errorCount += errCount;
    if (ret != cAtOk)
        return ret;

    /* To save time, exit on testing fail */
    if (*errorCount)
        return cAtOk;

    /* ==> (r1, w0) */
    ret = MemoryReadWriteCheckAndLog(self, cAtTrue, data1, data0, startAddress, endAddress, &errorAddress, &errCount);
    if ((*errorCount == 0) && (errCount != 0))
        *firstErrorAddress = errorAddress;
    *errorCount += errCount;
    if (ret != cAtOk)
        return ret;

    /* To save time, exit on testing fail */
    if (*errorCount)
        return cAtOk;

    /* <== (r0, w1) */
    ret = MemoryReadWriteCheckAndLog(self, cAtFalse, data0, data1, startAddress, endAddress, &errorAddress, &errCount);
    if ((*errorCount == 0) && (errCount != 0))
        *firstErrorAddress = errorAddress;
    *errorCount += errCount;
    if (ret != cAtOk)
        return ret;

    /* To save time, exit on testing fail */
    if (*errorCount)
        return cAtOk;

    /* <== (r1, w0) */
    ret = MemoryReadWriteCheckAndLog(self, cAtFalse, data1, data0, startAddress, endAddress, &errorAddress, &errCount);
    if ((*errorCount == 0) && (errCount != 0))
        *firstErrorAddress = errorAddress;
    *errorCount += errCount;
    if (ret != cAtOk)
        return ret;

    /* To save time, exit on testing fail */
    if (*errorCount)
        return cAtOk;

    /* <==> (r0) */
    ret = MemoryReadCheckAndLog(self, cAtTrue, data0, startAddress, endAddress, &errorAddress, &errCount);
    if (0 == *errorCount && errCount != 0)
        *firstErrorAddress = errorAddress;
    *errorCount += errCount;

    return ret;
    }

static eAtModuleRamRet MemoryTest(AtRam self, uint32 startAddress, uint32 endAddress, uint32 *firstErrorAddress, uint32 *errorCount)
    {
    eAtModuleRamRet ret;
    eBool disableAccessAfterTesting = cAtFalse;

    if (!AtRamAccessIsEnabled(self))
        {
        AtRamAccessEnable(self, cAtTrue);
        disableAccessAfterTesting = cAtTrue;
        }

    ret = MarchC(self, cDefaultPattern, cAntiPattern, startAddress, endAddress, firstErrorAddress, errorCount);
    self->memTestErrorCount = *errorCount;
    self->firstErrorAddress = *firstErrorAddress;

    /* If testing is enabled by this API, disable it */
    if (disableAccessAfterTesting)
        AtRamAccessEnable(self, cAtFalse);

    self->memoryIsGood = cAtFalse;
    if ((ret == cAtOk) && (*errorCount == 0))
        self->memoryIsGood = cAtTrue;

    self->memoryIsTested = cAtTrue;

    return ret;
    }

static eBool AddressRangeIsValid(AtRam self, uint32 startAddress, uint32 endAddress)
    {
    uint32 maxAddress = BusMask(AddressBusSize(self));
    if (startAddress > endAddress)
        return cAtFalse;

    if (endAddress > maxAddress)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 CellSizeGet(AtRam self)
    {
	AtUnused(self);
    /* Concrete class implement me */
    return 0;
    }

static uint32 CellSizeInBits(AtRam self)
    {
    return mMethodsGet(self)->CellSizeGet(self) * 32;
    }

static eAtRet CellRead(AtRam self, uint32 address, uint32 *value)
    {
	AtUnused(value);
	AtUnused(address);
	AtUnused(self);
    /* Concrete class implement me */
    return cAtErrorModeNotSupport;
    }

static eAtRet CellWrite(AtRam self, uint32 address, const uint32 *value)
    {
	AtUnused(value);
	AtUnused(address);
	AtUnused(self);
    /* Concrete class implement me */
    return cAtErrorModeNotSupport;
    }

static uint32 CellAddressGet(AtRam self, uint32 bank, uint32 row, uint32 column)
    {
	AtUnused(column);
	AtUnused(row);
	AtUnused(bank);
	AtUnused(self);
    /* Concrete class implement me */
    return 0;
    }

static uint32 CellBankGet(AtRam self, uint32 address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Concrete class implement me */
    return 0;
    }

static uint32 CellColumnGet(AtRam self, uint32 address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Concrete class implement me */
    return 0;
    }

static uint32 CellRowGet(AtRam self, uint32 address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Concrete class implement me */
    return 0;
    }

static eAtModuleRamRet AccessEnable(AtRam self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Concrete class implement me */
    return cAtOk;
    }

static eBool AccessIsEnabled(AtRam self)
    {
	AtUnused(self);
    /* Concrete class implement me */
    return cAtFalse;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
	AtUnused(self);
    return cAtErrorDdrInitFail;
    }

static uint32 NumBanks(AtRam self)
    {
	AtUnused(self);
    /* Let concrete class determine */
    return 0;
    }

static uint32 NumColumns(AtRam self)
    {
	AtUnused(self);
    /* Let concrete class determine */
    return 0;
    }

static uint32 NumRows(AtRam self)
    {
	AtUnused(self);
    /* Let concrete class determine */
    return 0;
    }

static void TestStatusShow(eBool tested, eBool isGood)
    {
    if (tested)
        AtPrintc(isGood ? cSevInfo : cSevCritical, "%s\r\n", isGood ? "PASS" : "FAIL");
    else
        AtPrintc(cSevWarning, "Not tested\r\n");
    }

static eBool NoTestRun(AtRam self)
    {
    if (!self->addressBusIsTested && !self->dataBusIsTested && !self->memoryIsTested)
        return cAtTrue;
    return cAtFalse;
    }

static void Debug(AtRam self)
    {
    AtPrintc(cSevNormal, "* RAM info:\r\n");
    AtPrintc(cSevNormal, "  - Address bus size: %u\r\n", AtRamAddressBusSizeGet(self));
    AtPrintc(cSevNormal, "  - Data bus size   : %u\r\n", AtRamDataBusSizeGet(self));

    AtPrintc(cSevNormal, "* RAM test status:\r\n");
    AtPrintc(cSevNormal, "  - Address bus: "); TestStatusShow(self->addressBusIsTested, self->addressBusIsGood);
    AtPrintc(cSevNormal, "  - Data bus   : "); TestStatusShow(self->dataBusIsTested, self->dataBusIsGood);
    AtPrintc(cSevNormal, "  - Memory     : "); TestStatusShow(self->memoryIsTested, self->memoryIsGood);

    /* Show RAM is good or bad */
    if (!NoTestRun(self))
        {
        eBool isGood = AtRamIsGood(self);
        AtPrintc(isGood ? cSevInfo: cSevCritical, "==> RAM is %s\r\n", isGood ? "GOOD" : "BAD");
        }
    }

static eBool IsTested(AtRam self)
    {
    return (self->addressBusIsTested && self->dataBusIsTested && self->memoryIsTested) ? cAtTrue : cAtFalse;
    }

static eBool IsGood(AtRam self)
    {
    /* No testing operations have been run, need to return calib status */
    if (NoTestRun(self))
        return (AtRamInitStatusGet(self) == cAtOk) ? cAtTrue : cAtFalse;

    /* Otherwise, return so far so good status */
    if ((self->addressBusIsTested && !self->addressBusIsGood) ||
        (self->dataBusIsTested    && !self->dataBusIsGood)    ||
        (self->memoryIsTested     && !self->memoryIsGood))
        return cAtFalse;

    return cAtTrue;
    }

static void CpuRest(AtOsal osal, tAtOsalCurTime *previousTime, tAtOsalCurTime *currentTime)
    {
    static const uint16 cTimeToRestMs = 200;
    static const uint16 cRestTimeMs   = 50;
    uint32 diffMs;

    /* Give CPU a rest if this jobs take long time */
    diffMs = mMethodsGet(osal)->DifferenceTimeInMs(osal, currentTime, previousTime);
    if (diffMs < cTimeToRestMs)
        return;

    /* Rest and prepare for the next */
    mMethodsGet(osal)->MemCpy(osal, previousTime, currentTime, sizeof(tAtOsalCurTime));
    mMethodsGet(osal)->USleep(osal, cRestTimeMs * 1000UL);
    }

static eAtModuleRamRet DataBusDurationTest(AtRam self)
    {
    return AtRamDataBusTest(self, 0x2345);
    }

static eAtModuleRamRet MemoryDurationTest(AtRam self)
    {
    uint32 firErrorAddress;
    return AtRamMemoryTest(self, 0, AtRamMaxAddressGet(self), &firErrorAddress);
    }

static eAtModuleRamRet AllDurationTest(AtRam self)
    {
    eAtModuleRamRet ret;

    ret = AtRamAddressBusTest(self);
    if (ret != cAtOk)
        return ret;

    ret = DataBusDurationTest(self);
    if (ret != cAtOk)
        return ret;

    ret = MemoryDurationTest(self);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtModuleRamRet DurationTest(AtRam self, uint32 durationMs, eAtModuleRamRet (*testFunction)(AtRam self))
    {
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, currentTime, cpuRest;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Make sure that testing must run at least one time */
    if (durationMs == 0)
        durationMs = 1;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    mMethodsGet(osal)->MemCpy(osal, &cpuRest, &startTime, sizeof(tAtOsalCurTime));

    while (elapseTime < durationMs)
        {
        eAtModuleRamRet ret = testFunction(self);
        if (ret != cAtOk)
            return ret;

        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &currentTime, &startTime);
        CpuRest(osal, &cpuRest, &currentTime);
        }

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtRam ram = (AtRam)self;
    AtOsal osal = AtSharedDriverOsalGet();

    AtListDeleteWithObjectHandler(ram->dataBusTestErrors, AtObjectDelete);
    ram->dataBusTestErrors = NULL;
    AtObjectDelete((AtObject)(ram->dataBusErrorsIterator));
    ram->dataBusErrorsIterator = NULL;

    AtListDeleteWithObjectHandler(ram->addressBusTestErrors, AtObjectDelete);
    ram->addressBusTestErrors = NULL;
    AtObjectDelete((AtObject)(ram->addressBusErrorsIterator));
    ram->addressBusErrorsIterator = NULL;

    mMethodsGet(osal)->MemFree(osal, ram->cellContent2);
    mMethodsGet(osal)->MemFree(osal, ram->cellContent1);
    ram->cellContent1 = NULL;
    ram->cellContent2 = NULL;

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet MarginDetect(AtRam self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 ReadLeftMargin(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ReadRightMargin(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 WriteLeftMargin(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 WriteRightMargin(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmGet(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryGet(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtRam self)
    {
    AtUnused(self);
    return 0;
    }

static eBool AlarmIsSupported(AtRam self, uint32 alarm)
    {
    AtUnused(self);
    AtUnused(alarm);
    return cAtFalse;
    }

static uint32 CounterGet(AtRam self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(AtRam self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static eBool CounterIsSupported(AtRam self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static eAtRet HwTestStart(AtRam self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet HwTestStop(AtRam self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool HwTestIsStarted(AtRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DebugAddressBusSizeSet(AtRam self, uint32 busSize)
    {
    self->addressBusSize = busSize;
    return cAtOk;
    }

static eAtRet DebugDataBusSizeSet(AtRam self, uint32 busSize)
    {
    self->dataBusSize = busSize;
    return cAtOk;
    }

static eAtRet HwTestErrorForce(AtRam self, eBool forced)
    {
    AtUnused(self);
    return forced ? cAtErrorNotImplemented : cAtOk;
    }

static eBool HwTestErrorIsForced(AtRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const char *TypeString(AtRam self)
    {
    AtUnused(self);
    return "RAM";
    }

static eBool NeedClearWholeMemoryBeforeTesting(AtRam self)
    {
    /* Some products require this */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TestSetup(AtRam self)
    {
    eAtRet ret;
    uint32 firstErrorAddress, errorCount;

    if (!mMethodsGet(self)->NeedClearWholeMemoryBeforeTesting(self))
        return cAtOk;

    /* Clear the whole memory */
    ret = AtRamMemoryFill(self, 0);
    AtRamLog(self, (ret == cAtOk) ? cAtLogLevelInfo : cAtLogLevelWarning,
            AtSourceLocation, "Flush memory: %s\r\n", AtRet2String(ret));

    /* Make sure that they are all 0 */
    return MemoryReadCheckAndLog(self, cAtTrue, 0, 0, AtRamMaxAddressGet(self),
                                 &firstErrorAddress, &errorCount);
    }

static uint32 MaxBurst(AtRam self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet BurstSet(AtRam self, uint32 burst)
    {
    AtUnused(self);
    return (burst == 1) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 BurstGet(AtRam self)
    {
    AtUnused(self);
    return 1;
    }

static eBool BurstIsSupported(AtRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TestHwAssistEnable(AtRam self, eBool enable)
    {
    self->hwAssistDisabled = enable ? cAtFalse : cAtTrue;
    return cAtOk;
    }

static eBool TestHwAssistIsEnabled(AtRam self)
    {
    return self->hwAssistDisabled ? cAtFalse : cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtRam object = (AtRam)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    mEncodeObjectDescription(core);
    mEncodeUInt(ramId);

    mEncodeNone(addressBusTestErrors);
    mEncodeNone(addressBusErrorsIterator);
    mEncodeNone(dataBusTestErrors);
    mEncodeNone(dataBusErrorsIterator);
    mEncodeNone(memTestErrorCount);
    mEncodeNone(firstErrorAddress);
    mEncodeNone(cellContent1);
    mEncodeNone(cellContent2);
    mEncodeNone(addressBusIsTested);
    mEncodeNone(addressBusIsGood);
    mEncodeNone(dataBusIsTested);
    mEncodeNone(dataBusIsGood);
    mEncodeNone(memoryIsTested);
    mEncodeNone(memoryIsGood);
    mEncodeNone(buffer);

    mEncodeUInt(addressBusSize);
    mEncodeUInt(dataBusSize);
    mEncodeUInt(hwAssistDisabled);
    }

static AtErrorGenerator ErrorGeneratorGet(AtRam self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool ShouldClearStateMachineBeforeAccessing(AtRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const uint32 *LatchedErrorHandle(AtRam self, uint32 *numDwords)
    {
    AtUnused(self);
    AtUnused(numDwords);
    return NULL;
    }

static const uint32 *LatchedErrorData(AtRam self, uint32 *numDwords)
    {
    return LatchedErrorHandle(self, numDwords);
    }

static const uint32 *LatchedExpectedData(AtRam self, uint32 *numDwords)
    {
    return LatchedErrorHandle(self, numDwords);
    }

static const uint32 *LatchedErrorDataBits(AtRam self, uint32 *numDwords)
    {
    return LatchedErrorHandle(self, numDwords);
    }

static eBool ErrorLatchingSupported(AtRam self)
    {
    if (AtRamIsSimulated(self))
        return cAtTrue;

    AtUnused(self);
    return cAtFalse;
    }

static const uint32 *ErrorLatchingWithHandler(AtRam self, uint32 *numDwords, const uint32 *(*handler)(AtRam self, uint32 *numDwords))
    {
    /* The outside may not initialize this output parameter */
    if (numDwords)
        *numDwords = 0;

    if (self == NULL)
        return NULL;

    if (AtRamErrorLatchingSupported(self))
        return handler(self, numDwords);

    return NULL;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtModule module = (AtModule)AtRamModuleGet((AtRam)self);
    AtDevice dev = AtModuleDeviceGet(module);

    AtSnprintf(description, sizeof(description), "%s%s.%d", AtDeviceIdToString(dev), AtRamTypeString((AtRam)self), AtRamIdGet((AtRam)self) + 1);
    return description;
    }

static uint32 LatchedErrorAddress(AtRam self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet HwAssistAcceptanceTest(AtRam self)
    {
    return AtRamHwAssistAcceptanceTestImplement(self);
    }

static void MethodsInit(AtRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Common */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, ShouldClearStateMachineBeforeAccessing);

        mMethodOverride(m_methods, CellSizeGet);
        mMethodOverride(m_methods, CellSizeInBits);
        mMethodOverride(m_methods, CellRead);
        mMethodOverride(m_methods, CellWrite);

        mMethodOverride(m_methods, AddressBusSizeGet);
        mMethodOverride(m_methods, DataBusSizeGet);

        mMethodOverride(m_methods, AccessEnable);
        mMethodOverride(m_methods, AccessIsEnabled);

        mMethodOverride(m_methods, AddressBusTest);
        mMethodOverride(m_methods, DataBusTest);
        mMethodOverride(m_methods, MemoryTest);

        mMethodOverride(m_methods, MarginDetect);
        mMethodOverride(m_methods, ReadLeftMargin);
        mMethodOverride(m_methods, ReadRightMargin);
        mMethodOverride(m_methods, WriteLeftMargin);
        mMethodOverride(m_methods, WriteRightMargin);

        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, AlarmIsSupported);

        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, CounterIsSupported);

        mMethodOverride(m_methods, MemoryFill);
        mMethodOverride(m_methods, MemoryReadWriteCheck);
        mMethodOverride(m_methods, MemoryReadCheck);

        mMethodOverride(m_methods, CellAddressGet);
        mMethodOverride(m_methods, CellBankGet);
        mMethodOverride(m_methods, CellColumnGet);
        mMethodOverride(m_methods, CellRowGet);

        mMethodOverride(m_methods, InitStatusGet);

        mMethodOverride(m_methods, NumBanks);
        mMethodOverride(m_methods, NumColumns);
        mMethodOverride(m_methods, NumRows);

        mMethodOverride(m_methods, TestDurationSet);
        mMethodOverride(m_methods, TestDurationGet);

        mMethodOverride(m_methods, HwTestStart);
        mMethodOverride(m_methods, HwTestStop);
        mMethodOverride(m_methods, HwTestIsStarted);
        mMethodOverride(m_methods, HwTestErrorForce);
        mMethodOverride(m_methods, HwTestErrorIsForced);
        mMethodOverride(m_methods, LatchedErrorData);
        mMethodOverride(m_methods, LatchedExpectedData);
        mMethodOverride(m_methods, LatchedErrorDataBits);
        mMethodOverride(m_methods, LatchedErrorAddress);
        mMethodOverride(m_methods, ErrorLatchingSupported);

        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, IsTested);
        mMethodOverride(m_methods, IsGood);
        mMethodOverride(m_methods, NeedClearWholeMemoryBeforeTesting);

        mMethodOverride(m_methods, MaxBurst);
        mMethodOverride(m_methods, BurstSet);
        mMethodOverride(m_methods, BurstGet);
        mMethodOverride(m_methods, BurstIsSupported);

        mMethodOverride(m_methods, TestHwAssistEnable);
        mMethodOverride(m_methods, TestHwAssistIsEnabled);

        mMethodOverride(m_methods, ErrorGeneratorGet);
        mMethodOverride(m_methods, HwAssistAcceptanceTest);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtObject(self);
    }

void AtRamCpuRest(AtOsal osal, tAtOsalCurTime *previousTime)
    {
    tAtOsalCurTime currentTime;
    mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
    CpuRest(osal, previousTime, &currentTime);
    }

AtRam AtRamObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private attributes */
    self->module = ramModule;
    self->ramId = ramId;
    self->core  = core;
    self->memTestErrorCount = 0;
    self->addressBusTestErrors = AtListCreate(0);
    self->dataBusTestErrors    = AtListCreate(0);
    self->firstErrorAddress    = 0x0;

    return self;
    }

uint32 AtRamNumBanks(AtRam self)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->NumBanks(self);
    return 0;
    }

uint32 AtRamNumColumns(AtRam self)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->NumColumns(self);
    return 0;
    }

uint32 AtRamNumRows(AtRam self)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->NumRows(self);
    return 0;
    }

uint32 AtRamErrorCountGet(AtRam self)
    {
    if (RamIsValid(self))
        return self->memTestErrorCount;

    return 0;
    }

eAtRet AtRamMemoryFill(AtRam self, uint32 data)
    {
    eAtRet ret;
    eBool disableAccessAfterTesting = cAtFalse;

    if (!RamIsValid(self))
    	return cAtErrorNullPointer;

    if (!AtRamAccessIsEnabled(self))
        {
        AtRamAccessEnable(self, cAtTrue);
        disableAccessAfterTesting = cAtTrue;
        }

    /* Fill all memory to 0 */
    ret = MemoryFillAndLog(self, cAtTrue, 0x0, data, AtRamMaxAddressGet(self));

    /* If testing is enabled by this API, disable it */
    if (disableAccessAfterTesting)
        AtRamAccessEnable(self, cAtFalse);

    return ret;
    }

uint32 AtRamRead(AtRam self, uint32 address)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->Read(self, address);

    return cInvalidValue;
    }

void AtRamWrite(AtRam self, uint32 address, uint32 value)
    {
    if (RamIsValid(self))
        mMethodsGet(self)->Write(self, address, value);
    }

eBool AtRamCellValueIsValid(AtRam self, const uint32 *value)
    {
    uint32 lastValue = value[AtRamCellSizeGet(self) - 1];
    return (lastValue <= LastDwordMask(self)) ? cAtTrue : cAtFalse;
    }

void AtRamLog(AtRam self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...)
    {
    va_list args;
    if (self == NULL)
        return;

    va_start(args, format);
    AtDriverVaListLog(AtDriverSharedDriverGet(), level, file, line, format, args);
    va_end(args);
    }

const char *AtRamTypeString(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->TypeString(self);
    return NULL;
    }

eBool AtRamIsSimulated(AtRam self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)AtRamModuleGet(self)));
    }

eBool AtRamShouldClearStateMachineBeforeAccessing(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->ShouldClearStateMachineBeforeAccessing(self);
    return cAtFalse;
    }

eAtRet AtRamHwAssistAcceptanceTest(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->HwAssistAcceptanceTest(self);
    return cAtErrorNullPointer;
    }

/**
 * @addtogroup AtRam
 * @{
 */

/**
 * Initialize RAM
 *
 * @param self This RAM
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamInit(AtRam self)
    {
    mAttributeGet(Init, eAtModuleRamRet, cAtErrorNullPointer);
    }

/**
 * Get the module that manages this RAM
 *
 * @param self This RAM
 * @return RAM module
 */
AtModuleRam AtRamModuleGet(AtRam self)
    {
    if (RamIsValid(self))
        return self->module;

    return NULL;
    }

/**
 * Get IP core that this RAM is working with
 *
 * @param self This RAM
 *
 * @return IP Core
 */
AtIpCore AtRamIpCoreGet(AtRam self)
    {
    if (RamIsValid(self))
        return self->core;

    return NULL;
    }

/**
 * Get ID of this RAM
 *
 * @param self This RAM
 *
 * @return RAM unique flat ID
 */
uint8 AtRamIdGet(AtRam self)
    {
    if (RamIsValid(self))
        return self->ramId;

    return 0;
    }

/**
 * Get size of a memory cell of RAM. Unit is word 32bits
 *
 * @param self This RAM
 * @return Cell size in number of 32-bit words
 */
uint32 AtRamCellSizeGet(AtRam self)
    {
    mAttributeGet(CellSizeGet, uint32, 0);
    }

/**
 * Get size of a memory cell of RAM. Unit is in bit
 *
 * @param self This RAM
 * @return Cell size in number of bits
 */
uint32 AtRamCellSizeInBits(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->CellSizeInBits(self);
    return 0x0;
    }

/**
 * Read a memory cell
 *
 * @param self This RAM
 * @param address Cell address
 * @param value Cell value
 *
 * @return AT return code
 */
eAtRet AtRamCellRead(AtRam self, uint32 address, uint32 *value)
    {
    if (value == NULL)
        return cAtErrorNullPointer;

    if (address > AtRamMaxAddressGet(self))
        return cAtErrorOutOfRangParm;

    if (RamIsValid(self))
        return CellReadWithLog(self, address, value);

    return cAtError;
    }

/**
 * Write value to memory cell
 *
 * @param self This RAM
 * @param address Cell address
 * @param value Cell value
 *
 * @return AT return code
 */
eAtRet AtRamCellWrite(AtRam self, uint32 address, const uint32 *value)
    {
    if (value == NULL)
        return cAtErrorNullPointer;

    if (address > AtRamMaxAddressGet(self))
        return cAtErrorOutOfRangParm;

    if (RamIsValid(self))
        return CellWriteWithLog(self, address, value);

    return cAtError;
    }

/**
 * Get flat address from bank, column and row
 *
 * @param self This RAM
 * @param bank Bank
 * @param row Row
 * @param column Column
 *
 * @return flat address
 */
uint32 AtRamCellAddressGet(AtRam self, uint32 bank, uint32 row, uint32 column)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->CellAddressGet(self, bank, row, column);
    return 0;
    }

/**
 * Get bank from flat address
 *
 * @param self This RAM
 * @param address Cell address
 *
 * @return Return bank if success, else return 0xFFFFFFFF
 */
uint32 AtRamCellBankGet(AtRam self, uint32 address)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->CellBankGet(self, address);
    return cInvalidValue;
    }

/**
 * Get row from address
 * @param self This RAM
 * @param address Cell address
 *
 * @return Return row if success, else return 0xFFFFFFFF
 */
uint32 AtRamCellRowGet(AtRam self, uint32 address)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->CellRowGet(self, address);
    return cInvalidValue;
    }

/**
 * Get column from address
 *
 * @param self This RAM
 * @param address Cell address
 *
 * @return Return column if success, else return 0xFFFFFFFF
 */
uint32 AtRamCellColumnGet(AtRam self, uint32 address)
    {
    if (RamIsValid(self))
        return mMethodsGet(self)->CellColumnGet(self, address);
    return cInvalidValue;
    }

/**
 * Test address bus
 *
 * @param self This RAM
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamAddressBusTest(AtRam self)
    {
    eAtModuleRamRet ret;

    if (!RamIsValid(self))
        return cAtErrorInvlParm;

    /* Make sure that RAM is initialized properly */
    ret = AtRamInitStatusGet(self);
    if (ret != cAtOk)
        return ret;

    TestSetup(self);
    ret = mMethodsGet(self)->AddressBusTest(self);

    self->addressBusIsGood = cAtFalse;
    if ((ret == cAtOk) && (AtRamAddressBusTestErrorCount(self) == 0))
        self->addressBusIsGood = cAtTrue;
    self->addressBusIsTested = cAtTrue;

    return ret;
    }

/**
 * Test data bus
 *
 * @param self This RAM
 * @param address Address
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamDataBusTest(AtRam self, uint32 address)
    {
    eAtModuleRamRet ret;

    if (!RamIsValid(self))
        return cAtErrorInvlParm;

    /* Make sure that RAM is initialized properly */
    ret = AtRamInitStatusGet(self);
    if (ret != cAtOk)
        return ret;

    /* And test it */
    TestSetup(self);
    ret = mMethodsGet(self)->DataBusTest(self, address);

    self->dataBusIsGood = cAtFalse;
    if ((ret == cAtOk) && (AtRamDataBusTestErrorCount(self) == 0))
        self->dataBusIsGood = cAtTrue;
    self->dataBusIsTested = cAtTrue;

    return ret;
    }

/**
 * Test memory
 *
 * @param self This RAM
 * @param startAddress Start address
 * @param endAddress End address
 * @param [out] firstErrorAddress First error address
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamMemoryTest(AtRam self, uint32 startAddress, uint32 endAddress, uint32 *firstErrorAddress)
    {
    eAtModuleRamRet ret;
    uint32 errorCount = 0;

    if (!RamIsValid(self))
        return cAtErrorInvlParm;

    if (!AddressRangeIsValid(self, startAddress, endAddress))
        return cAtErrorInvlParm;

    /* Make sure that RAM is initialized properly */
    ret = AtRamInitStatusGet(self);
    if (ret != cAtOk)
        return ret;

    TestSetup(self);
    return mMethodsGet(self)->MemoryTest(self, startAddress, endAddress, firstErrorAddress, &errorCount);
    }

/**
 * Enable or disable the accessing RAM from CPU
 *
 * @param self This RAM
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamAccessEnable(AtRam self, eBool enable)
    {
    mNumericalAttributeSet(AccessEnable, enable);
    }

/**
 * Check whether the accessing RAM from CPU is enabled or not
 *
 * @param self This RAM
 *
 * @return cAtTrue if the accessing is enabled, else cAtFalse
 */
eBool AtRamAccessIsEnabled(AtRam self)
    {
    mAttributeGet(AccessIsEnabled, eBool, cAtFalse);
    }

/**
 * Get initialization status
 *
 * @param self This RAM
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamInitStatusGet(AtRam self)
    {
    if (!RamIsValid(self))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->InitStatusGet(self);
    }

/**
 * Set duration for testing. The testing progress may take so long to complete,
 * this API is to limit this testing time.
 *
 * @param self This RAM
 * @param durationInMs Testing duration in milliseconds. If it is 0, default
 *                     implementation is used. The default implementation depends
 *                     on each concrete RAM, it can use default testing duration
 *                     or test until done.
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamTestDurationSet(AtRam self, uint32 durationInMs)
    {
    mNumericalAttributeSet(TestDurationSet, durationInMs);
    }

/**
 * Get testing duration
 *
 * @param self This RAM
 *
 * @return Testing duration
 */
uint32 AtRamTestDurationGet(AtRam self)
    {
    mAttributeGet(TestDurationGet, uint32, 0);
    }

/**
 * To show debugging information of RAM
 *
 * @param self
 */
void AtRamDebug(AtRam self)
    {
    if (RamIsValid(self))
        mMethodsGet(self)->Debug(self);
    }

/**
 * Get maximum RAM address
 *
 * @param self This RAM
 *
 * @return Max address
 */
uint32 AtRamMaxAddressGet(AtRam self)
    {
    if (RamIsValid(self))
        return BusMask(AddressBusSize(self));

    return 0;
    }

/**
 * Check if all of RAM test operations are tested
 *
 * @param self This RAM
 *
 * @retval cAtTrue if all of operations are tested
 * @retval cAtFalse if at least one of operations has not been tested
 */
eBool AtRamIsTested(AtRam self)
    {
    mAttributeGet(IsTested, eBool, cAtFalse);
    }

/**
 * Check whether RAM testing is good
 *
 * @param self This RAM
 * @return cAtTrue if RAM is good
 */
eBool AtRamIsGood(AtRam self)
    {
    mAttributeGet(IsGood, eBool, cAtFalse);
    }

/**
 * Test address bus with specified duration.
 *
 * @param self This RAM
 * @param durationMs Testing duration in milliseconds.
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamAddressBusTestWithDuration(AtRam self, uint32 durationMs)
    {
    return DurationTest(self, durationMs, AtRamAddressBusTest);
    }

/**
 * Test data bus with specified duration.
 *
 * @param self This RAM
 * @param durationMs Testing duration in milliseconds.
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamDataBusTestWithDuration(AtRam self, uint32 durationMs)
    {
    return DurationTest(self, durationMs, DataBusDurationTest);
    }

/**
 * Test memory with specified duration.
 *
 * @param self This RAM
 * @param durationMs Testing duration in milliseconds.
 *
 * @return Return cAtOk if test is pass, else test is failed
 */
eAtModuleRamRet AtRamMemoryTestWithDuration(AtRam self, uint32 durationMs)
    {
    return DurationTest(self, durationMs, MemoryDurationTest);
    }

/**
 * Test duration for all mode test of ram.
 *
 * @param self This RAM
 * @param durationMs Testing duration in milliseconds.
 *
 * @return AT return code
 */
eAtModuleRamRet AtRamTestWithDuration(AtRam self, uint32 durationMs)
    {
    return DurationTest(self, durationMs, AllDurationTest);
    }

/**
 * Get first error address.
 *
 * @param self This RAM
 *
 * @return First error address. Note, value is only valid when memory testing is
 *         fail.
 */
uint32 AtRamFirstErrorAddressGet(AtRam self)
    {
    return RamIsValid(self) ? self->firstErrorAddress : 0x0;
    }

/**
 * Get number of error detected in data bus testing
 *
 * @param self This Ram
 * @return Number of error
 */
uint32 AtRamDataBusTestErrorCount(AtRam self)
    {
    if (!RamIsValid(self))
        return 0;

    return AtListLengthGet(self->dataBusTestErrors);
    }

/**
 * Get errors detected in data bus test process. This API need to be called in a loop, each time, an error will be returned
 * until there is no more error.
 * Iterator will be restarted to the first error after API AtRamDataBusTest is called or when there is no more error
 *
 * @param self This RAM
 * @return an error if there is still error or NULL if there is no more error
 */
AtRamTestError AtRamDataBusTestNextError(AtRam self)
    {
    AtRamTestError error;

    if (self->dataBusTestErrors == NULL)
        return NULL;

    if (self->dataBusErrorsIterator == NULL)
        self->dataBusErrorsIterator = AtListIteratorCreate(self->dataBusTestErrors);

    if ((error = (AtRamTestError)AtIteratorNext(self->dataBusErrorsIterator)) != NULL)
        return error;

    /* No more error to return */
    AtObjectDelete((AtObject)(self->dataBusErrorsIterator));
    self->dataBusErrorsIterator = NULL;

    return NULL;
    }

/**
 * Get number of error detected in address bus testing
 *
 * @param self This Ram
 * @return Number of error
 */
uint32 AtRamAddressBusTestErrorCount(AtRam self)
    {
    if (!RamIsValid(self))
        return 0;

    return AtListLengthGet(self->addressBusTestErrors);
    }

/**
 * Get errors detected in address bus test process. This API need to be called in a loop, each time, an error will be returned
 * until there is no more error.
 * Iterator will be restarted to the first error after API AtRamAddressBusTest is called or when there is no more error
 *
 * @param self This RAM
 * @return an error if there is still error or NULL if there is no more error
 */
AtRamTestError AtRamAddressBusTestNextError(AtRam self)
    {
    AtRamTestError error;

    if (self->addressBusTestErrors == NULL)
        return NULL;

    if (self->addressBusErrorsIterator == NULL)
        self->addressBusErrorsIterator = AtListIteratorCreate(self->addressBusTestErrors);

    if ((error = (AtRamTestError)AtIteratorNext(self->addressBusErrorsIterator)) != NULL)
        return error;

    /* No more error to return */
    AtObjectDelete((AtObject)(self->addressBusErrorsIterator));
    self->addressBusErrorsIterator = NULL;

    return NULL;
    }

/**
 * Get number of error detected in RAM memory testing
 *
 * @param self This RAM
 * @param firstErrorAddress First error address, this value is valid only when returned error counter larger than 0
 * @return Number of error
 */
uint32 AtRamMemoryTestErrorCount(AtRam self, uint32 *firstErrorAddress)
    {
    if (!RamIsValid(self))
        return 0;

    *firstErrorAddress = self->firstErrorAddress;
    return self->memTestErrorCount;
    }

/**
 * Detect RAM left/right margin. This API should be called before reading
 * read/write left/right margins.
 *
 * @param self This RAM

 * @return At return code
 */
eAtRet AtRamMarginDetect(AtRam self)
    {
    mAttributeGet(MarginDetect, eAtModuleRamRet, cAtErrorNullPointer);
    }

/**
 * Get RAM read left margin
 *
 * @param self This RAM
 *
 * @return Read left margin in picosecond unit
 */
uint32 AtRamReadLeftMargin(AtRam self)
    {
    mAttributeGet(ReadLeftMargin, uint32, 0);
    }

/**
 * Get RAM read right margin
 *
 * @param self This RAM
 *
 * @return Read right margin in picosecond unit
 */
uint32 AtRamReadRightMargin(AtRam self)
    {
    mAttributeGet(ReadRightMargin, uint32, 0);
    }

/**
 * Get RAM write left margin
 *
 * @param self This RAM
 *
 * @return Write left margin in picosecond unit
 */
uint32 AtRamWriteLeftMargin(AtRam self)
    {
    mAttributeGet(WriteLeftMargin, uint32, 0);
    }

/**
 * Get RAM write right margin
 *
 * @param self This RAM
 *
 * @return Write right margin in picosecond unit
 */
uint32 AtRamWriteRightMargin(AtRam self)
    {
    mAttributeGet(WriteRightMargin, uint32, 0);
    }

/**
 * Get current alarm status
 *
 * @param self This RAM
 *
 * @return @ref eAtRamAlarm "Current alarm status"
 */
uint32 AtRamAlarmGet(AtRam self)
    {
    mAttributeGet(AlarmGet, uint32, 0);
    }

/**
 * Get alarm change history
 *
 * @param self This RAM
 *
 * @return @ref eAtRamAlarm "Alarm" change history
 */
uint32 AtRamAlarmHistoryGet(AtRam self)
    {
    mAttributeGet(AlarmHistoryGet, uint32, 0);
    }

/**
 * Get then clean alarm change history
 *
 * @param self This RAM
 *
 * @return @ref eAtRamAlarm "Alarm" change history before clearing
 */
uint32 AtRamAlarmHistoryClear(AtRam self)
    {
    mAttributeGet(AlarmHistoryClear, uint32, 0);
    }

/**
 * Check if a specific alarm is supported
 *
 * @param self This RAM
 * @param alarm @ref eAtRamAlarm "Alarm" to be checked
 *
 * @retval cAtTrue if input alarm type is supported
 * @retval cAtFalse if input alarm type is not supported
 *
 * @note Only one alarm type should be input. If ORed alarm types is input, the
 *       result is not determined and the internal implementation may return
 *       cAtTrue if at least one input alarm is supported.
 */
eBool AtRamAlarmIsSupported(AtRam self, uint32 alarm)
    {
    mOneParamAttributeGet(AlarmIsSupported, alarm, eBool, cAtFalse);
    }

/**
 * Read-only a counter
 *
 * @param self This RAM
 * @param counterType @ref eAtRamCounterType "Common counter type". Each concrete
 *        RAM such as DDR, QDR may have additional counters.
 *
 * @return Counter value
 *
 * @note A counter may not be supported by a RAM, use AtRamCounterIsSupported()
 *       to check if it is supported or not.
 */
uint32 AtRamCounterGet(AtRam self, uint32 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterGet(self, counterType);
    return 0;
    }

/**
 * Read then clear a counter
 *
 * @param self This RAM
 * @param counterType @ref eAtRamCounterType "Common counter type". Each concrete
 *        RAM such as DDR, QDR may have additional counters.
 *
 * @return Counter value before clearing
 *
 * @note A counter may not be supported by a RAM, use AtRamCounterIsSupported()
 *       to check if it is supported or not.
 */
uint32 AtRamCounterClear(AtRam self, uint32 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterClear(self, counterType);
    return 0;
    }

/**
 * Check if a specific counter type is supported or not
 *
 * @param self This RAM
 * @param counterType @ref eAtRamCounterType "Common counter type". Each concrete
 *        RAM such as DDR, QDR may have additional counters.
 *
 * @retval cAtTrue if input counter type is supported
 * @retval cAtFalse if input counter type is not supported
 */
eBool AtRamCounterIsSupported(AtRam self, uint32 counterType)
    {
    mOneParamAttributeGet(CounterIsSupported, counterType, eBool, cAtFalse);
    }

/**
 * Start hardware base testing
 *
 * @param self This RAM
 *
 * @return AT return code
 */
eAtRet AtRamHwTestStart(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->HwTestStart(self);
    return cAtErrorNullPointer;
    }

/**
 * Stop hardware base testing
 *
 * @param self This RAM
 *
 * @return AT return code
 */
eAtRet AtRamHwTestStop(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->HwTestStop(self);
    return cAtErrorNullPointer;
    }

/**
 * To check if hardware base testing has been started or not.
 *
 * @param self This RAM
 *
 * @retval cAtTrue if started
 * @retval cAtFalse if not started
 */
eBool AtRamHwTestIsStarted(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->HwTestIsStarted(self);
    return cAtFalse;
    }

/**
 * Force/unforce error in case of hardware base testing
 *
 * @param self This RAM
 * @param forced Force.
 *               - cAtTrue to force error
 *               - cAtFalse to unforce error
 *
 * @return AT return code
 */
eAtRet AtRamHwTestErrorForce(AtRam self, eBool forced)
    {
    if (self)
        return mMethodsGet(self)->HwTestErrorForce(self, forced);
    return cAtErrorNullPointer;
    }

/**
 * To check if error is forced in case of hardware base testing
 *
 * @param self This RAM
 *
 * @retval cAtTrue if error is being forced.
 * @retval cAtFalse if error is not being forced.
 */
eBool AtRamHwTestErrorIsForced(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->HwTestErrorIsForced(self);
    return cAtFalse;
    }

/**
 * When there is error with PRBS testing, hardware will latch error data
 *
 * @param self This RAM
 * @param [out] numDwords Number of dwords
 *
 * @return Latched error data
 * @see AtRamLatchedExpectedData()
 */
const uint32 *AtRamLatchedErrorData(AtRam self, uint32 *numDwords)
    {
    return ErrorLatchingWithHandler(self, numDwords, mMethodsGet(self)->LatchedErrorData);
    }

/**
 * When there is error with PRBS testing, hardware will latch data error along with
 * expected data
 *
 * @param self This RAM
 * @param numDwords Number of dwords
 *
 * @return Expected data
 */
const uint32 *AtRamLatchedExpectedData(AtRam self, uint32 *numDwords)
    {
    return ErrorLatchingWithHandler(self, numDwords, mMethodsGet(self)->LatchedExpectedData);
    }

/**
 * When there is error with PRBS testing, hardware will latch data error along with
 * expected data and returned value of this API is to indicate what bits are mismatched
 *
 * @param self This RAM
 * @param numDwords Number of dwords
 *
 * @return Latched data error bits
 */
const uint32 *AtRamLatchedErrorDataBits(AtRam self, uint32 *numDwords)
    {
    return ErrorLatchingWithHandler(self, numDwords, mMethodsGet(self)->LatchedErrorDataBits);
    }

/**
 * To know the latched error address
 *
 * @param self This RAM
 *
 * @return Latched error address
 */
uint32 AtRamLatchedErrorAddress(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->LatchedErrorAddress(self);
    return cInvalidUint32;
    }

/**
 * Check if PRBS test support error latching
 *
 * @param self This RAM
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtRamErrorLatchingSupported(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->ErrorLatchingSupported(self);
    return cAtFalse;
    }

/**
 * Get address bus size
 *
 * @param self This RAM
 * @return Address bus size
 */
uint32 AtRamAddressBusSizeGet(AtRam self)
    {
    if (self)
        return AddressBusSize(self);
    return 0x0;
    }

/**
 * Get data bus size
 *
 * @param self This RAM
 *
 * @return Data bus size
 */
uint32 AtRamDataBusSizeGet(AtRam self)
    {
    if (self)
        return DataBusSize(self);
    return 0x0;
    }

/**
 * This API is only used for debugging purpose. This is to test
 * address bus with specified size.
 *
 * @param self This RAM
 * @param busSize Bus size. If this is set to 0, the default bus size will be used.
 *
 * @return AT return code.
 */
eAtRet AtRamDebugAddressBusSizeSet(AtRam self, uint32 busSize)
    {
    if (self)
        return DebugAddressBusSizeSet(self, busSize);
    return cAtErrorNullPointer;
    }

/**
 * This API is only used for debugging purpose. This is to test
 * data bus with specified size.
 *
 * @param self This RAM.
 * @param busSize Bus size. If this is set to 0, the default bus size will be used.
 *
 * @return AT return code
 */
eAtRet AtRamDebugDataBusSizeSet(AtRam self, uint32 busSize)
    {
    if (self)
        return DebugDataBusSizeSet(self, busSize);
    return cAtErrorNullPointer;
    }

/**
 * Get maximum number of addresses hardware can do burst read/write
 *
 * @param self This RAM
 * @return Maximum number of addresses hardware can do burst read/write
 */
uint32 AtRamTestMaxBurst(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->MaxBurst(self);
    return 0;
    }

/**
 * Configure number of addresses hardware can do burst read/write
 *
 * @param self This RAM
 * @param burst Number of addresses hardware can do burst read/write
 *
 * @return AT return code
 */
eAtRet AtRamTestBurstSet(AtRam self, uint32 burst)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    if ((burst == 0) || (burst > AtRamTestMaxBurst(self)))
        return cAtErrorOutOfRangParm;
    return mMethodsGet(self)->BurstSet(self, burst);
    }

/**
 * Get number of addresses hardware uses to do burst read/write
 *
 * @param self This RAM
 *
 * @return Burst side
 */
uint32 AtRamTestBurstGet(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->BurstGet(self);
    return 0;
    }

/**
 * To check if RAM burst testing is supported or not
 *
 * @param self This RAM
 * @retval cAtTrue if burst testing is supported
 * @retval cAtFalse if burst testing is not supported
 */
eBool AtRamTestBurstIsSupported(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->BurstIsSupported(self);
    return cAtFalse;
    }

/**
 * Enable/disable hardware assist mode
 *
 * @param self This RAM
 * @param enable Assist enabling.
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtRamTestHwAssistEnable(AtRam self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TestHwAssistEnable(self, enable);
    return cAtErrorNullPointer;
    }

/**
 * Check if hardware assist is enabled or not
 *
 * @param self This RAM
 *
 * @retval cAtTrue if hardware assist is enabled
 * @retval cAtFalse if hardware assist is disabled
 */
eBool AtRamTestHwAssistIsEnabled(AtRam self)
    {
    if (self)
        return mMethodsGet(self)->TestHwAssistIsEnabled(self);
    return cAtFalse;
    }

/**
 * Measure calibration time
 *
 * @param self This RAM
 *
 * @return Calibration time in ms
 */
uint32 AtRamCalibMeasure(AtRam self)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    tAtOsalCurTime curTime, prevTime;
    const uint32 cTimeoutTimeInMs = 30000;
    uint32 elapseTime = 0;

    if (self == NULL)
        return 0x0;

    ret = AtDeviceAllCoresReset(device);
    if (ret != cAtOk)
        return 0x0;

    /* Keep track time when mesuring calib */
    AtOsalCurTimeGet(&prevTime);

    while (elapseTime < cTimeoutTimeInMs)
        {
        if (AtRamInitStatusGet(self) == cAtOk)
            {
            AtOsalCurTimeGet(&curTime);
            return AtOsalDifferenceTimeInMs(&curTime, &prevTime);
            }

        AtOsalCurTimeGet(&curTime);
        elapseTime = AtOsalDifferenceTimeInMs(&curTime, &prevTime);
        }

    return elapseTime;
    }

/**
 * Get error generator
 *
 * @param self This RAM
 *
 * @return The Error generator object
 */
AtErrorGenerator AtRamErrorGeneratorGet(AtRam self)
    {
    mAttributeGet(ErrorGeneratorGet, AtErrorGenerator, NULL);
    }

/** @} */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtRamTestErrorInternal.h
 * 
 * Created Date: Mar 10, 2015
 *
 * Description : RAM test error definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRAMTESTERRORINTERNAL_C_
#define _ATRAMTESTERRORINTERNAL_C_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtObjectInternal.h"
#include "AtRamTestError.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtRamTestErrorMethods
    {
    const char* (*Description)(AtRamTestError self);
    }tAtRamTestErrorMethods;

typedef struct tAtRamTestError
    {
    tAtObject super;
    const tAtRamTestErrorMethods *methods;

    uint32 errorAddress;
    uint32 actualValue;
    uint32 expectedValue;
    }tAtRamTestError;

typedef struct tAtRamAddressBusTestError
    {
    tAtRamTestError super;

    /* Error caused by writing pattern to address */
    uint32 causedPattern;
    uint32 causedAddress;
    }tAtRamAddressBusTestError;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRamTestError AtRamTestErrorObjectInit(AtRamTestError self, uint32 errorAddress, uint32 expectedValue, uint32 actualValue);

AtRamTestError AtRamTestErrorNew(uint32 errorAddress, uint32 expectedValue, uint32 actualValue);
AtRamTestError AtRamAddressBusTestErrorNew(uint32 errorAddress,
                                           uint32 actualValue,
                                           uint32 expectedValue,
                                           uint32 causedAddress,
                                           uint32 causedPattern);

#ifdef __cplusplus
}
#endif
#endif /* _ATRAMTESTERRORINTERNAL_C_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtRamInternal.h
 * 
 * Created Date: Jan 31, 2013
 *
 * Description : RAM class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRAMINTERNAL_H_
#define _ATRAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "AtRam.h"
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* RAM methods */
typedef struct tAtRamMethods
    {
	eAtModuleRamRet (*Init)(AtRam self);
	const char *(*TypeString)(AtRam self);

    /* Enable CPU accessing RAM */
	eAtModuleRamRet (*AccessEnable)(AtRam self, eBool enable);
    eBool (*AccessIsEnabled)(AtRam self);

    /* Read/write */
    uint32 (*Read)(AtRam self, uint32 address);
    void (*Write)(AtRam self, uint32 address, uint32 value);
    eBool (*ShouldClearStateMachineBeforeAccessing)(AtRam self);

    /* Read/write a memory cell in Ram */
    uint32 (*CellSizeGet)(AtRam self);
    uint32 (*CellSizeInBits)(AtRam self);
    eAtRet (*CellRead)(AtRam self, uint32 address, uint32 *value);
    eAtRet (*CellWrite)(AtRam self, uint32 address, const uint32 *value);

    /* Ram testing */
    uint32 (*AddressBusSizeGet)(AtRam self);
    uint32 (*DataBusSizeGet)(AtRam self);

    eAtModuleRamRet (*AddressBusTest)(AtRam self);
    eAtModuleRamRet (*DataBusTest)(AtRam self, uint32 address);
    eAtModuleRamRet (*MemoryTest)(AtRam self, uint32 startAddress, uint32 endAddress, uint32 *firstErrorAddress, uint32 *memTestErrorCount);
    eBool (*IsTested)(AtRam self);
    eBool (*IsGood)(AtRam self);
    
    /* Margin detection */
    eAtRet (*MarginDetect)(AtRam self);
    uint32 (*ReadLeftMargin)(AtRam self);
    uint32 (*ReadRightMargin)(AtRam self);
    uint32 (*WriteLeftMargin)(AtRam self);
    uint32 (*WriteRightMargin)(AtRam self);

    /* Alarms */
    uint32 (*AlarmGet)(AtRam self);
    uint32 (*AlarmHistoryGet)(AtRam self);
    uint32 (*AlarmHistoryClear)(AtRam self);
    eBool (*AlarmIsSupported)(AtRam self, uint32 alarm);

    /* Counters */
    uint32 (*CounterGet)(AtRam self, uint32 counterType);
    uint32 (*CounterClear)(AtRam self, uint32 counterType);
    eBool (*CounterIsSupported)(AtRam self, uint32 counterType);

    eAtModuleRamRet (*TestDurationSet)(AtRam self, uint32 durationInMs);
    uint32 (*TestDurationGet)(AtRam self);
    eAtModuleRamRet (*MemoryFill)(AtRam self, eBool addressIsIncreased, uint32 data, uint32 startAddress, uint32 endAddress);
    eAtModuleRamRet (*MemoryReadWriteCheck)(AtRam self,
                                            eBool addressIsIncreased,
                                            uint32 expectedReadData,
                                            uint32 writeData,
                                            uint32 startAddress,
                                            uint32 endAddress,
                                            uint32 *firstErrorAddress,
                                            uint32 *memTestErrorCount);

    eAtModuleRamRet (*MemoryReadCheck)(AtRam self,
                                       eBool addressIsIncreased,
                                       uint32 expectedData,
                                       uint32 startAddress,
                                       uint32 endAddress,
                                       uint32 *firstErrorAddress,
                                       uint32 *memTestErrorCount);

    uint32 (*CellAddressGet)(AtRam self, uint32 bank, uint32 row, uint32 column);
    uint32 (*CellBankGet)(AtRam self, uint32 address);
    uint32 (*CellColumnGet)(AtRam self, uint32 address);
    uint32 (*CellRowGet)(AtRam self, uint32 address);

    void (*Debug)(AtRam self);

    /* For hardware base testing */
    eAtRet (*HwTestErrorForce)(AtRam self, eBool forced);
    eBool (*HwTestErrorIsForced)(AtRam self);
    eAtRet (*HwTestStart)(AtRam self);
    eAtRet (*HwTestStop)(AtRam self);
    eBool (*HwTestIsStarted)(AtRam self);
    const uint32 *(*LatchedErrorData)(AtRam self, uint32 *numDwords);
    const uint32 *(*LatchedExpectedData)(AtRam self, uint32 *numDwords);
    const uint32 *(*LatchedErrorDataBits)(AtRam self, uint32 *numDwords);
    uint32 (*LatchedErrorAddress)(AtRam self);
    eBool (*ErrorLatchingSupported)(AtRam self);

    /* Bursting */
    uint32 (*MaxBurst)(AtRam self);
    eAtRet (*BurstSet)(AtRam self, uint32 burst);
    uint32 (*BurstGet)(AtRam self);
    eBool (*BurstIsSupported)(AtRam self);

    /* Hardware assist */
    eAtRet (*TestHwAssistEnable)(AtRam self, eBool enable);
    eBool (*TestHwAssistIsEnabled)(AtRam self);
    eAtRet (*HwAssistAcceptanceTest)(AtRam self);

    /* Internal */
    eAtModuleRamRet (*InitStatusGet)(AtRam self);
    uint32 (*NumBanks)(AtRam self);
    uint32 (*NumColumns)(AtRam self);
    uint32 (*NumRows)(AtRam self);

    eBool (*NeedClearWholeMemoryBeforeTesting)(AtRam self);
    AtErrorGenerator (*ErrorGeneratorGet)(AtRam self);
    }tAtRamMethods;

/* RAM class representation */
typedef struct tAtRam
    {
    tAtObject super;
    const tAtRamMethods *methods;

    /* Private data */
    AtModuleRam module;
    AtIpCore core;
    uint8 ramId;

    AtList addressBusTestErrors;
    AtIterator addressBusErrorsIterator;

    AtList dataBusTestErrors;
    AtIterator dataBusErrorsIterator;

    uint32 memTestErrorCount;
    uint32 firstErrorAddress;

    /* To avoid allocating many times */
    uint32 *cellContent1;
    uint32 *cellContent2;

	/* Save RAM testing status */
    eBool addressBusIsTested;
    eBool addressBusIsGood;
    eBool dataBusIsTested;
    eBool dataBusIsGood;
    eBool memoryIsTested;
    eBool memoryIsGood;

    /* Just for debugging purpose */
    uint32 addressBusSize;
    uint32 dataBusSize;

    /* Just for debugging purpose */
    char buffer[200];
    eBool hwAssistDisabled;
    }tAtRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtRam AtRamObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

/* Read-only attributes */
const char *AtRamTypeString(AtRam self);
uint32 AtRamNumBanks(AtRam self);
uint32 AtRamNumColumns(AtRam self);
uint32 AtRamNumRows(AtRam self);

uint32 AtRamRead(AtRam self, uint32 address);
void AtRamWrite(AtRam self, uint32 address, uint32 value);
eBool AtRamShouldClearStateMachineBeforeAccessing(AtRam self);

/* Helpers */
void AtRamCpuRest(AtOsal osal, tAtOsalCurTime *previousTime);
eAtRet AtRamMemoryFill(AtRam self, uint32 data);
eBool AtRamCellValueIsValid(AtRam self, const uint32 *value);

void AtRamLog(AtRam self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...) AtAttributePrintf(5, 6);
eBool AtRamIsSimulated(AtRam self);

eAtRet AtRamHwAssistAcceptanceTest(AtRam self);
eAtRet AtRamHwAssistAcceptanceTestImplement(AtRam self); /* Private use */

#ifdef __cplusplus
}
#endif
#endif /* _ATRAMINTERNAL_H_ */

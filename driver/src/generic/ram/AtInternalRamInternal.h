/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtInternalRamInternal.h
 * 
 * Created Date: Dec 4, 2015
 *
 * Description : Internal RAM representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTERNALRAMINTERNAL_H_
#define _ATINTERNALRAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtInternalRam.h"
#include "AtRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtInternalRamMethods
    {
    const char* (*Name)(AtInternalRam self);

    eAtRet (*ParityMonitorEnable)(AtInternalRam self, eBool enable);
    eBool  (*ParityMonitorIsEnabled)(AtInternalRam self);
    eBool (*ParityMonitorIsSupported)(AtInternalRam self);
    eAtRet (*EccMonitorEnable)(AtInternalRam self, eBool enable);
    eBool  (*EccMonitorIsEnabled)(AtInternalRam self);
    eBool (*EccMonitorIsSupported)(AtInternalRam self);
    eBool (*EccMonitorCanEnable)(AtInternalRam self, eBool enable);
    eAtRet (*CrcMonitorEnable)(AtInternalRam self, eBool enable);
    eBool  (*CrcMonitorIsEnabled)(AtInternalRam self);
    eBool (*CrcMonitorIsSupported)(AtInternalRam self);
    eBool (*CrcMonitorCanEnable)(AtInternalRam self, eBool enable);
    eAtRet (*ErrorForce)(AtInternalRam self, uint32 errors);
    eAtRet (*ErrorUnForce)(AtInternalRam self, uint32 errors);
    uint32 (*ForcedErrorsGet)(AtInternalRam self);
    uint32 (*ForcableErrorsGet)(AtInternalRam self);
    uint32 (*ErrorHistoryGet)(AtInternalRam self);
    uint32 (*ErrorHistoryClear)(AtInternalRam self);
    const char* (*Description)(AtInternalRam self);
    eBool (*IsReserved)(AtInternalRam self);
    AtErrorGenerator (*ErrorGeneratorGet)(AtInternalRam self);

    /* Performance counters */
    eBool (*CounterTypeIsSupported)(AtInternalRam self, uint16 counterType);
    uint32 (*CounterGet)(AtInternalRam self, uint16 counterType);
    uint32 (*CounterClear)(AtInternalRam self, uint16 counterType);
    void (*StatusClear)(AtInternalRam self);
    }tAtInternalRamMethods;

typedef struct tAtInternalRam
    {
    tAtObject super;
    const tAtInternalRamMethods* methods;

    /* Private data */
    AtModule phyModule; /* Also to return RAM module. */
    uint32 ramId;
    uint32 localId;
    }tAtInternalRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam AtInternalRamObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtModule AtInternalRamPhyModuleGet(AtInternalRam self);
uint32 AtInternalRamLocalIdGet(AtInternalRam self);
const char* AtInternalRamName(AtInternalRam self);
eBool AtInternalRamCrcMonitorCanEnable(AtInternalRam self, eBool enable);
eBool AtInternalRamEccMonitorCanEnable(AtInternalRam self, eBool enable);

void AtInternalRamStatusClear(AtInternalRam self);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTERNALRAMINTERNAL_H_ */


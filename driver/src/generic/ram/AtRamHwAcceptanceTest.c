/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtRamHwAcceptanceTest.c
 *
 * Created Date: May 25, 2017
 *
 * Description : Acceptance test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDeviceInternal.h"
#include "AtRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TestFill(AtRam self)
    {
    uint32 startAddress = 0;
    uint32 stopAddress = AtRamMaxAddressGet(self);
    eBool addressIncreased;
    uint32 patern     = 0xAAAAAAAA;
    uint32 antiPatern = 0x55555555;
    uint32 firstErrorAddress, memTestErrorCount;

    AtAssert(stopAddress > startAddress);

    /* Increase address */
    addressIncreased = cAtTrue;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, patern, startAddress, stopAddress) == cAtOk);
    memTestErrorCount = cInvalidUint32;
    AtAssert(mMethodsGet(self)->MemoryReadCheck(self, addressIncreased, patern,
                                                startAddress, stopAddress,
                                                &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 0);

    /* Decrease address */
    addressIncreased = cAtFalse;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, antiPatern, startAddress, stopAddress) == cAtOk);
    memTestErrorCount = cInvalidUint32;
    AtAssert(mMethodsGet(self)->MemoryReadCheck(self, addressIncreased, antiPatern,
                                                startAddress, stopAddress,
                                                &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 0);
    }

static void TestReadCheck(AtRam self)
    {
    uint32 startAddress = 0;
    uint32 stopAddress = AtRamMaxAddressGet(self);
    uint32 injectAddress = stopAddress / 2;
    eBool addressIncreased;
    uint32 patern     = 0xAAAAAAAA;
    uint32 antiPatern = 0x55555555;
    uint32 firstErrorAddress, memTestErrorCount;

    AtAssert(stopAddress > startAddress);

    /* Increase address */
    addressIncreased = cAtTrue;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, patern, startAddress, stopAddress) == cAtOk);

    /* Make one cell be wrong */
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, antiPatern, injectAddress, injectAddress) == cAtOk);

    /* Memory comparing must be fail */
    memTestErrorCount = cInvalidUint32;
    AtAssert(mMethodsGet(self)->MemoryReadCheck(self, addressIncreased, patern,
                                                startAddress, stopAddress,
                                                &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 1);
    AtAssert(firstErrorAddress == injectAddress);

    /* Decrease address */
    addressIncreased = cAtFalse;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, antiPatern, startAddress, stopAddress) == cAtOk);

    /* Make one cell be wrong */
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, patern, injectAddress, injectAddress) == cAtOk);

    /* Memory comparing must be fail */
    memTestErrorCount = cInvalidUint32;
    AtAssert(mMethodsGet(self)->MemoryReadCheck(self, addressIncreased, antiPatern,
                                                startAddress, stopAddress,
                                                &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 1);
    AtAssert(firstErrorAddress == injectAddress);
    }

static void TestReadWriteCheckMustBasicallyWork(AtRam self)
    {
    uint32 startAddress = 0;
    uint32 stopAddress = AtRamMaxAddressGet(self);
    eBool addressIncreased;
    uint32 patern     = 0xAAAAAAAA;
    uint32 antiPatern = 0x55555555;
    uint32 firstErrorAddress, memTestErrorCount;

    AtAssert(stopAddress > startAddress);

    /* Must basically work */
    addressIncreased = cAtTrue;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, patern, startAddress, stopAddress) == cAtOk);
    AtAssert(mMethodsGet(self)->MemoryReadWriteCheck(self, addressIncreased,
                                                     patern, antiPatern,
                                                     startAddress, stopAddress,
                                                     &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 0);
    addressIncreased = cAtFalse;
    AtAssert(mMethodsGet(self)->MemoryReadWriteCheck(self, addressIncreased,
                                                     antiPatern, patern,
                                                     startAddress, stopAddress,
                                                     &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 0);
    }

static void TestReadWriteCheckWithErrorInject(AtRam self)
    {
    uint32 startAddress = 0;
    uint32 stopAddress = AtRamMaxAddressGet(self);
    uint32 injectAddress = stopAddress / 2;
    eBool addressIncreased;
    uint32 patern     = 0xAAAAAAAA;
    uint32 antiPatern = 0x55555555;
    uint32 firstErrorAddress, memTestErrorCount;

    AtAssert(stopAddress > startAddress);

    /* Increase address */
    addressIncreased = cAtTrue;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, patern, startAddress, stopAddress) == cAtOk);

    /* Make one cell be wrong */
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, antiPatern, injectAddress, injectAddress) == cAtOk);

    /* Memory comparing must be fail */
    memTestErrorCount = cInvalidUint32;
    AtAssert(mMethodsGet(self)->MemoryReadWriteCheck(self, addressIncreased,
                                                     patern, antiPatern,
                                                     startAddress, stopAddress,
                                                     &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 1);
    AtAssert(firstErrorAddress == injectAddress);

    /* Decrease address */
    addressIncreased = cAtFalse;
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, antiPatern, startAddress, stopAddress) == cAtOk);

    /* Make one cell be wrong */
    AtAssert(mMethodsGet(self)->MemoryFill(self, addressIncreased, patern, injectAddress, injectAddress) == cAtOk);

    /* Memory comparing must be fail */
    memTestErrorCount = cInvalidUint32;
    AtAssert(mMethodsGet(self)->MemoryReadWriteCheck(self, addressIncreased,
                                                     antiPatern, patern,
                                                     startAddress, stopAddress,
                                                     &firstErrorAddress, &memTestErrorCount) == cAtOk);
    AtAssert(memTestErrorCount == 1);
    AtAssert(firstErrorAddress == injectAddress);
    }

eAtRet AtRamHwAssistAcceptanceTestImplement(AtRam self)
    {
    if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)AtRamModuleGet(self))))
        return cAtOk;

    AtAssert(AtRamAccessEnable(self, cAtTrue) == cAtOk);

    TestFill(self);
    TestReadCheck(self);
    TestReadWriteCheckMustBasicallyWork(self);
    TestReadWriteCheckWithErrorInject(self);
    return cAtOk;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsEngineInternal.h
 * 
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSENGINEINTERNAL_H_
#define _ATPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTimer.h"

#include "../common/AtObjectInternal.h"
#include "AtPrbsEngine.h"
#include "AtPrbsCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cAf6MaxDelay 0
#define cAf6MinDelay 1
#define cAf6AverageDelay 2
/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsEngineMethods
    {
    eAtModulePrbsRet (*Init)(AtPrbsEngine self);
    AtChannel (*ChannelGet)(AtPrbsEngine self);
    const char *(*TypeString)(AtPrbsEngine self);
    const char *(*ChannelDescription)(AtPrbsEngine self);

    /* Error forcing */
    eAtModulePrbsRet (*ErrorForce)(AtPrbsEngine self, eBool force);
    eBool (*ErrorIsForced)(AtPrbsEngine self);
    eBool (*ErrorForcingIsSupported)(AtPrbsEngine self);
    eAtModulePrbsRet (*TxErrorInject)(AtPrbsEngine self, uint32 numErrorForced);
    eAtModulePrbsRet (*TxErrorRateSet)(AtPrbsEngine self, eAtBerRate errorRate);
    eBool (*ErrorForcingRateIsSupported)(AtPrbsEngine self, eAtBerRate errorRate);
    eAtBerRate (*TxErrorRateGet)(AtPrbsEngine self);
    eAtBerRate (*RxErrorRateGet)(AtPrbsEngine self);
    eBool (*TxErrorInjectionIsSupported)(AtPrbsEngine self);

    /* Alarms */
    uint32 (*AlarmGet)(AtPrbsEngine self);
    uint32 (*AlarmHistoryGet)(AtPrbsEngine self);
    uint32 (*AlarmHistoryClear)(AtPrbsEngine self);
    uint32 (*DisruptionTimeGet)(AtPrbsEngine self);

    /* Invert */
    eAtModulePrbsRet (*Invert)(AtPrbsEngine self, eBool invert);
    eBool (*IsInverted)(AtPrbsEngine self);
    eAtModulePrbsRet (*TxInvert)(AtPrbsEngine self, eBool invert);
    eBool (*TxIsInverted)(AtPrbsEngine self);
    eAtModulePrbsRet (*RxInvert)(AtPrbsEngine self, eBool invert);
    eBool (*RxIsInverted)(AtPrbsEngine self);
    eBool (*InversionIsSupported)(AtPrbsEngine self);

    /* Enabled */
    eAtModulePrbsRet (*Enable)(AtPrbsEngine self, eBool enable);
    eBool (*IsEnabled)(AtPrbsEngine self);
    eAtModulePrbsRet (*TxEnable)(AtPrbsEngine self, eBool enable);
    eBool (*TxIsEnabled)(AtPrbsEngine self);
    eAtModulePrbsRet (*RxEnable)(AtPrbsEngine self, eBool enable);
    eBool (*RxIsEnabled)(AtPrbsEngine self);

    /* PRBS mode */
    eAtModulePrbsRet (*ModeSet)(AtPrbsEngine self, eAtPrbsMode prbsMode);
    eAtPrbsMode (*ModeGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*TxModeSet)(AtPrbsEngine self, eAtPrbsMode prbsMode);
    eAtPrbsMode (*TxModeGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*RxModeSet)(AtPrbsEngine self, eAtPrbsMode prbsMode);
    eAtPrbsMode (*RxModeGet)(AtPrbsEngine self);
    eBool (*ModeIsSupported)(AtPrbsEngine self, eAtPrbsMode prbsMode);

    /* PRBS mode */
    eAtRet (*DataModeSet)(AtPrbsEngine self, eAtPrbsDataMode prbsMode);
    eAtPrbsDataMode (*DataModeGet)(AtPrbsEngine self);
    eAtRet (*TxDataModeSet)(AtPrbsEngine self, eAtPrbsDataMode prbsMode);
    eAtPrbsDataMode (*TxDataModeGet)(AtPrbsEngine self);
    eAtRet (*RxDataModeSet)(AtPrbsEngine self, eAtPrbsDataMode prbsMode);
    eAtPrbsDataMode (*RxDataModeGet)(AtPrbsEngine self);
    eBool (*DataModeIsSupported)(AtPrbsEngine self);

    /* Fixed pattern configuration */
    eAtModulePrbsRet (*FixedPatternSet)(AtPrbsEngine self, uint32 fixedPattern);
    uint32 (*FixedPatternGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*TxFixedPatternSet)(AtPrbsEngine self, uint32 fixedPattern);
    uint32 (*TxFixedPatternGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*RxFixedPatternSet)(AtPrbsEngine self, uint32 fixedPattern);
    uint32 (*RxFixedPatternGet)(AtPrbsEngine self);
    uint32 (*RxCurrentFixedPatternGet)(AtPrbsEngine self);
    eBool (*CanUseAnyFixedPattern)(AtPrbsEngine self);

    /* Counters */
    uint32 (*CounterGet)(AtPrbsEngine self, uint16 counterType);
    uint32 (*CounterClear)(AtPrbsEngine self, uint16 counterType);
    uint64 (*Counter64BitGet)(AtPrbsEngine self, uint16 counterType);
    uint64 (*Counter64BitClear)(AtPrbsEngine self, uint16 counterType);
    eBool (*CounterIsSupported)(AtPrbsEngine self, uint16 counterType);
    eAtModulePrbsRet (*AllCountersLatchAndClear)(AtPrbsEngine self, eBool clear);

    /* Separate generating and monitoring sides */
    eAtModulePrbsRet (*GeneratingSideSet)(AtPrbsEngine self, eAtPrbsSide side);
    eAtPrbsSide (*GeneratingSideGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*MonitoringSideSet)(AtPrbsEngine self, eAtPrbsSide side);
    eAtPrbsSide (*MonitoringSideGet)(AtPrbsEngine self);

    /* Side to generate and monitor */
    eAtModulePrbsRet (*SideSet)(AtPrbsEngine self, eAtPrbsSide option);
    eAtPrbsSide (*SideGet)(AtPrbsEngine self);
    eBool (*SideIsSupported)(AtPrbsEngine self, eAtPrbsSide option);
    
    /* Debug */
    void (*Debug)(AtPrbsEngine self);
    AtQuerier (*QuerierGet)(AtPrbsEngine self);
    eBool (*IsRawPrbs)(AtPrbsEngine self);

    /* Bit order */
    eAtModulePrbsRet (*BitOrderSet)(AtPrbsEngine self, eAtPrbsBitOrder order);
    eAtPrbsBitOrder (*BitOrderGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*TxBitOrderSet)(AtPrbsEngine self, eAtPrbsBitOrder order);
    eAtPrbsBitOrder (*TxBitOrderGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*RxBitOrderSet)(AtPrbsEngine self, eAtPrbsBitOrder order);
    eAtPrbsBitOrder (*RxBitOrderGet)(AtPrbsEngine self);

    /* Internal */
    eBool (*SeparateTwoDirections)(AtPrbsEngine self);
    AtDevice (*DeviceGet)(AtPrbsEngine self);
    eAtRet (*HwCleanup)(AtPrbsEngine self);
    void (*StatusClear)(AtPrbsEngine self);
    void (*AllCountersClear)(AtPrbsEngine self);

    /* PRBS gating */
    AtTimer (*TimerObjectCreate)(AtPrbsEngine self);

    /* Counters */
    AtPrbsCounters (*CountersObjectCreate)(AtPrbsEngine self);

    /* Access hardware */
    uint32 (*Read)(AtPrbsEngine self, uint32 address, eAtModule module);
    void (*Write)(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module);
    float (*DelayGet)(AtPrbsEngine self, uint8 mode, eBool r2c);
    eAtRet (*DelayEnable)(AtPrbsEngine self, eBool enable);

    eBool  (*NeedSetDefaultMode)(AtPrbsEngine self);
    eBool  (*NeedSetDefaultPrbsLoss)(AtPrbsEngine self);
    uint16 (*DisruptionLongRead)(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen);
    uint16 (*DisruptionLongWrite)(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen);
    eAtRet (*DisruptionMaxExpectedTimeSet)(AtPrbsEngine self, uint32 time);
    uint32 (*DisruptionMaxExpectedTimeGet)(AtPrbsEngine self);
    uint32 (*DisruptionCurTimeGet)(AtPrbsEngine self, eBool r2c);
    eAtRet (*DisruptionLossSigThresSet)(AtPrbsEngine self, uint32 thres);
    eAtRet (*DisruptionPrbsSyncDectectThresSet)(AtPrbsEngine self, uint32 thres);
    eAtRet (*DisruptionPrbsLossDectectThresSet)(AtPrbsEngine self, uint32 thres);

    /* Special prbs function for eth prbs */
    eBool (*BandwidthControlIsSupported)(AtPrbsEngine self);
    eAtModulePrbsRet (*BurstSizeSet)(AtPrbsEngine self, uint32 burstSizeInBytes);
    uint32 (*BurstSizeGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*PercentageBandwidthSet)(AtPrbsEngine self, uint32 percentageOfBandwith);
    uint32 (*PercentageBandwidthGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*PayloadLenghtModeSet)(AtPrbsEngine self, eAtPrbsPayloadLengthMode payloadLenMode);
    eAtPrbsPayloadLengthMode (*PayloadLengthModeGet)(AtPrbsEngine self);
    eAtModulePrbsRet (*MinLengthSet)(AtPrbsEngine self, uint32 minLength);
    eAtModulePrbsRet (*MaxLengthSet)(AtPrbsEngine self, uint32 maxLength);
    uint32 (*MinLengthGet)(AtPrbsEngine self);
    uint32 (*MaxLengthGet)(AtPrbsEngine self);
    }tAtPrbsEngineMethods;

typedef struct tAtPrbsEngine
    {
    tAtObject super;
    const tAtPrbsEngineMethods *methods;

    /* Private data */
    AtChannel channel;
    uint32 engineId;
    eAtPrbsSide genSide;
    eAtPrbsSide monSide;
    AtTimer timer;
    uint32 regValue_stickyand_status;
    AtPrbsCounters counters;
    eAtPrbsMode txPrbsMode;
    uint32 txFixedPattern;
    eBool isTxInverted;
    }tAtPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine AtPrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId);

eBool  AtPrbsEngineAllZeroFixedPatternIsUsed(AtPrbsEngine self);
uint32 AtPrbsEngineRead(AtPrbsEngine self, uint32 address, eAtModule module);
void   AtPrbsEngineWrite(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module);
uint16 AtPrbsEngineDisruptionLongRead(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen);
uint16 AtPrbsEngineDisruptionLongWrite(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen);

eAtRet AtPrbsEngineEngineIdSet(AtPrbsEngine self, uint32 engineId);

eBool AtPrbsEngineTxErrorInjectionIsSupported(AtPrbsEngine self);
eBool AtPrbsEngineErrorForcingIsSupported(AtPrbsEngine self);
eBool AtPrbsEngineSideIsSupported(AtPrbsEngine self, eAtPrbsSide option);
eBool AtPrbsEngineInversionIsSupported(AtPrbsEngine self);
eBool AtPrbsEngineErrorForcingRateIsSupported(AtPrbsEngine self, eAtBerRate errorRate);
eAtRet AtPrbsEngineHwCleanup(AtPrbsEngine self);
eBool AtPrbsEngineCanUseAnyFixedPattern(AtPrbsEngine self);

AtDevice AtPrbsEngineDeviceGet(AtPrbsEngine self);
eBool AtPrbsEngineIsSimulated(AtPrbsEngine self);
void AtPrbsEngineStatusClear(AtPrbsEngine self);

/* Counters */
AtPrbsCounters AtPrbsEnginePrbsCountersGet(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSENGINEINTERNAL_H_ */

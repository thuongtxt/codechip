/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtPrbsEngine.c
 *
 * Created Date: Sep 20, 2012
 *
 * Description : PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../common/AtChannelInternal.h"
#include "AtPrbsEngineInternal.h"
#include "AtModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPrbsEngine)self)
#define mInAccessible(self) AtDeviceInAccessible(AtPrbsEngineDeviceGet(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPrbsEngineMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet DataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return cAtOk;
    }

static eAtPrbsDataMode DataModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsDataMode64kb;
    }

static eAtRet TxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return cAtOk;
    }

static eAtPrbsDataMode TxDataModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsDataMode64kb;
    }

static eAtRet RxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return cAtOk;
    }

static eAtPrbsDataMode RxDataModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsDataMode64kb;
    }

static eBool DataModeIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    AtUnused(self);
    AtUnused(thres);
    return cAtOk;
    }
static eAtRet DisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    AtUnused(self);
    AtUnused(thres);
    return cAtOk;
    }

static eAtRet DisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    AtUnused(self);
    AtUnused(thres);
    return cAtOk;
    }

static eAtRet DisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    AtUnused(self);
    AtUnused(time);
    return cAtOk;
    }

static uint32 DisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);
    return 0;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtOk;
    }

static float DelayGet(AtPrbsEngine self, uint8 mode, eBool r2c)
	{
	AtUnused(self);
	AtUnused(mode);
	AtUnused(r2c);
	return 0;
	}

static eAtRet DelayEnable(AtPrbsEngine self, eBool enable)
	{
	AtUnused(self);
	AtUnused(enable);
	return cAtOk;
	}

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorNotImplemented;

    /* Try reusing TX/RX enabled attributes */
    ret |= AtPrbsEngineTxEnable(self, enable);
    ret |= AtPrbsEngineRxEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtFalse;

    /* Try reusing TX/RX enabled attributes */
    if (AtPrbsEngineTxIsEnabled(self) &&
        AtPrbsEngineRxIsEnabled(self))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
	AtUnused(force);
	AtUnused(self);
    /* Concrete class will do */
    return cAtErrorNotImplemented;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
	AtUnused(self);
    /* Concrete class will do */
    return cAtFalse;
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    AtUnused(self);
    AtUnused(errorRate);
    return cAtErrorNotImplemented;
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtBerRateUnknown;
    }

static eAtBerRate RxErrorRateGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtBerRateUnknown;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
	AtUnused(self);
    /* Concrete class will do */
    return 0;
    }

static AtChannel ChannelGet(AtPrbsEngine self)
    {
    return self->channel;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtRet ret = cAtOk;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorNotImplemented;

    /* Try reusing TX and RX attributes */
    ret |= AtPrbsEngineRxModeSet(self, prbsMode);
    ret |= AtPrbsEngineTxModeSet(self, prbsMode);

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    eAtPrbsMode mode;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtPrbsModeInvalid;

    /* Just can determine if the PRBS mode is same for both directions */
    mode = AtPrbsEngineRxModeGet(self);
    return (mode == AtPrbsEngineTxModeGet(self)) ? mode : cAtPrbsModeInvalid;
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    eAtRet ret = cAtOk;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorNotImplemented;

    /* Try reusing TX and RX attributes */
    ret |= AtPrbsEngineRxInvert(self, invert);
    ret |= AtPrbsEngineTxInvert(self, invert);

    return ret;
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    eBool invert;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtFalse;

    /* Just can determine if the PRBS mode is same for both directions */
    invert = AtPrbsEngineRxIsInverted(self);
    return (eBool)((invert == AtPrbsEngineTxIsInverted(self)) ? invert : cAtFalse);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    /* Concrete class will do */
    AtUnused(self);
    AtUnused(invert);
    return cAtErrorNotImplemented;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    /* Concrete class will do */
    AtUnused(self);
    AtUnused(invert);
    return cAtErrorNotImplemented;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPrbsEngine);
    }

static void Debug(AtPrbsEngine self)
    {
	AtUnused(self);
    }

static eAtModulePrbsRet DirectionEnable(AtPrbsEngine self, eBool enable)
    {
    /* Let concrete determine */
    if (mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorNotImplemented;

    /* Try reusing enabled attribute */
    return (enable == AtPrbsEngineIsEnabled(self)) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool DirectionIsEnabled(AtPrbsEngine self)
    {
    /* Let concrete determine */
    if (mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtFalse;

    /* Try reusing enabled attribute */
    return AtPrbsEngineIsEnabled(self);
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    return DirectionEnable(self, enable);
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    return DirectionIsEnabled(self);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    return DirectionEnable(self, enable);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return DirectionIsEnabled(self);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    return 0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    return 0;
    }

static uint64 Counter64BitGet(AtPrbsEngine self, uint16 counterType)
    {
    return (uint64)AtPrbsEngineCounterGet(self, counterType);
    }

static uint64 Counter64BitClear(AtPrbsEngine self, uint16 counterType)
    {
    return (uint64)AtPrbsEngineCounterClear(self, counterType);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
	AtUnused(self);
    if ((counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame) ||
        (counterType == cAtPrbsEngineCounterRxBitError))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    self->genSide = side;
    self->monSide = side;
    return cAtOk;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    return (self->genSide == self->monSide) ? self->genSide : cAtPrbsSideUnknown;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModulePrbsRet DirectionModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    /* Let concrete determine */
    if (mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorNotImplemented;

    return (prbsMode == AtPrbsEngineModeGet(self)) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsMode DirectionModeGet(AtPrbsEngine self)
    {
    /* Let concrete determine */
    if (mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtPrbsModeInvalid;

    return AtPrbsEngineModeGet(self);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return DirectionModeSet(self, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return DirectionModeGet(self);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return DirectionModeSet(self, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return DirectionModeGet(self);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtUnused(self);
    AtUnused(fixedPattern);
    return cAtErrorNotImplemented;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtUnused(self);
    AtUnused(fixedPattern);
    return cAtErrorNotImplemented;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static const char *TypeString(AtPrbsEngine self)
    {
    static char typeString[16];
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);

    AtSnprintf(typeString,
               sizeof(typeString) - 1,
               "%s.%s",
               AtChannelTypeString(channel),
               AtChannelIdString(channel));

    return typeString;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    AtDevice dev = AtChannelDeviceGet(channel);

    AtSnprintf(description,
               sizeof(description) - 1,
               "%s%s_prbs_%u",
               AtDeviceIdToString(dev),
               mMethodsGet(engine)->TypeString(engine),
               AtPrbsEngineIdGet(engine) + 1);

    return description;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtPrbsEngine object = (AtPrbsEngine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeString(encoder, AtChannelIdDescriptionBuild(channel), "channel");
    mEncodeUInt(engineId);
    mEncodeUInt(genSide);
    mEncodeUInt(monSide);
    mEncodeObject(timer);
    mEncodeUInt(regValue_stickyand_status);
    mEncodeObject(counters);
    mEncodeUInt(txPrbsMode);
    mEncodeUInt(txFixedPattern);
    mEncodeUInt(isTxInverted);
    }

static AtTimer GatingTimer(AtPrbsEngine self)
    {
    if (self->timer == NULL)
        self->timer = mMethodsGet(self)->TimerObjectCreate(self);
    return self->timer;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPrbsCounters CountersGet(AtPrbsEngine self)
    {
    if (self->counters)
        return self->counters;

    self->counters = mMethodsGet(self)->CountersObjectCreate(self);
    return self->counters;
    }

static AtPrbsCounters CountersObjectCreate(AtPrbsEngine self)
    {
    AtUnused(self);
    return NULL;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->timer);
    mThis(self)->timer = NULL;
    AtObjectDelete((AtObject)mThis(self)->counters);
    mThis(self)->counters = NULL;
    m_AtObjectMethods->Delete(self);
    }

static AtDevice DeviceGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    return AtChannelDeviceGet(channel);
    }

static eBool ShouldInvalidStatusWhenRxIsDisabled(AtPrbsEngine self)
    {
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(AtPrbsEngineDeviceGet(self), cAtModulePrbs);
    return AtModulePrbsShouldInvalidateStatusWhenRxIsDisabled(prbsModule);
    }

static eBool ShouldInvalidateStatus(AtPrbsEngine self)
    {
    if (ShouldInvalidStatusWhenRxIsDisabled(self) && !AtPrbsEngineRxIsEnabled(self))
        return cAtTrue;
    return cAtFalse;
    }

static AtQuerier QuerierGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return AtPrbsEngineSharedQuerier();
    }

static uint32 RxCurrentFixedPatternGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet EngineIdSet(AtPrbsEngine self, uint32 engineId)
    {
    self->engineId = engineId;
    return cAtOk;
    }

static eBool CanUseAnyFixedPattern(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsRawPrbs(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
	AtUnused(self);
    /* Most of products cannot control two direction separately. So let each
     * concrete one determine this */
    return cAtFalse;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    self->genSide = side;
    return cAtOk;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    return self->genSide;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    self->monSide = side;
    return cAtOk;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    return self->monSide;
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    AtUnused(self);
    AtUnused(numErrors);
    return cAtErrorNotImplemented;
    }

static eAtModulePrbsRet BitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    eAtRet ret = cAtOk;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorNotImplemented;

    /* Try reusing TX and RX attributes */
    ret |= AtPrbsEngineRxBitOrderSet(self, order);
    ret |= AtPrbsEngineTxBitOrderSet(self, order);

    return ret;
    }

static eAtPrbsBitOrder BitOrderGet(AtPrbsEngine self)
    {
    eAtPrbsBitOrder order;

    /* Let concrete determine */
    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtPrbsBitOrderUnknown;

    /* Just can determine if the PRBS mode is same for both directions */
    order = AtPrbsEngineRxBitOrderGet(self);
    return (order == AtPrbsEngineTxBitOrderGet(self)) ? order : cAtPrbsBitOrderUnknown;
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtUnused(self);
    AtUnused(order);
    return cAtErrorNotImplemented;
    }

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
    {
    if (!AtPrbsModeIsFixedPattern(AtPrbsEngineTxModeGet(self)))
        return cAtPrbsBitOrderNotApplicable;

    return cAtPrbsBitOrderNotApplicable;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtUnused(self);
    AtUnused(order);
    return cAtErrorNotImplemented;
    }

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
    {
    if (!AtPrbsModeIsFixedPattern(AtPrbsEngineRxModeGet(self)))
        return cAtPrbsBitOrderNotApplicable;
    return cAtPrbsBitOrderNotApplicable;
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(self)->SeparateTwoDirections(self))
        return cAtErrorModeNotSupport;

    ret |= AtPrbsEngineTxFixedPatternSet(self, fixedPattern);
    ret |= AtPrbsEngineRxFixedPatternSet(self, fixedPattern);

    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    if (mMethodsGet(self)->SeparateTwoDirections(self))
        return AtPrbsEngineTxFixedPatternGet(self);
    return 0;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    if ((prbsMode >= cAtPrbsModeStart) && (prbsMode < cAtPrbsModeNum))
        return cAtTrue;
    return cAtFalse;
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    AtUnused(side);
    return cAtFalse;
    }

static eBool TxErrorInjectionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ErrorForcingIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool InversionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ErrorForcingRateIsSupported(AtPrbsEngine self, eAtBerRate errorRate)
    {
    AtUnused(self);
    AtUnused(errorRate);
    return cAtFalse;
    }

static uint32 Read(AtPrbsEngine self, uint32 address, eAtModule module)
    {
    return mChannelHwRead(AtPrbsEngineChannelGet(self), address, module);
    }

static void Write(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module)
    {
    mChannelHwWrite(AtPrbsEngineChannelGet(self), address, value, module);
    }

static uint16 DisruptionLongRead(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    AtUnused(bufferLen);
    regVal[0] = mChannelHwRead(AtPrbsEngineChannelGet(self), address, cAtModulePrbs);
    return 1;
    }

static uint16 DisruptionLongWrite(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    AtUnused(bufferLen);
    mChannelHwWrite(AtPrbsEngineChannelGet(self), address, regVal[0], cAtModulePrbs);
    return 1;
    }

static const char *ChannelDescription(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    static char idString[64];

    if (channel == NULL)
        {
        AtSprintf(idString, "none");
        return idString;
        }

    AtSnprintf(idString, sizeof(idString), "%s.%s", AtChannelTypeString(channel), AtChannelIdString(channel));

    return idString;
    }

static eAtRet HwCleanup(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return cAtOk;
    }

/* Special prbs function for eth prbs */
static eBool BandwidthControlIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet BurstSizeSet(AtPrbsEngine self, uint32 burstSizeInBytes)
    {
    AtUnused(self);
    AtUnused(burstSizeInBytes);
    return cAtErrorModeNotSupport;
    }

static uint32 BurstSizeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePrbsRet PercentageBandwidthSet(AtPrbsEngine self, uint32 percentageOfBandwith)
    {
    AtUnused(self);
    AtUnused(percentageOfBandwith);
    return cAtErrorModeNotSupport;
    }

static uint32 PercentageBandwidthGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 100;
    }

static eAtModulePrbsRet PayloadLenghtModeSet(AtPrbsEngine self, eAtPrbsPayloadLengthMode payloadLenMode)
    {
    AtUnused(self);
    AtUnused(payloadLenMode);
    return cAtErrorModeNotSupport;
    }

static eAtPrbsPayloadLengthMode PayloadLengthModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsLengthModeFixed;
    }

static eAtModulePrbsRet MinLengthSet(AtPrbsEngine self, uint32 minLength)
    {
    AtUnused(self);
    AtUnused(minLength);
    return cAtErrorModeNotSupport;
    }

static eAtModulePrbsRet MaxLengthSet(AtPrbsEngine self, uint32 maxLength)
    {
    AtUnused(self);
    AtUnused(maxLength);
    return cAtErrorModeNotSupport;
    }

static uint32 MinLengthGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxLengthGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static void AllCountersClear(AtPrbsEngine self)
    {
    AtUnused(self);
    /* Let subclass do */
    }

static void StatusClear(AtPrbsEngine self)
    {
    mMethodsGet(self)->AllCountersClear(self);

    AtPrbsEngineAlarmHistoryClear(self);
    }

static void CacheTxInvert(AtPrbsEngine self, eBool invert)
    {
    self->isTxInverted = invert;
    }

static eBool CacheTxIsInverted(AtPrbsEngine self)
    {
    return self->isTxInverted;
    }

static void CacheTxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    self->txPrbsMode = prbsMode;
    }

static eAtPrbsMode CacheTxModeGet(AtPrbsEngine self)
    {
    return self->txPrbsMode;
    }

static void CacheTxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    self->txFixedPattern = fixedPattern;
    }

static uint32 CacheTxFixedPatternGet(AtPrbsEngine self)
    {
    return self->txFixedPattern;
    }

static void MethodsInit(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, ErrorForce);
        mMethodOverride(m_methods, ErrorIsForced);
        mMethodOverride(m_methods, TxErrorRateSet);
        mMethodOverride(m_methods, TxErrorRateGet);
        mMethodOverride(m_methods, RxErrorRateGet);

        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, ChannelGet);
        mMethodOverride(m_methods, ChannelDescription);
        mMethodOverride(m_methods, ModeSet);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, Invert);
        mMethodOverride(m_methods, IsInverted);
        mMethodOverride(m_methods, TxInvert);
        mMethodOverride(m_methods, TxIsInverted);
        mMethodOverride(m_methods, RxInvert);
        mMethodOverride(m_methods, RxIsInverted);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, TxEnable);
        mMethodOverride(m_methods, TxIsEnabled);
        mMethodOverride(m_methods, RxEnable);
        mMethodOverride(m_methods, RxIsEnabled);
        mMethodOverride(m_methods, TxModeSet);
        mMethodOverride(m_methods, TxModeGet);
        mMethodOverride(m_methods, RxModeSet);
        mMethodOverride(m_methods, RxModeGet);
        mMethodOverride(m_methods, ModeIsSupported);

        mMethodOverride(m_methods, TxFixedPatternSet);
        mMethodOverride(m_methods, TxFixedPatternGet);
        mMethodOverride(m_methods, RxFixedPatternSet);
        mMethodOverride(m_methods, RxFixedPatternGet);
        mMethodOverride(m_methods, RxCurrentFixedPatternGet);
        mMethodOverride(m_methods, CanUseAnyFixedPattern);

        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, Counter64BitGet);
        mMethodOverride(m_methods, Counter64BitClear);
        mMethodOverride(m_methods, CounterIsSupported);
        mMethodOverride(m_methods, SeparateTwoDirections);
        mMethodOverride(m_methods, AllCountersLatchAndClear);
        mMethodOverride(m_methods, StatusClear);
        mMethodOverride(m_methods, CountersObjectCreate);

        mMethodOverride(m_methods, GeneratingSideSet);
        mMethodOverride(m_methods, GeneratingSideGet);
        mMethodOverride(m_methods, MonitoringSideSet);
        mMethodOverride(m_methods, MonitoringSideGet);
        mMethodOverride(m_methods, SideSet);
        mMethodOverride(m_methods, SideGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, TxErrorInject);
        mMethodOverride(m_methods, BitOrderSet);
        mMethodOverride(m_methods, BitOrderGet);
        mMethodOverride(m_methods, TxBitOrderSet);
        mMethodOverride(m_methods, TxBitOrderGet);
        mMethodOverride(m_methods, RxBitOrderSet);
        mMethodOverride(m_methods, RxBitOrderGet);
        mMethodOverride(m_methods, FixedPatternSet);
        mMethodOverride(m_methods, FixedPatternGet);
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, SideIsSupported);
        mMethodOverride(m_methods, TxErrorInjectionIsSupported);
        mMethodOverride(m_methods, ErrorForcingIsSupported);
        mMethodOverride(m_methods, InversionIsSupported);
        mMethodOverride(m_methods, ErrorForcingRateIsSupported);
        mMethodOverride(m_methods, TimerObjectCreate);
        mMethodOverride(m_methods, HwCleanup);

        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, DeviceGet);
        mMethodOverride(m_methods, QuerierGet);
        mMethodOverride(m_methods, DelayGet);
        mMethodOverride(m_methods, DelayEnable);
        mMethodOverride(m_methods, DisruptionLongRead);
        mMethodOverride(m_methods, DisruptionLongWrite);
        mMethodOverride(m_methods, DisruptionMaxExpectedTimeSet);
        mMethodOverride(m_methods, DisruptionMaxExpectedTimeGet);
        mMethodOverride(m_methods, DisruptionCurTimeGet);
        mMethodOverride(m_methods, DisruptionLossSigThresSet);
        mMethodOverride(m_methods, DisruptionPrbsSyncDectectThresSet);
        mMethodOverride(m_methods, DisruptionPrbsLossDectectThresSet);

        mMethodOverride(m_methods, BandwidthControlIsSupported);
        mMethodOverride(m_methods, BurstSizeSet);
        mMethodOverride(m_methods, BurstSizeGet);
        mMethodOverride(m_methods, MinLengthSet);
        mMethodOverride(m_methods, MinLengthGet);
        mMethodOverride(m_methods, MaxLengthSet);
        mMethodOverride(m_methods, MaxLengthGet);
        mMethodOverride(m_methods, PayloadLenghtModeSet);
        mMethodOverride(m_methods, PayloadLengthModeGet);
        mMethodOverride(m_methods, PercentageBandwidthSet);
        mMethodOverride(m_methods, PercentageBandwidthGet);
        mMethodOverride(m_methods, IsRawPrbs);
        mMethodOverride(m_methods, StatusClear);
        mMethodOverride(m_methods, AllCountersClear);

        mMethodOverride(m_methods, DataModeSet);
        mMethodOverride(m_methods, DataModeGet);
        mMethodOverride(m_methods, TxDataModeSet);
        mMethodOverride(m_methods, TxDataModeGet);
        mMethodOverride(m_methods, RxDataModeSet);
        mMethodOverride(m_methods, RxDataModeGet);
        mMethodOverride(m_methods, DataModeIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

AtPrbsEngine AtPrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->channel = channel;
    self->engineId = engineId;

    return self;
    }

uint32 AtPrbsEngineRead(AtPrbsEngine self, uint32 address, eAtModule module)
    {
    if (self)
        return mMethodsGet(self)->Read(self, address, module);
    return cInvalidUint32;
    }

void AtPrbsEngineWrite(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module)
    {
    if (self)
        mMethodsGet(self)->Write(self, address, value, module);
    }

uint16 AtPrbsEngineDisruptionLongRead(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    if (self)
        return mMethodsGet(self)->DisruptionLongRead(self, address, regVal, cAtModulePrbs);
    return bufferLen;
    }

uint16 AtPrbsEngineDisruptionLongWrite(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    if (self)
        mMethodsGet(self)->DisruptionLongWrite(self, address, regVal, cAtModulePrbs);
    return bufferLen;
    }

eBool AtPrbsEngineAllZeroFixedPatternIsUsed(AtPrbsEngine self)
    {
    eAtPrbsMode mode = AtPrbsEngineRxModeGet(self);

    if ((mode == cAtPrbsModePrbsFixedPattern1Byte)  ||
        (mode == cAtPrbsModePrbsFixedPattern2Bytes) ||
        (mode == cAtPrbsModePrbsFixedPattern3Bytes) ||
        (mode == cAtPrbsModePrbsFixedPattern4Bytes))
        return (AtPrbsEngineRxFixedPatternGet(self) == 0x0) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

eBool AtPrbsEngineTxErrorInjectionIsSupported(AtPrbsEngine self)
    {
    mAttributeGet(TxErrorInjectionIsSupported, eBool, cAtFalse);
    }

eBool AtPrbsEngineErrorForcingIsSupported(AtPrbsEngine self)
    {
    mAttributeGet(ErrorForcingIsSupported, eBool, cAtFalse);
    }

eBool AtPrbsEngineSideIsSupported(AtPrbsEngine self, eAtPrbsSide option)
    {
    if (self)
        return mMethodsGet(self)->SideIsSupported(self, option);
    return cAtFalse;
    }

eBool AtPrbsEngineInversionIsSupported(AtPrbsEngine self)
    {
    mAttributeGet(InversionIsSupported, eBool, cAtFalse);
    }

eBool AtPrbsEngineErrorForcingRateIsSupported(AtPrbsEngine self, eAtBerRate errorRate)
    {
    if (self)
        return mMethodsGet(self)->ErrorForcingRateIsSupported(self, errorRate);
    return cAtFalse;
    }

AtDevice AtPrbsEngineDeviceGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->DeviceGet(self);
    return NULL;
    }

eBool AtPrbsEngineIsSimulated(AtPrbsEngine self)
    {
    return AtDeviceIsSimulated(AtPrbsEngineDeviceGet(self));
    }

AtQuerier AtPrbsEngineQuerierGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->QuerierGet(self);
    return NULL;
    }

eAtRet AtPrbsEngineEngineIdSet(AtPrbsEngine self, uint32 engineId)
    {
    if (self)
        return EngineIdSet(self, engineId);
    return cAtErrorObjectNotExist;
    }

eAtRet AtPrbsEngineHwCleanup(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->HwCleanup(self);
    return cAtErrorObjectNotExist;
    }

eBool AtPrbsEngineCanUseAnyFixedPattern(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->CanUseAnyFixedPattern(self);
    return cAtFalse;
    }

AtPrbsCounters AtPrbsEnginePrbsCountersGet(AtPrbsEngine self)
    {
    if (self)
        return CountersGet(self);
    return NULL;
    }

/* Special prbs function for eth prbs */
eBool AtPrbsEngineBandwidthControlIsSupported(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->BandwidthControlIsSupported(self);

    return cAtFalse;
    }

eAtModulePrbsRet AtPrbsEngineBurstSizeSet(AtPrbsEngine self, uint32 burstSizeInBytes)
    {
    if (self)
        return mMethodsGet(self)->BurstSizeSet(self, burstSizeInBytes);

    return cAtErrorNullPointer;
    }

uint32 AtPrbsEngineBurstSizeGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->BurstSizeGet(self);

    return 0;
    }

eAtModulePrbsRet AtPrbsEnginePercentageBandwidthSet(AtPrbsEngine self, uint32 percentageOfBandwith)
    {
    if (self)
        return mMethodsGet(self)->PercentageBandwidthSet(self, percentageOfBandwith);

    return cAtErrorNullPointer;
    }

uint32 AtPrbsEnginePercentageBandwidthGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->PercentageBandwidthGet(self);

    return 0;
    }

eAtModulePrbsRet AtPrbsEnginePayloadLengthModeSet(AtPrbsEngine self, eAtPrbsPayloadLengthMode payloadLenMode)
    {
    if (self)
        return mMethodsGet(self)->PayloadLenghtModeSet(self, payloadLenMode);

    return cAtErrorNullPointer;
    }

eAtPrbsPayloadLengthMode AtPrbsEnginePayloadLengthModeGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->PayloadLengthModeGet(self);

    return 0;
    }

eAtModulePrbsRet AtPrbsEngineMinLengthSet(AtPrbsEngine self, uint32 minLength)
    {
    if (self)
        return mMethodsGet(self)->MinLengthSet(self, minLength);

    return cAtErrorNullPointer;
    }

eAtModulePrbsRet AtPrbsEngineMaxLengthSet(AtPrbsEngine self, uint32 maxLength)
    {
    if (self)
        return mMethodsGet(self)->MaxLengthSet(self, maxLength);

    return cAtErrorNullPointer;
    }

uint32 AtPrbsEngineMinLengthGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->MinLengthGet(self);

    return 0;
    }

uint32 AtPrbsEngineMaxLengthGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->MaxLengthGet(self);
    return 0;
    }

void AtPrbsEngineStatusClear(AtPrbsEngine self)
    {
    if (self)
        mMethodsGet(self)->StatusClear(self);
    }

float AtPrbsEngineMaxDelayGet(AtPrbsEngine self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->DelayGet(self, cAf6MaxDelay, r2c);
    return 0;
    }

float AtPrbsEngineMinDelayGet(AtPrbsEngine self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->DelayGet(self, cAf6MinDelay, r2c);
    return 0;
    }

float AtPrbsEngineAverageDelayGet(AtPrbsEngine self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->DelayGet(self, cAf6AverageDelay, r2c);
    return 0;
    }

eAtRet AtPrbsEngineDelayEnable(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->DelayEnable(self, cAtTrue);
    return cAtErrorNullPointer;
    }

eAtRet AtPrbsEngineDelayDisable(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->DelayEnable(self, cAtFalse);
    return cAtErrorNullPointer;
    }

eAtRet AtPrbsEngineDisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    if (self)
        return mMethodsGet(self)->DisruptionMaxExpectedTimeSet(self, time);
    return cAtErrorNullPointer;
    }

uint32 AtPrbsEngineDisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->DisruptionMaxExpectedTimeGet(self);
    return 0;
    }

uint32 AtPrbsEngineDisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->DisruptionCurTimeGet(self, r2c);
    return 0;
    }

eAtRet AtPrbsEngineDisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    if (self)
        return mMethodsGet(self)->DisruptionLossSigThresSet(self, thres);
    return cAtErrorNullPointer;
    }

eAtRet AtPrbsEngineDisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    if (self)
        return mMethodsGet(self)->DisruptionPrbsSyncDectectThresSet(self, thres);
    return cAtErrorNullPointer;
    }

eAtRet AtPrbsEngineDisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    if (self)
        return mMethodsGet(self)->DisruptionPrbsLossDectectThresSet(self, thres);
    return cAtErrorNullPointer;
    }

eAtRet AtPrbsEngineDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->DataModeSet(self, prbsMode);
    return cAtErrorNullPointer;
    }

eAtPrbsDataMode AtPrbsEngineDataModeGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->DataModeGet(self);
    return cAtPrbsDataModeInvalid;
    }

eAtRet AtPrbsEngineTxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->TxDataModeSet(self, prbsMode);
    return cAtErrorNullPointer;
    }

eAtPrbsDataMode AtPrbsEngineTxDataModeGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->TxDataModeGet(self);
    return cAtPrbsDataModeInvalid;
    }

eAtRet AtPrbsEngineRxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->RxDataModeSet(self, prbsMode);
    return cAtErrorNullPointer;
    }

eAtPrbsDataMode AtPrbsEngineRxDataModeGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->RxDataModeGet(self);
    return cAtPrbsDataModeInvalid;
    }

eBool AtPrbsEngineDataModeIsSupported(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->DataModeIsSupported(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtPrbsEngine
 * @{
 */

/**
 * Set PRBS mode
 *
 * @param self This engine
 * @param prbsMode @ref eAtPrbsMode "PRBS modes"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    mNumericalAttributeSet(ModeSet, prbsMode);
    }

/**
 * Get PRBS mode
 *
 * @param self This engine
 *
 * @return @ref eAtPrbsMode "PRBS modes"
 */
eAtPrbsMode AtPrbsEngineModeGet(AtPrbsEngine self)
    {
    mAttributeGet(ModeGet, eAtPrbsMode, cAtPrbsModeInvalid);
    }

/**
 * Enable/disable PRBS inversion
 *
 * @param self This engine
 * @param invert cAtTrue to invert, cAtFalse not to invert
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineInvert(AtPrbsEngine self, eBool invert)
    {
    mNumericalAttributeSet(Invert, invert);
    }

/**
 * Get engine ID
 *
 * @param self This engine
 *
 * @return Engine ID
 */
uint32 AtPrbsEngineIdGet(AtPrbsEngine self)
    {
    return self ? self->engineId : 0xCAFECAFE;
    }

/**
 * Check if PRBS is in invert mode
 *
 * @param self This engine
 *
 * @retval cAtTrue Invert
 * @retval cAtFalse No invert
 */
eBool AtPrbsEngineIsInverted(AtPrbsEngine self)
    {
    mAttributeGet(IsInverted, eBool, cAtFalse);
    }

/**
 * Enable/disable PRBS inversion of Tx side
 *
 * @param self This engine
 * @param invert cAtTrue to invert, cAtFalse not to invert
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineTxInvert(AtPrbsEngine self, eBool invert)
    {
    mNumericalAttributeSetWithCache(TxInvert, invert);
    }

/**
 * Check if PRBS is in invert mode  of Tx side
 *
 * @param self This engine
 *
 * @retval cAtTrue Invert
 * @retval cAtFalse No invert
 */
eBool AtPrbsEngineTxIsInverted(AtPrbsEngine self)
    {
    mAttributeGetWithCache(TxIsInverted, eBool, cAtFalse);
    }

/**
 * Enable/disable PRBS inversion of Rx side
 *
 * @param self This engine
 * @param invert cAtTrue to invert, cAtFalse not to invert
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineRxInvert(AtPrbsEngine self, eBool invert)
    {
    mNumericalAttributeSet(RxInvert, invert);
    }

/**
 * Check if PRBS is in invert mode  of Rx side
 *
 * @param self This engine
 *
 * @retval cAtTrue Invert
 * @retval cAtFalse No invert
 */
eBool AtPrbsEngineRxIsInverted(AtPrbsEngine self)
    {
    mAttributeGet(RxIsInverted, eBool, cAtFalse);
    }

/**
 * Get current alarm of PRBS engine
 *
 * @param self This engine
 * @return ORed of @ref eAtPrbsEngineAlarmType "PRBS engine alarm types"
 */
uint32 AtPrbsEngineAlarmGet(AtPrbsEngine self)
    {
    mNullObjectCheck(0);

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->AlarmGet(self);
    }

/**
 * Get channel that this engine generate PRBS data on
 *
 * @param self This engine
 *
 * @return Channel that this engine generate PRBS data on
 */
AtChannel AtPrbsEngineChannelGet(AtPrbsEngine self)
    {
    mNoParamObjectGet(ChannelGet, AtChannel);
    }

/**
 * Get channel description
 *
 * @param self This engine
 *
 * @return Channel description
 */
const char *AtPrbsEngineChannelDescription(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->ChannelDescription(self);
    return NULL;
    }

/**
 * Force/unforce error
 *
 * @param self This engine
 * @param force cAtTrue to force and cAtFalse to unforce
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineErrorForce(AtPrbsEngine self, eBool force)
    {
    mNumericalAttributeSet(ErrorForce, force);
    }

/**
 * Check if error is being forced
 *
 * @param self This engine
 *
 * @return cAtTrue if error is being forced, otherwise, cAtFalse is returned
 */
eBool AtPrbsEngineErrorIsForced(AtPrbsEngine self)
    {
    mAttributeGet(ErrorIsForced, eBool, cAtFalse);
    }

/**
 * Multiple error insert with a error rate at TX side
 *
 * @param self This engine
 * @param errorRate @ref eAtBerRate "Error rate".
 * Call AtPrbsEngineErrorForce() to control enable/disable forcing
 *
 * @return AT return code.
 */
eAtModulePrbsRet AtPrbsEngineTxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    mNumericalAttributeSet(TxErrorRateSet, errorRate);
    }

/**
 * Get configured error rate at TX side
 *
 * @param self This engine
 *
 * @return Configured @ref eAtBerRate "error rate"
 */
eAtBerRate AtPrbsEngineTxErrorRateGet(AtPrbsEngine self)
    {
    mAttributeGet(TxErrorRateGet, eAtBerRate, cAtBerRateUnknown);
    }

/**
 * Get bit error rate monitored at the RX side
 *
 * @param self This engine
 *
 * @return Monitored @ref eAtBerRate "error rate"
 */
eAtBerRate AtPrbsEngineRxErrorRateGet(AtPrbsEngine self)
    {
    mAttributeGet(RxErrorRateGet, eAtBerRate, cAtBerRateUnknown);
    }

/**
 * Enable/disable PRBS engine
 *
 * @param self This engine
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineEnable(AtPrbsEngine self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

/**
 * Check if PRBS engine is enabled.
 *
 * @param self This engine
 * @return cAtTrue if engine is enabled
 */
eBool AtPrbsEngineIsEnabled(AtPrbsEngine self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Initialize PRBS engine
 *
 * @param self This engine
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineInit(AtPrbsEngine self)
    {
    mNoParamCall(Init, eAtModulePrbsRet, cAtErrorNullPointer);
    }

/**
 * Show PRBS engine debug information
 *
 * @param self This engine
 */
void AtPrbsEngineDebug(AtPrbsEngine self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

/**
 * Enable/disable PRBS engine at transmit direction
 *
 * @param self This engine
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineTxEnable(AtPrbsEngine self, eBool enable)
    {
    mNumericalAttributeSet(TxEnable, enable);
    }

/**
 * Check if PRBS engine is enabled at transmit direction.
 *
 * @param self This engine
 * @return cAtTrue if engine is enabled
 */
eBool AtPrbsEngineTxIsEnabled(AtPrbsEngine self)
    {
    mAttributeGet(TxIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable PRBS engine at received direction
 *
 * @param self This engine
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineRxEnable(AtPrbsEngine self, eBool enable)
    {
    mNumericalAttributeSet(RxEnable, enable);
    }

/**
 * Check if PRBS engine is enabled at received direction.
 *
 * @param self This engine
 * @return cAtTrue if engine is enabled
 */
eBool AtPrbsEngineRxIsEnabled(AtPrbsEngine self)
    {
    mAttributeGet(RxIsEnabled, eBool, cAtFalse);
    }

/**
 * Set PRBS mode at transmit direction
 *
 * @param self This engine
 * @param prbsMode @ref eAtPrbsMode "PRBS modes"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineTxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    mNumericalAttributeSetThenCache(TxModeSet, prbsMode);
    }

/**
 * Get PRBS mode at transmit direction
 *
 * @param self This engine
 *
 * @return @ref eAtPrbsMode "PRBS modes"
 */
eAtPrbsMode AtPrbsEngineTxModeGet(AtPrbsEngine self)
    {
    mAttributeGetWithCache(TxModeGet, eAtPrbsMode, cAtPrbsModeInvalid);
    }

/**
 * Set PRBS mode at receive direction
 *
 * @param self This engine
 * @param prbsMode @ref eAtPrbsMode "PRBS modes"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineRxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    mNumericalAttributeSet(RxModeSet, prbsMode);
    }

/**
 * Get PRBS mode at receive direction
 *
 * @param self This engine
 *
 * @return @ref eAtPrbsMode "PRBS modes"
 */
eAtPrbsMode AtPrbsEngineRxModeGet(AtPrbsEngine self)
    {
    mAttributeGet(RxModeGet, eAtPrbsMode, cAtPrbsModeInvalid);
    }

/**
 * Check if a specific PRBS mode is supported
 *
 * @param self This engine
 * @param prbsMode @ref eAtPrbsMode "PRBS mode"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPrbsEngineModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->ModeIsSupported(self, prbsMode);
    return cAtFalse;
    }

/**
 * Set fixed pattern in the PRBS fixed pattern mode at transmit direction
 *
 * @param self This engine
 * @param fixedPattern Fixed pattern
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineTxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    mNumericalAttributeSetWithCache(TxFixedPatternSet, fixedPattern);
    }

/**
 * Get the fixed pattern at transmit direction
 *
 * @param self This engine
 *
 * @return a fixed pattern
 */
uint32 AtPrbsEngineTxFixedPatternGet(AtPrbsEngine self)
    {
    mAttributeGetWithCache(TxFixedPatternGet, uint32, cAtPrbsModeInvalid);
    }

/**
 * Set fixed pattern in the PRBS fixed pattern mode at both directions
 *
 * @param self This engine
 * @param fixedPattern Fixed pattern
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    mNumericalAttributeSet(FixedPatternSet, fixedPattern);
    }

/**
 * Get the fixed pattern at both directions
 *
 * @param self This engine
 *
 * @return a fixed pattern
 */
uint32 AtPrbsEngineFixedPatternGet(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->FixedPatternGet(self);
    return 0x0;
    }

/**
 * Set fixed pattern in the PRBS fixed pattern mode at receive direction
 *
 * @param self This engine
 * @param fixedPattern fixed pattern
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineRxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    mNumericalAttributeSet(RxFixedPatternSet, fixedPattern);
    }

/**
 * Get PRBS fixed pattern at receive direction
 *
 * @param self This engine
 *
 * @return a fixed pattern
 */
uint32 AtPrbsEngineRxFixedPatternGet(AtPrbsEngine self)
    {
    mAttributeGet(RxFixedPatternGet, uint32, cAtPrbsModeInvalid);
    }

/**
 * Get performance counter
 *
 * @param self This channel
 * @param counterType @ref eAtPrbsEngineCounterType "Counter types"
 *
 * @return Counter value
 */
uint32 AtPrbsEngineCounterGet(AtPrbsEngine self, uint16 counterType)
    {
    if (self == NULL)
        return 0;

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->CounterGet(self, counterType);
    }

/**
 * Read then clear performance counter
 *
 * @param self This channel
 * @param counterType @ref eAtPrbsEngineCounterType "Counter types"
 *
 * @return Value of counter before clearing
 */
uint32 AtPrbsEngineCounterClear(AtPrbsEngine self, uint16 counterType)
    {
    if (self == NULL)
        return 0;

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->CounterClear(self, counterType);
    }

/**
 * Get performance 64 bit counter
 *
 * @param self This channel
 * @param counterType @ref eAtPrbsEngineCounterType "Counter types"
 *
 * @return Counter value
 */
uint64 AtPrbsEngineCounter64BitGet(AtPrbsEngine self, uint16 counterType)
    {
    if (self == NULL)
        return 0;

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->Counter64BitGet(self, counterType);
    }

/**
 * Read then clear performance 64 bit counter
 *
 * @param self This channel
 * @param counterType @ref eAtPrbsEngineCounterType "Counter types"
 *
 * @return Value of counter before clearing
 */
uint64 AtPrbsEngineCounter64BitClear(AtPrbsEngine self, uint16 counterType)
    {
    if (self == NULL)
        return 0;

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->Counter64BitClear(self, counterType);
    }

/**
 * Check if a counter type is supported
 *
 * @param self This channel
 * @param counterType @ref eAtPrbsEngineCounterType "Counter types"
 *
 * @return cAtTrue if counter type is supported or cAtFalse if not supported
 */
eBool AtPrbsEngineCounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    mOneParamAttributeGet(CounterIsSupported, counterType, eBool, cAtFalse);
    }

/**
 * Set generating and monitoring sides
 *
 * @param self This engine
 * @param side @ref eAtPrbsSide "PRBS sides"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (AtPrbsEngineIsValidSide(side))
        mNumericalAttributeSet(SideSet, side);
    return cAtErrorInvlParm;
    }

/**
 * Get side that PRBS is generating and monitoring
 *
 * @param self This engine
 *
 * @return @ref eAtPrbsSide "Generating or monitoring sides"
 */
eAtPrbsSide AtPrbsEngineSideGet(AtPrbsEngine self)
    {
    mAttributeGet(SideGet, eAtPrbsSide, cAtPrbsSideUnknown);
    }

/**
 * Set side for PRBS generating
 *
 * @param self This engine
 * @param side @ref eAtPrbsSide "PRBS sides"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineGeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (AtPrbsEngineIsValidSide(side))
        mNumericalAttributeSet(GeneratingSideSet, side);
    return cAtErrorInvlParm;
    }

/**
 * Get side of PRBS generating
 *
 * @param self This engine
 *
 * @return side
 */
eAtPrbsSide AtPrbsEngineGeneratingSideGet(AtPrbsEngine self)
    {
    mAttributeGet(GeneratingSideGet, eAtPrbsSide, cAtPrbsSideUnknown);
    }

/**
 * Set side for PRBS monitoring
 *
 * @param self This engine
 * @param side @ref eAtPrbsSide "PRBS sides"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineMonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (AtPrbsEngineIsValidSide(side))
        mNumericalAttributeSet(MonitoringSideSet, side);
    return cAtErrorInvlParm;
    }

/**
 * Get side of PRBS monitoring
 *
 * @param self This engine
 *
 * @return side
 */
eAtPrbsSide AtPrbsEngineMonitoringSideGet(AtPrbsEngine self)
    {
    mAttributeGet(MonitoringSideGet, eAtPrbsSide, cAtPrbsSideUnknown);
    }

/**
 * Latch all of counters for reading later.
 *
 * @param self This channel
 * @param clear Clear counters after latching
 *              - cAtTrue: after latching, hardware counters will be clear
 *              - cAtFalse: just latch, hardware continue counts from current values
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineAllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    mOneParamAttributeGet(AllCountersLatchAndClear, clear, eAtModulePrbsRet, cAtErrorNullPointer)
    }

/**
 * Get alarm history
 *
 * @param self This engine
 *
 * @return alarm history mask (read only mode)
 */
uint32 AtPrbsEngineAlarmHistoryGet(AtPrbsEngine self)
    {
    mNullObjectCheck(0);

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->AlarmHistoryGet(self);
    }

/**
 * Get alarm history
 *
 * @param self This engine
 *
 * @return alarm history mask (read to clear mode)
 */
uint32 AtPrbsEngineAlarmHistoryClear(AtPrbsEngine self)
    {
    mNullObjectCheck(0);

    if (ShouldInvalidateStatus(self))
        return 0;

    return mMethodsGet(self)->AlarmHistoryClear(self);
    }

/**
 * Forced number bit error
 *
 * @param self This engine
 * @param numErrors Number of errors to force
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineTxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    mNumericalAttributeSet(TxErrorInject, numErrors);
    }

/**
 * Set bit order in PRBS mode fixed pattern
 *
 * @param self This engine
 * @param order Bit order @ref eAtPrbsBitOrder "Bit order"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    mNumericalAttributeSet(BitOrderSet, order);
    }

/**
 * Get bit order in PRBS mode fixed pattern
 *
 * @param self This engine
 *
 * @return Bit order
 */
eAtPrbsBitOrder AtPrbsEngineBitOrderGet(AtPrbsEngine self)
    {
    mAttributeGet(BitOrderGet, eAtPrbsBitOrder, cAtPrbsBitOrderUnknown);
    }

/**
 * Set PRBS bit order in PRBS mode fixed pattern at transmit direction
 *
 * @param self This engine
 * @param order Bit order @ref eAtPrbsBitOrder "Bit order"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineTxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    mNumericalAttributeSet(TxBitOrderSet, order);
    }

/**
 * Get PRBS bit order in PRBS mode fixed pattern at transmit direction
 *
 * @param self This engine
 *
 * @return Bit order
 */
eAtPrbsBitOrder AtPrbsEngineTxBitOrderGet(AtPrbsEngine self)
    {
    mAttributeGet(TxBitOrderGet, eAtPrbsBitOrder, cAtPrbsBitOrderUnknown);
    }

/**
 * Set PRBS bit order in PRBS mode fixed pattern at receive direction
 *
 * @param self This engine
 * @param order Bit order @ref eAtPrbsBitOrder "Bit order"
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineRxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    mNumericalAttributeSet(RxBitOrderSet, order);
    }

/**
 * Get PRBS bit order in PRBS mode fixed pattern at receive direction
 *
 * @param self This engine
 *
 * @return Bit order
 */
eAtPrbsBitOrder AtPrbsEngineRxBitOrderGet(AtPrbsEngine self)
    {
    mAttributeGet(RxBitOrderGet, eAtPrbsBitOrder, cAtPrbsBitOrderUnknown);
    }

/**
 * Check if input mode is fixed pattern
 *
 * @param mode Mode
 *
 * @retval cAtTrue if the input mode is fixed pattern
 * @retval cAtFalse if the input mode is not fixed pattern
 */
eBool AtPrbsModeIsFixedPattern(eAtPrbsMode mode)
    {
    if ((mode == cAtPrbsModePrbsFixedPattern1Byte)  ||
        (mode == cAtPrbsModePrbsFixedPattern2Bytes) ||
        (mode == cAtPrbsModePrbsFixedPattern3Bytes) ||
        (mode == cAtPrbsModePrbsFixedPattern4Bytes))
        return cAtTrue;
    return cAtFalse;
    }

/**
 * To check if input side is valid
 *
 * @param side @ref eAtPrbsSide "PRBS side"
 *
 * @retval cAtTrue if valid
 * @retval cAtFalse if invalid
 */
eBool AtPrbsEngineIsValidSide(eAtPrbsSide side)
    {
    switch (side)
        {
        case cAtPrbsSideTdm    : return cAtTrue;
        case cAtPrbsSidePsn    : return cAtTrue;
        case cAtPrbsSideUnknown: return cAtFalse;
        default:
            return cAtFalse;
        }
    }

/**
 * Get gating timer
 *
 * @param self This engine
 *
 * @return Gating timer
 */
AtTimer AtPrbsEngineGatingTimer(AtPrbsEngine self)
    {
    if (self)
        return GatingTimer(self);
    return NULL;
    }

/**
 * To check if the prbs egine is raw prbs
 *
 * @retval cAtTrue if it is raw prbs
 * @retval cAtFalse if it is framed prbs
 */
eBool AtPrbsEngineIsRawPrbs(AtPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->IsRawPrbs(self);
    return cAtFalse;
    }

/** @} */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsCountersInternal.h
 * 
 * Created Date: Oct 9, 2018
 *
 * Description : Generic PRBS counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSCOUNTERSINTERNAL_H_
#define _ATPRBSCOUNTERSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtPrbsCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsCountersMethods
    {
    uint64 *(*CounterAddress)(AtPrbsCounters self, uint16 counterType);
    }tAtPrbsCountersMethods;

typedef struct tAtPrbsCounters
    {
    tAtObject super;
    const tAtPrbsCountersMethods *methods;
    }tAtPrbsCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsCounters AtPrbsCountersObjectInit(AtPrbsCounters self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSCOUNTERSINTERNAL_H_ */


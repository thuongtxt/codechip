/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtModulePrbsInternal.h
 * 
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPRBSINTERNAL_H_
#define _ATMODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtModuleInternal.h"
#include "AtModulePrbs.h"
#include "AtModulePw.h"
#include "AtConcateGroup.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePrbsMethods
    {
    eBool (*NeedPwInitConfig)(AtModulePrbs self);
    AtPrbsEngine (*De3PrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3);
    AtPrbsEngine (*De1PrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1);
    AtPrbsEngine (*NxDs0PrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0);
    AtPrbsEngine (*SdhVcPrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
    AtPrbsEngine (*PwPrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtPw pw);
    AtPrbsEngine (*EncapPrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtEncapChannel encap);
    AtPrbsEngine (*EthPrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtEthPort port);
    eAtRet (*ConcateGroupPrbsEngineDelete)(AtModulePrbs self, AtPrbsEngine engine);
    eAtRet (*EngineDelete)(AtModulePrbs self, uint32 engineId);
    uint32 (*MaxNumEngines)(AtModulePrbs self);
    AtPrbsEngine (*PrbsEngineGet)(AtModulePrbs self, uint32 engineId);
    eAtRet (*TimeMeasurementErrorSet)(AtModulePrbs self, uint32 error);
    uint32 (*TimeMeasurementErrorGet)(AtModulePrbs self);

    /* Internal */
    eAtRet (*EngineObjectDelete)(AtModulePrbs self, AtPrbsEngine engine);
    eBool (*AllChannelsSupported)(AtModulePrbs self);
    eBool (*AllPwsPrbsIsSupported)(AtModulePrbs self);
    AtPrbsEngine (*De3LinePrbsEngineCreate)(AtModulePrbs self, AtPdhDe3 de3);
    AtPrbsEngine (*De1LinePrbsEngineCreate)(AtModulePrbs self, AtPdhDe1 de1);
    AtPrbsEngine (*SdhLinePrbsEngineCreate)(AtModulePrbs self, AtSdhLine line);
    AtPrbsEngine (*LineDccHdlcPrbsEngineCreate)(AtModulePrbs self, uint32 engineId, AtHdlcChannel hdlc);
    AtPrbsEngine (*ConcateGroupPrbsEngineCreate)(AtModulePrbs self, AtConcateGroup group);
    eBool (*ShouldInvalidateStatusWhenRxIsDisabled)(AtModulePrbs self);    
    AtAttPrbsManager (*AttPrbsManagerCreate)(AtModulePrbs self);
    eBool (*ChannelizedPrbsAllowed)(AtModulePrbs self);
    }tAtModulePrbsMethods;

typedef struct tAtModulePrbs
    {
    tAtModule super;
    const tAtModulePrbsMethods *methods;
    AtAttPrbsManager att;
    /* Private data */
    }tAtModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs AtModulePrbsObjectInit(AtModulePrbs self, AtDevice device);
eAtRet AtModulePrbsEngineObjectDelete(AtModulePrbs self, AtPrbsEngine engine);
eBool AtModulePrbsAllChannelsSupported(AtModulePrbs self);
AtPrbsEngine AtModulePrbsDe3LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe3 de3);
AtPrbsEngine AtModulePrbsDe1LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe1 de1);
AtPrbsEngine AtModulePrbsSdhLinePrbsEngineCreate(AtModulePrbs self, AtSdhLine line);
AtPrbsEngine AtModulePrbsConcateGroupPrbsEngineCreate(AtModulePrbs self, AtConcateGroup group);
eAtRet AtModulePrbsConcateGroupPrbsEngineDelete(AtModulePrbs self, AtPrbsEngine engine);
AtPrbsEngine AtModulePrbsLineDccHdlcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtHdlcChannel hdlc);
eBool AtModulePrbsNeedPwInitConfig(AtModulePrbs self);
eBool AtModulePrbsShouldInvalidateStatusWhenRxIsDisabled(AtModulePrbs self);
eBool AtModulePrbsNeedPwInitConfig(AtModulePrbs self);
eBool AtModulePrbsChannelizedPrbsAllowed(AtModulePrbs self);
eBool AtModulePrbsAllPwsPrbsIsSupported(AtModulePrbs self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPRBSINTERNAL_H_ */


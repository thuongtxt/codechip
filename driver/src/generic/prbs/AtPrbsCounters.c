/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtPrbsCounters.c
 *
 * Created Date: Oct 9, 2018
 *
 * Description : Abstract PRBS counters implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtPrbsCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPrbsCountersMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint64 *CounterAddress(AtPrbsCounters self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return NULL;
    }

static void CounterSet(AtPrbsCounters self, uint16 counterType, uint64 value)
    {
    uint64 *pCounter = mMethodsGet(self)->CounterAddress(self, counterType);
    if (pCounter)
        *pCounter = value;
    }

static void CounterUpdate(AtPrbsCounters self, uint16 counterType, uint64 value)
    {
    uint64 *pCounter = mMethodsGet(self)->CounterAddress(self, counterType);
    if (pCounter)
        *pCounter = *pCounter + value;
    }

static uint64 CounterRead2Clear(AtPrbsCounters self, uint16 counterType, eBool read2Clear)
    {
    uint64 *pCounter;
    uint64 counter;

    pCounter = mMethodsGet(self)->CounterAddress(self, counterType);
    if (pCounter == NULL)
        return 0;

    counter = *pCounter;
    if (read2Clear)
        *pCounter = 0;

    return counter;
    }

static void MethodsInit(AtPrbsCounters self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CounterAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPrbsCounters);
    }

AtPrbsCounters AtPrbsCountersObjectInit(AtPrbsCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtPrbsCountersCounterSet(AtPrbsCounters self, uint16 counterType, uint64 value)
    {
    if (self)
        CounterSet(self, counterType, value);
    }

void AtPrbsCountersCounterUpdate(AtPrbsCounters self, uint16 counterType, uint64 value)
    {
    if (self)
        CounterUpdate(self, counterType, value);
    }

uint64 AtPrbsCountersCounterClear(AtPrbsCounters self, uint16 counterType)
    {
    if (self)
        return CounterRead2Clear(self, counterType, cAtTrue);
    return 0;
    }

uint64 AtPrbsCountersCounterGet(AtPrbsCounters self, uint16 counterType)
    {
    if (self)
        return CounterRead2Clear(self, counterType, cAtFalse);
    return 0;
    }

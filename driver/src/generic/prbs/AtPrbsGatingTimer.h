/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsGatingTimer.h
 * 
 * Created Date: Apr 8, 2017
 *
 * Description : Gating timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSGATINGTIMER_H_
#define _ATPRBSGATINGTIMER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "../util/timer/AtHwTimer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsGatingTimer * AtPrbsGatingTimer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine AtPrbsGatingTimerPrbsEngine(AtPrbsGatingTimer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSGATINGTIMER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtPrbsGatingTimer.c
 *
 * Created Date: Apr 8, 2017
 *
 * Description : PRBS gating timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPrbsGatingTimerInternal.h"
#include "AtPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPrbsGatingTimer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtHwTimerMethods m_AtHwTimerOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(AtHwTimer self, uint32 address, eAtModule module)
    {
    return AtPrbsEngineRead(AtPrbsGatingTimerPrbsEngine(mThis(self)), address, module);
    }

static void Write(AtHwTimer self, uint32 address, uint32 value, eAtModule module)
    {
    AtPrbsEngineWrite(AtPrbsGatingTimerPrbsEngine(mThis(self)), address, value, module);
    }

static eBool IsSimulated(AtHwTimer self)
    {
    return AtPrbsEngineIsSimulated(mThis(self)->engine);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPrbsGatingTimer object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(engine);
    }

static void OverrideAtObject(AtTimer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtHwTimer(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHwTimerOverride, mMethodsGet(timer), sizeof(m_AtHwTimerOverride));

        mMethodOverride(m_AtHwTimerOverride, Read);
        mMethodOverride(m_AtHwTimerOverride, Write);
        mMethodOverride(m_AtHwTimerOverride, IsSimulated);
        }

    mMethodsSet(timer, &m_AtHwTimerOverride);
    }

static void Override(AtTimer self)
    {
    OverrideAtObject(self);
    OverrideAtHwTimer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPrbsGatingTimer);
    }

AtTimer AtPrbsGatingTimerObjectInit(AtTimer self, AtPrbsEngine engine)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtHwTimerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->engine = engine;

    return self;
    }

AtPrbsEngine AtPrbsGatingTimerPrbsEngine(AtPrbsGatingTimer self)
    {
    return self ? self->engine : NULL;
    }

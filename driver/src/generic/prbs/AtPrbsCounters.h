/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsCounters.h
 * 
 * Created Date: Oct 9, 2018
 *
 * Description : Abstract PRBS counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSCOUNTERS_H_
#define _ATPRBSCOUNTERS_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsCounters *AtPrbsCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtPrbsCountersCounterSet(AtPrbsCounters self, uint16 counterType, uint64 value);
void AtPrbsCountersCounterUpdate(AtPrbsCounters self, uint16 counterType, uint64 value);
uint64 AtPrbsCountersCounterClear(AtPrbsCounters self, uint16 counterType);
uint64 AtPrbsCountersCounterGet(AtPrbsCounters self, uint16 counterType);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSCOUNTERS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtModulePrbs.c
 *
 * Created Date: Sep 20, 2012
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtModulePrbs)self)
/*--------------------------- Macros -----------------------------------------*/
#define mPrbsEngineCreateWrap(self, engineId, channel, method)                 \
do                                                                             \
    {                                                                          \
    AtPrbsEngine newEngine = NULL;                                             \
                                                                               \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        const char* objectDesc;                                                \
        AtDriverLogLock();                                                     \
        objectDesc = AtDriverObjectStringToSharedBuffer((AtObject)channel);    \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %u, %s)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), engineId, objectDesc);\
        AtDriverLogUnLock();                                                   \
        }                                                                      \
                                                                               \
    if (self && channel)                                                       \
        newEngine = mMethodsGet(self)->method(self, engineId, channel);        \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return newEngine;                                                          \
    }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                m_methodsInit = 0;
static tAtModulePrbsMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectImplement = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePrbs);
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "prbs";
    }

static eBool NeedPwInitConfig(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
	AtUnused(engineId);
    return NULL;
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
	AtUnused(nxDs0);
	AtUnused(self);
    AtUnused(engineId);
    /* Sub class should do */
    return NULL;
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
	AtUnused(sdhVc);
	AtUnused(self);
    AtUnused(engineId);
    /* Sub class should do */
    return NULL;
    }

static AtPrbsEngine EncapPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEncapChannel encapChannel)
    {
    /* Sub class should do */
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(encapChannel);
    return NULL;
    }

static AtPrbsEngine PwPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPw pw)
    {
    /* Sub class should do */
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(pw);
    return NULL;
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    /* Sub class should do */
    AtUnused(self);
    AtUnused(de3);
    AtUnused(engineId);
    return NULL;
    }

static AtPrbsEngine ConcateGroupPrbsEngineCreate(AtModulePrbs self, AtConcateGroup group)
    {
    /* Sub class should do */
    AtUnused(self);
    AtUnused(group);
    return NULL;
    }

static eAtRet ConcateGroupPrbsEngineDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    /* Sub class should do */
    AtUnused(self);
    AtUnused(engine);
    return cAtErrorNotImplemented;
    }

static eAtRet EngineDelete(AtModulePrbs self, uint32 engineId)
    {
    AtUnused(self);
    AtUnused(engineId);
    return cAtErrorNotImplemented;
    }

static uint32 MaxNumEngines(AtModulePrbs self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static eAtRet TimeMeasurementErrorSet(AtModulePrbs self, uint32 error)
    {
    AtUnused(self);
    AtUnused(error);
    return cAtErrorNotImplemented;
    }

static uint32 TimeMeasurementErrorGet(AtModulePrbs self)
    {
    AtUnused(self);
    return 0;
    }

static AtPrbsEngine PrbsEngineGet(AtModulePrbs self, uint32 engineId)
    {
    AtUnused(self);
    AtUnused(engineId);

    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engineId);
    return NULL;
    }

static eAtRet EngineObjectDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)engine);
    return cAtOk;
    }

static eBool AllChannelsSupported(AtModulePrbs self)
    {
    /* Just ATT support full channels. Let it determine */
    AtUnused(self);
    return cAtFalse;
    }

static AtPrbsEngine De3LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static AtPrbsEngine De1LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return NULL;
    }

static AtPrbsEngine SdhLinePrbsEngineCreate(AtModulePrbs self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static AtPrbsEngine EthPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(port);
    return NULL;
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    uint32 engineId;
    AtModulePrbs prbsModule = (AtModulePrbs)self;

    for (engineId = 0; engineId < AtModulePrbsMaxNumEngines(prbsModule); engineId++)
        AtModulePrbsEngineDelete(prbsModule, engineId);

    return cAtOk;
    }

static AtAttPrbsManager AttPrbsManagerCreate(AtModulePrbs self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPrbsEngine LineDccHdlcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtHdlcChannel hdlc)
    {
    AtUnused(hdlc);
    AtUnused(self);
    AtUnused(engineId);
    return NULL;
    }

static eBool ShouldInvalidateStatusWhenRxIsDisabled(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ChannelizedPrbsAllowed(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    if (mThis(self)->att)
        AtObjectDelete((AtObject)(mThis(self)->att));
    mThis(self)->att = NULL;

    /* Super deletion */
    m_AtObjectImplement->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
	m_AtObjectImplement->Serialize(self, encoder);
    mEncodeNone(att);
    }

static eBool AllPwsPrbsIsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void MethodsInit(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NeedPwInitConfig);
        mMethodOverride(m_methods, De1PrbsEngineCreate);
        mMethodOverride(m_methods, NxDs0PrbsEngineCreate);
        mMethodOverride(m_methods, SdhVcPrbsEngineCreate);
        mMethodOverride(m_methods, EncapPrbsEngineCreate);
        mMethodOverride(m_methods, De3PrbsEngineCreate);
        mMethodOverride(m_methods, PwPrbsEngineCreate);
        mMethodOverride(m_methods, EthPrbsEngineCreate);
        mMethodOverride(m_methods, EngineDelete);
        mMethodOverride(m_methods, MaxNumEngines);
        mMethodOverride(m_methods, PrbsEngineGet);
        mMethodOverride(m_methods, TimeMeasurementErrorSet);
        mMethodOverride(m_methods, TimeMeasurementErrorGet);
        mMethodOverride(m_methods, AttPrbsManagerCreate);

        /* Internal */
        mMethodOverride(m_methods, EngineObjectDelete);
        mMethodOverride(m_methods, AllChannelsSupported);
        mMethodOverride(m_methods, De3LinePrbsEngineCreate);
        mMethodOverride(m_methods, De1LinePrbsEngineCreate);
        mMethodOverride(m_methods, SdhLinePrbsEngineCreate);
        mMethodOverride(m_methods, ConcateGroupPrbsEngineCreate);
        mMethodOverride(m_methods, ConcateGroupPrbsEngineDelete);
        mMethodOverride(m_methods, LineDccHdlcPrbsEngineCreate);
        mMethodOverride(m_methods, ShouldInvalidateStatusWhenRxIsDisabled);
        mMethodOverride(m_methods, ChannelizedPrbsAllowed);
        mMethodOverride(m_methods, AllPwsPrbsIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtObject self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectImplement = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectImplement, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }
    

static void Override(AtModulePrbs self)
    {
    OverrideAtObject((AtObject)self);
    OverrideAtModule(self);
    }

AtModulePrbs AtModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModulePrbs, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet AtModulePrbsEngineObjectDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->EngineObjectDelete(self, engine);
    return cAtErrorNullPointer;
    }

eBool AtModulePrbsAllChannelsSupported(AtModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->AllChannelsSupported(self);
    return cAtFalse;
    }

AtPrbsEngine AtModulePrbsDe3LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe3 de3)
    {
    if (self && de3)
        {
        AtPrbsEngine engine = mMethodsGet(self)->De3LinePrbsEngineCreate(self, de3);
        if (engine)
            AtPrbsEngineInit(engine);

        return engine;
        }
    return NULL;
    }

AtPrbsEngine AtModulePrbsDe1LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe1 de1)
    {
    if (self && de1)
        {
        AtPrbsEngine engine = mMethodsGet(self)->De1LinePrbsEngineCreate(self, de1);
        if (engine)
            AtPrbsEngineInit(engine);

        return engine;
        }
    return NULL;
    }

AtPrbsEngine AtModulePrbsSdhLinePrbsEngineCreate(AtModulePrbs self, AtSdhLine line)
    {
    if (self && line)
        {
        AtPrbsEngine engine = mMethodsGet(self)->SdhLinePrbsEngineCreate(self, line);
        if (engine)
            AtPrbsEngineInit(engine);

        return engine;
        }
    return NULL;
    }

AtPrbsEngine AtModulePrbsConcateGroupPrbsEngineCreate(AtModulePrbs self, AtConcateGroup group)
    {
    if (self && group)
        return mMethodsGet(self)->ConcateGroupPrbsEngineCreate(self, group);
    return NULL;
    }

eAtRet AtModulePrbsConcateGroupPrbsEngineDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    if (self && engine)
        return mMethodsGet(self)->ConcateGroupPrbsEngineDelete(self, engine);
    return cAtErrorNotExist;
    }

AtPrbsEngine AtModulePrbsLineDccHdlcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtHdlcChannel hdlc)
    {
    if (self && hdlc)
        return mMethodsGet(self)->LineDccHdlcPrbsEngineCreate(self, engineId, hdlc);
    return NULL;
    }

eBool AtModulePrbsShouldInvalidateStatusWhenRxIsDisabled(AtModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->ShouldInvalidateStatusWhenRxIsDisabled(self);
    return cAtFalse;
    }

eBool AtModulePrbsChannelizedPrbsAllowed(AtModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->ChannelizedPrbsAllowed(self);
    return cAtFalse;
    }

eBool AtModulePrbsAllPwsPrbsIsSupported(AtModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->AllPwsPrbsIsSupported(self);
    return cAtFalse;
    }
/**
 * @addtogroup AtModulePrbs
 * @{
 */

/**
 * Get maximum number of engines this module can support
 *
 * @param self This module
 * @return Maximum number of engines this module can support
 */
uint32 AtModulePrbsMaxNumEngines(AtModulePrbs self)
    {
    mAttributeGet(MaxNumEngines, uint32, 0);
    }

/**
 * Set PRBS Disruption Threshold
 *
 * @param self This module
 * @param period The maximum period in which detect LOS
 *
 * @return Maximum detection time
 */
eAtRet AtModulePrbsTimeMeasurementErrorSet(AtModulePrbs self, uint32 error)
    {
    mNumericalAttributeSet(TimeMeasurementErrorSet, error);
    }

/**
 * Get PRBS Disruption Threshold
 *
 * @param self This module
 *
 * @return Maximum detection time
 *
 */
uint32 AtModulePrbsTimeMeasurementErrorGet(AtModulePrbs self)
    {
    mAttributeGet(TimeMeasurementErrorGet, uint32, 0);
    }

/**
 * Get PRBS engine instance
 *
 * @param self This module
 * @param engineId Engine ID
 *
 * @return Engine instance if it was created, or NULL it it has not been created
 * yet or the ID is invalid
 */
AtPrbsEngine AtModulePrbsEngineGet(AtModulePrbs self, uint32 engineId)
    {
    mOneParamObjectGet(PrbsEngineGet, AtPrbsEngine, engineId);
    }

/**
 * Create DS1/E1 PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param de1 DS1/E1 object
 *
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsDe1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    mPrbsEngineCreateWrap(self, engineId, de1, De1PrbsEngineCreate);
    }

/**
 * Create NxDS0 PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param nxDs0 NxDS0 object
 *
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsNxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    mPrbsEngineCreateWrap(self, engineId, nxDs0, NxDs0PrbsEngineCreate);
    }

/**
 * Create SDH VC PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param sdhVc SDH VC object
 *
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsSdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    mPrbsEngineCreateWrap(self, engineId, sdhVc, SdhVcPrbsEngineCreate);
    }

/**
 * Create DS3/E3 PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param de3 DS3/E3 object
 *
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsDe3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    mPrbsEngineCreateWrap(self, engineId, de3, De3PrbsEngineCreate);
    }

/**
 * Create ENCAP PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param encap Encapsulation channel
 *
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsEncapChannelPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEncapChannel encap)
    {
    mPrbsEngineCreateWrap(self, engineId, encap, EncapPrbsEngineCreate);
    }

/**
 * Create PW PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param pw PW object
 *
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsPwPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPw pw)
    {
    mPrbsEngineCreateWrap(self, engineId, pw, PwPrbsEngineCreate);
    }

/**
 * Create Ethernet port PRBS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param port Ethernet port
 * @return AtPrbsEngine on success, otherwise, NULL is returned
 */
AtPrbsEngine AtModulePrbsEthPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEthPort port)
    {
    mPrbsEngineCreateWrap(self, engineId, port, EthPrbsEngineCreate);
    }

/**
 * Delete an engine
 *
 * @param self This module
 * @param engineId Engine to delete
 *
 * @return AT return code
 */
eAtRet AtModulePrbsEngineDelete(AtModulePrbs self, uint32 engineId)
    {
    mNumericalAttributeSet(EngineDelete, engineId);
    }

AtAttPrbsManager AtModulePrbsAttManager(AtModulePrbs self)
    {
    if (self)
        {
        if (self->att == NULL)
            {
            self->att = mMethodsGet(self)->AttPrbsManagerCreate(self);
            if (self->att)
                AtAttPrbsManagerInit(self->att);

            }
        return self->att;
        }
    return NULL;
    }    

/**
 * Delete an engine
 *
 * @param self This module
 * @param engineId Engine to delete
 *
 * @return AT return code
 */
eBool AtModulePrbsNeedPwInitConfig(AtModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->NeedPwInitConfig(self);
    return cAtErrorNullPointer;
    }

/** @} */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtPrbsEngineDeprecated.c
 *
 * Created Date: Dec 1, 2017
 *
 * Description : For deprecated functions and APIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Latch all of BERT counters.
 *
 * @param self This channel
 *
 * @return AT return code
 */
eAtModulePrbsRet AtPrbsEngineAllCountersLatch(AtPrbsEngine self)
    {
    return AtPrbsEngineAllCountersLatchAndClear(self, cAtTrue);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsGatingTimerInternal.h
 * 
 * Created Date: Apr 8, 2017
 *
 * Description : PRBS gating timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSGATINGTIMERINTERNAL_H_
#define _ATPRBSGATINGTIMERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../util/timer/AtHwTimerInternal.h"
#include "AtPrbsGatingTimer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsGatingTimer
    {
    tAtHwTimer super;

    /* Private data */
    AtPrbsEngine engine;
    }tAtPrbsGatingTimer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTimer AtPrbsGatingTimerObjectInit(AtTimer self, AtPrbsEngine engine);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSGATINGTIMERINTERNAL_H_ */


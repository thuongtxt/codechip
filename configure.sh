#!/bin/bash
# Quick configure invoke for predefined targets.

usage() { 
    echo "configure.sh is the wrapper call of 'configure' with automatic environment variables assignment based on selecting defined toolchains."
    echo "Usages:                                               "
    echo "$0 [target] [-u|--unittest]                           "
    echo "                                                      "
    echo " Targets:                                             "
    echo "     --ppc600           ELDK-4.0 for PowerPC 600 platforms."
    echo "     --ppce500v2        ELDK-5.3 for PowerPC E500v2 platforms."
    echo "     --ppce5500         Freescale QorIQ-SDK-V1.5 for PowerPC E5500 platforms."
    echo "     -s|--x86           The 32-bit simulation build (x86)"
    echo "     -x|--x64           The 64-bit simulation build (x86_64)"
    echo "     -h|--help          Help \n"
    echo " Optional:                                            "
    echo "     -u|--unittest      Enable unittest\n"
    } 

params="$(getopt -o csxtduhj \
                 -l ppc600,ppce500v2,ppce5500,x86,x64,unittest,help \
                 --name "$(basename "$0")" -- "$@")"

if [ $? -ne 0 ]
then
    usage
fi

cross_compile= 
cflags=

while true
do
    case $1 in
        --ppc600)
            cross_compile=/opt/tools/eldk4.0/usr/bin/ppc_6xx
            BUILD="--build=i686-linux-gnu"
            HOST="--host=powerpc-linux-gnu"
            cflags="-g -O0"
            shift
            ;;
        --ppce500v2)
            cross_compile=/opt/tools/eldk5.3/powerpc-e500v2/sysroots/i686-eldk-linux/usr/bin/ppce500v2-linux-gnuspe/powerpc-e500v2
            BUILD="--build=i686-linux-gnu"
            HOST="--host=powerpc-linux-gnu"
            cflags="-g3 -O0 -mcpu=8540 -mabi=spe --sysroot=/opt/tools/eldk5.3/powerpc-e500v2/sysroots/ppce500v2-linux-gnuspe"
            shift
            ;;
        --ppce5500)
            cross_compile=/opt/tools/freescale/QorIQ-SDK-V1.5/x86_64/sysroots/x86_64-fsl_networking_sdk-linux/usr/bin/ppce5500-fsl_networking-linux/powerpc-fsl_networking-linux
            BUILD="--build=x86_64-linux-gnu"
            HOST="--host=powerpc-linux-gnu"
            cflags="-g3 -O0 --sysroot=/opt/tools/freescale/QorIQ-SDK-V1.5/x86_64/sysroots/ppce5500-fsl_networking-linux"
            shift
            ;;
        -s|--x86)
            cflags="-g3 -O0 -m32 -Wno-enum-compare"
            shift
            ;;
        -x|--x64)
            cflags="-g3 -O0 -Wno-enum-compare -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast"
            shift
            ;;
        -u|--unittest)
            unittest_flags="--enable-unittest"
            shift
            ;;
        -h|--help)
            usage
            exit 1
            ;;
        *)
            break
            ;;
    esac
done

if [ ! -z $cross_compile ] ; then 
    cross_compiling_vars="CC=$cross_compile-gcc LD=$cross_compile-ld AR=$cross_compile-ar RANLIB=$cross_compile-ranlib AS=$cross_compile-as OBJCOPY=$cross_compile-objcopy STRIP=$cross_compile-strip CXX=$cross_compile-g++"
fi

if [ -z "$cflags" ] ; then
    echo "No target specified. Nothing to do."
    exit 1
fi


sh ../configure $BUILD $HOST --srcdir=../ CFLAGS="$cflags" $cross_compiling_vars $unittest_flags

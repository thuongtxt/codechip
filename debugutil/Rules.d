#-----------------------------------------------------------------------------
#
# COPYRIGHT (C) 2012 Arrive Technologies Inc.
#
# The information contained herein is confidential property of Arrive Tecnologies. 
# The use, copying, transfer or disclosure of such information 
# is prohibited except by express written agreement with Arrive Technologies.
#
# File        : Rules.d
#
# Created Date: Nov-05-2012
#
# Description : Dependencies
# 
#----------------------------------------------------------------------------
# ==============================================================================
# Public
# ==============================================================================
AT_DEBUGUTIL_LIB = ${BUILD_DIR}/debugutil/debugutil.o

# ==============================================================================
# Private
# ==============================================================================
.PHONY: all debugutil clean cleandebugutil

DEBUG_UTIL_DIR = $(ATSDK_HOME)/debugutil
DEBUG_UTIL_SRC = $(shell find ${DEBUG_UTIL_DIR}/src -iname "*.c")
DEBUG_UTIL_OBJ = $(call GetObjectsFromSource, $(DEBUG_UTIL_SRC))

all: debugutil

debugutil: ${AT_DEBUGUTIL_LIB}
${AT_DEBUGUTIL_LIB}: $(DEBUG_UTIL_OBJ)
	$(AT_LD) $(LDFLAGS) $@ $^

clean: cleandebugutil
cleandebugutil: 
	rm -f $(DEBUG_UTIL_OBJ) $(AT_DEBUGUTIL_LIB)

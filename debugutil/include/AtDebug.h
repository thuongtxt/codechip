/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtDebug.h
 * 
 * Created Date: Dec 12, 2012
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEBUG_H_
#define _ATDEBUG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "AtDevice.h"
#include "AtHal.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice AtDebugDevice(void);
void AtDebugDeviceSet(uint8 deviceId);
void AtDebugDump(uint32 startAddress, uint32 stopAddress);
void AtDumpByHal(AtHal hal, uint32 startAddress, uint32 stopAddress, uint32 maskValue, uint32 shiftValue);
void AtRegisterPrettyDump(AtHal hal, uint32 startAddress, uint32 stopAddress, uint32 maskValue, uint32 shiftValue);

/* Long register access */
void AtDebugLongRead(uint32 address);
void AtDebugLongWrite(uint32 address, char *longValueString);
const char *AtDebugLongRegValue2String(const uint32 *regValues, uint16 numDwords);
eBool AtRegisterPrettyLongRead(uint32 *regValues, uint16 numDwords);

/* Debug channels */
void AtDebugAtHdlcChannel(uint32 channelId);
void AtDebugAtPppLink(uint32 linkId);
void AtDebugAtMpBundle(uint32 bundleId);
void AtDebugAtEthFlow(uint32 flowId);
void AtDebugAtEthPort(uint32 portId);
void AtDebugAtPdhDe1(uint32 de1Id);

void AtTaskSuspend(void);
void AtDebugCpuRest(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEBUG_H_ */


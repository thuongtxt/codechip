/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : AtDebug.c
 *
 * Created Date: Dec 12, 2012
 *
 * Description : Debug function
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtDebug.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtLongRegMaxSize   8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_deviceId    = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtDevice AtDebugDevice(void)
    {
    uint8 numberOfAddedDevice;
    AtDevice* addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numberOfAddedDevice);

    if ((addedDevices == NULL) || (m_deviceId >= numberOfAddedDevice))
        return NULL;

    return addedDevices[m_deviceId];
    }

void AtDebugDeviceSet(uint8 deviceId)
    {
    m_deviceId = deviceId;
    }

void AtDebugDump(uint32 startAddress, uint32 stopAddress)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtDebugDevice(), 0);
    AtDumpByHal(hal, startAddress, stopAddress, 0xffffffff, 0);
    }

void AtDebugLongRead(uint32 address)
    {
    uint32 pValue[cAtLongRegMaxSize];
    uint16 numReadDwords;

    AtOsalMemInit(pValue, 0, sizeof(pValue));

    numReadDwords = AtDeviceLongReadOnCore(AtDebugDevice(), address, pValue, cAtLongRegMaxSize, 0);
    if (numReadDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\n");
        return;
        }

    AtPrintc(cSevInfo, "Read value: %s\n", AtDebugLongRegValue2String(pValue, (uint8)numReadDwords));
    }

void AtDebugLongWrite(uint32 address, char *longValueString)
    {
    uint32 longValue[cAtLongRegMaxSize];
    uint32 numDwords;
    uint16 numWrittenDwords;

    numDwords = 0;
    AtOsalMemInit(longValue, 0, sizeof(longValue));
    if (AtStrToArrayDw(longValueString, longValue, &numDwords) == cAtFalse )
        {
        AtPrintc(cSevCritical, "ERROR: Invalid long register value\n");
        return;
        }

    numWrittenDwords = AtDeviceLongWriteOnCore(AtDebugDevice(), address, longValue, (uint16)numDwords, 0);
    if (numWrittenDwords == 0)
        AtPrintc(cSevCritical, "ERROR: Can't write! This is not a long register or it is unknown\n");
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtDebugChannel.c
 *
 * Created Date: Dec 12, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDebug.h"
#include "AtChannel.h"
#include "AtHdlcChannel.h"
#include "AtModuleEth.h"
#include "AtModulePpp.h"
#include "AtModulePdh.h"
#include "AtEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap EncapModule(void)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtDebugDevice(), cAtModuleEncap);
    }

static AtModuleEth EthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtDebugDevice(), cAtModuleEth);
    }

static AtModulePdh PdhModule(void)
    {
    return (AtModulePdh)AtDeviceModuleGet(AtDebugDevice(), cAtModulePdh);
    }

static AtModulePpp PppModule(void)
    {
    return (AtModulePpp)AtDeviceModuleGet(AtDebugDevice(), cAtModulePpp);
    }

static AtHdlcChannel HdlcChannel(uint32 channelId)
    {
    AtEncapChannel encapChannel = AtModuleEncapChannelGet(EncapModule(), (uint16)channelId);
    if (encapChannel == NULL)
        return NULL;

    if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
        return NULL;

    return (AtHdlcChannel)encapChannel;
    }

void AtDebugAtHdlcChannel(uint32 channelId)
    {
    AtHdlcChannel hdlcChannel = HdlcChannel(channelId);

    if (hdlcChannel == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel\n");
        return;
        }

    AtChannelDebug((AtChannel)hdlcChannel);
    }

void AtDebugAtPppLink(uint32 linkId)
    {
    AtHdlcChannel hdlcChannel = HdlcChannel(linkId);

    if (hdlcChannel == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel\n");
        return;
        }

    if ((AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmCiscoHdlc) &&
        (AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmPpp))
        {
        AtPrintc(cSevCritical, "ERROR: Not a PPP link\n");
        return;
        }

    AtChannelDebug((AtChannel)AtHdlcChannelHdlcLinkGet(hdlcChannel));
    }

void AtDebugAtMpBundle(uint32 bundleId)
    {
    AtMpBundle bundle = AtModulePppMpBundleGet(PppModule(), bundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid bundle\n");
        return;
        }

    AtChannelDebug((AtChannel)bundle);
    }

void AtDebugAtEthFlow(uint32 flowId)
    {
    AtEthFlow flow = AtModuleEthFlowGet(EthModule(), (uint16)flowId);
    if (flow == NULL)
        {
        AtPrintc(cSevCritical, "ERRROR: Invalid flow\n");
        return;
        }

    AtChannelDebug((AtChannel)flow);
    }

void AtDebugAtEthPort(uint32 portId)
    {
    AtEthPort port = AtModuleEthPortGet(EthModule(), (uint8)portId);
    if (port == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid port\n");
        return;
        }

    AtChannelDebug((AtChannel)port);
    }

void AtDebugAtPdhDe1(uint32 de1Id)
    {
    AtPdhDe1 de1 = AtModulePdhDe1Get(PdhModule(), de1Id);

    if (de1 == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid port\n");
        return;
        }

    AtChannelDebug((AtChannel)de1);
    }

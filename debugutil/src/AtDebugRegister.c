/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : AtDebugRegister.c
 *
 * Created Date: Dec 12, 2012
 *
 * Description : Show register content
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDebug.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtLongRegMaxSize   8
#define cNumRegistersToDump 0x1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ValuePrint(uint32 regValue, uint32 address)
    {
    static uint8 cNumBitsInDword = 32;
    uint32 shiftBit = cBit31;
    uint8 postion;

    /* Print bit values */
    AtPrintc(cSevNormal, "\r\n%08X:  ", address);
    for (postion = cNumBitsInDword; postion > 0; postion--)
        {
        uint32 bitVal = (regValue & shiftBit) >> (postion - 1);
        AtPrintc(bitVal ? cSevInfo : cSevNormal, (postion > 10) ? "%u  " : "%u ", bitVal);
        shiftBit = shiftBit >> cBit0;
        }
    }

static void BitPostionPrint(uint32 registerIndex)
    {
    static const uint8 cNumBitsInDword = 32;
    uint8 postion;

    AtPrintc(cSevNormal, "%s", (registerIndex == 0) ? "Bit#       " : "\r\n\nBit#       ");
    for (postion = 0; postion < cNumBitsInDword; postion++)
        AtPrintc(cSevNormal, "%u ", (cNumBitsInDword - postion) - 1);
    }

static uint8 PrintBitPostionInDword(uint32 registerIndex, uint8 startBitPosition, uint8 numBits)
    {
    uint8 bitIndexOfFirstDword = 0;

    AtPrintc(cSevNormal, "%s", (registerIndex == 0) ? "Bit#    " : "\r\nBit#    ");
    while (startBitPosition < numBits)
        {
        AtPrintc(cSevNormal, "%d ", numBits - bitIndexOfFirstDword - 1);
        startBitPosition++;
        bitIndexOfFirstDword++;
        }
    AtPrintc(cSevNormal, "\r\n");

    return startBitPosition;
    }

static const char *BitPostionToFormatString(uint32 postion, uint8 startIndexInDword, uint8 bitVal)
    {
    static char str[16];
    static uint8 cNumBitIn3Dword = 96;
    static uint8 cNumBitIn2Dword = 64;
    eBool isNumberWith3Digit =  ((cNumBitIn3Dword + postion) > 100) ? cAtTrue : cAtFalse;

    if  (startIndexInDword > cNumBitIn3Dword)
        {
        if (isNumberWith3Digit)
            AtSprintf(str, "%u   ", bitVal);
        else
            AtSprintf(str, "%u  ", bitVal);
        }

    else if ((startIndexInDword >= cNumBitIn2Dword) && (startIndexInDword <= cNumBitIn3Dword))
        AtSprintf(str, "%u  ", bitVal);
    else if (postion > 10)
        AtSprintf(str, "%u  ", bitVal);
    else
        AtSprintf(str, "%u ", bitVal);

    return str;
    }

static void PrintBitValue(uint32 *regValues, uint8 startIndexInDw, uint32 mask, uint8 dword_i)
    {
    uint8 postion;
    static uint8 cNumBitInDword = 32;

    AtPrintc(cSevCritical, "        ");
    for (postion = cNumBitInDword; postion > 0; postion--)
        {
        uint8 bitVal = (uint8)((regValues[dword_i] & mask) >> (postion - 1));
        AtPrintc(bitVal ? cSevInfo : cSevNormal, "%s", BitPostionToFormatString(postion, startIndexInDw, bitVal));
        mask = mask >> cBit0;
        }

    AtPrintc(cSevNormal, "\r\n");
    }

const char *AtDebugLongRegValue2String(const uint32 *regValues, uint16 numDwords)
    {
    static char longRegValueString[150];
    static char dwordString[16];
    int32 i;

    if (numDwords == 0)
        return "None";

    if (numDwords > cAtLongRegMaxSize)
        numDwords = cAtLongRegMaxSize;

    AtSprintf(longRegValueString, "0x");
    for (i = (int16)(numDwords - 1); i >= 0; i--)
        {
        AtSprintf(dwordString, "%08x.", regValues[i]);
        AtStrcat(longRegValueString, dwordString);
        }

    /* Remove the last '.' */
    longRegValueString[AtStrlen(longRegValueString) - 1] = '\0';

    return longRegValueString;
    }


/* To give CPU a rest, if not, it will reboot because of too many print
 * actions */
void AtDebugCpuRest(void)
    {
    static uint32 numLines = 0;
    static const uint16 cNumLinesToHaveRest = 20;
    numLines = numLines + 1;
    if (numLines == cNumLinesToHaveRest)
        {
        AtOsalUSleep(10000);
        numLines = 0;
        }
    }

void AtRegisterPrettyDump(AtHal hal, uint32 startAddress, uint32 stopAddress, uint32 maskValue, uint32 shiftValue)
    {
    static const uint8 cNumContinuousRegisters = 8;
    uint32 register_i = 0;
    uint32 numRegisters = stopAddress - startAddress;

    /* check limit number address to dump */
    if (numRegisters > cNumRegistersToDump)
        numRegisters = cNumRegistersToDump - 1;

    while (register_i <= numRegisters)
        {
        uint32 value = AtHalRead(hal, startAddress);
        value = (value & maskValue) >> shiftValue;

        /* Just 8 continuous registers */
        if ((register_i % cNumContinuousRegisters) == 0)
            BitPostionPrint(register_i);

        /* Print bit values */
        ValuePrint(value, startAddress);
        startAddress++;
        register_i++;
        AtDebugCpuRest();
        }

    AtPrintf("\r\n");
    }

eBool AtRegisterPrettyLongRead(uint32 *regValues, uint16 numDwords)
    {
    uint8 dword_i = 0;
    uint8 numBits = 32;
    uint8 bitIndex = 0;

    while (dword_i < numDwords)
        {
        uint32 mask = cBit31;

        /* Print bit values */
        bitIndex = PrintBitPostionInDword(dword_i, bitIndex, numBits);
        PrintBitValue(regValues, bitIndex, mask, (uint8)dword_i);

        /* Calculate number bit of next dword */
        numBits = (uint8)(numBits + 32);
        dword_i = (uint8)(dword_i + 1);
        }

    return cAtTrue;
    }

void AtDumpByHal(AtHal hal, uint32 startAddress, uint32 stopAddress, uint32 maskValue, uint32 shiftValue)
    {
    static char buf[50];
    uint32 idx = 0;
    uint32 value;
    uint32 numRegisters = stopAddress - startAddress;
    uint32 offsetCount = 0;

    /* check limit number address to dump */
    if (numRegisters > cNumRegistersToDump)
        numRegisters = cNumRegistersToDump - 1;

    while (offsetCount <= numRegisters)
        {
        AtSprintf(buf, "\r\n%08X: ", startAddress);
        AtPrintc(cSevNormal, "%s", buf);
        AtOsalMemInit(buf, 0x0, 50);
        for (idx = 0; (idx < 4) && (offsetCount <= numRegisters); idx++)
            {
            value = AtHalRead(hal, startAddress);
            value = (value & maskValue) >> shiftValue;
            startAddress++;
            offsetCount++;
            AtSprintf(buf, "%08lX   ", (long unsigned int)value);
            AtPrintc(cSevNormal, "%s", buf);
            }

        AtDebugCpuRest();
        }

    AtPrintf("\r\n");
    }

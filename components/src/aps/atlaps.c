/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Linear APS
 *
 * File        : laps.c
 *
 * Created Date: Sep 4, 2010
 *
 * Author      : nnnam
 *
 * Description : Software Linear APS module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "atlaps.h"

/*--------------------------- Define -----------------------------------------*/
#define cLApsNumChnInOneEng 16 /* Number of channels in one engine */
#define cLApsCmpResGreaterThan  1   /* Compare result: greater than */
#define cLApsCmpResEqual        0   /* Compare result: equal */
#define cLApsCmpResLessThan     -1  /* Compare result: less than */

/*--------------------------- Macros -----------------------------------------*/
/* Get database from handle */
#define mLApsDb(module) ((tLApsModule*)module)

/* Check if a line is valid */
#define mLineValid(module_, lineId_) ((lineId_) < (AtLApsMaxNumLinesGet(module_)))
#define mLineInValid(module_, lineId_) (!mLineValid(module_, lineId_))
#define mChnInValid(chnId_) ((chnId_) > cLApsNumChnInOneEng)

/* Check engine information */
#define mArchValid(arch)  (((arch) == cAtLApsArch11) || ((arch) == cAtLApsArch1n))
#define mOpMdValid(opMod) (((opMod) == cAtLApsOpMdUni) || ((opMod) == cAtLApsOpMdBid))
#define mSwTypeValid(swType) (((swType) == cAtLApsSwTypeNonRev) || ((swType) == cAtLApsSwTypeRev))
#define mPrioValid(prio) (((prio) == cAtLApsPriorityLow) || ((prio) == cAtLApsPriorityHigh))

/* Set/clear bit */
#define mBitSet(val, bitMsk) (val) |= (bitMsk)
#define mBitClr(val, bitMsk) (val) = (val) & (uint32)(~(bitMsk))
#define mBitFieldSet(val, fieldMsk, fieldVal) (val) = (((val) & (~(fieldMsk))) | (fieldVal))

/* To check if request code is unused */
#define mReqCodeIsUnused(code) (((code) == 0x9) || ((code) == 0x7) || ((code) == 0x5) || ((code) == 0x3))

/* Check if the input channel can be bridged */
#define mChnCanBeBridged(pEngInfo, chn) (((pEngInfo)->brChn != cAtLApsUnknown) || ((chn) == 0) || mChnInValid(chn)) ? cAtFalse : cAtTrue

/* Check if bridge exists */
#define mEngBrExist(pEngInfo) (!mChnInValid((pEngInfo)->brChn) && ((pEngInfo)->brChn > 0))
#define mLineDefUpdate(curDef, defMsk, defStat) ((curDef) & (~(defMsk))) | (defStat)
#define mReqIsExtlCmd(req) (((req) == cAtLApsReqFS) || ((req) == cAtLApsReqMS) || ((req) == cAtLApsReqEXER) || ((req) == cAtLApsReqLP))
#define mReqIsStateReq(req) (((req) == cAtLApsReqDNR) || ((req) == cAtLApsReqWTR))
#define mLineIsFail(pLineInfo) ((pLineInfo)->defStat & (cAtLApsSwCondLos | cAtLApsSwCondLof | cAtLApsSwCondOof | cAtLApsSwCondAisl))

#define mLineIsInActive(pLineInfo) ((!(pLineInfo)->enable) || ((pLineInfo)->pEngInfo->engState != cAtLApsEngStateStart))

/* Check if switching operations can be performed */
#define mChnCanNotSwitch(pEngInfo, chn) (((pEngInfo)->swChn != cAtLApsUnknown) || ((chn) == 0) || mChnInValid(chn))
#define mEngCanRelSw(pEngInfo) (!mChnInValid((pEngInfo)->swChn) && ((pEngInfo)->swChn > 0))

/* Check if protection line is fail */
#define mPrtLineIsFail(pEngInfo) mLineIsFail(LineInfoInEng(pEngInfo, cAtLApsUnknown, 0))
#define mEngCanMonitorTrouble(pEngInfo) (!(((pEngInfo)->arch == cAtLApsArch11) && ((pEngInfo)->opMd == cAtLApsOpMdUni)))

/* Trouble helpers */
#define mEngCanHaveCM(pEngInfo) (mK1ReqChnGet((pEngInfo)->txK1) != (mK2ReqChnGet((pEngInfo)->rxK2)))
#define mEngCanHaveFEPL(pEngInfo)                                                  \
    (((mK1ReqGet(pEngInfo->rxK1) == cAtLApsReqSFL) ||                            \
      (mK1ReqGet(pEngInfo->rxK1) == cAtLApsReqSFH)) &&                           \
     (mK1ReqChnGet(pEngInfo->rxK1) == 0))
#define mEngSetTrouble(pEngInfo, trouble) (pEngInfo)->curTrbl |= (trouble)
#define mEngClearTrouble(pEngInfo, trouble) (pEngInfo)->curTrbl = (pEngInfo)->curTrbl & (uint32)(~(trouble))
#define mEngHasTrouble(pEngInfo, trouble) ((pEngInfo)->curTrbl & (trouble))
#define mIsFEPL(trouble) ((trouble) & cAtLApsEngTrblFEPL)
#define mIsCM(trouble) ((trouble) & cAtLApsEngTrblCM)
#define mIsPSB(trouble) ((trouble) & cAtLApsEngTrblPSB)

#define Unused(x) (void)(x)

/* Debug macro */
#define cColorRed     31
#define cColorYellow  33
#define cColorGreen   32
#define cColorDefault 0

/*--------------------------- Local Typedefs ---------------------------------*/
/* Timer */
typedef struct tLApsTimer
    {
    uint32 interval; /* Timer interval */
    uint32 counter;  /* Decrease counter to detect timer expires */
    }tLApsTimer;

/* Request information */
typedef struct tLApsReqInfo
    {
    uint8 req;     /* Request type */
    uint8 reqChn;  /* Request channel */
    atbool isLocal; /* Is local request ? */
    }tLApsReqInfo;

/* Engine information */
typedef struct tLApsEngInfo
    {
    void *pSem;
    atbool isValid;    /* To indicate that engine has been provisioned or not */
    uint8 lines[cLApsNumChnInOneEng]; /* List of provisioned lines, index 0 for
                                         protection line and others are working
                                         lines. Channels that have not been
                                         provisioned will have value cAtLApsUnknown */
    uint8 arch;     /* Architecture */
    uint8 opMd;     /* Operation mode */
    uint8 swType;   /* Switching type */
    uint8 engState; /* Engine state */
    uint16 swCond;   /* Switching condition */
    atbool etSupp;   /* Support Extra Traffic */
    uint32 curTrbl;  /* Current trouble status */
    uint8 brChn;    /* Channel that is bridging traffic */
    uint8 swChn;    /* Channel that selects traffic from protection line */
    tLApsReqInfo curReq; /* Current request */

    /* Timers */
    tLApsTimer wtr; /* WTR timer */

    /* K-byte information that are being handled on protection line */
    uint8 rxK1; /* RX K1 */
    uint8 txK1; /* TX K1 */
    uint8 rxK2; /* RX K2 */
    uint8 txK2; /* TX K2 */

    /* For application */
    AtLApsEngBrSwFunc swFunc; /* Switching function */
    AtLApsEngBrSwFunc brFunc; /* Bridging function */
    AtLApsEngKByteSendFunc kByteSendFunc; /* K-byte send function */
    void *pUsrData;    /* User data */
    uint32 usrDataSize; /* Size of use data */

    /* The module that this engine belongs to */
    AtLApsModule module;
    }tLApsEngInfo;

/* Line information */
typedef struct tLApsLineInfo
    {
    tLApsEngInfo *pEngInfo;  /* Information of engine containing this line */
    uint8 chnId;             /* Channel ID of line in engine */
    atbool hiPrio;           /* High priority indication */
    uint32 defStat;          /* Defect status */
    uint8 req;               /* Request corresponding with current defect status */
    atbool enable;           /* Enable/disable this line in engine */
    uint16 swCnt;            /* Switching count */
    }tLApsLineInfo;

/* The whole database */
typedef struct tLApsModule
    {
    /* Semaphore to protect this database */
    void *pSem;

    uint16 maxNumLines;    /* Maximum number of Lines managed by this module */
    tLApsLineInfo *lines;  /* Line configuration */
    tLApsEngInfo *engines; /* Engine configuration */
    uint8 *monLines;       /* Lines that need to be monitored for WTR state or failures */
    uint8 numMonLine;      /* Number of lines need to be monitored */

    /* OS interface */
    tAtLApsOsIntf osIntf;    /* OS interface */
    }tLApsModule;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* The whole database */
/*static*/ tLApsModule m_lapsDb; /* TODO: Open static */

static const char *apsReqStr[]  = {
                                  "NR",
                                  "DNR",
                                  "RR",
                                  "EXER",
                                  "WTR",
                                  "MS",
                                  "SDL",
                                  "SDH",
                                  "SFL",
                                  "SFH",
                                  "FS",
                                  "LP"
                                  };

static uint8 apsReqVal[] = {
                          cAtLApsReqNR,
                          cAtLApsReqDNR,
                          cAtLApsReqRR,
                          cAtLApsReqEXER,
                          cAtLApsReqWTR,
                          cAtLApsReqMS,
                          cAtLApsReqSDL,
                          cAtLApsReqSDH,
                          cAtLApsReqSFL,
                          cAtLApsReqSFH,
                          cAtLApsReqFS,
                          cAtLApsReqLP
                          };

static const char *archStr[] = {
                         "1+1",
                         "1:n"
                         };
static uint8 archVal[] = {
                        cAtLApsArch11,
                        cAtLApsArch1n
                        };

static const char *opModStr[] = {
                          "uni",
                          "bid",
                          "MS-RDI"
                          };
static uint8 opModVal[] = {
                         cAtLApsOpMdUni,
                         cAtLApsOpMdBid,
                         cAtLApsRdilCode
                         };


static const char *swTypeStr[] = {
                           "nonrev",
                           "rev",
                           "MS-RDI"
                           };
static uint8 swTypeVal[] = {
                          cAtLApsSwTypeNonRev,
                          cAtLApsSwTypeRev
                          };
static const char *extlCmdStr[]  = {
                             "CLEAR",
                             "EXER",
                             "MS",
                             "FS",
                             "LP"
                             };
static uint8 extlCmdVal[] = {
                           cAtLApsExtlCmdClear,
                           cAtLApsExtlCmdExer,
                           cAtLApsExtlCmdMs,
                           cAtLApsExtlCmdFs,
                           cAtLApsExtlCmdLp
                           };

/*--------------------------- Forward declarations ---------------------------*/
static __inline__ void EngCurHighestReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo);
static __inline__ void EngServReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo);

/*--------------------------- Implementation ---------------------------------*/
/*
 * Get OS interface of a module
 *
 * @param module Module
 *
 * @return OS interface
 */
static tAtLApsOsIntf *OsIntfGet(AtLApsModule module)
    {
    if (module == NULL)
        return NULL;
    return &(mLApsDb(module)->osIntf);
    }

/* Take semaphore */
static __inline__ eAtLApsRet EngSemTake(tLApsEngInfo *pEngInfo)
    {
    OsIntfGet(pEngInfo->module)->semTake(pEngInfo->pSem);
    return cAtLApsOk;
    }

/* Give semaphore */
static __inline__ eAtLApsRet EngSemGive(tLApsEngInfo *pEngInfo)
    {
    OsIntfGet(pEngInfo->module)->semGive(pEngInfo->pSem);
    return cAtLApsOk;
    }

/*
 * Convert defect to request
 * @param pEngInfo Engine information
 * @pram pLineInfo Line information
 * @param defStat Defect status
 * @return Request corresponding with defect status
 */
static __inline__ uint8 LineDef2Req(tLApsLineInfo *pLineInfo)
    {
    uint32 defStat;

    defStat = pLineInfo->defStat & pLineInfo->pEngInfo->swCond;

    /* SF */
    if (defStat & (cAtLApsSwCondLos | cAtLApsSwCondLof | cAtLApsSwCondOof | cAtLApsSwCondAisl | cAtLApsSwCondBerSf))
        {
        if (pLineInfo->pEngInfo->arch == cAtLApsArch1n)
            return (pLineInfo->hiPrio) ? cAtLApsReqSFH : cAtLApsReqSFL;
        else
            return cAtLApsReqSFL;
        }

    /* SD */
    else if (defStat & cAtLApsSwCondBerSd)
        {
        if (pLineInfo->pEngInfo->arch == cAtLApsArch1n)
            return (pLineInfo->hiPrio) ? cAtLApsReqSDH : cAtLApsReqSDL;
        else
            return cAtLApsReqSDL;
        }

    return cAtLApsReqNR;
    }

/* Check if PSB on K-byte */
static atbool IsPsb(tLApsEngInfo *pEngInfo, uint8 rxK1, uint8 rxK2)
    {
    uint8 req, reqChn, chnId;
    Unused(rxK2);

    req = mK1ReqGet(rxK1);
    reqChn = mK1ReqChnGet(rxK1);

    /* Check if request is valid (check unused code) */
    if (mReqCodeIsUnused(req) ||
        ((req == cAtLApsReqRR) && (pEngInfo->opMd == cAtLApsOpMdUni)) ||
        ((req == cAtLApsReqLP) && (reqChn != 0)))
        return cAtTrue;

    /* No local request but receive RR/WTR/DNR on any channel */
    if ((pEngInfo->curReq.req == cAtLApsReqNR) &&
        ((req == cAtLApsReqRR) || (req == cAtLApsReqDNR) || (req == cAtLApsReqWTR)))
        return cAtTrue;

    /* There may be inconsistency when receiving WTR */
    if (req == cAtLApsReqWTR)
        {
        if ((reqChn == 0) || (pEngInfo->swType != cAtLApsSwTypeRev))
            return cAtTrue;
        }

    /* There may be inconsistency when receiving DNR */
    else if (req == cAtLApsReqDNR)
        {
        if ((reqChn == 0) || (pEngInfo->swType != cAtLApsSwTypeNonRev))
            return cAtTrue;
        }

    /* NR is only applicable for channel 0 */
    if ((req == cAtLApsReqNR) && (reqChn != 0))
        return cAtTrue;

    /* Detect invalid channel number */
    if (pEngInfo->arch == cAtLApsArch11)
        {
        if (reqChn >= 2)
            return cAtTrue;
        }

    /* Scan all 16 channels of engine to detect if requested channel exists */
    else
        {
        for (chnId = 0; chnId < cLApsNumChnInOneEng; chnId++)
            {
            if ((pEngInfo->lines[chnId] != cAtLApsUnknown) && (reqChn == chnId))
                break;
            }
        if (chnId == cLApsNumChnInOneEng)
            return cAtTrue;
        }

    return cAtFalse;
    }

/*
 * Get request information from K-byte
 * @param k1 K1 byte
 * @param k2 K2 byte
 * @param pReqInfo Request information
 * @param pTrblMsk Trouble mask, just to mask corresponding bit in pTrbl to be
 *                 valid. Refer pTrbl parameter for bit position
 * @param pTrbl Trouble happen when converting K-bytes
 *              - cAtLApsEngTrblPSB
 *              - cAtLApsEngTrblCM
 *              - cAtLApsEngTrblMM
 *              - cAtLApsEngTrblFEPL
 */
static __inline__ void KByte2Req(tLApsEngInfo *pEngInfo, uint8 k1, uint8 k2, tLApsReqInfo *pReqInfo, uint32 *pTrblMsk, uint32 *pTrbl)
    {
    uint8 opMod;

    pReqInfo->req     = mK1ReqGet(k1);
    pReqInfo->reqChn  = mK1ReqChnGet(k1);
    pReqInfo->isLocal = cAtFalse;
    *pTrblMsk = (cAtLApsEngTrblPSB | cAtLApsEngTrblMM | cAtLApsEngTrblCM | cAtLApsEngTrblFEPL);
    *pTrbl = 0x0;

    /* Check if request is valid (check unused code) */
    if (IsPsb(pEngInfo, k1, k2))
        *pTrbl |= cAtLApsEngTrblPSB;

    /* Check if operation mode is mismatch */
    opMod = mK2OpMdGet(k2);
    if ((mK2ArchGet(k2) != pEngInfo->arch) ||
        ((opMod != pEngInfo->opMd) && ((opMod == cAtLApsOpMdBid) || (opMod == cAtLApsOpMdUni))))
        *pTrbl |= cAtLApsEngTrblMM;

    /* Channel mismatch */
    if (mK2ReqChnGet(k2) != mK1ReqChnGet(pEngInfo->txK1))
        *pTrbl |= cAtLApsEngTrblCM;

    /* Far-End protection line trouble */
    if (((pReqInfo->req == cAtLApsReqSFH) || (pReqInfo->req == cAtLApsReqSFL)) &&
        (pReqInfo->reqChn == 0))
        *pTrbl |= cAtLApsEngTrblFEPL;
    }

/*
 * Compare two requests
 * @param pReq1 First request
 * @param pReq2 Second request
 *
 * @return 0 Two requests are equal. Equal here is about priority, so two
 *           requests have the same priority do not mean that they are the same,
 *           for example, Local SF#1 is equal with Remote SF#1 but they are not
 *           the same
 * @return 1 Request 1 > request 2
 * @return -1 Request 1 < request 2
 */
static  __inline__ int ReqCmp(tLApsReqInfo *pReq1, tLApsReqInfo *pReq2)
    {
    /* Although SF < FS but SF#cLApsCmpResEqual has higher priority than FS */
    if (pReq1->req != pReq2->req)
        {
        if (((pReq1->req == cAtLApsReqSFL) || (pReq1->req == cAtLApsReqSFH)) && (pReq1->reqChn == 0) && (pReq2->req != cAtLApsReqLP))
            return cLApsCmpResGreaterThan;
        if (((pReq2->req == cAtLApsReqSFL) || (pReq2->req == cAtLApsReqSFH)) && (pReq2->reqChn == 0) && (pReq1->req != cAtLApsReqLP))
            return cLApsCmpResLessThan;
        }

    /* Compare normally for other cases */
    if (pReq1->req > pReq2->req)
        return cLApsCmpResGreaterThan;
    else if (pReq1->req == pReq2->req)
        {
        if (pReq1->reqChn < pReq2->reqChn)
            return cLApsCmpResGreaterThan;
        else if (pReq1->reqChn == pReq2->reqChn)
            return cLApsCmpResEqual;
        else
            return cLApsCmpResLessThan;
        }
    else
        return cLApsCmpResLessThan;
    }

/*
 * Create timer
 * @param pTimer Timer
 * @param interval Interval of timer
 */
static __inline__ void TimerCreate(tLApsTimer *pTimer, uint32 interval)
    {
    pTimer->interval = interval;
    pTimer->counter  = interval;
    }
/*
 * Reload timer
 * @param pTimer Timer
 */
static __inline__  void TimerReload(tLApsTimer *pTimer)
    {
    pTimer->counter = pTimer->interval;
    }

/*
 * Tick the timer
 * @param pTimer Timer
 * @return 0 if timer expires
 * @return Value of decrease counter
 */
static __inline__ uint32 TimerTick(tLApsTimer *pTimer)
    {
    if (pTimer->counter > 0)
        pTimer->counter = pTimer->counter - 1;
    return pTimer->counter;
    }

/*
 * Get line information
 * @param lineId Line ID
 * @return Line information
 */
static __inline__ tLApsLineInfo* LineInfo(AtLApsModule module, uint8 lineId)
    {
    if (!mLineValid(module, lineId))
        return NULL;
    return &(mLApsDb(module)->lines[lineId]);
    }

/*
 * Get line information in database
 * @param engine Engine
 * @param line Line belongs to this engine
 * @param chnId Channel ID of this line in the engine. It is only cared when
 *        the "line" parameter is cAtLApsUnknown
 * @return
 */
static tLApsLineInfo* LineInfoInEng(AtLApsEngine engine, uint8 line, uint8 chnId)
    {
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;

    /* Check information */
    if (engine == NULL)
        return NULL;

    /* Get handle */
    pEngInfo = (tLApsEngInfo*)engine;

    /* Known line */
    if (line != cAtLApsUnknown)
        {
        pLineInfo = LineInfo(pEngInfo->module, line);
        if ((pLineInfo == NULL) || mChnInValid(pLineInfo->chnId))
            return NULL;

        /* Make sure that this line belong to this engine */
        if (pEngInfo->lines[pLineInfo->chnId] != line)
            return NULL;
        else
            return LineInfo(pEngInfo->module, line);
        }

    /* Get configuration by channel ID in engine */
    if (!mChnInValid(chnId))
        return LineInfo(pEngInfo->module, pEngInfo->lines[chnId]);
    else
        return NULL;
    }

/* Check if protection line is fail */
static atbool PrtLineIsFail(tLApsEngInfo *pEngInfo)
    {
    return (LineInfoInEng(pEngInfo, cAtLApsUnknown, 0)->defStat & (cAtLApsSwCondLos | cAtLApsSwCondLof | cAtLApsSwCondOof | cAtLApsSwCondAisl)) ? cAtTrue : cAtFalse;
    }

/*
 * Send K-bytes
 * @param pEngInfo Engine
 * @param k1 TX K1
 * @param k2 TX K2
 */
static __inline__ void EngKByteSend(tLApsEngInfo *pEngInfo, uint8 k1, uint8 k2)
    {
    uint8 txK2;

    /* Update database */
    pEngInfo->txK1 = k1;
    pEngInfo->txK2 = k2;

    /* If protection line has alarm, overwrite K2[6:8] to indicate MS-RDI */
    txK2 = k2;
    if (PrtLineIsFail(pEngInfo))
        mK2OpMdSet(txK2, cAtLApsRdilCode);

    /* Call application interface to send K-bytes */
    pEngInfo->kByteSendFunc(pEngInfo,
                            pEngInfo->lines[0],
                            k1,
                            txK2,
                            pEngInfo->pUsrData,
                            pEngInfo->usrDataSize);
    }

/*
 * Bridge
 * @param pEngInfo Engine information
 * @param chn Channel that its traffic will be bridged to the protection channel
 */
static __inline__ void EngineBridge(tLApsEngInfo *pEngInfo, uint8 chn)
    {
    /* Make sure that the bridge is in release state and the channel is valid */
    if (!mChnCanBeBridged(pEngInfo, chn))
        return;

    /* Bridge traffic and update database */
    pEngInfo->brFunc(pEngInfo,
                     pEngInfo->lines[chn],
                     pEngInfo->lines[0],
                     cAtFalse,
                     pEngInfo->pUsrData,
                     pEngInfo->usrDataSize);
    pEngInfo->brChn = chn;
    }

static __inline__ void HwBridgeRelease(tLApsEngInfo *pEngInfo)
    {
    if (mEngBrExist(pEngInfo))
        pEngInfo->brFunc(pEngInfo,
                         pEngInfo->lines[pEngInfo->brChn],
                         pEngInfo->lines[0],
                         cAtTrue,
                         pEngInfo->pUsrData,
                         pEngInfo->usrDataSize);

    pEngInfo->brChn = cAtLApsUnknown;
    }

/*
 * Release current bridge
 * @param pEngInfo Engine information
 */
static __inline__ void EngineBridgeRelease(tLApsEngInfo *pEngInfo)
    {
    /* In 1+1 architecture, always bridge */
    if (pEngInfo->arch == cAtLApsArch11)
        return;

    /* Just release the bridge and update database */
    HwBridgeRelease(pEngInfo);
    }

/* Create request structure */
static tLApsReqInfo *ReqCreate(uint8 req, uint8 reqChn, atbool isLocal, tLApsReqInfo *pReqInfo)
    {
    pReqInfo->req     = req;
    pReqInfo->isLocal = isLocal;
    pReqInfo->reqChn  = reqChn;
    return pReqInfo;
    }

/* Process defect clear */
static void LineOnDefectClear(tLApsLineInfo *pLineInfo)
    {
    tLApsReqInfo newHiReq;
    uint8 k1, k2;
    tLApsReqInfo *pCurHiReq;
    tLApsEngInfo *pEngInfo;

    /* Check if WTR or DNR need to be activated */
    pEngInfo = pLineInfo->pEngInfo;
    pCurHiReq = &(pEngInfo->curReq);
    if (pCurHiReq->isLocal && (pCurHiReq->reqChn == pLineInfo->chnId) &&
        ((pCurHiReq->req == cAtLApsReqSDL) ||
         (pCurHiReq->req == cAtLApsReqSDH) ||
         (pCurHiReq->req == cAtLApsReqSFL) ||
         (pCurHiReq->req == cAtLApsReqSFH)))
        {
        if (pLineInfo->chnId != 0)
            {
            if (pEngInfo->swType == cAtLApsSwTypeRev)
                pLineInfo->req = cAtLApsReqWTR;
            else
                pLineInfo->req = cAtLApsReqDNR;
            }
        else
            pLineInfo->req = cAtLApsReqNR;

        /* Find new highest request */
        EngCurHighestReq(pEngInfo, &newHiReq);
        if (newHiReq.reqChn != pLineInfo->chnId)
            pLineInfo->req = 0x0;
        EngServReq(pEngInfo, &newHiReq);
        }
    else
        {
        pLineInfo->req = cAtLApsReqNR;

        /* Update bridging state */
        if (pLineInfo->chnId == 0)
            {
            k1 = pEngInfo->txK1;
            k2 = pEngInfo->txK2;

            /* There is remote request on protection channel */
            if (mK1ReqChnGet(pEngInfo->rxK1) == 0)
                {
                mK2ReqChnSet(k2, 0);
                switch (mK1ReqGet(pEngInfo->rxK1))
                    {
                    case cAtLApsReqNR:
                        EngKByteSend(pEngInfo, k1, k2); /* Not to send MS-RDI because now defect is clear */
                        break;
                    case cAtLApsReqRR:
                    case cAtLApsReqMS:
                    case cAtLApsReqFS:
                    case cAtLApsReqSDL:
                    case cAtLApsReqSDH:
                    case cAtLApsReqSFL:
                    case cAtLApsReqSFH:
                    case cAtLApsReqLP:
                        EngKByteSend(pEngInfo, k1, k2);
                        EngineBridgeRelease(pEngInfo);
                        break;
                    case cAtLApsReqEXER:
                        EngKByteSend(pEngInfo, k1, k2);
                        break;
                    default:
                        break;
                    }
                }

            /* Remote request on working channel */
            else
                {
                mK2ReqChnSet(k2, mK1ReqChnGet(pEngInfo->rxK1));
                switch (mK1ReqGet(pEngInfo->rxK1))
                    {
                    case cAtLApsReqMS:
                    case cAtLApsReqFS:
                    case cAtLApsReqSDL:
                    case cAtLApsReqSDH:
                    case cAtLApsReqSFL:
                    case cAtLApsReqSFH:
                    case cAtLApsReqRR:
                        EngineBridgeRelease(pEngInfo);
                        EngineBridge(pEngInfo, mK1ReqGet(pEngInfo->rxK1));
                        EngKByteSend(pEngInfo, k1, k2);
                        break;

                    case cAtLApsReqEXER:
                        EngKByteSend(pEngInfo, k1, k2);
                        break;

                    default:
                        break;
                    }
                }
            }
        }
    }

/* Process defect raise */
static void LineOnDefectChange(tLApsLineInfo *pLineInfo)
    {
    tLApsReqInfo *pCurHiReq;
    tLApsReqInfo newHiReq;
    tLApsReqInfo reqInfo;
    tLApsEngInfo *pEngInfo;

    pEngInfo = pLineInfo->pEngInfo;
    ReqCreate(LineDef2Req(pLineInfo), pLineInfo->chnId, cAtTrue, &reqInfo);
    pCurHiReq = &(pEngInfo->curReq);
    if (reqInfo.req != pLineInfo->req)
        {
        pLineInfo->req = reqInfo.req;

        /* Check if this request is higher than current highest request */
        /* Local request changes on same channel */
        if ((pCurHiReq->isLocal) && (reqInfo.reqChn == pCurHiReq->reqChn))
            {
            /* External command is in affect, compare new request with this external command */
            if (mReqIsExtlCmd(pCurHiReq->req))
                {
                if (ReqCmp(&reqInfo, pCurHiReq) == cLApsCmpResGreaterThan)
                    EngServReq(pEngInfo, &reqInfo);

                /* Although new request is lower, but if protection line has problem, MS-RDI need to be sent */
                else if ((reqInfo.reqChn == 0) && mLineIsFail(pLineInfo))
                    EngKByteSend(pEngInfo, pEngInfo->txK1, pEngInfo->txK2);
                }
            else
                {
                EngCurHighestReq(pEngInfo, &newHiReq);
                if (ReqCmp(&newHiReq, &reqInfo) == cLApsCmpResGreaterThan)
                    EngServReq(pEngInfo, &newHiReq);
                else
                    EngServReq(pEngInfo, &reqInfo);
                }
            }

        /* New local request on different channel, it must be compared with
         * current highest request */
        else if (ReqCmp(&reqInfo, pCurHiReq) == cLApsCmpResGreaterThan)
            {
            /* Clear State request */
            if (mReqIsStateReq(pCurHiReq->req))
                LineInfo(pEngInfo->module, pEngInfo->lines[pCurHiReq->reqChn])->req = cAtLApsReqNR;
            EngServReq(pEngInfo, &reqInfo);
            }

        /* New request on protection channel but it has lower priority than
         * current highest request so it is not severed. But in this case,
         * MS-RDI need to be sent */
        else if (reqInfo.reqChn == 0)
            EngKByteSend(pEngInfo, pEngInfo->txK1, pEngInfo->txK2);
        }
    }

/*
 * Process Line's defect
 */
static __inline__ void LineDefProc(AtLApsModule module, uint8 line, uint32 defMsk, uint32 defStat)
    {
    uint32 newStat;
    tLApsLineInfo *pLineInfo;

    /* Get engine */
    pLineInfo = LineInfo(module, line);

    /* Only process defect when line is enabled and there is a change in defect
     * status */
    defStat &= pLineInfo->pEngInfo->swCond;
    newStat = mLineDefUpdate(pLineInfo->defStat, defMsk, defStat);
    if (newStat == pLineInfo->defStat)
        return;

    /* Update defect status */
    pLineInfo->defStat = newStat;

    /* Just update database if its engine has not been activated */
    if (mLineIsInActive(pLineInfo))
        {
        pLineInfo->req = LineDef2Req(pLineInfo);
        return;
        }

    /* Process defect */
    if (newStat == 0x0)
        LineOnDefectClear(pLineInfo);
    else
        LineOnDefectChange(pLineInfo);
    }

/*
 * Switch
 * @param pEngInfo Engine information
 * @param chn Channel that its traffic will be selected to from protection channel
 */
static __inline__ void EngineSwitch(tLApsEngInfo *pEngInfo, uint8 chn)
    {
    /* Make sure that the switch is in release state and the channel is valid */
    if (mChnCanNotSwitch(pEngInfo, chn))
        return;

    /* Update switching counters */
    LineInfoInEng(pEngInfo, cAtLApsUnknown, chn)->swCnt++;
    LineInfoInEng(pEngInfo, cAtLApsUnknown, 0)->swCnt++;

    /* Switch traffic and update database */
    pEngInfo->swChn = chn;
    pEngInfo->swFunc(pEngInfo,
                     pEngInfo->lines[chn],
                     pEngInfo->lines[0],
                     cAtFalse,
                     pEngInfo->pUsrData,
                     pEngInfo->usrDataSize);
    }

/*
 * Release current switch
 * @param pEngInfo Engine information
 */
static __inline__ void EngineSwitchRelease(tLApsEngInfo *pEngInfo)
    {
    uint8 swChn;

    /* Do nothing if cannot release switch */
    if (!mEngCanRelSw(pEngInfo))
        return;

    /* Update switching counters */
    LineInfoInEng(pEngInfo, cAtLApsUnknown, pEngInfo->swChn)->swCnt++;
    LineInfoInEng(pEngInfo, cAtLApsUnknown, 0)->swCnt++;

    swChn = pEngInfo->swChn;
    pEngInfo->swChn = cAtLApsUnknown;
    pEngInfo->swFunc(pEngInfo,
                     pEngInfo->lines[swChn],
                     pEngInfo->lines[0],
                     cAtTrue,
                     pEngInfo->pUsrData,
                     pEngInfo->usrDataSize);
    }

/* Activate WTR state */
static void WtrActivate(tLApsEngInfo *pEngInfo, uint8 chn)
    {
    tLApsModule *pDb;
    uint8 idx;

    TimerReload(&(pEngInfo->wtr));
    pDb = mLApsDb(pEngInfo->module);
    for (idx = 0; idx < pDb->numMonLine; idx++)
        if (pDb->monLines[idx] == pEngInfo->lines[chn])
            break;
    if (idx == pDb->numMonLine)
        {
        pDb->monLines[pDb->numMonLine] = pEngInfo->lines[chn];
        pDb->numMonLine = (uint8)(pDb->numMonLine + 1);
        }

    /* TODO: Notify application */
    }

/* Serve local request on protection line */
static void PrtLineOnLocalReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    uint8 k1, k2;

    /* Create K-bytes */
    k1 = pEngInfo->txK1;
    k2 = pEngInfo->txK2;
    mK1ReqSet(k1, pReqInfo->req);
    mK1ReqChnSet(k1, pReqInfo->reqChn);

    switch (pReqInfo->req)
        {
        /* Cannot happen */
        case cAtLApsReqRR:
            break;

        /* Activate WTR state */
        case cAtLApsReqWTR:
            WtrActivate(pEngInfo, pReqInfo->reqChn);
            EngKByteSend(pEngInfo, k1, k2);
            break;

        /* DNR, EXER */
        case cAtLApsReqDNR:
        case cAtLApsReqEXER:
            EngKByteSend(pEngInfo, k1, k2);
            break;

        /* No Request */
        case cAtLApsReqNR:
            EngineSwitchRelease(pEngInfo);
            if (pEngInfo->opMd == cAtLApsOpMdBid)
                mK2ReqChnSet(k2, 0);
            EngKByteSend(pEngInfo, k1, k2);
            break;

        /* MS#0 and FS#0 are only applied for 1+1 architecture */
        case cAtLApsReqMS:
        case cAtLApsReqFS:
            if (pEngInfo->arch == cAtLApsArch11)
                {
                EngineSwitchRelease(pEngInfo);
                EngKByteSend(pEngInfo, k1, k2);
                if (pEngInfo->opMd == cAtLApsOpMdBid)
                    {
                    if (mK2ReqChnGet(pEngInfo->rxK2) == 0)
                        mBitClr(pEngInfo->curTrbl, cAtLApsEngTrblCM);
                    else
                        mBitSet(pEngInfo->curTrbl, cAtLApsEngTrblCM);
                    }
                }
            break;

        /* SD/SF, LP*/
        case cAtLApsReqSDL:
        case cAtLApsReqSDH:
        case cAtLApsReqSFL:
        case cAtLApsReqSFH:
        case cAtLApsReqLP:
            EngineSwitchRelease(pEngInfo);
            EngKByteSend(pEngInfo, k1, k2);

            /* Release the bridge when SF#0, bid */
            if ((pEngInfo->opMd == cAtLApsOpMdBid) &&
                ((pReqInfo->req == cAtLApsReqSFH) ||
                 (pReqInfo->req == cAtLApsReqSFL)))
                EngineBridgeRelease(pEngInfo);
            break;

        default:
            break;
        }
    }

/* Serve local request on working line */
static void WrkLineOnLocalReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    uint8 k1, k2;

    /* Create K-bytes */
    k1 = pEngInfo->txK1;
    k2 = pEngInfo->txK2;
    mK1ReqSet(k1, pReqInfo->req);
    mK1ReqChnSet(k1, pReqInfo->reqChn);

    switch (pReqInfo->req)
        {
        /* Cannot happen */
        case cAtLApsReqNR:
        case cAtLApsReqRR:
        case cAtLApsReqLP:
            break;

        /* Automatic requests or external requests */
        case cAtLApsReqMS:
        case cAtLApsReqFS:
        case cAtLApsReqSDL:
        case cAtLApsReqSDH:
        case cAtLApsReqSFL:
        case cAtLApsReqSFH:
            EngKByteSend(pEngInfo, k1, k2);
            if (pEngInfo->arch == cAtLApsArch11)
                EngineSwitch(pEngInfo, 1);
            else
                {
                if (!((LineInfo(pEngInfo->module, pEngInfo->lines[0])->defStat & (cAtLApsSwCondLos | cAtLApsSwCondLof | cAtLApsSwCondOof | cAtLApsSwCondAisl))))
                    {
                    if (mK2ReqChnGet(pEngInfo->rxK2) == pReqInfo->reqChn)
                        {
                        EngineSwitchRelease(pEngInfo);
                        EngineSwitch(pEngInfo, pReqInfo->reqChn);
                        mBitClr(pEngInfo->curTrbl, cAtLApsEngTrblCM);
                        }
                    else
                        {
                        EngineSwitchRelease(pEngInfo);
                        mBitSet(pEngInfo->curTrbl, cAtLApsEngTrblCM);
                        }
                    }

                /* FS has higher priority than SF on protection line, so switch */
                else if (pReqInfo->req == cAtLApsReqFS)
                    {
                    EngineSwitchRelease(pEngInfo);
                    EngineSwitch(pEngInfo, pReqInfo->reqChn);
                    }
                }
            break;

        /* State requests and EXER */
        case cAtLApsReqDNR:
        case cAtLApsReqWTR:
        case cAtLApsReqEXER:
            EngKByteSend(pEngInfo, k1, k2);

            /* Activate WTR */
            WtrActivate(pEngInfo, pReqInfo->reqChn);
            break;

        /* Do not care */
        default:
            break;
        }
    }

/*
 * Serve a new highest local request
 *
 * @param pEngInfo Engine information
 * @param pReqInfo New highest local request
 */
static __inline__ void EngServLocReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    /* Save this request to database */
    AtOsalMemCpy(&(pEngInfo->curReq), pReqInfo, sizeof(tLApsReqInfo));

    /* Local request on protection line */
    if (pReqInfo->reqChn == 0)
        PrtLineOnLocalReq(pEngInfo, pReqInfo);

    /* Local request on working line. */
    else
        WrkLineOnLocalReq(pEngInfo, pReqInfo);

    /* Update defects */
    if (mEngCanMonitorTrouble(pEngInfo) && (!mPrtLineIsFail(pEngInfo)))
        {
        /* Update CM */
        if (mEngCanHaveCM(pEngInfo))
            mEngSetTrouble(pEngInfo, cAtLApsEngTrblCM);
        else
            mEngClearTrouble(pEngInfo, cAtLApsEngTrblCM);

        /* Update FEPL */
        if (mEngCanHaveFEPL(pEngInfo))
            mEngSetTrouble(pEngInfo, cAtLApsEngTrblFEPL);
        else
            mEngClearTrouble(pEngInfo, cAtLApsEngTrblFEPL);
        }
    }

/*
 * Serve a new highest remote request
 * @param pEngInfo Engine information
 * @param pReqInfo New highest remote request
 */
static __inline__ void EngServRemReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    uint8 k1, k2;
    atbool canBrSw;

    /* Only serve remote request when there is alarm on protection line */
    if (mPrtLineIsFail(pEngInfo))
        return;

    /* Create K-bytes */
    k1 = pEngInfo->txK1;
    k2 = pEngInfo->txK2;

    mK1ReqChnSet(k1, pReqInfo->reqChn);
    if (pReqInfo->req != cAtLApsReqRR)
        {
        if (pReqInfo->req != cAtLApsReqNR)
            mK1ReqSet(k1, cAtLApsReqRR);;

        /* Save this request to database */
        AtOsalMemCpy(&(pEngInfo->curReq), pReqInfo, sizeof(tLApsReqInfo));
        }

    /* Remote request on protection line */
    if (pReqInfo->reqChn == 0)
        {
        mK2ReqChnSet(k2, 0);
        switch (pReqInfo->req)
            {
            case cAtLApsReqNR:
            case cAtLApsReqRR:
                if (pReqInfo->req == cAtLApsReqNR)
                    mK1ReqSet(k1, cAtLApsReqNR);
                EngKByteSend(pEngInfo, k1, k2);

                /* Release the bridge */
                EngineBridgeRelease(pEngInfo);

                /* Check if switch needs to be released */
                if ((mK1ReqChnGet(pEngInfo->txK1) == 0) &&
                    (mPrtLineIsFail(pEngInfo) ||
                    (mK2ReqChnGet(pEngInfo->rxK2) == 0)))
                    EngineSwitchRelease(pEngInfo);
                break;

            /* Ignore */
            case cAtLApsReqDNR:
            case cAtLApsReqWTR:
                break;

            case cAtLApsReqMS:
            case cAtLApsReqFS:
            case cAtLApsReqSDL:
            case cAtLApsReqSDH:
            case cAtLApsReqSFL:
            case cAtLApsReqSFH:
            case cAtLApsReqLP:
                EngKByteSend(pEngInfo, k1, k2);

                /* Release bridge */
                EngineBridgeRelease(pEngInfo);

                /* Release switch */
                EngineSwitchRelease(pEngInfo);
                break;

            case cAtLApsReqEXER:
                EngKByteSend(pEngInfo, k1, k2);
                break;

            default:
                return;
            }
        }

    /* Remote request on working line */
    else
        {
        switch (pReqInfo->req)
            {
            /* Cannot happen */
            case cAtLApsReqNR:
                break;

            /* State request */
            case cAtLApsReqDNR:
            case cAtLApsReqWTR:
                mK2ReqChnSet(k2, pReqInfo->reqChn);
                EngKByteSend(pEngInfo, k1, k2);
                break;

            /* Automatic or external request */
            case cAtLApsReqMS:
            case cAtLApsReqFS:
            case cAtLApsReqSDL:
            case cAtLApsReqSDH:
            case cAtLApsReqSFL:
            case cAtLApsReqSFH:
            case cAtLApsReqRR:
                /* Bridge then send K-byte. But in case of EXER, handle K-Byte only */
                canBrSw = cAtTrue;
                if ((pReqInfo->req == cAtLApsReqRR) && (pEngInfo->curReq.req == cAtLApsReqEXER))
                    canBrSw = cAtFalse;

                /* Bridge */
                if (canBrSw && (pEngInfo->brChn != pReqInfo->reqChn))
                    {
                    EngineBridgeRelease(pEngInfo);
                    EngineBridge(pEngInfo, pReqInfo->reqChn);
                    }
                mK2ReqChnSet(k2, pReqInfo->reqChn);
                EngKByteSend(pEngInfo, k1, k2);

                /* Check if switch need to be made */
                if (!canBrSw)
                    break;

                /* In 1+1, switch is allow regardless of channel mismatch */
                if (pEngInfo->arch == cAtLApsArch11)
                    {
                    if ((mK1ReqChnGet(pEngInfo->txK1) == pReqInfo->reqChn) &&
                        (pEngInfo->swChn != pReqInfo->reqChn))
                        {
                        EngineSwitchRelease(pEngInfo);
                        EngineSwitch(pEngInfo, pReqInfo->reqChn);
                        }
                    }

                /* In 1:n, switch is only allowed when channel ID on TX K1 matches that of RX K2 */
                else
                    {
                    if (mK1ReqChnGet(pEngInfo->txK1) == mK2ReqChnGet(pEngInfo->rxK2))
                        {
                        if (pEngInfo->swChn != mK1ReqChnGet(pEngInfo->txK1))
                            {
                            EngineSwitchRelease(pEngInfo);
                            EngineSwitch(pEngInfo, mK1ReqChnGet(pEngInfo->txK1));
                            }
                        }
                    else
                        EngineSwitchRelease(pEngInfo);
                    }
                break;

            case cAtLApsReqEXER:
                mK2ReqChnSet(k2, pReqInfo->reqChn);
                EngKByteSend(pEngInfo, k1, k2);
                break;

            /* Cannot happen */
            case cAtLApsReqLP:
                break;

            /* Do not care */
            default:
                break;
            }
        }
    }

/*
 * Serve a new highest request
 * @param pEngInfo Engine information
 * @param pReqInfo New highest request
 */
static __inline__ void EngServReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    if (pReqInfo->isLocal)
        EngServLocReq(pEngInfo, pReqInfo);
    else
        EngServRemReq(pEngInfo, pReqInfo);
    }

/* Get highest local request */
static tLApsReqInfo *EngHighestLocalReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    tLApsLineInfo *pLineInfo;
    uint8 numChn, idx, line;

    /* Start by NR#0 */
    pReqInfo->req     = cAtLApsReqNR;
    pReqInfo->reqChn  = 0;
    pReqInfo->isLocal = cAtTrue;

    /* Scan requests of all channels */
    numChn = (pEngInfo->arch == cAtLApsArch11) ? 2 : cLApsNumChnInOneEng;
    for (idx = 0; idx < numChn; idx++)
        {
        /* Ignore invalid line */
        line = pEngInfo->lines[idx];
        if (mLineInValid(pEngInfo->module, line))
            continue;

        /* Compare two requests */
        pLineInfo = LineInfo(pEngInfo->module, line);
        if (!pLineInfo->enable)
            continue;
        if (pLineInfo->req > pReqInfo->req)
            {
            pReqInfo->req = pLineInfo->req;
            pReqInfo->reqChn = idx;
            }
        else if ((pLineInfo->req == pReqInfo->req) &&
                 (pLineInfo->chnId < pReqInfo->reqChn))
            pReqInfo->reqChn = pLineInfo->chnId;
        }

    return pReqInfo;
    }

/*
 * Get current highest request
 * @param pEngInfo Engine info
 * @param pReqInfo Current highest request information
 */
static __inline__ void EngCurHighestReq(tLApsEngInfo *pEngInfo, tLApsReqInfo *pReqInfo)
    {
    uint8 req, reqChn;

    /* Get highest local request */
    EngHighestLocalReq(pEngInfo, pReqInfo);

    /* Compare this request with remote request if operation mode is
     * bid-direction. Do this when protection line has no problem */
    if (!((pEngInfo->opMd == cAtLApsOpMdBid) && !mPrtLineIsFail(pEngInfo) && (mK2OpMdGet(pEngInfo->rxK2) != cAtLApsAislCode)))
        return;

    /* Compare remote request with local request */
    req = mK1ReqGet(pEngInfo->rxK1);
    reqChn = mK1ReqChnGet(pEngInfo->rxK1);
    if (req == cAtLApsReqRR)
        return;
    if (req > pReqInfo->req)
        {
        pReqInfo->req     = req;
        pReqInfo->reqChn  = reqChn;
        pReqInfo->isLocal = cAtFalse;
        }
    else if ((req == pReqInfo->req) && (reqChn < pReqInfo->reqChn))
        {
        pReqInfo->reqChn  = reqChn;
        pReqInfo->isLocal = cAtFalse;
        }
    }

/*
 * Process RX K-byte
 *
 * @param pEngInfo Engine information
 * @param k1 RX K1
 * @param k2 RX K2
 */
static __inline__ void EngRxKbyteProc(AtLApsModule module, uint8 line, uint8 k1, uint8 k2)
    {
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;
    tLApsReqInfo reqInfo, newHiReq;
    uint32 trblMsk, trbl;
    uint8 engK2, reqChn, newK2;

    /* Get engine information */
    pLineInfo = LineInfo(module, line);
    pEngInfo = pLineInfo->pEngInfo;

    /* Do nothing if K-bytes are not changed */
    if ((pEngInfo->rxK1 == k1) && (pEngInfo->rxK2 == k2))
        return;

    /* Update K-byte to database */
    pEngInfo->rxK1 = k1;
    pEngInfo->rxK2 = k2;

    /* Also do nothing in these cases:
     * - Engine is inactive
     * - There is alarm on protection line,
     * - RX K2 indicate AIS-L */
    if ((pEngInfo->engState != cAtLApsEngStateStart) || mLineIsFail(pLineInfo) || (mK2OpMdGet(k2) == cAtLApsAislCode))
        return;

    /* Convert K-byte to request */
    KByte2Req(pEngInfo, k1, k2, &reqInfo, &trblMsk, &trbl);

    /* In case of uni-directional, just consider whether bridging indication
     * need to be sent or not */
    if (pEngInfo->opMd == cAtLApsOpMdUni)
        {
        if ((pEngInfo->arch == cAtLApsArch11) || (!mIsPSB(trbl)))
            {
            /* Update bridge */
            engK2 = pEngInfo->txK2;
            mK2ReqChnSet(engK2, reqInfo.reqChn);
            if (reqInfo.reqChn != 0)
                {
                if (pEngInfo->brChn != cAtLApsUnknown)
                    EngineBridgeRelease(pEngInfo);
                EngineBridge(pEngInfo, reqInfo.reqChn);
                }
            else
                EngineBridgeRelease(pEngInfo);

            /* Update switch, make switch when CM is clear or release switch when CM raise */
            if (pEngInfo->arch == cAtLApsArch1n)
                {
                if (!mIsCM(trbl))
                    {
                    reqChn = mK1ReqChnGet(pEngInfo->txK1);
                    if (reqChn != 0)
                        {
                        if (pEngInfo->swChn != cAtLApsUnknown)
                            EngineSwitchRelease(pEngInfo);
                        EngineSwitch(pEngInfo, reqChn);
                        }
                    }
                else if (pEngInfo->swChn != cAtLApsUnknown)
                    EngineSwitchRelease(pEngInfo);
                }

            /* Update K-byte */
            EngKByteSend(pEngInfo, pEngInfo->txK1, engK2);
            }
        }

    /* Process remote request in case of bidirectional */
    else
        {
        if (!mIsPSB(trbl))
            {
            /* State request ==> find a new highest request to serve */
            if ((reqInfo.req == cAtLApsReqNR)  ||
                (reqInfo.req == cAtLApsReqDNR) ||
                (reqInfo.req == cAtLApsReqWTR) ||
                (reqInfo.req == cAtLApsReqRR))
                {
                /* Find new highest request */
                if (reqInfo.req != cAtLApsReqRR)
                    {
                    EngCurHighestReq(pEngInfo, &newHiReq);
                    if (ReqCmp(&newHiReq, &reqInfo) == cLApsCmpResGreaterThan)
                        AtOsalMemCpy(&reqInfo, &newHiReq, sizeof(tLApsReqInfo));
                    }

                EngServReq(pEngInfo, &reqInfo);
                }

            /* Other request (not state request) */
            else
                {
                /* If remote request change, find new highest request */
                if (!pEngInfo->curReq.isLocal)
                    {
                    EngCurHighestReq(pEngInfo, &newHiReq);
                    if (ReqCmp(&newHiReq, &reqInfo) == cLApsCmpResGreaterThan)
                        EngServReq(pEngInfo, &newHiReq);
                    else
                        EngServReq(pEngInfo, &reqInfo);
                    }

                /* Compare remote request with current highest request */
                else if (ReqCmp(&reqInfo, &(pEngInfo->curReq)) == cLApsCmpResGreaterThan)
                    {
                    /* Due to remote request has higher priority. If line is in
                     * Request State, it means that no defect on line, just clear
                     * its state */
                    pLineInfo = LineInfo(module, pEngInfo->lines[pEngInfo->curReq.reqChn]);
                    if ((pLineInfo->req == cAtLApsReqWTR) ||
                        (pLineInfo->req == cAtLApsReqDNR))
                        pLineInfo->req = cAtLApsReqNR;

                    EngServReq(pEngInfo, &reqInfo);
                    }

                /* Request can be lower priority but if it is valid, bridge
                 * needs to be setup if valid channel is indicated on RX K1. And
                 * if there is a match on channel ID field of TX K1 and RX K2,
                 * switching must be made */
                else
                    {
                    /* Update bridge */
                    if (pEngInfo->arch != cAtLApsArch11)
                        {
                        reqChn = mK1ReqChnGet(k1);
                        newK2 = pEngInfo->txK2;
                        if (reqChn == 0)
                            {
                            EngineBridgeRelease(pEngInfo);
                            mK2ReqChnSet(newK2, 0);
                            }
                        else
                            {
                            if (pEngInfo->brChn != reqChn)
                                EngineBridgeRelease(pEngInfo);
                            EngineBridge(pEngInfo, reqChn);
                            mK2ReqChnSet(newK2, reqChn);
                            }

                        /* Let the remote site know about this bridging by updating K2 */
                        EngKByteSend(pEngInfo, pEngInfo->txK1, newK2);
                        }

                    /* Update switch */
                    reqChn = mK1ReqChnGet(pEngInfo->txK1);
                    if (reqChn == mK2ReqChnGet(k2))
                        {
                        if (reqChn != pEngInfo->swChn)
                            {
                            EngineSwitchRelease(pEngInfo);
                            EngineSwitch(pEngInfo, reqChn);
                            }
                        }
                    else
                        EngineSwitchRelease(pEngInfo);
                    }
                }
            }
        }

    /* Update troubles (do not update 1+1 unidirectional engine) */
    if (!((pEngInfo->arch == cAtLApsArch11) && (pEngInfo->opMd == cAtLApsOpMdUni)))
        {
        /* CM can happen after K-bytes are transmitted */
        if ((trblMsk & cAtLApsEngTrblCM) &&
            (mK1ReqChnGet(pEngInfo->txK1) != mK2ReqChnGet(pEngInfo->rxK2)))
            trbl |= cAtLApsEngTrblCM;
        else
            trbl = trbl & (uint32)(~cAtLApsEngTrblCM);

        /* If there is a change in trouble status */
        if ((trbl ^ pEngInfo->curTrbl) & trblMsk)
            {
            /* TODO: Make request to monitor these troubles for failure declaration */
            /* ... */

            /* Update trouble status */
            pEngInfo->curTrbl = (pEngInfo->curTrbl & (~trblMsk)) | trbl;
            }
        }
    }

/* Get a AtOsalMemFree Linear APS engine */
static tLApsEngInfo *EngFreeGet(AtLApsModule module)
    {
    uint8 idx;
    tLApsModule *pDb;

    /* Try to find a AtOsalMemFree engine */
    pDb = mLApsDb(module);
    for (idx = 0; idx < (pDb->maxNumLines / 2); idx++)
        {
        if (!(pDb->engines[idx].isValid))
            return &(pDb->engines[idx]);
        }

    /* Cannot find any engine */
    return NULL;
    }

/*
 * Validate OS interface
 */
static atbool OsIntfIsValid(tAtLApsOsIntf *pOsIntf)
    {
    if ((pOsIntf == NULL) ||
        (pOsIntf->semCreate == NULL) ||
        (pOsIntf->semDelete == NULL) ||
        (pOsIntf->semTake == NULL) ||
        (pOsIntf->semGive == NULL))
        return cAtFalse;

    return cAtTrue;
    }

/**
 * Create Linear APS module
 *
 * @param maxNumLines Maximum number of Lines that this module can supported.
 *        This parameter must be >= 2
 *
 * @return Linear APS module handle on success or NULL on failure (out of memory)
 */
AtLApsModule AtLApsCreate(uint16 maxNumLines, tAtLApsOsIntf *pOsIntf)
    {
    tLApsModule *pDb;
    uint8 idx;
    uint32 size;

    /* Validate OS interface */
    if (!OsIntfIsValid(pOsIntf))
        return NULL;

    /* Cannot create with number of Line < 2 */
    if (maxNumLines < 2)
        return NULL;

    /* Start module ... */
    pDb = AtOsalMemAlloc(sizeof(tLApsModule));
    AtOsalMemInit(pDb, 0, sizeof(tLApsModule));
    if (pDb == NULL)
        return NULL;
    AtOsalMemInit(pDb, 0, sizeof(tLApsModule));

    /* Allocate memory for all Lines */
    size = sizeof(tLApsLineInfo) * maxNumLines;
    pDb->lines = AtOsalMemAlloc(size);
    if (pDb->lines == NULL)
        {
        AtOsalMemFree(pDb);
        return NULL;
        }
    else
        AtOsalMemInit(pDb->lines, 0, size);
    pDb->maxNumLines = maxNumLines;

    size = sizeof(uint8) * maxNumLines;
    pDb->monLines = AtOsalMemAlloc(size);
    if (pDb->monLines == NULL)
        {
        AtOsalMemFree(pDb->lines);
        AtOsalMemFree(pDb);
        return NULL;
        }
    AtOsalMemInit(pDb->monLines, 0, size);
    pDb->numMonLine = 0;

    /* Allocate memory for all engines */
    size = sizeof(tLApsEngInfo) * (maxNumLines / 2);
    pDb->engines = AtOsalMemAlloc(size);
    if (pDb->engines == NULL)
        {
        AtOsalMemFree(pDb->lines);
        AtOsalMemFree(pDb->monLines);
        AtOsalMemFree(pDb);
        return NULL;
        }
    else
        AtOsalMemInit(pDb->engines, 0, size);

    /* Save OS interface */
    AtOsalMemCpy(&(pDb->osIntf), pOsIntf, sizeof(tAtLApsOsIntf));

    /* Initialize Line database */
    for (idx = 0; idx < maxNumLines; idx++)
        LineInfo(pDb, idx)->chnId = cAtLApsUnknown;

    /* Create semaphore to protect database */
    pDb->pSem = pDb->osIntf.semCreate();
    if (pDb->pSem == NULL)
        {
        AtOsalMemFree(pDb->lines);
        AtOsalMemFree(pDb->monLines);
        AtOsalMemFree(pDb->engines);
        AtOsalMemFree(pDb);

        return NULL;
        }

    return pDb;
    }

/**
 * To delete module, AtOsalMemFree all of allocated resources
 *
 * @param module Module to be deleted
 */
void AtLApsDelete(AtLApsModule module)
    {
    tLApsModule *pDb;
    uint8 idx;

    /* Check NULL */
    if (module == NULL)
        return;

    /* Get database */
    pDb = mLApsDb(module);

    /* Delete semaphore */
    for (idx = 0; idx < (pDb->maxNumLines) / 2; idx++)
        AtLApsEngDelete(&(pDb->engines[idx]));
    pDb->osIntf.semDelete(pDb->pSem);

    /* Free database */
    AtOsalMemFree(pDb->lines);
    AtOsalMemFree(pDb->monLines);
    AtOsalMemFree(pDb->engines);
    AtOsalMemFree(pDb);
    }

/**
 * Periodic processing function. This function should be called by the application
 * periodically so that WTR can be processed and L-APS defects and failures can
 * be reported or it can help this module perform switching/bridging action on
 * K-byte failure.
 *
 * @param module Module
 *
 * @retval Number of lines need to be processed.
 * @retval 0 if nothing to do
 */
uint8 AtLApsPeriodicProcess(AtLApsModule module)
    {
    tLApsModule *pDb;
    uint8 idx;
    tLApsLineInfo *pLineInfo;
    tLApsEngInfo *pEngInfo;
    tLApsReqInfo reqInfo;
    uint32 remain;

    /* Ignore on NULL */
    if (module == NULL)
        return 0;

    /* Return if no request */
    pDb = mLApsDb(module);
    if (pDb->numMonLine == 0)
        return 0;

    /* Serve all requesting lines */
    remain = 0;
    for (idx = 0; idx < pDb->numMonLine; idx++)
        {
        pLineInfo = LineInfo(module, pDb->monLines[idx]);
        pEngInfo = pLineInfo->pEngInfo;

        /* This line is in WTR state */
        if ((pLineInfo->req == cAtLApsReqWTR) && (TimerTick(&(pEngInfo->wtr)) == 0))
            {
            reqInfo.isLocal = cAtTrue;
            reqInfo.req     = cAtLApsReqNR;
            reqInfo.reqChn  = 0;
            pLineInfo->req  = cAtLApsReqNR;
            EngServReq(pEngInfo, &reqInfo);
            }

        /* No request on it */
        else
            {
            pDb->monLines[remain] = pDb->monLines[idx];
            remain = remain + 1;
            }

        /* TODO: Process for failures */
        }
    pDb->numMonLine = (uint8)remain;

    return pDb->numMonLine;
    }

/**
 * Get maximum number of Lines that a module can support
 *
 * @param module Linear APS Module
 *
 * @return Maximum number of Lines
 */
uint16 AtLApsMaxNumLinesGet(AtLApsModule module)
    {
    if (module == NULL)
        return 0;
    return mLApsDb(module)->maxNumLines;
    }

/**
 * Create new Linear APS engine
 *
 * @param module Linear APS Module handle
 * @param lines List of lines. One engine can have maximum 16 lines. Channel 0
 *              is used for protection line and remained indexes are used for
 *              working lines
 * @param numLines Number of lines to provision to the engine
 * @param arch @ref eAtLApsArch "Architecture"
 * @param opMod @ref eAtLApsOpMd "Operation mode"
 * @param swType @ref eAtLApsSwType "Switching type".
 * @param wtrTime Wait-To-Restore time (in seconds). Only applicable for
 *                revertive switching mode. Otherwise, it is ignored
 * @param swFunc Application interface for switching action
 * @param brFunc Application interface for bridging action
 * @param kByteSendFunc Application interface for sending K-bytes
 * @param pUsrData User data that this engine will input to application interface
 * @param usrDataSize Size of user data
 *
 * @return Engine handle if success
 * @return NULL if engine cannot be created
 */
AtLApsEngine AtLApsEngCreate(AtLApsModule module,
                         uint8 lines[],
                         uint8 numLines,
                         eAtLApsArch arch,
                         eAtLApsOpMd opMod,
                         eAtLApsSwType swType,
                         uint32 wtrTime,

                         /* Application interface */
                         AtLApsEngBrSwFunc swFunc,
                         AtLApsEngBrSwFunc brFunc,
                         AtLApsEngKByteSendFunc kByteSendFunc,

                         /* User data */
                         void *pUsrData,
                         uint32 usrDataSize)
    {
    uint8 idx;
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;

    /* Do nothing if there is not enough lines to form an engine */
    if ((numLines < 2) || (lines == NULL))
        return NULL;

    /* Cannot create engine with NULL application interfaces */
    if ((swFunc == NULL) || (brFunc == NULL) || (kByteSendFunc == NULL))
        return NULL;

    /* Check lines to make sure that they are all valid and do not belong to any
     * engine */
    for (idx = 0; idx < numLines; idx++)
        {
        if (mLineInValid(module, lines[idx]) || (LineInfo(module, lines[idx])->pEngInfo != NULL))
            return NULL;
        }

    /* Cannot create 1+1 engine with more than 2 lines */
    if ((arch == cAtLApsArch11) && (numLines > 2))
        return NULL;

    /* Check engine configuration */
    if ((arch == cAtLApsArch1n) && (swType == cAtLApsSwTypeNonRev))
        swType = cAtLApsUnknown;
    if (!(mArchValid(arch) && mOpMdValid(opMod) && mSwTypeValid(swType)))
        return NULL;

    /* Make sure that there is no line belong to another engine */
    for (idx = 0; idx < numLines; idx++)
        {
        pLineInfo = LineInfo(module, lines[idx]);
        if (pLineInfo->pEngInfo != NULL)
            return NULL;
        }

    /* Create engine */
    pEngInfo = EngFreeGet(module);
    if (pEngInfo == NULL)
        return NULL;
    AtOsalMemInit(pEngInfo, 0, sizeof(tLApsEngInfo));
    AtOsalMemInit(pEngInfo->lines, cAtLApsUnknown, sizeof(pEngInfo->lines));
    pEngInfo->arch     = arch;
    pEngInfo->opMd     = opMod;
    pEngInfo->swType   = swType;
    pEngInfo->engState = cAtLApsEngStateStop;
    pEngInfo->swCond   = cAtLApsSwCondAll;
    pEngInfo->swChn    = cAtLApsUnknown;
    pEngInfo->brChn    = cAtLApsUnknown;
    pEngInfo->module   = module;
    pEngInfo->curReq.isLocal = cAtTrue;
    mK2OpMdSet(pEngInfo->txK2, opMod);
    mK2ArchSet(pEngInfo->txK2, arch);
    TimerCreate(&(pEngInfo->wtr), wtrTime);
    pEngInfo->pSem = OsIntfGet(pEngInfo->module)->semCreate();
    if (pEngInfo->pSem == NULL)
        return NULL;

    /* Save application interfaces */
    pEngInfo->swFunc = swFunc;
    pEngInfo->brFunc = brFunc;
    pEngInfo->kByteSendFunc = kByteSendFunc;

    /* Copy user's data */
    if ((pUsrData != NULL) && (usrDataSize > 0))
        {
        pEngInfo->pUsrData = AtOsalMemAlloc(usrDataSize);
        if (pEngInfo->pUsrData == NULL)
            return NULL;
        AtOsalMemCpy(pEngInfo->pUsrData, pUsrData, usrDataSize);
        pEngInfo->usrDataSize = usrDataSize;
        }

    /* Reset its line database */
    for (idx = 0; idx < numLines; idx++)
        {
        pLineInfo = LineInfo(module, lines[idx]);
        AtOsalMemInit(pLineInfo, 0, sizeof(tLApsLineInfo));
        pLineInfo->pEngInfo  = pEngInfo;
        pLineInfo->chnId     = idx;
        pLineInfo->enable    = cAtTrue;
        pEngInfo->lines[idx] = lines[idx];
        }
    for (; idx < cLApsNumChnInOneEng; idx++)
        pEngInfo->lines[idx] = cAtLApsUnknown;

    /* Mask engine as used */
    pEngInfo->isValid = cAtTrue;

    return pEngInfo;
    }

/**
 * Delete created engine
 * @param engine The engine will be deleted
 *
 * @return
 */
eAtLApsRet AtLApsEngDelete(AtLApsEngine engine)
    {
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;
    uint8 idx;
    uint8 numChn;

    /* Check engine */
    if (engine == NULL)
        return cAtLApsErrUnknownId;

    /* Get engine handle and check if it is created */
    pEngInfo = (tLApsEngInfo*)engine;
    if (pEngInfo->module == NULL)
        return cAtLApsErrNotProv;

    /* Release bridge */
    HwBridgeRelease(pEngInfo);

    /* Clear line database */
    numChn = (pEngInfo->arch == cAtLApsArch11) ? 2 : cLApsNumChnInOneEng;
    for (idx = 0; idx < numChn; idx++)
        {
        if (pEngInfo->lines[idx] == cAtLApsUnknown)
            continue;
        pLineInfo = LineInfo(pEngInfo->module, pEngInfo->lines[idx]);
        pLineInfo->pEngInfo = NULL;
        pLineInfo->chnId    = cAtLApsUnknown;
        pLineInfo->defStat  = 0x0;
        }

    /* Free OS resource */
    OsIntfGet(pEngInfo->module)->semDelete(pEngInfo->pSem);

    /* Free user data and the whole engine */
    if (pEngInfo->pUsrData != NULL)
        AtOsalMemFree(pEngInfo->pUsrData);
    AtOsalMemInit(pEngInfo, 0, sizeof(tLApsEngInfo));

    return cAtLApsOk;
    }

/**
 * Change application interfaces for an engine
 * @param engine Engine
 * @param swFunc Switching interface. It is ignored if NULL
 * @param brFunc Bridging interface. It is ignored if NULL
 * @param kByteSendFunc K-Byte sending interface. It is ignored if NULL
 * @return
 */
eAtLApsRet AtLApsEngAppIntfSet(AtLApsEngine engine,
                           AtLApsEngBrSwFunc swFunc,
                           AtLApsEngBrSwFunc brFunc,
                           AtLApsEngKByteSendFunc kByteSendFunc)
    {
    tLApsEngInfo *pEngInfo;

    /* Get engine database */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Change interfaces */
    if (swFunc == NULL)
        pEngInfo->swFunc = swFunc;
    if (brFunc == NULL)
        pEngInfo->brFunc = brFunc;
    if (kByteSendFunc == NULL)
        pEngInfo->kByteSendFunc = kByteSendFunc;

    return cAtLApsOk;
    }

/**
 * Set priority for a Line or a channel in an engine
 * @param engine Engine handle
 * @param chnId [0..15] Channel ID of line in the engine.
 * @param priority @ref eAtLApsPriority "Priority"
 *
 * @return
 */
eAtLApsRet AtLApsChnPrioSet(AtLApsEngine engine, uint8 chnId, eAtLApsPriority priority)
    {
    tLApsLineInfo *pLineInfo;
    tLApsReqInfo *pCurReq;

    /* Check handle */
    if (engine == NULL)
        return cAtLApsErrNotProv;

    /* Get line information */
    pLineInfo = LineInfoInEng(engine, cAtLApsUnknown, chnId);
    if (pLineInfo == NULL)
        return cAtLApsErrUnknownId;

    /* Check if priority is valid */
    if (!mPrioValid(priority))
        return cAtLApsErrNotSupport;

    /* Higher priority is not applicable for 1+1 engine */
    if ((pLineInfo->pEngInfo->arch == cAtLApsArch11) && (priority == cAtLApsPriorityHigh))
        return cAtLApsErrNotSupport;

    EngSemTake(pLineInfo->pEngInfo);

    /* Set priority */
    if (pLineInfo->hiPrio != priority)
        {
        pLineInfo->hiPrio = priority;

        /* Update current request */
        pCurReq = &(pLineInfo->pEngInfo->curReq);
        if (pCurReq->isLocal && (pCurReq->reqChn == pLineInfo->chnId) &&
            ((pCurReq->req == cAtLApsReqSFH) || (pCurReq->req == cAtLApsReqSFL) ||
             (pCurReq->req == cAtLApsReqSDH) || (pCurReq->req == cAtLApsReqSDL)))
            {
            if (priority == cAtLApsPriorityLow)
                pCurReq->req = (uint8)(pCurReq->req - 1);
            else
                pCurReq->req = (uint8)(pCurReq->req + 1);
            mK1ReqSet(pLineInfo->pEngInfo->txK1, pCurReq->req);
            EngKByteSend(pLineInfo->pEngInfo, pLineInfo->pEngInfo->txK1, pLineInfo->pEngInfo->txK2);
            }
        }

    EngSemGive(pLineInfo->pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get priority of a Line or a Channel in an engine
 * @param engine Engine handle
 * @param chnId [0..15] Channel ID of line in the engine.
 *
 * @return @ref eAtLApsPriority "Priority" on success or cAtLApsUnknown on failure
 */
eAtLApsPriority AtLApsChnPrioGet(AtLApsEngine engine, uint8 chnId)
    {
    tLApsLineInfo *pLineInfo;

    /* Get line information */
    pLineInfo = LineInfoInEng(engine, cAtLApsUnknown, chnId);
    if (pLineInfo == NULL)
        return cAtLApsUnknown;

    /* Return */
    return pLineInfo->hiPrio;
    }

/**
 * Add a new line to existing engine at a specified channel ID in engine
 * @param engine Engine that has been already created
 * @param line Line that needs to be added to the engine
 * @param chnId [1..15] Channel ID in engine that is assigned for this line
 * @return
 */
eAtLApsRet AtAtLApsEngLineAdd(AtLApsEngine engine, uint8 line, uint8 chnId)
    {
    tLApsLineInfo *pLineInfo;
    tLApsEngInfo *pEngInfo;

    /* Check NULL engine */
    if (engine == NULL)
        return cAtLApsErrNotProv;

    /* Check if line and channel ID are valid */
    pEngInfo = engine;
    if (!mLineValid(pEngInfo->module, line) ||
         mChnInValid(chnId) ||
         ((pLineInfo = LineInfo(pEngInfo->module, line)) == NULL))
        return cAtLApsErrUnknownId;

    /* Cannot add line to 1+1 engine */
    if (pEngInfo->arch == cAtLApsArch11)
        return cAtLApsErrNotSupport;

    /* Make sure that this line does not belong to any engine or channel is in used */
    if ((pLineInfo->pEngInfo != NULL) || (pEngInfo->lines[chnId] != cAtLApsUnknown))
        return cAtLApsErrBusy;

    /* Add it */
    EngSemTake(pEngInfo);
    AtOsalMemInit(pLineInfo, 0, sizeof(tLApsLineInfo));
    pLineInfo->pEngInfo    = pEngInfo;
    pLineInfo->chnId       = chnId;
    pLineInfo->enable      = cAtFalse;
    pEngInfo->lines[chnId] = line;
    EngSemGive(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Delete a line out of an existing engine
 * @param engine The engine that containing the line that needs to be deleted
 * @param line Line that will be deleted from the engine
 * @param chnId [1..15] Working channel ID in engine. This parameter is only
 *              used when the "line" parameter is cAtLApsUnknown
 * @return
 */
eAtLApsRet AtLApsEngLineDelete(AtLApsEngine engine, uint8 line, uint8 chnId)
    {
    tLApsLineInfo *pLineInfo;
    tLApsEngInfo *pEngInfo;

    /* Check NULL engine */
    if (engine == NULL)
        return cAtLApsErrNotProv;

    /* Or line is invalid or it does not belong to this engine */
    pLineInfo = LineInfoInEng(engine, line, chnId);
    if (pLineInfo == NULL)
        return cAtLApsErrNotProv;

    /* Cannot delete channel 0 */
    if (pLineInfo->chnId == 0)
        return cAtLApsErrBusy;

    /* Or cannot delete line if number of lines in engine is 2 */
    if (AtLApsEngNumLineGet(engine) == 2)
        return cAtLApsErrNotSupport;

    /* Delete it */
    pEngInfo = engine;
    pEngInfo->lines[pLineInfo->chnId] = cAtLApsUnknown;
    pLineInfo->pEngInfo = NULL;
    pLineInfo->chnId    = cAtLApsUnknown;
    pLineInfo->defStat  = 0x0;

    return cAtLApsOk;
    }

/**
 * Start Linear APS engine from the begining
 * @param engine
 * @return
 */
eAtLApsRet AtLApsEngStart(AtLApsEngine engine)
    {
    tLApsEngInfo *pEngInfo;
    tLApsReqInfo reqInfo;
    uint8 rxK1, rxK2;

    /* Get engine */
    if (engine == NULL)
        return cAtLApsErrUnknownId;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Only start when engine is in stop state */
    if (pEngInfo->engState != cAtLApsEngStateStop)
        return cAtLApsOk;

    /* Start the engine, bridge traffic in case of 1+1 and then server the
     * highest request */
    pEngInfo->engState = cAtLApsEngStateStart;
    pEngInfo->brChn = cAtLApsUnknown;
    pEngInfo->swChn = cAtLApsUnknown;
    if (pEngInfo->arch == cAtLApsArch11)
        EngineBridge(engine, 1);
    mK2ArchSet(pEngInfo->txK2, pEngInfo->arch);
    mK2OpMdSet(pEngInfo->txK2, pEngInfo->opMd);
    AtOsalMemInit(&(pEngInfo->curReq), 0, sizeof(tLApsReqInfo));
    EngCurHighestReq(pEngInfo, &reqInfo);
    EngServReq(pEngInfo, &reqInfo);

    /* Force K-byte change */
    rxK1 = pEngInfo->rxK1;
    rxK2 = pEngInfo->rxK2;
    pEngInfo->rxK1 = 0;
    pEngInfo->rxK2 = 0;
    EngRxKbyteProc(pEngInfo->module, pEngInfo->lines[0], rxK1, rxK2);

    return cAtLApsOk;
    }

/**
 * Start Linear APS engine with current state
 * @param engine Engine
 * @return
 */
eAtLApsRet AtLApsEngStartWithCurState(AtLApsEngine engine)
    {
    tLApsEngInfo *pEngInfo;

    /* Get engine */
    if (engine == NULL)
        return cAtLApsErrUnknownId;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Only start when engine is in stop state */
    if (pEngInfo->engState != cAtLApsEngStateStop)
        return cAtLApsOk;

    /* Start the engine, bridge traffic in case of 1+1 and then server the
     * highest request */
    pEngInfo->engState = cAtLApsEngStateStart;
    if (pEngInfo->arch == cAtLApsArch11)
        EngineBridge(engine, 1);
    else if (pEngInfo->brChn != cAtLApsUnknown)
        EngineBridge(engine, pEngInfo->brChn);

    /* Switch */
    if (pEngInfo->swChn != cAtLApsUnknown)
        EngineSwitch(engine, pEngInfo->swChn);

    mK2ArchSet(pEngInfo->txK2, pEngInfo->arch);
    mK2OpMdSet(pEngInfo->txK2, pEngInfo->opMd);
    EngKByteSend(pEngInfo, pEngInfo->txK1, pEngInfo->txK2);

    return cAtLApsOk;
    }

/**
 * Stop the engine
 * @param engine
 * @return
 */
eAtLApsRet AtLApsEngStop(AtLApsEngine engine)
    {
    tLApsEngInfo *pEngInfo;

    /* Get engine */
    if (engine == NULL)
        return cAtLApsErrUnknownId;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Stop engine */
    pEngInfo->engState = cAtLApsEngStateStop;

    /* Release current switch or bridges */
    EngineSwitchRelease(pEngInfo);
    EngineBridgeRelease(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get engine state to know whether it is running or stops
 * @param engine Engine handle
 * @return @ref eAtLApsEngState "Engine state" if success or cAtLApsUnknown in case
 *         of failure
 */
eAtLApsEngState AtLApsEngState(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo *)engine)->engState;
    }

/**
 * Copy engine state from source to destination
 * @param src Source engine
 * @param des Destination engine
 * @return
 */
eAtLApsRet AtLApsEngStateCopy(AtLApsEngine src, AtLApsEngine des)
    {
    tLApsEngInfo *pSrcEng, *pDesEng;
    uint8 idx, numChn;
    uint8 stateReq;

    /* Make sure that two engines are valid */
    if ((src == NULL) || (des == NULL))
        return cAtLApsErrNotProv;
    pSrcEng = (tLApsEngInfo*)src;
    pDesEng = (tLApsEngInfo*)des;

    /* Make sure that two engines are in stop state */
    if ((pSrcEng->engState != cAtLApsEngStateStop) || (pDesEng->engState != cAtLApsEngStateStop))
        return cAtLApsErrNotSupport;

    /* Everything is OK, copy */
    AtOsalMemCpy(&(pDesEng->curReq), &(pDesEng->curReq), sizeof(tLApsReqInfo));
    AtOsalMemCpy(&(pDesEng->wtr), &(pDesEng->wtr), sizeof(tLApsTimer));
    pDesEng->rxK1  = pSrcEng->rxK1;
    pDesEng->rxK2  = pSrcEng->rxK2;
    pDesEng->txK1  = pSrcEng->txK1;
    pDesEng->txK2  = pSrcEng->txK2;
    pDesEng->swChn = pSrcEng->swChn;
    pDesEng->brChn = pSrcEng->brChn;

    /* Copy line state request */
    numChn = (pSrcEng->arch == cAtLApsArch11) ? 2 : cLApsNumChnInOneEng;
    for (idx = 0; idx < numChn; idx++)
        {
        stateReq = LineInfoInEng(src, cAtLApsUnknown, idx)->req;
        if ((stateReq == cAtLApsReqDNR) || (stateReq == cAtLApsReqWTR))
            LineInfoInEng(des, cAtLApsUnknown, idx)->req = stateReq;
        }

    return cAtLApsOk;
    }

/**
 * Change switching type
 * @param engine Engine
 * @param swType @ref eAtLApsSwType "Switching type"
 *
 * @return
 */
eAtLApsRet AtLApsEngSwTypeSet(AtLApsEngine engine, eAtLApsSwType swType)
    {
    tLApsEngInfo *pEngInfo;

    if (engine == NULL)
        return cAtLApsErrNotProv;

    /* Check switching type */
    pEngInfo = engine;
    if ((pEngInfo->arch == cAtLApsArch1n) && (swType == cAtLApsSwTypeNonRev))
        swType = cAtLApsUnknown;
    if (!mSwTypeValid(swType))
        return cAtLApsErrNotSupport;

    /* Update database */
    EngSemTake(pEngInfo);
    pEngInfo->swType = swType;
    EngSemGive(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get switching type
 * @param engine Engine
 * @return @ref eAtLApsSwType "Switching type" on success or cAtLApsUnknown if fail
 */
eAtLApsSwType AtLApsEngSwTypeGet(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->swType;
    }

/**
 * Change operation mode
 * @param engine Engine
 * @param opMd @ref eAtLApsOpMd "Operation mode"
 * @return
 */
eAtLApsRet AtLApsEngOpMdSet(AtLApsEngine engine, eAtLApsOpMd opMd)
    {
    tLApsEngInfo *pEngInfo;

    /* Check engine handle and operation mode */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    if (!mOpMdValid(opMd))
        return cAtLApsErrNotSupport;

    /* Return if nothing change */
    pEngInfo = engine;
    if (pEngInfo->opMd == opMd)
        return cAtLApsOk;

    /* Update database and reflect on TX K-bytes */
    EngSemTake(pEngInfo);
    pEngInfo->opMd = opMd;
    mK2OpMdSet(pEngInfo->txK2, opMd);
    if (pEngInfo->engState == cAtLApsEngStateStart)
        EngKByteSend(pEngInfo, pEngInfo->txK1, pEngInfo->txK2);
    EngSemGive(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get operation mode
 * @param engine Engine
 * @return @ref eAtLApsOpMd "Operation mode"
 */
eAtLApsOpMd AtLApsEngOpMdGet(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->opMd;
    }

/**
 * Change architecture
 * @param engine Engine
 * @param arch @ref eAtLApsArch "Architecture"
 * @return
 */
eAtLApsRet AtLApsEngArchSet(AtLApsEngine engine, eAtLApsArch arch)
    {
    tLApsEngInfo *pEngInfo;

    /* Check engine handle and architecture */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    if (!mArchValid(arch))
        return cAtLApsErrNotSupport;

    /* Return if nothing change */
    pEngInfo = engine;
    if (arch == pEngInfo->arch)
        return cAtLApsOk;

    /* Cannot change from 1:n to 1+1 if n > 2 */
    if ((pEngInfo->arch == cAtLApsArch1n) && (AtLApsEngNumLineGet(engine) > 2))
        return cAtLApsErrNotSupport;

    /* Update database */
    EngSemTake(pEngInfo);
    pEngInfo->arch = arch;
    mK2ArchSet(pEngInfo->txK2, arch);
    if (pEngInfo->engState == cAtLApsEngStateStart)
        EngKByteSend(pEngInfo, pEngInfo->txK1, pEngInfo->txK2);
    EngSemGive(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get architecture
 * @param engine Engine
 * @return @ref eAtLApsArch "Architecture" if success or cAtLApsUnknown if fail
 */
eAtLApsArch AtLApsEngArchGet(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->arch;
    }

/**
 * Issue external command
 * @param engine Engine
 * @param extlCmd @ref eAtLApsExtlCmd "External command"
 * @param line The line in engine that will be affected by this command. If its
 *             values is cAtLApsUnknown, it is ignored and the parameter chn will
 *             be considered to determine what line will be affected
 * @param chn The channel in engine will be affected by this command. It is only
 *            considered when the input line is invalid (cAtLApsUnknown)
 * @return
 */
eAtLApsRet AtLApsEngExtlCmdSet(AtLApsEngine engine, eAtLApsExtlCmd extlCmd, uint8 line, uint8 chn)
    {
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;
    tLApsReqInfo reqInfo, *pCurHiReq;
    atbool newReq;
    int cmpRes;

    /* Get engine and line information */
    if (engine == NULL)
        return cAtLApsErrUnknownId;
    pEngInfo = (tLApsEngInfo*)engine;
    if (extlCmd == cAtLApsExtlCmdClear)
        pLineInfo = LineInfoInEng(engine, cAtLApsUnknown, 0);
    else
        pLineInfo = LineInfoInEng(engine, line, chn);
    if (pLineInfo == NULL)
        return cAtLApsErrUnknownId;
    pCurHiReq = &(pEngInfo->curReq);

    /* Create request */
    switch (extlCmd)
        {
        /* CLEAR */
        case cAtLApsExtlCmdClear:
            if (pCurHiReq->isLocal)
                {
                newReq = cAtFalse;

                /* Clear existing command */
                if ((pCurHiReq->req == cAtLApsExtlCmdFs) ||
                    (pCurHiReq->req == cAtLApsExtlCmdLp) ||
                    (pCurHiReq->req == cAtLApsExtlCmdMs) ||
                    (pCurHiReq->req == cAtLApsExtlCmdExer))
                    {
                    if (pCurHiReq->reqChn != 0)
                        {
                        pLineInfo = LineInfo(pEngInfo->module, pEngInfo->lines[pCurHiReq->reqChn]);
                        if (!pLineInfo->defStat)
                            {
                            if ((pEngInfo->swType == cAtLApsSwTypeNonRev) && (pCurHiReq->req != cAtLApsExtlCmdExer))
                                pLineInfo->req = cAtLApsReqDNR;
                            else
                                pLineInfo->req = cAtLApsReqNR;
                            }
                        else
                            pLineInfo->req = LineDef2Req(pLineInfo);
                        }
                    else
                        {
                        pLineInfo->req = cAtLApsReqNR;

                        /* If switching is in active, activate DNR */
                        if (!mChnInValid(pEngInfo->swChn) && (pEngInfo->swChn > 0))
                            {
                            pLineInfo = LineInfoInEng(pEngInfo, cAtLApsUnknown, pEngInfo->swChn);
                            if (!pLineInfo->defStat)
                                {
                                if (pEngInfo->swType == cAtLApsSwTypeNonRev)
                                    pLineInfo->req = cAtLApsReqDNR;
                                else
                                    pLineInfo->req = cAtLApsReqWTR;
                                }
                            else
                                pLineInfo->req = LineDef2Req(pLineInfo);
                            }
                        }

                    newReq = cAtTrue;
                    }

                /* Clear WTR state */
                else if (pCurHiReq->req == cAtLApsReqWTR)
                    {
                    pLineInfo->req = cAtLApsReqNR;
                    newReq = cAtTrue;
                    }

                /* New request */
                if (newReq)
                    {
                    EngCurHighestReq(pEngInfo, &reqInfo);

                    /* Clear existing state request */
                    if (pEngInfo->swChn != reqInfo.reqChn)
                        {
                        pLineInfo = LineInfoInEng(pEngInfo, cAtLApsUnknown, pEngInfo->swChn);
                        if ((pLineInfo != NULL) &&
                            ((pLineInfo->req == cAtLApsReqDNR) || (pLineInfo->req == cAtLApsReqWTR)))
                            pLineInfo->req = cAtLApsReqNR;
                        }

                    /* And serve new highest request */
                    EngServReq(pEngInfo, &reqInfo);
                    }
                }

            break;

        /* EXER/FS/MS channel 0 are only applicable on 1+1 architecture */
        case cAtLApsExtlCmdExer:
        case cAtLApsExtlCmdMs:
        case cAtLApsExtlCmdFs:
        case cAtLApsExtlCmdLp:
            if (((extlCmd == cAtLApsExtlCmdExer) ||
                 (extlCmd == cAtLApsExtlCmdMs) ||
                 (extlCmd == cAtLApsExtlCmdFs)) &&
                ((pEngInfo->arch == cAtLApsArch1n) && (pLineInfo->chnId == 0)))
                return cAtLApsErrNotSupport;

            /* LP is only applicable for channel 0 */
            if ((extlCmd == cAtLApsExtlCmdLp) && (pLineInfo->chnId != 0))
                return cAtLApsErrNotSupport;

            reqInfo.isLocal = cAtTrue;
            reqInfo.req     = extlCmd;
            reqInfo.reqChn  = pLineInfo->chnId;
            cmpRes = ReqCmp(&reqInfo, pCurHiReq);
            if (cmpRes == cLApsCmpResLessThan)
                return cAtLApsErrLowPrio;
            else if (cmpRes == cLApsCmpResGreaterThan)
                {
                /* Clear State request for existing channel. Then serve the new
                 * highest request */
                if ((pCurHiReq->isLocal) && ((pCurHiReq->req == cAtLApsReqDNR) || (pCurHiReq->req == cAtLApsReqWTR)))
                    LineInfoInEng(pEngInfo, cAtLApsUnknown, pCurHiReq->reqChn)->req = cAtLApsReqNR;
                EngServLocReq(pEngInfo, &reqInfo);
                }
            else
                pLineInfo->req = extlCmd;
            break;

        /* Unknown command */
        default:
            return cAtLApsErrUnknownExtlCmd;
        }

    return cAtLApsOk;
    }

/**
 * Get external command that is active and the line (or channel) is affected
 * @param [in] engine Engine
 * @param [out] pAffLine Affected line. It can be NULL and ignored in this case
 * @param [out] pAffChn Affected channel. It can be NULL and ignored in this case
 *
 * @return @ref eAtLApsExtlCmd "External command" on success or cAtLApsUnknown if fail
 */
eAtLApsExtlCmd AtLApsEngExtlCmdGet(AtLApsEngine engine, uint8 *pAffLine, uint8 *pAffChn)
    {
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;

    /* Get engine information */
    if (engine == NULL)
        return cAtLApsUnknown;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Return external command */
    if ((pEngInfo->curReq.req == cAtLApsExtlCmdFs) ||
        (pEngInfo->curReq.req == cAtLApsExtlCmdLp) ||
        (pEngInfo->curReq.req == cAtLApsExtlCmdMs) ||
        (pEngInfo->curReq.req == cAtLApsExtlCmdExer))
        {
        if (pEngInfo->curReq.isLocal)
            {
            if (pAffChn != NULL)
                *pAffChn = pEngInfo->curReq.reqChn;
            if (pAffLine != NULL)
                *pAffLine = pEngInfo->lines[pEngInfo->curReq.reqChn];
            return pEngInfo->curReq.req;
            }

        /* Maybe local external command has the same priority with remote
         * external command but it is not served. Also consider it */
        else
            {
            pLineInfo = LineInfoInEng(pEngInfo, cAtLApsUnknown, pEngInfo->curReq.reqChn);
            if (pLineInfo->req == pEngInfo->curReq.req)
                {
                if (pAffChn != NULL)
                    *pAffChn = pLineInfo->chnId;
                if (pAffLine != NULL)
                    *pAffLine = pEngInfo->lines[pLineInfo->chnId];
                return pLineInfo->req;
                }
            }
        }

    /* CLEAR#0 */
    if (pAffChn != NULL)
        *pAffChn = 0;
    if (pAffLine != NULL)
        *pAffLine = pEngInfo->lines[0];

    return cAtLApsExtlCmdClear;
    }

/**
 * Get line ID in engine by its channel ID in engine
 * @param engine Engine
 * @param chnId Channel ID in engine
 * @return Line ID in engine
 * @return cAtLApsUnknown if engine has not been created or input channel ID is
 *         invalid
 */
uint8 AtLApsLineFromChnGet(AtLApsEngine engine, uint8 chnId)
    {
    if ((engine == NULL) || mChnInValid(chnId))
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->lines[chnId];
    }

/**
 * Get number of provisioned lines in an engine
 * @param engine Engine
 * @return Number of provisioned lines
 */
uint8 AtLApsEngNumLineGet(AtLApsEngine engine)
    {
    tLApsEngInfo *pEngInfo;
    uint8 numLines, idx;

    /* Get engine */
    if (engine == NULL)
        return 0;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Get number of provisioned lines */
    numLines = 0;
    for (idx = 0; idx < cLApsNumChnInOneEng; idx++)
        {
        if (pEngInfo->lines[idx] != cAtLApsUnknown)
            numLines = (uint8)(numLines + 1);
        }

    return numLines;
    }

/**
 * Get RX K1 byte on protection line
 * @param engine
 * @return RX K1
 * @return cAtLApsUnknown if engine is invalid
 */
uint8 AtLApsEngRxK1Get(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->rxK1;
    }

/**
 * Get RX K2 byte on protection line
 * @param engine
 * @return RX K2
 * @return cAtLApsUnknown if engine is invalid
 */
uint8 AtLApsEngRxK2Get(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->rxK2;
    }

/**
 * Get TX K1 byte on protection line
 * @param engine
 * @return TX K1
 * @return cAtLApsUnknown if engine is invalid
 */
uint8 AtLApsEngTxK1Get(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->txK1;
    }

/**
 * Set TX K1 byte on protection line. Note, K-byte cannot be changed when engine
 * is running, so this API will return error in this case
 * @param engine
 * @return TX K1
 * @return
 */
eAtLApsRet AtLApsEngTxK1Set(AtLApsEngine engine, uint8 k1)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    if (((tLApsEngInfo*)engine)->engState == cAtLApsEngStateStart)
        return cAtLApsErrNotSupport;

    ((tLApsEngInfo*)engine)->txK1 = k1;

    return cAtLApsOk;
    }

/**
 * Get TX K2 byte on protection line
 * @param engine
 * @return TX K2
 * @return cAtLApsUnknown if engine is invalid
 */
uint8 AtLApsEngTxK2Get(AtLApsEngine engine)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    return ((tLApsEngInfo*)engine)->txK2;
    }

/**
 * Set TX K2 byte on protection line. Note, K-byte cannot be changed when engine
 * is running, so this API will return error in this case
 * @param engine
 * @return TX K2
 * @return
 */
eAtLApsRet AtLApsEngTxK2Set(AtLApsEngine engine, uint8 k2)
    {
    if (engine == NULL)
        return cAtLApsUnknown;
    if (((tLApsEngInfo*)engine)->engState == cAtLApsEngStateStart)
        return cAtLApsErrNotSupport;

    ((tLApsEngInfo*)engine)->txK2 = k2;

    return cAtLApsOk;
    }

/**
 * Get WTR time
 * @param engine
 * @return WTR time in seconds of engine
 */
uint32 AtLApsEngWtrGet(AtLApsEngine engine)
    {
    if ((engine == NULL) || (AtLApsEngSwTypeGet(engine) == cAtLApsSwTypeNonRev))
        return 0;
    return ((tLApsEngInfo*)engine)->wtr.interval;
    }

/**
 * Set WTR time for an engine
 * @param engine Engine
 * @param wtr WTR time in seconds
 * @return
 */
eAtLApsRet AtLApsEngWtrSet(AtLApsEngine engine, uint32 wtr)
    {
    /* Check if engine is provisioned */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    if (AtLApsEngSwTypeGet(engine) == cAtLApsSwTypeNonRev)
        return cAtLApsErrNotSupport;

    ((tLApsEngInfo*)engine)->wtr.interval = wtr;
    return cAtLApsOk;
    }

/**
 * Get current switching channel
 * @param engine Engine
 * @return cAtLApsUnknown If engine has not been created or there is no switching
 *                      channel.
 * @return [1..14] The current switch channel
 */
uint8 AtLApsEngSwChnGet(AtLApsEngine engine)
    {
    /* Engine has not been created */
    if (engine == NULL)
        return cAtLApsUnknown;

    /* Return the switching channel */
    return ((tLApsEngInfo*)engine)->swChn;
    }

/**
 * Get current bridging channel
 * @param engine Engine
 * @return cAtLApsUnknown If engine has not been created or there is no bridging
 *                      channel.
 * @return [1..14] The current bridged channel
 */
uint8 AtLApsEngBrChnGet(AtLApsEngine engine)
    {
    /* Engine has not been created */
    if (engine == NULL)
        return cAtLApsUnknown;

    /* Return the switching channel */
    return ((tLApsEngInfo*)engine)->brChn;
    }

/**
 * Get current highest request that is being served
 * @param [in] engine Engine
 * @param [out] pReq Request
 * @param [out] pReqChn Request channel
 * @param [out] pIsLocal Local request indication
 *        - cAtTrue: local request
 *        - cAtFalse: remote request
 * @return
 * @note The output parameters can be NULL
 */
eAtLApsRet AtLApsEngCurHigReq(AtLApsEngine engine, eAtLApsReq *pReq, uint8 *pReqChn, atbool *pIsLocal)
    {
    tLApsEngInfo *pEngInfo;
    tLApsReqInfo *pCurHiReq;

    /* Get engine handle */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Return current highest request information */
    pCurHiReq = &(pEngInfo->curReq);
    if (pReq != NULL)
        *pReq = pCurHiReq->req;
    if (pReqChn != NULL)
        *pReqChn = pCurHiReq->reqChn;
    if (pIsLocal != NULL)
        *pIsLocal = pCurHiReq->isLocal;

    return cAtLApsOk;
    }

/**
 * Set engine's user data
 * @param engine Engine
 * @param pUsrData User data
 * @param usrSize Size of user data
 * @return
 */
eAtLApsRet AtLApsEngUsrDataSet(AtLApsEngine engine, void *pUsrData, uint32 usrSize)
    {
    tLApsEngInfo *pEngInfo;

    /* Get engine */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    pEngInfo = (tLApsEngInfo*)engine;

    /* Free old user data block */
    if (pEngInfo->pUsrData != NULL)
        {
        AtOsalMemFree(pEngInfo->pUsrData);
        pEngInfo->pUsrData = NULL;
        pEngInfo->usrDataSize = 0;
        }

    /* Do nothing if input user data is NULL */
    if ((pUsrData == NULL) || (usrSize == 0))
        return cAtLApsOk;

    /* Allocate memory to save it */
    pEngInfo->pUsrData = AtOsalMemAlloc(usrSize) ;
    if (pEngInfo->pUsrData == NULL)
        return cAtLApsErrOsRscAlloc;
    AtOsalMemCpy(pEngInfo->pUsrData, pUsrData, usrSize);
    pEngInfo->usrDataSize = usrSize;

    return cAtLApsOk;
    }

/**
 * Get engine's user data
 * @param [in] engine Engine
 * @param [out] pUsrSize Size of user data
 * @return user data. Note, it can be NULL if engine is not provisioned or
 * provisioned without user data
 */
void *AtLApsEngUsrDataGet(AtLApsEngine engine, uint32 *pUsrSize)
    {
    /* NULL engine */
    if (engine == NULL)
        return NULL;

    /* Return user data */
    if (pUsrSize != NULL)
        *pUsrSize = ((tLApsEngInfo*)engine)->usrDataSize;

    return ((tLApsEngInfo*)engine)->pUsrData;
    }

/**
 * Notify the engine about the K-byte change event on protection line
 *
 * @param engine Engine
 * @param k1 RX K1 on protection line
 * @param k2 RX K2 on protection line
 *
 * @return
 */
eAtLApsRet AtLApsEngKByteChange(AtLApsEngine engine, uint8 k1, uint8 k2)
    {
    tLApsEngInfo *pEngInfo;

    /* Not provision */
    if (engine == NULL)
        return cAtLApsErrNotProv;

    /* Only process when K-byte is really changed */
    pEngInfo = (tLApsEngInfo*)engine;
    if ((k1 == pEngInfo->rxK1) && (k2 == pEngInfo->rxK2))
        return cAtLApsOk;

    /* Relay to process K-byte in Linear APS task context */
    EngSemTake(pEngInfo);
    EngRxKbyteProc(pEngInfo->module, pEngInfo->lines[0], k1, k2);
    EngSemGive(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get engine troubles
 *
 * @param engine Engine
 *
 * @return @ref eAtLApsEngTrbl "Engine troubles"
 */
eAtLApsEngTrbl AtLApsEngTrblGet(AtLApsEngine engine)
    {
    /* NULL engine has no trouble */
    if (engine == NULL)
        return cAtLApsEngTrblNone;

    return ((tLApsEngInfo *)engine)->curTrbl;
    }

/**
 * Enable a line in an engine.
 * @param module Module that this line belongs to
 * @param line Line ID
 *
 * @return
 */
eAtLApsRet AtLApsLineEnable(AtLApsModule module, uint8 line)
    {
    tLApsLineInfo *pLineInfo;

    /* Get line information */
    pLineInfo = LineInfo(module, line);
    if ((pLineInfo == NULL) || (pLineInfo->pEngInfo == NULL))
        return cAtLApsErrNotProv;
    if (pLineInfo->enable)
        return cAtLApsOk;

    /* TODO: Handle enable/disable case */
    EngSemTake(pLineInfo->pEngInfo);
    pLineInfo->enable = cAtTrue;
    EngSemGive(pLineInfo->pEngInfo);

    return cAtLApsErrNotSupport;
    }

/**
 * Disable a line in an engine.
 *
 * @param module Module that this line belongs to
 * @param line Line ID
 *
 * @return
 */
eAtLApsRet AtLApsLineDisable(AtLApsModule module, uint8 line)
    {
    tLApsLineInfo *pLineInfo;

    /* Get line information */
    pLineInfo = LineInfo(module, line);
    if ((pLineInfo == NULL) || (pLineInfo->pEngInfo == NULL))
        return cAtLApsErrNotProv;
    if (!pLineInfo->enable)
        return cAtLApsOk;

    /* TODO: Handle enable/disable case */
    EngSemTake(pLineInfo->pEngInfo);
    pLineInfo->enable = cAtFalse;
    EngSemGive(pLineInfo->pEngInfo);

    return cAtLApsOk;
    }

/**
 * Check if a line is enabled in engine
 *
 * @param module Module that this line belongs to
 * @param line Line ID
 * @return cAtTrue if line is enabled in engine
 * @return cAtFalse if line is disabled in engine
 */
atbool AtLApsLineIsEnable(AtLApsModule module, uint8 line)
    {
    tLApsLineInfo *pLineInfo;

    /* Get line information */
    pLineInfo = LineInfo(module, line);
    if (pLineInfo == NULL)
        return cAtFalse;

    return pLineInfo->enable;
    }

/**
 * Get engine handle that contains this line
 * @param lineId Line ID
 * @return Handle of engine that contains this line
 * @return NULL if this line does not belong to any engine
 */
AtLApsEngine AtLApsLineEngGet(AtLApsModule module, uint8 lineId)
    {
    tLApsLineInfo *pLineInfo;

    /* Check if this line is already provisioned */
    pLineInfo = LineInfo(module, lineId);
    if (pLineInfo == NULL)
        return NULL;
    else
        return pLineInfo->pEngInfo;
    }

/**
 * Get channel of Line in engine
 * @param module Module that this line belong to
 * @param lineId Line ID
 * @return cAtLApsUnknown if fail
 */
uint8 AtLApsLineChnGet(AtLApsModule module, uint8 lineId)
    {
    tLApsLineInfo *pLineInfo;

    /* Get line information */
    if (module == NULL)
        return cAtLApsUnknown;
    pLineInfo = LineInfo(module, lineId);
    if (pLineInfo == NULL)
        return cAtLApsUnknown;
    return pLineInfo->chnId;
    }

/**
 * Notify Line's defect or user defined defect
 *
 * @param line Line. Value >= cLApsUsrDefType to indicate user defined defect
 * @param defMsk Defect mask. Refer
 *        - cAtLApsSwCondLos   : LOS
 *        - cAtLApsSwCondLof   : LOF
 *        - cAtLApsSwCondOof   : OOF
 *        - cAtLApsSwCondTims  : TIM-S
 *        - cAtLApsSwCondAisl  : AIS-L
 *        - cAtLApsSwCondBerSf : BER-SF
 *        - cAtLApsSwCondBerSd : BER-SD
 * @param defStat Defect status. Refer
 *        - cAtLApsSwCondLos   : LOS
 *        - cAtLApsSwCondLof   : LOF
 *        - cAtLApsSwCondOof   : OOF
 *        - cAtLApsSwCondTims  : TIM-S
 *        - cAtLApsSwCondAisl  : AIS-L
 *        - cAtLApsSwCondBerSf : BER-SF
 *        - cAtLApsSwCondBerSd : BER-SD
 *
 * @retval cLApsErrNotProv If Line is invalid
 * @retval cLApsOk Success
 */
eAtLApsRet AtLApsLineDefNotify(AtLApsModule module, uint8 line, uint32 defMsk, uint32 defStat)
    {
    tLApsLineInfo *pLineInfo;

    /* Check if line is valid */
    pLineInfo = LineInfo(module, line);
    if ((pLineInfo == NULL) || (pLineInfo->pEngInfo == NULL))
        return cAtLApsErrNotProv;

    EngSemTake(pLineInfo->pEngInfo);
    LineDefProc(module, line, defMsk, defStat);
    EngSemGive(pLineInfo->pEngInfo);

    return cAtLApsOk;
    }

/**
 * Report K-Byte change event along with the current value of K1 and K2 to this
 * module
 * @param line Line
 * @param k1 K1 value
 * @param k2 K2 value
 * @return
 */
eAtLApsRet AtLApsLineKByteChange(AtLApsModule module, uint8 line, uint8 k1, uint8 k2)
    {
    tLApsEngInfo *pEngInfo;
    tLApsLineInfo *pLineInfo;

    /* Check line ID */
    if (mLineInValid(module, line))
        return cAtLApsErrUnknownId;

    /* Get engine */
    pLineInfo = LineInfo(module, line);
    if ((pLineInfo == NULL) || (pLineInfo->pEngInfo == NULL))
        return cAtLApsErrNotProv;
    if ((pLineInfo->chnId != 0))
        return cAtLApsOk;
    pEngInfo = pLineInfo->pEngInfo;
    if ((k1 == pEngInfo->rxK1) && (k2 == pEngInfo->rxK2))
        return cAtLApsOk;

    EngSemTake(pEngInfo);
    EngRxKbyteProc(module, line, k1, k2);
    EngSemGive(pEngInfo);

    return cAtLApsOk;
    }

/**
 * Get channel defect status
 *
 * @param engine Engine that this channel belongs to
 * @param chn [0..15] Channel ID
 * @return Defect status
 * @note In case of failure happens, the return value is also 0x0, this means
 *       that no defect
 */
uint32 AtLApsChnDefStat(AtLApsEngine engine, uint8 chn)
    {
    tLApsLineInfo *pLineInfo;

    /* Check NULL module */
    if (engine == NULL)
        return 0;

    /* Get line information */
    pLineInfo = LineInfoInEng(engine, cAtLApsUnknown, chn);
    if (pLineInfo == NULL)
        return 0x0;

    return pLineInfo->defStat;
    }

/**
 * Get line defect status
 *
 * @param module Module that this line belongs to
 * @param line Line ID.
 * @return Defect status
 * @note In case of failure happens, the return value is also 0x0, this means
 *       that no defect
 */
uint32 AtLApsLineDefStat(AtLApsModule module, uint8 line)
    {
    tLApsLineInfo *pLineInfo;

    /* Check NULL module */
    if (module == NULL)
        return 0;

    /* Get line information */
    pLineInfo = LineInfo(module, line);
    if (pLineInfo == NULL)
        return 0x0;

    return pLineInfo->defStat;
    }

/**
 * Get line switching count
 *
 * @param engine Engine
 * @param line Line ID
 *
 * @return Line switching count
 */
uint16 AtLApsLineSwCnt(AtLApsEngine engine, uint8 line)
    {
    tLApsLineInfo *pLineInfo;

    /* Get Line database */
    pLineInfo = LineInfoInEng(engine, line, cAtLApsUnknown);
    if (pLineInfo == NULL)
        return 0;

    return pLineInfo->swCnt;
    }

/**
 * Notify defect status for a channel in engine
 * @param engine Engine
 * @param chnId [0..15] Channel ID
 * @param defMsk Defect mask
 * @param defStat Defect status
 * @param relay Relay defect to process at LAPS task context
 * @return
 */
eAtLApsRet AtLApsChnDefNotify(AtLApsEngine engine, uint8 chnId, uint32 defMsk, uint32 defStat)
    {
    tLApsLineInfo *pLineInfo;
    tLApsEngInfo *pEngInfo;

    /* Check parameters  */
    if (engine == NULL)
        return cAtLApsErrNotProv;
    if (mChnInValid(chnId))
        return cAtLApsErrUnknownId;
    pLineInfo = LineInfoInEng(engine, cAtLApsUnknown, chnId);
    if (pLineInfo == NULL)
        return cAtLApsErrUnknownId;

    EngSemTake(pLineInfo->pEngInfo);
    pEngInfo = engine;
    LineDefProc(pEngInfo->module, pEngInfo->lines[chnId], defMsk, defStat);
    EngSemGive(pLineInfo->pEngInfo);

    return cAtLApsOk;
    }

/*
 * =============================================================================
 * Debugging functions
 * =============================================================================
 */
/* Get string that represents input value */
static const char *StrGet(uint8 *pValTab, const char **pStrTab, uint8 tableLen, uint8 val)
    {
    uint8 idx;
    for (idx = 0; idx < tableLen; idx++)
        {
        if (pValTab[idx] == val)
            return pStrTab[idx];
        }
    return "Unknown";
    }

const char *LApsReqStr(uint8 reqVal)
    {
    return StrGet(apsReqVal, apsReqStr, mCount(apsReqVal), reqVal);
    }

const char *LApsArchStr(uint8 arch)
    {
    return StrGet(archVal, archStr, mCount(archVal), arch);
    }

const char *LApsOpMdStr(uint8 opMd)
    {
    return StrGet(opModVal, opModStr, mCount(opModVal), opMd);
    }

const char *LApsSwTypeStr(uint8 swType)
    {
    return StrGet(swTypeVal, swTypeStr, mCount(swTypeVal), swType);
    }

const char *LApsExtCmdStr(uint8 extCmd)
    {
    return StrGet(extlCmdVal, extlCmdStr, mCount(extlCmdVal), extCmd);
    }

static char *DefStr(uint32 def)
    {
    static char buf[64];

    buf[0] = '\0';

    /* All defects */
    if (def == cAtLApsSwCondAll)
        AtSprintf(buf, "ALL");
    else
        {
        if (def & cAtLApsSwCondLos)   AtSprintf(buf, "%s%s|", buf, "LOS");
        if (def & cAtLApsSwCondLof)   AtSprintf(buf, "%s%s|", buf, "LOF");
        if (def & cAtLApsSwCondOof)   AtSprintf(buf, "%s%s|", buf, "OOF");
        if (def & cAtLApsSwCondAisl)  AtSprintf(buf, "%s%s|", buf, "MS-AIS");
        if (def & cAtLApsSwCondBerSf) AtSprintf(buf, "%s%s|", buf, "BER-SF");
        if (def & cAtLApsSwCondBerSd) AtSprintf(buf, "%s%s|", buf, "BER-SD");
        if (def & cAtLApsSwCondTims)  AtSprintf(buf, "%s%s|", buf, "TIM-S");

        /* Remove the last '|' character */
        if (def != 0)
            buf[AtStrlen(buf) - 1] = '\0';
        else
            AtSprintf(buf, "None");
        }

    return buf;
    }

/* Translate K1 to string */
const char *LApsK1ToStr(uint8 k1)
    {
    static char str[16];

    AtSprintf(str, "0x%02x: %s#%d", k1, LApsReqStr(mK1ReqGet(k1)), mK1ReqChnGet(k1));
    return str;
    }

const char *LApsK2ToStr(uint8 k2)
    {
    static char str[32];

    str[0] = '\0';
    AtSprintf(str, "0x%02x: %s, %s, BR Chn: %d", k2, LApsArchStr(mK2ArchGet(k2)), LApsOpMdStr(mK2OpMdGet(k2)), mK2ReqChnGet(k2));
    return str;
    }

/* Show engine information */
void LApsEngShow(AtLApsModule module, uint8 line)
    {
    AtLApsEngine engHdl;
    uint8 affChn;
    eAtLApsReq req;
    uint8 reqChn, isLocal;
    uint8 idx, lineId;
    tLApsLineInfo *pLineInfo;
    eAtLApsEngTrbl trbl;
    char buf[32];

    /* Get engine handle */
    engHdl = AtLApsLineEngGet(module, line);
    if (engHdl == NULL)
        {
        AtPrintf("Line %d does not belong to any engine\n", line);
        return;
        }

    AtPrintf("Arch    : %s\n", LApsArchStr(AtLApsEngArchGet(engHdl)));
    AtPrintf("SwType  : %s\n", LApsSwTypeStr(AtLApsEngSwTypeGet(engHdl)));
    AtPrintf("OpMod   : %s\n", LApsOpMdStr(AtLApsEngOpMdGet(engHdl)));
    AtLApsEngExtlCmdGet(engHdl, NULL, &affChn);
    AtPrintf("ExtCmd  : %s#%d\n", LApsExtCmdStr(AtLApsEngExtlCmdGet(engHdl, NULL, NULL)), affChn);
    AtPrintf("SwChn   : %d\n", AtLApsEngSwChnGet(engHdl));
    AtPrintf("BrChn   : %d\n", AtLApsEngBrChnGet(engHdl));
    if (AtLApsEngSwTypeGet(engHdl) == cAtLApsSwTypeRev)
        AtPrintf("WTR     : TimerVal %u, curDownCount = %u\n", AtLApsEngWtrGet(engHdl), ((tLApsEngInfo*)engHdl)->wtr.counter);

    /* TX K1, K2 */
    AtPrintf("TX K1   : %s\n", LApsK1ToStr(AtLApsEngTxK1Get(engHdl)));
    AtPrintf("TX K2   : %s\n", LApsK2ToStr(AtLApsEngTxK2Get(engHdl)));

    /* RX K1, K2 */
    if (AtLApsChnDefStat(engHdl, 0) & ((cAtLApsSwCondLos | cAtLApsSwCondLof | cAtLApsSwCondOof | cAtLApsSwCondAisl)))
        {
        AtPrintf("RX K1   : (*)%s\n", LApsK1ToStr(AtLApsEngRxK1Get(engHdl)));
        AtPrintf("RX K2   : (*)%s\n", LApsK2ToStr(AtLApsEngRxK2Get(engHdl)));
        }
    else
        {
        AtPrintf("RX K1   : %s\n", LApsK1ToStr(AtLApsEngRxK1Get(engHdl)));
        AtPrintf("RX K2   : %s\n", LApsK2ToStr(AtLApsEngRxK2Get(engHdl)));
        }

    /* Engine state */
    AtPrintf("EngState: %s\n", (AtLApsEngState(engHdl) == cAtLApsEngStateStart) ? "Start" : "Stop");

    /* Current request */
    AtLApsEngCurHigReq(engHdl, &req, &reqChn, &isLocal);
    AtPrintf("CurReq  : %s %s#%d\n", isLocal ? "Local" : "Remote", LApsReqStr(req), reqChn);

    /* Troubles */
    buf[0] = '\0';
    trbl = AtLApsEngTrblGet(engHdl);
    if (trbl == cAtLApsEngTrblNone)
        AtSprintf(buf, "None");
    else
        {
        if (trbl & cAtLApsEngTrblPSB)
            AtStrcat(buf, "PSB|");
        if (trbl & cAtLApsEngTrblCM)
            AtStrcat(buf, "CM|");
        if (trbl & cAtLApsEngTrblMM)
            AtStrcat(buf, "MM|");
        if (trbl & cAtLApsEngTrblFEPL)
            AtStrcat(buf, "FEPL|");
        buf[AtStrlen(buf) - 1] = '\0';
        }
    AtPrintf("CurTrbl : %s\n", buf);

    /* Show information of all lines */
    AtPrintf("ChnStat : \n");
    for (idx = 0; idx < cLApsNumChnInOneEng; idx++)
        {
        lineId = AtLApsLineFromChnGet(engHdl, idx);
        if (lineId == cAtLApsUnknown)
            continue;
        pLineInfo = LineInfo(module, lineId);
        if (idx == 0)
            AtPrintf("   - Prot[%02d, Line %d]: ", idx, lineId);
        else
            AtPrintf("   - Work[%02d, Line %d]: ", idx, lineId);
        AtPrintf("Defect - %s, Priority - %s , CurReq - %s, State - %s, swCnt - %d\n",
               DefStr(pLineInfo->defStat), pLineInfo->hiPrio ? "High" : "Low",
               LApsReqStr(pLineInfo->req),
               pLineInfo->enable ? "enable" : "disable", pLineInfo->swCnt);
        }
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UPSR
 *
 * File        : upsr.c
 *
 * Created Date: Aug 20, 2010
 *
 * Author      : nnnam
 *
 * Description : UPSR implementation.
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "atupsr.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtUpsrNumTuInTug2      4
#define cAtUpsrNumTug2InAu3Tug3 7
#define cAtUpsrNumAu3Tug3InAug1 3
#define cAtUpsrNumAug1InAug4    4
#define cAtUpsrNumAug4InAug16   4

/* Unused value */
#define cAtUpsrNotMap  0

/* Module states */
#define cAtAtUpsrModuleStop  0 /* Module stopped */
#define cAtAtUpsrModuleStart 1 /* Module started */

/*--------------------------- Macros -----------------------------------------*/
/* Debug macro */
#define cColorRed     31
#define cColorYellow  33
#define cColorGreen   32
#define cColorDefault 0

#define mUpsrSemTake(module) ((tUpsrDb*)module)->osIntf.semTake(((tUpsrDb*)module)->sem)
#define mUpsrSemGive(module) ((tUpsrDb*)module)->osIntf.semGive(((tUpsrDb*)module)->sem)

/* Get AUG ID from AUG-1 */
#define mAug4FromAug1(aug1no) (aug1no / 4)

/* Update TU defect */
#define mTuDefUpd(pEngine, port, augno, au3Tu3no, tug2no, tuno, defMsk, def, pCxInfo)   \
    EngDefUpd(pEngine, port, augno, au3Tu3no, tug2no, tuno,           \
              (pCxInfo)->pairPort,                                         \
              (pCxInfo)->pairAugno,                                        \
              (pCxInfo)->pairAuno,                                         \
              (pCxInfo)->pairTug2no,                                       \
              (pCxInfo)->pairTuno,                                         \
              (pCxInfo)->isWrk,                                            \
              (pCxInfo)->pEngDb->funcSw,                                   \
              (pCxInfo)->pEngDb->pUsrData,                                 \
              (pCxInfo)->pEngDb->usrDataSize,                              \
              defMsk, def)

/* Macros to check mapping type */
#define mAug16MapTypeIsValid(mapType)                                          \
    (((mapType) == cAtUpsrAug16MapTypeNone)    ||                                \
     ((mapType) == cAtUpsrAug16MapTypeVc4_16c) ||                                \
     ((mapType) == cAtUpsrAug16MapTypeAug4))
#define mAug4MapTypeIsValid(mapType)                                           \
    (((mapType) == cAtUpsrAug4MapTypeNone)   ||                                  \
     ((mapType) == cAtUpsrAug4MapTypeVc4_4c) ||                                  \
     ((mapType) == cAtUpsrAug4MapTypeAug1))
#define mAug1MapTypeIsValid(mapType)                                           \
    (((mapType) == cAtUpsrAug1MapTypeNone) ||                                    \
     ((mapType) == cAtUpsrAug1MapTypeVc4)  ||                                    \
     ((mapType) == cAtUpsrAug1MapTypeTug3) ||                                    \
     ((mapType) == cAtUpsrAug1MapTypeAu3))
#define mAu3Tug3MapTypeIsValid(mapType)                                        \
    (((mapType) == cAtUpsrAu3Tug3MapTypeNone) ||                                 \
     ((mapType) == cAtUpsrAu3Tug3MapTypeVc3)  ||                                 \
     ((mapType) == cAtUpsrAu3Tug3MapTypeTug2))
#define mTug2MapTypeIsValid(mapType)                                           \
    (((mapType) == cAtUpsrTug2MapTypeNone) ||                                    \
     ((mapType) == cAtUpsrTug2MapTypeTu12) ||                                    \
     ((mapType) == cAtUpsrTug2MapTypeTu11))

/* Get number of TU in TUG-2 base on TUG-2 mapping type */
#define mNumTu(tug2Type)                                                       \
    (((tug2Type) == cAtUpsrTug2MapTypeTu12) ? 3 : (((tug2Type) == cAtUpsrTug2MapTypeTu11) ? 4 : 0))

#define Unused(x) (void)(x)

/*--------------------------- Database creation ------------------------------*/
typedef eAtUpsrRet (*UpsrEngTraverseFunc)(AtUpsrEngine engine);

/* AU types */
typedef enum eAtUpsrAuType
    {
    cAtUpsrAuTypeAu4_16c = cAtUpsrEngTypeAu4_16c, /* AU-4-16c */
    cAtUpsrAuTypeAu4_4c  = cAtUpsrEngTypeAu4_4c,  /* AU-4-4c */
    cAtUpsrAuTypeAu4     = cAtUpsrEngTypeAu4,     /* AU-4 */
    cAtUpsrAuTypeAu3     = cAtUpsrEngTypeAu3      /* AU-3 */
    }eAtUpsrAuType;

/* TU types */
typedef enum eAtUpsrTuType
    {
    cAtUpsrTuTypeTu3  = cAtUpsrEngTypeTu3,   /* TU-3 */
    cAtUpsrTuTypeTu12 = cAtUpsrEngTypeTu12, /* TU-12 */
    cAtUpsrTuTypeTu11 = cAtUpsrEngTypeTu11  /* TU-11 */
    }eAtUpsrTuType;

/*
 * Engine information
 */
typedef struct tAtUpsrEngInfo
    {
    uint8 engState; /**< Engine state */
    uint8 extlCmd;  /**< Active external command */
    uint32 swCond;  /**< Switching condition */
    uint8 swType;   /**< Switching type */
    uint32 wtrTime; /**< Wait-To-Restore time */
    uint32 wrkDef;  /**< Defect status of working channel */
    uint32 prtDef;  /**< Defect status of protection channel */
    uint8 swReason; /**< Switching reason. It stores the reason of the latest
                        switching action */
    }tAtUpsrEngInfo;

/* Engine database */
typedef struct tUpsrEngDb
    {
    tAtUpsrEngInfo engInfo;
    AtUpsrEngSwFunc funcSw;  /* Switching function */
    void *pUsrData;        /* User data */
    uint32 usrDataSize;    /* Size of user data */
    AtUpsrModule module;     /* Module that this engine belongs to */

    /* Just need to save the working channel for fast lookup */
    uint8 w_port;
    uint8 w_augno;
    uint8 w_au3Tug3no;
    uint8 w_tug2no;
    uint8 w_tuno;

    /* High-order engine indication */
    atbool isHo;
    }tUpsrEngDb;

/* High-order Cx (C4/C3) information */
typedef struct tUpsrHoCxInfo
    {
    /* Working indication */
    uint8 isWrk;

    /* Other channel */
    uint8 pairPort;
    uint8 pairAugno;
    uint8 pairAuno;

    /* Engine information. Both working and protection channels point to the
     * same address */
    tUpsrEngDb *pEngDb;
    }tUpsrHoCxInfo;

/* Lo-order Cx (C11/C12) information */
typedef struct tUpsrLoCxInfo
    {
    /* Working indication */
    uint8 isWrk;

    /* Other channel */
    uint8 pairPort;
    uint8 pairAugno;
    uint8 pairAuno;
    uint8 pairTug2no;
    uint8 pairTuno;

    /* Provision indication */
    atbool isProv;

    /* Engine information. This field only exists for working channel. For
     * protection channel, it is NULL */
    tUpsrEngDb *pEngDb;
    }tUpsrLoCxInfo;

/* TUG-2 database */
typedef struct tUpsrTug2Db
    {
    uint8 mapType;

    uint8 numProvTu; /* Number of provisioned TUs */
    uint8 provTu[cAtUpsrNumTuInTug2]; /* Provisioned TUs */
    tUpsrLoCxInfo cxInfo[cAtUpsrNumTuInTug2]; /* Database of each TU */
    }tUpsrTug2Db;

/* AU-3/TUG-3 database */
typedef struct tUpsrAu3Tug3Db
    {
    uint8 mapType;

    union uUpsrAu3Tug3Db
        {
        /* 7xTUG-2 mapping */
        struct tUpsrTug2Info
            {
            uint8 numProvTug2; /* Number of TUG-2s containing provisioned channels */
            uint8 provTug2[cAtUpsrNumTug2InAu3Tug3]; /* TUG-2s that contain provisioned channels */
            tUpsrTug2Db tug2[cAtUpsrNumTug2InAu3Tug3]; /* Database of each TUG-2 */
            }tug2Info;

        /* 1xVC-3 mapping */
        tUpsrHoCxInfo cxInfo;
        }mapInfo;
    }tUpsrAu3Tug3Db;

/* AUG-1 database */
typedef struct tUpsrAug1Db
    {
    uint8 mapType;

    union uUpsrAugDb
        {
        /* 3xAU-3/3xTUG-3 mapping */
        struct tUpsrAu3Tug3Info
            {
            uint8 numProvAu3Tug3; /* Number of AU-3s/TUG-3s containing provisioned channels */
            uint8 provAu3Tug3[cAtUpsrNumAu3Tug3InAug1]; /* AU-3s/TUG-3s that contain provisioned channels */
            tUpsrAu3Tug3Db au3Tug3[cAtUpsrNumAu3Tug3InAug1]; /* Database of each AU-3/TUG-3 */
            }au3Tug3;

        /* 1xVC-4 mapping */
        tUpsrHoCxInfo cxInfo;
        }mapInfo;
    }tUpsrAug1Db;

/* AUG-4 database */
typedef struct tUpsrAug4Db
    {
    uint8 mapType;

    union uUpsrAug4Db
        {
        /* 4xAUG-1s */
        struct tUpsrAug1Info
            {
            uint8 numProvAug1; /* Number of AUG-1s containing provisioned channels */
            uint8 provAug1[cAtUpsrNumAug1InAug4]; /* AUG-1s that contain provisioned channels */
            tUpsrAug1Db aug1[cAtUpsrNumAug1InAug4]; /* Database of each AUG-1 */
            }aug1;

        /* 1xVC4-4c mapping */
        tUpsrHoCxInfo cxInfo;
        }mapInfo;
    }tUpsrAug4Db;

/* AUG-16 database */
typedef struct tUpsrAug16Db
    {
    uint8 mapType;

    union uUpsrAug16Db
        {
        /* 4xAUG-4s */
        struct tUpsrAug4Info
            {
            uint8 numProvAug4; /* Number of AUG-4s containing provisioned channels */
            uint8 provAug4[cAtUpsrNumAug4InAug16]; /* AUG-1s that contain provisioned channels */
            tUpsrAug4Db aug4[cAtUpsrNumAug4InAug16]; /* Database of each AUG-4 */
            }aug4;

        /* 1xVC4-16c mapping */
        tUpsrHoCxInfo cxInfo;
        }mapInfo;
    }tUpsrAug16Db;

/* Line database */
typedef struct tUpsrLineDb
    {
    tUpsrAug16Db aug16; /* AUG-16 */
    }tUpsrLineDb;

/* UPSR database */
typedef struct tUpsrDb
    {
    /* OS interface */
    tUpsrOsIntf osIntf;

    /* Semaphore to protect this database */
    void *sem;

    /* Line database */
    uint8 maxNumLine;
    uint8 numProvLine;
    uint8 *provLine;
    tUpsrLineDb *lineDb;

    /* Lock state */
    atbool isLock;
    }tUpsrDb;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define mPortIsValid(pModule, port) ((port) < (pModule)->maxNumLine)

/*
 * To remove item out of array
 *
 * @param [in/out] array Array
 * @param [in/out] len Length of array. It is new length of array on return
 * @param [in] item Item need to be removed
 *
 * @return
 */
static atbool ArrayRemoveItem(uint8 *array, uint8 *len, uint8 item)
    {
    uint8 idx;

    /* Search index if this item */
    for (idx = 0; idx < *len; idx++)
        {
        if (array[idx] == item)
            break;
        }

    /* Remove it (if found) */
    if (idx >= *len)
        return cAtFalse;
    for (; idx < (*len - 1); idx++)
        array[idx] = array[idx + 1];
    array[idx] = cAtUpsrUnknown;
    *len = (uint8)(*len - 1);

    return cAtTrue;
    }

/* To check if an array contains an item */
static atbool ArrayContainItem(uint8 *array, uint8 len, uint8 item)
    {
    uint8 idx;

    for (idx = 0; idx < len; idx++)
        {
        if (array[idx] == item)
            return cAtTrue;
        }

    return cAtFalse;
    }

/* Get Line database */
static tUpsrLineDb *LineDb(tUpsrDb *pModule, uint8 port)
    {
    if (!mPortIsValid(pModule, port))
        return NULL;

    return &(pModule->lineDb[port]);
    }

/* Get AUG-16 database */
static tUpsrAug16Db *Aug16Db(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrLineDb *pLineDb;
    Unused(augno);

    /* Get line database */
    if ((pLineDb = LineDb(pModule, port)) == NULL)
        return NULL;
    return &(pLineDb->aug16);
    }

/* Get AUG-4 database */
static tUpsrAug4Db *Aug4Db(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug16Db *pAug16Db;

    /* Get AUG-16 database and check AUG-4 ID */
    if (((pAug16Db = Aug16Db(pModule, port, 0)) == NULL) ||
        (pAug16Db->mapType != cAtUpsrAug16MapTypeAug4) ||
        (augno >= cAtUpsrNumAug4InAug16))
        return NULL;

    return &(pAug16Db->mapInfo.aug4.aug4[augno]);
    }

/* Get AUG1 database */
static tUpsrAug1Db *Aug1Db(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug4Db *pAug4Db;

    pAug4Db = Aug4Db(pModule, port, mAug4FromAug1(augno));
    if ((pAug4Db == NULL) ||
        (pAug4Db->mapType != cAtUpsrAug4MapTypeAug1))
        return NULL;

    return &(pAug4Db->mapInfo.aug1.aug1[augno % cAtUpsrNumAug1InAug4]);
    }

/* Get AU-3 database */
static tUpsrAu3Tug3Db *Au3Db(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrAug1Db *pAugDb;

    /* Get AUG DB */
    pAugDb = Aug1Db(pModule, port, augno);
    if ((pAugDb == NULL) ||
        (pAugDb->mapType != cAtUpsrAug1MapTypeAu3) ||
        (auno >= cAtUpsrNumAu3Tug3InAug1))
        return NULL;

    /* Return database */
    return &(pAugDb->mapInfo.au3Tug3.au3Tug3[auno]);
    }

/* Get TUG-3 database */
static tUpsrAu3Tug3Db *Tug3Db(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 tug3no)
    {
    tUpsrAug1Db *pAugDb;

    /* Get AUG DB */
    pAugDb = Aug1Db(pModule, port, augno);
    if ((pAugDb == NULL) ||
        (pAugDb->mapType != cAtUpsrAug1MapTypeTug3) ||
        (tug3no >= cAtUpsrNumAu3Tug3InAug1))
        return NULL;

    /* Return database */
    return &(pAugDb->mapInfo.au3Tug3.au3Tug3[tug3no]);
    }

/* Get AU-3/TUG-3 database */
static tUpsrAu3Tug3Db *Au3Tu3Db(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no)
    {
    tUpsrAug1Db *pAugDb;

    /* Get AUG DB */
    pAugDb = Aug1Db(pModule, port, augno);
    if ((pAugDb == NULL) ||
        ((pAugDb->mapType != cAtUpsrAug1MapTypeAu3) && (pAugDb->mapType != cAtUpsrAug1MapTypeTug3)) ||
        (au3Tug3no >= cAtUpsrNumAu3Tug3InAug1))
        return NULL;

    /* Return database */
    return &(pAugDb->mapInfo.au3Tug3.au3Tug3[au3Tug3no]);
    }

/* Get TUG-2 database */
static tUpsrTug2Db *Tug2Db(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no)
    {
    tUpsrAu3Tug3Db *pAu3Tug3Db;

    /* Get AU-3/TUG-3 DB */
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    if ((pAu3Tug3Db == NULL) ||
        (pAu3Tug3Db->mapType != cAtUpsrAu3Tug3MapTypeTug2) ||
        (tug2no >= cAtUpsrNumTug2InAu3Tug3))
        return NULL;

    /* Return database */
    return &(pAu3Tug3Db->mapInfo.tug2Info.tug2[tug2no]);
    }

/* Get TU-1x database. To reduce context switching, access database directly */
static tUpsrLoCxInfo *TuDb(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrTug2Db *pTug2Db;
    tUpsrAu3Tug3Db *pAu3Tug3Db;
    tUpsrAug1Db *pAug1Db;
    tUpsrAug4Db *pAug4Db;
    tUpsrAug16Db *pAug16Db;
    uint8 aug4no;

    /* Get AUG-4 */
    aug4no = mAug4FromAug1(augno);
    if (((pAug16Db = Aug16Db(pModule, port, 0)) == NULL) ||
        (pAug16Db->mapType != cAtUpsrAug16MapTypeAug4) ||
        (aug4no >= cAtUpsrNumAug4InAug16))
        return NULL;
    pAug4Db = &(pAug16Db->mapInfo.aug4.aug4[aug4no]);

    /* Get AUG-1 */
    if (pAug4Db->mapType != cAtUpsrAug4MapTypeAug1)
        return NULL;
    pAug1Db = &(pAug4Db->mapInfo.aug1.aug1[augno % cAtUpsrNumAug1InAug4]);

    /* Get AU-3/TUG-3 DB */
    if (((pAug1Db->mapType != cAtUpsrAug1MapTypeAu3) && (pAug1Db->mapType != cAtUpsrAug1MapTypeTug3)) ||
        (au3Tug3no >= cAtUpsrNumAu3Tug3InAug1))
        return NULL;
    pAu3Tug3Db = &(pAug1Db->mapInfo.au3Tug3.au3Tug3[au3Tug3no]);

    /* Get TUG-2 database */
    if ((pAu3Tug3Db->mapType != cAtUpsrAu3Tug3MapTypeTug2) || (tug2no >= cAtUpsrNumTug2InAu3Tug3))
        return NULL;
    pTug2Db = &(pAu3Tug3Db->mapInfo.tug2Info.tug2[tug2no]);

    /* Return TU database */
    if (((pTug2Db->mapType != cAtUpsrTug2MapTypeTu11) && (pTug2Db->mapType != cAtUpsrTug2MapTypeTu12)) ||
        (tuno >= mNumTu(pTug2Db->mapType)))
        return NULL;

    /* Return database */
    return &(pTug2Db->cxInfo[tuno]);
    }

/* Check if line is provisioned */
static atbool LineIsProv(tUpsrDb *pModule, uint8 port)
    {
    /* Invalid port ID */
    if (!mPortIsValid(pModule, port))
        return cAtFalse;

    /* Search */
    return ArrayContainItem(pModule->provLine, pModule->numProvLine, port);
    }

/* Add provisioned Line to indicate that there are some sub channels provisioned
 * to UPSR engine */
static eAtUpsrRet LineProv(tUpsrDb *pModule, uint8 port)
    {
    if (LineIsProv(pModule, port))
        return cAtUpsrRetOk;

    pModule->provLine[pModule->numProvLine] = port;
    pModule->numProvLine = (uint8)(pModule->numProvLine + 1);

    return cAtUpsrRetOk;
    }

/* De-Provisioned Line in the case that there is no sub channel provisioned to
 * UPSR engine */
static eAtUpsrRet LineDeProv(tUpsrDb *pModule, uint8 port)
    {
    tUpsrAug16Db *pAug16Info;

    /* Get database */
    pAug16Info = Aug16Db(pModule, port, 0);
    if ((pAug16Info->mapType == cAtUpsrAug16MapTypeAug4) && (pAug16Info->mapInfo.aug4.numProvAug4 != 0))
        return cAtUpsrRetInvalid;

    /* Find line index */
    ArrayRemoveItem(pModule->provLine, &(pModule->numProvLine), port);

    return cAtUpsrRetOk;
    }

/* Check if AUG-4 is provisioned */
static atbool Aug4IsProv(tUpsrDb *pModule, uint8 port, uint8 aug4no)
    {
    tUpsrAug16Db *pAug16Db;

    /* Get AUG-16 database */
    if (((pAug16Db = Aug16Db(pModule, port, 0)) == NULL) ||
        (pAug16Db->mapType != cAtUpsrAug16MapTypeAug4))
        return cAtFalse;

    /* Then check if this AUG-4 is provisioned */
    return ArrayContainItem(pAug16Db->mapInfo.aug4.provAug4, pAug16Db->mapInfo.aug4.numProvAug4, aug4no);
    }

/* Add provisioned AUG-4 to indicate that there are some sub channels provisioned
 * to UPSR engine */
static eAtUpsrRet Aug4Prov(tUpsrDb *pModule, uint8 port, uint8 aug4no)
    {
    tUpsrAug16Db *pAug16Db;
    tUpsrAug4Db *pAug4Db;

    /* Not mapped AUG */
    pAug4Db = Aug4Db(pModule, port, aug4no);
    if ((pAug4Db == NULL) || (pAug4Db->mapType == 0))
        return cAtUpsrRetInvalid;

    /* Nothing to do if this AUG is already provisioned */
    if (Aug4IsProv(pModule, port, aug4no))
        return cAtUpsrRetOk;

    LineProv(pModule, port);
    pAug16Db = Aug16Db(pModule, port, 0);
    pAug16Db->mapInfo.aug4.provAug4[pAug16Db->mapInfo.aug4.numProvAug4] = aug4no;
    pAug16Db->mapInfo.aug4.numProvAug4 = (uint8)(pAug16Db->mapInfo.aug4.numProvAug4 + 1);

    return cAtUpsrRetOk;
    }

/* De-Provisioned AUG-4 in the case that there is no sub channel provisioned to
 * UPSR engine */
static eAtUpsrRet Aug4DeProv(tUpsrDb *pModule, uint8 port, uint8 aug4no)
    {
    tUpsrAug16Db *pAug16Db;
    tUpsrAug4Db *pAug4Db;

    /* Check if there is any provisioned sub channel */
    if (((pAug4Db = Aug4Db(pModule, port, aug4no)) == NULL))
        return cAtUpsrRetInvalid;
    if ((pAug4Db->mapType == cAtUpsrAug4MapTypeAug1) && (pAug4Db->mapInfo.aug1.numProvAug1 != 0))
        return cAtUpsrRetInvalid;

    /* Find index */
    pAug16Db = Aug16Db(pModule, port, 0);
    if (ArrayRemoveItem(pAug16Db->mapInfo.aug4.provAug4, &(pAug16Db->mapInfo.aug4.numProvAug4), aug4no))
        LineDeProv(pModule, port);

    return cAtUpsrRetOk;
    }

/* Check if AUG-1 is provisioned */
static atbool Aug1IsProv(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug4Db *pAug4Db;

    /* Get AUG-4 database */
    if (((pAug4Db = Aug4Db(pModule, port, mAug4FromAug1(augno))) == NULL) ||
        (pAug4Db->mapType != cAtUpsrAug4MapTypeAug1))
        return cAtFalse;

    /* Then search */
    return ArrayContainItem(pAug4Db->mapInfo.aug1.provAug1, pAug4Db->mapInfo.aug1.numProvAug1, augno % cAtUpsrNumAug1InAug4);
    }

/* Add provisioned AUG-1 to indicate that there are some sub channels provisioned
 * to UPSR engine */
static eAtUpsrRet Aug1Prov(tUpsrDb *pModule, uint8 port, uint8 aug1no)
    {
    tUpsrAug1Db *pAug1Db;
    tUpsrAug4Db *pAug4Db;
    uint8 aug4no;

    /* Not mapped AUG */
    pAug1Db = Aug1Db(pModule, port, aug1no);
    if ((pAug1Db == NULL) || (pAug1Db->mapType == 0))
        return cAtUpsrRetInvalid;

    /* Nothing to do if this AUG is already provisioned */
    if (Aug1IsProv(pModule, port, aug1no))
        return cAtUpsrRetOk;

    /* Provision this AUG-1 */
    aug4no = mAug4FromAug1(aug1no);
    pAug4Db = Aug4Db(pModule, port, aug4no);
    Aug4Prov(pModule, port, aug4no);
    pAug4Db->mapInfo.aug1.provAug1[pAug4Db->mapInfo.aug1.numProvAug1] = (aug1no % cAtUpsrNumAug1InAug4);
    pAug4Db->mapInfo.aug1.numProvAug1 = (uint8)(pAug4Db->mapInfo.aug1.numProvAug1 + 1);

    return cAtUpsrRetOk;
    }

/* De-Provisioned AUG in the case that there is no sub channel provisioned to
 * UPSR engine */
static eAtUpsrRet Aug1DeProv(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug4Db *pAug4Db;
    tUpsrAug1Db *pAug1Db;
    uint8 aug4no;

    /* Only de-provision AUG when there is no provisioned sub channel */
    pAug1Db = Aug1Db(pModule, port, augno);
    if ((pAug1Db->mapType != cAtUpsrAug1MapTypeVc4) && (pAug1Db->mapInfo.au3Tug3.numProvAu3Tug3 != 0))
        return cAtUpsrRetInvalid;

    /* De-provision */
    aug4no = mAug4FromAug1(augno);
    pAug4Db = Aug4Db(pModule, port, aug4no);
    if (ArrayRemoveItem(pAug4Db->mapInfo.aug1.provAug1, &(pAug4Db->mapInfo.aug1.numProvAug1), augno % cAtUpsrNumAug1InAug4))
        Aug4DeProv(pModule, port, aug4no);

    return cAtUpsrRetOk;
    }

/* Check if AU-3/TUG-3 is provisioned */
static atbool Au3Tug3IsProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no)
    {
    tUpsrAug1Db *pAugDb;

    /* Get database and search */
    if ((pAugDb = Aug1Db(pModule, port, augno)) == NULL)
        return cAtFalse;

    return ArrayContainItem(pAugDb->mapInfo.au3Tug3.provAu3Tug3, pAugDb->mapInfo.au3Tug3.numProvAu3Tug3, au3Tug3no);
    }

/* Add provisioned AU-3/TUG-3 to indicate that there are some sub channels provisioned
 * to UPSR engine */
static eAtUpsrRet Au3Tug3Prov(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no)
    {
    tUpsrAu3Tug3Db *pAu3Tug3Db;
    tUpsrAug1Db *pAug1Db;

    /* Not mapped AU-3/TUG-3 */
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    if ((pAu3Tug3Db == NULL) || (pAu3Tug3Db->mapType == 0))
        return cAtUpsrRetInvalid;

    /* Do nothing if it is already provisioned */
    if (Au3Tug3IsProv(pModule, port, augno, au3Tug3no))
        return cAtUpsrRetOk;

    Aug1Prov(pModule, port, augno);
    pAug1Db = Aug1Db(pModule, port, augno);
    pAug1Db->mapInfo.au3Tug3.provAu3Tug3[pAug1Db->mapInfo.au3Tug3.numProvAu3Tug3] = au3Tug3no;
    pAug1Db->mapInfo.au3Tug3.numProvAu3Tug3 = (uint8)(pAug1Db->mapInfo.au3Tug3.numProvAu3Tug3 + 1);

    return cAtUpsrRetOk;
    }

/* De-Provisioned AU-3/TUG-3 in the case that there is no sub channel provisioned to
 * UPSR engine */
static eAtUpsrRet Au3Tug3DeProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no)
    {
    tUpsrAug1Db *pAugDb;
    tUpsrAu3Tug3Db *pAu3Tug3Db;

    /* Only de-provision line when there is no provisioned sub channel */
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    if ((pAu3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeTug2) && (pAu3Tug3Db->mapInfo.tug2Info.numProvTug2 != 0))
        return cAtUpsrRetInvalid;

    /* Find line index */
    pAugDb = Aug1Db(pModule, port, augno);
    if (ArrayRemoveItem(pAugDb->mapInfo.au3Tug3.provAu3Tug3, &(pAugDb->mapInfo.au3Tug3.numProvAu3Tug3), au3Tug3no))
        Aug1DeProv(pModule, port, augno);

    return cAtUpsrRetOk;
    }

/* Check if TUG-2 is provisioned */
static atbool Tug2IsProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no)
    {
    tUpsrAu3Tug3Db *pAu3Tug3Db;

    /* Get database and search */
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    return ArrayContainItem(pAu3Tug3Db->mapInfo.tug2Info.provTug2, pAu3Tug3Db->mapInfo.tug2Info.numProvTug2, tug2no);
    }

/* Add provisioned TUG-2 to indicate that there are some sub channels provisioned
 * to UPSR engine */
static eAtUpsrRet Tug2Prov(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no)
    {
    tUpsrAu3Tug3Db *pAu3Tug3Db;

    /* Not mapped TUG-2 */
    if (Tug2Db(pModule, port, augno, au3Tug3no, tug2no)->mapType == 0)
        return cAtUpsrRetInvalid;

    /* Do nothing if it is already provisioned */
    if (Tug2IsProv(pModule, port, augno, au3Tug3no, tug2no))
        return cAtUpsrRetOk;

    Au3Tug3Prov(pModule, port, augno, au3Tug3no);
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    pAu3Tug3Db->mapInfo.tug2Info.provTug2[pAu3Tug3Db->mapInfo.tug2Info.numProvTug2] = tug2no;
    pAu3Tug3Db->mapInfo.tug2Info.numProvTug2 = (uint8)(pAu3Tug3Db->mapInfo.tug2Info.numProvTug2 + 1);

    return cAtUpsrRetOk;
    }

/* De-Provisioned TUG-2 in the case that there is no sub channel provisioned to
 * UPSR engine */
static eAtUpsrRet Tug2DeProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no)
    {
    tUpsrAu3Tug3Db *pAu3Tug3Db;

    /* Only de-provision line when there is no provisioned sub channel */
    if (Tug2Db(pModule, port, augno, au3Tug3no, tug2no)->numProvTu != 0)
        return cAtUpsrRetInvalid;

    /* De-provision */
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    if (ArrayRemoveItem(pAu3Tug3Db->mapInfo.tug2Info.provTug2, &(pAu3Tug3Db->mapInfo.tug2Info.numProvTug2), tug2no))
        Au3Tug3DeProv(pModule, port, augno, au3Tug3no);

    return cAtUpsrRetOk;
    }

/* Check if TU is provisioned */
static atbool TuIsProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrTug2Db *pTug2Db;

    /* Get database and search */
    if (((pTug2Db = Tug2Db(pModule, port, augno, au3Tug3no, tug2no)) == NULL) ||
        (tuno >= mNumTu(pTug2Db->mapType)))
        return cAtFalse;

    return pTug2Db->cxInfo[tuno].isProv;
    }

/* Add provisioned TU to indicate that it is provisioned to UPSR engine */
static eAtUpsrRet TuProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrTug2Db *pTug2Db;

    /* Do nothing if it is already provisioned */
    if (TuIsProv(pModule, port, augno, au3Tug3no, tug2no, tuno))
        return cAtUpsrRetOk;

    Tug2Prov(pModule, port, augno, au3Tug3no, tug2no);
    pTug2Db = Tug2Db(pModule, port, augno, au3Tug3no, tug2no);
    pTug2Db->provTu[pTug2Db->numProvTu] = tuno;
    pTug2Db->numProvTu = (uint8)(pTug2Db->numProvTu + 1);
    pTug2Db->cxInfo[tuno].isProv = cAtTrue;

    return cAtUpsrRetOk;
    }

/* De-Provisioned TU */
static eAtUpsrRet TuDeProv(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrTug2Db *pTug2Db;

    /* De-provision */
    pTug2Db = Tug2Db(pModule, port, augno, au3Tug3no, tug2no);
    if (ArrayRemoveItem(pTug2Db->provTu, &(pTug2Db->numProvTu), tuno))
        Tug2DeProv(pModule, port, augno, au3Tug3no, tug2no);

    return cAtUpsrRetOk;
    }

/* Common function to update defect for working and protection channel */
static void EngDefUpd(tUpsrEngDb *pEngine,
                      uint8 port, uint8 augno, uint8 auno, uint8 tug2no, uint8 tuno,
                      uint8 pairPort, uint8 pairAugno, uint8 pairAuno, uint8 pairTug2no, uint8 pairTuno,
                      atbool isWrk,
                      AtUpsrEngSwFunc swFunc,
                      void *pUsrData, uint32 usrDataSize,
                      uint32 defMsk, uint32 def)
    {
    uint32 newDefStat;
    tAtUpsrEngInfo *pEngInfo;

    /* Remove unused switching condition */
    pEngInfo = &(pEngine->engInfo);
    def = def & pEngInfo->swCond;
    defMsk = defMsk & pEngInfo->swCond;

    /* If engine is in in-active state, just update defect information and then
     * exit */
    if (pEngInfo->engState == cAtUpsrEngStateInActive)
        {
        if (isWrk)
            pEngInfo->wrkDef = (pEngInfo->wrkDef & (~defMsk)) | def;
        else
            pEngInfo->prtDef = (pEngInfo->prtDef & (~defMsk)) | def;
        return;
        }

    /* Defect changes on working channel */
    if (isWrk)
        {
        newDefStat = (pEngInfo->wrkDef & (~defMsk)) | def;

        /* Defect raise */
        if (newDefStat && (!pEngInfo->wrkDef))
            {
            switch (pEngInfo->engState)
                {
                /* Switch to protection channel if it has no alarm */
                case cAtUpsrEngStateIdle:
                case cAtUpsrEngStateMs2Wrk:
                    if (pEngInfo->prtDef)
                        pEngInfo->engState = cAtUpsrEngStateFailWrk;
                    else
                        {
                        pEngInfo->engState = cAtUpsrEngStateSw;
                        pEngInfo->swReason = cAtUpsrSwReasonAuto;
                        swFunc(pEngine,
                               port, augno, auno, tug2no, tuno,
                               pairPort, pairAugno, pairAuno, pairTug2no, pairTuno,
                               pUsrData, usrDataSize);
                        }
                    break;
                default:
                    break;
                }
            }

        /* Defect clear */
        else if (!newDefStat && pEngInfo->wrkDef)
            {
            switch (pEngInfo->engState)
                {
                /* Switch back traffic to working if protection has alarm */
                case cAtUpsrEngStateFailPrt:
                    pEngInfo->engState = cAtUpsrEngStateIdle;
                    pEngInfo->swReason = cAtUpsrSwReasonAuto;
                    swFunc(pEngine,
                           pairPort, pairAugno, pairAuno, pairTug2no, pairTuno,
                           port, augno, auno, tug2no, tuno,
                           pUsrData, usrDataSize);
                    break;

                /* Traffic is on protection, activate WTR if need */
                case cAtUpsrEngStateSw:
                    if (pEngInfo->swType == cAtUpsrSwTypeRev)
                        pEngInfo->engState = cAtUpsrEngStateWtr;
                    break;

                case cAtUpsrEngStateMs2Prt:
                    /* Switch back traffic to working if protection has alarm */
                    if (pEngInfo->prtDef)
                        {
                        pEngInfo->engState = cAtUpsrEngStateIdle;
                        pEngInfo->swReason = cAtUpsrSwReasonAuto;
                        swFunc(pEngine,
                               pairPort, pairAugno, pairAuno, pairTug2no, pairTuno,
                               port, augno, auno, tug2no, tuno,
                               pUsrData, usrDataSize);
                        }
                    break;

                /* Switching fail and traffic is current on working channel,
                 * just change to IDLE state */
                case cAtUpsrEngStateFailWrk:
                    pEngInfo->engState = cAtUpsrEngStateIdle;
                    break;

                /* Ignore */
                default:
                    break;
                }
            }

        /* Update defect */
        pEngInfo->wrkDef = newDefStat;
        }

    /* Defect change on protection channel */
    else
        {
        newDefStat = (pEngInfo->prtDef & (~defMsk)) | def;

        /* Defect raise */
        if (newDefStat && (!pEngInfo->prtDef))
            {
            switch (pEngInfo->engState)
                {
                /* Switch back to working channel if it has no alarm */
                case cAtUpsrEngStateSw:
                case cAtUpsrEngStateWtr:
                case cAtUpsrEngStateMs2Prt:
                    if (pEngInfo->wrkDef)
                        pEngInfo->engState = cAtUpsrEngStateFailPrt;
                    else
                        {
                        pEngInfo->engState = cAtUpsrEngStateIdle;
                        pEngInfo->swReason = cAtUpsrSwReasonAuto;
                        swFunc(pEngine,
                               port, augno, auno, tug2no, tuno,
                               pairPort, pairAugno, pairAuno, pairTug2no, pairTuno,
                               pUsrData, usrDataSize);
                        }

                    break;

                /* Ignore */
                default:
                    break;
                }
            }

        /* Defect clear */
        else if (!newDefStat && pEngInfo->prtDef)
            {
            switch (pEngInfo->engState)
                {
                /* Traffic currently fail on working */
                case cAtUpsrEngStateFailWrk:
                    pEngInfo->engState = cAtUpsrEngStateSw;
                    pEngInfo->swReason = cAtUpsrSwReasonAuto;
                    swFunc(pEngine,
                           pairPort, pairAugno, pairAuno, pairTug2no, pairTuno,
                           port, augno, auno, tug2no, tuno,
                           pUsrData, usrDataSize);
                    break;

                /* Protection fail and traffic is on protection, just change
                 * to switching state */
                case cAtUpsrEngStateFailPrt:
                    pEngInfo->engState = cAtUpsrEngStateSw;
                    break;
                default:
                    break;
                }
            }

        /* Update defect. Note, in case of defect is clear, do nothing for
         * protection channel */
        pEngInfo->prtDef = newDefStat;
        }
    }

/* Update TUG-2 defect */
static eAtUpsrRet Tug2DefUpd(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tu3no, uint8 tug2no, uint32 defMsk, uint32 def, tUpsrTug2Db *pTug2Db)
    {
    uint8 idx, tuno;
    eAtUpsrRet ret;

    ret = cAtUpsrRetOk;
    pTug2Db = Tug2Db(pModule, port, augno, au3Tu3no, tug2no);
    for (idx = 0; idx < pTug2Db->numProvTu; idx++)
        {
        tuno = pTug2Db->provTu[idx];
        mTuDefUpd(pTug2Db->cxInfo[tuno].pEngDb, port, augno, au3Tu3no, tug2no, tuno, defMsk, def, &(pTug2Db->cxInfo[tuno]));
        }

    return ret;
    }

/* Update AU-3/TUG-3 defect */
static eAtUpsrRet Au3Tug3DefUpd(tUpsrDb *pModule, uint8 port, uint8 aug1no, uint8 au3Tu3no, uint32 defMsk, uint32 def, tUpsrAu3Tug3Db *pAu3Tug3Db)
    {
    uint8 idx, tug2no;
    tUpsrHoCxInfo *pCxInfo;
    eAtUpsrRet ret;

    ret = cAtUpsrRetOk;
    if (pAu3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeVc3)
        {
        /* Update defect */
        pCxInfo  = &(pAu3Tug3Db->mapInfo.cxInfo);

        /* Update engine state machine */
        EngDefUpd(pCxInfo->pEngDb, port, aug1no, au3Tu3no, 0, 0,
                  pCxInfo->pairPort, pCxInfo->pairAugno, pCxInfo->pairAuno, 0, 0,
                  pCxInfo->isWrk, pCxInfo->pEngDb->funcSw,
                  pCxInfo->pEngDb->pUsrData, pCxInfo->pEngDb->usrDataSize,
                  defMsk, def);
        }
    else
        {
        for (idx = 0; idx < pAu3Tug3Db->mapInfo.tug2Info.numProvTug2; idx++)
            {
            tug2no = pAu3Tug3Db->mapInfo.tug2Info.provTug2[idx];
            ret |= Tug2DefUpd(pModule, port, aug1no, au3Tu3no, tug2no, defMsk, def, &(pAu3Tug3Db->mapInfo.tug2Info.tug2[tug2no]));
            }
        }

    return ret;
    }

/* Update AUG-1 defect */
static eAtUpsrRet Aug1DefUpd(tUpsrDb *pModule, uint8 port, uint8 aug1no, uint32 defMsk, uint32 def, tUpsrAug1Db *pAugDb)
    {
    uint8 idx, auno;
    tUpsrHoCxInfo *pCxInfo;
    eAtUpsrRet ret;

    ret = cAtUpsrRetOk;
    switch (pAugDb->mapType)
        {
        /* AU-3/TUG-3 mapping */
        case cAtUpsrAug1MapTypeTug3:
        case cAtUpsrAug1MapTypeAu3:
            for (idx = 0; idx < pAugDb->mapInfo.au3Tug3.numProvAu3Tug3; idx++)
                {
                auno = pAugDb->mapInfo.au3Tug3.provAu3Tug3[idx];
                ret |= Au3Tug3DefUpd(pModule, port, aug1no, auno, defMsk, def, &(pAugDb->mapInfo.au3Tug3.au3Tug3[auno]));
                }
            break;

        /* VC-4 mapping */
        case cAtUpsrAug1MapTypeVc4:
            pCxInfo  = &(pAugDb->mapInfo.cxInfo);

            /* Update engine state machine */
            EngDefUpd(pCxInfo->pEngDb, port, aug1no, 0, 0, 0,
                      pCxInfo->pairPort, pCxInfo->pairAugno, 0, 0, 0,
                      pCxInfo->isWrk, pCxInfo->pEngDb->funcSw,
                      pCxInfo->pEngDb->pUsrData, pCxInfo->pEngDb->usrDataSize,
                      defMsk, def);
            break;

        /* Invalid */
        default:
            return cAtUpsrRetInvalid;
        }

    return ret;
    }

/* Update AUG-4 defect */
static eAtUpsrRet Aug4DefUpd(tUpsrDb *pModule, uint8 port, uint8 aug4no, uint32 defMsk, uint32 def, tUpsrAug4Db *pAugDb)
    {
    uint8 idx, firstAug1, aug1no;
    tUpsrHoCxInfo *pCxInfo;
    eAtUpsrRet ret;

    ret = cAtUpsrRetOk;
    firstAug1 = (uint8)(aug4no * 4);
    switch (pAugDb->mapType)
        {
        /* AUG-1 mapping */
        case cAtUpsrAug4MapTypeAug1:
            for (idx = 0; idx < pAugDb->mapInfo.aug1.numProvAug1; idx++)
                {
                aug1no = pAugDb->mapInfo.aug1.provAug1[idx];
                ret |= Aug1DefUpd(pModule, port, (uint8)(aug1no + firstAug1), defMsk, def, &(pAugDb->mapInfo.aug1.aug1[aug1no]));
                }
            break;

        /* VC-4-4c mapping */
        case cAtUpsrAug4MapTypeVc4_4c:
            pCxInfo  = &(pAugDb->mapInfo.cxInfo);

            /* Update engine state machine */
            EngDefUpd(pCxInfo->pEngDb, port, firstAug1, 0, 0, 0,
                      pCxInfo->pairPort, pCxInfo->pairAugno, 0, 0, 0,
                      pCxInfo->isWrk, pCxInfo->pEngDb->funcSw,
                      pCxInfo->pEngDb->pUsrData, pCxInfo->pEngDb->usrDataSize,
                      defMsk, def);
            break;

        /* Invalid */
        default:
            return cAtUpsrRetInvalid;
        }

    return ret;
    }

/* Update AUG-16 defect */
static eAtUpsrRet Aug16DefUpd(tUpsrDb *pModule, uint8 port, uint8 augno, uint32 defMsk, uint32 def, tUpsrAug16Db *pAugDb)
    {
    uint8 idx, aug4no;
    tUpsrHoCxInfo *pCxInfo;
    eAtUpsrRet ret;
    Unused(augno);

    ret = cAtUpsrRetOk;
    switch (pAugDb->mapType)
        {
        /* AUG-4 mapping */
        case cAtUpsrAug16MapTypeAug4:
            for (idx = 0; idx < pAugDb->mapInfo.aug4.numProvAug4; idx++)
                {
                aug4no = pAugDb->mapInfo.aug4.provAug4[idx];
                ret |= Aug4DefUpd(pModule, port, aug4no, defMsk, def, &(pAugDb->mapInfo.aug4.aug4[aug4no]));
                }
            break;

        /* VC-4-4c mapping */
        case cAtUpsrAug16MapTypeVc4_16c:
            pCxInfo  = &(pAugDb->mapInfo.cxInfo);

            /* Update engine state machine */
            EngDefUpd(pCxInfo->pEngDb, port, 0, 0, 0, 0,
                      pCxInfo->pairPort, 0, 0, 0, 0,
                      pCxInfo->isWrk, pCxInfo->pEngDb->funcSw,
                      pCxInfo->pEngDb->pUsrData, pCxInfo->pEngDb->usrDataSize,
                      defMsk, def);
            break;

        /* Invalid */
        default:
            return cAtUpsrRetInvalid;
        }

    return ret;
    }

/* Update line defect */
static  eAtUpsrRet LineDefUpd(tUpsrDb *pModule, uint8 port, uint32 defMsk, uint32 def)
    {
    /* Only process when the line is provisioned */
    if (!LineIsProv(pModule, port))
        return cAtUpsrRetOk;

    return Aug16DefUpd(pModule, port, 0, defMsk, def, &(LineDb(pModule, port)->aug16));
    }

/* Free all of resources that allocated for all sub channels of an TU */
static void TuFree(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrLoCxInfo *pCxInfo;

    if ((pCxInfo = TuDb(pModule, port, augno, au3Tug3no, tug2no, tuno)) == NULL)
        return;
    AtUpsrEngDelete(pCxInfo->pEngDb);
    }

/* Free all of resources that allocated for all sub channels of an TUG-2 */
static void Tug2Free(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2)
    {
    tUpsrTug2Db *pTug2Db;

    /* Do nothing if this AUG is not mapped */
    pTug2Db = Tug2Db(pModule, port, augno, au3Tug3no, tug2);
    if (pTug2Db->mapType == cAtUpsrTug2MapTypeNone)
        return;

    while (pTug2Db->numProvTu > 0)
        TuFree(pModule, port, augno, au3Tug3no, tug2, pTug2Db->provTu[0]);
    }

/* Free all of resources that allocated for all sub channels of an AU-3/TUG-3 */
static void Au3Tug3Free(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no)
    {
    tUpsrAu3Tug3Db *pAu3Tug3Db;

    /* Do nothing if this AUG is not mapped */
    pAu3Tug3Db = Au3Tu3Db(pModule, port, augno, au3Tug3no);
    if (pAu3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeNone)
        return;

    if (pAu3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeVc3)
        AtUpsrEngDelete(pAu3Tug3Db->mapInfo.cxInfo.pEngDb);
    else
        {
        while (pAu3Tug3Db->mapInfo.tug2Info.numProvTug2 > 0)
            Tug2Free(pModule, port, augno, au3Tug3no, pAu3Tug3Db->mapInfo.tug2Info.provTug2[0]);
        }
    }

/* Free all of resources that allocated for all sub channels of an AUG */
static void Aug1Free(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug1Db *pAugDb;

    /* Do nothing if this AUG is not mapped */
    pAugDb = Aug1Db(pModule, port, augno);
    if (pAugDb->mapType == cAtUpsrAug1MapTypeNone)
        return;

    /* VC-4 */
    if (pAugDb->mapType == cAtUpsrAug1MapTypeVc4)
        AtUpsrEngDelete(pAugDb->mapInfo.cxInfo.pEngDb);

    /* 3xAU-3s/TUG-3s */
    else
        {
        while (pAugDb->mapInfo.au3Tug3.numProvAu3Tug3 > 0)
            Au3Tug3Free(pModule, port, augno, pAugDb->mapInfo.au3Tug3.provAu3Tug3[0]);
        }
    }

/* Free all of resources that allocated for all sub channels of an AUG-4 */
static void Aug4Free(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug4Db *pAugDb;
    uint8 firstAug1, aug1no;

    /* Do nothing if this AUG is not mapped */
    pAugDb = Aug4Db(pModule, port, augno);
    if (pAugDb->mapType == cAtUpsrAug4MapTypeNone)
        return;

    /* VC-4-4c */
    if (pAugDb->mapType == cAtUpsrAug4MapTypeVc4_4c)
        AtUpsrEngDelete(pAugDb->mapInfo.cxInfo.pEngDb);

    /* 4xAUG-1s */
    else
        {
        firstAug1 = (uint8)(augno * 4);
        while (pAugDb->mapInfo.aug1.numProvAug1 > 0)
            {
            aug1no = (uint8)(firstAug1 + pAugDb->mapInfo.aug1.provAug1[0]);
            Aug1Free(pModule, port, aug1no);
            }
        }
    }

/* Free all of resources that allocated for all sub channels of an AUG-16 */
static void Aug16Free(tUpsrDb *pModule, uint8 port, uint8 augno)
    {
    tUpsrAug16Db *pAug16Db;
    Unused(augno);

    /* Do nothing if this AUG is not mapped */
    pAug16Db = Aug16Db(pModule, port, 0);
    if (pAug16Db->mapType == cAtUpsrAug16MapTypeNone)
        return;

    if (pAug16Db->mapType == cAtUpsrAug16MapTypeVc4_16c)
        AtUpsrEngDelete(pAug16Db->mapInfo.cxInfo.pEngDb);
    else
        {
        while (pAug16Db->mapInfo.aug4.numProvAug4 > 0)
            Aug4Free(pModule, port, pAug16Db->mapInfo.aug4.provAug4[0]);
        }
    }

/* Free all of resources that allocated for all sub channels of a Line */
static void LineFree(tUpsrDb *pModule, uint8 port)
    {
    Aug16Free(pModule, port, 0);
    }

/*
 * TUG-2 traversing function.
 */
static eAtUpsrRet Tug2Traverse(UpsrEngTraverseFunc traverseFunc, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, tUpsrTug2Db *pTug2Db)
    {
    uint8 idx, tuno;
    tUpsrLoCxInfo *pCxInfo;
    uint8 provTu[4];
    uint8 numProvTu;
    Unused(port);
    Unused(augno);
    Unused(au3Tug3no);
    Unused(tug2no);

    /* During traversing, engine can be deleted, so save provisioned list
     * first to make sure that all of channels are traversed */
    AtOsalMemCpy(provTu, pTug2Db->provTu, sizeof(provTu));
    numProvTu = pTug2Db->numProvTu;

    for (idx = 0; idx < numProvTu; idx++)
        {
        tuno = provTu[idx];
        pCxInfo = &(pTug2Db->cxInfo[tuno]);
        if (pCxInfo->isWrk)
            traverseFunc(pCxInfo->pEngDb);
        }
    return cAtUpsrRetOk;
    }

/*
 * AU-3/TUG-3 traversing function.
 */
static eAtUpsrRet Au3Tug3Traverse(UpsrEngTraverseFunc traverseFunc, uint8 port, uint8 augno, uint8 au3Tug3no, tUpsrAu3Tug3Db *pAu3Tug3Db)
    {
    uint8 idx, tug2no;
    tUpsrHoCxInfo *pCxInfo;
    uint8 provTug2[7];
    uint8 numProvTug2;

    /* VC-3 mapping */
    if (pAu3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeVc3)
        {
        pCxInfo = &(pAu3Tug3Db->mapInfo.cxInfo);
        if (pCxInfo->isWrk)
            traverseFunc(pCxInfo->pEngDb);
        }

    /* TUG-2 mapping */
    else
        {
        /* During traversing, engine can be deleted, so save provisioned list
         * first to make sure that all of channels are traversed */
        AtOsalMemCpy(provTug2, pAu3Tug3Db->mapInfo.tug2Info.provTug2, sizeof(provTug2));
        numProvTug2 = pAu3Tug3Db->mapInfo.tug2Info.numProvTug2;
        for (idx = 0; idx < numProvTug2; idx++)
            {
            tug2no = provTug2[idx];
            Tug2Traverse(traverseFunc, port, augno, au3Tug3no, tug2no, &(pAu3Tug3Db->mapInfo.tug2Info.tug2[tug2no]));
            }
        }

    return cAtUpsrRetOk;
    }

/*
 * AUG-1 traversing function.
 */
static eAtUpsrRet Aug1Traverse(UpsrEngTraverseFunc traverseFunc, uint8 port, uint8 augno, tUpsrAug1Db *pAugDb)
    {
    uint8 idx, au3Tug3no;
    tUpsrHoCxInfo *pCxInfo;
    uint8 provAu3Tug3[3];
    uint8 numProvAu3Tug3;

    /* VC-4 */
    if (pAugDb->mapType == cAtUpsrAug1MapTypeVc4)
        {
        pCxInfo = &(pAugDb->mapInfo.cxInfo);
        if (pCxInfo->isWrk)
            traverseFunc(pCxInfo->pEngDb);
        }

    /* AU-3/TUG-3 */
    else
        {
        /* During traversing, engine can be deleted, so save provisioned list
         * first to make sure that all of channels are traversed */
        AtOsalMemCpy(provAu3Tug3, pAugDb->mapInfo.au3Tug3.provAu3Tug3, sizeof(provAu3Tug3));
        numProvAu3Tug3 = pAugDb->mapInfo.au3Tug3.numProvAu3Tug3;
        for (idx = 0; idx < numProvAu3Tug3; idx++)
            {
            au3Tug3no = provAu3Tug3[idx];
            Au3Tug3Traverse(traverseFunc, port, augno, au3Tug3no, &(pAugDb->mapInfo.au3Tug3.au3Tug3[au3Tug3no]));
            }
        }

    return cAtUpsrRetOk;
    }

/* AUG-4 traversing function */
static eAtUpsrRet Aug4Traverse(UpsrEngTraverseFunc traverseFunc, uint8 port, uint8 aug4no, tUpsrAug4Db *pAugDb)
    {
    uint8 idx, aug1no, firstAug1;
    tUpsrHoCxInfo *pCxInfo;
    uint8 provAug1[cAtUpsrNumAug1InAug4];
    uint8 numProvAug1;

    /* VC-4-4c */
    if (pAugDb->mapType == cAtUpsrAug4MapTypeVc4_4c)
        {
        pCxInfo = &(pAugDb->mapInfo.cxInfo);
        if (pCxInfo->isWrk)
            traverseFunc(pCxInfo->pEngDb);
        }

    /* 4xAUG-1s */
    else
        {
        /* During traversing, engine can be deleted, so save provisioned list
         * first to make sure that all of channels are traversed */
        AtOsalMemCpy(provAug1, pAugDb->mapInfo.aug1.provAug1, sizeof(provAug1));
        numProvAug1 = pAugDb->mapInfo.aug1.numProvAug1;
        firstAug1 = (uint8)(aug4no * 4);
        for (idx = 0; idx < numProvAug1; idx++)
            {
            aug1no = (uint8)(firstAug1 + provAug1[idx]);
            Aug1Traverse(traverseFunc, port, aug1no, &(pAugDb->mapInfo.aug1.aug1[provAug1[idx]]));
            }
        }

    return cAtUpsrRetOk;
    }

/* AUG-16 traversing function */
static eAtUpsrRet Aug16Traverse(UpsrEngTraverseFunc traverseFunc, uint8 port, uint8 augno, tUpsrAug16Db *pAugDb)
    {
    uint8 idx;
    tUpsrHoCxInfo *pCxInfo;
    uint8 provAug4[cAtUpsrNumAug4InAug16];
    uint8 numProvAug4;
    Unused(augno);

    /* VC-4-16c */
    if (pAugDb->mapType == cAtUpsrAug16MapTypeVc4_16c)
        {
        pCxInfo = &(pAugDb->mapInfo.cxInfo);
        if (pCxInfo->isWrk)
            traverseFunc(pCxInfo->pEngDb);
        }

    /* 4xAUG-4s */
    else
        {
        /* During traversing, engine can be deleted, so save provisioned list
         * first to make sure that all of channels are traversed */
        AtOsalMemCpy(provAug4, pAugDb->mapInfo.aug4.provAug4, sizeof(provAug4));
        numProvAug4 = pAugDb->mapInfo.aug4.numProvAug4;
        for (idx = 0; idx < numProvAug4; idx++)
            Aug4Traverse(traverseFunc, port, provAug4[idx], &(pAugDb->mapInfo.aug4.aug4[provAug4[idx]]));
        }

    return cAtUpsrRetOk;
    }

/* Check if AUG-16 contains any engine */
static atbool Aug16ContainEngine(tUpsrAug16Db *pAugDb)
    {
    if (pAugDb->mapType == cAtUpsrAug16MapTypeVc4_16c)
        {
        if (pAugDb->mapInfo.cxInfo.pEngDb != NULL)
            return cAtTrue;
        }
    else if ((pAugDb->mapType == cAtUpsrAug16MapTypeAug4) && (pAugDb->mapInfo.aug4.numProvAug4 > 0))
        return cAtTrue;

    return cAtFalse;
    }

/* Check if AUG-4 contains any engine */
static atbool Aug4ContainEngine(tUpsrAug4Db *pAugDb)
    {
    if (pAugDb->mapType == cAtUpsrAug4MapTypeVc4_4c)
        {
        if (pAugDb->mapInfo.cxInfo.pEngDb != NULL)
            return cAtTrue;
        }
    else if ((pAugDb->mapType == cAtUpsrAug4MapTypeAug1) && (pAugDb->mapInfo.aug1.numProvAug1 > 0))
        return cAtTrue;

    return cAtFalse;
    }

/* Check if AUG-1 contains any engine */
static atbool Aug1ContainEngine(tUpsrAug1Db *pAugDb)
    {
    if (pAugDb->mapType == cAtUpsrAug1MapTypeNone)
        return cAtFalse;

    if (pAugDb->mapType == cAtUpsrAug1MapTypeVc4)
        {
        if (pAugDb->mapInfo.cxInfo.pEngDb != NULL)
            return cAtTrue;
        }
    else if ((pAugDb->mapInfo.au3Tug3.numProvAu3Tug3 > 0))
        return cAtTrue;

    return cAtFalse;
    }

/* Check if AU-3/TUG-3 contains any engine */
static atbool Au3Tug3ContainEngine(tUpsrAu3Tug3Db *pAug3Tug3Db)
    {
    if (pAug3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeVc3)
        {
        if (pAug3Tug3Db->mapInfo.cxInfo.pEngDb != NULL)
            return cAtTrue;
        }
    else if ((pAug3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeTug2) && (pAug3Tug3Db->mapInfo.tug2Info.numProvTug2 > 0))
        return cAtTrue;

    return cAtFalse;
    }

/* Check if TUG-2 contains any engine */
static atbool Tug2ContainEngine(tUpsrTug2Db *pTug2Db)
    {
    return (pTug2Db->numProvTu > 0) ? cAtTrue : cAtFalse;
    }

/* Get AU type of a path */
static eAtUpsrAuType AuType(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrAug16Db *pAug16;
    tUpsrAug4Db *pAug4;
    tUpsrAug1Db *pAug1;
    tUpsrAu3Tug3Db *pAu3;
    uint8 aug1no, aug4no;

    pAug16 = Aug16Db(pModule, port, 0);
    if ((pAug16 == NULL) || (pAug16->mapType == cAtUpsrNotMap))
        return cAtUpsrUnknown;

    /* AUG-16 maps 1xVC4-16c */
    if (pAug16->mapType == cAtUpsrAug16MapTypeVc4_16c)
        return cAtUpsrAuTypeAu4_16c;

    /* This can be AU-4-4c */
    aug4no = augno / (cAtUpsrNumAug4InAug16);
    if (aug4no >= cAtUpsrNumAug4InAug16)
        return cAtUpsrUnknown;
    pAug4 = &(pAug16->mapInfo.aug4.aug4[aug4no]);
    if (pAug4->mapType == cAtUpsrNotMap)
        return cAtUpsrUnknown;
    if (pAug4->mapType == cAtUpsrAug4MapTypeVc4_4c)
        return cAtUpsrAuTypeAu4_4c;

    /* This can be AU-4 */
    aug1no = augno % cAtUpsrNumAug1InAug4;
    pAug1 = &(pAug4->mapInfo.aug1.aug1[aug1no]);
    if (pAug1->mapType == cAtUpsrNotMap)
        return cAtUpsrUnknown;
    if ((pAug1->mapType == cAtUpsrAug1MapTypeVc4) || (pAug1->mapType == cAtUpsrAug1MapTypeTug3))
        return cAtUpsrAuTypeAu4;

    /* This can be AU-3 */
    if (auno >= cAtUpsrNumAu3Tug3InAug1)
        return cAtUpsrUnknown;
    pAu3 = &(pAug1->mapInfo.au3Tug3.au3Tug3[auno]);
    if (pAu3->mapType == cAtUpsrNotMap)
        return cAtUpsrUnknown;

    return cAtUpsrAuTypeAu3;
    }

/* Get TU type */
static eAtUpsrTuType TuType(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrAug1Db *pAug1Db;
    tUpsrAu3Tug3Db *pAu3Tug3Db;
    tUpsrTug2Db *pTug2Db;

    /* Check if this channel is valid */
    pAug1Db = Aug1Db(pModule, port, augno);
    if ((pAug1Db == NULL) || (pAug1Db->mapType == cAtUpsrNotMap) || (pAug1Db->mapType == cAtUpsrAug1MapTypeVc4))
        return cAtUpsrUnknown;

    /* Get AU-3/TUG-3 database */
    if (au3Tug3no >= cAtUpsrNumAu3Tug3InAug1)
        return cAtUpsrUnknown;
    pAu3Tug3Db = &(pAug1Db->mapInfo.au3Tug3.au3Tug3[au3Tug3no]);
    if (pAu3Tug3Db->mapType == cAtUpsrNotMap)
        return cAtUpsrUnknown;

    /* Check if this can be TU-3 */
    if (pAug1Db->mapType == cAtUpsrAug1MapTypeTug3)
        {
        if (pAu3Tug3Db->mapType == cAtUpsrAu3Tug3MapTypeVc3)
            return cAtUpsrTuTypeTu3;
        }
    else if (pAu3Tug3Db->mapType != cAtUpsrAu3Tug3MapTypeTug2)
        return cAtUpsrUnknown;

    /* This must be TU-1x */
    if (tug2no >= cAtUpsrNumTug2InAu3Tug3)
        return cAtUpsrUnknown;
    pTug2Db = &(pAu3Tug3Db->mapInfo.tug2Info.tug2[tug2no]);
    if (tuno >= mNumTu(pTug2Db->mapType))
        return cAtUpsrUnknown;
    if (pTug2Db->mapType == cAtUpsrTug2MapTypeTu11)
        return cAtUpsrTuTypeTu11;
    if (pTug2Db->mapType == cAtUpsrTug2MapTypeTu12)
        return cAtUpsrTuTypeTu12;

    return cAtUpsrUnknown;
    }

/* Get HO VC information */
static tUpsrHoCxInfo *HoCx(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrAug16Db *pAug16;
    tUpsrAug4Db *pAug4;
    tUpsrAug1Db *pAug1;
    tUpsrAu3Tug3Db *pAu3;
    uint8 aug1no, aug4no;

    pAug16 = Aug16Db(pModule, port, 0);
    if ((pAug16 == NULL) || (pAug16->mapType == cAtUpsrNotMap))
        return NULL;

    /* AUG-16 maps 1xVC4-16c */
    if (pAug16->mapType == cAtUpsrAug16MapTypeVc4_16c)
        return &(pAug16->mapInfo.cxInfo);

    /* This can be AU-4-4c */
    aug4no = augno / (cAtUpsrNumAug4InAug16);
    if (aug4no >= cAtUpsrNumAug4InAug16)
        return NULL;
    pAug4 = &(pAug16->mapInfo.aug4.aug4[aug4no]);
    if (pAug4->mapType == cAtUpsrNotMap)
        return NULL;
    if (pAug4->mapType == cAtUpsrAug4MapTypeVc4_4c)
        return &(pAug4->mapInfo.cxInfo);

    /* This can be AU-4 */
    aug1no = augno % cAtUpsrNumAug1InAug4;
    pAug1 = &(pAug4->mapInfo.aug1.aug1[aug1no]);
    if (pAug1->mapType == cAtUpsrNotMap)
        return NULL;
    if (pAug1->mapType == cAtUpsrAug1MapTypeVc4)
        return &(pAug1->mapInfo.cxInfo);

    /* This can be AU-3 */
    if (auno >= cAtUpsrNumAu3Tug3InAug1)
        return NULL;
    pAu3 = &(pAug1->mapInfo.au3Tug3.au3Tug3[auno]);
    if (pAu3->mapType == cAtUpsrNotMap)
        return NULL;

    return &(pAu3->mapInfo.cxInfo);
    }

/* Get TU-3's VC-3 information */
static tUpsrHoCxInfo *Tu3Vc3(tUpsrDb *pModule, uint8 port, uint8 augno, uint8 tu3no)
    {
    tUpsrAug1Db *pAug1;
    tUpsrAu3Tug3Db *pTug3;

    /* Check if AUG-1 maps TUG-3s */
    pAug1 = Aug1Db(pModule, port, augno);
    if ((pAug1 == NULL) ||
        (pAug1->mapType != cAtUpsrAug1MapTypeTug3) ||
        (tu3no >= cAtUpsrNumAu3Tug3InAug1))
        return NULL;

    /* Check TUG-3 mapping */
    pTug3 = &(pAug1->mapInfo.au3Tug3.au3Tug3[tu3no]);
    if (pTug3->mapType != cAtUpsrAu3Tug3MapTypeVc3)
        return NULL;
    return &(pTug3->mapInfo.cxInfo);
    }

/*
 * Line traversing function.
 */
static eAtUpsrRet LineTraverse(UpsrEngTraverseFunc traverseFunc, uint8 port, tUpsrLineDb *pLineDb)
    {
    return Aug16Traverse(traverseFunc, port, 0, &(pLineDb->aug16));
    }

/* Save OS interface */
static atbool ModuleSaveOsIntf(tUpsrDb *pModule, const tUpsrOsIntf *pOsIntf)
    {
    /* Error on NULL OS interface */
    if (pOsIntf == NULL)
        return cAtFalse;

    /* Make sure that OS interface is valid */
    if ((pOsIntf->semCreate == NULL) ||
        (pOsIntf->semDelete == NULL) ||
        (pOsIntf->semTake == NULL) ||
        (pOsIntf->semGive == NULL))
        return cAtFalse;

    /* Save */
    AtOsalMemCpy(&(pModule->osIntf), pOsIntf, sizeof(tUpsrOsIntf));

    return cAtTrue;
    }

/*
 * Create Line's database
 */
static atbool LineDbCreate(tUpsrDb *pModule, uint8 maxNumLine)
    {
    uint32 size;

    /* Create list to store provisioned Lines */
    size = sizeof(uint8) * maxNumLine;
    pModule->provLine = AtOsalMemAlloc(size);
    if (pModule->provLine == NULL)
        return cAtFalse;
    AtOsalMemInit(pModule->provLine, 0, size);

    /* Create list of Line's database */
    size = sizeof(tUpsrLineDb) * maxNumLine;
    pModule->lineDb = AtOsalMemAlloc(size);
    if (pModule->lineDb == NULL)
        {
        AtOsalMemFree(pModule->provLine);
        return cAtFalse;
        }
    AtOsalMemInit(pModule->lineDb, 0, size);
    pModule->maxNumLine = maxNumLine;
    pModule->numProvLine = 0;

    return cAtTrue;
    }

/* Delete Line database */
static void LineDbDelete(tUpsrDb *pModule)
    {
    AtOsalMemFree(pModule->provLine);
    AtOsalMemFree(pModule->lineDb);
    }

/* Create engine */
static tUpsrEngDb *EngDbCreate(tUpsrDb *pModule,
                               uint8 w_port, uint8 w_augno, uint8 w_au3Tug3no, uint8 w_tug2no, uint8 w_tuno,
                               eAtUpsrSwType swType,
                               uint32 wtrTime,
                               AtUpsrEngSwFunc swFunc,
                               void *pUsrData, uint32 usrDataSize)
    {
    tUpsrEngDb *pEngDb;

    /* Cannot create engine with NULL application interface */
    if (swFunc == NULL)
        return NULL;

    /* Check if switching type is valid */
    if ((swType != cAtUpsrSwTypeNonRev) && (swType != cAtUpsrSwTypeRev))
        return NULL;

    /* Allocate memory */
    pEngDb = AtOsalMemAlloc(sizeof(tUpsrEngDb));
    if (pEngDb == NULL)
        return NULL;
    AtOsalMemInit(pEngDb, 0, sizeof(tUpsrEngDb));

    /* Copy user data */
    if ((pUsrData != NULL) && (usrDataSize > 0))
        {
        pEngDb->pUsrData = AtOsalMemAlloc(usrDataSize);
        if (pEngDb->pUsrData == NULL)
            {
            AtOsalMemFree(pEngDb);
            return NULL;
            }
        AtOsalMemCpy(pEngDb->pUsrData, pUsrData, usrDataSize);
        pEngDb->usrDataSize = usrDataSize;
        }

    /* Store engine configuration */
    pEngDb->engInfo.extlCmd = cAtUpsrExtlCmdClear;
    pEngDb->engInfo.swType  = swType;
    pEngDb->engInfo.wtrTime = wtrTime;

    /* Install application interfaces */
    pEngDb->funcSw = swFunc;

    /* Set default configuration */
    pEngDb->engInfo.swCond  = cAtUpsrSwCondLine | cAtUpsrSwCondHo;
    pEngDb->engInfo.swCond &= (~cAtUpsrEngSwCondOof); /* Ignore OOF as default */
    pEngDb->module = pModule;

    /* Save the working channel */
    pEngDb->w_port      = w_port;
    pEngDb->w_augno     = w_augno;
    pEngDb->w_au3Tug3no = w_au3Tug3no;
    pEngDb->w_tug2no    = w_tug2no;
    pEngDb->w_tuno      = w_tuno;

    return pEngDb;
    }

/* Delete engine */
static void EngDelete(tUpsrEngDb *pEngine)
    {
    AtOsalMemFree(pEngine->pUsrData);
    AtOsalMemFree(pEngine);
    }

/* Delete HO engine */
static eAtUpsrRet HoEngDelete(tUpsrEngDb *pEngine)
    {
    tUpsrHoCxInfo *pWrkCx, *pPrtCx;
    AtUpsrModule module;

    /* Get HO VC information */
    pWrkCx = HoCx(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
    pPrtCx = HoCx(pEngine->module, pWrkCx->pairPort, pWrkCx->pairAugno, pWrkCx->pairAuno);
    if ((pWrkCx == NULL) || (pWrkCx->pEngDb != pEngine) ||
        (pPrtCx == NULL) || (pPrtCx->pEngDb != pEngine))
        return cAtUpsrRetNotCreated;

    /* Free resource */
    module = pEngine->module;
    AtOsalMemFree(pEngine->pUsrData);
    AtOsalMemFree(pEngine);
    pWrkCx->pEngDb = NULL;
    pPrtCx->pEngDb = NULL;

    /* De-provision these channels */
    switch (AuType(module, pWrkCx->pairPort, pWrkCx->pairAugno, pWrkCx->pairAuno))
        {
        case cAtUpsrAuTypeAu4_16c:
            LineDeProv(module, pWrkCx->pairPort);
            LineDeProv(module, pPrtCx->pairPort);
            break;
        case cAtUpsrAuTypeAu4_4c:
            Aug4DeProv(module, pWrkCx->pairPort, pWrkCx->pairAugno / 4);
            Aug4DeProv(module, pPrtCx->pairPort, pPrtCx->pairAugno / 4);
            break;
        case cAtUpsrAuTypeAu4:
            Aug1DeProv(module, pWrkCx->pairPort, pWrkCx->pairAugno);
            Aug1DeProv(module, pPrtCx->pairPort, pPrtCx->pairAugno);
            break;
        case cAtUpsrAuTypeAu3:
            Au3Tug3DeProv(module, pWrkCx->pairPort, pWrkCx->pairAugno, pWrkCx->pairAuno);
            Au3Tug3DeProv(module, pPrtCx->pairPort, pPrtCx->pairAugno, pPrtCx->pairAuno);
            break;
        default:
            break;
        }

    return cAtUpsrRetOk;
    }

/* Delete LO engine */
static eAtUpsrRet LoEngDelete(tUpsrEngDb *pEngine)
    {
    tUpsrHoCxInfo *pWrkHoCx, *pPrtHoCx;
    tUpsrLoCxInfo *pWrkLoCx, *pPrtLoCx;
    AtUpsrModule module;

    /* Get VC information */
    module = pEngine->module;
    switch (TuType(module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno))
        {
        case cAtUpsrTuTypeTu3:
            /* Get VC information */
            pWrkHoCx = Tu3Vc3(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
            pPrtHoCx = Tu3Vc3(pEngine->module, pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno);
            if ((pWrkHoCx == NULL) || (pWrkHoCx->pEngDb != pEngine) ||
                (pPrtHoCx == NULL) || (pPrtHoCx->pEngDb != pEngine))
                return cAtUpsrRetNotCreated;

            /* Reset engine handle */
            pWrkHoCx->pEngDb = NULL;
            pPrtHoCx->pEngDb = NULL;

            /* De-provision these channels */
            Au3Tug3DeProv(module, pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno);
            Au3Tug3DeProv(module, pPrtHoCx->pairPort, pPrtHoCx->pairAugno, pPrtHoCx->pairAuno);
            break;
        case cAtUpsrTuTypeTu12:
        case cAtUpsrTuTypeTu11:
            /* Get VC information */
            pWrkLoCx = TuDb(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno);
            pPrtLoCx = TuDb(pEngine->module, pWrkLoCx->pairPort, pWrkLoCx->pairAugno, pWrkLoCx->pairAuno, pWrkLoCx->pairTug2no, pWrkLoCx->pairTuno);
            if ((pWrkLoCx == NULL) || (pWrkLoCx->pEngDb != pEngine) ||
                (pPrtLoCx == NULL) || (pPrtLoCx->pEngDb != pEngine))
                return cAtUpsrRetNotCreated;

            /* Reset engine handle */
            pWrkLoCx->pEngDb = NULL;
            pPrtLoCx->pEngDb = NULL;

            /* De-provision these channels */
            TuDeProv(module, pWrkLoCx->pairPort, pWrkLoCx->pairAugno, pWrkLoCx->pairAuno, pWrkLoCx->pairTug2no, pWrkLoCx->pairTuno);
            TuDeProv(module, pPrtLoCx->pairPort, pPrtLoCx->pairAugno, pPrtLoCx->pairAuno, pPrtLoCx->pairTug2no, pPrtLoCx->pairTuno);
            break;
        default:
            break;
        }

    /* Free resource */
    AtOsalMemFree(pEngine->pUsrData);
    AtOsalMemFree(pEngine);

    return cAtUpsrRetOk;
    }

/* Update defect status */
static eAtUpsrRet HoEngDefUpdate(tUpsrEngDb *pEngDb)
    {
    eAtUpsrRet ret;
    tUpsrHoCxInfo *pWrkHoCx;
    uint32 w_defStat, p_defStat;

    ret = cAtUpsrRetOk;
    pWrkHoCx = HoCx(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no);
    w_defStat = pEngDb->engInfo.wrkDef;
    p_defStat = pEngDb->engInfo.prtDef;
    if ((w_defStat | p_defStat) == 0)
        return cAtUpsrRetOk;

    pEngDb->engInfo.wrkDef = 0;
    pEngDb->engInfo.prtDef = 0;
    switch (AuType(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no))
        {
        /* AU-4-16c */
        case cAtUpsrAuTypeAu4_16c:
            if (p_defStat)
                ret |= Aug16DefUpd(pEngDb->module,
                                   pWrkHoCx->pairPort, 0, p_defStat, p_defStat,
                                   Aug16Db(pEngDb->module, pWrkHoCx->pairPort, 0));
            if (w_defStat)
                ret |= Aug16DefUpd(pEngDb->module,
                                   pEngDb->w_port, 0, w_defStat, w_defStat,
                                   Aug16Db(pEngDb->module, pEngDb->w_port, 0));
            break;

        /* AU-4-4c */
        case cAtUpsrAuTypeAu4_4c:
            if (p_defStat)
                ret |= Aug4DefUpd(pEngDb->module,
                                  pWrkHoCx->pairPort, pWrkHoCx->pairAugno / cAtUpsrNumAug4InAug16, p_defStat, p_defStat,
                                  Aug4Db(pEngDb->module, pWrkHoCx->pairPort, pWrkHoCx->pairAugno / cAtUpsrNumAug4InAug16));
            if (w_defStat)
                ret |= Aug4DefUpd(pEngDb->module,
                                  pEngDb->w_port, pEngDb->w_augno / cAtUpsrNumAug4InAug16, w_defStat, w_defStat,
                                  Aug4Db(pEngDb->module, pEngDb->w_port, pEngDb->w_augno / cAtUpsrNumAug4InAug16));
            break;

        /* AU-4 */
        case cAtUpsrAuTypeAu4:
            if (p_defStat)
                ret |= Aug1DefUpd(pEngDb->module,
                                  pWrkHoCx->pairPort, pWrkHoCx->pairAugno, p_defStat, p_defStat,
                                  Aug1Db(pEngDb->module, pWrkHoCx->pairPort, pWrkHoCx->pairAugno));
            if (w_defStat)
                ret |= Aug1DefUpd(pEngDb->module,
                                  pEngDb->w_port, pEngDb->w_augno, w_defStat, w_defStat,
                                  Aug1Db(pEngDb->module, pEngDb->w_port, pEngDb->w_augno));
            break;

        /* AU-3 */
        case cAtUpsrAuTypeAu3:
            if (p_defStat)
                ret |= Au3Tug3DefUpd(pEngDb->module,
                                     pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno, p_defStat, p_defStat,
                                     Au3Db(pEngDb->module, pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno));
            if (w_defStat)
                ret |= Au3Tug3DefUpd(pEngDb->module,
                                     pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no, w_defStat, w_defStat,
                                     Au3Db(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no));
            break;

        /* Unknown */
        default:
            ret = cAtUpsrRetInvalid;
            break;
        }

    return ret;
    }

/* Update defect status */
static eAtUpsrRet LoEngDefUpdate(tUpsrEngDb *pEngDb)
    {
    eAtUpsrRet ret;
    tUpsrHoCxInfo *pWrkHoCx;
    tUpsrLoCxInfo *pWrkLoCx, *pPrtLoCx;
    uint32 w_defStat, p_defStat;

    ret = cAtUpsrRetOk;
    w_defStat = pEngDb->engInfo.wrkDef;
    p_defStat = pEngDb->engInfo.prtDef;
    if ((w_defStat | p_defStat) == 0)
        return cAtUpsrRetOk;
    pEngDb->engInfo.wrkDef = 0;
    pEngDb->engInfo.prtDef = 0;
    switch (TuType(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no, pEngDb->w_tug2no, pEngDb->w_tuno))
        {
        /* TU-3 */
        case cAtUpsrTuTypeTu3:
            /* Get VC information */
            pWrkHoCx = Tu3Vc3(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no);

            /* Update defect status */
            if (p_defStat)
                ret |= Au3Tug3DefUpd(pEngDb->module,
                                     pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno, p_defStat, p_defStat,
                                     Au3Tu3Db(pEngDb->module, pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno));
            if (w_defStat)
                ret |= Au3Tug3DefUpd(pEngDb->module,
                                     pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no, w_defStat, w_defStat,
                                     Au3Tu3Db(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no));
            break;

        /* TU-1x */
        case cAtUpsrTuTypeTu12:
        case cAtUpsrTuTypeTu11:
            /* Get VC information */
            pWrkLoCx = TuDb(pEngDb->module, pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no, pEngDb->w_tug2no, pEngDb->w_tuno);
            pPrtLoCx = TuDb(pEngDb->module, pWrkLoCx->pairPort, pWrkLoCx->pairAugno, pWrkLoCx->pairAuno, pWrkLoCx->pairTug2no, pWrkLoCx->pairTuno);

            /* Update defect status */
            if (p_defStat)
                mTuDefUpd(pEngDb,
                          pWrkLoCx->pairPort, pWrkLoCx->pairAugno, pWrkLoCx->pairAuno, pWrkLoCx->pairTug2no, pWrkLoCx->pairTuno,
                          p_defStat, p_defStat, pPrtLoCx);
            if (w_defStat)
                mTuDefUpd(pEngDb,
                          pEngDb->w_port, pEngDb->w_augno, pEngDb->w_au3Tug3no, pEngDb->w_tug2no, pEngDb->w_tuno,
                          w_defStat, w_defStat, pWrkLoCx);
            break;

        /* Unknown */
        default:
            ret = cAtUpsrRetInvalid;
            break;
        }

    return ret;
    }

/* Stop HO engine */
static eAtUpsrRet HoEngStop(tUpsrEngDb *pEngine)
    {
    tUpsrHoCxInfo *pWrkHoCx;

    /* Check if traffic needs to be switched back to working */
    if ((pEngine->engInfo.engState == cAtUpsrEngStateFs2Prt) ||
        (pEngine->engInfo.engState == cAtUpsrEngStateMs2Prt) ||
        (pEngine->engInfo.engState == cAtUpsrEngStateSw))
        {
        pWrkHoCx = HoCx(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
        pEngine->funcSw(pEngine,
                        pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno, 0, 0,
                        pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, 0, 0,
                        pEngine->pUsrData, pEngine->usrDataSize);
        }

    /* Set engine state to in active */
    pEngine->engInfo.engState = cAtUpsrEngStateInActive;

    return cAtUpsrRetOk;
    }

/* Stop LO engine */
static eAtUpsrRet LoEngStop(tUpsrEngDb *pEngine)
    {
    tUpsrHoCxInfo *pWrkHoCx;
    tUpsrLoCxInfo *pWrkLoCx;

    /* Check if traffic needs to be switched back to working */
    if ((pEngine->engInfo.engState == cAtUpsrEngStateFs2Prt) ||
        (pEngine->engInfo.engState == cAtUpsrEngStateMs2Prt) ||
        (pEngine->engInfo.engState == cAtUpsrEngStateSw))
        {
        switch (TuType(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno))
            {
            /* TU-3 */
            case cAtUpsrTuTypeTu3:
                pWrkHoCx = Tu3Vc3(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
                pEngine->funcSw(pEngine,
                                pWrkHoCx->pairPort, pWrkHoCx->pairAugno, pWrkHoCx->pairAuno, 0, 0,
                                pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, 0, 0,
                                pEngine->pUsrData, pEngine->usrDataSize);
                break;

            /* TU-1x */
            case cAtUpsrTuTypeTu12:
            case cAtUpsrTuTypeTu11:
                pWrkLoCx = TuDb(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno);
                pEngine->funcSw(pEngine,
                                pWrkLoCx->pairPort, pWrkLoCx->pairAugno, pWrkLoCx->pairAuno, pWrkLoCx->pairTug2no, pWrkLoCx->pairTuno,
                                pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno,
                                pEngine->pUsrData, pEngine->usrDataSize);
                break;

            /* Unknown */
            default:
                return cAtUpsrRetInvalid;
            }
        }

    /* Set engine state to in active */
    pEngine->engInfo.engState = cAtUpsrEngStateInActive;
    return cAtUpsrRetOk;
    }

/**
 * Create UPSR module
 *
 * @param maxNumLine Maximum number of line
 * @param pOsIntf OS interface
 *
 * @return
 */
AtUpsrModule AtUpsrModuleCreate(uint8 maxNumLine, const tUpsrOsIntf *pOsIntf)
    {
    tUpsrDb *pDb;

    /* Cannot create with number of lines is 0 */
    if (maxNumLine == 0)
        return NULL;

    /* Create database */
    pDb = AtOsalMemAlloc(sizeof(tUpsrDb));
    if (pDb == NULL)
        return NULL;
    AtOsalMemInit(pDb, 0, sizeof(tUpsrDb));

    /* Save OS interface */
    if (!ModuleSaveOsIntf(pDb, pOsIntf))
        {
        AtOsalMemFree(pDb);
        return NULL;
        }

    /* Create semaphore to protect this module */
    pDb->sem = pOsIntf->semCreate();
    if (pDb->sem == NULL)
        {
        AtOsalMemFree(pDb);
        return NULL;
        }

    /* Create Line's database */
    if (!LineDbCreate(pDb, maxNumLine))
        {
        pOsIntf->semDelete(pDb->sem);
        AtOsalMemFree(pDb);
        }

    return pDb;
    }

/**
 * Stop UPSR module
 */
eAtUpsrRet AtUpsrModuleDelete(AtUpsrModule module)
    {
    uint8 port;
    tUpsrDb *pModule;

    /* Error on null module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Delete semaphore */
    pModule = module;
    pModule->osIntf.semDelete(pModule->sem);

    /* Free all allocated resources */
    for (port = 0; port < pModule->maxNumLine; port++)
        LineFree(module, port);

    /* Free database */
    LineDbDelete(module);
    AtOsalMemFree(module);

    return cAtUpsrRetOk;
    }

/**
 * Period processing function. This API must be called by application if revertive
 * switching type is used. When defect is clear on working channel, WTR state may
 * be activated, this function decreases WTR timer on each run. Upon WTR expire,
 * switch is released.
 *
 * @param module Module
 *
 * @retval 0 if no engine enter WTR state
 * @retval N (N > 0) Number of engines that are entering WTR state
 */
uint32 AtUpsrModulePeriodProcess(AtUpsrModule module)
    {
	Unused(module);
    /* TODO: At this time, revertive mode has not been supported, so it is not
     * necessary this task to process WTR ==> this task will be exit right away
     * after it is created */
#if 0
    while (1)
        {
        OsalSleep(pModule->idleTaskPeriod);

        /*
         * Pseudo code:
         *
         * for each engine in WTR state
         *     {
         *     if (WTR timer expires)
         *         {
         *         call swFunc to switch back to the working channel
         *         change engine state to IDLE
         *         }
         *     }
         */
        }
#endif

    return 0;
    }

/**
 * Traversing function. This function will traverse all provisioned engines and
 * call the traverse function
 *
 * @param [in] traverseFunc Traverse function
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrModuleTraverse(AtUpsrModule module, UpsrEngTraverseFunc traverseFunc)
    {
    uint8 idx, port;
    uint8 *provLine;
    uint8 numProvLine;
    tUpsrDb *pModule;
    uint16 size;

    /* Do nothing if traverse function is NULL or NULL module */
    if ((traverseFunc == NULL) || (module == NULL))
        return cAtUpsrRetOk;

    /* During traversing, engine can be deleted, so save provisioned list
     * first to make sure that all of channels are traversed */
    pModule = module;
    size = sizeof(uint8) * pModule->maxNumLine;
    provLine = AtOsalMemAlloc(size);
    if (provLine == NULL)
        return cAtUpsrRetOsRscAlloc;
    AtOsalMemCpy(provLine, pModule->provLine, size);
    numProvLine = pModule->numProvLine;

    /* Traverse all provisioned lines */
    for (idx = 0; idx < numProvLine; idx++)
        {
        port = provLine[idx];
        LineTraverse(traverseFunc, port, &(pModule->lineDb[port]));
        }

    /* Free allocated resource */
    AtOsalMemFree(provLine);

    return cAtUpsrRetOk;
    }

/**
 * Set AUG-16 mapping
 *
 * @param module Module
 * @param port Port number
 * @param augno AUG-16 number. It is ignored at this time
 * @param mapType @ref eAtUpsrAug16MapType "Mapping type"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet UpsrAug16MapSet(AtUpsrModule module, uint8 port, uint8 augno, eAtUpsrAug16MapType mapType)
    {
    tUpsrAug16Db *pAugDb;
    uint8 idx;
    Unused(augno);

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Error if mapping is invalid */
    if (!mAug16MapTypeIsValid(mapType))
        return cAtUpsrRetInvalid;

    /* Check if this AUG exists */
    pAugDb = Aug16Db(module, port, 0);
    if (pAugDb == NULL)
        return cAtUpsrRetInvalid;

    /* Do nothing if mapping is not changed */
    if (pAugDb->mapType == mapType)
        return cAtUpsrRetOk;

    /* Error if engines exist */
    if (Aug16ContainEngine(pAugDb))
        return cAtUpsrRetBusy;

    /* Initialize all sub database */
    AtOsalMemInit(pAugDb, 0, sizeof(tUpsrAug16Db));
    pAugDb->mapType = mapType;
    if (mapType == cAtUpsrAug16MapTypeAug4)
        {
        pAugDb->mapInfo.aug4.numProvAug4 = 0;
        for (idx = 0; idx < cAtUpsrNumAug4InAug16; idx++)
            pAugDb->mapInfo.aug4.provAug4[idx] = cAtUpsrUnknown;
        }

    return cAtUpsrRetOk;
    }

/**
 * Get AUG-16 mapping
 *
 * @param module Module
 * @param port Port number
 * @param augno AUG-16 number. It is ignored at this time
 *
 * @return @ref eAtUpsrAug16MapType "Mapping type"
 */
eAtUpsrAug16MapType UpsrAug16MapGet(AtUpsrModule module, uint8 port, uint8 augno)
    {
    tUpsrAug16Db *pAugDb;
    Unused(augno);
    /* Cannot get mapping for NULL module */
    if (module == NULL)
        return cAtUpsrUnknown;

    pAugDb = Aug16Db(module, port, 0);
    return (pAugDb == NULL) ? cAtUpsrUnknown : pAugDb->mapType;
    }

/**
 * Set AUG-4 mapping
 *
 * @param module Module
 * @param port Port number
 * @param augno [0..3] AUG-4 number
 * @param mapType @ref eAtUpsrAug4MapType "Mapping type"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet UpsrAug4MapSet(AtUpsrModule module, uint8 port, uint8 augno, eAtUpsrAug4MapType mapType)
    {
    tUpsrAug4Db *pAugDb;
    uint8 idx;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Error on invalid mapping type */
    if (!mAug4MapTypeIsValid(mapType))
        return cAtUpsrRetInvalid;

    /* Error when it does not exist */
    pAugDb = Aug4Db(module, port, augno);
    if (pAugDb == NULL)
        return cAtUpsrRetNotCreated;
    if (pAugDb->mapType == mapType)
        return cAtUpsrRetOk;

    /* Error if AUG-4 contains any engine */
    if (Aug4ContainEngine(pAugDb))
        return cAtUpsrRetBusy;

    /* Initialize all sub database */
    AtOsalMemInit(pAugDb, 0, sizeof(tUpsrAug4Db));
    pAugDb->mapType = mapType;
    if (mapType == cAtUpsrAug4MapTypeAug1)
        {
        pAugDb->mapInfo.aug1.numProvAug1 = 0;
        for (idx = 0; idx < cAtUpsrNumAug1InAug4; idx++)
            pAugDb->mapInfo.aug1.provAug1[idx] = cAtUpsrUnknown;
        }

    return cAtUpsrRetOk;
    }

/**
 * Get AUG-4 mapping
 *
 * @param module Module
 * @param port Port number
 * @param augno [0..3] AUG-4 number
 *
 * @return @ref eAtUpsrAug4MapType "Mapping type"
 */
eAtUpsrAug4MapType UpsrAug4MapGet(AtUpsrModule module, uint8 port, uint8 augno)
    {
    tUpsrAug4Db *pAugDb;

    /* Cannot get mapping for NULL module */
    if (module == NULL)
        return cAtUpsrUnknown;

    pAugDb = Aug4Db(module, port, augno);
    return (pAugDb == NULL) ? cAtUpsrUnknown : pAugDb->mapType;
    }

/**
 * Set AUG-1 mapping
 *
 * @param module Module
 * @param port Port number
 * @param augno AUG number
 * @param mapType @ref eAtUpsrAug1MapType "Mapping type"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet UpsrAug1MapSet(AtUpsrModule module, uint8 port, uint8 augno, eAtUpsrAug1MapType mapType)
    {
    tUpsrAug1Db *pAugDb;
    uint8 idx;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Error on invalid mapping */
    if (!mAug1MapTypeIsValid(mapType))
        return cAtUpsrRetInvalid;

    /* Error when it does not exists */
    pAugDb = Aug1Db(module, port, augno);
    if (pAugDb == NULL)
        return cAtUpsrRetNotCreated;
    if (pAugDb->mapType == mapType)
        return cAtUpsrRetOk;

    /* Error when AUG-1 contains any engine */
    if (Aug1ContainEngine(pAugDb))
        return cAtUpsrRetBusy;

    /* Initialize all sub database */
    AtOsalMemInit(pAugDb, 0, sizeof(tUpsrAug1Db));
    pAugDb->mapType = mapType;
    if ((mapType == cAtUpsrAug1MapTypeAu3) || (mapType == cAtUpsrAug1MapTypeTug3))
        {
        pAugDb->mapInfo.au3Tug3.numProvAu3Tug3 = 0;
        for (idx = 0; idx < 3; idx++)
            pAugDb->mapInfo.au3Tug3.provAu3Tug3[idx] = cAtUpsrUnknown;
        }

    return cAtUpsrRetOk;
    }

/**
 * Get AUG-1 mapping
 *
 * @param module Module
 * @param port Port number
 * @param augno AUG number
 *
 * @return @ref eAtUpsrAug1MapType "Mapping type"
 */
eAtUpsrAug1MapType UpsrAug1MapGet(AtUpsrModule module, uint8 port, uint8 augno)
    {
    tUpsrAug1Db *pAugDb;

    /* Cannot get mapping for NULL module */
    if (module == NULL)
        return cAtUpsrUnknown;

    pAugDb = Aug1Db(module, port, augno);
    return (pAugDb == NULL) ? cAtUpsrUnknown : pAugDb->mapType;
    }

/**
 * Set AU-3 mapping
 *
 * @param module Module
 * @param port Port
 * @param augno [0..15] AUG-1
 * @param auno [0..2] AU-3 ID
 * @param mapType @ref eAtUpsrAu3Tug3MapType "Mapping type"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet UpsrAu3MapSet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno, eAtUpsrAu3Tug3MapType mapType)
    {
    tUpsrAu3Tug3Db *pAu3Db;
    uint8 idx;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Error on invalid mapping type */
    if (!mAu3Tug3MapTypeIsValid(mapType))
        return cAtUpsrRetInvalid;

    /* Error when it does not exist */
    pAu3Db = Au3Db(module, port, augno, auno);
    if (pAu3Db == NULL)
        return cAtUpsrRetNotCreated;
    if (pAu3Db->mapType == mapType)
        return cAtUpsrRetOk;

    /* Error when AU-3 contains any engine */
    if (Au3Tug3ContainEngine(pAu3Db))
        return cAtUpsrRetBusy;

    /* Initialize all sub database */
    AtOsalMemInit(pAu3Db, 0, sizeof(tUpsrAu3Tug3Db));
    pAu3Db->mapType = mapType;
    if (mapType == cAtUpsrAu3Tug3MapTypeTug2)
        {
        pAu3Db->mapInfo.tug2Info.numProvTug2 = 0;
        for (idx = 0; idx < cAtUpsrNumTug2InAu3Tug3; idx++)
            pAu3Db->mapInfo.tug2Info.provTug2[idx] = cAtUpsrUnknown;
        }

    return cAtUpsrRetOk;
    }

/**
 * Get AU-3 mapping
 *
 * @param module Module
 * @param port Port
 * @param augno [0..15] AUG-1
 * @param auno [0..2] AU-3 ID
 *
 * @return @ref eAtUpsrAu3Tug3MapType "Mapping type"
 */
eAtUpsrAu3Tug3MapType UpsrAu3MapGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrAu3Tug3Db *pAu3Db;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrUnknown;

    pAu3Db = Au3Db(module, port, augno, auno);
    return (pAu3Db == NULL) ? cAtUpsrUnknown : pAu3Db->mapType;
    }

/**
 * Set TUG-3 mapping
 *
 * @param module Module
 * @param port Port
 * @param augno [0..15] AUG-1
 * @param auno [0..2] TUG-3 ID
 * @param mapType @ref eAtUpsrAu3Tug3MapType "Mapping type"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet UpsrTug3MapSet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno, eAtUpsrAu3Tug3MapType mapType)
    {
    tUpsrAu3Tug3Db *pTug3Db;
    uint8 idx;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Error on invalid mapping type */
    if (!mAu3Tug3MapTypeIsValid(mapType))
        return cAtUpsrRetInvalid;

    /* Error when it does not exist */
    pTug3Db = Tug3Db(module, port, augno, auno);
    if (pTug3Db == NULL)
        return cAtUpsrRetNotCreated;
    if (pTug3Db->mapType == mapType)
        return cAtUpsrRetOk;

    /* Error when TUG-3 contains any engine */
    if (Au3Tug3ContainEngine(pTug3Db))
        return cAtUpsrRetBusy;

    /* Initialize all sub database */
    AtOsalMemInit(pTug3Db, 0, sizeof(tUpsrAu3Tug3Db));
    pTug3Db->mapType = mapType;
    if (mapType == cAtUpsrAu3Tug3MapTypeTug2)
        {
        pTug3Db->mapInfo.tug2Info.numProvTug2 = 0;
        for (idx = 0; idx < cAtUpsrNumTug2InAu3Tug3; idx++)
            pTug3Db->mapInfo.tug2Info.provTug2[idx] = cAtUpsrUnknown;
        }

    return cAtUpsrRetOk;
    }

/**
 * Get TUG-3 mapping
 *
 * @param module Module
 * @param port Port
 * @param augno [0..15] AUG-1
 * @param auno [0..2] TUG-3 ID
 *
 * @return @ref eAtUpsrAu3Tug3MapType "Mapping type"
 */
eAtUpsrAu3Tug3MapType UpsrTug3MapGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrAu3Tug3Db *pTug3Db;

    /* Cannot get mapping for NULL module */
    if (module == NULL)
        return cAtUpsrUnknown;

    pTug3Db = Tug3Db(module, port, augno, auno);
    return (pTug3Db == NULL) ? cAtUpsrUnknown : pTug3Db->mapType;
    }

/**
 * Set TUG-2 mapping
 *
 * @param module Module
 * @param port Port
 * @param augno AUG
 * @param au3Tug3no AU-3/TUG-3
 * @param tug2no TUG-2
 * @param mapType @ref eAtUpsrTug2MapType "Mapping type"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet UpsrTug2MapSet(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, eAtUpsrTug2MapType mapType)
    {
    tUpsrTug2Db *pTug2Db;
    uint8 idx;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Error on invalid mapping type */
    if (!mTug2MapTypeIsValid(mapType))
        return cAtUpsrRetInvalid;

    /* Error when it does not exist */
    pTug2Db = Tug2Db(module, port, augno, au3Tug3no, tug2no);
    if (pTug2Db == NULL)
        return cAtUpsrRetNotCreated;
    if (pTug2Db->mapType == mapType)
        return cAtUpsrRetOk;

    /* Error when TUG-2 contains any engine */
    if (Tug2ContainEngine(pTug2Db))
        return cAtUpsrRetBusy;

    /* Initialize all sub database */
    AtOsalMemInit(pTug2Db, 0, sizeof(tUpsrTug2Db));
    pTug2Db->mapType = mapType;
    pTug2Db->numProvTu = 0;
    for (idx = 0; idx < 4; idx++)
        pTug2Db->provTu[idx] = cAtUpsrUnknown;

    return cAtUpsrRetOk;
    }

/**
 * Get TUG-2 mapping
 *
 * @param module Module
 * @param port Port
 * @param augno AUG
 * @param au3Tug3no AU-3/TUG-3
 * @param tug2no TUG-2
 *
 * @return @ref eAtUpsrTug2MapType "Mapping type"
 */
eAtUpsrTug2MapType UpsrTug2MapGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no)
    {
    tUpsrTug2Db *pTug2Db;

    /* Cannot set mapping for NULL module */
    if (module == NULL)
        return cAtUpsrUnknown;

    pTug2Db = Tug2Db(module, port, augno, au3Tug3no, tug2no);
    return (pTug2Db == NULL) ? cAtUpsrUnknown : pTug2Db->mapType;
    }

/**
 * Create a high-order UPSR engine
 *
 * @param module   UPSR module
 * @param w_port   Working Port
 * @param w_augno  Working AUG-1
 * @param w_auno   Working AU-3
 *
 * @param p_port   Protection Port
 * @param p_augno  Protection AUG-1
 * @param p_auno   Protection AU-3
 *
 * @param swType Switching type.
 *               - cAtUpsrEngSwTypeNonRev : Non-revertive switching type
 *               - cAtUpsrEngSwTypeRev    : Revertive switching type
 * @param wtrTime Wait-to-restore time if switching type is revertive.
 * @param swFunc Switching function. It will be called when engine perform
 *               switching
 * @param pUsrData User data. The user data is allocated and stored in database.
 *                 It may contain the real hardware information (working,
 *                 protection and traffic) or other user information. This data
 *                 will be input when switching happen so the application can
 *                 determine hardware resource immediately
 * @param usrDataSize Size of user data
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 * @note After creating, engine is in in-active state. The input application
 *       interfaces can be NULL
 */
AtUpsrEngine AtUpsrHoEngCreate(AtUpsrModule module,
                           uint8 w_port, uint8 w_augno, uint8 w_auno,
                           uint8 p_port, uint8 p_augno, uint8 p_auno,
                           eAtUpsrSwType swType,
                           uint32 wtrTime,
                           AtUpsrEngSwFunc swFunc,
                           void *pUsrData, uint32 usrDataSize)
    {
    eAtUpsrRet ret;
    uint8 auType;
    tUpsrHoCxInfo *pWrkCx, *pPrtCx;
    tUpsrEngDb *pEngDb;

    /* Cannot create engine in NULL module */
    if (module == NULL)
        return NULL;

    /* Check two channels */
    auType = AuType(module, w_port, w_augno, w_auno);
    if ((auType != AuType(module, p_port, p_augno, p_auno)) ||
        (auType == cAtUpsrUnknown))
        return NULL;

    /* Cannot create engine if two channels are same */
    if ((w_port == p_port) && (w_augno == p_augno) &&
        ((auType != cAtUpsrAuTypeAu3) || (w_auno == p_auno)))
        return NULL;

    /* Cannot create engine if one of channels belongs to another engine */
    pWrkCx = HoCx(module, w_port, w_augno, w_auno);
    pPrtCx = HoCx(module, p_port, p_augno, p_auno);
    if ((pWrkCx == NULL) || (pPrtCx == NULL) || (pWrkCx->pEngDb != NULL) || (pPrtCx->pEngDb != NULL))
        return NULL;

    /* Create engine */
    if (auType != cAtUpsrAuTypeAu3)
        w_auno = p_auno = 0;
    pEngDb = EngDbCreate(module,
                       w_port, w_augno, w_auno, 0, 0,
                       swType, wtrTime, swFunc, pUsrData, usrDataSize);
    if (pEngDb == NULL)
        return NULL;

    /* Add these channels to provisioned list */
    ret = cAtUpsrRetOk;
    switch (auType)
        {
        case cAtUpsrAuTypeAu4_16c:
            ret |= LineProv(module, w_port);
            ret |= LineProv(module, p_port);
            break;
        case cAtUpsrAuTypeAu4_4c:
            ret |= Aug4Prov(module, w_port, w_augno / 4);
            ret |= Aug4Prov(module, p_port, p_augno / 4);
            break;
        case cAtUpsrAuTypeAu4:
            ret |= Aug1Prov(module, w_port, w_augno);
            ret |= Aug1Prov(module, p_port, p_augno);
            break;
        case cAtUpsrAuTypeAu3:
            ret |= Au3Tug3Prov(module, w_port, w_augno, w_auno);
            ret |= Au3Tug3Prov(module, p_port, p_augno, p_auno);
            break;

        default:
            break;
        }

    /* Error may happen */
    if (ret != cAtUpsrRetOk)
        {
        AtOsalMemFree(pEngDb);
        return NULL;
        }

    /* Store configuration for working channel */
    pWrkCx->pEngDb    = pEngDb;
    pWrkCx->isWrk     = cAtTrue;
    pWrkCx->pairPort  = p_port;
    pWrkCx->pairAugno = p_augno;
    pWrkCx->pairAuno  = p_auno;

    /* Store configuration for protection channel */
    pPrtCx->pEngDb    = pEngDb;
    pPrtCx->isWrk     = cAtFalse;
    pPrtCx->pairPort  = w_port;
    pPrtCx->pairAugno = w_augno;
    pPrtCx->pairAuno  = w_auno;

    /* Make HO engine indication */
    pEngDb->isHo = cAtTrue;

    return pEngDb;
    }

/**
 * Create a low-order UPSR engine
 *
 * @param module      UPSR module
 *
 * @param w_port      Working Port
 * @param w_augno     Working AUG-1
 * @param w_au3Tug3no Working AU-3/TUG-3
 * @param w_tug2no    Working TUG-2 ID
 * @param w_tuno      Working TU ID
 *
 * @param w_port      Protection Port
 * @param w_augno     Protection AUG-1
 * @param w_au3Tug3no Protection AU-3/TUG-3
 * @param w_tug2no    Protection TUG-2 ID
 * @param w_tuno      Protection TU ID
 *
 * @param swType Switching type.
 *               - cAtUpsrEngSwTypeNonRev : Non-revertive switching type
 *               - cAtUpsrEngSwTypeRev    : Revertive switching type
 * @param wtrTime Wait-to-restore time if switching type is revertive.
 * @param swFunc Switching function. It will be called when engine perform
 *               switching
 * @param pUsrData User data. The user data is allocated and stored in database.
 *                 It may contain the real hardware information (working,
 *                 protection and traffic) or other user information. This data
 *                 will be input when switching happen so the application can
 *                 determine hardware resource immediately
 * @param usrDataSize Size of user data
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 * @note After creating, engine is in in-active state. The input application
 *       interfaces can be NULL
 */
AtUpsrEngine AtUpsrLoEngCreate(AtUpsrModule module,
                           uint8 w_port, uint8 w_augno, uint8 w_au3Tug3no, uint8 w_tug2no, uint8 w_tuno,
                           uint8 p_port, uint8 p_augno, uint8 p_au3Tug3no, uint8 p_tug2no, uint8 p_tuno,
                           eAtUpsrSwType swType,
                           uint32 wtrTime,
                           AtUpsrEngSwFunc swFunc,
                           void *pUsrData, uint32 usrDataSize)
    {
    eAtUpsrRet ret;
    uint8 tuType;
    tUpsrHoCxInfo *pWrkHoCx, *pPrtHoCx;
    tUpsrLoCxInfo *pWrkLoCx, *pPrtLoCx;
    tUpsrEngDb *pEngDb;

    /* Cannot create engine in NULL module */
    if (module == NULL)
        return NULL;

    /* Check two channels */
    tuType = TuType(module, w_port, w_augno, w_au3Tug3no, w_tug2no, w_tuno);
    if ((tuType != TuType(module, p_port, p_augno, p_au3Tug3no, p_tug2no, p_tuno)) ||
        (tuType == cAtUpsrUnknown))
        return NULL;

    ret = cAtUpsrRetOk;
    pEngDb = NULL;
    switch (tuType)
        {
        /* TU-3 */
        case cAtUpsrTuTypeTu3:
            /* Get HO VCs and cannot create engine if one of channel belongs to
             * another engine */
            pWrkHoCx = Tu3Vc3(module, w_port, w_augno, w_au3Tug3no);
            pPrtHoCx = Tu3Vc3(module, p_port, p_augno, p_au3Tug3no);
            if ((pWrkHoCx == NULL) || (pPrtHoCx == NULL) || (ret != cAtUpsrRetOk) ||
                (pWrkHoCx->pEngDb != NULL) || (pPrtHoCx->pEngDb != NULL))
                return NULL;

            /* Create engine */
            pEngDb = EngDbCreate(module,
                               w_port, w_augno, w_au3Tug3no, w_tug2no, w_tuno,
                               swType, wtrTime, swFunc, pUsrData, usrDataSize);
            if (pEngDb == NULL)
                return NULL;
            pEngDb->engInfo.swCond |= cAtUpsrSwCondLo;

            /* Add to provision list */
            ret |= Au3Tug3Prov(module, w_port, w_augno, w_au3Tug3no);
            ret |= Au3Tug3Prov(module, p_port, p_augno, p_au3Tug3no);
            if (ret != cAtUpsrRetOk)
                {
                EngDelete(pEngDb);
                return NULL;
                }

            /* Store configuration for working channel */
            pWrkHoCx->pEngDb    = pEngDb;
            pWrkHoCx->isWrk     = cAtTrue;
            pWrkHoCx->pairPort  = p_port;
            pWrkHoCx->pairAugno = p_augno;
            pWrkHoCx->pairAuno  = p_au3Tug3no;

            /* Store configuration for protection channel */
            pPrtHoCx->pEngDb    = pEngDb;
            pPrtHoCx->isWrk     = cAtFalse;
            pPrtHoCx->pairPort  = w_port;
            pPrtHoCx->pairAugno = w_augno;
            pPrtHoCx->pairAuno  = w_au3Tug3no;
            break;

        /* TU-1x */
        case cAtUpsrTuTypeTu11:
        case cAtUpsrTuTypeTu12:
            /* Get LO VCs and cannot create engine if one of channels belongs to
             * another engine */
            pWrkLoCx = TuDb(module, w_port, w_augno, w_au3Tug3no, w_tug2no, w_tuno);
            pPrtLoCx = TuDb(module, p_port, p_augno, p_au3Tug3no, p_tug2no, p_tuno);
            if ((pWrkLoCx == NULL) || (pPrtLoCx == NULL) || (ret != cAtUpsrRetOk) ||
                (pWrkLoCx->pEngDb != NULL) || (pPrtLoCx->pEngDb != NULL))
                return NULL;

            /* Create engine */
            pEngDb = EngDbCreate(module,
                               w_port, w_augno, w_au3Tug3no, w_tug2no, w_tuno,
                               swType, wtrTime, swFunc, pUsrData, usrDataSize);
            if (pEngDb == NULL)
                return NULL;
            pEngDb->engInfo.swCond |= cAtUpsrSwCondLo;

            /* Add to provision list */
            ret |= TuProv(module, w_port, w_augno, w_au3Tug3no, w_tug2no, w_tuno);
            ret |= TuProv(module, p_port, p_augno, p_au3Tug3no, p_tug2no, p_tuno);
            if (ret != cAtUpsrRetOk)
                {
                EngDelete(pEngDb);
                return NULL;
                }

            /* Store configuration for working channel */
            pWrkLoCx->pEngDb     = pEngDb;
            pWrkLoCx->isWrk      = cAtTrue;
            pWrkLoCx->pairPort   = p_port;
            pWrkLoCx->pairAugno  = p_augno;
            pWrkLoCx->pairAuno   = p_au3Tug3no;
            pWrkLoCx->pairTug2no = p_tug2no;
            pWrkLoCx->pairTuno   = p_tuno;

            /* Store configuration for protection channel */
            pPrtLoCx->pEngDb     = pEngDb;
            pPrtLoCx->isWrk      = cAtFalse;
            pPrtLoCx->pairPort   = w_port;
            pPrtLoCx->pairAugno  = w_augno;
            pPrtLoCx->pairAuno   = w_au3Tug3no;
            pPrtLoCx->pairTug2no = w_tug2no;
            pPrtLoCx->pairTuno   = w_tuno;
            break;

        default:
            break;
        }

    /* Make HO engine indication */
    pEngDb->isHo = cAtFalse;

    return pEngDb;
    }

/**
 * Delete an UPSR engine
 *
 * @param chnType Channel type
 *                - cAtUpsrChnTypeAu   : AU-4/AU-3
 *                - cAtUpsrChnTypeTu3  : TU-3
 *                - cAtUpsrChnTypeTu1x : VT/TU
 * @param port Port
 * @param augno AUG
 * @param auno AU-3/TUG-3
 * @param tug2no TUG-2
 * @param tuno TU
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 *
 * @note Input channel can be working or protection channel
 */
eAtUpsrRet AtUpsrEngDelete(AtUpsrEngine engine)
    {
    /* Do nothing if engine has not been created */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* Delete engine */
    if (((tUpsrEngDb*)engine)->isHo)
        return HoEngDelete(engine);
    else
        return LoEngDelete(engine);
    }

/**
 * Start an UPSR engine.
 *
 * @param engine Engine to start
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 *
 * @note Input channel can be working or protection channel
 */
eAtUpsrRet AtUpsrEngStart(AtUpsrEngine engine)
    {
    eAtUpsrRet ret;
    tUpsrEngDb *pEngDb;
    uint8 extCmd;

    /* Cannot start a NULL module */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* Get engine database */
    ret = cAtUpsrRetOk;
    pEngDb = engine;

    /* Start the engine */
    if (pEngDb->engInfo.engState == cAtUpsrEngStateInActive)
        pEngDb->engInfo.engState = cAtUpsrEngStateIdle;
    else
        return cAtUpsrRetOk;

    /* Take this database */
    mUpsrSemTake(pEngDb->module);

    /* Make sure that there is no garbage bit */
    pEngDb->engInfo.wrkDef &= (cAtUpsrSwCondAll | cAtUpsrEngSwCondInvl);
    pEngDb->engInfo.prtDef &= (cAtUpsrSwCondAll | cAtUpsrEngSwCondInvl);

    /* Apply external command */
    extCmd = pEngDb->engInfo.extlCmd;
    if (((extCmd == cAtUpsrExtlCmdFs2Wrk) ||
         (extCmd == cAtUpsrExtlCmdFs2Prt) ||
         (extCmd == cAtUpsrExtlCmdLp)) ||
        ((extCmd == cAtUpsrExtlCmdMs2Wrk) && (pEngDb->engInfo.wrkDef == 0)) ||
        ((extCmd == cAtUpsrExtlCmdMs2Prt) && (pEngDb->engInfo.prtDef == 0)))
        ret = AtUpsrEngExtlCmdSet(engine, extCmd);

    /* HO engine */
    if (pEngDb->isHo)
        ret = HoEngDefUpdate(pEngDb);

    /* LO engine */
    else
        ret = LoEngDefUpdate(pEngDb);

    mUpsrSemGive(pEngDb->module);

    return ret;
    }

/**
 * Stop an UPSR engine
 *
 * @param engine Engine to stop
 * @return cAtUpsrRetOk On success and other error code on failure
 *
 * @note Input channel can be working or protection channel
 */
eAtUpsrRet AtUpsrEngStop(AtUpsrEngine engine)
    {
    eAtUpsrRet ret;

    /* Do nothing if engine is NULL */
    if (engine == NULL)
        return cAtUpsrRetOk;

    /* Stop engine */
    if (((tUpsrEngDb *)engine)->isHo)
        ret = HoEngStop(engine);
    else
        ret = LoEngStop(engine);

    return ret;
    }

/**
 * Force engine to a specified state. It is useful when warm restore the engine
 *
 * @param engine Engine
 * @param [in] state @ref eAtUpsrEngState "Engine state"
 *
 * @return
 */
eAtUpsrRet AtUpsrEngStateSet(AtUpsrEngine engine, eAtUpsrEngState state)
    {
    /* Cannot configure a NULL engine */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* Update database */
    ((tUpsrEngDb*)engine)->engInfo.engState = state;

    return cAtUpsrRetOk;
    }

/**
 * Get UPSR engine type
 *
 * @param engine Engine
 *
 * @return @ref eAtUpsrEngType "Engine type"
 */
eAtUpsrEngType AtUpsrEngTypeGet(AtUpsrEngine engine)
    {
    tUpsrEngDb *pEngine;

    /* Type of NULL engine is unknown */
    if (engine == NULL)
        return cAtUpsrUnknown;

    /* HO engine */
    pEngine = engine;
    if (pEngine->isHo)
        return AuType(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
    else
        return TuType(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno);
    }

/**
 * Get UPSR engine state
 *
 * @param engine Engine
 *
 * @return @ref eAtUpsrEngState "Engine state"
 */
eAtUpsrEngState AtUpsrEngStateGet(AtUpsrEngine engine)
    {
    /* Engine state if NULL engine is unknown */
    if (engine == NULL)
        return cAtUpsrUnknown;

    return ((tUpsrEngDb *)engine)->engInfo.engState;
    }

/**
 * Set WTR time
 *
 * @param [in] engine Engine
 * @param [in] wtr WTR time in seconds
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrEngWtrSet(AtUpsrEngine engine, uint32 wtr)
    {
    /* Cannot set WTR of NULL engine */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* WTR is not applicable for non-revertive engine */
    if (AtUpsrEngSwTypeGet(engine) == cAtUpsrSwTypeNonRev)
        return cAtUpsrRetNotSupport;

    /* Update database */
    ((tUpsrEngDb*)engine)->engInfo.wtrTime = wtr;

    return cAtUpsrRetOk;
    }

/**
 * Get WTR time
 *
 * @param engine Engine
 *
 * @return WTR time
 */
uint32 AtUpsrEngWtrGet(AtUpsrEngine engine)
    {
    /* WTR of NULL engine or non-revertive engine is 0 */
    if ((engine == NULL) || (AtUpsrEngSwTypeGet(engine) == cAtUpsrSwTypeNonRev))
        return 0;

    return ((tUpsrEngDb *)engine)->engInfo.wtrTime;
    }

/**
 * Set switching type
 *
 * @param engine Engine
 * @param [in] swType @ref eAtUpsrEngSwType "Switching type:
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrEngSwTypeSet(AtUpsrEngine engine, eAtUpsrSwType swType)
    {
    tUpsrEngDb *pEngDb;
    eAtUpsrRet ret;

    /* Cannot change switching type for NULL engine */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* Get database and check if configuration is changed */
    pEngDb = engine;
    if (pEngDb->engInfo.swType == swType)
        return cAtUpsrRetOk;

    mUpsrSemTake(pEngDb->module);

    /* Try to change switching type */
    ret = cAtUpsrRetOk;
    switch (swType)
        {
        /* Non-revertive switching */
        case cAtUpsrSwTypeNonRev:
            /* Clear WTR state */
            if (pEngDb->engInfo.engState == cAtUpsrEngStateWtr)
                pEngDb->engInfo.engState = cAtUpsrEngStateSw;
            break;

        /* Revertive switching */
        case cAtUpsrSwTypeRev:
            /* Activate WTR state */
            if ((pEngDb->engInfo.engState == cAtUpsrEngStateSw) &&
                (!pEngDb->engInfo.wrkDef) &&
                (!pEngDb->engInfo.prtDef))
                pEngDb->engInfo.engState = cAtUpsrEngStateWtr;
            break;

        /* Not supported */
        default:
            ret = cAtUpsrRetNotSupport;
            break;
        }

    /* Update database */
    if (ret == cAtUpsrRetOk)
        pEngDb->engInfo.swType = swType;

    mUpsrSemGive(pEngDb->module);

    return ret;
    }

/**
 * Get switching type
 *
 * @param engine Engine
 *
 * @return @ret eAtUpsrEngSwType "Switching type"
 */
eAtUpsrSwType AtUpsrEngSwTypeGet(AtUpsrEngine engine)
    {
    /* Switching type of NULL engine is unknown */
    if (engine == NULL)
        return cAtUpsrUnknown;

    return ((tUpsrEngDb*)engine)->engInfo.swType;
    }

/**
 * To get working and protection channels that form this engine
 *
 * @param [in] engine Engine
 * @param [out] w_port      [0..N]  Working port
 * @param [out] w_augno     [0..15] Working AUG-1
 * @param [out] w_au3Tug3no [0..2]  Working AU-3/TUG-3
 * @param [out] w_tug2no    [0..6]  Working TUG-2
 * @param [out] w_tuno      [0..3]  Working TU-1x
 * @param [out] p_port      [0..N]  Protection port
 * @param [out] p_augno     [0..15] Protection AUG-1
 * @param [out] p_au3Tug3no [0..2]  Protection AU-3/TUG-3
 * @param [out] p_tug2no    [0..6]  Protection TUG-2
 * @param [out] p_tuno      [0..3]  Protection TU-1x
 * @return
 */
eAtUpsrRet AtUpsrEngChnGet(AtUpsrEngine engine,
                       uint8 *w_port, uint8 *w_augno, uint8 *w_au3Tug3no, uint8 *w_tug2no, uint8 *w_tuno,
                       uint8 *p_port, uint8 *p_augno, uint8 *p_au3Tug3no, uint8 *p_tug2no, uint8 *p_tuno)
    {
    tUpsrEngDb *pEngine;
    tUpsrHoCxInfo *pHoCx;
    tUpsrLoCxInfo *pLoCx;

    /* Error on NULL engine */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* HO engine */
    pEngine = engine;
    if (pEngine->isHo)
        {
        pHoCx = HoCx(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
        if (pHoCx == NULL)
            return cAtUpsrRetNotCreated;

        /* Return the working channel */
        if (w_port != NULL) *w_port = pEngine->w_port;
        if (w_augno != NULL) *w_augno = pEngine->w_augno;
        if (w_au3Tug3no != NULL)
            *w_au3Tug3no = pEngine->w_au3Tug3no;
        if (w_tug2no != NULL) *w_tug2no = 0;
        if (w_tuno != NULL) *w_tuno = 0;

        /* Return the protection channel */
        if (p_port != NULL) *p_port = pHoCx->pairPort;
        if (p_augno != NULL) *p_augno = pHoCx->pairAugno;
        if (p_au3Tug3no != NULL)
            *p_au3Tug3no = pHoCx->pairAuno;
        if (p_tug2no != NULL) *p_tug2no = 0;
        if (p_tuno != NULL) *p_tuno = 0;
        }

    /* LO engine */
    else
        {
        switch (TuType(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno))
            {
            /* TU-3 */
            case cAtUpsrTuTypeTu3:
                pHoCx = Tu3Vc3(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no);
                if (pHoCx == NULL)
                    return cAtUpsrRetNotCreated;

                /* Return the working channel */
                if (w_port != NULL) *w_port = pEngine->w_port;
                if (w_augno != NULL) *w_augno = pEngine->w_augno;
                if (w_au3Tug3no != NULL) *w_au3Tug3no = pEngine->w_au3Tug3no;
                if (w_tug2no != NULL) *w_tug2no = 0;
                if (w_tuno != NULL) *w_tuno = 0;

                /* Return the protection channel */
                if (p_port != NULL) *p_port = pHoCx->pairPort;
                if (p_augno != NULL) *p_augno = pHoCx->pairAugno;
                if (p_au3Tug3no != NULL) *p_au3Tug3no = pHoCx->pairAuno;
                if (p_tug2no != NULL) *p_tug2no = 0;
                if (p_tuno != NULL) *p_tuno = 0;
                break;

            /* TU-1x */
            case cAtUpsrTuTypeTu12:
            case cAtUpsrTuTypeTu11:
                pLoCx = TuDb(pEngine->module, pEngine->w_port, pEngine->w_augno, pEngine->w_au3Tug3no, pEngine->w_tug2no, pEngine->w_tuno);
                if (pLoCx == NULL)
                    return cAtUpsrRetNotCreated;

                /* Return the working channel */
                if (w_port != NULL) *w_port = pEngine->w_port;
                if (w_augno != NULL) *w_augno = pEngine->w_augno;
                if (w_au3Tug3no != NULL) *w_au3Tug3no = pEngine->w_au3Tug3no;
                if (w_tug2no != NULL) *w_tug2no = pEngine->w_tug2no;
                if (w_tuno != NULL) *w_tuno = pEngine->w_tuno;

                /* Return the protection channel */
                if (p_port != NULL) *p_port = pLoCx->pairPort;
                if (p_augno != NULL) *p_augno = pLoCx->pairAugno;
                if (p_au3Tug3no != NULL) *p_au3Tug3no = pLoCx->pairAuno;
                if (p_tug2no != NULL) *p_tug2no = pLoCx->pairTug2no;
                if (p_tuno != NULL) *p_tuno = pLoCx->pairTuno;
                break;

            default:
                return cAtUpsrRetInvalid;
            }
        }

    return cAtUpsrRetOk;
    }

/**
 * Get active path (working or protection channel)
 *
 * @param engine Engine
 *
 * @return @ref eAtUpsrChnType "Active path (working or protection channel)"
 */
eAtUpsrChnType AtUpsrEngActivePathGet(AtUpsrEngine engine)
    {
    tUpsrEngDb *pEngine;
    uint8 state;

    /* Error on NULL engine */
    if ((pEngine = engine) == NULL)
        return cAtUpsrUnknown;

    /* Return active path */
    state = pEngine->engInfo.engState;

    /* Working is active */
    if ((state == cAtUpsrEngStateInActive) ||
        (state == cAtUpsrEngStateIdle) ||
        (state == cAtUpsrEngStateFs2Wrk) ||
        (state == cAtUpsrEngStateMs2Wrk) ||
        (state == cAtUpsrEngStateLp) ||
        (state == cAtUpsrEngStateFailWrk))
        return cAtUpsrChnTypeWorking;

    /* Protection is active */
    if ((state == cAtUpsrEngStateSw) ||
        (state == cAtUpsrEngStateFs2Prt) ||
        (state == cAtUpsrEngStateMs2Prt) ||
        (state == cAtUpsrEngStateWtr) ||
        (state == cAtUpsrEngStateFailPrt))
        return cAtUpsrChnTypeProtection;

    /* There may be error */
    return cAtUpsrUnknown;
    }

/**
 * Get switching reason
 *
 * @param engine Engine
 *
 * @return @ref eAtUpsrSwReason "Switching reason"
 */
eAtUpsrSwReason AtUpsrEngSwReasonGet(AtUpsrEngine engine)
    {
    return (engine == NULL) ? cAtUpsrUnknown : ((tUpsrEngDb *)engine)->engInfo.swReason;
    }

/**
 * Set switching condition
 *
 * @param engine Engine
 * @param swCond Switching condition
 *
 * @return
 */
eAtUpsrRet AtUpsrEngSwCondSet(AtUpsrEngine engine, uint32 swCond)
    {
	Unused(engine);
	Unused(swCond);
    /* TODO */
    return cAtUpsrRetErr;
    }

/**
 * Get switching condition
 *
 * @param engine Engine
 *
 * @return @ref Switching condition
 */
uint32 AtUpsrEngSwCondGet(AtUpsrEngine engine)
    {
	Unused(engine);
    /* TODO */
    return cAtUpsrUnknown;
    }

/**
 * Get defect status of working channel
 *
 * @param engine Engine
 *
 * @return Defect status of working channel
 */
uint32 AtUpsrEngWrkDefGet(AtUpsrEngine engine)
    {
    if (engine == NULL)
        return 0x0;

    return ((tUpsrEngDb *)engine)->engInfo.wrkDef;
    }

/**
 * Get defect status of protection channel
 *
 * @param engine Engine
 *
 * @return Defect status of protection channel
 */
uint32 AtUpsrEngPrtDefGet(AtUpsrEngine engine)
    {
    if (engine == NULL)
        return 0x0;

    return ((tUpsrEngDb *)engine)->engInfo.prtDef;
    }

/**
 * Issue external command
 *
 * @param engine Engine
 * @param extlCmd @ref eAtUpsrExtlCmd "External command"
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrEngExtlCmdSet(AtUpsrEngine engine, eAtUpsrExtlCmd extlCmd)
    {
    eAtUpsrRet ret;
    tUpsrEngDb *pEngDb;
    uint8 w_port, w_augno, w_auno, w_tug2no, w_tuno;
    uint8 p_port, p_augno, p_auno, p_tug2no, p_tuno;

    /* Cannot issue command for NULL engine */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* Get engine database */
    pEngDb = engine;
    if (pEngDb == NULL)
        return cAtUpsrRetInvalid;

    /* Cannot issue command when module is stopped */
    if (pEngDb->engInfo.engState == cAtUpsrEngStateInActive)
        return cAtUpsrRetNotSupport;

    /* Check if command is applicable with switching type */
    if (pEngDb->engInfo.swType == cAtUpsrSwTypeNonRev)
        {
        if (extlCmd == cAtUpsrExtlCmdLp)
            return cAtUpsrRetNotSupport;
        }
    else if ((extlCmd == cAtUpsrExtlCmdFs2Wrk) || (extlCmd == cAtUpsrExtlCmdMs2Wrk))
        return cAtUpsrRetNotSupport;

    /* Apply command */
    mUpsrSemTake(pEngDb->module);

    /* Get working and protection channels */
    ret = AtUpsrEngChnGet(engine,
                    &w_port, &w_augno, &w_auno, &w_tug2no, &w_tuno,
                    &p_port, &p_augno, &p_auno, &p_tug2no, &p_tuno);
    if (ret != cAtUpsrRetOk)
        return ret;

    /* Issue external command */
    switch (extlCmd)
        {
        /* Clear */
        case cAtUpsrExtlCmdClear:
            switch (pEngDb->engInfo.engState)
                {
                /* The cases that traffic is on working */
                case cAtUpsrEngStateFs2Wrk:
                case cAtUpsrEngStateMs2Wrk:
                case cAtUpsrEngStateLp:
                    /* Working channel has alarm */
                    if (pEngDb->engInfo.wrkDef)
                        {
                        /* Protection channel also has alarm */
                        if (pEngDb->engInfo.prtDef)
                            {
                            pEngDb->engInfo.engState = cAtUpsrEngStateFailWrk;
                            pEngDb->engInfo.swReason = cAtUpsrSwReasonNone;
                            }

                        /* Switch to protection channel in case of no alarm on it */
                        else
                            {
                            pEngDb->engInfo.engState = cAtUpsrEngStateSw;
                            pEngDb->engInfo.swReason = cAtUpsrSwReasonAuto;
                            pEngDb->funcSw(pEngDb,
                                           w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                           p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                           pEngDb->pUsrData, pEngDb->usrDataSize);
                            }
                        }

                    /* No alarm on working, set engine state to IDLE */
                    else
                        {
                        pEngDb->engInfo.engState = cAtUpsrEngStateIdle;
                        pEngDb->engInfo.swReason = cAtUpsrSwReasonNone;
                        }
                    break;

                /* The cases that traffic is on protection channel. WTR can be
                 * activated */
                case cAtUpsrEngStateFs2Prt:
                case cAtUpsrEngStateMs2Prt:
                    /* Protection channel has alarm */
                    if (pEngDb->engInfo.prtDef)
                        {
                        /* Working channel has no alarm, switch back */
                        if (!pEngDb->engInfo.wrkDef)
                            {
                            pEngDb->engInfo.engState = cAtUpsrEngStateIdle;
                            pEngDb->engInfo.swReason = cAtUpsrSwReasonAuto;
                            pEngDb->funcSw(pEngDb,
                                           p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                           w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                           pEngDb->pUsrData, pEngDb->usrDataSize);
                            }

                        /* Switching fail, traffic is still on protection channel */
                        else
                            {
                            pEngDb->engInfo.engState = cAtUpsrEngStateFailPrt;
                            pEngDb->engInfo.swReason = cAtUpsrSwReasonAuto;
                            }
                        }

                    /* No alarm on protection */
                    else
                        {
                        /* No alarm on working, check if WTR need to be activated */
                        if ((!(pEngDb->engInfo.wrkDef)) && (pEngDb->engInfo.swType == cAtUpsrSwTypeRev))
                            pEngDb->engInfo.engState = cAtUpsrEngStateWtr;
                        else
                            {
                            pEngDb->engInfo.engState = cAtUpsrEngStateSw;
                            pEngDb->engInfo.swReason = cAtUpsrSwReasonNone;
                            }
                        }
                    break;

                default:
                    break;
                }
            break;

        /* Force switch to working channel */
        case cAtUpsrExtlCmdFs2Wrk:
            switch (pEngDb->engInfo.engState)
                {
                /* The cases that traffic is on working */
                case cAtUpsrEngStateMs2Wrk:
                case cAtUpsrEngStateFailWrk:
                case cAtUpsrEngStateIdle:
                    pEngDb->engInfo.engState = cAtUpsrEngStateFs2Wrk;
                    break;

                /* The cases that traffic is on protection */
                case cAtUpsrEngStateSw:
                case cAtUpsrEngStateFs2Prt:
                case cAtUpsrEngStateMs2Prt:
                case cAtUpsrEngStateWtr:
                case cAtUpsrEngStateFailPrt:
                    pEngDb->engInfo.engState = cAtUpsrEngStateFs2Wrk;
                    pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                    pEngDb->funcSw(pEngDb,
                                   p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                   w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                   pEngDb->pUsrData, pEngDb->usrDataSize);
                    break;

                /* Return error for other cases */
                default:
                    ret = cAtUpsrRetInvalid;
                    break;
                }
            break;

        /* Force switch to protection channel */
        case cAtUpsrExtlCmdFs2Prt:
            switch (pEngDb->engInfo.engState)
                {
                /* The cases that traffic is on working */
                case cAtUpsrEngStateIdle:
                case cAtUpsrEngStateMs2Wrk:
                case cAtUpsrEngStateFailWrk:
                    pEngDb->engInfo.engState = cAtUpsrEngStateFs2Prt;
                    pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                    pEngDb->funcSw(pEngDb,
                                   w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                   p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                   pEngDb->pUsrData, pEngDb->usrDataSize);
                    break;

                /* The cases traffic is on protection */
                case cAtUpsrEngStateSw:
                case cAtUpsrEngStateMs2Prt:
                case cAtUpsrEngStateWtr:
                case cAtUpsrEngStateFailPrt:
                    pEngDb->engInfo.engState = cAtUpsrEngStateFs2Prt;
                    pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                    break;

                /* Return error for other cases */
                default:
                    ret = cAtUpsrRetInvalid;
                    break;
                }

            break;

        /* Manual switch to working */
        case cAtUpsrExtlCmdMs2Wrk:
            switch (pEngDb->engInfo.engState)
                {
                /* The cases that traffic is on working */
                case cAtUpsrEngStateIdle:
                    pEngDb->engInfo.engState = cAtUpsrEngStateMs2Wrk;
                    pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                    break;

                /* The cases that traffic is on protection */
                case cAtUpsrEngStateSw:
                case cAtUpsrEngStateWtr:
                    /* Only allow when working has no alarm */
                    if (pEngDb->engInfo.wrkDef)
                        ret = cAtUpsrRetInvalid;

                    /* Working has no alarm, can switch */
                    else
                        {
                        pEngDb->engInfo.engState = cAtUpsrEngStateMs2Wrk;
                        pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                        pEngDb->funcSw(pEngDb,
                                       p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                       w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                       pEngDb->pUsrData, pEngDb->usrDataSize);
                        }

                    break;

                /* For other case, return error */
                default:
                    ret = cAtUpsrRetInvalid;
                    break;
                }
            break;

        /* Manual switch to protection */
        case cAtUpsrExtlCmdMs2Prt:
            switch (pEngDb->engInfo.engState)
                {
                /* The cases that traffic is on working */
                case cAtUpsrEngStateIdle:
                    /* Only switch when protection has no alarm */
                    if (pEngDb->engInfo.prtDef)
                        ret = cAtUpsrRetInvalid;
                    else
                        {
                        pEngDb->engInfo.engState = cAtUpsrEngStateMs2Prt;
                        pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                        pEngDb->funcSw(pEngDb,
                                       w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                       p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                       pEngDb->pUsrData, pEngDb->usrDataSize);
                        }

                    break;

                /* The cases that traffic is on protection */
                case cAtUpsrEngStateSw:
                    pEngDb->engInfo.engState = cAtUpsrEngStateMs2Prt;
                    break;

                /* Return error for other cases */
                default:
                    ret = cAtUpsrRetInvalid;
                    break;
                }

            break;

        /* Lock Out of Protection. It has the highest priority. In this case,
         * always switch traffic back to working */
        case cAtUpsrExtlCmdLp:
            /* This command is only applied on revertive mode */
            if (pEngDb->engInfo.swType == cAtUpsrSwTypeNonRev)
                {
                ret = cAtUpsrRetInvalid;
                break;
                }

            /* Switch back */
            switch (pEngDb->engInfo.engState)
                {
                case cAtUpsrEngStateSw:
                case cAtUpsrEngStateFs2Prt:
                case cAtUpsrEngStateMs2Prt:
                case cAtUpsrEngStateWtr:
                case cAtUpsrEngStateFailPrt:
                    pEngDb->engInfo.swReason = cAtUpsrSwReasonExtCmd;
                    pEngDb->engInfo.engState = cAtUpsrEngStateLp;
                    pEngDb->funcSw(pEngDb,
                                   p_port, p_augno, p_auno, p_tug2no, p_tuno,
                                   w_port, w_augno, w_auno, w_tug2no, w_tuno,
                                   pEngDb->pUsrData, pEngDb->usrDataSize);
                    break;
                default:
                    pEngDb->engInfo.engState = cAtUpsrEngStateLp;
                    break;
                }
            break;

        /* Not supported */
        default:
            ret = cAtUpsrRetNotSupport;
            break;
        }

    /* Update database */
    if (ret == cAtUpsrRetOk)
        pEngDb->engInfo.extlCmd = extlCmd;

    mUpsrSemGive(pEngDb->module);

    return ret;
    }

/**
 * Get active external command
 *
 * @param engine Engine
 *
 * @return @ref eAtUpsrExtlCmd "External command"
 */
eAtUpsrExtlCmd AtUpsrEngExtlCmdGet(AtUpsrEngine engine)
    {
    tAtUpsrEngInfo *pEngInfo;

    /* External command of NULL engine is unknown */
    if (engine == NULL)
        return cAtUpsrUnknown;

    /* Get engine database */
    pEngInfo = engine;
    if ((pEngInfo->engState == cAtUpsrEngStateFs2Wrk) ||
        (pEngInfo->engState == cAtUpsrEngStateFs2Prt) ||
        (pEngInfo->engState == cAtUpsrEngStateMs2Wrk) ||
        (pEngInfo->engState == cAtUpsrEngStateMs2Prt) ||
        (pEngInfo->engState == cAtUpsrEngStateLp))
        return pEngInfo->engState;
    else
        return cAtUpsrExtlCmdClear;

    return cAtUpsrRetOk;
    }

/**
 * Set user data
 *
 * @param engine Engine
 * @param [in] pUsrData User data
 * @param [in] usrDataSize Size of user data
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrEngUsrDataSet(AtUpsrEngine engine, void *pUsrData, uint32 usrDataSize)
    {
    tUpsrEngDb *pEngDb;

    /* Cannot set user data for NULL engine */
    if (engine == NULL)
        return cAtUpsrRetNotCreated;

    /* Free first */
    pEngDb = engine;
    AtOsalMemFree(pEngDb->pUsrData);
    pEngDb->pUsrData = NULL;
    if ((pUsrData == NULL) || (usrDataSize == 0))
        return cAtUpsrRetOk;

    /* Allocate memory and copy data */
    pEngDb->pUsrData = AtOsalMemAlloc(usrDataSize);
    if (pEngDb->pUsrData == NULL)
        return cAtUpsrRetOsRscAlloc;
    AtOsalMemCpy(pEngDb->pUsrData, pUsrData, usrDataSize);

    return cAtUpsrRetOk;
    }

/**
 * Get user data
 *
 * @param [in] engine Engine
 * @param [out] pUsrDataSize Size of user data. It is ignored if NULL
 *
 * @return User data on success or NULL if fail
 */
void *AtUpsrEngUsrDataGet(AtUpsrEngine engine, uint32 *pUsrDataSize)
    {
    tUpsrEngDb *pEngDb;

    /* NULL engine has no user data */
    if (engine == NULL)
        return NULL;

    /* Return user data */
    pEngDb = engine;
    if (pUsrDataSize != NULL)
        *pUsrDataSize = pEngDb->usrDataSize;

    return pEngDb->pUsrData;
    }

/**
 * Get switching handling function
 *
 * @param engine Engine
 *
 * @return Switching function on success or NULL if engine has not been created
 *         or any error happen
 */
AtUpsrEngSwFunc AtUpsrEngSwFuncGet(AtUpsrEngine engine)
    {
    return (engine == NULL) ? NULL : ((tUpsrEngDb*)engine)->funcSw;
    }

/**
 * Get the pair channel and working channel indication
 *
 * @param [in] module Module
 * @param [in] port Port ID
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] auno [0..2] AU-3 ID
 * @param [out] pairPort Pair port ID. It is ignored if NULL
 * @param [out] pairAugno Pair AUG-1 ID. It is ignored if NULL
 * @param [out] pairAuno Pair AU-3 ID. It is ignored if NULL
 *
 * @return @ref eAtUpsrChnType "Channel type (working or protection)" of the
 *         input channel
 */
eAtUpsrChnType AtUpsrHoChnGetPairChn(AtUpsrModule module,
                                 uint8 port, uint8 augno, uint8 auno,
                                 uint8 *pairPort, uint8 *pairAugno, uint8 *pairAuno)
    {
    tUpsrHoCxInfo *pCxInfo;

    /* Cannot return this information if module is NULL */
    if (module == NULL)
        return cAtUpsrUnknown;

    /* Get VC information */
    pCxInfo = HoCx(module, port, augno, auno);
    if (pCxInfo == NULL)
        return cAtUpsrUnknown;

    /* Error when it does not belong to any engine */
    if (pCxInfo->pEngDb == NULL)
        return cAtUpsrUnknown;

    /* Return ID of the pair channel */
    if (pairPort != NULL)
        *pairPort = pCxInfo->pairPort;
    if (pairAugno != NULL)
        *pairAugno = pCxInfo->pairAugno;
    if (pairAuno != NULL)
        *pairAuno = pCxInfo->pairAuno;

    /* Return channel type of input channel */
    return (pCxInfo->isWrk) ? cAtUpsrChnTypeWorking : cAtUpsrChnTypeProtection;
    }

/**
 * Notify defect for a channel so that it can perform its operations.
 *
 * @param module Module
 * @param [in] port Port
 * @param [in] augno [0..15] AUG-1
 * @param [in] auno [0..2] AU-3
 * @param [in] defMsk Defect mask to determine what kinds of defects need to be
 *                    reported to this module. If a bit is set, the value of
 *                    corresponding bit in defStat will be evaluated. Its value
 *                    can be ORed of:
 *                    - cAtUpsrSwCondAuLop : AU-LOP
 *                    - cAtUpsrSwCondAuAis : AU-AIS
 * @param [in] defStat Defect status. Each bit indicates defect status
 *                     (0-clear, 1-raise). Its value can be ORed of:
 *                     - cAtUpsrSwCondAuLop : AU-LOP
 *                     - cAtUpsrSwCondAuAis : AU-AIS
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrHoChnDefNotify(AtUpsrModule module,
                            uint8 port, uint8 augno, uint8 auno,
                            uint32 defMsk, uint32 defStat)
    {
    eAtUpsrRet ret;

    mUpsrSemTake(module);

    /* Input defect */
    ret = cAtUpsrRetOk;
    switch (AuType(module, port, augno, auno))
        {
        /* AU-4-16c */
        case cAtUpsrAuTypeAu4_16c:
            if (LineIsProv(module, port))
                ret = Aug16DefUpd(module, port, augno, defMsk, defStat, Aug16Db(module, port, 0));
            break;

        /* AU-4-4c */
        case cAtUpsrAuTypeAu4_4c:
            augno = augno / 4;
            if (Aug4IsProv(module, port, augno))
                ret = Aug4DefUpd(module, port, augno, defMsk, defStat, Aug4Db(module, port, augno));
            break;

        /* AU-4 */
        case cAtUpsrAuTypeAu4:
            if (Aug1IsProv(module, port, augno))
                ret = Aug1DefUpd(module, port, augno, defMsk, defStat, Aug1Db(module, port, augno));
            break;

        /* AU-3 */
        case cAtUpsrAuTypeAu3:
            if (Au3Tug3IsProv(module, port, augno, auno))
                ret = Au3Tug3DefUpd(module, port, augno, auno, defMsk, defStat, Au3Tu3Db(module, port, augno, auno));
            break;

        /* Not supported */
        default:
            ret = cAtUpsrRetNotSupport;
            break;
        }
    mUpsrSemGive(module);

    return ret;
    }

/**
 * Get engine that this channel is provisioned to
 *
 * @param [in] module Module
 * @param [in] port Port ID
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] auno [0..2] AU-3 ID
 *
 * @return Engine handle if success or NULL on failure
 */
AtUpsrEngine AtUpsrHoEngGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrHoCxInfo *pCxInfo;

    if ((module == NULL) || ((pCxInfo = HoCx(module, port, augno, auno)) == NULL))
        return NULL;
    return pCxInfo->pEngDb;
    }

/**
 * Get defect status of a channel
 *
 * @param [in] module Module
 * @param [in] port Port Id
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] auno [0..2] AU-3 ID
 *
 * @return Defect status. In case of failure, 0 is returned
 */
uint32 AtUpsrHoChnDefStatusGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno)
    {
    tUpsrHoCxInfo *pCxInfo;

    /* Cannot get defect if module has not been created */
    if (module == NULL)
        return 0;

    /* Get VC information */
    pCxInfo = HoCx(module, port, augno, auno);
    if (pCxInfo == NULL)
        return 0;

    return (pCxInfo->isWrk) ? pCxInfo->pEngDb->engInfo.wrkDef : pCxInfo->pEngDb->engInfo.prtDef;
    }

/**
 * Get the pair channel and working channel indication
 *
 * @param [in] module Module
 * @param [in] port Port ID
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] au3Tug3no [0..2] AU-3/TUG-3 ID
 * @param [in] tug2no [0..6] TUG-2 ID
 * @param [in] tuno [0..3] TU-1x ID
 * @param [out] pairPort Pair port ID
 * @param [out] pairAugno Pair AUG-1 ID
 * @param [out] pairAu3Tug3no Pair AU-3/TUG-3 ID
 * @param [out] pairTug2no Pair TUG-2 ID
 * @param [out] pairTuno Pair TU-1x ID
 *
 * @ref eAtUpsrChnType "Channel type (working or protection)" of the
 *         input channel
 */
eAtUpsrChnType AtUpsrLoChnGetPairChn(AtUpsrModule module,
                                 uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno,
                                 uint8 *pairPort, uint8 *pairAugno, uint8 *pairAu3Tug3no, uint8 *pairTug2no, uint8 *pairTuno)
    {
    tUpsrHoCxInfo *pHoCxInfo;
    tUpsrLoCxInfo *pLoCxInfo;
    eAtUpsrChnType chnType;

    /* Cannot return this information if module is NULL */
    if (module == NULL)
        return cAtUpsrUnknown;

    switch (TuType(module, port, augno, au3Tug3no, tug2no, tuno))
        {
        /* TU-3 */
        case cAtUpsrTuTypeTu3:
            /* Get VC information */
            pHoCxInfo = Tu3Vc3(module, port, augno, au3Tug3no);
            if (pHoCxInfo == NULL)
                return cAtUpsrUnknown;
            if (pHoCxInfo->pEngDb == NULL)
                return cAtUpsrUnknown;

            /* Return ID of the pair channel */
            if (pairPort != NULL)
                *pairPort = pHoCxInfo->pairPort;
            if (pairAugno != NULL)
                *pairAugno = pHoCxInfo->pairAugno;
            if (pairAu3Tug3no != NULL)
                *pairAu3Tug3no = pHoCxInfo->pairAuno;
            if (pairTug2no != NULL)
                *pairTug2no = 0;
            if (pairTuno != NULL)
                *pairTuno = 0;

            chnType = (pHoCxInfo->isWrk) ? cAtUpsrChnTypeWorking : cAtUpsrChnTypeProtection;
            break;

        /* TU-1x */
        case cAtUpsrTuTypeTu12:
        case cAtUpsrTuTypeTu11:
            pLoCxInfo = TuDb(module, port, augno, au3Tug3no, tug2no, tuno);
            if ((pLoCxInfo == NULL) || (pLoCxInfo->pEngDb == NULL))
                return cAtUpsrUnknown;

            /* Return ID of the pair channel */
            if (pairPort != NULL)
                *pairPort = pLoCxInfo->pairPort;
            if (pairAugno != NULL)
                *pairAugno = pLoCxInfo->pairAugno;
            if (pairAu3Tug3no != NULL)
                *pairAu3Tug3no = pLoCxInfo->pairAuno;
            if (pairTug2no != NULL)
                *pairTug2no = pLoCxInfo->pairTug2no;
            if (pairTuno != NULL)
                *pairTuno = pLoCxInfo->pairTuno;

            chnType = (pLoCxInfo->isWrk) ? cAtUpsrChnTypeWorking : cAtUpsrChnTypeProtection;
            break;

        /* Unknown */
        default:
            return cAtUpsrRetInvalid;
        }

    /* Return channel type of input channel */
    return chnType;
    }

/**
 * Get defect status of a channel
 *
 * @param [in] module Module
 * @param [in] port Port ID
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] au3Tug3no [0..2] AU-3/TUG-3 ID
 * @param [in] tug2no [0..6] TUG-2 ID
 * @param [in] tuno [0..3] TU-1x ID
 *
 * @return Defect status. In case of failure, 0 is returned
 */
uint32 AtUpsrLoChnDefStatusGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrHoCxInfo *pHoCx;
    tUpsrLoCxInfo *pLoCx;

    /* Cannot get defect if module has not been created */
    if (module == NULL)
        return 0;

    switch (TuType(module, port, augno, au3Tug3no, tug2no, tuno))
        {
        /* TU-3 */
        case cAtUpsrTuTypeTu3:
            pHoCx = Tu3Vc3(module, port, augno, au3Tug3no);
            if (pHoCx == NULL)
                return 0;
            return (pHoCx->isWrk) ? pHoCx->pEngDb->engInfo.wrkDef : pHoCx->pEngDb->engInfo.prtDef;

        /* TU-1x */
        case cAtUpsrTuTypeTu12:
        case cAtUpsrTuTypeTu11:
            pLoCx = TuDb(module, port, augno, au3Tug3no, tug2no, tuno);
            if (pLoCx == NULL)
                return 0;
            return (pLoCx->isWrk) ? pLoCx->pEngDb->engInfo.wrkDef : pLoCx->pEngDb->engInfo.prtDef;

        /* Unknown */
        default:
            return cAtUpsrRetInvalid;
        }
    }

/**
 * Get engine that this channel is provisioned to
 *
 * @param [in] module Module
 * @param [in] port Port ID
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] au3Tug3no [0..2] AU-3/TUG-3 ID
 * @param [in] tug2no [0..6] TUG-2 ID
 * @param [in] tuno [0..3] TU-1x ID
 *
 * @return Engine handle if success or NULL on failure
 */
AtUpsrEngine AtUpsrLoEngGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno)
    {
    tUpsrHoCxInfo *pHoCx;
    tUpsrLoCxInfo *pLoCx;

    /* Cannot get defect if module has not been created */
    if (module == NULL)
        return 0;

    switch (TuType(module, port, augno, au3Tug3no, tug2no, tuno))
        {
        /* TU-3 */
        case cAtUpsrTuTypeTu3:
            pHoCx = Tu3Vc3(module, port, augno, au3Tug3no);
            return (pHoCx == NULL) ? NULL : pHoCx->pEngDb;

        /* TU-1x */
        case cAtUpsrTuTypeTu12:
        case cAtUpsrTuTypeTu11:
            pLoCx = TuDb(module, port, augno, au3Tug3no, tug2no, tuno);
            return (pLoCx == NULL) ? NULL : pLoCx->pEngDb;

        /* Unknown */
        default:
            return NULL;
        }
    }

/**
 * Notify defect for a channel so that it can perform its operations.
 *
 * @param [in] module Module
 * @param [in] port Port ID
 * @param [in] augno [0..15] AUG-1 ID
 * @param [in] au3Tug3no [0..2] AU-3/TUG-3 ID
 * @param [in] tug2no [0..6] TUG-2 ID
 * @param [in] tuno [0..3] TU-1x ID
 * @param [in] defMsk Defect mask to determine what kinds of defects need to be
 *                    reported to this module. If a bit is set, the value of
 *                    corresponding bit in defStat will be evaluated. Its value
 *                    can be ORed of:
 *                    - cAtUpsrSwCondTuLop : TU-LOP
 *                    - cAtUpsrSwCondTuAis : TU-AIS
 * @param [in] defStat Defect status. Each bit indicates defect status
 *                     (0-clear, 1-raise). Its value can be ORed of:
 *                     - cAtUpsrSwCondTuLop : TU-LOP
 *                     - cAtUpsrSwCondTuAis : TU-AIS
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrLoChnDefNotify(AtUpsrModule module,
                            uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno,
                            uint32 defMsk, uint32 defStat)
    {
    tUpsrLoCxInfo *pLoCx;
    eAtUpsrRet ret;

    /* Cannot get defect if module has not been created */
    if (module == NULL)
        return 0;

    mUpsrSemTake(module);
    ret = cAtUpsrRetOk;
    switch (TuType(module, port, augno, au3Tug3no, tug2no, tuno))
        {
        /* TU-3 */
        case cAtUpsrTuTypeTu3:
            if (Au3Tug3IsProv(module, port, augno, au3Tug3no))
                ret = Au3Tug3DefUpd(module, port, augno, au3Tug3no, defMsk, defStat, Au3Tu3Db(module, port, augno, au3Tug3no));
            break;

        /* TU-1x */
        case cAtUpsrTuTypeTu12:
        case cAtUpsrTuTypeTu11:
            pLoCx = TuDb(module, port, augno, au3Tug3no,  tug2no, tuno);
            if (pLoCx->isProv)
                mTuDefUpd(pLoCx->pEngDb, port, augno, au3Tug3no, tug2no, tuno, defMsk, defStat, pLoCx);
            break;

        /* Unknown */
        default:
            ret = cAtUpsrRetInvalid;
            break;
        }
    mUpsrSemGive(module);

    return ret;
    }

/**
 * Notify defect for a Line
 *
 * @param [in] module Module
 * @param [in] port Port
 * @param [in] defMsk Defect mask to determine what kinds of defects need to be
 *                    reported to this module. If a bit is set, the value of
 *                    corresponding bit in defStat will be evaluated. Its value
 *                    can be ORed of:
 *                    - cAtUpsrSwCondLos   : LOS
 *                    - cAtUpsrSwCondLof   : LOF
 *                    - cAtUpsrEngSwCondOof   : OOF
 *                    - cAtUpsrEngSwCondMsAis : MS-AIS
 * @param [in] defStat Defect status. Each bit indicates defect status
 *                     (0-clear, 1-raise). Its value can be ORed of:
 *                     - cAtUpsrSwCondLos   : LOS
 *                     - cAtUpsrSwCondLof   : LOF
 *                     - cAtUpsrEngSwCondOof   : OOF
 *                     - cAtUpsrEngSwCondMsAis : MS-AIS
 *
 * @return cAtUpsrRetOk On success and other error code on failure
 */
eAtUpsrRet AtUpsrLineDefNotify(AtUpsrModule module, uint8 port, uint32 defMsk, uint32 defStat)
    {
    /* Error on NULL module */
    if (module == NULL)
        return cAtUpsrRetNotCreated;

    /* Do nothing if Line has no provisioned channels */
    if (!LineIsProv(module, port))
        return cAtUpsrRetOk;

    return LineDefUpd(module, port, defMsk, defStat);
    }

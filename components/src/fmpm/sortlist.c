/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DATA STRUCTURE
 *
 * File        : sortlist.c
 *
 * Created Date: 04-Jan-09
 *
 * Description : This file contains source code of sorted linked-list
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include "sortlist.h"
#include "suradapt.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
/* Message type */
typedef enum eMsgType
    {
    cErrMsg  = 31,  /* Error message */
    cInfoMsg = 32,  /* Information message */
    cWarnMsg = 33   /* Warning message */
    }eMsgType;

/* Node in list */
typedef struct tNode
    {
    void         *pData;    /* Data */
    struct tNode *pPrev;    /* Previous node */
    struct tNode *pNext;    /* Next node */
    }tNode;

/* Sorted Linked List */
typedef struct _tSortList
    {
    osalSemKey       sem;   /* Semaphore */
    tNode           *pHead; /* Head */
    dword            size;  /* Size of list */
    eListOrder       order; /* List order */
    eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2); /* Compare function */
    }_tSortList;

/*--------------------------- Macros -----------------------------------------*/
/* Node create and destroy */
#define mNodeCreate(pNewNode, dataSize)                                        \
    (pNewNode)        = OsalMemAlloc(sizeof(tNode));                           \
    (pNewNode)->pNext = null;                                                  \
    (pNewNode)->pPrev = null;                                                  \
    if ((pNewNode) != null)                                                    \
        {                                                                      \
        (pNewNode)->pData = OsalMemAlloc(dataSize);                            \
        if ((pNewNode)->pData == null)                                         \
            {                                                                  \
            OsalMemFree(pNewNode);                                             \
            (pNewNode) = null;                                                 \
            }                                                                  \
        }
    
#define mNodeDestroy(pNode)                                                    \
    OsalMemFree((pNode)->pData);                                               \
    OsalMemFree(pNode);                                                        \
    (pNode) = null;

/* Take/give semaphore of list */
#define mListTake(pList, ret)                                                  \
    (ret) = ((OsalSemTake(&((pList)->sem)) != cOsalOk) ? cListErrSem : cListSucc)
#define mListGive(pList, ret)                                                  \
    (ret) = ((OsalSemGive(&((pList)->sem)) != cOsalOk) ? cListErrSem : cListSucc)



void mErrLog(uint32 msgType, const char *format, ...);


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
void mErrLog(uint32 msgType, const char *format, ...)
    {
    AtUnused(msgType);
    AtUnused(format);
    }
/*------------------------------------------------------------------------------
Prototype    : eListRet ListCreate(tSortList       *pList, 
                                   eListOrder       order, 
                                   eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2))

Purpose      : This function is used to create a sorted linked list. This 
               function must be called before calling other functions

Inputs       : pList                 - Sorted Linked List
               order                 - Order in list
               pCmpFunc              - Compare function

Outputs      : None

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListCreate(tSortList       *pList,
                    eListOrder       order, 
                    eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2))
    {
    _tSortList *_pList;
    
    /* Allocate memory */
    _pList = OsalMemAlloc(sizeof(_tSortList));
    if (_pList == null)
        {
        return cListErr;
        }
    
    /* Initialize */
    if (OsalSemInit(&(_pList->sem), cAtFalse, 1) != cOsalOk)
        {
        mErrLog(cErrMsg, "Cannot create semaphore\n");
        return cListSucc;
        }
    _pList->pHead    = null;
    _pList->size     = 0;
    _pList->order    = order;
    _pList->pCmpFunc = pCmpFunc;
    
    /* Return */
    *pList = _pList;
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListDestroy(tSortList *pList)

Purpose      : This function is used to destroy a sorted linked list. This 
               function must be called if list is not used anymore to make sure 
               that all allocated resources are freed.

Inputs       : pList                 - Sorted Linked List

Outputs      : None

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListDestroy(tSortList *pList)
    {
    eListRet    ret;
    _tSortList *_pList;
    
    /* Check parameter */
    if (pList == null)
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    _pList = (_tSortList *)(*pList);
    
    /* Flush all elements */
    ret = ListFlush(*pList);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot flush list\n");
        return ret;
        }
    
    /* Free semaphore */
    if (OsalSemDestroy(&(_pList->sem)) != cOsalOk)
        {
        mErrLog(cWarnMsg, "Cannot destroy semaphore\n");
        }
    
    /* Deallocate list */
    OsalMemFree(*pList);
    *pList = null;
    
    return cListSucc;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eListRet ListCmpFuncSet(tSortList       list, 
                                       eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2))

Purpose      : This function is used to set compare function

Inputs       : list                  - Sorted Linked List
               pCmpFunc              - Compare function

Outputs      : None

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListCmpFuncSet(tSortList       list, 
                        eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2))
    {
    _tSortList *pList;
    eListRet    ret;
    
    /* Check parameters */
    if ((list == null) || (pCmpFunc == null))
        {
        mErrLog(cErrMsg, "Invalid parametes\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Set compare function */
    pList->pCmpFunc = pCmpFunc;
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListOrderSet(tSortList list, eListOrder order)

Purpose      : This function is used to set order for a list

Inputs       : list                  - Sorted Linked List
               order                 - List order

Outputs      : None

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListOrderSet(tSortList list, eListOrder order)
    {
    _tSortList *pList;
    tNode      *pNode;
    eListRet    ret;
    
    /* Check parameter */
    if (list == null)
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Set order */
    if (pList->order != order)
        {
        pList->order = order;
        if (pList->size > 1)
            {
            /* Interchange between previous and next pointer */
            while ((pNode = pList->pHead->pNext) != null)
                {
                pList->pHead->pNext = pList->pHead->pPrev;
                pList->pHead->pPrev = pNode;
                pList->pHead        = pNode;
                }
            pList->pHead->pNext = pList->pHead->pPrev;
            pList->pHead->pPrev = null;
            }
        }
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListOrderGet(tSortList list, eListOrder *pOrder)

Purpose      : This function is used to get order of a list

Inputs       : list                  - Sorted Linked List

Outputs      : pOrder                - List order

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListOrderGet(tSortList list, eListOrder *pOrder)
    {
    _tSortList *pList;
    eListRet    ret;
    
    /* Check parameter */
    if ((list == null) || (pOrder == null))
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Return list order */
    *pOrder = pList->order;
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListSearch(tSortList list, 
                                   void      *pEle, 
                                   dword     *pIndex, 
                                   bool       rmv)

Purpose      : This function is used to search and remove (if enable) a element 
               in list

Inputs       : list                  - Sorted Linked List
               pEle                  - Searched element
               rmv                   - true: the searched element will be 
                                       removed from list
Outputs      : pIndex                - Index of searched element in list

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListSearch(tSortList list, void *pEle, dword *pIndex, bool rmv)
    {
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListRmv(tSortList list, 
                                dword      idx,
                                void      *pData, 
                                dword      size)

Purpose      : This function is used to remove a element at specified index and 
               return the removed element

Inputs       : list                  - Sorted Linked List
               idx                   - Index of element in list
               size                  - Data size
               
Outputs      : pData                 - Element data

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListRmv(tSortList list, dword idx, void *pData, dword size)
    {
    _tSortList *pList;
    tNode      *pNode;
    dword       dIdx;
    eListRet    ret;
    
    /* Check parameter */
    if ((list == null) || (pData == null) || (size == 0))
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Check index */
    if (idx >= pList->size)
        {
        mErrLog(cErrMsg, 
                "Invalid index %lu, list size = %lu\n",
                idx, pList->size);
        
        /* Give semaphore */
        mListGive(pList, ret);
        if ((ret != cListSucc))
            {
            mErrLog(cWarnMsg, "Cannot give semaphore\n");
            }
        return cListErr;
        }
    
    /* Get first element */
    if (idx == 0)
        {
        /* Return data */
        OsalMemCpy(pList->pHead->pData, pData, size);
        
        /* Delete node and update links */
        if (pList->pHead->pNext == null)
            {
            mNodeDestroy(pList->pHead);
            }
        else
            {
            pList->pHead = pList->pHead->pNext;
            mNodeDestroy(pList->pHead->pPrev);
            }
        }
    
    /* Get other element */
    else
        {
        /* Go to the specified position */
        pNode = pList->pHead;
        dIdx  = 0;
        for (dIdx = 0; dIdx < idx; dIdx++)
            {
            pNode = pNode->pNext;
            }
        
        /* Return data */
        OsalMemCpy(pNode->pData, pData, size);
        
        /* Update links */
        /* Node is the last node */
        if (idx == (pList->size - 1))
            {
            pNode->pPrev->pNext = null;
            }
        
        /* Node is intermediate node */
        else
            {
            pNode->pPrev->pNext = pNode->pNext;
            pNode->pNext->pPrev = pNode->pPrev;
            }
        
        /* Delete node */
        mNodeDestroy(pNode);
        }
    
    /* Decrease list size */
    pList->size = pList->size - 1;
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListAdd(tSortList list, void *pData, dword size)

Purpose      : This function is used to add one element to list at a specified 
               index

Inputs       : list                  - Sorted Linked List
               pData                 - Data
               size                  - Data size

Outputs      : None

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListAdd(tSortList list, void *pData, dword size)
    {
    _tSortList     *pList;
    tNode          *pNewNode;
    tNode          *pCurNode;
    bool            blForward;
    eListCmpResult  cmpRes;
    dword           dLow;
    dword           dHigh;
    dword           dStep;
    dword           dMid;
    eListRet        ret;
    
    /* Check parameters */
    if ((list == null) || (pData == null) || (size == 0))
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Do nothing if the compare function has not been set */
    if (pList->pCmpFunc == null)
        {
        mErrLog(cErrMsg, "Compare function is NULL\n");
        return cListErr;
        }
    
    /* Create new node */
    mNodeCreate(pNewNode, size);
    if (pNewNode == null)
        {
        mErrLog(cErrMsg, "Cannot allocated memory for new element");
        mListGive(pList, ret);
        if (ret != cListSucc)
            {
            mErrLog(cErrMsg, "Cannot give semaphore");
            }
        return cListErrMem;
        }
    OsalMemCpy(pData, pNewNode->pData, size);
        
    /* List is empty */
    if (pList->size == 0)
        {
        pList->pHead = pNewNode;
        pList->size  = pList->size + 1;
        }
    
    /* List is not empty */
    else
        {
        pCurNode  = pList->pHead;
        dLow      = 0;
        dHigh     = pList->size - 1;
        blForward = true;
        while (dLow < dHigh)
            {
            dMid  = ((dLow + dHigh) / 2);
            dStep = (blForward) ? (dHigh - dMid) : (dMid - dLow);
            /* Go dStep steps */
            while (dStep > 0)
                {
                pCurNode = (blForward) ? (pCurNode->pNext) : (pCurNode->pPrev);
                dStep    = dStep - 1;
                }
            
            /* Compare */
            cmpRes = pList->pCmpFunc(pCurNode->pData, pData);
            switch (cmpRes)
                {
                /* Equal */
                case cListEqual:
                    mErrLog(cWarnMsg, "Element existed\n");
                    
                    /* Give semaphore */
                    mNodeDestroy(pNewNode);
                    mListGive(pList, ret);
                    if ((ret != cListSucc))
                        {
                        mErrLog(cWarnMsg, "Cannot give semaphore\n");
                        }
                    return cListErrEleExist;
                
                /* Greater */
                case cListGreater:
                    blForward = (pList->order == cListInc) ? false : true;
                    dHigh = (dMid == 0) ? 0 : (dMid - 1);
                    break;
                
                /* Lower */
                case cListLower:
                    blForward = (pList->order == cListInc) ? true : false;
                    dLow      = dMid + 1;
                    break;
                
                /* Compare fail */
                default:
                    mErrLog(cErrMsg, "Compare fail\n");
                    
                    /* Give semaphore */
                    mNodeDestroy(pNewNode);
                    mListGive(pList, ret);
                    if ((ret != cListSucc))
                        {
                        mErrLog(cWarnMsg, "Cannot give semaphore\n");
                        }
                    return cListErr;
                }
            
            /* Shift pointer */
            if (blForward)
                {
                if (pCurNode->pNext != null)
                    {
                    pCurNode = pCurNode->pNext;
                    }
                }
            else
                {
                if (pCurNode->pPrev != null)
                    {
                    pCurNode = pCurNode->pPrev;
                    }
                }
            }
        
        /* Compare the current node with new node to add new node */
        cmpRes    = pList->pCmpFunc(pCurNode->pData, pData);
        blForward = false;
        switch (cmpRes)
            {
            /* Equal */
            case cListEqual:
                mErrLog(cWarnMsg, "Element existed\n");
                
                /* Give semaphore */
                mNodeDestroy(pNewNode);
                mListGive(pList, ret);
                if ((ret != cListSucc))
                    {
                    mErrLog(cWarnMsg, "Cannot give semaphore\n");
                    }
                return cListErrEleExist;
            
            /* Greater */
            case cListGreater:
                blForward = (pList->order == cListInc) ? false : true;
                break;
            
            /* Lower */
            case cListLower:
                blForward = (pList->order == cListInc) ? true : false;
                break;
            
            /* Compare fail */
            default:
                mErrLog(cErrMsg, "Compare fail\n");
                
                /* Give semaphore */
                mNodeDestroy(pNewNode);
                mListGive(pList, ret);
                if ((ret != cListSucc))
                    {
                    mErrLog(cWarnMsg, "Cannot give semaphore\n");
                    }
                return cListErr;
            }
        
        /* Update link */
        if (blForward)
            {
            /* Let new node link to next node */
            pNewNode->pNext = pCurNode->pNext;
            if (pCurNode->pNext != null)
                {
                pCurNode->pNext->pPrev = pNewNode;
                }
            
            /* Let new node link to current node */
            pCurNode->pNext = pNewNode;
            pNewNode->pPrev = pCurNode;
            }
        else
            {
            /* Let new node link to the previous node */
            pNewNode->pPrev = pCurNode->pPrev;
            if (pCurNode->pPrev != null)
                {
                pCurNode->pPrev->pNext = pNewNode;
                }
            
            /* Let new node link to current node */
            pCurNode->pPrev = pNewNode;
            pNewNode->pNext = pCurNode;
            
            /* Update head */
            if (pNewNode->pPrev == null)
                {
                pList->pHead = pNewNode;
                }
            }
        
        /* Increase list size */
        pList->size = pList->size + 1;
        }
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListGet(tSortList  list, 
                                dword      idx,
                                void      *pData, 
                                dword      size)

Purpose      : This function is used to get a element at specified index

Inputs       : list                  - Sorted Linked List
               idx                   - Index
               size                  - Size of data

Outputs      : pData                 - Data                   

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListGet(tSortList list, dword idx, void *pData, dword size)
    {
    _tSortList *pList;
    tNode      *pNode;
    dword       dIdx;
    eListRet    ret;
    
    /* Check parameter */
    if ((list == null) || (pData == null) || (size == 0))
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Check index */
    if (idx >= pList->size)
        {
        mErrLog(cErrMsg, "Invalid index\n");
        
        /* Give semaphore */
        mListGive(pList, ret);
        if ((ret != cListSucc))
            {
            mErrLog(cWarnMsg, "Cannot give semaphore\n");
            }
        
        return cListErr;
        }
    
    /* Go to the specified position */
    pNode = pList->pHead;
    for (dIdx = 0; dIdx < idx; dIdx++)
        {
        pNode = pNode->pNext;
        }
    
    /* Return data */
    OsalMemCpy(pNode->pData, pData, size);
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListFlush(tSortList list)

Purpose      : This function is used to flush a list

Inputs       : list                  - Sorted Linked List

Outputs      : None

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListFlush(tSortList list)
    {
    _tSortList *pList;
    tNode      *pNode;
    eListRet    ret;
    
    /* Check parameter */
    if (list == null)
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Remove all elements */
    while (pList->pHead != null)
        {
        pNode        = pList->pHead;
        pList->pHead = pNode->pNext;
        mNodeDestroy(pNode);
        }
    
    /* Reset list size */
    pList->size = 0;
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
        
    return cListSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eListRet ListSize(tSortList list, dword *pSize)

Purpose      : This function is used to get size of list

Inputs       : list                  - Sorted Linked List

Outputs      : pSize                 - Size of list

Return       : List return code
------------------------------------------------------------------------------*/
eListRet ListSize(tSortList list, dword *pSize)
    {
    _tSortList *pList;
    eListRet    ret;
    
    /* Check parameter */
    if ((list == null) || (pSize == null))
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cListErr;
        }
    
    /* Get handle */
    pList = (_tSortList *)list;
    mListTake(pList, ret);
    if (ret != cListSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Return size */
    *pSize = pList->size;
    
    /* Give semaphore */
    mListGive(pList, ret);
    if ((ret != cListSucc))
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cListSucc;
    }

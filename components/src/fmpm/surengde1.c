/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengde1.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code DS3/E3 Transmisstion 
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ---------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mDe1LineK(pSurInfo)     
            mDe1PathK(pSurInfo) 
Purpose   : These macros are used to get K thresholds for DS1/E1 line and path.
Inputs    : pSurInfo                 device database
Return    : K threshold value
------------------------------------------------------------------------------*/
#define mDe1LineK(pSurInfo)  ((pSurInfo)->pDb->mastConf.kThres.ds1Line)
#define mDe1PathK(pSurInfo, frmType)                                                \
    (((frmType) == cSurDs1FrmEsf) ? ((pSurInfo)->pDb->mastConf.kThres.ds1EsfPath) : \
                                    ((pSurInfo)->pDb->mastConf.kThres.ds1SfPath))
    
/*------------------------------------------------------------------------------
Prototype : mNumCvPfe(prmMsg, numCv)
Purpose   : This macro is used to get number of CV-PFE base on PRM message
Inputs    : prmMsg                   - PRM message
Output    : numCv                    - number of CV
------------------------------------------------------------------------------*/
#define mNumCvPfe(prmMsg, numCv)                                               \
    if ((prmMsg).g1 == 1)                                                      \
        {                                                                      \
        numCv = 1;                                                             \
        }                                                                      \
    else if ((prmMsg).g2 == 1)                                                 \
        {                                                                      \
        numCv = 5;                                                             \
        }                                                                      \
    else if ((prmMsg).g3 == 1)                                                 \
        {                                                                      \
        numCv = 10;                                                            \
        }                                                                      \
    else if ((prmMsg).g4 == 1)                                                 \
        {                                                                      \
        numCv = 100;                                                           \
        }                                                                      \
    else if ((prmMsg).g5 == 1)                                                 \
        {                                                                      \
        numCv = 319;                                                           \
        }                                                                      \
    else if ((prmMsg).g6 == 1)                                                 \
        {                                                                      \
        numCv = 333;                                                           \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        numCv = 0;                                                             \
        }

/*------------------------------------------------------------------------------
Prototype : mDe1FailIndUpd(pSurInfo, pChnInfo, defName, failName, failChg)
Purpose   : This macro is used to update failure indication for DS1/E1 channel
Inputs    : pSurInfo				 - Surveillance device information
			pChnInfo                 - channel information
            defName                  - defect name (in structure)
            failName                 - failure name (in structure)
Outputs   : failChg                  - true if failure changed status
------------------------------------------------------------------------------*/
#define mDe1TerSoakExpr(defTime, terSoakTime)     (((dCurTime - (defTime)) >= (terSoakTime)) ? true : false)
#define mDe1FailIndUpd(pSurInfo, pChnInfo, defName, failName, failChg)                   \
    (failChg) = false;                                                         \
    if ((pChnInfo)->failure.failName.status != (pChnInfo)->status.defName.status)\
        {                                                                      \
        /* Set failure indication */                                           \
        if ((pChnInfo)->status.defName.status == true)                         \
            {                                                                  \
            if (mDecSoakExpr((pChnInfo)->status.defName.time, pSurInfo->pDb->mastConf.decSoakTime) == true)         \
                {                                                              \
                (pChnInfo)->failure.failName.status = true;                    \
                (pChnInfo)->failure.failName.time   = dCurTime;                \
                (failChg) = true;                                              \
                }                                                              \
            }                                                                  \
                                                                               \
        /* Clear failure indication */                                         \
        else                                                                   \
            {                                                                  \
            if (mDe1TerSoakExpr((pChnInfo)->status.defName.time, pSurInfo->pDb->mastConf.terSoakTime) == true)      \
                {                                                              \
                (pChnInfo)->failure.failName.status = false;                   \
                (pChnInfo)->failure.failName.time   = dCurTime;                \
                (failChg) = true;                                              \
                }                                                              \
            }                                                                  \
        }
        
/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
extern void AppThaPdhIdToSonetIdConvert(byte	ds3e3,
										byte	ds2e2,
										byte	ds1e1,
										byte	*ocnLine,
										byte	*sts,
										byte	*vtg,
										byte	*vt);

/*------------------------------------------------------------------------------
Prototype    : friend void SurDe1DefUpd(tSurDe1Info *pDe1Info, tSurDe1Def *pDe1Def)

Purpose      : This function is used to update DS1/E1 defect database

Inputs       : pDe1Def               - DS1/E1 defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurDe1DefUpd(tSurDe1Info *pDe1Info, tSurDe1Def *pDe1Def)
    {
    switch (pDe1Def->defect)
        {
        /* DS1/E1 line Loss Of Signal failure */
        case cFmDe1Los:
        	if (pDe1Info->status.los.status != pDe1Def->status)
        		{
				pDe1Info->status.los.status = pDe1Def->status;
				pDe1Info->status.los.time   = pDe1Def->time;
        		}
            break;
            
        case cFmDe1Lof:
            /*pDe1Info->status.lof.status = pDe1Def->status;
            pDe1Info->status.lof.time   = pDe1Def->time;*/
            break;

        /* DS1/E1 path Loss Of Frame failure */
        case cFmDe1Oof:
        	if (pDe1Info->status.oof.status != pDe1Def->status)
        		{
				pDe1Info->status.oof.status = pDe1Def->status;
				pDe1Info->status.oof.time   = pDe1Def->time;
        		}
            break;
    
        /* DS1/E1 path Severely Errored Frame */
        case cFmDe1Sef:
        	if (pDe1Info->status.sef.status != pDe1Def->status)
        		{
				pDe1Info->status.sef.status = pDe1Def->status;
				pDe1Info->status.sef.time   = pDe1Def->time;
        		}
            break;
            
        /* DS1/E1 path Alarm Indication Signal failure */
        case cFmDe1Ais:
        	if (pDe1Info->status.ais.status != pDe1Def->status)
        		{
				pDe1Info->status.ais.status = pDe1Def->status;
				pDe1Info->status.ais.time   = pDe1Def->time;
        		}
            break;
            
        /* DS1/E1 path Alarm Indication Signal - Customer Installation failure */
        case cFmDe1AisCi:
			if (pDe1Info->status.aisCi.status != pDe1Def->status)
        		{
				pDe1Info->status.aisCi.status = pDe1Def->status;
				pDe1Info->status.aisCi.time   = pDe1Def->time;
        		}
            break;
            
        /* DS1/E1 path Remote Alarm Indication failure */                     
        case cFmDe1Rai:
        	if (pDe1Info->status.rai.status != pDe1Def->status)
        		{
				pDe1Info->status.rai.status  = pDe1Def->status;
				pDe1Info->status.rai.time    = pDe1Def->time;
        		}
            break;
            
        /* DS1/E1 path Remote Alarm Indication - Customer Installation failure */
        case cFmDe1RaiCi:
        	if (pDe1Info->status.raiCi.status != pDe1Def->status)
        		{
				pDe1Info->status.raiCi.status  = pDe1Def->status;
				pDe1Info->status.raiCi.time    = pDe1Def->time;
        		}
            break;

        /* L bit defect */
        case cFmDe1Lbit:
        	if (pDe1Info->status.lBit.status != pDe1Def->status)
        		{
				pDe1Info->status.lBit.status  = pDe1Def->status;
				pDe1Info->status.lBit.time    = pDe1Def->time;
        		}
            break;
        /* R bit defect */
        case cFmDe1Rbit:
        	if (pDe1Info->status.rBit.status != pDe1Def->status)
        		{
				pDe1Info->status.rBit.status  = pDe1Def->status;
				pDe1Info->status.rBit.time    = pDe1Def->time;
        		}
            break;
        /* M bit defect */
        case cFmDe1Mbit:
        	if (pDe1Info->status.mBit.status != pDe1Def->status)
        		{
				pDe1Info->status.mBit.status  = pDe1Def->status;
				pDe1Info->status.mBit.time    = pDe1Def->time;
        		}
            break;


        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De1AllDefUpd(tSurInfo        *pSurInfo,
                                         const tSurDe1Id *pDe1Id)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pDe1Id                - DS1/E1 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
private void De1AllDefUpd(tSurInfo        *pSurInfo,
                          const tSurDe1Id *pDe1Id)
    {
    bool          blNewStat;
    tSurDe1Def    def;
    tSurDe1Info  *pDe1Info;
    dword         dDefStat;

    pDe1Info = NULL;
    /* Get DS1/E1 information */
    mSurDe1Info(pSurInfo, pDe1Id, pDe1Info);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pDe1Id, &(def.id), sizeof(tSurDe1Id));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurDe1, pDe1Id, &dDefStat);
    
    /* DS1/E1 line Loss Of Signal */
    blNewStat = mBitStat(dDefStat, cSurDe1LosMask);
    if (blNewStat != pDe1Info->status.los.status)
        {
        def.defect = cFmDe1Los;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* DS1/E1 path Out Of Frame */
    blNewStat = mBitStat(dDefStat, cSurDe1OofMask);
    if (blNewStat != pDe1Info->status.oof.status)
        {
        def.defect = cFmDe1Oof;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* DS1/E1 path Severely Errored Frame */
    blNewStat = mBitStat(dDefStat, cSurDe1SefMask);
    if (blNewStat != pDe1Info->status.sef.status)
        {
        def.defect = cFmDe1Sef;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* DS1/E1 path Alarm Indication Signal */
    blNewStat = mBitStat(dDefStat, cSurDe1AisMask);
    if (blNewStat != pDe1Info->status.ais.status)
        {
        def.defect = cFmDe1Ais;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* DS1/E1 path Alarm Indication Signal � Customer Installation */
    blNewStat = mBitStat(dDefStat, cSurDe1AisCiMask);
    if (blNewStat != pDe1Info->status.aisCi.status)
        {
        def.defect = cFmDe1AisCi;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* DS1/E1 path Remote Alarm Indication */
    blNewStat = mBitStat(dDefStat, cSurDe1RaiMask);
    if (blNewStat != pDe1Info->status.rai.status)
        {
        def.defect = cFmDe1Rai;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* DS1/E1 path Remote Alarm Indication - Customer Installation */
    blNewStat = mBitStat(dDefStat, cSurDe1RaiCiMask);
    if (blNewStat != pDe1Info->status.raiCi.status)
        {
        def.defect = cFmDe1RaiCi;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* L bit defect */
    blNewStat = mBitStat(dDefStat, cSurDe1LbitMask);
    if (blNewStat != pDe1Info->status.lBit.status)
        {
        def.defect = cFmDe1Lbit;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* R bit defect */
    blNewStat = mBitStat(dDefStat, cSurDe1RbitMask);
    if (blNewStat != pDe1Info->status.rBit.status)
        {
        def.defect = cFmDe1Rbit;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }

    /* M bit defect */
    blNewStat = mBitStat(dDefStat, cSurDe1MbitMask);
    if (blNewStat != pDe1Info->status.mBit.status)
        {
        def.defect = cFmDe1Mbit;
        def.status = blNewStat;
        SurDe1DefUpd(pDe1Info, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De1AllFailUpd(tSurInfo        *pSurInfo,
                                          const tSurDe1Id *pDe1Id)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pDe1Id                - DS1/E1 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void De1AllFailUpd(tSurInfo        *pSurInfo,
                           const tSurDe1Id *pDe1Id)
    {
    tSurTrblProf  *pTrblProf;
    tSurDe1Fail   *pFailInd;
    tSortList      failHis;
    tSurDe1Info   *pDe1Info;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;

    pDe1Info = NULL;

    /* Get DS1/E1 information */
    mSurDe1Info(pSurInfo, pDe1Id, pDe1Info);
    
    /* If DS1/E1 is over SONET/SDH,
     * check if LOS, LOF, AIS-L, AIS-P, LOP-P exist do not process DS1/E1 failure*/
    if (pDe1Info->conf.overSonetSdh == true)
    	{
		tSurStsInfo  *pStsInfo;
		tSurLineInfo *pLineInfo;
		tSurVtId	 vtId;
		byte		 line = 0;
		byte		 sts = 0;

		pStsInfo = NULL;
		pLineInfo = NULL;

		/* Get line, STS & VT information */
		/*AppThaPdhIdToSonetIdConvert(pDe1Id->de3Id - 1, pDe1Id->de2Id - 1, pDe1Id->de1Id - 1,
									&line, &sts, &(vtId.vtg), &vtId.vt);*/

		vtId.sts.line = line + 1;
		vtId.sts.localId = sts + 1;
		vtId.vtg++;
		vtId.vt++;
		mSurStsInfo(pSurInfo, &(vtId.sts), pStsInfo);
		mSurLineInfo(pSurInfo, &(vtId.sts.line), pLineInfo);

		/* Do not process any DS1/E1 failure if either LOS, LOF failure is declared at
		   section/line level */
		if ((pLineInfo->failure.los.status  == true) ||
			(pLineInfo->failure.lof.status  == true) ||
			(pStsInfo->failure.aisp.status  == true) ||
			(pStsInfo->failure.lopp.status  == true))
			{
			return;
			}
    	}

    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pDe1Info->failure);
    failHis   = pDe1Info->failHis;
    
    /* DS1/E1 Line LOS failure */
    mDe1FailIndUpd(pSurInfo, pDe1Info, los, los, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe1Info->msgInh.de1 == false) && 
                  (pDe1Info->msgInh.los == false));
        mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->los, failHis, failInfo, blNotf, cFmDe1Los, pTrblProf->de1Los);
        }
    
    /* DS1/E1 Path AIS-CI failure */
    mDe1FailIndUpd(pSurInfo, pDe1Info, aisCi, aisCi, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe1Info->msgInh.de1   == false) && 
                  (pDe1Info->msgInh.aisCi == false));
        mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->aisCi, failHis, failInfo, blNotf, cFmDe1AisCi, pTrblProf->de1AisCi);
        
        /* Update FC-P parameter */
        if (pFailInd->aisCi.status == true)
            {
            pDe1Info->parm.fcP.curSec += 1;
            }
            
        /* Clear LOF failure if it is present */
        if ((pFailInd->aisCi.status == true) &&
            (pFailInd->lof.status   == true))
            {
            pFailInd->lof.status = false;
            pFailInd->lof.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = ((pDe1Info->msgInh.de1 == false) && 
                      (pDe1Info->msgInh.lof == false));
            mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->lof, failHis, failInfo, blNotf, cFmDe1Lof, pTrblProf->de1Lof);            
            }
            
        /* Clear AIS failure if it is present */
        if ((pFailInd->aisCi.status == true) &&
            (pFailInd->ais.status   == true))
            {
            pFailInd->ais.status = false;
            pFailInd->ais.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = ((pDe1Info->msgInh.de1 == false) && 
                      (pDe1Info->msgInh.ais == false));
            mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->ais, failHis, failInfo, blNotf, cFmDe1Ais, pTrblProf->de1Ais);
            }
        }
    
    /* DS1/E1 Path AIS failure. Only declare this failure if AIS-CI failure is
       not present */
    if (pFailInd->aisCi.status == false)
        {
        mDe1FailIndUpd(pSurInfo, pDe1Info, ais, ais, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pDe1Info->msgInh.de1 == false) && 
                      (pDe1Info->msgInh.ais == false));
            mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->ais, failHis, failInfo, blNotf, cFmDe1Ais, pTrblProf->de1Ais);
            
            /* Update FC-P parameter */
            if (pFailInd->ais.status == true)
                {
                pDe1Info->parm.fcP.curSec += 1;
                }
                
            /* Clear LOF failure if it is present */
            if ((pFailInd->ais.status == true) &&
                (pFailInd->lof.status == true))
                {
                pFailInd->lof.status = false;
                pFailInd->lof.time   = dCurTime;
                
                /* Notify trouble */
                blNotf = ((pDe1Info->msgInh.de1 == false) && 
                          (pDe1Info->msgInh.lof == false));
                mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->lof, failHis, failInfo, blNotf, cFmDe1Lof, pTrblProf->de1Lof);            
                }
            }
        }
           
    /* DS1/E1 Path LOF failure. Only declare this failure if AIS and AIS-CI failure
       is not present */
    if ((pFailInd->ais.status   == false) && 
        (pFailInd->aisCi.status == false))
        {
        mDe1FailIndUpd(pSurInfo, pDe1Info, oof, lof, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pDe1Info->msgInh.de1 == false) && 
                      (pDe1Info->msgInh.lof == false));
            mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->lof, failHis, failInfo, blNotf, cFmDe1Lof, pTrblProf->de1Lof);
            
            /* Update FC-P parameter */
            if (pFailInd->lof.status == true)
                {
                pDe1Info->parm.fcP.curSec += 1;
                }
            }
        }
    
    /* DS1/E1 Path RAI-CI failure */
    blFailChg = (pDe1Info->status.raiCi.status != pFailInd->raiCi.status);
    if (blFailChg == true)
        {
        pFailInd->raiCi.status = pDe1Info->status.raiCi.status;
        pFailInd->raiCi.time   = dCurTime;
        
        /* Notify trouble */
        blNotf = (pDe1Info->msgInh.de1   == false) && 
                 (pDe1Info->msgInh.raiCi == false);
        mFailRept(pSurInfo,cSurDe1, pDe1Id, pFailInd->raiCi, failHis, failInfo, blNotf, cFmDe1RaiCi, pTrblProf->de1RaiCi);
                  
        /* Update FC-PFE parameter */
        if (pFailInd->raiCi.status == true)
            {
            pDe1Info->parm.fcPfe.curSec += 1;
            }
            
        /* Clear RAI failure if it is present */
        if ((pFailInd->raiCi.status == true) && 
            (pFailInd->rai.status   == true))
            {
            pFailInd->rai.status = false;
            pFailInd->rai.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = (pDe1Info->msgInh.de1 == false) && 
                     (pDe1Info->msgInh.rai == false);
            mFailRept(pSurInfo,cSurDe1, pDe1Id, pFailInd->rai, failHis, failInfo, blNotf, cFmDe1Rai, pTrblProf->de1Rai);
            }
        }
            
    /* DS1/E1 Path RAI failure. Only declare this failure if RAI-CI failure is 
       not present */
    if (pFailInd->raiCi.status == false)
        {
        blFailChg = (pDe1Info->status.rai.status != pFailInd->rai.status);
        if (blFailChg == true)
            {
            pFailInd->rai.status = pDe1Info->status.rai.status;
            pFailInd->rai.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = (pDe1Info->msgInh.de1 == false) && 
                     (pDe1Info->msgInh.rai == false);
            mFailRept(pSurInfo,cSurDe1, pDe1Id, pFailInd->rai, failHis, failInfo, blNotf, cFmDe1Rai, pTrblProf->de1Rai);
                      
            /* Update FC-PFE parameter */
            if (pFailInd->rai.status == true)
                {
                pDe1Info->parm.fcPfe.curSec += 1;
                }
            }
        }

    /* L bit failure */
	mDe1FailIndUpd(pSurInfo, pDe1Info, lBit, lBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pDe1Info->msgInh.de1 == false) &&
				  (pDe1Info->msgInh.lBit == false));
		mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->lBit, failHis, failInfo, blNotf, cFmDe1Lbit, pTrblProf->lBit);
		}

    /* R bit failure */
	mDe1FailIndUpd(pSurInfo, pDe1Info, rBit, rBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pDe1Info->msgInh.de1 == false) &&
				  (pDe1Info->msgInh.rBit == false));
		mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->rBit, failHis, failInfo, blNotf, cFmDe1Rbit, pTrblProf->rBit);
		}

    /* M bit failure */
	mDe1FailIndUpd(pSurInfo, pDe1Info, mBit, mBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pDe1Info->msgInh.de1 == false) &&
				  (pDe1Info->msgInh.mBit == false));
		mFailRept(pSurInfo, cSurDe1, pDe1Id, pFailInd->mBit, failHis, failInfo, blNotf, cFmDe1Mbit, pTrblProf->mBit);
		}
    }

/*------------------------------------------------------------------------------
Prototype    : private void De1PmParmUpd(tSurInfo        *pSurInfo,
                                         const tSurDe1Id *pDe1Id)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pDe1Id                - DS1/E1 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void De1PmParmUpd(tSurInfo        *pSurInfo,
                          const tSurDe1Id *pDe1Id)
    {
    tSurDe1Info   *pDe1Info;
    word           wDe1K;
    bool           blNewStat;
    tSurDe1Anomaly anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    word           cvPfe;
    tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pDe1Info = NULL;
    /* Get DS1/E1 information */
    mSurDe1Info(pSurInfo, pDe1Id, pDe1Info);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pDe1Info->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pDe1Info->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pDe1Info->parm.curEngTime.minute)
        {
        pDe1Info->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pDe1Info->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pDe1Info->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurDe1Anomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurDe1, pDe1Id, &anomaly);
        pDe1Info->parm.cvL.curSec += anomaly.bpvExz;
        
        /* Update CV-P based on frame type */
        if (pDe1Info->conf.frmType == cSurDs1FrmSf)
            {
            pDe1Info->parm.cvP.curSec += anomaly.fe;
            }
        else
            {
            pDe1Info->parm.cvP.curSec += anomaly.crc;
            }
        
        /* Update ES-P and CSS-P based on CS anomaly */
        if (anomaly.cs > 0)
            {
            pDe1Info->parm.cssP.curSec = 1;
            pDe1Info->parm.esP.curSec  = 1;
            }
        
        /* Update Far-End Line and Path parameters based on PRM. Only update if
           RAI and RAI-CI defect are not present */
        if ((pDe1Info->conf.frmType        == cSurDs1FrmEsf) &&
            (anomaly.prmMsg.newMsg         == true)          &&
            (pDe1Info->status.rai.status   == false)         &&
            (pDe1Info->status.raiCi.status == false))
            {
            /* Update CV-PFE parameter based on Gn bits in PRM */
            mNumCvPfe(anomaly.prmMsg, cvPfe);
            pDe1Info->parm.cvPfe.curSec += cvPfe;
            
            /* Update ES-LFE parameter based on LV bit in PRM */
            if (anomaly.prmMsg.lv == 1)
                {
                pDe1Info->parm.esLfe.curSec = 1;
                }
            
            /* Update SEFS-PFE, ES-PFE, and SES-PFE parameter based on SE bit in PRM */
            if (anomaly.prmMsg.se == 1)
                {
                pDe1Info->parm.sefsPfe.curSec = 1;
                pDe1Info->parm.esPfe.curSec   = 1;
                pDe1Info->parm.sesPfe.curSec  = 1;
                }
            
            /* Update CSS-PFE and ES-PFE parameter based on SL bit in PRM */
            if (anomaly.prmMsg.sl == 1)
                {
                pDe1Info->parm.cssPfe.curSec = 1;
                pDe1Info->parm.esPfe.curSec  = 1;
                }
            }
        }
    
    /* Update ES-L and SES-L parameters based on LOS defect */
    if (pDe1Info->status.los.status == true)
        {
        pDe1Info->parm.esL.curSec   = 1;
        pDe1Info->parm.sesL.curSec  = 1;
        pDe1Info->parm.lossL.curSec = 1;
        }
        
    /* Update SAS-P, ES-P and SES-P parameter based on AIS, AIS-CI and SEF defect */
    if ((pDe1Info->status.ais.status   == true) ||
        (pDe1Info->status.aisCi.status == true) ||
        (pDe1Info->status.sef.status   == true))
        {
        pDe1Info->parm.esP.curSec   = 1;
        pDe1Info->parm.sesP.curSec  = 1;
        pDe1Info->parm.sasP.curSec  = 1;
        }
    
    /* Update AISS-P and AISSCI-P parameter based on AIS and AIS-CI defect respectively */
    if (pDe1Info->status.ais.status == true)
        {
        pDe1Info->parm.aissP.curSec = 1;
        }
    if (pDe1Info->status.aisCi.status == true)
        {
        pDe1Info->parm.aissCiP.curSec = 1;
        }
    
    /* Update ES-PFE and SES-PFE parameter based on RAI defect */
    if ((pDe1Info->status.rai.status   == true) ||
        (pDe1Info->status.raiCi.status == true))
        {
        pDe1Info->parm.esPfe.curSec  = 1;
        pDe1Info->parm.sesPfe.curSec = 1;
        }
    
    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update ES-L parameter based on CV-L */
        if (pDe1Info->parm.cvL.curSec != 0)
            {
            pDe1Info->parm.esL.curSec = 1;
            }
        
        /* Update ES-P, ESA-P, and ESB-P parameter based on CV-P */
        wDe1K = mDe1PathK(pSurInfo, pDe1Info->conf.frmType);
        if (pDe1Info->parm.cvP.curSec != 0)
            {
            pDe1Info->parm.esP.curSec = 1;
            
            /* In ESF format, only update ESA-P and ESB-P parameter only if BOTH
               AIS defect AND SEF defect are not present */
            if ((pDe1Info->conf.frmType == cSurDs1FrmEsf) &&
                (pDe1Info->parm.sasP.curSec == 0))
                {
                /* Update ESA-P parameter if CV-P = 1 */
                if (pDe1Info->parm.cvP.curSec == 1)
                    {
                    pDe1Info->parm.esaP.curSec = 1;
                    }
                    
                /* Update ESB-P parameter if < CV-P < K-threshold */
                else if (pDe1Info->parm.cvP.curSec < wDe1K)
                    {
                    pDe1Info->parm.esbP.curSec = 1;
                    }
                }
            }
        
        /* Update ES-PFE, ESA-PFE, and ESB-PFE parameter based on CV-PFE */
        if (pDe1Info->parm.cvPfe.curSec != 0)
            {
            pDe1Info->parm.esPfe.curSec = 1;
            
            /* In ESF format, only update ESA-PFE and ESB-PFE parameter only if
               far-end SEF defect is not present (SE bit = 0) */
            if ((pDe1Info->conf.frmType == cSurDs1FrmEsf) &&
                (pDe1Info->parm.sefsPfe.curSec == 0))
                {
                /* Update ESA-PFE if CV-PFE = 1 */
                if (pDe1Info->parm.cvPfe.curSec == 1)
                    {
                    pDe1Info->parm.esaPfe.curSec = 1;
                    }
                    
                /* Update ESB-PFE if 1 < CV-PFE < K-threshold */
                else if (pDe1Info->parm.cvPfe.curSec < wDe1K)
                    {
                    pDe1Info->parm.esbPfe.curSec = 1;
                    }
                }
            }

        /* Update SES-L parameter based CV-L and DS1 Line K-threshold */
        wDe1K = mDe1LineK(pSurInfo);
        if (pDe1Info->parm.cvL.curSec >= wDe1K)
            {
            pDe1Info->parm.sesL.curSec = 1;
            }

        /* Update SES-P parameter based on CV-P and DS1 Path K-threshold */
        wDe1K = mDe1PathK(pSurInfo, pDe1Info->conf.frmType);
        if ((pDe1Info->conf.frmType   == cSurDs1FrmSf) &&
            (pDe1Info->conf.measureFs == true))
            {
            wDe1K = wDe1K * 2;
            }
        if (pDe1Info->parm.cvP.curSec >= wDe1K)
            {
            pDe1Info->parm.sesP.curSec = 1;
            }
        
        /* Update SES-PFE parameter based on CV-PFE and DS1 Path K-threshold */
        if ((pDe1Info->conf.frmType      == cSurDs1FrmEsf) && 
            (pDe1Info->parm.cvPfe.curSec >= wDe1K))
            {
            pDe1Info->parm.sesPfe.curSec = 1;
            }
              
        /* Update UAS- parameter during unavailable time (near-end and far-end) */
        if (pDe1Info->status.neUat == true)
            {
            pDe1Info->parm.uasP.curSec = 1;
            }

        if ((pDe1Info->conf.frmType == cSurDs1FrmEsf) &&
            (pDe1Info->status.feUat == true))
            {
            pDe1Info->parm.uasPfe.curSec = 1;
            }

        /* Check if DS1/E1 Near-End UAS just enter or exit and adjust registers */
        blNewStat = (pDe1Info->parm.sesP.curSec == 1) ? true : false;
        if (blNewStat != pDe1Info->status.neUatPot)
            {
            pDe1Info->status.nePotCnt = 1;
            }
        else
            {
            pDe1Info->status.nePotCnt++;
            }

        /* Declare potential for entering or exiting UAT if the condition is met */
        if (pDe1Info->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pDe1Info->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pDe1Info->parm.esP));
                mCurPerNegAdj(&(pDe1Info->parm.esaP));
                mCurPerNegAdj(&(pDe1Info->parm.esbP));
                mCurPerNegAdj(&(pDe1Info->parm.sesP));
                mCurPerNegAdj(&(pDe1Info->parm.aissP));
                mCurPerNegAdj(&(pDe1Info->parm.aissCiP));
                mCurPerNegAdj(&(pDe1Info->parm.sasP));
                mCurPerNegAdj(&(pDe1Info->parm.cssP));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pDe1Info->parm.esP));
                    mPrePerNegAdj(&(pDe1Info->parm.esaP));
                    mPrePerNegAdj(&(pDe1Info->parm.esbP));
                    mPrePerNegAdj(&(pDe1Info->parm.sesP));
                    mPrePerNegAdj(&(pDe1Info->parm.aissP));
                    mPrePerNegAdj(&(pDe1Info->parm.aissCiP));
                    mPrePerNegAdj(&(pDe1Info->parm.sasP));
                    mPrePerNegAdj(&(pDe1Info->parm.cssP));
                    }

                /* Update UAS */
            	pDe1Info->parm.uasP.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pDe1Info->parm.cvP));
                mCurPerPosAdj(&(pDe1Info->parm.esP));
                mCurPerPosAdj(&(pDe1Info->parm.esaP));
                mCurPerPosAdj(&(pDe1Info->parm.esbP));
                mCurPerPosAdj(&(pDe1Info->parm.sesP));
                mCurPerPosAdj(&(pDe1Info->parm.aissP));
                mCurPerPosAdj(&(pDe1Info->parm.aissCiP));
                mCurPerPosAdj(&(pDe1Info->parm.sasP));
                mCurPerPosAdj(&(pDe1Info->parm.cssP));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pDe1Info->parm.cvP));
                    mPrePerPosAdj(&(pDe1Info->parm.esP));
                    mPrePerPosAdj(&(pDe1Info->parm.esaP));
                    mPrePerPosAdj(&(pDe1Info->parm.esbP));
                    mPrePerPosAdj(&(pDe1Info->parm.sesP));
                    mPrePerPosAdj(&(pDe1Info->parm.aissP));
                    mPrePerPosAdj(&(pDe1Info->parm.aissCiP));
                    mPrePerPosAdj(&(pDe1Info->parm.sasP));
                    mPrePerPosAdj(&(pDe1Info->parm.cssP));
                    }

                /* Update UAS */
            	if (pDe1Info->parm.uasP.curPer.value >= 10)
            		{
            		pDe1Info->parm.uasP.curPer.value -= 10;
            		}
            	else
            		{
            		pDe1Info->parm.uasP.curPer.value = 0;
            		}
            	pDe1Info->parm.uasP.curSec = 0;
                }
            }

        /* Check if DS1/E1 Far-End UAS just enter or exit and adjust registers */
        if (pDe1Info->conf.frmType == cSurDs1FrmEsf)
            {
            blNewStat = (pDe1Info->parm.sesPfe.curSec == 1) ? true : false;
            if (blNewStat != pDe1Info->status.feUatPot)
                {
                pDe1Info->status.fePotCnt = 1;
                }
            else
                {
                pDe1Info->status.fePotCnt++;
                }

            /* Declare potential for entering or exiting UAT if the condition is met */
            if (pDe1Info->status.fePotCnt == 10)
                {
                /* Update unavailable time status */
                pDe1Info->status.feUat = blNewStat;

                /* UAS just entered */
                if (blNewStat == true)
                    {
                    mCurPerNegAdj(&(pDe1Info->parm.esLfe));
                    mCurPerNegAdj(&(pDe1Info->parm.sefsPfe));
                    mCurPerNegAdj(&(pDe1Info->parm.esPfe));
                    mCurPerNegAdj(&(pDe1Info->parm.esaPfe));
                    mCurPerNegAdj(&(pDe1Info->parm.esbPfe));
                    mCurPerNegAdj(&(pDe1Info->parm.sesPfe));
                    mCurPerNegAdj(&(pDe1Info->parm.cssPfe));
                    if (((currentTime.minute % 15) == 0) &&
                        (currentTime.second < 10))
                        {
                        mPrePerNegAdj(&(pDe1Info->parm.esLfe));
                        mPrePerNegAdj(&(pDe1Info->parm.sefsPfe));
                        mPrePerNegAdj(&(pDe1Info->parm.esPfe));
                        mPrePerNegAdj(&(pDe1Info->parm.esaPfe));
                        mPrePerNegAdj(&(pDe1Info->parm.esbPfe));
                        mPrePerNegAdj(&(pDe1Info->parm.sesPfe));
                        mPrePerNegAdj(&(pDe1Info->parm.cssPfe));
                        }

					/* Update UAS */
					pDe1Info->parm.uasPfe.curPer.value += 10;
                    }

                /* UAS just exited */
                else
                    {
                    mCurPerPosAdj(&(pDe1Info->parm.esLfe));
                    mCurPerPosAdj(&(pDe1Info->parm.sefsPfe));
                    mCurPerPosAdj(&(pDe1Info->parm.cvPfe));
                    mCurPerPosAdj(&(pDe1Info->parm.esPfe));
                    mCurPerPosAdj(&(pDe1Info->parm.esaPfe));
                    mCurPerPosAdj(&(pDe1Info->parm.esbPfe));
                    mCurPerPosAdj(&(pDe1Info->parm.sesPfe));
                    mCurPerPosAdj(&(pDe1Info->parm.cssPfe));
                    if (((currentTime.minute % 15) == 0) &&
                        (currentTime.second < 10))
                        {
                        mPrePerPosAdj(&(pDe1Info->parm.esLfe));
                        mPrePerPosAdj(&(pDe1Info->parm.sefsPfe));
                        mPrePerPosAdj(&(pDe1Info->parm.cvPfe));
                        mPrePerPosAdj(&(pDe1Info->parm.esPfe));
                        mPrePerPosAdj(&(pDe1Info->parm.esaPfe));
                        mPrePerPosAdj(&(pDe1Info->parm.esbPfe));
                        mPrePerPosAdj(&(pDe1Info->parm.sesPfe));
                        mPrePerPosAdj(&(pDe1Info->parm.cssPfe));
                        }

					/* Update UAS */
					if (pDe1Info->parm.uasPfe.curPer.value >= 10)
						{
						pDe1Info->parm.uasPfe.curPer.value -= 10;
						}
					else
						{
						pDe1Info->parm.uasPfe.curPer.value = 0;
						}
					pDe1Info->parm.uasPfe.curSec = 0;
                    }
                }
            }
        
        /* Reset all parameters' accumulate inhibition mode */
        pDe1Info->parm.cvL.inhibit     = false;
        pDe1Info->parm.cvP.inhibit     = false;
        pDe1Info->parm.esP.inhibit     = false;
        pDe1Info->parm.esaP.inhibit    = false;
        pDe1Info->parm.esbP.inhibit    = false;
        pDe1Info->parm.sesP.inhibit    = false;
        pDe1Info->parm.aissP.inhibit   = false;
        pDe1Info->parm.aissCiP.inhibit = false;
        pDe1Info->parm.sasP.inhibit    = false;
        pDe1Info->parm.cssP.inhibit    = false;
        pDe1Info->parm.esLfe.inhibit   = false;
        pDe1Info->parm.sefsPfe.inhibit = false;
        pDe1Info->parm.cvPfe.inhibit   = false;
        pDe1Info->parm.esPfe.inhibit   = false;
        pDe1Info->parm.esaPfe.inhibit  = false;
        pDe1Info->parm.esbPfe.inhibit  = false;
        pDe1Info->parm.sesPfe.inhibit  = false;
        pDe1Info->parm.cssPfe.inhibit  = false;
        
        /* Inhibit CV-L when the current second is in SES-L */
        if (pDe1Info->parm.sesL.curSec != 0)
            {
            pDe1Info->parm.cvL.inhibit = true;
            }
        
        /* Inhibit all parameters (except UAS-P, FC-P, UAS-PFE, and FCP-PFE) if
           the current second is UAS (near-end and far-end) */
        if ((pDe1Info->status.neUat == true))
            {
            pDe1Info->parm.cvP.inhibit     = true;
            pDe1Info->parm.esP.inhibit     = true;
            pDe1Info->parm.esaP.inhibit    = true;
            pDe1Info->parm.esbP.inhibit    = true;
            pDe1Info->parm.sesP.inhibit    = true;
            pDe1Info->parm.aissP.inhibit   = true;
            pDe1Info->parm.aissCiP.inhibit = true;
            pDe1Info->parm.sasP.inhibit    = true;
            pDe1Info->parm.cssP.inhibit    = true;
            }
        if (pDe1Info->status.feUat == true)
            {
            pDe1Info->parm.cvPfe.inhibit   = true;
            pDe1Info->parm.sefsPfe.inhibit = true;
            pDe1Info->parm.esPfe.inhibit   = true;
            pDe1Info->parm.esaPfe.inhibit  = true;
            pDe1Info->parm.esbPfe.inhibit  = true;
            pDe1Info->parm.sesPfe.inhibit  = true;
            pDe1Info->parm.cssPfe.inhibit  = true;
            }

        /* Inhibit CV-P when the current second is in SES-P */
        if (pDe1Info->parm.sesP.curSec == 1)
            {
            pDe1Info->parm.cvP.inhibit = true;
            }
            
        /* Inhibit CV-PFE when the current second is in SES-PFE */
        if (pDe1Info->parm.sesPfe.curSec == 1)
            {
            pDe1Info->parm.cvPfe.inhibit = true;
            }

        /* Increase DS1/E1 Near-End parameters' adjustment registers */
        /* In available time */
        if (pDe1Info->status.neUat == false)
            {
            /* Potential for entering unavailable time */
            if ((pDe1Info->parm.sesP.curSec == 1))
                {
                mNegInc(&(pDe1Info->parm.esP));
                mNegInc(&(pDe1Info->parm.esaP));
                mNegInc(&(pDe1Info->parm.esbP));
                mNegInc(&(pDe1Info->parm.sesP));
                mNegInc(&(pDe1Info->parm.aissP));
                mNegInc(&(pDe1Info->parm.aissCiP));
                mNegInc(&(pDe1Info->parm.sasP));
                mNegInc(&(pDe1Info->parm.cssP));
                }
            }
            
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pDe1Info->parm.sesP.curSec == 0)
                {
                mPosInc(&(pDe1Info->parm.cvP));
                mPosInc(&(pDe1Info->parm.esP));
                mPosInc(&(pDe1Info->parm.esaP));
                mPosInc(&(pDe1Info->parm.esbP));
                mPosInc(&(pDe1Info->parm.sesP));
                mPosInc(&(pDe1Info->parm.aissP));
                mPosInc(&(pDe1Info->parm.aissCiP));
                mPosInc(&(pDe1Info->parm.sasP));
                mPosInc(&(pDe1Info->parm.cssP));
                }
            }

        /* Increase DS1/E1 Far-End parameter's adjustment registers */
        if (pDe1Info->status.neUat == false)
            {
            /* In available time */
            if (pDe1Info->status.feUat == false)
                {
                /* Potential for entering unavailable time */
                if (pDe1Info->parm.sesPfe.curSec == 1)
                    {
                    mNegInc(&(pDe1Info->parm.sefsPfe));
                    mNegInc(&(pDe1Info->parm.esPfe));
                    mNegInc(&(pDe1Info->parm.esaPfe));
                    mNegInc(&(pDe1Info->parm.esbPfe));
                    mNegInc(&(pDe1Info->parm.sesPfe));
                    mNegInc(&(pDe1Info->parm.cssPfe));
                    }
                }
                
            /* In unavailable time */
            else
                {
                /* Potential for exiting unavailable time */
                if (pDe1Info->parm.sesPfe.curSec == 0)
                    {
                    mPosInc(&(pDe1Info->parm.sefsPfe));
                    mPosInc(&(pDe1Info->parm.cvPfe));
                    mPosInc(&(pDe1Info->parm.esPfe));
                    mPosInc(&(pDe1Info->parm.esaPfe));
                    mPosInc(&(pDe1Info->parm.esbPfe));
                    mPosInc(&(pDe1Info->parm.sesPfe));
                    mPosInc(&(pDe1Info->parm.cssPfe));
                    }
                }
            }
            
        /* Clear Near-End parameters' adjustment registers */
        if (pDe1Info->status.neUat == false)
            {
            if ((pDe1Info->parm.sesP.curSec == 0) &&
                (pDe1Info->status.neUatPot  == true))
                {
                mNegClr(&(pDe1Info->parm.esP));
                mNegClr(&(pDe1Info->parm.esaP));
                mNegClr(&(pDe1Info->parm.esbP));
                mNegClr(&(pDe1Info->parm.sesP));
                mNegClr(&(pDe1Info->parm.aissP));
                mNegClr(&(pDe1Info->parm.aissCiP));
                mNegClr(&(pDe1Info->parm.sasP));
                mNegClr(&(pDe1Info->parm.cssP));
                }
            }
        else if ((pDe1Info->parm.sesP.curSec == 1) &&
                 (pDe1Info->status.neUatPot  == false))
            {
            mPosClr(&(pDe1Info->parm.cvP));
            mPosClr(&(pDe1Info->parm.esP));
            mPosClr(&(pDe1Info->parm.esaP));
            mPosClr(&(pDe1Info->parm.esbP));
            mPosClr(&(pDe1Info->parm.sesP));
            mPosClr(&(pDe1Info->parm.aissP));
            mPosClr(&(pDe1Info->parm.aissCiP));
            mPosClr(&(pDe1Info->parm.sasP));
            mPosClr(&(pDe1Info->parm.cssP));
            }

        /* Clear DS1/E1 Far-End parameters' adjustment registers */
        if (pDe1Info->conf.frmType == cSurDs1FrmEsf)
            {
            if (pDe1Info->status.feUat == false)
                {
                if ((pDe1Info->parm.sesPfe.curSec == 0) &&
                    (pDe1Info->status.feUatPot    == true))
                    {
                    mNegClr(&(pDe1Info->parm.sefsPfe));
                    mNegClr(&(pDe1Info->parm.esPfe));
                    mNegClr(&(pDe1Info->parm.esaPfe));
                    mNegClr(&(pDe1Info->parm.esbPfe));
                    mNegClr(&(pDe1Info->parm.sesPfe));
                    mNegClr(&(pDe1Info->parm.cssPfe));
                    }
                }
            else if ((pDe1Info->parm.sesPfe.curSec == 1) &&
                     (pDe1Info->status.feUatPot    == false))
                {
                mPosClr(&(pDe1Info->parm.sefsPfe));
                mPosClr(&(pDe1Info->parm.cvPfe));
                mPosClr(&(pDe1Info->parm.esPfe));
                mPosClr(&(pDe1Info->parm.esaPfe));
                mPosClr(&(pDe1Info->parm.esbPfe));
                mPosClr(&(pDe1Info->parm.sesPfe));
                mPosClr(&(pDe1Info->parm.cssPfe));
                }
            }

        /* Store current second statuses */
        pDe1Info->status.neUatPot = (pDe1Info->parm.sesP.curSec   == 1) ? true : false;
        pDe1Info->status.feUatPot = (pDe1Info->parm.sesPfe.curSec == 1) ? true : false;

        /* Accumulate current period registers */
        mCurPerAcc(&(pDe1Info->parm.cvL));
        mCurPerAcc(&(pDe1Info->parm.esL));
        mCurPerAcc(&(pDe1Info->parm.sesL));
        mCurPerAcc(&(pDe1Info->parm.lossL));
        mCurPerAcc(&(pDe1Info->parm.cvP));
        mCurPerAcc(&(pDe1Info->parm.esP));
        mCurPerAcc(&(pDe1Info->parm.esaP));
        mCurPerAcc(&(pDe1Info->parm.esbP));
        mCurPerAcc(&(pDe1Info->parm.sesP));
        mCurPerAcc(&(pDe1Info->parm.aissP));
        mCurPerAcc(&(pDe1Info->parm.aissCiP));
        mCurPerAcc(&(pDe1Info->parm.sasP));
        mCurPerAcc(&(pDe1Info->parm.cssP));
        mCurPerAcc(&(pDe1Info->parm.uasP));
        mCurPerAcc(&(pDe1Info->parm.fcP));
        mCurPerAcc(&(pDe1Info->parm.esLfe));
        mCurPerAcc(&(pDe1Info->parm.sefsPfe));
        mCurPerAcc(&(pDe1Info->parm.cvPfe));
        mCurPerAcc(&(pDe1Info->parm.esPfe));
        mCurPerAcc(&(pDe1Info->parm.esaPfe));
        mCurPerAcc(&(pDe1Info->parm.esbPfe));
        mCurPerAcc(&(pDe1Info->parm.sesPfe));
        mCurPerAcc(&(pDe1Info->parm.cssPfe));
        mCurPerAcc(&(pDe1Info->parm.fcPfe));
        mCurPerAcc(&(pDe1Info->parm.uasPfe));

        /* Check and report TCA */
        blNotf = ((pDe1Info->msgInh.de1        == false) &&
                  (pDe1Info->msgInh.tca        == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Report TCAs of Line parameters */
        SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1CvL,   &(pDe1Info->parm.cvL),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsL,   &(pDe1Info->parm.esL),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1SesL,  &(pDe1Info->parm.sesL),  blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1LossL, &(pDe1Info->parm.lossL), blNotf, &tcaInfo);

        /* Report TCAs of Near-End Path parameters */
        if ((pDe1Info->status.neUat    == false) &&
            (pDe1Info->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1CvP    ,&(pDe1Info->parm.cvP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsP    ,&(pDe1Info->parm.esP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsaP   ,&(pDe1Info->parm.esaP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsbP   ,&(pDe1Info->parm.esbP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1SesP   ,&(pDe1Info->parm.sesP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1AissP  ,&(pDe1Info->parm.aissP),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1AissCiP,&(pDe1Info->parm.aissCiP), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1SasP   ,&(pDe1Info->parm.sasP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1CssP   ,&(pDe1Info->parm.cssP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1FcP    ,&(pDe1Info->parm.fcP),     blNotf, &tcaInfo);
            }
        
        /* Report TCAs of Far-End Path parameters */
        if ((pDe1Info->conf.frmType == cSurDs1FrmEsf) &&
            (pDe1Info->status.feUat    == false)      &&
            (pDe1Info->status.feUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsLfe,  &(pDe1Info->parm.esLfe),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1SefsPfe,&(pDe1Info->parm.sefsPfe), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1CvPfe,  &(pDe1Info->parm.cvPfe),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsPfe,  &(pDe1Info->parm.esPfe),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsaPfe, &(pDe1Info->parm.esaPfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1EsbPfe, &(pDe1Info->parm.esbPfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1SesPfe, &(pDe1Info->parm.sesPfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1CssPfe, &(pDe1Info->parm.cssPfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1FcPfe,  &(pDe1Info->parm.fcPfe),   blNotf, &tcaInfo);
            }
        
        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1UasP, &(pDe1Info->parm.uasP), blNotf, &tcaInfo);
        if (pDe1Info->conf.frmType == cSurDs1FrmEsf)
            {
            SurTcaReport(pSurInfo, cSurDe1, pDe1Id, pDe1Info, cPmDe1UasPfe, &(pDe1Info->parm.uasPfe), blNotf, &tcaInfo);
            }
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pDe1Info->parm.cvL));
        mStackDown(&(pDe1Info->parm.esL));
        mStackDown(&(pDe1Info->parm.sesL));
        mStackDown(&(pDe1Info->parm.lossL));
        mStackDown(&(pDe1Info->parm.cvP));
        mStackDown(&(pDe1Info->parm.esP));
        mStackDown(&(pDe1Info->parm.esaP));
        mStackDown(&(pDe1Info->parm.esbP));
        mStackDown(&(pDe1Info->parm.sesP));
        mStackDown(&(pDe1Info->parm.aissP));
        mStackDown(&(pDe1Info->parm.aissCiP));
        mStackDown(&(pDe1Info->parm.sasP));
        mStackDown(&(pDe1Info->parm.cssP));
        mStackDown(&(pDe1Info->parm.uasP));
        mStackDown(&(pDe1Info->parm.fcP));
        mStackDown(&(pDe1Info->parm.esLfe));
        mStackDown(&(pDe1Info->parm.sefsPfe));
        mStackDown(&(pDe1Info->parm.cvPfe));
        mStackDown(&(pDe1Info->parm.esPfe));
        mStackDown(&(pDe1Info->parm.esaPfe));
        mStackDown(&(pDe1Info->parm.esbPfe));
        mStackDown(&(pDe1Info->parm.sesPfe));
        mStackDown(&(pDe1Info->parm.cssPfe));
        mStackDown(&(pDe1Info->parm.fcPfe));
        mStackDown(&(pDe1Info->parm.uasPfe));

        /* Update time for period */
        pDe1Info->parm.prePerStartTime = pDe1Info->parm.curPerStartTime;
        pDe1Info->parm.prePerEndTime = dCurTime;
        pDe1Info->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pDe1Info->parm.cvL));
        mDayShift(&(pDe1Info->parm.esL));
        mDayShift(&(pDe1Info->parm.sesL));
        mDayShift(&(pDe1Info->parm.lossL));
        mDayShift(&(pDe1Info->parm.cvP));
        mDayShift(&(pDe1Info->parm.esP));
        mDayShift(&(pDe1Info->parm.esaP));
        mDayShift(&(pDe1Info->parm.esbP));
        mDayShift(&(pDe1Info->parm.sesP));
        mDayShift(&(pDe1Info->parm.aissP));
        mDayShift(&(pDe1Info->parm.aissCiP));
        mDayShift(&(pDe1Info->parm.sasP));
        mDayShift(&(pDe1Info->parm.cssP));
        mDayShift(&(pDe1Info->parm.uasP));
        mDayShift(&(pDe1Info->parm.fcP));
        mDayShift(&(pDe1Info->parm.esLfe));
        mDayShift(&(pDe1Info->parm.sefsPfe));
        mDayShift(&(pDe1Info->parm.cvPfe));
        mDayShift(&(pDe1Info->parm.esPfe));
        mDayShift(&(pDe1Info->parm.esaPfe));
        mDayShift(&(pDe1Info->parm.esbPfe));
        mDayShift(&(pDe1Info->parm.sesPfe));
        mDayShift(&(pDe1Info->parm.cssPfe));
        mDayShift(&(pDe1Info->parm.fcPfe));
        mDayShift(&(pDe1Info->parm.uasPfe));

        /* Update time for day */
        pDe1Info->parm.preDayStartTime = pDe1Info->parm.curDayStartTime;
        pDe1Info->parm.preDayEndTime = dCurTime;
        pDe1Info->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurDe1StatUpd(tSurDev          device, 
                                         const tSurDe1Id *pDe1Id)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input DS1/E1.

Inputs       : device                - device
               pDe1Id                - DS1/E1 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurDe1StatUpd(tSurDev device, const tSurDe1Id *pDe1Id)
    {
    tSurInfo       *pSurInfo;

    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) && 
        (pSurInfo->interface.DefStatGet    != null))
        {
        De1AllDefUpd(pSurInfo, pDe1Id);
        }
    
    /* Declare/terminate failures */
    De1AllFailUpd(pSurInfo, pDe1Id);
    
    /* Update performance registers */
    De1PmParmUpd(pSurInfo, pDe1Id);
    }

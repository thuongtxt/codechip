/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengline.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code SONET/SDH Line Transmisstion
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mLineKval(pSurInfo, lineRate, kSec, kLine, valid)
Purpose   : This macro is used to get K threshold of Section and Line
Inputs    : pSurInfo                 - device database
            lineRate                 - line rate
outputs   : kSec                     - K threshold of Section
            kLine                    - K threshold of Line
            valid                    - true to indicate that K threshold is
                                       valid or not
------------------------------------------------------------------------------*/
#define mLineKval(pSurInfo, lineRate, kSec, kLine, valid)                      \
    valid = true;                                                              \
    switch (lineRate)                                                          \
        {                                                                      \
        case cSurOc1Stm0:                                                      \
            kSec  = (pSurInfo)->pDb->mastConf.kThres.oc1Sec;                    \
            kLine = (pSurInfo)->pDb->mastConf.kThres.oc1Line;                   \
            break;                                                             \
        case cSurOc3Stm1:                                                      \
            kSec  = (pSurInfo)->pDb->mastConf.kThres.oc3Sec;                    \
            kLine = (pSurInfo)->pDb->mastConf.kThres.oc3Line;                   \
            break;                                                             \
        case cSurOc12Stm4:                                                     \
            kSec  = (pSurInfo)->pDb->mastConf.kThres.oc12Sec;                   \
            kLine = (pSurInfo)->pDb->mastConf.kThres.oc12Line;                  \
            break;                                                             \
        case cSurOc48Stm16:                                                    \
            kSec  = (pSurInfo)->pDb->mastConf.kThres.oc48Sec;                   \
            kLine = (pSurInfo)->pDb->mastConf.kThres.oc48Line;                  \
            break;                                                             \
        case cSurOc192Stm64:                                                   \
            kSec  = (pSurInfo)->pDb->mastConf.kThres.oc192Sec;                  \
            kLine = (pSurInfo)->pDb->mastConf.kThres.oc192Line;                 \
            break;                                                             \
        case cSurOc768Stm256:                                                  \
            kSec  = (pSurInfo)->pDb->mastConf.kThres.oc768Sec;                  \
            kLine = (pSurInfo)->pDb->mastConf.kThres.oc768Line;                 \
            break;                                                             \
                                                                               \
        /* Not support */                                                      \
        default:                                                               \
            valid = false;                                                     \
            break;                                                             \
        }                                                                      \

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : friend void SurLineDefUpd(tSurLineInfo *pLineInfo, tSurLineDef *pLineDef)

Purpose      : This function is used to update line defect database

Inputs       : pLineDef              - Line defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurLineDefUpd(tSurLineInfo *pLineInfo, tSurLineDef *pLineDef)
    {
    switch (pLineDef->defect)
        {
        /* Section LOS defect */
        case cFmSecLos:
        	if (pLineInfo->status.los.status != pLineDef->status)
        		{
				pLineInfo->status.los.status = pLineDef->status;
				pLineInfo->status.los.time   = pLineDef->time;
        		}
            break;

        /* Section LOF defect */
        case cFmSecLof:
        	if (pLineInfo->status.lof.status != pLineDef->status)
        		{
				pLineInfo->status.lof.status = pLineDef->status;
				pLineInfo->status.lof.time   = pLineDef->time;
        		}
            break;

        /* Section SEF defect */
        case cFmSecSef:
        	if (pLineInfo->status.sef.status != pLineDef->status)
        		{
				pLineInfo->status.sef.status = pLineDef->status;
				pLineInfo->status.sef.time   = pLineDef->time;
        		}
            break;

        /* Section TIM-S defect */
        case cFmSecTim:
        	if (pLineInfo->status.tims.status != pLineDef->status)
        		{
				pLineInfo->status.tims.status = pLineDef->status;
				pLineInfo->status.tims.time   = pLineDef->time;
        		}
            break;

        /* Line BER-SF */
        case cFmLineBerSf:
        	if (pLineInfo->status.lineBerSf.status != pLineDef->status)
        		{
				pLineInfo->status.lineBerSf.status = pLineDef->status;
				pLineInfo->status.lineBerSf.time   = pLineDef->time;
        		}
            break;

        /* Line BER-SD */
        case cFmLineBerSd:
        	if (pLineInfo->status.lineBerSd.status != pLineDef->status)
        		{
				pLineInfo->status.lineBerSd.status = pLineDef->status;
				pLineInfo->status.lineBerSd.time   = pLineDef->time;
        		}
            break;

        /* DCC failure */
        case cFmLineDccFail:
        	if (pLineInfo->status.dcc.status != pLineDef->status)
        		{
				pLineInfo->status.dcc.status = pLineDef->status;
				pLineInfo->status.dcc.time   = pLineDef->time;
        		}
            break;

        /* AIS-L defect */
        case cFmLineAis:
        	if (pLineInfo->status.aisl.status != pLineDef->status)
        		{
				pLineInfo->status.aisl.status = pLineDef->status;
				pLineInfo->status.aisl.time   = pLineDef->time;
        		}
            break;

        /* RDI-L defect */
        case cFmLineRdi:
        	if (pLineInfo->status.rdil.status != pLineDef->status)
        		{
				pLineInfo->status.rdil.status = pLineDef->status;
				pLineInfo->status.rdil.time   = pLineDef->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void LineAllDefUpd(tSurInfo         *pSurInfo,
                                          const tSurLineId *pLineId)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pLineId               - Line ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void LineAllDefUpd(tSurInfo         *pSurInfo,
                               const tSurLineId *pLineId)
    {
    bool          blNewStat;
    tSurLineDef   def;
    tSurLineInfo *pLineInfo;
    dword         dDefStat;

    /* Get line information */
    mSurLineInfo(pSurInfo, pLineId, pLineInfo);

    /* Create defect with default information */
    OsalMemCpy((void *)pLineId, &(def.id), sizeof(tSurLineId));
    def.time   = dCurTime;

    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurLine, pLineId, &dDefStat);

    /* Section LOS defect */
    blNewStat = mBitStat(dDefStat, cSurSecLosMask);
    if (blNewStat != pLineInfo->status.los.status)
        {
        def.defect = cFmSecLos;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* Section LOF defect */
    blNewStat = mBitStat(dDefStat, cSurSecLofMask);
    if (blNewStat != pLineInfo->status.lof.status)
        {
        def.defect = cFmSecLof;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* Section SEF defect */
    blNewStat = mBitStat(dDefStat, cSurSecSefMask);
    if (blNewStat != pLineInfo->status.sef.status)
        {
        def.defect = cFmSecSef;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* TIM-S defect */
    blNewStat = mBitStat(dDefStat, cSurSecTimsMask);
    if (blNewStat != pLineInfo->status.tims.status)
        {
        def.defect = cFmSecTim;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* AIS-L decfect */
    blNewStat = mBitStat(dDefStat, cSurLineAislMask);
    if (blNewStat != pLineInfo->status.aisl.status)
        {
        def.defect = cFmLineAis;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* DCC failure */
    blNewStat = mBitStat(dDefStat, cSurLineDccMask);
    if (blNewStat != pLineInfo->status.dcc.status)
        {
        def.defect = cFmLineDccFail;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* RDI-L defect */
    blNewStat = mBitStat(dDefStat, cSurLineRdilMask);
    if (blNewStat != pLineInfo->status.rdil.status)
        {
        def.defect = cFmLineRdi;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* BER-SF */
    blNewStat = mBitStat(dDefStat, cSurLineBerSfMask);
    if (blNewStat != pLineInfo->status.lineBerSf.status)
        {
        def.defect = cFmLineBerSf;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }

    /* BER-SD */
    blNewStat = mBitStat(dDefStat, cSurLineBerSdMask);
    if (blNewStat != pLineInfo->status.lineBerSd.status)
        {
        def.defect = cFmLineBerSd;
        def.status = blNewStat;
        SurLineDefUpd(pLineInfo, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void LineAllFailUpd(tSurInfo         *pSurInfo,
                                           const tSurLineId *pLineId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pLineId               - Line ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void LineAllFailUpd(tSurInfo         *pSurInfo,
                                const tSurLineId *pLineId)
    {
    tSurTrblProf   *pTrblProf;
    tSurLineFail   *pFailInd;
    tSortList       failHis;
    tSurLineInfo   *pLineInfo;
    tFmFailInfo     failInfo;
    bool            blFailChg;
    bool            blNotf;

    /* Get line information */
    mSurLineInfo(pSurInfo, pLineId, pLineInfo);

    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pLineInfo->failure);
    failHis   = pLineInfo->failHis;

    /* LOS failure */
    mFailIndUpd(pSurInfo, pLineInfo, los, los, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pLineInfo->msgInh.section == false) &&
                  (pLineInfo->msgInh.los     == false));
        mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->los, failHis, failInfo, blNotf, cFmSecLos, pTrblProf->los);

        /* Update failure count register */
        if (pLineInfo->failure.los.status == true)
            {
            pLineInfo->parm.fcL.curSec += 1;
            }
                
        /* Clear LOF failure if it was already declared */
        if ((pLineInfo->failure.los.status == true) &&
            (pLineInfo->failure.lof.status == true))
            {
            /* Clear failure indication */
            pLineInfo->failure.lof.status = false;
            pLineInfo->failure.lof.time   = dCurTime;

            /* Notify trouble */
            blNotf = ((pLineInfo->msgInh.section == false) &&
                      (pLineInfo->msgInh.lof     == false));
            mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->lof, failHis, failInfo, blNotf, cFmSecLof, pTrblProf->lof);
            }
        }

    /* LOF failure */
    /* Only declare/clear LOF failure when LOS failure is not present */
    if (pLineInfo->failure.los.status == false)
        {
        mFailIndUpd(pSurInfo, pLineInfo, lof, lof, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pLineInfo->msgInh.section == false) &&
                      (pLineInfo->msgInh.lof     == false));
            mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->lof, failHis, failInfo, blNotf, cFmSecLof, pTrblProf->lof);
            
            /* Update failure count register */
            if (pLineInfo->failure.lof.status == true)
                {
                pLineInfo->parm.fcL.curSec += 1;
                }
            }
        }
    
    /* Only declare TIM-S, AIS-L, RDI-L, BER-SF, BER-SD failure when LOS and LOF 
       defect are absent */
    if ((pLineInfo->failure.los.status == false) &&
        (pLineInfo->failure.lof.status == false))
        {
        /* TIM-S failure */
        mFailIndUpd(pSurInfo, pLineInfo, tims, tims, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pLineInfo->msgInh.section == false) &&
                      (pLineInfo->msgInh.tim     == false));
            mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->tims, failHis, failInfo, blNotf, cFmSecTim, pTrblProf->tims);
            }

        /* AIS-L failure */
        mFailIndUpd(pSurInfo, pLineInfo, aisl, aisl, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pLineInfo->msgInh.line == false) &&
                      (pLineInfo->msgInh.aisl == false));
            mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->aisl, failHis, failInfo, blNotf, cFmLineAis, pTrblProf->aisl);
            
            /* Update failure count register */
            if (pLineInfo->failure.aisl.status == true)
                {
                pLineInfo->parm.fcL.curSec += 1;
                }
            }
        
        /* RFI-L failure */
        mFailIndUpd(pSurInfo, pLineInfo, rdil, rfil, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pLineInfo->msgInh.line == false) &&
                      (pLineInfo->msgInh.rfil == false));
            mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->rfil, failHis, failInfo, blNotf, cFmLineRfi, pTrblProf->rfil);
            
            /* Update FC-LFE parameter */
            if (pLineInfo->failure.rfil.status == true)
                {
                pLineInfo->parm.fcLfe.curSec += 1;
                }
            }
            
        /* BER-SF failure */
        mFailIndUpd(pSurInfo, pLineInfo, lineBerSf, lineBerSf, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pLineInfo->msgInh.line  == false) &&
                      (pLineInfo->msgInh.berSf == false));
            mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->lineBerSf, failHis, failInfo, blNotf, cFmLineBerSf, pTrblProf->lineBerSf);

            /* Clear BER-SD failure if it was already declared */
            if ((pLineInfo->failure.lineBerSf.status == true) &&
                (pLineInfo->failure.lineBerSd.status == true))
                {
                /* Clear failure indication */
                pLineInfo->failure.lineBerSd.status = false;
                pLineInfo->failure.lineBerSd.time   = dCurTime;

                /* Notify trouble */
                blNotf = ((pLineInfo->msgInh.line  == false) &&
                          (pLineInfo->msgInh.berSd == false));
                mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->lineBerSd, failHis, failInfo, blNotf, cFmLineBerSd, pTrblProf->lineBerSd);
                }
            }

        /* BER-SD failure */
        /* Only declare/clear BER-SD failure when BER-SF failure is not present */
        if (pLineInfo->failure.lineBerSf.status == false)
            {
            mFailIndUpd(pSurInfo, pLineInfo, lineBerSd, lineBerSd, blFailChg);
            if (blFailChg == true)
                {
                blNotf = ((pLineInfo->msgInh.line  == false) &&
                          (pLineInfo->msgInh.berSd == false));
                mFailRept(pSurInfo, cSurLine, pLineId, pFailInd->lineBerSd, failHis, failInfo, blNotf, cFmLineBerSd, pTrblProf->lineBerSd);
                }
            }
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void LinePmParmUpd(tSurInfo         *pSurInfo,
                                          const tSurLineId *pLineId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pLineId               - Line ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void LinePmParmUpd(tSurInfo         *pSurInfo,
                               const tSurLineId *pLineId)
    {
    tSurLineInfo   *pLineInfo;
    word            wSecK;
    word            wLineK;
    bool            blNewStat;
    tSurLineAnomaly anomaly;
    tPmTcaInfo      tcaInfo;
    bool            blNotf;
    tSurTime		currentTime;
    bool			isSecExpr;
    bool			isPerExpr;
    bool			isDayExpr;

    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    /* Get line information */
    mSurLineInfo(pSurInfo, pLineId, pLineInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pLineInfo->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pLineInfo->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pLineInfo->parm.curEngTime.minute)
        {
        pLineInfo->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pLineInfo->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pLineInfo->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurLineAnomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurLine, pLineId, &anomaly);
        pLineInfo->parm.cvS.curSec   += anomaly.b1;
        pLineInfo->parm.cvL.curSec   += anomaly.b2;
        pLineInfo->parm.cvLfe.curSec += anomaly.reil;
        }

    /* Update current second registers when SEF defect occurred */
    if (pLineInfo->status.sef.status == true)
        {
        pLineInfo->parm.sefsS.curSec = 1;
        pLineInfo->parm.esS.curSec   = 1;
        pLineInfo->parm.sesS.curSec  = 1;
        }
    
    /* Update current second registers when LOS/LOF/SEF defect occurred */
    if ((pLineInfo->status.lof.status == true) ||
        (pLineInfo->status.los.status == true))
        {
        pLineInfo->parm.esS.curSec  = 1;
        pLineInfo->parm.sesS.curSec = 1;
        pLineInfo->parm.esL.curSec  = 1;
        pLineInfo->parm.sesL.curSec = 1;
        }
    
    /* Update current second registers when AIS-L defect occurred */
    if (pLineInfo->status.aisl.status == true)
        {
        pLineInfo->parm.esL.curSec  = 1;
        pLineInfo->parm.sesL.curSec = 1;
        }
    
    /* Update current second registers when RDI-L defect occurred */
    if (pLineInfo->status.rdil.status == true)
        {
        pLineInfo->parm.esLfe.curSec  = 1;
        pLineInfo->parm.sesLfe.curSec = 1;
        }
    
    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update ES-S */
        if (pLineInfo->parm.cvS.curSec != 0)
            {
            pLineInfo->parm.esS.curSec = 1;
            }

        /* Update ES-L */
        if (pLineInfo->parm.cvL.curSec != 0)
            {
            pLineInfo->parm.esL.curSec = 1;
            }

        /* Update ES-LFE */
        if (pLineInfo->parm.cvLfe.curSec != 0)
            {
            pLineInfo->parm.esLfe.curSec = 1;
            }

        /* Update SES parameters */
        wSecK  = 0;
        wLineK = 0;
        mLineKval(pSurInfo, pLineInfo->conf.rate, wSecK, wLineK, blNewStat);
        if (blNewStat == false)
            {
            mSurDebug(cSurWarnMsg, "Not support line rate\n");
            }
        else
            {
            /* Section */
            if (pLineInfo->parm.cvS.curSec >= wSecK)
                {
                pLineInfo->parm.sesS.curSec = 1;
                }

            /* Line Near-end */
            if (pLineInfo->parm.cvL.curSec >= wLineK)
                {
                pLineInfo->parm.sesL.curSec = 1;
                }

            /* Line Far-end */
            if (pLineInfo->parm.cvLfe.curSec >= wLineK)
                {
                pLineInfo->parm.sesLfe.curSec = 1;
                }
            }
        
        /* Update UAS parameter */
        if (pLineInfo->status.neUat == true)
            {
            pLineInfo->parm.uasL.curSec = 1;
            }
        if (pLineInfo->status.feUat == true)
            {
            pLineInfo->parm.uasLfe.curSec = 1;
            }

        /* Check if Line Near-End UAS just enter or exit and adjust registers */
        blNewStat = (pLineInfo->parm.sesL.curSec == 1) ? true : false;
        if (blNewStat != pLineInfo->status.neUatPot)
            {
            pLineInfo->status.nePotCnt = 1;
            }
        else
            {
            pLineInfo->status.nePotCnt++;
            }
            
        /* Declare potential for entering or exiting UAT if the condition is met */
        if (pLineInfo->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pLineInfo->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pLineInfo->parm.esL));
                mCurPerNegAdj(&(pLineInfo->parm.sesL));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pLineInfo->parm.esL));
                    mPrePerNegAdj(&(pLineInfo->parm.sesL));
                    }

                /* Update UAS */
            	pLineInfo->parm.uasL.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pLineInfo->parm.cvL));
                mCurPerPosAdj(&(pLineInfo->parm.esL));
                mCurPerPosAdj(&(pLineInfo->parm.sesL));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pLineInfo->parm.cvL));
                    mPrePerPosAdj(&(pLineInfo->parm.esL));
                    mPrePerPosAdj(&(pLineInfo->parm.sesL));
                    }

                /* Update UAS */
            	if (pLineInfo->parm.uasL.curPer.value >= 10)
            		{
            		pLineInfo->parm.uasL.curPer.value -= 10;
            		}
            	else
            		{
            		pLineInfo->parm.uasL.curPer.value = 0;
            		}
            	pLineInfo->parm.uasL.curSec = 0;
                }
            }

        /* Check if Line Far-End UAS just enter or exit and adjust registers */
        blNewStat = (pLineInfo->parm.sesLfe.curSec == 1) ? true : false;
        if (blNewStat != pLineInfo->status.feUatPot)
            {
            pLineInfo->status.fePotCnt = 1;
            }
        else
            {
            pLineInfo->status.fePotCnt++;
            }
            
        /* Declare potential for entering or exiting UAT if the condition is met */
        if (pLineInfo->status.fePotCnt == 10)
            {
            /* Update unavailable time status */
            pLineInfo->status.feUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pLineInfo->parm.esLfe));
                mCurPerNegAdj(&(pLineInfo->parm.sesLfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pLineInfo->parm.esLfe));
                    mPrePerNegAdj(&(pLineInfo->parm.sesLfe));
                    }

                /* Update UAS */
            	pLineInfo->parm.uasLfe.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pLineInfo->parm.cvLfe));
                mCurPerPosAdj(&(pLineInfo->parm.esLfe));
                mCurPerPosAdj(&(pLineInfo->parm.sesLfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pLineInfo->parm.cvLfe));
                    mPrePerPosAdj(&(pLineInfo->parm.esLfe));
                    mPrePerPosAdj(&(pLineInfo->parm.sesLfe));
                    }

                /* Update UAS */
            	if (pLineInfo->parm.uasLfe.curPer.value >= 10)
            		{
            		pLineInfo->parm.uasLfe.curPer.value -= 10;
            		}
            	else
            		{
            		pLineInfo->parm.uasLfe.curPer.value = 0;
            		}
            	pLineInfo->parm.uasLfe.curSec = 0;
                }
            }
        
        /* Accumulate inhibition */
        pLineInfo->parm.cvL.inhibit    = false;
        pLineInfo->parm.esL.inhibit    = false;
        pLineInfo->parm.sesL.inhibit   = false;
        pLineInfo->parm.cvLfe.inhibit  = false;
        pLineInfo->parm.esLfe.inhibit  = false;
        pLineInfo->parm.sesLfe.inhibit = false;
        pLineInfo->parm.uasLfe.inhibit = false;
        pLineInfo->parm.fcLfe.inhibit  = false;

        /* Inhibit when UAS */
        if (pLineInfo->status.neUat == true)
            {
            pLineInfo->parm.cvL.inhibit  = true;
            pLineInfo->parm.esL.inhibit  = true;
            pLineInfo->parm.sesL.inhibit = true;
            }
        if (pLineInfo->status.feUat == true)
            {
            pLineInfo->parm.cvLfe.inhibit  = true;
            pLineInfo->parm.esLfe.inhibit  = true;
            pLineInfo->parm.sesLfe.inhibit = true;
            }

        /* Inhibit when SES */
        pLineInfo->parm.cvS.inhibit = (pLineInfo->parm.sesS.curSec == 1) ? true : false;
        if (pLineInfo->parm.sesL.curSec == 1)
            {
            pLineInfo->parm.cvL.inhibit = true;
            }
        if (pLineInfo->parm.sesLfe.curSec == 1)
            {
            pLineInfo->parm.cvLfe.inhibit = true;
            }

        /* Inhibit Line Far-End parameters when AIS-L or lower-level defects
           occurred */
        if ((pLineInfo->status.los.status  == true) ||
            (pLineInfo->status.lof.status  == true) ||
            (pLineInfo->status.tims.status == true) ||
            (pLineInfo->status.aisl.status == true))
            {
            pLineInfo->parm.cvLfe.inhibit  = true;
            pLineInfo->parm.esLfe.inhibit  = true;
            pLineInfo->parm.sesLfe.inhibit = true;
            pLineInfo->parm.uasLfe.inhibit = true;
            pLineInfo->parm.fcLfe.inhibit  = true;

            pLineInfo->parm.cvLfe.curPer.invalid  = true;
            pLineInfo->parm.esLfe.curPer.invalid  = true;
            pLineInfo->parm.sesLfe.curPer.invalid = true;
            pLineInfo->parm.uasLfe.curPer.invalid = true;
            pLineInfo->parm.fcLfe.curPer.invalid  = true;
            }

        /* Increase Line Near-End adjustment registers */
        /* In available time */
        if (pLineInfo->status.neUat == false)
            {
            /* Potential for entering unavailable time */
            if (pLineInfo->parm.sesL.curSec == 1)
                {
                mNegInc(&(pLineInfo->parm.esL));
                mNegInc(&(pLineInfo->parm.sesL));
                }
            }
        
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pLineInfo->parm.sesL.curSec == 0)
                {
                mPosInc(&(pLineInfo->parm.cvL));
                mPosInc(&(pLineInfo->parm.esL));
                mPosInc(&(pLineInfo->parm.sesL));
                }
            }

        /* Increase Line Far-End adjustment registers */
        if (pLineInfo->status.neUat == false)
            {
            /* Unavailable time */
            if (pLineInfo->status.feUat == false)
                {
                /* Potential for entering unavailable time */
                if (pLineInfo->parm.sesLfe.curSec == 1)
                    {
                    mNegInc(&(pLineInfo->parm.esLfe));
                    mNegInc(&(pLineInfo->parm.sesLfe));
                    }
                }
                
            /* In unavailable time */
            else
                {
                /* Potential for exiting unavailable time */
                if (pLineInfo->parm.sesLfe.curSec == 0)
                    {
                    mPosInc(&(pLineInfo->parm.cvLfe));
                    mPosInc(&(pLineInfo->parm.esLfe));
                    mPosInc(&(pLineInfo->parm.sesLfe));
                    }
                }
            }
            
        /* Clear Line Near-End adjustment registers */
        if (pLineInfo->status.neUat == false)
            {
            if ((pLineInfo->parm.sesL.curSec == 0) &&
                (pLineInfo->status.neUatPot  == true))
                {
                mNegClr(&(pLineInfo->parm.esL));
                mNegClr(&(pLineInfo->parm.sesL));
                }
            }
        else if ((pLineInfo->parm.sesL.curSec == 1) &&
                 (pLineInfo->status.neUatPot  == false))
            {
            mPosClr(&(pLineInfo->parm.cvL));
            mPosClr(&(pLineInfo->parm.esL));
            mPosClr(&(pLineInfo->parm.sesL));
            }

        /* Clear Line Far-End adjustment registers */
        if (pLineInfo->status.feUat == false)
            {
            if ((pLineInfo->parm.sesLfe.curSec == 0) &&
                (pLineInfo->status.feUatPot    == true))
                {
                mNegClr(&(pLineInfo->parm.esLfe));
                mNegClr(&(pLineInfo->parm.sesLfe));
                }
            }
        else if ((pLineInfo->parm.sesLfe.curSec == 1) &&
                 (pLineInfo->status.feUatPot    == false))
            {
            mPosClr(&(pLineInfo->parm.cvLfe));
            mPosClr(&(pLineInfo->parm.esLfe));
            mPosClr(&(pLineInfo->parm.sesLfe));
            }

        /* Store current second statuses */
        pLineInfo->status.neUatPot = (pLineInfo->parm.sesL.curSec   == 1) ? true : false;
        pLineInfo->status.feUatPot = (pLineInfo->parm.sesLfe.curSec == 1) ? true : false;

        /* Accumulate current period registers */
        mCurPerAcc(&(pLineInfo->parm.sefsS));
        mCurPerAcc(&(pLineInfo->parm.cvS));
        mCurPerAcc(&(pLineInfo->parm.esS));
        mCurPerAcc(&(pLineInfo->parm.sesS));
        mCurPerAcc(&(pLineInfo->parm.cvL));
        mCurPerAcc(&(pLineInfo->parm.esL));
        mCurPerAcc(&(pLineInfo->parm.sesL));
        mCurPerAcc(&(pLineInfo->parm.uasL));
        mCurPerAcc(&(pLineInfo->parm.fcL));
        mCurPerAcc(&(pLineInfo->parm.psc));
        mCurPerAcc(&(pLineInfo->parm.psd));
        mCurPerAcc(&(pLineInfo->parm.cvLfe));
        mCurPerAcc(&(pLineInfo->parm.esLfe));
        mCurPerAcc(&(pLineInfo->parm.sesLfe));
        mCurPerAcc(&(pLineInfo->parm.uasLfe));
        mCurPerAcc(&(pLineInfo->parm.fcLfe));

        /* Check and report TCA */
        blNotf = ((pLineInfo->msgInh.section   == false) &&
                  (pLineInfo->msgInh.tca       == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Report TCAs of Section parameters */
        SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmSecSefs, &(pLineInfo->parm.sefsS), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmSecCv,   &(pLineInfo->parm.cvS),  blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmSecEs,   &(pLineInfo->parm.esS),  blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmSecSes,  &(pLineInfo->parm.sesS), blNotf, &tcaInfo);
        
        /* Report TCAs of Line Near-End parameters */
        if ((pLineInfo->status.neUat    == false) &&
            (pLineInfo->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineCv,  &(pLineInfo->parm.cvL),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineEs,  &(pLineInfo->parm.esL),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineSes, &(pLineInfo->parm.sesL),   blNotf, &tcaInfo);
            }
        
        /* Report TCAs of Line Far-End parameters */
        if ((pLineInfo->status.feUat    == false) &&
            (pLineInfo->status.feUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineCvLfe,  &(pLineInfo->parm.cvLfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineEsLfe,  &(pLineInfo->parm.esLfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineSesLfe, &(pLineInfo->parm.sesLfe), blNotf, &tcaInfo);
            }
        
        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineUas,    &(pLineInfo->parm.uasL),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurLine, pLineId, pLineInfo, cPmLineUasLfe, &(pLineInfo->parm.uasLfe), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pLineInfo->parm.sefsS));
        mStackDown(&(pLineInfo->parm.cvS));
        mStackDown(&(pLineInfo->parm.esS));
        mStackDown(&(pLineInfo->parm.sesS));
        mStackDown(&(pLineInfo->parm.cvL));
        mStackDown(&(pLineInfo->parm.esL));
        mStackDown(&(pLineInfo->parm.sesL));
        mStackDown(&(pLineInfo->parm.uasL));
        mStackDown(&(pLineInfo->parm.fcL));
        mStackDown(&(pLineInfo->parm.psc));
        mStackDown(&(pLineInfo->parm.psd));
        mStackDown(&(pLineInfo->parm.cvLfe));
        mStackDown(&(pLineInfo->parm.esLfe));
        mStackDown(&(pLineInfo->parm.sesLfe));
        mStackDown(&(pLineInfo->parm.uasLfe));
        mStackDown(&(pLineInfo->parm.fcLfe));

        /* Update time for period */
        pLineInfo->parm.prePerStartTime = pLineInfo->parm.curPerStartTime;
        pLineInfo->parm.prePerEndTime = dCurTime;
        pLineInfo->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pLineInfo->parm.sefsS));
        mDayShift(&(pLineInfo->parm.cvS));
        mDayShift(&(pLineInfo->parm.esS));
        mDayShift(&(pLineInfo->parm.sesS));
        mDayShift(&(pLineInfo->parm.cvL));
        mDayShift(&(pLineInfo->parm.esL));
        mDayShift(&(pLineInfo->parm.sesL));
        mDayShift(&(pLineInfo->parm.uasL));
        mDayShift(&(pLineInfo->parm.fcL));
        mDayShift(&(pLineInfo->parm.psc));
        mDayShift(&(pLineInfo->parm.psd));
        mDayShift(&(pLineInfo->parm.cvLfe));
        mDayShift(&(pLineInfo->parm.esLfe));
        mDayShift(&(pLineInfo->parm.sesLfe));
        mDayShift(&(pLineInfo->parm.uasLfe));
        mDayShift(&(pLineInfo->parm.fcLfe));

        /* Update time for day */
        pLineInfo->parm.preDayStartTime = pLineInfo->parm.curDayStartTime;
        pLineInfo->parm.preDayEndTime = dCurTime;
        pLineInfo->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurLineStatUpd(tSurDev           device,
                                          const tSurLineId *pLineId)

Purpose      : This function is used to update all defect statuses and anomaly
               counters of input line.

Inputs       : device                - device
               pLineId               - line ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurLineStatUpd(tSurDev device, const tSurLineId *pLineId)
    {
    tSurInfo       *pSurInfo;

    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) &&
        (pSurInfo->interface.DefStatGet    != null))
        {
        LineAllDefUpd(pSurInfo, pLineId);
        }
    
    /* Declare/terminate failures */
    LineAllFailUpd(pSurInfo, pLineId);
    
    /* Update performance registers */
    LinePmParmUpd(pSurInfo, pLineId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DATA STRUCTURE
 *
 * File        : linkqueue.c
 *
 * Created Date: 04-Jan-09
 *
 * Description : This file contains source code of linked queue
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include "linkqueue.h"
#include "suradapt.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
/* Message type */
typedef enum eMsgType
    {
    cErrMsg  = 31,  /* Error message */
    cInfoMsg = 32,  /* Information message */
    cWarnMsg = 33   /* Warning message */
    }eMsgType;
    
/* Queue node */
typedef struct tNode
    {
    void         *pData;  /* Data */
    struct tNode *pNext;  /* Points to next node */
    }tNode;

/* Queue */
typedef struct _tLinkQueue
    {
    osalSemKey  sem;    /* Semaphore */
    tNode      *pFront; /* Front node */
    tNode      *pRear;  /* Rear node */
    dword       size;   /* Size of queue */
    }_tLinkQueue;

/*--------------------------- Macros -----------------------------------------*/
/* Node create and destroy */
#define mNodeCreate(pNewNode, pData, size)                                     \
    (pNewNode)        = OsalMemAlloc(sizeof(tNode));                           \
    (pNewNode)->pNext = null;                                                  \
    if ((pNewNode) != null)                                                    \
        {                                                                      \
        (pNewNode)->pData = OsalMemAlloc(size);                                \
        if (((pNewNode)->pData) == null)                                       \
            {                                                                  \
            OsalMemFree(pNewNode);                                             \
            (pNewNode) = null;                                                 \
            }                                                                  \
        else                                                                   \
            {                                                                  \
            OsalMemCpy(pData, (pNewNode)->pData, size);                        \
            }                                                                  \
        }
    
#define mNodeDestroy(pNode)                                                    \
    OsalMemFree((pNode)->pData);                                               \
    OsalMemFree(pNode);                                                        \
    (pNode) = null;                                                            \

/* Take/give semaphore of list */
#define mQueueTake(pQueue, ret)                                                \
    (ret) = ((OsalSemTake(&((pQueue)->sem)) != cOsalOk) ? cQueErrSem : cQueSucc)
#define mQueueGive(pQueue, ret)                                                \
    (ret) = ((OsalSemGive(&((pQueue)->sem)) != cOsalOk) ? cQueErrSem : cQueSucc)





/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
void mErrLog(uint32 msgType, const char *format, ...);
/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueCreate(tLinkQueue *pQueue)

Purpose      : This function is used to create a linked queue. This function 
               must be called before using any queue functions.

Inputs       : None

Outputs      : pQueue                - Linked queue

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueCreate(tLinkQueue *pQueue)
    {
    _tLinkQueue *_pQueue;
    
    /* Allocate memory */
    _pQueue = OsalMemAlloc(sizeof(_tLinkQueue));
    if (_pQueue == null)
        {
        return cQueErr;
        }
    
    /* Initialize */
    OsalSemInit(&(_pQueue->sem), cAtFalse, 1);
    _pQueue->pFront = null;
    _pQueue->pRear  = null;
    _pQueue->size   = 0;
    
    /* Return */
    *pQueue = _pQueue;
    
    return cQueSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueDestroy(tLinkQueue *pQueue)

Purpose      : This function is used to destroy a linked queue. This function 
               must be called to make sure that all allocated memories are 
               deallocated.

Inputs       : pQueue                - Linked queue

Outputs      : 

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueDestroy(tLinkQueue *pQueue)
    {
    eQueRet        ret;
    _tLinkQueue  *_pQueue;
    
    /* Check pointer */
    if (pQueue == null)
        {
        return cQueErr;
        }
        
    /* Get handle */
    _pQueue = (_tLinkQueue*)(*pQueue);
    
    /* Flush all data */
    ret = LQueFlush(*pQueue);
    if (ret != cQueSucc)
        {
        mErrLog(cErrMsg, "Cannot flush all data\n");
        return ret;
        }
    
    /* Free semaphore */
    if (OsalSemDestroy(&(_pQueue->sem)) != cOsalOk)
        {
        mErrLog(cWarnMsg, "Cannot destroy semaphore\n");
        }
    
    /* Deallocate queue */
    OsalMemFree(*pQueue);
    *pQueue = null;
    
    return cQueSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueRmv(tLinkQueue queue, void *pData, dword size)

Purpose      : This function is used to remove the first element and return the removed element

Inputs       : queue                 - Linked queue
               size                  - number of bytes that output data can hold

Outputs      : pData                 - Returned data

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueRmv(tLinkQueue queue, void *pData, dword size)
    {
    eQueRet      ret;
    _tLinkQueue *pQueue;
    tNode       *pNext;
    
    /* Check parameters */
    if ((queue == null) || (pData == null) || (size == 0))
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cQueErr;
        }
    
    /* Get handle */
    pQueue = (_tLinkQueue *)queue;
    
    /* Take semaphore */
    mQueueTake(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Check queue size */
    if (pQueue->size == 0)
        {
        /* Give semaphore */
        mQueueGive(pQueue, ret);
        if (ret != cQueSucc)
            {
            mErrLog(cWarnMsg, "Cannot give semaphore\n");
            }
        
        return cQueEmpty;
        }
    
    /* Queue has one element */
    if (pQueue->size == 1)
        {
        OsalMemCpy(pQueue->pFront->pData, pData, size);
        mNodeDestroy(pQueue->pFront);
        pQueue->pFront = null;
        pQueue->pRear  = null;
        }
    
    /* Queue has more than one elements */
    else
        {
        /* Copy data and free the first element */
        OsalMemCpy(pQueue->pFront->pData, pData, size);
        pNext = pQueue->pFront->pNext;
        mNodeDestroy(pQueue->pFront);
        pQueue->pFront = pNext;
        }
    
    /* Decrease queue size and give semaphore */
    pQueue->size = pQueue->size - 1;
    mQueueGive(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cQueSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueIns(tLinkQueue queue, 
                                     const void *pData, 
                                     dword       size)

Purpose      : This function is used to insert a new element to queue

Inputs       : queue                 - Linked queue
               pData                 - data
               size                  - data size

Outputs      : None

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueIns(tLinkQueue queue, void *pData, dword size)
    {
    eQueRet      ret;
    _tLinkQueue *pQueue;
    tNode       *pNewNode;
    
    /* Check parameters */
    if ((queue == null) || (pData == null) || (size == 0))
        {
        mErrLog(cErrMsg, "Invalid parameters\n");
        return cQueErr;
        }
    
    /* Get handle */
    pQueue = (_tLinkQueue *)queue;
    
    /* Take semaphore */
    mQueueTake(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cErrMsg, "Can not take semaphore\n");
        return ret;
        }
    
    /* Create new node and copy data */
    mNodeCreate(pNewNode, pData, size);
    if (pNewNode == null)
        {
        mErrLog(cErrMsg, "Cannot allocate memory\n");
        
        /* Give semaphore */
        mQueueGive(pQueue, ret);
        if (ret != cQueSucc)
            {
            mErrLog(cWarnMsg, "Cannot give semaphore\n");
            }
        
        return cQueErr;
        }
    
    /* Add new node to queue */
    if (pQueue->size == 0)
        {
        pQueue->pFront = pNewNode;
        pQueue->pRear  = pNewNode;
        }
    else
        {
        pQueue->pRear->pNext = pNewNode;
        pQueue->pRear        = pNewNode;
        }
    
    /* Increase queue size, give semaphore and return */
    pQueue->size = pQueue->size + 1;
    mQueueGive(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
        
    return cQueSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueFront(tLinkQueue queue, 
                                       void       *pData, 
                                       dword       size)

Purpose      : This function is used to get (not remove) the first element of 
               queue

Inputs       : queue                 - Linked queue
               size                  - data size

Outputs      : pData                 - data

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueFront(tLinkQueue queue, void *pData, dword size)
    {
    eQueRet      ret;
    _tLinkQueue *pQueue;
    
    /* Check parameter */
    if ((queue == null) || (pData == null) || (size == 0))
        {
        mErrLog(cErrMsg, "Invalid parameters\n");
        return cQueErr;
        }
    
    /* Get handle and take semaphore */
    pQueue = (_tLinkQueue *)queue;
    mQueueTake(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Check if queue is empty */
    if (pQueue->size == 0)
        {
        mErrLog(cErrMsg, "Queue is empty\n");
        
        /* Give semaphore */
        mQueueGive(pQueue, ret);
        if (ret != cQueSucc)
            {
            mErrLog(cWarnMsg, "Cannot give semaphore\n");
            }

        return cQueEmpty;
        }
    
    /* Return data of the first element */
    OsalMemCpy(pQueue->pFront->pData, pData, size);
    
    /* Give semaphore */
    mQueueGive(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cQueSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueSize(tLinkQueue queue, dword *pSize)

Purpose      : This function is used to get size of queue

Inputs       : queue                 - Linked queue
               
Outputs      : pSize                 - Size of queue

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueSize(tLinkQueue queue, dword *pSize)
    {
    eQueRet     ret;
    _tLinkQueue *pQueue;
    
    /* Check parameters */
    if ((queue == null) || (pSize == null))
        {
        mErrLog(cErrMsg, "Parameters are wrong\n");
        return cQueErr;
        }
    
    /* Get handle */
    pQueue = (_tLinkQueue *)queue;
    mQueueTake(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cErrMsg, "Cannot take semaphore\n");
        return ret;
        }
    
    /* Return queue size */
    *pSize = pQueue->size;
    
    /* Give semaphore and return */
    mQueueGive(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cWarnMsg, "Cannot take semaphore\n");
        }
    return cQueSucc;
    }

/*------------------------------------------------------------------------------
Prototype    : eQueRet LQueFlush(tLinkQueue queue)

Purpose      : This function is used to flush (remove all data) queue

Inputs       : queue                 - Linked queue
               
Outputs      : None

Return       : Queue return code
------------------------------------------------------------------------------*/
eQueRet LQueFlush(tLinkQueue queue)
    {
    eQueRet      ret;
    _tLinkQueue *pQueue;
    tNode       *pNext;
    
    /* Check parameters */
    if (queue == null)
        {
        mErrLog(cErrMsg, "Invalid parameter\n");
        return cQueErr;
        }
        
    /* Get handle */
    pQueue = (_tLinkQueue*)queue;
    
    /* Take semaphore */
    mQueueTake(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cErrMsg, "Can not take semaphore\n");
        return ret;
        }
    
    /* Remove all elements */
    if (pQueue->size > 0)
        {
        do
            {
            /* Pointer to the next and free the front, after that update front */
            pNext = pQueue->pFront->pNext;
            mNodeDestroy(pQueue->pFront);
            pQueue->pFront = pNext;
            }
        while (pNext != null);
        }
    
    /* Reset front and rear pointer */
    pQueue->pFront = null;
    pQueue->pRear  = null;
    pQueue->size   = 0;
    
    /* Give semaphore */
    mQueueGive(pQueue, ret);
    if (ret != cQueSucc)
        {
        mErrLog(cWarnMsg, "Cannot give semaphore\n");
        }
    
    return cQueSucc;
    }

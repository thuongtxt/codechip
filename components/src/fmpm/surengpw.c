/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengpw.c
 *
 * Created Date: 22-Feb-2010
 *
 * Description : This file contain all source code PW Transmisstion
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ---------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/


/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*------------------------------------------------------------------------------
Prototype    : friend void SurPwDefUpd(tSurPwInfo *pPwInfo, tSurPwDef *pPwDef)

Purpose      : This function is used to update PW defect database

Inputs       : pPwDef               - PW defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurPwDefUpd(tSurPwInfo *pPwInfo, tSurPwDef *pPwDef)
    {
    switch (pPwDef->defect)
        {
        /* PW Stray packet */
        case cFmPwStrayPkt:
        	if (pPwInfo->status.strayPkt.status != pPwDef->status)
        		{
				pPwInfo->status.strayPkt.status = pPwDef->status;
				pPwInfo->status.strayPkt.time   = pPwDef->time;
        		}
            break;
            
        /* PW Malformed Packet */
        case cFmPwMalformedPkt:
        	if (pPwInfo->status.malformedPkt.status != pPwDef->status)
        		{
				pPwInfo->status.malformedPkt.status = pPwDef->status;
				pPwInfo->status.malformedPkt.time   = pPwDef->time;
        		}
            break;

        /* PW Excessive packet loss rate */
        case cFmPwExcPktLossRate:
        	if (pPwInfo->status.excPktLossRate.status != pPwDef->status)
        		{
				pPwInfo->status.excPktLossRate.status = pPwDef->status;
				pPwInfo->status.excPktLossRate.time   = pPwDef->time;
        		}
            break;
    
        /* PW Jitter Buffer overrun */
        case cFmPwJitBufOverrun:
        	if (pPwInfo->status.jitBufOverrun.status != pPwDef->status)
        		{
				pPwInfo->status.jitBufOverrun.status = pPwDef->status;
				pPwInfo->status.jitBufOverrun.time   = pPwDef->time;
        		}
            break;

        /* PW Remote packet loss */
        case cFmPwRemotePktLoss:
        	if (pPwInfo->status.remotePktLoss.status != pPwDef->status)
        		{
				pPwInfo->status.remotePktLoss.status = pPwDef->status;
				pPwInfo->status.remotePktLoss.time   = pPwDef->time;
        		}
            break;

        /* PW LOFS */
        case cFmPwLofs:
        	if (pPwInfo->status.lofs.status != pPwDef->status)
        		{
				pPwInfo->status.lofs.status = pPwDef->status;
				pPwInfo->status.lofs.time   = pPwDef->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void PwAllDefUpd(tSurInfo        *pSurInfo,
										 const tSurPwId *pPwId)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo         - Device database
               pPwId            - PW ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
private void PwAllDefUpd(tSurInfo        	  *pSurInfo,
						  const tSurPwId *pPwId)
    {
    bool          blNewStat;
    tSurPwDef    def;
    tSurPwInfo  *pPwInfo;
    dword         dDefStat;

    pPwInfo = NULL;
    /* Get PW information */
    mSurPwInfo(pSurInfo, pPwId, pPwInfo);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pPwId, &(def.id), sizeof(tSurPwId));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurPw, pPwId, &dDefStat);
    
    /* PW Stray packet */
    blNewStat = mBitStat(dDefStat, cSurPwStrayPktMask);
    if (blNewStat != pPwInfo->status.strayPkt.status)
        {
        def.defect = cFmPwStrayPkt;
        def.status = blNewStat;
        SurPwDefUpd(pPwInfo, &def);
        }

    /* PW Malformed Packet */
    blNewStat = mBitStat(dDefStat, cSurPwMalformedPktMask);
    if (blNewStat != pPwInfo->status.malformedPkt.status)
        {
        def.defect = cFmPwMalformedPkt;
        def.status = blNewStat;
        SurPwDefUpd(pPwInfo, &def);
        }

    /* PW Excessive packet loss rate */
    blNewStat = mBitStat(dDefStat, cSurPwExcPktLossRateMask);
    if (blNewStat != pPwInfo->status.excPktLossRate.status)
        {
        def.defect = cFmPwExcPktLossRate;
        def.status = blNewStat;
        SurPwDefUpd(pPwInfo, &def);
        }

    /* PW Jitter Buffer overrun */
    blNewStat = mBitStat(dDefStat, cSurPwJitBufOverrunMask);
    if (blNewStat != pPwInfo->status.jitBufOverrun.status)
        {
        def.defect = cFmPwJitBufOverrun;
        def.status = blNewStat;
        SurPwDefUpd(pPwInfo, &def);
        }

    /* PW Remote packet loss */
    blNewStat = mBitStat(dDefStat, cSurPwRemotePktLossMask);
    if (blNewStat != pPwInfo->status.remotePktLoss.status)
        {
        def.defect = cFmPwRemotePktLoss;
        def.status = blNewStat;
        SurPwDefUpd(pPwInfo, &def);
        }

    /* PW LOFS */
    blNewStat = mBitStat(dDefStat, cSurPwLofsMask);
    if (blNewStat != pPwInfo->status.lofs.status)
        {
        def.defect = cFmPwLofs;
        def.status = blNewStat;
        SurPwDefUpd(pPwInfo, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void PwAllFailUpd(tSurInfo        *pSurInfo,
											  const tSurPwId *pPwId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo         - Device database
               pPwId            - PW ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void PwAllFailUpd(tSurInfo        		*pSurInfo,
								   const tSurPwId 	*pPwId)
    {
    tSurTrblProf  *pTrblProf;
    tSurPwFail    *pFailInd;
    tSortList      failHis;
    tSurPwInfo    *pPwInfo;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;

    pPwInfo = NULL;
    blNotf		 = true;

    /* Get PW information */
    mSurPwInfo(pSurInfo, pPwId, pPwInfo);
    
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pPwInfo->failure);
    failHis   = pPwInfo->failHis;
    
    /* PW Stray packet */
	mFailIndUpd(pSurInfo, pPwInfo, strayPkt, strayPkt, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pPwInfo->msgInh.pw == false) &&
				  (pPwInfo->msgInh.strayPkt == false));
		mFailRept(pSurInfo, cSurPw, pPwId, pFailInd->strayPkt, failHis, failInfo, blNotf, cFmPwStrayPkt, pTrblProf->pwStrayPkt);
		}

    /* PW Malformed Packet */
	mFailIndUpd(pSurInfo, pPwInfo, malformedPkt, malformedPkt, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pPwInfo->msgInh.pw == false) &&
				  (pPwInfo->msgInh.malformedPkt == false));
		mFailRept(pSurInfo, cSurPw, pPwId, pFailInd->malformedPkt, failHis, failInfo, blNotf, cFmPwMalformedPkt, pTrblProf->pwMalformedPkt);
		}

    /* PW Excessive packet loss rate */
	mFailIndUpd(pSurInfo, pPwInfo, excPktLossRate, excPktLossRate, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pPwInfo->msgInh.pw == false) &&
				  (pPwInfo->msgInh.excPktLossRate == false));
		mFailRept(pSurInfo, cSurPw, pPwId, pFailInd->excPktLossRate, failHis, failInfo, blNotf, cFmPwExcPktLossRate, pTrblProf->pwExcPktLossRate);
		}

    /* PW Jitter Buffer overrun */
	mFailIndUpd(pSurInfo, pPwInfo, jitBufOverrun, jitBufOverrun, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pPwInfo->msgInh.pw == false) &&
				  (pPwInfo->msgInh.jitBufOverrun == false));
		mFailRept(pSurInfo, cSurPw, pPwId, pFailInd->jitBufOverrun, failHis, failInfo, blNotf, cFmPwJitBufOverrun, pTrblProf->pwJitBufOverrun);
		}

    /* PW Remote packet loss */
	mFailIndUpd(pSurInfo, pPwInfo, remotePktLoss, remotePktLoss, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pPwInfo->msgInh.pw == false) &&
				  (pPwInfo->msgInh.remotePktLoss == false));
		mFailRept(pSurInfo, cSurPw, pPwId, pFailInd->remotePktLoss, failHis, failInfo, blNotf, cFmPwRemotePktLoss, pTrblProf->pwRemotePktLoss);
		}

    /* PW LOFS */
	mFailIndUpd(pSurInfo, pPwInfo, lofs, lofs, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pPwInfo->msgInh.pw == false) &&
				  (pPwInfo->msgInh.lofs == false));
		mFailRept(pSurInfo, cSurPw, pPwId, pFailInd->lofs, failHis, failInfo, blNotf, cFmPwLofs, pTrblProf->pwLofs);
		}
    }

/*------------------------------------------------------------------------------
Prototype    : private void PwPmParmUpd(tSurInfo        	 *pSurInfo,
											 const tSurPwId *pPwId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pPwId            - PW ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void PwPmParmUpd(tSurInfo        	  *pSurInfo,
								  const tSurPwId *pPwId)
    {
    tSurPwInfo   *pPwInfo;
    bool           blNewStat;
    tSurPwAnomaly anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pPwInfo = NULL;
    /* Get PW information */
    mSurPwInfo(pSurInfo, pPwId, pPwInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pPwInfo->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pPwInfo->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pPwInfo->parm.curEngTime.minute)
        {
        pPwInfo->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pPwInfo->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pPwInfo->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Update ES, SES parameters based on defect */
    if ((pPwInfo->status.strayPkt.status == true) ||
    	(pPwInfo->status.malformedPkt.status == true) ||
    	(pPwInfo->status.excPktLossRate.status == true) ||
    	(pPwInfo->status.jitBufOverrun.status == true) ||
    	(pPwInfo->status.remotePktLoss.status == true) ||
    	(pPwInfo->status.lofs.status == true))
        {
        pPwInfo->parm.es.curSec   = 1;
        pPwInfo->parm.ses.curSec   = 1;
        }
        
    /* Second expired */
    if (isSecExpr == true)
        {
		/* Get anomaly counters and update current second registers */
		if (pSurInfo->interface.AnomalyGet != null)
			{
			OsalMemInit(&anomaly, sizeof(tSurPwAnomaly), 0);
			pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurPw, pPwId, &anomaly);

			pPwInfo->parm.numErrDataBlock += anomaly.playoutJitBufUnderrun;
			pPwInfo->parm.numErrDataBlock += anomaly.misorderedPktDropped;
			pPwInfo->parm.numErrDataBlock += anomaly.missingPkt;
			pPwInfo->parm.numErrDataBlock += anomaly.malformedPkt;
			pPwInfo->parm.numErrDataBlock += anomaly.rxLbitPkt;
			pPwInfo->parm.numErrDataBlock += anomaly.remotePktLoss;
			pPwInfo->parm.numErrDataBlock += anomaly.strayPkt;
			pPwInfo->parm.numPkt += anomaly.rxPkt;

			/* Update PM parameters */
			pPwInfo->parm.txPkt.curSec 					+= anomaly.txPkt;
			pPwInfo->parm.rxPkt.curSec 					+= anomaly.rxPkt;
			pPwInfo->parm.missingPkt.curSec 			+= anomaly.missingPkt;
			pPwInfo->parm.misorderedPkt.curSec 			+= anomaly.misorderedPkt;
			pPwInfo->parm.misorderedPktDropped.curSec 	+= anomaly.misorderedPktDropped;
			pPwInfo->parm.playoutJitBufOverrun.curSec 	+= anomaly.playoutJitBufOverrun;
			pPwInfo->parm.playoutJitBufUnderrun.curSec 	+= anomaly.playoutJitBufUnderrun;
			pPwInfo->parm.lops.curSec 					+= anomaly.lops;
			pPwInfo->parm.malformedPkt.curSec 			+= anomaly.malformedPkt;
			pPwInfo->parm.strayPkt.curSec 				+= anomaly.strayPkt;
			pPwInfo->parm.remotePktLoss.curSec 			+= anomaly.remotePktLoss;
			pPwInfo->parm.txLbitPkt.curSec 				+= anomaly.txLbitPkt;
			}

    	/* Update ES parameters */
    	if (pPwInfo->parm.numErrDataBlock != 0)
    		{
    		pPwInfo->parm.es.curSec   = 1;
    		}

    	/* Update SES parameters */
    	/* In 1 second, contains >= 30% errored data block, update SES */
    	if (pPwInfo->parm.numErrDataBlock * 100 >= pPwInfo->parm.numPkt * 30)
    		{
    		pPwInfo->parm.ses.curSec   = 1;
    		}

		/* Update UAS parameter during unavailable time */
        if (pPwInfo->status.neUat == true)
            {
            pPwInfo->parm.uas.curSec = 1;
            }

        /* Check if PW UAS just enter or exit and adjust registers */
        blNewStat = (pPwInfo->parm.ses.curSec == 1)? true : false;
        if (blNewStat != pPwInfo->status.neUatPot)
            {
            pPwInfo->status.nePotCnt = 1;
            }
        else
            {
            pPwInfo->status.nePotCnt++;
            }

        /* Declare potential for entering or exiting UAT if the condition is met */
        if (pPwInfo->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pPwInfo->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pPwInfo->parm.es));
                mCurPerNegAdj(&(pPwInfo->parm.ses));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pPwInfo->parm.es));
                    mPrePerNegAdj(&(pPwInfo->parm.ses));
                    }

                /* Update UAS */
            	pPwInfo->parm.uas.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pPwInfo->parm.es));
                mCurPerPosAdj(&(pPwInfo->parm.ses));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pPwInfo->parm.es));
                    mPrePerPosAdj(&(pPwInfo->parm.ses));
                    }

                /* Update UAS */
            	if (pPwInfo->parm.uas.curPer.value >= 10)
            		{
            		pPwInfo->parm.uas.curPer.value -= 10;
            		}
            	else
            		{
            		pPwInfo->parm.uas.curPer.value = 0;
            		}
            	pPwInfo->parm.uas.curSec = 0;
                }
            }

        /* Reset all parameters' accumulate inhibition mode */
        pPwInfo->parm.es.inhibit     					= false;
        pPwInfo->parm.ses.inhibit     					= false;
        pPwInfo->parm.txPkt.inhibit 					= false;
        pPwInfo->parm.rxPkt.inhibit 					= false;
        pPwInfo->parm.missingPkt.inhibit 				= false;
        pPwInfo->parm.misorderedPkt.inhibit 			= false;
        pPwInfo->parm.misorderedPktDropped.inhibit 		= false;
        pPwInfo->parm.playoutJitBufOverrun.inhibit 		= false;
        pPwInfo->parm.playoutJitBufUnderrun.inhibit 	= false;
        pPwInfo->parm.lops.inhibit 						= false;
        pPwInfo->parm.malformedPkt.inhibit 				= false;
        pPwInfo->parm.strayPkt.inhibit 					= false;
        pPwInfo->parm.remotePktLoss.inhibit 			= false;
        pPwInfo->parm.txLbitPkt.inhibit 				= false;

        /* Inhibit ES, SES parameters if the current second is UAS */
        if ((pPwInfo->status.neUat == true))
            {
            pPwInfo->parm.es.inhibit     = true;
            pPwInfo->parm.ses.inhibit     = true;
            }

        /* Increase PW PM parameters' adjustment registers */
        /* In available time */
        if (pPwInfo->status.neUat == false)
            {
            /* Potential for entering unavailable time */
            if ((pPwInfo->parm.ses.curSec == 1))
                {
                mNegInc(&(pPwInfo->parm.es));
                mNegInc(&(pPwInfo->parm.ses));
                }
            }
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pPwInfo->parm.ses.curSec == 0)
                {
                mPosInc(&(pPwInfo->parm.es));
                mPosInc(&(pPwInfo->parm.ses));
                }
            }

        /* Clear PM parameters' adjustment registers */
        if (pPwInfo->status.neUat == false)
            {
            if ((pPwInfo->parm.ses.curSec == 0) &&
                (pPwInfo->status.neUatPot  == true))
                {
                mNegClr(&(pPwInfo->parm.es));
                mNegClr(&(pPwInfo->parm.ses));
                }
            }
        else if ((pPwInfo->parm.ses.curSec == 1) &&
                 (pPwInfo->status.neUatPot  == false))
            {
            mPosClr(&(pPwInfo->parm.es));
            mPosClr(&(pPwInfo->parm.ses));
            }

        /* Store current second statuses */
        pPwInfo->status.neUatPot = (pPwInfo->parm.ses.curSec   == 1) ? true : false;

        /* Accumulate current period registers */
        mCurPerAcc(&(pPwInfo->parm.es));
        mCurPerAcc(&(pPwInfo->parm.ses));
        mCurPerAcc(&(pPwInfo->parm.uas));

        mCurPerAcc(&(pPwInfo->parm.txPkt));
        mCurPerAcc(&(pPwInfo->parm.rxPkt));
        mCurPerAcc(&(pPwInfo->parm.missingPkt));
        mCurPerAcc(&(pPwInfo->parm.misorderedPkt));
        mCurPerAcc(&(pPwInfo->parm.misorderedPktDropped));
        mCurPerAcc(&(pPwInfo->parm.playoutJitBufOverrun));
        mCurPerAcc(&(pPwInfo->parm.playoutJitBufUnderrun));
        mCurPerAcc(&(pPwInfo->parm.lops));
        mCurPerAcc(&(pPwInfo->parm.malformedPkt));
        mCurPerAcc(&(pPwInfo->parm.strayPkt));
        mCurPerAcc(&(pPwInfo->parm.remotePktLoss));
        mCurPerAcc(&(pPwInfo->parm.txLbitPkt));

    	pPwInfo->parm.numPkt = 0;
    	pPwInfo->parm.numErrDataBlock = 0;


        /* Check and report TCA */
        blNotf = ((pPwInfo->msgInh.pw    == false) &&
                  (pPwInfo->msgInh.tca   == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;

        /* Report TCAs of parameters are independent of SES and UAS */
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwTxPkt, &(pPwInfo->parm.txPkt), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwRxPkt, &(pPwInfo->parm.rxPkt), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwMissingPkt, &(pPwInfo->parm.missingPkt), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwMisorderedPkt, &(pPwInfo->parm.misorderedPkt), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwMisorderedPktDrop, &(pPwInfo->parm.misorderedPktDropped), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwPlayoutJitBufOverrun, &(pPwInfo->parm.playoutJitBufOverrun), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwPlayoutJitBufUnderrun, &(pPwInfo->parm.playoutJitBufUnderrun), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwLops, &(pPwInfo->parm.lops), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwMalformedPkt, &(pPwInfo->parm.malformedPkt), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwStrayPkt, &(pPwInfo->parm.strayPkt), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwRemotePktLoss, &(pPwInfo->parm.remotePktLoss), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwTxLbitPkt, &(pPwInfo->parm.txLbitPkt), blNotf, &tcaInfo);


        /* Report TCAs of PM parameters */
        if ((pPwInfo->status.neUat    == false) &&
            (pPwInfo->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwEs    ,&(pPwInfo->parm.es),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwSes   ,&(pPwInfo->parm.ses),    blNotf, &tcaInfo);
            }

        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurPw, pPwId, pPwInfo, cPmPwUas, &(pPwInfo->parm.uas), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pPwInfo->parm.es));
        mStackDown(&(pPwInfo->parm.ses));
        mStackDown(&(pPwInfo->parm.uas));

        mStackDown(&(pPwInfo->parm.txPkt));
        mStackDown(&(pPwInfo->parm.rxPkt));
        mStackDown(&(pPwInfo->parm.missingPkt));
        mStackDown(&(pPwInfo->parm.misorderedPkt));
        mStackDown(&(pPwInfo->parm.misorderedPktDropped));
        mStackDown(&(pPwInfo->parm.playoutJitBufOverrun));
        mStackDown(&(pPwInfo->parm.playoutJitBufUnderrun));
        mStackDown(&(pPwInfo->parm.lops));
        mStackDown(&(pPwInfo->parm.malformedPkt));
        mStackDown(&(pPwInfo->parm.strayPkt));
        mStackDown(&(pPwInfo->parm.remotePktLoss));
        mStackDown(&(pPwInfo->parm.txLbitPkt));

        /* Update time for period */
        pPwInfo->parm.prePerStartTime = pPwInfo->parm.curPerStartTime;
        pPwInfo->parm.prePerEndTime = dCurTime;
        pPwInfo->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pPwInfo->parm.es));
        mDayShift(&(pPwInfo->parm.ses));
        mDayShift(&(pPwInfo->parm.uas));

        mDayShift(&(pPwInfo->parm.txPkt));
        mDayShift(&(pPwInfo->parm.rxPkt));
        mDayShift(&(pPwInfo->parm.missingPkt));
        mDayShift(&(pPwInfo->parm.misorderedPkt));
        mDayShift(&(pPwInfo->parm.misorderedPktDropped));
        mDayShift(&(pPwInfo->parm.playoutJitBufOverrun));
        mDayShift(&(pPwInfo->parm.playoutJitBufUnderrun));
        mDayShift(&(pPwInfo->parm.lops));
        mDayShift(&(pPwInfo->parm.malformedPkt));
        mDayShift(&(pPwInfo->parm.strayPkt));
        mDayShift(&(pPwInfo->parm.remotePktLoss));
        mDayShift(&(pPwInfo->parm.txLbitPkt));

        /* Update time for day */
        pPwInfo->parm.preDayStartTime = pPwInfo->parm.curDayStartTime;
        pPwInfo->parm.preDayEndTime = dCurTime;
        pPwInfo->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurPwStatUpd(tSurDev          device,
											 const tSurPwId *pPwId)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input PW.

Inputs       : device                - device
               pPwId            - PW ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurPwStatUpd(tSurDev device, const tSurPwId *pPwId)
    {
    tSurInfo       *pSurInfo;

    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) && 
        (pSurInfo->interface.DefStatGet    != null))
        {
        PwAllDefUpd(pSurInfo, pPwId);
        }
    
    /* Declare/terminate failures */
    PwAllFailUpd(pSurInfo, pPwId);
    
    /* Update performance registers */
    PwPmParmUpd(pSurInfo, pPwId);
    }

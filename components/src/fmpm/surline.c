/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - SONET/SDH line
 *
 * File        : surline.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of SONET/SDH line
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/
/*--------------------------- Macros -----------------------------------------*/
/*--------------------------- Local Typedefs ---------------------------------*/
/*--------------------------- Global variables -------------------------------*/
/*--------------------------- Local variables --------------------------------*/
/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
This function is used to de-provision all channels in line.
------------------------------------------------------------------------------*/
extern void SurLineAllChnDeProv(tSurInfo         *pSurInfo,
                                const tSurLineId *pLineId);
                                
/*------------------------------------------------------------------------------
This function is used to deallocate all resource in line information structure
------------------------------------------------------------------------------*/
extern void SurLineInfoFinal(tSurLineInfo *pInfo);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in STS information structure
------------------------------------------------------------------------------*/
extern void SurStsInfoFinal(tSurStsInfo *pInfo);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in VT information structure
------------------------------------------------------------------------------*/
extern void SurVtInfoFinal(tSurVtInfo *pInfo);

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDefParmThresSet(tSurLineInfo *pInfo)

Purpose      : This function is used to set default values for line's PM parameters.

Inputs       : pInfo                 - line channel info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
private eSurRet SurLineDefParmSet(tSurLineInfo *pInfo)
    {
    /* Section parameters */
    mPmDefParmSet(&(pInfo->parm.sefsS), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.cvS), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esS), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesS), cPmPerThresVal, cPmDayThresVal);
    
    /* Near-end line parameters */
    mPmDefParmSet(&(pInfo->parm.cvL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.psc), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.psd), cPmPerThresVal, cPmDayThresVal);
    
    /* Far-end line parameters */
    mPmDefParmSet(&(pInfo->parm.cvLfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esLfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesLfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasLfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcLfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }


/*------------------------------------------------------------------------------
Prototype    : private tSurLineInfo* LineInfoCreate(tSurInfo   *pSurInfo,
                                                    const tSurLineId *pLineId)

Purpose      : This function is used to allocate resources for line

Inputs       : pSurInfo              - database handle of device
               pLineId               - line id

Outputs      : None

Return       : tSurLineInfo*         - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurLineInfo* LineInfoCreate(tSurInfo *pSurInfo, const tSurLineId *pLineId)
    {
    tSurLineInfo *pChnInfo;

    pChnInfo = OsalMemAlloc(sizeof(tSurLineInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurLineInfo), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history\n");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.sefsS));
        mPmParmInit(&(pChnInfo->parm.cvS));
        mPmParmInit(&(pChnInfo->parm.esS));
        mPmParmInit(&(pChnInfo->parm.sesS));
        mPmParmInit(&(pChnInfo->parm.cvL));
        mPmParmInit(&(pChnInfo->parm.esL));
        mPmParmInit(&(pChnInfo->parm.sesL));
        mPmParmInit(&(pChnInfo->parm.uasL));
        mPmParmInit(&(pChnInfo->parm.fcL));
        mPmParmInit(&(pChnInfo->parm.psc));
        mPmParmInit(&(pChnInfo->parm.psd));
        mPmParmInit(&(pChnInfo->parm.cvLfe));
        mPmParmInit(&(pChnInfo->parm.esLfe));
        mPmParmInit(&(pChnInfo->parm.sesLfe));
        mPmParmInit(&(pChnInfo->parm.uasLfe));
        mPmParmInit(&(pChnInfo->parm.fcLfe));
        
        /* Add to database */
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(pLineId)]->pInfo   = pChnInfo;
        }

    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurLineInfoFinal(tSurLineInfo *pInfo)

Purpose      : This function is used to deallocate all resource in line 
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurLineInfoFinal(tSurLineInfo *pInfo)
    {
    /* Finalize all parameters */
    mPmParmFinal(&(pInfo->parm.sefsS));
    mPmParmFinal(&(pInfo->parm.cvS));
    mPmParmFinal(&(pInfo->parm.esS));
    mPmParmFinal(&(pInfo->parm.sesS));
    mPmParmFinal(&(pInfo->parm.cvL));
    mPmParmFinal(&(pInfo->parm.esL));
    mPmParmFinal(&(pInfo->parm.sesL));
    mPmParmFinal(&(pInfo->parm.uasL));
    mPmParmFinal(&(pInfo->parm.fcL));
    mPmParmFinal(&(pInfo->parm.psc));
    mPmParmFinal(&(pInfo->parm.psd));
    mPmParmFinal(&(pInfo->parm.cvLfe));
    mPmParmFinal(&(pInfo->parm.esLfe));
    mPmParmFinal(&(pInfo->parm.sesLfe));
    mPmParmFinal(&(pInfo->parm.uasLfe));
    mPmParmFinal(&(pInfo->parm.fcLfe));

    /* Destroy failure history and failure history */
    if (*(&(pInfo->failHis))!=NULL) {
        if (ListDestroy(&(pInfo->failHis)) != cListSucc){
            mSurDebug(cSurWarnMsg, "Cannot destroy Failure History \n");
        }
    }
    if (*(&(pInfo->tcaHis))!=NULL){
         if (ListDestroy(&(pInfo->tcaHis))  != cListSucc){
             mSurDebug(cSurWarnMsg, "Cannot destroy TCA history\n");
         }
    }
    }

/*------------------------------------------------------------------------------
Prototype    : private void LineInfoDestroy(tSurInfo   *pSurInfo,
                                            tSurLineId *pLineId)

Purpose      : This function is used to deallocate resources of line

Inputs       : pSurInfo                 - database handle of device
               pLineId                  - line id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
private void LineInfoDestroy(tSurInfo *pSurInfo, tSurLineId *pLineId)
    {
    tSurLineInfo *pInfo;

    /* Get channel information */
    SurLineAllChnDeProv(pSurInfo, pLineId);
    mSurLineInfo(pSurInfo, pLineId, pInfo);
    if (pInfo != null)
        {
        SurLineInfoFinal(pInfo);

        /* Destroy other resources */
        OsalMemFree(mChnDb(pSurInfo).pLine[mSurLineIdFlat(pLineId)]->pInfo);
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(pLineId)]->pInfo = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet LineParmGet(tSurLineParm    *pLineParm, 
                                   ePmLineParm      parm, 
                                   tPmParm         *pParm)

Purpose      : This functions is used to get a  pointer to a line parameter.

Inputs       : pLineParm             - pointer to the line parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet LineParmGet(tSurLineParm *pLineParm, ePmLineParm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pLineParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Section SEFS */
        case cPmSecSefs:
            *ppParm = &(pLineParm->sefsS);
            break;
            
        /* Section CV */
        case cPmSecCv:
            *ppParm = &(pLineParm->cvS);
            break;
        
        /* Section ES */
        case cPmSecEs:
            *ppParm = &(pLineParm->esS);
            break;
        
        /* Section SES */
        case cPmSecSes:
            *ppParm = &(pLineParm->sesS);
            break;
        
        /* Near-end line CV */
        case cPmLineCv:
            *ppParm = &(pLineParm->cvL);
            break;
        
        /* Near-end line ES */
        case cPmLineEs:
            *ppParm = &(pLineParm->esL);
            break;
        
        /* Near-end line SES */
        case cPmLineSes:
            *ppParm = &(pLineParm->sesL);
            break;
        
        /* Near-end line UAS */
        case cPmLineUas:
            *ppParm = &(pLineParm->uasL);
            break;
        
        /* Near-end line FC */
        case cPmLineFc:
            *ppParm = &(pLineParm->fcL);
            break;
        
        /* Near-end line PSC */
        case cPmLinePsc:
            *ppParm = &(pLineParm->psc);
            break;
        
        /* Near-end line PSD */
        case cPmLinePsd:
            *ppParm = &(pLineParm->psd);
            break;
        
        /* Far-end line CV */
        case cPmLineCvLfe:
            *ppParm = &(pLineParm->cvLfe);
            break;
        
        /* Far-end line ES */
        case cPmLineEsLfe:
            *ppParm = &(pLineParm->esLfe);
            break;
        
        /* Far-end line SES */
        case cPmLineSesLfe:
            *ppParm = &(pLineParm->sesLfe);
            break;
        
        /* Far-end line UAS */
        case cPmLineUasLfe:
            *ppParm = &(pLineParm->uasLfe);
            break;
        
        /* Far-end line FC */
        case cPmLineFcLfe:
            *ppParm = &(pLineParm->fcLfe);
            break;
        
        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllLineParmRegReset(tSurLineParm *pLineParm, 
                                           ePmRegType    regType,
                                           byte          lineParmType)

Purpose      : This functions is used to reset registers of a group of line 
               parameters.

Inputs       : pLineParm             - pointer to the line parameters
               regType               - register type
               lineParmType          - line parameter group type:
                                        + 0: section parameters
                                        + 1: near-end parameters
                                        + 2: far-end parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllLineParmRegReset(tSurLineParm *pLineParm, 
                                   ePmRegType    regType,
                                   byte          lineParmType)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmLineParm  start;
    ePmLineParm  end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pLineParm == null)
        {
        return cSurErrNullParm;
        }
    
    switch (lineParmType)
        {
        /* Section parameter type */
        case cPmSecParmType:
            start = cPmSecSefs;
            end   = cPmSecAllParm;
            break;
            
        /* Near-end parameter type */
        case cPmNeLineParmType:
            start = cPmLineCv;
            end   = cPmLineNeAllParm;
            break;
        
        /* Far-end parameter type */    
        case cPmFaLineParmType:
            start = cPmLineCvLfe;
            end   = cPmLineFeAllParm;
            break;
        
        /* Invalid parameter type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Reset registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = LineParmGet(pLineParm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllLineParmAccInhSet(tSurLineParm *pLineParm, 
                                            bool          inhibit, 
                                            byte          lineParmType)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               line parameters.

Inputs       : pLineParm             - pointer to the line parameters
               inhibit               - inhibition mode
               lineParmType          - line parameter group type:
                                        + 0: section parameters
                                        + 1: near-end parameters
                                        + 2: far-end parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllLineParmAccInhSet(tSurLineParm *pLineParm, 
                                    bool          inhibit, 
                                    byte          lineParmType)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    ePmLineParm  start;
    ePmLineParm  end;
    
    /* Initialize */
    pParm = null;
    
    /* Check for null pointers */
    if (pLineParm == null)
        {
        return cSurErrNullParm;
        }
        
    switch (lineParmType)
        {
        /* Section parameter type */
        case cPmSecParmType:
            start = cPmSecSefs;
            end   = cPmSecAllParm;
            break;
            
        /* Near-end parameter type */
        case cPmNeLineParmType:
            start = cPmLineCv;
            end   = cPmLineNeAllParm;
            break;
        
        /* Far-end parameter type */    
        case cPmFaLineParmType:
            start = cPmLineCvLfe;
            end   = cPmLineFeAllParm;
            break;
            
        /* Invalid parameter type */
        default:
            return cSurErrInvlParm;
        }
    
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = LineParmGet(pLineParm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet AllLineParmRegThresSet(tSurLineParm *pLineParm, 
                                              ePmRegType    regType,
                                              dword         thresVal, 
                                              byte          lineParmType)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               line parameters.

Inputs       : pLineParm             - pointer to the line parameters
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               lineParmType          - line parameter group type:
                                        + 0: section parameters
                                        + 1: near-end parameters
                                        + 2: far-end parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllLineParmRegThresSet(tSurLineParm *pLineParm, 
                                      ePmRegType    regType,
                                      dword         thresVal, 
                                      byte          lineParmType)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    ePmLineParm  start;
    ePmLineParm  end;
    
    /* Initialize */
    pParm = null;
    
    /* Check for null pointers */
    if (pLineParm == null)
        {
        return cSurErrNullParm;
        }
        
    switch (lineParmType)
        {
        /* Section parameter type */
        case cPmSecParmType:
            start = cPmSecSefs;
            end   = cPmSecAllParm;
            break;
            
        /* Near-end parameter type */
        case cPmNeLineParmType:
            start = cPmLineCv;
            end   = cPmLineNeAllParm;
            break;
        
        /* Far-end parameter type */    
        case cPmFaLineParmType:
            start = cPmLineCvLfe;
            end   = cPmLineFeAllParm;
            break;
            
        /* Invalid parameter type */
        default:
            return cSurErrInvlParm;
        }
    
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = LineParmGet(pLineParm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllLineParmNumRecRegSet(tSurLineParm *pLineParm, 
                                               byte          numRecReg, 
                                               byte          lineParmType)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of line parameters.

Inputs       : pLineParm             - pointer to the line parameters
               numRecReg             - number of recent registers                       
               lineParmType          - line parameter group type:
                                        + 0: section parameters
                                        + 1: near-end parameters
                                        + 2: far-end parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllLineParmNumRecRegSet(tSurLineParm *pLineParm, 
                                       byte          numRecReg, 
                                       byte          lineParmType)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    ePmLineParm  start;
    ePmLineParm  end;
    
    /* Initialize */
    pParm = null;
    
    /* Check for null pointers */
    if (pLineParm == null)
        {
        return cSurErrNullParm;
        }
        
    switch (lineParmType)
        {
        /* Section parameter type */
        case cPmSecParmType:
            start = cPmSecSefs;
            end   = cPmSecAllParm;
            break;
            
        /* Near-end parameter type */
        case cPmNeLineParmType:
            start = cPmLineCv;
            end   = cPmLineNeAllParm;
            break;
        
        /* Far-end parameter type */    
        case cPmFaLineParmType:
            start = cPmLineCvLfe;
            end   = cPmLineFeAllParm;
            break;
            
        /* Invalid parameter type */
        default:
            return cSurErrInvlParm;
        }
    
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = LineParmGet(pLineParm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                                          
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurLineProv(tSurDev device, tSurLineId *pLineId)

Purpose      : This function is used to provision a line for FM & PM.

Inputs       : device                - Device
               pLineId               - Line identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineProv(tSurDev device, tSurLineId *pLineId)
    {
    tSurInfo     *pSurInfo;
    tSurLineInfo *pChnInfo;
    tSurLineDb   *pLineDb;
    
    /* Check parameter's validity */
    if (mIsNull(pLineId))
        {
        return cSurErrNullParm;
        }
    
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrSem;
        }

    /* Get line information */
    mSurLineInfo(pSurInfo, pLineId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "Line is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for line */
    pChnInfo = LineInfoCreate(pSurInfo, pLineId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for line\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default configuration for line channel */
    pChnInfo->conf.rate = cSurOc1Stm0;
    pChnInfo->conf.decSoakTime = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime = cSurDefaultTerSoakTime;
    
    pLineDb = mChnDb(pSurInfo).pLine[mSurLineIdFlat(pLineId)];
    pLineDb->numSts  = mSurNumSts(cSurOc1Stm0);
    pLineDb->ppStsDb = OsalMemAlloc(pLineDb->numSts * sizeof(tSurStsDb*));
    OsalMemInit(pLineDb->ppStsDb, pLineDb->numSts * sizeof(tSurStsDb*), 0);

        /* Allocate database for STSs within the line channel */
    if (pLineDb->ppStsDb == null)
        {
        mSurDebug(cSurWarnMsg, "Cannot allocate STS resources for line\n");
        }
    else
        {
        OsalMemInit(pLineDb->ppStsDb, pLineDb->numSts * sizeof(tSurStsDb*), 0);
        }
        
    /* Set default parameters' values for line channel */
    if (SurLineDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for channel's PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErr;
        }
        
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurLineDeProv(tSurDev device, tSurLineId *pLineId)

Purpose      : This function is used to de-provision FM & PM of one line. It
               will deallocated all resources.

Inputs       : device                - device
               pLineId               - Line identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineDeProv(tSurDev device, tSurLineId *pLineId)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameter's validity */
    if (mIsNull(pLineId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrSem;
        }

    /* Deallocate all resources of line */
    LineInfoDestroy(pSurInfo, pLineId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineTrblDesgnSet(tSurDev              device, 
                                           eSurLineTrbl         trblType, 
                                           const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set line trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               pTrblDesgn            - trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineTrblDesgnSet(tSurDev              device, 
                                   eSurLineTrbl         trblType, 
                                   const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrNullParm;
        }
    
    /* Set trouble designation */
    switch (trblType)
        {
        /* Section */
        /* Loss Of Signal status */    
        case cFmSecLos:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.los);
            break;
            
        /* Loss Of Frame status */
        case cFmSecLof:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lof);
            break;
            
        /* Section Trace Identifier Mismatch */
        case cFmSecTim:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tims);
            break;
            
        /* All Section failures */
        case cFmSecAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.los);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lof);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tims);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
            
        /* Line */        
        /* Line BER-based Signal Failure failure indication */    
        case cFmLineBerSf:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lineBerSf);
            break;
            
        /* Line BER-based Signal Degrade failure indication */
        case cFmLineBerSd:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lineBerSd);
            break;
            
        /* DCC failure indication */
        case cFmLineDccFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.dcc);
            break;
            
        /* Alarm Indication Signal failure indication */
        case cFmLineAis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisl);
            break;
            
        /* Remote Failure Indication */
        case cFmLineRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfil);
            break;
            
        /* All Line failures */
        case cFmLineAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lineBerSf);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lineBerSd);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.dcc);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisl);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfil);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
            
        /* Performance monitoring */
        case cPmLineTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineTrblDesgnGet(tSurDev        device, 
                                           eSurLineTrbl   trblType, 
                                           tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get line trouble designation of 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               
Outputs      : pTrblDesgn            - trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineTrblDesgnGet(tSurDev        device, 
                                   eSurLineTrbl   trblType, 
                                   tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    pSurInfo = mDevDb(device);
    switch (trblType)
        {
        /* Section */
        /* Loss Of Signal status */    
        case cFmSecLos:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.los);
            break;
            
        /* Loss Of Frame status */
        case cFmSecLof:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lof);
            break;
            
        /* Section Trace Identifier Mismatch */
        case cFmSecTim:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tims);
            break;
            
        /* Line */        
        /* Line BER-based Signal Failure failure indication */    
        case cFmLineBerSf:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lineBerSf);
            break;
            
        /* Line BER-based Signal Degrade failure indication */
        case cFmLineBerSd:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lineBerSd);
            break;
            
        /* DCC failure indication */
        case cFmLineDccFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.dcc);
            break;
            
        /* Alarm Indication Signal failure indication */
        case cFmLineAis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisl);
            break;
            
        /* Remote Failure Indication */
        case cFmLineRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfil);
            break;
                
        /* Performance monitoring */
        case cPmLineTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Get configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineEngStatSet(tSurDev             device, 
                                         const tSurLineId   *pChnId, 
                                         eSurEngStat         stat)

Purpose      : This API is used to set engine state for a line channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurLineEngStatSet(tSurDev             device, 
                                 const tSurLineId   *pChnId, 
                                 eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (pInfo == null)
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineEngStatGet(tSurDev             device, 
                                         const tSurLineId   *pChnId, 
                                         eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a line APS channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurLineEngStatGet(tSurDev             device, 
                                 const tSurLineId   *pChnId, 
                                 eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineChnCfgSet(tSurDev             device,
                                        const tSurLineId   *pChnId, 
                                        const tSurLineConf *pChnCfg)

Purpose      : This internal API is use to set line channel's configuration.
               Note: when line rate is changed, all channels (STSs/AUs, VTs, 
               TU3s) are de-provisioned.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineChnCfgSet(tSurDev  device, 
                                const tSurLineId    *pChnId, 
                                const tSurLineConf  *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurLineInfo   *pInfo;
    tSurLineDb     *pLineDb;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get line info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Change line rate and de-provision all channels in line */
    if (pInfo->conf.rate != pChnCfg->rate)
        {
        SurLineAllChnDeProv(pSurInfo, pChnId);
        
        /* Reallocate memory for new line */
        pInfo = LineInfoCreate(pSurInfo, pChnId);
        if (pInfo == null)
            {
            mSurDebug(cSurErrMsg, "Cannot allocate resources for line\n");
            ret = mDevGive(device);
            return cSurErrRsrNoAvai;
            }        
        
        pInfo->conf.rate = pChnCfg->rate;
        pInfo->conf.decSoakTime = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
        pInfo->conf.terSoakTime = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
        
        pLineDb = mChnDb(pSurInfo).pLine[mSurLineIdFlat(pChnId)];
        pLineDb->numSts  = mSurNumSts(pChnCfg->rate);
        pLineDb->ppStsDb = OsalMemAlloc(pLineDb->numSts * sizeof(tSurStsDb*));
        
        /* Allocate resources fail */
        if (pLineDb->ppStsDb == null)
            {
            mSurDebug(cSurWarnMsg, "Cannot allocate STS resources for line\n");
            }
        else
            {
            OsalMemInit(pLineDb->ppStsDb, pLineDb->numSts * sizeof(tSurStsDb*), 0);
            }
            
        /* Set default parameters' values for line channel */
        if (SurLineDefParmSet(pInfo) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot set default values for channel's PM parameters\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineChnCfgGet(tSurDev     device,
                                        const tSurLineId *pChnId, 
                                        tSurLineConf *pChnCfg)
    {
Purpose      : This API is used to get line channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineChnCfgGet(tSurDev           device,
                                const tSurLineId *pChnId, 
                                tSurLineConf     *pChnCfg)
    {
    /* Declare local variables */
    tSurLineInfo  *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get configuration */
    pChnCfg->rate = pInfo->conf.rate;
    pChnCfg->decSoakTime = pInfo->conf.decSoakTime;
    pChnCfg->terSoakTime = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineTrblNotfInhSet(tSurDev               device,
                                             const tSurLineId     *pChnId, 
                                             eSurLineTrbl          trblType, 
                                             bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a line channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineTrblNotfInhSet(tSurDev           device,
                                     const tSurLineId *pChnId, 
                                     eSurLineTrbl      trblType, 
                                     bool              inhibit)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* Section */
        case cFmSecLos:
            pInfo->msgInh.los = inhibit;
            break;
        case cFmSecLof:
            pInfo->msgInh.lof = inhibit;
            break;
        case cFmSecTim:
            pInfo->msgInh.tim = inhibit;
            break;
        case cFmSecAllFail:
            pInfo->msgInh.section = inhibit;
            break;
        
        /* Line */
        case cFmLineBerSf:
            pInfo->msgInh.berSf = inhibit;
            break;
        case cFmLineBerSd:
            pInfo->msgInh.berSd = inhibit;
            break;
        case cFmLineDccFail:
            pInfo->msgInh.dcc = inhibit;
            break;
        case cFmLineAis:
            pInfo->msgInh.aisl = inhibit;
            break;
        case cFmLineRfi:
            pInfo->msgInh.rfil = inhibit;
            break;
        case cFmLineAllFail:
            pInfo->msgInh.line = inhibit;
            
        /* Performance monitoring */
        case cPmLineTca:
            pInfo->msgInh.tca = inhibit;
            break;
            
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineTrblNotfInhGet(tSurDev               device,
                                             const tSurLineId     *pChnId, 
                                             eSurLineTrbl          trblType, 
                                             bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a line channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineTrblNotfInhGet(tSurDev           device,
                                     const tSurLineId *pChnId, 
                                     eSurLineTrbl      trblType, 
                                     bool             *pInhibit)
    {
    /* Declare local varibles */
    tSurLineInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Switch trouble type */
    switch (trblType)
        {
        /* Section */
        case cFmSecLos:
            *pInhibit = pInfo->msgInh.los;
            break;
        case cFmSecLof:
            *pInhibit = pInfo->msgInh.lof;
            break;
        case cFmSecTim:
            *pInhibit = pInfo->msgInh.tim;
            break;
        case cFmSecAllFail:
            *pInhibit = pInfo->msgInh.section;
            break;
        
        /* Line */
        case cFmLineBerSf:
            *pInhibit = pInfo->msgInh.berSf;
            break;
        case cFmLineBerSd:
            *pInhibit = pInfo->msgInh.berSd;
            break;
        case cFmLineDccFail:
            *pInhibit = pInfo->msgInh.dcc;
            break;
        case cFmLineAis:
            *pInhibit = pInfo->msgInh.aisl;
            break;
        case cFmLineRfi:
            *pInhibit = pInfo->msgInh.rfil;
            break;
        case cFmLineAllFail:
            *pInhibit = pInfo->msgInh.line;
            break;
            
        /* Performance monitoring */
        case cPmLineTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
            
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }    

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurLineChnTrblNotfInhInfoGet(tSurDev             device, 
                                                    const tSurLineId   *pChnId, 
                                                    tSurLineMsgInh     *pTrblInh)

Purpose      : This API is used to get line channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurLineChnTrblNotfInhInfoGet(tSurDev             device, 
                                            const tSurLineId   *pChnId, 
                                            tSurLineMsgInh     *pTrblInh)
    {
    tSurLineInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurLineMsgInh));
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : friend void SurLineAllChnDeProv(tSurDev          *pSurInfo,
                                               const tSurLineId *pLineId)

Purpose      : This function is used to de-provision all channels in line.

Inputs       : device                - Device
               pLineId               - Line ID
               
Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurLineAllChnDeProv(tSurInfo         *pSurInfo,
                                const tSurLineId *pLineId)
    {
    tSurLineDb *pLineDb;
    tSurStsDb  *pStsDb;
    word        wSts;
    byte        bVt;
    
    pLineDb = mChnDb(pSurInfo).pLine[mSurLineIdFlat(pLineId)];
    if (pLineDb != null)
        {
        if (pLineDb->pInfo != null)
            {
            SurLineInfoFinal(pLineDb->pInfo);
            OsalMemFree(pLineDb->pInfo);
            pLineDb->pInfo = null;
            }
        
        /* De-provision all STSs of line */
        for (wSts = 0; wSts < pLineDb->numSts; wSts++)
            {
            pStsDb = pLineDb->ppStsDb[wSts];
            if (pStsDb != null)
                {
                if (pStsDb->pInfo != null)
                    {
                    SurStsInfoFinal(pStsDb->pInfo);
                    OsalMemFree(pStsDb->pInfo);
                    pStsDb->pInfo = null;
                    }
                
                /* De-provision TU3 */
                if (pStsDb->pTu3Info != null)
                    {
                    SurStsInfoFinal(pStsDb->pTu3Info);
                    OsalMemFree(pStsDb->pTu3Info);
                    pStsDb->pTu3Info = null;
                    }
                
                /* De-provision all VTs in STS */
                for (bVt = 0; bVt < cSurMaxVtInSts; bVt++)
                    {
                    if (pStsDb->ppVtInfo[bVt] != null)
                        {
                        SurVtInfoFinal(pStsDb->ppVtInfo[bVt]);
                        OsalMemFree(pStsDb->ppVtInfo[bVt]);
                        pStsDb->ppVtInfo[bVt] = null;
                        }
                    }
                
                OsalMemFree(pLineDb->ppStsDb[wSts]);
                pLineDb->ppStsDb[wSts] = null;
                }
            }
        
        if (pLineDb->ppStsDb != null)
            {
            OsalMemFree(pLineDb->ppStsDb);
            pLineDb->ppStsDb = null;
            }
        pLineDb->numSts = 0;
        }
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmLineFailIndGet(tSurDev             device, 
                                        const tSurLineId   *pChnId, 
                                        eSurLineTrbl        trblType, 
                                        tSurFailStat       *pFailStat)

Purpose      : This API is used to get failure indication of a line channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmLineFailIndGet(tSurDev             device, 
                                const tSurLineId   *pChnId, 
                                eSurLineTrbl        trblType, 
                                tSurFailStat       *pFailStat)
    {
    /* Declare local variables */
    tSurLineInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
        
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Switch trouble type */
    switch (trblType)
        {
        /* Section */
        case cFmSecLos:
            mSurFailStatCopy(&(pInfo->failure.los), pFailStat);
            break;
            
        case cFmSecLof:
            mSurFailStatCopy(&(pInfo->failure.lof), pFailStat);
            break;
            
        case cFmSecTim:
            mSurFailStatCopy(&(pInfo->failure.tims), pFailStat);
            break;
        
        /* Line */
        case cFmLineBerSf:
            mSurFailStatCopy(&(pInfo->failure.lineBerSf), pFailStat);
            break;
            
        case cFmLineBerSd:
            mSurFailStatCopy(&(pInfo->failure.lineBerSd), pFailStat);
            break;
            
        case cFmLineDccFail:
            mSurFailStatCopy(&(pInfo->failure.dcc), pFailStat);
            break;
            
        case cFmLineAis:
            mSurFailStatCopy(&(pInfo->failure.aisl), pFailStat);
            break;
            
        case cFmLineRfi:
            mSurFailStatCopy(&(pInfo->failure.rfil), pFailStat);
            break;
            
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmLineFailHisGet(tSurDev           device,
                                                       const tSurLineId *pChnId)

Purpose      : This API is used to get a line channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmLineFailHisGet(tSurDev           device,
                                        const tSurLineId *pChnId)
    {
    /* Declare local variables */
    tSurLineInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmLineFailHisClear(tSurDev device, 
                                          const tSurLineId *pChnId, 
                                          bool flush)

Purpose      : This API is used to clear a line channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.
                                     
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmLineFailHisClear(tSurDev device, const tSurLineId *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineRegReset(tSurDev       device, 
                                      tSurLineId   *pChnId, 
                                      ePmLineParm   parm, 
                                      ePmRegType    regType)

Purpose      : This API is used to reset a line channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmLineRegReset(tSurDev       device, 
                              tSurLineId   *pChnId, 
                              ePmLineParm   parm, 
                              ePmRegType    regType)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all section parameters */
    if (parm == cPmSecAllParm)
        {
        ret = AllLineParmRegReset(&(pInfo->parm), regType, cPmSecParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all section parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all near-end line parameters */
    else if (parm == cPmLineNeAllParm)
        {
        ret = AllLineParmRegReset(&(pInfo->parm), regType, cPmNeLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end line parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end line parameters */
    else if (parm == cPmLineFeAllParm)
        {
        ret = AllLineParmRegReset(&(pInfo->parm), regType, cPmFaLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end line parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = LineParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }  

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineRegGet(tSurDev             device, 
                                    const tSurLineId   *pChnId, 
                                    ePmLineParm         parm, 
                                    ePmRegType          regType, 
                                    tPmReg             *pRetReg)

Purpose      : This API is used to get a line channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmLineRegGet(tSurDev             device, 
                            const tSurLineId   *pChnId, 
                            ePmLineParm         parm, 
                            ePmRegType          regType, 
                            tPmReg             *pRetReg)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = LineParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->value   = pReg->value;
    pRetReg->invalid = pReg->invalid;
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : const tSortList PmLineTcaHisGet(tSurDev device, const tSurLineId *pChnId)

Purpose      : This API is used to get a line channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmLineTcaHisGet(tSurDev device, const tSurLineId *pChnId)
    {
    /* Declare local variables */
    tSurLineInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineTcaHisClear(tSurDev device, const void *pChnId)

Purpose      : This API is used to clear a line channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmLineTcaHisClear(tSurDev device, const tSurLineId *pChnId)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmLineParmAccInhSet(tSurDev           device, 
                                            const tSurLineId *pChnId, 
                                            ePmLineParm       parm, 
                                            bool              inhibit)

Purpose      : This API is used to enable/disable a line channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmLineParmAccInhSet(tSurDev           device, 
                                   const tSurLineId *pChnId, 
                                   ePmLineParm       parm, 
                                   bool              inhibit)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all section parameters */
    if (parm == cPmSecAllParm)
        {
        ret = AllLineParmAccInhSet(&(pInfo->parm), inhibit, cPmSecParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Setting section parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all near-end line parameters */
    else if (parm == cPmLineNeAllParm)
        {
        ret = AllLineParmAccInhSet(&(pInfo->parm), inhibit, cPmNeLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Setting near-end line parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end line parameters */
    else if (parm == cPmLineFeAllParm)
        {
        ret = AllLineParmAccInhSet(&(pInfo->parm), inhibit, cPmFaLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Setting far-end line parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = LineParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineParmAccInhGet(tSurDev           device, 
                                           const tSurLineId *pChnId, 
                                           ePmLineParm       parm, 
                                           bool             *pInhibit)

Purpose      : This API is used to get a line channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmLineParmAccInhGet(tSurDev           device, 
                                   const tSurLineId *pChnId, 
                                   ePmLineParm       parm, 
                                   bool             *pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurLineInfo    *pInfo;
    tPmParm         *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = LineParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineRegThresSet(tSurDev            device, 
                                         const tSurLineId  *pChnId, 
                                         ePmLineParm        parm, 
                                         ePmRegType         regType, 
                                         dword              thresVal)

Purpose      : This API is used to set threshold for a line channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmLineRegThresSet(tSurDev            device, 
                                 const tSurLineId  *pChnId, 
                                 ePmLineParm        parm, 
                                 ePmRegType         regType, 
                                 dword              thresVal)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all section parameters */
    if (parm == cPmSecAllParm)
        {
        ret = AllLineParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal, 
                                     cPmSecParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting section parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all near-end line parameters */
    else if (parm == cPmLineNeAllParm)
        {
        ret = AllLineParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     cPmNeLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end line parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end line parameters */
    else if (parm == cPmLineFeAllParm)
        {
        ret = AllLineParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     cPmFaLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end line parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = LineParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineRegThresGet(tSurDev            device, 
                                         const tSurLineId  *pChnId, 
                                         ePmLineParm        parm, 
                                         ePmRegType         regType, 
                                         dword             *pThresVal)

Purpose      : This API is used to get threshold of a line channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmLineRegThresGet(tSurDev            device, 
                                 const tSurLineId  *pChnId, 
                                 ePmLineParm        parm, 
                                 ePmRegType         regType, 
                                 dword             *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurLineInfo    *pInfo;
    tPmParm         *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = LineParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    }        

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineParmNumRecRegSet(tSurDev           device, 
                                              const tSurLineId *pChnId, 
                                              ePmLineParm       parm, 
                                              byte              numRecReg)

Purpose      : This API is used to set a line channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmLineParmNumRecRegSet(tSurDev           device, 
                                      const tSurLineId *pChnId, 
                                      ePmLineParm       parm, 
                                      byte              numRecReg)
    {
    /* Declare local variables */
    tSurLineInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    mPmRecRegCheck(numRecReg);  
    
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurLineInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set number of recent registers for all section parameters */
    if (parm == cPmSecAllParm)
        {
        ret = AllLineParmNumRecRegSet(&(pInfo->parm), numRecReg, cPmSecParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting section parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all near-end line parameters */
    else if (parm == cPmLineNeAllParm)
        {
        ret = AllLineParmNumRecRegSet(&(pInfo->parm), numRecReg, cPmNeLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end line parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end line parameters */
    else if (parm == cPmLineFeAllParm)
        {
        ret = AllLineParmNumRecRegSet(&(pInfo->parm), numRecReg, cPmFaLineParmType);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end line parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = LineParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmLineParmNumRecRegGet(tSurDev           device, 
                                              const tSurLineId *pChnId, 
                                              ePmLineParm       parm, 
                                              byte             *pNumRecReg)

Purpose      : This API is used to get a line channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmLineParmNumRecRegGet(tSurDev           device, 
                                      const tSurLineId *pChnId, 
                                      ePmLineParm       parm, 
                                      byte             *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurLineInfo    *pInfo;
    tPmParm         *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurLineInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = LineParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
            
            

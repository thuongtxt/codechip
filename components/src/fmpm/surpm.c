/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR
 *
 * File        : surpm.c
 *
 * Created Date: 06-Jan-09
 *
 * Description : This file contain all source code for Performance Monitoring 
 *               module.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
 
 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include "stdio.h"

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/
/*--------------------------- Macros -----------------------------------------*/
#define mPmTimeStampGet(timeType, pInfo, pStartTime, pEndTime, ret) \
	{ \
	ret = cSurOk; \
	switch (timeType) \
		{ \
		case cPmCurPer: \
			*pStartTime = pInfo->parm.curPerStartTime; \
			*pEndTime = dCurTime; \
			break; \
		case cPmPrePer: \
			*pStartTime = pInfo->parm.prePerStartTime; \
			*pEndTime = pInfo->parm.prePerEndTime; \
			break; \
		case cPmCurDay: \
			*pStartTime = pInfo->parm.curDayStartTime; \
			*pEndTime = dCurTime; \
			break; \
		case cPmPreDay: \
			*pStartTime = pInfo->parm.preDayStartTime; \
			*pEndTime = pInfo->parm.preDayEndTime; \
			break; \
		default: \
			ret = cSurErrInvlParm; \
			break; \
		} \
	}
/*--------------------------- Local Typedefs ---------------------------------*/
/*--------------------------- Global variables -------------------------------*/
extern dword    dCurTime;
/*--------------------------- Local variables --------------------------------*/
/*--------------------------- Forward declaration ----------------------------*/

/*------------------------------------------------------------------------------
Prototype    : eSurRet   RegGet(tPmParm      *pParm, 
                                ePmRegType    regType, 
                                tPmReg       **ppReg)

Purpose      : This functions is used to get a  pointer to a register.

Inputs       : pParm                 - pointer to the parameter
               regType               - register type
               
Outputs      : ppReg                 - pointer to the register

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet RegGet(tPmParm *pParm, ePmRegType regType, tPmReg **ppReg)
    {
    /* Check for null pointers */
    if (pParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Normal registers */
    if (regType >= 0 && regType < cPmRecPer)
        {
        switch(regType)
            {
            /* Current Period register */
            case cPmCurPer:
                *ppReg = &(pParm->curPer);
                break;
            
            /* Previous period register */
            case cPmPrePer:
                *ppReg = &(pParm->prePer);
                break;
                
            /* Current day register */
            case cPmCurDay:
                *ppReg = &(pParm->curDay);
                break;
                                
            /* Previous day register */
            case cPmPreDay:
                *ppReg = &(pParm->preDay);
                break;
            
            /* Default */  
            default:
                break;
            }
        }
        
    /* Recent registers */
    else if ( regType >= cPmRecPer && regType < (pParm->numRecReg + cPmRecPer)) 
        {
        *ppReg = &(pParm->pRecent[regType - cPmRecPer]);
        }
        
    /* Invalid register type */
    else
        {
        return cSurErrInvlParm;
        }
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet RegReset(tPmParm *pParm, ePmRegType regType)

Purpose      : This functions is used to reset register(s) based on it channel type.

Inputs       : pParm                 - parameter that contains the register(s)
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet RegReset(tPmParm *pParm, ePmRegType regType)
    {
    /* Check for null pointers */
    if (pParm == null)
        {
        return cSurErrNullParm;
        }
    
    /* Normal registers */
    if (regType >= 0 && regType < cPmRecPer)
        {
        switch(regType)
            {
            /* Current Period register */
            case cPmCurPer:
                mPmRegReset(&(pParm->curPer));
                mPmRegReset(&(pParm->curPerNegAdj));
                mPmRegReset(&(pParm->curPerPosAdj));
                break;
            
            /* Previous period register */
            case cPmPrePer:
                mPmRegReset(&(pParm->prePer));
                mPmRegReset(&(pParm->prePerNegAdj));
                mPmRegReset(&(pParm->prePerPosAdj));
                break;
                
            /* Current day register */
            case cPmCurDay:
                mPmRegReset(&(pParm->curDay));
                break;
                                
            /* Previous day register */
            case cPmPreDay:
                mPmRegReset(&(pParm->preDay));
                break;
            
            /* Default */  
            default:
                break;
            }
        }
        
     /* Recent registers */
    else if ( regType >= cPmRecPer && regType < (pParm->numRecReg + cPmRecPer))
        {
        mPmRegReset(&(pParm->pRecent[regType - cPmRecPer]));
        }
    
    /* All registers */
    else if (regType == cPmAllReg)
        {
        mPmRegReset(&(pParm->curPer));
        mPmRegReset(&(pParm->prePer));
        mPmRegReset(&(pParm->curPerNegAdj));
        mPmRegReset(&(pParm->prePerNegAdj));
        mPmRegReset(&(pParm->curPerPosAdj));
        mPmRegReset(&(pParm->prePerPosAdj));
        mPmRegReset(&(pParm->curDay));
        mPmRegReset(&(pParm->preDay));
        }
        
    /* Invalid register type */
    else
        {
        return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmRegReset(tSurDev       device, 
                                  eSurChnType   chnType, 
                                  const void   *pChnId, 
                                  byte          parm, 
                                  ePmRegType    regType)

Purpose      : This API is used to reset a channel's parameter's resigter.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmRegReset(tSurDev       device, 
                          eSurChnType   chnType, 
                          const void   *pChnId, 
                          byte          parm, 
                          ePmRegType    regType)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Get regsiter */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineRegReset(device, (tSurLineId *)pChnId, parm, regType);
            break;
           
        /* STS path */
        case cSurSts:
            ret = PmStsRegReset(device, (tSurStsId *)pChnId, parm, regType);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtRegReset(device, (tSurVtId *)pChnId, parm, regType);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuRegReset(device, (tSurAuId *)pChnId, parm, regType);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3RegReset(device, (tSurTu3Id *)pChnId, parm, regType);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuRegReset(device, (tSurTuId *)pChnId, parm, regType);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3RegReset(device, (tSurDe3Id *)pChnId, parm, regType);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1RegReset(device, (tSurDe1Id *)pChnId, parm, regType);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkRegReset(device, (tSurImaLinkId *)pChnId, parm, regType);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpRegReset(device, (tSurImaGrpId *)pChnId, parm, regType);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwRegReset(device, (tSurPwId *)pChnId, parm, regType);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmRegGet(tSurDev         device, 
                                eSurChnType     chnType, 
                                const void     *pChnId, 
                                byte            parm, 
                                ePmRegType      regType, 
                                tPmReg         *pRetReg)

Purpose      : This API is used to get a channel's parameter's register's value.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm
               regType               - register type       

Outputs      : pRetReg               - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
public eSurRet PmRegGet(tSurDev         device, 
                        eSurChnType     chnType, 
                        const void     *pChnId, 
                        byte            parm, 
                        ePmRegType      regType, 
                        tPmReg         *pRetReg)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Get regsiter */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineRegGet(device, (tSurLineId *)pChnId, parm, regType, pRetReg);
            break;
           
        /* STS path */
        case cSurSts:
            ret = PmStsRegGet(device, (tSurStsId *)pChnId, parm, regType, pRetReg);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtRegGet(device, (tSurVtId *)pChnId, parm, regType, pRetReg);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuRegGet(device, (tSurAuId *)pChnId, parm, regType, pRetReg);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3RegGet(device, (tSurTu3Id *)pChnId, parm, regType, pRetReg);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuRegGet(device, (tSurTuId *)pChnId, parm, regType, pRetReg);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3RegGet(device, (tSurDe3Id *)pChnId, parm, regType, pRetReg);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1RegGet(device, (tSurDe1Id *)pChnId, parm, regType, pRetReg);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkRegGet(device, (tSurImaLinkId *)pChnId, parm, regType, pRetReg);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpRegGet(device, (tSurImaGrpId *)pChnId, parm, regType, pRetReg);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwRegGet(device, (tSurPwId *)pChnId, parm, regType, pRetReg);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }                        

/*------------------------------------------------------------------------------
Prototype    : const tSortList PmTcaHisGet(tSurDev           device, 
                                           eSurChnType       chnType, 
                                           const void       *pChnId) 

Purpose      : This API is used to get a channel's TCA history.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if ffail
------------------------------------------------------------------------------*/
public tSortList PmTcaHisGet(tSurDev           device,
                                   eSurChnType       chnType, 
                                   const void       *pChnId)
    {
    /* Get TCA history */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            return PmLineTcaHisGet(device, (tSurLineId *)pChnId);
            break;
            
        /* STS path */
        case cSurSts:
            return PmStsTcaHisGet(device, (tSurStsId *)pChnId);
            break;
            
        /* VT path */
        case cSurVt:
            return PmVtTcaHisGet(device, (tSurVtId *)pChnId);
            break;
            
        /* AU-n path */
        case cSurAu:
            return PmAuTcaHisGet(device, (tSurAuId *)pChnId);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            return PmTu3TcaHisGet(device, (tSurTu3Id *)pChnId);
            break;
            
        /* TU path */
        case cSurTu:
            return PmTuTcaHisGet(device, (tSurTuId *)pChnId);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            return PmDe3TcaHisGet(device, (tSurDe3Id *)pChnId);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            return PmDe1TcaHisGet(device, (tSurDe1Id *)pChnId);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            return PmImaLinkTcaHisGet(device, (tSurImaLinkId *)pChnId);
            break;

        /* IMA Group */
        case cSurImaGrp:
            return PmImaGrpTcaHisGet(device, (tSurImaGrpId *)pChnId);
            break;

        /* PW */
        case cSurPw:
            return PmPwTcaHisGet(device, (tSurPwId *)pChnId);
            break;

        /* Invalid channel type */
        default:
            return null;
        }
        
    return null;
    } 

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTcaHisClear(tSurDev      device, 
                                     eSurChnType  chnType, 
                                     const void  *pChnId)

Purpose      : This API is used to clear a channel's TCA history.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTcaHisClear(tSurDev device, eSurChnType chnType, const void *pChnId)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Clear TCA history */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineTcaHisClear(device, (tSurLineId *)pChnId);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsTcaHisClear(device, (tSurStsId *)pChnId);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtTcaHisClear(device, (tSurVtId *)pChnId);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuTcaHisClear(device, (tSurAuId *)pChnId);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3TcaHisClear(device, (tSurTu3Id *)pChnId);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuTcaHisClear(device, (tSurTuId *)pChnId);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3TcaHisClear(device, (tSurDe3Id *)pChnId);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1TcaHisClear(device, (tSurDe1Id *)pChnId);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkTcaHisClear(device, (tSurImaLinkId *)pChnId);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpTcaHisClear(device, (tSurImaGrpId *)pChnId);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwTcaHisClear(device, (tSurPwId *)pChnId);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmParmAccInhSet(tSurDev      device, 
                                       eSurChnType  chnType, 
                                       const void  *pChnId, 
                                       byte         parm, 
                                       bool         inhibit)

Purpose      : This API is used to enable/disable a channel's accumlate inhibition.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm
               inibit                - inhibittion enable/disable

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmParmAccInhSet(tSurDev      device, 
                               eSurChnType  chnType, 
                               const void  *pChnId, 
                               byte         parm, 
                               bool         inhibit)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Set accumulate inhibition */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineParmAccInhSet(device, (tSurLineId *)pChnId, parm, inhibit);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsParmAccInhSet(device, (tSurStsId *)pChnId, parm, inhibit);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtParmAccInhSet(device, (tSurVtId *)pChnId, parm, inhibit);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuParmAccInhSet(device, (tSurAuId *)pChnId, parm, inhibit);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3ParmAccInhSet(device, (tSurTu3Id *)pChnId, parm, inhibit);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuParmAccInhSet(device, (tSurTuId *)pChnId, parm, inhibit);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3ParmAccInhSet(device, (tSurDe3Id *)pChnId, parm, inhibit);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1ParmAccInhSet(device, (tSurDe1Id *)pChnId, parm, inhibit);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkParmAccInhSet(device, (tSurImaLinkId *)pChnId, parm, inhibit);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpParmAccInhSet(device, (tSurImaGrpId *)pChnId, parm, inhibit);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwParmAccInhSet(device, (tSurPwId *)pChnId, parm, inhibit);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmParmAccInhGet(tSurDev      device, 
                                       eSurChnType  chnType, 
                                       const void  *pChnId, 
                                       byte         parm, 
                                       bool        *pInhibit)

Purpose      : This API is used to get a channel's accumulate inhibition mode.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmParmAccInhGet(tSurDev      device, 
                               eSurChnType  chnType, 
                               const void  *pChnId, 
                               byte         parm, 
                               bool        *pInhibit)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Get accumulate inhibition */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineParmAccInhGet(device, (tSurLineId *)pChnId, parm, pInhibit);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsParmAccInhGet(device, (tSurStsId *)pChnId, parm, pInhibit);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtParmAccInhGet(device, (tSurVtId *)pChnId, parm, pInhibit);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuParmAccInhGet(device, (tSurAuId *)pChnId, parm, pInhibit);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3ParmAccInhGet(device, (tSurTu3Id *)pChnId, parm, pInhibit);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuParmAccInhGet(device, (tSurTuId *)pChnId, parm, pInhibit);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3ParmAccInhGet(device, (tSurDe3Id *)pChnId, parm, pInhibit);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1ParmAccInhGet(device, (tSurDe1Id *)pChnId, parm, pInhibit);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkParmAccInhGet(device, (tSurImaLinkId *)pChnId, parm, pInhibit);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpParmAccInhGet(device, (tSurImaGrpId *)pChnId, parm, pInhibit);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwParmAccInhGet(device, (tSurPwId *)pChnId, parm, pInhibit);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }                                            

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmRegThresSet(tSurDev        device, 
                                     eSurChnType    chnType, 
                                     const void    *pChnId, 
                                     byte           parm, 
                                     ePmRegType     regType, 
                                     dword          thresVal)

Purpose      : This API is used to set threshold for a channel's paramter's
               register.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                         + tSurPwParm
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmRegThresSet(tSurDev        device, 
                             eSurChnType    chnType, 
                             const void    *pChnId, 
                             byte           parm, 
                             ePmRegType     regType, 
                             dword          thresVal)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Set register's threshold */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineRegThresSet(device, 
                                   (tSurLineId *)pChnId, 
                                   parm, 
                                   regType, 
                                   thresVal);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsRegThresSet(device, 
                                   (tSurStsId *)pChnId, 
                                   parm, 
                                   regType, 
                                   thresVal);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtRegThresSet(device, 
                                  (tSurVtId *)pChnId, 
                                  parm, 
                                  regType, 
                                  thresVal);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuRegThresSet(device, 
                                  (tSurAuId *)pChnId, 
                                  parm, 
                                  regType, 
                                  thresVal);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3RegThresSet(device, 
                                   (tSurTu3Id *)pChnId, 
                                   parm, 
                                   regType, 
                                   thresVal);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuRegThresSet(device, 
                                  (tSurTuId *)pChnId, 
                                  parm, 
                                  regType, 
                                  thresVal);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3RegThresSet(device, 
                                   (tSurDe3Id *)pChnId, 
                                   parm, 
                                   regType, 
                                   thresVal);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1RegThresSet(device, 
                                   (tSurDe1Id *)pChnId, 
                                   parm, 
                                   regType, 
                                   thresVal);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkRegThresSet(device,
									   (tSurImaLinkId *)pChnId,
									   parm,
									   regType,
									   thresVal);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpRegThresSet(device,
									   (tSurImaGrpId *)pChnId,
									   parm,
									   regType,
									   thresVal);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwRegThresSet(device,
								   (tSurPwId *)pChnId,
								   parm,
								   regType,
								   thresVal);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmRegThresGet(tSurDev        device, 
                                     eSurChnType    chnType, 
                                     const void    *pChnId, 
                                     byte           parm, 
                                     ePmRegType     regType, 
                                     dword         *pThresVal)

Purpose      : This API is used to get threshold of a channel's paramter's
               register.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm
               regType               - register type. the only valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmRegThresGet(tSurDev        device, 
                             eSurChnType    chnType, 
                             const void    *pChnId, 
                             byte           parm, 
                             ePmRegType     regType, 
                             dword         *pThresVal)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Get register's threshold */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineRegThresGet(device, 
                                   (tSurLineId *)pChnId, 
                                   parm, 
                                   regType, 
                                   pThresVal);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsRegThresGet(device, 
                                   (tSurStsId *)pChnId, 
                                   parm, 
                                   regType, 
                                   pThresVal);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtRegThresGet(device, 
                                  (tSurVtId *)pChnId, 
                                  parm, 
                                  regType, 
                                  pThresVal);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuRegThresGet(device, 
                                  (tSurAuId *)pChnId, 
                                  parm, 
                                  regType, 
                                  pThresVal);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3RegThresGet(device, 
                                   (tSurTu3Id *)pChnId, 
                                   parm, 
                                   regType, 
                                   pThresVal);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuRegThresGet(device, 
                                  (tSurTuId *)pChnId, 
                                  parm, 
                                  regType, 
                                  pThresVal);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3RegThresGet(device, 
                                   (tSurDe3Id *)pChnId, 
                                   parm, 
                                   regType, 
                                   pThresVal);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1RegThresGet(device, 
                                   (tSurDe1Id *)pChnId, 
                                   parm, 
                                   regType, 
                                   pThresVal);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkRegThresGet(device,
									   (tSurImaLinkId *)pChnId,
									   parm,
									   regType,
									   pThresVal);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpRegThresGet(device,
									   (tSurImaGrpId *)pChnId,
									   parm,
									   regType,
									   pThresVal);
            break;

        /* IMA Group */
        case cSurPw:
            ret = PmPwRegThresGet(device,
								   (tSurPwId *)pChnId,
								   parm,
								   regType,
								   pThresVal);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }
                                                                
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmParmNumRecRegSet(tSurDev       device, 
                                          eSurChnType   chnType, 
                                          const void   *pChnId, 
                                          byte          parm, 
                                          byte          numRecReg)

Purpose      : This API is used to set a channel's parameter's number of recent
               registers.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmParmNumRecRegSet(tSurDev       device, 
                                  eSurChnType   chnType, 
                                  const void   *pChnId, 
                                  byte          parm, 
                                  byte          numRecReg)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Set parameter's number of recent registers */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineParmNumRecRegSet(device, (tSurLineId *)pChnId, parm, numRecReg);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsParmNumRecRegSet(device, (tSurStsId *)pChnId, parm, numRecReg);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtParmNumRecRegSet(device, (tSurVtId *)pChnId, parm, numRecReg);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuParmNumRecRegSet(device, (tSurAuId *)pChnId, parm, numRecReg);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3ParmNumRecRegSet(device, (tSurTu3Id *)pChnId, parm, numRecReg);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuParmNumRecRegSet(device, (tSurTuId *)pChnId, parm, numRecReg);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3ParmNumRecRegSet(device, (tSurDe3Id *)pChnId, parm, numRecReg);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1ParmNumRecRegSet(device, (tSurDe1Id *)pChnId, parm, numRecReg);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkParmNumRecRegSet(device, (tSurImaLinkId *)pChnId, parm, numRecReg);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpParmNumRecRegSet(device, (tSurImaGrpId *)pChnId, parm, numRecReg);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwParmNumRecRegSet(device, (tSurPwId *)pChnId, parm, numRecReg);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmParmNumRecRegGet(tSurDev       device, 
                                          eSurChnType   chnType, 
                                          const void   *pChnId, 
                                          byte          parm, 
                                          byte         *pNumRecReg)

Purpose      : This API is used to get a channel's parameter's number of recent
               registers.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               parm                  - parameter type. Depend on channel type,
                                     parameter type can be 1 of the following:
                                        + tSurApslParm
                                        + tSurLineParm
                                        + tSurStsParm
                                        + tSurVtParm
                                        + tSurDe3parm
                                        + tSurDe1Parm
                                        + tSurImaLinkParm
                                        + tSurImaGrpParm
                                        + tSurPwParm

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmParmNumRecRegGet(tSurDev       device, 
                                  eSurChnType   chnType, 
                                  const void   *pChnId, 
                                  byte          parm, 
                                  byte         *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Get parameter's number of recent registers */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
            ret = PmLineParmNumRecRegGet(device, (tSurLineId *)pChnId, parm, pNumRecReg);
            break;
            
        /* STS path */
        case cSurSts:
            ret = PmStsParmNumRecRegGet(device, (tSurStsId *)pChnId, parm, pNumRecReg);
            break;
            
        /* VT path */
        case cSurVt:
            ret = PmVtParmNumRecRegGet(device, (tSurVtId *)pChnId, parm, pNumRecReg);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = PmAuParmNumRecRegGet(device, (tSurAuId *)pChnId, parm, pNumRecReg);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = PmTu3ParmNumRecRegGet(device, (tSurTu3Id *)pChnId, parm, pNumRecReg);
            break;
            
        /* TU path */
        case cSurTu:
            ret = PmTuParmNumRecRegGet(device, (tSurTuId *)pChnId, parm, pNumRecReg);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = PmDe3ParmNumRecRegGet(device, (tSurDe3Id *)pChnId, parm, pNumRecReg);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = PmDe1ParmNumRecRegGet(device, (tSurDe1Id *)pChnId, parm, pNumRecReg);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = PmImaLinkParmNumRecRegGet(device, (tSurImaLinkId *)pChnId, parm, pNumRecReg);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = PmImaGrpParmNumRecRegGet(device, (tSurImaGrpId *)pChnId, parm, pNumRecReg);
            break;

        /* PW */
        case cSurPw:
            ret = PmPwParmNumRecRegGet(device, (tSurPwId *)pChnId, parm, pNumRecReg);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : public eSurRet PmTimeStampGet(tSurDev        device,
											  eSurChnType   chnType,
											  const void    *pChnId,
											  ePmRegType    timeType,
											  dword         *startTime,
											  dword         *endTime)

Purpose      : This API is used to get a time stamp of performance parameters

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               timeType              - The type of time stamp
										+ Current 15 minutes
									    + Previous 15 minutes
									    + Current day
									    + Previous day

Outputs      : startTime             - Start time
			   endTime				 - End time

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTimeStampGet(tSurDev       device,
							  eSurChnType   chnType,
							  const void    *pChnId,
							  ePmRegType    timeType,
							  dword         *startTime,
							  dword         *endTime)
    {
    /* Declare local variables */
    eSurRet     ret;

    ret = cSurOk;
    *startTime = 0;
    *endTime = 0;
    /* Get parameter's number of recent registers */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
        	{
            tSurLineInfo   *pInfo;

            mSurLineInfo(mDevDb(device), (tSurLineId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
        	}
            break;

        /* STS path */
        case cSurSts:
            {
            tSurStsInfo    *pInfo;

            pInfo = NULL;
            /* Get info */
			mSurStsInfo(mDevDb(device), (tSurStsId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
        	}
            break;

        /* VT path */
        case cSurVt:
        	{
            tSurVtInfo     *pInfo;

            pInfo = NULL;
            /* Get info */
			mSurVtInfo(mDevDb(device), (tSurVtId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
        	}
            break;

        /* AU-n path */
        case cSurAu:
            {
            tSurStsId   stsId;
            tSurStsInfo *pInfo;

            pInfo = NULL;

            SurAu2Sts((tSurAuId *)pChnId, &stsId, null);

            /* Get info */
			mSurStsInfo(mDevDb(device), &stsId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* Tu-3 path */
        case cSurTu3:
        	{
            tSurStsInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurTu3Info(mDevDb(device), (tSurTu3Id *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* TU path */
        case cSurTu:
            {
			tSurVtId vtId;
			tSurVtInfo *pInfo;

			pInfo = NULL;

			/* Convert to STS ID */
			SurTu2Vt((tSurTuId *)pChnId, &vtId);

			/* Get info */
			mSurVtInfo(mDevDb(device), &vtId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* DS3/E3 path */
        case cSurDe3:
            {
            tSurDe3Info *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurDe3Info(mDevDb(device), (tSurDe3Id *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* DS1/E1 path */
        case cSurDe1:
            {
            tSurDe1Info *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurDe1Info(mDevDb(device), (tSurDe1Id *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* IMA Link */
        case cSurImaLink:
            {
            tSurImaLinkInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurImaLinkInfo(mDevDb(device), (tSurImaLinkId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* IMA Group */
        case cSurImaGrp:
            {
            tSurImaGrpInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurImaGrpInfo(mDevDb(device), (tSurImaGrpId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* PW */
        case cSurPw:
            {
            tSurPwInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurPwInfo(mDevDb(device), (tSurPwId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			if (pInfo->status.state == cSurStart)
				{
				mPmTimeStampGet(timeType, pInfo, startTime, endTime, ret);
				}
            }
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }

    return ret;
    }                                      

/*------------------------------------------------------------------------------
Prototype    : public eSurRet PmTotalTimeGet(tSurDev       device,
											  eSurChnType   chnType,
											  const void    *pChnId,
											  dword         *totalTime)

Purpose      : This API is used to get total time from starting monitoring Performance in seconds

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID

Outputs      : totalTime             - Total time from starting monitoring Performance

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTotalTimeGet(tSurDev       device,
							  eSurChnType   chnType,
							  const void    *pChnId,
							  dword         *totalTime)
    {
    /* Declare local variables */
    eSurRet     ret;

    ret = cSurOk;
    /* Get parameter's number of recent registers */
    switch (chnType)
        {
        /* SONET/SDH line */
        case cSurLine:
        	{
            tSurLineInfo   *pInfo;

            mSurLineInfo(mDevDb(device), (tSurLineId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
			}
            break;

        /* STS path */
        case cSurSts:
            {
            tSurStsInfo    *pInfo;

            pInfo = NULL;
            /* Get info */
			mSurStsInfo(mDevDb(device), (tSurStsId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
        	}
            break;

        /* VT path */
        case cSurVt:
        	{
            tSurVtInfo     *pInfo;

            pInfo = NULL;
            /* Get info */
			mSurVtInfo(mDevDb(device), (tSurVtId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
        	}
            break;

        /* AU-n path */
        case cSurAu:
            {
            tSurStsId   stsId;
            tSurStsInfo *pInfo;

            pInfo = NULL;

            SurAu2Sts((tSurAuId *)pChnId, &stsId, null);

            /* Get info */
			mSurStsInfo(mDevDb(device), &stsId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* Tu-3 path */
        case cSurTu3:
        	{
            tSurStsInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurTu3Info(mDevDb(device), (tSurTu3Id *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* TU path */
        case cSurTu:
            {
			tSurVtId vtId;
			tSurVtInfo *pInfo;

			pInfo = NULL;

			/* Convert to STS ID */
			SurTu2Vt((tSurTuId *)pChnId, &vtId);

			/* Get info */
			mSurVtInfo(mDevDb(device), &vtId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* DS3/E3 path */
        case cSurDe3:
            {
            tSurDe3Info *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurDe3Info(mDevDb(device), (tSurDe3Id *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* DS1/E1 path */
        case cSurDe1:
            {
            tSurDe1Info *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurDe1Info(mDevDb(device), (tSurDe1Id *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* IMA Link */
        case cSurImaLink:
            {
            tSurImaLinkInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurImaLinkInfo(mDevDb(device), (tSurImaLinkId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* IMA Group */
        case cSurImaGrp:
            {
            tSurImaGrpInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurImaGrpInfo(mDevDb(device), (tSurImaGrpId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* PW */
        case cSurPw:
            {
            tSurPwInfo *pInfo;

            pInfo = NULL;

            /* Get info */
			mSurPwInfo(mDevDb(device), (tSurPwId *)pChnId, pInfo);
			if (mIsNull(pInfo))
				{
				return cSurErrNoSuchChn;
				}

			*totalTime = dCurTime - pInfo->status.start;
            }
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }

    return ret;
    }

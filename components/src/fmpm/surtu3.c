/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - TU3
 *
 * File        : surtu3.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of TU-3
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
This function is used to set default values for STS path's PM parameters.
------------------------------------------------------------------------------*/
extern eSurRet SurStsDefParmSet(tSurStsInfo *pInfo);

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTu3FromStsGet(const tSurStsId *pStsId, 
                                               tSurTu3Id       *pTu3Id)

Purpose      : This function is used to convert STS to TU3

Inputs       : pStsId                - STS ID
               
Outputs      : pTu3Id                - TU3 ID

Return       : Flat number
------------------------------------------------------------------------------*/
friend eSurRet SurTu3FromStsGet(const tSurStsId *pStsId, tSurTu3Id *pTu3Id)
    {
    mSurDebug(cSurWarnMsg, "Not implement yet\n");
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend word SurTu3IdFlat(const tSurTu3Id *pTu3Id)

Purpose      : This function is used to get flat number of a TU-3

Inputs       : pTu3Id                 - TU-3 ID

Outputs      : None

Return       : Flat number
------------------------------------------------------------------------------*/
friend word SurTu3IdFlat(const tSurTu3Id *pTu3Id)
    {
    word wRes;
    
    wRes = 0;
    
    /* TU3 in AU-3 */
    if (pTu3Id->au.au > 0) wRes = SurAuIdFlat(&(pTu3Id->au));

    /* TU3 in AU-4 */
    else if ((pTu3Id->au.aug1 > 0) && (pTu3Id->tu3 > 0))
        wRes = SurAuIdFlat(&(pTu3Id->au)) + (pTu3Id->tu3 - 1);

    /* Invalid */
    else
        wRes = cSurInvlVal;
	
    return wRes;
    }

/*------------------------------------------------------------------------------
Prototype    : private tSurTu3Info *Tu3InfoCreate(tSurInfo  *pSurInfo, 
                                                  tSurTu3Id *pTu3Id)

Purpose      : This function is used to allocate resources for TU-3

Inputs       : pSurInfo              - database handle of device
               pTu3Id                - TU-3 id

Outputs      : None

Return       : tSurTu3Info*          - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurTu3Info *Tu3InfoCreate(tSurInfo *pSurInfo, tSurTu3Id *pTu3Id)
    {
    tSurTu3Info *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurTu3Info));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurTu3Info), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.cvP));
        mPmParmInit(&(pChnInfo->parm.esP));
        mPmParmInit(&(pChnInfo->parm.sesP));
        mPmParmInit(&(pChnInfo->parm.uasP));
        mPmParmInit(&(pChnInfo->parm.fcP));
        mPmParmInit(&(pChnInfo->parm.ppjcPdet));
        mPmParmInit(&(pChnInfo->parm.npjcPdet));
        mPmParmInit(&(pChnInfo->parm.ppjcPgen));
        mPmParmInit(&(pChnInfo->parm.npjcPgen));
        mPmParmInit(&(pChnInfo->parm.pjcDiffP));
        mPmParmInit(&(pChnInfo->parm.pjcsPdet));
        mPmParmInit(&(pChnInfo->parm.pjcsPgen));
        mPmParmInit(&(pChnInfo->parm.cvPfe));
        mPmParmInit(&(pChnInfo->parm.esPfe));
        mPmParmInit(&(pChnInfo->parm.sesPfe));
        mPmParmInit(&(pChnInfo->parm.uasPfe));
        mPmParmInit(&(pChnInfo->parm.fcPfe));

        /* Add to database */
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pTu3Id->au.line))]->ppStsDb[SurTu3IdFlat(pTu3Id)]->pTu3Info = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : private void Tu3InfoDestroy(tSurInfo   *pSurInfo,
                                           tSurTu3Id *pTu3Id)

Purpose      : This function is used to deallocate resources of TU-3

Inputs       : pSurInfo              - database handle of device
               pTu3Id                - TU-3 id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void Tu3InfoDestroy(tSurInfo *pSurInfo, tSurTu3Id *pTu3Id)
    {
    tSurTu3Info *pInfo;

    pInfo = NULL;
    /* Get channel information */
    mSurTu3Info(pSurInfo, pTu3Id, pInfo);
    if (pInfo != null)
        {
        /* Finalize all parameters */
        mPmParmFinal(&(pInfo->parm.cvP));
        mPmParmFinal(&(pInfo->parm.esP));
        mPmParmFinal(&(pInfo->parm.sesP));
        mPmParmFinal(&(pInfo->parm.uasP));
        mPmParmFinal(&(pInfo->parm.fcP));
        mPmParmFinal(&(pInfo->parm.ppjcPdet));
        mPmParmFinal(&(pInfo->parm.npjcPdet));
        mPmParmFinal(&(pInfo->parm.ppjcPgen));
        mPmParmFinal(&(pInfo->parm.npjcPgen));
        mPmParmFinal(&(pInfo->parm.pjcDiffP));
        mPmParmFinal(&(pInfo->parm.pjcsPdet));
        mPmParmFinal(&(pInfo->parm.pjcsPgen));
        mPmParmFinal(&(pInfo->parm.cvPfe));
        mPmParmFinal(&(pInfo->parm.esPfe));
        mPmParmFinal(&(pInfo->parm.sesPfe));
        mPmParmFinal(&(pInfo->parm.uasPfe));
        mPmParmFinal(&(pInfo->parm.fcPfe));

        /* Destroy failure history and failure history */
        if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
            (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
            }

        /* Destroy other resources */
        OsalMemFree(pInfo);
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pTu3Id->au.line))]->ppStsDb[SurTu3IdFlat(pTu3Id)]->pTu3Info = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTu3Prov(tSurDev device, tSurTu3Id *pTu3Id)

Purpose      : This function is used to provision a TU-3 for FM & PM.

Inputs       : device                - Device
               pTu3Id                - TU-3 identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3Prov(tSurDev device, tSurTu3Id *pTu3Id)
    {
    /* Declare local variables */
    tSurInfo    *pSurInfo;
    tSurTu3Info *pChnInfo;
    tSurStsDb   *pStsDb;
    tSurLineDb  *pLineDb;
    
    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pTu3Id))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Check if TU-3 ID is valid within the line channel */
    pLineDb = mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pTu3Id->au.line))];
    if (SurTu3IdFlat(pTu3Id) >= pLineDb->numSts)
        {
        mSurDebug(cSurErrMsg, "TU-3 D is out of range\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrNoSuchChn;
        }
    
    /* Check for STS DB's existence */
    if (pLineDb->ppStsDb[mSurTu3IdFlat(pTu3Id)] == null)
        {
        /* Allocate memory for STS DB */
        pStsDb = OsalMemAlloc(sizeof(tSurStsDb));
        if (pStsDb != null)
            {
            OsalMemInit(pStsDb, sizeof(tSurStsDb), 0);
            pLineDb->ppStsDb[mSurTu3IdFlat(pTu3Id)] = pStsDb;
            }
        else
            {
            mSurDebug(cSurErrMsg, "Cannot allocate memory for STS database\n");
            if (mDevGive(device) != cSurOk)
                {
                mSurDebug(cSurErrMsg, "Cannot give device\n");
                }
            return cSurErrRsrNoAvai;
            }
        }
    
    /* Get TU-3 information */
    mSurTu3Info(pSurInfo, pTu3Id, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "TU-3 is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for TU-3 */
    pChnInfo = Tu3InfoCreate(pSurInfo, pTu3Id);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for TU-3\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default channel configuration for TU-3 path */
    pChnInfo->conf.rate           = cSurSts1;
    pChnInfo->conf.erdiEn         = false;
    pChnInfo->conf.decSoakTime    = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultTerSoakTime;

    
    /* Set default parameters' values for TU-3 path */
    if (SurStsDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErr;
        }
    
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTu3DeProv(tSurDev device, tSurTu3Id *pTu3Id)

Purpose      : This function is used to de-provision FM & PM of one TU-3. It
               will deallocated all resources.

Inputs       : device                - device
               pTu3Id                - TU-3 identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3DeProv(tSurDev device, tSurTu3Id *pTu3Id)
    {
    tSurInfo *pSurInfo;

    /* Check parameters' validity */
    if (mIsNull(pTu3Id))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of TU-3 */
    Tu3InfoDestroy(pSurInfo, pTu3Id);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3TrblDesgnSet(tSurDev              device, 
                                          eSurTu3Trbl          trblType, 
                                          const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set TU-3 path trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               tSurTrblDesg          - trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3TrblDesgnSet(tSurDev              device, 
                                  eSurTu3Trbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn)
    {
    return SurStsTrblDesgnSet(device, (eSurStsTrbl)trblType, pTrblDesgn);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3TrblDesgnGet(tSurDev              device, 
                                          eSurTu3Trbl          trblType, 
                                          tSurTrblDesgn       *pTrblDesgn)

Purpose      : This function is used to get TU-3 path trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               tSurTrblDesgn         - returned trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3TrblDesgnGet(tSurDev              device, 
                                  eSurTu3Trbl          trblType, 
                                  tSurTrblDesgn       *pTrblDesgn)
    {
    return SurStsTrblDesgnGet(device, (eSurStsTrbl)trblType, pTrblDesgn);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3EngStatSet(tSurDev             device, 
                                        const tSurTu3Id    *pChnId, 
                                        eSurEngStat         stat)

Purpose      : This API is used to set engine state for a TU-3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurTu3EngStatSet(tSurDev             device, 
                                const tSurTu3Id    *pChnId, 
                                eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3EngStatGet(tSurDev             device, 
                                        const tSurTu3Id    *pChnId, 
                                        eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a TU-3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurTu3EngStatGet(tSurDev             device, 
                                const tSurTu3Id    *pChnId, 
                                eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
          
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTu3ChnCfgSet(tSurDev            device,
                                              const tSurTu3Id   *pChnId, 
                                              const tSurStsConf *pChnCfg)

Purpose      : This internal API is use to set TU-3 channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3ChnCfgSet(tSurDev            device,
                               const tSurTu3Id   *pChnId, 
                               const tSurStsConf *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set configuration */
    pInfo->conf.rate   = pChnCfg->rate;
    pInfo->conf.erdiEn = pChnCfg->erdiEn;
	pInfo->conf.decSoakTime = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
	pInfo->conf.terSoakTime = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3ChnCfgGettSurDev     device,
                                       const tSurTu3Id *pChnId, 
                                       tSurStsConf *pChnCfg)
    {
Purpose      : This API is used to set TU-3 channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3ChnCfgGet(tSurDev          device,
                               const tSurTu3Id *pChnId, 
                               tSurStsConf     *pChnCfg)
    {
    /* Declare local variables */
    tSurStsInfo  *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get configuration */
    pChnCfg->rate   = pInfo->conf.rate;
    pChnCfg->erdiEn = pInfo->conf.erdiEn;
    pChnCfg->decSoakTime = pInfo->conf.decSoakTime;
    pChnCfg->terSoakTime = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }        
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3TrblNotfInhSet(tSurDev               device,
                                            const tSurTu3Id       *pChnId, 
                                            eSurTu3Trbl            trblType, 
                                            bool                   inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a TU-3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3TrblNotfInhSet(tSurDev                device,
                                    const tSurTu3Id       *pChnId, 
                                    eSurTu3Trbl            trblType, 
                                    bool                   inhibit)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* STS fault monitoring */
        case cFmStsLop:
            pInfo->msgInh.lopp = inhibit;
            break;
        case cFmStsPlm:
            pInfo->msgInh.plmp = inhibit;
            break;
        case cFmStsUneq:
            pInfo->msgInh.uneqp = inhibit;
            break;
        case cFmStsTim:
            pInfo->msgInh.timp = inhibit;
            break;
        case cFmStsAis:
            pInfo->msgInh.aisp = inhibit;
            break;
        case cFmStsRfi:
        case cFmStsErfiS:
        case cFmStsErfiC:
        case cFmStsErfiP:
            pInfo->msgInh.rfip = inhibit;
            break;
        case cFmStsBerSf:
            pInfo->msgInh.pathBerSf = inhibit;
            break;
        case cFmStsBerSd:
            pInfo->msgInh.pathBerSd = inhibit;
            break;
        case cFmStsAllFail:
            pInfo->msgInh.sts = inhibit;
            break;
                
        /* STS Performance monitoring */
        case cPmStsTca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
        
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3TrblNotfInhGet(tSurDev               device,
                                            const tSurTu3Id      *pChnId, 
                                            eSurTu3Trbl           trblType, 
                                            bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a TU-3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3TrblNotfInhGet(tSurDev               device,
                                    const tSurTu3Id      *pChnId, 
                                    eSurTu3Trbl           trblType, 
                                    bool                 *pInhibit)
    {
    /* Declare local variables */
    tSurStsInfo *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* STS fault monitoring */
        case cFmStsLop:
            *pInhibit = pInfo->msgInh.lopp;
            break;
        case cFmStsPlm:
            *pInhibit = pInfo->msgInh.plmp;
            break;
        case cFmStsUneq:
            *pInhibit = pInfo->msgInh.uneqp;
            break;
        case cFmStsTim:
            *pInhibit = pInfo->msgInh.timp;
            break;
        case cFmStsAis:
            *pInhibit = pInfo->msgInh.aisp;
            break;
        case cFmStsRfi:
        case cFmStsErfiS:
        case cFmStsErfiC:
        case cFmStsErfiP:
            *pInhibit = pInfo->msgInh.rfip;
            break;
        case cFmStsBerSf:
            *pInhibit = pInfo->msgInh.pathBerSf;
            break;
        case cFmStsBerSd:
            *pInhibit = pInfo->msgInh.pathBerSd;
            break;
        case cFmStsAllFail:
            *pInhibit = pInfo->msgInh.sts;
            break;
                
        /* STS Performance monitoring */
        case cPmStsTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }    

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTu3ChnTrblNotfInhInfoGet(tSurDev             device, 
                                                   const tSurTu3Id    *pChnId, 
                                                   tSurStsMsgInh      *pTrblInh

Purpose      : This API is used to get TU-3 channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTu3ChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurTu3Id    *pChnId, 
                                           tSurStsMsgInh      *pTrblInh)
    {
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurStsMsgInh));
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmTu3FailIndGet(tSurDev            device, 
                                       const tSurTu3Id   *pChnId, 
                                       eSurTu3Trbl        trblType, 
                                       tSurFailStat      *pFailStat)

Purpose      : This API is used to get failure indication of a TU-3 channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmTu3FailIndGet(tSurDev            device, 
                               const tSurTu3Id   *pChnId, 
                               eSurTu3Trbl        trblType, 
                               tSurFailStat      *pFailStat)
    {
    /* Declare local variables */
    tSurStsInfo *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmStsLop:
            mSurFailStatCopy(&(pInfo->failure.lopp), pFailStat);
            break;
            
        case cFmStsPlm:
            mSurFailStatCopy(&(pInfo->failure.plmp), pFailStat);
            break;
            
        case cFmStsUneq:
            mSurFailStatCopy(&(pInfo->failure.uneqp), pFailStat);
            break;
            
        case cFmStsTim:
            mSurFailStatCopy(&(pInfo->failure.timp), pFailStat);
            break;
            
        case cFmStsAis:
            mSurFailStatCopy(&(pInfo->failure.aisp), pFailStat);
            break;
            
        case cFmStsRfi:
            mSurFailStatCopy(&(pInfo->failure.rfip), pFailStat);
            break;
        
        case cFmStsErfiS:
            mSurFailStatCopy(&(pInfo->failure.erfipS), pFailStat);
            break;
            
        case cFmStsErfiC:
            mSurFailStatCopy(&(pInfo->failure.erfipC), pFailStat);
            break;
            
        case cFmStsErfiP:
            mSurFailStatCopy(&(pInfo->failure.erfipP), pFailStat);
            break;
            
        case cFmStsBerSf:
            mSurFailStatCopy(&(pInfo->failure.pathBerSf), pFailStat);
            break;
            
        case cFmStsBerSd:
            mSurFailStatCopy(&(pInfo->failure.pathBerSd), pFailStat);
            break;
                
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmTu3FailHisGet(tSurDev         device, 
                                                const tSurTu3Id *pChnId)

Purpose      : This API is used to get a TU-3 channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmTu3FailHisGet(tSurDev         device, 
                                 const tSurTu3Id *pChnId)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmTu3FailHisClear(tSurDev         device, 
                                        const tSurTu3Id *pChnId,
                                        bool             flush)

Purpose      : This API is used to clear a TU-3 channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmTu3FailHisClear(tSurDev device, const tSurTu3Id *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3RegReset(tSurDev       device, 
                                     tSurTu3Id    *pChnId, 
                                     ePmTu3Parm    parm, 
                                     ePmRegType    regType)

Purpose      : This API is used to reset a TU-3 channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTu3RegReset(tSurDev       device, 
                             tSurTu3Id    *pChnId, 
                             ePmTu3Parm    parm, 
                             ePmRegType    regType)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end TU-3 path parameters */
    if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end TU-3 parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end TU-3 path parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end TU-3 parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }      
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3RegGet(tSurDev            device, 
                                   const tSurTu3Id   *pChnId, 
                                   ePmTu3Parm         parm, 
                                   ePmRegType         regType, 
                                   tPmReg            *pRetReg)

Purpose      : This API is used to get a TU-3 channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned register's value

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmTu3RegGet(tSurDev            device, 
                           const tSurTu3Id   *pChnId, 
                           ePmTu3Parm         parm, 
                           ePmRegType         regType, 
                           tPmReg            *pRetReg)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Resetting register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
    
    return cSurOk;
    }
                
/*------------------------------------------------------------------------------
Prototype    : tSortList PmTu3TcaHisGet(tSurDev device, const tSurTu3Id  *pChnId)

Purpose      : This API is used to get a TU-3 channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmTu3TcaHisGet(tSurDev device, const tSurTu3Id  *pChnId)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3TcaHisClear(tSurDev device, const tSurTu3Id *pChnId)

Purpose      : This API is used to clear a TU-3 channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTu3TcaHisClear(tSurDev device, const tSurTu3Id *pChnId)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
            
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmTu3ParmAccInhSet(tSurDev          device, 
                                           const tSurTu3Id *pChnId, 
                                           ePmTu3Parm       parm, 
                                           bool             inhibit)

Purpose      : This API is used to enable/disable a TU-3 channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTu3ParmAccInhSet(tSurDev          device, 
                                  const tSurTu3Id *pChnId, 
                                  ePmTu3Parm       parm, 
                                  bool             inhibit)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end TU-3 parameters */
    if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end near-end TU-3 parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end ATU-3 parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting far-end far-end TU-3 parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }


/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3ParmAccInhGet(tSurDev          device, 
                                          const tSurTu3Id *pChnId, 
                                          ePmTu3Parm       parm, 
                                          bool            *pInhibit)

Purpose      : This API is used to get a TU-3 channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTu3ParmAccInhGet(tSurDev          device, 
                                  const tSurTu3Id *pChnId, 
                                  ePmTu3Parm       parm, 
                                  bool            *pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurStsInfo     *pInfo;
    tPmParm         *pParm;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3RegThresSet(tSurDev           device, 
                                        const tSurTu3Id  *pChnId, 
                                        ePmTu3Parm        parm, 
                                        ePmRegType        regType, 
                                        dword             thresVal)

Purpose      : This API is used to set threshold for a TU-3 channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTu3RegThresSet(tSurDev           device, 
                                const tSurTu3Id  *pChnId, 
                                ePmTu3Parm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end TU-3 parameters */
    else if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmRegThresSet(&(pInfo->parm), 
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end TU-3 parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end TU-3 parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end TU-3 parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3RegThresGet(tSurDev           device, 
                                        const tSurTu3Id  *pChnId, 
                                        ePmTu3Parm        parm, 
                                        ePmRegType        regType, 
                                        dword            *pThresVal)

Purpose      : This API is used to get threshold for a TU-3 channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTu3RegThresGet(tSurDev           device, 
                                const tSurTu3Id  *pChnId, 
                                ePmTu3Parm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurStsInfo     *pInfo;
    tPmParm         *pParm;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3ParmNumRecRegSet(tSurDev          device, 
                                             const tSurTu3Id *pChnId, 
                                             ePmTu3Parm       parm, 
                                             byte             numRecReg)

Purpose      : This API is used to set a TU-3 channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTu3ParmNumRecRegSet(tSurDev          device, 
                                     const tSurTu3Id *pChnId, 
                                     ePmTu3Parm       parm, 
                                     byte             numRecReg)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    mPmRecRegCheck(numRecReg);
    
    /* Initialize */
    pParm = null;
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurTu3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end TU-3 parameters */
    else if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end TU-3 parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end TU-3 parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end TU-3 parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTu3ParmNumRecRegGet(tSurDev          device, 
                                             const tSurTu3Id *pChnId, 
                                             ePmTu3Parm       parm, 
                                             byte            *pNumRecReg)
                                             
Purpose      : This API is used to get a TU-3 channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTu3ParmNumRecRegGet(tSurDev          device, 
                                     const tSurTu3Id *pChnId, 
                                     ePmTu3Parm       parm, 
                                     byte            *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurStsInfo     *pInfo;
    tPmParm         *pParm;

    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurTu3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
                

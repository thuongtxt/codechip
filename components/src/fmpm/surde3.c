/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - DS3/E3
 *
 * File        : surde3.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of DS3/E3
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3DefParmSet(tSurDe3Info *pInfo)

Purpose      : This function is used to set default values for DE3/E3 path's PM 
               parameters.

Inputs       : pInfo                 - line channel info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3DefParmSet(tSurDe3Info *pInfo)
    {
    /* Near-end DS3/E3 line parameters */
    mPmDefParmSet(&(pInfo->parm.cvL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esaL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esbL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.lossL), cPmPerThresVal, cPmDayThresVal);
    
    /* Near-end DS3/E3 path parameters */
    mPmDefParmSet(&(pInfo->parm.cvpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.cvcpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.espP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esapP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esbpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.escpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esacpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esbcpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sespP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sescpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sasP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.aissP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uaspP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uascpP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcP), cPmPerThresVal, cPmDayThresVal);
    
    /* Far-end DS3/E3 path parameters */
    mPmDefParmSet(&(pInfo->parm.cvcpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.escpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esacpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esbcpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sescpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sascpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uascpPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcPfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : private tSurDe3Info *De3InfoCreate(tSurInfo  *pSurInfo, 
                                                  tSurDe3Id *pDe3Id)

Purpose      : This function is used to allocate resources for DS3/E3

Inputs       : pSurInfo              - database handle of device
               pDe3Id                - DS3/E3 id

Outputs      : None

Return       : tSurDe3Info*          - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurDe3Info *De3InfoCreate(tSurInfo *pSurInfo, tSurDe3Id *pDe3Id)
    {
    tSurDe3Info *pChnInfo;
    
    /* Check DS3/E3 ID */
    if (*pDe3Id > cSurMaxDe3Line)
        {
        return null;
        }
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurDe3Info));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurDe3Info), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.cvL));
        mPmParmInit(&(pChnInfo->parm.esL));
        mPmParmInit(&(pChnInfo->parm.esaL));
        mPmParmInit(&(pChnInfo->parm.esbL));
        mPmParmInit(&(pChnInfo->parm.sesL));
        mPmParmInit(&(pChnInfo->parm.lossL));
        mPmParmInit(&(pChnInfo->parm.cvpP));
        mPmParmInit(&(pChnInfo->parm.cvcpP));
        mPmParmInit(&(pChnInfo->parm.espP));
        mPmParmInit(&(pChnInfo->parm.esapP));
        mPmParmInit(&(pChnInfo->parm.esbpP));
        mPmParmInit(&(pChnInfo->parm.escpP));
        mPmParmInit(&(pChnInfo->parm.esacpP));
        mPmParmInit(&(pChnInfo->parm.esbcpP));
        mPmParmInit(&(pChnInfo->parm.sespP));
        mPmParmInit(&(pChnInfo->parm.sescpP));
        mPmParmInit(&(pChnInfo->parm.sasP));
        mPmParmInit(&(pChnInfo->parm.aissP));
        mPmParmInit(&(pChnInfo->parm.uaspP));
        mPmParmInit(&(pChnInfo->parm.uascpP));
        mPmParmInit(&(pChnInfo->parm.fcP));
        mPmParmInit(&(pChnInfo->parm.cvcpPfe));
        mPmParmInit(&(pChnInfo->parm.escpPfe));
        mPmParmInit(&(pChnInfo->parm.esacpPfe));
        mPmParmInit(&(pChnInfo->parm.esbcpPfe));
        mPmParmInit(&(pChnInfo->parm.sescpPfe));
        mPmParmInit(&(pChnInfo->parm.sascpPfe));
        mPmParmInit(&(pChnInfo->parm.uascpPfe));
        mPmParmInit(&(pChnInfo->parm.fcPfe));

        /* Add to database */
        mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(pDe3Id)]->pInfo = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurDe3InfoFinal(tSurDe3Info *pInfo)

Purpose      : This function is used to deallocate all resource in DS3/E3 
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurDe3InfoFinal(tSurDe3Info *pInfo)
    {
    /* Finalize all parameters */
    mPmParmFinal(&(pInfo->parm.cvL));
    mPmParmFinal(&(pInfo->parm.esL));
    mPmParmFinal(&(pInfo->parm.esaL));
    mPmParmFinal(&(pInfo->parm.esbL));
    mPmParmFinal(&(pInfo->parm.sesL));
    mPmParmFinal(&(pInfo->parm.lossL));
    mPmParmFinal(&(pInfo->parm.cvpP));
    mPmParmFinal(&(pInfo->parm.cvcpP));
    mPmParmFinal(&(pInfo->parm.espP));
    mPmParmFinal(&(pInfo->parm.esapP));
    mPmParmFinal(&(pInfo->parm.esbpP));
    mPmParmFinal(&(pInfo->parm.escpP));
    mPmParmFinal(&(pInfo->parm.esacpP));
    mPmParmFinal(&(pInfo->parm.esbcpP));
    mPmParmFinal(&(pInfo->parm.sespP));
    mPmParmFinal(&(pInfo->parm.sescpP));
    mPmParmFinal(&(pInfo->parm.sasP));
    mPmParmFinal(&(pInfo->parm.aissP));
    mPmParmFinal(&(pInfo->parm.uaspP));
    mPmParmFinal(&(pInfo->parm.uascpP));
    mPmParmFinal(&(pInfo->parm.fcP));
    mPmParmFinal(&(pInfo->parm.cvcpPfe));
    mPmParmFinal(&(pInfo->parm.escpPfe));
    mPmParmFinal(&(pInfo->parm.esacpPfe));
    mPmParmFinal(&(pInfo->parm.esbcpPfe));
    mPmParmFinal(&(pInfo->parm.sescpPfe));
    mPmParmFinal(&(pInfo->parm.sascpPfe));
    mPmParmFinal(&(pInfo->parm.uascpPfe));
    mPmParmFinal(&(pInfo->parm.fcPfe));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De3InfoDestroy(tSurInfo   *pSurInfo,
                                           tSurDe3Id *pDe3Id)

Purpose      : This function is used to deallocate resources of DS3/E3

Inputs       : pSurInfo              - database handle of device
               pDe3Id                - DS3/E3 id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void De3InfoDestroy(tSurInfo *pSurInfo, tSurDe3Id *pDe3Id)
    {
    tSurDe3Info *pInfo;

    /* Get channel information */
    mSurDe3Info(pSurInfo, pDe3Id, pInfo);
    if (pInfo != null)
        {
        SurDe3InfoFinal(pInfo);

        /* Destroy other resources */
        OsalMemFree(pInfo);
        mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(pDe3Id)]->pInfo = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet De3ParmGet(tSurDe3Parm   *pDe3Parm, 
                                  ePmDe3Parm     parm, 
                                  tPmParm       **ppParm)

Purpose      : This functions is used to get a pointer to a DS3/E3 parameter.

Inputs       : pDe3Parm              - pointer to the DS3/E3 parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet De3ParmGet(tSurDe3Parm *pDe3Parm, ePmDe3Parm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pDe3Parm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Near-end DS3/E3 line CV */
        case cPmDe3CvL:
            *ppParm = &(pDe3Parm->cvL);
            break;
        
        /* Near-end DS3/E3 line ES */
        case cPmDe3EsL:
            *ppParm = &(pDe3Parm->esL);
            break;
        
        /* Near-end DS3/E3 line ESL type A */
        case cPmDe3EsaL:
            *ppParm = &(pDe3Parm->esaL);
            break;
        
        /* Near-end DS3/E3 line ESL type B */
        case cPmDe3EsbL:
            *ppParm = &(pDe3Parm->esbL);
            break;
        
        /* Near-end DS3/E3 line SES */
        case cPmDe3SesL:
            *ppParm = &(pDe3Parm->sesL);
            break;
        
        /* Near-end DS3/E3 line LOSS */
        case cPmDe3LossL:
            *ppParm = &(pDe3Parm->lossL);
            break;
        
        /* Near-end DS3/E3 path CVP */
        case cPmDe3CvpP:
            *ppParm = &(pDe3Parm->cvpP);
            break;
        
        /* Near-end DS3/E3 path CVCP */
        case cPmDe3CvcpP:
            *ppParm = &(pDe3Parm->cvcpP);
            break;
        
        /* Near-end DS3/E3 path ESP */
        case cPmDe3EspP:
            *ppParm = &(pDe3Parm->espP);
            break;
        
        /* Near-end DS3/E3 path ES P-bit type A */
        case cPmDe3EsapP:
            *ppParm = &(pDe3Parm->esapP);
            break;
        
        /* Near-end DS3/E3 path ES P-bit type B */
        case cPmDe3EsbpP:
            *ppParm = &(pDe3Parm->esbpP);
            break;
        
        /* Near-end DS3/E3 path ESCP */
        case cPmDe3EscpP:
            *ppParm = &(pDe3Parm->escpP);
            break;
        
        /* Near-end DS3/E3 ES CP-bit type A */
        case cPmDe3EsacpP:
            *ppParm = &(pDe3Parm->esacpP);
            break;
        
        /* Near-end /DS3/E3 ES CP-bit type B */
        case cPmDe3EsbcpP:
            *ppParm = &(pDe3Parm->esbcpP);
            break;
        
        /* Near-end DS3/E3 path SESP */
        case cPmDe3SespP:
            *ppParm = &(pDe3Parm->sespP);
            break;
        
        /* Near-end DS3/E3 path SESCP */
        case cPmDe3SescpP:
            *ppParm = &(pDe3Parm->sescpP);
            break;
        
        /* Near-end DS3/E3 path SAS */
        case cPmDe3SasP:
            *ppParm = &(pDe3Parm->sasP);
            break;
        
        /* Near-end DS3/E3 path AISS */
        case cPmDe3AissP:
            *ppParm = &(pDe3Parm->aissP);
            break;
        
        /* Near-end DS3/E3 path UASP */
        case cPmDe3UaspP:
            *ppParm = &(pDe3Parm->uaspP);
            break;
        
        /* Near-end DS3/E3 path UASCP */
        case cPmDe3UascpP:
            *ppParm = &(pDe3Parm->uascpP);
            break;
        
        /* Near-end DS3/E3 path FC */
        case cPmDe3FcP:
            *ppParm = &(pDe3Parm->fcP);
            break;
        
        /* Far-end DS3/E3 CV */
        case cPmDe3CvcpPfe:
            *ppParm = &(pDe3Parm->cvcpPfe);
            break;
        
        /* Far-end DS3/E3 ES */
        case cPmDe3EscpPfe:
            *ppParm = &(pDe3Parm->escpPfe);
            break;
        
        /* Far-end DS3/E3 ES type A */
        case cPmDe3EsacpPfe:
            *ppParm = &(pDe3Parm->esacpPfe);
            break;
        
        /* Far-end DS3/E3 ES type B */
        case cPmDe3EsbcpPfe:
            *ppParm = &(pDe3Parm->esbcpPfe);
            break;
        
        /* Far-end DS3/E3 SES */
        case cPmDe3SescpPfe:
            *ppParm = &(pDe3Parm->sescpPfe);
            break;
        
        /* Far-end DS3/E3 SEF/AIS sec */
        case cPmDe3SascpPfe:
            *ppParm = &(pDe3Parm->sascpPfe);
            break;
        
        /* Far-end DS3/E3 UAS */
        case cPmDe3UascpPfe:
            *ppParm = &(pDe3Parm->uascpPfe);
            break;
        
        /* Far-end DS3/E3 FC */
        case cPmDe3FcPfe:
            *ppParm = &(pDe3Parm->fcPfe);
            break;
        
        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe3ParmRegReset(tSurDe3Parm *pDe3Parm, 
                                          ePmRegType   regType,
                                          bool         neParm)

Purpose      : This functions is used to reset registers of a group of DS3/E3 
               parameters.

Inputs       : pDe3Parm              - pointer to the DS3/E3 parameters
               regType               - register type
               neParm                - is near-end  DS3/E3 parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe3ParmRegReset(tSurDe3Parm *pDe3Parm, 
                                  ePmRegType   regType,
                                  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe3Parm   start;
    ePmDe3Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe3Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe3CvL;
        end   = cPmDe3NeAllParm;
        }
    else
        {
        start = cPmDe3CvcpPfe;
        end   = cPmDe3NeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De3ParmGet(pDe3Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe3ParmAccInhSet(tSurDe3Parm *pDe3Parm, 
                                           bool         inhibit, 
                                           bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               DS3/E3 parameters.

Inputs       : pDe3Parm              - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end DS3/E3 path parameters,
                                      false for far-end DS3/E3 path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe3ParmAccInhSet(tSurDe3Parm  *pDe3Parm, 
                                   bool          inhibit, 
                                   bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe3Parm   start;
    ePmDe3Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe3Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe3CvL;
        end   = cPmDe3NeAllParm;
        }
    else
        {
        start = cPmDe3CvcpPfe;
        end   = cPmDe3FeAllParm;
        }
        
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De3ParmGet(pDe3Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe3ParmRegThresSet(tSurDe3Parm  *pDe3Parm, 
                                             ePmRegType    regType,
                                             dword         thresVal, 
                                             bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               DS3/E3 parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end DS3/E3 path parameters, false
                                     for far-end DS3/E3 path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe3ParmRegThresSet(tSurDe3Parm  *pDe3Parm, 
                                     ePmRegType    regType,
                                     dword         thresVal, 
                                     bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe3Parm   start;
    ePmDe3Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe3Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe3NeAllParm;
        end   = cPmDe3FcP;
        }
    else
        {
        start = cPmDe3CvcpPfe;
        end   = cPmDe3FeAllParm;
        }
        
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De3ParmGet(pDe3Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }         

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe3ParmNumRecRegSet(tSurDe3Parm  *pDe3Parm, 
                                              byte          numRecReg, 
                                              bool          neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of DS3/E3 parameters.

Inputs       : pDe3Parm              - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end DS3/E3 path parameters, 
                                     false for far-end DS3/E3 path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe3ParmNumRecRegSet(tSurDe3Parm  *pDe3Parm, 
                                      byte          numRecReg, 
                                      bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe3Parm   start;
    ePmDe3Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe3Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe3CvL;
        end   = cPmDe3NeAllParm;
        }
    else
        {
        start = cPmDe3CvcpPfe;
        end   = cPmDe3FeAllParm;
        }
        
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De3ParmGet(pDe3Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }                                      
                                                                                                                        
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe3Prov(tSurDev device, tSurDe3Id *pDe3Id)

Purpose      : This function is used to provision a DS3/E3 for FM & PM.

Inputs       : device                - Device
               pDe3Id                - DS3/E3 identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3Prov(tSurDev device, tSurDe3Id *pDe3Id)
    {
    tSurInfo    *pSurInfo;
    tSurDe3Info *pChnInfo;

    /* Check parameters' validity */
    if (mIsNull(pDe3Id))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Get DS3/E3 information */
    mSurDe3Info(pSurInfo, pDe3Id, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "DS3/E3 is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for DS3/E3 */
    pChnInfo = De3InfoCreate(pSurInfo, pDe3Id);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for DS3/E3\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
        
    /* Set default configuration for DS3/E3 path */
    pChnInfo->conf.decSoakTime = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime = cSurDefaultTerSoakTime;
    
    
    /* Set default parameters' values for DS3/E3 path */
    if (SurDe3DefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
        
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe3DeProv(tSurDev device, tSurDe3Id *pDe3Id)

Purpose      : This function is used to de-provision FM & PM of one DS3/E3. It
               will deallocated all resources.

Inputs       : device                - device
               pDe3Id                - DS3/E3 identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3DeProv(tSurDev device, tSurDe3Id *pDe3Id)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pDe3Id))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of DS3/E3 */
    De3InfoDestroy(pSurInfo, pDe3Id);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe3TrblDesgnSet(tSurDev              device, 
                                                 eSurDe3Trbl          trblType, 
                                                 const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set DS3/E3 path trouble designation for 
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3TrblDesgnSet(tSurDev              device, 
                                  eSurDe3Trbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Set trouble designation */
    switch (trblType)
        {
        /* DS3/E3 line Loss Of Signal failure */    
        case cFmDe3Los:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Los);
            break;
            
        /* DS3/E3 path Loss Of Frame failure */
        case cFmDe3Lof:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Lof);
            break;
            
        /* DS3/E3 path Alarm Indication Signal failure */
        case cFmDe3Ais:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Ais);
            break;
            
        /* DS3/E3 path Remote Alarm Indication */
        case cFmDe3Rai:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Rai);
            break;
            
        /* L bit failure */
        case cFmDe3Lbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit failure */
        case cFmDe3Rbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit failure */
        case cFmDe3Mbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* DS3/E3 all failures */
        case cFmDe3AllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Los);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Lof);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Ais);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Rai);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));

            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
        
        /* Performance monitoring */
        case cPmDe3Tca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe3TrblDesgnGet(tSurDev        device, 
                                                 eSurDe3Trbl    trblType, 
                                                 tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get DS3/E3 path  trouble designation of 
               a device.

Inputs       : device                ? device
               trblType              ? trouble type
               
Outputs      : pTrblDesgn            ? trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3TrblDesgnGet(tSurDev        device, 
                                  eSurDe3Trbl    trblType, 
                                  tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pSurInfo = mDevDb(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* DS3/E3 line Loss Of Signal failure */    
        case cFmDe3Los:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Los);
            break;
            
        /* DS3/E3 path Loss Of Frame failure */
        case cFmDe3Lof:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Lof);
            break;
            
        /* DS3/E3 path Alarm Indication Signal failure */
        case cFmDe3Ais:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Ais);
            break;
            
        /* DS3/E3 path Remote Alarm Indication */
        case cFmDe3Rai:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de3Rai);
            break;
            
        /* L bit failure */
        case cFmDe3Lbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit failure */
        case cFmDe3Rbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit failure */
        case cFmDe3Mbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* Performance monitoring */
        case cPmDe3Tca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
                
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3EngStatSet(tSurDev             device, 
                                        const tSurDe3Id    *pChnId, 
                                        eSurEngStat         stat)

Purpose      : This API is used to set engine state for a DS3/E3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurDe3EngStatSet(tSurDev             device, 
                                const tSurDe3Id    *pChnId, 
                                eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3EngStatGet(tSurDev             device, 
                                        const tSurDe3Id    *pChnId, 
                                        eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a DS3/E3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurDe3EngStatGet(tSurDev             device, 
                                const tSurDe3Id    *pChnId, 
                                eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe3ChnCfgSet(tSurDev            device,
                                              const tSurDe3Id   *pChnId, 
                                              const tSurDe3Conf *pChnCfg)

Purpose      : This internal API is use to set DS3/E3 channel's configuration.

Inputs       : device                - device
               pChnId                - channel ID
               pChnCfg               - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3ChnCfgSet(tSurDev            device,
                               const tSurDe3Id   *pChnId, 
                               const tSurDe3Conf *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurDe3Info    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set configuration */
	pInfo->conf.decSoakTime = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
	pInfo->conf.terSoakTime = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release decice's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3ChnCfgGet(tSurDev          device,
                                       const tSurDe3Id *pChnId, 
                                       tSurDe3Conf     *pChnCfg)
    {
Purpose      : This API is used to set DS3/E3 channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3ChnCfgGet(tSurDev          device,
                               const tSurDe3Id *pChnId, 
                               tSurDe3Conf     *pChnCfg)
    {
    /* Declare local variables */
    tSurDe3Info  *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get configuration */
    pChnCfg->decSoakTime = pInfo->conf.decSoakTime;
    pChnCfg->terSoakTime = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3TrblNotfInhSet(tSurDev               device,
                                            const tSurDe3Id      *pChnId, 
                                            eSurDe3Trbl           trblType, 
                                            bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a DS3/E3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3TrblNotfInhSet(tSurDev               device,
                                    const tSurDe3Id      *pChnId, 
                                    eSurDe3Trbl           trblType, 
                                    bool                  inhibit)
    {
    /* Declare local variables */
    tSurDe3Info *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* DS3/E3 fault monitoring */
        case cFmDe3Los:
            pInfo->msgInh.los = inhibit;
            break;
        case cFmDe3Lof:
            pInfo->msgInh.lof = inhibit;
            break;
        case cFmDe3Rai:
            pInfo->msgInh.rai = inhibit;
            break;
        case cFmDe3Ais:
            pInfo->msgInh.ais = inhibit;
            break;

        case cFmDe3Lbit:
            pInfo->msgInh.lBit = inhibit;
            break;

        case cFmDe3Rbit:
            pInfo->msgInh.rBit = inhibit;
            break;

        case cFmDe3Mbit:
            pInfo->msgInh.mBit = inhibit;
            break;

        case cFmDe3AllFail:
            pInfo->msgInh.de3 = inhibit;
            break;
        
        /* DS3/E3 performance monitoring */
        case cPmDe3Tca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
  
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3TrblNotfInhGet(tSurDev               device,
                                            const tSurDe3Id      *pChnId, 
                                            eSurDe3Trbl           trblType, 
                                            bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a DS3/E3 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3TrblNotfInhGet(tSurDev               device,
                                    const tSurDe3Id      *pChnId, 
                                    eSurDe3Trbl           trblType, 
                                    bool                 *pInhibit)
    {
    /* Declare local variables */
    tSurDe3Info *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* DS3/E3 fault monitoring */
        case cFmDe3Los:
            *pInhibit = pInfo->msgInh.los;
            break;
        case cFmDe3Lof:
            *pInhibit = pInfo->msgInh.lof;
            break;
        case cFmDe3Rai:
            *pInhibit = pInfo->msgInh.rai;
            break;
        case cFmDe3Ais:
            *pInhibit = pInfo->msgInh.ais;
            break;
        case cFmDe3Lbit:
            *pInhibit = pInfo->msgInh.lBit;
            break;
        case cFmDe3Rbit:
            *pInhibit = pInfo->msgInh.rBit;
            break;
        case cFmDe3Mbit:
            *pInhibit = pInfo->msgInh.mBit;
            break;

        case cFmDe3AllFail:
            *pInhibit = pInfo->msgInh.de3;
            break;
        
        /* DS3/E3 performance monitoring */
        case cPmDe3Tca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe3ChnTrblNotfInhInfoGet(tSurDev             device, 
                                                   const tSurDe3Id    *pChnId, 
                                                   tSurDe3MsgInh      *pTrblInh)

Purpose      : This API is used to get DS3/E3 channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe3ChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurDe3Id    *pChnId, 
                                           tSurDe3MsgInh      *pTrblInh)
    {
    tSurDe3Info    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurDe3MsgInh));
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmDe3FailIndGet(tSurDev            device, 
                                       const tSurDe3Id   *pChnId, 
                                       eSurDe3Trbl        trblType, 
                                       tSurFailStat      *pFailStat)

Purpose      : This API is used to get failure indication of a DS3/E3 channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmDe3FailIndGet(tSurDev            device, 
                               const tSurDe3Id   *pChnId, 
                               eSurDe3Trbl        trblType, 
                               tSurFailStat      *pFailStat)
    {
    /* Declare local variables */
    tSurDe3Info *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmDe3Los:
            mSurFailStatCopy(&(pInfo->failure.los), pFailStat);
            break;
            
        case cFmDe3Lof:
            mSurFailStatCopy(&(pInfo->failure.lof), pFailStat);
            break;
            
        case cFmDe3Rai:
            mSurFailStatCopy(&(pInfo->failure.rai), pFailStat);
            break;
            
        case cFmDe3Ais:
            mSurFailStatCopy(&(pInfo->failure.ais), pFailStat);
            break;
            
        case cFmDe3Lbit:
            mSurFailStatCopy(&(pInfo->failure.lBit), pFailStat);
            break;

        case cFmDe3Rbit:
            mSurFailStatCopy(&(pInfo->failure.rBit), pFailStat);
            break;

        case cFmDe3Mbit:
            mSurFailStatCopy(&(pInfo->failure.mBit), pFailStat);
            break;

        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmDe3FailHisGet(tSurDev          device, 
                                                const tSurDe3Id *pChnId)

Purpose      : This API is used to get a DS3/E3 channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmDe3FailHisGet(tSurDev          device, 
                                 const tSurDe3Id *pChnId)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmDe3FailHisClear(tSurDev          device, 
                                         const tSurDe3Id *pChnId,
                                         bool             flush)

Purpose      : This API is used to clear a DS3/E3 channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmDe3FailHisClear(tSurDev device, const tSurDe3Id *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3RegReset(tSurDev       device, 
                                     tSurDe3Id    *pChnId, 
                                     ePmDe3Parm    parm, 
                                     ePmRegType    regType)
                                     
Purpose      : This API is used to reset a DS3/E3 channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe3RegReset(tSurDev       device, 
                             tSurDe3Id    *pChnId, 
                             ePmDe3Parm    parm, 
                             ePmRegType    regType)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end DS3/E3 path parameters */
    if (parm == cPmDe3NeAllParm)
        {
        ret = AllDe3ParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end DS3/E3 parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end DS3/E3 path parameters */
    else if (parm == cPmDe3FeAllParm)
        {
        ret = AllDe3ParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end DS3/E3 parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3RegGet(tSurDev            device, 
                                   const tSurDe3Id   *pChnId, 
                                   ePmDe3Parm         parm, 
                                   ePmRegType         regType, 
                                   tPmReg            *pRetReg)

Purpose      : This API is used to get a DS3/E3 channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmDe3RegGet(tSurDev            device, 
                           const tSurDe3Id   *pChnId, 
                           ePmDe3Parm         parm, 
                           ePmRegType         regType, 
                           tPmReg            *pRetReg)
    
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
            
    return cSurOk;
    }    
        
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmDe3TcaHisGet(tSurDev device, const tSurDe3Id *pChnId) 

Purpose      : This API is used to get a DS3/E3 channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmDe3TcaHisGet(tSurDev device, const tSurDe3Id *pChnId)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }
     
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3TcaHisClear(tSurDev device, const tSurDe3Id *pChnId)

Purpose      : This API is used to clear a DS3/E3 channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe3TcaHisClear(tSurDev device, const tSurDe3Id *pChnId)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }    
                
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmDe3ParmAccInhSet(tSurDev          device, 
                                           const tSurDe3Id *pChnId, 
                                           ePmDe3Parm       parm, 
                                           bool             inhibit)

Purpose      : This API is used to enable/disable a DS3/E3 channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe3ParmAccInhSet(tSurDev          device, 
                                  const tSurDe3Id *pChnId, 
                                  ePmDe3Parm       parm, 
                                  bool             inhibit)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end DS3/E3 parameters */
    if (parm == cPmDe3NeAllParm)
        {
        ret = AllDe3ParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end DS3/E3 parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end DS3/E3 parameters */
    else if (parm == cPmDe3FeAllParm)
        {
        ret = AllDe3ParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end DS3/E3 parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3ParmAccInhGet(tSurDev          device, 
                                          const tSurDe3Id *pChnId, 
                                          ePmDe3Parm       parm, 
                                          bool            *pInhibit)

Purpose      : This API is used to get a DS3/E3 channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe3ParmAccInhGet(tSurDev          device, 
                                  const tSurDe3Id *pChnId, 
                                  ePmDe3Parm       parm, 
                                  bool            *pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurDe3Info     *pInfo;
    tPmParm         *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }
 
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3RegThresSet(tSurDev           device, 
                                        const tSurDe3Id  *pChnId, 
                                        ePmDe3Parm        parm, 
                                        ePmRegType        regType, 
                                        dword             thresVal)

Purpose      : This API is used to set threshold for a DS3/E3 channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe3RegThresSet(tSurDev           device, 
                                const tSurDe3Id  *pChnId, 
                                ePmDe3Parm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end DS3/E3 parameters */
    else if (parm == cPmDe3NeAllParm)
        {
        ret = AllDe3ParmRegThresSet(&(pInfo->parm), 
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end DS3/E3 parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end DS3/E3 parameters */
    else if (parm == cPmDe3FeAllParm)
        {
        ret = AllDe3ParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end DS3/E3 parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3RegThresGet(tSurDev           device, 
                                        const tSurDe3Id  *pChnId, 
                                        ePmDe3Parm        parm, 
                                        ePmRegType        regType, 
                                        dword            *pThresVal)

Purpose      : This API is used to get threshold for a DS3/E3 channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe3RegThresGet(tSurDev           device, 
                                const tSurDe3Id  *pChnId, 
                                ePmDe3Parm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurDe3Info     *pInfo;
    tPmParm         *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        } 
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    }     
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3ParmNumRecRegSet(tSurDev          device, 
                                             const tSurDe3Id *pChnId, 
                                             ePmDe3Parm       parm, 
                                             byte             numRecReg)

Purpose      : This API is used to set a DS3/E3 channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmDe3ParmNumRecRegSet(tSurDev          device, 
                                     const tSurDe3Id *pChnId, 
                                     ePmDe3Parm       parm, 
                                     byte             numRecReg)
    {
    /* Declare local variables */
    tSurDe3Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    mPmRecRegCheck(numRecReg);
        
    /* Initialize */
    pParm = null;
    
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe3Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end DS3/E3 parameters */
    else if (parm == cPmDe3NeAllParm)
        {
        ret = AllDe3ParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end DS3/E3 parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end DS3/E3 parameters */
    else if (parm == cPmDe3FeAllParm)
        {
        ret = AllDe3ParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end DS3/E3 parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe3ParmNumRecRegGet(tSurDev          device, 
                                             const tSurDe3Id *pChnId, 
                                             ePmDe3Parm       parm, 
                                             byte            *pNumRecReg)
                                             
Purpose      : This API is used to get a DS3/E3 channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmDe3ParmNumRecRegGet(tSurDev          device, 
                                     const tSurDe3Id *pChnId, 
                                     ePmDe3Parm       parm, 
                                     byte            *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurDe3Info     *pInfo;
    tPmParm         *pParm;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurDe3Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De3ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
        

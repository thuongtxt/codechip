/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - STS
 *
 * File        : sursts.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of STS
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsDefParmSet(tSurStsInfo *pInfo)

Purpose      : This function is used to set default values for STS path's PM 
               parameters.

Inputs       : pInfo                 - line channel info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsDefParmSet(tSurStsInfo *pInfo)
    {
    /* Near-end STS path parameters */
    mPmDefParmSet(&(pInfo->parm.cvP), 15, 125);
    mPmDefParmSet(&(pInfo->parm.esP), 12, 100);
    mPmDefParmSet(&(pInfo->parm.sesP), 3, 7);
    mPmDefParmSet(&(pInfo->parm.uasP), 10, 10);
    mPmDefParmSet(&(pInfo->parm.fcP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.ppjcPdet), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.npjcPdet), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.ppjcPgen), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.npjcPgen), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.pjcDiffP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.pjcsPdet), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.pjcsPgen), cPmPerThresVal, cPmDayThresVal);
    
    /* Far-end STs path parameters */
    mPmDefParmSet(&(pInfo->parm.cvPfe), 15, 125);
    mPmDefParmSet(&(pInfo->parm.esPfe), 12, 100);
    mPmDefParmSet(&(pInfo->parm.sesPfe), 3, 7);
    mPmDefParmSet(&(pInfo->parm.uasPfe), 10, 10);
    mPmDefParmSet(&(pInfo->parm.fcPfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : private tSurStsInfo *StsInfoCreate(tSurInfo  *pSurInfo, 
                                                  tSurStsId *pStsId)

Purpose      : This function is used to allocate resources for STS

Inputs       : pSurInfo              - database handle of device
               pStsId                - STS id

Outputs      : None

Return       : tSurStsInfo*          - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurStsInfo *StsInfoCreate(tSurInfo *pSurInfo, tSurStsId *pStsId)
    {
    tSurStsInfo *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurStsInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurStsInfo), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history\n");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.cvP));
        mPmParmInit(&(pChnInfo->parm.esP));
        mPmParmInit(&(pChnInfo->parm.sesP));
        mPmParmInit(&(pChnInfo->parm.uasP));
        mPmParmInit(&(pChnInfo->parm.fcP));
        mPmParmInit(&(pChnInfo->parm.ppjcPdet));
        mPmParmInit(&(pChnInfo->parm.npjcPdet));
        mPmParmInit(&(pChnInfo->parm.ppjcPgen));
        mPmParmInit(&(pChnInfo->parm.npjcPgen));
        mPmParmInit(&(pChnInfo->parm.pjcDiffP));
        mPmParmInit(&(pChnInfo->parm.pjcsPdet));
        mPmParmInit(&(pChnInfo->parm.pjcsPgen));
        mPmParmInit(&(pChnInfo->parm.cvPfe));
        mPmParmInit(&(pChnInfo->parm.esPfe));
        mPmParmInit(&(pChnInfo->parm.sesPfe));
        mPmParmInit(&(pChnInfo->parm.uasPfe));
        mPmParmInit(&(pChnInfo->parm.fcPfe));

        /* Add to database */
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pStsId->line))]->ppStsDb[mSurStsIdFlat(pStsId)]->pInfo = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurStsInfoFinal(tSurStsInfo *pInfo)

Purpose      : This function is used to deallocate all resource in STS 
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurStsInfoFinal(tSurStsInfo *pInfo)
    {
    /* Finalize all parameters */
    mPmParmFinal(&(pInfo->parm.cvP));
    mPmParmFinal(&(pInfo->parm.esP));
    mPmParmFinal(&(pInfo->parm.sesP));
    mPmParmFinal(&(pInfo->parm.uasP));
    mPmParmFinal(&(pInfo->parm.fcP));
    mPmParmFinal(&(pInfo->parm.ppjcPdet));
    mPmParmFinal(&(pInfo->parm.npjcPdet));
    mPmParmFinal(&(pInfo->parm.ppjcPgen));
    mPmParmFinal(&(pInfo->parm.npjcPgen));
    mPmParmFinal(&(pInfo->parm.pjcDiffP));
    mPmParmFinal(&(pInfo->parm.pjcsPdet));
    mPmParmFinal(&(pInfo->parm.pjcsPgen));
    mPmParmFinal(&(pInfo->parm.cvPfe));
    mPmParmFinal(&(pInfo->parm.esPfe));
    mPmParmFinal(&(pInfo->parm.sesPfe));
    mPmParmFinal(&(pInfo->parm.uasPfe));
    mPmParmFinal(&(pInfo->parm.fcPfe));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history\n");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void StsInfoDestroy(tSurInfo   *pSurInfo,
                                           tSurStsId *pStsId)

Purpose      : This function is used to deallocate resources of STS

Inputs       : pSurInfo              - database handle of device
               pStsId                - STS id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void StsInfoDestroy(tSurInfo *pSurInfo, tSurStsId *pStsId)
    {
    tSurStsInfo *pInfo;

    pInfo = NULL;

    /* Get channel information */
    mSurStsInfo(pSurInfo, pStsId, pInfo);
    if (pInfo != null)
        {
        SurStsInfoFinal(pInfo);

        /* Destroy other resources */
        OsalMemFree(mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pStsId->line))]->ppStsDb[mSurStsIdFlat(pStsId)]->pInfo);
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pStsId->line))]->ppStsDb[mSurStsIdFlat(pStsId)]->pInfo = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet StsParmGet(tSurStsParm *pStsParm, 
                                  ePmStsParm   parm, 
                                  tPmParm     **ppParm)

Purpose      : This functions is used to get a  pointer to a STS path parameter.

Inputs       : pStsParm              - pointer to the STS parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet StsParmGet(tSurStsParm *pStsParm, ePmStsParm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pStsParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Near-end STS CV */
        case cPmStsCv:
            *ppParm = &(pStsParm->cvP);
            break;
        
        /* Near-end STS ES */
        case cPmStsEs:
            *ppParm = &(pStsParm->esP);
            break;
        
        /* Near-end STS SES */
        case cPmStsSes:
            *ppParm = &(pStsParm->sesP);
            break;
        
        /* Near-end STS UAS */
        case cPmStsUas:
            *ppParm = &(pStsParm->uasP);
            break;
        
        /* Near-end STS FC */
        case cPmStsFc:
            *ppParm = &(pStsParm->fcP);
            break;
        
        /* Near-end STS detected PPJC */
        case cPmStsPpjcPdet:
            *ppParm = &(pStsParm->ppjcPdet);
            break;
        
        /* Near-end STS detected PPJC */
        case cPmStsNpjcPdet:
            *ppParm = &(pStsParm->npjcPdet);
            break;
        
        /* Near-end STS generated PPLC */
        case cPmStsPpjcPgen:
            *ppParm = &(pStsParm->ppjcPgen);
            break;
        
        /* Near-end STS generated NPJC */
        case cPmStsNpjcPgen:
            *ppParm = &(pStsParm->npjcPgen);
            break;
        
        /* Near-end STS PJCD */
        case cPmStsPjcDiff:
            *ppParm = &(pStsParm->pjcDiffP);
            break;
        
        /* Near-end STS detected PJCS */ 
        case cPmStsPjcsPdet:
            *ppParm = &(pStsParm->pjcsPdet);
            break;
        
        /* Near-end STS generated PJCS */
        case cPmStsPjcsPgen:
            *ppParm = &(pStsParm->pjcsPgen);
            break;
        
        /* Far-end STS CV */
        case cPmStsCvPfe:
            *ppParm = &(pStsParm->cvPfe);
            break;
        
        /* Far-end STS ES */
        case cPmStsEsPfe:
            *ppParm = &(pStsParm->esPfe);
            break;
        
        /* Far-end STS SES */
        case cPmStsSesPfe:
            *ppParm = &(pStsParm->sesPfe);
            break;
        
        /* Far-end STS UAS */
        case cPmStsUasPfe:
            *ppParm = &(pStsParm->uasPfe);
            break;
        
        /* Far-end STS FC */
        case cPmStsFcPfe:
            *ppParm = &(pStsParm->fcPfe);
            break;
            
        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllStsParmRegReset(tSurStsParm *pStsParm, 
                                          ePmRegType   regType,
                                          bool         neParm)

Purpose      : This functions is used to reset registers of a group of STS 
               parameters.

Inputs       : pStsParm              - pointer to the STS parameters
               regType               - register type
               neParm                - true for near-end STS path parameters, false
                                     for far-end STS path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllStsParmRegReset(tSurStsParm *pStsParm, 
                                  ePmRegType   regType,
                                  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmStsParm   start;
    ePmStsParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pStsParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmStsCv;
        end   = cPmStsNeAllParm;
        }
    else
        {
        start = cPmStsCvPfe;
        end   = cPmStsFeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = StsParmGet(pStsParm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllStsParmAccInhSet(tSurStsParm  *pStsParm, 
                                           bool         inhibit, 
                                           bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               STS parameters.

Inputs       : pStsParm              - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end STS path parameters, false
                                     for far-end STS path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllStsParmAccInhSet(tSurStsParm  *pStsParm, 
                                   bool          inhibit, 
                                   bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmStsParm   start;
    ePmStsParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pStsParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmStsCv;
        end   = cPmStsNeAllParm;
        }
    else
        {
        start = cPmStsCvPfe;
        end   = cPmStsFeAllParm;
        }
    
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = StsParmGet(pStsParm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllStsParmRegThresSet(tSurStsParm  *pStsParm, 
                                             ePmRegType    regType,
                                             dword         thresVal, 
                                             bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               STS parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end STS path parameters, false
                                     for far-end STS path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllStsParmRegThresSet(tSurStsParm  *pStsParm, 
                                     ePmRegType    regType,
                                     dword         thresVal, 
                                     bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmStsParm   start;
    ePmStsParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pStsParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmStsCv;
        end   = cPmStsNeAllParm;
        }
    else
        {
        start = cPmStsCvPfe;
        end   = cPmStsFeAllParm;
        }
    
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = StsParmGet(pStsParm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllStsParmNumRecRegSet(tSurStsParm  *pStsParm, 
                                              byte          numRecReg, 
                                              bool          neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of STS parameters.

Inputs       : pStsParm              - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end STS path parameters, false
                                     for far-end STS path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllStsParmNumRecRegSet(tSurStsParm  *pStsParm, 
                                      byte          numRecReg, 
                                      bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmStsParm   start;
    ePmStsParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pStsParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmStsCv;
        end   = cPmStsNeAllParm;
        }
    else
        {
        start = cPmStsCvPfe;
        end   = cPmStsFeAllParm;
        }
    
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = StsParmGet(pStsParm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                   
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurStsProv(tSurDev device, tSurStsId *pStsId)

Purpose      : This function is used to provision a STS for FM & PM.

Inputs       : device                - Device
               pStsId                - STS identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsProv(tSurDev device, tSurStsId *pStsId)
    {
    tSurInfo    *pSurInfo;
    tSurStsInfo *pChnInfo;
    tSurStsDb   *pStsDb;
    tSurLineDb  *pLineDb;
    
    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pStsId))
        {
        return cSurErrNullParm;
        }
    
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
        
    /* Check if STS ID is valid within the line channel */
    pLineDb = mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pStsId->line))];
    if (mSurStsIdFlat(pStsId) >= pLineDb->numSts)
        {
        mSurDebug(cSurErrMsg, "STS ID is out of range\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrNoSuchChn;
        }
        
    /* Check for STS DB's existence */
    if (pLineDb->ppStsDb[mSurStsIdFlat(pStsId)] == null)
        {
        /* Allocate memory for STS DB */
        pStsDb = OsalMemAlloc(sizeof(tSurStsDb));
        if (pStsDb != null)
            {
            OsalMemInit(pStsDb, sizeof(tSurStsDb), 0);
            pLineDb->ppStsDb[mSurStsIdFlat(pStsId)] = pStsDb;
            }
        else
            {
            mSurDebug(cSurErrMsg, "Cannot allocate memory for STS database\n");
            if (mDevGive(device) != cSurOk)
                {
                mSurDebug(cSurErrMsg, "Cannot give device\n");
                }
            return cSurErrRsrNoAvai;
            }
        }
    
    /* Get STS information */
    mSurStsInfo(pSurInfo, pStsId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "STS is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for STS */
    pChnInfo = StsInfoCreate(pSurInfo, pStsId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for STS\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }

    /* Set default channel configuration for STS path */
    pChnInfo->conf.rate           = cSurSts1;
    pChnInfo->conf.erdiEn         = false;
    pChnInfo->conf.decSoakTime 	  = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultTerSoakTime;
    
    /* Set default parameters' values for STS path */
    if (SurStsDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErr;
        }
        
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurStsDeProv(tSurDev device, tSurStsId *pStsId)

Purpose      : This function is used to de-provision FM & PM of one STS. It
               will deallocated all resources.

Inputs       : device                - device
               pStsId                - STS identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsDeProv(tSurDev device, tSurStsId *pStsId)
    {
    eSurRet ret = cSurOk;
    tSurInfo *pSurInfo;
    tSurStsInfo *pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pStsId))
        {
        return cSurErrNullParm;
        }

    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    mSurStsInfo(pSurInfo, pStsId, pInfo);
    if (pInfo == null)
        ret = cSurErrNoSuchChn;
    /* Deallocate all resources of STS */
    else
        StsInfoDestroy(pSurInfo, pStsId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return ret;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsTrblDesgnSet(tSurDev              device, 
                                          eSurStsTrbl          trblType, 
                                          const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set STS path trouble designation for 
               a device.

Inputs       : device                 - device
               trblType               - trouble type
               pTrblDesgn             - trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsTrblDesgnSet(tSurDev              device, 
                                  eSurStsTrbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Set troble type */
    switch (trblType)
        {
        /* STS path Loss Of Pointer */    
        case cFmStsLop:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lopp);
            break;
            
        /* STS Path Payload Label Mismatch */
        case cFmStsPlm:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.plmp);
            break;
            
        /* STS Path Unequipped */
        case cFmStsUneq:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.uneqp);
            break;
            
        /* STS Path Trace Identifier Mismatch */
        case cFmStsTim:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.timp);
            break;
            
        /* STS Path Alarm Indication Signal */
        case cFmStsAis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisp);
            break;
            
        /* STS Path one-bit Remote Failure Indication. */
        case cFmStsRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfip);
            break;
        
        /* STS Path Server Enhanced Remote Failure Indication. */
        case cFmStsErfiS:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipS);
            break;
        
        /* STS Path Connectivity Enhanced Remote Failure Indication. */
        case cFmStsErfiC:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipC);
            break;
        
        /* STS Path Payload Enhanced Remote Failure Indication */    
        case cFmStsErfiP:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipP);
            break;
            
        /* STS Path BER-based Signal Failure */
        case cFmStsBerSf:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pathBerSf);
            break;
            
        /* STS Path BER-based Signal Degrade */
        case cFmStsBerSd:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pathBerSd);
            break;
            
        /* L bit error, only used for PW */
        case cFmStsLbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit error, only used for PW */
        case cFmStsRbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit error, only used for PW */
        case cFmStsMbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf. mBit);
            break;

        /* STS Path all failures */
        case cFmStsAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lopp);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.plmp);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.uneqp);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.timp);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisp);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfip);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipS);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipC);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipP);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pathBerSf);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pathBerSd);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));

            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
         
         /* Performance monitoring */
        case cPmStsTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsTrblDesgnGet(tSurDev        device, 
                                          eSurStsTrbl    trblType, 
                                          tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get STS path trouble designation of 
               a device.

Inputs       : device                 - device
               trblType               - trouble type
               
Outputs      : pTrblDesgn             - trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsTrblDesgnGet(tSurDev        device, 
                                  eSurStsTrbl    trblType, 
                                  tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if(mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }    
    
    pSurInfo = mDevDb(device);
    switch (trblType)
        {
        
        /* STS path Loss Of Pointer */    
        case cFmStsLop:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lopp);
            break;
            
        /* STS Path Payload Label Mismatch */
        case cFmStsPlm:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.plmp);
            break;
            
        /* STS Path Unequipped */
        case cFmStsUneq:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.uneqp);
            break;
            
        /* STS Path Trace Identifier Mismatch */
        case cFmStsTim:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.timp);
            break;
            
        /* STS Path Alarm Indication Signal */
        case cFmStsAis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisp);
            break;
            
        /* STS Path one-bit Remote Failure Indication. */
        case cFmStsRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfip);
            break;
        
        /* STS Path Server Enhanced Remote Failure Indication. */
        case cFmStsErfiS:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipS);
            break;
        
        /* STS Path Connectivity Enhanced Remote Failure Indication. */
        case cFmStsErfiC:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipC);
            break;
        
        /* STS Path Payload Enhanced Remote Failure Indication */    
        case cFmStsErfiP:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfipP);
            break;
            
        /* STS Path BER-based Signal Failure */
        case cFmStsBerSf:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pathBerSf);
            break;
            
        /* STS Path BER-based Signal Degrade */
        case cFmStsBerSd:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pathBerSd);
            break;
            
        /* L bit error, only used for PW */
        case cFmStsLbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit error, only used for PW */
        case cFmStsRbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit error, only used for PW */
        case cFmStsMbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* performance monitoring */
        case cPmStsTca:
           pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
           break;
           
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsEngStatSet(tSurDev             device, 
                                        const tSurStsId    *pChnId, 
                                        eSurEngStat         stat)

Purpose      : This API is used to set engine state for a STS channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurStsEngStatSet(tSurDev             device, 
                                const tSurStsId    *pChnId, 
                                eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsEngStatGet(tSurDev             device, 
                                        const tSurStsId    *pChnId, 
                                        eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a STS APS channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurStsEngStatGet(tSurDev             device, 
                                const tSurStsId    *pChnId, 
                                eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsChnCfgSet(tSurDev         device,
                                       const tSurStsId *pChnId, 
                                       const tSurStsConf *pChnCfg)

Purpose      : This internal API is use to set STS channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsChnCfgSet(tSurDev            device,
                               const tSurStsId   *pChnId, 
                               const tSurStsConf *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set configuration */
    pInfo->conf.rate           = pChnCfg->rate;
    pInfo->conf.erdiEn         = pChnCfg->erdiEn;
	pInfo->conf.decSoakTime    = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
	pInfo->conf.terSoakTime    = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }   
 
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsChnCfgGet(tSurDev     device,
                                       const tSurStsId *pChnId, 
                                       tSurStsConf *pChnCfg)
Purpose      : This API is used to get STS channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsChnCfgGet(tSurDev          device,
                               const tSurStsId *pChnId, 
                               tSurStsConf     *pChnCfg)
    {
    /* Declare local variables */
    tSurStsInfo  *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get configuration */
    pChnCfg->rate   = pInfo->conf.rate;
    pChnCfg->erdiEn = pInfo->conf.erdiEn;
    pChnCfg->decSoakTime = pInfo->conf.decSoakTime;
    pChnCfg->terSoakTime = pInfo->conf.terSoakTime;
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet SurStsTrblNotfInhSet(tSurDev               device,
                                             const tSurStsId      *pChnId, 
                                             eSurStsTrbl           trblType, 
                                             bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a STS channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsTrblNotfInhSet(tSurDev           device,
                                    const tSurStsId  *pChnId, 
                                    eSurStsTrbl       trblType, 
                                    bool              inhibit)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* STS fault monitoring */
        case cFmStsLop:
            pInfo->msgInh.lopp = inhibit;
            break;
        case cFmStsPlm:
            pInfo->msgInh.plmp = inhibit;
            break;
        case cFmStsUneq:
            pInfo->msgInh.uneqp = inhibit;
            break;
        case cFmStsTim:
            pInfo->msgInh.timp = inhibit;
            break;
        case cFmStsAis:
            pInfo->msgInh.aisp = inhibit;
            break;
        case cFmStsRfi:
        case cFmStsErfiS:
        case cFmStsErfiC:
        case cFmStsErfiP:
            pInfo->msgInh.rfip = inhibit;
            break;
        case cFmStsBerSf:
            pInfo->msgInh.pathBerSf = inhibit;
            break;
        case cFmStsBerSd:
            pInfo->msgInh.pathBerSd = inhibit;
            break;
        case cFmStsLbit:
            pInfo->msgInh.lBit = inhibit;
            break;
        case cFmStsRbit:
            pInfo->msgInh.rBit = inhibit;
            break;
        case cFmStsMbit:
            pInfo->msgInh.mBit = inhibit;
            break;
        case cFmStsAllFail:
            pInfo->msgInh.sts = inhibit;
            break;
                
        /* STS Performance monitoring */
        case cPmStsTca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsTrblNotfInhGet(tSurDev               device,
                                            const tSurStsId      *pChnId, 
                                            eSurStsTrbl           trblType, 
                                            bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               of a STS channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsTrblNotfInhGet(tSurDev          device,
                                    const tSurStsId *pChnId, 
                                    eSurStsTrbl      trblType, 
                                    bool            *pInhibit)
    {
    /* Declare local variables */
    tSurStsInfo *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* STS fault monitoring */
        case cFmStsLop:
            *pInhibit = pInfo->msgInh.lopp;
            break;
        case cFmStsPlm:
            *pInhibit = pInfo->msgInh.plmp;
            break;
        case cFmStsUneq:
            *pInhibit = pInfo->msgInh.uneqp;
            break;
        case cFmStsTim:
            *pInhibit = pInfo->msgInh.timp;
            break;
        case cFmStsAis:
            *pInhibit = pInfo->msgInh.aisp;
            break;
        case cFmStsRfi:
        case cFmStsErfiS:
        case cFmStsErfiC:
        case cFmStsErfiP:
            *pInhibit = pInfo->msgInh.rfip;
            break;
        case cFmStsBerSf:
            *pInhibit = pInfo->msgInh.pathBerSf;
            break;
        case cFmStsBerSd:
            *pInhibit = pInfo->msgInh.pathBerSd;
            break;
        case cFmStsLbit:
            *pInhibit = pInfo->msgInh.lBit;
            break;
        case cFmStsRbit:
            *pInhibit = pInfo->msgInh.rBit;
            break;
        case cFmStsMbit:
            *pInhibit = pInfo->msgInh.mBit;
            break;
        case cFmStsAllFail:
            *pInhibit = pInfo->msgInh.sts;
            break;
                
        /* STS Performance monitoring */
        case cPmStsTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurStsChnTrblNotfInhInfoGet(tSurDev             device, 
                                                   const tSurStsId    *pChnId, 
                                                   tSurStsMsgInh      *pTrblInh)

Purpose      : This API is used to get STS channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurStsChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurStsId    *pChnId, 
                                           tSurStsMsgInh      *pTrblInh)
    {
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(pTrblInh));
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmStsFailIndGet(tSurDev            device, 
                                       const tSurStsId   *pChnId, 
                                       eSurStsTrbl        trblType, 
                                       tSurFailStat      *pFailStat)

Purpose      : This API is used to get failure indication of a STS channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmStsFailIndGet(tSurDev            device, 
                               const tSurStsId   *pChnId, 
                               eSurStsTrbl        trblType, 
                               tSurFailStat      *pFailStat)
    {
    /* Declare local variables */
    tSurStsInfo *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmStsLop:
            mSurFailStatCopy(&(pInfo->failure.lopp), pFailStat);
            break;
            
        case cFmStsPlm:
            mSurFailStatCopy(&(pInfo->failure.plmp), pFailStat);
            break;
            
        case cFmStsUneq:
            mSurFailStatCopy(&(pInfo->failure.uneqp), pFailStat);
            break;
            
        case cFmStsTim:
            mSurFailStatCopy(&(pInfo->failure.timp), pFailStat);
            break;
            
        case cFmStsAis:
            mSurFailStatCopy(&(pInfo->failure.aisp), pFailStat);
            break;
            
        case cFmStsRfi:
            mSurFailStatCopy(&(pInfo->failure.rfip), pFailStat);
            break;
            
        case cFmStsErfiS:
            mSurFailStatCopy(&(pInfo->failure.erfipS), pFailStat);
            break;
            
        case cFmStsErfiC:
            mSurFailStatCopy(&(pInfo->failure.erfipC), pFailStat);
            break;
            
        case cFmStsErfiP:
            mSurFailStatCopy(&(pInfo->failure.erfipP), pFailStat);
            break;
            
        case cFmStsBerSf:
            mSurFailStatCopy(&(pInfo->failure.pathBerSf), pFailStat);
            break;
            
        case cFmStsBerSd:
            mSurFailStatCopy(&(pInfo->failure.pathBerSd), pFailStat);
            break;
                
        case cFmStsLbit:
            mSurFailStatCopy(&(pInfo->failure.lBit), pFailStat);
            break;
        case cFmStsRbit:
            mSurFailStatCopy(&(pInfo->failure.rBit), pFailStat);
            break;
        case cFmStsMbit:
            mSurFailStatCopy(&(pInfo->failure.mBit), pFailStat);
            break;
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }  
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmStsFailHisGet(tSurDev          device, 
                                                const tSurStsId *pChnId)

Purpose      : This API is used to get a STS channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmStsFailHisGet(tSurDev          device, 
                                 const tSurStsId *pChnId)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }    
        
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmStsFailHisClear(tSurDev          device, 
                                         const tSurStsId *pChnId,
                                         bool             flush)

Purpose      : This API is used to clear a STS channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmStsFailHisClear(tSurDev device, const tSurStsId *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;

    pInfo = NULL;
 
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsRegReset(tSurDev       device, 
                                     tSurStsId    *pChnId, 
                                     ePmStsParm    parm, 
                                     ePmRegType    regType)

Purpose      : This API is used to reset a STS channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmStsRegReset(tSurDev       device, 
                             tSurStsId    *pChnId, 
                             ePmStsParm    parm, 
                             ePmRegType    regType)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end STS path parameters */
    if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end STS parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end STS path parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end STS parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }      
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsRegGet(tSurDev            device, 
                                   const tSurStsId   *pChnId, 
                                   ePmStsParm         parm, 
                                   ePmRegType         regType, 
                                   tPmReg            *pRetReg)

Purpose      : This API is used to get a STS channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned registere

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmStsRegGet(tSurDev            device, 
                           const tSurStsId   *pChnId, 
                           ePmStsParm         parm, 
                           ePmRegType         regType, 
                           tPmReg            *pRetReg)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }    
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->value   = pReg->value;
    pRetReg->invalid = pReg->invalid;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmStsTcaHisGet(tSurDev device, const tSurStsId *pChnId)

Purpose      : This API is used to get a STS channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history of null if fail
------------------------------------------------------------------------------*/
friend tSortList PmStsTcaHisGet(tSurDev device, const tSurStsId *pChnId)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsTcaHisClear(tSurDev device, const tSurStsId *pChnId)

Purpose      : This API is used to clear a STS channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmStsTcaHisClear(tSurDev device, const tSurStsId *pChnId)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmStsParmAccInhSet(tSurDev          device, 
                                           const tSurStsId *pChnId, 
                                           ePmStsParm       parm, 
                                           bool             inhibit)

Purpose      : This API is used to enable/disable a STS channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmStsParmAccInhSet(tSurDev          device, 
                                  const tSurStsId *pChnId, 
                                  ePmStsParm       parm, 
                                  bool             inhibit)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end STS parameters */
    if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Setting near-end STS parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end STS parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Setting far-end STS parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsParmAccInhGet(tSurDev          device, 
                                          const tSurStsId *pChnId, 
                                          ePmStsParm       parm, 
                                          bool            *pInhibit)

Purpose      : This API is used to get a STS channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmStsParmAccInhGet(tSurDev          device, 
                                  const tSurStsId *pChnId, 
                                  ePmStsParm       parm, 
                                  bool            *pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurStsInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsRegThresSet(tSurDev           device, 
                                        const tSurStsId  *pChnId, 
                                        ePmStsParm        parm, 
                                        ePmRegType        regType, 
                                        dword             thresVal)

Purpose      : This API is used to set threshold for a STS channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmStsRegThresSet(tSurDev           device, 
                                const tSurStsId  *pChnId, 
                                ePmStsParm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end STS parameters */
    else if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmRegThresSet(&(pInfo->parm), 
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end STS parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end STS parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end STS parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsRegThresGet(tSurDev           device, 
                                        const tSurStsId  *pChnId, 
                                        ePmStsParm        parm, 
                                        ePmRegType        regType, 
                                        dword            *pThresVal)

Purpose      : This API is used to get threshold for a STS channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmStsRegThresGet(tSurDev           device, 
                                const tSurStsId  *pChnId, 
                                ePmStsParm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurStsInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
    
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsParmNumRecRegSet(tSurDev          device, 
                                             const tSurStsId *pChnId, 
                                             ePmStsParm       parm, 
                                             byte             numRecReg)

Purpose      : This API is used to set a STS channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmStsParmNumRecRegSet(tSurDev          device, 
                                     const tSurStsId *pChnId, 
                                     ePmStsParm       parm, 
                                     byte             numRecReg)
    {
    /* Declare local variables */
    tSurStsInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    mPmRecRegCheck(numRecReg);
      
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurStsInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end STS parameters */
    else if (parm == cPmStsNeAllParm)
        {
        ret = AllStsParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end STS parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end STS parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllStsParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end STS parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = StsParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmStsParmNumRecRegGet(tSurDev          device, 
                                             const tSurStsId *pChnId, 
                                             ePmStsParm       parm, 
                                             byte            *pNumRecReg)
                                             
Purpose      : This API is used to get a STS channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmStsParmNumRecRegGet(tSurDev          device, 
                                     const tSurStsId *pChnId, 
                                     ePmStsParm       parm, 
                                     byte            *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurStsInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurStsInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = StsParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }        
    
    

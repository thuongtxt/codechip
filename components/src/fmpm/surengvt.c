/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengvt.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code VT path Transmisstion 
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "suradapt.h"
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mVtKval(pSurInfo, vtRate, kVal, valid)
Purpose   : This macro is used to get K threshold of VT channel
Inputs    : pSurInfo                 - device database
            vtRate                   - VT rate
outputs   : kVal                     - K threshold
            valid                    - true to indicate that K threshold is 
                                       valid or not
------------------------------------------------------------------------------*/
#define mVtKval(pSurInfo, vtRate, kVal, valid)                                 \
    valid = true;                                                              \
    switch (vtRate)                                                            \
        {                                                                      \
        case cSurVt15:                                                         \
            kVal = (pSurInfo)->pDb->mastConf.kThres.vt15;                      \
            break;                                                             \
        case cSurVt2:                                                          \
            kVal = (pSurInfo)->pDb->mastConf.kThres.vt2;                       \
            break;                                                             \
        case cSurVt3:                                                          \
            kVal = (pSurInfo)->pDb->mastConf.kThres.vt3;                       \
            break;                                                             \
        case cSurVt6:                                                          \
            kVal = (pSurInfo)->pDb->mastConf.kThres.vt6;                       \
            break;                                                             \
                                                                               \
        /* Not support */                                                      \
        default:                                                               \
            valid = false;                                                     \
            break;                                                             \
        }

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : friend void SurVtDefUpd(tSurVtInfo *pVtInfo, tSurVtDef *pVtDef)

Purpose      : This function is used to update VT defect database

Inputs       : pVtDef                - VT defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurVtDefUpd(tSurVtInfo *pVtInfo, tSurVtDef *pVtDef)
    {
    switch (pVtDef->defect)
        {
        /* VT path Loss Of Pointer */
        case cFmVtLop:
        	if (pVtInfo->status.lopv.status != pVtDef->status)
        		{
				pVtInfo->status.lopv.status = pVtDef->status;
				pVtInfo->status.lopv.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Payload Label Mismatch */
        case cFmVtPlm:
        	if (pVtInfo->status.plmv.status != pVtDef->status)
        		{
				pVtInfo->status.plmv.status = pVtDef->status;
				pVtInfo->status.plmv.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Unequipped */
        case cFmVtUneq:
        	if (pVtInfo->status.uneqv.status != pVtDef->status)
        		{
				pVtInfo->status.uneqv.status = pVtDef->status;
				pVtInfo->status.uneqv.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Trace Identifier Mismatch */
        case cFmVtTim:
        	if (pVtInfo->status.timv.status != pVtDef->status)
        		{
				pVtInfo->status.timv.status = pVtDef->status;
				pVtInfo->status.timv.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Alarm Indication Signal */
        case cFmVtAis:
        	if (pVtInfo->status.aisv.status != pVtDef->status)
        		{
				pVtInfo->status.aisv.status = pVtDef->status;
				pVtInfo->status.aisv.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Remote Failure Indication */
        case cFmVtRdi:
        	if (pVtInfo->status.rdiv.status != pVtDef->status)
        		{
				pVtInfo->status.rdiv.status = pVtDef->status;
				pVtInfo->status.rdiv.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Server Enhanced Remote Failure Indication. */
        case cFmVtErdiS:
        	if (pVtInfo->status.erdivS.status != pVtDef->status)
        		{
				pVtInfo->status.erdivS.status = pVtDef->status;
				pVtInfo->status.erdivS.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Connectivity Enhanced Remote Failure Indication. */
        case cFmVtErdiC:
        	if (pVtInfo->status.erdivC.status != pVtDef->status)
        		{
				pVtInfo->status.erdivC.status = pVtDef->status;
				pVtInfo->status.erdivC.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path Payload Enhanced Remote Failure Indication */
        case cFmVtErdiP:
        	if (pVtInfo->status.erdivP.status != pVtDef->status)
        		{
				pVtInfo->status.erdivP.status = pVtDef->status;
				pVtInfo->status.erdivP.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path BER-based Signal Failure */
        case cFmVtBerSf:
        	if (pVtInfo->status.vtBerSf.status != pVtDef->status)
        		{
				pVtInfo->status.vtBerSf.status = pVtDef->status;
				pVtInfo->status.vtBerSf.time   = pVtDef->time;
        		}
            break;
            
        /* VT Path BER-based Signal Degrade */
        case cFmVtBerSd:
        	if (pVtInfo->status.vtBerSd.status != pVtDef->status)
        		{
				pVtInfo->status.vtBerSd.status = pVtDef->status;
				pVtInfo->status.vtBerSd.time   = pVtDef->time;
        		}
            break;

        /* L bit error */
        case cFmVtLbit:
        	if (pVtInfo->status.lBit.status != pVtDef->status)
        		{
				pVtInfo->status.lBit.status = pVtDef->status;
				pVtInfo->status.lBit.time   = pVtDef->time;
        		}
            break;

        /* R bit error */
        case cFmVtRbit:
        	if (pVtInfo->status.rBit.status != pVtDef->status)
        		{
				pVtInfo->status.rBit.status = pVtDef->status;
				pVtInfo->status.rBit.time   = pVtDef->time;
        		}
            break;

        /* M bit error */
        case cFmVtMbit:
        	if (pVtInfo->status.mBit.status != pVtDef->status)
        		{
				pVtInfo->status.mBit.status = pVtDef->status;
				pVtInfo->status.mBit.time   = pVtDef->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void VtAllDefUpd(tSurInfo       *pSurInfo,
                                        const tSurVtId *pVtId)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pVtId                 - VT ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void VtAllDefUpd(tSurInfo       *pSurInfo,
                         const tSurVtId *pVtId)
    {
    bool         blNewStat;
    tSurVtDef    def;
    tSurVtInfo  *pVtInfo;
    dword        dDefStat;

    pVtInfo = NULL;
    /* Get VT information */
    mSurVtInfo(pSurInfo, pVtId, pVtInfo);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pVtId, &(def.id), sizeof(tSurVtId));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurVt, pVtId, &dDefStat);
    
    /* VT Path Alarm Indication Signal */
    blNewStat = mBitStat(dDefStat, cSurVtAisMask);
    if (blNewStat != pVtInfo->status.aisv.status)
        {
        def.defect = cFmVtAis;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }
        
    /* VT path Loss Of Pointer */
    blNewStat = mBitStat(dDefStat, cSurVtLopMask);
    if (blNewStat != pVtInfo->status.lopv.status)
        {
        def.defect = cFmVtLop;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }
    
    /* VT Path Unequipped */
    blNewStat = mBitStat(dDefStat, cSurVtUneqMask);
    if (blNewStat != pVtInfo->status.uneqv.status)
        {
        def.defect = cFmVtUneq;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }
    
    /* VT Path Trace Identifier Mismatch */
    blNewStat = mBitStat(dDefStat, cSurVtTimMask);
    if (blNewStat != pVtInfo->status.timv.status)
        {
        def.defect = cFmVtTim;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }
        
    /* VT Path Payload Label Mismatch */
    blNewStat = mBitStat(dDefStat, cSurVtPlmMask);
    if (blNewStat != pVtInfo->status.plmv.status)
        {
        def.defect = cFmVtPlm;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }
        
    /* VT Path one-bit Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurVtRdiMask);
    if (blNewStat != pVtInfo->status.rdiv.status)
        {
        def.defect = cFmVtRdi;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* VT Path Server Enhanced Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurVtErdiSMask);
    if (blNewStat != pVtInfo->status.erdivS.status)
        {
        def.defect = cFmVtErdiS;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* VT Path Connectivity Enhanced Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurVtErdiCMask);
    if (blNewStat != pVtInfo->status.erdivC.status)
        {
        def.defect = cFmVtErdiC;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* VT Path Payload Enhanced Remote Defect Indication */
    blNewStat = mBitStat(dDefStat, cSurVtErdiPMask);
    if (blNewStat != pVtInfo->status.erdivP.status)
        {
        def.defect = cFmVtErdiP;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* VT Path BER-based Signal Failure */
    blNewStat = mBitStat(dDefStat, cSurVtBerSfMask);
    if (blNewStat != pVtInfo->status.vtBerSf.status)
        {
        def.defect = cFmVtBerSf;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* VT Path BER-based Signal Degrade */
    blNewStat = mBitStat(dDefStat, cSurVtBerSdMask);
    if (blNewStat != pVtInfo->status.vtBerSd.status)
        {
        def.defect = cFmVtBerSd;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* L bit defect */
    blNewStat = mBitStat(dDefStat, cSurVtLbitMask);
    if (blNewStat != pVtInfo->status.lBit.status)
        {
        def.defect = cFmVtLbit;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* R bit defect */
    blNewStat = mBitStat(dDefStat, cSurVtRbitMask);
    if (blNewStat != pVtInfo->status.rBit.status)
        {
        def.defect = cFmVtRbit;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }

    /* M bit defect */
    blNewStat = mBitStat(dDefStat, cSurVtMbitMask);
    if (blNewStat != pVtInfo->status.mBit.status)
        {
        def.defect = cFmVtMbit;
        def.status = blNewStat;
        SurVtDefUpd(pVtInfo, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void VtAllFailUpd(tSurInfo       *pSurInfo,
                                         const tSurVtId *pVtId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pVtId                 - VT ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void VtAllFailUpd(tSurInfo       *pSurInfo,
                          const tSurVtId *pVtId)
    {
    tSurTrblProf *pTrblProf;
    tSurVtFail   *pFailInd;
    tSortList     failHis;
    tSurVtInfo   *pVtInfo;
    tSurStsInfo  *pStsInfo;
    tSurLineInfo *pLineInfo;
    tFmFailInfo   failInfo;
    bool          blFailChg;
    bool          blNotf;

    pVtInfo = NULL;
    pStsInfo = NULL;

    /* Get line, STS & VT information */
    mSurVtInfo(pSurInfo, pVtId, pVtInfo);
    mSurStsInfo(pSurInfo, &(pVtId->sts), pStsInfo);
    mSurLineInfo(pSurInfo, &(pVtId->sts.line), pLineInfo);
    
    /* Do not process any VT failure if either LOS, LOF failure is declared at 
       section/line level */
    if ((pLineInfo->failure.los.status  == true) ||
        (pLineInfo->failure.lof.status  == true) ||
        (pStsInfo->failure.aisp.status  == true) ||
        (pStsInfo->failure.lopp.status  == true))
        {
        return;
        }
        
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pVtInfo->failure);
    failHis   = pVtInfo->failHis;
    
    /* VT AIS-V failure */
    mFailIndUpd(pSurInfo, pVtInfo, aisv, aisv, blFailChg);
    if (blFailChg == true)
        {
        blNotf = true;
        if ((pLineInfo->failure.aisl.status == true) ||
            (pVtInfo->msgInh.vt             == true) ||
            (pVtInfo->msgInh.aisv           == true))
            {
            blNotf = false;
            }
        mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->aisv, failHis, failInfo, blNotf, cFmVtAis, pTrblProf->aisv);
        
        /* Update failure count parameter */
        if (pVtInfo->failure.aisv.status == true)
            {
            pVtInfo->parm.fcV.curSec += 1;
            }
            
        /* Clear UNEQ-V failure if it is present */
        if ((pVtInfo->failure.aisv.status  == true) &&
            (pVtInfo->failure.uneqv.status == true))
            {
            pVtInfo->failure.uneqv.status = false;
            pVtInfo->failure.uneqv.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.uneqv          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->uneqv, failHis, failInfo, blNotf, cFmVtUneq, pTrblProf->uneqv);
            }
            
        /* Clear PLM-V failure if it is present */
        if ((pVtInfo->failure.aisv.status == true) &&
            (pVtInfo->failure.plmv.status == true))
            {
            pVtInfo->failure.plmv.status = false;
            pVtInfo->failure.plmv.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pStsInfo->failure.timp.status  == true) ||
                (pStsInfo->failure.plmp.status  == true) ||
                (pVtInfo->failure.uneqv.status  == true) ||
                (pVtInfo->failure.timv.status   == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.plmv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->plmv, failHis, failInfo, blNotf, cFmVtPlm, pTrblProf->plmv);
            }
        }
        
    /* VT LOP-V failure */
    mFailIndUpd(pSurInfo, pVtInfo, lopv, lopv, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pVtInfo->msgInh.vt   == false) && 
                  (pVtInfo->msgInh.lopv == false));
        mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->lopv, failHis, failInfo, blNotf, cFmVtLop, pTrblProf->lopv);
        
        /* Update failure count parameter */
        if (pVtInfo->failure.lopv.status == true)
            {
            pVtInfo->parm.fcV.curSec += 1;
            }
            
        /* Clear UNEQ-V failure if it is present */
        if ((pVtInfo->failure.lopv.status  == true) &&
            (pVtInfo->failure.uneqv.status == true))
            {
            pVtInfo->failure.uneqv.status = false;
            pVtInfo->failure.uneqv.time   = dCurTime;
            
            /* Notify trouble */
             blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.uneqv          == true))
                {
                blNotf = false;
                }
             mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->uneqv, failHis, failInfo, blNotf, cFmVtUneq, pTrblProf->uneqv);
            }
            
        /* Clear PLM-V failure if it is present */
        if ((pVtInfo->failure.lopv.status == true) &&
            (pVtInfo->failure.plmv.status == true))
            {
            pVtInfo->failure.plmv.status = false;
            pVtInfo->failure.plmv.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pStsInfo->failure.timp.status  == true) ||
                (pStsInfo->failure.plmp.status  == true) ||
                (pVtInfo->failure.uneqv.status  == true) ||
                (pVtInfo->failure.timv.status   == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.plmv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->plmv, failHis, failInfo, blNotf, cFmVtPlm, pTrblProf->plmv);
            }
        }

    /* Only declare/clear UNEQ-V or PLM-V failure when AIS-V or LOP-V failure is 
       not present */
    if ((pVtInfo->failure.aisv.status == false) &&
        (pVtInfo->failure.lopv.status == false))
        {
        /* VT UNEQ-V failure */
        mFailIndUpd(pSurInfo, pVtInfo, uneqv, uneqv, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.uneqv          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->uneqv, failHis, failInfo, blNotf, cFmVtUneq, pTrblProf->uneqv);
            
            /* Update failure count parameter */
            if ((pVtInfo->failure.uneqv.status == true) &&
                (pVtInfo->conf.erdiEn          == true))
                {
                pVtInfo->parm.fcV.curSec += 1;
                }
            }
            
        /* VT PLM-V failure */
        mFailIndUpd(pSurInfo, pVtInfo, plmv, plmv, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if (/*(pLineInfo->failure.tims.status == true) ||*/
                (pStsInfo->failure.uneqp.status == true) ||
                /*(pStsInfo->failure.timp.status  == true) ||*/
                (pStsInfo->failure.plmp.status  == true) ||
                (pVtInfo->failure.uneqv.status  == true) ||
                /*(pVtInfo->failure.timv.status   == true) ||*/
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.plmv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->plmv, failHis, failInfo, blNotf, cFmVtPlm, pTrblProf->plmv);
            }
        }
        
    /* VT TIM-V failure */
    mFailIndUpd(pSurInfo, pVtInfo, timv, timv, blFailChg);
    if (blFailChg == true)
        {
        blNotf = true;
        if ((pLineInfo->failure.tims.status == true) ||
            (pStsInfo->failure.uneqp.status == true) ||
            (pStsInfo->failure.timp.status  == true) ||
            (pStsInfo->failure.plmp.status  == true) ||
            (pVtInfo->failure.uneqv.status  == true) ||
            (pVtInfo->msgInh.vt             == true) ||
            (pVtInfo->msgInh.timv           == true))
            {
            blNotf = false;
            }
        mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->timv, failHis, failInfo, blNotf, cFmVtTim, pTrblProf->timv);
        }
    
    /* If ERDI-V is not supported , declare/clear 1-bit RFI-V */
    if (pVtInfo->conf.erdiEn == false)
        {
        /* VT Path one-bit Remote Failure Indication. */
        mFailIndUpd(pSurInfo, pVtInfo, rdiv, rfiv, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if (/*(pLineInfo->failure.rfil.status == true) ||*/
                /*(pStsInfo->failure.rfip.status  == true) ||*/
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.rfiv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->rfiv, failHis, failInfo, blNotf, cFmVtRfi, pTrblProf->rfiv);
            
            /* Update far-end failure count parameter */
            if (pVtInfo->failure.rfiv.status == true)
                {
                pVtInfo->parm.fcVfe.curSec += 1;
                }
            }
        }
    
     /* If ERDI-V is supported, declare/clear 3-bits RFI-V */
     else
        {
        /* VT Path Server Enhanced Remote Failure Indication. */
        mFailIndUpd(pSurInfo, pVtInfo, erdivS, erfivS, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pStsInfo->failure.rfip.status  == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.rfiv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->erfivS, failHis, failInfo, blNotf, cFmVtErfiS, pTrblProf->erfivS);
            
            /* Update far-end failure count parameter */
            if (pVtInfo->failure.erfivS.status == true)
                {
                pVtInfo->parm.fcVfe.curSec += 1;
                }
            }
        
        /* VT Path Connectivity Enhanced Remote Failure Indication. */
        mFailIndUpd(pSurInfo, pVtInfo, erdivC, erfivC, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pStsInfo->failure.rfip.status  == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.rfiv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->erfivC, failHis, failInfo, blNotf, cFmVtErfiC, pTrblProf->erfivC);
            
            /* Update far-end failure count parameter */
            if (pVtInfo->failure.erfivC.status == true)
                {
                pVtInfo->parm.fcVfe.curSec += 1;
                }
            }

        /* VT Path Payload Enhanced Remote Failure Indication */
        mFailIndUpd(pSurInfo, pVtInfo, erdivP, erfivP, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pStsInfo->failure.rfip.status  == true) ||
                (pVtInfo->msgInh.vt             == true) ||
                (pVtInfo->msgInh.rfiv           == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->erfivP, failHis, failInfo, blNotf, cFmVtErfiP, pTrblProf->erfivP);
            }
        }
        
    /* VT Path BER-based Signal Failure */
    mFailIndUpd(pSurInfo, pVtInfo, vtBerSf, vtBerSf, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pVtInfo->msgInh.vt      == false) && 
                  (pVtInfo->msgInh.vtBerSf == false));
        mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->vtBerSf, failHis, failInfo, blNotf, cFmVtBerSf, pTrblProf->vtBerSf);
        
        /* Terminate BER-SD failure if it was already declared */
        if ((pVtInfo->failure.vtBerSf.status == true) && 
            (pVtInfo->failure.vtBerSd.status == true))
            {
            /* Clear failure indication */
            pVtInfo->failure.vtBerSd.status = false;
            pVtInfo->failure.vtBerSd.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = ((pVtInfo->msgInh.vt      == false) && 
                      (pVtInfo->msgInh.vtBerSd == false));
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->vtBerSd, failHis, failInfo, blNotf, cFmVtBerSd, pTrblProf->vtBerSd);
            }
        }
    
    /* VT Path BER-based Signal Degrade */
    /* Only declare/terminate BER-SD failure when BER-SF failure is not present */
    if (pVtInfo->failure.vtBerSf.status == false)
        {
        mFailIndUpd(pSurInfo, pVtInfo, vtBerSd, vtBerSd, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pVtInfo->msgInh.vt      == false) && 
                      (pVtInfo->msgInh.vtBerSd == false));
            mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->vtBerSd, failHis, failInfo, blNotf, cFmVtBerSd, pTrblProf->vtBerSd);
            }
        }

    /* L bit failure */
	mFailIndUpd(pSurInfo, pVtInfo, lBit, lBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pVtInfo->msgInh.vt   == false) &&
				  (pVtInfo->msgInh.lBit == false));
		mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->lBit, failHis, failInfo, blNotf, cFmVtLbit, pTrblProf->lBit);
        }

    /* R bit failure */
	mFailIndUpd(pSurInfo, pVtInfo, rBit, rBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pVtInfo->msgInh.vt   == false) &&
				  (pVtInfo->msgInh.rBit == false));
		mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->rBit, failHis, failInfo, blNotf, cFmVtRbit, pTrblProf->rBit);
        }

	/* M bit failure */
	mFailIndUpd(pSurInfo, pVtInfo, mBit, mBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pVtInfo->msgInh.vt   == false) &&
				  (pVtInfo->msgInh.mBit == false));
		mFailRept(pSurInfo, cSurVt, pVtId, pFailInd->mBit, failHis, failInfo, blNotf, cFmVtMbit, pTrblProf->mBit);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void VtPmParmUpd(tSurInfo       *pSurInfo,
                                        const tSurVtId *pVtId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pVtId                 - VT ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void VtPmParmUpd(tSurInfo       *pSurInfo,
                         const tSurVtId *pVtId)
    {
    tSurVtInfo    *pVtInfo;
    word           wVtK;
    bool           blNewStat;
    tSurVtAnomaly  anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    tSurLineInfo  *pLineInfo;
    tSurStsInfo   *pStsInfo;
    long           pjcDiff;
    bool           blLoStat;
	tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pVtInfo = NULL;
    pStsInfo = NULL;

    /* Get VT information */
    mSurVtInfo(pSurInfo, pVtId, pVtInfo);
    mSurStsInfo(pSurInfo, &(pVtId->sts), pStsInfo);
    mSurLineInfo(pSurInfo, &(pVtId->sts.line), pLineInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pVtInfo->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pVtInfo->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pVtInfo->parm.curEngTime.minute)
        {
        pVtInfo->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pVtInfo->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pVtInfo->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }


    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurVtAnomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurVt, pVtId, &anomaly);
        pVtInfo->parm.cvV.curSec      += anomaly.bipV;
        pVtInfo->parm.cvVfe.curSec    += anomaly.reiV;
        pVtInfo->parm.ppjcVdet.curSec += anomaly.piInc;
        pVtInfo->parm.npjcVdet.curSec += anomaly.piDec;
        pVtInfo->parm.ppjcVgen.curSec += anomaly.pgInc;
        pVtInfo->parm.npjcVgen.curSec += anomaly.pgDec;
        }
    
    /* Check if defect exists at lower layer (section/line & STS path) */
    blLoStat = ((((pLineInfo                     != null)        &&
                  (pLineInfo->status.state       == cSurStart))  && 
                 ((pLineInfo->status.los.status  == true)        ||
                  (pLineInfo->status.lof.status  == true)        ||
                  (pLineInfo->status.aisl.status == true)        ||
                  (pLineInfo->status.tims.status == true)))      ||
                (((pStsInfo                      != null)        &&
                  (pStsInfo->status.state        == cSurStart))  &&
                 ((pStsInfo->status.lopp.status  == true)        ||
                  (pStsInfo->status.aisp.status  == true)        ||
                  (pStsInfo->status.uneqp.status == true)        ||
                  (pStsInfo->status.timp.status  == true)        ||
                  (pStsInfo->status.plmp.status  == true)))) ? true : false;
                  
    /* Update ES-V & SES-V parameters if lower layer (section/line & STS path) 
       defect, or AIS-V defect, or LOP-V defect, or (if ERDI-V is supported) 
       UNEQ-V is present */
    if ((blLoStat                       == true) ||
        (pVtInfo->status.aisv.status    == true) ||
        (pVtInfo->status.lopv.status    == true) ||
        ((pVtInfo->conf.erdiEn          == true) && 
         ((pVtInfo->status.uneqv.status == true))))
        {
        pVtInfo->parm.esV.curSec  = 1;
        pVtInfo->parm.sesV.curSec = 1;
        }
        
    /* Update ES-VFE & SES-VFE parameters */
    /* If ERDI-V is not supported, update ES-VFE & SES-VFE parameters only if 1-bit
       RDI-V defect (detected in V5 byte) is present */
    if (pVtInfo->conf.erdiEn == false)
        {
        if (pVtInfo->status.rdiv.status == true)
            {
            pVtInfo->parm.esVfe.curSec  = 1;
            pVtInfo->parm.sesVfe.curSec = 1;
            }
        }
    
    /* If ERDI-V is supported, update ES-VFE & SES-VFE parameters only if 3-bits 
       RDI-V defect (detected in Z7 byte) is present */
    else
        {
        if ((pVtInfo->status.erdivS.status == true) ||
            (pVtInfo->status.erdivC.status == true)) 
            {
            pVtInfo->parm.esVfe.curSec  = 1;
            pVtInfo->parm.sesVfe.curSec = 1;
            }
        }
    
    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update ES-V */
        if (pVtInfo->parm.cvV.curSec != 0)
            {
            pVtInfo->parm.esV.curSec = 1;
            }

        /* Update ES-VFE */
        if (pVtInfo->parm.cvVfe.curSec != 0)
            {
            pVtInfo->parm.esVfe.curSec = 1;
            }

        /* Update SES-P parameters */
        wVtK = 0;
        mVtKval(pSurInfo, pVtInfo->conf.rate, wVtK, blNewStat);
        if (blNewStat == false)
            {
            mSurDebug(cSurWarnMsg, "Not support VT rate\n");
            }
        else
            {
            /* VT Near-end */
            if (pVtInfo->parm.cvV.curSec >= wVtK)
                {
                pVtInfo->parm.sesV.curSec = 1;
                }

            /* VT Far-end */
            if (pVtInfo->parm.cvVfe.curSec >= wVtK)
                {
                pVtInfo->parm.sesVfe.curSec = 1;
                }
            }
        
        /* Update VT PJ-Related registers  */
        if ((pVtInfo->parm.ppjcVdet.curSec != 0) ||
            (pVtInfo->parm.npjcVdet.curSec != 0))
            {
            pVtInfo->parm.pjcsVdet.curSec = 1;
            }
        if ((pVtInfo->parm.ppjcVgen.curSec != 0) ||
            (pVtInfo->parm.npjcVgen.curSec != 0))
            {
            pVtInfo->parm.pjcsVgen.curSec = 1;
            }
        pjcDiff = (pVtInfo->parm.ppjcVgen.curSec - pVtInfo->parm.npjcVgen.curSec) - 
                  (pVtInfo->parm.ppjcVdet.curSec - pVtInfo->parm.npjcVdet.curSec);
        pVtInfo->parm.pjcDiffV.curSec = (pjcDiff > 0) ? pjcDiff : (0 - pjcDiff);
        
        /* Update UAS parameter */
        if (pVtInfo->status.neUat == true)
            {
            pVtInfo->parm.uasV.curSec = 1;
            }
        if (pVtInfo->status.feUat == true)
            {
            pVtInfo->parm.uasVfe.curSec = 1;
            }

        /* Check if VT Near-End UAS just enter or exit and adjust registers */
        blNewStat = (pVtInfo->parm.sesV.curSec == 1) ? true : false;
        if (blNewStat != pVtInfo->status.neUatPot)
            {
            pVtInfo->status.nePotCnt = 1;
            }
        else
            {
            pVtInfo->status.nePotCnt++;
            }
        if (pVtInfo->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pVtInfo->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pVtInfo->parm.esV));
                mCurPerNegAdj(&(pVtInfo->parm.sesV));
                mCurPerNegAdj(&(pVtInfo->parm.ppjcVdet));
                mCurPerNegAdj(&(pVtInfo->parm.npjcVdet));
                mCurPerNegAdj(&(pVtInfo->parm.ppjcVgen));
                mCurPerNegAdj(&(pVtInfo->parm.npjcVgen));
                mCurPerNegAdj(&(pVtInfo->parm.pjcDiffV));
                mCurPerNegAdj(&(pVtInfo->parm.pjcsVdet));
                mCurPerNegAdj(&(pVtInfo->parm.pjcsVgen));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pVtInfo->parm.esV));
                    mPrePerNegAdj(&(pVtInfo->parm.sesV));
                    mPrePerNegAdj(&(pVtInfo->parm.ppjcVdet));
                    mPrePerNegAdj(&(pVtInfo->parm.npjcVdet));
                    mPrePerNegAdj(&(pVtInfo->parm.ppjcVgen));
                    mPrePerNegAdj(&(pVtInfo->parm.npjcVgen));
                    mPrePerNegAdj(&(pVtInfo->parm.pjcDiffV));
                    mPrePerNegAdj(&(pVtInfo->parm.pjcsVdet));
                    mPrePerNegAdj(&(pVtInfo->parm.pjcsVgen));
                    }

                /* Update UAS */
            	pVtInfo->parm.uasV.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pVtInfo->parm.cvV));
                mCurPerPosAdj(&(pVtInfo->parm.esV));
                mCurPerPosAdj(&(pVtInfo->parm.sesV));
                mCurPerPosAdj(&(pVtInfo->parm.ppjcVdet));
                mCurPerPosAdj(&(pVtInfo->parm.npjcVdet));
                mCurPerPosAdj(&(pVtInfo->parm.ppjcVgen));
                mCurPerPosAdj(&(pVtInfo->parm.npjcVgen));
                mCurPerPosAdj(&(pVtInfo->parm.pjcDiffV));
                mCurPerPosAdj(&(pVtInfo->parm.pjcsVdet));
                mCurPerPosAdj(&(pVtInfo->parm.pjcsVgen));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pVtInfo->parm.cvV));
                    mPrePerPosAdj(&(pVtInfo->parm.esV));
                    mPrePerPosAdj(&(pVtInfo->parm.sesV));
                    mPrePerPosAdj(&(pVtInfo->parm.ppjcVdet));
                    mPrePerPosAdj(&(pVtInfo->parm.npjcVdet));
                    mPrePerPosAdj(&(pVtInfo->parm.ppjcVgen));
                    mPrePerPosAdj(&(pVtInfo->parm.npjcVgen));
                    mPrePerPosAdj(&(pVtInfo->parm.pjcDiffV));
                    mPrePerPosAdj(&(pVtInfo->parm.pjcsVdet));
                    mPrePerPosAdj(&(pVtInfo->parm.pjcsVgen));
                    }

                /* Update UAS */
            	if (pVtInfo->parm.uasV.curPer.value >= 10)
            		{
            		pVtInfo->parm.uasV.curPer.value -= 10;
            		}
            	else
            		{
            		pVtInfo->parm.uasV.curPer.value = 0;
            		}
            	pVtInfo->parm.uasV.curSec = 0;
                }
            }

        /* Check if VT Far-End UAS just enter or exit and adjust registers */
        blNewStat = (pVtInfo->parm.sesVfe.curSec == 1) ? true : false;
        if (blNewStat != pVtInfo->status.feUatPot)
            {
            pVtInfo->status.fePotCnt = 1;
            }
        else
            {
            pVtInfo->status.fePotCnt++;
            }
        if (pVtInfo->status.fePotCnt == 10)
            {
            /* Update unavailable time status */
            pVtInfo->status.feUat = blNewStat;
            SurCurTime(&currentTime);

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pVtInfo->parm.esVfe));
                mCurPerNegAdj(&(pVtInfo->parm.sesVfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pVtInfo->parm.esVfe));
                    mPrePerNegAdj(&(pVtInfo->parm.sesVfe));
                    }

                /* Update UAS */
            	pVtInfo->parm.uasVfe.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pVtInfo->parm.cvVfe));
                mCurPerPosAdj(&(pVtInfo->parm.esVfe));
                mCurPerPosAdj(&(pVtInfo->parm.sesVfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pVtInfo->parm.cvVfe));
                    mPrePerPosAdj(&(pVtInfo->parm.esVfe));
                    mPrePerPosAdj(&(pVtInfo->parm.sesVfe));
                    }

                /* Update UAS */
            	if (pVtInfo->parm.uasVfe.curPer.value >= 10)
            		{
            		pVtInfo->parm.uasVfe.curPer.value -= 10;
            		}
            	else
            		{
            		pVtInfo->parm.uasVfe.curPer.value = 0;
            		}
            	pVtInfo->parm.uasVfe.curSec = 0;
                }
            }
        
        /* Accumulate inhibition */
        pVtInfo->parm.cvV.inhibit      = false;
        pVtInfo->parm.esV.inhibit      = false;
        pVtInfo->parm.sesV.inhibit     = false;
        pVtInfo->parm.ppjcVdet.inhibit = false;
        pVtInfo->parm.npjcVdet.inhibit = false;
        pVtInfo->parm.ppjcVgen.inhibit = false;
        pVtInfo->parm.npjcVgen.inhibit = false;
        pVtInfo->parm.pjcDiffV.inhibit = false;
        pVtInfo->parm.pjcsVdet.inhibit = false;
        pVtInfo->parm.pjcsVgen.inhibit = false;
        pVtInfo->parm.cvVfe.inhibit    = false;
        pVtInfo->parm.esVfe.inhibit    = false;
        pVtInfo->parm.sesVfe.inhibit   = false;

        /* Inhibit when UAS */
        if (pVtInfo->status.neUat == true)
            {
            pVtInfo->parm.cvV.inhibit      = true;
            pVtInfo->parm.esV.inhibit      = true;
            pVtInfo->parm.sesV.inhibit     = true;
            pVtInfo->parm.ppjcVdet.inhibit = true;
            pVtInfo->parm.npjcVdet.inhibit = true;
            pVtInfo->parm.ppjcVgen.inhibit = true;
            pVtInfo->parm.npjcVgen.inhibit = true;
            pVtInfo->parm.pjcDiffV.inhibit = true;
            pVtInfo->parm.pjcsVdet.inhibit = true;
            pVtInfo->parm.pjcsVgen.inhibit = true;
            }
        if (pVtInfo->status.feUat == true)
            {
            pVtInfo->parm.cvVfe.inhibit = true;
            pVtInfo->parm.esVfe.inhibit = true;
            pVtInfo->parm.sesVfe.inhibit= true;
            }

        /* Inhibit when SES */
        if (pVtInfo->parm.sesV.curSec == 1)
            {
            pVtInfo->parm.cvV.inhibit = true;
            }
        if (pVtInfo->parm.sesVfe.curSec == 1)
            {
            pVtInfo->parm.cvVfe.inhibit = true;
            }

        /* Inhibit VT Far-End parameters when AIS-P or lower-level defects
           occurred */
        if ((blLoStat                     == true) ||
            (pVtInfo->status.aisv.status  == true) ||
            (pVtInfo->status.lopv.status  == true) ||
            (pVtInfo->status.uneqv.status == true))
            {
            pVtInfo->parm.cvVfe.inhibit  = true;
            pVtInfo->parm.esVfe.inhibit  = true;
            pVtInfo->parm.sesVfe.inhibit = true;
            pVtInfo->parm.uasVfe.inhibit = true;
            pVtInfo->parm.fcVfe.inhibit  = true;

            pVtInfo->parm.cvVfe.curPer.invalid  = true;
            pVtInfo->parm.esVfe.curPer.invalid  = true;                               
            pVtInfo->parm.sesVfe.curPer.invalid = true;                               
            pVtInfo->parm.uasVfe.curPer.invalid = true;                               
            pVtInfo->parm.fcVfe.curPer.invalid  = true;                               
            }

        /* Increase VT Near-End adjustment registers */
        /* In available time */
        if (pVtInfo->status.neUat == false)
            {
            /* Potential for enterning unavailable time */
            if (pVtInfo->parm.sesV.curSec == 1)
                {
                mNegInc(&(pVtInfo->parm.esV));
                mNegInc(&(pVtInfo->parm.sesV));
                mNegInc(&(pVtInfo->parm.ppjcVdet));
                mNegInc(&(pVtInfo->parm.npjcVdet));
                mNegInc(&(pVtInfo->parm.ppjcVgen));
                mNegInc(&(pVtInfo->parm.npjcVgen));
                mNegInc(&(pVtInfo->parm.pjcDiffV));
                mNegInc(&(pVtInfo->parm.pjcsVdet));
                mNegInc(&(pVtInfo->parm.pjcsVgen));
                }
            }
            
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pVtInfo->parm.sesV.curSec == 0)
                {
                mPosInc(&(pVtInfo->parm.cvV));
                mPosInc(&(pVtInfo->parm.esV));
                mPosInc(&(pVtInfo->parm.sesV));
                mPosInc(&(pVtInfo->parm.ppjcVdet));
                mPosInc(&(pVtInfo->parm.npjcVdet));
                mPosInc(&(pVtInfo->parm.ppjcVgen));
                mPosInc(&(pVtInfo->parm.npjcVgen));
                mPosInc(&(pVtInfo->parm.pjcDiffV));
                mPosInc(&(pVtInfo->parm.pjcsVdet));
                mPosInc(&(pVtInfo->parm.pjcsVgen));
                }
            }

        /* Increase VT Far-End adjustment registers */
        if (pVtInfo->status.neUat == false)
            {
            /* In available time */
            if (pVtInfo->status.feUat == false)
                {
                /* Potential for entering unavailable time */
                if (pVtInfo->parm.sesVfe.curSec == 1)
                    {
                    mNegInc(&(pVtInfo->parm.esVfe));
                    mNegInc(&(pVtInfo->parm.sesVfe));
                    mNegInc(&(pVtInfo->parm.ppjcVdet));
                    mNegInc(&(pVtInfo->parm.npjcVdet));
                    mNegInc(&(pVtInfo->parm.ppjcVgen));
                    mNegInc(&(pVtInfo->parm.npjcVgen));
                    mNegInc(&(pVtInfo->parm.pjcDiffV));
                    mNegInc(&(pVtInfo->parm.pjcsVdet));
                    mNegInc(&(pVtInfo->parm.pjcsVgen));
                    }
                }
                
            /* In unavailable time */
            else
                {
                /* Potential for exiting unavailable time */
                if (pVtInfo->parm.sesV.curSec == 0)
                    {
                    mPosInc(&(pVtInfo->parm.cvVfe));
                    mPosInc(&(pVtInfo->parm.esVfe));
                    mPosInc(&(pVtInfo->parm.sesVfe));
                    mPosInc(&(pVtInfo->parm.ppjcVdet));
                    mPosInc(&(pVtInfo->parm.npjcVdet));
                    mPosInc(&(pVtInfo->parm.ppjcVgen));
                    mPosInc(&(pVtInfo->parm.npjcVgen));
                    mPosInc(&(pVtInfo->parm.pjcDiffV));
                    mPosInc(&(pVtInfo->parm.pjcsVdet));
                    mPosInc(&(pVtInfo->parm.pjcsVgen));
                    }
                }
            }
            
        /* Clear VT Near-End adjustment registers */
        if (pVtInfo->status.neUat == false)
            {
            if ((pVtInfo->parm.sesV.curSec == 0) &&
                (pVtInfo->status.neUatPot  == true))
                {
                mNegClr(&(pVtInfo->parm.esV));
                mNegClr(&(pVtInfo->parm.sesV));
                mNegClr(&(pVtInfo->parm.ppjcVdet));
                mNegClr(&(pVtInfo->parm.npjcVdet));
                mNegClr(&(pVtInfo->parm.ppjcVgen));
                mNegClr(&(pVtInfo->parm.npjcVgen));
                mNegClr(&(pVtInfo->parm.pjcDiffV));
                mNegClr(&(pVtInfo->parm.pjcsVdet));
                mNegClr(&(pVtInfo->parm.pjcsVgen));
                }
            }
        else if ((pVtInfo->parm.sesV.curSec == 1) &&
                 (pVtInfo->status.neUatPot  == false))
            {
            mPosClr(&(pVtInfo->parm.cvV));
            mPosClr(&(pVtInfo->parm.esV));
            mPosClr(&(pVtInfo->parm.sesV));
            mPosClr(&(pVtInfo->parm.ppjcVdet));
            mPosClr(&(pVtInfo->parm.npjcVdet));
            mPosClr(&(pVtInfo->parm.ppjcVgen));
            mPosClr(&(pVtInfo->parm.npjcVgen));
            mPosClr(&(pVtInfo->parm.pjcDiffV));
            mPosClr(&(pVtInfo->parm.pjcsVdet));
            mPosClr(&(pVtInfo->parm.pjcsVgen));
            }

        /* Clear VT Far-End adjustment registers */
        if (pVtInfo->status.feUat == false)
            {
            if ((pVtInfo->parm.sesVfe.curSec == 0) &&
                (pVtInfo->status.feUatPot    == true))
                {
                mNegClr(&(pVtInfo->parm.esVfe));
                mNegClr(&(pVtInfo->parm.sesVfe));
                }
            }
        else if ((pVtInfo->parm.sesVfe.curSec == 1) &&
                 (pVtInfo->status.feUatPot    == false))
            {
            mPosClr(&(pVtInfo->parm.cvVfe));
            mPosClr(&(pVtInfo->parm.esVfe));
            mPosClr(&(pVtInfo->parm.sesVfe));
            }

        /* Store current second statuses */
        pVtInfo->status.neUatPot = (pVtInfo->parm.sesV.curSec   == 1) ? true : false;
        pVtInfo->status.feUatPot = (pVtInfo->parm.sesVfe.curSec == 1) ? true : false;

        /* Accumulate current period registers */
        mCurPerAcc(&(pVtInfo->parm.cvV));
        mCurPerAcc(&(pVtInfo->parm.esV));
        mCurPerAcc(&(pVtInfo->parm.sesV));
        mCurPerAcc(&(pVtInfo->parm.uasV));
        mCurPerAcc(&(pVtInfo->parm.fcV));
        mCurPerAcc(&(pVtInfo->parm.ppjcVdet));
        mCurPerAcc(&(pVtInfo->parm.npjcVdet));
        mCurPerAcc(&(pVtInfo->parm.ppjcVgen));
        mCurPerAcc(&(pVtInfo->parm.npjcVgen));
        mCurPerAcc(&(pVtInfo->parm.pjcDiffV));
        mCurPerAcc(&(pVtInfo->parm.pjcsVdet));
        mCurPerAcc(&(pVtInfo->parm.pjcsVgen));
        mCurPerAcc(&(pVtInfo->parm.cvVfe));
        mCurPerAcc(&(pVtInfo->parm.esVfe));
        mCurPerAcc(&(pVtInfo->parm.sesVfe));
        mCurPerAcc(&(pVtInfo->parm.uasVfe));
        mCurPerAcc(&(pVtInfo->parm.fcVfe));

        /* Check and report TCA */
        blNotf = ((pVtInfo->msgInh.vt == false) &&
                  (pVtInfo->msgInh.tca == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Report TCAs of Path Near-End parameters */
        if ((pVtInfo->status.neUat    == false) &&
            (pVtInfo->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtCv,       &(pVtInfo->parm.cvV),      blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtEs,       &(pVtInfo->parm.esV),      blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtSes,      &(pVtInfo->parm.sesV),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtPpjcVdet, &(pVtInfo->parm.ppjcVdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtNpjcVdet, &(pVtInfo->parm.npjcVdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtPpjcVgen, &(pVtInfo->parm.ppjcVgen), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtNpjcVgen, &(pVtInfo->parm.npjcVgen), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtPjcDiff,  &(pVtInfo->parm.pjcDiffV), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtPjcsVdet, &(pVtInfo->parm.pjcsVdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtPjcsVgen, &(pVtInfo->parm.pjcsVgen), blNotf, &tcaInfo);
            }
        
        /* Report TCAs of Path Far-End parameters */
        if ((pVtInfo->status.feUat    == false) &&
            (pVtInfo->status.feUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtCvVfe,  &(pVtInfo->parm.cvVfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtEsVfe,  &(pVtInfo->parm.esVfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtSesVfe, &(pVtInfo->parm.sesVfe), blNotf, &tcaInfo);
            }
        
        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtUas,    &(pVtInfo->parm.uasV),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurVt, pVtId, pVtInfo, cPmVtUasVfe, &(pVtInfo->parm.uasVfe), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pVtInfo->parm.cvV));
        mStackDown(&(pVtInfo->parm.esV));
        mStackDown(&(pVtInfo->parm.sesV));
        mStackDown(&(pVtInfo->parm.uasV));
        mStackDown(&(pVtInfo->parm.fcV));
        mStackDown(&(pVtInfo->parm.ppjcVdet));
        mStackDown(&(pVtInfo->parm.npjcVdet));
        mStackDown(&(pVtInfo->parm.ppjcVgen));
        mStackDown(&(pVtInfo->parm.npjcVgen));
        mStackDown(&(pVtInfo->parm.pjcDiffV));
        mStackDown(&(pVtInfo->parm.pjcsVdet));
        mStackDown(&(pVtInfo->parm.pjcsVgen));
        mStackDown(&(pVtInfo->parm.cvVfe));
        mStackDown(&(pVtInfo->parm.esVfe));
        mStackDown(&(pVtInfo->parm.sesVfe));
        mStackDown(&(pVtInfo->parm.uasVfe));
        mStackDown(&(pVtInfo->parm.fcVfe));

        /* Update time for period */
        pVtInfo->parm.prePerStartTime = pVtInfo->parm.curPerStartTime;
        pVtInfo->parm.prePerEndTime = dCurTime;
        pVtInfo->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pVtInfo->parm.cvV));
        mDayShift(&(pVtInfo->parm.esV));
        mDayShift(&(pVtInfo->parm.sesV));
        mDayShift(&(pVtInfo->parm.uasV));
        mDayShift(&(pVtInfo->parm.fcV));
        mDayShift(&(pVtInfo->parm.ppjcVdet));
        mDayShift(&(pVtInfo->parm.npjcVdet));
        mDayShift(&(pVtInfo->parm.ppjcVgen));
        mDayShift(&(pVtInfo->parm.npjcVgen));
        mDayShift(&(pVtInfo->parm.pjcDiffV));
        mDayShift(&(pVtInfo->parm.pjcsVdet));
        mDayShift(&(pVtInfo->parm.pjcsVgen));
        mDayShift(&(pVtInfo->parm.cvVfe));
        mDayShift(&(pVtInfo->parm.esVfe));
        mDayShift(&(pVtInfo->parm.sesVfe));
        mDayShift(&(pVtInfo->parm.uasVfe));
        mDayShift(&(pVtInfo->parm.fcVfe));

        /* Update time for day */
        pVtInfo->parm.preDayStartTime = pVtInfo->parm.curDayStartTime;
        pVtInfo->parm.preDayEndTime = dCurTime;
        pVtInfo->parm.curDayStartTime = dCurTime;
        }
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend void SurVtStatUpd(tSurDev         device, 
                                        const tSurVtId *pVtId)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input VT.

Inputs       : device                - device 
               pVtId                 - VT ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurVtStatUpd(tSurDev device, const tSurVtId *pVtId)
    {
    tSurInfo *pSurInfo;
    
    /* Get line information */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) && 
        (pSurInfo->interface.DefStatGet    != null))
        {
        VtAllDefUpd(pSurInfo, pVtId);
        }
    
    /* Declare/terminate failures */
    VtAllFailUpd(pSurInfo, pVtId);
    
    /* Update performance registers */
    VtPmParmUpd(pSurInfo, pVtId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - AU-n
 *
 * File        : surau.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of AU-n
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : friend word SurAuIdFlat(const tSurAuId *pChnId)

Purpose      : This function is used to get flat number of a AU-n

Inputs       : pChnId                 - AU-n ID

Outputs      : None

Return       : Flat number (0 base)
------------------------------------------------------------------------------*/
friend word SurAuIdFlat(const tSurAuId *pChnId)
    {
    word wRes;
    
    /* Get STS ID */
    wRes = 0;
	if (pChnId->aug64 > 0) wRes += ((pChnId->aug64 - 1) * 192);
	if (pChnId->aug16 > 0) wRes += ((pChnId->aug16 - 1) * 48);
	if (pChnId->aug4  > 0) wRes += ((pChnId->aug4 - 1)  * 12);
	if (pChnId->aug1  > 0) wRes += ((pChnId->aug1 - 1)  * 3);
	if (pChnId->au    > 0) wRes += pChnId->au - 1;
	
    return wRes;
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurAu2Sts(const tSurAuId *pChnId, 
                                        tSurStsId      *pStsId, 
                                        eSurStsRate    *pRate)
Purpose      : This function is used to convert an AU-n to STS

Inputs       : pChnId                 - AU-n ID
               
Outputs      : pStsId                - STS ID
               pRate                 - STS rate

Return       : cSurOk
------------------------------------------------------------------------------*/
friend eSurRet SurAu2Sts(const tSurAuId *pChnId, 
                         tSurStsId      *pStsId, 
                         eSurStsRate    *pRate)
    {
    /* Get STS ID */
    pStsId->line    = pChnId->line;
    pStsId->localId = SurAuIdFlat(pChnId) + 1;
	
	/* Get rate */
	if (pRate != null)
	    {
	    if      (pChnId->au    > 0) *pRate = cSurSts1;
	    else if (pChnId->aug1  > 0) *pRate = cSurSts3c;
	    else if (pChnId->aug4  > 0) *pRate = cSurSts12c;
	    else if (pChnId->aug16 > 0) *pRate = cSurSts48c;
	    else if (pChnId->aug64 > 0) *pRate = cSurSts192c;
	    else *pRate = cSurSts768c;
	    }
	    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurAuProv(tSurDev device, tSurAuId *pChnId)

Purpose      : This function is used to provision a AU-n for FM & PM.

Inputs       : device                - Device
               pChnId                - AU-n identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuProv(tSurDev device, tSurAuId *pChnId)
    {
    eSurRet      ret;
    tSurStsId    stsId;
    eSurStsRate  rate;
    tSurStsInfo *pInfo = NULL;
    tSurInfo    *pSurInfo = NULL;
    
    /* Convert to STS ID and provision */
    SurAu2Sts(pChnId, &stsId, &rate);
    ret = SurStsProv(device, &stsId);
    mRetCheck(ret);
    
    /* Set STS rate */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device handle\n");
        return cSurErrNullParm;
        }
    
    /* Get STS information structure */
    mSurStsInfo(pSurInfo, &stsId, pInfo);
    pInfo->conf.rate = rate;
    
    /* Give devive */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurAuDeProv(tSurDev device, tSurAuId *pChnId)

Purpose      : This function is used to de-provision FM & PM of one AU-n. It
               will deallocated all resources.

Inputs       : device                - device
               pChnId                - AU-n identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuDeProv(tSurDev device, tSurAuId *pChnId)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* De-provision */
    return SurStsDeProv(device, &stsId);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuTrblDesgnSet(tSurDev              device, 
                                         eSurAuTrbl           trblType,  
                                         const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set AU-n path trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               tSurTrblDesg          - trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuTrblDesgnSet(tSurDev              device, 
                                 eSurAuTrbl           trblType, 
                                 const tSurTrblDesgn *pTrblDesgn)
    {
    return SurStsTrblDesgnSet(device, (eSurStsTrbl)trblType, pTrblDesgn);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuTrblDesgnGet(tSurDev              device, 
                                         eSurAuTrbl           trblType, 
                                         tSurTrblDesgn       *pTrblDesgn)

Purpose      : This function is used to get AU-n path trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               tSurTrblDesgn         - returned trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuTrblDesgnGet(tSurDev              device, 
                                 eSurAuTrbl           trblType, 
                                 tSurTrblDesgn       *pTrblDesgn)
    {
    return SurStsTrblDesgnGet(device, (eSurStsTrbl)trblType, pTrblDesgn);
    }    

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuEngStatSet(tSurDev           device, 
                                       const tSurAuId   *pChnId, 
                                       eSurEngStat       stat)

Purpose      : This API is used to set engine state for a AU-n channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurAuEngStatSet(tSurDev             device, 
                               const tSurAuId     *pChnId, 
                               eSurEngStat         stat)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Set engine state */
    return SurStsEngStatSet(device, &stsId, stat);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuEngStatGet(tSurDev             device, 
                                       const tSurAuId     *pChnId, 
                                       eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a AU-n channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurAuEngStatGet(tSurDev             device, 
                               const tSurAuId     *pChnId, 
                               eSurEngStat        *pStat)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get engine state */
    return SurStsEngStatGet(device, &stsId, pStat);
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurAuChnCfgSet(tSurDev            device,
                                             const tSurAuId    *pChnId, 
                                             const tSurStsConf *pChnCfg)

Purpose      : This internal API is use to set AU channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuChnCfgSet(tSurDev            device,
                              const tSurAuId    *pChnId, 
                              const tSurStsConf *pChnCfg)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Configure channel */
    return SurStsChnCfgSet(device, &stsId, pChnCfg);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuChnCfgGet(tSurDev          device,
                                       const tSurAuId *pChnId, 
                                       tSurStsConf    *pChnCfg)
    {
Purpose      : This API is used to set AU channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuChnCfgGet(tSurDev           device,
                              const tSurAuId   *pChnId, 
                              tSurStsConf      *pChnCfg)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get channel configuration */
    return SurStsChnCfgGet(device, &stsId, pChnCfg);
    }        
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuTrblNotfInhSet(tSurDev               device,
                                           const tSurAuId       *pChnId, 
                                           eSurAuTrbl            trblType, 
                                           bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a AU-n channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuTrblNotfInhSet(tSurDev               device,
                                   const tSurAuId       *pChnId, 
                                   eSurAuTrbl            trblType, 
                                   bool                  inhibit)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Set trouble notification inhibition */
    return SurStsTrblNotfInhSet(device, &stsId, trblType, inhibit);
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuTrblNotfInhGet(tSurDev               device,
                                           const tSurAuId       *pChnId, 
                                           eSurAuTrbl            trblType, 
                                           bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a AU-n channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuTrblNotfInhGet(tSurDev               device,
                                   const tSurAuId       *pChnId, 
                                   eSurAuTrbl            trblType, 
                                   bool                 *pInhibit)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get trouble notification inhibition */
    return SurStsTrblNotfInhGet(device, &stsId, trblType, pInhibit);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurAuChnTrblNotfInhInfoGet(tSurDev             device, 
                                                  const tSurAuId     *pChnId, 
                                                  tSurStsMsgInh      *pTrblInh)

Purpose      : This API is used to get AU-n channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurAuChnTrblNotfInhInfoGet(tSurDev             device, 
                                          const tSurAuId     *pChnId, 
                                          tSurStsMsgInh      *pTrblInh)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get trouble notification inhibition info */
    return SurStsChnTrblNotfInhInfoGet(device, &stsId, pTrblInh);
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmAuFailIndGet(tSurDev           device, 
                                      const tSurAuId   *pChnId, 
                                      eSurAuTrbl        trblType, 
                                      tSurFailStat     *pFailStat)

Purpose      : This API is used to get failure indication of a AU-n channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmAuFailIndGet(tSurDev           device, 
                              const tSurAuId   *pChnId, 
                              eSurAuTrbl        trblType, 
                              tSurFailStat     *pFailStat)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get failure indication */
    return FmStsFailIndGet(device, &stsId, trblType, pFailStat);
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmAuFailHisGet(tSurDev         device, 
                                               const tSurAuId *pChnId)

Purpose      : This API is used to get a AU-n channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmAuFailHisGet(tSurDev         device, 
                                const tSurAuId *pChnId)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get failure history */
    return FmStsFailHisGet(device, &stsId);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmAuFailHisClear(tSurDev         device, 
                                        const tSurAuId *pChnId, 
                                        bool            flush)

Purpose      : This API is used to clear a AU-n channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmAuFailHisClear(tSurDev device, const tSurAuId *pChnId, bool flush)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Clear failure history */
    return FmStsFailHisClear(device, &stsId, flush);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuRegReset(tSurDev       device, 
                                    tSurAuId     *pChnId, 
                                    ePmAuParm     parm, 
                                    ePmRegType    regType)

Purpose      : This API is used to reset a AU-n channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmAuRegReset(tSurDev       device, 
                            tSurAuId     *pChnId, 
                            ePmAuParm     parm, 
                            ePmRegType    regType)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* reset register */
    return PmStsRegReset(device, &stsId, parm, regType);
    }      
      
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuRegGet(tSurDev           device, 
                                  const tSurAuId   *pChnId, 
                                  ePmAuParm         parm, 
                                  ePmRegType        regType, 
                                  tPmReg           *pRetReg)

Purpose      : This API is used to get a AU-n channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmAuRegGet(tSurDev           device, 
                          const tSurAuId   *pChnId, 
                          ePmAuParm         parm, 
                          ePmRegType        regType, 
                          tPmReg           *pRetReg)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get register's value */
    return PmStsRegGet(device, &stsId, parm, regType, pRetReg);
    }
            
/*------------------------------------------------------------------------------
Prototype    : tSortList PmAuTcaHisGet(tSurDev device, const tSurAuId *pChnId)

Purpose      : This API is used to get a AU-n channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmAuTcaHisGet(tSurDev device, const tSurAuId *pChnId)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get TCA history */
    return PmStsTcaHisGet(device, &stsId);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuTcaHisClear(tSurDev device, const tSurAuId *pChnId)

Purpose      : This API is used to clear an AU-n channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmAuTcaHisClear(tSurDev device, const tSurAuId *pChnId)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Clear TCA history */
    return PmStsTcaHisClear(device, &stsId);
    }       
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmAuParmAccInhSet(tSurDev          device, 
                                          const tSurAuId  *pChnId, 
                                          ePmAuParm        parm, 
                                          bool             inhibit)

Purpose      : This API is used to enable/disable an AU-n channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmAuParmAccInhSet(tSurDev          device, 
                                 const tSurAuId  *pChnId, 
                                 ePmAuParm        parm, 
                                 bool             inhibit)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Set parameter accummulate inhibition mode */
    return PmStsParmAccInhSet(device, &stsId, parm, inhibit);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuParmAccInhGet(tSurDev          device, 
                                         const tSurAuId  *pChnId, 
                                         ePmAuParm        parm, 
                                         bool            *pInhibit)
                                         
Purpose      : This API is used to get an AU-n channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmAuParmAccInhGet(tSurDev          device, 
                                 const tSurAuId  *pChnId, 
                                 ePmAuParm        parm, 
                                 bool            *pInhibit)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get parameter's accummulate inhibition mode */
    return PmStsParmAccInhGet(device, &stsId, parm, pInhibit);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuRegThresSet(tSurDev            device, 
                                       const tSurAuId   *pChnId, 
                                       ePmAuParm         parm, 
                                       ePmRegType        regType, 
                                       dword             thresVal)

Purpose      : This API is used to set threshold for an AU-n channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmAuRegThresSet(tSurDev           device, 
                               const tSurAuId   *pChnId, 
                               ePmAuParm         parm, 
                               ePmRegType        regType, 
                               dword             thresVal)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Set register's threshold */
    return PmStsRegThresSet(device, &stsId, parm, regType, thresVal);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuRegThresGet(tSurDev           device, 
                                       const tSurAuId   *pChnId, 
                                       ePmAuParm         parm, 
                                       ePmRegType        regType, 
                                       dword            *pThresVal)

Purpose      : This API is used to get threshold for an AU-n channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmAuRegThresGet(tSurDev           device, 
                               const tSurAuId   *pChnId, 
                               ePmAuParm         parm, 
                               ePmRegType        regType, 
                               dword            *pThresVal)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get register's theshold */
    return PmStsRegThresGet(device, &stsId, parm, regType, pThresVal);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuParmNumRecRegSet(tSurDev          device, 
                                            const tSurAuId  *pChnId, 
                                            ePmAuParm        parm, 
                                            byte             numRecReg)

Purpose      : This API is used to set a AU-n channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmAuParmNumRecRegSet(tSurDev          device, 
                                    const tSurAuId  *pChnId, 
                                    ePmAuParm        parm, 
                                    byte             numRecReg)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Set number of recent registers */
    return PmStsParmNumRecRegSet(device, &stsId, parm, numRecReg);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmAuParmNumRecRegGet(tSurDev          device, 
                                            const tSurAuId  *pChnId, 
                                            ePmAuParm        parm, 
                                            byte            *pNumRecReg)
                                             
Purpose      : This API is used to get a AU-n channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmAuParmNumRecRegGet(tSurDev          device, 
                                    const tSurAuId  *pChnId, 
                                    ePmAuParm        parm, 
                                    byte            *pNumRecReg)
    {
    tSurStsId   stsId;
    
    /* Convert to STS ID */
    SurAu2Sts(pChnId, &stsId, null);
    
    /* Get number of recent registers */
    return PmStsParmNumRecRegGet(device, &stsId, parm, pNumRecReg);
    }
    
            

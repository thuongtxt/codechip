/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : sureng.c
 *
 * Created Date: 06-Jan-09
 *
 * Description : This file contain all source code Transmisstion Surveillance
 *               engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/
/* Number of timers */
#define cSurNumTimer                 1

/* Maximum number of served defects from defect queue in an interval */
#define cSurMaxServDef               100
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Timer information */
private dword dTmPool;
private dword dTmId;

/* Task information */
private osalTaskId engTask;

/* Timer information */
private tSurTime engTime;   /* Temporary time, used to check if period or day 
                               expired */
friend  tSurTime curTime;   /* Current time in details */
friend  dword    dCurTime;  /* Current time (in second) */
friend  bool     blSecExpr; /* Second timer expired indication */
friend  bool     blPerExpr; /* Period timer expired indication */
friend  bool     blDayExpr; /* Day timer expired indication */

/* Invalid flags */
friend bool blInvlPer; /* Invalid period due to system time has been changed */
friend bool blInvlDay; /* Invalid day due to system time has been changed */

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of 
input Linear APS engine.
------------------------------------------------------------------------------*/
extern void SurApslStatUpd(tSurDev device, const tSurApslId *pApslId);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input Linear APS.
------------------------------------------------------------------------------*/
extern void SurApslDefUpd(tSurApslInfo  *pApslInfo, 
                          tSurLineInfo  *pProtLineInfo, 
                          tSurApslDef   *pApslDef);
                              
/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of an
input line.
------------------------------------------------------------------------------*/
extern void SurLineStatUpd(tSurDev device, const tSurLineId *pLineId);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input line.
------------------------------------------------------------------------------*/
extern void SurLineDefUpd(tSurLineInfo *pLineInfo, tSurLineDef *pLineDef);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of 
input STS.
------------------------------------------------------------------------------*/
extern void SurStsStatUpd(tSurDev device, const tSurStsId *pStsId);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input STS.
------------------------------------------------------------------------------*/
extern void SurStsDefUpd(tSurStsInfo *pStsInfo, tSurStsDef *pStsDef);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of 
input VT.
------------------------------------------------------------------------------*/
extern void SurVtStatUpd(tSurDev device, const tSurVtId *pVtId);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input VT.
------------------------------------------------------------------------------*/
extern void SurVtDefUpd(tSurVtInfo *pVtInfo, tSurVtDef *pVtDef);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of 
input TU-3.
------------------------------------------------------------------------------*/
extern void SurTu3StatUpd(tSurDev device, const tSurTu3Id *pTu3Id);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input TU-3.
------------------------------------------------------------------------------*/
extern void SurTu3DefUpd(tSurTu3Info *pDe3Info, tSurTu3Def *pTu3Def);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of 
input DS1/E1.
------------------------------------------------------------------------------*/
extern void SurDe1StatUpd(tSurDev device, const tSurDe1Id *pDe1Id);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of
input IMA Link
------------------------------------------------------------------------------*/
extern void SurImaLinkStatUpd(tSurDev device, const tSurImaLinkId *pImaLinkId);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of
input IMA Group
------------------------------------------------------------------------------*/
extern void SurImaGrpStatUpd(tSurDev device, const tSurImaGrpId *pImaGrpId);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of PW
------------------------------------------------------------------------------*/
extern void SurPwStatUpd(tSurDev device, const tSurPwId *pPwId);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input DS1/E1.
------------------------------------------------------------------------------*/
extern void SurDe1DefUpd(tSurDe1Info *pDe1Info, tSurDe1Def *pDe1Def);

/*------------------------------------------------------------------------------
This function is used to update all defect statuses and anomaly counters of 
input DS3/E3.
------------------------------------------------------------------------------*/
extern void SurDe3StatUpd(tSurDev device, const tSurDe3Id *pDe3Id);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input DS3/E3.
------------------------------------------------------------------------------*/
extern void SurDe3DefUpd(tSurDe3Info *pDe3Info, tSurDe3Def *pDe3Def);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input IMA Link
------------------------------------------------------------------------------*/
extern void SurImaLinkDefUpd(tSurImaLinkInfo *pImaLinkInfo, tSurImaLinkDef *pImaLinkDef);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an input IMA Group
------------------------------------------------------------------------------*/
extern void SurImaGrpDefUpd(tSurImaGrpInfo *pImaGrpInfo, tSurImaGrpDef *pImaGrpDef);

/*------------------------------------------------------------------------------
This function is used to update a defect status of an PW
------------------------------------------------------------------------------*/
extern void SurPwDefUpd(tSurPwInfo *pPwInfo, tSurPwDef *pPwDef);
void EngPoll(void);
/*------------------------------------------------------------------------------
Prototype    : friend eListCmpResult SurFailCmp(void *pFail1, void *pFail2)

Purpose      : This function is used to compare two failures

Inputs       : pFail1                - failure 1
               pFail2                - failure 2

Outputs      : None

Return       : cListEqual            - fail1 = fail2
               cListGreater          - fail1 > fail2
               cListLower            - fail1 < fail2
               cListCmpFail          - compare fail
------------------------------------------------------------------------------*/
friend eListCmpResult SurFailCmp(void *pFail1, void *pFail2)
    {
    tFmFailInfo    *pFailure1;
    tFmFailInfo    *pFailure2;
    
    /* Initialize */
    pFailure1 = (tFmFailInfo *)pFail1;
    pFailure2 = (tFmFailInfo *)pFail2;
    
    /* Compare trouble time */
    if (pFailure1->time > pFailure2->time)
        {
        return cListGreater;
        }
    if (pFailure1->time < pFailure2->time)
        {
        return cListLower;
        }
    
    /* Compare trouble designation */
    if (pFailure1->trblDesgn.alarmType < pFailure2->trblDesgn.alarmType)
        {
        return cListGreater;
        }
    else if (pFailure1->trblDesgn.alarmType > pFailure2->trblDesgn.alarmType)
        {
        return cListLower;
        }
    
    /* Compare alarm severity */
    if (pFailure1->trblDesgn.alarmType == cSurAlarm)
        {
        if (pFailure1->trblDesgn.alarmSev < pFailure2->trblDesgn.alarmSev)
            {
            return cListGreater;
            }
        if (pFailure1->trblDesgn.alarmSev > pFailure2->trblDesgn.alarmSev)
            {
            return cListLower;
            }
        }
    
    /* Compare service affecting */
    if (pFailure1->trblDesgn.serviceAff < pFailure2->trblDesgn.serviceAff)
        {
        return cListGreater;
        }
    if (pFailure1->trblDesgn.serviceAff > pFailure2->trblDesgn.serviceAff)
        {
        return cListLower;
        }
    
    /* Compare defect type */
    if (pFailure1->trblType > pFailure2->trblType)
        {
        return cListGreater;
        }
    if (pFailure1->trblType < pFailure2->trblType)
        {
        return cListLower;
        }
    
    /* Compare status */
    if (pFailure1->status < pFailure2->status)
        {
        return cListGreater;
        }
    if (pFailure1->status > pFailure2->status)
        {
        return cListLower;
        }
    
    return cListEqual;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eListCmpResult SurTcaCmp(void *pTca1, void *pTca2)

Purpose      : This function is used to compare two TCAs

Inputs       : pTca1                 - TCA 1
               pTca2                 - TCA 2

Outputs      : None

Return       : cListEqual            - pTca1 = pTca2
               cListGreater          - pTca1 > pTca2
               cListLower            - pTca1 < pTca2
               cListCmpFail          - compare fail
------------------------------------------------------------------------------*/
friend eListCmpResult SurTcaCmp(void *pTca1, void *pTca2)
    {
    tPmTcaInfo *pInfo1;
    tPmTcaInfo *pInfo2;
    
    /* Initialize */
    pInfo1 = (tPmTcaInfo*)pTca1;
    pInfo2 = (tPmTcaInfo*)pTca2;
    
    /* Compare time */
    if (pInfo1->time > pInfo2->time)
        {
        return cListGreater;
        }
    if (pInfo1->time < pInfo2->time)
        {
        return cListLower;
        }
        
    /* Compare parameter type */
    if (pInfo1->parm < pInfo2->parm)
        {
        return cListGreater;
        }
    if (pInfo1->parm > pInfo2->parm)
        {
        return cListLower;
        }
    
    /* Compare register type */
    if (pInfo1->reg < pInfo2->reg)
        {
        return cListGreater;
        }
    if (pInfo1->reg > pInfo2->reg)
        {
        return cListLower;
        }
    
    return cListEqual;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurTcaReport(tSurInfo    *pSurInfo,
                                        eSurChnType  chnType,     
                                        const void  *pChnId,
                                        void        *pChnInfo,
                                        byte         parmType,
                                        tPmParm     *pParm,
                                        bool         notify,
                                        tPmTcaInfo  *pTcaInfo)

Purpose      : This function is used to check and report TCA for each channel's
               parameter

Inputs       : pSurInfo              - Device database
               chnType               - channel type
               pChnId                - Channel ID
               pChnInfo              - Channel database
               parmType              - parameter type
               pParm                 - parameter structure
               notify                - true to notify TCA (if exist)
               pTcaInfo              - TCA information (to increase performance)

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurTcaReport(tSurInfo    *pSurInfo,
                         eSurChnType  chnType,     
                         const void  *pChnId,
                         void        *pChnInfo,
                         byte         parmType,
                         tPmParm     *pParm,
                         bool         notify,
                         tPmTcaInfo  *pTcaInfo)
    {
    bool      blCurPerTca;
    bool      blCurDayTca;
    bool      blPrePerTca;
    bool      blPreDayTca;
    tSortList tcaHis;

    /* Check if TCA need to be reported */
    mTcaCheck(pParm, blCurPerTca, blCurDayTca, blPrePerTca, blPreDayTca);
    
    /* Get TCA history */
    switch ((uint32)chnType)
        {
        /* Line */
        case cSurLine:
            tcaHis = ((tSurLineInfo*)pChnInfo)->tcaHis;
            break;
        
        /* STS */
        case cSurSts:
            tcaHis = ((tSurStsInfo*)pChnInfo)->tcaHis;
            break;
        
        /* VT */
        case cSurVt:
            tcaHis = ((tSurVtInfo*)pChnInfo)->tcaHis;
            break;
        
        /* AU-n */    
        case cSurAu:
            tcaHis = ((tSurAuInfo*)pChnInfo)->tcaHis;
            break;
            
        /* TU-3 */    
        case cSurTu3:
            tcaHis = ((tSurTu3Info*)pChnInfo)->tcaHis;
            break;
            
        /* TU */    
        case cSurTu:
            tcaHis = ((tSurTuInfo*)pChnInfo)->tcaHis;
            break;
            
        /* DS3/E3 */    
        case cSurDe3:
            tcaHis = ((tSurDe3Info*)pChnInfo)->tcaHis;
            break;
            
        /* DS1/E1 */    
        case cSurDe1:
            tcaHis = ((tSurDe1Info*)pChnInfo)->tcaHis;
            break;
            
        /* IMA Link */
        case cSurImaLink:
            tcaHis = ((tSurImaLinkInfo*)pChnInfo)->tcaHis;
            break;

        /* IMA Group */
        case cSurImaGrp:
            tcaHis = ((tSurImaGrpInfo*)pChnInfo)->tcaHis;
            break;

        /* PW */
        case cSurPw:
            tcaHis = ((tSurPwInfo*)pChnInfo)->tcaHis;
            break;

        /* Not supported */    
        default:
            mSurDebug(cSurWarnMsg, "Channel type is not supported\n");
            tcaHis = null;
            break;
        }

    /* Current period */
    pTcaInfo->thres = pParm->perThres;
    pTcaInfo->parm  = parmType;
    if (blCurPerTca == true)
        {
        pTcaInfo->reg   = cPmCurPer;
        pTcaInfo->value = pParm->curPer.value;
        pParm->perTca   = true;

        /* Add to TCA history */
        if (ListAdd(tcaHis, pTcaInfo, sizeof(tPmTcaInfo)) != cListSucc)
            {
            mSurDebug(cSurWarnMsg, "Cannot add TCA to TCA history\n");
            }

        /* Notify this TCA */
        if (notify == true)
            {
            pSurInfo->interface.TcaNotf(pSurInfo->pHdl, chnType, pChnId, pTcaInfo);
            }
        }

    /* Previous period */
    if (blPrePerTca == true)
        {
        pTcaInfo->reg    = cPmPrePer;
        pTcaInfo->value  = pParm->prePer.value;
        pParm->prePerTca = true;

        /* Add to TCA history */
        if (ListAdd(tcaHis, pTcaInfo, sizeof(tPmTcaInfo)) != cListSucc)
            {
            mSurDebug(cSurWarnMsg, "Cannot add TCA to TCA history\n");
            }

        /* Notify this TCA */
        if (notify == true)
            {
            pSurInfo->interface.TcaNotf(pSurInfo->pHdl, chnType, pChnId, pTcaInfo);
            }
        }

    /* Current day */
    pTcaInfo->thres = pParm->dayThres;
    if (blCurDayTca == true)
        {
        pTcaInfo->reg   = cPmCurDay;
        pTcaInfo->value = pParm->curDay.value;
        pParm->dayTca   = true;

        /* Add to TCA history */
        if (ListAdd(tcaHis, pTcaInfo, sizeof(tPmTcaInfo)) != cListSucc)
            {
            mSurDebug(cSurWarnMsg, "Cannot add TCA to TCA history\n");
            }

        /* Notify this TCA */
        if (notify == true)
            {
            pSurInfo->interface.TcaNotf(pSurInfo->pHdl, chnType, pChnId, pTcaInfo);
            }
        }

    /* Previous day */
    if (blPreDayTca == true)
        {
        pTcaInfo->reg    = cPmPreDay;
        pTcaInfo->value  = pParm->preDay.value;
        pParm->preDayTca = true;

        /* Add to TCA history */
        if (ListAdd(tcaHis, pTcaInfo, sizeof(tPmTcaInfo)) != cListSucc)
            {
            mSurDebug(cSurWarnMsg, "Cannot add TCA to TCA history\n");
            }

        /* Notify this TCA */
        if (notify == true)
            {
            pSurInfo->interface.TcaNotf(pSurInfo->pHdl, chnType, pChnId, pTcaInfo);
            }
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void DevicePoll(tSurDev device)

Purpose      : This function is used to poll anomaly counters and check defect
               status periodically for each device

Inputs       : None

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*private*/ void DevicePoll(tSurDev device)
    {
    tSurInfo       *pSurInfo;
    word            wLine;
    word            wSts;
    byte            bVt;
    word			wImaIdx;
    word			wPwIdx;
    byte            bSerDef;
    dword           dSize;
    bool            isQueEmpty;

    tSurLineDb     *pLineDb;
    tSurStsDb      *pStsDb;
    tSurDe3Db      *pDe3Db;

    tSurVtId        vtId;
    tSurTu3Id       tu3Id;
    tSurDe1Id       de1Id;
    tSurApslId      apslId;
    tSurImaLinkId   imaLinkId;
    tSurImaGrpId    imaGrpId;
    tSurPwId    	pwId;

    tSurDefQue     *pDefQue;
    tSurLineDef     lineDef;
    tSurStsDef      stsDef;
    tSurTu3Def      tu3Def;
    tSurVtDef       vtDef;
    tSurDe3Def      de3Def;
    tSurDe1Def      de1Def;
    tSurImaLinkDef	imaLinkDef;
    tSurImaGrpDef	imaGrpDef;
    tSurPwDef		pwDef;
    tSurApslDef     apslDef;

    tSurLineInfo   *pLineInfo;
    tSurStsInfo    *pStsInfo;
    tSurVtInfo     *pVtInfo;
    tSurTu3Info    *pTu3Info;
    tSurDe3Info    *pDe3Info;
    tSurDe1Info    *pDe1Info;
    tSurApslInfo   *pApslInfo;
    tSurImaLinkInfo  *pImaLinkInfo;
    tSurImaGrpInfo   *pImaGrpInfo;
    tSurPwInfo   	*pPwInfo;
    
    pStsInfo = NULL;
    pVtInfo	 = NULL;
    pTu3Info = NULL;
    pDe1Info = NULL;
    pImaLinkInfo = NULL;
    pImaGrpInfo = NULL;
    pPwInfo = NULL;

    /* Take device handle */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return;
        }
    
    /* Check if device engine was stopped and give device */
    if (pSurInfo->pDb->state == cSurStop)
        {
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurWarnMsg, "Cannot give device\n");
            }
        return;
        }
        
    /* Scan defect queue if Service Mode is enable */
    bSerDef    = 0;
    isQueEmpty = false;
    pDefQue = pSurInfo->pDb->pDefQue;
    if (pSurInfo->pDb->mastConf.defServMd == cSurDefServ)
        {
        while ((isQueEmpty == false) && (bSerDef < cSurMaxServDef))
            {
            /* Scan for Line defect */
            dSize   = 0;
            LQueSize(pDefQue->line, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&lineDef, sizeof(tSurLineDef), 0);
                LQueRmv(pDefQue->line, &lineDef, sizeof(tSurLineDef));

                mSurLineInfo(pSurInfo, &(lineDef.id), pLineInfo);
                if ((pLineInfo != null) &&
                    (pLineInfo->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurLineDefUpd(pLineInfo, &lineDef);
                    }
                bSerDef= (uint8)(bSerDef + 1);
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for Linear APS engine defect */
            dSize   = 0;
            LQueSize(pDefQue->apsl, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&apslDef, sizeof(tSurApslDef), 0);
                LQueRmv(pDefQue->apsl, &apslDef, sizeof(tSurApslDef));

                mSurApslInfo(pSurInfo, &(apslDef.id), pApslInfo);
                if ((pApslInfo != null) &&
                    (pApslInfo->status.state == cSurStart))
                    {
                    mSurLineInfo(pSurInfo, &(pApslInfo->conf.protLine), pLineInfo);

                    /* Update defect status in DB */
                    SurApslDefUpd(pApslInfo, pLineInfo, &apslDef);
                    }
                bSerDef = (uint8)(bSerDef + 1);
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for STS defect */
            dSize = 0;
            LQueSize(pDefQue->sts, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&stsDef, sizeof(tSurStsDef), 0);
                LQueRmv(pDefQue->sts, &stsDef, sizeof(tSurStsDef));

                mSurStsInfo(pSurInfo, &(stsDef.id), pStsInfo);
                if ((pStsInfo != null) &&
                    (pStsInfo->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurStsDefUpd(pStsInfo, &stsDef);
                    }
                bSerDef = (uint8)(bSerDef + 1);
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for VT defect */
            dSize = 0;
            LQueSize(pDefQue->vt, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&vtDef, sizeof(tSurVtDef), 0);
                LQueRmv(pDefQue->vt, &vtDef, sizeof(tSurVtDef));

                mSurVtInfo(pSurInfo, &(vtDef.id), pVtInfo);
                if ((pVtInfo != null) &&
                    (pVtInfo->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurVtDefUpd(pVtInfo, &vtDef);
                    }
                bSerDef += 1;
                }
            else
                {
                isQueEmpty = true;
                }
            
            /* Scan for TU-3 defect */
            dSize = 0;
            LQueSize(pDefQue->tu3, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&tu3Def, sizeof(tSurTu3Def), 0);
                LQueRmv(pDefQue->tu3, &tu3Def, sizeof(tSurTu3Def));

                mSurTu3Info(pSurInfo, &(tu3Def.id), pTu3Info);
                if ((pTu3Info != null) &&
                    (pTu3Info->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurTu3DefUpd(pTu3Info, &tu3Def);
                    }
                bSerDef += 1;
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for DS3/E3 defect */
            dSize = 0;
            LQueSize(pDefQue->de3, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&de3Def, sizeof(tSurDe3Def), 0);
                LQueRmv(pDefQue->de3, &de3Def, sizeof(tSurDe3Def));

                mSurDe3Info(pSurInfo, &(de3Def.id), pDe3Info);
                if ((pDe3Info != null) &&
                    (pDe3Info->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurDe3DefUpd(pDe3Info, &de3Def);
                    }
                bSerDef += 1;
                }
            else
                {
                isQueEmpty = true;
                }
            
            /* Scan for DS1/E1 defect */
            dSize = 0;
            LQueSize(pDefQue->de1, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&de1Def, sizeof(tSurDe1Def), 0);
                LQueRmv(pDefQue->de1, &de1Def, sizeof(tSurDe1Def));
                
                mSurDe1Info(pSurInfo, &(de1Def.id), pDe1Info);
                if ((pDe1Info != null) &&
                    (pDe1Info->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurDe1DefUpd(pDe1Info, &de1Def);
                    }
                bSerDef += 1;
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for IMA Link defect */
            dSize = 0;
            LQueSize(pDefQue->imaLink, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&imaLinkDef, sizeof(tSurImaLinkDef), 0);
                LQueRmv(pDefQue->imaLink, &imaLinkDef, sizeof(tSurImaLinkDef));

                mSurImaLinkInfo(pSurInfo, &(imaLinkDef.id), pImaLinkInfo);
                if ((pImaLinkInfo != null) &&
                    (pImaLinkInfo->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurImaLinkDefUpd(pImaLinkInfo, &imaLinkDef);
                    }
                bSerDef += 1;
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for IMA Group defect */
            dSize = 0;
            LQueSize(pDefQue->imaGrp, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&imaGrpDef, sizeof(tSurImaGrpDef), 0);
                LQueRmv(pDefQue->imaGrp, &imaGrpDef, sizeof(tSurImaGrpDef));

                mSurImaGrpInfo(pSurInfo, &(imaGrpDef.id), pImaGrpInfo);
                if ((pImaGrpInfo != null) &&
                    (pImaGrpInfo->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurImaGrpDefUpd(pImaGrpInfo, &imaGrpDef);
                    }
                bSerDef = (byte)(bSerDef +1);
                }
            else
                {
                isQueEmpty = true;
                }

            /* Scan for PW defect */
            dSize = 0;
            LQueSize(pDefQue->pw, &dSize);
            if (dSize > 0)
                {
                isQueEmpty = false;
                OsalMemInit(&pwDef, sizeof(tSurPwDef), 0);
                LQueRmv(pDefQue->pw, &pwDef, sizeof(tSurPwDef));

                mSurPwInfo(pSurInfo, &(pwDef.id), pPwInfo);
                if ((pPwInfo != null) &&
                    (pPwInfo->status.state == cSurStart))
                    {
                    /* Update defect status in DB */
                    SurPwDefUpd(pPwInfo, &pwDef);
                    }
                bSerDef = (byte)(bSerDef +1);
                }
            else
                {
                isQueEmpty = true;
                }
            }
        }
    
    /* Scan all Linear APS engines */
    for (wLine = 0; wLine < cSurMaxApslEng; wLine++)
        {
        apslId = (tSurApslId)(wLine + 1);
        if ((mChnDb(pSurInfo).pApsl[wLine]               != null) && 
            (mChnDb(pSurInfo).pApsl[wLine]->status.state == cSurStart))
            {
            SurApslStatUpd(pSurInfo, &apslId);
            }
        }
    
    /* Scan all channels */
    for (wLine = 0; wLine < cSurMaxLine; wLine++)
        {
        pLineDb = mChnDb(pSurInfo).pLine[wLine];
        vtId.sts.line = (tSurLineId)(wLine + 1);
        if (pLineDb != null)
            {
            /* Update status for this line */
            if ((pLineDb->pInfo               != null) && 
                (pLineDb->pInfo->status.state == cSurStart))
                {
                SurLineStatUpd(pSurInfo, &(vtId.sts.line));
                }
            
            /* Update status for all STSs in line */
            for (wSts = 0; wSts < pLineDb->numSts; wSts++)
                {
                pStsDb = pLineDb->ppStsDb[wSts];
                vtId.sts.localId = wSts + 1;
                if (pStsDb != null)
                    {
                    if ((pStsDb->pInfo               != null) && 
                        (pStsDb->pInfo->status.state == cSurStart))
                        {
                        SurStsStatUpd(pSurInfo, &(vtId.sts));
                        }
                    
                    /* Update status for TU3 */
                    if ((pStsDb->pTu3Info               != null) &&
                        (pStsDb->pTu3Info->status.state == cSurStart))
                        {
                        if (SurTu3FromStsGet(&(vtId.sts), &tu3Id) != cSurOk)
                            {
                            mSurDebug(cSurErrMsg, "Cannot get TU3 ID\n");
                            }
                        else
                            {
                            SurTu3StatUpd(pSurInfo, &tu3Id);
                            }
                        }
                    
                    /* Update status for all VTs in STS */
                    for (bVt = 0; bVt < cSurMaxVtInSts; bVt++)
                        {
                        if ((pStsDb->ppVtInfo[bVt]               != null) && 
                            (pStsDb->ppVtInfo[bVt]->status.state == cSurStart))
                            {
                            mSurVtIdUnFlat(bVt, &(vtId));
                            SurVtStatUpd(pSurInfo, &vtId);
                            }
                        }
                    }
                }
            }
        }
    
    /* Scan all DS3/E3 channels */
    for (wLine = 0; wLine < cSurMaxDe3Line; wLine++)
        {
        pDe3Db = mChnDb(pSurInfo).pDe3[wLine];
        if (pDe3Db != null)
            {
            de1Id.de3Id = wLine + 1;
            if ((pDe3Db->pInfo               != null) && 
                (pDe3Db->pInfo->status.state == cSurStart))
                {
                SurDe3StatUpd(pSurInfo, &(de1Id.de3Id));
                }
            
            /* Update all DS1s/E1s in this channel */
            for (bVt = 0; bVt < cSurMaxDe1InDe3; bVt++)
                {
                if ((pDe3Db->pDe1Db[bVt]               != null) && 
                    (pDe3Db->pDe1Db[bVt]->status.state == cSurStart))
                    {
                    mSurDe1IdUnFlat(bVt, &(de1Id));
                    SurDe1StatUpd(pSurInfo, &de1Id);
                    }
                }
            }
        }
    
    /* Scan all IMA Link */
    for (wImaIdx = 0; wImaIdx < cSurMaxImaLink; wImaIdx++)
        {
        imaLinkId = wImaIdx + 1;
        if ((mChnDb(pSurInfo).pImaLink[wImaIdx]               != null) &&
            (mChnDb(pSurInfo).pImaLink[wImaIdx]->status.state == cSurStart))
            {
            SurImaLinkStatUpd(pSurInfo, &imaLinkId);
            }
        }

    /* Scan all IMA Group */
    for (wImaIdx = 0; wImaIdx < cSurMaxImaGrp; wImaIdx++)
        {
        imaGrpId = wImaIdx + 1;
        if ((mChnDb(pSurInfo).pImaGrp[wImaIdx]               != null) &&
            (mChnDb(pSurInfo).pImaGrp[wImaIdx]->status.state == cSurStart))
            {
            SurImaGrpStatUpd(pSurInfo, &imaGrpId);
            }
        }

    /* Scan all PW */
    for (wPwIdx = 0; wPwIdx < cSurMaxPw; wPwIdx++)
        {
        pwId = wPwIdx + 1;
        if ((mChnDb(pSurInfo).pPw[wPwIdx]               != null) &&
            (mChnDb(pSurInfo).pPw[wPwIdx]->status.state == cSurStart))
            {
            SurPwStatUpd(pSurInfo, &pwId);
            }
        }

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurWarnMsg, "Cannot give devive\n");
        }
    
    return;
    }
    

/*------------------------------------------------------------------------------
Prototype    : private void EngPoll()

Purpose      : This function is used to poll anomaly counters and check defect
               status periodically

Inputs       : None

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*private*/ void EngPoll(void)
    {
    byte     bDev;
    
    /* Take database */
    if (mDbTake() != cSurOk)
        {
        mSurDebug(cSurWarnMsg, "Cannot take database\n");
        return;
        }
    
    /* Get current system time */
    SurCurTime(&curTime);
    
    /* Check if period is invalid due to system time has been changed */
    
    /* Check if day is invalid due to system time has been changed */
    
    /* Check if second timer expired */
    if (curTime.second != engTime.second)
        {
        blSecExpr      = true; /* TODO: relate to second timer, need research, other thing like period timer and day timer is near here */
        engTime.second = curTime.second;
        }
    else
        {
        blSecExpr = false;
        }
    
    /* Check if current period expired */
    if (curTime.minute != engTime.minute)
        {
        engTime.minute = curTime.minute;
        if ((curTime.minute % cSurDefaultPmPeriod) == 0)
            {
            blPerExpr = true;
            }
        }
    else
        {
        blPerExpr = false;
        }
    
    /* Check if day expired */
    if (curTime.day != engTime.day)
        {
        blDayExpr   = true;
        engTime.day = curTime.day;
        }
    else
        {
        blDayExpr = false;
        }
    
    /* Polling all devices */
    for (bDev = 0; bDev < cSurMaxDev; bDev++)
        {
        SurCurTimeInS(&dCurTime);
        if (surDb.devices[bDev] != null)
            {
            DevicePoll(surDb.devices[bDev]);
            }
        }
    
    /* Give database */
    if (mDbGive() != cSurOk)
        {
        mSurDebug(cSurWarnMsg, "Cannot give database\n");
        return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void *EngFunc(void *pData)

Purpose      : Engine function

Inputs       : None

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
private void *EngFunc(void *pData)
    {
    /* Set task cancel type */
    if(OsalTaskCancelAttrSet(PTHREAD_CANCEL_DEFERRED) != cOsalOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set task cancel type for engine\n");
        return NULL;
        }

    /* Poll */
    if (OsalTmStart(dTmPool, dTmId, cSurEngPollTime) == cOsalOk)
        {
        while(true == true)
	        {
	        OsalTaskCancelCheck();
			if (OsalTmIsTimeout(dTmPool, dTmId) == cOsalOk)
		        {
		        if(OsalTmStop(dTmPool, dTmId) == cOsalOk)
		            {
					EngPoll();
    	            (void)OsalTmRestart(dTmPool, dTmId, cSurEngPollTime);
		            }
		        }
	        }
        }
    else
        {
        mSurDebug(cSurErrMsg, "Cannot start timer\n");
        }

    return null;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurEngStart()

Purpose      : This function is used to start Surveillance engine

Inputs       : None

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
void SurEngDbStart(void)
    {
    SurCurTimeInS(&dCurTime);
    SurCurTime(&engTime);
    blInvlPer = false;
    blInvlDay = false;
    blSecExpr = false;
    blPerExpr = false;
    blDayExpr = false;
    }

friend eSurRet SurEngStart(void)
    {
    SurEngDbStart();
    
    /* Create timer pool */
    if (OsalTmPoolCreate(cSurNumTimer, &dTmPool) != cOsalOk)
        {
        mSurDebug(cSurErrMsg, "Cannot create timer pool\n");
        return cSurErr;
        }

    /* Create engine */
    if (OsalTmCreate(dTmPool, &dTmId) != cOsalOk)
        {
        mSurDebug(cSurErrMsg, "Cannot create timer for engine\n");
        return cSurErr;
        }
    if (OsalTaskCreate(&engTask, null, EngFunc, null) != cOsalOk)
        {
        mSurDebug(cSurErrMsg, "Cannot create engine\n");
        return cSurErr;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurEngStop()

Purpose      : This function is used to stop Surveillance engine

Inputs       : None

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurEngStop(void)
    {
    OsalTaskCancel(engTask);
    OsalTaskWaitForTerm(engTask, null);

    /* Destroy timer */
    if (OsalTmDestroy(dTmPool, dTmId) != cOsalOk)
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy timer\n");
        }
    if (OsalTmPoolDestroy(dTmPool) != cOsalOk)
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy timer pool\n");
        }
    
    return cSurOk;
    }

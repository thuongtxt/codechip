/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - DS1/E1
 *
 * File        : surde1.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of DS1/E1
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1DefParmSet(tSurDe1Info *pInfo)

Purpose      : This function is used to set default values for DS1/E1 path's PM 
               parameters.

Inputs       : pInfo                 - line channel info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1DefParmSet(tSurDe1Info *pInfo)
    {
    /* Near-end DS1/E1 line parameters */
    mPmDefParmSet(&(pInfo->parm.cvL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesL), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.lossL), cPmPerThresVal, cPmDayThresVal);
    
    /* Near-end DS1/E1 path parameters */
    mPmDefParmSet(&(pInfo->parm.cvP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esaP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esbP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.aissP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.aissCiP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sasP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.cssP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasP), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcP), cPmPerThresVal, cPmDayThresVal);
    
    /* Far-end DS1/E1 path parameters */
    mPmDefParmSet(&(pInfo->parm.esLfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sefsPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.cvPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esaPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esbPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.cssPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcPfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasPfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }
/*------------------------------------------------------------------------------
Prototype    : private tSurDe1Info *De1InfoCreate(tSurInfo  *pSurInfo, 
                                                  tSurDe1Id *pDe1Id)

Purpose      : This function is used to allocate resources for DS1/E1

Inputs       : pSurInfo              - database handle of device
               pDe1Id                - DS1/E1 id

Outputs      : None

Return       : tSurDe1Info*          - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurDe1Info *De1InfoCreate(tSurInfo *pSurInfo, tSurDe1Id *pDe1Id)
    {
    tSurDe1Info *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurDe1Info));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurDe1Info), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.cvL));
        mPmParmInit(&(pChnInfo->parm.esL));
        mPmParmInit(&(pChnInfo->parm.sesL));
        mPmParmInit(&(pChnInfo->parm.lossL));
        mPmParmInit(&(pChnInfo->parm.cvP));
        mPmParmInit(&(pChnInfo->parm.esP));
        mPmParmInit(&(pChnInfo->parm.esaP));
        mPmParmInit(&(pChnInfo->parm.esbP));
        mPmParmInit(&(pChnInfo->parm.sesP));
        mPmParmInit(&(pChnInfo->parm.aissP));
        mPmParmInit(&(pChnInfo->parm.aissCiP));
        mPmParmInit(&(pChnInfo->parm.sasP));
        mPmParmInit(&(pChnInfo->parm.cssP));
        mPmParmInit(&(pChnInfo->parm.uasP));
        mPmParmInit(&(pChnInfo->parm.fcP));
        mPmParmInit(&(pChnInfo->parm.esLfe));
        mPmParmInit(&(pChnInfo->parm.cvPfe));
        mPmParmInit(&(pChnInfo->parm.sefsPfe));
        mPmParmInit(&(pChnInfo->parm.esPfe));
        mPmParmInit(&(pChnInfo->parm.esaPfe));
        mPmParmInit(&(pChnInfo->parm.esbPfe));
        mPmParmInit(&(pChnInfo->parm.sesPfe));
        mPmParmInit(&(pChnInfo->parm.cssPfe));
        mPmParmInit(&(pChnInfo->parm.fcPfe));
        mPmParmInit(&(pChnInfo->parm.uasPfe));

        /* Add to database */
        mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(&(pDe1Id->de3Id))]->pDe1Db[mSurDe1IdFlat(pDe1Id)] = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurDe1InfoFinal(tSurDe1Info *pInfo)

Purpose      : This function is used to deallocate all resource in DS1/E1 
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurDe1InfoFinal(tSurDe1Info *pInfo)
    {
    /* Finalize all parameters */
    mPmParmFinal(&(pInfo->parm.cvL));
    mPmParmFinal(&(pInfo->parm.esL));
    mPmParmFinal(&(pInfo->parm.sesL));
    mPmParmFinal(&(pInfo->parm.lossL));
    mPmParmFinal(&(pInfo->parm.cvP));
    mPmParmFinal(&(pInfo->parm.esP));
    mPmParmFinal(&(pInfo->parm.esaP));
    mPmParmFinal(&(pInfo->parm.esbP));
    mPmParmFinal(&(pInfo->parm.sesP));
    mPmParmFinal(&(pInfo->parm.aissP));
    mPmParmFinal(&(pInfo->parm.aissCiP));
    mPmParmFinal(&(pInfo->parm.sasP));
    mPmParmFinal(&(pInfo->parm.cssP));
    mPmParmFinal(&(pInfo->parm.uasP));
    mPmParmFinal(&(pInfo->parm.fcP));
    mPmParmFinal(&(pInfo->parm.esLfe));
    mPmParmFinal(&(pInfo->parm.cvPfe));
    mPmParmFinal(&(pInfo->parm.sefsPfe));
    mPmParmFinal(&(pInfo->parm.esPfe));
    mPmParmFinal(&(pInfo->parm.esaPfe));
    mPmParmFinal(&(pInfo->parm.esbPfe));
    mPmParmFinal(&(pInfo->parm.sesPfe));
    mPmParmFinal(&(pInfo->parm.cssPfe));
    mPmParmFinal(&(pInfo->parm.fcPfe));
    mPmParmFinal(&(pInfo->parm.uasPfe));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De1InfoDestroy(tSurInfo   *pSurInfo,
                                           tSurDe1Id  *pDe1Id)

Purpose      : This function is used to deallocate resources of DS1/E1

Inputs       : pSurInfo              - database handle of device
               pDe1Id                - DS1/E1 id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void De1InfoDestroy(tSurInfo *pSurInfo, tSurDe1Id *pDe1Id)
    {
    tSurDe1Info *pInfo;

    pInfo = NULL;
    /* Get channel information */
    mSurDe1Info(pSurInfo, pDe1Id, pInfo);
    if (pInfo != null)
        {
        SurDe1InfoFinal(pInfo);

        /* Destroy other resources */
        OsalMemFree(mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(&(pDe1Id->de3Id))]->pDe1Db[mSurDe1IdFlat(pDe1Id)]);
        mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(&(pDe1Id->de3Id))]->pDe1Db[mSurDe1IdFlat(pDe1Id)] = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet De1ParmGet(tSurDe1Parm   *pDe1Parm, 
                                  ePmDe1Parm     parm, 
                                  tPmParm       **ppParm)

Purpose      : This functions is used to get a pointer to a DS1/E1 parameter.

Inputs       : pDe3Parm              - pointer to the DS1/E1 parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet De1ParmGet(tSurDe1Parm *pDe1Parm, ePmDe1Parm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pDe1Parm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Near-end DS1/E1 line CV */
        case cPmDe1CvL:
            *ppParm = &(pDe1Parm->cvL);
            break;
        
        /* Near-end DS1/E1 line ES */
        case cPmDe1EsL:
            *ppParm = &(pDe1Parm->esL);
            break;
        
        /* Near-end DS1/E1 line SES */
        case cPmDe1SesL:
            *ppParm = &(pDe1Parm->sesL);
            break;
        
        /* Near-end DS1/E1 line LOSS */
        case cPmDe1LossL:
            *ppParm = &(pDe1Parm->lossL);
            break;
        
        /* Near-end DS3/E3 path CV */
        case cPmDe1CvP:
            *ppParm = &(pDe1Parm->cvP);
            break;
        
        /* Near-end DS1/E1 path ES */
        case cPmDe1EsP:
            *ppParm = &(pDe1Parm->esP);
            break;
        
        /* Near-end DS1/E1 path SES */
        case cPmDe1SesP:
            *ppParm = &(pDe1Parm->sesP);
            break;
        
        /* Near-end DS3/E1 path AISS */
        case cPmDe1AissP:
            *ppParm = &(pDe1Parm->aissP);
            break;
            
        /* Near-end DS1/E1 path SAS */
        case cPmDe1SasP:
            *ppParm = &(pDe1Parm->sasP);
            break;
        
        /* Near-end DS1/E1 path CSS */
        case cPmDe1CssP:
            *ppParm = &(pDe1Parm->cssP);
            break;
            
        /* Near-end DS1/E1 path UAS */
        case cPmDe1UasP:
            *ppParm = &(pDe1Parm->uasP);
            break;
            
        /* Near-end DS1/E1 path FC */
        case cPmDe1FcP:
            *ppParm = &(pDe1Parm->fcP);
            break;
        
        /* Near-end DS1/E1 path AISS CI */
        case cPmDe1AissCiP:
            *ppParm = &(pDe1Parm->aissCiP);
            break;
        
        /* Near-end DS1/E1 path ES type A */
        case cPmDe1EsaP:
            *ppParm = &(pDe1Parm->esaP);
            break;
        
        /* Near-end DS1/E1 path ES type B */
        case cPmDe1EsbP:
            *ppParm = &(pDe1Parm->esbP);
            break;
        
        /* Far-end DS1/E1 path CV */
        case cPmDe1CvPfe:
            *ppParm = &(pDe1Parm->cvPfe);
            break;
        
        /* Far-end DS1/E1 path ES type A */
        case cPmDe1EsaPfe:
            *ppParm = &(pDe1Parm->esaPfe);
            break;
        
        /* Far-end DS1/E1 path ES type B */
        case cPmDe1EsbPfe:
            *ppParm = &(pDe1Parm->esbPfe);
            break;
        
        /* Far-end DS1/E1 line ES */
        case cPmDe1EsLfe:
            *ppParm = &(pDe1Parm->esLfe);
            break;
        
        /* Far-end DS1/E1 path SEFS */
        case cPmDe1SefsPfe:
            *ppParm = &(pDe1Parm->sefsPfe);
            break;
        
        /* Far-end DS1/E1 path ES */
        case cPmDe1EsPfe:
            *ppParm = &(pDe1Parm->esPfe);
            break;
        
        /* Far-end DS1/E1 path SES */
        case cPmDe1SesPfe:
            *ppParm = &(pDe1Parm->sesPfe);
            break;
        
        /* Far-end DS1/E1 path CSS */
        case cPmDe1CssPfe:
            *ppParm = &(pDe1Parm->cssPfe);
            break;
        
        /* Far-end DS1/E1 path FC */
        case cPmDe1FcPfe:
            *ppParm = &(pDe1Parm->fcPfe);
            break;
        
        /* Far-end DS1/E1 path UAS */
        case cPmDe1UasPfe:
            *ppParm = &(pDe1Parm->uasPfe);
            break;
        
        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe1ParmRegReset(tSurDe1Parm *pDe1Parm, 
                                          ePmRegType   regType,
                                          bool         neParm)

Purpose      : This functions is used to reset registers of a group of DS1/E1 
               parameters.

Inputs       : pDe1Parm              - pointer to the DS1/E1 parameters
               regType               - register type
               neParm                - is near-end  DS1/E1 parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe1ParmRegReset(tSurDe1Parm *pDe1Parm, 
                                  ePmRegType   regType,
                                  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe1Parm   start;
    ePmDe1Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe1Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe1CvL;
        end   = cPmDe1NeAllParm;
        }
    else
        {
        start = cPmDe1EsLfe;
        end   = cPmDe1FeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De1ParmGet(pDe1Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe1ParmAccInhSet(tSurDe1Parm *pDe1Parm, 
                                           bool         inhibit, 
                                           bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               DS1/E1 parameters.

Inputs       : pDe1Parm              - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end DS1/E1 path parameters,
                                     false for far-end DS1/E1 path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe1ParmAccInhSet(tSurDe1Parm  *pDe1Parm, 
                                   bool          inhibit, 
                                   bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe1Parm   start;
    ePmDe1Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe1Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe1CvL;
        end   = cPmDe1NeAllParm;
        }
    else
        {
        start = cPmDe1EsLfe;
        end   = cPmDe1FeAllParm;
        }
        
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De1ParmGet(pDe1Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe1ParmRegThresSet(tSurDe1Parm  *pDe1Parm, 
                                             ePmRegType    regType,
                                             dword         thresVal, 
                                             bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               DS1/E1 parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end DS1/E1 path parameters, false
                                     for far-end DS1/E1 path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe1ParmRegThresSet(tSurDe1Parm  *pDe1Parm, 
                                     ePmRegType    regType,
                                     dword         thresVal, 
                                     bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe1Parm   start;
    ePmDe1Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe1Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe1CvL;
        end   = cPmDe1NeAllParm;
        }
    else
        {
        start = cPmDe1EsLfe;
        end   = cPmDe1FeAllParm;
        }
        
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De1ParmGet(pDe1Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }       

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllDe1ParmNumRecRegSet(tSurDe1Parm  *pDe1Parm, 
                                              byte          numRecReg, 
                                              bool          neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of DS1/E1 parameters.

Inputs       : pDe1Parm              - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end DS1/E1 path parameters, 
                                     false for far-end DS1/E1 path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllDe1ParmNumRecRegSet(tSurDe1Parm  *pDe1Parm, 
                                      byte          numRecReg, 
                                      bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmDe1Parm   start;
    ePmDe1Parm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pDe1Parm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmDe1CvL;
        end   = cPmDe1NeAllParm;
        }
    else
        {
        start = cPmDe1EsLfe;
        end   = cPmDe1FeAllParm;
        }
        
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = De1ParmGet(pDe1Parm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                                                      
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe1Prov(tSurDev device, tSurDe1Id *pDe1Id)

Purpose      : This function is used to provision a DS1/E1 for FM & PM.

Inputs       : device                - Device
               pDe1Id                - DS1/E1 identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1Prov(tSurDev device, tSurDe1Id *pDe1Id)
    {
    tSurInfo    *pSurInfo;
    tSurDe1Info *pChnInfo;
    
    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pDe1Id))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Get DS1/E1 information */
    mSurDe1Info(pSurInfo, pDe1Id, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "DS1/E1 is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for DS1/E1 */
    pChnInfo = De1InfoCreate(pSurInfo, pDe1Id);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for DS1/E1\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default configuration for DS1/E1 path */
    pChnInfo->conf.measureFs      = false;
    pChnInfo->conf.frmType        = cSurDs1FrmSf;
    pChnInfo->conf.overSonetSdh   = false;
    pChnInfo->conf.decSoakTime    = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultDe1TerSoakTime;
    
    /* Set default parameters's values for DS1/E1 path */
    if (SurDe1DefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe1DeProv(tSurDev device, tSurDe1Id *pDe1Id)

Purpose      : This function is used to de-provision FM & PM of one DS1/E1. It
               will deallocated all resources.

Inputs       : device                - device
               pDe1Id                - DS1/E1 identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1DeProv(tSurDev device, tSurDe1Id *pDe1Id)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pDe1Id))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of DS1/E1 */
    De1InfoDestroy(pSurInfo, pDe1Id);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe1TrblDesgnSet(tSurDev              device, 
                                                 eSurDe1Trbl          trblType, 
                                                 const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set DS1/E1 path trouble designation for 
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1TrblDesgnSet(tSurDev              device, 
                                  eSurDe1Trbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* DS1/E1 line Loss Of Signal failure */    
        case cFmDe1Los:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Los);
            break;
            
        /* DS1/E1 path Loss Of Frame failure */
        case cFmDe1Lof:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Lof);
            break;
            
        /* DS1/E1 path Alarm Indication Signal failure */
        case cFmDe1Ais:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Ais);
            break;
            
        /* DS1/E1 path Alarm Indication Signal - Customer Installation failure */
        case cFmDe1AisCi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1AisCi);
            break;
            
        /* DS1/E1 path Remote Alarm Indication failure */
        case cFmDe1Rai:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Rai);
            break;
            
        /* DS1/E1 path Remote Alarm Indication - Customer Installation failure */
        case cFmDe1RaiCi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1RaiCi);
            break;
            
        /* L bit failure */
        case cFmDe1Lbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;
        /* R bit failure */
        case cFmDe1Rbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;
        /* M bit failure */
        case cFmDe1Mbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* DS1/E1 all failures */
        case cFmDe1AllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Los);  
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Lof);  
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Ais);  
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1AisCi);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Rai);  
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1RaiCi);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));

            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
        
        /* Performance monitoring */
        case cPmDe1Tca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurDe1TrblDesgnGet(tSurDev        device, 
                                                 eSurDe1Trbl    trblType, 
                                                 tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get DS1/E1 path  trouble designation of 
               a device.

Inputs       : device                ? device
               trblType              ? trouble type
               
Outputs      : pTrblDesgn            ? trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1TrblDesgnGet(tSurDev        device, 
                                  eSurDe1Trbl    trblType, 
                                  tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pSurInfo = mDevDb(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* DS1/E1 line Loss Of Signal failure */    
        case cFmDe1Los:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Los);
            break;
            
        /* DS1/E1 path Loss Of Frame failure */
        case cFmDe1Lof:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Lof);
            break;
            
        /* DS1/E1 path Alarm Indication Signal failure */
        case cFmDe1Ais:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Ais);
            break;
            
        /* DS1/E1 path Alarm Indication Signal - Customer Installation failure */
        case cFmDe1AisCi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1AisCi);
            break;
            
        /* DS1/E1 path Remote Alarm Indication failure */
        case cFmDe1Rai:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1Rai);
            break;
            
        /* DS1/E1 path Remote Alarm Indication - Customer Installation failure */
        case cFmDe1RaiCi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.de1RaiCi);
            break;
            
        /* L bit failure */
        case cFmDe1Lbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit failure */
        case cFmDe1Rbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit failure */
        case cFmDe1Mbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* Performance monitoring */
        case cPmDe1Tca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
                
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }  

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1EngStatSet(tSurDev             device, 
                                        const tSurDe1Id    *pChnId, 
                                        eSurEngStat         stat)

Purpose      : This API is used to set engine state for a DS1/E1 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurDe1EngStatSet(tSurDev             device, 
                                const tSurDe1Id    *pChnId, 
                                eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1EngStatGet(tSurDev             device, 
                                        const tSurDe1Id    *pChnId, 
                                        eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a DS1/E1 channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurDe1EngStatGet(tSurDev             device, 
                                const tSurDe1Id    *pChnId, 
                                eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1ChnCfgSet(tSurDev              device,
                                       const tSurDe1Id     *pChnId, 
                                       const tSurDe1Conf   *pChnCfg)
                         
Purpose      : This internal API is use to set DS1/E1 channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1ChnCfgSet(tSurDev              device,
                               const tSurDe1Id     *pChnId, 
                               const tSurDe1Conf   *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurDe1Info    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrInvlParm;
        }
        
    /* Set configuration */
    pInfo->conf.measureFs      = pChnCfg->measureFs;
    pInfo->conf.frmType        = pChnCfg->frmType;
    pInfo->conf.overSonetSdh   = pChnCfg->overSonetSdh;
    pInfo->conf.decSoakTime    = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
    pInfo->conf.terSoakTime    = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultDe1TerSoakTime;
    
    /* Release device's sempahore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1ChnCfgGet(tSurDev          device,
                                       const tSurDe1Id *pChnId, 
                                       tSurDe1Conf     *pChnCfg)
    {
Purpose      : This API is used to set DS1/E1 channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1ChnCfgGet(tSurDev          device,
                               const tSurDe1Id *pChnId, 
                               tSurDe1Conf     *pChnCfg)
    {
    /* Declare local variables */
    tSurDe1Info  *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrInvlParm;
        }
        
    /* Get configuration */
    pChnCfg->frmType        = pInfo->conf.frmType;
    pChnCfg->measureFs      = pInfo->conf.measureFs;
	pChnCfg->overSonetSdh   = pInfo->conf.overSonetSdh;
	pChnCfg->decSoakTime    = pInfo->conf.decSoakTime;
	pChnCfg->terSoakTime    = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1TrblNotfInhSet(tSurDev               device,
                                            const tSurDe1Id      *pChnId, 
                                            eSurDe1Trbl           trblType, 
                                            bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a DS1/E1 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*friend*/ eSurRet SurDe1TrblNotfInhSet(tSurDev               device,
                                    const tSurDe1Id      *pChnId, 
                                    eSurDe1Trbl           trblType, 
                                    bool                  inhibit)
    {
    /* Declare local variable */
    tSurDe1Info *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* DS1/E1 fault monitoring */
        case cFmDe1Los:
            pInfo->msgInh.los = inhibit;
            break;
        case cFmDe1Lof:
            pInfo->msgInh.lof = inhibit;
            break;
        case cFmDe1Ais:
            pInfo->msgInh.ais = inhibit;
            break;
        case cFmDe1AisCi:
            pInfo->msgInh.aisCi = inhibit;
            break;
        case cFmDe1Rai:
            pInfo->msgInh.rai = inhibit;
            break;
        case cFmDe1RaiCi:
            pInfo->msgInh.raiCi = inhibit;
            break;

        case cFmDe1Lbit:
            pInfo->msgInh.lBit = inhibit;
            break;
        case cFmDe1Rbit:
            pInfo->msgInh.rBit = inhibit;
            break;
        case cFmDe1Mbit:
            pInfo->msgInh.mBit = inhibit;
            break;

        case cFmDe1AllFail:
            pInfo->msgInh.de1 = inhibit;
            break;
        
        /* DS1/E1 performance monitoring */
        case cPmDe1Tca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
      
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet  SurDe1TrblNotfInhGet(tSurDev               device,
                                            const tSurDe1Id      *pChnId, 
                                            eSurDe1Trbl           trblType, 
                                            bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a DS1/E1 channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1TrblNotfInhGet(tSurDev               device,
                                    const tSurDe1Id      *pChnId, 
                                    eSurDe1Trbl           trblType, 
                                    bool                 *pInhibit)
    {
    /* Declare local variable */
    tSurDe1Info *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* DS1/E1 fault monitoring */
        case cFmDe1Los:
            *pInhibit = pInfo->msgInh.los;
            break;
        case cFmDe1Lof:
            *pInhibit = pInfo->msgInh.lof;
            break;
        case cFmDe1Ais:
            *pInhibit = pInfo->msgInh.ais;
            break;
        case cFmDe1AisCi:
            *pInhibit = pInfo->msgInh.aisCi;
            break;
        case cFmDe1Rai:
            *pInhibit = pInfo->msgInh.rai;
            break;
        case cFmDe1RaiCi:
            *pInhibit = pInfo->msgInh.raiCi;
            break;

        case cFmDe1Lbit:
            *pInhibit = pInfo->msgInh.lBit;
            break;
        case cFmDe1Rbit:
            *pInhibit = pInfo->msgInh.rBit;
            break;
        case cFmDe1Mbit:
            *pInhibit = pInfo->msgInh.mBit;
            break;

        case cFmDe1AllFail:
            *pInhibit = pInfo->msgInh.de1;
            break;
        
        /* DS1/E1 performance monitoring */
        case cPmDe1Tca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
      
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDe1ChnTrblNotfInhInfoGet(tSurDev             device, 
                                                   const tSurDe1Id    *pChnId, 
                                                   tSurDe1MsgInh      *pTrblInh)

Purpose      : This API is used to get DS1/E1 channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurDe1ChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurDe1Id    *pChnId, 
                                           tSurDe1MsgInh      *pTrblInh)
    {
    tSurDe1Info    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurDe1MsgInh));
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmDe1FailIndGet(tSurDev            device, 
                                       const tSurDe1Id   *pChnId, 
                                       eSurDe1Trbl        trblType, 
                                       tSurFailStat      *pFailStat)

Purpose      : This API is used to get failure indication of a DS1/E1 channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmDe1FailIndGet(tSurDev            device, 
                               const tSurDe1Id   *pChnId, 
                               eSurDe1Trbl        trblType, 
                               tSurFailStat      *pFailStat)
    {
    /* Declare local variables */
    tSurDe1Info *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmDe1Los:
            mSurFailStatCopy(&(pInfo->failure.los), pFailStat);
            break;
            
        case cFmDe1Lof:
            mSurFailStatCopy(&(pInfo->failure.lof), pFailStat);
            break;
        
        case cFmDe1Ais:
            mSurFailStatCopy(&(pInfo->failure.ais), pFailStat);
            break;
        
        case cFmDe1AisCi:
            mSurFailStatCopy(&(pInfo->failure.aisCi), pFailStat);
            break;
            
        case cFmDe1Rai:
            mSurFailStatCopy(&(pInfo->failure.rai), pFailStat);
            break;
        
        case cFmDe1RaiCi:
            mSurFailStatCopy(&(pInfo->failure.raiCi), pFailStat);
            break;
            
        case cFmDe1Lbit:
            mSurFailStatCopy(&(pInfo->failure.lBit), pFailStat);
            break;

        case cFmDe1Rbit:
            mSurFailStatCopy(&(pInfo->failure.rBit), pFailStat);
            break;

        case cFmDe1Mbit:
            mSurFailStatCopy(&(pInfo->failure.mBit), pFailStat);
            break;

        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmDe1FailHisGet(tSurDev          device, 
                                                const tSurDe1Id *pChnId)

Purpose      : This API is used to get a DS1/E1 channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmDe1FailHisGet(tSurDev          device, 
                                 const tSurDe1Id *pChnId)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmDe1FailHisClear(tSurDev          device, 
                                         const tSurDe1Id *pChnId,
                                         bool             flush)

Purpose      : This API is used to clear a DS1/E1 channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmDe1FailHisClear(tSurDev device, const tSurDe1Id *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1RegReset(tSurDev       device, 
                                     tSurDe1Id    *pChnId, 
                                     ePmDe1Parm    parm, 
                                     ePmRegType    regType)
                                     
Purpose      : This API is used to reset a DS1/E1 channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe1RegReset(tSurDev       device, 
                             tSurDe1Id    *pChnId, 
                             ePmDe1Parm    parm, 
                             ePmRegType    regType)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end DS3/E3 path parameters */
    if (parm == cPmDe1NeAllParm)
        {
        ret = AllDe1ParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end DS1/E1 parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end DS1/E1 path parameters */
    else if (parm == cPmDe1FeAllParm)
        {
        ret = AllDe1ParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end DS1/E1 parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1RegGet(tSurDev            device, 
                                   const tSurDe1Id   *pChnId, 
                                   ePmDe1Parm         parm, 
                                   ePmRegType         regType, 
                                   tPmReg            *pRetReg)

Purpose      : This API is used to get a DS1/E1 channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg                - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmDe1RegGet(tSurDev            device, 
                           const tSurDe1Id   *pChnId, 
                           ePmDe1Parm         parm, 
                           ePmRegType         regType, 
                           tPmReg            *pRetReg)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Resetting register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmDe1TcaHisGet(tSurDev device, const tSurDe1Id *pChnId)

Purpose      : This API is used to get a DS1/E1 channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmDe1TcaHisGet(tSurDev device, const tSurDe1Id *pChnId)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
            
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }        
   
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1TcaHisClear(tSurDev device, const tSurDe1Id *pChnId)

Purpose      : This API is used to clear a DS1/E1 channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe1TcaHisClear(tSurDev device, const tSurDe1Id *pChnId)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmDe1ParmAccInhSet(tSurDev          device, 
                                           const tSurDe1Id *pChnId, 
                                           ePmDe1Parm       parm, 
                                           bool             inhibit)

Purpose      : This API is used to enable/disable a DS1/E1 channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe1ParmAccInhSet(tSurDev          device, 
                                  const tSurDe1Id *pChnId, 
                                  ePmDe1Parm       parm, 
                                  bool             inhibit)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end DS1/E1 parameters */
    if (parm == cPmDe1NeAllParm)
        {
        ret = AllDe1ParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end DS1/E1 parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end DS1/E1 parameters */
    else if (parm == cPmDe1FeAllParm)
        {
        ret = AllDe1ParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end DS1/E1 parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1ParmAccInhGet(tSurDev          device, 
                                          const tSurDe1Id *pChnId, 
                                          ePmDe1Parm       parm, 
                                          bool            *pInhibit)

Purpose      : This API is used to get a DS1/E1 channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe1ParmAccInhGet(tSurDev          device, 
                                  const tSurDe1Id *pChnId, 
                                  ePmDe1Parm       parm, 
                                  bool            *pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurDe1Info     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }       
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1RegThresSet(tSurDev           device, 
                                        const tSurDe1Id  *pChnId, 
                                        ePmDe1Parm        parm, 
                                        ePmRegType        regType, 
                                        dword             thresVal)

Purpose      : This API is used to set threshold for a DS1/E1 channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe1RegThresSet(tSurDev           device, 
                                const tSurDe1Id  *pChnId, 
                                ePmDe1Parm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end DS1/E1 parameters */
    else if (parm == cPmDe1NeAllParm)
        {
        ret = AllDe1ParmRegThresSet(&(pInfo->parm), 
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end DS1/E1 parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end DS1/E1 parameters */
    else if (parm == cPmDe1FeAllParm)
        {
        ret = AllDe1ParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end DS1/E1 parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1RegThresGet(tSurDev           device, 
                                        const tSurDe1Id  *pChnId, 
                                        ePmDe1Parm        parm, 
                                        ePmRegType        regType, 
                                        dword            *pThresVal)

Purpose      : This API is used to get threshold for a DS1/E1 channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmDe1RegThresGet(tSurDev           device, 
                                const tSurDe1Id  *pChnId, 
                                ePmDe1Parm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurDe1Info     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1ParmNumRecRegSet(tSurDev          device, 
                                             const tSurDe1Id *pChnId, 
                                             ePmDe1Parm       parm, 
                                             byte             numRecReg)

Purpose      : This API is used to set a DS1/E1 channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmDe1ParmNumRecRegSet(tSurDev          device, 
                                     const tSurDe1Id *pChnId, 
                                     ePmDe1Parm       parm, 
                                     byte             numRecReg)
    {
    /* Declare local variables */
    tSurDe1Info    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
     mPmRecRegCheck(numRecReg);
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurDe1Info(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end DS1/E1 parameters */
    else if (parm == cPmDe1NeAllParm)
        {
        ret = AllDe1ParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end DS1/E1 parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end DS1/E1 parameters */
    else if (parm == cPmDe1FeAllParm)
        {
        ret = AllDe1ParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end DS1/E1 parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmDe1ParmNumRecRegGet(tSurDev          device, 
                                             const tSurDe1Id *pChnId, 
                                             ePmDe1Parm       parm, 
                                             byte            *pNumRecReg)
                                             
Purpose      : This API is used to get a DS1/E1 channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmDe1ParmNumRecRegGet(tSurDev          device, 
                                     const tSurDe1Id *pChnId, 
                                     ePmDe1Parm       parm, 
                                     byte            *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurDe1Info     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurDe1Info(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = De1ParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
        

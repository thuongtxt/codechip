/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - IMA LINK
 *
 * File        : surimalink.c
 *
 * Created Date: 22-Feb-2010
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of IMA Link
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkDefParmSet(tSurImaLinkInfo *pInfo)

Purpose      : This function is used to set default values for IMA Link PM
               parameters.

Inputs       : pInfo                 - IMA Link info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkDefParmSet(tSurImaLinkInfo *pInfo)
    {
    /* Near-end IMA Link parameters */
    mPmDefParmSet(&(pInfo->parm.iv), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.oif), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.ses), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uas), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uusTx), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uusRx), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcTx), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcRx), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.stufTx), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.stufRx), cPmPerThresVal, cPmDayThresVal);

    /* Far-end IMA Link parameters */
    mPmDefParmSet(&(pInfo->parm.sesfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uusTxfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uusRxfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcTxfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcRxfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }
/*------------------------------------------------------------------------------
Prototype    : private tSurImaLinkInfo *ImaLinkInfoCreate(tSurInfo  *pSurInfo,
														  tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to allocate resources for IMA Link

Inputs       : pSurInfo              - database handle of device
               pImaLinkId            - IMA link identifier

Outputs      : None

Return       : tSurImaLinkInfo*      - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurImaLinkInfo *ImaLinkInfoCreate(tSurInfo *pSurInfo, tSurImaLinkId *pImaLinkId)
    {
    tSurImaLinkInfo *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurImaLinkInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurImaLinkInfo), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.iv));
        mPmParmInit(&(pChnInfo->parm.oif));
        mPmParmInit(&(pChnInfo->parm.ses));
        mPmParmInit(&(pChnInfo->parm.uas));
        mPmParmInit(&(pChnInfo->parm.uusTx));
        mPmParmInit(&(pChnInfo->parm.uusRx));
        mPmParmInit(&(pChnInfo->parm.fcTx));
        mPmParmInit(&(pChnInfo->parm.fcRx));
        mPmParmInit(&(pChnInfo->parm.stufTx));
        mPmParmInit(&(pChnInfo->parm.stufRx));
        mPmParmInit(&(pChnInfo->parm.sesfe));
        mPmParmInit(&(pChnInfo->parm.uasfe));
        mPmParmInit(&(pChnInfo->parm.uusTxfe));
        mPmParmInit(&(pChnInfo->parm.uusRxfe));
        mPmParmInit(&(pChnInfo->parm.fcTxfe));
        mPmParmInit(&(pChnInfo->parm.fcRxfe));

        /* Add to database */
        mChnDb(pSurInfo).pImaLink[mSurImaLinkIdFlat(pImaLinkId)] = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurImaLinkInfoFinal(tSurImaLinkInfo *pInfo)

Purpose      : This function is used to deallocate all resource in IMA link
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurImaLinkInfoFinal(tSurImaLinkInfo *pInfo)
    {
    /* Finalize all parameters */
	mPmParmFinal(&(pInfo->parm.iv));
	mPmParmFinal(&(pInfo->parm.oif));
	mPmParmFinal(&(pInfo->parm.ses));
	mPmParmFinal(&(pInfo->parm.uas));
	mPmParmFinal(&(pInfo->parm.uusTx));
	mPmParmFinal(&(pInfo->parm.uusRx));
	mPmParmFinal(&(pInfo->parm.fcTx));
	mPmParmFinal(&(pInfo->parm.fcRx));
	mPmParmFinal(&(pInfo->parm.stufTx));
	mPmParmFinal(&(pInfo->parm.stufRx));
	mPmParmFinal(&(pInfo->parm.sesfe));
	mPmParmFinal(&(pInfo->parm.uasfe));
	mPmParmFinal(&(pInfo->parm.uusTxfe));
	mPmParmFinal(&(pInfo->parm.uusRxfe));
	mPmParmFinal(&(pInfo->parm.fcTxfe));
	mPmParmFinal(&(pInfo->parm.fcRxfe));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaLinkInfoDestroy(tSurInfo   *pSurInfo,
											   tSurImaLinkId  *pImaLinkId)

Purpose      : This function is used to deallocate resources of IMA Link

Inputs       : pSurInfo              - database handle of device
               pImaLinkId            - IMA Link ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ImaLinkInfoDestroy(tSurInfo *pSurInfo, tSurImaLinkId *pImaLinkId)
    {
    tSurImaLinkInfo *pInfo;

    pInfo = NULL;
    /* Get channel information */
    mSurImaLinkInfo(pSurInfo, pImaLinkId, pInfo);
    if (pInfo != null)
        {
        SurImaLinkInfoFinal(pInfo);

        /* Free resources */
        OsalMemFree(mChnDb(pSurInfo).pImaLink[mSurImaLinkIdFlat(pImaLinkId)]);
        mChnDb(pSurInfo).pImaLink[mSurImaLinkIdFlat(pImaLinkId)] = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet ImaLinkParmGet(tSurImaLinkParm   *pImaLinkParm,
									  ePmImaLinkParm     parm,
									  tPmParm       **ppParm)

Purpose      : This functions is used to get a pointer to a IMA Link parameter.

Inputs       : pImaLinkParm          - pointer to the IMA Link parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet ImaLinkParmGet(tSurImaLinkParm *pImaLinkParm, ePmImaLinkParm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pImaLinkParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Near-end IMA Link IV */
        case cPmImaLinkIv:
            *ppParm = &(pImaLinkParm->iv);
            break;
        
        /* Near-end IMA Link OIF */
        case cPmImaLinkOif:
            *ppParm = &(pImaLinkParm->oif);
            break;
        
        /* Near-end IMA Link SES */
        case cPmImaLinkSes:
            *ppParm = &(pImaLinkParm->ses);
            break;
        
        /* Near-end IMA Link UAS */
        case cPmImaLinkUas:
            *ppParm = &(pImaLinkParm->uas);
            break;
        
        /* Near-end IMA Link TX-UUS */
        case cPmImaLinkUusTx:
            *ppParm = &(pImaLinkParm->uusTx);
            break;
        
        /* Near-end IMA Link RX-UUS */
        case cPmImaLinkUusRx:
            *ppParm = &(pImaLinkParm->uusRx);
            break;
        
        /* Near-end IMA Link TX-FC */
        case cPmImaLinkFcTx:
            *ppParm = &(pImaLinkParm->fcTx);
            break;
        
        /* Near-end IMA Link RX-FC */
        case cPmImaLinkFcRx:
            *ppParm = &(pImaLinkParm->fcRx);
            break;
            
        /* Near-end IMA Link TX-Stuff */
        case cPmImaLinkStufTx:
            *ppParm = &(pImaLinkParm->stufTx);
            break;
        
        /* Near-end IMA Link RX-Stuff */
        case cPmImaLinkStufRx:
            *ppParm = &(pImaLinkParm->stufRx);
            break;
            
        /* Far-end IMA Link SES-FE */
        case cPmImaLinkSesfe:
            *ppParm = &(pImaLinkParm->sesfe);
            break;
            
        /* Far-end IMA Link UAS-FE */
        case cPmImaLinkUasfe:
            *ppParm = &(pImaLinkParm->uasfe);
            break;
        
        /* Far-end IMA Link TX-UUS-FE */
        case cPmImaLinkUusTxfe:
            *ppParm = &(pImaLinkParm->uusTxfe);
            break;
        
        /* Far-end IMA Link RX-UUS-FE */
        case cPmImaLinkUusRxfe:
            *ppParm = &(pImaLinkParm->uusRxfe);
            break;
        
        /* Near-end IMA Link TX-FC-FE */
        case cPmImaLinkFcTxfe:
            *ppParm = &(pImaLinkParm->fcTxfe);
            break;
        
        /* Far-end IMA Link RX-FC-FE */
        case cPmImaLinkFcRxfe:
            *ppParm = &(pImaLinkParm->fcRxfe);
            break;
        
        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaLinkParmRegReset(tSurImaLinkParm *pImaLinkParm,
											  ePmRegType   regType,
											  bool         neParm)

Purpose      : This functions is used to reset registers of a group of IMA Link
               parameters.

Inputs       : pImaLinkParm          - pointer to the  IMA Link parameters
               regType               - register type
               neParm                - If parameter is near-end, set to true.
								       Else, set to false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaLinkParmRegReset(tSurImaLinkParm *pImaLinkParm,
									  ePmRegType   regType,
									  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaLinkParm   start;
    ePmImaLinkParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaLinkParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaLinkIv;
        end   = cPmImaLinkNeAllParm;
        }
    else
        {
        start = cPmImaLinkSesfe;
        end   = cPmImaLinkFeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaLinkParmGet(pImaLinkParm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaLinkParmAccInhSet(tSurImaLinkParm *pImaLinkParm,
											   bool         inhibit,
											   bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               IMA Link parameters.

Inputs       : pImaLinkParm              - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end IMA Link parameters,
                                       false for far-end IMA Link pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaLinkParmAccInhSet(tSurImaLinkParm  *pImaLinkParm,
									   bool          inhibit,
									   bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaLinkParm   start;
    ePmImaLinkParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaLinkParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaLinkIv;
        end   = cPmImaLinkNeAllParm;
        }
    else
        {
        start = cPmImaLinkSesfe;
        end   = cPmImaLinkFeAllParm;
        }
        
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaLinkParmGet(pImaLinkParm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaLinkParmRegThresSet(tSurImaLinkParm  *pImaLinkParm,
												 ePmRegType    regType,
												 dword         thresVal,
												 bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               IMA Link parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the only valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end IMA Link parameters,
									   false for far-end IMA Link parameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaLinkParmRegThresSet(tSurImaLinkParm  *pImaLinkParm,
										 ePmRegType    regType,
										 dword         thresVal,
										 bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaLinkParm   start;
    ePmImaLinkParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaLinkParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaLinkIv;
        end   = cPmImaLinkNeAllParm;
        }
    else
        {
        start = cPmImaLinkSesfe;
        end   = cPmImaLinkFeAllParm;
        }
        
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaLinkParmGet(pImaLinkParm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }       

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaLinkParmNumRecRegSet(tSurImaLinkParm  *pImaLinkParm,
												  byte          numRecReg,
												  bool          neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of IMA Link parameters.

Inputs       : pImaLinkParm          - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end IMA Link parameters,
                                       false for far-end IMA Link parameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaLinkParmNumRecRegSet(tSurImaLinkParm  *pImaLinkParm,
										  byte          numRecReg,
										  bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaLinkParm   start;
    ePmImaLinkParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaLinkParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaLinkIv;
        end   = cPmImaLinkNeAllParm;
        }
    else
        {
        start = cPmImaLinkSesfe;
        end   = cPmImaLinkFeAllParm;
        }
        
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaLinkParmGet(pImaLinkParm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                                                      
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaLinkProv(tSurDev device, tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to provision a IMA Link for FM & PM.

Inputs       : device                - Device
               pImaLinkId            - IMA Link identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkProv(tSurDev device, tSurImaLinkId *pImaLinkId)
    {
    tSurInfo    *pSurInfo;
    tSurImaLinkInfo *pChnInfo;
    
    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pImaLinkId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Get IMA Link information */
    mSurImaLinkInfo(pSurInfo, pImaLinkId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "IMA Link is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for IMA Link */
    pChnInfo = ImaLinkInfoCreate(pSurInfo, pImaLinkId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for IMA Link\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default configuration for IMA Link */
    pChnInfo->conf.decSoakTime    = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultTerSoakTime;
    
    /* Set default parameters's values for IMA Link */
    if (SurImaLinkDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaLinkDeProv(tSurDev device, tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to de-provision FM & PM of one IMA Link. It
               will deallocated all resources.

Inputs       : device                - device
               pImaLinkId            - IMA Link identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkDeProv(tSurDev device, tSurImaLinkId *pImaLinkId)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pImaLinkId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of IMA Link */
    ImaLinkInfoDestroy(pSurInfo, pImaLinkId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaLinkTrblDesgnSet(tSurDev              device,
													 eSurImaLinkTrbl      trblType,
													 const tSurTrblDesgn  *pTrblDesgn)

Purpose      : This function is used to set IMA Link trouble designation for
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkTrblDesgnSet(tSurDev              device,
									  eSurImaLinkTrbl      trblType,
									  const tSurTrblDesgn  *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* IMA Link Loss of IMA Frame */
        case cFmImaLinkLif:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkLif);
            break;
            
        /* IMA Link Out of Delay Synchronization */
        case cFmImaLinkLods:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkLods);
            break;
            
        /* IMA Link Remote Failure Indicator */
        case cFmImaLinkRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRfi);
            break;
            
        case cFmImaLinkTxMisConn:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxMisConn);
            break;
        case cFmImaLinkRxMisConn:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxMisConn);
            break;
        case cFmImaLinkTxFault:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxFault);
            break;
        case cFmImaLinkRxFault:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxFault);
            break;
        case cFmImaLinkTxUnusableFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxUnusableFe);
            break;
        case cFmImaLinkRxUnusableFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxUnusableFe);
            break;

        /* IMA Link all failures */
        case cFmImaLinkAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkLif);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkLods);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRfi);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxMisConn);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxMisConn);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxFault);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxFault);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxUnusableFe);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxUnusableFe);
            break;
        
        /* Performance monitoring */
        case cPmImaLinkTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaLinkTrblDesgnGet(tSurDev        device,
													 eSurImaLinkTrbl    trblType,
													 tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get IMA Link  trouble designation of
               a device.

Inputs       : device                � device
               trblType              � trouble type
               
Outputs      : pTrblDesgn            - trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkTrblDesgnGet(tSurDev        	device,
									  eSurImaLinkTrbl   trblType,
									  tSurTrblDesgn 	*pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pSurInfo = mDevDb(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* IMA Link Loss of IMA Frame */
        case cFmImaLinkLif:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkLif);
            break;
            
        /* IMA Link Out of Delay Synchronization */
        case cFmImaLinkLods:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkLods);
            break;
            
        /* IMA Link Remote Failure Indicator */
        case cFmImaLinkRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRfi);
            break;
            
        case cFmImaLinkTxMisConn:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxMisConn);
            break;
        case cFmImaLinkRxMisConn:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxMisConn);
            break;
        case cFmImaLinkTxFault:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxFault);
            break;
        case cFmImaLinkRxFault:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxFault);
            break;
        case cFmImaLinkTxUnusableFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkTxUnusableFe);
            break;
        case cFmImaLinkRxUnusableFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaLinkRxUnusableFe);
            break;

        /* Performance monitoring */
        case cPmImaLinkTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
                
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }  

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkEngStatSet(tSurDev             device,
											const tSurImaLinkId *pChnId,
											eSurEngStat         stat)

Purpose      : This API is used to set engine state for a IMA Link.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkEngStatSet(tSurDev             device,
									const tSurImaLinkId *pChnId,
									eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkEngStatGet(tSurDev             device,
											const tSurImaLinkId    *pChnId,
											eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a IMA Link.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine state

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkEngStatGet(tSurDev             device,
									const tSurImaLinkId *pChnId,
									eSurEngStat         *pStat)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkChnCfgSet(tSurDev              	device,
										   const tSurImaLinkId      *pChnId,
										   const tSurImaLinkConf    *pChnCfg)
                         
Purpose      : This internal API is use to set IMA Link's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkChnCfgSet(tSurDev              	device,
								   const tSurImaLinkId      *pChnId,
								   const tSurImaLinkConf    *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurImaLinkInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrInvlParm;
        }
        
    /* Set configuration */
    pInfo->conf.decSoakTime    = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
    pInfo->conf.terSoakTime    = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release device's sempahore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkChnCfgGet(tSurDev          	device,
										   const tSurImaLinkId  *pChnId,
										   tSurImaLinkConf      *pChnCfg)
    {
Purpose      : This API is used to set IMA Link's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkChnCfgGet(tSurDev          	device,
								   const tSurImaLinkId  *pChnId,
								   tSurImaLinkConf      *pChnCfg)
    {
    /* Declare local variables */
    tSurImaLinkInfo  *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrInvlParm;
        }
        
    /* Get configuration */
	pChnCfg->decSoakTime    = pInfo->conf.decSoakTime;
	pChnCfg->terSoakTime    = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkTrblNotfInhSet(tSurDev               device,
												const tSurImaLinkId   *pChnId,
												eSurImaLinkTrbl       trblType,
												bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a IMA Link.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*friend*/ eSurRet SurImaLinkTrblNotfInhSet(tSurDev               device,
											const tSurImaLinkId   *pChnId,
											eSurImaLinkTrbl       trblType,
											bool                  inhibit)
    {
    /* Declare local variable */
    tSurImaLinkInfo *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* IMA Link fault monitoring */
        case cFmImaLinkLif:
            pInfo->msgInh.lif = inhibit;
            break;
        case cFmImaLinkLods:
            pInfo->msgInh.lods = inhibit;
            break;
        case cFmImaLinkRfi:
            pInfo->msgInh.rfi = inhibit;
            break;
        case cFmImaLinkTxMisConn:
            pInfo->msgInh.txMisConn = inhibit;
            break;
        case cFmImaLinkRxMisConn:
            pInfo->msgInh.rxMisConn = inhibit;
            break;
        case cFmImaLinkTxFault:
            pInfo->msgInh.txFault = inhibit;
            break;
        case cFmImaLinkRxFault:
            pInfo->msgInh.rxFault = inhibit;
            break;
        case cFmImaLinkTxUnusableFe:
            pInfo->msgInh.txUnusableFe = inhibit;
            break;
        case cFmImaLinkRxUnusableFe:
            pInfo->msgInh.rxUnusableFe = inhibit;
            break;

        case cFmImaLinkAllFail:
            pInfo->msgInh.imaLink = inhibit;
            break;
        
        /* IMA Link performance monitoring */
        case cPmImaLinkTca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
      
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet  SurImaLinkTrblNotfInhGet(tSurDev               device,
												 const tSurImaLinkId   *pChnId,
												 eSurImaLink		   trblType,
												 bool                  *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a IMA Link.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkTrblNotfInhGet(tSurDev          	 device,
										const tSurImaLinkId  *pChnId,
										eSurImaLinkTrbl      trblType,
										bool                 *pInhibit)
    {
    /* Declare local variable */
    tSurImaLinkInfo *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* IMA Link fault monitoring */
        case cFmImaLinkLif:
            *pInhibit = pInfo->msgInh.lif;
            break;
        case cFmImaLinkLods:
            *pInhibit = pInfo->msgInh.lods;
            break;
        case cFmImaLinkRfi:
            *pInhibit = pInfo->msgInh.rfi;
            break;
        case cFmImaLinkTxMisConn:
            *pInhibit = pInfo->msgInh.txMisConn;
            break;
        case cFmImaLinkRxMisConn:
            *pInhibit = pInfo->msgInh.rxMisConn;
            break;
        case cFmImaLinkTxFault:
            *pInhibit = pInfo->msgInh.txFault;
            break;
        case cFmImaLinkRxFault:
            *pInhibit = pInfo->msgInh.rxFault;
            break;
        case cFmImaLinkTxUnusableFe:
            *pInhibit = pInfo->msgInh.txUnusableFe;
            break;
        case cFmImaLinkRxUnusableFe:
            *pInhibit = pInfo->msgInh.rxUnusableFe;
            break;

        case cFmImaLinkAllFail:
            *pInhibit = pInfo->msgInh.imaLink;
            break;
        
        /* IMA Link performance monitoring */
        case cPmImaLinkTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
      
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaLinkChnTrblNotfInhInfoGet(tSurDev             	device,
													   const tSurImaLinkId  *pChnId,
													   tSurImaLinkMsgInh    *pTrblInh)

Purpose      : This API is used to get IMA Link's trouble notification
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaLinkChnTrblNotfInhInfoGet(tSurDev             	device,
											   const tSurImaLinkId  *pChnId,
											   tSurImaLinkMsgInh    *pTrblInh)
    {
    tSurImaLinkInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurImaLinkMsgInh));
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmImaLinkFailIndGet(tSurDev            	device,
										   const tSurImaLinkId  *pChnId,
										   eSurImaLinkTrbl      trblType,
										   tSurFailStat      	*pFailStat)

Purpose      : This API is used to get failure indication of a IMA Link's
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmImaLinkFailIndGet(tSurDev            	device,
								   const tSurImaLinkId  *pChnId,
								   eSurImaLinkTrbl      trblType,
								   tSurFailStat      	*pFailStat)
    {
    /* Declare local variables */
    tSurImaLinkInfo *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmImaLinkLif:
            mSurFailStatCopy(&(pInfo->failure.lif), pFailStat);
            break;
            
        case cFmImaLinkLods:
            mSurFailStatCopy(&(pInfo->failure.lods), pFailStat);
            break;
        
        case cFmImaLinkRfi:
            mSurFailStatCopy(&(pInfo->failure.rfi), pFailStat);
            break;
        
        case cFmImaLinkTxMisConn:
        	mSurFailStatCopy(&(pInfo->failure.txMisConn), pFailStat);
            break;

        case cFmImaLinkRxMisConn:
        	mSurFailStatCopy(&(pInfo->failure.rxMisConn), pFailStat);
            break;

        case cFmImaLinkTxFault:
        	mSurFailStatCopy(&(pInfo->failure.txFault), pFailStat);
            break;

        case cFmImaLinkRxFault:
        	mSurFailStatCopy(&(pInfo->failure.rxFault), pFailStat);
            break;

        case cFmImaLinkTxUnusableFe:
        	mSurFailStatCopy(&(pInfo->failure.txUnusableFe), pFailStat);
            break;

        case cFmImaLinkRxUnusableFe:
        	mSurFailStatCopy(&(pInfo->failure.rxUnusableFe), pFailStat);
            break;

        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmImaLinkFailHisGet(tSurDev             device,
													const tSurImaLinkId *pChnId)

Purpose      : This API is used to get a IMA Link's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmImaLinkFailHisGet(tSurDev          	 device,
									 const tSurImaLinkId *pChnId)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmImaLinkFailHisClear(tSurDev          	 device,
											 const tSurImaLinkId *pChnId,
											 bool             	 flush)

Purpose      : This API is used to clear a IMA Link's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmImaLinkFailHisClear(tSurDev device,
									 const tSurImaLinkId *pChnId,
									 bool flush)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkRegReset(tSurDev       	device,
										 tSurImaLinkId  *pChnId,
										 ePmImaLinkParm  parm,
										 ePmRegType    	 regType)
                                     
Purpose      : This API is used to reset a IMA Link's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaLinkRegReset(tSurDev       	device,
								 tSurImaLinkId  *pChnId,
								 ePmImaLinkParm parm,
								 ePmRegType     regType)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end IMA Link parameters */
    if (parm == cPmImaLinkNeAllParm)
        {
        ret = AllImaLinkParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end IMA Link parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end IMA Link parameters */
    else if (parm == cPmImaLinkFeAllParm)
        {
        ret = AllImaLinkParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end IMA Link parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkRegGet(tSurDev            	device,
									   const tSurImaLinkId  *pChnId,
									   ePmImaLinkParm       parm,
									   ePmRegType           regType,
									   tPmReg               *pRetReg)

Purpose      : This API is used to get a IMA Link's parameter's register's
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg                - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmImaLinkRegGet(tSurDev            	device,
							   const tSurImaLinkId  *pChnId,
							   ePmImaLinkParm       parm,
							   ePmRegType           regType,
							   tPmReg               *pRetReg)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Resetting register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmImaLinkTcaHisGet(tSurDev device, const tSurImaLinkId *pChnId)

Purpose      : This API is used to get a IMA Link's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmImaLinkTcaHisGet(tSurDev device, const tSurImaLinkId *pChnId)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
            
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }        
   
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkTcaHisClear(tSurDev device, const tSurImaLinkId *pChnId)

Purpose      : This API is used to clear a IMA Link's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaLinkTcaHisClear(tSurDev device, const tSurImaLinkId *pChnId)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmImaLinkParmAccInhSet(tSurDev          	device,
											   const tSurImaLinkId  *pChnId,
											   ePmImaLinkParm       parm,
											   bool             	inhibit)

Purpose      : This API is used to enable/disable a IMA Link's accumlate
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaLinkParmAccInhSet(tSurDev          		device,
									  const tSurImaLinkId 	*pChnId,
									  ePmImaLinkParm       	parm,
									  bool             		inhibit)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end IMA Link parameters */
    if (parm == cPmImaLinkNeAllParm)
        {
        ret = AllImaLinkParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end IMA Link parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end IMA Link parameters */
    else if (parm == cPmImaLinkFeAllParm)
        {
        ret = AllImaLinkParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end IMA Link parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkParmAccInhGet(tSurDev          		device,
											  const tSurImaLinkId 	*pChnId,
											  ePmImaLinkParm       	parm,
											  bool            		*pInhibit)

Purpose      : This API is used to get a IMA Link's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaLinkParmAccInhGet(tSurDev          		device,
									  const tSurImaLinkId 	*pChnId,
									  ePmImaLinkParm       	parm,
									  bool           		*pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurImaLinkInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }       
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkRegThresSet(tSurDev           	 device,
											const tSurImaLinkId  *pChnId,
											ePmImaLinkParm       parm,
											ePmRegType        	 regType,
											dword             	 thresVal)

Purpose      : This API is used to set threshold for a IMA Link's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaLinkRegThresSet(tSurDev           	 device,
									const tSurImaLinkId  *pChnId,
									ePmImaLinkParm       parm,
									ePmRegType           regType,
									dword                thresVal)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end IMA Link parameters */
    else if (parm == cPmImaLinkNeAllParm)
        {
        ret = AllImaLinkParmRegThresSet(&(pInfo->parm),
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end IMA Link parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end IMA Link parameters */
    else if (parm == cPmImaLinkFeAllParm)
        {
        ret = AllImaLinkParmRegThresSet(&(pInfo->parm),
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end IMA Link parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkRegThresGet(tSurDev           	 device,
											const tSurImaLinkId  *pChnId,
											ePmImaLinkParm        parm,
											ePmRegType        	 regType,
											dword             	 *pThresVal)

Purpose      : This API is used to get threshold for a IMA Link's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaLinkRegThresGet(tSurDev           	 device,
									const tSurImaLinkId  *pChnId,
									ePmImaLinkParm       parm,
									ePmRegType           regType,
									dword                *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurImaLinkInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkParmNumRecRegSet(tSurDev          	 device,
												 const tSurImaLinkId *pChnId,
												 ePmImaLinkParm       parm,
												 byte             	 numRecReg)

Purpose      : This API is used to set a IMA Link's parameter's number of
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmImaLinkParmNumRecRegSet(tSurDev          	 device,
										 const tSurImaLinkId *pChnId,
										 ePmImaLinkParm       parm,
										 byte             	 numRecReg)
    {
    /* Declare local variables */
    tSurImaLinkInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
     mPmRecRegCheck(numRecReg);
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaLinkInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end IMA Link parameters */
    else if (parm == cPmImaLinkNeAllParm)
        {
        ret = AllImaLinkParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end IMA Link parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end IMA Link parameters */
    else if (parm == cPmImaLinkFeAllParm)
        {
        ret = AllImaLinkParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end IMA Link parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaLinkParmNumRecRegGet(tSurDev          	 device,
												 const tSurImaLinkId *pChnId,
												 ePmImaLinkParm       parm,
												 byte            	 *pNumRecReg)
                                             
Purpose      : This API is used to get a IMA Link's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmImaLinkParmNumRecRegGet(tSurDev          	 device,
										 const tSurImaLinkId *pChnId,
										 ePmImaLinkParm       parm,
										 byte            	 *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurImaLinkInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurImaLinkInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaLinkParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
        

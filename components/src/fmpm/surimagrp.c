/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - IMA GROUP
 *
 * File        : surimagrp.c
 *
 * Created Date: 22-Feb-2010
 *
 * Description : This file contain all Transmission Surveillance source code
 *               of IMA Group
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpDefParmSet(tSurImaGrpInfo *pInfo)

Purpose      : This function is used to set default values for IMA Group PM
               parameters.

Inputs       : pInfo                 - IMA Group info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpDefParmSet(tSurImaGrpInfo *pInfo)
    {
    /* Near-end IMA Group parameters */
    mPmDefParmSet(&(pInfo->parm.fc), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uas), cPmPerThresVal, cPmDayThresVal);

    /* Far-end IMA Group parameters */
    mPmDefParmSet(&(pInfo->parm.fcfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }
/*------------------------------------------------------------------------------
Prototype    : private tSurImaGrpInfo *ImaGrpInfoCreate(tSurInfo  *pSurInfo,
														  tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to allocate resources for IMA Group

Inputs       : pSurInfo              - database handle of device
               pImaGrpId            - IMA link identifier

Outputs      : None

Return       : tSurImaGrpInfo*      - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurImaGrpInfo *ImaGrpInfoCreate(tSurInfo *pSurInfo, tSurImaGrpId *pImaGrpId)
    {
    tSurImaGrpInfo *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurImaGrpInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurImaGrpInfo), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.uas));
        mPmParmInit(&(pChnInfo->parm.fc));
        mPmParmInit(&(pChnInfo->parm.fcfe));

        /* Add to database */
        mChnDb(pSurInfo).pImaGrp[mSurImaGrpIdFlat(pImaGrpId)] = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurImaGrpInfoFinal(tSurImaGrpInfo *pInfo)

Purpose      : This function is used to deallocate all resource in IMA link
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurImaGrpInfoFinal(tSurImaGrpInfo *pInfo)
    {
    /* Finalize all parameters */
	mPmParmFinal(&(pInfo->parm.uas));
	mPmParmFinal(&(pInfo->parm.fc));
	mPmParmFinal(&(pInfo->parm.fcfe));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaGrpInfoDestroy(tSurInfo   *pSurInfo,
											   tSurImaGrpId  *pImaGrpId)

Purpose      : This function is used to deallocate resources of IMA Group

Inputs       : pSurInfo             - database handle of device
               pImaGrpId            - IMA Group ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ImaGrpInfoDestroy(tSurInfo *pSurInfo, tSurImaGrpId *pImaGrpId)
    {
    tSurImaGrpInfo *pInfo;

    pInfo = NULL;
    /* Get channel information */
    mSurImaGrpInfo(pSurInfo, pImaGrpId, pInfo);
    if (pInfo != null)
        {
        SurImaGrpInfoFinal(pInfo);

        /* Free resources */
        OsalMemFree(mChnDb(pSurInfo).pImaGrp[mSurImaGrpIdFlat(pImaGrpId)]);
        mChnDb(pSurInfo).pImaGrp[mSurImaGrpIdFlat(pImaGrpId)] = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet ImaGrpParmGet(tSurImaGrpParm   *pImaGrpParm,
									  ePmImaGrpParm     parm,
									  tPmParm       **ppParm)

Purpose      : This functions is used to get a pointer to a IMA Group parameter.

Inputs       : pImaGrpParm          - pointer to the IMA Group parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet ImaGrpParmGet(tSurImaGrpParm *pImaGrpParm, ePmImaGrpParm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pImaGrpParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Near-end IMA Group UAS */
        case cPmImaGrpUas:
            *ppParm = &(pImaGrpParm->uas);
            break;
        
        /* Near-end IMA Group FC */
        case cPmImaGrpFc:
            *ppParm = &(pImaGrpParm->fc);
            break;
        
        /* Far-end IMA Group FC-FE */
        case cPmImaGrpFcfe:
            *ppParm = &(pImaGrpParm->fcfe);
            break;

        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaGrpParmRegReset(tSurImaGrpParm *pImaGrpParm,
											  ePmRegType   regType,
											  bool         neParm)

Purpose      : This functions is used to reset registers of a group of IMA Group
               parameters.

Inputs       : pImaGrpParm          - pointer to the  IMA Group parameters
               regType               - register type
               neParm                - If parameter is near-end, set to true.
								       Else, set to false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaGrpParmRegReset(tSurImaGrpParm *pImaGrpParm,
									  ePmRegType   regType,
									  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaGrpParm   start;
    ePmImaGrpParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaGrpParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaGrpUas;
        end   = cPmImaGrpNeAllParm;
        }
    else
        {
        start = cPmImaGrpFcfe;
        end   = cPmImaGrpFeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaGrpParmGet(pImaGrpParm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaGrpParmAccInhSet(tSurImaGrpParm *pImaGrpParm,
											   bool         inhibit,
											   bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               IMA Group parameters.

Inputs       : pImaGrpParm              - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end IMA Group parameters,
                                       false for far-end IMA Group pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaGrpParmAccInhSet(tSurImaGrpParm  *pImaGrpParm,
									   bool          inhibit,
									   bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaGrpParm   start;
    ePmImaGrpParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaGrpParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaGrpUas;
        end   = cPmImaGrpNeAllParm;
        }
    else
        {
        start = cPmImaGrpFcfe;
        end   = cPmImaGrpFeAllParm;
        }
        
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaGrpParmGet(pImaGrpParm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaGrpParmRegThresSet(tSurImaGrpParm  *pImaGrpParm,
												 ePmRegType    regType,
												 dword         thresVal,
												 bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               IMA Group parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the only valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end IMA Group parameters,
									   false for far-end IMA Group parameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaGrpParmRegThresSet(tSurImaGrpParm  *pImaGrpParm,
										 ePmRegType    regType,
										 dword         thresVal,
										 bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaGrpParm   start;
    ePmImaGrpParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaGrpParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaGrpUas;
        end   = cPmImaGrpNeAllParm;
        }
    else
        {
        start = cPmImaGrpFcfe;
        end   = cPmImaGrpFeAllParm;
        }
        
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaGrpParmGet(pImaGrpParm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }       

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllImaGrpParmNumRecRegSet(tSurImaGrpParm  *pImaGrpParm,
												  byte          numRecReg,
												  bool          neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of IMA Group parameters.

Inputs       : pImaGrpParm          - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end IMA Group parameters,
                                       false for far-end IMA Group parameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllImaGrpParmNumRecRegSet(tSurImaGrpParm  *pImaGrpParm,
										  byte          numRecReg,
										  bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmImaGrpParm   start;
    ePmImaGrpParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pImaGrpParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmImaGrpUas;
        end   = cPmImaGrpNeAllParm;
        }
    else
        {
        start = cPmImaGrpFcfe;
        end   = cPmImaGrpFeAllParm;
        }
        
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = ImaGrpParmGet(pImaGrpParm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                                                      
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaGrpProv(tSurDev device, tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to provision a IMA Group for FM & PM.

Inputs       : device               - Device
               pImaGrpId            - IMA Group identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpProv(tSurDev device, tSurImaGrpId *pImaGrpId)
    {
    tSurInfo    *pSurInfo;
    tSurImaGrpInfo *pChnInfo;
    
    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pImaGrpId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Get IMA Group information */
    mSurImaGrpInfo(pSurInfo, pImaGrpId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "IMA Group is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for IMA Group */
    pChnInfo = ImaGrpInfoCreate(pSurInfo, pImaGrpId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for IMA Group\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default configuration for IMA Group */
    pChnInfo->conf.decSoakTime    = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultTerSoakTime;
    
    /* Set default parameters's values for IMA Group */
    if (SurImaGrpDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaGrpDeProv(tSurDev device, tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to de-provision FM & PM of one IMA Group. It
               will deallocated all resources.

Inputs       : device                - device
               pImaGrpId            - IMA Group identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpDeProv(tSurDev device, tSurImaGrpId *pImaGrpId)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pImaGrpId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of IMA Group */
    ImaGrpInfoDestroy(pSurInfo, pImaGrpId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaGrpTrblDesgnSet(tSurDev              device,
													 eSurImaGrpTrbl      trblType,
													 const tSurTrblDesgn  *pTrblDesgn)

Purpose      : This function is used to set IMA Group trouble designation for
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpTrblDesgnSet(tSurDev              device,
									  eSurImaGrpTrbl      trblType,
									  const tSurTrblDesgn  *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* IMA Group Start up FE */
        case cFmImaGrpStartupFe:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpStartupFe);
            break;
            
        /* IMA Group Config aborted */
        case cFmImaGrpCfgAborted:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpCfgAborted);
            break;
            
        /* IMA Group Config aborted FE */
        case cFmImaGrpCfgAbortedFe:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpCfgAbortedFe);
            break;
            
        /* IMA Group Insufficient-Links */
        case cFmImaGrpInsuffLink:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpInsuffLink);
            break;

         /* IMA Group Insufficient-Links FE */
        case cFmImaGrpInsuffLinkFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpInsuffLinkFe);
            break;

        /* IMA Group Blocked-FE */
        case cFmImaGrpBlockedFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpBlockedFe);
            break;

        /* IMA Group Timing-Mismatch */
        case cFmImaGrpTimingMismatch:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpTimingMismatch);
            break;

        /* IMA Group all failures */
        case cFmImaGrpAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpStartupFe);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpCfgAborted);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpCfgAbortedFe);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpInsuffLink);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpInsuffLinkFe);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpBlockedFe);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpTimingMismatch);
            break;
        
        /* Performance monitoring */
        case cPmImaGrpTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurImaGrpTrblDesgnGet(tSurDev        device,
													 eSurImaGrpTrbl    trblType,
													 tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get IMA Group  trouble designation of
               a device.

Inputs       : device                � device
               trblType              � trouble type
               
Outputs      : pTrblDesgn            - trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpTrblDesgnGet(tSurDev        	device,
									  eSurImaGrpTrbl   trblType,
									  tSurTrblDesgn 	*pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pSurInfo = mDevDb(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* IMA Group Start up FE */
        case cFmImaGrpStartupFe:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpStartupFe);
            break;
            
        /* IMA Group Config aborted */
        case cFmImaGrpCfgAborted:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpCfgAborted);
            break;
            
        /* IMA Group Config aborted FE */
        case cFmImaGrpCfgAbortedFe:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpCfgAbortedFe);
            break;
            
        /* IMA Group Insufficient-Links */
        case cFmImaGrpInsuffLink:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpInsuffLink);
            break;

         /* IMA Group Insufficient-Links FE */
        case cFmImaGrpInsuffLinkFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpInsuffLinkFe);
            break;

        /* IMA Group Blocked-FE */
        case cFmImaGrpBlockedFe:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpBlockedFe);
            break;

        /* IMA Group Timing-Mismatch */
        case cFmImaGrpTimingMismatch:
        	pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.imaGrpTimingMismatch);
            break;

        /* Performance monitoring */
        case cPmImaGrpTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
                
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }  

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpEngStatSet(tSurDev             device,
											const tSurImaGrpId *pChnId,
											eSurEngStat         stat)

Purpose      : This API is used to set engine state for a IMA Group.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpEngStatSet(tSurDev             device,
									const tSurImaGrpId *pChnId,
									eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpEngStatGet(tSurDev             device,
											const tSurImaGrpId    *pChnId,
											eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a IMA Group.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine state

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpEngStatGet(tSurDev             device,
									const tSurImaGrpId *pChnId,
									eSurEngStat         *pStat)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpChnCfgSet(tSurDev              	device,
										   const tSurImaGrpId      *pChnId,
										   const tSurImaGrpConf    *pChnCfg)
                         
Purpose      : This internal API is use to set IMA Group's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpChnCfgSet(tSurDev              	device,
								   const tSurImaGrpId      *pChnId,
								   const tSurImaGrpConf    *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurImaGrpInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrInvlParm;
        }
        
    /* Set configuration */
    pInfo->conf.decSoakTime    = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
    pInfo->conf.terSoakTime    = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release device's sempahore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpChnCfgGet(tSurDev          	device,
										   const tSurImaGrpId  *pChnId,
										   tSurImaGrpConf      *pChnCfg)
    {
Purpose      : This API is used to set IMA Group's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpChnCfgGet(tSurDev          	device,
								   const tSurImaGrpId  *pChnId,
								   tSurImaGrpConf      *pChnCfg)
    {
    /* Declare local variables */
    tSurImaGrpInfo  *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrInvlParm;
        }
        
    /* Get configuration */
	pChnCfg->decSoakTime    = pInfo->conf.decSoakTime;
	pChnCfg->terSoakTime    = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpTrblNotfInhSet(tSurDev               device,
												const tSurImaGrpId   *pChnId,
												eSurImaGrpTrbl       trblType,
												bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a IMA Group.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*friend*/ eSurRet SurImaGrpTrblNotfInhSet(tSurDev               device,
											const tSurImaGrpId   *pChnId,
											eSurImaGrpTrbl       trblType,
											bool                  inhibit)
    {
    /* Declare local variable */
    tSurImaGrpInfo *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* IMA Group Start up FE */
        case cFmImaGrpStartupFe:
        	pInfo->msgInh.startupFe = inhibit;
            break;

        /* IMA Group Config aborted */
        case cFmImaGrpCfgAborted:
        	pInfo->msgInh.cfgAborted = inhibit;
            break;

        /* IMA Group Config aborted FE */
        case cFmImaGrpCfgAbortedFe:
        	pInfo->msgInh.cfgAbortedFe = inhibit;
            break;

        /* IMA Group Insufficient-Links */
        case cFmImaGrpInsuffLink:
        	pInfo->msgInh.insuffLink = inhibit;
            break;

         /* IMA Group Insufficient-Links FE */
        case cFmImaGrpInsuffLinkFe:
        	pInfo->msgInh.insuffLinkFe = inhibit;
            break;

        /* IMA Group Blocked-FE */
        case cFmImaGrpBlockedFe:
        	pInfo->msgInh.blockedFe = inhibit;
            break;

        /* IMA Group Timing-Mismatch */
        case cFmImaGrpTimingMismatch:
        	pInfo->msgInh.timingMismatch = inhibit;
            break;

        case cFmImaGrpAllFail:
            pInfo->msgInh.imaGrp = inhibit;
            break;
        
        /* IMA Group performance monitoring */
        case cPmImaGrpTca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
      
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet  SurImaGrpTrblNotfInhGet(tSurDev               device,
												 const tSurImaGrpId   *pChnId,
												 eSurImaGrp		   trblType,
												 bool                  *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a IMA Group.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpTrblNotfInhGet(tSurDev          	 device,
										const tSurImaGrpId  *pChnId,
										eSurImaGrpTrbl      trblType,
										bool                 *pInhibit)
    {
    /* Declare local variable */
    tSurImaGrpInfo *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* IMA Group fault monitoring */
        /* IMA Group Start up FE */
        case cFmImaGrpStartupFe:
        	*pInhibit = pInfo->msgInh.startupFe;
            break;

        /* IMA Group Config aborted */
        case cFmImaGrpCfgAborted:
        	*pInhibit = pInfo->msgInh.cfgAborted;
            break;

        /* IMA Group Config aborted FE */
        case cFmImaGrpCfgAbortedFe:
        	*pInhibit = pInfo->msgInh.cfgAbortedFe;
            break;

        /* IMA Group Insufficient-Links */
        case cFmImaGrpInsuffLink:
        	*pInhibit = pInfo->msgInh.insuffLink;
            break;

         /* IMA Group Insufficient-Links FE */
        case cFmImaGrpInsuffLinkFe:
        	*pInhibit = pInfo->msgInh.insuffLinkFe;
            break;

        /* IMA Group Blocked-FE */
        case cFmImaGrpBlockedFe:
        	*pInhibit = pInfo->msgInh.blockedFe;
            break;

        /* IMA Group Timing-Mismatch */
        case cFmImaGrpTimingMismatch:
        	*pInhibit = pInfo->msgInh.timingMismatch;
            break;

        case cFmImaGrpAllFail:
            *pInhibit = pInfo->msgInh.imaGrp;
            break;
        
        /* IMA Group performance monitoring */
        case cPmImaGrpTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
      
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurImaGrpChnTrblNotfInhInfoGet(tSurDev             	device,
													   const tSurImaGrpId  *pChnId,
													   tSurImaGrpMsgInh    *pTrblInh)

Purpose      : This API is used to get IMA Group's trouble notification
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurImaGrpChnTrblNotfInhInfoGet(tSurDev             	device,
											   const tSurImaGrpId  *pChnId,
											   tSurImaGrpMsgInh    *pTrblInh)
    {
    tSurImaGrpInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurImaGrpMsgInh));
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmImaGrpFailIndGet(tSurDev            	device,
										   const tSurImaGrpId  *pChnId,
										   eSurImaGrpTrbl      trblType,
										   tSurFailStat      	*pFailStat)

Purpose      : This API is used to get failure indication of a IMA Group's
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmImaGrpFailIndGet(tSurDev            	device,
								   const tSurImaGrpId  *pChnId,
								   eSurImaGrpTrbl      trblType,
								   tSurFailStat      	*pFailStat)
    {
    /* Declare local variables */
    tSurImaGrpInfo *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmImaGrpStartupFe:
            mSurFailStatCopy(&(pInfo->failure.startupFe), pFailStat);
            break;
            
        case cFmImaGrpCfgAborted:
            mSurFailStatCopy(&(pInfo->failure.cfgAborted), pFailStat);
            break;
        
        case cFmImaGrpCfgAbortedFe:
            mSurFailStatCopy(&(pInfo->failure.cfgAbortedFe), pFailStat);
            break;
        
        case cFmImaGrpInsuffLink:
        	mSurFailStatCopy(&(pInfo->failure.insuffLink), pFailStat);
            break;

        case cFmImaGrpInsuffLinkFe:
        	mSurFailStatCopy(&(pInfo->failure.insuffLinkFe), pFailStat);
            break;

        case cFmImaGrpBlockedFe:
        	mSurFailStatCopy(&(pInfo->failure.blockedFe), pFailStat);
            break;

        case cFmImaGrpTimingMismatch:
        	mSurFailStatCopy(&(pInfo->failure.timingMismatch), pFailStat);
            break;

        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmImaGrpFailHisGet(tSurDev             device,
													const tSurImaGrpId *pChnId)

Purpose      : This API is used to get a IMA Group's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmImaGrpFailHisGet(tSurDev          	 device,
									 const tSurImaGrpId *pChnId)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmImaGrpFailHisClear(tSurDev          	 device,
											 const tSurImaGrpId *pChnId,
											 bool             	 flush)

Purpose      : This API is used to clear a IMA Group's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmImaGrpFailHisClear(tSurDev device,
									 const tSurImaGrpId *pChnId,
									 bool flush)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpRegReset(tSurDev       	device,
										 tSurImaGrpId  *pChnId,
										 ePmImaGrpParm  parm,
										 ePmRegType    	 regType)
                                     
Purpose      : This API is used to reset a IMA Group's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaGrpRegReset(tSurDev       	device,
								 tSurImaGrpId  *pChnId,
								 ePmImaGrpParm parm,
								 ePmRegType     regType)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end IMA Group parameters */
    if (parm == cPmImaGrpNeAllParm)
        {
        ret = AllImaGrpParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end IMA Group parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end IMA Group parameters */
    else if (parm == cPmImaGrpFeAllParm)
        {
        ret = AllImaGrpParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end IMA Group parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpRegGet(tSurDev            	device,
									   const tSurImaGrpId  *pChnId,
									   ePmImaGrpParm       parm,
									   ePmRegType           regType,
									   tPmReg               *pRetReg)

Purpose      : This API is used to get a IMA Group's parameter's register's
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg                - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmImaGrpRegGet(tSurDev            	device,
							   const tSurImaGrpId  *pChnId,
							   ePmImaGrpParm       parm,
							   ePmRegType           regType,
							   tPmReg               *pRetReg)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Resetting register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmImaGrpTcaHisGet(tSurDev device, const tSurImaGrpId *pChnId)

Purpose      : This API is used to get a IMA Group's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmImaGrpTcaHisGet(tSurDev device, const tSurImaGrpId *pChnId)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
            
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }        
   
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpTcaHisClear(tSurDev device, const tSurImaGrpId *pChnId)

Purpose      : This API is used to clear a IMA Group's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaGrpTcaHisClear(tSurDev device, const tSurImaGrpId *pChnId)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmImaGrpParmAccInhSet(tSurDev          	device,
											   const tSurImaGrpId  *pChnId,
											   ePmImaGrpParm       parm,
											   bool             	inhibit)

Purpose      : This API is used to enable/disable a IMA Group's accumlate
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaGrpParmAccInhSet(tSurDev          		device,
									  const tSurImaGrpId 	*pChnId,
									  ePmImaGrpParm       	parm,
									  bool             		inhibit)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end IMA Group parameters */
    if (parm == cPmImaGrpNeAllParm)
        {
        ret = AllImaGrpParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end IMA Group parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end IMA Group parameters */
    else if (parm == cPmImaGrpFeAllParm)
        {
        ret = AllImaGrpParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end IMA Group parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpParmAccInhGet(tSurDev          		device,
											  const tSurImaGrpId 	*pChnId,
											  ePmImaGrpParm       	parm,
											  bool            		*pInhibit)

Purpose      : This API is used to get a IMA Group's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaGrpParmAccInhGet(tSurDev          		device,
									  const tSurImaGrpId 	*pChnId,
									  ePmImaGrpParm       	parm,
									  bool           		*pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurImaGrpInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }       
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpRegThresSet(tSurDev           	 device,
											const tSurImaGrpId  *pChnId,
											ePmImaGrpParm       parm,
											ePmRegType        	 regType,
											dword             	 thresVal)

Purpose      : This API is used to set threshold for a IMA Group's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaGrpRegThresSet(tSurDev           	 device,
									const tSurImaGrpId  *pChnId,
									ePmImaGrpParm       parm,
									ePmRegType           regType,
									dword                thresVal)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end IMA Group parameters */
    else if (parm == cPmImaGrpNeAllParm)
        {
        ret = AllImaGrpParmRegThresSet(&(pInfo->parm),
										regType,
										thresVal,
										true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end IMA Group parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end IMA Group parameters */
    else if (parm == cPmImaGrpFeAllParm)
        {
        ret = AllImaGrpParmRegThresSet(&(pInfo->parm),
										 regType,
										 thresVal,
										 false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end IMA Group parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpRegThresGet(tSurDev           	 device,
											const tSurImaGrpId  *pChnId,
											ePmImaGrpParm        parm,
											ePmRegType        	 regType,
											dword             	 *pThresVal)

Purpose      : This API is used to get threshold for a IMA Group's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmImaGrpRegThresGet(tSurDev           	 device,
									const tSurImaGrpId  *pChnId,
									ePmImaGrpParm       parm,
									ePmRegType           regType,
									dword                *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurImaGrpInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpParmNumRecRegSet(tSurDev          	 device,
												 const tSurImaGrpId *pChnId,
												 ePmImaGrpParm       parm,
												 byte             	 numRecReg)

Purpose      : This API is used to set a IMA Group's parameter's number of
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmImaGrpParmNumRecRegSet(tSurDev          	 device,
										 const tSurImaGrpId *pChnId,
										 ePmImaGrpParm       parm,
										 byte             	 numRecReg)
    {
    /* Declare local variables */
    tSurImaGrpInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
     mPmRecRegCheck(numRecReg);
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurImaGrpInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end IMA Group parameters */
    else if (parm == cPmImaGrpNeAllParm)
        {
        ret = AllImaGrpParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end IMA Group parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end IMA Group parameters */
    else if (parm == cPmImaGrpFeAllParm)
        {
        ret = AllImaGrpParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end IMA Group parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmImaGrpParmNumRecRegGet(tSurDev          	 device,
												 const tSurImaGrpId *pChnId,
												 ePmImaGrpParm       parm,
												 byte            	 *pNumRecReg)
                                             
Purpose      : This API is used to get a IMA Group's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmImaGrpParmNumRecRegGet(tSurDev          	 device,
										 const tSurImaGrpId *pChnId,
										 ePmImaGrpParm       parm,
										 byte            	 *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurImaGrpInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurImaGrpInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = ImaGrpParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
        

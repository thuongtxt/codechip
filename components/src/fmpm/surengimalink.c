/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengimalink.c
 *
 * Created Date: 22-Feb-2010
 *
 * Description : This file contain all source code IMA Link Transmisstion
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ---------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mFailLsmIndUpd(pSurInfo, pChnInfo, defName, failName, failChg)
Purpose   : This macro is used to update failure indication of Link State Machine
Inputs    : pSurInfo				 - Surveillance device information
			pChnInfo                 - channel information
            defName                  - defect name (in structure)
            failName                 - failure name (in structure)
            failStatVal				 - Value of LSM state that raising failure
Outputs   : failChg                  - true if failure changed status
------------------------------------------------------------------------------*/
#define mFailLsmIndUpd(pSurInfo, pChnInfo, defName, failName, failStatVal, failChg)                      \
    (failChg) = false;                                                         \
    if ((pChnInfo)->failure.failName.status != (((pChnInfo)->status.defName.status == failStatVal)? true : false))\
        {                                                                      \
        /* Set failure indication */                                           \
        if ((pChnInfo)->status.defName.status == failStatVal)                  \
            {                                                                  \
            if (mDecSoakExpr((pChnInfo)->status.defName.time, pSurInfo->pDb->mastConf.decSoakTime) == true)         \
                {                                                              \
                (pChnInfo)->failure.failName.status = true;                    \
                (pChnInfo)->failure.failName.time   = dCurTime;                \
                (failChg) = true;                                              \
                }                                                              \
            }                                                                  \
                                                                               \
        /* Clear failure indication */                                         \
        else                                                                   \
            {                                                                  \
            if (mTerSoakExpr((pChnInfo)->status.defName.time, pSurInfo->pDb->mastConf.terSoakTime) == true)         \
                {                                                              \
                (pChnInfo)->failure.failName.status = false;                   \
                (pChnInfo)->failure.failName.time   = dCurTime;                \
                (failChg) = true;                                              \
                }                                                              \
            }                                                                  \
        }


/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*------------------------------------------------------------------------------
Prototype    : friend void SurImaLinkDefUpd(tSurImaLinkInfo *pImaLinkInfo, tSurImaLinkDef *pImaLinkDef)

Purpose      : This function is used to update IMA Link defect database

Inputs       : pImaLinkDef               - IMA Link defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurImaLinkDefUpd(tSurImaLinkInfo *pImaLinkInfo, tSurImaLinkDef *pImaLinkDef)
    {
    switch (pImaLinkDef->defect)
        {
        /* IMA Link Loss of IMA Frame */
        case cFmImaLinkLif:
        	if (pImaLinkInfo->status.lif.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.lif.status = pImaLinkDef->status;
				pImaLinkInfo->status.lif.time   = pImaLinkDef->time;
        		}
            break;
            
        /* IMA Link Out of Delay Synchronization */
        case cFmImaLinkLods:
        	if (pImaLinkInfo->status.lods.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.lods.status = pImaLinkDef->status;
				pImaLinkInfo->status.lods.time   = pImaLinkDef->time;
        		}
            break;

        /* IMA Link Remote Defect Indicator */
        case cFmImaLinkRdi:
        	if (pImaLinkInfo->status.rdi.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.rdi.status = pImaLinkDef->status;
				pImaLinkInfo->status.rdi.time   = pImaLinkDef->time;
        		}
            break;
    
        /* IMA Link Change of Tx LSM state NE */
        case cFmImaLinkTxLsmChange:
        	if (pImaLinkInfo->status.txLsm.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.txLsm.status = pImaLinkDef->status;
				pImaLinkInfo->status.txLsm.time   = pImaLinkDef->time;
        		}
            break;

        /* IMA Link Change of Rx LSM state NE */
        case cFmImaLinkRxLsmChange:
        	if (pImaLinkInfo->status.rxLsm.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.rxLsm.status = pImaLinkDef->status;
				pImaLinkInfo->status.rxLsm.time   = pImaLinkDef->time;
        		}
            break;

        /* IMA Link Change of Tx LSM state FE */
        case cFmImaLinkTxLsmChangeFe:
        	if (pImaLinkInfo->status.txLsmFe.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.txLsmFe.status = pImaLinkDef->status;
				pImaLinkInfo->status.txLsmFe.time   = pImaLinkDef->time;
        		}
            break;

        /* IMA Link Change of Rx LSM state FE */
        case cFmImaLinkRxLsmChangeFe:
        	if (pImaLinkInfo->status.rxLsmFe.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.rxLsmFe.status = pImaLinkDef->status;
				pImaLinkInfo->status.rxLsmFe.time   = pImaLinkDef->time;
        		}
            break;

        case cFmImaLinkPhyDef:
        	if (pImaLinkInfo->status.phyLinkDef.status != pImaLinkDef->status)
        		{
				pImaLinkInfo->status.phyLinkDef.status = pImaLinkDef->status;
				pImaLinkInfo->status.phyLinkDef.time   = pImaLinkDef->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaLinkAllDefUpd(tSurInfo        *pSurInfo,
											 const tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pImaLinkId            - IMA Link ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
private void ImaLinkAllDefUpd(tSurInfo        	  *pSurInfo,
							  const tSurImaLinkId *pImaLinkId)
    {
    bool          blNewStat;
    tSurImaLinkDef    def;
    tSurImaLinkInfo  *pImaLinkInfo;
    dword         dDefStat;

    pImaLinkInfo = NULL;
    /* Get IMA Link information */
    mSurImaLinkInfo(pSurInfo, pImaLinkId, pImaLinkInfo);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pImaLinkId, &(def.id), sizeof(tSurImaLinkId));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurImaLink, pImaLinkId, &dDefStat);
    
    /* IMA Link Loss of IMA Frame */
    blNewStat = mBitStat(dDefStat, cSurImaLinkLifMask);
    if (blNewStat != pImaLinkInfo->status.lif.status)
        {
        def.defect = cFmImaLinkLif;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Link Out of Delay Synchronization */
    blNewStat = mBitStat(dDefStat, cSurImaLinkLodsMask);
    if (blNewStat != pImaLinkInfo->status.lods.status)
        {
        def.defect = cFmImaLinkLods;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Link Remote Defect Indicator */
    blNewStat = mBitStat(dDefStat, cSurImaLinkRdiMask);
    if (blNewStat != pImaLinkInfo->status.rdi.status)
        {
        def.defect = cFmImaLinkRdi;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Link Changing of Tx LSM NE */
    blNewStat = mBitStat(dDefStat, cSurImaLinkTxLsmChangeMask);
    if (blNewStat != pImaLinkInfo->status.txLsm.status)
        {
        def.defect = cFmImaLinkTxLsmChange;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Link Changing of Rx LSM NE */
    blNewStat = mBitStat(dDefStat, cSurImaLinkRxLsmChangeMask);
    if (blNewStat != pImaLinkInfo->status.rxLsm.status)
        {
        def.defect = cFmImaLinkRxLsmChange;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Link Changing of Tx LSM FE */
    blNewStat = mBitStat(dDefStat, cSurImaLinkTxLsmChangeFeMask);
    if (blNewStat != pImaLinkInfo->status.txLsmFe.status)
        {
        def.defect = cFmImaLinkTxLsmChangeFe;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Link Changing of Rx LSM FE */
    blNewStat = mBitStat(dDefStat, cSurImaLinkRxLsmChangeFeMask);
    if (blNewStat != pImaLinkInfo->status.rxLsmFe.status)
        {
        def.defect = cFmImaLinkRxLsmChangeFe;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }

    /* IMA Physical link defect */
    blNewStat = mBitStat(dDefStat, cSurImaLinkPhyDefMask);
    if (blNewStat != pImaLinkInfo->status.phyLinkDef.status)
        {
        def.defect = cFmImaLinkPhyDef;
        def.status = blNewStat;
        SurImaLinkDefUpd(pImaLinkInfo, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaLinkAllFailUpd(tSurInfo        *pSurInfo,
											  const tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pImaLinkId            - IMA Link ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ImaLinkAllFailUpd(tSurInfo        		*pSurInfo,
								   const tSurImaLinkId 	*pImaLinkId)
    {
    tSurTrblProf  *pTrblProf;
    tSurImaLinkFail   *pFailInd;
    tSortList      failHis;
    tSurImaLinkInfo   *pImaLinkInfo;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;
    bool		   failStatus;

    pImaLinkInfo = NULL;
    blNotf		 = true;

    /* Get IMA Link information */
    mSurImaLinkInfo(pSurInfo, pImaLinkId, pImaLinkInfo);
    
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pImaLinkInfo->failure);
    failHis   = pImaLinkInfo->failHis;
    
    /* IMA Link LIF failure */
    mFailIndUpd(pSurInfo, pImaLinkInfo, lif, lif, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.lif == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->lif, failHis, failInfo, blNotf, cFmImaLinkLif, pTrblProf->imaLinkLif);

        /* Update PM parameter Rx FC */
        if (pImaLinkInfo->failure.lif.status == true)
        	{
        	pImaLinkInfo->parm.fcRx.curSec += 1;
        	}
        }
    
    /* IMA Link LODS failure */
    mFailIndUpd(pSurInfo, pImaLinkInfo, lods, lods, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.lods == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->lods, failHis, failInfo, blNotf, cFmImaLinkLods, pTrblProf->imaLinkLods);

        /* Update PM parameter Rx FC */
        if (pImaLinkInfo->failure.lods.status == true)
        	{
        	pImaLinkInfo->parm.fcRx.curSec += 1;
        	}
        }

    /* IMA Link RFI */
    mFailIndUpd(pSurInfo, pImaLinkInfo, rdi, rfi, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.rfi == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->rfi, failHis, failInfo, blNotf, cFmImaLinkRfi, pTrblProf->imaLinkRfi);

        /* Update PM parameter Far-end Rx FC */
        if (pImaLinkInfo->failure.rfi.status == true)
        	{
			pImaLinkInfo->parm.fcRxfe.curSec += 1;
        	}
        }

    /* IMA Link Tx Mis-connected failure is happened if Tx link is detected as mis-connected
     * (do not wait soaking time) */
    if (pImaLinkInfo->failure.txMisConn.status != (((pImaLinkInfo)->status.txLsm.status == cFmImaLinkUnusableMisConn)? true : false))
        {
        /* Set failure indication */
        if (pImaLinkInfo->status.txLsm.status == cFmImaLinkUnusableMisConn)
            {
            pImaLinkInfo->failure.txMisConn.status = 1;
			pImaLinkInfo->failure.txMisConn.time   = dCurTime;

			/* Update PM parameter Tx FC */
			pImaLinkInfo->parm.fcTx.curSec += 1;
            }
        /* Clear failure indication */
        else
            {
			(pImaLinkInfo)->failure.txMisConn.status = 0;
			(pImaLinkInfo)->failure.txMisConn.time   = dCurTime;
            }

    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.txMisConn == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->txMisConn, failHis, failInfo, blNotf, cFmImaLinkTxMisConn, pTrblProf->imaLinkTxMisConn);
        }

    /* IMA Link Rx Mis-connected failure is happened if Rx link is detected as mis-connected
     * (do not wait soaking time) */
    if (pImaLinkInfo->failure.rxMisConn.status != (((pImaLinkInfo)->status.rxLsm.status == cFmImaLinkUnusableMisConn)? true : false))
        {
        /* Set failure indication */
        if (pImaLinkInfo->status.rxLsm.status == cFmImaLinkUnusableMisConn)
            {
            pImaLinkInfo->failure.rxMisConn.status = 1;
			pImaLinkInfo->failure.rxMisConn.time   = dCurTime;

			/* Update PM parameter Rx FC */
			pImaLinkInfo->parm.fcRx.curSec += 1;
            }
        /* Clear failure indication */
        else
            {
			(pImaLinkInfo)->failure.rxMisConn.status = 0;
			(pImaLinkInfo)->failure.rxMisConn.time   = dCurTime;
            }

    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.rxMisConn == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->rxMisConn, failHis, failInfo, blNotf, cFmImaLinkRxMisConn, pTrblProf->imaLinkRxMisConn);
        }

    /* IMA Link Tx Fault */
    mFailLsmIndUpd(pSurInfo, pImaLinkInfo, txLsm, txFault, cFmImaLinkUnusableFault, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.txFault == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->txFault, failHis, failInfo, blNotf, cFmImaLinkTxFault, pTrblProf->imaLinkTxFault);

        /* Update PM parameter Tx FC */
        if (pImaLinkInfo->failure.txFault.status == true)
        	{
			pImaLinkInfo->parm.fcTx.curSec += 1;
        	}
        }

    /* IMA Link Rx Fault */
    mFailLsmIndUpd(pSurInfo, pImaLinkInfo, rxLsm, rxFault, cFmImaLinkUnusableFault, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.rxFault == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->rxFault, failHis, failInfo, blNotf, cFmImaLinkRxFault, pTrblProf->imaLinkRxFault);

        /* Update PM parameter Rx FC */
        if (pImaLinkInfo->failure.rxFault.status == true)
        	{
			pImaLinkInfo->parm.fcRx.curSec += 1;
        	}
        }

    /* IMA Link Tx Unusable FE failure is happened when the FE reports Tx-Unusable
       (do not wait soaking time) */
    failStatus = (((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableFailed) ||
				 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableFault) ||
				 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableInhibited) ||
				 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableMisConn) ||
				 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableNoReason))? 1 : 0;
    if (pImaLinkInfo->failure.txUnusableFe.status != failStatus) /* Status changed */
        {
		pImaLinkInfo->failure.txUnusableFe.status = failStatus;
		pImaLinkInfo->failure.txUnusableFe.time   = dCurTime;

    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.txUnusableFe == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->txUnusableFe, failHis, failInfo, blNotf, cFmImaLinkTxUnusableFe, pTrblProf->imaLinkTxUnusableFe);

        /* Update PM parameter Far-end Tx FC */
        if (pImaLinkInfo->failure.txUnusableFe.status == true)
        	{
			pImaLinkInfo->parm.fcTxfe.curSec += 1;
        	}
        }

    /* IMA Link Rx Unusable FE failure is happened when the FE reports Rx-Unusable
       (do not wait soaking time) */
    failStatus = (((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableFailed) ||
				 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableFault) ||
				 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableInhibited) ||
				 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableMisConn) ||
				 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableNoReason))? 1 : 0;
    if (pImaLinkInfo->failure.rxUnusableFe.status != failStatus) /* Status changed */
        {
		pImaLinkInfo->failure.rxUnusableFe.status = failStatus;
		pImaLinkInfo->failure.rxUnusableFe.time   = dCurTime;

    	blNotf = ((pImaLinkInfo->msgInh.imaLink == false) &&
				  (pImaLinkInfo->msgInh.rxUnusableFe == false));
        mFailRept(pSurInfo, cSurImaLink, pImaLinkId, pFailInd->rxUnusableFe, failHis, failInfo, blNotf, cFmImaLinkRxUnusableFe, pTrblProf->imaLinkRxUnusableFe);

        /* Update PM parameter Far-end Rx FC */
        if (pImaLinkInfo->failure.rxUnusableFe.status == true)
        	{
			pImaLinkInfo->parm.fcRxfe.curSec += 1;
        	}
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaLinkPmParmUpd(tSurInfo        	 *pSurInfo,
											 const tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pImaLinkId            - IMA Link ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ImaLinkPmParmUpd(tSurInfo        	  *pSurInfo,
								  const tSurImaLinkId *pImaLinkId)
    {
    tSurImaLinkInfo   *pImaLinkInfo;
    bool           blNewStat;
    tSurImaLinkAnomaly anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pImaLinkInfo = NULL;
    /* Get IMA Link information */
    mSurImaLinkInfo(pSurInfo, pImaLinkId, pImaLinkInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pImaLinkInfo->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pImaLinkInfo->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pImaLinkInfo->parm.curEngTime.minute)
        {
        pImaLinkInfo->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pImaLinkInfo->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pImaLinkInfo->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurImaLinkAnomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurImaLink, pImaLinkId, &anomaly);

        pImaLinkInfo->parm.iv.curSec += anomaly.iv;
        pImaLinkInfo->parm.oif.curSec += anomaly.oif;
        pImaLinkInfo->parm.stufTx.curSec += anomaly.stuffTx;
        pImaLinkInfo->parm.stufRx.curSec += anomaly.stuffRx;
        pImaLinkInfo->parm.totalIcp += anomaly.totalIcp;
        }
    
    /* Update SES parameters based on Physical Link defect (LOS/OOF/LOF/AIS)
       or LIF or LODS */
    if ((pImaLinkInfo->status.phyLinkDef.status == true) ||
    	(pImaLinkInfo->status.lif.status == true) ||
    	(pImaLinkInfo->status.lods.status == true))
        {
        pImaLinkInfo->parm.ses.curSec   = 1;
        }
        
    /* Update SES-FE parameters based on RDI */
    if (pImaLinkInfo->status.rdi.status == true)
        {
        pImaLinkInfo->parm.sesfe.curSec   = 1;
        }

    /* Second expired */
    if (isSecExpr == true)
        {
    	/* Update SES parameters */
    	/* In 1 second, if ICP violation cells >= 30% ICP cells, update SES */
    	if (pImaLinkInfo->parm.iv.curSec * 100 >= pImaLinkInfo->parm.totalIcp * 30)
    		{
    		pImaLinkInfo->parm.ses.curSec   = 1;
    		}

		/* Update UAS- parameter during unavailable time (near-end and far-end) */
        if (pImaLinkInfo->status.neUat == true)
            {
            pImaLinkInfo->parm.uas.curSec = 1;
            }
        if (pImaLinkInfo->status.feUat == true)
            {
            pImaLinkInfo->parm.uasfe.curSec = 1;
            }

        /* Check if IMA Link Near-End UAS just enter or exit and adjust registers */
        blNewStat = (pImaLinkInfo->parm.ses.curSec == 1)? true : false;
        if (blNewStat != pImaLinkInfo->status.neUatPot)
            {
            pImaLinkInfo->status.nePotCnt = 1;
            }
        else
            {
            pImaLinkInfo->status.nePotCnt++;
            }

        /* Declare potential for entering or exiting UAT if the condition is met */
        if (pImaLinkInfo->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pImaLinkInfo->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pImaLinkInfo->parm.oif));
                mCurPerNegAdj(&(pImaLinkInfo->parm.ses));
                mCurPerNegAdj(&(pImaLinkInfo->parm.stufTx));
                mCurPerNegAdj(&(pImaLinkInfo->parm.stufRx));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pImaLinkInfo->parm.oif));
                    mPrePerNegAdj(&(pImaLinkInfo->parm.ses));
                    mPrePerNegAdj(&(pImaLinkInfo->parm.stufTx));
                    mPrePerNegAdj(&(pImaLinkInfo->parm.stufRx));
                    }

                /* Update UAS */
            	pImaLinkInfo->parm.uas.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pImaLinkInfo->parm.iv));
                mCurPerPosAdj(&(pImaLinkInfo->parm.oif));
                mCurPerPosAdj(&(pImaLinkInfo->parm.ses));
                mCurPerPosAdj(&(pImaLinkInfo->parm.stufTx));
                mCurPerPosAdj(&(pImaLinkInfo->parm.stufRx));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pImaLinkInfo->parm.iv));
                    mPrePerPosAdj(&(pImaLinkInfo->parm.oif));
                    mPrePerPosAdj(&(pImaLinkInfo->parm.ses));
                    mPrePerPosAdj(&(pImaLinkInfo->parm.stufTx));
                    mPrePerPosAdj(&(pImaLinkInfo->parm.stufRx));
                    }

                /* Update UAS */
            	if (pImaLinkInfo->parm.uas.curPer.value >= 10)
            		{
            		pImaLinkInfo->parm.uas.curPer.value -= 10;
            		}
            	else
            		{
            		pImaLinkInfo->parm.uas.curPer.value = 0;
            		}
            	pImaLinkInfo->parm.uas.curSec = 0;
                }
            }

        /* Check if IMA Link Far-End UAS just enter or exit and adjust registers */
		blNewStat = (pImaLinkInfo->parm.sesfe.curSec == 1) ? true : false;
		if (blNewStat != pImaLinkInfo->status.feUatPot)
			{
			pImaLinkInfo->status.fePotCnt = 1;
			}
		else
			{
			pImaLinkInfo->status.fePotCnt++;
			}

		/* Declare potential for entering or exiting UAT if the condition is met */
		if (pImaLinkInfo->status.fePotCnt == 10)
			{
			/* Update unavailable time status */
			pImaLinkInfo->status.feUat = blNewStat;

			/* UAS just entered */
			if (blNewStat == true)
				{
				mCurPerNegAdj(&(pImaLinkInfo->parm.sesfe));
				if (((currentTime.minute % 15) == 0) &&
					(currentTime.second < 10))
					{
					mPrePerNegAdj(&(pImaLinkInfo->parm.sesfe));
					}

                /* Update UAS */
            	pImaLinkInfo->parm.uasfe.curPer.value += 10;
				}

			/* UAS just exited */
			else
				{
				mCurPerPosAdj(&(pImaLinkInfo->parm.sesfe));
				if (((currentTime.minute % 15) == 0) &&
					(currentTime.second < 10))
					{
					mPrePerPosAdj(&(pImaLinkInfo->parm.sesfe));
					}

                /* Update UAS */
            	if (pImaLinkInfo->parm.uasfe.curPer.value >= 10)
            		{
            		pImaLinkInfo->parm.uasfe.curPer.value -= 10;
            		}
            	else
            		{
            		pImaLinkInfo->parm.uasfe.curPer.value = 0;
            		}
            	pImaLinkInfo->parm.uasfe.curSec = 0;
				}
			}

        /* Reset all parameters' accumulate inhibition mode */
        pImaLinkInfo->parm.iv.inhibit     	= false;
        pImaLinkInfo->parm.oif.inhibit     	= false;
        pImaLinkInfo->parm.ses.inhibit     	= false;
        pImaLinkInfo->parm.uusTx.inhibit    = false;
        pImaLinkInfo->parm.uusRx.inhibit    = false;
        pImaLinkInfo->parm.fcTx.inhibit   	= false;
        pImaLinkInfo->parm.fcRx.inhibit 	= false;
        pImaLinkInfo->parm.stufTx.inhibit   = false;
        pImaLinkInfo->parm.stufRx.inhibit   = false;
        pImaLinkInfo->parm.sesfe.inhibit   	= false;
        pImaLinkInfo->parm.uusTxfe.inhibit  = false;
        pImaLinkInfo->parm.uusRxfe.inhibit  = false;
        pImaLinkInfo->parm.fcTxfe.inhibit  	= false;
        pImaLinkInfo->parm.fcRxfe.inhibit  	= false;

        /* Inhibit IV, OIF, SES, Tx Stuff, Rx Stuff parameters if
           the current second is UAS (near-end and far-end) */
        if ((pImaLinkInfo->status.neUat == true))
            {
            pImaLinkInfo->parm.iv.inhibit     = true;
            pImaLinkInfo->parm.oif.inhibit     = true;
            pImaLinkInfo->parm.ses.inhibit    = true;
            pImaLinkInfo->parm.stufTx.inhibit    = true;
            pImaLinkInfo->parm.stufRx.inhibit    = true;
            }

        if (pImaLinkInfo->status.feUat == true)
            {
            pImaLinkInfo->parm.sesfe.inhibit   = true;
            }

        /* Inhibit IV, OIF, Tx Stuff, Rx Stuff when the current second is in SES */
        if (pImaLinkInfo->parm.ses.curSec != 0)
            {
            pImaLinkInfo->parm.iv.inhibit     	= true;
            pImaLinkInfo->parm.oif.inhibit     	= true;
            pImaLinkInfo->parm.stufTx.inhibit   = true;
            pImaLinkInfo->parm.stufRx.inhibit   = true;
            }

        /* Increase IMA Link Near-End parameters' adjustment registers */
        /* In available time */
        if (pImaLinkInfo->status.neUat == false)
            {
            /* Potential for entering unavailable time */
            if ((pImaLinkInfo->parm.ses.curSec == 1))
                {
                mNegInc(&(pImaLinkInfo->parm.oif));
                mNegInc(&(pImaLinkInfo->parm.ses));
                mNegInc(&(pImaLinkInfo->parm.stufTx));
                mNegInc(&(pImaLinkInfo->parm.stufRx));
                }
            }
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pImaLinkInfo->parm.ses.curSec == 0)
                {
                mPosInc(&(pImaLinkInfo->parm.iv));
                mPosInc(&(pImaLinkInfo->parm.oif));
                mPosInc(&(pImaLinkInfo->parm.ses));
                mPosInc(&(pImaLinkInfo->parm.stufTx));
                mPosInc(&(pImaLinkInfo->parm.stufRx));
                }
            }

        /* Increase IMA Link Far-End parameter's adjustment registers */
        if (pImaLinkInfo->status.neUat == false)
            {
            /* In available time */
            if (pImaLinkInfo->status.feUat == false)
                {
                /* Potential for entering unavailable time */
                if (pImaLinkInfo->parm.sesfe.curSec == 1)
                    {
                    mNegInc(&(pImaLinkInfo->parm.sesfe));
                    }
                }

            /* In unavailable time */
            else
                {
                /* Potential for exiting unavailable time */
                if (pImaLinkInfo->parm.sesfe.curSec == 0)
                    {
                    mPosInc(&(pImaLinkInfo->parm.sesfe));
                    }
                }
            }

        /* Clear Near-End parameters' adjustment registers */
        if (pImaLinkInfo->status.neUat == false)
            {
            if ((pImaLinkInfo->parm.ses.curSec == 0) &&
                (pImaLinkInfo->status.neUatPot  == true))
                {
                mNegClr(&(pImaLinkInfo->parm.oif));
                mNegClr(&(pImaLinkInfo->parm.ses));
                mNegClr(&(pImaLinkInfo->parm.stufTx));
                mNegClr(&(pImaLinkInfo->parm.stufRx));
                }
            }
        else if ((pImaLinkInfo->parm.ses.curSec == 1) &&
                 (pImaLinkInfo->status.neUatPot  == false))
            {
            mPosClr(&(pImaLinkInfo->parm.iv));
            mPosClr(&(pImaLinkInfo->parm.oif));
            mPosClr(&(pImaLinkInfo->parm.ses));
            mPosClr(&(pImaLinkInfo->parm.stufTx));
            mPosClr(&(pImaLinkInfo->parm.stufRx));
            }

        /* Clear IMA Link Far-End parameters' adjustment registers */
		if (pImaLinkInfo->status.feUat == false)
			{
			if ((pImaLinkInfo->parm.sesfe.curSec == 0) &&
				(pImaLinkInfo->status.feUatPot    == true))
				{
				mNegClr(&(pImaLinkInfo->parm.sesfe));
				}
			}
		else if ((pImaLinkInfo->parm.sesfe.curSec == 1) &&
				 (pImaLinkInfo->status.feUatPot    == false))
			{
			mPosClr(&(pImaLinkInfo->parm.sesfe));
			}

        /* Store current second statuses */
        pImaLinkInfo->status.neUatPot = (pImaLinkInfo->parm.ses.curSec   == 1) ? true : false;
        pImaLinkInfo->status.feUatPot = (pImaLinkInfo->parm.sesfe.curSec == 1) ? true : false;

        /* Update UUS NE */
        if (((pImaLinkInfo)->status.txLsm.status == cFmImaLinkUnusableFailed) ||
			 ((pImaLinkInfo)->status.txLsm.status == cFmImaLinkUnusableFault) ||
			 ((pImaLinkInfo)->status.txLsm.status == cFmImaLinkUnusableInhibited) ||
			 ((pImaLinkInfo)->status.txLsm.status == cFmImaLinkUnusableMisConn) ||
			 ((pImaLinkInfo)->status.txLsm.status == cFmImaLinkUnusableNoReason))
        	{
        	pImaLinkInfo->parm.uusTx.curSec = 1;
        	}

        if (((pImaLinkInfo)->status.rxLsm.status == cFmImaLinkUnusableFailed) ||
			 ((pImaLinkInfo)->status.rxLsm.status == cFmImaLinkUnusableFault) ||
			 ((pImaLinkInfo)->status.rxLsm.status == cFmImaLinkUnusableInhibited) ||
			 ((pImaLinkInfo)->status.rxLsm.status == cFmImaLinkUnusableMisConn) ||
			 ((pImaLinkInfo)->status.rxLsm.status == cFmImaLinkUnusableNoReason))
        	{
        	pImaLinkInfo->parm.uusRx.curSec = 1;
        	}

        /* Update UUS FE */
        if (((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableFailed) ||
			 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableFault) ||
			 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableInhibited) ||
			 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableMisConn) ||
			 ((pImaLinkInfo)->status.txLsmFe.status == cFmImaLinkUnusableNoReason))
        	{
        	pImaLinkInfo->parm.uusTxfe.curSec = 1;
        	}
        if (((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableFailed) ||
			 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableFault) ||
			 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableInhibited) ||
			 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableMisConn) ||
			 ((pImaLinkInfo)->status.rxLsmFe.status == cFmImaLinkUnusableNoReason))
        	{
        	pImaLinkInfo->parm.uusRxfe.curSec = 1;
        	}

        /* Accumulate current period registers */
        mCurPerAcc(&(pImaLinkInfo->parm.iv));
        mCurPerAcc(&(pImaLinkInfo->parm.oif));
        mCurPerAcc(&(pImaLinkInfo->parm.ses));
        mCurPerAcc(&(pImaLinkInfo->parm.uas));
        mCurPerAcc(&(pImaLinkInfo->parm.uusTx));
        mCurPerAcc(&(pImaLinkInfo->parm.uusRx));
        mCurPerAcc(&(pImaLinkInfo->parm.fcTx));
        mCurPerAcc(&(pImaLinkInfo->parm.fcRx));
        mCurPerAcc(&(pImaLinkInfo->parm.stufTx));
        mCurPerAcc(&(pImaLinkInfo->parm.stufRx));
        mCurPerAcc(&(pImaLinkInfo->parm.sesfe));
        mCurPerAcc(&(pImaLinkInfo->parm.uasfe));
        mCurPerAcc(&(pImaLinkInfo->parm.uusTxfe));
        mCurPerAcc(&(pImaLinkInfo->parm.uusRxfe));
        mCurPerAcc(&(pImaLinkInfo->parm.fcTxfe));
        mCurPerAcc(&(pImaLinkInfo->parm.fcRxfe));
    	pImaLinkInfo->parm.totalIcp = 0;

        /* Check and report TCA */
        blNotf = ((pImaLinkInfo->msgInh.imaLink    == false) &&
                  (pImaLinkInfo->msgInh.tca        == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;

        /* Report TCAs of parameters are independent of SES and UAS */
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkUusTx,   &(pImaLinkInfo->parm.uusTx),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkUusRx,   &(pImaLinkInfo->parm.uusRx),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkFcTx,  &(pImaLinkInfo->parm.fcTx),  blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkFcRx, &(pImaLinkInfo->parm.fcRx), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkUusTxfe, &(pImaLinkInfo->parm.uusTxfe), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkUusRxfe, &(pImaLinkInfo->parm.uusRxfe), blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkFcTxfe,  &(pImaLinkInfo->parm.fcTxfe),  blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkFcRxfe, &(pImaLinkInfo->parm.fcRxfe), blNotf, &tcaInfo);

        /* Report TCAs of Near-End parameters */
        if ((pImaLinkInfo->status.neUat    == false) &&
            (pImaLinkInfo->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkIv    ,&(pImaLinkInfo->parm.iv),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkOif    ,&(pImaLinkInfo->parm.oif),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkSes   ,&(pImaLinkInfo->parm.ses),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkStufTx   ,&(pImaLinkInfo->parm.stufTx),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkStufRx   ,&(pImaLinkInfo->parm.stufRx),    blNotf, &tcaInfo);
            }

        /* Report TCAs of Far-End parameters */
        if ((pImaLinkInfo->status.feUat    == false) &&
            (pImaLinkInfo->status.feUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkSesfe    ,&(pImaLinkInfo->parm.sesfe),     blNotf, &tcaInfo);
            }

        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkUas, &(pImaLinkInfo->parm.uas), blNotf, &tcaInfo);
		SurTcaReport(pSurInfo, cSurImaLink, pImaLinkId, pImaLinkInfo, cPmImaLinkUasfe, &(pImaLinkInfo->parm.uasfe), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pImaLinkInfo->parm.iv));
        mStackDown(&(pImaLinkInfo->parm.oif));
        mStackDown(&(pImaLinkInfo->parm.ses));
        mStackDown(&(pImaLinkInfo->parm.uas));
        mStackDown(&(pImaLinkInfo->parm.uusTx));
        mStackDown(&(pImaLinkInfo->parm.uusRx));
        mStackDown(&(pImaLinkInfo->parm.fcTx));
        mStackDown(&(pImaLinkInfo->parm.fcRx));
        mStackDown(&(pImaLinkInfo->parm.stufTx));
        mStackDown(&(pImaLinkInfo->parm.stufRx));
        mStackDown(&(pImaLinkInfo->parm.sesfe));
        mStackDown(&(pImaLinkInfo->parm.uasfe));
        mStackDown(&(pImaLinkInfo->parm.uusTxfe));
        mStackDown(&(pImaLinkInfo->parm.uusRxfe));
        mStackDown(&(pImaLinkInfo->parm.fcTxfe));
        mStackDown(&(pImaLinkInfo->parm.fcRxfe));

        /* Update time for period */
        pImaLinkInfo->parm.prePerStartTime = pImaLinkInfo->parm.curPerStartTime;
        pImaLinkInfo->parm.prePerEndTime = dCurTime;
        pImaLinkInfo->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pImaLinkInfo->parm.iv));
        mDayShift(&(pImaLinkInfo->parm.oif));
        mDayShift(&(pImaLinkInfo->parm.ses));
        mDayShift(&(pImaLinkInfo->parm.uas));
        mDayShift(&(pImaLinkInfo->parm.uusTx));
        mDayShift(&(pImaLinkInfo->parm.uusRx));
        mDayShift(&(pImaLinkInfo->parm.fcTx));
        mDayShift(&(pImaLinkInfo->parm.fcRx));
        mDayShift(&(pImaLinkInfo->parm.stufTx));
        mDayShift(&(pImaLinkInfo->parm.stufRx));
        mDayShift(&(pImaLinkInfo->parm.sesfe));
        mDayShift(&(pImaLinkInfo->parm.uasfe));
        mDayShift(&(pImaLinkInfo->parm.uusTxfe));
        mDayShift(&(pImaLinkInfo->parm.uusRxfe));
        mDayShift(&(pImaLinkInfo->parm.fcTxfe));
        mDayShift(&(pImaLinkInfo->parm.fcRxfe));

        /* Update time for day */
        pImaLinkInfo->parm.preDayStartTime = pImaLinkInfo->parm.curDayStartTime;
        pImaLinkInfo->parm.preDayEndTime = dCurTime;
        pImaLinkInfo->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurImaLinkStatUpd(tSurDev          device,
											 const tSurImaLinkId *pImaLinkId)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input IMA Link.

Inputs       : device                - device
               pImaLinkId            - IMA Link ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurImaLinkStatUpd(tSurDev device, const tSurImaLinkId *pImaLinkId)
    {
    tSurInfo       *pSurInfo;

    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) && 
        (pSurInfo->interface.DefStatGet    != null))
        {
        ImaLinkAllDefUpd(pSurInfo, pImaLinkId);
        }
    
    /* Declare/terminate failures */
    ImaLinkAllFailUpd(pSurInfo, pImaLinkId);
    
    /* Update performance registers */
    ImaLinkPmParmUpd(pSurInfo, pImaLinkId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengde3.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code DS1/E1 Transmisstion 
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mDe3LineK(pSurInfo)     
            mDe3PathPbitK(pSurInfo) 
            mDe3PathCPbitK(pSurInfo)
Purpose   : These macros are used to get K thresholds for DS3/E3 line and path.
Inputs    : pSurInfo                 device database
Return    : K threshold value
------------------------------------------------------------------------------*/
#define mDe3LineK(pSurInfo)      ((pSurInfo)->pDb->mastConf.kThres.ds3Line)
#define mDe3PathPbitK(pSurInfo)  ((pSurInfo)->pDb->mastConf.kThres.ds3PathPbit)
#define mDe3PathCPbitK(pSurInfo) ((pSurInfo)->pDb->mastConf.kThres.ds3PathCPbit)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : friend void SurDe3DefUpd(tSurDe3Info *pDe3Info, tSurDe3Def *pDe3Def)

Purpose      : This function is used to update DS3/E3 defect database

Inputs       : pDe3Def               - DS3/E3 defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurDe3DefUpd(tSurDe3Info *pDe3Info, tSurDe3Def *pDe3Def)
    {
    switch (pDe3Def->defect)
        {
        /* DS3/E3 Line LOS defect */
        case cFmDe3Los:
        	if (pDe3Info->status.los.status != pDe3Def->status)
        		{
				pDe3Info->status.los.status = pDe3Def->status;
				pDe3Info->status.los.time   = pDe3Def->time;
        		}
            break;
            
        /* DS3/E3 Path OOF defect */
        case cFmDe3Oof:
        	if (pDe3Info->status.oof.status != pDe3Def->status)
        		{
				pDe3Info->status.oof.status = pDe3Def->status;
				pDe3Info->status.oof.time   = pDe3Def->time;
        		}
            break;
        
        /* DS3/E3 Path SEF defect */
        case cFmDe3Sef:
        	if (pDe3Info->status.sef.status != pDe3Def->status)
        		{
				pDe3Info->status.sef.status = pDe3Def->status;
				pDe3Info->status.sef.time   = pDe3Def->time;
        		}
            break;
            
        /* DS3/E3 Path AIS defect */
        case cFmDe3Ais:
        	if (pDe3Info->status.ais.status != pDe3Def->status)
        		{
				pDe3Info->status.ais.status = pDe3Def->status;
				pDe3Info->status.ais.time   = pDe3Def->time;
        		}
            break;
            
        /* DS3/E3 Far-End Path RAI defect */
        case cFmDe3Rai:
        	if (pDe3Info->status.rai.status != pDe3Def->status)
        		{
				pDe3Info->status.rai.status     = pDe3Def->status;
				pDe3Info->status.rai.time       = pDe3Def->time;
        		}
            break;

        /* DS3/E3 Far-End Path AIS/SEF defect */
        case cFmDe3SefAisFar:
        	if (pDe3Info->status.sefAisFe.status != pDe3Def->status)
        		{
				pDe3Info->status.sefAisFe.status = pDe3Def->status;
				pDe3Info->status.sefAisFe.time   = pDe3Def->time;
        		}
            break;
            
        /* L bit failure */
        case cFmDe3Lbit:
        	if (pDe3Info->status.lBit.status != pDe3Def->status)
        		{
				pDe3Info->status.lBit.status = pDe3Def->status;
				pDe3Info->status.lBit.time   = pDe3Def->time;
        		}
            break;
        /* R bit failure */
        case cFmDe3Rbit:
        	if (pDe3Info->status.rBit.status != pDe3Def->status)
        		{
				pDe3Info->status.rBit.status = pDe3Def->status;
				pDe3Info->status.rBit.time   = pDe3Def->time;
        		}
            break;
        /* M bit failure */
        case cFmDe3Mbit:
        	if (pDe3Info->status.mBit.status != pDe3Def->status)
        		{
				pDe3Info->status.mBit.status = pDe3Def->status;
				pDe3Info->status.mBit.time   = pDe3Def->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De3AllDefUpd(tSurInfo        *pSurInfo,
                                         const tSurDe3Id *pDe3Id)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pDe3Id                - DS3/E3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
private void De3AllDefUpd(tSurInfo        *pSurInfo,
                          const tSurDe3Id *pDe3Id)
    {
    bool          blNewStat;
    tSurDe3Def    def;
    tSurDe3Info  *pDe3Info;
    dword         dDefStat;

    /* Get DS3/E3 information */
    mSurDe3Info(pSurInfo, pDe3Id, pDe3Info);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pDe3Id, &(def.id), sizeof(tSurDe3Id));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurDe3, pDe3Id, &dDefStat);
    
    /* DS3/E3 Line Loss Of Signal */
    blNewStat = mBitStat(dDefStat, cSurDe3LosMask);
    if (blNewStat != pDe3Info->status.los.status)
        {
        def.defect = cFmDe3Los;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* DS3/E3 Path Out Of Frame */
    blNewStat = mBitStat(dDefStat, cSurDe3OofMask);
    if (blNewStat != pDe3Info->status.oof.status)
        {
        def.defect = cFmDe3Oof;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* DS3/E3 Path Severely Errored Frame */
    blNewStat = mBitStat(dDefStat, cSurDe3SefMask);
    if (blNewStat != pDe3Info->status.sef.status)
        {
        def.defect = cFmDe3Sef;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* DS3/E3 Path Alarm Indication Signal */
    blNewStat = mBitStat(dDefStat, cSurDe3AisMask);
    if (blNewStat != pDe3Info->status.ais.status)
        {
        def.defect = cFmDe3Ais;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* Far-End SEF/AIS */
    blNewStat = mBitStat(dDefStat, cSurDe3SefAisFeMask);
    if (blNewStat != pDe3Info->status.sefAisFe.status)
        {
        def.defect = cFmDe3SefAisFar;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* DS3/E3 Remote Alarm Indication */
    blNewStat = mBitStat(dDefStat, cSurDe3RaiMask);
    if (blNewStat != pDe3Info->status.rai.status)
        {
        def.defect = cFmDe3Rai;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* L bit failure */
    blNewStat = mBitStat(dDefStat, cSurDe3LbitMask);
    if (blNewStat != pDe3Info->status.lBit.status)
        {
        def.defect = cFmDe3Lbit;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* R bit failure */
    blNewStat = mBitStat(dDefStat, cSurDe3RbitMask);
    if (blNewStat != pDe3Info->status.rBit.status)
        {
        def.defect = cFmDe3Rbit;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }

    /* M bit failure */
    blNewStat = mBitStat(dDefStat, cSurDe3MbitMask);
    if (blNewStat != pDe3Info->status.mBit.status)
        {
        def.defect = cFmDe3Mbit;
        def.status = blNewStat;
        SurDe3DefUpd(pDe3Info, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De3AllFailUpd(tSurInfo        *pSurInfo,
                                          const tSurDe3Id *pDe3Id)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pDe3Id                - DS3/E3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void De3AllFailUpd(tSurInfo        *pSurInfo,
                           const tSurDe3Id *pDe3Id)
    {
    tSurTrblProf  *pTrblProf;
    tSurDe3Fail   *pFailInd;
    tSortList      failHis;
    tSurDe3Info   *pDe3Info;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;

    /* Get DS3/E3 information */
    mSurDe3Info(pSurInfo, pDe3Id, pDe3Info);
    
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pDe3Info->failure);
    failHis   = pDe3Info->failHis;
    
    /* DS3/E3 Line LOS failure */
    mFailIndUpd(pSurInfo, pDe3Info, los, los, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe3Info->msgInh.de3 == false) && 
                  (pDe3Info->msgInh.los == false));
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->los, failHis, failInfo, blNotf, cFmDe3Los, pTrblProf->de3Los);
        }
    
    /* DS3/E3 Path LOF failure */
    mFailIndUpd(pSurInfo, pDe3Info, oof, lof, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe3Info->msgInh.de3 == false) && 
                  (pDe3Info->msgInh.lof == false));
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->lof, failHis, failInfo, blNotf, cFmDe3Lof, pTrblProf->de3Lof);
        
        /* Update FC-P parameter */
        if (pFailInd->lof.status == true)
            {
            pDe3Info->parm.fcP.curSec += 1;
            }
        }
    
    /* DS3/E3 Path AIS failure */
    mFailIndUpd(pSurInfo, pDe3Info, ais, ais, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe3Info->msgInh.de3 == false) && 
                  (pDe3Info->msgInh.ais == false));
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->ais, failHis, failInfo, blNotf, cFmDe3Ais, pTrblProf->de3Ais);
        
        /* Update FC-P parameter */
        if (pFailInd->ais.status == true)
            {
            pDe3Info->parm.fcP.curSec += 1;
            }
        }
        
    /* DS3/E3 Path RAI failure */
    blFailChg = (pDe3Info->status.rai.status != pFailInd->rai.status);
    if (blFailChg == true)
        {
        pFailInd->rai.status    = pDe3Info->status.rai.status;
        pFailInd->rai.time      = dCurTime;
        
        /* Report failure */
        blNotf = (pDe3Info->msgInh.de3 == false) && 
                 (pDe3Info->msgInh.rai == false);
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->rai,failHis, failInfo, blNotf, cFmDe3Rai, pTrblProf->de3Rai);
        
        /* Update FC-PFE parameter */
        if (pFailInd->rai.status == true)
            {
            pDe3Info->parm.fcPfe.curSec += 1;
            }
        }

    /* L bit failure */
    mFailIndUpd(pSurInfo, pDe3Info, lBit, lBit, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe3Info->msgInh.de3 == false) &&
                  (pDe3Info->msgInh.lBit == false));
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->lBit, failHis, failInfo, blNotf, cFmDe3Lbit, pTrblProf->lBit);
        }

    /* R bit failure */
    mFailIndUpd(pSurInfo, pDe3Info, rBit, rBit, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe3Info->msgInh.de3 == false) &&
                  (pDe3Info->msgInh.rBit == false));
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->rBit, failHis, failInfo, blNotf, cFmDe3Rbit, pTrblProf->rBit);
        }

    /* M bit failure */
    mFailIndUpd(pSurInfo, pDe3Info, mBit, mBit, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pDe3Info->msgInh.de3 == false) &&
                  (pDe3Info->msgInh.mBit == false));
        mFailRept(pSurInfo, cSurDe3, pDe3Id, pFailInd->mBit, failHis, failInfo, blNotf, cFmDe3Mbit, pTrblProf->mBit);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void De3PmParmUpd(tSurInfo        *pSurInfo,
                                         const tSurDe3Id *pDe3Id)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pDe3Id                - DS3/E3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void De3PmParmUpd(tSurInfo        *pSurInfo,
                          const tSurDe3Id *pDe3Id)
    {
    tSurDe3Info   *pDe3Info;
    word           wDe3K;
    bool           blNewStat;
    tSurDe3Anomaly anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    bool           blUasChg;
    tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    /* Get DS3/E3 information */
    mSurDe3Info(pSurInfo, pDe3Id, pDe3Info);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pDe3Info->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pDe3Info->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pDe3Info->parm.curEngTime.minute)
        {
        pDe3Info->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pDe3Info->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pDe3Info->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurDe3Anomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurDe3, pDe3Id, &anomaly);
        pDe3Info->parm.cvL.curSec     += anomaly.bpv + anomaly.exz;
        pDe3Info->parm.cvpP.curSec    += anomaly.pBitPar;
        pDe3Info->parm.cvcpP.curSec   += anomaly.cpBitPar;
        pDe3Info->parm.cvcpPfe.curSec += anomaly.febe;
        }
        
    /* Update parameters based on defects' presence */
    /* Update ES-L, SES-L, & LOSS-P parameter if a LOS defect is present */
    if (pDe3Info->status.los.status == true)
        {
        pDe3Info->parm.esL.curSec   = 1;
        pDe3Info->parm.sesL.curSec  = 1;
        pDe3Info->parm.lossL.curSec = 1;
        }
        
    /* Update ESP-P, SESP-P , SAS-P(for M23 & C-Bit app.), ESCP-P, SESCP-P (for 
       C-Bit app.) parameter when either an AIS OR a SEF defect is present */
    if ((pDe3Info->status.ais.status == true) ||
        (pDe3Info->status.sef.status == true))
        {
        pDe3Info->parm.espP.curSec   = 1;
        pDe3Info->parm.sespP.curSec  = 1;
        pDe3Info->parm.escpP.curSec  = 1;
        pDe3Info->parm.sescpP.curSec = 1;
        pDe3Info->parm.sasP.curSec   = 1;
        }
        
    /* Update AISS-P parameter when an AIS defect is present */
    if (pDe3Info->status.ais.status == true)
        {
        pDe3Info->parm.aissP.curSec = 1;
        }
        
    /* Update ESCP-PFE, SESCP-PFE, and SASCP-PFE (for C-Bit app.) when a far-end
       SEF/AIS defect is present */
    if (pDe3Info->status.sefAisFe.status == true)
        {
        pDe3Info->parm.escpPfe.curSec  = 1;
        pDe3Info->parm.sescpPfe.curSec = 1;
        pDe3Info->parm.sascpPfe.curSec = 1;
        }
    
    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update ES-L, ESA-L, and ESB-L parameter based on CV-L */
        if (pDe3Info->parm.cvL.curSec != 0)
            {
            pDe3Info->parm.esL.curSec = 1;    
            
            /* Update ESA-L & ESB-L parameter only if LOS defect is NOT present */
            if (pDe3Info->parm.lossL.curSec == 0)
                 {
                 /* Update ESA-L parameter if the count CV-L = 1 */
                 if (pDe3Info->parm.cvL.curSec == 1)
                     {
                     pDe3Info->parm.esaL.curSec = 1;
                     }
                     
                 /* Update ESB-L parameter if 1 < CV-L < K-threshold */
                 else if (pDe3Info->parm.cvL.curSec < mDe3LineK(pSurInfo))
                     {
                     pDe3Info->parm.esbL.curSec = 1;
                     }
                 }
            }
                
        /* Update ESP-P, ESAP-P, and ESBP-P parameter based on CVP-P */
        if (pDe3Info->parm.cvpP.curSec != 0)
            {
            pDe3Info->parm.espP.curSec = 1;
            
            /* Update ESAP-P, ESBP-P parameter only if BOTH AIS and SEF defect 
               are NOT present */
            if (pDe3Info->parm.sasP.curSec == 0)
                 {
                 /* Update ESAP-P parameter if the count CVP-P = 1 */
                 if (pDe3Info->parm.cvpP.curSec == 1)
                     {
                     pDe3Info->parm.esapP.curSec = 1;
                     }
                     
                 /* Update ESBP-P parameter if 1 < CVP-P < K-threshold */
                 else if (pDe3Info->parm.cvpP.curSec < mDe3PathPbitK(pSurInfo))
                     {
                     pDe3Info->parm.esbpP.curSec = 1;
                     }
                }
            }
            
        /* Update ESCP-P, ESACP-P, and ESBCP-P parameter based on CVCP-P */
        if (pDe3Info->parm.cvcpP.curSec != 0)
            {
            pDe3Info->parm.escpP.curSec = 1;
            
            /* Update ESACP-P, and ESBCP-P parameter only if BOTH AIS and SEF defect 
               are NOT present */
            if (pDe3Info->parm.sasP.curSec == 0)
                {
                 /* Update ESACP-P parameter if the count CVCP-P = 1 */
                 if (pDe3Info->parm.cvcpP.curSec == 1)
                     {
                     pDe3Info->parm.esacpP.curSec = 1;
                     }
                     
                 /* Update ESBCP-CP if 1 < CVCP-P < K-threshold */
                 else if (pDe3Info->parm.cvcpP.curSec < mDe3PathCPbitK(pSurInfo))
                     {
                     pDe3Info->parm.esbcpP.curSec = 1;
                     }
                }
            }
                
        /* Update ESCP-PFE, ESACP-PFE, and ESBCP-PFE parameter  based on CVCP-PFE */
        if (pDe3Info->parm.cvcpPfe.curSec != 0)
            {
            pDe3Info->parm.escpPfe.curSec = 1;
            
            /* Update SESACP-PFE & SESBCP-PFE only if far-end SEF/AIS is not present */
            if (pDe3Info->parm.sascpPfe.curSec == 0)
                {
                /* Update SESACP-PFE if the count CVCP-PFE = 1 */
                if (pDe3Info->parm.cvcpPfe.curSec == 1)
                    {
                    pDe3Info->parm.esacpPfe.curSec = 1;
                    }
                    
                /* Update SESBCP-PFE if 1 < CVCP-PFE < K-threshold */
                else if (pDe3Info->parm.cvcpPfe.curSec < mDe3PathCPbitK(pSurInfo))
                    {
                    pDe3Info->parm.esbcpPfe.curSec = 1;
                    }
                }
            }
                
        /* Update SES-L based on CV-L and Line K-threshold */
        wDe3K = mDe3LineK(pSurInfo);
        if (pDe3Info->parm.cvL.curSec >= wDe3K)
            {
            pDe3Info->parm.sesL.curSec = 1;
            }

        /* Update SESP-P parameter based on CVP-P and P-Bit Path K-threshold */ 
        wDe3K = mDe3PathPbitK(pSurInfo);
        if (pDe3Info->parm.cvpP.curSec >= wDe3K)
            {
            pDe3Info->parm.sespP.curSec = 1;
            }
        
        /* Update SESCP-P parameter based on CVCP-P and CP-Bit Path K-theshold */
        wDe3K = mDe3PathCPbitK(pSurInfo);
        if (pDe3Info->parm.cvcpP.curSec >= wDe3K)
            {
            pDe3Info->parm.sescpP.curSec = 1;
            }
        
        /* Update SESCP-PFE based on CVCP-PFE and CP-Bit Path K-threshold */
        if (pDe3Info->parm.cvcpPfe.curSec >= wDe3K)
            {
            pDe3Info->parm.sescpPfe.curSec = 1;
            }
              
        /* Update UASP-P, and UASCP-P parameter if the current second is counted
           as a Near-End UAS */
        if (pDe3Info->status.neUatp == true)
            {
            pDe3Info->parm.uaspP.curSec = 1;
            }
        if (pDe3Info->status.neUatcp == true)
            {
            pDe3Info->parm.uascpP.curSec = 1;
            }

        /* Update UASCP-PFE parameter if the current second is counted as a
           Far-End UAS */
        if (pDe3Info->status.feUatcp == true)
            {
            pDe3Info->parm.uascpPfe.curSec = 1;
            }

        /* Check if Near-End UAS just enter or exit and adjust registers */
        blUasChg  = false;
        blNewStat = (pDe3Info->parm.sespP.curSec == 1) ? true : false;
        if (blNewStat != pDe3Info->status.neUatpPot)
            {
            pDe3Info->status.nePPotCnt = 1;
            }
        else
            {
            pDe3Info->status.nePPotCnt++;
            }
        if (pDe3Info->status.nePPotCnt == 10)
            {
            /* Update unavailable time status */
            pDe3Info->status.neUatp = blNewStat;
            blUasChg = true;
            }
        
        /* Check if Near-End UAS (in CP-Bit app.) just enter or exit and adjust registers */
        blNewStat = (pDe3Info->parm.sescpP.curSec == 1) ? true : false;
        if (blNewStat != pDe3Info->status.neUatcpPot)
            {
            pDe3Info->status.neCpPotCnt = 1;
            }
        else
            {
            pDe3Info->status.neCpPotCnt++;
            }
        if (pDe3Info->status.neCpPotCnt == 10)
            {
            /* Update unavailable time status */
            pDe3Info->status.neUatcp   = blNewStat;
            blUasChg = true;
            }
        
        /* UAS has changed status */
        if (blUasChg == true)
            {
            /* Upon entering UAS, update current negative adjustment registers */
            if ((pDe3Info->status.neUatcp == true) ||
                (pDe3Info->status.neUatp  == true))
                {
                mCurPerNegAdj(&(pDe3Info->parm.espP));
                mCurPerNegAdj(&(pDe3Info->parm.esapP));
                mCurPerNegAdj(&(pDe3Info->parm.esbpP));
                mCurPerNegAdj(&(pDe3Info->parm.escpP));
                mCurPerNegAdj(&(pDe3Info->parm.esacpP));
                mCurPerNegAdj(&(pDe3Info->parm.esbcpP));
                mCurPerNegAdj(&(pDe3Info->parm.sespP));
                mCurPerNegAdj(&(pDe3Info->parm.sescpP));
                mCurPerNegAdj(&(pDe3Info->parm.sasP));
                mCurPerNegAdj(&(pDe3Info->parm.aissP));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pDe3Info->parm.espP));
                    mPrePerNegAdj(&(pDe3Info->parm.esapP));
                    mPrePerNegAdj(&(pDe3Info->parm.esbpP));
                    mPrePerNegAdj(&(pDe3Info->parm.escpP));
                    mPrePerNegAdj(&(pDe3Info->parm.esacpP));
                    mPrePerNegAdj(&(pDe3Info->parm.esbcpP));
                    mPrePerNegAdj(&(pDe3Info->parm.sespP));
                    mPrePerNegAdj(&(pDe3Info->parm.sescpP));
                    mPrePerNegAdj(&(pDe3Info->parm.sasP));
                    mPrePerNegAdj(&(pDe3Info->parm.aissP));
                    }

                /* Update UAS */
               	pDe3Info->parm.uaspP.curPer.value += 10;
               	pDe3Info->parm.uascpP.curPer.value += 10;
                }
            
            /* Upon exiting UAS, update current positive adjustment registers */ 
            else if ((pDe3Info->status.neUatcp == false) ||
            		(pDe3Info->status.neUatp  == false))
                {
                mCurPerPosAdj(&(pDe3Info->parm.cvpP));
                mCurPerPosAdj(&(pDe3Info->parm.cvcpP));
                mCurPerPosAdj(&(pDe3Info->parm.espP));
                mCurPerPosAdj(&(pDe3Info->parm.esapP));
                mCurPerPosAdj(&(pDe3Info->parm.esbpP));
                mCurPerPosAdj(&(pDe3Info->parm.escpP));
                mCurPerPosAdj(&(pDe3Info->parm.esacpP));
                mCurPerPosAdj(&(pDe3Info->parm.esbcpP));
                mCurPerPosAdj(&(pDe3Info->parm.sespP));
                mCurPerPosAdj(&(pDe3Info->parm.sescpP));
                mCurPerPosAdj(&(pDe3Info->parm.sasP));
                mCurPerPosAdj(&(pDe3Info->parm.aissP));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pDe3Info->parm.cvpP));
                    mPrePerPosAdj(&(pDe3Info->parm.cvcpP));
                    mPrePerPosAdj(&(pDe3Info->parm.espP));
                    mPrePerPosAdj(&(pDe3Info->parm.esapP));
                    mPrePerPosAdj(&(pDe3Info->parm.esbpP));
                    mPrePerPosAdj(&(pDe3Info->parm.escpP));
                    mPrePerPosAdj(&(pDe3Info->parm.esacpP));
                    mPrePerPosAdj(&(pDe3Info->parm.esbcpP));
                    mPrePerPosAdj(&(pDe3Info->parm.sespP));
                    mPrePerPosAdj(&(pDe3Info->parm.sescpP));
                    mPrePerPosAdj(&(pDe3Info->parm.sasP));
                    mPrePerPosAdj(&(pDe3Info->parm.aissP));
                    }

                /* Update UAS */
            	if (pDe3Info->parm.uaspP.curPer.value >= 10)
            		{
            		pDe3Info->parm.uaspP.curPer.value -= 10;
            		}
            	else
            		{
            		pDe3Info->parm.uaspP.curPer.value = 0;
            		}
            	pDe3Info->parm.uaspP.curSec = 0;

            	if (pDe3Info->parm.uascpP.curPer.value >= 10)
            		{
            		pDe3Info->parm.uascpP.curPer.value -= 10;
            		}
            	else
            		{
            		pDe3Info->parm.uascpP.curPer.value = 0;
            		}
            	pDe3Info->parm.uascpP.curSec = 0;
                }
            }
        
        /* Check if Far-End UAS just enter or exit and adjust registers */
        blNewStat = (pDe3Info->parm.sescpPfe.curSec == 1) ? true : false;
        if (blNewStat != pDe3Info->status.feUatcpPot)
            {
            pDe3Info->status.feCpPotCnt = 1;
            }
        else
            {
            pDe3Info->status.feCpPotCnt++;
            }
        if (pDe3Info->status.feCpPotCnt == 10)
            {
            /* Update unavailable time status */
            pDe3Info->status.feUatcp = blNewStat;
            
            /* Upon entering UAS, update current negative adjustment registers */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pDe3Info->parm.escpPfe));
                mCurPerNegAdj(&(pDe3Info->parm.esacpPfe));
                mCurPerNegAdj(&(pDe3Info->parm.esbcpPfe));
                mCurPerNegAdj(&(pDe3Info->parm.sescpPfe));
                mCurPerNegAdj(&(pDe3Info->parm.sascpPfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pDe3Info->parm.escpPfe));
                    mPrePerNegAdj(&(pDe3Info->parm.esacpPfe));
                    mPrePerNegAdj(&(pDe3Info->parm.esbcpPfe));
                    mPrePerNegAdj(&(pDe3Info->parm.sescpPfe));
                    mPrePerNegAdj(&(pDe3Info->parm.sascpPfe));
                    }

                /* Update UAS */
               	pDe3Info->parm.uascpPfe.curPer.value += 10;
                }

            /* Upon exiting UAS, update current positive adjustment registers */
            else
                {
                mCurPerPosAdj(&(pDe3Info->parm.cvcpPfe));
                mCurPerPosAdj(&(pDe3Info->parm.escpPfe));
                mCurPerPosAdj(&(pDe3Info->parm.esacpPfe));
                mCurPerPosAdj(&(pDe3Info->parm.esbcpPfe));
                mCurPerPosAdj(&(pDe3Info->parm.sescpPfe));
                mCurPerPosAdj(&(pDe3Info->parm.sascpPfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pDe3Info->parm.cvcpPfe));
                    mPrePerPosAdj(&(pDe3Info->parm.escpPfe));
                    mPrePerPosAdj(&(pDe3Info->parm.esacpPfe));
                    mPrePerPosAdj(&(pDe3Info->parm.esbcpPfe));
                    mPrePerPosAdj(&(pDe3Info->parm.sescpPfe));
                    mPrePerPosAdj(&(pDe3Info->parm.sascpPfe));
                    }

                /* Update UAS */
            	if (pDe3Info->parm.uascpPfe.curPer.value >= 10)
            		{
            		pDe3Info->parm.uascpPfe.curPer.value -= 10;
            		}
            	else
            		{
            		pDe3Info->parm.uascpPfe.curPer.value = 0;
            		}
            	pDe3Info->parm.uascpPfe.curSec = 0;
                }
            }
        
        /* Reset all parameters' accumulate inhibition to false */
        pDe3Info->parm.cvL.inhibit      = false;
        pDe3Info->parm.cvpP.inhibit     = false;
        pDe3Info->parm.cvcpP.inhibit    = false;
        pDe3Info->parm.espP.inhibit     = false;
        pDe3Info->parm.esapP.inhibit    = false;
        pDe3Info->parm.esbpP.inhibit    = false;
        pDe3Info->parm.escpP.inhibit    = false;
        pDe3Info->parm.esacpP.inhibit   = false;
        pDe3Info->parm.esbcpP.inhibit   = false;
        pDe3Info->parm.sespP.inhibit    = false;
        pDe3Info->parm.sescpP.inhibit   = false;
        pDe3Info->parm.sasP.inhibit     = false;
        pDe3Info->parm.aissP.inhibit    = false;
        pDe3Info->parm.cvcpPfe.inhibit  = false;
        pDe3Info->parm.escpPfe.inhibit  = false;
        pDe3Info->parm.esacpPfe.inhibit = false;
        pDe3Info->parm.esbcpPfe.inhibit = false;
        pDe3Info->parm.sescpPfe.inhibit = false;
        pDe3Info->parm.sascpPfe.inhibit = false;
        
        /* Inhibit CV-L if the current second is in SES-L */
        if (pDe3Info->parm.sesL.curSec != 0)
            {
            pDe3Info->parm.cvL.inhibit = true;
            }
        
        /* Inhibit all Near-End parameters (except UASP-P, UASCP-P, and FC-P) if
           the current second is UAS */
        if ((pDe3Info->status.neUatcp == true) ||
            (pDe3Info->status.neUatp  == true))
            {
            pDe3Info->parm.cvpP.inhibit   = true;
            pDe3Info->parm.cvcpP.inhibit  = true;
            pDe3Info->parm.espP.inhibit   = true;
            pDe3Info->parm.esapP.inhibit  = true;
            pDe3Info->parm.esbpP.inhibit  = true;
            pDe3Info->parm.escpP.inhibit  = true;
            pDe3Info->parm.esacpP.inhibit = true;
            pDe3Info->parm.esbcpP.inhibit = true;
            pDe3Info->parm.sespP.inhibit  = true;
            pDe3Info->parm.sescpP.inhibit = true;
            pDe3Info->parm.sasP.inhibit   = true;
            pDe3Info->parm.aissP.inhibit  = true;
            }
            
        /* Inhibit all Far-End parameters (except UASCP-PFE and FC-PFE) if the
           current second is UAS */
        if (pDe3Info->status.feUatcp == true)
            {
            pDe3Info->parm.cvcpPfe.inhibit  = true;
            pDe3Info->parm.escpPfe.inhibit  = true;
            pDe3Info->parm.esacpPfe.inhibit = true;
            pDe3Info->parm.esbcpPfe.inhibit = true;
            pDe3Info->parm.sescpPfe.inhibit = true;
            pDe3Info->parm.sascpPfe.inhibit = true;
            }

        /* Inhibit CVP-P, or CVCP-P if the current second is in SESP-P or SESCP-P */
        if (pDe3Info->parm.sespP.curSec == 1)
            {
            pDe3Info->parm.cvpP.inhibit = true;
            }
        if (pDe3Info->parm.sescpP.curSec == 1)
            {
            pDe3Info->parm.cvcpP.inhibit = true;
            }
            
        /* Inhibit CVCP-PFE if the current second is in SESCP-PFE */
        if (pDe3Info->parm.sescpPfe.curSec == 1)
            {
            pDe3Info->parm.cvcpPfe.inhibit = true;
            }

        /* Increase Near-End parameters' adjustment registers */
        /* In available time */
        if ((pDe3Info->status.neUatp  == false) &&
            (pDe3Info->status.neUatcp == false))
            {
            /* Potential for entering unavailable time */
            if ((pDe3Info->parm.sespP.curSec  == 1) ||
                (pDe3Info->parm.sescpP.curSec == 1))
                {
                mNegInc(&(pDe3Info->parm.espP));
                mNegInc(&(pDe3Info->parm.esapP));
                mNegInc(&(pDe3Info->parm.esbpP));
                mNegInc(&(pDe3Info->parm.escpP));
                mNegInc(&(pDe3Info->parm.esacpP));
                mNegInc(&(pDe3Info->parm.esbcpP));
                mNegInc(&(pDe3Info->parm.sespP));
                mNegInc(&(pDe3Info->parm.sescpP));
                mNegInc(&(pDe3Info->parm.sasP));
                mNegInc(&(pDe3Info->parm.aissP));
                }
            }
            
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (((pDe3Info->parm.sespP.curSec  == 0) && (pDe3Info->status.neUatp == true)) ||
                ((pDe3Info->parm.sescpP.curSec == 0) && (pDe3Info->status.neUatcp == true)))
                {
                mPosInc(&(pDe3Info->parm.cvpP));
                mPosInc(&(pDe3Info->parm.cvcpP));
                mPosInc(&(pDe3Info->parm.espP));
                mPosInc(&(pDe3Info->parm.esapP));
                mPosInc(&(pDe3Info->parm.esbpP));
                mPosInc(&(pDe3Info->parm.escpP));
                mPosInc(&(pDe3Info->parm.esacpP));
                mPosInc(&(pDe3Info->parm.esbcpP));
                mPosInc(&(pDe3Info->parm.sespP));
                mPosInc(&(pDe3Info->parm.sescpP));
                mPosInc(&(pDe3Info->parm.sasP));
                mPosInc(&(pDe3Info->parm.aissP));
                }
            }
        
        
        /* Increase Far-End parameters' adjustment registers */
        /* In available time */
        if (pDe3Info->status.feUatcp == false)
            {
            /* Potential for entering unavailable time */
            if (pDe3Info->parm.sescpPfe.curSec == 1)
                {
                mNegInc(&(pDe3Info->parm.escpPfe));
                mNegInc(&(pDe3Info->parm.esacpPfe));
                mNegInc(&(pDe3Info->parm.esbcpPfe));
                mNegInc(&(pDe3Info->parm.sescpPfe));
                mNegInc(&(pDe3Info->parm.sascpPfe));
                }
            }
            
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pDe3Info->parm.sescpPfe.curSec == 0)
                {
                mPosInc(&(pDe3Info->parm.cvcpPfe));
                mPosInc(&(pDe3Info->parm.escpPfe));
                mPosInc(&(pDe3Info->parm.esacpPfe));
                mPosInc(&(pDe3Info->parm.esbcpPfe));
                mPosInc(&(pDe3Info->parm.sescpPfe));
                mPosInc(&(pDe3Info->parm.sascpPfe));
                }
            }
            
        /* Clear Near-End parameters' adjustment registers */
        if ((pDe3Info->status.neUatp  == false) &&
            (pDe3Info->status.neUatcp == false))
            {
            if (((pDe3Info->parm.sespP.curSec  == 0)   &&
                 (pDe3Info->parm.sescpP.curSec == 0))  &&
                ((pDe3Info->status.neUatpPot  == true) ||
                 (pDe3Info->status.neUatcpPot == true)))
                {
                mNegClr(&(pDe3Info->parm.espP));
                mNegClr(&(pDe3Info->parm.esapP));
                mNegClr(&(pDe3Info->parm.esbpP));
                mNegClr(&(pDe3Info->parm.escpP));
                mNegClr(&(pDe3Info->parm.esacpP));
                mNegClr(&(pDe3Info->parm.esbcpP));
                mNegClr(&(pDe3Info->parm.sespP));
                mNegClr(&(pDe3Info->parm.sescpP));
                mNegClr(&(pDe3Info->parm.sasP));
                mNegClr(&(pDe3Info->parm.aissP));
                }
            }
        else if (((pDe3Info->parm.sespP.curSec  == 1)     || 
                  (pDe3Info->parm.sescpP.curSec == 1))    &&
                 ((pDe3Info->status.neUatpPot   == false) &&
                  (pDe3Info->status.neUatcpPot  == false)))
            {
            mPosClr(&(pDe3Info->parm.cvpP));
            mPosClr(&(pDe3Info->parm.cvcpP));
            mPosClr(&(pDe3Info->parm.espP));
            mPosClr(&(pDe3Info->parm.esapP));
            mPosClr(&(pDe3Info->parm.esbpP));
            mPosClr(&(pDe3Info->parm.escpP));
            mPosClr(&(pDe3Info->parm.esacpP));
            mPosClr(&(pDe3Info->parm.esbcpP));
            mPosClr(&(pDe3Info->parm.sespP));
            mPosClr(&(pDe3Info->parm.sescpP));
            mPosClr(&(pDe3Info->parm.sasP));
            mPosClr(&(pDe3Info->parm.aissP));
            }

        /* Clear Far-End parameters' adjustment registers */
        if (pDe3Info->status.feUatcp == false)
            {
            if ((pDe3Info->parm.sescpPfe.curSec == 0) &&
                (pDe3Info->status.feUatcpPot    == true))
                {
                mNegClr(&(pDe3Info->parm.escpPfe));
                mNegClr(&(pDe3Info->parm.esacpPfe));
                mNegClr(&(pDe3Info->parm.esbcpPfe));
                mNegClr(&(pDe3Info->parm.sescpPfe));
                mNegClr(&(pDe3Info->parm.sascpPfe));
                }
            }
            else if ((pDe3Info->parm.sescpPfe.curSec == 1) &&
                     (pDe3Info->status.feUatcpPot    == false))
            {
            mPosClr(&(pDe3Info->parm.cvcpPfe));
            mPosClr(&(pDe3Info->parm.escpPfe));
            mPosClr(&(pDe3Info->parm.esacpPfe));
            mPosClr(&(pDe3Info->parm.esbcpPfe));
            mPosClr(&(pDe3Info->parm.sescpPfe));
            mPosClr(&(pDe3Info->parm.sascpPfe));
            }

        /* Store current second statuses */
        pDe3Info->status.neUatpPot  = (pDe3Info->parm.sespP.curSec    == 1) ? true : false;
        pDe3Info->status.neUatcpPot = (pDe3Info->parm.sescpP.curSec   == 1) ? true : false;
        pDe3Info->status.feUatcpPot = (pDe3Info->parm.sescpPfe.curSec == 1) ? true : false;
        
        /* Accumulate parameters' current period registers */
        mCurPerAcc(&(pDe3Info->parm.cvL));
        mCurPerAcc(&(pDe3Info->parm.esL));
        mCurPerAcc(&(pDe3Info->parm.esaL));
        mCurPerAcc(&(pDe3Info->parm.esbL));
        mCurPerAcc(&(pDe3Info->parm.sesL));
        mCurPerAcc(&(pDe3Info->parm.lossL));
        mCurPerAcc(&(pDe3Info->parm.cvpP));
        mCurPerAcc(&(pDe3Info->parm.cvcpP));
        mCurPerAcc(&(pDe3Info->parm.espP));
        mCurPerAcc(&(pDe3Info->parm.esapP));
        mCurPerAcc(&(pDe3Info->parm.esbpP));
        mCurPerAcc(&(pDe3Info->parm.escpP));
        mCurPerAcc(&(pDe3Info->parm.esacpP));
        mCurPerAcc(&(pDe3Info->parm.esbcpP));
        mCurPerAcc(&(pDe3Info->parm.sespP));
        mCurPerAcc(&(pDe3Info->parm.sescpP));
        mCurPerAcc(&(pDe3Info->parm.sasP));
        mCurPerAcc(&(pDe3Info->parm.aissP));
        mCurPerAcc(&(pDe3Info->parm.uaspP));
        mCurPerAcc(&(pDe3Info->parm.uascpP));
        mCurPerAcc(&(pDe3Info->parm.fcP));
        mCurPerAcc(&(pDe3Info->parm.cvcpPfe));
        mCurPerAcc(&(pDe3Info->parm.escpPfe));
        mCurPerAcc(&(pDe3Info->parm.esacpPfe));
        mCurPerAcc(&(pDe3Info->parm.esbcpPfe));
        mCurPerAcc(&(pDe3Info->parm.sescpPfe));
        mCurPerAcc(&(pDe3Info->parm.sascpPfe));
        mCurPerAcc(&(pDe3Info->parm.uascpPfe));
        mCurPerAcc(&(pDe3Info->parm.fcPfe));

        /* Check and report TCA */
        blNotf = ((pDe3Info->msgInh.de3        == false) &&
                  (pDe3Info->msgInh.tca        == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Report TCAs of Line parameters */
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3CvL,      &(pDe3Info->parm.cvL),      blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsL,      &(pDe3Info->parm.esL),      blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsaL,     &(pDe3Info->parm.esaL),     blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsbL,     &(pDe3Info->parm.esbL),     blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3SesL,     &(pDe3Info->parm.sesL),     blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3LossL,    &(pDe3Info->parm.lossL),    blNotf, &tcaInfo);

        /* Report TCAs of Near-End Path parameters */
        if ((pDe3Info->status.neUatp     == false) &&
            (pDe3Info->status.neUatcp    == false) &&
            (pDe3Info->status.neUatpPot  == false) &&
            (pDe3Info->status.neUatcpPot == false))
            {
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3CvpP,     &(pDe3Info->parm.cvpP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3CvcpP,    &(pDe3Info->parm.cvcpP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EspP,     &(pDe3Info->parm.espP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsapP,    &(pDe3Info->parm.esapP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsbpP,    &(pDe3Info->parm.esbpP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EscpP,    &(pDe3Info->parm.escpP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsacpP,   &(pDe3Info->parm.esacpP),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsbcpP,   &(pDe3Info->parm.esbcpP),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3SespP,    &(pDe3Info->parm.sespP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3SescpP,   &(pDe3Info->parm.sescpP),   blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3SasP,     &(pDe3Info->parm.sasP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3AissP,    &(pDe3Info->parm.aissP),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3FcP,      &(pDe3Info->parm.fcP),      blNotf, &tcaInfo);
            }
        
        /* Report TCAs of Far-End Path parameters */
        if ((pDe3Info->status.feUatcp    == false) &&
            (pDe3Info->status.feUatcpPot == false))
            {
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3CvcpPfe,  &(pDe3Info->parm.cvcpPfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EscpPfe,  &(pDe3Info->parm.escpPfe),  blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsacpPfe, &(pDe3Info->parm.esacpPfe), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3EsbcpPfe, &(pDe3Info->parm.esbcpPfe), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3SescpPfe, &(pDe3Info->parm.sescpPfe), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3SascpPfe, &(pDe3Info->parm.sascpPfe), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3FcPfe   , &(pDe3Info->parm.fcPfe),    blNotf, &tcaInfo);
            }
        
        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3UaspP,    &(pDe3Info->parm.uaspP),    blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3UascpP,   &(pDe3Info->parm.uascpP),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurDe3, pDe3Id, pDe3Info, cPmDe3UascpPfe, &(pDe3Info->parm.uascpPfe), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pDe3Info->parm.cvL));
        mStackDown(&(pDe3Info->parm.esL));
        mStackDown(&(pDe3Info->parm.esaL));
        mStackDown(&(pDe3Info->parm.esbL));
        mStackDown(&(pDe3Info->parm.sesL));
        mStackDown(&(pDe3Info->parm.lossL));
        mStackDown(&(pDe3Info->parm.cvpP));
        mStackDown(&(pDe3Info->parm.cvcpP));
        mStackDown(&(pDe3Info->parm.espP));
        mStackDown(&(pDe3Info->parm.esapP));
        mStackDown(&(pDe3Info->parm.esbpP));
        mStackDown(&(pDe3Info->parm.escpP));
        mStackDown(&(pDe3Info->parm.esacpP));
        mStackDown(&(pDe3Info->parm.esbcpP));
        mStackDown(&(pDe3Info->parm.sespP));
        mStackDown(&(pDe3Info->parm.sescpP));
        mStackDown(&(pDe3Info->parm.sasP));
        mStackDown(&(pDe3Info->parm.aissP));
        mStackDown(&(pDe3Info->parm.uaspP));
        mStackDown(&(pDe3Info->parm.uascpP));
        mStackDown(&(pDe3Info->parm.fcP));
        mStackDown(&(pDe3Info->parm.cvcpPfe));
        mStackDown(&(pDe3Info->parm.escpPfe));
        mStackDown(&(pDe3Info->parm.esacpPfe));
        mStackDown(&(pDe3Info->parm.esbcpPfe));
        mStackDown(&(pDe3Info->parm.sescpPfe));
        mStackDown(&(pDe3Info->parm.sascpPfe));
        mStackDown(&(pDe3Info->parm.uascpPfe));
        mStackDown(&(pDe3Info->parm.fcPfe));

        /* Update time for period */
        pDe3Info->parm.prePerStartTime = pDe3Info->parm.curPerStartTime;
        pDe3Info->parm.prePerEndTime = dCurTime;
        pDe3Info->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pDe3Info->parm.cvL));
        mDayShift(&(pDe3Info->parm.esL));
        mDayShift(&(pDe3Info->parm.esaL));
        mDayShift(&(pDe3Info->parm.esbL));
        mDayShift(&(pDe3Info->parm.sesL));
        mDayShift(&(pDe3Info->parm.lossL));
        mDayShift(&(pDe3Info->parm.cvpP));
        mDayShift(&(pDe3Info->parm.cvcpP));
        mDayShift(&(pDe3Info->parm.espP));
        mDayShift(&(pDe3Info->parm.esapP));
        mDayShift(&(pDe3Info->parm.esbpP));
        mDayShift(&(pDe3Info->parm.escpP));
        mDayShift(&(pDe3Info->parm.esacpP));
        mDayShift(&(pDe3Info->parm.esbcpP));
        mDayShift(&(pDe3Info->parm.sespP));
        mDayShift(&(pDe3Info->parm.sescpP));
        mDayShift(&(pDe3Info->parm.sasP));
        mDayShift(&(pDe3Info->parm.aissP));
        mDayShift(&(pDe3Info->parm.uaspP));
        mDayShift(&(pDe3Info->parm.uascpP));
        mDayShift(&(pDe3Info->parm.fcP));
        mDayShift(&(pDe3Info->parm.cvcpPfe));
        mDayShift(&(pDe3Info->parm.escpPfe));
        mDayShift(&(pDe3Info->parm.esacpPfe));
        mDayShift(&(pDe3Info->parm.esbcpPfe));
        mDayShift(&(pDe3Info->parm.sescpPfe));
        mDayShift(&(pDe3Info->parm.sascpPfe));
        mDayShift(&(pDe3Info->parm.uascpPfe));
        mDayShift(&(pDe3Info->parm.fcPfe));

        /* Update time for day */
        pDe3Info->parm.preDayStartTime = pDe3Info->parm.curDayStartTime;
        pDe3Info->parm.preDayEndTime = dCurTime;
        pDe3Info->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurDe3StatUpd(tSurDev          device, 
                                         const tSurDe3Id *pDe3Id)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input DS3/E3.

Inputs       : device                - device database
               pDe3Id                - DS3/E3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
void SurDe3StatUpd(tSurDev device, const tSurDe3Id *pDe3Id)
    {
    tSurInfo *pSurInfo;
    
    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) &&
        (pSurInfo->interface.DefStatGet    != null))
        {
        De3AllDefUpd(pSurInfo, pDe3Id);
        }
    
    /* Declare/terminate failures */
    De3AllFailUpd(pSurInfo, pDe3Id);
    
    /* Update performance registers */
    De3PmParmUpd(pSurInfo, pDe3Id);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - TU
 *
 * File        : surtu.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of TU-m
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTu2Vt(const tSurTuId *pTuId, 
                                       tSurVtId       *pVtId)
Purpose      : This function is used to convert an TU-m to VT

Inputs       : pTuId                 - TU-m ID
               
Outputs      : pVtId                 - VT ID

Return       : cSurOk
------------------------------------------------------------------------------*/
friend eSurRet SurTu2Vt(const tSurTuId *pTuId, 
                        tSurVtId       *pVtId)
    {
    SurAu2Sts(&(pTuId->au), &(pVtId->sts), null);
    pVtId->vtg = pTuId->tug2;
    pVtId->vt  = pTuId->tu;
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTuProv(tSurDev device, tSurTuId *pTuId)

Purpose      : This function is used to provision a TU-m for FM & PM.

Inputs       : device                - Device
               pTuId                 - TU-m identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuProv(tSurDev device, tSurTuId *pTuId)
    {
    tSurVtId   vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pTuId, &vtId);
    
    /* Provision */
    return SurVtProv(device, &vtId);
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurTuDeProv(tSurDev device, tSurTuId *pTuId)

Purpose      : This function is used to de-provision FM & PM of one TU-m. It
               will deallocated all resources.

Inputs       : device                - device
               pTuId                 - TU-m identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuDeProv(tSurDev device, tSurTuId *pTuId)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pTuId, &vtId);
    
    /* De-provision */
    return SurVtDeProv(device, &vtId);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuTrblDesgnSet(tSurDev              device, 
                                         eSurTuTrbl           trblType, 
                                         const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set TU-m path trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               tSurTrblDesgn         - trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuTrblDesgnSet(tSurDev              device, 
                                 eSurTuTrbl           trblType, 
                                 const tSurTrblDesgn *pTrblDesgn)
    {
    return SurVtTrblDesgnSet(device, (eSurVtTrbl)trblType, pTrblDesgn);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuTrblDesgnGet(tSurDev              device, 
                                         eSurTuTrbl           trblType, 
                                         tSurTrblDesgn       *pTrblDesgn)

Purpose      : This function is used to get TU-m path trouble designation for 
               a device.

Inputs       : device                - device
               trblType              - trouble type
               tSurTrblDesgn         - returned trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuTrblDesgnGet(tSurDev              device, 
                                 eSurTuTrbl           trblType, 
                                 tSurTrblDesgn       *pTrblDesgn)
    {
    return SurVtTrblDesgnGet(device, (eSurVtTrbl)trblType, pTrblDesgn);
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuEngStatSet(tSurDev             device, 
                                       const tSurTuId     *pChnId, 
                                       eSurEngStat         stat)

Purpose      : This API is used to set engine state for a TU-m channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurTuEngStatSet(tSurDev             device, 
                               const tSurTuId     *pChnId, 
                               eSurEngStat         stat)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Set engine state */
    return SurVtEngStatSet(device, &vtId, stat);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuEngStatGet(tSurDev             device, 
                                       const tSurTuId     *pChnId, 
                                       eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a TU-m channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurTuEngStatGet(tSurDev             device, 
                               const tSurTuId     *pChnId, 
                               eSurEngStat        *pStat)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get engine state */
    return SurVtEngStatGet(device, &vtId, pStat);
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuChnCfgSet(tSurDev     device,
                                       const tSurTuId *pChnId, 
                                       const tSurVtConf *pChnCfg)

Purpose      : This internal API is use to set TU-n channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuChnCfgSet(tSurDev           device,
                              const tSurTuId   *pChnId, 
                              const tSurVtConf *pChnCfg)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Set configuration */
    return SurVtChnCfgSet(device, &vtId, pChnCfg);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuChnCfgGet(tSurDev          device,
                                       const tSurTuId *pChnId, 
                                       tSurVtConf     *pChnCfg)

Purpose      : This internal API is use to get TU-n channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuChnCfgGet(tSurDev           device,
                              const tSurTuId   *pChnId, 
                              tSurVtConf       *pChnCfg)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get channel configuration */
    return SurVtChnCfgGet(device, &vtId, pChnCfg);
    } 
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuTrblNotfInhSet(tSurDev               device,
                                           const tSurTuId       *pChnId, 
                                           eSurVtTrbl            trblType, 
                                           bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a TU-m channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuTrblNotfInhSet(tSurDev               device,
                                   const tSurTuId       *pChnId, 
                                   eSurVtTrbl            trblType, 
                                   bool                  inhibit)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Set trouble notification inhibition mode */
    return SurVtTrblNotfInhSet(device, &vtId, trblType, inhibit);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuTrblNotfInhGet(tSurDev               device,
                                           const tSurTuId       *pChnId, 
                                           eSurVtTrbl            trblType, 
                                           bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a TU-m channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuTrblNotfInhGet(tSurDev               device,
                                   const tSurTuId       *pChnId, 
                                   eSurVtTrbl            trblType, 
                                   bool                 *pInhibit)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get trouble notification inhibition mode */
    return SurVtTrblNotfInhGet(device, &vtId, trblType, pInhibit);
    }    

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTuChnTrblNotfInhInfoGet(tSurDev             device, 
                                                  const tSurTuId     *pChnId, 
                                                  tSurVtMsgInh       *pTrblInh)

Purpose      : This API is used to get TU-m channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurTuChnTrblNotfInhInfoGet(tSurDev             device, 
                                          const tSurTuId     *pChnId, 
                                          tSurVtMsgInh       *pTrblInh)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get trouble notification info */
    return SurVtChnTrblNotfInhInfoGet(device, &vtId, pTrblInh);
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmTuFailIndGet(tSurDev           device, 
                                      const tSurTuId   *pChnId, 
                                      eSurTuTrbl        trblType, 
                                      tSurFailStat     *pFailStat)

Purpose      : This API is used to get failure indication of a TU-m channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmTuFailIndGet(tSurDev           device, 
                              const tSurTuId   *pChnId, 
                              eSurTuTrbl        trblType, 
                              tSurFailStat     *pFailStat)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get failure indication */
    return FmVtFailIndGet(device, &vtId, trblType, pFailStat);
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmTuFailHisGet(tSurDev         device, 
                                               const tSurTuId *pChnId)

Purpose      : This API is used to get a TU-m channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmTuFailHisGet(tSurDev         device, 
                                const tSurTuId *pChnId)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get failure history */
    return FmVtFailHisGet(device, &vtId);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmTuFailHisClear(tSurDev         device, 
                                        const tSurTuId *pChnId,
                                        bool            flush)

Purpose      : This API is used to clear a Tu-m channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmTuFailHisClear(tSurDev device, const tSurTuId *pChnId, bool flush)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Clear failure history */
    return FmVtFailHisClear(device, &vtId, flush);
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuRegReset(tSurDev       device, 
                                    tSurTuId     *pChnId, 
                                    ePmTuParm     parm, 
                                    ePmRegType    regType)

Purpose      : This API is used to reset a TU-m channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTuRegReset(tSurDev       device, 
                            tSurTuId     *pChnId, 
                            ePmTuParm     parm, 
                            ePmRegType    regType)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Reset register */
    return PmVtRegReset(device, &vtId, parm, regType);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuRegGet(tSurDev           device, 
                                  const tSurTuId   *pChnId, 
                                  ePmTuParm         parm, 
                                  ePmRegType        regType, 
                                  tPmReg           *pRetReg)

Purpose      : This API is used to get a TU-m channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmTuRegGet(tSurDev           device, 
                          const tSurTuId   *pChnId, 
                          ePmTuParm         parm, 
                          ePmRegType        regType, 
                          tPmReg           *pRetReg)
    
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get register's value */
    return PmVtRegGet(device, &vtId, parm, regType, pRetReg);
    }    
        
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmTuTcaHisGet(tSurDev device, const tSurTuId *pChnId,)

Purpose      : This API is used to get a TU-m channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmTuTcaHisGet(tSurDev device, const tSurTuId *pChnId)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get TCA history */
    return PmVtTcaHisGet(device, &vtId);
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuTcaHisClear(tSurDev device, const tSurTuId *pChnId)

Purpose      : This API is used to clear a TU-m channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTuTcaHisClear(tSurDev device, const tSurTuId *pChnId)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Clear TCA history */
    return PmVtTcaHisClear(device, &vtId);
    }    
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmTuParmAccInhSet(tSurDev          device, 
                                          const tSurTuId  *pChnId, 
                                          ePmTuParm        parm, 
                                          bool             inhibit)

Purpose      : This API is used to enable/disable a TU-m channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTuParmAccInhSet(tSurDev          device, 
                                 const tSurTuId  *pChnId, 
                                 ePmTuParm        parm, 
                                 bool             inhibit)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Set parameter accummulate inhibition mode */
    return PmVtParmAccInhSet(device, &vtId, parm, inhibit);
    }


/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuParmAccInhGet(tSurDev          device, 
                                         const tSurTuId  *pChnId, 
                                         ePmTuParm        parm, 
                                         bool            *pInhibit)

Purpose      : This API is used to get a TU-m channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTuParmAccInhGet(tSurDev          device, 
                                 const tSurTuId  *pChnId, 
                                 ePmTuParm        parm, 
                                 bool            *pInhibit)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get parameter accummulate inhibition mode */
    return PmVtParmAccInhGet(device, &vtId, parm, pInhibit);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuRegThresSet(tSurDev          device, 
                                       const tSurTuId  *pChnId, 
                                       ePmTuParm        parm, 
                                       ePmRegType       regType, 
                                       dword            thresVal)

Purpose      : This API is used to set threshold for a TU-m channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTuRegThresSet(tSurDev          device, 
                               const tSurTuId  *pChnId, 
                               ePmTuParm        parm, 
                               ePmRegType       regType, 
                               dword            thresVal)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Set register's threshold */
    return PmVtRegThresSet(device, &vtId, parm, regType, thresVal);
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuRegThresGet(tSurDev          device, 
                                       const tSurTuId  *pChnId, 
                                       ePmTuParm        parm, 
                                       ePmRegType       regType, 
                                       dword           *pThresVal)

Purpose      : This API is used to get threshold for a TU-m channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmTuRegThresGet(tSurDev          device, 
                               const tSurTuId  *pChnId, 
                               ePmTuParm        parm, 
                               ePmRegType       regType, 
                               dword           *pThresVal)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get register's threshold */
    return PmVtRegThresGet(device, &vtId, parm, regType, pThresVal);
    }
  
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmTuParmNumRecRegSet(tSurDev         device, 
                                            const tSurTuId *pChnId, 
                                            ePmTuParm       parm, 
                                            byte            numRecReg)
                                            
Purpose      : This API is used to set a TU-m channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTuParmNumRecRegSet(tSurDev         device, 
                                    const tSurTuId *pChnId, 
                                    ePmTuParm       parm, 
                                    byte            numRecReg)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Set number of recent registers */
    return PmVtParmNumRecRegSet(device, &vtId, parm, numRecReg);
    }
    
/*------------------------------------------------------------------------------
Prototype    : PmTuParmNumRecRegGet(tSurDev         device, 
                                    const tSurTuId *pChnId, 
                                    ePmTuParm       parm, 
                                    byte           *pNumRecReg)
                                             
Purpose      : This API is used to get a TU-m channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmTuParmNumRecRegGet(tSurDev         device, 
                                    const tSurTuId *pChnId, 
                                    ePmTuParm       parm, 
                                    byte           *pNumRecReg)
    {
    tSurVtId vtId;
    
    /* Convert to STS ID */
    SurTu2Vt(pChnId, &vtId);
    
    /* Get number of recent registers */
    return PmVtParmNumRecRegGet(device, &vtId, parm, pNumRecReg);
    }
    
      

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengapsl.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code Linear APS Transmisstion 
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : void SurApslDefUpd(tSurApslInfo  *pApslInfo, 
                                  tSurLineInfo  *pProtLineInfo, 
                                  tSurApslDef   *pApslDef)

Purpose      : This function is used to update Linear APS engine's defect database.

Inputs       : pApslInfo             - Liear APS engine's information.
               pProtLineInfo         - Protection line's information.
               pApslDef              - Linear APS defect information.

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurApslDefUpd(tSurApslInfo  *pApslInfo, 
                          tSurLineInfo  *pProtLineInfo, 
                          tSurApslDef   *pApslDef)
    {
    byte        bWorkChn;
    
    switch (pApslDef->defect)
        {
        /* Protection Switching Byte defect */
        case cFmApslKbyteFail:
            /* Only update this status when protection line has not detected 
               AIS-L */
            if ((pProtLineInfo == null) || (pProtLineInfo->status.aisl.status == false))
                {
                pApslInfo->status.kbyteFail.status = pApslDef->status;
                pApslInfo->status.kbyteFail.time   = pApslDef->time;
                }
            break;

        /* APS Channel Mismatch defect */
        case cFmApslChnMis:
            /* Only update this status when protection line has not detected 
               AIS-L */
            if ((pProtLineInfo == null) || (pProtLineInfo->status.aisl.status == false))
                {
                pApslInfo->status.chnMis.status = pApslDef->status;
                pApslInfo->status.chnMis.time   = pApslDef->time;
                }
            break;

        /* APS Mode Mismatch defect */
        case cFmApslMdMis:
            /* Only update this status when protection line has not detected 
               AIS-L */
            if ((pProtLineInfo == null) || (pProtLineInfo->status.aisl.status == false))
                {
                pApslInfo->status.mdMis.status = pApslDef->status;
                pApslInfo->status.mdMis.time   = pApslDef->time;
                }
            break;

        /* Far-End Protection Line defect */
        case cFmApslFeFail:
            pApslInfo->status.feFail.status = pApslDef->status;
            pApslInfo->status.feFail.time   = pApslDef->time;
            break;

        /* Linear APS APS switching failure */
        case cFmLineApsFail:
            pApslInfo->status.protFail.status = pApslDef->status;
            pApslInfo->status.protFail.time   = pApslDef->time;
            break;
        
        /* Linear APS APS switching event */
        case cEvLineApsSw0:
        case cEvLineApsSw1:
        case cEvLineApsSw2:
        case cEvLineApsSw3:
        case cEvLineApsSw4:
        case cEvLineApsSw5:
        case cEvLineApsSw6:
        case cEvLineApsSw7:
        case cEvLineApsSw8:
        case cEvLineApsSw9:
        case cEvLineApsSw10:
        case cEvLineApsSw11:
        case cEvLineApsSw12:
        case cEvLineApsSw13:
        case cEvLineApsSw14:
            bWorkChn = (byte)(pApslDef->defect - cEvLineApsSw0);
            pApslInfo->status.prevWorkChn = pApslInfo->status.curWorkChn;
            pApslInfo->status.curWorkChn = bWorkChn;
            break;
            
        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : void SurApslDefUpd(tSurApslInfo  *pApslInfo, 
                                  tSurLineInfo  *pProtLineInfo, 
                                  tSurApslDef   *pApslDef)

Purpose      : This function is used to update all defect statuses in a Linear
               APS engine.

Inputs       : pSurInf o             - Device database.
               pApslId               - Linear APS engine's ID.

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ApslAllDefUpd(tSurInfo         *pSurInfo,
                           const tSurApslId *pApslId)
    {
    bool          blNewStat;
    tSurApslDef   def;
    tSurApslInfo *pApslInfo;
    tSurLineInfo *pLineInfo;        /* Protection line's information */
    dword         dDefStat;
    byte          bWorkChn;
    
    /* Get Linear APS engine's information */
    mSurApslInfo(pSurInfo, pApslId, pApslInfo);
    
    /* Get protection line's information */
    mSurLineInfo(pSurInfo, &(pApslInfo->conf.protLine), pLineInfo);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pApslId, &(def.id), sizeof(tSurApslId));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurApslEng, pApslId, &dDefStat);
    
    /* Protection Switching Byte defect */
    blNewStat = mBitStat(dDefStat, cSurApslKByteFailMask);
    if (blNewStat != pApslInfo->status.kbyteFail.status)
        {
        def.defect = cFmApslKbyteFail;
        def.status = blNewStat;
        SurApslDefUpd(pApslInfo, pLineInfo, &def);
        }

    /* APS Channel Mismatch defect */
    blNewStat = mBitStat(dDefStat, cSurApslChnMisMask);
    if (blNewStat != pApslInfo->status.chnMis.status)
        {
        def.defect = cFmApslChnMis;
        def.status = blNewStat;
        SurApslDefUpd(pApslInfo, pLineInfo, &def);
        }

    /* APS Mode Mismatch defect */
    blNewStat = mBitStat(dDefStat, cSurApslMdMisMask);
    if (blNewStat != pApslInfo->status.mdMis.status)
        {
        def.defect = cFmApslMdMis;
        def.status = blNewStat;
        SurApslDefUpd(pApslInfo, pLineInfo, &def);
        }
    
    /* Far-End Protection Line defect */
    blNewStat = mBitStat(dDefStat, cSurApslFeFailMask);
    if (blNewStat != pApslInfo->status.feFail.status)
        {
        def.defect = cFmApslFeFail;
        def.status = blNewStat;
        SurApslDefUpd(pApslInfo, pLineInfo, &def);
        }

    /* Linear APS switching failure */
    blNewStat = mBitStat(dDefStat, cSurApslFailMask);
    if (blNewStat != pApslInfo->status.protFail.status)
        {
        def.defect = cFmLineApsFail;
        def.status = blNewStat;
        SurApslDefUpd(pApslInfo, pLineInfo, &def);
        }  
    
    /* Linear APS switching event */
    bWorkChn = (byte)((dDefStat & cSurApslSwChnMask) >> cSurApslSwChnShift);
    pApslInfo->status.prevWorkChn = pApslInfo->status.curWorkChn;
    pApslInfo->status.curWorkChn = bWorkChn;
    }

/*------------------------------------------------------------------------------
Prototype    : private void ApslAllFailUpd(tSurInfo         *pSurInfo,
                                           const tSurApslId *pApslId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database.
               pApslId               - Linear APS engine's ID.

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ApslAllFailUpd(tSurInfo         *pSurInfo,
                            const tSurApslId *pApslId)
    {
    tSurTrblProf   *pTrblProf;
    tSurApslFail   *pFailInd;
    tSortList       failHis;
    tSurFailStat    failStat;
    tSurApslInfo   *pApslInfo;
    tFmFailInfo     failInfo;
    bool            blFailChg;
    bool            blNotf;
    byte            bSwEvent;

    /* Get Linear APS engine's information */
    mSurApslInfo(pSurInfo, pApslId, pApslInfo);

    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pApslInfo->failure);
    failHis   = pApslInfo->failHis;
    
    
    /* Only declare the following failure unless the engine is operating in
       1+1 uni-directional mode */
    if ((pApslInfo->conf.arch != cSurLinear11) &&
        (pApslInfo->conf.dir != cSurUniDir))
        {
        /* APS K-Byte switching failure */
        mFailIndUpd(pSurInfo, pApslInfo, kbyteFail, kbyteFail, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pApslInfo->msgInh.apsl      == false) &&
                      (pApslInfo->msgInh.kbyteFail == false));
            mFailRept(pSurInfo, cSurApslEng, pApslId, pFailInd->kbyteFail, failHis, failInfo, blNotf, cFmApslKbyteFail, pTrblProf->apsKbyteFail);
            }
            
            /* APS Channel Mismatch failure */
        mFailIndUpd(pSurInfo, pApslInfo, chnMis, chnMis, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pApslInfo->msgInh.apsl   == false) &&
                      (pApslInfo->msgInh.chnMis == false));
            mFailRept(pSurInfo, cSurApslEng, pApslId, pFailInd->chnMis, failHis, failInfo, blNotf, cFmApslChnMis, pTrblProf->apsChnMis);
            }
            
        /* APS Mode Mismatch failure */
        mFailIndUpd(pSurInfo, pApslInfo, mdMis, mdMis, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pApslInfo->msgInh.apsl   == false) &&
                      (pApslInfo->msgInh.mdMis  == false));
            mFailRept(pSurInfo, cSurApslEng, pApslId, pFailInd->mdMis, failHis, failInfo, blNotf, cFmApslMdMis, pTrblProf->apsMdMis);
            }
            
        /* APS Far-End failure */
        mFailIndUpd(pSurInfo, pApslInfo, feFail, feFail, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pApslInfo->msgInh.apsl   == false) &&
                      (pApslInfo->msgInh.feFail == false));
            mFailRept(pSurInfo, cSurApslEng, pApslId, pFailInd->feFail, failHis, failInfo, blNotf, cFmApslFeFail, pTrblProf->apsFeFail);
            }
        }
        
    /* APS Switching Failure */
    if (pApslInfo->status.protFail.status != pFailInd->protFail.status)
        {
        pFailInd->protFail.status = pApslInfo->status.protFail.status;
        pFailInd->protFail.time   = dCurTime;
        
        /* Notify */
        blNotf = ((pApslInfo->msgInh.apsl     == false) &&
                  (pApslInfo->msgInh.protFail == false) &&
                  (pFailInd->protFail.status  == true));
        mFailRept(pSurInfo, cSurApslEng, pApslId, pFailInd->protFail, failHis, failInfo, blNotf, cFmLineApsFail, pTrblProf->apsFail);
        }
    
    /* APS Switching event */
    if (pApslInfo->status.curWorkChn != pApslInfo->status.prevWorkChn)
        {
        bSwEvent = pApslInfo->status.curWorkChn + cEvLineApsSw0;
        
        /* Notify */
        blNotf = (pApslInfo->msgInh.swEvent == false);
        failStat.time   = dCurTime;
        failStat.status = false;
        mFailRept(pSurInfo, cSurApslEng, pApslId, failStat, failHis, failInfo, blNotf, bSwEvent, pTrblProf->apsSw);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ApslPmParmUpd(tSurInfo         *pSurInfo,
                                          const tSurApslId *pApslId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database.
               pApslId               - Linear APS engine's ID.

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ApslPmParmUpd(tSurInfo         *pSurInfo,
                           const tSurApslId *pApslId)
    {
    tSurApslInfo   *pApslInfo;
    tSurLineInfo   *pProtLineInfo;
    tSurLineInfo   *pCurLineInfo;
    tSurLineInfo   *pPrevLineInfo;
    tPmTcaInfo      tcaInfo;
    bool            blNotf;
    
    pCurLineInfo = NULL;
    pPrevLineInfo = NULL;
    /* Get Linear APS engine's information */
    mSurApslInfo(pSurInfo, pApslId, pApslInfo);
    
    /* Get protection line's information */
    mSurLineInfo(pSurInfo, &(pApslInfo->conf.protLine), pProtLineInfo);
    
    /* Get current working line's information */
    if (pApslInfo->status.curWorkChn  != 0)
        {
        mSurLineInfo(pSurInfo, 
                     &(pApslInfo->conf.workLine[pApslInfo->status.curWorkChn - 1]), 
                     pCurLineInfo);
        }
        
    /* Get previous working line's information */
    if (pApslInfo->status.prevWorkChn  != 0)
        {
        mSurLineInfo(pSurInfo, 
                     &(pApslInfo->conf.workLine[pApslInfo->status.prevWorkChn - 1]), 
                     pPrevLineInfo);
        }
                     
    /* Update PSD parameter if the protection line is in used */
    if (pApslInfo->status.curWorkChn != 0)
        {
        pProtLineInfo->parm.psd.curSec = 1;
        pCurLineInfo->parm.psd.curSec  = 1;
        
        /* Update PSC parameter */
        if (pApslInfo->status.curWorkChn != pApslInfo->status.prevWorkChn)
            {
            /* Update PSC for protection, current, and previous line */
            if (pApslInfo->status.prevWorkChn != 0)
                {
                pProtLineInfo->parm.psc.curSec  += 1;
                pPrevLineInfo->parm.psc.curSec  += 1;
                
                if (pApslInfo->status.curWorkChn  != 0)
                    {
                    pCurLineInfo->parm.psc.curSec += 1;
                    }
                }
            /* Update PSC for protection, and current line */    
            else
                {
                pProtLineInfo->parm.psc.curSec  += 1;
                pCurLineInfo->parm.psc.curSec   += 1;
                }
            }
        }
    
    /* Second expired */
    if (blSecExpr == true)
        {
        /* Initilalize TCA information for reporting */
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Accumulate current period register for protection line */
        mCurPerAcc(&(pProtLineInfo->parm.psc));
        mCurPerAcc(&(pProtLineInfo->parm.psd));
        
        /* Check and report TCA for protection line */
        blNotf = ((pProtLineInfo->msgInh.tca   == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        SurTcaReport(pSurInfo, 
                     cSurLine, 
                     &(pApslInfo->conf.protLine), 
                     pProtLineInfo, 
                     cPmLinePsc, 
                     &(pProtLineInfo->parm.psc), 
                     blNotf, 
                     &tcaInfo);
        SurTcaReport(pSurInfo, 
                     cSurLine, 
                     &(pApslInfo->conf.protLine), 
                     pProtLineInfo, 
                     cPmLinePsd, 
                     &(pProtLineInfo->parm.psd), 
                     blNotf, 
                     &tcaInfo);
        
        /* Accumulate the current period register for current line */
        if (pApslInfo->status.curWorkChn  != 0)
            {
            mCurPerAcc(&(pCurLineInfo->parm.psc));
            mCurPerAcc(&(pCurLineInfo->parm.psd));
            
            /* Check and report TCA for  current line */
            blNotf = ((pCurLineInfo->msgInh.tca   == false) &&
                      (pSurInfo->interface.TcaNotf != null)) ? true : false;
            SurTcaReport(pSurInfo, 
                         cSurLine, 
                         &(pApslInfo->conf.workLine[pApslInfo->status.curWorkChn - 1]), 
                         pCurLineInfo, 
                         cPmLinePsc, 
                         &(pCurLineInfo->parm.psc), 
                         blNotf, 
                         &tcaInfo);
            SurTcaReport(pSurInfo, 
                         cSurLine, 
                         &(pApslInfo->conf.workLine[pApslInfo->status.curWorkChn - 1]), 
                         pCurLineInfo, 
                         cPmLinePsd, 
                         &(pCurLineInfo->parm.psd), 
                         blNotf, 
                         &tcaInfo);
            }
            
        /* Accumulate the current period register for previous line */
        if (pApslInfo->status.prevWorkChn  != 0)
            {
            mCurPerAcc(&(pPrevLineInfo->parm.psc));
            mCurPerAcc(&(pPrevLineInfo->parm.psd));
            
            /* Check and report TCA for previous line */
            blNotf = ((pPrevLineInfo->msgInh.tca   == false) &&
                      (pSurInfo->interface.TcaNotf != null)) ? true : false;
            SurTcaReport(pSurInfo, 
                         cSurLine, 
                         &(pApslInfo->conf.workLine[pApslInfo->status.prevWorkChn -1]), 
                         pPrevLineInfo, 
                         cPmLinePsc, 
                         &(pPrevLineInfo->parm.psc), 
                         blNotf, 
                         &tcaInfo);
            SurTcaReport(pSurInfo, 
                         cSurLine, 
                         &(pApslInfo->conf.workLine[pApslInfo->status.prevWorkChn -1]), 
                         pPrevLineInfo, 
                         cPmLinePsd, 
                         &(pPrevLineInfo->parm.psd), 
                         blNotf, 
                         &tcaInfo);
            }
        }

    /* Period expired */
    if (blPerExpr == true)
        {
        mStackDown(&(pProtLineInfo->parm.psc));
        mStackDown(&(pProtLineInfo->parm.psd));
        
        /* Update time for period */
        pProtLineInfo->parm.prePerStartTime = pProtLineInfo->parm.curPerStartTime;
        pProtLineInfo->parm.prePerEndTime = dCurTime;
        pProtLineInfo->parm.curPerStartTime = dCurTime;

        if (pApslInfo->status.curWorkChn  != 0)
            {
            mStackDown(&(pCurLineInfo->parm.psc));
            mStackDown(&(pCurLineInfo->parm.psd));

			/* Update time for period */
			pCurLineInfo->parm.prePerStartTime = pCurLineInfo->parm.curPerStartTime;
			pCurLineInfo->parm.prePerEndTime = dCurTime;
			pCurLineInfo->parm.curPerStartTime = dCurTime;
            }
            
        if (pApslInfo->status.prevWorkChn  != 0)
            {
            mStackDown(&(pPrevLineInfo->parm.psc));
            mStackDown(&(pPrevLineInfo->parm.psd));

			/* Update time for period */
			pPrevLineInfo->parm.prePerStartTime = pPrevLineInfo->parm.curPerStartTime;
			pPrevLineInfo->parm.prePerEndTime = dCurTime;
			pPrevLineInfo->parm.curPerStartTime = dCurTime;
            }

        }

    /* Day expired */
    if (blDayExpr == true)
        {
        mDayShift(&(pProtLineInfo->parm.psc));
        mDayShift(&(pProtLineInfo->parm.psd));
        
        /* Update time for day */
        pProtLineInfo->parm.preDayStartTime = pProtLineInfo->parm.curDayStartTime;
        pProtLineInfo->parm.preDayEndTime = dCurTime;
        pProtLineInfo->parm.curDayStartTime = dCurTime;

        if (pApslInfo->status.curWorkChn  != 0)
            {
            mDayShift(&(pCurLineInfo->parm.psc));
            mDayShift(&(pCurLineInfo->parm.psd));

			/* Update time for day */
			pCurLineInfo->parm.preDayStartTime = pCurLineInfo->parm.curDayStartTime;
			pCurLineInfo->parm.preDayEndTime = dCurTime;
			pCurLineInfo->parm.curDayStartTime = dCurTime;
            }
            
        if (pApslInfo->status.prevWorkChn  != 0)
            {
            mDayShift(&(pPrevLineInfo->parm.psc));
            mDayShift(&(pPrevLineInfo->parm.psd));

			/* Update time for day */
			pPrevLineInfo->parm.preDayStartTime = pPrevLineInfo->parm.curDayStartTime;
			pPrevLineInfo->parm.preDayEndTime = dCurTime;
			pPrevLineInfo->parm.curDayStartTime = dCurTime;
            }

        }
    }   
                                     
/*------------------------------------------------------------------------------
Prototype    : friend void SurApslStatUpd(tSurDev           device, 
                                          const tSurApslId *pApslId)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input Linear APS engine.

Inputs       : device                - device
               pApslId               - Linear APS engine ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurApslStatUpd(tSurDev device, const tSurApslId *pApslId)
    {
    tSurInfo       *pSurInfo;

    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) &&
        (pSurInfo->interface.DefStatGet    != null))
        {
        ApslAllDefUpd(pSurInfo, pApslId);
        }
    
    /* Declare/terminate failures */
    ApslAllFailUpd(pSurInfo, pApslId);
    
    /* Update performance registers */
    ApslPmParmUpd(pSurInfo, pApslId);
    }

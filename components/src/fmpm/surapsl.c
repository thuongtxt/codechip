/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - Linear APS engine
 *
 * File        : surapsl.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of Linear APS engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surdb.h"
#include "surutil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : private tSurApslInfo *ApslInfoCreate(tSurInfo   *pSurInfo, 
                                                    tSurApslId *pApslId)

Purpose      : This function is used to allocate resources for Linear APS engine

Inputs       : pSurInfo              - database handle of device
               pApslId               - Linear APS engine id

Outputs      : None

Return       : tSurApslInfo*          - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurApslInfo *ApslInfoCreate(tSurInfo *pSurInfo, tSurApslId *pApslId)
    {
    tSurApslInfo *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurApslInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurApslInfo), 0);

        /* Create failure history and TCA history */
        if (ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc)
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History\n");
            }

        /* Add to database */
        mChnDb(pSurInfo).pApsl[mSurApslIdFlat(pApslId)] = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void ApslInfoFinal(tSurApslInfo *pInfo)

Purpose      : This function is used to deallocate resources of Linear APS engine

Inputs       : pInfo                 - information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurApslInfoFinal(tSurApslInfo *pInfo)
    {
    /* Destroy failure history and failure history */
    if (ListDestroy(&(pInfo->failHis)) != cListSucc)
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History\n");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ApslInfoDestroy(tSurInfo   *pSurInfo,
                                           tSurApslId *pApslId)

Purpose      : This function is used to deallocate resources of Linear APS engine

Inputs       : pSurInfo              - database handle of device
               pApslId               - Linear APS engine id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ApslInfoDestroy(tSurInfo *pSurInfo, tSurApslId *pApslId)
    {
    tSurApslInfo *pInfo;

    /* Get channel information */
    mSurApslInfo(pSurInfo, pApslId, pInfo);
    if (pInfo != null)
        {
        SurApslInfoFinal(pInfo);

        /* Destroy other resources */
        OsalMemFree(mChnDb(pSurInfo).pApsl[mSurApslIdFlat(pApslId)]);
        mChnDb(pSurInfo).pApsl[mSurApslIdFlat(pApslId)] = null;
        }
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslProv(tSurDev device, tSurApslId *pApslId)

Purpose      : This function is used to provision a Linear APS engine for FM & PM.

Inputs       : device                - Device
               pApslId               - Linear APS engine identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurApslProv(tSurDev device, tSurApslId *pApslId)
    {
    tSurInfo     *pSurInfo;
    tSurApslInfo *pChnInfo;

    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrSem;
        }

    /* Get Linear APS engine information */
    mSurApslInfo(pSurInfo, pApslId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "Linear APS engine is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for Linear APS engine */
    pChnInfo = ApslInfoCreate(pSurInfo, pApslId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for Linear APS engine\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }

    /* Set default configuration */
    pChnInfo->conf.decSoakTime = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime = cSurDefaultTerSoakTime;


    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslDeProv(tSurDev device, tSurApslId *pApslId)

Purpose      : This function is used to de-provision FM & PM of one Linear APS engine. It
               will deallocated all resources.

Inputs       : device                - device
               pApslId                - Linear APS engine identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurApslDeProv(tSurDev device, tSurApslId *pApslId)
    {
    tSurInfo     *pSurInfo;
    
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrSem;
        }

    /* Deallocate all resources of Linear APS engine */
    ApslInfoDestroy(pSurInfo, pApslId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurApslTrblDesgnSet(tSurDev              device, 
                                                  eSurApslTrbl         trblType, 
                                                  const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set linear APS trouble designation for 
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslTrblDesgnSet(tSurDev              device, 
                                   eSurApslTrbl         trblType, 
                                   const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrNullParm;
        }
    
    /* Set trouble designation */
    switch (trblType)
        {
        /* K-Byte failure */
        case cFmApslKbyteFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsKbyteFail);
            break;
        
        /* Channel Mismatch failure */
        case cFmApslChnMis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsChnMis);
            break;
        
        /* Mode Mismatch failure */
        case cFmApslMdMis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsMdMis);
            break;
        
        /* Linear APS Far-End */
        case cFmApslFeFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsFeFail);
            break;
        
        /* Linear APS Protection failure */
        case cFmLineApsFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsFail);
            break;
        
        /* All failures */
        case cFmApslAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsKbyteFail);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsChnMis);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsMdMis);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsFeFail);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsFail);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
        
        /* Linear APS switching event */
        case cEvLineApsSw0:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsSw);
            break;
      
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurApslTrblDesgnGet(tSurDev        device, 
                                                  eSurApslTrbl   trblType, 
                                                  tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get linear APS trouble designation of 
               a device.

Inputs       : device                � device
               trblType              � trouble type
               
Outputs      : pTrblDesgn            � trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslTrblDesgnGet(tSurDev        device, 
                                   eSurApslTrbl   trblType, 
                                   tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    pSurInfo = mDevDb(device);
    /* Set trouble designation */
    switch (trblType)
        {
        /* K-Byte failure */
        case cFmApslKbyteFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsKbyteFail);
            break;
        
        /* Channel Mismatch failure */
        case cFmApslChnMis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsChnMis);
            break;
        
        /* Mode Mismatch failure */
        case cFmApslMdMis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsMdMis);
            break;
        
        /* Linear APS Far-End */
        case cFmApslFeFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsFeFail);
            break;
        
        /* Linear APS Protection failure */
        case cFmLineApsFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsFail);
            break;
        
        /* Linear APS switching event */
        case cEvLineApsSw0:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.apsSw);
            break;
      
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
    
    /* Get configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslEngStatSet(tSurDev             device, 
                                         const tSurApslId   *pChnId, 
                                         eSurEngStat         stat)

Purpose      : This API is used to set engine state for a  linear APS channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurApslEngStatSet(tSurDev             device, 
                                 const tSurApslId   *pChnId, 
                                 eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurApslInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurApslInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslEngStatGet(tSurDev             device, 
                                         const tSurApslId   *pChnId, 
                                         eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a  linear APS channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurApslEngStatGet(tSurDev             device, 
                                 const tSurApslId   *pChnId, 
                                 eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurApslInfo   *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurApslInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }                            
                             
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurApslChnCfgSet(tSurDev             device, 
                                               const tSurApslId   *pChnId, 
                                               const tSurApslConf *pChnCfg)

Purpose      : This internal API is use to set APS channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslChnCfgSet(tSurDev             device, 
                                const tSurApslId   *pChnId, 
                                const tSurApslConf *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret; 
    tSurInfo       *pSurInfo;
    tSurApslInfo   *pInfo;
    tSurLineInfo   *pLineInfo;
    byte            bIdx;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
    if (pChnCfg->numWorkLine > cSurApslMaxWorkLine)
        {
        return cSurErrInvlParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Check if the protection line is provisioned */
    mSurLineInfo(pSurInfo, &(pChnCfg->protLine), pLineInfo)
    if (mIsNull(pLineInfo))
        {
        ret = mDevGive(device);
        return cSurErrInvlParm;
        }
    
    /* Check if the working line are provisioned */
    for (bIdx = 0; bIdx < pChnCfg->numWorkLine; bIdx++)
        {
        mSurLineInfo(pSurInfo, &(pChnCfg->workLine[bIdx]), pLineInfo)
        if (mIsNull(pLineInfo))
            {
            ret = mDevGive(device);
            return cSurErrInvlParm;
            }
        }
        
    /* Get channel info */
    mSurApslInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrInvlParm;
        }
    
    /* Set configuration */
    pInfo->conf.arch        = pChnCfg->arch;
    pInfo->conf.dir         = pChnCfg->dir;
    pInfo->conf.rev         = pChnCfg->rev;
    pInfo->conf.numWorkLine = pChnCfg->numWorkLine;
    pInfo->conf.protLine    = pChnCfg->protLine;
	pInfo->conf.decSoakTime = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
	pInfo->conf.terSoakTime = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    for (bIdx = 0; bIdx < pChnCfg->numWorkLine; bIdx++)
        {
        pInfo->conf.workLine[bIdx] = pChnCfg->workLine[bIdx];
        } 
    
    /* Realease device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslChnCfgGet(tSurDev     device,
                                        const tSurApslId *pChnId, 
                                        tSurApslConf *pChnCfg)

Purpose      : This API is used to set APS channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslChnCfgGet(tSurDev           device,
                                const tSurApslId *pChnId,
                                tSurApslConf     *pChnCfg)
    {
    /* Declare local variables */
    tSurApslInfo *pInfo;
    byte          bIdx;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurApslInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get configuration */
    pChnCfg->arch        = pInfo->conf.arch;
    pChnCfg->dir         = pInfo->conf.dir;
    pChnCfg->rev         = pInfo->conf.rev;
    pChnCfg->numWorkLine = pInfo->conf.numWorkLine;
    pChnCfg->protLine    = pInfo->conf.protLine;
    pChnCfg->decSoakTime = pInfo->conf.decSoakTime;
    pChnCfg->terSoakTime = pInfo->conf.terSoakTime;
    
    for (bIdx = 0; bIdx < pInfo->conf.numWorkLine; bIdx++)
        {
        pChnCfg->workLine[bIdx] = pInfo->conf.workLine[bIdx];
        } 
    
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslTrblNotfInhSet(tSurDev               device,
                                             const tSurApslId     *pChnId, 
                                             eSurApslTrbl          trblType, 
                                             bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a linear APS channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslTrblNotfInhSet(tSurDev               device,
                                     const tSurApslId     *pChnId, 
                                     eSurApslTrbl          trblType, 
                                     bool                  inhibit)
    {
    /* Declare local variables */
    tSurInfo       *pSurInfo;
    eSurRet         ret;
    tSurApslInfo   *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurApslInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Switch trouble type */
    switch (trblType)
        {
        /* K-byte failure */
        case cFmApslKbyteFail:
            pInfo->msgInh.kbyteFail = inhibit;
            break;
        
        /* Channel Mismatch */
        case cFmApslChnMis:
            pInfo->msgInh.chnMis = inhibit;
            break;
        
        /* Mode Mismatch */
        case cFmApslMdMis:
            pInfo->msgInh.mdMis = inhibit;
            break;
        
        /* Linear APS Far-End failure */
        case cFmApslFeFail:
            pInfo->msgInh.feFail = inhibit;
            break;
        
        /* Linear APS switching failure */
        case cFmLineApsFail:
            pInfo->msgInh.protFail = inhibit;
            break;
        
        /* All */
        case cFmApslAllFail:
            pInfo->msgInh.apsl = inhibit;
            break;
        
        /* Linear APS switching event */
        case cEvLineApsSw0:
            pInfo->msgInh.swEvent = inhibit;
            break;
        
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslTrblNotfInhGet(tSurDev               device,
                                             const tSurApslId     *pChnId, 
                                             eSurApslTrbl          trblType, 
                                             bool                 *inhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a linear APS channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslTrblNotfInhGet(tSurDev               device,
                                     const tSurApslId     *pChnId, 
                                     eSurApslTrbl          trblType, 
                                     bool                 *pInhibit)
    {
    /* Declare local varibles */
    tSurApslInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurApslInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Switch trouble type */
    switch (trblType)
        {
        /* K-byte failure */
        case cFmApslKbyteFail:
            *pInhibit = pInfo->msgInh.kbyteFail;
            break;
        
        /* Channel Mismatch */
        case cFmApslChnMis:
            *pInhibit = pInfo->msgInh.chnMis;
            break;
        
        /* Mode Mismatch */
        case cFmApslMdMis:
            *pInhibit = pInfo->msgInh.mdMis;
            break;
        
        /* Linear APS Far-End failure */
        case cFmApslFeFail:
            *pInhibit = pInfo->msgInh.feFail;
            break;
        
        /* Linear APS switching failure */
        case cFmLineApsFail:
            *pInhibit = pInfo->msgInh.protFail;
            break;
        
        /* All */
        case cFmApslAllFail:
            *pInhibit = pInfo->msgInh.apsl;
            break;
        
        /* Linear APS switching event */
        case cEvLineApsSw0:
            *pInhibit = pInfo->msgInh.swEvent;
            break;
        
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurApslChnTrblNotfInhInfoGet(tSurDev             device, 
                                                    const tSurApslId   *pChnId, 
                                                    tSurApslMsgInh     *pTrblInh)

Purpose      : This API is used to get  linear APS channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurApslChnTrblNotfInhInfoGet(tSurDev             device, 
                                            const tSurApslId   *pChnId, 
                                            tSurApslMsgInh     *pTrblInh)
    {
    tSurApslInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurApslInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurApslMsgInh));
    
    return cSurOk;
    } 
        
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet FmApslFailIndGet(tSurDev             device, 
                                               const tSurApslId   *pChnId, 
                                               eSurApslTrbl        trblType, 
                                               tSurFailStat       *pFailStat)

Purpose      : This API is used to get failure indication of a linear APS 
               channel's specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmApslFailIndGet(tSurDev             device, 
                                const tSurApslId   *pChnId, 
                                eSurApslTrbl        trblType, 
                                tSurFailStat       *pFailStat)
    {
    /* Declare local variables */
    tSurApslInfo    *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
        
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
    
    /* Get info */
    mSurApslInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Switch trouble type */
    switch (trblType)
        {
        /* K-byte failure */
        case cFmApslKbyteFail:
            mSurFailStatCopy(&(pInfo->failure.kbyteFail), pFailStat);
            break;
        
        /* Channel Mismatch */
        case cFmApslChnMis:
            mSurFailStatCopy(&(pInfo->failure.chnMis), pFailStat);
            break;
        
        /* Mode Mismatch */
        case cFmApslMdMis:
            mSurFailStatCopy(&(pInfo->failure.mdMis), pFailStat);
            break;
        
        /* Linear APS Far-End failure */
        case cFmApslFeFail:
            mSurFailStatCopy(&(pInfo->failure.feFail), pFailStat);
            break;
        
        /* Linear APS switching failure */
        case cFmLineApsFail:
            mSurFailStatCopy(&(pInfo->failure.protFail), pFailStat);
            break;
        
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }                            

/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmApslFailHisGet(tSurDev           device, 
                                                 const tSurApslId *pChnId)

Purpose      : This API is used to get a linear APS channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmApslFailHisGet(tSurDev           device,
                                        const tSurApslId *pChnId)
    {
    /* Declare local variables */
    tSurApslInfo  *pInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurApslInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmApslFailHisClear(tSurDev device, const void *pChnId, bool flush)

Purpose      : This API is used to clear a linear APS channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmApslFailHisClear(tSurDev device, const tSurApslId *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurApslInfo   *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurApslInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
    


        

    


    
    
    

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - Fault Montiroring
 *
 * File        : surfm.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Failure Monitoring source code
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surfm.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*------------------------------------------------------------------------------
Prototype     : eSurRet FailHisClear(tSortList pFailHis, bool flush)

Purpose      : This internal function is used to clear failure history of a channel.

Inputs       : 

Outputs      :                     
Return       : Surveillance return code            
------------------------------------------------------------------------------*/
friend eSurRet FailHisClear(tSortList pFailHis, bool flush)
    {
    /* Declare local variables */
    eListRet        lstRet;                                                   
    dword           dIdx;                                                     
    dword           dIdy;                                                     
    dword           dSize;                                                    
    tFmFailInfo     failInfoSet;                                              
    tFmFailInfo     failInfoClr;                                              
                                                                               
    /* If flushing is enable, flush the entire history */                      
    if (flush == true)                                                       
        {                                                                      
        lstRet = ListFlush(pFailHis);                                       
        mLstRetCheck(lstRet);                                        
        }                                                                      
    else                                                                       
        {                                                                      
        /* Get failure history's list size */                                  
        lstRet = ListSize(pFailHis, &dSize);                               
        mLstRetCheck(lstRet);
        
        dIdx = 0;
        while ((dSize > 0) && (dIdx < dSize))
            {                                                                      
            /* Loop the list to find a cleared failure entry */                    
            for (dIdx = 0; dIdx < dSize; dIdx++)                               
                {                                                                  
                lstRet = ListGet(pFailHis,                                      
                                 dIdx,                                           
                                 (tFmFailInfo *)&failInfoClr,                    
                                 (dword)sizeof(tFmFailInfo));                     
                mLstRetCheck(lstRet);
                                                                                   
                /* If status of the failure is clear, process it */                
                if (failInfoClr.status == false)                                  
                    {                                                              
                    /* Search the list for a compatible raised failure entry */    
                    for (dIdy = dIdx + 1; dIdy < dSize; dIdy++)                       
                        {                                                          
                        lstRet = ListGet(pFailHis,                              
                                         dIdy,                                    
                                         (tFmFailInfo *)&failInfoSet,             
                                         (dword)sizeof(tFmFailInfo));              
                        mLstRetCheck(lstRet);
                                                                                   
                        if (failInfoClr.trblType == failInfoSet.trblType &&      
                            failInfoSet.status == true)                           
                            {                                                      
                            break;                                                 
                            }                                                      
                        }                                                          
                                                                                   
                    /* If the entry cannot be found, return engine error */        
                    if (dIdy == dSize)                                           
                        {                                                          
                        /* Return engine error */                                  
                        return cSurErr;                                           
                        }                                                          
                    else                                                           
                        {                                                          
                        /* Delete entries from the list */                         
                        lstRet = ListRmv(pFailHis,                             
                                         dIdx,                                    
                                         &failInfoClr,                            
                                         (dword)sizeof(tFmFailInfo));              
                        mLstRetCheck(lstRet);
                                                                                   
                        lstRet = ListRmv(pFailHis,                              
                                         (dIdy - 1),                                    
                                         &failInfoSet,                            
                                         (dword)sizeof(tFmFailInfo));              
                        mLstRetCheck(lstRet);
                        
                        /* Recalulate list size & then exit the for..loop */
                        dSize = dSize - 2;
                        break;
                        }
                    }
                }
            }
        }
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmFailIndGet(tSurDev         device, 
                                    eSurChnType     chnType, 
                                    const void     *pChnId, 
                                    byte            trblType, 
                                    tSurFailStat   *pFailStat)

Purpose      : This API is used to get failure indication of a channel's specific
               failure.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               trblType              - trouble type. Depend on channel type, trouble
                                     type can be one of the following:
                                        + eSurApslTrbl
                                        + eSurLineTrbl
                                        + eSurStsTrbl
                                        + eSurAuTrbl
                                        + eSurTu3Trbl
                                        + eSurVtTrbl
                                        + eSurTuTrgl
                                        + eSurDe3Trbl
                                        + eSurDe1Trbl
                                        + eSurImaLinkTrbl

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet FmFailIndGet(tSurDev         device, 
                            eSurChnType     chnType, 
                            const void     *pChnId, 
                            byte            trblType, 
                            tSurFailStat   *pFailStat)
    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Get failure indication */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            ret = FmApslFailIndGet(device, (tSurApslId *)pChnId, trblType, pFailStat);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            ret = FmLineFailIndGet(device, (tSurLineId *)pChnId, trblType, pFailStat);
            break;
            
        /* STS path */
        case cSurSts:
            ret = FmStsFailIndGet(device, (tSurStsId *)pChnId, trblType, pFailStat);
            break;
            
        /* VT path */
        case cSurVt:
            ret = FmVtFailIndGet(device, (tSurVtId *)pChnId, trblType, pFailStat);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret  = FmAuFailIndGet(device, (tSurAuId *)pChnId, trblType, pFailStat);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = FmTu3FailIndGet(device, (tSurTu3Id *)pChnId, trblType, pFailStat);
            break;
            
        /* TU path */
        case cSurTu:
            ret = FmTuFailIndGet(device, (tSurTuId *)pChnId, trblType, pFailStat);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = FmDe3FailIndGet(device, (tSurDe3Id *)pChnId, trblType, pFailStat);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = FmDe1FailIndGet(device, (tSurDe1Id *)pChnId, trblType, pFailStat);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = FmImaLinkFailIndGet(device, (tSurImaLinkId *)pChnId, trblType, pFailStat);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = FmImaGrpFailIndGet(device, (tSurImaGrpId *)pChnId, trblType, pFailStat);
            break;

        /* PW */
        case cSurPw:
            ret = FmPwFailIndGet(device, (tSurPwId *)pChnId, trblType, pFailStat);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }
    
/*------------------------------------------------------------------------------
Prototype    : public const tSortList FmFailHisGet(tSurDev      device, 
                                                   eSurChnType  chnType, 
                                                   const void  *pChnId)

Purpose      : This API is used to get a channel's failure history.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
public tSortList FmFailHisGet(tSurDev      device,
                                    eSurChnType  chnType, 
                                    const void  *pChnId)
    {
    /* Get failure history */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            return FmApslFailHisGet(device, (tSurApslId *)pChnId);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            return FmLineFailHisGet(device, (tSurLineId *)pChnId);
            break;
            
        /* STS path */
        case cSurSts:
            return FmStsFailHisGet(device, (tSurStsId *)pChnId);
            break;
            
        /* VT path */
        case cSurVt:
            return FmVtFailHisGet(device, (tSurVtId *)pChnId);
            break;
            
        /* AU-n path */
        case cSurAu:
            return FmAuFailHisGet(device, (tSurAuId *)pChnId);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            return FmTu3FailHisGet(device, (tSurTu3Id *)pChnId);
            break;
            
        /* TU path */
        case cSurTu:
            return FmTuFailHisGet(device, (tSurTuId *)pChnId);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            return FmDe3FailHisGet(device, (tSurDe3Id *)pChnId);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            return FmDe1FailHisGet(device, (tSurDe1Id *)pChnId);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            return FmImaLinkFailHisGet(device, (tSurImaLinkId *)pChnId);
            break;

        /* IMA Group */
        case cSurImaGrp:
            return FmImaGrpFailHisGet(device, (tSurImaGrpId *)pChnId);
            break;

        /* PW */
        case cSurPw:
            return FmPwFailHisGet(device, (tSurPwId *)pChnId);
            break;

        /* Invalid channel type */
        default:
            return null;
        }
    return null;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmFailHisClear(tSurDev       device, 
                                      eSurChnType   chnType, 
                                      const void   *pChnId, 
                                      bool          flush)

Purpose      : This API is used to clear a channel's failure history

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.
                                     
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet FmFailHisClear(tSurDev       device, 
                              eSurChnType   chnType, 
                              const void   *pChnId, 
                              bool          flush)

    {
    /* Declare local variables */
    eSurRet     ret;
    
    /* Clear failure history */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            ret = FmApslFailHisClear(device, (tSurApslId *)pChnId, flush);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            ret = FmLineFailHisClear(device, (tSurLineId *)pChnId, flush);
            break;
            
        /* STS path */
        case cSurSts:
            ret = FmStsFailHisClear(device, (tSurStsId *)pChnId, flush);
            break;
            
        /* VT path */
        case cSurVt:
            ret = FmVtFailHisClear(device, (tSurVtId *)pChnId, flush);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = FmAuFailHisClear(device, (tSurAuId *)pChnId, flush);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = FmTu3FailHisClear(device, (tSurTu3Id *)pChnId, flush);
            break;
            
        /* TU path */
        case cSurTu:
            ret = FmTuFailHisClear(device, (tSurTuId *)pChnId, flush);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = FmDe3FailHisClear(device, (tSurDe3Id *)pChnId, flush);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = FmDe1FailHisClear(device, (tSurDe1Id *)pChnId, flush);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = FmImaLinkFailHisClear(device, (tSurImaLinkId *)pChnId, flush);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = FmImaGrpFailHisClear(device, (tSurImaGrpId *)pChnId, flush);
            break;

        /* PW */
        case cSurPw:
            ret = FmPwFailHisClear(device, (tSurPwId *)pChnId, flush);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }   
    
                 

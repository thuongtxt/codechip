/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : atfmpm.c
 *
 * Created Date: Mar 20, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "AtChannel.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtSdhChannel.h"
#include "AtPdhChannel.h"
#include "AtPdhDe3.h"
#include "AtPdhDe1.h"
#include "AtPw.h"
#include "AtPwCounters.h"
#include "AtSurEngine.h"
#include "AtSdhAug.h"
#include "linkqueue.h"
#include "sortlist.h"
#include "fmpm/sur.h"
#include "fmpm/atfmpm.h"
#include "fmpm/surpm.h"
#include "fmpm/surdb.h"
#include "fmpm/surfm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAdaptChannelId(chnId) ((chnId) - 1)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tSurDev surDevice = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern void EngPoll(void);
extern eSurRet SurDbInit(void);
extern void SurEngDbStart(void);
extern eSurRet SurDevAdd(tSurDevHdl *pDeviceHdl, tSurDev *pDevice, const tSurDevIntf *pIntf);
extern eSurRet SurDevStatSet(tSurDev device, eSurEngStat state);
extern friend eSurRet SurLineEngStatSet(tSurDev device, const tSurLineId *pChnId, eSurEngStat stat);

extern void AtSurEngineFailureNotify(AtSurEngine self, uint32 failureType, uint32 currentStatus);

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts1InLine(eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm1)
        return 3;

    if (rate == cAtSdhLineRateStm4)
        return 12;

    if (rate == cAtSdhLineRateStm16)
        return 48;

    return 0;
    }

static void SurSts2Au(const tSurStsId *pStsId, tSurAuId *pChnId)
    {
    uint16 localId = (uint16)(pStsId->localId - 1);
    pChnId->line  = pStsId->line ;
    pChnId->aug64 = (byte)(localId / 192);
    pChnId->aug16 = (byte)((localId - (pChnId->aug64 * 192)) / 48);
    pChnId->aug4  = (byte)((localId - (pChnId->aug64 * 192) - (pChnId->aug16 * 48)) / 12);
    pChnId->aug1  = (byte)((localId - (pChnId->aug64 * 192) - (pChnId->aug16 * 48) - (pChnId->aug4 * 12)) / 3);
    pChnId->au    = (byte)((localId % 3) + 1);
    }

static void SurVt2Tu(const tSurVtId *pVtId, tSurTuId *pTuId)
    {
    SurSts2Au(&pVtId->sts, &pTuId->au);
    pTuId->tug2 = pVtId->vtg;
    pTuId->tu = pVtId->vt;
    }

void AtSurAdaptEngPoll(void)
    {
    EngPoll();
    }

static AtModuleSdh SdhModule(tSurDevHdl *pHdl)
    {
    return (AtModuleSdh)AtDeviceModuleGet((AtDevice)pHdl, cAtModuleSdh);
    }

static AtModulePw PwModule(tSurDevHdl *pHdl)
    {
    return (AtModulePw)AtDeviceModuleGet((AtDevice)pHdl, cAtModulePw);
    }

static uint32 SdhLineOffset(uint16 lineId)
    {
    AtUnused(lineId);
    return 0;
    }

static AtPdhDe3 De3FromChannelId(tSurDevHdl *pHdl, const void *pChnId)
    {
    uint8 aug1Id, tug3Id;
    uint16 de3Id = (uint16)(*((tSurDe3Id *)pChnId));
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(pHdl), 0); /* TODO: temp assume all lines have the same rate */
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint16 lineId = (uint16)(de3Id / NumSts1InLine(rate));

    if (rate == cAtSdhLineRateStm16)
        de3Id = (uint16) (de3Id - SdhLineOffset(lineId) - (uint16) (lineId * 48) - 1);

    if (rate == cAtSdhLineRateStm4)
        de3Id = (uint16)(de3Id - SdhLineOffset(lineId)- (uint16)(lineId * 12) - 1);

    if (rate == cAtSdhLineRateStm1)
        de3Id = (uint16)(de3Id - SdhLineOffset(lineId)- (uint16)(lineId * 4) - 1);

    aug1Id = (uint8) (de3Id / 3);
    tug3Id = (uint8) (de3Id % 3);

    return (AtPdhDe3)AtSdhChannelMapChannelGet((AtSdhChannel)AtSdhLineVc3Get(line,(uint8)( aug1Id - 1), (uint8)(tug3Id - 1)));
    }

static AtPdhDe1 De1FromChannelId(tSurDevHdl *pHdl, const void *pChnId)
    {
    uint8 aug1Id, tug3Id;
    AtSdhVc vc1x;
    tSurDe1Id *de1Id = (tSurDe1Id *)(pChnId);
    uint16 de3Id = de1Id->de3Id;
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(pHdl), 0);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint16 lineId = (uint16)(de3Id / NumSts1InLine(rate));

    if (rate == cAtSdhLineRateStm16)
        de3Id = (uint16)(de3Id - SdhLineOffset(lineId)- (uint16)(lineId * 48) - 1);

    if (rate == cAtSdhLineRateStm4)
        de3Id =(uint16)( de3Id - SdhLineOffset(lineId)- (uint16)(lineId * 12) - 1);

    if (rate == cAtSdhLineRateStm1)
        de3Id = (uint16)(de3Id - SdhLineOffset(lineId)- (uint16)(lineId * 4) - 1);

    aug1Id = (uint8)(de3Id / 30);
    tug3Id = (uint8)(de3Id % 3);

    vc1x = AtSdhLineVc1xGet(line, aug1Id, tug3Id, (uint8)(de1Id->de2Id - 1), (uint8)(de1Id->de1Id - 1));
    return (AtPdhDe1)AtSdhChannelMapChannelGet((AtSdhChannel)vc1x);
    }

/* If a channelId is 0, it means parent channel includes only one child channel */
static uint8 Aug1Id(const tSurAuId *auId)
    {
    uint8 aug4Id = (uint8)((auId->aug4 != 0) ? (auId->aug4) : 1);
    uint8 aug1Id = (uint8)((auId->aug1 != 0) ? (auId->aug1) : 1);
    return (uint8)(aug1Id * aug4Id - 1);
    }

static uint8 Aug4Id(uint8 aug4Id)
    {
    return (uint8)((aug4Id != 0) ? (aug4Id - 1) : 0);
    }

/* This function should have ability to return all kinds of VC */
static AtSdhVc HoVcFromChannelId(tSurDevHdl *pHdl, const void *pChnId)
    {
    tSurAuId auId;
    AtSdhLine line;
    AtSdhChannel subChannel;
    eAtSdhChannelType channelType;
    eAtSdhAugMapType mapType;

    SurSts2Au((const tSurStsId*)pChnId, &auId);
    line = AtModuleSdhLineGet(SdhModule(pHdl),(uint8)( auId.line - 1));
    subChannel = AtSdhChannelSubChannelGet((AtSdhChannel)line, 0);
    channelType = AtSdhChannelTypeGet(subChannel);
    mapType = AtSdhChannelMapTypeGet(subChannel);

    if ((channelType == cAtSdhChannelTypeAug16) && (mapType == cAtSdhAugMapTypeAug16MapVc4_16c))
        {
        /* Not support currently */
        return NULL;
        }

    if ((channelType == cAtSdhChannelTypeAug16) && (mapType == cAtSdhAugMapTypeAug16Map4xAug4s))
        {
        AtSdhChannel aug1;
        eAtSdhAugMapType aug1MapType;
        AtSdhChannel aug4 = AtSdhChannelSubChannelGet(subChannel, 0);
        eAtSdhAugMapType aug4MapType = AtSdhChannelMapTypeGet(aug4);

        if (aug4MapType == cAtSdhAugMapTypeAug4MapVc4_4c)
            return AtSdhLineVc4_4cGet(line, Aug4Id(auId.aug4));

        aug1 = AtSdhChannelSubChannelGet(aug4, 0);
        aug1MapType = AtSdhChannelMapTypeGet(aug1);

        if (aug1MapType == cAtSdhAugMapTypeAug1MapVc4)
            return AtSdhLineVc4Get(line, Aug1Id(&auId));

        return AtSdhLineVc3Get(line, Aug1Id(&auId), (uint8)(auId.au - 1));
        }

    if ((channelType == cAtSdhChannelTypeAug4) && (mapType == cAtSdhAugMapTypeAug4MapVc4_4c))
        return AtSdhLineVc4_4cGet(line, Aug4Id(auId.aug4));

    if ((channelType == cAtSdhChannelTypeAug4) && (mapType == cAtSdhAugMapTypeAug4Map4xAug1s))
        {
        AtSdhChannel aug1 = AtSdhChannelSubChannelGet(subChannel, 0);
        eAtSdhAugMapType aug1MapType = AtSdhChannelMapTypeGet(aug1);

        if (aug1MapType == cAtSdhAugMapTypeAug1MapVc4)
            return AtSdhLineVc4Get(line, Aug1Id(&auId));

        return AtSdhLineVc3Get(line, Aug1Id(&auId), (uint8)(auId.au - 1));
        }

    if (mapType == cAtSdhAugMapTypeAug1MapVc4)
        return AtSdhLineVc4Get(line, Aug1Id(&auId));

    return AtSdhLineVc3Get(line, Aug1Id(&auId), (uint8)(auId.au - 1));
    }

static AtSdhAu HoAuFromChannelId(tSurDevHdl *pHdl, const void *pChnId)
    {
    tSurAuId auId;
    AtSdhLine line;
    AtSdhChannel subChannel;
    eAtSdhChannelType channelType;
    eAtSdhAugMapType mapType;

    SurSts2Au((const tSurStsId*)pChnId, &auId);
    line = AtModuleSdhLineGet(SdhModule(pHdl),(uint8) (auId.line - 1));
    subChannel = AtSdhChannelSubChannelGet((AtSdhChannel)line, 0);
    channelType = AtSdhChannelTypeGet(subChannel);
    mapType = AtSdhChannelMapTypeGet(subChannel);

    if ((channelType == cAtSdhChannelTypeAug16) && (mapType == cAtSdhAugMapTypeAug16MapVc4_16c))
        {
        /* Not support currently */
        return NULL;
        }

    if ((channelType == cAtSdhChannelTypeAug16) && (mapType == cAtSdhAugMapTypeAug16Map4xAug4s))
        {
        AtSdhChannel aug1;
        eAtSdhAugMapType aug1MapType;
        AtSdhChannel aug4 = AtSdhChannelSubChannelGet(subChannel, 0);
        eAtSdhAugMapType aug4MapType = AtSdhChannelMapTypeGet(aug4);

        if (aug4MapType == cAtSdhAugMapTypeAug4MapVc4_4c)
            return AtSdhLineAu4_4cGet(line, Aug4Id(auId.aug4));

        aug1 = AtSdhChannelSubChannelGet(aug4, 0);
        aug1MapType = AtSdhChannelMapTypeGet(aug1);

        if (aug1MapType == cAtSdhAugMapTypeAug1MapVc4)
            return AtSdhLineAu4Get(line, Aug1Id(&auId));

        return AtSdhLineAu3Get(line, Aug1Id(&auId), (uint8)(auId.au - 1));
        }

    if ((channelType == cAtSdhChannelTypeAug4) && (mapType == cAtSdhAugMapTypeAug4MapVc4_4c))
        return AtSdhLineAu4_4cGet(line, Aug4Id(auId.aug4));

    if ((channelType == cAtSdhChannelTypeAug4) && (mapType == cAtSdhAugMapTypeAug4Map4xAug1s))
        {
        AtSdhChannel aug1 = AtSdhChannelSubChannelGet(subChannel, 0);
        eAtSdhAugMapType aug1MapType = AtSdhChannelMapTypeGet(aug1);

        if (aug1MapType == cAtSdhAugMapTypeAug1MapVc4)
            return AtSdhLineAu4Get(line, Aug1Id(&auId));

        return AtSdhLineAu3Get(line, Aug1Id(&auId),(uint8) (auId.au - 1));
        }

    if (mapType == cAtSdhAugMapTypeAug1MapVc4)
        return AtSdhLineAu4Get(line, Aug1Id(&auId));

    return AtSdhLineAu3Get(line, Aug1Id(&auId), (uint8) (auId.au - 1));
    }

static AtSdhVc Vc3Tu3FromChannelId(tSurDevHdl *pHdl, const void *pChnId)
    {
    tSurTu3Id *tu3Id = (tSurTu3Id *)pChnId;
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(pHdl), (uint8) (tu3Id->au.line - 1));
    return AtSdhLineVc3Get(line, Aug1Id(&tu3Id->au), (uint8) (tu3Id->tu3 - 1));
    }

static AtSdhVc Vc1xFromChannelId(tSurDevHdl *pHdl, const void *pChnId)
    {
    tSurTuId tuId;
    AtSdhLine line;

    SurVt2Tu((tSurVtId*)pChnId, &tuId);
    line = AtModuleSdhLineGet(SdhModule(pHdl),(uint8) ( tuId.au.line - 1));
    return AtSdhLineVc1xGet(line, Aug1Id(&tuId.au), (uint8) (tuId.au.au - 1), (uint8) (tuId.tug2 - 1), (uint8) (tuId.tu - 1));
    }

static eSurRet AnomalyGet(tSurDevHdl *pHdl, eSurChnType chnType, const void *pChnId, void *pAnomaly)
    {
    switch ((uint32)chnType)
        {
        case cSurLine:
            {
            tSurLineAnomaly *lineAnomaly = (tSurLineAnomaly *)pAnomaly;
            AtChannel line = (AtChannel)AtModuleSdhLineGet(SdhModule(pHdl),
                                                           (uint8)((*((uint16*)pChnId)) - 1));
            lineAnomaly->b1 = AtChannelCounterClear(line, cAtSdhLineCounterTypeB1);
            lineAnomaly->b2 = AtChannelCounterClear(line, cAtSdhLineCounterTypeB2);
            lineAnomaly->reil = AtChannelCounterClear(line, cAtSdhLineCounterTypeRei);
            return cSurOk;
            }
        case cSurSts:
        case cSurAu:
            {
            tSurAuAnomaly *auAnomaly = (tSurAuAnomaly*)pAnomaly;
            AtChannel vc = (AtChannel)HoVcFromChannelId(pHdl, pChnId);
            auAnomaly->b3 = AtChannelCounterClear(vc, cAtSdhPathCounterTypeBip);
            auAnomaly->pgDec = AtChannelCounterClear(vc, cAtSdhPathCounterTypeTxPPJC);
            auAnomaly->pgInc = AtChannelCounterClear(vc, cAtSdhPathCounterTypeTxNPJC);
            auAnomaly->piDec = AtChannelCounterClear(vc, cAtSdhPathCounterTypeRxNPJC);
            auAnomaly->piInc = AtChannelCounterClear(vc, cAtSdhPathCounterTypeRxPPJC);
            auAnomaly->reip = AtChannelCounterClear(vc, cAtSdhPathCounterTypeRei);
            return cSurOk;
            }
        case cSurTu3:
            {
            tSurTu3Anomaly *tu3Anomaly = (tSurTu3Anomaly*)pAnomaly;
            AtChannel vc3 = (AtChannel)Vc3Tu3FromChannelId(pHdl, pChnId);
            tu3Anomaly->b3 = AtChannelCounterClear(vc3, cAtSdhPathCounterTypeBip);
            tu3Anomaly->pgDec = AtChannelCounterClear(vc3, cAtSdhPathCounterTypeTxPPJC);
            tu3Anomaly->pgInc = AtChannelCounterClear(vc3, cAtSdhPathCounterTypeTxNPJC);
            tu3Anomaly->piDec = AtChannelCounterClear(vc3, cAtSdhPathCounterTypeRxNPJC);
            tu3Anomaly->piInc = AtChannelCounterClear(vc3, cAtSdhPathCounterTypeRxPPJC);
            tu3Anomaly->reip = AtChannelCounterClear(vc3, cAtSdhPathCounterTypeRei);
            return cSurOk;
            }
        case cSurVt:
        case cSurTu:
            {
            tSurTuAnomaly *tuAnomaly = (tSurTuAnomaly*)pAnomaly;
            AtChannel vc1x = (AtChannel)Vc1xFromChannelId(pHdl, pChnId);
            tuAnomaly->bipV = AtChannelCounterClear(vc1x, cAtSdhPathCounterTypeBip);
            tuAnomaly->pgInc = AtChannelCounterClear(vc1x, cAtSdhPathCounterTypeTxPPJC);
            tuAnomaly->pgDec = AtChannelCounterClear(vc1x, cAtSdhPathCounterTypeTxNPJC);
            tuAnomaly->piInc = AtChannelCounterClear(vc1x, cAtSdhPathCounterTypeRxNPJC);
            tuAnomaly->piDec = AtChannelCounterClear(vc1x, cAtSdhPathCounterTypeRxPPJC);
            tuAnomaly->reiV = AtChannelCounterClear(vc1x, cAtSdhPathCounterTypeRei);
            return cSurOk;
            }
        case cSurDe3:
            {
            AtChannel de3  = (AtChannel)De3FromChannelId(pHdl, pChnId);
            tSurDe3Anomaly *de3Anomaly = (tSurDe3Anomaly *)pAnomaly;

            /* TODO: old surveillance module may not good in this case */
            de3Anomaly->pBitPar = AtChannelCounterClear(de3, cAtPdhDe3CounterPBit);
            de3Anomaly->cpBitPar = AtChannelCounterClear(de3, cAtPdhDe3CounterCPBit);
            de3Anomaly->febe = AtChannelCounterClear(de3, cAtPdhDe3CounterRei);

            return cSurOk;
            }
        case cSurDe1:
            {
            AtChannel de1 = (AtChannel)De1FromChannelId(pHdl, pChnId);
            tSurDe1Anomaly *de1Anomaly = (tSurDe1Anomaly *)pAnomaly;

            /* TODO: old surveillance module may not good in this case */
            de1Anomaly->bpvExz = AtChannelCounterClear(de1, cAtPdhDe1CounterBpvExz);
            de1Anomaly->crc = AtChannelCounterClear(de1, cAtPdhDe1CounterCrc);
            de1Anomaly->fe = AtChannelCounterClear(de1, cAtPdhDe1CounterFe);

            return cSurOk;
            }
        case cSurPw:
            {
            AtPwCounters counters;
            tSurPwAnomaly *pwAnomaly = (tSurPwAnomaly *)pAnomaly;
            AtChannel pw = (AtChannel)AtModulePwGetPw(PwModule(pHdl), (uint32)((*((uint16 *)pChnId)) - 1));
            AtChannelAllCountersClear(pw, &counters);
            pwAnomaly->lops = AtPwTdmCountersRxLopsGet((AtPwTdmCounters)counters);
            pwAnomaly->malformedPkt = AtPwCountersRxMalformedPacketsGet(counters);
            pwAnomaly->misorderedPkt = AtPwCountersRxReorderedPacketsGet(counters);
            pwAnomaly->misorderedPktDropped = AtPwCountersRxReorderedPacketsGet(counters);
            pwAnomaly->missingPkt = AtPwCountersRxLostPacketsGet(counters);
            pwAnomaly->playoutJitBufOverrun = AtPwTdmCountersRxJitBufOverrunGet((AtPwTdmCounters)counters);
            pwAnomaly->playoutJitBufUnderrun = AtPwTdmCountersRxJitBufUnderrunGet((AtPwTdmCounters)counters);
            pwAnomaly->remotePktLoss = AtPwTdmCountersTxRbitPacketsGet((AtPwTdmCounters)counters);
            pwAnomaly->rxLbitPkt = AtPwTdmCountersRxLbitPacketsGet((AtPwTdmCounters)counters);
            pwAnomaly->rxPkt = AtPwCountersRxPacketsGet(counters);
            pwAnomaly->rxRbitPkt = AtPwTdmCountersRxRbitPacketsGet((AtPwTdmCounters)counters);
            pwAnomaly->strayPkt = AtPwCountersRxStrayPacketsGet(counters);
            pwAnomaly->txLbitPkt = AtPwTdmCountersTxLbitPacketsGet((AtPwTdmCounters)counters);
            pwAnomaly->txPkt = AtPwCountersTxPacketsGet(counters);

            return cSurOk;
            }
        default:
            return cSurOk;
        }
    }

static uint32 AdaptLineFailureTypeGet(const struct tFmFailInfo *pFailInfo)
    {
    eSurLineTrbl failures = pFailInfo->trblType;

    if (failures == cFmSecLos)
        return cAtSdhLineAlarmLos;

    if (failures == cFmSecLof)
        return cAtSdhLineAlarmLof;

    if (failures == cFmSecTim)
        return cAtSdhLineAlarmTim;

    if (failures == cFmLineAis)
        return cAtSdhLineAlarmAis;

    if (failures == cFmLineBerSf)
        return cAtSdhLineAlarmBerSf;

    if (failures == cFmLineBerSd)
        return cAtSdhLineAlarmBerSd;

    if (failures == cFmLineRdi)
        return cAtSdhLineAlarmRdi;

    return 0;
    }

static uint32 AdaptAuFailureTypeGet(const struct tFmFailInfo *pFailInfo)
    {
    eSurAuTrbl failures = pFailInfo->trblType;

    if (failures == cFmStsAis)
        return cAtSdhPathAlarmAis;

    if (failures == cFmStsLop)
        return cAtSdhPathAlarmLop;

    if (failures == cFmStsBerSf)
        return cAtSdhPathAlarmBerSf;

    if (failures == cFmStsBerSd)
        return cAtSdhPathAlarmBerSd;

    if (failures == cFmStsUneq)
        return cAtSdhPathAlarmUneq;

    if (failures == cFmStsTim)
        return cAtSdhPathAlarmTim;

    if (failures == cFmStsPlm)
        return cAtSdhPathAlarmPlm;

    if (failures == cFmStsRfi)
        return cAtSdhPathAlarmRfi;

    if (failures == cFmStsErfiS)
        return cAtSdhPathAlarmErdiS;

    if (failures == cFmStsErfiC)
        return cAtSdhPathAlarmErdiC;

    if (failures == cFmStsErfiP)
        return cAtSdhPathAlarmErdiP;

    return 0;
    }

static uint32 AdaptTuFailureTypeGet(const struct tFmFailInfo *pFailInfo)
    {
    eSurTuTrbl failures = pFailInfo->trblType;

    if (failures == cFmVtAis)
        return cAtSdhPathAlarmAis;

    if (failures == cFmVtLop)
        return cAtSdhPathAlarmLop;

    if (failures == cFmVtBerSf)
        return cAtSdhPathAlarmBerSf;

    if (failures == cFmVtBerSd)
        return cAtSdhPathAlarmBerSd;

    if (failures == cFmVtUneq)
        return cAtSdhPathAlarmUneq;

    if (failures == cFmVtTim)
        return cAtSdhPathAlarmTim;

    if (failures == cFmVtPlm)
        return cAtSdhPathAlarmPlm;

    if (failures == cFmVtRfi)
        return cAtSdhPathAlarmRdi;

    if (failures == cFmVtErfiS)
        return cAtSdhPathAlarmErdiS;

    if (failures == cFmVtErfiC)
        return cAtSdhPathAlarmErdiC;

    if (failures == cFmVtErfiP)
        return cAtSdhPathAlarmErdiP;

    return 0;
    }

static uint32 AdaptPdhDe1FailureTypeGet(const struct tFmFailInfo *pFailInfo)
    {
    eSurDe1Trbl failures = pFailInfo->trblType;

    if (failures == cFmDe1Los)
        return cAtPdhDe1AlarmLos;

    if (failures == cFmDe1Lof)
        return cAtPdhDe1AlarmLof;

    if (failures == cFmDe1Ais)
        return cAtPdhDe1AlarmAis;

    if (failures == cFmDe1Rai)
        return cAtPdhDe1AlarmRai;

    return 0;
    }

static uint32 AdaptPdhDe3FailureTypeGet(const struct tFmFailInfo *pFailInfo)
    {
    eSurDe3Trbl failures = pFailInfo->trblType;

    if (failures == cFmDe3Los)
        return cAtPdhDe3AlarmLos;

    if (failures == cFmDe3Lof)
        return cAtPdhDe3AlarmLof;

    if (failures == cFmDe3Ais)
        return cAtPdhDe3AlarmAis;

    if (failures == cFmDe3Rai)
        return cAtPdhDe3AlarmRai;

    return 0;
    }

static uint32 AdaptPwFailureTypeGet(const struct tFmFailInfo *pFailInfo)
    {
    eSurPwTrbl failures = pFailInfo->trblType;

    if (failures == cFmPwJitBufOverrun)
        return cAtPwAlarmTypeJitterBufferOverrun;

    if (failures == cFmPwLofs)
        return cAtPwAlarmTypeLops;

    if (failures == cFmPwRemotePktLoss)
        return cAtPwAlarmTypeRBit;

    return 0;
    }

static AtSurEngine SurEngineFromChannelId(tSurDevHdl *hdl, eSurChnType chnType, const void *chnId)
    {
    switch ((uint32)chnType)
        {
        case cSurLine:
            {
            AtChannel line = (AtChannel)AtModuleSdhLineGet(SdhModule(hdl),
                                                           (uint8)((*((uint16 *)chnId))- 1));
            return AtChannelSurEngineGet(line);
            }
        case cSurSts:
        case cSurAu:
            return AtChannelSurEngineGet((AtChannel)HoVcFromChannelId(hdl, chnId));
        case cSurTu3:
            return AtChannelSurEngineGet((AtChannel)Vc3Tu3FromChannelId(hdl, chnId));
        case cSurVt:
        case cSurTu:
            return AtChannelSurEngineGet((AtChannel)Vc1xFromChannelId(hdl, chnId));
        case cSurDe3:
            {
            AtChannel de3 = (AtChannel)De3FromChannelId(hdl, chnId);
            return AtChannelSurEngineGet((AtChannel)de3);
            }
        case cSurDe1:
            {
            AtChannel de1 = (AtChannel)De1FromChannelId(hdl, chnId);
            return AtChannelSurEngineGet((AtChannel)de1);
            }
        case cSurPw:
            {
            AtChannel pw = (AtChannel)AtModulePwGetPw(PwModule(hdl),(uint32)(*((uint16 *)chnId) - 1));
            return AtChannelSurEngineGet((AtChannel)pw);
            }
        default:
            return NULL;
        }
    }

static uint32 AdaptFailureTypeGet(eSurChnType chnType, const struct tFmFailInfo *pFailInfo)
    {
    switch ((uint32)chnType)
        {
        case cSurLine:
            return AdaptLineFailureTypeGet(pFailInfo);
            return 0;
        case cSurSts:
        case cSurAu:
            return AdaptAuFailureTypeGet(pFailInfo);
        case cSurTu3:
            return AdaptAuFailureTypeGet(pFailInfo);
        case cSurVt:
        case cSurTu:
            return AdaptTuFailureTypeGet(pFailInfo);
        case cSurDe3:
            return AdaptPdhDe3FailureTypeGet(pFailInfo);
        case cSurDe1:
            return AdaptPdhDe1FailureTypeGet(pFailInfo);
        case cSurPw:
            return AdaptPwFailureTypeGet(pFailInfo);
        default:
            return 0;
        }
    }

static void FailNotf(tSurDevHdl *pHdl, eSurChnType chnType, const void *pChnId, const struct tFmFailInfo *pFailInfo)
    {
    AtSurEngineFailureNotify(SurEngineFromChannelId(pHdl, chnType, pChnId), AdaptFailureTypeGet(chnType, pFailInfo), pFailInfo->status);
    }

static void TcaNotf(tSurDevHdl *hdl, eSurChnType chnType, const void *chnId, void * info)
    {
    tPmTcaInfo *tcaInfo = (tPmTcaInfo*)info;
    AtPmParam param;
    AtSurEngine engine = SurEngineFromChannelId(hdl, chnType, chnId);

    switch ((uint32)chnType)
        {
        case cSurLine:
            {
            ePmLineParm paramType = tcaInfo->parm;

            if (paramType == cPmSecSefs)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamSefsS);
            if (paramType == cPmSecCv)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamCvS);
            if (paramType == cPmSecEs)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamEsS);
            if (paramType == cPmSecSes)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamSesS);
            if (paramType == cPmLineCv)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamCvL);
            if (paramType == cPmLineEs)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamEsL);
            if (paramType == cPmLineSes)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamSesL);
            if (paramType == cPmLineUas)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamUasL);
            if (paramType == cPmLineFc)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamFcL);
            if (paramType == cPmLineCvLfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamCvLfe);
            if (paramType == cPmLineEsLfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamEsLfe);
            if (paramType == cPmLineSesLfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamSesLfe);
            if (paramType == cPmLineUasLfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamUasLfe);
            if (paramType == cPmLineFcLfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhLinePmParamFcLfe);
            break;
            }
        case cSurSts:
        case cSurAu:
        case cSurTu3:
            {
            ePmAuParm paramType = tcaInfo->parm;

            if (paramType == cPmStsPpjcPdet)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPpjcPdet);
            if (paramType == cPmStsNpjcPdet)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamNpjcPdet);
            if (paramType == cPmStsPpjcPgen)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPpjcPgen);
            if (paramType == cPmStsNpjcPgen)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamNpjcPgen);
            if (paramType == cPmStsPjcDiff)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcDiff);
            if (paramType == cPmStsPjcsPdet)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcsPdet);
            if (paramType == cPmStsPjcsPgen)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcsPgen);
            if (paramType == cPmStsCv)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamCvP);
            if (paramType == cPmStsEs)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamEsP);
            if (paramType == cPmStsSes)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamSesP);
            if (paramType == cPmStsUas)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamUasP);
            if (paramType == cPmStsFc)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamFcP);
            if (paramType == cPmStsCvPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamCvPfe);
            if (paramType == cPmStsEsPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamEsPfe);
            if (paramType == cPmStsSesPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamSesPfe);
            if (paramType == cPmStsUasPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamUasPfe);
            if (paramType == cPmStsFcPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamFcPfe);
            break;
            }
        case cSurVt:
        case cSurTu:
            {
            ePmTuParm paramType = tcaInfo->parm;

            if (paramType == cPmVtPpjcVdet)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPpjcPdet);
            if (paramType == cPmVtNpjcVdet)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamNpjcPdet);
            if (paramType == cPmVtPpjcVgen)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPpjcPgen);
            if (paramType == cPmVtNpjcVgen)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamNpjcPgen);
            if (paramType == cPmVtPjcDiff)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcDiff);
            if (paramType == cPmVtPjcsVdet)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcsPdet);
            if (paramType == cPmVtPjcsVgen)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcsPgen);
            if (paramType == cPmVtCv)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamCvP);
            if (paramType == cPmVtEs)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamEsP);
            if (paramType == cPmVtSes)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamSesP);
            if (paramType == cPmVtUas)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamUasP);
            if (paramType == cPmVtFc)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamFcP);
            if (paramType == cPmVtCvVfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamCvPfe);
            if (paramType == cPmVtEsVfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamEsPfe);
            if (paramType == cPmVtSesVfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamSesPfe);
            if (paramType == cPmVtUasVfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamUasPfe);
            if (paramType == cPmVtFcVfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamFcPfe);
            break;
            }
        case cSurDe3:
            {
            ePmDe3Parm paramType = tcaInfo->parm;

            if (paramType == cPmDe3CvL)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamCvL);
            if (paramType == cPmDe3EsL)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamEsL);
            if (paramType == cPmDe3SesL)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamSesL);
            if (paramType == cPmDe3LossL)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamLossL);
            if (paramType == cPmDe3CvpP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamCvpP);
            if (paramType == cPmDe3CvcpP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamCvcpP);
            if (paramType == cPmDe3EspP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamEspP);
            if (paramType == cPmDe3EscpP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamEscpP);
            if (paramType == cPmDe3SespP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamSespP);
            if (paramType == cPmDe3SescpP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamSescpP);
            if (paramType == cPmDe3SasP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamSasP);
            if (paramType == cPmDe3AissP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamAissP);
            if (paramType == cPmDe3UaspP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamUaspP);
            if (paramType == cPmDe3UascpP)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamUascpP);
            if (paramType == cPmDe3CvcpPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamCvcpPfe);
            if (paramType == cPmDe3EscpPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamEscpPfe);
            if (paramType == cPmDe3SescpPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamSescpPfe);
            if (paramType == cPmDe3SascpPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamSascpPfe);
            if (paramType == cPmDe3UascpPfe)
                param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe3PmParamUascpPfe);
            break;
            }
        case cSurDe1:
            {
            ePmDe1Parm paramType = tcaInfo->parm;

            if (paramType == cPmDe1CvL)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamCvL);
            if (paramType == cPmDe1EsL)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamEsL);
            if (paramType == cPmDe1SesL)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamSesL);
            if (paramType == cPmDe1LossL)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamLossL);
            if (paramType == cPmDe1EsLfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamEsLfe);
            if (paramType == cPmDe1CvP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamCvP);
            if (paramType == cPmDe1EsP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamEsP);
            if (paramType == cPmDe1SesP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamSesP);
            if (paramType == cPmDe1AissP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamAissP);
            if (paramType == cPmDe1SasP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamSasP);
            if (paramType == cPmDe1CssP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamCssP);
            if (paramType == cPmDe1UasP)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamUasP);
            if (paramType == cPmDe1SefsPfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamSefsPfe);
            if (paramType == cPmDe1EsPfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamEsPfe);
            if (paramType == cPmDe1SesPfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamSesPfe);
            if (paramType == cPmDe1CssPfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamCssPfe);
            if (paramType == cPmDe1FcPfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamFcPfe);
            if (paramType == cPmDe1UasPfe)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePdhDe1PmParamUasPfe);
            break;
            }
        case cSurPw:
            {
            ePmPwParm paramType = tcaInfo->parm;

            if (paramType == cPmPwEs)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePwPmParamEs);
            if (paramType == cPmPwSes)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePwPmParamSes);
            if (paramType == cPmPwUas)
               param = AtSurEnginePmParamGet(engine, cAtSurEnginePwPmParamUas);
            break;
            }
        default:
            break;
        }
    }

static void SurAdaptDefectStatusGet(uint32 alarms, dword *pDefStat)
    {
    if (alarms & cAtSdhPathAlarmAis)
        *pDefStat |= cSurStsAisMask;
    if (alarms & cAtSdhPathAlarmLop)
        *pDefStat |= cSurStsLopMask;
    if (alarms & cAtSdhPathAlarmBerSf)
        *pDefStat |= cSurStsBerSfMask;
    if (alarms & cAtSdhPathAlarmBerSd)
        *pDefStat |= cSurStsBerSdMask;
    if (alarms & cAtSdhPathAlarmUneq)
        *pDefStat |= cSurStsUneqMask;
    if (alarms & cAtSdhPathAlarmTim)
        *pDefStat |= cSurStsTimMask;
    if (alarms & cAtSdhPathAlarmPlm)
        *pDefStat |= cSurStsPlmMask;
    if (alarms & cAtSdhPathAlarmRfi)
        *pDefStat |= cSurStsRdiMask;
    if (alarms & cAtSdhPathAlarmErdiS)
        *pDefStat |= cSurStsErdiSMask;
    if (alarms & cAtSdhPathAlarmErdiC)
        *pDefStat |= cSurStsErdiCMask;
    if (alarms & cAtSdhPathAlarmErdiP)
        *pDefStat |= cSurStsErdiPMask;
    }

static eSurRet DefStatGet(tSurDevHdl  *pHdl,
                          eSurChnType  chnType,
                          const void  *pChnId,
                          dword       *pDefStat)
    {
    switch ((uint32)chnType)
        {
        case cSurLine:
            {
            AtChannel line = (AtChannel)AtModuleSdhLineGet(SdhModule(pHdl),
                                                           (uint8)((*((uint16 *)pChnId)) - 1));
            uint32 alarms = AtChannelAlarmGet(line);
            if (alarms & cAtSdhLineAlarmLos)
                *pDefStat |= cSurSecLosMask;
            if (alarms & cAtSdhLineAlarmLof)
                *pDefStat |= cSurSecLofMask;
            if (alarms & cAtSdhLineAlarmTim)
                *pDefStat |= cSurSecTimsMask;
            if (alarms & cAtSdhLineAlarmAis)
                *pDefStat |= cSurLineAislMask;
            if (alarms & cAtSdhLineAlarmRdi)
                *pDefStat |= cSurLineRdilMask;
            if (alarms & cAtSdhLineAlarmBerSd)
                *pDefStat |= cSurLineBerSdMask;
            if (alarms & cAtSdhLineAlarmBerSf)
                *pDefStat |= cSurLineBerSfMask;
            return cSurOk;
            }
        case cSurSts:
        case cSurAu:
            {
            AtSdhAu au = HoAuFromChannelId(pHdl, pChnId);
            uint32 alarms = AtChannelAlarmGet((AtChannel)au);

            SurAdaptDefectStatusGet(alarms, pDefStat);
            return cSurOk;
            }
        case cSurTu3:
            {
            tSurTu3Id *tu3Id = (tSurTu3Id *)pChnId;
            AtSdhLine line = AtModuleSdhLineGet(SdhModule(pHdl),(uint8)( tu3Id->au.line - 1));
            AtSdhTu tu3 = AtSdhLineTu3Get(line, Aug1Id(&tu3Id->au), (uint8)(tu3Id->tu3 - 1));
            uint32 alarms = AtChannelAlarmGet((AtChannel)tu3);

            SurAdaptDefectStatusGet(alarms, pDefStat);
            return cSurOk;
            }
        case cSurVt:
        case cSurTu:
            {
            tSurTuId tuId;
            AtSdhLine line;
            AtSdhTu tu1x;
            uint32 alarms;

            SurVt2Tu((const tSurVtId*)pChnId, &tuId);
            line = AtModuleSdhLineGet(SdhModule(pHdl),(uint8)( tuId.au.line - 1));
            tu1x = AtSdhLineTu1xGet(line, Aug1Id(&tuId.au), (uint8)(tuId.au.au - 1), (uint8)(tuId.tug2 - 1), (uint8)(tuId.tu - 1));
            alarms = AtChannelAlarmGet((AtChannel)tu1x);

            if (alarms & cAtSdhPathAlarmAis)
               *pDefStat |= cSurVtAisMask;
            if (alarms & cAtSdhPathAlarmLop)
               *pDefStat |= cSurVtLopMask;
            if (alarms & cAtSdhPathAlarmBerSf)
               *pDefStat |= cSurVtBerSfMask;
            if (alarms & cAtSdhPathAlarmBerSd)
               *pDefStat |= cSurVtBerSdMask;
            if (alarms & cAtSdhPathAlarmUneq)
               *pDefStat |= cSurVtUneqMask;
            if (alarms & cAtSdhPathAlarmTim)
               *pDefStat |= cSurVtTimMask;
            if (alarms & cAtSdhPathAlarmPlm)
               *pDefStat |= cSurVtPlmMask;
            if (alarms & cAtSdhPathAlarmRdi)
               *pDefStat |= cSurVtRdiMask;
            if (alarms & cAtSdhPathAlarmErdiS)
               *pDefStat |= cSurVtErdiSMask;
            if (alarms & cAtSdhPathAlarmErdiC)
               *pDefStat |= cSurVtErdiCMask;
            if (alarms & cAtSdhPathAlarmErdiP)
               *pDefStat |= cSurVtErdiPMask;
            return cSurOk;
            }
        case cSurDe3:
            {
            AtChannel de3 = (AtChannel)De3FromChannelId(pHdl, pChnId);
            uint32 alarms = AtChannelAlarmGet((AtChannel)de3);

            if (alarms & cAtPdhDe3AlarmLos)
               *pDefStat |= cSurDe3LosMask;
            if (alarms & cAtPdhDe3AlarmLof)
               *pDefStat |= cSurDe3OofMask;
            if (alarms & cAtPdhDe3AlarmAis)
               *pDefStat |= cSurDe3AisMask;
            if (alarms & cAtPdhDe3AlarmRai)
               *pDefStat |= cSurDe3RaiMask;
            return cSurOk;
            }
        case cSurDe1:
            {
            AtChannel de1 = (AtChannel)De1FromChannelId(pHdl, pChnId);
            uint32 alarms = AtChannelAlarmGet((AtChannel)de1);

            if (alarms & cAtPdhDe1AlarmLos)
               *pDefStat |= cSurDe1LosMask;
            if (alarms & cAtPdhDe1AlarmLof)
               *pDefStat |= cSurDe1OofMask;
            if (alarms & cAtPdhDe1AlarmAis)
               *pDefStat |= cSurDe1AisMask;
            if (alarms & cAtPdhDe3AlarmRai)
               *pDefStat |= cSurDe1RaiMask;
            return cSurOk;
            }
        case cSurPw:
            {
            AtChannel pw = (AtChannel)AtModulePwGetPw(PwModule(pHdl),
                                                      (uint32)(*((uint16 *)pChnId) - 1));
            uint32 alarms = AtChannelAlarmGet((AtChannel)pw);

            if (alarms & cAtPwAlarmTypeJitterBufferOverrun)
               *pDefStat |= cSurPwJitBufOverrunMask;
            if (alarms & cAtPwAlarmTypeLops)
               *pDefStat |= cSurPwLofsMask;
            if (alarms & cAtPwAlarmTypeRBit)
               *pDefStat |= cSurPwRemotePktLossMask;
            return cSurOk;
            }
        default:
            return cSurOk;
        }

    return cSurOk;
    }

static void HoVcId(AtSdhChannel channel, tSurAuId *channelId)
    {
    AtSdhChannel au = AtSdhChannelParentChannelGet(channel);
    AtChannel aug = (AtChannel)AtSdhChannelParentChannelGet(au);
    uint16 lineId = AtSdhChannelLineGet(channel);
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModuleSdh), (uint8)lineId);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    channelId->line = (tSurLineId)(AtChannelIdGet((AtChannel)line) + 1);

    if (channelType == cAtSdhChannelTypeVc4)
        {
        if (rate == cAtSdhLineRateStm1)
            return;

        channelId->aug1 =  (byte)(AtChannelIdGet(aug) % 4 + 1);
        if (rate == cAtSdhLineRateStm4)
            return;

        channelId->aug4 = (byte)(AtChannelIdGet(aug) / 4 + 1);
        }
    else if (channelType == cAtSdhChannelTypeVc3)
        {
        if (rate == cAtSdhLineRateStm1)
            {
            channelId->au =  (byte)(AtChannelIdGet((AtChannel)au) + 1);
            return;
            }

        channelId->aug1 =  (byte)(AtChannelIdGet(aug) % 4 + 1);
        if (rate == cAtSdhLineRateStm4)
            return;

        channelId->aug4 =  (byte)(AtChannelIdGet(aug) / 4 + 1);
        }
    else if (channelType == cAtSdhChannelTypeVc4_4c)
        {
        if (rate == cAtSdhLineRateStm4)
            return;
        channelId->aug4 =  (byte)(AtChannelIdGet(aug) + 1);
        }
    else
        {
        /* TODO: not support yet */
        }
    }

static void Tu3Id(AtSdhChannel channel, tSurTu3Id *channelId)
    {
    AtSdhChannel tu3 = AtSdhChannelParentChannelGet(channel); /* TODO: assume that we only support path TU3 first */
    AtSdhChannel tug3 = AtSdhChannelParentChannelGet(tu3);
    AtSdhChannel vc4 = AtSdhChannelParentChannelGet(tug3);

    HoVcId(vc4, &(channelId->au));
    channelId->tu3 = (byte)(AtChannelIdGet((AtChannel)tug3) + 1);
    }

static void TuId(AtSdhChannel channel, tSurTuId *channelId)
    {
    AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
    AtSdhChannel tug2 = AtSdhChannelParentChannelGet(tu1x);
    AtSdhChannel tug3 = AtSdhChannelParentChannelGet(tug2);
    AtSdhChannel vc4 = AtSdhChannelParentChannelGet(tug3);

    HoVcId(vc4, &(channelId->au));
    channelId->au.au =(byte)( AtChannelIdGet((AtChannel)tug3) + 1);
    channelId->tug2 = (byte)(AtChannelIdGet((AtChannel)tug2) + 1);
    channelId->tu = (byte)(AtChannelIdGet((AtChannel)tu1x) + 1);
    }

/* TODO: assume it is DE3 from SDH VC4 multiplexing */
static void De3Id(AtPdhChannel channel, tSurDe3Id *channelId)
    {
    AtSdhChannel vc3 = (AtSdhChannel)AtPdhChannelVcGet(channel);
    AtSdhChannel tu3 = AtSdhChannelParentChannelGet(vc3);
    AtSdhChannel tug3 = AtSdhChannelParentChannelGet(tu3);
    AtSdhChannel vc4 = AtSdhChannelParentChannelGet(tug3);
    AtSdhChannel au4 = AtSdhChannelParentChannelGet(vc4);
    AtChannel aug = (AtChannel)AtSdhChannelParentChannelGet(au4);
    uint16 lineId = AtSdhChannelLineGet(vc3);
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModuleSdh), (uint8)lineId);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    *channelId = (tSurDe3Id)(lineId * NumSts1InLine(rate) +
                 (uint32) (AtChannelIdGet(aug) * 3) +
                 AtChannelIdGet((AtChannel)tug3) + 1 +
                 SdhLineOffset(lineId));
    }

/* TODO: assume it is DE1 from SDH VC4 multiplexing */
static void De1Id(AtPdhChannel channel, tSurDe1Id *channelId)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtPdhChannelVcGet(channel);
    AtSdhChannel tu1x = AtSdhChannelParentChannelGet(vc1x);
    AtSdhChannel tug2 = AtSdhChannelParentChannelGet(tu1x);
    AtSdhChannel tug3 = AtSdhChannelParentChannelGet(tug2);
    AtSdhChannel vc4 = AtSdhChannelParentChannelGet(tug3);
    AtSdhChannel au4 = AtSdhChannelParentChannelGet(vc4);
    AtChannel aug = (AtChannel)AtSdhChannelParentChannelGet(au4);
    uint16 lineId = AtSdhChannelLineGet(vc1x);
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModuleSdh), (uint8)lineId);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    channelId->de1Id = (byte)(AtChannelIdGet((AtChannel)tu1x) + 1);
    channelId->de2Id = (byte)(AtChannelIdGet((AtChannel)tug2) + 1);
    channelId->de3Id = (tSurDe3Id)(lineId * NumSts1InLine(rate) + AtChannelIdGet(aug) * 3 + AtChannelIdGet((AtChannel)tug3) + 1 + SdhLineOffset((uint16)lineId));
    }

eAtRet AtSurAdaptInit(AtDevice device)
    {
    eSurRet ret;
    tSurDevIntf pIntf;

    if (surDevice != NULL)
        return cSurOk;

    if (SurDbInit() != cSurOk)
        return cAtError;

    SurEngDbStart();

    pIntf.AnomalyGet = AnomalyGet;
    pIntf.DefStatGet = DefStatGet;
    pIntf.FailNotf = FailNotf;
    pIntf.TcaNotf = TcaNotf;

    ret = SurDevAdd(device, &surDevice, &pIntf);
    if (ret != cSurOk)
        return cAtError;

    ret = SurDevStatSet(surDevice, cSurStart);
    if (ret != cSurOk)
        return cAtError;

    return cAtOk;
    }

eAtRet AtSurAdaptDelete(AtDevice device)
    {
    eSurRet ret;

    ret = SurDevStatSet(surDevice, cSurStop);
    if (ret != cSurOk)
        return cAtError;

    ret = SurDevRemove(&surDevice);
    if (ret != cSurOk)
        return cAtError;

    return cAtOk;
    }

eAtRet AtSurAdaptLineSurEngineEnable(AtSdhLine line, eBool enable)
    {
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    eSurRet ret  = SurLineEngStatSet(surDevice, &lineId, enable ? cSurStart : cSurStop);
    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptLineSurEngineIsEnabled(AtSdhLine line)
    {
    eSurEngStat stat;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    SurLineEngStatGet(surDevice, &lineId, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static uint8 DefaultNumRecentRegister(void)
    {
    return 16;
    }

static void DefaultLineNumRecentRegisterSet(uint16 lineId)
    {
    PmLineParmNumRecRegSet(surDevice, &lineId, cPmSecAllParm,    DefaultNumRecentRegister());
    PmLineParmNumRecRegSet(surDevice, &lineId, cPmLineNeAllParm, DefaultNumRecentRegister());
    PmLineParmNumRecRegSet(surDevice, &lineId, cPmLineFeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptLineSurEngineProvision(AtSdhLine line, eBool enable)
    {
    eSurRet ret = cAtOk;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);

    if (enable)
        {
        tSurLineConf chnCfg;
        eAtSdhLineRate rate = AtSdhLineRateGet(line);

        ret |= SurLineProv(surDevice, &lineId);
        ret |= SurLineChnCfgGet(surDevice, &lineId, &chnCfg);
        if (rate == cAtSdhLineRateStm1)
            chnCfg.rate = cSurOc3Stm1;
        if (rate == cAtSdhLineRateStm4)
            chnCfg.rate = cSurOc12Stm4;
        if (rate == cAtSdhLineRateStm16)
            chnCfg.rate = cSurOc48Stm16;
        ret |= SurLineChnCfgSet(surDevice, &lineId, &chnCfg);
        DefaultLineNumRecentRegisterSet(lineId);
        }
    else
        ret = SurLineDeProv(surDevice, &lineId);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eAtRet AtSurAdaptHoVcSurEngineEnable(AtSdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    ret = SurAuEngStatSet(surDevice, &auId, enable ? cSurStart : cSurStop);
    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptHoVcSurEngineIsEnabled(AtSdhChannel channel)
    {
    eSurEngStat stat;
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    SurAuEngStatGet(surDevice, &auId, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static void DefaultHoVcNumRecentRegisterSet(tSurAuId *pChnId)
    {
    PmAuParmNumRecRegSet(surDevice, pChnId, cPmStsNeAllParm, DefaultNumRecentRegister());
    PmAuParmNumRecRegSet(surDevice, pChnId, cPmStsFeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptHoVcSurEngineProvision(AtSdhChannel channel, eBool enable)
    {
    eSurRet ret = cAtOk;
    uint8 tug3Id;
    tSurAuId auId;
    eAtSdhVcMapType mapType = AtSdhChannelMapTypeGet(channel);
    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    if (enable)
        {
        if (mapType == cAtSdhVcMapTypeVc4MapC4)
            {
            ret |= SurAuProv(surDevice, &auId);
            DefaultHoVcNumRecentRegisterSet(&auId);
            }
        /* Provision three STSs in-case run VC4 channelize */
        else
            {
            for (tug3Id = 1; tug3Id <= 3; tug3Id++)
                {
                auId.au = tug3Id;
                ret |= SurAuProv(surDevice, &auId);
                DefaultHoVcNumRecentRegisterSet(&auId);
                }
            }
        }
    else
        {
        ret = cSurErr;

        /* Always try to de-provision 3 TUG3s, return OK if first TUG3 de-provision OK */
        for (tug3Id = 1; tug3Id <= 3; tug3Id++)
            {
            if (SurAuDeProv(surDevice, &auId) == cSurOk)
                ret = cSurOk;
            }
        }

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eAtRet AtSurAdaptTu3SurEngineEnable(AtSdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    ret = SurTu3EngStatSet(surDevice, &tu3Id, enable ? cSurStart : cSurStop);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptTu3SurEngineIsEnabled(AtSdhChannel channel)
    {
    eSurEngStat stat;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    SurTu3EngStatGet(surDevice, &tu3Id, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static void DefaultTu3NumRecentRegisterSet(tSurTu3Id *pChnId)
    {
    PmTu3ParmNumRecRegSet(surDevice, pChnId, cPmStsNeAllParm, DefaultNumRecentRegister());
    PmTu3ParmNumRecRegSet(surDevice, pChnId, cPmStsFeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptTu3SurEngineProvision(AtSdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    if (enable)
        {
        ret = SurTu3Prov(surDevice, &tu3Id);
        DefaultTu3NumRecentRegisterSet(&tu3Id);
        }
    else
        ret = SurTu3DeProv(surDevice, &tu3Id);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eAtRet AtSurAdaptTuSurEngineEnable(AtSdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    ret = SurTuEngStatSet(surDevice, &tuId, enable ? cSurStart : cSurStop);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptTuSurEngineIsEnabled(AtSdhChannel channel)
    {
    eSurEngStat stat;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    SurTuEngStatGet(surDevice, &tuId, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static void DefaultTuNumRecentRegisterSet(tSurTuId *pChnId)
    {
    PmTuParmNumRecRegSet(surDevice, pChnId, cPmVtNeAllParm, DefaultNumRecentRegister());
    PmTuParmNumRecRegSet(surDevice, pChnId, cPmVtFeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptTuSurEngineProvision(AtSdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    if (enable)
        {
        ret = SurTuProv(surDevice, &tuId);
        DefaultTuNumRecentRegisterSet(&tuId);
        }
    else
        ret = SurTuDeProv(surDevice, &tuId);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eAtRet AtSurAdaptPdhDe3SurEngineEnable(AtPdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    ret = SurDe3EngStatSet(surDevice, &de3Id, enable ? cSurStart : cSurStop);
    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptPdhDe3SurEngineIsEnabled(AtPdhChannel channel)
    {
    eSurEngStat stat;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    SurDe3EngStatGet(surDevice, &de3Id, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static void DefaultDe3NumRecentRegisterSet(tSurDe3Id *pChnId)
    {
    PmDe3ParmNumRecRegSet(surDevice, pChnId, cPmDe3NeAllParm, DefaultNumRecentRegister());
    PmDe3ParmNumRecRegSet(surDevice, pChnId, cPmDe3FeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptPdhDe3SurEngineProvision(AtPdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    if (enable)
        {
        ret = SurDe3Prov(surDevice, &de3Id);
        DefaultDe3NumRecentRegisterSet(&de3Id);
        }
    else
        ret = SurDe3DeProv(surDevice, &de3Id);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eAtRet AtSurAdaptPdhDe1SurEngineEnable(AtPdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    ret = SurDe1EngStatSet(surDevice, &de1Id, enable ? cSurStart : cSurStop);
    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptPdhDe1SurEngineIsEnabled(AtPdhChannel channel)
    {
    eSurEngStat stat;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    SurDe1EngStatGet(surDevice, &de1Id, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static void DefaultDe1NumRecentRegisterSet(tSurDe1Id *pChnId)
    {
    PmDe1ParmNumRecRegSet(surDevice, pChnId, cPmDe1NeAllParm, DefaultNumRecentRegister());
    PmDe1ParmNumRecRegSet(surDevice, pChnId, cPmDe1FeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptPdhDe1SurEngineProvision(AtPdhChannel channel, eBool enable)
    {
    eSurRet ret;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    if (enable)
        {
        ret = SurDe1Prov(surDevice, &de1Id);
        DefaultDe1NumRecentRegisterSet(&de1Id);
        }
    else
        ret = SurDe1DeProv(surDevice, &de1Id);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eAtRet AtSurAdaptPwSurEngineEnable(AtPw pw, eBool enable)
    {
    eSurRet ret;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    ret = SurPwEngStatSet(surDevice, &pwId, enable ? cSurStart : cSurStop);
    return (ret != cSurOk) ? cAtError : cAtOk;
    }

eBool AtSurAdaptPwSurEngineIsEnabled(AtPw pw)
    {
    eSurEngStat stat;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    SurPwEngStatGet(surDevice, &pwId, &stat);
    return (stat == cSurStart) ? cAtTrue : cAtFalse;
    }

static void DefaultPwNumRecentRegisterSet(uint16 pwId)
    {
    PmPwParmNumRecRegSet(surDevice, &pwId, cPmPwNeAllParm, DefaultNumRecentRegister());
    }

eAtRet AtSurAdaptPwSurEngineProvision(AtPw pw, eBool enable)
    {
    eSurRet ret;
    tSurPwId pwId = AtChannelIdGet((AtChannel)pw) + 1;

    if (enable)
        {
        ret = SurPwProv(surDevice, &pwId);
        DefaultPwNumRecentRegisterSet(pwId);
        }
    else
        ret = SurPwDeProv(surDevice, &pwId);

    return (ret != cSurOk) ? cAtError : cAtOk;
    }

uint32 AtSurAdaptLineFailureGet(AtSdhLine line)
    {
    eSurRet ret;
    uint32 alarms = 0;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    uint16 *pLineId = &lineId;
    tSurFailStat failStat;

    /* Only need to check the first time to make sure this channel is provisioned */
    ret = FmLineFailIndGet(surDevice, pLineId, cFmSecLos, &failStat);
    if (ret != cSurOk)
        return 0;
    if (failStat.status)
        alarms |= cAtSdhLineAlarmLos;

    FmLineFailIndGet(surDevice, pLineId, cFmSecLof, &failStat);
    if (failStat.status)
        alarms |= cAtSdhLineAlarmLof;

    FmLineFailIndGet(surDevice, pLineId, cFmSecTim, &failStat);
    if (failStat.status)
        alarms |= cAtSdhLineAlarmTim;

    FmLineFailIndGet(surDevice, pLineId, cFmLineAis, &failStat);
    if (failStat.status)
        alarms |= cAtSdhLineAlarmAis;

    FmLineFailIndGet(surDevice, pLineId, cFmLineBerSf, &failStat);
    if (failStat.status)
        alarms |= cAtSdhLineAlarmBerSf;

    FmLineFailIndGet(surDevice, pLineId, cFmLineBerSd, &failStat);
    if (failStat.status)
        alarms |= cAtSdhLineAlarmBerSd;

    FmLineFailIndGet(surDevice, pLineId, cFmLineRdi, &failStat);
    if (failStat.status)
        alarms |= cAtSdhLineAlarmRdi;

    return alarms;
    }

uint32 AtSurAdaptHoVcFailureGet(AtSdhChannel channel)
    {
    eSurRet ret;
    uint32 alarms = 0;
    tSurFailStat failStat;
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    /* Only need to check the first time to make sure this channel is provisioned */
    ret = FmAuFailIndGet(surDevice, &auId, cFmStsAis, &failStat);
    if (ret != cSurOk)
        return 0;
    if (failStat.status)
        alarms |= cAtSdhPathAlarmAis;

    FmAuFailIndGet(surDevice, &auId, cFmStsLop, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmLop;

    FmAuFailIndGet(surDevice, &auId, cFmStsBerSf, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmBerSf;

    FmAuFailIndGet(surDevice, &auId, cFmStsBerSd, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmBerSd;

    FmAuFailIndGet(surDevice, &auId, cFmStsUneq, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmUneq;

    FmAuFailIndGet(surDevice, &auId, cFmStsTim, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmTim;

    FmAuFailIndGet(surDevice, &auId, cFmStsPlm, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmPlm;

    FmAuFailIndGet(surDevice, &auId, cFmStsRfi, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmRfi;

    FmAuFailIndGet(surDevice, &auId, cFmStsErfiS, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiS;

    FmAuFailIndGet(surDevice, &auId, cFmStsErfiC, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiC;

    FmAuFailIndGet(surDevice, &auId, cFmStsErfiP, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiS;

    FmAuFailIndGet(surDevice, &auId, cFmStsErfiP, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiP;

    return alarms;
    }

uint32 AtSurAdaptTu3FailureGet(AtSdhChannel channel)
    {
    eSurRet ret;
    uint32 alarms = 0;
    tSurFailStat failStat;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tu3Id));
    Tu3Id(channel, &tu3Id);

    /* Only need to check the first time to make sure this channel is provisioned */
    ret = FmTu3FailIndGet(surDevice, &tu3Id, cFmStsAis, &failStat);
    if (ret != cSurOk)
        return 0;
    if (failStat.status)
        alarms |= cAtSdhPathAlarmAis;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsLop, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmLop;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsBerSf, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmBerSf;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsBerSd, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmBerSd;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsUneq, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmUneq;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsTim, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmTim;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsPlm, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmPlm;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsRfi, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmRfi;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsErfiS, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiS;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsErfiC, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiC;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsErfiP, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiS;

    FmTu3FailIndGet(surDevice, &tu3Id, cFmStsErfiP, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiP;

    return alarms;
    }

uint32 AtSurAdaptTuFailureGet(AtSdhChannel channel)
    {
    eSurRet ret;
    uint32 alarms = 0;
    tSurFailStat failStat;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tuId));
    TuId(channel, &tuId);

    ret = FmTuFailIndGet(surDevice, &tuId, cFmVtAis, &failStat);
    if (ret != cSurOk)
        return 0;

    if (failStat.status)
        alarms |= cAtSdhPathAlarmAis;

    FmTuFailIndGet(surDevice, &tuId, cFmVtLop, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmLop;

    FmTuFailIndGet(surDevice, &tuId, cFmVtBerSf, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmBerSf;

    FmTuFailIndGet(surDevice, &tuId, cFmVtBerSd, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmBerSd;

    FmTuFailIndGet(surDevice, &tuId, cFmVtUneq, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmUneq;

    FmTuFailIndGet(surDevice, &tuId, cFmVtTim, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmTim;

    FmTuFailIndGet(surDevice, &tuId, cFmVtPlm, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmPlm;

    FmTuFailIndGet(surDevice, &tuId, cFmVtRfi, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmRdi;

    FmTuFailIndGet(surDevice, &tuId, cFmVtErfiS, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiS;

    FmTuFailIndGet(surDevice, &tuId, cFmVtErfiC, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiC;

    FmTuFailIndGet(surDevice, &tuId, cFmVtErfiP, &failStat);
    if (failStat.status)
        alarms |= cAtSdhPathAlarmErdiP;

    return alarms;
    }

uint32 AtSurAdaptPdhDe3FailureGet(AtPdhChannel channel)
    {
    eSurRet ret;
    uint32 alarms = 0;
    tSurFailStat failStat;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    ret = FmDe3FailIndGet(surDevice, &de3Id, cFmDe3Los, &failStat);
    if (ret != cSurOk)
        return 0;

    if (failStat.status)
        alarms |= cAtPdhDe3AlarmLos;

    FmDe3FailIndGet(surDevice, &de3Id, cFmDe3Lof, &failStat);
    if (failStat.status)
        alarms |= cAtPdhDe3AlarmLof;

    FmDe3FailIndGet(surDevice, &de3Id, cFmDe3Ais, &failStat);
    if (failStat.status)
        alarms |= cAtPdhDe3AlarmAis;

    FmDe3FailIndGet(surDevice, &de3Id, cFmDe3Rai, &failStat);
    if (failStat.status)
        alarms |= cAtPdhDe3AlarmRai;

    return alarms;
    }

uint32 AtSurAdaptPdhDe1FailureGet(AtPdhChannel channel)
    {
    eSurRet ret;
    uint32 alarms = 0;
    tSurFailStat SurFailStat;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    ret = FmDe1FailIndGet(surDevice, &de1Id, cFmDe1Los, &SurFailStat);
    if (ret != cSurOk)
        return 0;

    if (SurFailStat.status)
        alarms |= cAtPdhDe1AlarmLos;

    FmDe1FailIndGet(surDevice, &de1Id, cFmDe1Lof, &SurFailStat);
    if (SurFailStat.status)
        alarms |= cAtPdhDe1AlarmLof;

    FmDe1FailIndGet(surDevice, &de1Id, cFmDe1Ais, &SurFailStat);
    if (SurFailStat.status)
        alarms |= cAtPdhDe1AlarmAis;

    FmDe1FailIndGet(surDevice, &de1Id, cFmDe1Rai, &SurFailStat);
    if (SurFailStat.status)
        alarms |= cAtPdhDe1AlarmRai;

    return alarms;
    }

uint32 AtSurAdaptPwFailureGet(AtPw pw)
    {
    eSurRet ret;
    uint32 alarms = 0;
    tSurFailStat SurFailStat;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    /* TODO: Failure control of old Surveillance module may not good */
    ret = FmPwFailIndGet(surDevice, &pwId, cFmPwJitBufOverrun, &SurFailStat);
    if (ret != cSurOk)
        return 0;

    if (SurFailStat.status)
        alarms |= cAtPwAlarmTypeJitterBufferOverrun;

    FmPwFailIndGet(surDevice, &pwId, cFmPwLofs, &SurFailStat);
    if (SurFailStat.status)
        alarms |= cAtPwAlarmTypeLops;

    FmPwFailIndGet(surDevice, &pwId, cFmPwRemotePktLoss, &SurFailStat);
    if (SurFailStat.status)
        alarms |= cAtPwAlarmTypeRBit;

    return alarms;
    }

uint32 AtSurAdaptLineFailureHistoryGet(AtSdhLine line)
    {
    uint32 alarms = 0;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    tSortList list = FmLineFailHisGet(surDevice, &lineId);
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;

    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptLineFailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptHoVcFailureHistoryGet(AtSdhChannel channel)
    {
    uint32 alarms = 0;
    tSurAuId auId;
    tSortList list;
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);
    list = FmAuFailHisGet(surDevice, &auId);
    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptAuFailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptTu3FailureHistoryGet(AtSdhChannel channel)
    {
    uint32 alarms = 0;
    tSurTu3Id tu3Id;
    tSortList list;
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);
    list = FmTu3FailHisGet(surDevice, &tu3Id);
    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptAuFailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptTuFailureHistoryGet(AtSdhChannel channel)
    {
    uint32 alarms = 0;
    tSurTuId tuId;
    tSortList list;
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);
    list = FmTuFailHisGet(surDevice, &tuId);
    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptTuFailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptPdhDe1FailureHistoryGet(AtPdhChannel channel)
    {
    uint32 alarms = 0;
    tSurDe1Id de1Id;
    tSortList list;
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);
    list = FmDe1FailHisGet(surDevice, &de1Id);
    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptPdhDe1FailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptPdhDe3FailureHistoryGet(AtPdhChannel channel)
    {
    uint32 alarms = 0;
    tSurDe3Id de3Id;
    tSortList list;
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);
    list = FmDe3FailHisGet(surDevice, &de3Id);
    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptPdhDe3FailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptPwFailureHistoryGet(AtPw pw)
    {
    uint32 alarms = 0;
    tSortList list;
    dword listSize;
    uint32 listIndex;
    tFmFailInfo failInfo;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    list = FmPwFailHisGet(surDevice, &pwId);
    ListSize(list, &listSize);

    for (listIndex = 0; listIndex < listSize; listIndex++)
        {
        ListGet(list, listIndex, &failInfo, sizeof(tFmFailInfo));

        /* No failure */
        if (failInfo.status == 0)
            continue;

        alarms |= AdaptPwFailureTypeGet(&failInfo);
        }

    return alarms;
    }

uint32 AtSurAdaptLineFailureHistoryClear(AtSdhLine line)
    {
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    uint32 alarms = AtSurAdaptLineFailureHistoryGet(line);
    FmLineFailHisClear(surDevice, &lineId, 1);
    return alarms;
    }

uint32 AtSurAdaptHoVcFailureHistoryClear(AtSdhChannel channel)
    {
    uint32 alarms;
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    alarms = AtSurAdaptHoVcFailureHistoryGet(channel);
    FmAuFailHisClear(surDevice, &auId, 1);
    return alarms;
    }

uint32 AtSurAdaptTu3FailureHistoryClear(AtSdhChannel channel)
    {
    uint32 alarms;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    alarms = AtSurAdaptTu3FailureHistoryGet(channel);
    FmTu3FailHisClear(surDevice, &tu3Id, 1);
    return alarms;
    }

uint32 AtSurAdaptTuFailureHistoryClear(AtSdhChannel channel)
    {
    uint32 alarms;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    alarms = AtSurAdaptTuFailureHistoryGet(channel);
    FmTuFailHisClear(surDevice, &tuId, 1);
    return alarms;
    }

uint32 AtSurAdaptPdhDe3FailureHistoryClear(AtPdhChannel channel)
    {
    uint32 alarms;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    alarms = AtSurAdaptPdhDe3FailureHistoryGet(channel);
    FmDe3FailHisClear(surDevice, &de3Id, 1);
    return alarms;
    }

uint32 AtSurAdaptPdhDe1FailureHistoryClear(AtPdhChannel channel)
    {
    uint32 alarms;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    alarms = AtSurAdaptPdhDe1FailureHistoryGet(channel);
    FmDe1FailHisClear(surDevice, &de1Id, 1);
    return alarms;
    }

uint32 AtSurAdaptPwFailureHistoryClear(AtPw pw)
    {
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);
    uint32 alarms = AtSurAdaptPwFailureHistoryGet(pw);
    FmLineFailHisClear(surDevice, &pwId, 1);
    return alarms;
    }

static ePmRegType PmRegType(eAtSurAdaptPmRegType pmRegType)
    {
    return (ePmRegType)pmRegType;
    }

static ePmLineParm LinePmParamType(eAtSurEngineSdhLinePmParam pmType)
    {
    switch (pmType)
        {
        case cAtSurEngineSdhLinePmParamSefsS  :
            return cPmSecSefs;
        case cAtSurEngineSdhLinePmParamCvS    :
            return cPmSecCv;
        case cAtSurEngineSdhLinePmParamEsS    :
            return cPmSecEs;
        case cAtSurEngineSdhLinePmParamSesS   :
            return cPmSecSes;
        case cAtSurEngineSdhLinePmParamCvL    :
            return cPmLineCv;
        case cAtSurEngineSdhLinePmParamEsL    :
            return cPmLineEs;
        case cAtSurEngineSdhLinePmParamSesL   :
            return cPmLineSes;
        case cAtSurEngineSdhLinePmParamUasL   :
            return cPmLineUas;
        case cAtSurEngineSdhLinePmParamFcL    :
            return cPmLineFc;
        case cAtSurEngineSdhLinePmParamCvLfe  :
            return cPmLineCvLfe;
        case cAtSurEngineSdhLinePmParamEsLfe  :
            return cPmLineEsLfe;
        case cAtSurEngineSdhLinePmParamSesLfe :
            return cPmLineSesLfe;
        case cAtSurEngineSdhLinePmParamUasLfe :
            return cPmLineUasLfe;
        case cAtSurEngineSdhLinePmParamFcLfe  :
            return cPmLineFcLfe;
        default:
            return 0;
        }
    }

static ePmAuParm AuPmParamType(eAtSurEngineSdhPathPmParam pmType)
    {
    switch (pmType)
        {
        case cAtSurEngineSdhPathPmParamPpjcPdet:
            return cPmStsPpjcPdet;
        case cAtSurEngineSdhPathPmParamNpjcPdet:
            return cPmStsNpjcPdet;
        case cAtSurEngineSdhPathPmParamPpjcPgen:
            return cPmStsPpjcPgen;
        case cAtSurEngineSdhPathPmParamNpjcPgen:
            return cPmStsNpjcPgen;
        case cAtSurEngineSdhPathPmParamPjcDiff:
            return cPmStsPjcDiff;
        case cAtSurEngineSdhPathPmParamPjcsPdet:
            return cPmStsPjcsPdet;
        case cAtSurEngineSdhPathPmParamPjcsPgen:
            return cPmStsPjcsPgen;
        case cAtSurEngineSdhPathPmParamCvP:
            return cPmStsCv;
        case cAtSurEngineSdhPathPmParamEsP:
            return cPmStsEs;
        case cAtSurEngineSdhPathPmParamSesP:
            return cPmStsSes;
        case cAtSurEngineSdhPathPmParamUasP:
            return cPmStsUas;
        case cAtSurEngineSdhPathPmParamFcP:
            return cPmStsFc;
        case cAtSurEngineSdhPathPmParamCvPfe:
            return cPmStsCvPfe;
        case cAtSurEngineSdhPathPmParamEsPfe:
            return cPmStsEsPfe;
        case cAtSurEngineSdhPathPmParamSesPfe:
            return cPmStsSesPfe;
        case cAtSurEngineSdhPathPmParamUasPfe:
            return cPmStsUasPfe;
        case cAtSurEngineSdhPathPmParamFcPfe:
            return cPmStsFcPfe;
        default:
            return 0;
        }
    }

static ePmTuParm TuPmParamType(eAtSurEngineSdhPathPmParam pmType)
    {
    switch (pmType)
        {
        case cAtSurEngineSdhPathPmParamPpjcPdet:
            return cPmVtPpjcVdet;
        case cAtSurEngineSdhPathPmParamNpjcPdet:
            return cPmVtNpjcVdet;
        case cAtSurEngineSdhPathPmParamPpjcPgen:
            return cPmVtPpjcVgen;
        case cAtSurEngineSdhPathPmParamNpjcPgen:
            return cPmVtNpjcVgen;
        case cAtSurEngineSdhPathPmParamPjcDiff:
            return cPmVtPjcDiff;
        case cAtSurEngineSdhPathPmParamPjcsPdet:
            return cPmVtPjcsVdet;
        case cAtSurEngineSdhPathPmParamPjcsPgen:
            return cPmVtPjcsVgen;
        case cAtSurEngineSdhPathPmParamCvP:
            return cPmVtCv;
        case cAtSurEngineSdhPathPmParamEsP:
            return cPmVtEs;
        case cAtSurEngineSdhPathPmParamSesP:
            return cPmVtSes;
        case cAtSurEngineSdhPathPmParamUasP:
            return cPmVtUas;
        case cAtSurEngineSdhPathPmParamFcP:
            return cPmVtFc;
        case cAtSurEngineSdhPathPmParamCvPfe:
            return cPmVtCvVfe;
        case cAtSurEngineSdhPathPmParamEsPfe:
            return cPmVtEsVfe;
        case cAtSurEngineSdhPathPmParamSesPfe:
            return cPmVtSesVfe;
        case cAtSurEngineSdhPathPmParamUasPfe:
            return cPmVtUasVfe;
        case cAtSurEngineSdhPathPmParamFcPfe:
            return cPmVtFcVfe;
        default:
            return 0;
        }
    }

static ePmDe1Parm De1PmParamType(eAtSurEnginePdhDe1PmParam pmType)
    {
    switch (pmType)
        {
        case cAtSurEnginePdhDe1PmParamCvL:
            return cPmDe1CvL;
        case cAtSurEnginePdhDe1PmParamEsL:
            return cPmDe1EsL;
        case cAtSurEnginePdhDe1PmParamSesL:
            return cPmDe1SesL;
        case cAtSurEnginePdhDe1PmParamLossL:
            return cPmDe1LossL;
        case cAtSurEnginePdhDe1PmParamEsLfe:
            return cPmDe1EsLfe;
        case cAtSurEnginePdhDe1PmParamCvP:
            return cPmDe1CvP;
        case cAtSurEnginePdhDe1PmParamEsP:
            return cPmDe1EsP;
        case cAtSurEnginePdhDe1PmParamSesP:
            return cPmDe1SesP;
        case cAtSurEnginePdhDe1PmParamAissP:
            return cPmDe1AissP;
        case cAtSurEnginePdhDe1PmParamSasP:
            return cPmDe1SasP;
        case cAtSurEnginePdhDe1PmParamCssP:
            return cPmDe1CssP;
        case cAtSurEnginePdhDe1PmParamUasP:
            return cPmDe1UasP;
        case cAtSurEnginePdhDe1PmParamSefsPfe:
            return cPmDe1SefsPfe;
        case cAtSurEnginePdhDe1PmParamEsPfe:
            return cPmDe1EsPfe;
        case cAtSurEnginePdhDe1PmParamSesPfe:
            return cPmDe1SesPfe;
        case cAtSurEnginePdhDe1PmParamCssPfe:
            return cPmDe1CssPfe;
        case cAtSurEnginePdhDe1PmParamFcPfe:
            return cPmDe1FcPfe;
        case cAtSurEnginePdhDe1PmParamUasPfe:
            return cPmDe1UasPfe;
        default:
            return 0;
        }
    }

static ePmDe3Parm De3PmParamType(eAtSurEnginePdhDe3PmParam pmType)
    {
    switch (pmType)
        {
        case cAtSurEnginePdhDe3PmParamCvL     :
            return cPmDe3CvL;
        case cAtSurEnginePdhDe3PmParamEsL     :
            return cPmDe3EsL;
        case cAtSurEnginePdhDe3PmParamSesL    :
            return cPmDe3SesL;
        case cAtSurEnginePdhDe3PmParamLossL   :
            return cPmDe3LossL;
        case cAtSurEnginePdhDe3PmParamCvpP    :
            return cPmDe3CvpP;
        case cAtSurEnginePdhDe3PmParamCvcpP   :
            return cPmDe3CvcpP;
        case cAtSurEnginePdhDe3PmParamEspP    :
            return cPmDe3EspP;
        case cAtSurEnginePdhDe3PmParamEscpP   :
            return cPmDe3EscpP;
        case cAtSurEnginePdhDe3PmParamSespP   :
            return cPmDe3SespP;
        case cAtSurEnginePdhDe3PmParamSescpP  :
            return cPmDe3SescpP;
        case cAtSurEnginePdhDe3PmParamSasP    :
            return cPmDe3SasP;
        case cAtSurEnginePdhDe3PmParamAissP   :
            return cPmDe3AissP;
        case cAtSurEnginePdhDe3PmParamUaspP   :
            return cPmDe3UaspP;
        case cAtSurEnginePdhDe3PmParamUascpP  :
            return cPmDe3UascpP;
        case cAtSurEnginePdhDe3PmParamCvcpPfe :
            return cPmDe3CvcpPfe;
        case cAtSurEnginePdhDe3PmParamEscpPfe :
            return cPmDe3EscpPfe;
        case cAtSurEnginePdhDe3PmParamSescpPfe:
            return cPmDe3SescpPfe;
        case cAtSurEnginePdhDe3PmParamSascpPfe:
            return cPmDe3SascpPfe;
        case cAtSurEnginePdhDe3PmParamUascpPfe:
            return cPmDe3UascpPfe;
        default:
            return 0;
        }
    }

static ePmPwParm PwPmParamType(eAtSurEnginePwPmParam pmType)
    {
    switch ((uint32)pmType)
        {
        case cAtSurEnginePwPmParamEs:
            return cPmPwEs;
        case cAtSurEnginePwPmParamSes:
            return cPmPwSes;
        case cAtSurEnginePwPmParamUas:
            return cPmPwUas;
        default:
            return 0;
        }
    }

uint32 AtSurAdaptLinePmValueGet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType)
    {
    tPmReg pmReg;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    eSurRet ret = PmLineRegGet(surDevice, &lineId, LinePmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

eBool AtSurAdaptLinePmIsValid(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType)
    {
    tPmReg pmReg;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    eSurRet ret = PmLineRegGet(surDevice, &lineId, LinePmParamType(pmType), PmRegType(pmRegType), &pmReg);

    /* TODO: check it */
    return (ret == cSurOk) ? (!pmReg.invalid) : cAtFalse;
    }

void AtSurAdaptLinePmReset(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType)
    {
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    PmLineRegReset(surDevice, &lineId, LinePmParamType(pmType), PmRegType(pmRegType));
    }

eAtRet AtSurAdaptLinePeriodThresholdSet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType, uint32 threshold)
    {
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    return (PmLineRegThresSet(surDevice, &lineId, LinePmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

uint32 AtSurAdaptLinePeriodThresholdGet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType)
    {
    dword threshold;
    uint16 lineId = (uint16)(AtChannelIdGet((AtChannel)line) + 1);
    eSurRet ret = PmLineRegThresGet(surDevice, &lineId, LinePmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : 0;
    }

uint32 AtSurAdaptHoVcPmValueGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    tPmReg pmReg;
    tSurAuId auId;
    eSurRet ret;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    ret = PmAuRegGet(surDevice, &auId, AuPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

uint32 AtSurAdaptTu3PmValueGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    ret = PmTu3RegGet(surDevice, &tu3Id, AuPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

uint32 AtSurAdaptTuPmValueGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    ret = PmTuRegGet(surDevice, &tuId, TuPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

uint32 AtSurAdaptPdhDe1PmValueGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    ret = PmDe1RegGet(surDevice, &de1Id, De1PmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

uint32 AtSurAdaptPdhDe3PmValueGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    ret = PmDe3RegGet(surDevice, &de3Id, De3PmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

uint32 AtSurAdaptPwPmValueGet(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    ret = PmPwRegGet(surDevice, &pwId, PwPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? pmReg.value : 0;
    }

void AtSurAdaptHoVcPmReset(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    PmAuRegReset(surDevice, &auId, AuPmParamType(pmType), PmRegType(pmRegType));
    }

void AtSurAdaptTu3PmReset(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    PmTu3RegReset(surDevice, &tu3Id, AuPmParamType(pmType), PmRegType(pmRegType));
    }

void AtSurAdaptTuPmReset(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    PmTuRegReset(surDevice, &tuId, TuPmParamType(pmType), PmRegType(pmRegType));
    }

void AtSurAdaptPdhDe1PmReset(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType)
    {
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    PmDe1RegReset(surDevice, &de1Id, De1PmParamType(pmType), PmRegType(pmRegType));
    }

void AtSurAdaptPdhDe3PmReset(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType)
    {
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    PmDe3RegReset(surDevice, &de3Id, De3PmParamType(pmType), PmRegType(pmRegType));
    }

void AtSurAdaptPwPmReset(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType)
    {
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    PmPwRegReset(surDevice, &pwId, PwPmParamType(pmType), PmRegType(pmRegType));
    }

eBool AtSurAdaptHoVcPmIsValid(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    tPmReg pmReg;
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    PmAuRegGet(surDevice, &auId, AuPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return !pmReg.invalid;
    }

eBool AtSurAdaptTu3PmIsValid(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    ret = PmTu3RegGet(surDevice, &tu3Id, AuPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? (!pmReg.invalid) : cAtFalse;
    }

eBool AtSurAdaptTuPmIsValid(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    ret = PmTuRegGet(surDevice, &tuId, TuPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? (!pmReg.invalid) : cAtFalse;
    }

eBool AtSurAdaptPdhDe1PmIsValid(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    ret = PmDe1RegGet(surDevice, &de1Id, De1PmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? (!pmReg.invalid) : cAtFalse;
    }

eBool AtSurAdaptPdhDe3PmIsValid(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    ret = PmDe3RegGet(surDevice, &de3Id, De3PmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? (!pmReg.invalid) : cAtFalse;
    }

eBool AtSurAdaptPwPmIsValid(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType)
    {
    eSurRet ret;
    tPmReg pmReg;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    ret = PmPwRegGet(surDevice, &pwId, PwPmParamType(pmType), PmRegType(pmRegType), &pmReg);
    return (ret == cSurOk) ? (!pmReg.invalid) : cAtFalse;
    }

eAtRet AtSurAdaptHoVcPmThresholdSet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType, uint32 threshold)
    {
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    return (PmAuRegThresSet(surDevice, &auId, AuPmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

eAtRet AtSurAdaptTu3PmThresholdSet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType, uint32 threshold)
    {
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    return (PmTu3RegThresSet(surDevice, &tu3Id, AuPmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

eAtRet AtSurAdaptTuPmThresholdSet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType, uint32 threshold)
    {
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    return (PmTuRegThresSet(surDevice, &tuId, TuPmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

eAtRet AtSurAdaptPdhDe1PmThresholdSet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType, uint32 threshold)
    {
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    return (PmDe1RegThresSet(surDevice, &de1Id, De1PmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

eAtRet AtSurAdaptPdhDe3PmThresholdSet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType, uint32 threshold)
    {
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    return (PmDe3RegThresSet(surDevice, &de3Id, De3PmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

eAtRet AtSurAdaptPwPmThresholdSet(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType, uint32 threshold)
    {
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    return (PmPwRegThresSet(surDevice, &pwId, PwPmParamType(pmType), PmRegType(pmRegType), threshold) == cSurOk) ? cAtOk : cAtError;
    }

uint32 AtSurAdaptHoVcPmThresholdGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    dword threshold;
    tSurAuId auId;

    AtOsalMemInit(&auId, 0, sizeof(tSurAuId));
    HoVcId(channel, &auId);

    ret = PmAuRegThresGet(surDevice, &auId, AuPmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : 0;
    }

uint32 AtSurAdaptTu3PmThresholdGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    dword threshold;
    tSurTu3Id tu3Id;

    AtOsalMemInit(&tu3Id, 0, sizeof(tSurTu3Id));
    Tu3Id(channel, &tu3Id);

    ret = PmTu3RegThresGet(surDevice, &tu3Id, AuPmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : cAtFalse;
    }

uint32 AtSurAdaptTuPmThresholdGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType)
    {
    eSurRet ret;
    dword threshold;
    tSurTuId tuId;

    AtOsalMemInit(&tuId, 0, sizeof(tSurTuId));
    TuId(channel, &tuId);

    ret = PmTuRegThresGet(surDevice, &tuId, TuPmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : cAtFalse;
    }

uint32 AtSurAdaptPdhDe1PmThresholdGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType)
    {
    eSurRet ret;
    dword threshold;
    tSurDe1Id de1Id;

    AtOsalMemInit(&de1Id, 0, sizeof(tSurDe1Id));
    De1Id(channel, &de1Id);

    ret = PmDe1RegThresGet(surDevice, &de1Id, De1PmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : cAtFalse;
    }

uint32 AtSurAdaptPdhDe3PmThresholdGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType)
    {
    eSurRet ret;
    dword threshold;
    tSurDe3Id de3Id;

    AtOsalMemInit(&de3Id, 0, sizeof(tSurDe3Id));
    De3Id(channel, &de3Id);

    ret = PmDe3RegThresGet(surDevice, &de3Id, De3PmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : cAtFalse;
    }

uint32 AtSurAdaptPwPmThresholdGet(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType)
    {
    eSurRet ret;
    dword threshold;
    tSurPwId pwId = (tSurPwId)(AtChannelIdGet((AtChannel)pw) + 1);

    ret = PmPwRegThresGet(surDevice, &pwId, PwPmParamType(pmType), PmRegType(pmRegType), &threshold);
    return (ret == cSurOk) ? (uint32)threshold : cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengsts.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code STS path Transmisstion 
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mStsKval(pSurInfo, stsRate, kVal, valid)
Purpose   : This macro is used to get K threshold of STS channel
Inputs    : pSurInfo                 - device database
            stsRate                  - STS rate
outputs   : kVal                     - K threshold
            valid                    - true to indicate that K threshold is 
                                       valid or not
------------------------------------------------------------------------------*/
#define mStsKval(pSurInfo, stsRate, kVal, valid)                               \
    valid = true;                                                              \
    switch (stsRate)                                                           \
        {                                                                      \
        case cSurSts1:                                                         \
            kVal = (pSurInfo)->pDb->mastConf.kThres.sts1;                      \
            break;                                                             \
        case cSurSts3c:                                                        \
            kVal = (pSurInfo)->pDb->mastConf.kThres.sts3c;                     \
            break;                                                             \
        case cSurSts12c:                                                       \
            kVal = (pSurInfo)->pDb->mastConf.kThres.sts12c;                    \
            break;                                                             \
        case cSurSts48c:                                                       \
            kVal = (pSurInfo)->pDb->mastConf.kThres.sts48c;                    \
            break;                                                             \
        case cSurSts192c:                                                      \
            kVal = (pSurInfo)->pDb->mastConf.kThres.sts192c;                   \
            break;                                                             \
        case cSurSts768c:                                                      \
            kVal = (pSurInfo)->pDb->mastConf.kThres.sts768c;                   \
            break;                                                             \
                                                                               \
        /* Not support */                                                      \
        default:                                                               \
            valid = false;                                                     \
            break;                                                             \
        }

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : void SurStsDefUpd(tSurStsInfo *pStsInfo, tSurStsDef *pStsDef)

Purpose      : This function is used to update STS defect database

Inputs       : pStsDef               - STS defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurStsDefUpd(tSurStsInfo *pStsInfo, tSurStsDef *pStsDef)
    {
    switch (pStsDef->defect)
        {
        /* STS Path Alarm Indication Signal */
        case cFmStsAis:
        	if (pStsInfo->status.aisp.status != pStsDef->status)
        		{
				pStsInfo->status.aisp.status = pStsDef->status;
				pStsInfo->status.aisp.time   = pStsDef->time;
        		}
            
            /* Clear LOP defect */
            if ((pStsDef->status == true) && (pStsInfo->status.lopp.status == true))
                {
                pStsInfo->status.lopp.status = false;
                pStsInfo->status.lopp.time   = dCurTime;
                }
            break;
            
        /* STS path Loss Of Pointer */
        case cFmStsLop:
        	if (pStsInfo->status.lopp.status != pStsDef->status)
        		{
				pStsInfo->status.lopp.status = pStsDef->status;
				pStsInfo->status.lopp.time   = pStsDef->time;
        		}
            
            /* Clear AIS-P defect */
            if ((pStsDef->status == true) && (pStsInfo->status.aisp.status == true))
                {
                pStsInfo->status.aisp.status = false;
                pStsInfo->status.aisp.time   = dCurTime;
                }
            break;
        
        /* STS Path Unequipped */
        case cFmStsUneq:
        	if (pStsInfo->status.uneqp.status != pStsDef->status)
        		{
				pStsInfo->status.uneqp.status = pStsDef->status;
				pStsInfo->status.uneqp.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path Trace Identifier Mismatch */
        case cFmStsTim:
        	if (pStsInfo->status.timp.status != pStsDef->status)
        		{
				pStsInfo->status.timp.status = pStsDef->status;
				pStsInfo->status.timp.time   = pStsDef->time;
        		}
            break;
            
        /* STS Path Payload Label Mismatch */
        case cFmStsPlm:
        	if (pStsInfo->status.plmp.status != pStsDef->status)
        		{
				pStsInfo->status.plmp.status = pStsDef->status;
				pStsInfo->status.plmp.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path one-bit Remote Failure Indication. */
        case cFmStsRdi:
        	if (pStsInfo->status.rdip.status != pStsDef->status)
        		{
				pStsInfo->status.rdip.status = pStsDef->status;
				pStsInfo->status.rdip.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path Server Enhanced Remote Failure Indication. */
        case cFmStsErdiS:
        	if (pStsInfo->status.erdipS.status != pStsDef->status)
        		{
				pStsInfo->status.erdipS.status = pStsDef->status;
				pStsInfo->status.erdipS.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path Connectivity Enhanced Remote Failure Indication. */
        case cFmStsErdiC:
        	if (pStsInfo->status.erdipC.status != pStsDef->status)
        		{
				pStsInfo->status.erdipC.status = pStsDef->status;
				pStsInfo->status.erdipC.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path Payload Enhanced Remote Failure Indication */
        case cFmStsErdiP:
        	if (pStsInfo->status.erdipP.status != pStsDef->status)
        		{
				pStsInfo->status.erdipP.status = pStsDef->status;
				pStsInfo->status.erdipP.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path BER-based Signal Failure */
        case cFmStsBerSf:
        	if (pStsInfo->status.pathBerSf.status != pStsDef->status)
        		{
				pStsInfo->status.pathBerSf.status = pStsDef->status;
				pStsInfo->status.pathBerSf.time   = pStsDef->time;
        		}
            break;
        
        /* STS Path BER-based Signal Degrade */
        case cFmStsBerSd:
        	if (pStsInfo->status.pathBerSd.status != pStsDef->status)
        		{
				pStsInfo->status.pathBerSd.status = pStsDef->status;
				pStsInfo->status.pathBerSd.time   = pStsDef->time;
        		}
            break;

        case cFmStsLbit:
        	if (pStsInfo->status.lBit.status != pStsDef->status)
        		{
				pStsInfo->status.lBit.status = pStsDef->status;
				pStsInfo->status.lBit.time   = pStsDef->time;
        		}
            break;
        case cFmStsRbit:
        	if (pStsInfo->status.rBit.status != pStsDef->status)
        		{
				pStsInfo->status.rBit.status = pStsDef->status;
				pStsInfo->status.rBit.time   = pStsDef->time;
        		}
            break;
        case cFmStsMbit:
        	if (pStsInfo->status.mBit.status != pStsDef->status)
        		{
				pStsInfo->status.mBit.status = pStsDef->status;
				pStsInfo->status.mBit.time   = pStsDef->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void StsAllDefUpd(tSurInfo        *pSurInfo,
                                         const tSurStsId *pStsId)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pStsId                - STS ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void StsAllDefUpd(tSurInfo        *pSurInfo,
                              const tSurStsId *pStsId)
    {
    bool          blNewStat;
    tSurStsDef    def;
    tSurStsInfo  *pStsInfo;
    dword         dDefStat;

    pStsInfo = NULL;

    /* Get STS information */
    mSurStsInfo(pSurInfo, pStsId, pStsInfo);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pStsId, &(def.id), sizeof(tSurStsId));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurSts, pStsId, &dDefStat);
    
    /* STS Path Alarm Indication Signal */
    blNewStat = mBitStat(dDefStat, cSurStsAisMask);
    if (blNewStat != pStsInfo->status.aisp.status)
        {
        def.defect = cFmStsAis;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }
        
    /* STS path Loss Of Pointer */
    blNewStat = mBitStat(dDefStat, cSurStsLopMask);
    if (blNewStat != pStsInfo->status.lopp.status)
        {
        def.defect = cFmStsLop;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }
    
    /* STS Path Unequipped */
    blNewStat = mBitStat(dDefStat, cSurStsUneqMask);
    if (blNewStat != pStsInfo->status.uneqp.status)
        {
        def.defect = cFmStsUneq;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }
    
    /* STS Path Trace Identifier Mismatch */
    blNewStat = mBitStat(dDefStat, cSurStsTimMask);
    if (blNewStat != pStsInfo->status.timp.status)
        {
        def.defect = cFmStsTim;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }
        
    /* STS Path Payload Label Mismatch */
    blNewStat = mBitStat(dDefStat, cSurStsPlmMask);
    if (blNewStat != pStsInfo->status.plmp.status)
        {
        def.defect = cFmStsPlm;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }
        
    /* STS Path one-bit Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurStsRdiMask);
    if (blNewStat != pStsInfo->status.rdip.status)
        {
        def.defect = cFmStsRdi;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* STS Path Server Enhanced Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurStsErdiSMask);
    if (blNewStat != pStsInfo->status.erdipS.status)
        {
        def.defect = cFmStsErdiS;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* STS Path Connectivity Enhanced Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurStsErdiCMask);
    if (blNewStat != pStsInfo->status.erdipC.status)
        {
        def.defect = cFmStsErdiC;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* STS Path Payload Enhanced Remote Defect Indication */
    blNewStat = mBitStat(dDefStat, cSurStsErdiPMask);
    if (blNewStat != pStsInfo->status.erdipP.status)
        {
        def.defect = cFmStsErdiP;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* STS Path BER-based Signal Failure */
    blNewStat = mBitStat(dDefStat, cSurStsBerSfMask);
    if (blNewStat != pStsInfo->status.pathBerSf.status)
        {
        def.defect = cFmStsBerSf;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* STS Path BER-based Signal Degrade */
    blNewStat = mBitStat(dDefStat, cSurStsBerSdMask);
    if (blNewStat != pStsInfo->status.pathBerSd.status)
        {
        def.defect = cFmStsBerSd;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* L bit error, only used for PW */
    blNewStat = mBitStat(dDefStat, cSurStsLbitMask);
    if (blNewStat != pStsInfo->status.lBit.status)
        {
        def.defect = cFmStsLbit;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* R bit error, only used for PW */
    blNewStat = mBitStat(dDefStat, cSurStsRbitMask);
    if (blNewStat != pStsInfo->status.rBit.status)
        {
        def.defect = cFmStsRbit;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }

    /* M bit error, only used for PW */
    blNewStat = mBitStat(dDefStat, cSurStsMbitMask);
    if (blNewStat != pStsInfo->status.mBit.status)
        {
        def.defect = cFmStsMbit;
        def.status = blNewStat;
        SurStsDefUpd(pStsInfo, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void StsAllFailUpd(tSurInfo        *pSurInfo,
                                          const tSurStsId *pStsId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pStsId                - STS ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void StsAllFailUpd(tSurInfo *pSurInfo, const tSurStsId *pStsId)
    {
    tSurTrblProf  *pTrblProf;
    tSurStsFail   *pFailInd;
    tSortList      failHis;
    tSurStsInfo   *pStsInfo;
    tSurLineInfo  *pLineInfo;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;

    pStsInfo = NULL;

    /* Get line & STS information */
    mSurStsInfo(pSurInfo, pStsId, pStsInfo);
    mSurLineInfo(pSurInfo, &(pStsId->line), pLineInfo);
    
    /* Do not process any STS failure if either LOS, LOF failure is declared at 
       section/line level */
    if ((pLineInfo->failure.los.status  == true) ||
        (pLineInfo->failure.lof.status  == true))
        {
        return;
        }
    
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pStsInfo->failure);
    failHis   = pStsInfo->failHis;
    
    /* STS AIS-P failure */
    mFailIndUpd(pSurInfo, pStsInfo, aisp, aisp, blFailChg);
    if (blFailChg == true)
        {
        blNotf = true;
        if ((pLineInfo->failure.aisl.status == true) ||
            (pStsInfo->msgInh.sts           == true) ||
            (pStsInfo->msgInh.aisp          == true))
            {
            blNotf = false;
            }
        mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->aisp, failHis, failInfo, blNotf, cFmStsAis, pTrblProf->aisp);
        
        /* Update failure count parameter */
        if (pStsInfo->failure.aisp.status == true)
            {
            pStsInfo->parm.fcP.curSec += 1;
            }
        
        /* Clear UNEQ-P failure if it is present */
        if ((pStsInfo->failure.aisp.status  == true) &&
            (pStsInfo->failure.uneqp.status == true))
            {
            pStsInfo->failure.uneqp.status = false;
            pStsInfo->failure.uneqp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->msgInh.sts           == true) ||
                (pStsInfo->msgInh.uneqp         == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->uneqp, failHis, failInfo, blNotf, cFmStsUneq, pTrblProf->uneqp);
            }
            
        /* Clear PLM-P failure if it is present */
        if ((pStsInfo->failure.aisp.status == true) &&
            (pStsInfo->failure.plmp.status == true))
            {
            pStsInfo->failure.plmp.status = false;
            pStsInfo->failure.plmp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pStsInfo->failure.timp.status  == true) ||
                (pStsInfo->msgInh.sts           == true) ||
                (pStsInfo->msgInh.plmp          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->plmp, failHis, failInfo, blNotf, cFmStsPlm, pTrblProf->plmp);
            }
        }
        
    /* STS LOP-P failure */
    mFailIndUpd(pSurInfo, pStsInfo, lopp, lopp, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pStsInfo->msgInh.sts  == false) && 
                  (pStsInfo->msgInh.lopp == false));
        mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->lopp, failHis, failInfo, blNotf, cFmStsLop, pTrblProf->lopp);
        
        /* Update failure count parameter */
        if (pStsInfo->failure.lopp.status == true)
            {
            pStsInfo->parm.fcP.curSec += 1;
            }
        
        /* Clear UNEQ-P failure if it is present */
        if ((pStsInfo->failure.lopp.status  == true) &&
            (pStsInfo->failure.uneqp.status == true))
            {
            pStsInfo->failure.uneqp.status = false;
            pStsInfo->failure.uneqp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->msgInh.sts           == true) ||
                (pStsInfo->msgInh.uneqp         == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->uneqp, failHis, failInfo, blNotf, cFmStsUneq, pTrblProf->uneqp);
            }
            
        /* Clear PLM-P failure if it is present */
        if ((pStsInfo->failure.lopp.status == true) &&
            (pStsInfo->failure.plmp.status == true))
            {
            pStsInfo->failure.plmp.status = false;
            pStsInfo->failure.plmp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                (pStsInfo->failure.timp.status  == true) ||
                (pStsInfo->msgInh.sts           == true) ||
                (pStsInfo->msgInh.plmp          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->plmp, failHis, failInfo, blNotf, cFmStsPlm, pTrblProf->plmp);
            }
        }
    
    /* Only declare/clear UNEQ-P or PLM-P failure when AIS-P or LOP-P failure is 
       not present */
    if ((pStsInfo->failure.aisp.status == false) &&
        (pStsInfo->failure.lopp.status == false))
        {
        /* STS UNEQ-P failure */
        mFailIndUpd(pSurInfo, pStsInfo, uneqp, uneqp, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pStsInfo->msgInh.sts   == false) && 
                      (pStsInfo->msgInh.uneqp == false));
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->uneqp, failHis, failInfo, blNotf, cFmStsUneq, pTrblProf->uneqp);
            
            /* Update failure count parameter */
            if ((pStsInfo->conf.erdiEn          == true) && 
                (pStsInfo->failure.uneqp.status == true))
                {
                pStsInfo->parm.fcP.curSec += 1;
                }
            }
           
        /* STS PLM-P failure */
        mFailIndUpd(pSurInfo, pStsInfo, plmp, plmp, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pStsInfo->failure.uneqp.status == true) ||
                /*(pStsInfo->failure.timp.status  == true) ||*/
                (pStsInfo->msgInh.sts           == true) ||
                (pStsInfo->msgInh.plmp          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->plmp, failHis, failInfo, blNotf, cFmStsPlm, pTrblProf->plmp);
            }
        }
    
    /* STS TIM-P failure */
    mFailIndUpd(pSurInfo, pStsInfo, timp, timp, blFailChg);
    if (blFailChg == true)
        {
        blNotf = true;
        if ((pLineInfo->failure.tims.status == true) ||
            (pStsInfo->failure.uneqp.status == true) ||
            (pStsInfo->msgInh.sts           == true) ||
            (pStsInfo->msgInh.timp          == true))
            {
            blNotf = false;
            }
        mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->timp, failHis, failInfo, blNotf, cFmStsTim, pTrblProf->timp);
        
        /* Update failure count parameter */
        if ((pStsInfo->conf.erdiEn         == true) && 
            (pStsInfo->failure.timp.status == true))
            {
            pStsInfo->parm.fcP.curSec += 1;
            }
        }
        
    /* If ERDI-P is not supported, declare/clear 1-bit RFI-P */
    if (pStsInfo->conf.erdiEn == false)
        {
        /* STS Path one-bit Remote Failure Indication */
        mFailIndUpd(pSurInfo, pStsInfo, rdip, rfip, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if((pLineInfo->failure.rfil.status == true) ||
               (pStsInfo->msgInh.sts           == true) || 
               (pStsInfo->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->rfip, failHis, failInfo, blNotf, cFmStsRfi, pTrblProf->rfip);
            
            /* Update far-end failure count parameter */
            if (pStsInfo->failure.rfip.status == true)
                {
                pStsInfo->parm.fcPfe.curSec += 1;
                }
            }
        }
    
    /* If ERDI-P is supported, declare/clear 3-bits RFI-P */
    else
        {
        /* STS Path Server Enhanced Remote Failure Indication */
        mFailIndUpd(pSurInfo, pStsInfo, erdipS, erfipS, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if((pLineInfo->failure.rfil.status == true) ||
               (pStsInfo->msgInh.sts           == true) || 
               (pStsInfo->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->erfipS, failHis, failInfo, blNotf, cFmStsErfiS, pTrblProf->erfipS);
            
            /* Update far-end failure count parameter */
            if (pStsInfo->failure.erfipS.status == true)
                {
                pStsInfo->parm.fcPfe.curSec += 1;
                }
            }
        
        /* STS Path Connectivity Enhanced Remote Failure Indication */
        mFailIndUpd(pSurInfo, pStsInfo, erdipC, erfipC, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if((pLineInfo->failure.rfil.status == true) ||
               (pStsInfo->msgInh.sts           == true) || 
               (pStsInfo->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->erfipC, failHis, failInfo, blNotf, cFmStsErfiC, pTrblProf->erfipC);
            
            /* Update far-end failure count parameter */
            if (pStsInfo->failure.erfipC.status == true)
                {
                pStsInfo->parm.fcPfe.curSec += 1;
                }
            }

        /* STS Path Payload Enhanced Remote Failure Indication */
        mFailIndUpd(pSurInfo, pStsInfo, erdipP, erfipP, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if((pLineInfo->failure.rfil.status == true) ||
               (pStsInfo->msgInh.sts           == true) || 
               (pStsInfo->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->erfipP, failHis, failInfo, blNotf, cFmStsErfiP, pTrblProf->erfipP);
            }
        }

    /* STS Path BER-based Signal Failure */
    mFailIndUpd(pSurInfo, pStsInfo, pathBerSf, pathBerSf, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pStsInfo->msgInh.sts       == false) && 
                  (pStsInfo->msgInh.pathBerSf == false));
        mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->pathBerSf, failHis, failInfo, blNotf, cFmStsBerSf, pTrblProf->pathBerSf);
        
        /* Terminate BER-SD failure if it was already declared */
        if ((pStsInfo->failure.pathBerSf.status == true) && 
            (pStsInfo->failure.pathBerSd.status == true))
            {
            /* Clear failure indication */
            pStsInfo->failure.pathBerSd.status = false;
            pStsInfo->failure.pathBerSd.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = ((pStsInfo->msgInh.sts       == false) && 
                      (pStsInfo->msgInh.pathBerSd == false));
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->pathBerSd, failHis, failInfo, blNotf, cFmStsBerSd, pTrblProf->pathBerSd);
            }
        }
    
    /* STS Path BER-based Signal Degrade */
    /* Only declare/terminate BER-SD failure when BER-SF failure is not present */
    if (pStsInfo->failure.pathBerSf.status == false)
        {
        mFailIndUpd(pSurInfo, pStsInfo, pathBerSd, pathBerSd, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pStsInfo->msgInh.sts       == false) && 
                      (pStsInfo->msgInh.pathBerSd == false));
            mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->pathBerSd, failHis, failInfo, blNotf, cFmStsBerSd, pTrblProf->pathBerSd);
            }
        }


	/* L bit error, only used for PW */
	mFailIndUpd(pSurInfo, pStsInfo, lBit, lBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pStsInfo->msgInh.sts   == false) &&
				  (pStsInfo->msgInh.lBit == false));
		mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->lBit, failHis, failInfo, blNotf, cFmStsLbit, pTrblProf->lBit);
		}

	/* R bit error, only used for PW */
	mFailIndUpd(pSurInfo, pStsInfo, rBit, rBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pStsInfo->msgInh.sts   == false) &&
				  (pStsInfo->msgInh.rBit == false));
		mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->rBit, failHis, failInfo, blNotf, cFmStsRbit, pTrblProf->rBit);
		}

	/* M bit error, only used for PW */
	mFailIndUpd(pSurInfo, pStsInfo, mBit, mBit, blFailChg);
	if (blFailChg == true)
		{
		blNotf = ((pStsInfo->msgInh.sts   == false) &&
				  (pStsInfo->msgInh.mBit == false));
		mFailRept(pSurInfo, cSurSts, pStsId, pFailInd->mBit, failHis, failInfo, blNotf, cFmStsMbit, pTrblProf->mBit);
		}
    }

/*------------------------------------------------------------------------------
Prototype    : private void StsPmParmUpd(tSurInfo        *pSurInfo,
                                         const tSurStsId *pStsId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pStsId                - STS ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void StsPmParmUpd(tSurInfo        *pSurInfo,
                          const tSurStsId *pStsId)
    {
    tSurStsInfo   *pStsInfo;
    word           wStsK;
    bool           blNewStat;
    tSurStsAnomaly anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    tSurLineInfo  *pLineInfo;
    long           pjcDiff;
    bool           blLoStat;
	tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;

    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pStsInfo = NULL;
    pLineInfo = NULL;

    /* Get STS information */
    mSurStsInfo(pSurInfo, pStsId, pStsInfo);
    mSurLineInfo(pSurInfo, &(pStsId->line), pLineInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pStsInfo->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pStsInfo->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pStsInfo->parm.curEngTime.minute)
        {
        pStsInfo->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pStsInfo->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pStsInfo->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurStsAnomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurSts, pStsId, &anomaly);
        pStsInfo->parm.cvP.curSec      += anomaly.b3;
        pStsInfo->parm.cvPfe.curSec    += anomaly.reip;
        pStsInfo->parm.ppjcPdet.curSec += anomaly.piInc;
        pStsInfo->parm.npjcPdet.curSec += anomaly.piDec;
        pStsInfo->parm.ppjcPgen.curSec += anomaly.pgInc;
        pStsInfo->parm.npjcPgen.curSec += anomaly.pgDec;
        }
    
    /* Check if defect exists at lower layer (section/line) */
    blLoStat = (((pLineInfo                      != null)        &&
                 (pLineInfo->status.state        == cSurStart))  && 
                ((pLineInfo->status.los.status   == true)  ||
                 (pLineInfo->status.lof.status   == true)  ||
                 (pLineInfo->status.aisl.status  == true)  ||
                 (pLineInfo->status.tims.status  == true))) ? true : false;
                 
    /* Update ES-P & SES-P parameters if lower layer (section/line) defect, or 
       AIS-P defect, or LOP-P defect, or (if ERDI-P is supported) UNEQ-P or TIM-P 
       is present */
    if ((blLoStat                        == true) ||
        (pStsInfo->status.aisp.status    == true) ||
        (pStsInfo->status.lopp.status    == true) ||
        ((pStsInfo->conf.erdiEn          == true) && 
         ((pStsInfo->status.uneqp.status == true) ||
          (pStsInfo->status.timp.status  == true))))
        {
        pStsInfo->parm.esP.curSec  = 1;
        pStsInfo->parm.sesP.curSec = 1;
        }
        
    /* Update ES-PFE & SES-PFE parameters */
    /* If ERDI-P is not supported, update ES-PFE & SES-PFE parameters only if 1-bit
       RDI-P defect (detected in G2 byte) is present */
    if (pStsInfo->conf.erdiEn == false)
        {
        if (pStsInfo->status.rdip.status == true)
            {
            pStsInfo->parm.esPfe.curSec  = 1;
            pStsInfo->parm.sesPfe.curSec = 1;
            }
        }
        
    /* If ERDI-P is supported, update ES-PFE & SES-PFE parameters only if 
       3-bits RDI-P defect (detected in G2 byte) is present */
    else
        {
        if ((pStsInfo->status.erdipS.status == true) ||
            (pStsInfo->status.erdipC.status == true))
            {
            pStsInfo->parm.esPfe.curSec  = 1;
            pStsInfo->parm.sesPfe.curSec = 1;
            }
        }

    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update ES-P */
        if (pStsInfo->parm.cvP.curSec != 0)
            {
            pStsInfo->parm.esP.curSec = 1;
            }

        /* Update ES-PFE */
        if (pStsInfo->parm.cvPfe.curSec != 0)
            {
            pStsInfo->parm.esPfe.curSec = 1;
            }

        /* Update SES parameters */
        wStsK = 0;
        mStsKval(pSurInfo, pStsInfo->conf.rate, wStsK, blNewStat);
        if (blNewStat == false)
            {
            mSurDebug(cSurWarnMsg, "Not support STS rate\n");
            }
        else
            {
            /* STS Near-end */
            if (pStsInfo->parm.cvP.curSec >= wStsK)
                {
                pStsInfo->parm.sesP.curSec = 1;
                }

            /* STS Far-end */
            if (pStsInfo->parm.cvPfe.curSec >= wStsK)
                {
                pStsInfo->parm.sesPfe.curSec = 1;
                }
            }
        
        /* Update STS PJ-Related registers  */
        if ((pStsInfo->parm.ppjcPdet.curSec != 0) ||
            (pStsInfo->parm.npjcPdet.curSec != 0))
            {
            pStsInfo->parm.pjcsPdet.curSec = 1;
            }
        if ((pStsInfo->parm.ppjcPgen.curSec != 0) ||
            (pStsInfo->parm.npjcPgen.curSec != 0))
            {
            pStsInfo->parm.pjcsPgen.curSec = 1;
            }
        pjcDiff = (pStsInfo->parm.ppjcPgen.curSec - pStsInfo->parm.npjcPgen.curSec) - 
                  (pStsInfo->parm.ppjcPdet.curSec - pStsInfo->parm.npjcPdet.curSec);
        pStsInfo->parm.pjcDiffP.curSec = (pjcDiff > 0) ? pjcDiff : (0 - pjcDiff);
        
        /* Update UAS parameter */
        if (pStsInfo->status.neUat == true)
            {
            pStsInfo->parm.uasP.curSec = 1;
            }
        if (pStsInfo->status.feUat == true)
            {
            pStsInfo->parm.uasPfe.curSec = 1;
            }

        /* Check if STS Near-End UAS just enter or exit and adjust registers */
        blNewStat = (pStsInfo->parm.sesP.curSec == 1) ? true : false;
        if (blNewStat != pStsInfo->status.neUatPot)
            {
            pStsInfo->status.nePotCnt = 1;
            }
        else
            {
            pStsInfo->status.nePotCnt++;
            }
        if (pStsInfo->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pStsInfo->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pStsInfo->parm.esP));
                mCurPerNegAdj(&(pStsInfo->parm.sesP));
                mCurPerNegAdj(&(pStsInfo->parm.ppjcPdet));
                mCurPerNegAdj(&(pStsInfo->parm.npjcPdet));
                mCurPerNegAdj(&(pStsInfo->parm.ppjcPgen));
                mCurPerNegAdj(&(pStsInfo->parm.npjcPgen));
                mCurPerNegAdj(&(pStsInfo->parm.pjcDiffP));
                mCurPerNegAdj(&(pStsInfo->parm.pjcsPdet));
                mCurPerNegAdj(&(pStsInfo->parm.pjcsPgen));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pStsInfo->parm.esP));
                    mPrePerNegAdj(&(pStsInfo->parm.sesP));
                    mPrePerNegAdj(&(pStsInfo->parm.ppjcPdet));
                    mPrePerNegAdj(&(pStsInfo->parm.npjcPdet));
                    mPrePerNegAdj(&(pStsInfo->parm.ppjcPgen));
                    mPrePerNegAdj(&(pStsInfo->parm.npjcPgen));
                    mPrePerNegAdj(&(pStsInfo->parm.pjcDiffP));
                    mPrePerNegAdj(&(pStsInfo->parm.pjcsPdet));
                    mPrePerNegAdj(&(pStsInfo->parm.pjcsPgen));
                    }

                /* Update UAS */
            	pStsInfo->parm.uasP.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pStsInfo->parm.cvP));
                mCurPerPosAdj(&(pStsInfo->parm.esP));
                mCurPerPosAdj(&(pStsInfo->parm.sesP));
                mCurPerPosAdj(&(pStsInfo->parm.ppjcPdet));
                mCurPerPosAdj(&(pStsInfo->parm.npjcPdet));
                mCurPerPosAdj(&(pStsInfo->parm.ppjcPgen));
                mCurPerPosAdj(&(pStsInfo->parm.npjcPgen));
                mCurPerPosAdj(&(pStsInfo->parm.pjcDiffP));
                mCurPerPosAdj(&(pStsInfo->parm.pjcsPdet));
                mCurPerPosAdj(&(pStsInfo->parm.pjcsPgen));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pStsInfo->parm.cvP));
                    mPrePerPosAdj(&(pStsInfo->parm.esP));
                    mPrePerPosAdj(&(pStsInfo->parm.sesP));
                    mPrePerPosAdj(&(pStsInfo->parm.ppjcPdet));
                    mPrePerPosAdj(&(pStsInfo->parm.npjcPdet));
                    mPrePerPosAdj(&(pStsInfo->parm.ppjcPgen));
                    mPrePerPosAdj(&(pStsInfo->parm.npjcPgen));
                    mPrePerPosAdj(&(pStsInfo->parm.pjcDiffP));
                    mPrePerPosAdj(&(pStsInfo->parm.pjcsPdet));
                    mPrePerPosAdj(&(pStsInfo->parm.pjcsPgen));
                    }

                /* Update UAS */
            	if (pStsInfo->parm.uasP.curPer.value >= 10)
            		{
            		pStsInfo->parm.uasP.curPer.value -= 10;
            		}
            	else
            		{
            		pStsInfo->parm.uasP.curPer.value = 0;
            		}
            	pStsInfo->parm.uasP.curSec = 0;
                }
            }

        /* Check if STS Far-End UAS just enter or exit and adjust registers */
        blNewStat = (pStsInfo->parm.sesPfe.curSec == 1) ? true : false;
        if (blNewStat != pStsInfo->status.feUatPot)
            {
            pStsInfo->status.fePotCnt = 1;
            }
        else
            {
            pStsInfo->status.fePotCnt++;
            }
        if (pStsInfo->status.fePotCnt == 10)
            {
            /* Update unavailable time status */
            pStsInfo->status.feUat = blNewStat;
            SurCurTime(&currentTime);

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pStsInfo->parm.esPfe));
                mCurPerNegAdj(&(pStsInfo->parm.sesPfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pStsInfo->parm.esPfe));
                    mPrePerNegAdj(&(pStsInfo->parm.sesPfe));
                    }

                /* Update UAS */
            	pStsInfo->parm.uasPfe.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pStsInfo->parm.cvPfe));
                mCurPerPosAdj(&(pStsInfo->parm.esPfe));
                mCurPerPosAdj(&(pStsInfo->parm.sesPfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pStsInfo->parm.cvPfe));
                    mPrePerPosAdj(&(pStsInfo->parm.esPfe));
                    mPrePerPosAdj(&(pStsInfo->parm.sesPfe));
                    }

                /* Update UAS */
            	if (pStsInfo->parm.uasPfe.curPer.value >= 10)
            		{
            		pStsInfo->parm.uasPfe.curPer.value -= 10;
            		}
            	else
            		{
            		pStsInfo->parm.uasPfe.curPer.value = 0;
            		}
            	pStsInfo->parm.uasPfe.curSec = 0;
                }
            }
        
        /* Accumulate inhibition */
        pStsInfo->parm.cvP.inhibit      = false;
        pStsInfo->parm.esP.inhibit      = false;
        pStsInfo->parm.sesP.inhibit     = false;
        pStsInfo->parm.ppjcPdet.inhibit = false;
        pStsInfo->parm.npjcPdet.inhibit = false;
        pStsInfo->parm.ppjcPgen.inhibit = false;
        pStsInfo->parm.npjcPgen.inhibit = false;
        pStsInfo->parm.pjcDiffP.inhibit = false;
        pStsInfo->parm.pjcsPdet.inhibit = false;
        pStsInfo->parm.pjcsPgen.inhibit = false;
        pStsInfo->parm.cvPfe.inhibit    = false;
        pStsInfo->parm.esPfe.inhibit    = false;
        pStsInfo->parm.sesPfe.inhibit   = false;

        /* Inhibit when UAS */
        if (pStsInfo->status.neUat == true)
            {
            pStsInfo->parm.cvP.inhibit      = true;
            pStsInfo->parm.esP.inhibit      = true;
            pStsInfo->parm.sesP.inhibit     = true;
            pStsInfo->parm.ppjcPdet.inhibit = true;
            pStsInfo->parm.npjcPdet.inhibit = true;
            pStsInfo->parm.ppjcPgen.inhibit = true;
            pStsInfo->parm.npjcPgen.inhibit = true;
            pStsInfo->parm.pjcDiffP.inhibit = true;
            pStsInfo->parm.pjcsPdet.inhibit = true;
            pStsInfo->parm.pjcsPgen.inhibit = true;
            }
        if (pStsInfo->status.feUat == true)
            {
            pStsInfo->parm.cvPfe.inhibit = true;
            pStsInfo->parm.esPfe.inhibit = true;
            pStsInfo->parm.sesPfe.inhibit= true;
            }

        /* Inhibit when SES */
        if (pStsInfo->parm.sesP.curSec == 1)
            {
            pStsInfo->parm.cvP.inhibit = true;
            }
        if (pStsInfo->parm.sesPfe.curSec == 1)
            {
            pStsInfo->parm.cvPfe.inhibit = true;
            }

        /* Inhibit STS Far-End parameters when AIS-P, LOP, UNEQ or lower-level 
           defects occurred */
        if ((blLoStat                      == true) ||
            (pStsInfo->status.aisp.status  == true) ||
            (pStsInfo->status.lopp.status  == true) ||
            (pStsInfo->status.uneqp.status == true))
            {
            pStsInfo->parm.cvPfe.inhibit  = true;
            pStsInfo->parm.esPfe.inhibit  = true;
            pStsInfo->parm.sesPfe.inhibit = true;
            pStsInfo->parm.uasPfe.inhibit = true;
            pStsInfo->parm.fcPfe.inhibit  = true;

            pStsInfo->parm.cvPfe.curPer.invalid  = true;
            pStsInfo->parm.esPfe.curPer.invalid  = true;                               
            pStsInfo->parm.sesPfe.curPer.invalid = true;                               
            pStsInfo->parm.uasPfe.curPer.invalid = true;                               
            pStsInfo->parm.fcPfe.curPer.invalid  = true;                               
            }

        /* Increase STS Near-End adjustment registers */
        /* In available time */
        if (pStsInfo->status.neUat == false)
            {
            /* Potential for entering unavailable time */
            if (pStsInfo->parm.sesP.curSec == 1)
                {
                mNegInc(&(pStsInfo->parm.esP));
                mNegInc(&(pStsInfo->parm.sesP));
                mNegInc(&(pStsInfo->parm.ppjcPdet));
                mNegInc(&(pStsInfo->parm.npjcPdet));
                mNegInc(&(pStsInfo->parm.ppjcPgen));
                mNegInc(&(pStsInfo->parm.npjcPgen));
                mNegInc(&(pStsInfo->parm.pjcDiffP));
                mNegInc(&(pStsInfo->parm.pjcsPdet));
                mNegInc(&(pStsInfo->parm.pjcsPgen));
                }
            }
        
        /* In unavailable time */    
        else
            {
            /* Potential for exiting unavailable time */
            if (pStsInfo->parm.sesP.curSec == 0)
                {
                mPosInc(&(pStsInfo->parm.cvP));
                mPosInc(&(pStsInfo->parm.esP));
                mPosInc(&(pStsInfo->parm.sesP));
                mPosInc(&(pStsInfo->parm.ppjcPdet));
                mPosInc(&(pStsInfo->parm.npjcPdet));
                mPosInc(&(pStsInfo->parm.ppjcPgen));
                mPosInc(&(pStsInfo->parm.npjcPgen));
                mPosInc(&(pStsInfo->parm.pjcDiffP));
                mPosInc(&(pStsInfo->parm.pjcsPdet));
                mPosInc(&(pStsInfo->parm.pjcsPgen));
                }
            }

        /* Increase STS Far-End adjustment registers */
        if (pStsInfo->status.neUat == false)
            {
            /* In available time */
            if (pStsInfo->status.feUat == false)
                {
                /* Potential for entering unavailable time */
                if (pStsInfo->parm.sesPfe.curSec == 1)
                    {
                    mNegInc(&(pStsInfo->parm.esPfe));
                    mNegInc(&(pStsInfo->parm.sesPfe));
                    mNegInc(&(pStsInfo->parm.ppjcPdet));
                    mNegInc(&(pStsInfo->parm.npjcPdet));
                    mNegInc(&(pStsInfo->parm.ppjcPgen));
                    mNegInc(&(pStsInfo->parm.npjcPgen));
                    mNegInc(&(pStsInfo->parm.pjcDiffP));
                    mNegInc(&(pStsInfo->parm.pjcsPdet));
                    mNegInc(&(pStsInfo->parm.pjcsPgen));
                    }
                }
                
            /* In unavailable time */
            else
                {
                /* Potential for exiting unavailable time */
                if (pStsInfo->parm.sesPfe.curSec == 0)
                    {
                    mPosInc(&(pStsInfo->parm.cvPfe));
                    mPosInc(&(pStsInfo->parm.esPfe));
                    mPosInc(&(pStsInfo->parm.sesPfe));
                    mPosInc(&(pStsInfo->parm.ppjcPdet));
                    mPosInc(&(pStsInfo->parm.npjcPdet));
                    mPosInc(&(pStsInfo->parm.ppjcPgen));
                    mPosInc(&(pStsInfo->parm.npjcPgen));
                    mPosInc(&(pStsInfo->parm.pjcDiffP));
                    mPosInc(&(pStsInfo->parm.pjcsPdet));
                    mPosInc(&(pStsInfo->parm.pjcsPgen));
                    }
                }
            }

        /* Clear STS Near-End adjustment registers */
        if (pStsInfo->status.neUat == false)
            {
            if ((pStsInfo->parm.sesP.curSec == 0) &&
                (pStsInfo->status.neUatPot  == true))
                {
                mNegClr(&(pStsInfo->parm.esP));
                mNegClr(&(pStsInfo->parm.sesP));
                mNegClr(&(pStsInfo->parm.ppjcPdet));
                mNegClr(&(pStsInfo->parm.npjcPdet));
                mNegClr(&(pStsInfo->parm.ppjcPgen));
                mNegClr(&(pStsInfo->parm.npjcPgen));
                mNegClr(&(pStsInfo->parm.pjcDiffP));
                mNegClr(&(pStsInfo->parm.pjcsPdet));
                mNegClr(&(pStsInfo->parm.pjcsPgen));
                }
            }
        else if ((pStsInfo->parm.sesP.curSec == 1) &&
                 (pStsInfo->status.neUatPot  == false))
            {
            mPosClr(&(pStsInfo->parm.cvP));
            mPosClr(&(pStsInfo->parm.esP));
            mPosClr(&(pStsInfo->parm.sesP));
            mPosClr(&(pStsInfo->parm.ppjcPdet));
            mPosClr(&(pStsInfo->parm.npjcPdet));
            mPosClr(&(pStsInfo->parm.ppjcPgen));
            mPosClr(&(pStsInfo->parm.npjcPgen));
            mPosClr(&(pStsInfo->parm.pjcDiffP));
            mPosClr(&(pStsInfo->parm.pjcsPdet));
            mPosClr(&(pStsInfo->parm.pjcsPgen));
            }

        /* Clear STS Far-End adjustment registers */
        if (pStsInfo->status.feUat == false)
            {
            if ((pStsInfo->parm.sesPfe.curSec == 0) &&
                (pStsInfo->status.feUatPot    == true))
                {
                mNegClr(&(pStsInfo->parm.esPfe));
                mNegClr(&(pStsInfo->parm.sesPfe));
                }
            }
        else if ((pStsInfo->parm.sesPfe.curSec == 1) &&
                 (pStsInfo->status.feUatPot    == false))
            {
            mPosClr(&(pStsInfo->parm.cvPfe));
            mPosClr(&(pStsInfo->parm.esPfe));
            mPosClr(&(pStsInfo->parm.sesPfe));
            }

        /* Store current second statuses */
        pStsInfo->status.neUatPot = (pStsInfo->parm.sesP.curSec   == 1) ? true : false;
        pStsInfo->status.feUatPot = (pStsInfo->parm.sesPfe.curSec == 1) ? true : false;

        /* Accumulate current period registers */
        mCurPerAcc(&(pStsInfo->parm.cvP));
        mCurPerAcc(&(pStsInfo->parm.esP));
        mCurPerAcc(&(pStsInfo->parm.sesP));
        mCurPerAcc(&(pStsInfo->parm.uasP));
        mCurPerAcc(&(pStsInfo->parm.fcP));
        mCurPerAcc(&(pStsInfo->parm.ppjcPdet));
        mCurPerAcc(&(pStsInfo->parm.npjcPdet));
        mCurPerAcc(&(pStsInfo->parm.ppjcPgen));
        mCurPerAcc(&(pStsInfo->parm.npjcPgen));
        mCurPerAcc(&(pStsInfo->parm.pjcDiffP));
        mCurPerAcc(&(pStsInfo->parm.pjcsPdet));
        mCurPerAcc(&(pStsInfo->parm.pjcsPgen));
        mCurPerAcc(&(pStsInfo->parm.cvPfe));
        mCurPerAcc(&(pStsInfo->parm.esPfe));
        mCurPerAcc(&(pStsInfo->parm.sesPfe));
        mCurPerAcc(&(pStsInfo->parm.uasPfe));
        mCurPerAcc(&(pStsInfo->parm.fcPfe));

        /* Check and report TCA */
        blNotf = ((pStsInfo->msgInh.sts == false) &&
                  (pStsInfo->msgInh.tca == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Report TCAs of Path Near-End parameters */
        if ((pStsInfo->status.neUat    == false) &&
            (pStsInfo->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsCv,       &(pStsInfo->parm.cvP),      blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsEs,       &(pStsInfo->parm.esP),      blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsSes,      &(pStsInfo->parm.sesP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsPpjcPdet, &(pStsInfo->parm.ppjcPdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsNpjcPdet, &(pStsInfo->parm.npjcPdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsPpjcPgen, &(pStsInfo->parm.ppjcPgen), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsNpjcPgen, &(pStsInfo->parm.npjcPgen), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsPjcDiff,  &(pStsInfo->parm.pjcDiffP), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsPjcsPdet, &(pStsInfo->parm.pjcsPdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsPjcsPgen, &(pStsInfo->parm.pjcsPgen), blNotf, &tcaInfo);
            }
        
        /* Report TCAs of Path Far-End parameters */
        if ((pStsInfo->status.feUat    == false) &&
            (pStsInfo->status.feUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsCvPfe,    &(pStsInfo->parm.cvPfe),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsEsPfe,    &(pStsInfo->parm.esPfe),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsSesPfe,   &(pStsInfo->parm.sesPfe),   blNotf, &tcaInfo);
            }
        
        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsUas,    &(pStsInfo->parm.uasP),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurSts, pStsId, pStsInfo, cPmStsUasPfe, &(pStsInfo->parm.uasPfe), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pStsInfo->parm.cvP));
        mStackDown(&(pStsInfo->parm.esP));
        mStackDown(&(pStsInfo->parm.sesP));
        mStackDown(&(pStsInfo->parm.uasP));
        mStackDown(&(pStsInfo->parm.fcP));
        mStackDown(&(pStsInfo->parm.ppjcPdet));
        mStackDown(&(pStsInfo->parm.npjcPdet));
        mStackDown(&(pStsInfo->parm.ppjcPgen));
        mStackDown(&(pStsInfo->parm.npjcPgen));
        mStackDown(&(pStsInfo->parm.pjcDiffP));
        mStackDown(&(pStsInfo->parm.pjcsPdet));
        mStackDown(&(pStsInfo->parm.pjcsPgen));
        mStackDown(&(pStsInfo->parm.cvPfe));
        mStackDown(&(pStsInfo->parm.esPfe));
        mStackDown(&(pStsInfo->parm.sesPfe));
        mStackDown(&(pStsInfo->parm.uasPfe));
        mStackDown(&(pStsInfo->parm.fcPfe));

        /* Update time for period */
        pStsInfo->parm.prePerStartTime = pStsInfo->parm.curPerStartTime;
        pStsInfo->parm.prePerEndTime = dCurTime;
        pStsInfo->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pStsInfo->parm.cvP));
        mDayShift(&(pStsInfo->parm.esP));
        mDayShift(&(pStsInfo->parm.sesP));
        mDayShift(&(pStsInfo->parm.uasP));
        mDayShift(&(pStsInfo->parm.fcP));
        mDayShift(&(pStsInfo->parm.ppjcPdet));
        mDayShift(&(pStsInfo->parm.npjcPdet));
        mDayShift(&(pStsInfo->parm.ppjcPgen));
        mDayShift(&(pStsInfo->parm.npjcPgen));
        mDayShift(&(pStsInfo->parm.pjcDiffP));
        mDayShift(&(pStsInfo->parm.pjcsPdet));
        mDayShift(&(pStsInfo->parm.pjcsPgen));
        mDayShift(&(pStsInfo->parm.cvPfe));
        mDayShift(&(pStsInfo->parm.esPfe));
        mDayShift(&(pStsInfo->parm.sesPfe));
        mDayShift(&(pStsInfo->parm.uasPfe));
        mDayShift(&(pStsInfo->parm.fcPfe));

        /* Update time for day */
        pStsInfo->parm.preDayStartTime = pStsInfo->parm.curDayStartTime;
        pStsInfo->parm.preDayEndTime = dCurTime;
        pStsInfo->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurStsStatUpd(tSurDev          device, 
                                         const tSurStsId *pStsId)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input STS.

Inputs       : device                - device 
               pStsId                - STS ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurStsStatUpd(tSurDev device, const tSurStsId *pStsId)
    {
    tSurInfo *pSurInfo;
    
    /* Get database */
    pSurInfo = mDevDb(device);

    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) &&
        (pSurInfo->interface.DefStatGet    != null))
        {
        StsAllDefUpd(pSurInfo, pStsId);
        }
    
    /* Declare/terminate failures */
    StsAllFailUpd(pSurInfo, pStsId);
    
    /* Update performance registers */
    StsPmParmUpd(pSurInfo, pStsId);
    }

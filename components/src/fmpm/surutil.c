/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR - UTILITIES
 *
 * File        : surutil.c
 *
 * Created Date: 04-Jan-09
 *
 * Description : This file contains source code of utility functions
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/time.h>
#include <time.h>
#include "sur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype    : bool SurCurTime(tSurTime *pTime)

Purpose      : This function is used to get current system time

Inputs       : None
            
Outputs      : pTime                 - Current system time

Return       : true                  - success
               false                 - fail
------------------------------------------------------------------------------*/
bool SurCurTime(tSurTime *pTime)
    {
    struct tm *pCurTime;
    time_t curTime;
    
    /* Check parameter */
    if (pTime == null)
        {
        return false;
        }
    
    /* Get system time */
    curTime = time(&curTime);
    pCurTime = localtime(&curTime);
    if ((pCurTime) == null)
        {
        return false;
        }
    
    /* Return time */
    pTime->second = pCurTime->tm_sec;
    pTime->minute = pCurTime->tm_min;
    pTime->hour   = pCurTime->tm_hour;
    pTime->day    = pCurTime->tm_mday;
    pTime->month  = pCurTime->tm_mon + 1;
    pTime->year   = pCurTime->tm_year + 1900;
    
    return true;
    }

/*------------------------------------------------------------------------------
Prototype    : bool SurTimeChange(dword timeVal, tSurTime *pTime)

Purpose      : This function is used to get time detail from time value

Inputs       : timeVal               - Time value
                           
Outputs      : pTime                 - Time detail

Return       : true                  - success
               false                 - fail
------------------------------------------------------------------------------*/
bool SurTimeChange(dword timeVal, tSurTime *pTime)
    {
    struct tm *_pTime;
    time_t _time;
    
    /* Check parameter */
    if (pTime == null)
        {
        return false;
        }
    
    /* Convert */
    _time  = timeVal;
    _pTime = localtime(&_time);
    if ((_pTime) == null)
        {
        return false;
        }
    
    /* Return time */
    pTime->second = _pTime->tm_sec;
    pTime->minute = _pTime->tm_min;
    pTime->hour   = _pTime->tm_hour;
    pTime->day    = _pTime->tm_mday;
    pTime->month  = _pTime->tm_mon + 1;
    pTime->year   = _pTime->tm_year + 1900;
    
    return true;
    }

/*------------------------------------------------------------------------------
Prototype    : bool SurCurTimeInS(dword *pTimeVal)

Purpose      : This function is used to get current system time in second

Inputs       : None
            
Outputs      : pTimeVal              - Current system time in second

Return       : true                  - success
               false                 - fail
------------------------------------------------------------------------------*/
bool SurCurTimeInS(dword *pTimeVal)
    {
    struct timeval tm;
	
	gettimeofday(&tm, NULL);
    *pTimeVal = tm.tv_sec;
    
    return true;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR
 *
 * File        : sur.c
 *
 * Created Date: 06-Jan-09
 *
 * Description : This file contain all source code Transmisstion Surveillance
 *               module management
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include "stdio.h"

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/
/* Version information */
#define cSurVersion                  "Version: 1.0, Release Date: 12-Feb-09"

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mTrblDesgnSet(pTrbl, alarm, sev, serAff) 
Purpose   : This macro is used to set trouble designation
Inputs    : pTrbl                    - target trouble designation
            alarm                    - alarm type
            sev                      - alarm severity
            serAff                   - service affecting
Outputs   : None
------------------------------------------------------------------------------*/
#define mTrblDesgnSet(pTrbl, alarm, sev, serAff)                                \
    {                                                                           \
    (pTrbl)->alarmType = alarm;                                                 \
    (pTrbl)->alarmSev  = sev;                                                   \
    (pTrbl)->serviceAff= serAff;                                                \
    }

/*------------------------------------------------------------------------------
Prototype : mSurDeviceGet()
Purpose   : This macro is used to check if a pointer to a tSurInfo using a 
            tSurDevHdl pointer.
Inputs    : 
Outputs   : 
------------------------------------------------------------------------------*/  
#define mSurDeviceGet(pHandle, pSurInfo)                                        \
    {                                                                           \
    byte _wIdx;                                                                 \
    for (_wIdx = 0; _wIdx < cSurMaxDev; _wIdx++)                                \
        {                                                                       \
        if (mDevDb(surDb.devices[_wIdx])->pHdl == pHandle)                      \
            {                                                                   \
            pSurInfo = mDevDb(surDb.devices[_wIdx]);                            \
            break;                                                              \
            }                                                                   \
        else                                                                    \
            {                                                                   \
            pSurInfo = null;                                                    \
            }                                                                   \
        }                                                                       \
    }

/*--------------------------- Local Typedefs ---------------------------------*/
/*--------------------------- Global variables -------------------------------*/
/* Database */
tSurDb surDb;

/*--------------------------- Local variables --------------------------------*/
/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
This function is used to start Surveillance engine
------------------------------------------------------------------------------*/
extern eSurRet SurEngStart(void);

/*------------------------------------------------------------------------------
This function is used to stop Surveillance engine
------------------------------------------------------------------------------*/
extern eSurRet SurEngStop(void);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in Linear APS engine 
information structure
------------------------------------------------------------------------------*/
extern void SurApslInfoFinal(tSurApslInfo *pInfo);

/*------------------------------------------------------------------------------
This function is used to de-provision all channels in line.
------------------------------------------------------------------------------*/
extern void SurLineAllChnDeProv(tSurInfo         *pSurInfo,
                                const tSurLineId *pLineId);
                                
/*------------------------------------------------------------------------------
This function is used to deallocate all resource in DS3/E3 information structure
------------------------------------------------------------------------------*/
extern void SurDe3InfoFinal(tSurDe3Info *pInfo);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in DS1/E1 information structure
------------------------------------------------------------------------------*/
extern void SurDe1InfoFinal(tSurDe1Info *pInfo);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in IMA Link information structure
------------------------------------------------------------------------------*/
extern void SurImaLinkInfoFinal(tSurImaLinkInfo *pInfo);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in IMA Group information structure
------------------------------------------------------------------------------*/
extern void SurImaGrpInfoFinal(tSurImaGrpInfo *pInfo);

/*------------------------------------------------------------------------------
This function is used to deallocate all resource in PW information structure
------------------------------------------------------------------------------*/
extern void SurPwInfoFinal(tSurPwInfo *pInfo);

void mSurMsgPrint(uint32 msgType, const char *format)
    {
    AtUnused(msgType);
    AtUnused(format);
    }

void mSurPrintc(uint32 surColor, const char *format)
    {
    AtUnused(surColor);
    AtUnused(format);
    }
void mSurDebug(uint32 msgType,const char *format)
    {
    AtUnused(msgType);
    AtUnused(format);
    }
/*------------------------------------------------------------------------------
Prototype    : public char* SurVersion()

Purpose      : This function is used to get version description of SUR module

Inputs       : None

Outputs      : None

Return       : Version information
------------------------------------------------------------------------------*/
public char* SurVersion()
    {
    return cSurVersion;
    }

/*------------------------------------------------------------------------------
Prototype    : public eSurRet SurChnProv(tSurDev     device,
                                         eSurChnType chnType,
                                         const void *pChnId)

Purpose      : This function is used to provision a channel to monitor failures
               and performance. Note, channel's engine will not start
               automatically when it has just been provisioned.

Inputs       : device                - Device
               chnType               - Channel type
               pChnId                - Channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet SurChnProv(tSurDev     device, 
                          eSurChnType chnType, 
                          const void *pChnId)
    {
    eSurRet ret;

    /* Provision */
    ret = cSurOk;
    switch (chnType)
        {
        /* Linear APS */
        case cSurApslEng:
            ret = SurApslProv(device, (tSurApslId *)pChnId);
            break;

        /* SONET/SDH line */
        case cSurLine:
            ret = SurLineProv(device, (tSurLineId *)pChnId);
            break;

        /* STS Path */
        case cSurSts:
            ret = SurStsProv(device, (tSurStsId *)pChnId);
            break;

        /* VT Path */
        case cSurVt:
            ret = SurVtProv(device, (tSurVtId *)pChnId);
            break;

        /* AU-n */
        case cSurAu:
            ret = SurAuProv(device, (tSurAuId *)pChnId);
            break;

        /* TU-3 */
        case cSurTu3:
            ret = SurTu3Prov(device, (tSurTu3Id *)pChnId);
            break;

        /* TU-m (TU11, TU12, TU2) */
        case cSurTu:
            ret = SurTuProv(device, (tSurTuId *)pChnId);
            break;

        /* DS3/E3 channel */
        case cSurDe3:
            ret = SurDe3Prov(device, (tSurDe3Id *)pChnId);
            break;

        /* DS1/E1 channel */
        case cSurDe1:
            ret = SurDe1Prov(device, (tSurDe1Id *)pChnId);
            break;

        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkProv(device, (tSurImaLinkId *)pChnId);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpProv(device, (tSurImaGrpId *)pChnId);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwProv(device, (tSurPwId *)pChnId);
            break;

        /* Channel is not supported */
        default:
            ret = cSurErrNoSuchChn;
            break;
        }

    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : public eSurRet SurChnDeProv(tSurDev     device,
                                           eSurChnType chnType,
                                           const void *pChnId)

Purpose      : This function is used to deprovision a monitored channel. All 
               resources of this channel will be deallocated.

Inputs       : device                - Device
               chnType               - Channel type
               pChnId                - Channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet SurChnDeProv(tSurDev     device, 
                            eSurChnType chnType, 
                            const void *pChnId)
    {
    eSurRet ret;

    /* DeProvision */
    ret = cSurOk;
    switch (chnType)
        {
        /* Linear APS */
        case cSurApslEng:
            ret = SurApslDeProv(device, (tSurApslId *)pChnId);
            break;

        /* SONET/SDH line */
        case cSurLine:
            ret = SurLineDeProv(device, (tSurLineId *)pChnId);
            break;

        /* STS Path */
        case cSurSts:
            ret = SurStsDeProv(device, (tSurStsId *)pChnId);
            break;

        /* VT Path */
        case cSurVt:
            ret = SurVtDeProv(device, (tSurVtId *)pChnId);
            break;

        /* AU-n */
        case cSurAu:
            ret = SurAuDeProv(device, (tSurAuId *)pChnId);
            break;

        /* TU-3 */
        case cSurTu3:
            ret = SurTu3DeProv(device, (tSurTu3Id *)pChnId);
            break;

        /* TU-m (TU11, TU12, TU2) */
        case cSurTu:
            ret = SurTuDeProv(device, (tSurTuId *)pChnId);
            break;

        /* DS3/E3 channel */
        case cSurDe3:
            ret = SurDe3DeProv(device, (tSurDe3Id *)pChnId);
            break;

        /* DS1/E1 channel */
        case cSurDe1:
            ret = SurDe1DeProv(device, (tSurDe1Id *)pChnId);
            break;

        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkDeProv(device, (tSurImaLinkId *)pChnId);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpDeProv(device, (tSurImaGrpId *)pChnId);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwDeProv(device, (tSurPwId *)pChnId);
            break;

        /* Channel is not supported */
        default:
            ret = cSurErrNoSuchChn;
            break;
        }

    return ret;
    }
                                     
/*------------------------------------------------------------------------------
 Prototype    : friend char* SurDevDesGet(tSurDev device)
 
 Purpose      : This function is used to get description of a device
 
 Inputs       : device               - device
 
 Outputs      : None
 
 Return       : device description or empty if cannot get device description
*-----------------------------------------------------------------------------*/
friend char* SurDevDesGet(tSurDev device)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return "";
        }
        
    /* Get device */
    pSurInfo = mDevDb(device);
    
    /* Return description */
    if (pSurInfo->interface.DevDes != null)
        {
        pSurInfo->interface.DevDes(pSurInfo->pHdl, pSurInfo->desBuf);
        return pSurInfo->desBuf;
        }
    
    return "";
    }

/*------------------------------------------------------------------------------
 Prototype    : eSurRet SurChnDbInit(tSurDevDb *pDb))
 
 Purpose      : This function is used to initialize channel database
 
 Inputs       : pDb                   - pointer to the database
 
 Outputs      : None
 
 Return       : None
*-----------------------------------------------------------------------------*/
friend eSurRet SurChnDbInit(tSurDevDb *pDb)
    {
    /*Declare local variables */
    word    wIdx;
    
    /* Allocate & Initialize line database */
    for (wIdx = 0; wIdx < cSurMaxLine; wIdx++)
        {
        pDb->chnDb.pLine[wIdx] = OsalMemAlloc(sizeof(tSurLineDb));
        if (pDb->chnDb.pLine[wIdx] != null)
            {
            OsalMemInit(pDb->chnDb.pLine[wIdx], sizeof(tSurLineDb), 0);
            }
        else
            {
            return cSurErrRsrNoAvai;
            }
        }
    
    /* Allocate & Initilize DS3/E3 database */
    for (wIdx = 0; wIdx < cSurMaxDe3Line; wIdx++)
        {
        pDb->chnDb.pDe3[wIdx] = OsalMemAlloc(sizeof(tSurDe3Db));
        if (pDb->chnDb.pDe3[wIdx] != null)
            {
            OsalMemInit(pDb->chnDb.pDe3[wIdx], sizeof(tSurDe3Db), 0);
            }
        else
            {
            return cSurErrRsrNoAvai;
            }
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
 Prototype    : eSurRet SurDefMastConfSet(tSurInfo *pSurInfo)
 
 Purpose      : This function is used to set default configuration for a device
 
 Inputs       : pSurInfo              - pointer to the device
 
 Outputs      : None
 
 Return       : None
*-----------------------------------------------------------------------------*/
friend eSurRet SurDefMastConfSet(tSurInfo *pSurInfo)
    {
    tSurMastConf    *pMastConf;
    
    pMastConf = &(pSurInfo->pDb->mastConf);
    
    /* Set default trouble profiles */
    /* Linear APS */
    mTrblDesgnSet(&(pMastConf->troubleProf.apsKbyteFail), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.apsChnMis), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.apsMdMis), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.apsFeFail), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.apsFail), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.apsSw), cSurNonReport, cSurMn, cSurNsa);
    
    /* Section/Line */
    mTrblDesgnSet(&(pMastConf->troubleProf.los), cSurAlarm, cSurCr, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.lof), cSurAlarm, cSurMj, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.tims), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.lineBerSf), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.lineBerSd), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.dcc), cSurAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.aisl), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.rfil), cSurNonReport, cSurMn, cSurNsa);
    
    /* STS path */
    mTrblDesgnSet(&(pMastConf->troubleProf.lopp), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.plmp), cSurAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.uneqp), cSurAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.timp), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.aisp), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.rfip), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.erfipS), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.erfipC), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.erfipP), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pathBerSf), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pathBerSd),cSurNonAlarm, cSurMn, cSurNsa);
    
    /* VT path */
    mTrblDesgnSet(&(pMastConf->troubleProf.lopv), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.plmv), cSurAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.uneqv), cSurAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.timv), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.aisv), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.rfiv), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.erfivS), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.erfivC), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.erfivP), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.vtBerSf), cSurNonAlarm, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.vtBerSd),cSurNonAlarm, cSurMn, cSurNsa);
    
    /* DS3/E3 path */
    mTrblDesgnSet(&(pMastConf->troubleProf.de3Los), cSurAlarm, cSurCr, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.de3Lof), cSurAlarm, cSurMj, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.de3Ais), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.de3Rai), cSurNonReport, cSurMn, cSurNsa);
    
    /* DS1/E1 path */
    mTrblDesgnSet(&(pMastConf->troubleProf.de1Los), cSurAlarm, cSurCr, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.de1Lof), cSurAlarm, cSurMj, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.de1Ais), cSurNonReport, cSurMn, cSurNsa);
    mTrblDesgnSet(&(pMastConf->troubleProf.de1Rai), cSurNonReport, cSurMn, cSurNsa);
    
    /* PW */
    mTrblDesgnSet(&(pMastConf->troubleProf.lBit), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.rBit), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.mBit), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pwStrayPkt), cSurAlarm, cSurCr, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pwMalformedPkt), cSurAlarm, cSurCr, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pwExcPktLossRate), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pwJitBufOverrun), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pwRemotePktLoss), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.pwLofs), cSurAlarm, cSurMn, cSurSa);

    /* IMA Link */
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkLif), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkLods), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkRfi), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkTxMisConn), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkRxMisConn), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkTxFault), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkRxFault), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkTxUnusableFe), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaLinkRxUnusableFe), cSurAlarm, cSurMn, cSurSa);

    /* IMA Group */
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpStartupFe), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpCfgAborted), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpCfgAbortedFe), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpInsuffLink), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpInsuffLinkFe), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpBlockedFe), cSurAlarm, cSurMn, cSurSa);
    mTrblDesgnSet(&(pMastConf->troubleProf.imaGrpTimingMismatch), cSurAlarm, cSurMn, cSurSa);

    /* TCA */
    mTrblDesgnSet(&(pMastConf->troubleProf.tca), cSurNonReport, cSurMn, cSurNsa);
    
    /* Set default color profiles */
    pMastConf->colorProf.criticalFail  = cSurColorRed;
    pMastConf->colorProf.majorFail     = cSurColorRed;
    pMastConf->colorProf.error         = cSurColorRed;
    pMastConf->colorProf.danger        = cSurColorRed;
    pMastConf->colorProf.minorFail     = cSurColorYellow;
    pMastConf->colorProf.nonAlarm      = cSurColorAmber;
    pMastConf->colorProf.caution       = cSurColorAmber;
    pMastConf->colorProf.warning       = cSurColorYellow;
    pMastConf->colorProf.tmpMalfunc    = cSurColorAmber;
    pMastConf->colorProf.success       = cSurColorGreen;
    pMastConf->colorProf.neutral       = cSurColorWhite;
    
    /* Set default K-threholds */
    /* Set section K-thresholds */
    pMastConf->kThres.oc1Sec       = 52;
    pMastConf->kThres.oc3Sec       = 155;
    pMastConf->kThres.oc12Sec      = 616;
    pMastConf->kThres.oc24Sec      = 1220;
    pMastConf->kThres.oc48Sec      = 2392;
    pMastConf->kThres.oc192Sec     = 8854;
    pMastConf->kThres.oc768Sec     = 35416;
    
    /* Set line K-thresholds */
    pMastConf->kThres.oc1Line      = 51;
    pMastConf->kThres.oc3Line      = 154;
    pMastConf->kThres.oc12Line     = 615;
    pMastConf->kThres.oc24Line     = 1230;
    pMastConf->kThres.oc48Line     = 2459;
    pMastConf->kThres.oc192Line    = 9835;
    pMastConf->kThres.oc768Line    = 39340;
    
    /* Set STS path K-thresholds */
    pMastConf->kThres.sts1         = 2400;
    pMastConf->kThres.sts3c        = 2400;
    pMastConf->kThres.sts12c       = 2400;
    pMastConf->kThres.sts48c       = 2400;
    pMastConf->kThres.sts192c      = 2400;
    pMastConf->kThres.sts768c      = 2400;
    
    /* Set VT path K-thresholds */
    pMastConf->kThres.vt15         = 600;
    pMastConf->kThres.vt2          = 600;
    pMastConf->kThres.vt3          = 600;
    pMastConf->kThres.vt6          = 600;
    
    /* Set DS3 path K-thresholds */
    pMastConf->kThres.ds3Line      = 44;
    pMastConf->kThres.ds3PathPbit  = 44;
    pMastConf->kThres.ds3PathCPbit = 44;
    
    /* Set DS1 path K-thresholds */
    pMastConf->kThres.ds1Line      = 1544;
    pMastConf->kThres.ds1EsfPath   = 320;
    pMastConf->kThres.ds1SfPath    = 4;
    
    /* Set default service mode */    
    pMastConf->defServMd   = cSurDefPoll;
    pSurInfo->pDb->pDefQue = null;
    
    /* Set default soaking time */
    pSurInfo->pDb->mastConf.decSoakTime = cSurDefaultDecSoakTime;
    pSurInfo->pDb->mastConf.terSoakTime = cSurDefaultTerSoakTime;

    return cSurOk;
    }

/*------------------------------------------------------------------------------
 Prototype    : eSurRet SurInit()
 
 Purpose      : This function is used to initialize Transmission Surveillance
                module. This function must be called before calling any
                functions of this module
 
 Inputs       : None
 
 Outputs      : None
 
 Return       : Surveillance return code
*-----------------------------------------------------------------------------*/
eSurRet SurDbInit(void)
    {
    word wIdx;
    
    /* Initialize semaphore */
    if (OsalSemInit(&(surDb.sem), cAtFalse, 1) != cOsalOk)
        {
        mSurDebug(cSurErrMsg, "Cannot initialize semaphore\n");
        return cSurErrSem;
        }
    
    /* Set all handles to null */
    for (wIdx = 0; wIdx < cSurMaxDev; wIdx++)
        {
        surDb.devices[wIdx] = null;
        }

    return cSurOk;
    }

eSurRet SurInit()
    {
    eSurRet ret = SurDbInit();
    if (ret != cSurOk)
        return ret;
    
    /* Start engine */
    SurEngStart();
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : public eSurRet SurFinalize()

Purpose      : This function is used to finalize Transmission Surveillance 
               module. This function must be called when this module is not 
               used no more to release all allocated resources and stop its 
               tasks

Inputs       : None

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet SurFinalize()
    {
    word    wIdx;
    eSurRet ret;
    
    /* Stop engine first */
    SurEngStop();
    
    /* Remove all device */
    for (wIdx = 0; wIdx < cSurMaxDev; wIdx++)
        {
        if (surDb.devices[wIdx] != null)
            {
            ret = SurDevRemove(&surDb.devices[wIdx]);
            if (ret != cSurOk)
                {
                mSurDebug(cSurErrMsg, "Cannot remove device\n");
                }
            }
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : private eSurRet SurDevReset(tSurDev device)

Purpose      : This function is used to reset database of one device. All 
               resources allocated for all channnels of this device will be 
               freed.

Inputs       : pDevice               - Device

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
private eSurRet SurDevReset(tSurDev device)
    {
    tSurInfo  *pSurInfo;
    word       wLine;
    word       wDe3;
    word       wDe1;
    word	   wImaIdx;
    word	   wPw;
    tSurLineId lineId;
    tSurDe3Db *pDe3Db;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get device database */
    pSurInfo = mDevDb(device);
    
    /* De-provision all lines */
    for (wLine = 0; wLine < cSurMaxLine; wLine++)
        {
        lineId = (tSurLineId)( wLine + 1);
        SurLineAllChnDeProv(pSurInfo, &lineId);
        OsalMemFree(pSurInfo->pDb->chnDb.pLine[wLine]);
        pSurInfo->pDb->chnDb.pLine[wLine] = null;
        }
    
    /* De-provision all Linear APS engines */
    for (wLine = 0; wLine < cSurMaxApslEng; wLine++)
        {
        if (mChnDb(pSurInfo).pApsl[wLine] != null)
            {
            SurApslInfoFinal(mChnDb(pSurInfo).pApsl[wLine]);
            OsalMemFree(mChnDb(pSurInfo).pApsl[wLine]);
            mChnDb(pSurInfo).pApsl[wLine] = null;
            }
        }
    
    /* De-provision all DS3s/E3s */
    for (wDe3 = 0; wDe3 < cSurMaxDe3Line; wDe3++)
        {
        pDe3Db = mChnDb(pSurInfo).pDe3[wDe3];
        if (pDe3Db != null)
            {
            if (pDe3Db->pInfo != null)
                {
                SurDe3InfoFinal(pDe3Db->pInfo);
                OsalMemFree(pDe3Db->pInfo);
                pDe3Db->pInfo = null;
                }
            
            /* De-provision all DS1s/E1s */
            for (wDe1 = 0; wDe1 < cSurMaxDe1InDe3; wDe1++)
                {
                if (pDe3Db->pDe1Db[wDe1] != null)
                    {
                    SurDe1InfoFinal(pDe3Db->pDe1Db[wDe1]);
                    OsalMemFree(pDe3Db->pDe1Db[wDe1]);
                    pDe3Db->pDe1Db[wDe1] = null;
                    }
                }
            OsalMemFree(mChnDb(pSurInfo).pDe3[wDe3]);
            mChnDb(pSurInfo).pDe3[wDe3] = null;
            }
        }
    
    /* De-provision all IMA Link */
	for (wImaIdx = 0; wImaIdx < cSurMaxImaLink; wImaIdx++)
		{
		if (mChnDb(pSurInfo).pImaLink[wImaIdx] != null)
            {
            SurImaLinkInfoFinal(mChnDb(pSurInfo).pImaLink[wImaIdx]);
            OsalMemFree(mChnDb(pSurInfo).pImaLink[wImaIdx]);
            mChnDb(pSurInfo).pImaLink[wImaIdx] = null;
            }
		}

	/* De-provision all IMA Group */
	for (wImaIdx = 0; wImaIdx < cSurMaxImaGrp; wImaIdx++)
		{
		if (mChnDb(pSurInfo).pImaGrp[wImaIdx] != null)
            {
            SurImaGrpInfoFinal(mChnDb(pSurInfo).pImaGrp[wImaIdx]);
            OsalMemFree(mChnDb(pSurInfo).pImaGrp[wImaIdx]);
            mChnDb(pSurInfo).pImaGrp[wImaIdx] = null;
            }
		}

	/* De-provision all PW */
	for (wPw = 0; wPw < cSurMaxPw; wPw++)
		{
		if (mChnDb(pSurInfo).pPw[wPw] != null)
            {
            SurPwInfoFinal(mChnDb(pSurInfo).pPw[wPw]);
            OsalMemFree(mChnDb(pSurInfo).pPw[wPw]);
            mChnDb(pSurInfo).pPw[wPw] = null;
            }
		}

    /* Clear defect queue */
    if (pSurInfo->pDb->pDefQue != null)
        {
        LQueFlush(pSurInfo->pDb->pDefQue->line);
        LQueFlush(pSurInfo->pDb->pDefQue->apsl);
        LQueFlush(pSurInfo->pDb->pDefQue->sts);
        LQueFlush(pSurInfo->pDb->pDefQue->vt);
        LQueFlush(pSurInfo->pDb->pDefQue->tu3);
        LQueFlush(pSurInfo->pDb->pDefQue->de3);
        LQueFlush(pSurInfo->pDb->pDefQue->de1);
        LQueFlush(pSurInfo->pDb->pDefQue->imaLink);
        LQueFlush(pSurInfo->pDb->pDefQue->imaGrp);
        LQueFlush(pSurInfo->pDb->pDefQue->pw);
        OsalMemFree(pSurInfo->pDb->pDefQue);
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevAdd(tSurDevHdl        *pHdl, 
                                 tSurDev           *pDevice,
                                 const tSurDevIntf *pIntf);

Purpose      : This function is used to add a new device to monitor failures 
               and performance

Inputs       : pHdl                  - Device handle
               pIntf                 - Device interface
            
Outputs      : pDevice               - device

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurDevAdd(tSurDevHdl        *pDeviceHdl, 
                             tSurDev           *pDevice,
                             const tSurDevIntf *pIntf)
    {
    eSurRet      ret;
    word         wIdx;
    tSurInfo    *pSurInfo;
    
    /* Take semaphore */
    ret = mDbTake();
    mRetCheck(ret);
    
    /* Find an available slot */
    ret = cSurOk;
    for (wIdx = 0; wIdx < cSurMaxDev; wIdx++)
        {
        if (surDb.devices[wIdx] == null)
            {
            /* Create device */
            pSurInfo = OsalMemAlloc(sizeof(tSurInfo));
            if (pSurInfo != null)
                {
                OsalMemInit(pSurInfo, sizeof(tSurInfo), 0);
                
                /* Allocate database for device */
                pSurInfo->pDb = OsalMemAlloc(sizeof(tSurDevDb));
                if (pSurInfo->pDb == null)
                    {
                    mSurDebug(cSurErrMsg, "Cannot allocate resources\n");
                    ret = mDbGive();
                    return cSurErrRsrNoAvai;
                    }
                OsalMemInit(pSurInfo->pDb, sizeof(tSurDevDb), 0);
                
                /* Set device handle */
                pSurInfo->pHdl = pDeviceHdl;
                
                /* Initialize semaphore */
                if (OsalSemInit(&(pSurInfo->sem), cAtFalse, 1) != cOsalOk)
                    {
                    mSurDebug(cSurErrMsg, "Cannot initialize semaphore\n");
                    
                    /* Free allocated memory and give semaphore */
                    OsalMemFree(pSurInfo);
                    ret = mDbGive();
                    
                    return cSurErrSem;
                    }
                
                /* Set device interface */
                if (pIntf != null)
                    {
                    OsalMemCpy((void*) pIntf,
                               (void*)(&(pSurInfo->interface)), sizeof(tSurDevIntf));
                    }
                else
                    {
                    OsalMemInit(&(pSurInfo->interface), sizeof(tSurDevIntf), 0);
                    }
                
                /* Add to database and return the handle */
                surDb.devices[wIdx] = pSurInfo;
                *pDevice            = pSurInfo;
                }
            
            /* Cannot allocate resources */
            else
                {
                mSurDebug(cSurErrMsg, "Cannot allocate resources\n");
                ret = mDbGive();
                return cSurErrRsrNoAvai;
                }
            
            /* Initialize device */
            ret = SurDevInit(*pDevice);
            if (ret != cSurOk)
                {
                mSurDebug(cSurErrMsg, "Cannot initialize device\n");
                wIdx = mDbGive();
                return ret;
                }
            
            break;
            }
        }
    
    /* There is no resource left */
    if (wIdx == cSurMaxDev)
        {
        mSurDebug(cSurErrMsg, "No resource left\n");
        ret = mDbGive();
        return cSurErrRsrNoAvai;
        }
    
    /* Give semaphore */
    ret = mDbGive();
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevRemove(tSurDev pDevice)

Purpose      : This function is used to remove a device from database, Fault 
               Monitoring and Performance Monitoring of this device will be 
               disabled.

Inputs       : pDevice               - Device

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDevRemove(tSurDev *pDevice)
    {
    eSurRet       ret;
    word          wIdx;
    tSurInfo     *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pDevice))
        {
        return cSurErrDevNotFound;
        }
    
    /* Take semaphore */
    ret = mDbTake();
    mRetCheck(ret);
    
    /* Find device */
    for (wIdx = 0; wIdx < cSurMaxDev; wIdx++)
        {
        if ((surDb.devices[wIdx] != null) &&
            (surDb.devices[wIdx] == *pDevice))
            {
            break;
            }
        }
    
    /* Device found */
    if (wIdx < cSurMaxDev)
        {
        pSurInfo = mDevTake(surDb.devices[wIdx]);
        SurDevReset(surDb.devices[wIdx]);
        
        /* Remove device from database */
        OsalMemFree(pSurInfo->pDb);
        if (OsalSemDestroy(&(pSurInfo->sem)) != cOsalOk)
            {
            mSurDebug(cSurWarnMsg, "Cannot destroy semaphore\n");
            }
        OsalMemFree(surDb.devices[wIdx]);
        surDb.devices[wIdx] = null;
        *pDevice            = null;
        }
    
    /* Give semaphore */
    ret = mDbGive();
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : const tSurDevHdl *SurDevHdlGet(tSurDev device)

Purpose      : This function is used to get device handle

Inputs       : device                - Device

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
const tSurDevHdl *SurDevHdlGet(tSurDev device)
    {
    /* Check paramters' validity */
    if (mIsNull(device))
        {
        return null;
        }
    
    return mDevDb(device)->pHdl;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevInit(tSurDev pDevice)

Purpose      : This function is used to initialize Surveillance database of a 
               device

Inputs       : pDevice               - Device

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDevInit(tSurDev device)
    {
    tSurInfo *pSurInfo;
    eSurRet   ret;
    
    /* Get handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot get handle of device\n");
        return cSurErrNullParm;
        }
    
    /* Reset database to free all allocated resources */
    SurDevReset(device);
    
    /* Initialize */
    OsalMemInit(pSurInfo->pDb, sizeof(tSurDevDb), 0);
    pSurInfo->pDb->state = cSurStop;
    SurCurTimeInS(&(pSurInfo->pDb->time));
    
    /* Initialize channel DB */
    ret = SurChnDbInit(pSurInfo->pDb);
    if ( ret != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for channel DB\n");
        ret = mDbGive();
        return cSurErrRsrNoAvai;
        }
    
    /* Set default configuration for the device */
    ret = SurDefMastConfSet(pSurInfo);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default configuration\n");
        ret = mDbGive();
        return cSurErr;
        }

    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevIntfSet(tSurDev device, const tSurDevIntf *pIntf)

Purpose      : This function is used to set device interface.

Inputs       : device                - Device
               pIntf                 - Device interface

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDevIntfSet(tSurDev device, const tSurDevIntf *pIntf)
    {
    tSurInfo *pSurInfo;
    eSurRet   ret;
    
    
    /* Get handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot get handle of device\n");
        return cSurErrDevNotFound;
        }
    
    /* Set device interface */
    if (pIntf != null)
        {
        OsalMemCpy((void*)pIntf, &(pSurInfo->interface), sizeof(tSurDevIntf));
        }
    else
        {
        return cSurErrNullParm;
        }
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevIntfGet(tSurDev device, tSurDevIntf *pIntf)

Purpose      : This function is used to get device interface.

Inputs       : device                - Device

Outputs      : pIntf                 - Device interface

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDevIntfGet(tSurDev device, tSurDevIntf *pIntf)
    {
    tSurInfo *pSurInfo;
    eSurRet   ret;
    
    /* Get handle */
    if ((pSurInfo = mDevDb(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot get handle of device\n");
        return cSurErrDevNotFound;
        }
    
    /* Get device interface */
    if (pIntf != null)
        {
        OsalMemCpy(&(pSurInfo->interface), pIntf, sizeof(tSurDevIntf));
        }
    else
        {
        return cSurErrNullParm;
        }
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevStatSet(tSurDev device, eSurEngStat state)

Purpose      : This function is used to set state for a device (to make engine 
               start or stop)

Inputs       : device                - Device
               state                 - Engine state

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDevStatSet(tSurDev device, eSurEngStat state)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
        
    /* Change engine state */
    if (state != pSurInfo->pDb->state)
        {
        pSurInfo->pDb->state = state;
        SurCurTimeInS(&(pSurInfo->pDb->time));
        }
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDevStatGet(tSurDev device, eSurEngStat *pState)

Purpose      : This function is used to get state of a device to check if engine 
               is running or stop

Inputs       : device                - Device

Outputs      : pState                - Engine status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDevStatGet(tSurDev device, eSurEngStat *pState)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Get engine state */
    *pState  = pSurInfo->pDb->state;
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDefServMdSet(tSurDev device, eSurDefServMd servMd)

Purpose      : This function is used to set defect service mode. 
               - In service mode, engine will serve defects sent from other 
                 tasks and update engine status
               - In polling mode, engine will poll hardware periodically to 
                 defect new detects and update engine status.

Inputs       : device                - device
               servMd                - defect service mode 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDefServMdSet(tSurDev device, eSurDefServMd servMd)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Set defect servicing mode */
    if (pSurInfo->pDb->mastConf.defServMd != servMd)
        {
        pSurInfo->pDb->mastConf.defServMd = servMd;
        
        /* Allocate memmory for the defect queue if the device is changing to
           Service mode */
        if (servMd == cSurDefServ)
            {
            pSurInfo->pDb->pDefQue = OsalMemAlloc(sizeof(tSurDefQue));
            OsalMemInit(pSurInfo->pDb->pDefQue, sizeof(tSurDefQue), 0);
            
            LQueCreate(&(pSurInfo->pDb->pDefQue->line));
            LQueCreate(&(pSurInfo->pDb->pDefQue->apsl));
            LQueCreate(&(pSurInfo->pDb->pDefQue->sts));
            LQueCreate(&(pSurInfo->pDb->pDefQue->vt));
            LQueCreate(&(pSurInfo->pDb->pDefQue->tu3));
            LQueCreate(&(pSurInfo->pDb->pDefQue->de3));
            LQueCreate(&(pSurInfo->pDb->pDefQue->de1));
            LQueCreate(&(pSurInfo->pDb->pDefQue->imaLink));
            LQueCreate(&(pSurInfo->pDb->pDefQue->imaGrp));
            LQueCreate(&(pSurInfo->pDb->pDefQue->pw));
            }
            
        /* Deallocate mempry of the defect queue if the device is changing to
           Polling mode */
        else
            {
            LQueFlush(pSurInfo->pDb->pDefQue->line);
            LQueFlush(pSurInfo->pDb->pDefQue->apsl);
            LQueFlush(pSurInfo->pDb->pDefQue->sts);
            LQueFlush(pSurInfo->pDb->pDefQue->vt);
            LQueFlush(pSurInfo->pDb->pDefQue->tu3);
            LQueFlush(pSurInfo->pDb->pDefQue->de3);
            LQueFlush(pSurInfo->pDb->pDefQue->de1);
            LQueFlush(pSurInfo->pDb->pDefQue->imaLink);
            LQueFlush(pSurInfo->pDb->pDefQue->imaGrp);
            LQueFlush(pSurInfo->pDb->pDefQue->pw);
            
            OsalMemFree(pSurInfo->pDb->pDefQue);
            pSurInfo->pDb->pDefQue = null;
            }
        }
        
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDefServMdGet(tSurDev device, eSurDefServMd *pServMd)

Purpose      : This function is used to get defect service mode.
               - In service mode, engine will serve defects sent from other 
                 tasks and update engine status
               - In polling mode, engine will poll hardware periodically to 
                 detect new defects and update engine status.

Inputs       : device                - Device

Outputs      : pServMd               - Defect service mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurDefServMdGet(tSurDev device, eSurDefServMd *pServMd)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    if (mIsNull(pServMd))
        {
        return cSurErrNullParm;
        }
    
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Set defect service mode */
    *pServMd = pSurInfo->pDb->mastConf.defServMd;
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurKThresSet(tSurDev device, const tSurKThres *pKThres)

Purpose      : This function is used to set K thresholds for SONET/SDH and PDH

Inputs       : device                - Device
               pKThres               - K threshold configuration

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurKThresSet(tSurDev device, const tSurKThres *pKThres)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    if (mIsNull(pKThres))
        {
        return cSurErrNullParm;
        }
    
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Set configuration */
    OsalMemCpy((void *)pKThres, 
               (void *)&(pSurInfo->pDb->mastConf.kThres), 
               sizeof(tSurKThres));
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurKThresGet(tSurDev device, tSurKThres *pKThres)

Purpose      : This function is used to get K thresholds of SONET/SDH and PDH

Inputs       : device                - Device

Outputs      : pKThres               - K threshold configuration

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurKThresGet(tSurDev device, tSurKThres *pKThres)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    if (mIsNull(pKThres))
        {
        return cSurErrNullParm;
        }
        
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Get configuration */
    OsalMemCpy((void *)&(pSurInfo->pDb->mastConf.kThres), 
               (void *)pKThres,
               sizeof(tSurKThres));
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurColorProfSet(tSurDev              device, 
                                       const tSurColorProf *pColorProf)

Purpose      : This function is used to set color profile for a device

Inputs       : device                - Device
               pColorProf            - Color profile

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurColorProfSet(tSurDev device, const tSurColorProf *pColorProf)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    if (mIsNull(pColorProf))
        {
        return cSurErrNullParm;
        }
        
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Set color profile */
    OsalMemCpy((void *)pColorProf, 
               (void *)&(pSurInfo->pDb->mastConf.colorProf),
               sizeof(tSurColorProf));
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurColorProfGet(tSurDev device, tSurColorProf *pColorProf)

Purpose      : This function is used to get color profile of a device

Inputs       : device                - Device

Outputs      : pColorProf            - Color profile

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurColorProfGet(tSurDev device, tSurColorProf *pColorProf)
    {
    eSurRet   ret;
    tSurInfo *pSurInfo;
    
    /* Initialize */
    ret = cSurOk;
    
    if (mIsNull(pColorProf))
        {
        return cSurErrNullParm;
        }
        
    /* Take device */
    pSurInfo = mDevTake(device);
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
    
    /* Get color profile */
    OsalMemCpy((void *)&(pSurInfo->pDb->mastConf.colorProf), 
               (void *)pColorProf,
               sizeof(tSurColorProf));
    
    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTrblDesgnSet(tSurDev              device, 
                                       eSurChnType          chnType, 
                                       byte                 trblType, 
                                       const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set trouble designation for a device.

Inputs       : device                - device
               chnType               - channel type
               trblType              - trouble type
               pTrblDesgn            - trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurTrblDesgnSet(tSurDev              device, 
                               eSurChnType          chnType, 
                               byte                 trblType, 
                               const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    switch (chnType)
        {   
            /* Linear APS channel */
            case cSurApslEng:
                ret = SurApslTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
            
            /* section/line channel */
            case cSurLine:
                ret = SurLineTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
            
            /* STS channel */
            case cSurSts:
                ret = SurStsTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
                
            /* AU-n channel */
            case cSurAu:
                ret = SurAuTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
                
            /* TU-3 channel */
            case cSurTu3:
                ret = SurAuTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
            
            /* VT channel */
            case cSurVt:
                ret = SurVtTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
                
            /* TU-m channel */
            case cSurTu:
                ret = SurTuTrblDesgnSet(device, trblType, pTrblDesgn);
                break;
            
            /* DS3/E3 channel */
            case cSurDe3:
                ret = SurDe3TrblDesgnSet(device, trblType, pTrblDesgn);
                break;
                
            /* DS1/E1 channel */
            case cSurDe1:
                ret = SurDe1TrblDesgnSet(device, trblType, pTrblDesgn);
                break;
                
            /* IMA Link */
            case cSurImaLink:
                ret = SurImaLinkTrblDesgnSet(device, trblType, pTrblDesgn);
                break;

            /* IMA Group */
            case cSurImaGrp:
                ret = SurImaGrpTrblDesgnSet(device, trblType, pTrblDesgn);
                break;

            /* PW */
            case cSurPw:
                ret = SurPwTrblDesgnSet(device, trblType, pTrblDesgn);
                break;

            /* Invalid channel type */
            default:
                return cSurErrInvlParm;
        }
    
    if (ret != cSurOk)
        {
        return ret;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTrblDesgnGet(tSurDev        device, 
                                       eSurChnType    chnType, 
                                       byte           trblType, 
                                       tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get trouble designation of a device.

Inputs       : device                - device
               chnType               - channel type
               trblType              - trouble type
               
Outputs      : pTrblDesgn            - trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurTrblDesgnGet(tSurDev        device, 
                               eSurChnType    chnType, 
                               byte           trblType, 
                               tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    switch (chnType)
        {   
            /* Linear APS channel */
            case cSurApslEng:
                ret = SurApslTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
                
            /* section/line channel */
            case cSurLine:
                ret = SurLineTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
            
            /* STS channel */
            case cSurSts:
                ret = SurStsTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
                
            /* AU-n channel */
            case cSurAu:
                ret = SurAuTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
            
            /* TU-3 channel */
            case cSurTu3:
                ret = SurAuTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
            
            /* VT channel */
            case cSurVt:
                ret = SurVtTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
                
            /* TU-m channel */
            case cSurTu:
                ret = SurTuTrblDesgnGet(device, trblType, pTrblDesgn);
                break;
            
            /* DS3/E3 channel */
            case cSurDe3:
                ret = SurDe3TrblDesgnGet(device, trblType, pTrblDesgn);
                break;
            
            /* DS1/E1 channel */
            case cSurDe1:
                ret = SurDe1TrblDesgnGet(device, trblType, pTrblDesgn);
                break;
            
            /* IMA Link */
            case cSurImaLink:
                ret = SurImaLinkTrblDesgnGet(device, trblType, pTrblDesgn);
                break;

            /* IMA Group */
            case cSurImaGrp:
                ret = SurImaGrpTrblDesgnGet(device, trblType, pTrblDesgn);
                break;

            /* PW */
            case cSurPw:
                ret = SurPwTrblDesgnGet(device, trblType, pTrblDesgn);
                break;

            /* Invalid channel type */
            default:
                return cSurErrInvlParm;
        }
    
    if (ret != cSurOk)
        {
        return ret;
        }
    
    return cSurOk;
    }
                        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurEngStatSet(tSurDev        device, 
                                     eSurChnType    chnType, 
                                     const void    *pChnId, 
                                     eSurEngStat    stat)

Purpose      : This API is used to set engine state for a channel.

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurEngStatSet(tSurDev       device, 
                             eSurChnType   chnType, 
                             const void   *pChnId, 
                             eSurEngStat   stat)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Set engine stat */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            ret = SurApslEngStatSet(device, (tSurApslId *)pChnId, stat);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            ret = SurLineEngStatSet(device, (tSurLineId *)pChnId, stat);
            break;
            
        /* STS path */
        case cSurSts:
            ret = SurStsEngStatSet(device, (tSurStsId *)pChnId, stat);
            break;
            
        /* VT path */
        case cSurVt:
            ret = SurVtEngStatSet(device, (tSurVtId *)pChnId, stat);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = SurAuEngStatSet(device, (tSurAuId *)pChnId, stat);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = SurTu3EngStatSet(device, (tSurTu3Id *)pChnId, stat);
            break;
            
        /* TU path */
        case cSurTu:
            ret = SurTuEngStatSet(device, (tSurTuId *)pChnId, stat);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = SurDe3EngStatSet(device, (tSurDe3Id *)pChnId, stat);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = SurDe1EngStatSet(device, (tSurDe1Id *)pChnId, stat);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkEngStatSet(device, (tSurImaLinkId *)pChnId, stat);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpEngStatSet(device, (tSurImaGrpId *)pChnId, stat);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwEngStatSet(device, (tSurPwId *)pChnId, stat);
            break;

        /* Invalid channel type */
        default:
            return cSurErrInvlParm;
        }
        
    if (ret != cSurOk)
        {
        return ret;
        }
        
    return cSurOk;      
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurEngStatGet(tSurDev      device, 
                                     eSurChnType  chnType, 
                                     const void  *pChnId, 
                                     eSurEngStat *pStat)

Purpose      : This API is used to get engine state of a channel.

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pStat                  - engine state

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurEngStatGet(tSurDev       device, 
                             eSurChnType   chnType, 
                             const void   *pChnId, 
                             eSurEngStat  *pStat)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Set engine stat */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            ret = SurApslEngStatGet(device, (tSurApslId *)pChnId, pStat);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            ret = SurLineEngStatGet(device, (tSurLineId *)pChnId, pStat);
            break;
            
        /* STS path */
        case cSurSts:
            ret = SurStsEngStatGet(device, (tSurStsId *)pChnId, pStat);
            break;
            
        /* VT path */
        case cSurVt:
            ret = SurVtEngStatGet(device, (tSurVtId *)pChnId, pStat);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = SurAuEngStatGet(device, (tSurAuId *)pChnId, pStat);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = SurTu3EngStatGet(device, (tSurTu3Id *)pChnId, pStat);
            break;
            
        /* TU path */
        case cSurTu:
            ret = SurTuEngStatGet(device, (tSurTuId *)pChnId, pStat);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = SurDe3EngStatGet(device, (tSurDe3Id *)pChnId, pStat);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = SurDe1EngStatGet(device, (tSurDe1Id *)pChnId, pStat);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkEngStatGet(device, (tSurImaLinkId *)pChnId, pStat);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpEngStatGet(device, (tSurImaGrpId *)pChnId, pStat);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwEngStatGet(device, (tSurPwId *)pChnId, pStat);
            break;

        /* Invalid channel type */
        default:
            return cSurErrInvlParm;
        }
        
    return ret;     
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurChnCfgSet(tSurDev         device, 
                                    eSurChnType     chnType, 
                                    const void     *pChnId, 
                                    const void     *pChnCfg)

Purpose      : This API is used to set a channel's configuration.

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurChnCfgSet(tSurDev        device, 
                            eSurChnType    chnType, 
                            const void    *pChnId, 
                            const void    *pChnCfg)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Set configuration */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            ret = SurApslChnCfgSet(device, (tSurApslId *)pChnId, (tSurApslConf *)pChnCfg);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            ret = SurLineChnCfgSet(device, (tSurLineId *)pChnId, (tSurLineConf *)pChnCfg);
            break;
            
        /* STS path */
        case cSurSts:
            ret = SurStsChnCfgSet(device, (tSurStsId *)pChnId, (tSurStsConf *)pChnCfg);
            break;
            
        /* VT path */
        case cSurVt:
            ret = SurVtChnCfgSet(device, (tSurVtId *)pChnId, (tSurVtConf *)pChnCfg);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = SurAuChnCfgSet(device, (tSurAuId *)pChnId, (tSurStsConf *)pChnCfg);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = SurTu3ChnCfgSet(device, (tSurTu3Id *)pChnId, (tSurStsConf *)pChnCfg);
            break;
            
        /* TU path */
        case cSurTu:
            ret = SurTuChnCfgSet(device, (tSurTuId *)pChnId, (tSurVtConf *)pChnCfg);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = SurDe3ChnCfgSet(device, (tSurDe3Id *)pChnId, (tSurDe3Conf *)pChnCfg);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = SurDe1ChnCfgSet(device, (tSurDe1Id *)pChnId, (tSurDe1Conf *)pChnCfg);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkChnCfgSet(device, (tSurImaLinkId *)pChnId, (tSurImaLinkConf *)pChnCfg);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpChnCfgSet(device, (tSurImaGrpId *)pChnId, (tSurImaGrpConf *)pChnCfg);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwChnCfgSet(device, (tSurPwId *)pChnId, (tSurPwConf *)pChnCfg);
            break;

        /* Invalid channel type */
        default:
            ret =  cSurErrInvlParm;
        }
        
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurChnCfgGet(tSurDev         device, 
                                    eSurChnType     chnType, 
                                    const void     *pChnId, 
                                    void           *pChnCfg)

Purpose      : This API is used to get channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurChnCfgGet(tSurDev        device, 
                            eSurChnType    chnType, 
                            const void    *pChnId, 
                            void          *pChnCfg)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Set configuration */
    switch (chnType)
        {
        /* line APS */
        case cSurApslEng:
            ret = SurApslChnCfgGet(device, (tSurApslId *)pChnId, (tSurApslConf *)pChnCfg);
            break;
            
        /* SONET/SDH line */
        case cSurLine:
            ret = SurLineChnCfgGet(device, (tSurLineId *)pChnId, (tSurLineConf *)pChnCfg);
            break;
            
        /* STS path */
        case cSurSts:
            ret = SurStsChnCfgGet(device, (tSurStsId *)pChnId, (tSurStsConf *)pChnCfg);
            break;
            
        /* VT path */
        case cSurVt:
            ret = SurVtChnCfgGet(device, (tSurVtId *)pChnId, (tSurVtConf *)pChnCfg);
            break;
            
        /* AU-n path */
        case cSurAu:
            ret = SurAuChnCfgGet(device, (tSurAuId *)pChnId, (tSurStsConf *)pChnCfg);
            break;
            
        /* Tu-3 path */
        case cSurTu3:
            ret = SurTu3ChnCfgGet(device, (tSurTu3Id *)pChnId, (tSurStsConf *)pChnCfg);
            break;
            
        /* TU path */
        case cSurTu:
            ret = SurTuChnCfgGet(device, (tSurTuId *)pChnId, (tSurVtConf *)pChnCfg);
            break;
            
        /* DS3/E3 path */
        case cSurDe3:
            ret = SurDe3ChnCfgGet(device, (tSurDe3Id *)pChnId, (tSurDe3Conf *)pChnCfg);
            break;
            
        /* DS1/E1 path */
        case cSurDe1:
            ret = SurDe1ChnCfgGet(device, (tSurDe1Id *)pChnId, (tSurDe1Conf *)pChnCfg);
            break;
            
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkChnCfgGet(device, (tSurImaLinkId *)pChnId, (tSurImaLinkConf *)pChnCfg);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpChnCfgGet(device, (tSurImaGrpId *)pChnId, (tSurImaGrpConf *)pChnCfg);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwChnCfgGet(device, (tSurPwId *)pChnId, (tSurPwConf *)pChnCfg);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
        
    return ret;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTrblNotfInhSet(tSurDev       device, 
                                         eSurChnType   chnType, 
                                         const void   *pChnId, 
                                         byte          trblType, 
                                         bool          inhibit)

Purpose      : This API is used to set trouble notification inhibition for a
               channel.

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - pChnId
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurTrblNotfInhSet(tSurDev       device, 
                                 eSurChnType   chnType, 
                                 const void   *pChnId, 
                                 byte          trblType, 
                                 bool          inhibit)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Set trouble notification inhibition (depend on channel type)*/
    switch (chnType)
        {
        /* Linear APS channel */
        case cSurApslEng:
            ret = SurApslTrblNotfInhSet(device, 
                                        (tSurApslId *)pChnId, 
                                        trblType, 
                                        inhibit);
            break;
        
        /* Section/line channel */
        case cSurLine:
            ret = SurLineTrblNotfInhSet(device, 
                                        (tSurLineId *)pChnId,
                                        trblType,
                                        inhibit);
            break;
        
        /* STS channel */
        case cSurSts:
            ret = SurStsTrblNotfInhSet(device,
                                       (tSurStsId *)pChnId,
                                       trblType,
                                       inhibit);
            break;    
            
        /* AU-n channel */
        case cSurAu:
            ret = SurAuTrblNotfInhSet(device,
                                      (tSurAuId *)pChnId,
                                      trblType,
                                      inhibit);
            break;
        
        /* TU-3 channel */
        case cSurTu3:
            ret = SurTu3TrblNotfInhSet(device,
                                       (tSurTu3Id *)pChnId,
                                       trblType,
                                       inhibit);
            break;
        
        /* VT channel */
        case cSurVt:
            ret = SurVtTrblNotfInhSet(device,
                                      (tSurVtId *)pChnId,
                                      trblType,
                                      inhibit);
            break;
        
        /* TU-m channel */
        case cSurTu:
            ret = SurTuTrblNotfInhSet(device,
                                      (tSurTuId *)pChnId,
                                      trblType,
                                      inhibit);
            break;
        
        /* DS3/E3 channel */
        case cSurDe3:
            ret = SurDe3TrblNotfInhSet(device,
                                      (tSurDe3Id *)pChnId,
                                      trblType,
                                      inhibit);
            break;
        
        /* DS1/E1 channel */
        case cSurDe1:
            ret = SurDe1TrblNotfInhSet(device,
                                       (tSurDe1Id *)pChnId,
                                       trblType,
                                       inhibit);
            break;
        
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkTrblNotfInhSet(device,
										   (tSurImaLinkId *)pChnId,
										   trblType,
										   inhibit);
            break;

       /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpTrblNotfInhSet(device,
										   (tSurImaGrpId *)pChnId,
										   trblType,
										   inhibit);
            break;

       /* PW */
        case cSurPw:
            ret = SurPwTrblNotfInhSet(device,
									   (tSurPwId *)pChnId,
									   trblType,
									   inhibit);
            break;

       /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
    
    return ret;
    }                          

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurTrblNotfInhGet(tSurDev       device, 
                                         eSurChnType   chnType, 
                                         const void   *pChnId, 
                                         byte          trblType, 
                                         bool         *pInhibit)

Purpose      : This API is used to get trouble notification inhibition for a
               channel.

Inputs       : device                 - device
               pChnType               - channel type
               pChnId                 - pChnId
               trblType               - trouble type

Outputs      : inhibit                - true/fase

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurTrblNotfInhGet(tSurDev       device, 
                                 eSurChnType   chnType, 
                                 const void   *pChnId, 
                                 byte          trblType, 
                                 bool         *pInhibit)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Get trouble notification inhibition (depend on channel type) */
    switch (chnType)
        {
        /* Linear APS channel */
        case cSurApslEng:
            ret = SurApslTrblNotfInhGet(device,
                                        (tSurApslId *)pChnId,
                                        trblType,
                                        pInhibit);
            break;
        
        /* Section/line channel */
        case cSurLine:
            ret = SurLineTrblNotfInhGet(device,
                                        (tSurLineId *)pChnId,
                                        trblType,
                                        pInhibit);
            break;
        
        /* STS channel */
        case cSurSts:
            ret = SurStsTrblNotfInhGet(device,
                                       (tSurStsId *)pChnId,
                                       trblType,
                                       pInhibit);
            break;
        
        /* AU-n channel */
        case cSurAu:
            ret = SurAuTrblNotfInhGet(device,
                                      (tSurAuId *)pChnId,
                                      trblType,
                                      pInhibit);
            break;
        
        /* TU-3 channel */
        case cSurTu3:
            ret = SurTu3TrblNotfInhGet(device,
                                       (tSurTu3Id *)pChnId,
                                       trblType,
                                       pInhibit);
            break;
        
        /* VT channel */
        case cSurVt:
            ret = SurVtTrblNotfInhGet(device,
                                      (tSurVtId *)pChnId,
                                      trblType,
                                      pInhibit);
            break;
        
        /* TU-m channel */
        case cSurTu:
            ret = SurTuTrblNotfInhGet(device,
                                      (tSurTuId *)pChnId,
                                      trblType,
                                      pInhibit);
            break;
            
        /* DS3/E3 channel */
        case cSurDe3:
            ret = SurDe3TrblNotfInhGet(device,
                                       (tSurDe3Id *)pChnId,
                                       trblType,
                                       pInhibit);
            break;
        
        /* DS1/E1 channel */
        case cSurDe1:
            ret = SurDe1TrblNotfInhGet(device,
                                       (tSurDe1Id *)pChnId,
                                       trblType,
                                       pInhibit);
            break;
        
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkTrblNotfInhGet(device,
										   (tSurImaLinkId *)pChnId,
										   trblType,
										   pInhibit);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpTrblNotfInhGet(device,
										   (tSurImaGrpId *)pChnId,
										   trblType,
										   pInhibit);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwTrblNotfInhGet(device,
									   (tSurPwId *)pChnId,
									   trblType,
									   pInhibit);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
    
    return ret;
    }                            

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurChnTrblNotfInhInfoGet(tSurDev        device, 
                                                eSurChnType    chnType, 
                                                const void    *pChnId, 
                                                void          *pTrblInh)

Purpose      : This API is used to get a channel's trouble notification inhibition.

Inputs       : device                - device
               chnType               - channel type
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurChnTrblNotfInhInfoGet(tSurDev        device, 
                                        eSurChnType    chnType, 
                                        const void    *pChnId, 
                                        void          *pTrblInh)
    {
    /* Declare local variables */
    eSurRet        ret;
    
    /* Get trouble notification inhibition (depend on channel type) */
    switch (chnType)
        {
        /* Linear APS channel */
        case cSurApslEng:
            ret = SurApslChnTrblNotfInhInfoGet(device, 
                                               (tSurApslId *)pChnId, 
                                               (tSurApslMsgInh *)pTrblInh);
            break;
        
        /* Section/line channel */
        case cSurLine:
            ret = SurLineChnTrblNotfInhInfoGet(device, 
                                               (tSurLineId *)pChnId, 
                                               (tSurLineMsgInh *)pTrblInh);
            break;
        
        /* STS channel */
        case cSurSts:
            ret = SurStsChnTrblNotfInhInfoGet(device, 
                                              (tSurStsId *)pChnId, 
                                              (tSurStsMsgInh *)pTrblInh);
            break;
        
        /* AU-n channel */
        case cSurAu:
            ret = SurAuChnTrblNotfInhInfoGet(device, 
                                             (tSurAuId *)pChnId, 
                                             (tSurStsMsgInh *)pTrblInh);
            break;
        
        /* TU-3 channel */
        case cSurTu3:
            ret = SurTu3ChnTrblNotfInhInfoGet(device, 
                                              (tSurTu3Id *)pChnId, 
                                              (tSurStsMsgInh *)pTrblInh);
            break;
        
        /* VT channel */
        case cSurVt:
            ret = SurVtChnTrblNotfInhInfoGet(device, 
                                             (tSurVtId *)pChnId, 
                                             (tSurVtMsgInh *)pTrblInh);
            break;
        
        /* TU-m channel */
        case cSurTu:
            ret = SurTuChnTrblNotfInhInfoGet(device, 
                                             (tSurTuId *)pChnId, 
                                             (tSurVtMsgInh *)pTrblInh);
            break;
            
        /* DS3/E3 channel */
        case cSurDe3:
            ret = SurDe3ChnTrblNotfInhInfoGet(device, 
                                              (tSurDe3Id *)pChnId, 
                                              (tSurDe3MsgInh *)pTrblInh);
            break;
        
        /* DS1/E1 channel */
        case cSurDe1:
            ret = SurDe1ChnTrblNotfInhInfoGet(device, 
                                              (tSurDe1Id *)pChnId, 
                                              (tSurDe1MsgInh *)pTrblInh);
            break;
        
        /* IMA Link */
        case cSurImaLink:
            ret = SurImaLinkChnTrblNotfInhInfoGet(device,
                                              (tSurImaLinkId *)pChnId,
                                              (tSurImaLinkMsgInh *)pTrblInh);
            break;

        /* IMA Group */
        case cSurImaGrp:
            ret = SurImaGrpChnTrblNotfInhInfoGet(device,
                                              (tSurImaGrpId *)pChnId,
                                              (tSurImaGrpMsgInh *)pTrblInh);
            break;

        /* PW */
        case cSurPw:
            ret = SurPwChnTrblNotfInhInfoGet(device,
                                              (tSurPwId *)pChnId,
                                              (tSurPwMsgInh *)pTrblInh);
            break;

        /* Invalid channel type */
        default:
            ret = cSurErrInvlParm;
        }
    
    return ret;
    }                                 

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurDefFetch(tSurDevHdl   *pDevHandle, 
                                   eSurChnType   chnType, 
                                   void         *pDefStat)

Purpose      : This API is used to fetch a defect for the surveillance module 
               when it is running in service mode

Inputs       : device                 - device
               chnType                - channel type
               pDefStat               - fetched defect's status
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*public*/ eSurRet SurDefFetch(tSurDevHdl *pDevHandle, eSurChnType chnType, void *pDefStat)
    {
    tSurInfo       *pSurInfo;
    eSurRet         ret; 
    tSurDefQue     *pDefQue;
    
    /* Check parameters' validity */
    if ((pDevHandle == null) || (pDefStat == null))
        {
        return cSurErrNullParm;
        }

    /* Get DB */
    mSurDeviceGet(pDevHandle, pSurInfo);
    /*pSurInfo = mDevTake(pSurInfo);*/
    if (pSurInfo == null)
        {
        return cSurErrDevNotFound;
        }
        
    /* Check if the device is in Service mode */   
    if (pSurInfo->pDb->mastConf.defServMd != cSurDefServ)
        {
        ret = mDevGive(pSurInfo);
        return cSurOk;
        }

    /* Insert defect based on channel type */
    pDefQue = pSurInfo->pDb->pDefQue;
    switch (chnType)
        {
        /* Linear APS defect */
        case cSurApslEng:
            mSurDebug(cSurInfoMsg, "not implemented yet\n");
            break;
            
        /* Line defect */
        case cSurLine:
            LQueIns(pDefQue->line, (tSurLineDef *)pDefStat, sizeof(tSurLineDef));
            break;
        
        /* STS/AU-n defect */
        case cSurSts:
        case cSurAu:
            LQueIns(pDefQue->sts, (tSurStsDef *)pDefStat, sizeof(tSurStsDef));
            break;
        
        case cSurTu3:
            LQueIns(pDefQue->tu3, (tSurTu3Def *)pDefStat, sizeof(tSurTu3Def));
            break;
            
        /* Vt/Tu-m defect */
        case cSurVt:
        case cSurTu:
            LQueIns(pDefQue->vt, (tSurVtDef *)pDefStat, sizeof(tSurVtDef));
            break;
            
        /* DS3/E3 defect */
        case cSurDe3:
            LQueIns(pDefQue->de3, (tSurDe3Def *)pDefStat, sizeof(tSurDe3Def));
            break;
        
        /* DS1/E1 defect */
        case cSurDe1:
            LQueIns(pDefQue->de1, (tSurDe1Def *)pDefStat, sizeof(tSurDe1Def));
            break;
            
        /* IMA Link defect */
        case cSurImaLink:
            LQueIns(pDefQue->imaLink, (tSurImaLinkDef *)pDefStat, sizeof(tSurImaLinkDef));
            break;

        /* IMA Group defect */
        case cSurImaGrp:
            LQueIns(pDefQue->imaGrp, (tSurImaGrpDef *)pDefStat, sizeof(tSurImaGrpDef));
            break;

        /* PW defect */
        case cSurPw:
            LQueIns(pDefQue->pw, (tSurPwDef *)pDefStat, sizeof(tSurPwDef));
            break;

        default:
            break;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(pSurInfo);
    mRetCheck(ret);
    
    return cSurOk;
    }                    

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurSoakTimeSet(tSurDev device, dword decSoakTime, dword terSoakTime)

Purpose      : This function is used to set soaking time to declare or terminate the failure

Inputs       : device                - Device
               decSoakTime           - Soaking time to declare the failure in seconds
               terSoakTime			 - Soaking time to terminate the failure in seconds

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurSoakTimeSet(tSurDev device, dword decSoakTime, dword terSoakTime)
    {
    tSurInfo *pSurInfo;
    eSurRet   ret;

    /* Get handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot get handle of device\n");
        return cSurErrNullParm;
        }

    pSurInfo->pDb->mastConf.decSoakTime = decSoakTime;
    pSurInfo->pDb->mastConf.terSoakTime = terSoakTime;

    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurSoakTimeSet(tSurDev device, dword decSoakTime, dword terSoakTime)

Purpose      : This function is used to get soaking time to declare or terminate the failure

Inputs       : device                - Device

Outputs      : decSoakTime           - Soaking time to declare the failure in seconds
               terSoakTime			 - Soaking time to terminate the failure in seconds

Return       : Surveillance return code
------------------------------------------------------------------------------*/
eSurRet SurSoakTimeGet(tSurDev device, dword *decSoakTime, dword *terSoakTime)
    {
    tSurInfo *pSurInfo;
    eSurRet   ret;

    /* Get handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot get handle of device\n");
        return cSurErrNullParm;
        }

    *decSoakTime = pSurInfo->pDb->mastConf.decSoakTime;
    *terSoakTime = pSurInfo->pDb->mastConf.terSoakTime;

    /* Give device */
    ret = mDevGive(device);
    mRetCheck(ret);

    return cSurOk;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - VT
 *
 * File        : survt.c
 *
 * Created Date: 14-Jan-09
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of VT
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtDefParmSet(tSurVtInfo *pInfo)

Purpose      : This function is used to set default values for VT path's PM 
               parameters.

Inputs       : pInfo                 - line channel info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtDefParmSet(tSurVtInfo *pInfo)
    {
    /* Near-end VT path parameters */
    mPmDefParmSet(&(pInfo->parm.cvV), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esV), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesV), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasV), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcV), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.ppjcVdet), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.npjcVdet), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.ppjcVgen), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.npjcVgen), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.pjcDiffV), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.pjcsVdet), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.pjcsVgen), cPmPerThresVal, cPmDayThresVal);
    
    /* Far-end VT path parameters */
    mPmDefParmSet(&(pInfo->parm.cvVfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.esVfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.sesVfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uasVfe), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.fcVfe), cPmPerThresVal, cPmDayThresVal);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : private tSurVtInfo *VtInfoCreate(tSurInfo  *pSurInfo, 
                                                tSurVtId  *pVtId)

Purpose      : This function is used to allocate resources for VT

Inputs       : pSurInfo              - database handle of device
               pVtId                 - VT id

Outputs      : None

Return       : tSurVtInfo*           - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurVtInfo *VtInfoCreate(tSurInfo *pSurInfo, tSurVtId *pVtId)
    {
    tSurVtInfo *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurVtInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurVtInfo), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.cvV));
        mPmParmInit(&(pChnInfo->parm.esV));
        mPmParmInit(&(pChnInfo->parm.sesV));
        mPmParmInit(&(pChnInfo->parm.uasV));
        mPmParmInit(&(pChnInfo->parm.fcV));
        mPmParmInit(&(pChnInfo->parm.ppjcVdet));
        mPmParmInit(&(pChnInfo->parm.npjcVdet));
        mPmParmInit(&(pChnInfo->parm.ppjcVgen));
        mPmParmInit(&(pChnInfo->parm.npjcVgen));
        mPmParmInit(&(pChnInfo->parm.pjcDiffV));
        mPmParmInit(&(pChnInfo->parm.pjcsVdet));
        mPmParmInit(&(pChnInfo->parm.pjcsVgen));
        mPmParmInit(&(pChnInfo->parm.cvVfe));
        mPmParmInit(&(pChnInfo->parm.esVfe));
        mPmParmInit(&(pChnInfo->parm.sesVfe));
        mPmParmInit(&(pChnInfo->parm.uasVfe));
        mPmParmInit(&(pChnInfo->parm.fcVfe));

        /* Add to database */
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pVtId->sts.line))]->ppStsDb[mSurStsIdFlat(&(pVtId->sts))]->ppVtInfo[mSurVtIdFlat(pVtId)] = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurVtInfoFinal(tSurVtInfo *pInfo)

Purpose      : This function is used to deallocate all resource in VT 
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurVtInfoFinal(tSurVtInfo *pInfo)
    {
    /* Finalize all parameters */
    mPmParmFinal(&(pInfo->parm.cvV));
    mPmParmFinal(&(pInfo->parm.esV));
    mPmParmFinal(&(pInfo->parm.sesV));
    mPmParmFinal(&(pInfo->parm.uasV));
    mPmParmFinal(&(pInfo->parm.fcV));
    mPmParmFinal(&(pInfo->parm.ppjcVdet));
    mPmParmFinal(&(pInfo->parm.npjcVdet));
    mPmParmFinal(&(pInfo->parm.ppjcVgen));
    mPmParmFinal(&(pInfo->parm.npjcVgen));
    mPmParmFinal(&(pInfo->parm.pjcDiffV));
    mPmParmFinal(&(pInfo->parm.pjcsVdet));
    mPmParmFinal(&(pInfo->parm.pjcsVgen));
    mPmParmFinal(&(pInfo->parm.cvVfe));
    mPmParmFinal(&(pInfo->parm.esVfe));
    mPmParmFinal(&(pInfo->parm.sesVfe));
    mPmParmFinal(&(pInfo->parm.uasVfe));
    mPmParmFinal(&(pInfo->parm.fcVfe));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
        }
    }
    
/*------------------------------------------------------------------------------
Prototype    : private void VtInfoDestroy(tSurInfo   *pSurInfo,
                                           tSurVtId *pVtId)

Purpose      : This function is used to deallocate resources of VT

Inputs       : pSurInfo              - database handle of device
               pVtId                - VT id

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void VtInfoDestroy(tSurInfo *pSurInfo, tSurVtId *pVtId)
    {
    tSurVtInfo *pInfo;

    pInfo = NULL;
    /* Get channel information */
    mSurVtInfo(pSurInfo, pVtId, pInfo);
    if (pInfo != null)
        {
        SurVtInfoFinal(pInfo);

        /* Destroy other resources */
        OsalMemFree(pInfo);
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pVtId->sts.line))]->ppStsDb[mSurStsIdFlat(&(pVtId->sts))]->ppVtInfo[mSurVtIdFlat(pVtId)] = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet VtParmGet(tSurVtParm *pVtParm, 
                                 ePmVtParm   parm, 
                                 tPmParm    **ppParm)

Purpose      : This functions is used to get a  pointer to a VT path parameter.

Inputs       : pVtParm               - pointer to the VT parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet VtParmGet(tSurVtParm *pVtParm, ePmVtParm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pVtParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Ger parameter */
    switch (parm)
        {
        /* Near-end VT CV */
        case cPmVtCv:
            *ppParm = &(pVtParm->cvV);
            break;
        
        /* Near-end VT ES */
        case cPmVtEs:
            *ppParm = &(pVtParm->esV);
            break;
        
        /* Near-end VT SES */
        case cPmVtSes:
            *ppParm = &(pVtParm->sesV);
            break;
            
        /* Near-end VT UAS */
        case cPmVtUas:
            *ppParm = &(pVtParm->uasV);
            break;
        
        /* Near-end VT FC */
        case cPmVtFc:
            *ppParm = &(pVtParm->fcV);
            break;
            
        /* Near-end VT detected PPJC */
        case cPmVtPpjcVdet:
            *ppParm = &(pVtParm->ppjcVdet);
            break;
        
        /* Near-end VT detected PNJC */
        case cPmVtNpjcVdet:
            *ppParm = &(pVtParm->npjcVdet);
            break;
        
        /* Near-end VT generated PPJC */
        case cPmVtPpjcVgen:
            *ppParm = &(pVtParm->ppjcVgen);
            break;
        
        /* Near-end VT generated NPJC */
        case cPmVtNpjcVgen:
            *ppParm = &(pVtParm->npjcVgen);
            break;
            
        /* Near-end VT PJCD */
        case cPmVtPjcDiff:
            *ppParm = &(pVtParm->pjcDiffV);
            break;
        
        /* Near-end VT detected PJCS */
        case cPmVtPjcsVdet:
            *ppParm = &(pVtParm->pjcsVdet);
            break;
        
        /* Near-end VT generated PJCS */
        case cPmVtPjcsVgen:
            *ppParm = &(pVtParm->pjcsVgen);
            break;
        
        /* Far-end VT CV */
        case cPmVtCvVfe:
            *ppParm = &(pVtParm->cvVfe);
            break;  
        
        /* Far-end VT ES */
        case cPmVtEsVfe:
            *ppParm = &(pVtParm->esVfe);
            break;
        
        /* Far-end VT SES */
        case cPmVtSesVfe:
            *ppParm = &(pVtParm->sesVfe);
            break;
        
        /* Far-end VT UAS */
        case cPmVtUasVfe:
            *ppParm = &(pVtParm->uasVfe);
            break;
        
        /* Far-end VT FC */
        case cPmVtFcVfe:
            *ppParm = &(pVtParm->fcVfe);
            break;
            
        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllVtParmRegReset(tSurVtParm  *pVtParm, 
                                         ePmRegType   regType,
                                         bool         neParm)

Purpose      : This functions is used to reset registers of a group of VT 
               parameters.

Inputs       : pVtParm               - pointer to the VT parameters
               regType               - register type
               neParm                - is near-end VT path parameters
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllVtParmRegReset(tSurVtParm  *pVtParm, 
                                 ePmRegType   regType,
                                 bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmVtParm    start;
    ePmVtParm    end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pVtParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmVtCv;
        end   = cPmVtNeAllParm;
        }
    else
        {
        start = cPmVtCvVfe;
        end   = cPmVtFeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = VtParmGet(pVtParm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllVtParmAccInhSet(tSurVtParm  *pVtParm, 
                                          bool         inhibit, 
                                          bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               VT parameters.

Inputs       : pVtParm               - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end VT path parameters, false
                                     for far-end VT path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllVtParmAccInhSet(tSurVtParm  *pVtParm, 
                                  bool         inhibit, 
                                  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmVtParm    start;
    ePmVtParm    end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pVtParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmVtCv;
        end   = cPmVtNeAllParm;
        }
    else
        {
        start = cPmVtCvVfe;
        end   = cPmVtFeAllParm;
        }
    
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = VtParmGet(pVtParm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllVtParmRegThresSet(tSurVtParm    *pVtParm, 
                                            ePmRegType    regType,
                                            dword         thresVal, 
                                            bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               VT parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end VT path parameters, false
                                     for far-end VT path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllVtParmRegThresSet(tSurVtParm    *pVtParm, 
                                     ePmRegType    regType,
                                     dword         thresVal, 
                                     bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmVtParm    start;
    ePmVtParm    end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pVtParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmVtCv;
        end   = cPmVtNeAllParm;
        }
    else
        {
        start = cPmVtCvVfe;
        end   = cPmVtFeAllParm;
        }
    
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = VtParmGet(pVtParm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }                                         

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllVtParmNumRecRegSet(tSurVtParm  *pVtParm, 
                                             byte         numRecReg, 
                                             bool         neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of VT parameters.

Inputs       : pVtParm               - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end VT path parameters, false
                                     for far-end VT path pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllVtParmNumRecRegSet(tSurVtParm  *pVtParm, 
                                     byte         numRecReg, 
                                     bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmVtParm    start;
    ePmVtParm    end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pVtParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmVtCv;
        end   = cPmVtNeAllParm;
        }
    else
        {
        start = cPmVtCvVfe;
        end   = cPmVtFeAllParm;
        }
    
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = VtParmGet(pVtParm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                                                                                           
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurVtProv(tSurDev device, tSurVtId *pVtId)

Purpose      : This function is used to provision a VT for FM & PM.

Inputs       : device                - Device
               pVtId                - VT identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtProv(tSurDev device, tSurVtId *pVtId)
    {
    tSurInfo   *pSurInfo;
    tSurVtInfo *pChnInfo;

    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pVtId))
        {
        return cSurErrNullParm;
        }
    
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Check if the STS channel that contains this VT ID is valid */
    if (mSurStsIdFlat(&(pVtId->sts)) >= 
        mChnDb(pSurInfo).pLine[mSurLineIdFlat(&(pVtId->sts.line))]->numSts)
        {
        mSurDebug(cSurErrMsg, "STS channel doesn't exist\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrNoSuchChn;
        }
        
    /* Get VT information */
    mSurVtInfo(pSurInfo, pVtId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "VT is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for VT */
    pChnInfo = VtInfoCreate(pSurInfo, pVtId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for VT\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default channel configuration for VT path */
    pChnInfo->conf.rate           = cSurVt15;
    pChnInfo->conf.erdiEn         = false;
    pChnInfo->conf.decSoakTime    = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultTerSoakTime;
    
    /* Set default parameters' values for VT path */
    /* Set default parameters' values for line channel */
    if (SurVtDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for channel's PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErr;
        }
        
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurVtDeProv(tSurDev device, tSurVtId *pVtId)

Purpose      : This function is used to de-provision FM & PM of one VT. It
               will deallocated all resources.

Inputs       : device                - device
               pVtId                - VT identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtDeProv(tSurDev device, tSurVtId *pVtId)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pVtId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of VT */
    VtInfoDestroy(pSurInfo, pVtId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurVtTrblDesgnSet(tSurDev              device, 
                                                eSurVtTrbl           trblType, 
                                                const tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to set VT path trouble designation for 
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtTrblDesgnSet(tSurDev              device, 
                                 eSurVtTrbl           trblType, 
                                 const tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    /* Set trouble designation */
    switch (trblType)
        {
        /* VT path Loss Of Pointer */    
        case cFmVtLop:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lopv);
            break;
            
        /* VT Path Payload Label Mismatch */
        case cFmVtPlm:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.plmv);
            break;
            
        /* VT Path Unequipped */
        case cFmVtUneq:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.uneqv);
            break;
            
        /* VT Path Trace Identifier Mismatch */
        case cFmVtTim:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.timv);
            break;
            
        /* VT Path Alarm Indication Signal */
        case cFmVtAis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisv);
            break;
            
        /* VT Path Remote Failure Indication */
        case cFmVtRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfiv);
            break;
            
        /* VT Path Server Enhanced Remote Failure Indication. */
        case cFmVtErfiS:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivS);
            break;
        
        /* VT Path Connectivity Enhanced Remote Failure Indication. */
        case cFmVtErfiC:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivC);
            break;
        
        /* VT Path Payload Enhanced Remote Failure Indication */
        case cFmVtErfiP:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivP);
            break;
            
        /* VT Path BER-based Signal Failure */
        case cFmVtBerSf:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.vtBerSf);
            break;
            
        /* VT Path BER-based Signal Degrade */
        case cFmVtBerSd:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.vtBerSd);
            break;
            
        /* L bit failure */
        case cFmVtLbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit failure */
        case cFmVtRbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit failure */
        case cFmVtMbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* VT Path all failures */
        case cFmVtAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lopv);   
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.plmv);   
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.uneqv);  
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.timv);   
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisv);   
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfiv);   
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivS); 
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivC); 
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivP); 
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.vtBerSf);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.vtBerSd);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));

            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            break;
        
        /* Performance monitoring */
        case cPmVtTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurVtTrblDesgnGet(tSurDev        device, 
                                                eSurVtTrbl     trblType, 
                                                tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get VT path trouble designation of 
               a device.

Inputs       : device                ? device
               trblType              ? trouble type
               
Outputs      : pTrblDesgn            ? trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtTrblDesgnGet(tSurDev        device, 
                                 eSurVtTrbl     trblType, 
                                 tSurTrblDesgn *pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    pSurInfo = mDevDb(device);
    switch (trblType)
        {
        
        /* VT path Loss Of Pointer */    
        case cFmVtLop:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lopv);
            break;
            
        /* VT Path Payload Label Mismatch */
        case cFmVtPlm:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.plmv);
            break;
            
        /* VT Path Unequipped */
        case cFmVtUneq:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.uneqv);
            break;
            
        /* VT Path Trace Identifier Mismatch */
        case cFmVtTim:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.timv);
            break;
            
        /* VT Path Alarm Indication Signal */
        case cFmVtAis:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.aisv);
            break;
            
        /* VT Path Remote Failure Indication */
        case cFmVtRfi:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rfiv);
            break;
            
        /* VT Path Server Enhanced Remote Failure Indication. */
        case cFmVtErfiS:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivS);
            break;
        
        /* VT Path Connectivity Enhanced Remote Failure Indication. */
        case cFmVtErfiC:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivC);
            break;
        
        /* VT Path Payload Enhanced Remote Failure Indication */
        case cFmVtErfiP:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.erfivP);
            break;
            
        /* VT Path BER-based Signal Failure */
        case cFmVtBerSf:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.vtBerSf);
            break;
            
        /* VT Path BER-based Signal Degrade */
        case cFmVtBerSd:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.vtBerSd);
            break;
            
        /* L bit failure */
        case cFmVtLbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.lBit);
            break;

        /* R bit failure */
        case cFmVtRbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.rBit);
            break;

        /* M bit failure */
        case cFmVtMbit:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.mBit);
            break;

        /* Performance monitoring */
        case cPmVtTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
            
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtEngStatSet(tSurDev             device, 
                                       const tSurVtId     *pChnId, 
                                       eSurEngStat         stat)

Purpose      : This API is used to set engine state for a VT channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurVtEngStatSet(tSurDev             device, 
                               const tSurVtId     *pChnId, 
                               eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtEngStatGet(tSurDev             device, 
                                       const tSurVtId     *pChnId, 
                                       eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a VT channel.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine stat

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurVtEngStatGet(tSurDev             device, 
                               const tSurVtId     *pChnId, 
                               eSurEngStat        *pStat)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurVtChnCfgSet(tSurDev           device,
                                             const tSurVtId   *pChnId, 
                                             const tSurVtConf *pChnCfg)

Purpose      : This internal API is use to set VT channel's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtChnCfgSet(tSurDev           device,
                              const tSurVtId   *pChnId, 
                              const tSurVtConf *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurVtInfo     *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set configuration */
    pInfo->conf.rate           = pChnCfg->rate;
    pInfo->conf.erdiEn         = pChnCfg->erdiEn;
	pInfo->conf.decSoakTime    = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
	pInfo->conf.terSoakTime    = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtChnCfgGet(tSurDev           device,
                                      const tSurVtId   *pChnId, 
                                      tSurVtConf       *pChnCfg)
    {
Purpose      : This API is used to set VT channel's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtChnCfgGet(tSurDev            device,
                              const tSurVtId    *pChnId, 
                              tSurVtConf        *pChnCfg)
    {
    /* Declare local variables */
    tSurVtInfo *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get configuration */
    pChnCfg->rate             = pInfo->conf.rate;
    pChnCfg->erdiEn           = pInfo->conf.erdiEn;
    pChnCfg->decSoakTime      = pInfo->conf.decSoakTime;
    pChnCfg->terSoakTime      = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtTrblNotfInhSet(tSurDev               device,
                                           const tSurVtId       *pChnId, 
                                           eSurVtTrbl            trblType, 
                                           bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a VT channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtTrblNotfInhSet(tSurDev               device,
                                   const tSurVtId       *pChnId, 
                                   eSurVtTrbl            trblType, 
                                   bool                  inhibit)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* VT fault monitoring */
        case cFmVtLop:
            pInfo->msgInh.lopv = inhibit;
            break;
        case cFmVtPlm:
            pInfo->msgInh.plmv = inhibit;
            break;
        case cFmVtUneq:
            pInfo->msgInh.uneqv = inhibit;
            break;
        case cFmVtAis:
            pInfo->msgInh.aisv = inhibit;
            break;
        case cFmVtTim:
            pInfo->msgInh.timv = inhibit;
            break;
        case cFmVtRfi:
        case cFmVtErfiS:
        case cFmVtErfiC:
        case cFmVtErfiP:
            pInfo->msgInh.rfiv = inhibit;
            break;
        case cFmVtBerSf:
            pInfo->msgInh.vtBerSf = inhibit;
            break;
        case cFmVtBerSd:
            pInfo->msgInh.vtBerSd = inhibit;
            break;

        case cFmVtLbit:
            pInfo->msgInh.lBit = inhibit;
            break;
        case cFmVtRbit:
            pInfo->msgInh.rBit = inhibit;
            break;
        case cFmVtMbit:
            pInfo->msgInh.mBit = inhibit;
            break;

        case cFmVtAllFail:
            pInfo->msgInh.vt = inhibit;
            break;
            
        /* VT performance monitoring */
        case cPmVtTca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }      
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtTrblNotfInhGet(tSurDev               devicee,
                                           const tSurVtId       *pChnId, 
                                           eSurVtTrbl            trblType, 
                                           bool                 *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a VT channel.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtTrblNotfInhGet(tSurDev               device,
                                   const tSurVtId       *pChnId, 
                                   eSurVtTrbl            trblType, 
                                   bool                 *pInhibit)
    {
    /* Declare local variables */
    tSurVtInfo  *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* VT fault monitoring */
        case cFmVtLop:
            *pInhibit = pInfo->msgInh.lopv;
            break;
        case cFmVtPlm:
            *pInhibit = pInfo->msgInh.plmv;
            break;
        case cFmVtUneq:
            *pInhibit = pInfo->msgInh.uneqv;
            break;
        case cFmVtAis:
            *pInhibit = pInfo->msgInh.aisv;
            break;
        case cFmVtTim:
            *pInhibit = pInfo->msgInh.timv;
            break;
        case cFmVtRfi:
        case cFmVtErfiS:
        case cFmVtErfiC:
        case cFmVtErfiP:
            *pInhibit = pInfo->msgInh.rfiv;
            break;
        case cFmVtBerSf:
            *pInhibit = pInfo->msgInh.vtBerSf;
            break;
        case cFmVtBerSd:
            *pInhibit = pInfo->msgInh.vtBerSd;
            break;

        case cFmVtLbit:
            *pInhibit = pInfo->msgInh.lBit;
            break;
        case cFmVtRbit:
            *pInhibit = pInfo->msgInh.rBit;
            break;
        case cFmVtMbit:
            *pInhibit = pInfo->msgInh.mBit;
            break;

        case cFmVtAllFail:
            *pInhibit = pInfo->msgInh.vt;
            break;
            
        /* VT performance monitoring */
        case cPmVtTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }               

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurVtChnTrblNotfInhInfoGet(tSurDev             device, 
                                                  const tSurVtId     *pChnId, 
                                                  tSurVtMsgInh       *pTrblInh)

Purpose      : This API is used to get VT channel's trouble notification 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurVtChnTrblNotfInhInfoGet(tSurDev             device, 
                                          const tSurVtId     *pChnId, 
                                          tSurVtMsgInh       *pTrblInh)
    {
    tSurVtInfo    *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurVtMsgInh));
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmVtFailIndGet(tSurDev           device, 
                                      const tSurVtId   *pChnId, 
                                      eSurVtTrbl        trblType, 
                                      tSurFailStat     *pFailStat)

Purpose      : This API is used to get failure indication of a VT channel's 
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmVtFailIndGet(tSurDev           device, 
                              const tSurVtId   *pChnId, 
                              eSurVtTrbl        trblType, 
                              tSurFailStat     *pFailStat)
    {
    /* Declare local variables */
    tSurVtInfo  *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        case cFmVtLop:
            mSurFailStatCopy(&(pInfo->failure.lopv), pFailStat);
            break;
            
        case cFmVtPlm:
            mSurFailStatCopy(&(pInfo->failure.plmv), pFailStat);
            break;
            
        case cFmVtUneq:
            mSurFailStatCopy(&(pInfo->failure.uneqv), pFailStat);
            break;
            
        case cFmVtAis:
            mSurFailStatCopy(&(pInfo->failure.aisv), pFailStat);
            break;
            
        case cFmVtTim:
            mSurFailStatCopy(&(pInfo->failure.timv), pFailStat);
            break;
            
        case cFmVtRfi:
            mSurFailStatCopy(&(pInfo->failure.rfiv), pFailStat);
            break;
        
        case cFmVtErfiS:
            mSurFailStatCopy(&(pInfo->failure.erfivS), pFailStat);
            break;
            
        case cFmVtErfiC:
            mSurFailStatCopy(&(pInfo->failure.erfivC), pFailStat);
            break;
            
        case cFmVtErfiP:
            mSurFailStatCopy(&(pInfo->failure.erfivP), pFailStat);
            break;
            
        case cFmVtBerSf:
            mSurFailStatCopy(&(pInfo->failure.vtBerSf), pFailStat);
            break;
            
        case cFmVtBerSd:
            mSurFailStatCopy(&(pInfo->failure.vtBerSd), pFailStat);
            break;
        
        case cFmVtLbit:
            mSurFailStatCopy(&(pInfo->failure.lBit), pFailStat);
        case cFmVtRbit:
            mSurFailStatCopy(&(pInfo->failure.rBit), pFailStat);
        case cFmVtMbit:
            mSurFailStatCopy(&(pInfo->failure.mBit), pFailStat);
            break;
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }  
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmVtFailHisGet(tSurDev         device, 
                                               const tSurVtId *pChnId)

Purpose      : This API is used to get a VT channel's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmVtFailHisGet(tSurDev         device, 
                                const tSurVtId *pChnId)
    {
    /* Declare local variables */
    tSurVtInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmVtFailHisClear(tSurDev         device, 
                                        const tSurVtId *pChnId,
                                        bool            flush)

Purpose      : This API is used to clear a VT channel's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmVtFailHisClear(tSurDev device, const tSurVtId *pChnId, bool flush)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtRegReset(tSurDev       device, 
                                    tSurVtId     *pChnId, 
                                    ePmVtParm     parm, 
                                    ePmRegType    regType)

Purpose      : This API is used to reset a VT channel's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmVtRegReset(tSurDev       device, 
                            tSurVtId     *pChnId, 
                            ePmVtParm     parm, 
                            ePmRegType    regType)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end VT path parameters */
    if (parm == cPmVtNeAllParm)
        {
        ret = AllVtParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end VT parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end STS path parameters */
    else if (parm == cPmVtFeAllParm)
        {
        ret = AllVtParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end VT parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = VtParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtRegGet(tSurDev           device, 
                                  const tSurVtId   *pChnId, 
                                  ePmVtParm         parm, 
                                  ePmRegType        regType, 
                                  tPmReg           *pRetReg)

Purpose      : This API is used to get a VT channel's parameter's register's 
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg               - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmVtRegGet(tSurDev           device, 
                          const tSurVtId   *pChnId, 
                          ePmVtParm         parm, 
                          ePmRegType        regType, 
                          tPmReg           *pRetReg)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = VtParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Resetting register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
        
    return cSurOk;
    }    
         
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmVtTcaHisGet(tSurDev device, const tSurVtId *pChnId)

Purpose      : This API is used to get a VT channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmVtTcaHisGet(tSurDev device, const tSurVtId *pChnId)
    {
    /* Declare local variables */
    tSurVtInfo    *pInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }
              
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtTcaHisClear(tSurDev device, const tSurVtId *pChnId)

Purpose      : This API is used to clear a VT channel's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmVtTcaHisClear(tSurDev device, const tSurVtId *pChnId)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmVtParmAccInhSet(tSurDev          device, 
                                          const tSurVtId  *pChnId, 
                                          ePmVtParm        parm, 
                                          bool             inhibit)

Purpose      : This API is used to enable/disable a VT channel's accumlate 
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmVtParmAccInhSet(tSurDev          device, 
                                 const tSurVtId  *pChnId, 
                                 ePmVtParm        parm, 
                                 bool             inhibit)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end VT parameters */
    if (parm == cPmVtNeAllParm)
        {
        ret = AllVtParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end near-end VT parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end VT parameters */
    else if (parm == cPmVtFeAllParm)
        {
        ret = AllVtParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting far-end far-end VT parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = VtParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtParmAccInhGet(tSurDev          device, 
                                         const tSurVtId  *pChnId, 
                                         ePmVtParm        parm, 
                                         bool            *pInhibit)

Purpose      : This API is used to get a VT channel's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmVtParmAccInhGet(tSurDev          device, 
                                 const tSurVtId  *pChnId, 
                                 ePmVtParm        parm, 
                                 bool            *pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurVtInfo      *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = VtParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtRegThresSet(tSurDev          device, 
                                       const tSurVtId  *pChnId, 
                                       ePmVtParm        parm, 
                                       ePmRegType       regType, 
                                       dword            thresVal)

Purpose      : This API is used to set threshold for a VT channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmVtRegThresSet(tSurDev          device, 
                               const tSurVtId  *pChnId, 
                               ePmVtParm        parm, 
                               ePmRegType       regType, 
                               dword            thresVal)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end Vt parameters */
    else if (parm == cPmVtNeAllParm)
        {
        ret = AllVtParmRegThresSet(&(pInfo->parm), 
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end VT parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end VT parameters */
    else if (parm == cPmVtFeAllParm)
        {
        ret = AllVtParmRegThresSet(&(pInfo->parm), 
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end VT parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = VtParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtRegThresGet(tSurDev          device, 
                                       const tSurVtId  *pChnId, 
                                       ePmVtParm        parm, 
                                       ePmRegType       regType, 
                                       dword           *pThresVal)

Purpose      : This API is used to get threshold for a VT channel's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmVtRegThresGet(tSurDev          device, 
                               const tSurVtId  *pChnId, 
                               ePmVtParm        parm, 
                               ePmRegType       regType, 
                               dword           *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurVtInfo      *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = VtParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtParmNumRecRegSet(tSurDev         device, 
                                            const tSurVtId *pChnId, 
                                            ePmVtParm       parm, 
                                            byte            numRecReg)
                                            
Purpose      : This API is used to set a VT channel's parameter's number of 
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmVtParmNumRecRegSet(tSurDev         device, 
                                    const tSurVtId *pChnId, 
                                    ePmVtParm       parm, 
                                    byte            numRecReg)
    {
    /* Declare local variables */
    tSurVtInfo     *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;

    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
    mPmRecRegCheck(numRecReg);
    
    /* Initialize */
    pParm = null;
    
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurVtInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end VT parameters */
    else if (parm == cPmVtNeAllParm)
        {
        ret = AllVtParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end VT parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end VT parameters */
    else if (parm == cPmStsFeAllParm)
        {
        ret = AllVtParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end VT parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = VtParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmVtParmNumRecRegGet(tSurDev         device, 
                                            const tSurVtId *pChnId, 
                                            ePmVtParm       parm, 
                                            byte           *pNumRecReg)
                                             
Purpose      : This API is used to get a VT channel's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmVtParmNumRecRegGet(tSurDev         device, 
                                    const tSurVtId *pChnId, 
                                    ePmVtParm       parm, 
                                    byte           *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurVtInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurVtInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = VtParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
        

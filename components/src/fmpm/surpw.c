/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - PSEUDOWIRE
 *
 * File        : surpw.c
 *
 * Created Date: 22-Feb-2010
 *
 * Description : This file contain all Transmisstion Surveillance source code 
 *               of Pseudowire
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

 /*--------------------------- Include files ---------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "surid.h"
#include "sur.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwDefParmSet(tSurPwInfo *pInfo)

Purpose      : This function is used to set default values for PW PM
               parameters.

Inputs       : pInfo                 - PW info

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwDefParmSet(tSurPwInfo *pInfo)
    {
    mPmDefParmSet(&(pInfo->parm.es), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.ses), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.uas), cPmPerThresVal, cPmDayThresVal);
    
    mPmDefParmSet(&(pInfo->parm.txPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.rxPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.missingPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.misorderedPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.misorderedPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.playoutJitBufOverrun), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.playoutJitBufUnderrun), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.lops), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.malformedPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.strayPkt), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.remotePktLoss), cPmPerThresVal, cPmDayThresVal);
    mPmDefParmSet(&(pInfo->parm.txLbitPkt), cPmPerThresVal, cPmDayThresVal);
    return cSurOk;
    }
/*------------------------------------------------------------------------------
Prototype    : private tSurPwInfo *PwInfoCreate(tSurInfo  *pSurInfo,
														  tSurPwId *pPwId)

Purpose      : This function is used to allocate resources for PW

Inputs       : pSurInfo              - database handle of device
               pPwId            - PW identifier

Outputs      : None

Return       : tSurPwInfo*      - points to resources structure or null if
                                       fail
------------------------------------------------------------------------------*/
/*private*/ tSurPwInfo *PwInfoCreate(tSurInfo *pSurInfo, tSurPwId *pPwId)
    {
    tSurPwInfo *pChnInfo;
    
    /* Allocate resources */
    pChnInfo = OsalMemAlloc(sizeof(tSurPwInfo));
    if (pChnInfo != null)
        {
        OsalMemInit(pChnInfo, sizeof(tSurPwInfo), 0);

        /* Create failure history and TCA history */
        if ((ListCreate(&(pChnInfo->failHis), cListDec, &SurFailCmp) != cListSucc) ||
            (ListCreate(&(pChnInfo->tcaHis), cListDec, &SurTcaCmp)   != cListSucc))
            {
            mSurDebug(cSurWarnMsg, "Cannot create Failure History or TCA history");
            }

        /* Create parameters */
        mPmParmInit(&(pChnInfo->parm.es));
        mPmParmInit(&(pChnInfo->parm.ses));
        mPmParmInit(&(pChnInfo->parm.uas));
		mPmParmInit(&(pChnInfo->parm.txPkt));
		mPmParmInit(&(pChnInfo->parm.rxPkt));
		mPmParmInit(&(pChnInfo->parm.missingPkt));
		mPmParmInit(&(pChnInfo->parm.misorderedPkt));
		mPmParmInit(&(pChnInfo->parm.misorderedPkt));
		mPmParmInit(&(pChnInfo->parm.playoutJitBufOverrun));
		mPmParmInit(&(pChnInfo->parm.playoutJitBufUnderrun));
		mPmParmInit(&(pChnInfo->parm.lops));
		mPmParmInit(&(pChnInfo->parm.malformedPkt));
		mPmParmInit(&(pChnInfo->parm.strayPkt));
		mPmParmInit(&(pChnInfo->parm.remotePktLoss));
		mPmParmInit(&(pChnInfo->parm.txLbitPkt));

        /* Add to database */
        mChnDb(pSurInfo).pPw[mSurPwIdFlat(pPwId)] = pChnInfo;
        }
    
    return pChnInfo;
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurPwInfoFinal(tSurPwInfo *pInfo)

Purpose      : This function is used to deallocate all resource in PW
               information structure

Inputs       : pInfo                 - information

Outputs      : pInfo

Return       : None
------------------------------------------------------------------------------*/
friend void SurPwInfoFinal(tSurPwInfo *pInfo)
    {
    /* Finalize all parameters */
	mPmParmFinal(&(pInfo->parm.es));
	mPmParmFinal(&(pInfo->parm.ses));
	mPmParmFinal(&(pInfo->parm.uas));
	mPmParmFinal(&(pInfo->parm.txPkt));
	mPmParmFinal(&(pInfo->parm.rxPkt));
	mPmParmFinal(&(pInfo->parm.missingPkt));
	mPmParmFinal(&(pInfo->parm.misorderedPkt));
	mPmParmFinal(&(pInfo->parm.misorderedPkt));
	mPmParmFinal(&(pInfo->parm.playoutJitBufOverrun));
	mPmParmFinal(&(pInfo->parm.playoutJitBufUnderrun));
	mPmParmFinal(&(pInfo->parm.lops));
	mPmParmFinal(&(pInfo->parm.malformedPkt));
	mPmParmFinal(&(pInfo->parm.strayPkt));
	mPmParmFinal(&(pInfo->parm.remotePktLoss));
	mPmParmFinal(&(pInfo->parm.txLbitPkt));

    /* Destroy failure history and failure history */
    if ((ListDestroy(&(pInfo->failHis)) != cListSucc) ||
        (ListDestroy(&(pInfo->tcaHis))  != cListSucc))
        {
        mSurDebug(cSurWarnMsg, "Cannot destroy Failure History or TCA history");
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void PwInfoDestroy(tSurInfo   *pSurInfo,
											   tSurPwId  *pPwId)

Purpose      : This function is used to deallocate resources of PW

Inputs       : pSurInfo              - database handle of device
               pPwId            - PW ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void PwInfoDestroy(tSurInfo *pSurInfo, tSurPwId *pPwId)
    {
    tSurPwInfo *pInfo;

    pInfo = NULL;
    /* Get channel information */
    mSurPwInfo(pSurInfo, pPwId, pInfo);
    if (pInfo != null)
        {
        SurPwInfoFinal(pInfo);

        /* Free resources */
        OsalMemFree(mChnDb(pSurInfo).pPw[mSurPwIdFlat(pPwId)]);
        mChnDb(pSurInfo).pPw[mSurPwIdFlat(pPwId)] = null;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PwParmGet(tSurPwParm   *pPwParm,
									  ePmPwParm     parm,
									  tPmParm       **ppParm)

Purpose      : This functions is used to get a pointer to a PW parameter.

Inputs       : pPwParm          - pointer to the PW parameters
               parm                  - parameter type
               
Outputs      : pParm                 - pointer to the desired paramter

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PwParmGet(tSurPwParm *pPwParm, ePmPwParm parm, tPmParm **ppParm)
    {
    /* Check for null pointers */
    if (pPwParm == null)
        {
        return cSurErrNullParm;
        }
        
    /* Get parameter */
    switch (parm)
        {
        /* PW Errored Seconds */
        case cPmPwEs:
            *ppParm = &(pPwParm->es);
            break;
        
        /* PW Severely Errored Second */
        case cPmPwSes:
            *ppParm = &(pPwParm->ses);
            break;
        
        /* PW Unavailable Second */
        case cPmPwUas:
            *ppParm = &(pPwParm->uas);
            break;

        /* PW Transmitted packet */
        case cPmPwTxPkt:
            *ppParm = &(pPwParm->txPkt);
            break;

        /* PW Received packet */
        case cPmPwRxPkt:
            *ppParm = &(pPwParm->rxPkt);
            break;

        /* PW Missing packet */
        case cPmPwMissingPkt:
            *ppParm = &(pPwParm->missingPkt);
            break;

        /* PW Mis-ordered packet, and successfully re-ordered */
        case cPmPwMisorderedPkt:
            *ppParm = &(pPwParm->misorderedPkt);
            break;

        /* PW Mis-ordered packet dropped */
        case cPmPwMisorderedPktDrop:
            *ppParm = &(pPwParm->misorderedPktDropped);
            break;

        /* PW Play-out buffer overrun */
        case cPmPwPlayoutJitBufOverrun:
            *ppParm = &(pPwParm->playoutJitBufOverrun);
            break;

        /* PW Play-out buffer underrun */
        case cPmPwPlayoutJitBufUnderrun:
            *ppParm = &(pPwParm->playoutJitBufUnderrun);
            break;

        /* PW Loss of Packet Synchronization */
        case cPmPwLops:
            *ppParm = &(pPwParm->lops);
            break;

        /* PW Malformed packet */
        case cPmPwMalformedPkt:
            *ppParm = &(pPwParm->malformedPkt);
            break;

        /* PW Stray packet */
        case cPmPwStrayPkt:
            *ppParm = &(pPwParm->strayPkt);
            break;

        /* PW Remote packet loss */
        case cPmPwRemotePktLoss:
            *ppParm = &(pPwParm->remotePktLoss);
            break;

        /* PW Transmitted L-Bit packet */
        case cPmPwTxLbitPkt:
            *ppParm = &(pPwParm->txLbitPkt);
            break;

        /* Default */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllPwParmRegReset(tSurPwParm *pPwParm,
											  ePmRegType   regType,
											  bool         neParm)

Purpose      : This functions is used to reset registers of a group of PW
               parameters.

Inputs       : pPwParm          - pointer to the  PW parameters
               regType          - register type
               neParm           - If parameter is near-end, set to true.
								       Else, set to false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllPwParmRegReset(tSurPwParm *pPwParm,
								  ePmRegType   regType,
								  bool         neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmPwParm   start;
    ePmPwParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pPwParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmPwEs;
        end   = cPmPwNeAllParm;
        }
    else
        {
        start = cPmPwFeStartParm;
        end   = cPmPwFeAllParm;
        }
        
    /* Reset all registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = PwParmGet(pPwParm, bIdx, &pParm);
        mRetCheck(ret);
        
        /* Reset registers */
        ret = RegReset(pParm, regType);
        mRetCheck(ret);
        }
        
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllPwParmAccInhSet(tSurPwParm *pPwParm,
										   bool         inhibit,
										   bool         neParm)
                                            
Purpose      : This functions is used to set accumulate inhibition for a group of  
               PW parameters.

Inputs       : pPwParm              - pointer to the Sts parameters
               inhibit               - inhibition mode
               neParm                - true for near-end PW parameters,
                                       false for far-end PW pamameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllPwParmAccInhSet(tSurPwParm  *pPwParm,
									   bool          inhibit,
									   bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmPwParm   start;
    ePmPwParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pPwParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmPwEs;
        end   = cPmPwNeAllParm;
        }
    else
        {
        start = cPmPwFeStartParm;
        end   = cPmPwFeAllParm;
        }
        
    /* Set accumulate inhibition mode */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = PwParmGet(pPwParm, bIdx, &pParm);
        mRetCheck(ret);
        
        pParm->inhibit = inhibit;
        }
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllPwParmRegThresSet(tSurPwParm  *pPwParm,
											 ePmRegType    regType,
											 dword         thresVal,
											 bool          neParm)
                                            
Purpose      : This functions is used to set register threshold for a group of  
               PW parameters.

Inputs       : pStsParm              - pointer to the line parameters
               regType               - register type. the only valid types are:
                                        + cPmCurPer : set threshold for period register
                                        + cPmCurDay : set threshold for day register
               thresVal              - threshold value                         
               neParm                - true for near-end PW parameters,
									   false for far-end PW parameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllPwParmRegThresSet(tSurPwParm  *pPwParm,
									 ePmRegType    regType,
									 dword         thresVal,
									 bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmPwParm   start;
    ePmPwParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pPwParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmPwEs;
        end   = cPmPwNeAllParm;
        }
    else
        {
        start = cPmPwFeStartParm;
        end   = cPmPwFeAllParm;
        }
        
    /* Set register thresholds */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = PwParmGet(pPwParm, bIdx, &pParm);
        mRetCheck(ret);
        
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                return cSurErrInvlParm;
            }
        }
    
    return cSurOk;
    }       

/*------------------------------------------------------------------------------
Prototype    : eSurRet AllPwParmNumRecRegSet(tSurPwParm  *pPwParm,
											  byte          numRecReg,
											  bool          neParm)
                                            
Purpose      : This functions is used to set number of recent registers for a 
               group of PW parameters.

Inputs       : pPwParm          - pointer to the line parameters
               numRecReg             - number of recent registers                       
               neParm                - true for near-end PW parameters,
                                       false for far-end PW parameters.
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet AllPwParmNumRecRegSet(tSurPwParm  *pPwParm,
									  byte          numRecReg,
									  bool          neParm)
    {
    /* Declare local variables */
    eSurRet      ret;
    byte         bIdx;
    tPmParm     *pParm;
    tPmReg      *pReg;
    ePmPwParm   start;
    ePmPwParm   end;
    
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Check for null pointers */
    if (pPwParm == null)
        {
        return cSurErrNullParm;
        }
    
    if (neParm == true)
        {
        start = cPmPwEs;
        end   = cPmPwNeAllParm;
        }
    else
        {
        start = cPmPwFeStartParm;
        end   = cPmPwFeAllParm;
        }
        
    /* Set number of recent registers */
    for (bIdx = (byte)start; bIdx < (byte)end; bIdx++)
        {
        /* Get parameters */
        ret = PwParmGet(pPwParm, bIdx, &pParm);
        mRetCheck(ret);
        
        mPmRecRegSet(pParm, numRecReg);
        }
    
    return cSurOk;
    }
                                                      
/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurPwProv(tSurDev device, tSurPwId *pPwId)

Purpose      : This function is used to provision a PW for FM & PM.

Inputs       : device                - Device
               pPwId            - PW identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwProv(tSurDev device, tSurPwId *pPwId)
    {
    tSurInfo    *pSurInfo;
    tSurPwInfo *pChnInfo;
    
    pChnInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pPwId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }
    
    /* Get PW information */
    mSurPwInfo(pSurInfo, pPwId, pChnInfo);
    if (pChnInfo != null)
        {
        mSurDebug(cSurErrMsg, "PW is busy\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrChnBusy;
        }

    /* Allocate resources for PW */
    pChnInfo = PwInfoCreate(pSurInfo, pPwId);
    if (pChnInfo == null)
        {
        mSurDebug(cSurErrMsg, "Cannot allocate resources for PW\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Set default configuration for PW */
    pChnInfo->conf.decSoakTime    = cSurDefaultDecSoakTime;
    pChnInfo->conf.terSoakTime    = cSurDefaultTerSoakTime;
    
    /* Set default parameters's values for PW */
    if (SurPwDefParmSet(pChnInfo) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot set default values for PM parameters\n");
        if (mDevGive(device) != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Cannot give device\n");
            }
        return cSurErrRsrNoAvai;
        }
    
    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }

    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurPwDeProv(tSurDev device, tSurPwId *pPwId)

Purpose      : This function is used to de-provision FM & PM of one PW. It
               will deallocated all resources.

Inputs       : device                - device
               pPwId            - PW identifier

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwDeProv(tSurDev device, tSurPwId *pPwId)
    {
    tSurInfo *pSurInfo;
    
    /* Check parameters' validity */
    if (mIsNull(pPwId))
        {
        return cSurErrNullParm;
        }
        
    /* Get device handle */
    if ((pSurInfo = mDevTake(device)) == null)
        {
        mSurDebug(cSurErrMsg, "Cannot take device\n");
        return cSurErrDevNotFound;
        }

    /* Deallocate all resources of PW */
    PwInfoDestroy(pSurInfo, pPwId);

    /* Give device */
    if (mDevGive(device) != cSurOk)
        {
        mSurDebug(cSurErrMsg, "Cannot give device\n");
        return cSurErrSem;
        }
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurPwTrblDesgnSet(tSurDev              device,
												 eSurPwTrbl      trblType,
												 const tSurTrblDesgn  *pTrblDesgn)

Purpose      : This function is used to set PW trouble designation for
               a device.

Inputs       : device                � device
               trblType              � trouble type
               pTrblDesgn            � trouble designation

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwTrblDesgnSet(tSurDev              device,
								  eSurPwTrbl      trblType,
								  const tSurTrblDesgn  *pTrblDesgn)
    {
    /* Declare local variables */
    eSurRet        ret;
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* PW Stray packet */
        case cFmPwStrayPkt:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwStrayPkt);
            break;

        /* PW Malformed Packet */
        case cFmPwMalformedPkt:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwMalformedPkt);
            break;

        /* PW Excessive packet loss rate */
        case cFmPwExcPktLossRate:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwExcPktLossRate);
            break;

        /* PW Jitter Buffer overrun */
        case cFmPwJitBufOverrun:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwJitBufOverrun);
            break;

        /* PW Remote packet loss */
        case cFmPwRemotePktLoss:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwRemotePktLoss);
            break;

        /* PW LOFS */
        case cFmPwLofs:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwLofs);
            break;

        /* PW all failures */
        case cFmPwAllFail:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwStrayPkt);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwMalformedPkt);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwExcPktLossRate);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwJitBufOverrun);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwRemotePktLoss);
            OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwLofs);
            break;
        
        /* Performance monitoring */
        case cPmPwTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
               
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Set configuration */
    OsalMemCpy((void*)pTrblDesgn, pTrbl, sizeof(tSurTrblDesgn));
    
    /*Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : friend eSurRet SurPwTrblDesgnGet(tSurDev        device,
													 eSurPwTrbl    trblType,
													 tSurTrblDesgn *pTrblDesgn)

Purpose      : This function is used to get PW  trouble designation of
               a device.

Inputs       : device                � device
               trblType              � trouble type
               
Outputs      : pTrblDesgn            - trouble designation

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwTrblDesgnGet(tSurDev        	device,
									  eSurPwTrbl   trblType,
									  tSurTrblDesgn 	*pTrblDesgn)
    {
    /* Declare local variables */
    tSurInfo      *pSurInfo;
    tSurTrblDesgn *pTrbl;
    
    /* Check parameters' validity */
    if (mIsNull(pTrblDesgn))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pSurInfo = mDevDb(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    switch (trblType)
        {
        /* PW Stray packet */
        case cFmPwStrayPkt:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwStrayPkt);
            break;

        /* PW Malformed Packet */
        case cFmPwMalformedPkt:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwMalformedPkt);
            break;

        /* PW Excessive packet loss rate */
        case cFmPwExcPktLossRate:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwExcPktLossRate);
            break;

        /* PW Jitter Buffer overrun */
        case cFmPwJitBufOverrun:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwJitBufOverrun);
            break;

        /* PW Remote packet loss */
        case cFmPwRemotePktLoss:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwRemotePktLoss);
            break;

        /* PW LOFS */
        case cFmPwLofs:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.pwLofs);
            break;

        /* Performance monitoring */
        case cPmPwTca:
            pTrbl = &(pSurInfo->pDb->mastConf.troubleProf.tca);
            break;
                
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    /* Set configuration */
    OsalMemCpy((void*)pTrbl, pTrblDesgn, sizeof(tSurTrblDesgn));
     
    return cSurOk;
    }  

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwEngStatSet(tSurDev             device,
											const tSurPwId *pChnId,
											eSurEngStat         stat)

Purpose      : This API is used to set engine state for a PW.

Inputs       : device                 - device
               pChnId                 - channel ID
               stat                   - engine state

Outputs      : None

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurPwEngStatSet(tSurDev             device,
								const tSurPwId *pChnId,
								eSurEngStat         stat)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set engine state */
    if (pInfo->status.state != stat)
        {
        pInfo->status.state = stat;
        if (stat == cSurStop)
            {
            SurCurTimeInS(&(pInfo->status.stop));
            }
        else
            {
            SurCurTimeInS(&(pInfo->status.start));
            SurCurTime(&(pInfo->parm.curEngTime));

            SurCurTimeInS(&(pInfo->parm.curPerStartTime));
            SurCurTimeInS(&(pInfo->parm.curDayStartTime));
            }
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwEngStatGet(tSurDev             device,
											const tSurPwId    *pChnId,
											eSurEngStat        *pStat)

Purpose      : This API is used to get engine state of a PW.

Inputs       : device                 - device
               pChnId                 - channel ID

Outputs      : pStat                  - engine state

Return       : cSurErrSem             - if device's semaphore isn't available
               cSurErrInvlParm        - if channel type is invalid
               cSurOk                 - if success
------------------------------------------------------------------------------*/
friend eSurRet SurPwEngStatGet(tSurDev             device,
									const tSurPwId *pChnId,
									eSurEngStat         *pStat)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    
    pInfo = NULL;
    /* Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Get engine state */
    *pStat = pInfo->status.state;
    
    return cSurOk;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwChnCfgSet(tSurDev              	device,
										   const tSurPwId      *pChnId,
										   const tSurPwConf    *pChnCfg)
                         
Purpose      : This internal API is use to set PW's configuration.

Inputs       : device                 - device
               pChnId                 - channel ID
               pChnCfg                - configuration information

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwChnCfgSet(tSurDev              	device,
							   const tSurPwId      *pChnId,
							   const tSurPwConf    *pChnCfg)
    {
    /* Declare local variables */
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tSurPwInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
        
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrInvlParm;
        }
        
    /* Set configuration */
    pInfo->conf.decSoakTime    = (pChnCfg->decSoakTime != 0)? pChnCfg->decSoakTime : cSurDefaultDecSoakTime;
    pInfo->conf.terSoakTime    = (pChnCfg->terSoakTime != 0)? pChnCfg->terSoakTime : cSurDefaultTerSoakTime;
    
    /* Release device's sempahore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }    
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwChnCfgGet(tSurDev          	device,
										   const tSurPwId  *pChnId,
										   tSurPwConf      *pChnCfg)
    {
Purpose      : This API is used to set PW's configuration

Inputs       : device                 - device
               chnType                - channel type
               pChnId                 - channel ID

Outputs      : pChnCfg                - configuration information

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwChnCfgGet(tSurDev          	device,
								   const tSurPwId  *pChnId,
								   tSurPwConf      *pChnCfg)
    {
    /* Declare local variables */
    tSurPwInfo  *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pChnCfg))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrInvlParm;
        }
        
    /* Get configuration */
	pChnCfg->decSoakTime    = pInfo->conf.decSoakTime;
	pChnCfg->terSoakTime    = pInfo->conf.terSoakTime;
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwTrblNotfInhSet(tSurDev               device,
											const tSurPwId   *pChnId,
											eSurPwTrbl       trblType,
											bool                  inhibit)

Purpose      : This internal API is used to set trouble notification inhibition 
               for a PW.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               inhibit                - true/false
               
Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
/*friend*/ eSurRet SurPwTrblNotfInhSet(tSurDev               device,
										const tSurPwId   *pChnId,
										eSurPwTrbl       trblType,
										bool                  inhibit)
    {
    /* Declare local variable */
    tSurPwInfo *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* PW fault monitoring */
        /* PW Stray packet */
        case cFmPwStrayPkt:
        	pInfo->msgInh.strayPkt = inhibit;
            break;

        /* PW Malformed Packet */
        case cFmPwMalformedPkt:
        	pInfo->msgInh.malformedPkt = inhibit;
            break;

        /* PW Excessive packet loss rate */
        case cFmPwExcPktLossRate:
        	pInfo->msgInh.excPktLossRate = inhibit;
            break;

        /* PW Jitter Buffer overrun */
        case cFmPwJitBufOverrun:
        	pInfo->msgInh.jitBufOverrun = inhibit;
            break;

        /* PW Remote packet loss */
        case cFmPwRemotePktLoss:
        	pInfo->msgInh.remotePktLoss = inhibit;
            break;

        /* PW LOFS */
        case cFmPwLofs:
        	pInfo->msgInh.lofs = inhibit;
            break;

        case cFmPwAllFail:
            pInfo->msgInh.pw = inhibit;
            break;
        
        /* PW performance monitoring */
        case cPmPwTca:
            pInfo->msgInh.tca = inhibit;
            break;
        
        /* Invalid trouble type */
        default:
            ret = mDevGive(device);
            return cSurErrInvlParm;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
      
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet  SurPwTrblNotfInhGet(tSurDev               device,
												 const tSurPwId   *pChnId,
												 eSurPw		   trblType,
												 bool                  *pInhibit)

Purpose      : This internal API is used to get trouble notification inhibition 
               for a PW.

Inputs       : device                 - device
               pChnId                 - channel ID
               trblType               - trouble type
               
Outputs      : pInhibit                - true/false

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwTrblNotfInhGet(tSurDev          	 device,
										const tSurPwId  *pChnId,
										eSurPwTrbl      trblType,
										bool                 *pInhibit)
    {
    /* Declare local variable */
    tSurPwInfo *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* PW fault monitoring */
        /* PW Stray packet */
        case cFmPwStrayPkt:
        	*pInhibit = pInfo->msgInh.strayPkt;
            break;

        /* PW Malformed Packet */
        case cFmPwMalformedPkt:
        	*pInhibit = pInfo->msgInh.malformedPkt;
            break;

        /* PW Excessive packet loss rate */
        case cFmPwExcPktLossRate:
        	*pInhibit = pInfo->msgInh.excPktLossRate;
            break;

        /* PW Jitter Buffer overrun */
        case cFmPwJitBufOverrun:
        	*pInhibit = pInfo->msgInh.jitBufOverrun;
            break;

        /* PW Remote packet loss */
        case cFmPwRemotePktLoss:
        	*pInhibit = pInfo->msgInh.remotePktLoss;
            break;

        /* PW LOFS */
        case cFmPwLofs:
        	*pInhibit = pInfo->msgInh.lofs;
            break;

        case cFmPwAllFail:
            *pInhibit = pInfo->msgInh.pw;
            break;
        
        /* PW performance monitoring */
        case cPmPwTca:
            *pInhibit = pInfo->msgInh.tca;
            break;
        
        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
      
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet SurPwChnTrblNotfInhInfoGet(tSurDev             	device,
												   const tSurPwId  *pChnId,
												   tSurPwMsgInh    *pTrblInh)

Purpose      : This API is used to get PW's trouble notification
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : pTrblInh              - trouble notification inhibition

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet SurPwChnTrblNotfInhInfoGet(tSurDev             	device,
										   const tSurPwId  *pChnId,
										   tSurPwMsgInh    *pTrblInh)
    {
    tSurPwInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pTrblInh))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Return channel's trouble notification inhibition */
    OsalMemCpy(&(pInfo->msgInh), pTrblInh, sizeof(tSurPwMsgInh));
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmPwFailIndGet(tSurDev            	device,
									   const tSurPwId  *pChnId,
									   eSurPwTrbl      trblType,
									   tSurFailStat      	*pFailStat)

Purpose      : This API is used to get failure indication of a PW's
               specific failure.

Inputs       : device                - device
               pChnId                - channel ID
               trblType              - trouble type

Outputs      : pFailStat             - failure's status

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmPwFailIndGet(tSurDev            	device,
							   const tSurPwId  *pChnId,
							   eSurPwTrbl      trblType,
							   tSurFailStat      	*pFailStat)
    {
    /* Declare local variables */
    tSurPwInfo *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pFailStat))
        {
        return cSurErrNullParm;
        }
        
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
        
    /* Switch trouble type */
    switch (trblType)
        {
        /* PW Stray packet */
        case cFmPwStrayPkt:
            mSurFailStatCopy(&(pInfo->failure.strayPkt), pFailStat);
            break;

        /* PW Malformed Packet */
        case cFmPwMalformedPkt:
            mSurFailStatCopy(&(pInfo->failure.malformedPkt), pFailStat);
            break;

        /* PW Excessive packet loss rate */
        case cFmPwExcPktLossRate:
            mSurFailStatCopy(&(pInfo->failure.excPktLossRate), pFailStat);
            break;

        /* PW Jitter Buffer overrun */
        case cFmPwJitBufOverrun:
            mSurFailStatCopy(&(pInfo->failure.jitBufOverrun), pFailStat);
            break;

        /* PW Remote packet loss */
        case cFmPwRemotePktLoss:
            mSurFailStatCopy(&(pInfo->failure.remotePktLoss), pFailStat);
            break;

        /* PW LOFS */
        case cFmPwLofs:
            mSurFailStatCopy(&(pInfo->failure.lofs), pFailStat);
            break;

        /* Invalid trouble type */
        default:
            return cSurErrInvlParm;
        }
        
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : friend tSortList FmPwFailHisGet(tSurDev             device,
													const tSurPwId *pChnId)

Purpose      : This API is used to get a PW's failure history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Failure history or null if fail
------------------------------------------------------------------------------*/
friend tSortList FmPwFailHisGet(tSurDev          	 device,
									 const tSurPwId *pChnId)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
        
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Return failure history */
    return pInfo->failHis;
    }
        
/*------------------------------------------------------------------------------
Prototype    : eSurRet FmPwFailHisClear(tSurDev          	 device,
											 const tSurPwId *pChnId,
											 bool             	 flush)

Purpose      : This API is used to clear a PW's failure history

Inputs       : device                - device
               pChnId                - channel ID
               flush                 - if true, the entire history will be cleared.
                                     Otherwise, only cleared failures will be
                                     removed.

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet FmPwFailHisClear(tSurDev device,
									 const tSurPwId *pChnId,
									 bool flush)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear failure history */
    ret = FailHisClear(pInfo->failHis, flush);
    if (ret != cSurOk)
        {
        ret = mDevGive(device);
        return ret;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwRegReset(tSurDev       	device,
									 tSurPwId  *pChnId,
									 ePmPwParm  parm,
									 ePmRegType    	 regType)
                                     
Purpose      : This API is used to reset a PW's parameter's resigter.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmPwRegReset(tSurDev       	device,
							 tSurPwId  *pChnId,
							 ePmPwParm parm,
							 ePmRegType     regType)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Reset register */
    /* Process all near-end PW parameters */
    if (parm == cPmPwNeAllParm)
        {
        ret = AllPwParmRegReset(&(pInfo->parm), regType, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all near-end PW parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process all far-end PW parameters */
    else if (parm == cPmPwFeAllParm)
        {
        ret = AllPwParmRegReset(&(pInfo->parm), regType, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, "Resetting all far-end PW parameters' registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Process a single  parameter */
    else
        {
        /* Get parameter */
        ret = PwParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Reset register */
        ret = RegReset(pParm, regType);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Reseting parameter failed\n");
            return cSurErr;
            }
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
       
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwRegGet(tSurDev            	device,
									   const tSurPwId  *pChnId,
									   ePmPwParm       parm,
									   ePmRegType           regType,
									   tPmReg               *pRetReg)

Purpose      : This API is used to get a PW's parameter's register's
               value.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type. 
               regType               - register type       

Outputs      : pRetReg                - returned register

Return       : Surveillance return code
------------------------------------------------------------------------------*/                              
friend eSurRet PmPwRegGet(tSurDev            	device,
							   const tSurPwId  *pChnId,
							   ePmPwParm       parm,
							   ePmRegType           regType,
							   tPmReg               *pRetReg)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tPmParm        *pParm;
    tPmReg         *pReg;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pRetReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    pReg  = null;
    
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = PwParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
    
    /* Get register */
    ret = RegGet(pParm, regType, &pReg);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving register failed\n");
        return cSurErr;
        }
    
    /* Get register's value */
    pRetReg->invalid = pReg->invalid;
    pRetReg->value   = pReg->value;
        
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : const tSortList PmPwTcaHisGet(tSurDev device, const tSurPwId *pChnId)

Purpose      : This API is used to get a PW's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : TCA history or null if fail
------------------------------------------------------------------------------*/
friend tSortList PmPwTcaHisGet(tSurDev device, const tSurPwId *pChnId)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device) || mIsNull(pChnId))
        {
        return null;
        }
            
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return null;
        }
    
    /* Get TCA history */
    return pInfo->tcaHis;
    }        
   
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwTcaHisClear(tSurDev device, const tSurPwId *pChnId)

Purpose      : This API is used to clear a PW's TCA history.

Inputs       : device                - device
               pChnId                - channel ID

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmPwTcaHisClear(tSurDev device, const tSurPwId *pChnId)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    eListRet        lstRet;
    tSurInfo       *pSurInfo;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Clear TCA history */
    lstRet = ListFlush(pInfo->tcaHis);
    if (lstRet != cListSucc)
        {
        ret = mDevGive(device);
        mSurDebug(cSurErrMsg,"Flushing TCA history failed\n");
        return cSurErr;
        }
        
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    :  eSurRet PmPwParmAccInhSet(tSurDev          	device,
											   const tSurPwId  *pChnId,
											   ePmPwParm       parm,
											   bool             	inhibit)

Purpose      : This API is used to enable/disable a PW's accumlate
               inhibition.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               inibit                - enable/disable inhibittion 

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmPwParmAccInhSet(tSurDev          		device,
									  const tSurPwId 	*pChnId,
									  ePmPwParm       	parm,
									  bool             		inhibit)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set inhibition mode for all near-end PW parameters */
    if (parm == cPmPwNeAllParm)
        {
        ret = AllPwParmAccInhSet(&(pInfo->parm), inhibit, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                     "Setting near-end PW parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for all far-end PW parameters */
    else if (parm == cPmPwFeAllParm)
        {
        ret = AllPwParmAccInhSet(&(pInfo->parm), inhibit, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end PW parameters' inhibition mode failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set inhibition mode for a single parameter */
    else
        {
        /* Get parameter */
        ret = PwParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        /* Set accumulate inhibition mode */
        pParm->inhibit = inhibit;
        }
    
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }

/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwParmAccInhGet(tSurDev          		device,
											  const tSurPwId 	*pChnId,
											  ePmPwParm       	parm,
											  bool            		*pInhibit)

Purpose      : This API is used to get a PW's accumulate inhibition mode.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pInibit               - returned inhibition mode

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmPwParmAccInhGet(tSurDev          		device,
									  const tSurPwId 	*pChnId,
									  ePmPwParm       	parm,
									  bool           		*pInhibit)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurPwInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pInhibit))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = PwParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get accumulate inhibition mode */
    *pInhibit = pParm->inhibit;
    
    return cSurOk;
    }       
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwRegThresSet(tSurDev           	 device,
										const tSurPwId  *pChnId,
										ePmPwParm       parm,
										ePmRegType        	 regType,
										dword             	 thresVal)

Purpose      : This API is used to set threshold for a PW's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register
               thresVal              - threshold value            

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmPwRegThresSet(tSurDev           	 device,
								const tSurPwId  *pChnId,
								ePmPwParm       parm,
								ePmRegType           regType,
								dword                thresVal)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
    
    /* Set register threshold for all near-end PW parameters */
    else if (parm == cPmPwNeAllParm)
        {
        ret = AllPwParmRegThresSet(&(pInfo->parm),
                                    regType, 
                                    thresVal,
                                    true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end PW parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for all far-end PW parameters */
    else if (parm == cPmPwFeAllParm)
        {
        ret = AllPwParmRegThresSet(&(pInfo->parm),
                                     regType, 
                                     thresVal,
                                     false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end PW parameters' register threshold failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set register threshold for a single parameter */
    else
        {
        /* Get parameter */
        ret = PwParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        switch (regType)
            {
            case cPmCurPer:
                pParm->perThres = thresVal;
                break;
            case cPmCurDay:
                pParm->dayThres = thresVal;
                break;
            default:
                ret = mDevGive(device);
                return cSurErrInvlParm;
            }
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwRegThresGet(tSurDev           	 device,
											const tSurPwId  *pChnId,
											ePmPwParm        parm,
											ePmRegType        	 regType,
											dword             	 *pThresVal)

Purpose      : This API is used to get threshold for a PW's paramter's
               register.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               regType               - register type. the onlid valid types are:
                                        + cPmCurPer : threshold for period register
                                        + cPmCurDay : threshold for day register

Outputs      : pThresVal             - returned threshold value

Return       : Surveillance return code
------------------------------------------------------------------------------*/
friend eSurRet PmPwRegThresGet(tSurDev           	 device,
									const tSurPwId  *pChnId,
									ePmPwParm       parm,
									ePmRegType           regType,
									dword                *pThresVal)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurPwInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pThresVal))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = PwParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    switch (regType)
        {
        case cPmCurPer:
            *pThresVal = pParm->perThres;
            break;
        case cPmCurDay:
            *pThresVal = pParm->dayThres;
            break;
        default:
            return cSurErrInvlParm;
        }
    
    return cSurOk;
    } 
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwParmNumRecRegSet(tSurDev          	 device,
												 const tSurPwId *pChnId,
												 ePmPwParm       parm,
												 byte             	 numRecReg)

Purpose      : This API is used to set a PW's parameter's number of
               recent registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type
               numRecReg             - number of recent registers

Outputs      : None

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmPwParmNumRecRegSet(tSurDev          	 device,
										 const tSurPwId *pChnId,
										 ePmPwParm       parm,
										 byte             	 numRecReg)
    {
    /* Declare local variables */
    tSurPwInfo    *pInfo;
    eSurRet         ret;
    tSurInfo       *pSurInfo;
    tPmParm        *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(pChnId))
        {
        return cSurErrNullParm;
        }
     mPmRecRegCheck(numRecReg);
        
    /* Initialize */
    pParm = null;
    
    /* Lock device's semaphore */
    pSurInfo = mDevTake(device);
    if (mIsNull(pSurInfo))
        {
        return cSurErrDevNotFound;
        }
    
    /* Get info */
    mSurPwInfo(pSurInfo, pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        ret = mDevGive(device);
        return cSurErrNoSuchChn;
        }
        
    /* Set number of recent registers for all near-end PW parameters */
    else if (parm == cPmPwNeAllParm)
        {
        ret = AllPwParmNumRecRegSet(&(pInfo->parm), numRecReg, true);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting near-end PW parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for all far-end PW parameters */
    else if (parm == cPmPwFeAllParm)
        {
        ret = AllPwParmNumRecRegSet(&(pInfo->parm), numRecReg, false);
        if (ret != cSurOk)
            {
            mSurDebug(cSurErrMsg, 
                      "Setting far-end PW parameters' number of recent registers failed\n");
            ret = mDevGive(device);
            return cSurErr;
            }
        }
        
    /* Set number of recent registers for a single parameter */
    else
        {
        /* Get parameter */
        ret = PwParmGet(&(pInfo->parm), parm, &pParm);
        if (ret != cSurOk)
            {
            ret = mDevGive(device);
            mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
            return cSurErr;
            }
            
        mPmRecRegSet(pParm, numRecReg);
        }
          
    /* Release device's semaphore */
    ret = mDevGive(device);
    mRetCheck(ret);
    
    return cSurOk;
    }
    
/*------------------------------------------------------------------------------
Prototype    : eSurRet PmPwParmNumRecRegGet(tSurDev          	 device,
												 const tSurPwId *pChnId,
												 ePmPwParm       parm,
												 byte            	 *pNumRecReg)
                                             
Purpose      : This API is used to get a PW's parameter's number of recent
               registers.

Inputs       : device                - device
               pChnId                - channel ID
               parm                  - parameter type

Outputs      : pNumRecReg            - returned number of recent registers

Return       : Surveillance return code
------------------------------------------------------------------------------*/
public eSurRet PmPwParmNumRecRegGet(tSurDev          	 device,
										 const tSurPwId *pChnId,
										 ePmPwParm       parm,
										 byte            	 *pNumRecReg)
    {
    /* Declare local variables */
    eSurRet          ret;
    tSurPwInfo     *pInfo;
    tPmParm         *pParm;
    
    pInfo = NULL;
    /*Check parameters' validity */
    if (mIsNull(device))
        {
        return cSurErrDevNotFound;
        }
    if (mIsNull(pChnId) || mIsNull(pNumRecReg))
        {
        return cSurErrNullParm;
        }
        
    /* Initialize */
    pParm = null;
    
    /* Get info */
    mSurPwInfo(mDevDb(device), pChnId, pInfo);
    if (mIsNull(pInfo))
        {
        return cSurErrNoSuchChn;
        }
    
    /* Get parameter */
    ret = PwParmGet(&(pInfo->parm), parm, &pParm);
    if (ret != cSurOk)
        {
        mSurDebug(cSurErrMsg,"Retrieving parameter failed\n");
        return cSurErr;
        }
            
    /* Get number of recent registers */
    *pNumRecReg = pParm->numRecReg;
    
    return cSurOk;
    }
    
        

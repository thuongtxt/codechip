/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengtu3.c
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain all source code TU-3 path Transmisstion 
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/*------------------------------------------------------------------------------
Prototype    : friend void SurTu3DefUpd(tSurTu3Info *pTu3Info, tSurTu3Def *pTu3Def)

Purpose      : This function is used to update TU-3 defect database

Inputs       : pTu3Def               - TU-3 defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurTu3DefUpd(tSurTu3Info *pTu3Info, tSurTu3Def *pTu3Def)
    {
    switch (pTu3Def->defect)
        {
        /* TU-3 Path Alarm Indication Signal */
        case cFmStsAis:
        	if (pTu3Info->status.aisp.status != pTu3Def->status)
        		{
				pTu3Info->status.aisp.status = pTu3Def->status;
				pTu3Info->status.aisp.time   = pTu3Def->time;
        		}
            
            /* Clear LOP defect */
            if ((pTu3Def->status == true) && (pTu3Info->status.lopp.status == true))
                {
                pTu3Info->status.lopp.status = false;
                pTu3Info->status.lopp.time   = dCurTime;
                }
            break;
            
        /* TU-3 path Loss Of Pointer */
        case cFmStsLop:
        	if (pTu3Info->status.lopp.status != pTu3Def->status)
        		{
				pTu3Info->status.lopp.status = pTu3Def->status;
				pTu3Info->status.lopp.time   = pTu3Def->time;
        		}
            
            /* Clear AIS-P defect */
            if ((pTu3Def->status == true) && (pTu3Info->status.aisp.status == true))
                {
                pTu3Info->status.aisp.status = false;
                pTu3Info->status.aisp.time   = dCurTime;
                }
            break;
        
        /* TU-3 Path Unequipped */
        case cFmStsUneq:
        	if (pTu3Info->status.uneqp.status != pTu3Def->status)
        		{
				pTu3Info->status.uneqp.status = pTu3Def->status;
				pTu3Info->status.uneqp.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path Trace Identifier Mismatch */
        case cFmStsTim:
        	if (pTu3Info->status.timp.status != pTu3Def->status)
        		{
				pTu3Info->status.timp.status = pTu3Def->status;
				pTu3Info->status.timp.time   = pTu3Def->time;
        		}
            break;
            
        /* TU-3 Path Payload Label Mismatch */
        case cFmStsPlm:
        	if (pTu3Info->status.plmp.status != pTu3Def->status)
        		{
				pTu3Info->status.plmp.status = pTu3Def->status;
				pTu3Info->status.plmp.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path one-bit Remote Failure Indication. */
        case cFmStsRdi:
        	if (pTu3Info->status.rdip.status != pTu3Def->status)
        		{
				pTu3Info->status.rdip.status = pTu3Def->status;
				pTu3Info->status.rdip.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path Server Enhanced Remote Failure Indication. */
        case cFmStsErdiS:
        	if (pTu3Info->status.erdipS.status != pTu3Def->status)
        		{
				pTu3Info->status.erdipS.status = pTu3Def->status;
				pTu3Info->status.erdipS.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path Connectivity Enhanced Remote Failure Indication. */
        case cFmStsErdiC:
        	if (pTu3Info->status.erdipC.status != pTu3Def->status)
        		{
				pTu3Info->status.erdipC.status = pTu3Def->status;
				pTu3Info->status.erdipC.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path Payload Enhanced Remote Failure Indication */
        case cFmStsErdiP:
        	if (pTu3Info->status.erdipP.status != pTu3Def->status)
        		{
				pTu3Info->status.erdipP.status = pTu3Def->status;
				pTu3Info->status.erdipP.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path BER-based Signal Failure */
        case cFmStsBerSf:
        	if (pTu3Info->status.pathBerSf.status != pTu3Def->status)
        		{
				pTu3Info->status.pathBerSf.status = pTu3Def->status;
				pTu3Info->status.pathBerSf.time   = pTu3Def->time;
        		}
            break;
        
        /* TU-3 Path BER-based Signal Degrade */
        case cFmStsBerSd:
        	if (pTu3Info->status.pathBerSd.status != pTu3Def->status)
        		{
				pTu3Info->status.pathBerSd.status = pTu3Def->status;
				pTu3Info->status.pathBerSd.time   = pTu3Def->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void Tu3AllDefUpd(tSurInfo        *pSurInfo,
                                         const tSurTu3Id *pTu3Id)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pTu3Id                - TU-3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void Tu3AllDefUpd(tSurInfo        *pSurInfo,
                              const tSurTu3Id *pTu3Id)
    {
    bool          blNewStat;
    tSurTu3Def    def;
    tSurTu3Info  *pTu3Info;
    dword         dDefStat;

    pTu3Info = NULL;

    /* Get TU-3 information */
    mSurTu3Info(pSurInfo, pTu3Id, pTu3Info);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pTu3Id, &(def.id), sizeof(tSurTu3Id));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurTu3, pTu3Id, &dDefStat);
    
    /* TU-3 Path Alarm Indication Signal */
    blNewStat = mBitStat(dDefStat, cSurStsAisMask);
    if (blNewStat != pTu3Info->status.aisp.status)
        {
        def.defect = cFmStsAis;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }
        
    /* TU-3 path Loss Of Pointer */
    blNewStat = mBitStat(dDefStat, cSurStsLopMask);
    if (blNewStat != pTu3Info->status.lopp.status)
        {
        def.defect = cFmStsLop;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }
    
    /* TU-3 Path Unequipped */
    blNewStat = mBitStat(dDefStat, cSurStsUneqMask);
    if (blNewStat != pTu3Info->status.uneqp.status)
        {
        def.defect = cFmStsUneq;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }
    
    /* TU-3 Path Trace Identifier Mismatch */
    blNewStat = mBitStat(dDefStat, cSurStsTimMask);
    if (blNewStat != pTu3Info->status.timp.status)
        {
        def.defect = cFmStsTim;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }
        
    /* TU-3 Path Payload Label Mismatch */
    blNewStat = mBitStat(dDefStat, cSurStsPlmMask);
    if (blNewStat != pTu3Info->status.plmp.status)
        {
        def.defect = cFmStsPlm;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }
        
    /* TU-3 Path one-bit Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurStsRdiMask);
    if (blNewStat != pTu3Info->status.rdip.status)
        {
        def.defect = cFmStsRdi;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }

    /* TU-3 Path Server Enhanced Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurStsErdiSMask);
    if (blNewStat != pTu3Info->status.erdipS.status)
        {
        def.defect = cFmStsErdiS;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }

    /* TU-3 Path Connectivity Enhanced Remote Defect Indication. */
    blNewStat = mBitStat(dDefStat, cSurStsErdiCMask);
    if (blNewStat != pTu3Info->status.erdipC.status)
        {
        def.defect = cFmStsErdiC;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }

    /* TU-3 Path Payload Enhanced Remote Defect Indication */
    blNewStat = mBitStat(dDefStat, cSurStsErdiPMask);
    if (blNewStat != pTu3Info->status.erdipP.status)
        {
        def.defect = cFmStsErdiP;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }

    /* TU-3 Path BER-based Signal Failure */
    blNewStat = mBitStat(dDefStat, cSurStsBerSfMask);
    if (blNewStat != pTu3Info->status.pathBerSf.status)
        {
        def.defect = cFmStsBerSf;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }

    /* TU-3 Path BER-based Signal Degrade */
    blNewStat = mBitStat(dDefStat, cSurStsBerSdMask);
    if (blNewStat != pTu3Info->status.pathBerSd.status)
        {
        def.defect = cFmStsBerSd;
        def.status = blNewStat;
        SurTu3DefUpd(pTu3Info, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void Tu3AllFailUpd(tSurInfo        *pSurInfo,
                                          const tSurTu3Id *pTu3Id)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo              - Device database
               pTu3Id                - TU-3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void Tu3AllFailUpd(tSurInfo        *pSurInfo,
                           const tSurTu3Id *pTu3Id)
    {
    tSurTrblProf  *pTrblProf;
    tSurStsFail   *pFailInd;
    tSortList      failHis;
    tSurTu3Info   *pTu3Info;
    tSurLineInfo  *pLineInfo;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;

    pTu3Info = NULL;
    /* Get line & TU-3 information */
    mSurTu3Info(pSurInfo, pTu3Id, pTu3Info);
    mSurLineInfo(pSurInfo, &(pTu3Id->au.line), pLineInfo);
    
    /* Do not process any TU-3 failure if either LOS, LOF failure is declared at 
       section/line level */
    if ((pLineInfo->failure.los.status  == true) ||
        (pLineInfo->failure.lof.status  == true))
        {
        return;
        }
        
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pTu3Info->failure);
    failHis   = pTu3Info->failHis;
    
    /* TU-3 Path AIS-P */
    mFailIndUpd(pSurInfo, pTu3Info, aisp, aisp, blFailChg);
    if (blFailChg == true)
        {
        blNotf = true;
        if ((pLineInfo->failure.aisl.status == true) ||
            (pTu3Info->msgInh.sts           == true) ||
            (pTu3Info->msgInh.aisp          == true))
            {
            blNotf = false;
            }
        mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->aisp, failHis, failInfo, blNotf, cFmStsAis, pTrblProf->aisp);
        
        /* Update failure count register */
        if (pTu3Info->failure.aisp.status == true)
            {
            pTu3Info->parm.fcP.curSec += 1;
            }
            
        /* Clear UNEQ-P failure if it is present */
        if ((pTu3Info->failure.aisp.status  == true) &&
            (pTu3Info->failure.uneqp.status == true))
            {
            pTu3Info->failure.uneqp.status = false;
            pTu3Info->failure.uneqp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.uneqp         == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->uneqp, failHis, failInfo, blNotf, cFmStsUneq, pTrblProf->uneqp);
            }
            
        /* Clear PLM-P failure if it is present */
        if ((pTu3Info->failure.aisp.status == true) &&
            (pTu3Info->failure.plmp.status == true))
            {
            pTu3Info->failure.plmp.status = false;
            pTu3Info->failure.plmp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pTu3Info->failure.uneqp.status == true) ||
                (pTu3Info->failure.timp.status  == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.plmp          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->plmp, failHis, failInfo, blNotf, cFmStsPlm, pTrblProf->plmp);
            }
        }
        
    /* TU-3 LOP-P */
    mFailIndUpd(pSurInfo, pTu3Info, lopp, lopp, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pTu3Info->msgInh.sts  == false) && 
                  (pTu3Info->msgInh.lopp == false));
        mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->lopp, failHis, failInfo, blNotf, cFmStsLop, pTrblProf->lopp);
        
        /* Update failure count register */
        if (pTu3Info->failure.lopp.status == true)
            {
            pTu3Info->parm.fcP.curSec += 1;
            }
            
        /* Clear UNEQ-P failure if it is present */
        if ((pTu3Info->failure.lopp.status  == true) &&
            (pTu3Info->failure.uneqp.status == true))
            {
            pTu3Info->failure.uneqp.status = false;
            pTu3Info->failure.uneqp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.uneqp         == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->uneqp, failHis, failInfo, blNotf, cFmStsUneq, pTrblProf->uneqp);
            }
            
        /* Clear PLM-P failure if it is present */
        if ((pTu3Info->failure.lopp.status == true) &&
            (pTu3Info->failure.plmp.status == true))
            {
            pTu3Info->failure.plmp.status = false;
            pTu3Info->failure.plmp.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pTu3Info->failure.uneqp.status == true) ||
                (pTu3Info->failure.timp.status  == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.plmp          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->plmp, failHis, failInfo, blNotf, cFmStsPlm, pTrblProf->plmp);
            }
        }
    
    /* Only declare/clear UNEQ-P or PLM-P failure when AIS-P or LOP-P failure is 
       not present */
    if ((pTu3Info->failure.aisp.status == false) &&
        (pTu3Info->failure.lopp.status == false))
        {
        /* TU-3 UNEQ-P */
        mFailIndUpd(pSurInfo, pTu3Info, uneqp, uneqp, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.uneqp         == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->uneqp, failHis, failInfo, blNotf, cFmStsUneq, pTrblProf->uneqp);
            
            /* Update failure count register */
            if ((pTu3Info->conf.erdiEn          == true) && 
                (pTu3Info->failure.uneqp.status == true))
                {
                pTu3Info->parm.fcP.curSec += 1;
                }
            }
        
        /* TU-3 PLM-P */
        mFailIndUpd(pSurInfo, pTu3Info, plmp, plmp, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.tims.status == true) ||
                (pTu3Info->failure.uneqp.status == true) ||
                (pTu3Info->failure.timp.status  == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.plmp          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->plmp, failHis, failInfo, blNotf, cFmStsPlm, pTrblProf->plmp);
            }
        }
        
    /* TU-3 TIM-P */
    mFailIndUpd(pSurInfo, pTu3Info, timp, timp, blFailChg);
    if (blFailChg == true)
        {
         blNotf = true;
        if ((pLineInfo->failure.tims.status == true) ||
            (pTu3Info->failure.uneqp.status == true) ||
            (pTu3Info->msgInh.sts           == true) ||
            (pTu3Info->msgInh.timp          == true))
            {
            blNotf = false;
            }
        mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->timp, failHis, failInfo, blNotf, cFmStsTim, pTrblProf->timp);
        
        /* Update failure count register */
        if ((pTu3Info->conf.erdiEn         == true) && 
            (pTu3Info->failure.timp.status == true))
            {
            pTu3Info->parm.fcP.curSec += 1;
            }
        }
    
    /* If ERDI-P is not supported, declare/clear 1-bit RFI-P */
    if (pTu3Info->conf.erdiEn == false)
        {
        /* TU-3 Path one-bit Remote Failure Indication. */
        mFailIndUpd(pSurInfo, pTu3Info, rdip, rfip, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->rfip, failHis, failInfo, blNotf, cFmStsRfi, pTrblProf->rfip);
            
            /* Update failure count register */
            if (pTu3Info->failure.rfip.status == true)
                {
                pTu3Info->parm.fcPfe.curSec += 1;
                }
            }
        }
    
    /* If ERDI-P is supported, declare/clear 3-bits RFI-P */
    else
        {
        /* TU-3 Path Server Enhanced Remote Failure Indication. */
        mFailIndUpd(pSurInfo, pTu3Info, erdipS, erfipS, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->erfipS, failHis, failInfo, blNotf, cFmStsErfiS, pTrblProf->erfipS);
            
            /* Update failure count register */
            if (pTu3Info->failure.erfipS.status == true)
                {
                pTu3Info->parm.fcPfe.curSec += 1;
                }
            }

        /* TU-3 Path Connectivity Enhanced Remote Failure Indication. */
        mFailIndUpd(pSurInfo, pTu3Info, erdipC, erfipC, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->erfipC, failHis, failInfo, blNotf, cFmStsErfiC, pTrblProf->erfipC);
            
            /* Update failure count register */
            if (pTu3Info->failure.erfipC.status == true)
                {
                pTu3Info->parm.fcPfe.curSec += 1;
                }
            }

        /* TU-3 Path Payload Enhanced Remote Failure Indication */
        mFailIndUpd(pSurInfo, pTu3Info, erdipP, erfipP, blFailChg);
        if (blFailChg == true)
            {
            blNotf = true;
            if ((pLineInfo->failure.rfil.status == true) ||
                (pTu3Info->msgInh.sts           == true) ||
                (pTu3Info->msgInh.rfip          == true))
                {
                blNotf = false;
                }
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->erfipP, failHis, failInfo, blNotf, cFmStsErfiP, pTrblProf->erfipP);
            }
        }

    /* TU-3 BER-SF */
    mFailIndUpd(pSurInfo, pTu3Info, pathBerSf, pathBerSf, blFailChg);
    if (blFailChg == true)
        {
        blNotf = ((pTu3Info->msgInh.sts       == false) && 
                  (pTu3Info->msgInh.pathBerSf == false));
        mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->pathBerSf, failHis, failInfo, blNotf, cFmStsBerSf, pTrblProf->pathBerSf);
        
        /* Terminate BER-SD failure if it was already declared */
        if ((pTu3Info->failure.pathBerSf.status == true) && 
            (pTu3Info->failure.pathBerSd.status == true))
            {
            /* Clear failure indication */
            pTu3Info->failure.pathBerSd.status = false;
            pTu3Info->failure.pathBerSd.time   = dCurTime;
            
            /* Notify trouble */
            blNotf = ((pTu3Info->msgInh.sts       == false) && 
                      (pTu3Info->msgInh.pathBerSd == false));
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->pathBerSd, failHis, failInfo, blNotf, cFmStsBerSd, pTrblProf->pathBerSd);
            }
        }
    
    /* TU-3 BER-SD */
    /* Only declare/terminate BER-SD failure when BER-SF failure is not present */
    if (pTu3Info->failure.pathBerSf.status == false)
        {
        mFailIndUpd(pSurInfo, pTu3Info, pathBerSd, pathBerSd, blFailChg);
        if (blFailChg == true)
            {
            blNotf = ((pTu3Info->msgInh.sts       == false) && 
                      (pTu3Info->msgInh.pathBerSd == false));
            mFailRept(pSurInfo, cSurTu3, pTu3Id, pFailInd->pathBerSd, failHis, failInfo, blNotf, cFmStsBerSd, pTrblProf->pathBerSd);
            }
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void Tu3PmParmUpd(tSurInfo        *pSurInfo,
                                         const tSurTu3Id *pTu3Id)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo              - Device database
               pTu3Id                - TU-3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void Tu3PmParmUpd(tSurInfo        *pSurInfo,
                          const tSurTu3Id *pTu3Id)
    {
    tSurTu3Info   *pTu3Info;
    word           wTu3K;
    bool           blNewStat;
    tSurTu3Anomaly anomaly;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    tSurLineInfo  *pLineInfo;
    tSurAuInfo    *pAuInfo;
    long           pjcDiff;
    bool           blLoStat;
	tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pTu3Info = NULL;
    pAuInfo  = NULL;
    /* Get TU-3 information */
    mSurTu3Info(pSurInfo, pTu3Id, pTu3Info);
    mSurAuInfo(pSurInfo, &(pTu3Id->au), pAuInfo);
    mSurLineInfo(pSurInfo, &(pTu3Id->au.line), pLineInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pTu3Info->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pTu3Info->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pTu3Info->parm.curEngTime.minute)
        {
        pTu3Info->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pTu3Info->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pTu3Info->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Get anomaly counters and update current second registers */
    if (pSurInfo->interface.AnomalyGet != null)
        {
        OsalMemInit(&anomaly, sizeof(tSurTu3Anomaly), 0);
        pSurInfo->interface.AnomalyGet(pSurInfo->pHdl, cSurTu3, pTu3Id, &anomaly);
        pTu3Info->parm.cvP.curSec      += anomaly.b3;
        pTu3Info->parm.cvPfe.curSec    += anomaly.reip;
        pTu3Info->parm.ppjcPdet.curSec += anomaly.piInc;
        pTu3Info->parm.npjcPdet.curSec += anomaly.piDec;
        pTu3Info->parm.ppjcPgen.curSec += anomaly.pgInc;
        pTu3Info->parm.npjcPgen.curSec += anomaly.pgDec;
        }
    
    /* Check if defect exists at lower level (line, AU-n path) */
    blLoStat = ((((pLineInfo                     != null)       &&
                  (pLineInfo->status.state       == cSurStart)) && 
                 ((pLineInfo->status.los.status  == true)       ||
                  (pLineInfo->status.lof.status  == true)       ||
                  (pLineInfo->status.aisl.status == true)       ||
                  (pLineInfo->status.tims.status == true)))     ||
                (((pAuInfo                       != null)       &&
                  (pAuInfo->status.state         == cSurStart)) &&
                 ((pAuInfo->status.lopp.status   == true)       ||
                  (pAuInfo->status.aisp.status   == true)       ||
                  (pAuInfo->status.uneqp.status  == true)       ||
                  (pAuInfo->status.timp.status   == true)       ||
                  (pAuInfo->status.plmp.status   == true)))) ? true : false;
                  
    /* Update ES-P & SES-P parameters if lower layer (section/line, AU-n path) defect, or 
       AIS-P defect, or LOP-P defect, or (if ERDI-P is supported) UNEQ-P or TIM-P 
       is present */
    if ((blLoStat                        == true) ||
        (pTu3Info->status.aisp.status    == true) ||
        (pTu3Info->status.lopp.status    == true) ||
        ((pTu3Info->conf.erdiEn          == true) && 
         ((pTu3Info->status.uneqp.status == true) ||
          (pTu3Info->status.timp.status  == true))))
        {
        pTu3Info->parm.esP.curSec  = 1;
        pTu3Info->parm.sesP.curSec = 1;
        }
        
    /* Update ES-PFE & SES-PFE parameters */
    /* If ERDI-P is not supported, update ES-PFE & SES-PFE parameters only if 1-bit
       RDI-P defect (detected in G2 byte) is present */
    if (pTu3Info->conf.erdiEn == false)
        {
        if (pTu3Info->status.rdip.status == true)
            {
            pTu3Info->parm.esPfe.curSec  = 1;
            pTu3Info->parm.sesPfe.curSec = 1;
            }
        }
        
    /* If ERDI-P is supported, update ES-PFE & SES-PFE parameters only if 
       3-bits RDI-P defect (detected in G2 byte) is present */
    else
        {
        if ((pTu3Info->status.erdipS.status == true) ||
            (pTu3Info->status.erdipC.status == true))
            {
            pTu3Info->parm.esPfe.curSec  = 1;
            pTu3Info->parm.sesPfe.curSec = 1;
            }
        }
    
    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update ES-P */
        if (pTu3Info->parm.cvP.curSec != 0)
            {
            pTu3Info->parm.esP.curSec = 1;
            }

        /* Update ES-PFE */
        if (pTu3Info->parm.cvPfe.curSec != 0)
            {
            pTu3Info->parm.esPfe.curSec = 1;
            }

        /* Update SES parameters */
        wTu3K = pSurInfo->pDb->mastConf.kThres.sts1;
        
        /* TU-3 Near-end */
        if (pTu3Info->parm.cvP.curSec >= wTu3K)
            {
            pTu3Info->parm.sesP.curSec = 1;
            }

        /* TU-3 Far-end */
        if (pTu3Info->parm.cvPfe.curSec >= wTu3K)
            {
            pTu3Info->parm.sesPfe.curSec = 1;
            }
        
        /* Update TU-3 PJ-Related registers  */
        if ((pTu3Info->parm.ppjcPdet.curSec != 0) ||
            (pTu3Info->parm.npjcPdet.curSec != 0))
            {
            pTu3Info->parm.pjcsPdet.curSec = 1;
            }
        if ((pTu3Info->parm.ppjcPgen.curSec != 0) ||
            (pTu3Info->parm.npjcPgen.curSec != 0))
            {
            pTu3Info->parm.pjcsPgen.curSec = 1;
            }
        pjcDiff = (pTu3Info->parm.ppjcPgen.curSec - pTu3Info->parm.npjcPgen.curSec) - 
                  (pTu3Info->parm.ppjcPdet.curSec - pTu3Info->parm.npjcPdet.curSec);
        pTu3Info->parm.pjcDiffP.curSec = (pjcDiff > 0) ? pjcDiff : (0 - pjcDiff);
        
        /* Update UAS parameter */
        if (pTu3Info->status.neUat == true)
            {
            pTu3Info->parm.uasP.curSec = 1;
            }
        if (pTu3Info->status.feUat == true)
            {
            pTu3Info->parm.uasPfe.curSec = 1;
            }

        /* Check if TU-3 Near-End UAS just enter or exit and adjust registers */
        blNewStat = (pTu3Info->parm.sesP.curSec == 1) ? true : false;
        if (blNewStat != pTu3Info->status.neUatPot)
            {
            pTu3Info->status.nePotCnt = 1;
            }
        else
            {
            pTu3Info->status.nePotCnt++;
            }
        if (pTu3Info->status.nePotCnt == 10)
            {
            /* Update unavailable time status */
            pTu3Info->status.neUat = blNewStat;

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pTu3Info->parm.esP));
                mCurPerNegAdj(&(pTu3Info->parm.sesP));
                mCurPerNegAdj(&(pTu3Info->parm.ppjcPdet));
                mCurPerNegAdj(&(pTu3Info->parm.npjcPdet));
                mCurPerNegAdj(&(pTu3Info->parm.ppjcPgen));
                mCurPerNegAdj(&(pTu3Info->parm.npjcPgen));
                mCurPerNegAdj(&(pTu3Info->parm.pjcDiffP));
                mCurPerNegAdj(&(pTu3Info->parm.pjcsPdet));
                mCurPerNegAdj(&(pTu3Info->parm.pjcsPgen));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pTu3Info->parm.esP));
                    mPrePerNegAdj(&(pTu3Info->parm.sesP));
                    mPrePerNegAdj(&(pTu3Info->parm.ppjcPdet));
                    mPrePerNegAdj(&(pTu3Info->parm.npjcPdet));
                    mPrePerNegAdj(&(pTu3Info->parm.ppjcPgen));
                    mPrePerNegAdj(&(pTu3Info->parm.npjcPgen));
                    mPrePerNegAdj(&(pTu3Info->parm.pjcDiffP));
                    mPrePerNegAdj(&(pTu3Info->parm.pjcsPdet));
                    mPrePerNegAdj(&(pTu3Info->parm.pjcsPgen));
                    }

                /* Update UAS */
            	pTu3Info->parm.uasP.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pTu3Info->parm.cvP));
                mCurPerPosAdj(&(pTu3Info->parm.esP));
                mCurPerPosAdj(&(pTu3Info->parm.sesP));
                mCurPerPosAdj(&(pTu3Info->parm.ppjcPdet));
                mCurPerPosAdj(&(pTu3Info->parm.npjcPdet));
                mCurPerPosAdj(&(pTu3Info->parm.ppjcPgen));
                mCurPerPosAdj(&(pTu3Info->parm.npjcPgen));
                mCurPerPosAdj(&(pTu3Info->parm.pjcDiffP));
                mCurPerPosAdj(&(pTu3Info->parm.pjcsPdet));
                mCurPerPosAdj(&(pTu3Info->parm.pjcsPgen));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pTu3Info->parm.cvP));
                    mPrePerPosAdj(&(pTu3Info->parm.esP));
                    mPrePerPosAdj(&(pTu3Info->parm.sesP));
                    mPrePerPosAdj(&(pTu3Info->parm.ppjcPdet));
                    mPrePerPosAdj(&(pTu3Info->parm.npjcPdet));
                    mPrePerPosAdj(&(pTu3Info->parm.ppjcPgen));
                    mPrePerPosAdj(&(pTu3Info->parm.npjcPgen));
                    mPrePerPosAdj(&(pTu3Info->parm.pjcDiffP));
                    mPrePerPosAdj(&(pTu3Info->parm.pjcsPdet));
                    mPrePerPosAdj(&(pTu3Info->parm.pjcsPgen));
                    }

                /* Update UAS */
            	if (pTu3Info->parm.uasP.curPer.value >= 10)
            		{
            		pTu3Info->parm.uasP.curPer.value -= 10;
            		}
            	else
            		{
            		pTu3Info->parm.uasP.curPer.value = 0;
            		}
            	pTu3Info->parm.uasP.curSec = 0;
                }
            }

        /* Check if TU-3 Far-End UAS just enter or exit and adjust registers */
        blNewStat = (pTu3Info->parm.sesPfe.curSec == 1) ? true : false;
        if (blNewStat != pTu3Info->status.feUatPot)
            {
            pTu3Info->status.fePotCnt = 1;
            }
        else
            {
            pTu3Info->status.fePotCnt++;
            }
        if (pTu3Info->status.fePotCnt == 10)
            {
            /* Update unavailable time status */
            pTu3Info->status.feUat = blNewStat;
            SurCurTime(&currentTime);

            /* UAS just entered */
            if (blNewStat == true)
                {
                mCurPerNegAdj(&(pTu3Info->parm.esPfe));
                mCurPerNegAdj(&(pTu3Info->parm.sesPfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerNegAdj(&(pTu3Info->parm.esPfe));
                    mPrePerNegAdj(&(pTu3Info->parm.sesPfe));
                    }

                /* Update UAS */
            	pTu3Info->parm.uasPfe.curPer.value += 10;
                }

            /* UAS just exited */
            else
                {
                mCurPerPosAdj(&(pTu3Info->parm.cvPfe));
                mCurPerPosAdj(&(pTu3Info->parm.esPfe));
                mCurPerPosAdj(&(pTu3Info->parm.sesPfe));
                if (((currentTime.minute % 15) == 0) &&
                    (currentTime.second < 10))
                    {
                    mPrePerPosAdj(&(pTu3Info->parm.cvPfe));
                    mPrePerPosAdj(&(pTu3Info->parm.esPfe));
                    mPrePerPosAdj(&(pTu3Info->parm.sesPfe));
                    }

                /* Update UAS */
            	if (pTu3Info->parm.uasPfe.curPer.value >= 10)
            		{
            		pTu3Info->parm.uasPfe.curPer.value -= 10;
            		}
            	else
            		{
            		pTu3Info->parm.uasPfe.curPer.value = 0;
            		}
            	pTu3Info->parm.uasPfe.curSec = 0;
                }
            }

        /* Accumulate inhibition */
        pTu3Info->parm.cvP.inhibit      = false;
        pTu3Info->parm.esP.inhibit      = false;
        pTu3Info->parm.sesP.inhibit     = false;
        pTu3Info->parm.ppjcPdet.inhibit = false;
        pTu3Info->parm.npjcPdet.inhibit = false;
        pTu3Info->parm.ppjcPgen.inhibit = false;
        pTu3Info->parm.npjcPgen.inhibit = false;
        pTu3Info->parm.pjcDiffP.inhibit = false;
        pTu3Info->parm.pjcsPdet.inhibit = false;
        pTu3Info->parm.pjcsPgen.inhibit = false;
        pTu3Info->parm.cvPfe.inhibit    = false;
        pTu3Info->parm.esPfe.inhibit    = false;
        pTu3Info->parm.sesPfe.inhibit   = false;

        /* Inhibit when UAS */
        if (pTu3Info->status.neUat == true)
            {
            pTu3Info->parm.cvP.inhibit      = true;
            pTu3Info->parm.esP.inhibit      = true;
            pTu3Info->parm.sesP.inhibit     = true;
            pTu3Info->parm.ppjcPdet.inhibit = true;
            pTu3Info->parm.npjcPdet.inhibit = true;
            pTu3Info->parm.ppjcPgen.inhibit = true;
            pTu3Info->parm.npjcPgen.inhibit = true;
            pTu3Info->parm.pjcDiffP.inhibit = true;
            pTu3Info->parm.pjcsPdet.inhibit = true;
            pTu3Info->parm.pjcsPgen.inhibit = true;
            }
        if (pTu3Info->status.feUat == true)
            {
            pTu3Info->parm.cvPfe.inhibit = true;
            pTu3Info->parm.esPfe.inhibit = true;
            pTu3Info->parm.sesPfe.inhibit= true;
            }

        /* Inhibit when SES */
        if (pTu3Info->parm.sesP.curSec == 1)
            {
            pTu3Info->parm.cvP.inhibit = true;
            }
        if (pTu3Info->parm.sesPfe.curSec == 1)
            {
            pTu3Info->parm.cvPfe.inhibit = true;
            }

        /* Inhibit TU-3 Far-End parameters when AIS-P, LOP, UNEQ or lower-level 
           defects occurred */
        if ((blLoStat                      == true) ||
            (pTu3Info->status.aisp.status  == true) ||
            (pTu3Info->status.lopp.status  == true) ||
            (pTu3Info->status.uneqp.status == true))
            {
            pTu3Info->parm.cvPfe.inhibit  = true;
            pTu3Info->parm.esPfe.inhibit  = true;
            pTu3Info->parm.sesPfe.inhibit = true;
            pTu3Info->parm.uasPfe.inhibit = true;
            pTu3Info->parm.fcPfe.inhibit  = true;

            pTu3Info->parm.cvPfe.curPer.invalid  = true;
            pTu3Info->parm.esPfe.curPer.invalid  = true;                               
            pTu3Info->parm.sesPfe.curPer.invalid = true;                               
            pTu3Info->parm.uasPfe.curPer.invalid = true;                               
            pTu3Info->parm.fcPfe.curPer.invalid  = true;                               
            }

        /* Increase TU-3 Near-End adjustment registers */
        /* In available time */
        if (pTu3Info->status.neUat == false)
            {
            /* Potential for entering unavailable time */
            if (pTu3Info->parm.sesP.curSec == 1)
                {
                mNegInc(&(pTu3Info->parm.esP));
                mNegInc(&(pTu3Info->parm.sesP));
                mNegInc(&(pTu3Info->parm.ppjcPdet));
                mNegInc(&(pTu3Info->parm.npjcPdet));
                mNegInc(&(pTu3Info->parm.ppjcPgen));
                mNegInc(&(pTu3Info->parm.npjcPgen));
                mNegInc(&(pTu3Info->parm.pjcDiffP));
                mNegInc(&(pTu3Info->parm.pjcsPdet));
                mNegInc(&(pTu3Info->parm.pjcsPgen));
                }
            }
            
        /* In unavailable time */
        else
            {
            /* Potential for exiting unavailable time */
            if (pTu3Info->parm.sesP.curSec == 0)
                {
                mPosInc(&(pTu3Info->parm.cvP));
                mPosInc(&(pTu3Info->parm.esP));
                mPosInc(&(pTu3Info->parm.sesP));
                mPosInc(&(pTu3Info->parm.ppjcPdet));
                mPosInc(&(pTu3Info->parm.npjcPdet));
                mPosInc(&(pTu3Info->parm.ppjcPgen));
                mPosInc(&(pTu3Info->parm.npjcPgen));
                mPosInc(&(pTu3Info->parm.pjcDiffP));
                mPosInc(&(pTu3Info->parm.pjcsPdet));
                mPosInc(&(pTu3Info->parm.pjcsPgen));
                }
            }

        /* Increase TU-3 Far-End adjustment registers */
        if (pTu3Info->status.neUat == false)
            {
            /* In available time */
            if  (pTu3Info->status.feUat == false)
                {
                /* Potential for entering unavailable time */
                if (pTu3Info->parm.sesPfe.curSec == 1)
                    {
                    mNegInc(&(pTu3Info->parm.esPfe));
                    mNegInc(&(pTu3Info->parm.sesPfe));
                    mNegInc(&(pTu3Info->parm.ppjcPdet));
                    mNegInc(&(pTu3Info->parm.npjcPdet));
                    mNegInc(&(pTu3Info->parm.ppjcPgen));
                    mNegInc(&(pTu3Info->parm.npjcPgen));
                    mNegInc(&(pTu3Info->parm.pjcDiffP));
                    mNegInc(&(pTu3Info->parm.pjcsPdet));
                    mNegInc(&(pTu3Info->parm.pjcsPgen));
                    }
                }
            
            /* In unavailable time */
            else
                {
                /* Potential for exiting unavailable time */
                if (pTu3Info->parm.sesPfe.curSec == 0)
                    {
                    mPosInc(&(pTu3Info->parm.cvPfe));
                    mPosInc(&(pTu3Info->parm.esPfe));
                    mPosInc(&(pTu3Info->parm.sesPfe));
                    mPosInc(&(pTu3Info->parm.ppjcPdet));
                    mPosInc(&(pTu3Info->parm.npjcPdet));
                    mPosInc(&(pTu3Info->parm.ppjcPgen));
                    mPosInc(&(pTu3Info->parm.npjcPgen));
                    mPosInc(&(pTu3Info->parm.pjcDiffP));
                    mPosInc(&(pTu3Info->parm.pjcsPdet));
                    mPosInc(&(pTu3Info->parm.pjcsPgen));
                    }
                }
            }
            
        /* Clear TU-3 Near-End adjustment registers */
        if (pTu3Info->status.neUat == false)
            {
            if ((pTu3Info->parm.sesP.curSec == 0) &&
                (pTu3Info->status.neUatPot  == true))
                {
                mNegClr(&(pTu3Info->parm.esP));
                mNegClr(&(pTu3Info->parm.sesP));
                mNegClr(&(pTu3Info->parm.ppjcPdet));
                mNegClr(&(pTu3Info->parm.npjcPdet));
                mNegClr(&(pTu3Info->parm.ppjcPgen));
                mNegClr(&(pTu3Info->parm.npjcPgen));
                mNegClr(&(pTu3Info->parm.pjcDiffP));
                mNegClr(&(pTu3Info->parm.pjcsPdet));
                mNegClr(&(pTu3Info->parm.pjcsPgen));
                }
            }
        else if ((pTu3Info->parm.sesP.curSec == 1) &&
                 (pTu3Info->status.neUatPot  == false))
            {
            mPosClr(&(pTu3Info->parm.cvP));
            mPosClr(&(pTu3Info->parm.esP));
            mPosClr(&(pTu3Info->parm.sesP));
            mPosClr(&(pTu3Info->parm.ppjcPdet));
            mPosClr(&(pTu3Info->parm.npjcPdet));
            mPosClr(&(pTu3Info->parm.ppjcPgen));
            mPosClr(&(pTu3Info->parm.npjcPgen));
            mPosClr(&(pTu3Info->parm.pjcDiffP));
            mPosClr(&(pTu3Info->parm.pjcsPdet));
            mPosClr(&(pTu3Info->parm.pjcsPgen));
            }

        /* Clear TU-3 Far-End adjustment registers */
        if (pTu3Info->status.feUat == false)
            {
            if ((pTu3Info->parm.sesPfe.curSec == 0) &&
                (pTu3Info->status.feUatPot    == true))
                {
                mNegClr(&(pTu3Info->parm.esPfe));
                mNegClr(&(pTu3Info->parm.sesPfe));
                }
            }
        else if ((pTu3Info->parm.sesPfe.curSec == 1) &&
                 (pTu3Info->status.feUatPot    == false))
            {
            mPosClr(&(pTu3Info->parm.cvPfe));
            mPosClr(&(pTu3Info->parm.esPfe));
            mPosClr(&(pTu3Info->parm.sesPfe));
            }

        /* Store current second statuses */
        pTu3Info->status.neUatPot = (pTu3Info->parm.sesP.curSec   == 1) ? true : false;
        pTu3Info->status.feUatPot = (pTu3Info->parm.sesPfe.curSec == 1) ? true : false;

        /* Accumulate current period registers */
        mCurPerAcc(&(pTu3Info->parm.cvP));
        mCurPerAcc(&(pTu3Info->parm.esP));
        mCurPerAcc(&(pTu3Info->parm.sesP));
        mCurPerAcc(&(pTu3Info->parm.uasP));
        mCurPerAcc(&(pTu3Info->parm.fcP));
        mCurPerAcc(&(pTu3Info->parm.ppjcPdet));
        mCurPerAcc(&(pTu3Info->parm.npjcPdet));
        mCurPerAcc(&(pTu3Info->parm.ppjcPgen));
        mCurPerAcc(&(pTu3Info->parm.npjcPgen));
        mCurPerAcc(&(pTu3Info->parm.pjcDiffP));
        mCurPerAcc(&(pTu3Info->parm.pjcsPdet));
        mCurPerAcc(&(pTu3Info->parm.pjcsPgen));
        mCurPerAcc(&(pTu3Info->parm.cvPfe));
        mCurPerAcc(&(pTu3Info->parm.esPfe));
        mCurPerAcc(&(pTu3Info->parm.sesPfe));
        mCurPerAcc(&(pTu3Info->parm.uasPfe));
        mCurPerAcc(&(pTu3Info->parm.fcPfe));

        /* Check and report TCA */
        blNotf = ((pTu3Info->msgInh.sts == false) &&
                  (pTu3Info->msgInh.tca == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;
        
        /* Report TCAs of Path Near-End parameters */
        if ((pTu3Info->status.neUat    == false) &&
            (pTu3Info->status.neUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsCv,       &(pTu3Info->parm.cvP),      blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsEs,       &(pTu3Info->parm.esP),      blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsSes,      &(pTu3Info->parm.sesP),     blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsPpjcPdet, &(pTu3Info->parm.ppjcPdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsNpjcPdet, &(pTu3Info->parm.npjcPdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsPpjcPgen, &(pTu3Info->parm.ppjcPgen), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsNpjcPgen, &(pTu3Info->parm.npjcPgen), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsPjcDiff,  &(pTu3Info->parm.pjcDiffP), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsPjcsPdet, &(pTu3Info->parm.pjcsPdet), blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsPjcsPgen, &(pTu3Info->parm.pjcsPgen), blNotf, &tcaInfo);
            }
        
        /* Report TCAs of Path Far-End parameters */
        if ((pTu3Info->status.feUat    == false) &&
            (pTu3Info->status.feUatPot == false))
            {
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsCvPfe,    &(pTu3Info->parm.cvPfe),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsEsPfe,    &(pTu3Info->parm.esPfe),    blNotf, &tcaInfo);
            SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsSesPfe,   &(pTu3Info->parm.sesPfe),   blNotf, &tcaInfo);
            }
        
        /* Report UAS TCA */
        SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsUas,    &(pTu3Info->parm.uasP),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurTu3, pTu3Id, pTu3Info, cPmStsUasPfe, &(pTu3Info->parm.uasPfe), blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pTu3Info->parm.cvP));
        mStackDown(&(pTu3Info->parm.esP));
        mStackDown(&(pTu3Info->parm.sesP));
        mStackDown(&(pTu3Info->parm.uasP));
        mStackDown(&(pTu3Info->parm.fcP));
        mStackDown(&(pTu3Info->parm.ppjcPdet));
        mStackDown(&(pTu3Info->parm.npjcPdet));
        mStackDown(&(pTu3Info->parm.ppjcPgen));
        mStackDown(&(pTu3Info->parm.npjcPgen));
        mStackDown(&(pTu3Info->parm.pjcDiffP));
        mStackDown(&(pTu3Info->parm.pjcsPdet));
        mStackDown(&(pTu3Info->parm.pjcsPgen));
        mStackDown(&(pTu3Info->parm.cvPfe));
        mStackDown(&(pTu3Info->parm.esPfe));
        mStackDown(&(pTu3Info->parm.sesPfe));
        mStackDown(&(pTu3Info->parm.uasPfe));
        mStackDown(&(pTu3Info->parm.fcPfe));

        /* Update time for period */
        pTu3Info->parm.prePerStartTime = pTu3Info->parm.curPerStartTime;
        pTu3Info->parm.prePerEndTime = dCurTime;
        pTu3Info->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pTu3Info->parm.cvP));
        mDayShift(&(pTu3Info->parm.esP));
        mDayShift(&(pTu3Info->parm.sesP));
        mDayShift(&(pTu3Info->parm.uasP));
        mDayShift(&(pTu3Info->parm.fcP));
        mDayShift(&(pTu3Info->parm.ppjcPdet));
        mDayShift(&(pTu3Info->parm.npjcPdet));
        mDayShift(&(pTu3Info->parm.ppjcPgen));
        mDayShift(&(pTu3Info->parm.npjcPgen));
        mDayShift(&(pTu3Info->parm.pjcDiffP));
        mDayShift(&(pTu3Info->parm.pjcsPdet));
        mDayShift(&(pTu3Info->parm.pjcsPgen));
        mDayShift(&(pTu3Info->parm.cvPfe));
        mDayShift(&(pTu3Info->parm.esPfe));
        mDayShift(&(pTu3Info->parm.sesPfe));
        mDayShift(&(pTu3Info->parm.uasPfe));
        mDayShift(&(pTu3Info->parm.fcPfe));

        /* Update time for day */
        pTu3Info->parm.preDayStartTime = pTu3Info->parm.curDayStartTime;
        pTu3Info->parm.preDayEndTime = dCurTime;
        pTu3Info->parm.curDayStartTime = dCurTime;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurTu3StatUpd(tSurDev          device, 
                                         const tSurTu3Id *pTu3Id)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input TU-3.

Inputs       : device                - device 
               pTu3Id                - TU-3 ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurTu3StatUpd(tSurDev device, const tSurTu3Id *pTu3Id)
    {
    tSurInfo *pSurInfo;
    
    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) &&
        (pSurInfo->interface.DefStatGet    != null))
        {
        Tu3AllDefUpd(pSurInfo, pTu3Id);
        }
    
    /* Declare/terminate failures */
    Tu3AllFailUpd(pSurInfo, pTu3Id);
    
    /* Update performance registers */
    Tu3PmParmUpd(pSurInfo, pTu3Id);
    }

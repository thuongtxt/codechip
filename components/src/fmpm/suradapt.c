/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : suradapt.c
 *
 * Created Date: Mar 16, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "suradapt.h"
#include <sys/time.h>

/*--------------------------- Define -----------------------------------------*/
/* The constant value indicates our OSAL is used in the multithreads or multiprocesses enviroment */
#define     cOsalForMultiProcess            cAtFalse

/* The initial value of semaphore used in the message queue pool group structure */
#define     cOsalMqPoolGroupInitValSem      1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tOsalMqPool
    {
    dword           numMq;      /* The number of message queues in this pool */
    tOsalEventWord  eventWord;  /* EventWord of message queue pool */
    osalSemKey      sem;        /* The semaphore is used to avoid the conflict
                                    in operation in EventWord */
    eBool           isCreated;  /* The flag is used to indicate pool has been created
                                    cAtTrue: has been created
                                    cAtFalse: otherwise */
    }tOsalMqPool;

/* The message queue pool group structure */
typedef struct tOsalMqPoolGroup
    {
    tOsalMqPool     mqPoolGroup[cOsalMaxNumMqPool]; /* The group of message
                                                       queue pools */
#ifdef _LINUX_2_6_
    osalMutexId     sem;
#else
    osalSemKey      sem;                            /* The semaphore is used to
                                                       avoid the conflict in operation
                                                       in the message queue group */
#endif /* _LINUX_2_6_ */
    }tOsalMqPoolGroup;

/* The timer pool structure */
typedef struct tOsalTmPool
    {
    eBool       isCreated;  /* The flag is used to indicate this timer pool has been created or not.
                                cAtTrue: has been created.
                                cAtFalse: otherwise */

    tAtTmPool   *pTmPool;   /* The pointer to the timer pool */
#ifdef _LINUX_2_6_
    osalMutexId     sem;
#else
    osalSemKey  sem;        /* The semaphore is used to avoid the conflict in operation
                                in the timer pool */
#endif /* _LINUX_2_6_ */
    }tOsalTmPool;

/* The timer pool group structure */
typedef struct tOsalTmPoolGroup
    {
    tOsalTmPool tmPoolGroup[cOsalTmPoolMaxNum]; /* The group of timer pools */
    }tOsalTmPoolGroup;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
tOsalMqPoolGroup    osalMqPoolGroup;
tOsalTmPoolGroup    osalTmPoolGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*-----------------------------------------------------------------------------
Function Name: AtSemInit

Description  : Initiate a semaphore

Inputs       : procEn       - The flag is used to indicate the semaphore is
                              shared between processes or not.
                              It has 2 values:
                              cAtTrue: shared between processes
                              cAtFalse: shared between threads/tasks
               initVal      - The initiated value of semaphore

Outputs      : semKey   - The pointer to the  semaphore key

Return Code  : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet AtSemInit(atSemKey *semKey, eBool procEn, dword initVal)
    {
    if(semKey == NULL)
        {
#ifdef _OSAL_DEBUG_
        printf("Error AtSemInit : semKey == NULL\n");
#endif
        return cOsalInval;
        }

    /* Create semaphore */
    if(procEn == cAtTrue)
        {
        if(sem_init(semKey, 1, initVal) < 0)
            {
#ifdef _OSAL_DEBUG_
            printf("Error AtSemInit : sem_init procEn == cAtTrue \n");
#endif
            return cOsalFailed;
            }
        }
    else
        {
        if(sem_init(semKey, 0, initVal) < 0)
            {
#ifdef _OSAL_DEBUG_
            printf("Error AtSemInit : sem_init procEn == cAtFalse \n");
#endif
            return cOsalFailed;
            }
        }

    return cOsalOk;
    }

/*-----------------------------------------------------------------------------
Function Name: AtSemDestroy

Description  : Destroy a semaphore

Inputs       : semKey   - the pointer to the semaphore key

Outputs      : None

Return Code  : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet AtSemDestroy(atSemKey *semKey)
    {
    if(semKey == NULL)
        {
        return cOsalInval;
        }

    /* Destroy semaphore */
    if(sem_destroy(semKey) < 0)
        {
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtSemTake

Description  : Wait on a semaphore

Inputs       : semKey   - the pointer to the semaphore key

Outputs      : None

Return Code  : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet AtSemTake(atSemKey *semKey)
    {
    if(semKey == NULL)
        {
#ifdef _OSAL_DEBUG_
        printf("Error AtSemTake : semKey == NULL\n");
#endif
        return cOsalInval;
        }

    /* Wait for taking semaphore */
    if(sem_wait(semKey) < 0)
        {
#ifdef _OSAL_DEBUG_
        printf("Error AtSemTake : sem_wait\n");
#endif
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtSemGive

Description  : Give a semaphore

Inputs       : semKey   - the pointer to the semaphore key

Outputs      : None

Return Code  : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet AtSemGive(atSemKey *semKey)
    {
    if(semKey == NULL)
        {
        return cOsalInval;
        }

    /* Give semaphore */
    if(sem_post(semKey) < 0)
        {
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Prototype    : void*    OsalMemAlloc(dword size)

Purpose      : Allocate a memory area

Inputs       : size     - Size of the allocated memory in bytes

Outputs      : None

Returns      : The pointer to the allocated memory          - On success
               NULL                                         - Otherwise
------------------------------------------------------------------------------*/
void *OsalMemAlloc(dword size)
    {
    return AtOsalMemAlloc(size);
    }

/*-----------------------------------------------------------------------------
Prototype    : void OsalMemFree(void *pMem)

Purpose      : Free a memory area

Inputs       : pMem     - The pointer to the memory area

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void OsalMemFree(void *pMem)
    {
    AtOsalMemFree(pMem);
    }

/*-----------------------------------------------------------------------------
Prototype    : void OsalMemInit(void* pMem, dword size, dword val)

Purpose      : Initialize a memory area

Inputs       : pMem     - The pointer to the memory area
               val      - The initial value
               size     - The size of the memory area needed to be initiated, in bytes

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void OsalMemInit(void* pMem, dword size, dword val)
    {
    AtOsalMemInit(pMem, val, size);
    }

/*-----------------------------------------------------------------------------
Prototype    : void OsalMemCpy(void *pSrc, void *pDest, dword size)

Purpose      : Copy a memory area from source to destination area

Inputs       : pSrc     - The pointer to the source memory area
               pDest    - The pointer to the destination memory area
               size     - The size of memory area needed to be copied, in bytes

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void OsalMemCpy(void *pSrc, void *pDest, dword size)
    {
    AtOsalMemCpy(pDest, pSrc, size);
    }

/*-----------------------------------------------------------------------------
Prototype    : dword    OsalMemCmp(void *pMem1, void *pMem2, dword size)

Purpose      : Compare memory areas

Inputs       : pMem1    - The pointer to the memory area 1
               pMem2    - The pointer to the memory area 2
               size     - The size of memory area needed to be compared, in bytes

Outputs      : None

Returns      : 0        - If 2 memory areas are like
               1        - If 2 memory areas are different
------------------------------------------------------------------------------*/
dword   OsalMemCmp(void *pMem1, void *pMem2, dword size)
    {
    return AtOsalMemCmp(pMem1, pMem2, size);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalSemInit(osalSemKey *semKey, eBool procEn, dword initVal)

Purpose      : Initiate a semaphore

Inputs       : procEn       - The flag is used to indicate the semaphore is
                              shared between processes or not.
                              It has 2 values:
                              cAtTrue: shared between processes
                              cAtFalse: shared between threads/tasks
               initVal      - The initiated value of semaphore

Outputs      : semKey   - The pointer to the  semaphore key

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalSemInit(osalSemKey *semKey, eBool procEn, dword initVal)
    {
    return AtSemInit(semKey, procEn, initVal);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalSemDestroy(osalSemKey *semKey)

Purpose      : Destroy a semaphore

Inputs       : semKey   - the pointer to the semaphore key

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalSemDestroy(osalSemKey *semKey)
    {
    return AtSemDestroy(semKey);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalSemTake(osalSemKey *semKey)

Purpose      : Wait on a semaphore

Inputs       : semKey   - the pointer to the semaphore key

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalSemTake(osalSemKey *semKey)
    {
    return AtSemTake(semKey);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalSemGive(osalSemKey *semKey)

Purpose      : Give a semaphore

Inputs       : semKey   - the pointer to the semaphore key

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalSemGive(osalSemKey *semKey)
    {
    return AtSemGive(semKey);
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskCreate

Description  : Create task

Inputs       : taskHandler   - Task's handler
               data          - Data is passed to task handler
               pTaskAttr     - The pointer to task attribute structure.

Outputs      : taskId        - Task's indentifier

Return Code  : cAtOsalOk     - On success
               cAtOsalInval  - Parameter is invalid
               cAtOsalFailed - On failure
------------------------------------------------------------------------------*/
eOsalRet AtTaskCreate(osalTaskId *taskId, tOsalTaskAttr *pTaskAttr, void *(*taskHandler)(void*), void *data)
    {
    eOsalRet            ret;
#if defined(NPTL) || defined(_LINUX_2_4_)
    pthread_attr_t      attr;
#endif /* NPTL */
#ifdef _OSAL_TASK_INFO_
    eBool               blFound;
    dword               dIndex;
    dword               dFirstUnusedId = cOsalTaskInfoMaxNum + 1;
#endif /* _OSAL_TASK_INFO_ */

#ifdef USER_LINUX
    struct sched_param  schedParam;
#endif /* USER_LINUX */

    if((taskId == NULL) || (taskHandler == NULL))
        {
        return cOsalInval;
        }
#ifdef NPTL

    /* If set task attribute */
    if(pTaskAttr != NULL)
        {
        /* Initiate task attribute */
        pthread_attr_init(&attr);

        if(pTaskAttr->stackSize > 0)
            {
            if (pthread_attr_setstacksize(&attr, pTaskAttr->stackSize) != 0)
                {
                ret =  cOsalFailed;
                }
            }

        /*if(pTaskAttr->stackAddr != NULL)
            {
            if (pthread_attr_setstackaddr(&attr, pTaskAttr->stackAddr) != 0)
                {
                ret =  cOsalFailed;
                }
            }*/

        /*if((pTaskAttr->stackAddr != NULL) && (pTaskAttr->stackSize > 0))
            {
            if( pthread_attr_setstack(&attr, pTaskAttr->stackAddr, pTaskAttr->stackSize) != 0 )
                {
                ret =  cOsalFailed;
                }
            }*/

        pthread_attr_setdetachstate(&attr, pTaskAttr->detachState);

        /* In the case, setting scheduling policy and priority */
        pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        if(pthread_attr_setschedpolicy(&attr, pTaskAttr->schedPolicy) != 0)
            {
            ret =  cOsalFailed;
            }
        schedParam.sched_priority = pTaskAttr->priority;
        if(pthread_attr_setschedparam(&attr, &schedParam) != 0)
            {
            ret =  cOsalFailed;
            }


        /* Create task */
        if(pthread_create(taskId, &attr, (void*)taskHandler, data) != 0)
            {
#ifdef _OSAL_DEBUG_
            printf("Error AtTaskCreate: pthread_create\n");
#endif

            ret =  cOsalFailed;
            }
        else
            {
            ret = cOsalOk;
            }

        }

#endif /* NPTL */

#ifdef LINUXTHREAD

    /* If set task attribute */
    if(pTaskAttr != NULL)
        {

        /* Initiate task attribute */
        pthread_attr_init(&attr);

        pthread_attr_setdetachstate(&attr, pTaskAttr->detachState);

        /* In the case, setting scheduling policy and priority */
        pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        pthread_attr_setschedpolicy(&attr, pTaskAttr->schedPolicy);
        schedParam.sched_priority = pTaskAttr->priority;
        pthread_attr_setschedparam(&attr, &schedParam);

        /* Create task */
        if(pthread_create(taskId, &attr, (void*)taskHandler, data) != 0)
            {
            ret =  cOsalFailed;
            }
        else
            {
            ret = cOsalOk;
            }
        }
#endif /* LINUXTHREAD */
    else
        {
        /* Create task */
        if(pthread_create(taskId, NULL, taskHandler, data) != 0)
            {
            ret =  cOsalFailed;
            }
        else
            {
            ret = cOsalOk;
            }
        }


#ifdef _OSAL_TASK_INFO_
#ifdef _LINUX_2_6_
    if((OsalMutexLock(&osalTaskInfoSem) == cOsalOk) && (ret == cOsalOk))
#else
    if((OsalSemTake(&osalTaskInfoSem) == cOsalOk) && (ret == cOsalOk))
#endif /* _LINUX_2_6_ */
        {
        /* Search in osalTaskInfo to make sure taskId not need to add again */
        blFound = cAtFalse;
        for(dIndex = 0; dIndex < cOsalTaskInfoMaxNum; dIndex++)
            {
            if(*taskId == osalTaskInfo[dIndex].taskId)
                {
                blFound = cAtTrue;
                break;
                }
             if((osalTaskInfo[dIndex].isUsed == 0) && (dFirstUnusedId == (cOsalTaskInfoMaxNum + 1)))
                {
                dFirstUnusedId = dIndex;
                }
            }

        /* If found taskId has been added into osalTaskInfo before */
        if(blFound == cAtTrue)
            {
            int schedPol = osalTaskInfo[dIndex].schedPolicy;
#ifdef NPTL
            /* Store Scheduling policy, Priority */
            pthread_getschedparam(*taskId,
                                    &schedPol,
                                    &schedParam);
            osalTaskInfo[dIndex].schedPrio =  schedParam.sched_priority;
#endif /* NPTL */
            }
        else
            {
            if(dFirstUnusedId != (cOsalTaskInfoMaxNum + 1))
                {
                int schedPol = osalTaskInfo[dFirstUnusedId].schedPolicy;

                /* Store PTHREAD ID */
                osalTaskInfo[dFirstUnusedId].taskId = *taskId;

#ifdef NPTL
                /* Store Scheduling policy, Priority */
                pthread_getschedparam(*taskId,
                                        &schedPol,
                                        &schedParam);
                osalTaskInfo[dFirstUnusedId].schedPrio =  schedParam.sched_priority;
#endif /* NPTL */

                /* Set Used flag */
                osalTaskInfo[dFirstUnusedId].isUsed = 1;

                osalTaskInfoCurNum++;
                }
            }
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTaskInfoSem);
#else
        OsalSemGive(&osalTaskInfoSem);
#endif /* _LINUX_2_6_ */
        }

#endif /* _OSAL_TASK_INFO_ */

    return ret;
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskExit

Description  : Exit from task handler task

Inputs       : data     - the return value of task, it can be set to NULL.

Outputs      : None

Return Code  : None
------------------------------------------------------------------------------*/
void AtTaskExit(void* data)
    {
#ifdef _OSAL_TASK_INFO_
    dword               dIndex;
    osalTaskId          taskId;
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTaskInfoSem) == cOsalOk)
#else
    if(OsalSemTake(&osalTaskInfoSem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {
         /* Get task identifier */
        taskId = pthread_self();

        /* Clear Used flag */
        for(dIndex = 0; dIndex <  cOsalTaskInfoMaxNum; dIndex++)
            {
            if(taskId == osalTaskInfo[dIndex].taskId)
                {
                osalTaskInfo[dIndex].isUsed = 0;
                osalTaskInfoCurNum--;
                }
            }

#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTaskInfoSem);
#else
        OsalSemGive(&osalTaskInfoSem);
#endif /* _LINUX_2_6_ */
        }
#endif /* _OSAL_TASK_INFO_ */

    pthread_exit((void*)data);
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskWaitForTerm

Description  : Wait for the termination of another task

Inputs       : taskId        - task's indentifier

Outputs      : data          -  the pointer to the return value of task, it can
                                be set to NULL.

Return Code  : cOsalOk       - On success
               cOsalFailed   - On failure
------------------------------------------------------------------------------*/
eOsalRet AtTaskWaitForTerm(osalTaskId taskId, void **data )
    {
    if(pthread_join(taskId, data) != 0)
        {
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskCancel

Description  : Cancel the execution of another task by sending the terminating request to it

Inputs       : taskId        - task's indentifier

Outputs      : None

Return Code  : cOsalOk       - On success
               cOsalFailed   - On failure
------------------------------------------------------------------------------*/
eOsalRet AtTaskCancel(osalTaskId taskId)
    {
#ifdef _OSAL_TASK_INFO_
    dword               dIndex;
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTaskInfoSem) == cOsalOk)
#else
    if(OsalSemTake(&osalTaskInfoSem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {
        /* Clear Used flag */
        for(dIndex = 0; dIndex <  cOsalTaskInfoMaxNum; dIndex++)
            {
            if(taskId == osalTaskInfo[dIndex].taskId)
                {
                osalTaskInfo[dIndex].isUsed = 0;
                osalTaskInfoCurNum--;
                }
            }

#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTaskInfoSem);
#else
        OsalSemGive(&osalTaskInfoSem);
#endif /* _LINUX_2_6_ */
        }
#endif /* _OSAL_TASK_INFO_ */

    if(pthread_cancel(taskId) != 0)
        {
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskCancelAttrSet

Description  : Set the terminating type of task

Inputs       : type - The type indicates the way that task will process when it
                      receives the terminating request.
                      There are 2 values of this type:
                         PTHREAD_CANCEL_ASYNCHRONOUS - task will be terminated
                                                       as soon as the terminating
                                                       request is received.
                         PTHREAD_CANCEL_DEFERRED     - task will be terminated when
                                                       it call  AtTaskCancelCheck()
                                                       to check the terminating request.

Outputs      : None

Return Code  : cOsalOk       - On success
               cOsalFailed   - On failure
------------------------------------------------------------------------------*/
eOsalRet AtTaskCancelAttrSet(dword type)
    {
    if (pthread_setcanceltype(type, NULL) == 0)
        {
        return cOsalOk;
        }
    else
        {
        return cOsalFailed;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskCancelCheck

Description  : Check the terminating request

Inputs       : None

Outputs      : None

Return Code  : None
------------------------------------------------------------------------------*/
void AtTaskCancelCheck(void)
    {
    pthread_testcancel();
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskAttrInit

Description  : Initiate the task attribute structure

Inputs       : pTaskAttr    - the pointer to task attribute structure

Outputs      : None

Return Code  : cOsalOk       - On success
               cOsalInval    - Parameter is invalid
------------------------------------------------------------------------------*/
eOsalRet AtTaskAttrInit(tOsalTaskAttr *pTaskAttr)
    {
    if(pTaskAttr == NULL)
        {
        return cOsalInval;
        }
    else
        {

        pTaskAttr->stackSize = 0;
        pTaskAttr->stackAddr = NULL;
        pTaskAttr->detachState = PTHREAD_CREATE_JOINABLE;
        pTaskAttr->schedPolicy = SCHED_FIFO;
        pTaskAttr->inheritSched = PTHREAD_INHERIT_SCHED;
        pTaskAttr->priority = cOsalPrioMin;

        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtTaskSelf

Description  : Return the calling task's handle

Inputs       : None

Outputs      : None

Return Code  : Task handle
------------------------------------------------------------------------------*/
osalTaskId  AtTaskSelf(void)
    {
    return pthread_self();
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmPoolDestroy

Description  : Destroy a pool of timers

Inputs       : pTmPool  - The pointer to the timer pool

Outputs      : None

Return Code  : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
------------------------------------------------------------------------------*/
eOsalRet AtTmPoolDestroy(tAtTmPool *pTmPool)
    {
    if(pTmPool == NULL)
        {
        return cOsalInval;
        }

    /* Free the allocated memory area of timer list */
    OsalMemFree(pTmPool->tmList);

    /* Free the allocated memory area of timer pool */
    OsalMemFree(pTmPool);

    return cOsalOk;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalStart(void)

Purpose      : Start OSAL

Inputs       : None

Outputs      : cOsalOk         - On success
               cOsalFailed    - Otherwise

Returns      : None
------------------------------------------------------------------------------*/
eOsalRet OsalStart(void)
    {
    eOsalRet    ret;
    dword       i;
    dword       createdSemNum;

    /* Initialize for message queue pools */
    /* Initialize the message queue pool group structure */
    for(i = 0; i < cOsalMaxNumMqPool; i++)
        {
        osalMqPoolGroup.mqPoolGroup[i].isCreated = cAtFalse;
        }

    /* Initialize the semaphore used in message queue pool group */
#ifdef _LINUX_2_6_
    if(OsalMutexInit(&osalMqPoolGroup.sem, NULL) != cOsalOk)
#else
    if(OsalSemInit(&osalMqPoolGroup.sem, cOsalForMultiProcess, cOsalMqPoolGroupInitValSem) != cOsalOk)
#endif /* _LINUX_2_6_ */
        {
        ret = cOsalFailed;
        }
    else
        {
        ret = cOsalOk;
        }

    /* Initialize for timer pools */
    if(ret == cOsalOk)
        {
        createdSemNum = 0;

        /* Initialize the timer pool group structure */
        for(i = 0; i < cOsalTmPoolMaxNum; i++)
            {
            osalTmPoolGroup.tmPoolGroup[i].isCreated = cAtFalse;
            osalTmPoolGroup.tmPoolGroup[i].pTmPool = NULL;

            /* Initialize the semaphore which is used in the timer pool */
#ifdef _LINUX_2_6_
            if(OsalMutexInit(&osalTmPoolGroup.tmPoolGroup[i].sem, NULL) != cOsalOk)
#else
            if(OsalSemInit(&osalTmPoolGroup.tmPoolGroup[i].sem, cOsalForMultiProcess, cOsalMqPoolGroupInitValSem) != cOsalOk)
#endif /* _LINUX_2_6_ */
                {
                ret = cOsalFailed;

                /* Destroy the created semaphores */
                for(i = 0; i < createdSemNum; i++)
                    {
#ifdef _LINUX_2_6_
                    OsalMutexDestroy(&osalTmPoolGroup.tmPoolGroup[i].sem);
#else
                    OsalSemDestroy(&osalTmPoolGroup.tmPoolGroup[i].sem);
#endif /* _LINUX_2_6_ */
                    }

                break;
                }
            else
                {
                createdSemNum++;
                }
            }
        }
#ifdef _OSAL_TASK_INFO_
#ifdef _LINUX_2_6_
    if(OsalMutexInit(&osalTaskInfoSem, NULL) != cOsalOk)
        {
        ret = cOsalFailed;
        }
#else
    /* Initialize the semaphore used in access the task information structure */
    if(OsalSemInit(&osalTaskInfoSem, cOsalForMultiProcess, cOsalMqPoolGroupInitValSem) != cOsalOk)
        {
        ret = cOsalFailed;
        }
    for(i = 0; i < cOsalTaskInfoMaxNum; i++)
        {
        osalTaskInfo[i].isUsed = 0;
        }

#endif /* _LINUX_2_6_ */
    osalTaskInfoCurNum = 0;
#endif /* _OSAL_TASK_INFO_ */

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmPoolDestroy(dword  tmPoolId)

Purpose      : Destroy a pool of timers

Inputs       : tmPoolId - The timer pool identifier

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If the timer pool hasn't been created or due
                                to other failures
------------------------------------------------------------------------------*/
eOsalRet OsalTmPoolDestroy(dword  tmPoolId)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Take semaphore of this timer pool */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#else
    if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Check to know this pool has been created or not */
        if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
            {
            ret = cOsalFailed;
            }
        else
            {
            osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated = cAtFalse;
            ret = AtTmPoolDestroy(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool);
            }

        /* Give semaphore */
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#else
        OsalSemGive(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#endif /* _LINUX_2_6_ */
        }
    else
        {
        ret = cOsalFailed;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalShutdown(void)

Purpose      : Shut down OSAL

Inputs       : None

Outputs      : cOsalOk         - On success
               cOsalFailed    - Otherwise

Returns      : None
------------------------------------------------------------------------------*/
eOsalRet OsalShutdown(void)
    {
    eOsalRet    ret;
    dword       i;

    /* Destroy the semaphore used in message queue pool group */
#ifdef _LINUX_2_6_
    if(OsalMutexDestroy(&osalMqPoolGroup.sem) != cOsalOk)
#else
    if(OsalSemDestroy(&osalMqPoolGroup.sem) != cOsalOk)
#endif /* _LINUX_2_6_ */
        {
        ret = cOsalFailed;
        }
    else
        {
        ret = cOsalOk;
        }

    /* Destroy the created timer pools */
    for(i = 0; i < cOsalTmPoolMaxNum; i++)
        {
        if(osalTmPoolGroup.tmPoolGroup[i].isCreated == cAtTrue)
            {
            OsalTmPoolDestroy(i + 1);
            }

        /* Destroy the semaphore which is used in this timer pool */
#ifdef _LINUX_2_6_
        ret = OsalMutexDestroy(&osalTmPoolGroup.tmPoolGroup[i].sem);
#else
        ret = OsalSemDestroy(&osalTmPoolGroup.tmPoolGroup[i].sem);
#endif /* _LINUX_2_6_ */
        }

#ifdef _OSAL_TASK_INFO_
#ifdef _LINUX_2_6_
    ret = OsalMutexDestroy(&osalTaskInfoSem);
#else
    /* Destroy the semaphore used in access the task information structure */
    ret = OsalSemDestroy(&osalTaskInfoSem);
#endif /* _LINUX_2_6_ */
#endif /* _OSAL_TASK_INFO_ */



    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTaskCreate(osalTaskId *taskId, tOsalTaskAttr *pTaskAttr, void *(*taskHandler)(void*), void *data)

Purpose      : Create task

Inputs       : taskHandler   - Task's handler
               data          - Data is passed to task handler
               pTaskAttr     - The pointer to task attribute structure

Outputs      : taskId        - Task's indentifier

Returns      : cAtOsalOk     - On success
               cAtOsalInval  - Parameter is invalid
               cAtOsalFailed - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalTaskCreate(osalTaskId *taskId, tOsalTaskAttr *pTaskAttr, void *(*taskHandler)(void*), void *data)
    {
    return AtTaskCreate(taskId, pTaskAttr, taskHandler, data);
    }

/*-----------------------------------------------------------------------------
Prototype    : void OsalTaskExit(void* data)

Purpose      : Exit from task handler task

Inputs       : data     - the return value of task, it can be set to NULL.

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void OsalTaskExit(void* data)
    {
    AtTaskExit(data);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTaskWaitForTerm(osalTaskId taskId, void **data )

Purpose      : Wait for the termination of another task

Inputs       : taskId        - task's indentifier

Outputs      : data          -  the pointer to the return value of task, it can
                                be set to NULL.

Returns      : cOsalOk       - On success
               cOsalFailed   - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalTaskWaitForTerm(osalTaskId taskId, void **data )
    {
    return AtTaskWaitForTerm(taskId, data);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTaskCancel(osalTaskId taskId)

Purpose      : Cancel the execution of another task by sending the terminating request to it

Inputs       : taskId        - task's indentifier

Outputs      : None

Returns      : cOsalOk       - On success
               cOsalFailed   - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalTaskCancel(osalTaskId taskId)
    {
    return AtTaskCancel(taskId);
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTaskCancelAttrSet(dword type)

Purpose      : Set the terminating type of task

Inputs       : type - The type indicates the way that task will process when it
                      receives the terminating request.
                      There are 2 values of this type:
                         PTHREAD_CANCEL_ASYNCHRONOUS - task will be terminated
                                                       as soon as the terminating
                                                       request is received.
                         PTHREAD_CANCEL_DEFERRED     - task will be terminated when
                                                       it call  AtTaskCancelCheck()
                                                       to check the terminating request.

Outputs      : None

Returns      : cOsalOk       - On success
               cOsalFailed   - On failure
------------------------------------------------------------------------------*/
eOsalRet OsalTaskCancelAttrSet(dword type)
    {
    return AtTaskCancelAttrSet(type);
    }

/*-----------------------------------------------------------------------------
Prototype    : void OsalTaskCancelCheck(void)

Purpose      : Check the terminating request

Inputs       : None

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void OsalTaskCancelCheck(void)
    {
    AtTaskCancelCheck();
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTaskAttrInit(tOsalTaskAttr *pTaskAttr)

Purpose      : Initiate the task attribute structure

Inputs       : pTaskAttr    - the pointer to task attribute structure

Outputs      : None

Returns      : cOsalOk       - On success
               cOsalInval    - Parameter is invalid
------------------------------------------------------------------------------*/
eOsalRet OsalTaskAttrInit(tOsalTaskAttr *pTaskAttr)
    {
    return AtTaskAttrInit(pTaskAttr);
    }

/*-----------------------------------------------------------------------------
Prototype    : osalTaskId   OsalTaskSelf(void)

Purpose      : Return the calling task's handle

Inputs       : None

Outputs      : None

Returns      : Task handle
------------------------------------------------------------------------------*/
osalTaskId  OsalTaskSelf(void)
    {
    return AtTaskSelf();
    }

/*-----------------------------------------------------------------------------
Prototype    : void    OsalTaskInfoShow(void)

Purpose      : Display tasks' information

Inputs       : None

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void    OsalTaskInfoShow(void)
    {
#ifdef _OSAL_TASK_INFO_
    tTab   tab;
#define cOsalTaskInfoTableTmpStrLen   128
    char   pBuf[cOsalTaskInfoTableTmpStrLen];
#ifdef NPTL
    struct sched_param  param;
#define cOsalTaskInfoTableColNum   5
#else
#define cOsalTaskInfoTableColNum   3
#endif /* NPTL */
    dword               dIndex;

#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTaskInfoSem) != cOsalOk)
#else
    if(OsalSemTake(&osalTaskInfoSem) != cOsalOk)
#endif /* _LINUX_2_6_ */
        {
        return;
        }

    if(TableInit(&tab, osalTaskInfoCurNum, cOsalTaskInfoTableColNum) == cAtFalse)
        {
        return;
        }

    StrToCell(&tab.tittle[0], "TASK NAME");
    StrToCell(&tab.tittle[1], "PID");
    StrToCell(&tab.tittle[2], "TASKID");
#ifdef NPTL
    StrToCell(&tab.tittle[3], "PRIO");
    StrToCell(&tab.tittle[4], "SCHED POLICY");
#endif /* NPTL */

    for(dIndex = 0; dIndex < cOsalTaskInfoMaxNum; dIndex++)
        {
        if(osalTaskInfo[dIndex].isUsed == 0)
            {
            continue;
            }
        StrToCell(&tab.cell[dIndex][0], osalTaskInfo[dIndex].taskName);
        AtSprintf(pBuf, "%lu", osalTaskInfo[dIndex].pid);
        StrToCell(&tab.cell[dIndex][1], pBuf);
        AtSprintf(pBuf, "%lu", osalTaskInfo[dIndex].taskId);
        StrToCell(&tab.cell[dIndex][2], pBuf);

#ifdef NPTL
        if(osalTaskInfo[dIndex].schedPolicy == cOsalTaskFifoSched)
            {
            StrToCell(&tab.cell[dIndex][4], "FIFO");
            AtSprintf(pBuf, "%lu", osalTaskInfo[dIndex].schedPrio);
            StrToCell(&tab.cell[dIndex][3], pBuf);
            }
        else if(osalTaskInfo[dIndex].schedPolicy == cOsalTaskRRSched)
            {
            StrToCell(&tab.cell[dIndex][4], "RR");
            AtSprintf(pBuf, "%lu", osalTaskInfo[dIndex].schedPrio);
            StrToCell(&tab.cell[dIndex][3], pBuf);
            }
        else if(osalTaskInfo[dIndex].schedPolicy == cOsalTaskOtherSched)
            {
            StrToCell(&tab.cell[dIndex][4], "OTHER");
            sched_getparam(osalTaskInfo[dIndex].pid, &param);
            StrToCell(&tab.cell[dIndex][3], pBuf);
            }
#endif /* NPTL */

        }

    TablePrint(&tab, cAtTrue);

    TableFree(&tab);

#ifdef _LINUX_2_6_
    OsalMutexUnlock(&osalTaskInfoSem);
#else
    OsalSemGive(&osalTaskInfoSem);
#endif /* _LINUX_2_6_ */

#endif /* _OSAL_TASK_INFO_ */
    }

/*-----------------------------------------------------------------------------
Prototype    : void    OsalTaskInfoNameAdd(const char* taskName)

Purpose      : Store task name into the OSAL task information structure

Inputs       : taskName: Task's name

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void    OsalTaskInfoNameAdd(const char* taskName)
    {
#ifdef _OSAL_TASK_INFO_
    osalTaskId taskId;
    dword   dIndex;
    eBool   blFound;
    dword   dFirstUnusedId = cOsalTaskInfoMaxNum + 1;

    if(taskName == NULL)
        {
        return;
        }

    /* Search in osalTaskInfo to match taskId and add taskName */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTaskInfoSem) == cOsalOk)
#else
    if(OsalSemTake(&osalTaskInfoSem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Get task identifier */
        taskId = pthread_self();


        blFound = cAtFalse;

        for(dIndex = 0; dIndex <  cOsalTaskInfoMaxNum; dIndex++)
            {
            if(taskId == osalTaskInfo[dIndex].taskId)
                {
                /* Store PID */
                osalTaskInfo[dIndex].pid = getpid();

                /* Store Name */
                strcpy(osalTaskInfo[dIndex].taskName, taskName);

                blFound = cAtTrue;

                break;
                }
            if((osalTaskInfo[dIndex].isUsed == 0) && (dFirstUnusedId == (cOsalTaskInfoMaxNum + 1)))
                {
                dFirstUnusedId = dIndex;
                }
            }

        if((blFound == cAtFalse) && (dFirstUnusedId != (cOsalTaskInfoMaxNum + 1)))
            {
            /* Store PID */
            osalTaskInfo[dFirstUnusedId].pid = getpid();

            /* Store TASK ID */
            osalTaskInfo[dFirstUnusedId].taskId = taskId;

            /* Store Name */
            strcpy(osalTaskInfo[dFirstUnusedId].taskName, taskName);

            /* Set Used flag */
            osalTaskInfo[dFirstUnusedId].isUsed = 1;

            osalTaskInfoCurNum++;
            }

#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTaskInfoSem);
#else
        OsalSemGive(&osalTaskInfoSem);
#endif /* _LINUX_2_6_ */
        }
#endif /* _OSAL_TASK_INFO_ */
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmCurTimeGet

Description  : Get current time in microseconds counted from 1/1/1970

Inputs       : None

Outputs      : curTime  - The pointer to the current time structure

Return Code  : None
------------------------------------------------------------------------------*/
void AtTmCurTimeGet(tOsalCurTime* curTime)
    {
    struct timeval tm;

    if(curTime != NULL)
        {
        gettimeofday(&tm, NULL);
        curTime->sec = tm.tv_sec;
        curTime->usec = tm.tv_usec;
        }
    }

/*-----------------------------------------------------------------------------
Prototype    : void OsalTmCurTimeGet(tOsalCurTime* curTime)

Purpose      : Get current time in microseconds counted from 1/1/1970

Inputs       : None

Outputs      : curTime  - The pointer to the current time structure

Returns      : None
------------------------------------------------------------------------------*/
void OsalTmCurTimeGet(tOsalCurTime* curTime)
    {
    AtTmCurTimeGet(curTime);
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmStart

Description  : Start an individual timer in an individual timer pool

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool
               duration - The timer's duration in microseconds

Outputs      : None

Return Code  : cOsalOk          - On success
               cOsalInval       - Parameter is invalid
               cOsalFailed      - If timer hasn't been created
------------------------------------------------------------------------------*/
eOsalRet AtTmStart(tAtTmPool *pTmPool, dword tmId, dword duration)
    {
    tOsalCurTime curTime;

    if((pTmPool == NULL) || (tmId > pTmPool->numTm) || (tmId < 1))
        {
        return cOsalInval;
        }

    if(pTmPool->tmList[tmId - 1].isCreated == cAtFalse)
        {
        return cOsalFailed;
        }

    /* Set the value for timer information */
    OsalTmCurTimeGet(&curTime);
    pTmPool->tmList[tmId - 1].startTime.sec = curTime.sec;
    pTmPool->tmList[tmId - 1].startTime.usec = curTime.usec;
    pTmPool->tmList[tmId - 1].isStarted = cAtTrue;
    pTmPool->tmList[tmId - 1].duration = duration;

    return cOsalOk;
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmRestart

Description  : Restart  an individual timer in an individual timer pool

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool
               duration - The timer's duration in milliseconds

Outputs      : None

Return Code  : cOsalOk          - On success
               cOsalInval       - Parameter is invalid
               cOsalFailed      - If timer hasn't been created
------------------------------------------------------------------------------*/
eOsalRet AtTmRestart(tAtTmPool *pTmPool, dword tmId, dword duration)
    {
    return AtTmStart(pTmPool, tmId, duration);
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmStop

Description  : Stop an individual timer in an individual timer pool

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool

Outputs      : None

Return Code  : cOsalOk          - On success
               cOsalInval       - Parameter is invalid
               cOsalFailed      - If timer hasn't been created or started
------------------------------------------------------------------------------*/
eOsalRet AtTmStop(tAtTmPool *pTmPool, dword tmId)
    {
    if((pTmPool == NULL) || (tmId > pTmPool->numTm) || (tmId < 1))
        {
        return cOsalInval;
        }

    if((pTmPool->tmList[tmId - 1].isCreated == cAtFalse) ||
      (pTmPool->tmList[tmId - 1].isStarted == cAtFalse))
        {
        return cOsalFailed;
        }

    /* Set the value for timer information */
    pTmPool->tmList[tmId - 1].startTime.sec = 0;
    pTmPool->tmList[tmId - 1].startTime.usec = 0;
    pTmPool->tmList[tmId - 1].isStarted = cAtFalse;
    pTmPool->tmList[tmId - 1].duration = 0;

    return cOsalOk;
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmIsCreated

Description  : Check an individual timer in an individual timer pool has been
               created or not

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool

Outputs      : None

Return Code  : cOsalOk          - Timer has been created
               cOsalInval       - Parameter is invalid
               cOsalFailed      - Timer hasn't been created
------------------------------------------------------------------------------*/
eOsalRet AtTmIsCreated(tAtTmPool *pTmPool, dword tmId)
    {
    if((pTmPool == NULL) || (tmId > pTmPool->numTm) || (tmId < 1))
        {
        return cOsalInval;
        }

    if(pTmPool->tmList[tmId - 1].isCreated == cAtFalse)
        {
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmIsStarted

Description  : Check an individual timer in an individual timer pool has been
               started  or not

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool

Outputs      : None

Return Code  : cOsalOk          - Timer has been started
               cOsalInval       - Parameter is invalid
               cOsalFailed      - Timer hasn't been started
------------------------------------------------------------------------------*/
eOsalRet AtTmIsStarted(tAtTmPool *pTmPool, dword tmId)
    {
    if((pTmPool == NULL) || (tmId > pTmPool->numTm) || (tmId < 1))
        {
        return cOsalInval;
        }

    if((pTmPool->tmList[tmId - 1].isCreated == cAtFalse) ||
      (pTmPool->tmList[tmId - 1].isStarted == cAtFalse))
        {
        return cOsalFailed;
        }
    else
        {
        return cOsalOk;
        }
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmIsTimeout

Description  : Check an individual timer in an individual timer pool is timeout or not

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool

Outputs      : None

Return Code  : cOsalOk          - Timer is timeout
               cOsalInval       - Parameter is invalid
               cOsalFailed      - Timer isn't timeout or hasn't been created
                                  or started
------------------------------------------------------------------------------*/
eOsalRet AtTmIsTimeout(tAtTmPool *pTmPool, dword tmId)
    {
    dword           curDuration;
    tOsalCurTime    curTime;

    if((pTmPool == NULL) || (tmId > pTmPool->numTm) || (tmId < 1))
        {
        return cOsalInval;
        }

    if((pTmPool->tmList[tmId - 1].isCreated == cAtFalse) ||
      (pTmPool->tmList[tmId - 1].isStarted == cAtFalse))
        {
        return cOsalFailed;
        }
    else
        {
        AtTmCurTimeGet(&curTime);

        /* Calculate the current duration */
        curDuration = (curTime.sec * OneMilli) + (curTime.usec / OneMilli)  - (pTmPool->tmList[tmId - 1].startTime.sec * OneMilli) - (pTmPool->tmList[tmId - 1].startTime.usec / OneMilli);
        if (curDuration >= pTmPool->tmList[tmId - 1].duration)
            {
            return cOsalOk;
            }
        else
            {
            return cOsalFailed;
            }
        }
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmStart(dword  tmPoolId, dword  tmId, dword  duration)

Purpose      : Start a timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool
               duration     - The timer's duration in milliseconds

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created due to the
                                 other failures
------------------------------------------------------------------------------*/
eOsalRet OsalTmStart(dword  tmPoolId, dword  tmId, dword  duration)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Take semaphore of this timer pool */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#else
    if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Check to know this pool has been created or not */
        if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
            {
            ret = cOsalFailed;
            }
        else
            {
            ret = AtTmStart(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId, duration);
            }

        /* Give semaphore */
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#else
        OsalSemGive(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#endif /* _LINUX_2_6_ */
        }
    else
        {
        ret = cOsalFailed;
        }

    return ret;
    }


/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmRestart(dword  tmPoolId, dword  tmId, dword  duration)

Purpose      : Restart a timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool
               duration     - The timer's duration in milliseconds

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created due to the
                                 other failures
------------------------------------------------------------------------------*/
eOsalRet OsalTmRestart(dword  tmPoolId, dword  tmId, dword  duration)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Take semaphore of this timer pool */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#else
    if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Check to know this pool has been created or not */
        if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
            {
            ret = cOsalFailed;
            }
        else
            {
            ret = AtTmRestart(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId, duration);
            }

        /* Give semaphore */
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#else
        OsalSemGive(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#endif /* _LINUX_2_6_ */
        }
    else
        {
        ret = cOsalFailed;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmStop(dword  tmPoolId, dword  tmId)

Purpose      : Stop a timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created due to the
                                 other failures
------------------------------------------------------------------------------*/
eOsalRet OsalTmStop(dword  tmPoolId, dword  tmId)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Take semaphore of this timer pool */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#else
    if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Check to know this pool has been created or not */
        if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
            {
            ret = cOsalFailed;
            }
        else
            {
            ret = AtTmStop(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId);
            }

        /* Give semaphore */
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#else
        OsalSemGive(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#endif /* _LINUX_2_6_ */
        }
    else
        {
        ret = cOsalFailed;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmIsCreated(dword tmPoolId, dword tmId)

Purpose      : Check the existing of timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool

Outputs      : None

Returns      : cOsalOk         - The timer has been created
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created
------------------------------------------------------------------------------*/
eOsalRet OsalTmIsCreated(dword tmPoolId, dword tmId)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Check to know this pool has been created or not */
    if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
        {
        ret = cOsalFailed;
        }
    else
        {
        ret = AtTmIsCreated(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId);
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmIsStarted(dword tmPoolId, dword tmId)

Purpose      : Check the starting status of timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool

Outputs      : None

Returns      : cOsalOk         - The timer has been started
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created or
                                 it hasn't been started
------------------------------------------------------------------------------*/
eOsalRet OsalTmIsStarted(dword tmPoolId, dword tmId)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Check to know this pool has been created or not */
    if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
        {
        ret = cOsalFailed;
        }
    else
        {
        ret = AtTmIsStarted(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId);
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmIsTimeout(dword tmPoolId, dword tmId)

Purpose      : Check the timeout status of timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool

Outputs      : None

Returns      : cOsalOk         - The timer is timeout
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created or
                                 it isn't timeout
------------------------------------------------------------------------------*/
eOsalRet OsalTmIsTimeout(dword tmPoolId, dword tmId)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Check to know this pool has been created or not */
    if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
        {
        ret = cOsalFailed;
        }
    else
        {
        ret = AtTmIsTimeout(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId);
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmDestroy

Description  : Destroy a timer in an individual timer pool

Inputs       : pTmPool  - The pointer to the timer pool
               tmId     - The timer identifier, range from 1 to the number of timers
                          of timer pool

Outputs      : None

Return Code  : cOsalOk          - On success
               cOsalInval       - Parameter is invalid
               cOsalFailed      - If timer hasn't been created
------------------------------------------------------------------------------*/
eOsalRet AtTmDestroy(tAtTmPool *pTmPool, dword tmId)
    {
    if((pTmPool == NULL) || (tmId > pTmPool->numTm) || (tmId < 1))
        {
        return cOsalInval;
        }

    if(pTmPool->tmList[tmId - 1].isCreated == cAtFalse)
        {
        return cOsalFailed;
        }

    /* Reset this timer information */
    pTmPool->tmList[tmId - 1].startTime.sec = 0;
    pTmPool->tmList[tmId - 1].startTime.usec = 0;
    pTmPool->tmList[tmId - 1].isCreated = cAtFalse;
    pTmPool->tmList[tmId - 1].isStarted = cAtFalse;
    pTmPool->tmList[tmId - 1].duration = 0;

    return cOsalOk;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmDestroy(dword  tmPoolId, dword  tmId)

Purpose      : Destroy a timer

Inputs       : tmPoolId     - The timer pool identifier
               tmId         - The timer's identifier which is range from 1 to
                              the number of timers in pool

Outputs      : None

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool or timer hasn't created due to the
                                 other failures
------------------------------------------------------------------------------*/
eOsalRet OsalTmDestroy(dword  tmPoolId, dword  tmId)
    {
    eOsalRet    ret;

    if((tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Take semaphore of this timer pool */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#else
    if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Check to know this pool has been created or not */
        if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
            {
            ret = cOsalFailed;
            }
        else
            {
            ret = AtTmDestroy(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, tmId);
            }

        /* Give semaphore */
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#else
        OsalSemGive(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#endif /* _LINUX_2_6_ */
        }
    else
        {
        ret = cOsalFailed;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmCreate

Description  : Create a timer in an individual timer pool

Inputs       : pTmPool  - The pointer to the timer pool

Outputs      : pTmPool  - The pointer to the timer pool

Return Code  : cOsalOk          - On success
               cOsalInval       - Parameter is invalid
               cOsalFailed      - If there is no free timer in pool
------------------------------------------------------------------------------*/
eOsalRet AtTmCreate(tAtTmPool *pTmPool, dword *pTmId)
    {
    dword       dIndex;
    eOsalRet    ret;

    if((pTmPool == NULL) || (pTmId == NULL))
        {
        return cOsalInval;
        }

    /* Look for a free timer in pool */
    for(dIndex = 0; dIndex < pTmPool->numTm; dIndex++)
        {
        if(pTmPool->tmList[dIndex].isCreated == cAtFalse)
            {
            *pTmId = dIndex + 1;
            break;
            }
        }

    /* In the case not find a free timer */
    if(dIndex >= pTmPool->numTm)
        {
        ret = cOsalFailed;
        }
    else
        {
        pTmPool->tmList[dIndex].isCreated = cAtTrue;
        ret = cOsalOk;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmCreate(dword  tmPoolId, dword  *pTmId)

Purpose      : Create a timer

Inputs       : tmPoolId     - The timer pool identifier

Outputs      : pTmId        - The pointer to the created timer's identifier

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If this timer pool hasn't created or
                                 there's no free timer in this pool or due to the
                                 other failures
------------------------------------------------------------------------------*/
eOsalRet OsalTmCreate(dword  tmPoolId, dword  *pTmId)
    {
    eOsalRet    ret;

    if((pTmId == NULL) || (tmPoolId < 1) || (tmPoolId > cOsalTmPoolMaxNum))
        {
        return cOsalInval;
        }

    /* Take semaphore of this timer pool */
#ifdef _LINUX_2_6_
    if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#else
    if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
        {

        /* Check to know this pool has been created or not */
        if(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].isCreated == cAtFalse)
            {
            ret = cOsalFailed;
            }
        else
            {
            ret = AtTmCreate(osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].pTmPool, pTmId);
            }

        /* Give semaphore */
#ifdef _LINUX_2_6_
        OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#else
        OsalSemGive(&osalTmPoolGroup.tmPoolGroup[tmPoolId - 1].sem);
#endif /* _LINUX_2_6_ */
        }
    else
        {
        ret = cOsalFailed;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Function Name: AtTmPoolCreate

Description  : Create a pool of timers

Inputs       : numTm    - Number of timers in pool

Outputs      : None

Return Code  : The pointer to the created timer pool    - On success
               NULL                                     - Otherwise
------------------------------------------------------------------------------*/
tAtTmPool*  AtTmPoolCreate(dword numTm)
    {
    tAtTmPool*   tmPool;
    dword        index;

    /* Allocate an memory area for timer pool */
    tmPool = OsalMemAlloc(sizeof(tAtTmPool));

    if(tmPool != NULL)
        {

        /* Allocate an memory area for timer list */
        tmPool->tmList = OsalMemAlloc(sizeof(tAtTmInfor) * numTm);

        if(tmPool->tmList != NULL)
            {

            tmPool->numTm = numTm;

            /* Initialize for timers in list */
            for(index = 0; index < numTm; index++)
                {
                tmPool->tmList[index].startTime.sec = 0;
                tmPool->tmList[index].startTime.usec = 0;
                tmPool->tmList[index].isCreated = cAtFalse;
                tmPool->tmList[index].isStarted = cAtFalse;
                tmPool->tmList[index].duration = 0;
                }

            return tmPool;
            }
        else
            {

            /* Free the allocated memory area of timer pool */
            OsalMemFree(tmPool);

            return NULL;
            }
        }
    else
        {
        return NULL;
        }
    }

/*-----------------------------------------------------------------------------
Prototype    : eOsalRet OsalTmPoolCreate(dword numTm, dword  *pTmPoolId)

Purpose      : Create a pool of timers

Inputs       : numTm        - The number of timers in pool

Outputs      : pTmPoolId    - The pointer to the created timer pool's identifier.

Returns      : cOsalOk         - On success
               cOsalInval      - Parameter is invalid
               cOsalFailed     - If there's no free timer pool
------------------------------------------------------------------------------*/
eOsalRet OsalTmPoolCreate(dword numTm, dword  *pTmPoolId)
    {
    eOsalRet    ret;
    dword       i;

    if(pTmPoolId == NULL)
        {
#ifdef _OSAL_DEBUG_
        printf("Error OsalTmPoolCreate: pTmPoolId == NULL\n");
#endif
        return cOsalInval;
        }

    ret =   cOsalFailed;

    /* Look for the free timer pool */
    for(i = 0; i < cOsalTmPoolMaxNum; i++)
        {

        /* Take semaphore of timer pool */
#ifdef _LINUX_2_6_
        if(OsalMutexLock(&osalTmPoolGroup.tmPoolGroup[i].sem) == cOsalOk)
#else
        if(OsalSemTake(&osalTmPoolGroup.tmPoolGroup[i].sem) == cOsalOk)
#endif /* _LINUX_2_6_ */
            {

            /* Check to know this pool has been created or not */
            if(osalTmPoolGroup.tmPoolGroup[i].isCreated == cAtFalse)
                {

                /* Create timer pool */
                osalTmPoolGroup.tmPoolGroup[i].pTmPool = AtTmPoolCreate(numTm);

                if(osalTmPoolGroup.tmPoolGroup[i].pTmPool != NULL)
                    {
                    osalTmPoolGroup.tmPoolGroup[i].isCreated = cAtTrue;
                    *pTmPoolId = i + 1;
                    ret = cOsalOk;

                    /* Give semaphore */
#ifdef _LINUX_2_6_
                    OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[i].sem);
#else
                    OsalSemGive(&osalTmPoolGroup.tmPoolGroup[i].sem);
#endif /* _LINUX_2_6_ */
                    break;
                    }
                }

            /* Give semaphore */
#ifdef _LINUX_2_6_
            OsalMutexUnlock(&osalTmPoolGroup.tmPoolGroup[i].sem);
#else
            OsalSemGive(&osalTmPoolGroup.tmPoolGroup[i].sem);
#endif /* _LINUX_2_6_ */
            }
#ifdef _OSAL_DEBUG_
        else
            {
            printf("Error OsalTmPoolCreate: OsalSemTake\n");
            }
#endif
        }
#ifdef _OSAL_DEBUG_
    if(ret == cOsalFailed)
        {
        printf("Error OsalTmPoolCreate: There is no free timer pool\n");
        }
#endif

    return ret;
    }

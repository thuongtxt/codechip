/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - ENGINE
 *
 * File        : surengimagrp.c
 *
 * Created Date: 22-Feb-2010
 *
 * Description : This file contain all source code IMA Group Transmisstion
 *               Surveillance engine
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ---------------------------------*/
#include "sureng.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mFailGsmIndUpd(pChnInfo, defName, failName, failChg)
Purpose   : This macro is used to update failure indication of IMA Group State Machine
Inputs    : pChnInfo                 - channel information
            defName                  - defect name (in structure)
            failName                 - failure name (in structure)
            failStatVal				 - Value of LSM state that raising failure
Outputs   : failChg                  - true if failure changed status
------------------------------------------------------------------------------*/
#define mFailGsmIndUpd(pChnInfo, defName, failName, failStatVal, failChg)      \
    (failChg) = false;                                                         \
    if ((pChnInfo)->failure.failName.status != (((pChnInfo)->status.defName.status == failStatVal)? true : false))\
        {                                                                      \
		(pChnInfo)->failure.failName.status = (((pChnInfo)->status.defName.status == failStatVal)? true : false); \
		(pChnInfo)->failure.failName.time   = dCurTime;                        \
        }


/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*------------------------------------------------------------------------------
Prototype    : friend void SurImaGrpDefUpd(tSurImaGrpInfo *pImaGrpInfo, tSurImaGrpDef *pImaGrpDef)

Purpose      : This function is used to update IMA Group defect database

Inputs       : pImaGrpDef               - IMA Group defect information

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*friend*/ void SurImaGrpDefUpd(tSurImaGrpInfo *pImaGrpInfo, tSurImaGrpDef *pImaGrpDef)
    {
    switch (pImaGrpDef->defect)
        {
        /* IMA Group Near-End GSM state change */
        case cFmImaGrpGsmChange:
        	if (pImaGrpInfo->status.gsm.status != pImaGrpDef->status)
        		{
				pImaGrpInfo->status.gsm.status = pImaGrpDef->status;
				pImaGrpInfo->status.gsm.time   = pImaGrpDef->time;
        		}
            break;

        /* IMA Group Near-End GTSM state change */
        case cFmImaGrpGtsmChange:
        	if (pImaGrpInfo->status.gtsm.status != pImaGrpDef->status)
        		{
				pImaGrpInfo->status.gtsm.status = pImaGrpDef->status;
				pImaGrpInfo->status.gtsm.time   = pImaGrpDef->time;
        		}
            break;

        /* IMA Group Far-End GSM state change */
        case cFmImaGrpGsmChangeFe:
        	if (pImaGrpInfo->status.gsmfe.status != pImaGrpDef->status)
        		{
				pImaGrpInfo->status.gsmfe.status = pImaGrpDef->status;
				pImaGrpInfo->status.gsmfe.time   = pImaGrpDef->time;
        		}
            break;
            
        /* IMA Group Timing-Mismatch */
        case cFmImaGrpTimingMismatch:
        	if (pImaGrpInfo->status.timingMismatch.status != pImaGrpDef->status)
        		{
				pImaGrpInfo->status.timingMismatch.status = pImaGrpDef->status;
				pImaGrpInfo->status.timingMismatch.time   = pImaGrpDef->time;
        		}
            break;

        /* Not supported defect */
        default:
            mSurDebug(cSurErrMsg, "Not supported defect\n");
            return;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaGrpAllDefUpd(tSurInfo        *pSurInfo,
											const tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to update all defect statuses.

Inputs       : pSurInfo              - Device database
               pImaGrpId            - IMA Group ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
private void ImaGrpAllDefUpd(tSurInfo        	  *pSurInfo,
							 const tSurImaGrpId *pImaGrpId)
    {
    bool          blNewStat;
    tSurImaGrpDef    def;
    tSurImaGrpInfo  *pImaGrpInfo;
    dword         dDefStat;

    pImaGrpInfo = NULL;
    /* Get IMA Group information */
    mSurImaGrpInfo(pSurInfo, pImaGrpId, pImaGrpInfo);
    
    /* Create defect with default information */
    OsalMemCpy((void *)pImaGrpId, &(def.id), sizeof(tSurImaGrpId));
    def.time   = dCurTime;
    
    /* Get defect status */
    dDefStat = 0;
    pSurInfo->interface.DefStatGet(pSurInfo->pHdl, cSurImaGrp, pImaGrpId, &dDefStat);
    
    /* IMA Near-End Group State Machine change state */
    blNewStat = mBitStat(dDefStat, cSurImaGrpGsmChangeMask);
    if (blNewStat != pImaGrpInfo->status.gsm.status)
        {
        def.defect = cFmImaGrpGsmChange;
        def.status = blNewStat;
        SurImaGrpDefUpd(pImaGrpInfo, &def);
        }

    /* IMA Near-End Group Traffic State Machine change state */
    blNewStat = mBitStat(dDefStat, cSurImaGrpGtsmChangeMask);
    if (blNewStat != pImaGrpInfo->status.gtsm.status)
        {
        def.defect = cFmImaGrpGtsmChange;
        def.status = blNewStat;
        SurImaGrpDefUpd(pImaGrpInfo, &def);
        }

    /* IMA Far-End Group State Machine change state */
    blNewStat = mBitStat(dDefStat, cSurImaGrpGsmChangeFeMask);
    if (blNewStat != pImaGrpInfo->status.gsmfe.status)
        {
        def.defect = cFmImaGrpGsmChangeFe;
        def.status = blNewStat;
        SurImaGrpDefUpd(pImaGrpInfo, &def);
        }

    /* IMA Group Timing-Mismatch */
    blNewStat = mBitStat(dDefStat, cSurImaGrpTimingMismatchMask);
    if (blNewStat != pImaGrpInfo->status.timingMismatch.status)
        {
        def.defect = cFmImaGrpTimingMismatch;
        def.status = blNewStat;
        SurImaGrpDefUpd(pImaGrpInfo, &def);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaGrpAllFailUpd(tSurInfo        *pSurInfo,
											  const tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to update all failure statuses and also
               notify failures.

Inputs       : pSurInfo             - Device database
               pImaGrpId            - IMA Group ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ImaGrpAllFailUpd(tSurInfo        		*pSurInfo,
								   const tSurImaGrpId 	*pImaGrpId)
    {
    tSurTrblProf  *pTrblProf;
    tSurImaGrpFail   *pFailInd;
    tSortList      failHis;
    tSurImaGrpInfo   *pImaGrpInfo;
    tFmFailInfo    failInfo;
    bool           blFailChg;
    bool           blNotf;
    bool		   failStatus;

    pImaGrpInfo = NULL;
    blNotf		 = true;

    /* Get IMA Group information */
    mSurImaGrpInfo(pSurInfo, pImaGrpId, pImaGrpInfo);
    
    /* Declare/terminate failures */
    pTrblProf = &(pSurInfo->pDb->mastConf.troubleProf);
    pFailInd  = &(pImaGrpInfo->failure);
    failHis   = pImaGrpInfo->failHis;

    /* IMA Group Start up FE */
    mFailGsmIndUpd(pImaGrpInfo, gsmfe, startupFe, cFmImaGrpStatStartup, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.startupFe == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->startupFe, failHis, failInfo, blNotf, cFmImaGrpStartupFe, pTrblProf->imaGrpStartupFe);

        /* Update PM parameter Far-End FC */
        if (pImaGrpInfo->failure.startupFe.status == true)
        	{
        	pImaGrpInfo->parm.fcfe.curSec += 1;
        	}
        }

    /* IMA Group Config-Aborted */
    failStatus = (((pImaGrpInfo)->status.gsm.status == cFmImaGrpStatCfgAbrtUnsuppM) ||
				 ((pImaGrpInfo)->status.gsm.status == cFmImaGrpStatCfgAbrtInCompGrpSym) ||
				 ((pImaGrpInfo)->status.gsm.status == cFmImaGrpStatCfgAbrtUnsuppImaVer) ||
				 ((pImaGrpInfo)->status.gsm.status == cFmImaGrpStatCfgAbrtResvd1) ||
				 ((pImaGrpInfo)->status.gsm.status == cFmImaGrpStatCfgAbrtResvd2) ||
				 ((pImaGrpInfo)->status.gsm.status == cFmImaGrpStatCfgAbrtOtherReason))? 1 : 0;
    if (pImaGrpInfo->failure.cfgAborted.status != failStatus) /* Status changed */
        {
		pImaGrpInfo->failure.cfgAborted.status = failStatus;
		pImaGrpInfo->failure.cfgAborted.time   = dCurTime;

    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.cfgAborted == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->cfgAborted, failHis, failInfo, blNotf, cFmImaGrpCfgAborted, pTrblProf->imaGrpCfgAborted);

        /* Update PM parameter FC */
        if (pImaGrpInfo->failure.cfgAborted.status == true)
        	{
        	pImaGrpInfo->parm.fc.curSec += 1;
        	}
        }

    /* IMA Group Config-Aborted FE */
    failStatus = (((pImaGrpInfo)->status.gsmfe.status == cFmImaGrpStatCfgAbrtUnsuppM) ||
				 ((pImaGrpInfo)->status.gsmfe.status == cFmImaGrpStatCfgAbrtInCompGrpSym) ||
				 ((pImaGrpInfo)->status.gsmfe.status == cFmImaGrpStatCfgAbrtUnsuppImaVer) ||
				 ((pImaGrpInfo)->status.gsmfe.status == cFmImaGrpStatCfgAbrtResvd1) ||
				 ((pImaGrpInfo)->status.gsmfe.status == cFmImaGrpStatCfgAbrtResvd2) ||
				 ((pImaGrpInfo)->status.gsmfe.status == cFmImaGrpStatCfgAbrtOtherReason))? 1 : 0;
    if (pImaGrpInfo->failure.cfgAbortedFe.status != failStatus) /* Status changed */
        {
		pImaGrpInfo->failure.cfgAbortedFe.status = failStatus;
		pImaGrpInfo->failure.cfgAbortedFe.time   = dCurTime;

    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.cfgAbortedFe == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->cfgAbortedFe, failHis, failInfo, blNotf, cFmImaGrpCfgAbortedFe, pTrblProf->imaGrpCfgAbortedFe);

        /* Update PM parameter Far-End FC */
        if (pImaGrpInfo->failure.cfgAbortedFe.status == true)
        	{
        	pImaGrpInfo->parm.fcfe.curSec += 1;
        	}
        }

    /* IMA Group Insufficient-Links */
    mFailGsmIndUpd(pImaGrpInfo, gsm, insuffLink, cFmImaGrpStatInsuffLink, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.insuffLink == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->insuffLink, failHis, failInfo, blNotf, cFmImaGrpInsuffLink, pTrblProf->imaGrpInsuffLink);

        /* Update PM parameter FC */
        if (pImaGrpInfo->failure.insuffLink.status == true)
        	{
        	pImaGrpInfo->parm.fc.curSec += 1;
        	}
        }

    /* IMA Group Insufficient-Links FE */
    mFailGsmIndUpd(pImaGrpInfo, gsmfe, insuffLinkFe, cFmImaGrpStatInsuffLink, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.insuffLinkFe == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->insuffLinkFe, failHis, failInfo, blNotf, cFmImaGrpInsuffLinkFe, pTrblProf->imaGrpInsuffLinkFe);

        /* Update PM parameter Far-End FC */
        if (pImaGrpInfo->failure.insuffLinkFe.status == true)
        	{
        	pImaGrpInfo->parm.fcfe.curSec += 1;
        	}
        }

    /* IMA Group Blocked-FE */
    mFailGsmIndUpd(pImaGrpInfo, gsmfe, blockedFe, cFmImaGrpStatBlocked, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.blockedFe == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->blockedFe, failHis, failInfo, blNotf, cFmImaGrpBlockedFe, pTrblProf->imaGrpBlockedFe);

        /* Update PM parameter Far-End FC */
        if (pImaGrpInfo->failure.blockedFe.status == true)
        	{
        	pImaGrpInfo->parm.fcfe.curSec += 1;
        	}
        }

    /* IMA Group Timing-Mismatch */
    mFailIndUpd(pSurInfo, pImaGrpInfo, timingMismatch, timingMismatch, blFailChg);
    if (blFailChg == true)
        {
    	blNotf = ((pImaGrpInfo->msgInh.imaGrp == false) &&
				  (pImaGrpInfo->msgInh.timingMismatch == false));
        mFailRept(pSurInfo, cSurImaGrp, pImaGrpId, pFailInd->timingMismatch, failHis, failInfo, blNotf, cFmImaGrpTimingMismatch, pTrblProf->imaGrpTimingMismatch);
        }
    }

/*------------------------------------------------------------------------------
Prototype    : private void ImaGrpPmParmUpd(tSurInfo        	 *pSurInfo,
											 const tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to update PM parameters.

Inputs       : pSurInfo             - Device database
               pImaGrpId            - IMA Group ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
/*private*/ void ImaGrpPmParmUpd(tSurInfo        	  *pSurInfo,
								  const tSurImaGrpId *pImaGrpId)
    {
    tSurImaGrpInfo   *pImaGrpInfo;
    tPmTcaInfo     tcaInfo;
    bool           blNotf;
    tSurTime	   currentTime;
    bool		   isSecExpr;
    bool		   isPerExpr;
    bool		   isDayExpr;
    
    isSecExpr = false;
    isPerExpr = false;
    isDayExpr = false;

    pImaGrpInfo = NULL;
    /* Get IMA Group information */
    mSurImaGrpInfo(pSurInfo, pImaGrpId, pImaGrpInfo);

    /* Check if second timer expired */
    SurCurTime(&currentTime);

    if (currentTime.second != pImaGrpInfo->parm.curEngTime.second)
        {
        isSecExpr      = true;
        pImaGrpInfo->parm.curEngTime.second = currentTime.second;
        }
    else
        {
        isSecExpr = false;
        }

    /* Check if current period expired */
    if (currentTime.minute != pImaGrpInfo->parm.curEngTime.minute)
        {
        pImaGrpInfo->parm.curEngTime.minute = currentTime.minute;
        if ((currentTime.minute % cSurDefaultPmPeriod) == 0)
            {
            isPerExpr = true;
            }
        }
    else
        {
        isPerExpr = false;
        }

    /* Check if day expired */
    if (currentTime.day != pImaGrpInfo->parm.curEngTime.day)
        {
        isDayExpr   = true;
        pImaGrpInfo->parm.curEngTime.day = currentTime.day;
        }
    else
        {
        isDayExpr = false;
        }

    /* Second expired */
    if (isSecExpr == true)
        {
        /* Update UAS */
        if ((pImaGrpInfo)->status.gtsm.status == cFmImaGrpStatGtsmDown)
        	{
        	pImaGrpInfo->parm.uas.curSec = 1;
        	}

        /* Accumulate current period registers */
        mCurPerAcc(&(pImaGrpInfo->parm.uas));
        mCurPerAcc(&(pImaGrpInfo->parm.fc));
        mCurPerAcc(&(pImaGrpInfo->parm.fcfe));

        /* Check and report TCA */
        blNotf = ((pImaGrpInfo->msgInh.imaGrp    == false) &&
                  (pImaGrpInfo->msgInh.tca        == false) &&
                  (pSurInfo->interface.TcaNotf != null)) ? true : false;
        OsalMemCpy(&(pSurInfo->pDb->mastConf.troubleProf.tca),
                   &(tcaInfo.trblDesgn),
                   sizeof(tSurTrblDesgn));
        mTrblColorGet(pSurInfo, tcaInfo.trblDesgn, &(tcaInfo.color));
        tcaInfo.time = dCurTime;

        /* Report TCAs of parameters are independent of SES and UAS */
        SurTcaReport(pSurInfo, cSurImaGrp, pImaGrpId, pImaGrpInfo, cPmImaGrpUas,   &(pImaGrpInfo->parm.uas),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaGrp, pImaGrpId, pImaGrpInfo, cPmImaGrpFc,   &(pImaGrpInfo->parm.fc),   blNotf, &tcaInfo);
        SurTcaReport(pSurInfo, cSurImaGrp, pImaGrpId, pImaGrpInfo, cPmImaGrpFcfe,  &(pImaGrpInfo->parm.fcfe),  blNotf, &tcaInfo);
        }

    /* Period expired */
    if (isPerExpr == true)
        {
        mStackDown(&(pImaGrpInfo->parm.uas));
        mStackDown(&(pImaGrpInfo->parm.fc));
        mStackDown(&(pImaGrpInfo->parm.fcfe));

        /* Update time for period */
        pImaGrpInfo->parm.prePerStartTime = pImaGrpInfo->parm.curPerStartTime;
        pImaGrpInfo->parm.prePerEndTime = dCurTime;
        pImaGrpInfo->parm.curPerStartTime = dCurTime;
        }

    /* Day expired */
    if (isDayExpr == true)
        {
        mDayShift(&(pImaGrpInfo->parm.uas));
        mDayShift(&(pImaGrpInfo->parm.fc));
        mDayShift(&(pImaGrpInfo->parm.fcfe));

        /* Update time for day */
        pImaGrpInfo->parm.preDayStartTime = pImaGrpInfo->parm.curDayStartTime;
        pImaGrpInfo->parm.preDayEndTime = dCurTime;
        pImaGrpInfo->parm.curDayStartTime = dCurTime;
        }

    }

/*------------------------------------------------------------------------------
Prototype    : friend void SurImaGrpStatUpd(tSurDev          device,
											 const tSurImaGrpId *pImaGrpId)

Purpose      : This function is used to update all defect statuses and anomaly 
               counters of input IMA Group.

Inputs       : device                - device
               pImaGrpId            - IMA Group ID

Outputs      : None

Return       : None
------------------------------------------------------------------------------*/
friend void SurImaGrpStatUpd(tSurDev device, const tSurImaGrpId *pImaGrpId)
    {
    tSurInfo       *pSurInfo;

    /* Get database */
    pSurInfo = mDevDb(device);
    
    /* Update all defect statuses */
    if ((pSurInfo->pDb->mastConf.defServMd == cSurDefPoll) && 
        (pSurInfo->interface.DefStatGet    != null))
        {
        ImaGrpAllDefUpd(pSurInfo, pImaGrpId);
        }
    
    /* Declare/terminate failures */
    ImaGrpAllFailUpd(pSurInfo, pImaGrpId);
    
    /* Update performance registers */
    ImaGrpPmParmUpd(pSurInfo, pImaGrpId);
    }

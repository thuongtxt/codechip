/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : BER calculator
 *
 * File        : atber.c
 *
 * Created Date: Aug 25, 2012
 *
 * Author      : ttchi
 *
 * Description : This file contains source code of internal functions and APIs
 *               of BER module.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/


/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "atber.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtBerMaxCountEntries  10 /* Number of entries of BIP accumulation queue */

/* Undetectable BER */
#ifdef UBER
#undef UBER
#endif
#define UBER    cBit31_0

/* Time-slot for workload balance */
#define BER_TIMESLOT_NOID   0xFF
#define BER_TIMESLOT_MASK   0x3

/*--------------------------- Local Typedefs ---------------------------------*/

/**< BER thresholds entry per path type and rate  */
typedef struct tBerThresEntry
    {
    uint32 setThres; /* Declaration threshold */
    uint32 clrThres; /* Clearance threshold */
    }tBerThresEntry;

/**< BER timer */

/*
 * Accumulate bit error counts per BER rate
 */
typedef struct tBerBuffer
    {
    uint32 counts[cAtBerMaxCountEntries]; /**< The queue for storing BIP counts */
    uint8 pointer; /**< Indicate the current element of the queue */
    atbool rollover; /**< Indicate the pointer has roll-over value (MAX-MIN transition) */
    }tBerBuffer;

/**< BER engine database */
typedef struct tBerEngInfo
    {
    tBerBuffer buffer[cAtBerMaxRates]; /**< BIP Error Accumulation */

    AtBerModule module;
    void *pUsrData; /* User data */

    uint16 chn; /**< Engine's user number */
    uint8 type; /**< Channel type
                     We can retrieve the BIP threshold from the
                     Lookup Table Memory from path type. */
    atbool enable; /**< Monitoring is enabled */
    uint8 excThres; /**< EXC declaration threshold */
    uint8 degThres; /**< DEG declaration threshold */
    uint8 rate; /**< The current target BER rate */
    atbool excDef; /**< EXC/BER-SF defect */
    atbool degDef; /**< DEG/BER-SD defect */
    atbool excEvent; /**< The history of raised EXC/BER-SF defect */
    atbool degEvent; /**< The history of raised DEG/BER-SD defect */
    uint32 interval; /**< Time interval to get bit error counters */
    uint32 stampst; /**< The latest system time */
    uint8 timeslot; /**< Time-slot ID: 0-3 SONET/SDH VC-2/VC-12/VC-11 path, 255 for others */

    AtBerStatusChange staChange;
    AtBerBitErrGet bitErrGet;

    uint8 debugEnabled;
    }tBerEngInfo;

/**< BER module database */
typedef struct tAtBerDb
    {
    tAtBerOsIntf osIntf;
    void *locker; /*> Resource locker */

    tBerEngInfo *engines; /* Engine list */
    uint16 num_engines; /* Maximum number of engines */

    tBerEngInfo **active_engines; /* Active engine list */
    uint16 num_active_engines; /* Number of active engines */

    uint32 interval; /* Time period for bit error polling */

    /* Store threshold information */
    tBerThresEntry table[cAtBerMaxChnType][cAtBerMaxRates - 1];

    uint8 timeslot; /* Four time-slots for balance */

    char version[10];

    uint8 debugEnabled;
    }tAtBerDb;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/* Table of default set threshold of 50ms polling */
static const uint32
set_threshold_table_50ms[cAtBerMaxChnType][cAtBerMaxRates - 1] =
     {
     /* sonet/sdh ho paths */
     {UBER, UBER, 8271, 11340, 11728, 11767, 11771},
     {UBER, 1291, 2625,  2852,  2876,  2879,  2879},
     {UBER,  548,  672,   686,   688,   688,   688},
     { 105,  193,  207,   208,   208,   208,   208},

     /* sonet/sdh lo paths */
     {UBER,  214,  316,   330,   331,   331,   331},
     {  17,   28,   29,    29,    29,    29,    29},
     {  13,   19,   20,    20,    20,    20,    20},

     /* sonet/sdh line/ms */
     {5994, 11131, 11950, 12036, 12044, 12045, 12045},
     {1463,  2723,  2923,  2944,  2947,  2947,  2947},
     { 348,   651,   699,   704,   704,   704,   704},
     { 107,   202,   217,   219,   219,   219,   219},

     /* sonet/sdh section/rs */
     {UBER, UBER, 8463, 11721, 12135, 12177, 12182},
     {UBER, 1306, 2709,  2952,  2978,  2980,  2981},
     {UBER,  564,  696,   711,   713,   713,   713},
     { 108,  205,  220,   222,   222,   222,   222}
     };

/* Table of default clear threshold of 50ms polling */
static const uint32
clear_threshold_table_50ms[cAtBerMaxChnType][cAtBerMaxRates - 1] =
     {
     /* sonet/sdh ho paths */
     {UBER,UBER, 1236, 1279, 1283, 1283, 1283},
     {UBER, 311,  338,  341,  341,  341,  341},
     {UBER,  93,   95,   95,   95,   95,   95},
     {  33,  36,   36,   36,   36,   36,   36},

     /* sonet/sdh lo paths */
     {UBER,  50,   52,   52,   52,   52,   52},
     {   9,   9,    9,    9,    9,    9,    9},
     {   7,   8,    8,    8,    8,    8,    8},

     /* sonet/sdh line/ms */
     {1212, 1301, 1311, 1312, 1312, 1312, 1312},
     { 322,  346,  348,  348,  348,  348,  348},
     {  90,   97,   97,   97,   97,   97,   97},
     {  35,   37,   37,   37,   37,   37,   37},

     /* sonet/sdh section/rs */
     {UBER,UBER, 1276, 1321, 1326, 1326, 1326},
     {UBER, 320,  349,  352,  352,  352,  352},
     {UBER,  96,   98,   98,   98,   98,   98},
     {  35,  37,   38,   38,   38,   38,   38}
     };

/* Table of default set threshold of 100ms polling */
static const uint32
set_threshold_table_100ms[cAtBerMaxChnType][cAtBerMaxRates - 1] =
    {
     /* sonet/sdh ho paths */
     {UBER, UBER, 16650, 22824, 23604, 23684, 23692},
     {UBER, 2619,  5318,  5779,  5828,  5833,  5833},
     {UBER, 1127,  1380,  1410,  1413,  1413,  1413},
     { 222,  406,   435,   438,   438,   438,   438},

     /* sonet/sdh lo paths */
     {UBER,  445,   658,   686,   689,   689,   689},
     {  40,   64,    67,    68,    68,    68,    68},
     {  32,   46,    47,    48,    48,    48,    48},

     /* sonet/sdh line/ms */
     {12070, 22401, 24049, 24222, 24240, 24242, 24242},
     { 2968,  5516,  5922,  5964,  5969,  5969,  5969},
     {  717,  1337,  1435,  1446,  1447,  1447,  1447},
     {  227,   425,   456,   460,   460,   460,   460},

     /* sonet/sdh section/rs */
     {UBER, UBER, 17035, 23587, 24421, 24507, 24515},
     {UBER, 2650,  5487,  5979,  6031,  6037,  6037},
     {UBER, 1158,  1428,  1460,  1463,  1463,  1463},
     { 228,  430,   462,   465,   465,   465,   465}
     };

/* Table of default clear threshold of 100ms polling */
static const uint32
clear_threshold_table_100ms[cAtBerMaxChnType][cAtBerMaxRates - 1] =
    {
     /* sonet/sdh ho paths */
     {UBER, UBER, 2427, 2510, 2519, 2519, 2519},
     {UBER,  600,  652,  658,  658,  658,  658},
     {UBER,  175,  178,  179,  179,  179,  179},
     {  61,   65,   65,   65,   65,   65,   65},

     /* sonet/sdh lo paths */
     {UBER,   91,   95,   95,   96,   96,   96},
     {  15,   16,   16,   16,   16,   16,   16},
     {  12,   13,   13,   13,   13,   13,   13},

     /* sonet/sdh line/ms */
     {2381, 2556, 2574, 2576, 2576, 2576, 2576},
     { 622,  668,  672,  673,  673,  673,  673},
     { 169,  181,  182,  183,  183,  183,  183},
     {  63,   67,   68,   68,   68,   68,   68},

     /* sonet/sdh section/rs */
     {UBER, UBER, 2506, 2594, 2603, 2604, 2604},
     {UBER,  618,  674,  679,  680,  680,  680},
     {UBER,  180,  184,  184,  185,  185,  185},
     {  63,   68,   69,   69,   69,   69,   69}
    };

/* BER revision */
static char ber_version[] =
                "BER Calculator version 1.10 \n"
                "Copyright (c) 2013. Arrive Technologies Inc.\n";

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
static __inline__ uint32 BitErrorDiv10(uint32 bitErrors);

/*
 * Lock/unlock resource protection
 */
static __inline__ int BerLock(AtBerModule module)
    {
    return ((tAtBerDb*)module)->osIntf.lock(((tAtBerDb*)module)->locker);
    }
static __inline__ int BerUnlock(AtBerModule module)
    {
    return ((tAtBerDb*)module)->osIntf.unlock(((tAtBerDb*)module)->locker);
    }


/*
 * Remove item from list
 */
static __inline__ void ItemRemove(void **item_list, uint16 *num_items, void *remove_item)
    {
    uint16 idx;
    for (idx = 0; idx < *num_items; idx++)
        if (item_list[idx] == remove_item)
            {
            item_list[idx] = item_list[idx + 1];
            (*num_items)--;
            break;
            }

    for (; idx < *num_items; idx++)
        item_list[idx] = item_list[idx + 1];
    }

/*
 * Add item at the end of list
 */
static __inline__ void ItemAdd(void **item_list, uint16 *num_items, void *add_item)
    {
    item_list[*num_items] = add_item;
    (*num_items)++;
    }

/* Save OS interface */
static atbool ModuleSaveOsIntf(tAtBerDb *pModule, const tAtBerOsIntf *pOsIntf)
    {
    /* Error on null OS interface */
    if (pOsIntf == null)
        return cAtFalse;

    /* Make sure that OS interface is valid */
    if ((pOsIntf->_create == null) ||
        (pOsIntf->_delete == null) ||
        (pOsIntf->lock == null) ||
        (pOsIntf->unlock == null) ||
        (pOsIntf->gettime == null) ||
        (pOsIntf->_malloc == null) ||
        (pOsIntf->_memcpy == null) ||
        (pOsIntf->_memset == null) ||
        (pOsIntf->_free == null))
        return cAtFalse;

    /* Save */
    pOsIntf->_memcpy(&(pModule->osIntf), pOsIntf, sizeof(tAtBerOsIntf));

    return cAtTrue;
    }

/*
 * Create engine's database
 */
static atbool EngineDbCreate(tAtBerDb *pModule, uint16 maxNumEngine)
    {
    uint32 size;

    /* Create list of Line's database */
    size = sizeof(tBerEngInfo) * maxNumEngine;
    pModule->engines = pModule->osIntf._malloc(size);
    if (pModule->engines == null)
        {
        return cAtFalse;
        }
    pModule->osIntf._memset(pModule->engines, 0, size);
    pModule->num_engines = maxNumEngine;

    size = sizeof(tBerEngInfo*) * maxNumEngine;
    pModule->active_engines = pModule->osIntf._malloc(size);
    if (pModule->active_engines == null)
        {
        pModule->osIntf._free(pModule->engines);
        return cAtFalse;
        }
    pModule->osIntf._memset(pModule->active_engines, 0, size);
    pModule->num_active_engines = 0;

    return cAtTrue;
    }

/* Get a free UPSR engine */
static tBerEngInfo *EngFreeGet(AtBerModule module)
    {
    uint16 idx;
    tAtBerDb *pDb;

    /* Try to find a free engine */
    pDb = module;
    for (idx = 0; idx < pDb->num_engines; idx++)
        {
        if (pDb->engines[idx].module == null)
            return &(pDb->engines[idx]);
        }

    /* Cannot find any engine */
    return null;
    }

/*
 * Get the bit error count from hardware device.
 */
static uint32 BitErrorGet(tBerEngInfo *pEngInfo)
    {
    uint32 numBitErrors = 0;
    uint32 current, interval;

    /* Get bit error counts */
    pEngInfo->bitErrGet((void*)pEngInfo,
                        pEngInfo->chn,
                        &numBitErrors,
                        pEngInfo->pUsrData);

    /* Get current system time */
    current = ((tAtBerDb*)pEngInfo->module)->osIntf.gettime();

    /* Get the time interval from the latest getting */
    if (current > pEngInfo->stampst)
        interval = current - pEngInfo->stampst;
    /* Time roll-over */
    else if (current < pEngInfo->stampst)
        interval = (uint32)((int)current + ((-1) - (int)pEngInfo->stampst));
    else
        interval = pEngInfo->interval;
    pEngInfo->stampst = current;

    if (interval == pEngInfo->interval)
        return numBitErrors;

    /* In case the time interval differs from BER timer, the BIP count will be
     * calculated based on time interval and the BIP count will be a average value */
    else
        {
        numBitErrors *= pEngInfo->interval;
        return (uint32)(numBitErrors / interval);
        }
    }

/*
 * Get the current pointer indication
 */
static __inline__ uint8 CurPointer(tBerEngInfo *pEngInfo, uint8 ber)
    {
    return pEngInfo->buffer[ber].pointer;
    }

/*
 * Get the latest pointer indication
 */
static __inline__ uint8 LastPointer(tBerEngInfo *pEngInfo, uint8 ber)
    {
    return (uint8)((pEngInfo->buffer[ber].pointer) ? pEngInfo->buffer[ber].pointer - 1 : cAtBerMaxCountEntries - 1);
    }

/*
 * Check if the pointer's value is rolled-over
 */
static __inline__ atbool PointerIsRolled(tBerEngInfo *pEngInfo, uint8 ber)
    {
    return pEngInfo->buffer[ber].rollover;
    }

/*
 * Get the next pointer indication
 */
static __inline__ uint8 NextPointer(tBerEngInfo *pEngInfo, uint8 ber)
    {
    if (pEngInfo->buffer[ber].pointer < (cAtBerMaxCountEntries - 1))
        {
        pEngInfo->buffer[ber].rollover = cAtFalse;
        pEngInfo->buffer[ber].pointer  = (uint8)(pEngInfo->buffer[ber].pointer + 1);
        }
    else
        {
        pEngInfo->buffer[ber].rollover = cAtTrue;
        pEngInfo->buffer[ber].pointer = 0;
        }
    return pEngInfo->buffer[ber].pointer;
    }

/*
 * Get the current buffer of bit error counts in relationship with BER target
 */
static __inline__ uint32 *CurBuffer(tBerEngInfo *pEngInfo, uint8 ber)
    {
    return &(pEngInfo->buffer[ber].counts[CurPointer(pEngInfo, ber)]);
    }

/*
 * Get the threshold of defect declaration per path & related rate
 */
static __inline__ uint32 ThresDeclare(tAtBerDb *pModule, uint8 path, uint8 rate)
    {
    return pModule->table[path][rate].setThres;
    }

/*
 * Get the threshold of defect clearance per path & related rate
 */
static __inline__ uint32 ThresClearance(tAtBerDb *pModule, uint8 path, uint8 rate)
    {
    return pModule->table[path][rate].clrThres;
    }

/*
 * Get the latest buffer of bit error counts in relationship with BER target
 */
static __inline__ uint32 *LastBuffer(tBerEngInfo *pEngInfo, uint8 ber)
    {
    return &(pEngInfo->buffer[ber].counts[LastPointer(pEngInfo, ber)]);
    }

/*
 * Accumulate the bit error counts
 */
static void BitErrorAccumulater(tBerEngInfo *pEngInfo, uint32 bitErrors)
    {
    uint8 target = cAtBer10e3;

    /* Update database: accumulation process here */
    while (target < cAtBerMaxRates)
        {
        switch (target)
            {
            case cAtBer10e3:
                *CurBuffer(pEngInfo, target) = BitErrorDiv10(bitErrors);
                NextPointer(pEngInfo, target);
                break;
            case cAtBer10e4:
                *CurBuffer(pEngInfo, target) = bitErrors;
                NextPointer(pEngInfo, target);
                break;
            default:
                /* Update cascaded queues */
                *CurBuffer(pEngInfo, target) = (*CurBuffer(pEngInfo, target) + *LastBuffer(pEngInfo, (uint8)(target - 1)));

                /* If queue pointer of level N is not rolled over, do not increase
                 * queue pointer of level N+1 */
                if (!PointerIsRolled(pEngInfo, (uint8)(target - 1)))
                    return;

                /* Update next pointer */
                NextPointer(pEngInfo, target);
                *CurBuffer(pEngInfo, target) = 0;
                break;
            }

        target++;
        }

    return;
    }

/*
 * Calculate the current bit errors rate
 */
static eAtUtilBerRate BitErrorCalculator(tBerEngInfo *pEngInfo)
    {
    eAtUtilBerRate curRate;
    tAtBerDb *pModule;

    pModule = pEngInfo->module;
    curRate = cAtBer10e3;
    /* Check the rate increasing condition for rates that are higher than current rate */
    while (((curRate <= cAtBer10e4) || PointerIsRolled(pEngInfo, (uint8)(curRate - 1)))
                    && (curRate < pEngInfo->rate))
        {
        if (*LastBuffer(pEngInfo, curRate) >= ThresDeclare(pModule, pEngInfo->type, curRate))
            return curRate;
        curRate++;
        }

    /* Check the rate decreasing condition for the current rate */
    if ((curRate <= cAtBer10e4  || PointerIsRolled(pEngInfo, (uint8)(curRate - 1))) &&
        (*LastBuffer(pEngInfo, curRate) < ThresClearance(pModule, pEngInfo->type, curRate)))
        return (curRate + 1);

    return pEngInfo->rate;
    }

/*
 * Compare rate and threshold
 *
 * @return cAtTrue if rate is larger or equal to threshold.
 *         cAtFalse if rate is less than threshold.
 */
static __inline__ atbool RateCmp(uint8 rate, uint8 threshold)
    {
    if (rate > threshold)
        return cAtFalse;
    else
        return cAtTrue;
    }

/*
 * Process signal defects change
 */
static __inline__ void DefectProc(tBerEngInfo *pEngInfo, uint8 rate)
    {
    atbool excDef, degDef;

    excDef = RateCmp(rate, pEngInfo->excThres);
    degDef = RateCmp(rate, pEngInfo->degThres);
    if ((excDef != pEngInfo->excDef) || (degDef != pEngInfo->degDef))
        {
        /* Latch EXC and DEG events */
        if (excDef && !pEngInfo->excDef)
            pEngInfo->excEvent = cAtTrue;
        if (degDef && !pEngInfo->degEvent)
            pEngInfo->degEvent = cAtTrue;

        if (pEngInfo->staChange != null)
            pEngInfo->staChange((void*)pEngInfo,
                                pEngInfo->chn,
                                excDef, degDef,
                                pEngInfo->pUsrData);

        pEngInfo->excDef = excDef;
        pEngInfo->degDef = degDef;
        }
    }

/*
 * The bit error counter should be updated for each 5ms-base or 10ms-base to
 * support 10E-3 BER detection. The software cannot work under the 5ms or 10ms-base
 * timer, then a 50ms/100ms-base counter should be divided by 10 and resulted with
 * the rounding rule.
 */
static __inline__ uint32 BitErrorDiv10(uint32 bitErrors)
    {
    bitErrors = bitErrors / 10;
    if ((bitErrors % 10) < 5)
        return bitErrors;
    else
        return (bitErrors + 1);
    }

/*
 * Process engine periodically
 */
static __inline__ void EngPeriodProcess(tBerEngInfo *pEngInfo)
    {
    uint8 rate;

    /* Accumulate bit errors */
    BitErrorAccumulater(pEngInfo, BitErrorGet(pEngInfo));
    /* If bit error rate is changed, compare EXC and DEG thresholds */
    if ((rate = BitErrorCalculator(pEngInfo)) != pEngInfo->rate)
        {
        DefectProc(pEngInfo, rate);
        pEngInfo->rate = rate;
        }
    }

/*
 * Multiple polling time unit depends on channel type
 */
static __inline__ uint32 MultiUnits(uint32 unit, eAtBerChnType chnType)
    {
    switch (chnType)
        {
        /* single unit */
        case cAtBerPathVc416c:
        case cAtBerPathVc44c:
        case cAtBerPathVc4:
        case cAtBerPathVc3:
        case cAtBerLineStm0:
        case cAtBerLineStm1:
        case cAtBerLineStm4:
        case cAtBerLineStm16:
        case cAtBerLineStm0RS:
        case cAtBerLineStm1RS:
        case cAtBerLineStm4RS:
        case cAtBerLineStm16RS:
            return unit;
            break;

        /* multiple of 4 units */
        case cAtBerPathVc2:
        case cAtBerPathVc12:
        case cAtBerPathVc11:
            return unit << 2;

        case cAtBerMaxChnType:
        default:
            return 0;
        }
    }

/**
 * Create BER module
 *
 * @param numEngines Number of BER engines.
 * @param timer      Timer for BIP/Error bits polling.
 * @param pOsIntf    OS interface
 *
 * @return BER module pointer.
 */
AtBerModule AtBerModuleCreate(uint16 numEngines, uint32 timer, const tAtBerOsIntf *pOsIntf)
    {
    tAtBerDb *pDb;
    uint8 idx, idy;

    /* Cannot create with number of lines is 0 */
    if ((numEngines == 0) || ((timer != cAtBerTimer50ms) && (timer != cAtBerTimer100ms)))
        return null;

    /* Create database */
    if (pOsIntf == null || !pOsIntf->_malloc || !pOsIntf->_memset)
        return null;

    pDb = pOsIntf->_malloc(sizeof(tAtBerDb));
    if (pDb == null)
        return null;
    pOsIntf->_memset(pDb, 0, sizeof(tAtBerDb));

    /* Save OS interface */
    if (!ModuleSaveOsIntf(pDb, pOsIntf))
        {
        if (pOsIntf->_free != NULL)
            pOsIntf->_free(pDb);
        return null;
        }

    /* Create semaphore to protect this module */
    pDb->locker = pOsIntf->_create();
    if (pDb->locker == null)
        {
        pOsIntf->_free(pDb);
        return null;
        }

    /* Create engine's database*/
    if (!EngineDbCreate(pDb, numEngines))
        {
        pOsIntf->_delete(pDb->locker);
        pOsIntf->_free(pDb);
        return null;
        }

    pDb->interval = timer;
    /* Lookup table initiation, compliant to the rule of 99% ITU-T */
    for (idx = 0; idx < cAtBerMaxChnType; idx++)
        {
        for (idy = 0; idy < cAtBerMaxRates - 1; idy++)
            {
            if (timer == cAtBerTimer50ms)
                {
                pDb->table[idx][idy].setThres = set_threshold_table_50ms[idx][idy];
                pDb->table[idx][idy].clrThres = clear_threshold_table_50ms[idx][idy];
                }
            else if (timer == cAtBerTimer100ms)
                {
                pDb->table[idx][idy].setThres = set_threshold_table_100ms[idx][idy];
                pDb->table[idx][idy].clrThres = clear_threshold_table_100ms[idx][idy];
                }
            }
        }


    return pDb;
    }

/**
 * Delete BER module
 *
 * @param module BER module
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerModuleDelete(AtBerModule module)
    {
    tAtBerDb *pModule;

    /* Error on null module */
    if (module == null)
        return cAtBerNotCreated;

    /* Delete semaphore */
    pModule = module;
    pModule->osIntf._delete(pModule->locker);

    /* Free database */
    pModule->osIntf._free(pModule->active_engines);
    pModule->osIntf._free(pModule->engines);
    pModule->osIntf._free(module);

    return cAtBerOk;
    }

/**
 * Initialize BER module
 *
 * @param module BER module
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerModuleInit(AtBerModule module)
    {
    tAtBerDb *pModule;

    /* Error on null module */
    if (module == null)
        return cAtBerNotCreated;

    pModule = module;

    BerLock(module);
    pModule->osIntf._memset((void*)pModule->engines, 0, sizeof(tBerEngInfo) * pModule->num_engines);
    pModule->osIntf._memset((void*)pModule->active_engines, 0, sizeof(tBerEngInfo*) * pModule->num_engines);
    pModule->num_active_engines = 0;
    BerUnlock(module);

    return cAtBerOk;
    }

/**
 * Process BER module periodically
 *
 * @param module BER module
 *
 * @return Number of monitoring channels
 */
uint16 AtBerModulePeriodProcess(AtBerModule module)
    {
    tAtBerDb *pModule;
    tBerEngInfo *pEngInfo;
    uint16 idx;

    /* Ignore on null */
    if (module == null)
        return 0;

    /* Return if no request */
    pModule = (tAtBerDb*)module;
    if (pModule->num_active_engines == 0)
        return 0;

    BerLock(module);
    pModule->timeslot++;
    /* Serve all requesting lines */
    for (idx = 0; idx < pModule->num_active_engines; idx++)
        {
        pEngInfo = pModule->active_engines[idx];

        /* This engine is served all times or depending its assigned time-slot */
        if ((pEngInfo->timeslot == BER_TIMESLOT_NOID) ||
            ((pEngInfo->timeslot & BER_TIMESLOT_MASK) == (pModule->timeslot & BER_TIMESLOT_MASK)))
            {
            EngPeriodProcess(pEngInfo);
            }
        }
    BerUnlock(module);

    return pModule->num_active_engines;
    }

/**
 * Create BER engine channel
 *
 * @param module BER module
 * @param chn    User input channel id
 * @param pathType BER path type
 * @param dExcThres Threshold for dExc declaration
 * @param dDegThres Threshold for dDeg declaration
 * @param pUsrData User data. The user data is allocated and stored in database.
 *                 It may contain the real hardware information or other user
 *                 information.
 *
 * @return BER engine pointer.
 */
AtBerEngine AtBerEngineCreate(AtBerModule module,
                              uint16 chid,
                              eAtBerChnType chnType,
                              eAtUtilBerRate excThres,
                              eAtUtilBerRate degThres,
                              AtBerStatusChange staChangeFunc,
                              AtBerBitErrGet bitErrFunc,
                              void *pUsrData)
    {
    tAtBerDb *pModule;
    tBerEngInfo *pEngInfo;

    /* Cannot create engine in null module */
    if (module == null)
        return null;

    pModule = module;
    /* Cannot create engine */
    if ((chnType >= cAtBerMaxChnType) || (excThres >= cAtBerMaxRates) || (degThres >= cAtBerMaxRates))
        return null;

    /* Cannot create engine with null application interface */
    if ((bitErrFunc == null) || (pUsrData == null))
        return null;

    pEngInfo = EngFreeGet(module);
    if (pEngInfo == null)
        return null;

    BerLock(module);

    ((tAtBerDb*)module)->osIntf._memset((void*)pEngInfo, 0 , sizeof(tBerEngInfo));

    pEngInfo->enable = cAtTrue;
    pEngInfo->chn = chid;
    pEngInfo->type = chnType;
    pEngInfo->rate = cAtBer10e10;
    pEngInfo->excThres = excThres;
    pEngInfo->degThres = degThres;
    pEngInfo->interval = MultiUnits(pModule->interval, chnType);

    pEngInfo->module = module;
    pEngInfo->bitErrGet = bitErrFunc;
    pEngInfo->staChange = staChangeFunc;
    pEngInfo->pUsrData = pUsrData;
    BitErrorGet(pEngInfo);

    /* Assign channel's timeslot */
    if (chnType == cAtBerPathVc2 || chnType == cAtBerPathVc12 || chnType == cAtBerPathVc11)
        pEngInfo->timeslot = pModule->num_active_engines & BER_TIMESLOT_MASK;
    else
        pEngInfo->timeslot = BER_TIMESLOT_NOID;

    ItemAdd((void*)pModule->active_engines, &pModule->num_active_engines, (void*)pEngInfo);
    BerUnlock(module);

    return pEngInfo;
    }

/**
 * Delete BER engine channel
 *
 * @param engine BER Engine
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineDelete(AtBerEngine engine)
    {
    tBerEngInfo *pEngInfo;
    tAtBerDb *pModule;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;

    pEngInfo = engine;
    pModule = pEngInfo->module;
    BerLock(pEngInfo->module);
    if (pEngInfo->enable)
        ItemRemove((void*)pModule->active_engines, &pModule->num_active_engines, (void*)pEngInfo);

    pEngInfo->enable = cAtFalse;
    pEngInfo->module = null;
    BerUnlock((void*)pModule);

    return cAtBerOk;
    }

/**
 * Enable/disable BER engine for monitoring
 *
 * @param engine BER Engine
 * @param enable Enable/disable BER monitoring
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineEnable(AtBerEngine engine, atbool enable)
    {
    tBerEngInfo *pEngInfo;
    tAtBerDb *pModule;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;

    pEngInfo = engine;
    pModule = pEngInfo->module;
    if (pModule == null)
        return cAtBerNotCreated;

    if (pEngInfo->enable == enable)
        return cAtBerOk;

    BerLock(pEngInfo->module);
    if (pEngInfo->enable)
        ItemRemove((void*)pModule->active_engines, &pModule->num_active_engines, (void*)pEngInfo);
    else
        {
        ItemAdd((void*)pModule->active_engines, &pModule->num_active_engines, (void*)pEngInfo);
        pEngInfo->stampst = pModule->osIntf.gettime();
        }
    pEngInfo->enable = enable;
    BerUnlock(pEngInfo->module);

    return cAtBerOk;
    }

/**
 * To get the state of BER engine monitoring
 *
 * @param engine BER Engine
 * @param enable Enable/disable BER monitoring
 *
 * @return "eAtBerRet" BER returned codes.
 */
atbool AtBerEngineEnableGet(AtBerEngine engine)
    {
    tBerEngInfo *pEngInfo;

    /* If engine is null, return error */
    if (engine == null)
        return cAtFalse;

    pEngInfo = engine;
    if (pEngInfo->module == null)
        return cAtFalse;
    else
        return pEngInfo->enable;
    }

/**
 * To set BER thresholds manually
 *
 * @param engine BER Engine
 * @param dExcThres Threshold for dExc declaration
 * @param dDegThres Threshold for dDeg declaration
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineThresSet(AtBerEngine engine, eAtUtilBerRate *excThres, eAtUtilBerRate *degThres)
    {
    tBerEngInfo *pEngInfo;
    tAtBerDb *pModule;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;

    pEngInfo = engine;
    pModule = pEngInfo->module;
    if (pModule == null)
        return cAtBerNotCreated;

    if (((excThres != null) && (pEngInfo->excThres != *excThres)) ||
        ((degThres != null) && (pEngInfo->degThres != *degThres)))
        {
        BerLock(pEngInfo->module);
        if (excThres != null)
            pEngInfo->excThres = *excThres;
        if (degThres != null)
            pEngInfo->degThres = *degThres;
        DefectProc(pEngInfo, pEngInfo->rate);
        BerUnlock(pEngInfo->module);
        }

    return cAtBerOk;
    }

/**
 * To get BER thresholds info.
 *
 * @param engine BER Engine
 * @param dExcThres Returned threshold for dExc declaration
 * @param dDegThres Returned threshold for dDeg declaration
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineThresGet(AtBerEngine engine, eAtUtilBerRate *excThres, eAtUtilBerRate *degThres)
    {
    tBerEngInfo *pEngInfo;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;

    pEngInfo = engine;
    if (excThres != null)
        *excThres = pEngInfo->excThres;
    if (degThres != null)
        *degThres = pEngInfo->degThres;

    return cAtBerOk;
    }

/**
 * To set BER channel type.
 *
 * @param engine BER Engine
 * @param Channel type.
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineChnTypeSet(AtBerEngine engine, eAtBerChnType chnType)
    {
    tBerEngInfo *pEngInfo;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;
    if (chnType >= cAtBerMaxChnType)
        return cAtBerNotSupport;

    pEngInfo = engine;
    /* Channel type changed */
    if (chnType != pEngInfo->type)
        {
        BerLock(pEngInfo->module);
        pEngInfo->type = chnType;
        pEngInfo->rate = cAtBer10e10;
        pEngInfo->interval = MultiUnits(((tAtBerDb*)pEngInfo->module)->interval, chnType);
        BitErrorGet(pEngInfo);
        BerUnlock(pEngInfo->module);
        }

    return cAtBerOk;
    }

/**
 * To get BER channel type.
 *
 * @param engine BER Engine
 *
 * @return Channel type.
 */
eAtBerChnType AtBerEngineChnTypeGet(AtBerEngine engine)
    {
    /* If engine is null, return error */
    if (engine == null)
        return cAtBerUnknown;

    return ((tBerEngInfo*)engine)->type;
    }

/**
 * To get the status of BER engine
 *
 * @param engine BER Engine
 * @param dExc Returned EXC defect
 * @param dDeg Returned DEG defect
 * @param rate Returned current bit error rate
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineStatusGet(AtBerEngine engine, atbool *excDef, atbool *degDef, eAtUtilBerRate *rate)
    {
    tBerEngInfo *pEngInfo;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;

    pEngInfo = engine;
    if (excDef != null)
        *excDef = pEngInfo->excDef;
    if (degDef != null)
        *degDef = pEngInfo->degDef;
    if (rate != null)
        *rate = pEngInfo->rate;

    return cAtBerOk;
    }

/**
 * To get the history event of raised dEXC and/or raised dDEG of BER engine
 *
 * @param engine BER Engine
 * @param dExc Returned EXC defect
 * @param dDeg Returned DEG defect
 * @param clrHistory [input] Clear event or not.
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineEventGet(AtBerEngine engine, atbool *excDef, atbool *degDef, atbool clrHistory)
    {
    tBerEngInfo *pEngInfo;

    /* If engine is null, return error */
    if (engine == null)
        return cAtBerNotCreated;

    pEngInfo = engine;
    if (excDef != null)
        *excDef = pEngInfo->excEvent;
    if (degDef != null)
        *degDef = pEngInfo->degEvent;

    if (clrHistory)
        {
        BerLock(pEngInfo->module);
        if (excDef != null)
            pEngInfo->excEvent = cAtFalse;
        if (degDef != null)
            pEngInfo->degEvent = cAtFalse;
        BerUnlock(pEngInfo->module);
        }

    return cAtBerOk;
    }

/**
 * To get the source code revision
 *
 * @param module Module information
 *
 * @return Revision code in string.
 */
char * AtBerModuleVersion(AtBerModule module)
    {
    if (module != null)
        return ber_version;
    else
        return null;
    }

void AtBerEngineDebugEnable(AtBerEngine engine, eBool enable)
    {
    ((tBerEngInfo *)engine)->debugEnabled = enable;
    }

eBool AtBerEngineDebugIsEnabled(AtBerEngine engine)
    {
    tBerEngInfo *pEngInfo;
    tAtBerDb *pModule;

    if (engine == NULL)
        return cAtFalse;

    pEngInfo = engine;
    pModule  = pEngInfo->module;
    if (!pModule->debugEnabled)
        return cAtFalse;

    return ((tBerEngInfo *)engine)->debugEnabled;
    }

void AtBerModuleDebugEnable(AtBerModule module, eBool enable)
    {
    ((tAtBerDb *)module)->debugEnabled = enable;
    }

eBool AtBerModuleDebugIsEnabled(AtBerModule module)
    {
    return ((tAtBerDb *)module)->debugEnabled;
    }

#-----------------------------------------------------------------------------
#
# COPYRIGHT (C) 2012 Arrive Technologies Inc.
#
# The information contained herein is confidential property of Arrive Tecnologies. 
# The use, copying, transfer or disclosure of such information 
# is prohibited except by express written agreement with Arrive Technologies.
#
# File        : Rules.d
#
# Created Date: Feb-22-2013
#
# Description : Dependencies
# 
#----------------------------------------------------------------------------
# ==============================================================================
# Private
# ==============================================================================
.PHONY: all atcomponents cleanatcomponents

AT_COMPONENTS_SRC = $(shell find ${COMPONENTS_DIR} -iname "*.c")
AT_COMPONENTS_SPECIFIC_FLAGS = 
AT_COMPONENTS_FLAGS = -I${COMPONENTS_DIR}/include/atber -I${COMPONENTS_DIR}/include/aps -I${COMPONENTS_DIR}/include/fmpm ${AT_COMPONENTS_SPECIFIC_FLAGS}

AT_MIB_COMMON_FLAGS = -mno-branch-likely -fno-pic -mno-abicalls -msoft-float -pipe -fdollars-in-identifiers -fno-builtin -fno-common -I/opt/eldk-5.2.1/mips/sysroots/mips32-linux/usr/include
AT_MIPS32_FLAGS = ${AT_MIB_COMMON_FLAGS} -mips2
AT_MIPS64_FLAGS = -fsigned-char -Wvla -D_DEBUG_VERSION -DMODULE -mlong-calls -isystem -DCONFIG_DISTRIBUTED -D__KERNEL__ -DVMLINUX_LOAD_ADDRESS=0xffffffff80000000 -DDATAOFFSET=0 -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-builtin -fno-common -ffunction-sections -fdata-sections -falign-functions=16 -fno-tree-dominator-opts -fno-tree-switch-conversion -fno-jump-tables -fno-inline-small-functions -Wframe-larger-than=2048 -fno-stack-protector -mabi=32 -G 0 -mno-abicalls -fno-pic -pipe -msoft-float -ffreestanding -DCVMX_ENABLE_POW_CHECKS=0 -march=xlp -Wa,--trap -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign

# Open this line to have a MIPS lib
# AT_COMPONENTS_FLAGS += $(AT_MIPS32_FLAGS)
# AT_COMPONENTS_FLAGS += $(AT_MIPS64_FLAGS)

AT_CFLAGS += $(AT_COMPONENTS_FLAGS)

all: atcomponents

AT_COMPONENTS_SRC_OBJ = $(call GetObjectsFromSource, ${AT_COMPONENTS_SRC})
atcomponents: ${AT_COMPONENTS_LIB} ${AT_COMPONENTS_OBJ}
${AT_COMPONENTS_LIB}: $(AT_COMPONENTS_SRC_OBJ)
	$(AT_AR) cvr $@ $^
	$(AT_RANLIB) $@
	
${AT_COMPONENTS_OBJ}: $(AT_COMPONENTS_SRC_OBJ)
	$(AT_LD) $(LDFLAGS) $@ $^

clean: cleanatcomponents
cleanatcomponents: 
	rm -f $(AT_COMPONENTS_SRC_OBJ) $(AT_COMPONENTS_LIB) $(AT_COMPONENTS_OBJ)

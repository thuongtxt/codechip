/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : UPSR
 *
 * File        : upsr.h
 *
 * Created Date: Aug 20, 2010
 *
 * Author      : nnnam
 *
 * Description : UPSR APIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _UPSR_H_
#define _UPSR_H_

/*--------------------------- Includes ---------------------------------------*/

#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * Switching conditions
 */
/* Line defects */
#define cAtUpsrSwCondLos       cBit0  /**< LOS */
#define cAtUpsrSwCondLof       cBit1  /**< LOF */
#define cAtUpsrEngSwCondOof    cBit2  /**< OOF */
#define cAtUpsrEngSwCondMsAis  cBit3  /**< MS-AIS */

/* AU/HP defects */
#define cAtUpsrSwCondAuLop     cBit4  /**< AU-LOP */
#define cAtUpsrSwCondAuAis     cBit5  /**< AU-AIS */
#define cAtUpsrEngSwCondHpUneq cBit6  /**< HP-UNEQ */
#define cAtUpsrSwCondHpBerSf   cBit7  /**< Excessive HO BER */
#define cAtUpsrSwCondHpBerSd   cBit8  /**< HO Signal Degrade */

/* TU/LP defects */
#define cAtUpsrSwCondTuLop     cBit9  /**< TU-LOP */
#define cAtUpsrSwCondTuAis     cBit10 /**< TU-AIS */
#define cAtUpsrSwCondLpUneq    cBit11 /**< HP-UNEQ */
#define cAtUpsrSwCondLpBerSf   cBit12 /**< Excessive LO BER */
#define cAtUpsrSwCondLpBerSd   cBit13 /**< LO Signal Degrade */

#define cAtUpsrEngSwCondInvl   cBit31  /**< Invalid indicate of defect status. When
                                            this bit is set all of information in
                                            defect status will not be consider and
                                            the channel that has invalid alarm will
                                            be considered as a fail channel */

/* All masks */
#define cAtUpsrSwCondLine (cAtUpsrSwCondLos | cAtUpsrSwCondLof | cAtUpsrEngSwCondOof | cAtUpsrEngSwCondMsAis)
#define cAtUpsrSwCondHo   (cAtUpsrSwCondAuLop | cAtUpsrSwCondAuAis | cAtUpsrSwCondHpBerSf | cAtUpsrSwCondHpBerSd)
#define cAtUpsrSwCondLo   (cAtUpsrSwCondTuLop | cAtUpsrSwCondTuAis | cAtUpsrSwCondLpUneq | cAtUpsrSwCondLpBerSf | cAtUpsrSwCondLpBerSd)
#define cAtUpsrSwCondAll  (cAtUpsrSwCondLine | cAtUpsrSwCondHo | cAtUpsrSwCondLo)

/* Unknown (invalid) value */
#define cAtUpsrUnknown 0xFF

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief UPSR module return code
 */
typedef enum eAtUpsrRet
    {
    cAtUpsrRetOk,             /**< Success */
    cAtUpsrRetNotCreated,     /**< Not created */
    cAtUpsrRetNotSupport,     /**< Not support */
    cAtUpsrRetBusy,           /**< Engine is busy */
    cAtUpsrRetLowerPriority,  /**< Lower priority */
    cAtUpsrRetInvalid,        /**< Invalid */
    cAtUpsrRetOsRscAlloc,     /**< Error on allocating OS resources */
    cAtUpsrRetErr             /**< General error */
    }eAtUpsrRet;

/** @brief AUG-16 mapping types */
typedef enum eAtUpsrAug16MapType
    {
    cAtUpsrAug16MapTypeNone,     /**< No mapping */
    cAtUpsrAug16MapTypeVc4_16c,  /**< AUG-16 maps 1xVC4-16c */
    cAtUpsrAug16MapTypeAug4      /**< AUG-16 maps 4xAUG-4s */
    }eAtUpsrAug16MapType;

/** @brief AUG-4 mapping types */
typedef enum eAtUpsrAug4MapType
    {
    cAtUpsrAug4MapTypeNone,   /**< No mapping */
    cAtUpsrAug4MapTypeVc4_4c, /**< AUG-4 maps 1xVC4-4c */
    cAtUpsrAug4MapTypeAug1    /**< AUG-4 maps 4xAUG-1s */
    }eAtUpsrAug4MapType;

/** @brief AUG-1 mapping type */
typedef enum eAtUpsrAug1MapType
    {
    cAtUpsrAug1MapTypeNone, /**< No mapping */
    cAtUpsrAug1MapTypeVc4,  /**< AUG-1 maps 1xVC4 */
    cAtUpsrAug1MapTypeTug3, /**< AUG-1 maps 3xTUG-3s */
    cAtUpsrAug1MapTypeAu3   /**< AUG-1 maps 3xAU-3s */
    }eAtUpsrAug1MapType;

/** @brief AU-3 mapping types */
typedef enum eAtUpsrAu3Tug3MapType
    {
    cAtUpsrAu3Tug3MapTypeNone, /**< No mapping */
    cAtUpsrAu3Tug3MapTypeVc3,  /**< AU-3/TUG-3 maps 1xVC3 */
    cAtUpsrAu3Tug3MapTypeTug2  /**< AU-3/TUG-3 maps 7xTUG-2s */
    }eAtUpsrAu3Tug3MapType;

/** @brief TUG-2 mapping types */
typedef enum eAtUpsrTug2MapType
    {
    cAtUpsrTug2MapTypeNone, /**< No mapping */
    cAtUpsrTug2MapTypeTu12, /**< TUG-2 maps 3xTU-12 */
    cAtUpsrTug2MapTypeTu11  /**< TUG-2 maps 4xTU-11 */
    }eAtUpsrTug2MapType;

/**
 * @brief Engine type
 */
typedef enum eAtUpsrEngType
    {
    cAtUpsrEngTypeAu4_16c, /**< AU-4-16c */
    cAtUpsrEngTypeAu4_4c,  /**< AU-4-4c */
    cAtUpsrEngTypeAu4,     /**< AU-4 */
    cAtUpsrEngTypeAu3,     /**< AU-3 */
    cAtUpsrEngTypeTu3,     /**< TU-3 */
    cAtUpsrEngTypeTu12,    /**< TU-12 */
    cAtUpsrEngTypeTu11     /**< TU-11 */
    }eAtUpsrEngType;

/**
 * @brief UPSR engine states
 */
typedef enum eAtUpsrEngState
    {
    cAtUpsrEngStateInActive, /**< In active state */
    cAtUpsrEngStateIdle,     /**< IDLE state. In this state, traffic is selected from working channel */
    cAtUpsrEngStateSw,       /**< Switch state. In this state, traffic is selected from protection channel */
    cAtUpsrEngStateFs2Wrk,   /**< Force switch (to working) state */
    cAtUpsrEngStateFs2Prt,   /**< Force switch (to protection) state */
    cAtUpsrEngStateMs2Wrk,   /**< Manual switch (to working) state */
    cAtUpsrEngStateMs2Prt,   /**< Manual switch (to protection) state */
    cAtUpsrEngStateLp,       /**< Lock out of Protection state */
    cAtUpsrEngStateWtr,      /**< Wait-To-Restore state */
    cAtUpsrEngStateFailWrk,  /**< Fail on switching and traffic is on working channel */
    cAtUpsrEngStateFailPrt   /**< Fail on switching and traffic is on protection channel */
    }eAtUpsrEngState;

/**
 * @brief Switching reasons
 */
typedef enum eAtUpsrSwReason
    {
    cAtUpsrSwReasonNone,   /**< None, engine has not done any switching action */
    cAtUpsrSwReasonAuto,   /**< Switch due to auto switching criteria */
    cAtUpsrSwReasonExtCmd  /**< Switch due to external command */
    }eAtUpsrSwReason;

/**
 * @brief UPSR external commands
 */
typedef enum eAtUpsrExtlCmd
    {
    cAtUpsrExtlCmdClear  = cAtUpsrEngStateIdle,   /**< Clear */
    cAtUpsrExtlCmdFs2Wrk = cAtUpsrEngStateFs2Wrk, /**< Force switch to working */
    cAtUpsrExtlCmdFs2Prt = cAtUpsrEngStateFs2Prt, /**< Force switch to protection */
    cAtUpsrExtlCmdMs2Wrk = cAtUpsrEngStateMs2Wrk, /**< Manual switch to working */
    cAtUpsrExtlCmdMs2Prt = cAtUpsrEngStateMs2Prt, /**< Manual switch to protection */
    cAtUpsrExtlCmdLp     = cAtUpsrEngStateLp      /**< Lock out of Protection */
    }eAtUpsrExtlCmd;

/**
 * @brief UPSR switching types
 */
typedef enum eAtUpsrSwType
    {
    cAtUpsrSwTypeNonRev = 1, /**< Non-revertive switching type */
    cAtUpsrSwTypeRev    = 2  /**< Revertive switching type */
    }eAtUpsrSwType;

/**
 * @brief Channel type
 */
typedef enum eAtUpsrChnType
    {
    cAtUpsrChnTypeProtection, /**< Protection channel */
    cAtUpsrChnTypeWorking     /**< Working channel */
    }eAtUpsrChnType;

/**
 * @brief Abstract UPSR module
 */
typedef void* AtUpsrModule;

/**
 * @brief Abstract UPSR engine
 */
typedef void* AtUpsrEngine;

/**
 * @brief Linear APS OS interface
 */
typedef struct tUpsrOsIntf
    {
    void* (*semCreate)(void);     /**< Function to create semaphore */
    int (*semDelete)(void *pSem); /**< Function to delete semaphore */
    int (*semTake)(void *pSem);   /**< Function to take semaphore */
    int (*semGive)(void *pSem);   /**< Function to give semaphore */
    }tUpsrOsIntf;

/**
 * Switching interface. This function is called when there is any switching
 * action from source channel to destination channel
 *
 * @param [in] engine Engine
 * @param [in] s_port   [0..N] Source port
 * @param [in] s_augno  [0..15] Source AUG-1
 * @param [in] s_auno   [0..2]  Source AU-3/TUG-3
 * @param [in] s_tug2no [0..6]  Source
 * @param [in] s_tuno   [0..3]  Source TU-1x
 * @param [in] d_port   [0..N]  Destination port
 * @param [in] d_augno  [0..15] Destination AUG-1
 * @param [in] d_auno   [0..2]  Destination AU-3/TUG-3
 * @param [in] d_tug2no [0..6]  Destination
 * @param [in] d_tuno   [0..3]  Destination TU-1x
 * @param [in] pUsrData User's data that is input by UpsrHoEngCreate() or UpsrLoEngCreate()
 * @param [in] usrDataLen Size of user's data
 *
 * @retval cAtUpsrRetOk on success or other return code to let this module know
 *         the application cannot perform its switching function
 */
typedef eAtUpsrRet (*AtUpsrEngSwFunc)(AtUpsrEngine engine,
                                      uint8 s_port, uint8 s_augno, uint8 s_auno, uint8 s_tug2no, uint8 s_tuno,
                                      uint8 d_port, uint8 d_augno, uint8 d_auno, uint8 d_tug2no, uint8 d_tuno,
                                      void *pUsrData, uint32 usrDataLen);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Module control APIs */
AtUpsrModule AtUpsrModuleCreate(uint8 maxNumLine, const tUpsrOsIntf *pOsIntf);
eAtUpsrRet AtUpsrModuleDelete(AtUpsrModule module);
eAtUpsrRet AtUpsrModuleTraverse(AtUpsrModule module, eAtUpsrRet (*traverseFunc)(AtUpsrEngine engine));
uint32 AtUpsrModulePeriodProcess(AtUpsrModule module);

/* Mapping APIs */
eAtUpsrRet UpsrAug16MapSet(AtUpsrModule module, uint8 port, uint8 augno, eAtUpsrAug16MapType mapType);
eAtUpsrAug16MapType UpsrAug16MapGet(AtUpsrModule module, uint8 port, uint8 augno);
eAtUpsrRet UpsrAug4MapSet (AtUpsrModule module, uint8 port, uint8 augno, eAtUpsrAug4MapType mapType);
eAtUpsrAug4MapType UpsrAug4MapGet (AtUpsrModule module, uint8 port, uint8 augno);
eAtUpsrRet UpsrAug1MapSet (AtUpsrModule module, uint8 port, uint8 augno, eAtUpsrAug1MapType mapType);
eAtUpsrAug1MapType UpsrAug1MapGet (AtUpsrModule module, uint8 port, uint8 augno);
eAtUpsrRet UpsrAu3MapSet  (AtUpsrModule module, uint8 port, uint8 augno, uint8 au3no, eAtUpsrAu3Tug3MapType mapType);
eAtUpsrAu3Tug3MapType UpsrAu3MapGet  (AtUpsrModule module, uint8 port, uint8 augno, uint8 au3no);
eAtUpsrRet UpsrTug3MapSet (AtUpsrModule module, uint8 port, uint8 augno, uint8 tug3no, eAtUpsrAu3Tug3MapType mapType);
eAtUpsrAu3Tug3MapType UpsrTug3MapGet (AtUpsrModule module, uint8 port, uint8 augno, uint8 tug3no);
eAtUpsrRet UpsrTug2MapSet (AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, eAtUpsrTug2MapType mapType);
eAtUpsrTug2MapType UpsrTug2MapGet (AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no);

/* UPSR engine creating */
AtUpsrEngine AtUpsrHoEngCreate(AtUpsrModule module,
                               uint8 w_port, uint8 w_augno, uint8 w_auno,
                               uint8 p_port, uint8 p_augno, uint8 p_auno,
                               eAtUpsrSwType swType,
                               uint32 wtrTime,
                               AtUpsrEngSwFunc swFunc,
                               void *pUsrData, uint32 usrDataSize);
AtUpsrEngine AtUpsrLoEngCreate(AtUpsrModule module,
                               uint8 w_port, uint8 w_augno, uint8 w_au3Tug3no, uint8 w_tug2no, uint8 w_tuno,
                               uint8 p_port, uint8 p_augno, uint8 p_au3Tug3no, uint8 p_tug2no, uint8 p_tuno,
                               eAtUpsrSwType swType,
                               uint32 wtrTime,
                               AtUpsrEngSwFunc swFunc,
                               void *pUsrData, uint32 usrDataSize);
eAtUpsrRet AtUpsrEngDelete(AtUpsrEngine engine);

/* Engine APIs */
eAtUpsrRet AtUpsrEngStart(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngStop(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngStateSet(AtUpsrEngine engine, eAtUpsrEngState state);
eAtUpsrEngType AtUpsrEngTypeGet(AtUpsrEngine engine);
eAtUpsrEngState AtUpsrEngStateGet(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngWtrSet(AtUpsrEngine engine, uint32 wtr);
uint32 AtUpsrEngWtrGet(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngSwTypeSet(AtUpsrEngine engine, eAtUpsrSwType swType);
eAtUpsrSwType AtUpsrEngSwTypeGet(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngExtlCmdSet(AtUpsrEngine engine, eAtUpsrExtlCmd extlCmd);
eAtUpsrExtlCmd AtUpsrEngExtlCmdGet(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngUsrDataSet(AtUpsrEngine engine, void *pUsrData, uint32 usrDataSize);
void *AtUpsrEngUsrDataGet(AtUpsrEngine engine, uint32 *pUsrDataSize);
AtUpsrEngSwFunc AtUpsrEngSwFuncGet(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngChnGet(AtUpsrEngine engine,
                           uint8 *w_port, uint8 *w_augno, uint8 *w_au3Tug3no, uint8 *w_tug2no, uint8 *w_tuno,
                           uint8 *p_port, uint8 *p_augno, uint8 *p_au3Tug3no, uint8 *p_tug2no, uint8 *p_tuno);
eAtUpsrChnType AtUpsrEngActivePathGet(AtUpsrEngine engine);
eAtUpsrSwReason AtUpsrEngSwReasonGet(AtUpsrEngine engine);
eAtUpsrRet AtUpsrEngSwCondSet(AtUpsrEngine engine, uint32 swCond);
uint32 AtUpsrEngSwCondGet(AtUpsrEngine engine);
uint32 AtUpsrEngWrkDefGet(AtUpsrEngine engine);
uint32 AtUpsrEngPrtDefGet(AtUpsrEngine engine);

/* HO channel APIs */
eAtUpsrChnType AtUpsrHoChnGetPairChn(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno, uint8 *pairPort, uint8 *pairAugno, uint8 *pairAuno);
uint32 AtUpsrHoChnDefStatusGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno);
AtUpsrEngine AtUpsrHoEngGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno);

/* LO channel APIs */
eAtUpsrChnType AtUpsrLoChnGetPairChn(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno,
                                     uint8 *pairPort, uint8 *pairAugno, uint8 *pairAu3Tug3no, uint8 *pPairTug2no, uint8 *pPairTuno);
uint32 AtUpsrLoChnDefStatusGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno);
AtUpsrEngine AtUpsrLoEngGet(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno);

/* Defect notifications */
eAtUpsrRet AtUpsrLineDefNotify(AtUpsrModule module, uint8 port, uint32 defMsk, uint32 defStat);
eAtUpsrRet AtUpsrHoChnDefNotify(AtUpsrModule module, uint8 port, uint8 augno, uint8 auno, uint32 defMsk, uint32 defStat);
eAtUpsrRet AtUpsrLoChnDefNotify(AtUpsrModule module, uint8 port, uint8 augno, uint8 au3Tug3no, uint8 tug2no, uint8 tuno, uint32 defMsk, uint32 defStat);

#endif /* _UPSR_H_ */





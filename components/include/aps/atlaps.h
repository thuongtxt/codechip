/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Linear APS
 * 
 * File        : laps.h
 * 
 * Created Date: Sep 4, 2010
 *
 * Author      : nnnam
 * 
 * Description : Software Linear APS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _LAPS_H_
#define _LAPS_H_

/*--------------------------- Includes ---------------------------------------*/
/*#include "atapstype.h"*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
/* Unknown information */
#define cAtLApsUnknown 0xFF

/* Other codes in K2[6:8]: RDI-L, AIS-L */
#define cAtLApsRdilCode 0x6
#define cAtLApsAislCode 0x7

/* Switching conditions */
#define cAtLApsSwCondLos   cBit0  /**< LOS */
#define cAtLApsSwCondLof   cBit1  /**< LOF */
#define cAtLApsSwCondOof   cBit2  /**< OOF */
#define cAtLApsSwCondTims  cBit3  /**< TIM-S */
#define cAtLApsSwCondAisl  cBit4  /**< AIS-L */
#define cAtLApsSwCondBerSf cBit5  /**< BER-SF */
#define cAtLApsSwCondBerSd cBit6  /**< BER-SD */
#define cAtLApsSwCondAll   (cAtLApsSwCondLos | cAtLApsSwCondLof | cAtLApsSwCondOof | cAtLApsSwCondTims | cAtLApsSwCondAisl | cAtLApsSwCondBerSf | cAtLApsSwCondBerSd)

/*--------------------------- Macros -----------------------------------------*/
/* Macro to access K-bytes fields in case of Linear APS */
#define mK1ReqGet(k1_)         (((k1_) & 0xF0) >> 4)
#define mK1ReqSet(k1_, req_)    (k1_) = (uint8)((((k1_) & (uint8)(~0xF0)) | ((req_) << 4)))
#define mK1ReqChnGet(k1_)      (uint8)((k1_) & 0xF)
#define mK1ReqChnSet(k1_, chn_) (k1_) = (uint8)(((k1_) & (uint8)(~0xF)) | (chn_))
#define mK2ReqChnGet(k2_)      (uint8)(((k2_) & 0xF0) >> 4)
#define mK2ReqChnSet(k2_, chn_) (k2_) = (uint8)(((k2_) & (uint8)(~0xF0)) | ((chn_) << 4))
#define mK2ArchGet(k2_)        (uint8)(((k2_) & 0x8) >> 3)
#define mK2ArchSet(k2_, arch_)  (k2_) = (uint8)(((k2_) & (uint8)(~0x8)) | ((arch_) << 3))
#define mK2OpMdGet(k2_)        (uint8)((k2_) & 0x7)
#define mK2OpMdSet(k2_, opMod_) (k2_) = (uint8)(((k2_) & (uint8)(~0x7)) | (opMod_))

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief Linear APS module return code
 */
typedef enum eAtLApsRet
    {
    cAtLApsOk,                  /**< Success return code */
    cAtLApsErrUnknownId,        /**< Unknown ID */
    cAtLApsErrBusy,             /**< Busy */
    cAtLApsErrNotProv,          /**< Not provision */
    cAtLApsErrNotSupport,       /**< Not support */
    cAtLApsErrUnknownExtlCmd,   /**< Unknown external command */
    cAtLApsErrLowPrio,          /**< Lower priority */
    cAtLApsErrOsRscAlloc,       /**< Error on allocating OS resources */
    cAtLApsErr                  /**< General return code */
    }eAtLApsRet;

/**
 * @brief Linear APS Architectures
 */
typedef enum eAtLApsArch
    {
    cAtLApsArch11, /**< 1+1 architecture */
    cAtLApsArch1n  /**< 1:n architecture */
    }eAtLApsArch;

/**
 * @brief Linear APS Operation modes
 */
typedef enum eAtLApsOpMd
    {
    cAtLApsOpMdUni = 4, /**< Uni-direction operation */
    cAtLApsOpMdBid = 5  /**< Bid-direction operation */
    }eAtLApsOpMd;

/**
 * @brief Linear APS Switching types
 */
typedef enum eAtLApsSwType
    {
    cAtLApsSwTypeNonRev = 1, /**< Non-revertive switching */
    cAtLApsSwTypeRev    = 2  /**< Revertive switching */
    }eAtLApsSwType;

/**
 * @brief APS requests
 */
typedef enum eAtLApsReq
    {
    cAtLApsReqNR   = 0x0, /**< No Request */
    cAtLApsReqDNR  = 0x1, /**< Do Not Revert (only 1+1 LTE provisioned for non-revertive
                               switching transmits Do Not Revert) */
    cAtLApsReqRR   = 0x2, /**< Reverse Request (applies only to bidirectional systems) */
    cAtLApsReqEXER = 0x4, /**< Exercise */
    cAtLApsReqWTR  = 0x6, /**< Wait-to-Restore (1+1 LTE provisioned for
                               non-revertive switching does not transmit
                               Wait-to-Restore */
    cAtLApsReqMS   = 0x8, /**< Manual Switch */
    cAtLApsReqSDL  = 0xA, /**< SD-Low Priority */
    cAtLApsReqSDH  = 0xB, /**< SD-High Priority */
    cAtLApsReqSFL  = 0xC, /**< SF-Low Priority */
    cAtLApsReqSFH  = 0xD, /**< SF-High Priority */
    cAtLApsReqFS   = 0xE, /**< Forced Switch */
    cAtLApsReqLP   = 0xF  /**< Lockout of Protection */
    }eAtLApsReq;

/**
 * @brief Engine troubles
 */
typedef enum eAtLApsEngTrbl
    {
    cAtLApsEngTrblNone = 0x0, /**< No trouble */
    cAtLApsEngTrblPSB  = 0x1, /**< Protection Switching Byte trouble */
    cAtLApsEngTrblCM   = 0x2, /**< Channel Mismatch trouble */
    cAtLApsEngTrblMM   = 0x4, /**< Mode Mismatch trouble */
    cAtLApsEngTrblFEPL = 0x8  /**< Far-End Protection Line trouble */
    }eAtLApsEngTrbl;

/**
 * @brief External commands
 */
typedef enum eAtLApsExtlCmd
    {
    cAtLApsExtlCmdClear = 0,            /**< Clear */
    cAtLApsExtlCmdExer  = cAtLApsReqEXER, /**< Exercise */
    cAtLApsExtlCmdMs    = cAtLApsReqMS,   /**< Manual switch */
    cAtLApsExtlCmdFs    = cAtLApsReqFS,   /**< Force switch */
    cAtLApsExtlCmdLp    = cAtLApsReqLP    /**< Lock out of Protection */
    }eAtLApsExtlCmd;

/**
 * @brief Engine states
 */
typedef enum eAtLApsEngState
    {
    cAtLApsEngStateStop, /**< Engine stopped */
    cAtLApsEngStateStart /**< Engine started */
    }eAtLApsEngState;

/**
 * @brief Priority value
 */
typedef enum eAtLApsPriority
    {
    cAtLApsPriorityLow, /**< Low priority */
    cAtLApsPriorityHigh /**< High priority */
    }eAtLApsPriority;

/* Handles */
typedef void * AtLApsModule;
typedef void * AtLApsEngine;

/**
 * Bridging/Switching interfaces to bridge/switch traffic from request working
 * line to protection line
 * @param engine Engine handle
 * @param w_line Working line
 * @param p_line Protection line
 * @param release Release the bridge/switch
 * @param pUsrData User data
 * @param usrDataLen Length of user data
 */
typedef eAtLApsRet (*AtLApsEngBrSwFunc)(AtLApsEngine engine,
                                        uint8 w_line,
                                        uint8 p_line,
                                        atbool release,
                                        void *pUsrData,
                                        uint32 usrDataLen);

/**
 * Interface to send K-bytes
 * @param engine Engine handle
 * @param line Line on that the K-byte will be sent
 * @param k1 K1 byte
 * @param k2 K2 byte
 * @param pUsrData User data
 * @param usrDataLen Length of user data
 */
typedef eAtLApsRet (*AtLApsEngKByteSendFunc)(AtLApsEngine engine,
                                             uint8 line,
                                             uint8 k1,
                                             uint8 k2,
                                             void *pUsrData,
                                             uint32 usrDataLen);

/**
 * @brief Linear APS OS interface
 */
typedef struct tAtLApsOsIntf
    {
    void* (*semCreate)(void);         /**< Function to create semaphore */
    int (*semDelete)(void *pSem); /**< Function to delete semaphore */
    int (*semTake)(void *pSem);   /**< Function to take semaphore */
    int (*semGive)(void *pSem);   /**< Function to give semaphore */
    }tAtLApsOsIntf;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Module APIs */
AtLApsModule AtLApsCreate(uint16 maxNumLines, tAtLApsOsIntf *pOsIntf);
void AtLApsDelete(AtLApsModule module);
uint8 AtLApsPeriodicProcess(AtLApsModule module);
uint16 AtLApsMaxNumLinesGet(AtLApsModule module);

/* Engine APIs */
AtLApsEngine AtLApsEngCreate(AtLApsModule module,
                             uint8 lines[],
                             uint8 numLines,
                             eAtLApsArch arch,
                             eAtLApsOpMd opMod,
                             eAtLApsSwType swType,
                             uint32 wtrTime,

                             /* Application interface */
                             AtLApsEngBrSwFunc swFunc,
                             AtLApsEngBrSwFunc brFunc,
                             AtLApsEngKByteSendFunc kByteSendFunc,

                             /* User data */
                             void *pUsrData,
                             uint32 usrDataSize);
eAtLApsRet AtLApsEngDelete(AtLApsEngine engine);

eAtLApsRet AtLApsEngAppIntfSet(AtLApsEngine engine, AtLApsEngBrSwFunc swFunc, AtLApsEngBrSwFunc brFunc, AtLApsEngKByteSendFunc kByteSendFunc);

eAtLApsRet AtLApsEngStart(AtLApsEngine engine);
eAtLApsRet AtLApsEngStartWithCurState(AtLApsEngine engine);
eAtLApsRet AtLApsEngStop(AtLApsEngine engine);
eAtLApsEngState AtLApsEngState(AtLApsEngine engine);
eAtLApsRet AtLApsEngStateCopy(AtLApsEngine src, AtLApsEngine des);

eAtLApsRet AtLApsEngSwTypeSet(AtLApsEngine engine, eAtLApsSwType swType);
eAtLApsSwType AtLApsEngSwTypeGet(AtLApsEngine engine);

eAtLApsRet AtLApsEngOpMdSet(AtLApsEngine engine, eAtLApsOpMd opMd);
eAtLApsOpMd AtLApsEngOpMdGet(AtLApsEngine engine);

eAtLApsRet AtLApsEngArchSet(AtLApsEngine engine, eAtLApsArch arch);
eAtLApsArch AtLApsEngArchGet(AtLApsEngine engine);

eAtLApsRet AtLApsEngExtlCmdSet(AtLApsEngine engine, eAtLApsExtlCmd extlCmd, uint8 line, uint8 chn);
eAtLApsExtlCmd AtLApsEngExtlCmdGet(AtLApsEngine engine, uint8 *pAffLine, uint8 *pAffChn);

uint8 AtLApsEngRxK1Get(AtLApsEngine engine);
uint8 AtLApsEngRxK2Get(AtLApsEngine engine);
uint8 AtLApsEngTxK1Get(AtLApsEngine engine);
eAtLApsRet AtLApsEngTxK1Set(AtLApsEngine engine, uint8 k1);
uint8 AtLApsEngTxK2Get(AtLApsEngine engine);
eAtLApsRet AtLApsEngTxK2Set(AtLApsEngine engine, uint8 k2);

uint32 AtLApsEngWtrGet(AtLApsEngine engine);
eAtLApsRet AtLApsEngWtrSet(AtLApsEngine engine, uint32 wtr);

uint8 AtLApsEngSwChnGet(AtLApsEngine engine);
uint8 AtLApsEngBrChnGet(AtLApsEngine engine);
eAtLApsRet AtLApsEngCurHigReq(AtLApsEngine engine, eAtLApsReq *pReq, uint8 *pReqChn, atbool *pIsLocal);
eAtLApsRet AtLApsEngUsrDataSet(AtLApsEngine engine, void *pUsrData, uint32 usrSize);
void *AtLApsEngUsrDataGet(AtLApsEngine engine, uint32 *pUsrSize);
eAtLApsEngTrbl AtLApsEngTrblGet(AtLApsEngine engine);

/* Engine's Lines and channels management */
eAtLApsRet AtLApsChnPrioSet(AtLApsEngine engine, uint8 chnId, eAtLApsPriority priority);
eAtLApsPriority AtLApsChnPrioGet(AtLApsEngine engine, uint8 chnId);
uint32 AtLApsChnDefStat(AtLApsEngine engine, uint8 chn);
uint8 AtLApsLineFromChnGet(AtLApsEngine engine, uint8 chnId);
eAtLApsRet AtAtLApsEngLineAdd(AtLApsEngine engine, uint8 line, uint8 chnId);
eAtLApsRet AtLApsEngLineDelete(AtLApsEngine engine, uint8 line, uint8 chnId);
uint8 AtLApsEngNumLineGet(AtLApsEngine engine);

/* Line APIs */
eAtLApsRet AtLApsLineEnable(AtLApsModule module, uint8 line);
eAtLApsRet AtLApsLineDisable(AtLApsModule module, uint8 line);
atbool AtLApsLineIsEnable(AtLApsModule module, uint8 line);
AtLApsEngine AtLApsLineEngGet(AtLApsModule module, uint8 lineId);
uint8 AtLApsLineChnGet(AtLApsModule module, uint8 lineId);
eAtLApsRet AtLApsLineDefNotify(AtLApsModule module, uint8 line, uint32 defMsk, uint32 defStat);
eAtLApsRet AtLApsLineKByteChange(AtLApsModule module, uint8 line, uint8 k1, uint8 k2);
uint32 AtLApsLineDefStat(AtLApsModule module, uint8 line);
uint16 AtLApsLineSwCnt(AtLApsEngine engine, uint8 line);

/* Notify */
eAtLApsRet AtLApsEngKByteChange(AtLApsEngine engine, uint8 k1, uint8 k2);
eAtLApsRet AtLApsChnDefNotify(AtLApsEngine engine, uint8 chnId, uint32 defMsk, uint32 defStat);

/* For debugging purpose */
const char *LApsReqStr(uint8 reqVal);
const char *LApsArchStr(uint8 arch);
const char *LApsOpMdStr(uint8 opMd);
const char *LApsSwTypeStr(uint8 swType);
const char *LApsExtCmdStr(uint8 extCmd);
const char *LApsK1ToStr(uint8 k1);
const char *LApsK2ToStr(uint8 k2);
void LApsEngShow(AtLApsModule, uint8 line);

#endif /* _LAPS_H_ */


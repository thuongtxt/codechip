/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : BER calculator
 *
 * File        : atber.h
 *
 * Created Date: Aug 25, 2012
 *
 * Author      : ttchi
 *
 * Description : This file contains source code of internal functions and APIs
 *               of BER module.
 *
 * Notes       : Version 2.0
 *----------------------------------------------------------------------------*/

#ifndef ATBER_H_
#define ATBER_H_

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtBerTimer50ms       50
#define cAtBerTimer100ms      100

#define cAtBerUnknown  0xFF

/*--------------------------- Typedefs ---------------------------------------*/
typedef void * AtBerModule;
typedef void * AtBerEngine;

/**< @brief Returned code lists */
typedef enum eAtBerRet
    {
    cAtBerOk,          /* No error returned */
    cAtBerInvalid,     /* Invalid input parameter */
    cAtBerNotCreated,  /* The BER-object is not created */
    cAtBerNotSupport   /* This mode doesn't support */
    }eAtBerRet;

/**< @brief BER levels */
typedef enum eAtUtilBerRate
    {
    cAtBer10e3          = 0,    /* BER level 10E-3 */
    cAtBer10e4          = 1,    /* BER level 10E-4 */
    cAtBer10e5          = 2,    /* BER level 10E-5 */
    cAtBer10e6          = 3,    /* BER level 10E-6 */
    cAtBer10e7          = 4,    /* BER level 10E-7 */
    cAtBer10e8          = 5,    /* BER level 10E-8 */
    cAtBer10e9          = 6,    /* BER level 10E-9 */
    cAtBer10e10         = 7,    /* BER level 10E-10 */
    cAtBerMaxRates      = 8     /* Number of BER rates */
    }eAtUtilBerRate;

/**< @brief BER channel types */
typedef enum eAtBerChnType
    {
    cAtBerPathVc416c    = 0,    /* SONET/SDH VC-4-16c path */
    cAtBerPathVc44c     = 1,    /* SONET/SDH VC-4-4c path */
    cAtBerPathVc4       = 2,    /* SONET/SDH VC-4 path */
    cAtBerPathVc3       = 3,    /* SONET/SDH VC-3 path */
    cAtBerPathVc2       = 4,    /* SONET/SDH VC-2 path */
    cAtBerPathVc12      = 5,    /* SONET/SDH VC-12 path */
    cAtBerPathVc11      = 6,    /* SONET/SDH VC-11 path */

    cAtBerLineStm16     = 7,    /* SONET/SDH OC-48 Line/STM-16 MS */
    cAtBerLineStm4      = 8,    /* SONET/SDH OC-12 Line/STM-4 MS */
    cAtBerLineStm1      = 9,    /* SONET/SDH OC-3 Line/STM-1 MS */
    cAtBerLineStm0      = 10,   /* SONET/SDH EC-1 Line/STM-0 MS */

    cAtBerLineStm16RS   = 11,    /* SONET/SDH OC-48 Section/STM-16 RS */
    cAtBerLineStm4RS    = 12,    /* SONET/SDH OC-12 Section/STM-4 RS */
    cAtBerLineStm1RS    = 13,    /* SONET/SDH OC-3 Section/STM-1 RS */
    cAtBerLineStm0RS    = 14,    /* SONET/SDH EC-1 Section/STM-0 RS */

    cAtBerMaxChnType    = 15    /* Number of channel types */
    }eAtBerChnType;

/**
 * @brief BER OS interface
 */
typedef struct tAtBerOsIntf
    {
    /* Semaphore */
    void* (*_create)(void);     /**< Function to create semaphore */
    int (*_delete)(void *locker); /**< Function to delete semaphore */
    int (*lock)(void *locker);   /**< Function to lock semaphore */
    int (*unlock)(void *locker);   /**< Function to unlock semaphore */

    /* System time */
    uint32 (*gettime)(void); /**< Function to get system time (ms unit) */

    /* Memory */
    void *(*_malloc)(uint32 msize); /**< Function to allocate memory */
    void *(*_memset)(void *_mem, int val, uint32 msize); /**< Function to set value into memory */
    void *(*_memcpy)(void *_mdest, const void *_msrc, uint32 msize); /**< Function to copy value between two memories */
    void (*_free)(void *_mem); /**< Function to free memory */

    }tAtBerOsIntf;

/**
 * Report BER status change
 *
 * @param engine BER engine
 * @param chn    User channel id
 * @param excDef BER excessive status change
 * @param degDef BER degrade status change
 * @param pUsrData User data. The user data is allocated and stored in database.
 *                 It may contain the real hardware information or other user
 *                 information.
 *
 * @return BER engine pointer.
 */
typedef eAtBerRet (*AtBerStatusChange)(AtBerEngine engine,
                                       uint16 chid,
                                       atbool excessive,
                                       atbool degrade,
                                       void *pUsrData);

/**
 * Get Bit error count periodically for BER calculation
 *
 * @param engine BER engine
 * @param chn    User channel id
 * @param bitErrCount Bit Error counter
 * @param pUsrData User data. The user data is allocated and stored in database.
 *                 It may contain the real hardware information or other user
 *                 information.
 *
 * @return BER engine pointer.
 */
typedef eAtBerRet (*AtBerBitErrGet)(AtBerEngine engine,
                                    uint16 chid,
                                    uint32 *bitErrCount,
                                    void *pUsrData);

/*--------------------------- Entries ----------------------------------------*/

/**
 * Create BER module
 *
 * @param numEngines Number of BER engines.
 * @param timer      Timer for BIP/Error bits polling.
 *
 * @return BER module pointer.
 */
AtBerModule AtBerModuleCreate(uint16 numEngines, uint32 timer, const tAtBerOsIntf *pOsIntf);

/**
 * Delete BER module
 *
 * @param module BER module
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerModuleDelete(AtBerModule module);

/**
 * Initialize BER module
 *
 * @param module BER module
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerModuleInit(AtBerModule module);

/**
 * Process BER module periodically
 *
 * @param module BER module
 *
 * @return Number of monitoring channels
 */
uint16 AtBerModulePeriodProcess(AtBerModule module);

/**
 * Create BER engine channel
 *
 * @param module BER module
 * @param chn    User input channel id
 * @param pathType BER path type
 * @param dExcThres Threshold for dExc declaration
 * @param dDegThres Threshold for dDeg declaration
 * @param pUsrData User data. The user data is allocated and stored in database.
 *                 It may contain the real hardware information or other user
 *                 information.
 *
 * @return BER engine pointer.
 */
AtBerEngine AtBerEngineCreate(AtBerModule module,
                              uint16 chid,
                              eAtBerChnType chnType,
                              eAtUtilBerRate excThres,
                              eAtUtilBerRate degThres,
                              AtBerStatusChange staChangeFunc,
                              AtBerBitErrGet bitErrFunc,
                              void *pUsrData);

/**
 * Delete BER engine channel
 *
 * @param engine BER Engine
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineDelete(AtBerEngine engine);

/**
 * Enable/disable BER engine for monitoring
 *
 * @param engine BER Engine
 * @param enable Enable/disable BER monitoring
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineEnable(AtBerEngine engine, atbool enable);

/**
 * To get the state of BER engine monitoring
 *
 * @param engine BER Engine
 * @param enable Enable/disable BER monitoring
 *
 * @return "eAtBerRet" BER returned codes.
 */
atbool AtBerEngineEnableGet(AtBerEngine engine);

/**
 * To set BER thresholds manually
 *
 * @param engine BER Engine
 * @param excThres Threshold for dExc declaration
 * @param degThres Threshold for dDeg declaration
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineThresSet(AtBerEngine engine, eAtUtilBerRate *excThres, eAtUtilBerRate *degThres);

/**
 * To get BER thresholds info.
 *
 * @param engine BER Engine
 * @param dExcThres Returned threshold for dExc declaration
 * @param dDegThres Returned threshold for dDeg declaration
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineThresGet(AtBerEngine engine, eAtUtilBerRate *excThres, eAtUtilBerRate *degThres);

/**
 * To set BER channel type.
 *
 * @param engine BER Engine
 * @param Channel type.
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineChnTypeSet(AtBerEngine engine, eAtBerChnType chnType);

/**
 * To get BER channel type.
 *
 * @param engine BER Engine
 *
 * @return Channel type.
 */
eAtBerChnType AtBerEngineChnTypeGet(AtBerEngine engine);

/**
 * To get the current status of BER engine
 *
 * @param engine BER Engine
 * @param dExc Returned EXC defect
 * @param dDeg Returned DEG defect
 * @param rate Returned current bit error rate
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineStatusGet(AtBerEngine engine, atbool *excDef, atbool *degDef, eAtUtilBerRate *rate);

/**
 * To get the history event of raised dEXC and/or raised dDEG of BER engine
 *
 * @param engine BER Engine
 * @param dExc Returned EXC defect
 * @param dDeg Returned DEG defect
 * @param clrHistory [input] Clear event or not.
 *
 * @return "eAtBerRet" BER returned codes.
 */
eAtBerRet AtBerEngineEventGet(AtBerEngine engine, atbool *excDef, atbool *degDef, atbool clrHistory);

/**
 * To get the source code revision
 *
 * @param module Module information
 *
 * @return Revision code in string.
 */
char * AtBerModuleVersion(AtBerModule module);

/* Debugging */
void AtBerModuleDebugEnable(AtBerModule module, eBool enable);
eBool AtBerModuleDebugIsEnabled(AtBerModule module);
void AtBerEngineDebugEnable(AtBerEngine engine, eBool enable);
eBool AtBerEngineDebugIsEnabled(AtBerEngine engine);

#ifdef __cplusplus
}
#endif
#endif /* ATBER_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module SUR
 * 
 * File        : surtypes.h
 * 
 * Created Date: Oct 10, 2014
 *
 * Description : Define types use in SUR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _SURTYPES_H_
#define _SURTYPES_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#define bool                unsigned char
#define public
#define friend
#define private static
#define    null            ((void*)0)
#define true            1
#define false           0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef unsigned char           byte;
typedef unsigned short          word;
typedef unsigned long           dword;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _SURTYPES_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - UTILITIES
 *
 * File        : surutil.h
 *
 * Created Date: 03-Jan-09
 *
 * Description : This file contain definitions of utility functions
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
 
 /*--------------------------- Define ----------------------------------------*/
 
 /*--------------------------- Macros ----------------------------------------*/
 
 /*--------------------------- Typedefs --------------------------------------*/
 
 /*--------------------------- Entries ---------------------------------------*/
 /*-----------------------------------------------------------------------------
 This function is used to get current system time
 -----------------------------------------------------------------------------*/
 bool SurCurTime(tSurTime *pTime);
 
 /*-----------------------------------------------------------------------------
 This function is used to get current system time in second
 -----------------------------------------------------------------------------*/
 bool SurCurTimeInS(dword *pTimeVal);
 
 /*-----------------------------------------------------------------------------
 This function is used to get time detail from time value
 -----------------------------------------------------------------------------*/
 bool SurTimeChange(dword timeVal, tSurTime *pTime);

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - TRANSMISSION SURVEILLANCE
 *
 * File        : surdb.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contain database definitions of Transmisstion 
 *               Surveillance module
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _SUR_DB_HEADER_
#define _SUR_DB_HEADER_

/*--------------------------- Includes ---------------------------------------*/
#include "suradapt.h"
#include "surtypes.h"
#include "sur.h"

/*--------------------------- Define -----------------------------------------*/
/* Maximum number of VTs in one STS */
#define cSurMaxVtInSts               28
#define cSurMaxDe1InDe3              cSurMaxVtInSts

/* Device description length */
#define cSurDevDesLen                64

/* Invalid value */
#define cSurInvlVal                  0xFFFF

/* Reset value */
#define cSurResetVal                 0

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mRetCheck(retCode)
Purpose   : This macro is used to check return code. If return code is not ok,
            it will exit the calling function
Inputs    : retCode                  - return code
------------------------------------------------------------------------------*/
#define mRetCheck(retCode)                                                     \
    {                                                                          \
    if ((retCode) != cSurOk)                                                   \
        {                                                                      \
        return retCode;                                                        \
        }                                                                      \
    }
    
/*------------------------------------------------------------------------------
Prototype : mLstRetCheck(retCode)
Purpose   : This macro is used to check list's return code.
Inputs    : retCode                  - return code
------------------------------------------------------------------------------*/
#define mLstRetCheck(retCode)                                                  \
    {                                                                          \
    if ((retCode) != cListSucc)                                                \
        {                                                                      \
        return cSurErr;                                                        \
        }                                                                      \
    }
    
/*------------------------------------------------------------------------------
Prototype : mLstRet2SurRet(lstRet, surRet)
Purpose   : This macro is used to convert eListRet to eSurRet
Inputs    : retCode                  - return code
------------------------------------------------------------------------------*/
#define mLstRet2SurRet(lstRet, surRet)                                         \
    {                                                                          \
    if ((lstRet) == cListSucc)                                                 \
        {                                                                      \
        (surRet) = cSurOk;                                                      \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (surRet) = cSurErr;                                                      \
        }                                                                      \
    }
                                                                               
/*------------------------------------------------------------------------------
Prototype : mSurApslIdFlat(pApslId) 
            mSurLineIdFlat(pLineId) 
            mSurStsIdFlat(pStsId)   
            mSurAuIdFlat(pStsId)    
            mSurTu3IdFlat(pTu3Id)   
            mSurVtIdFlat(pVtId)     
            mSurTuIdFlat(pTuId)     
            mSurDe3IdFlat(pDe3Id)   
            mSurDe1IdFlat(pDe1Id)   
Purpose   : These macros are used to convert channel identifiers to a flat number
Inputs    : Channel ID
Return    : flat number
------------------------------------------------------------------------------*/
#define mSurApslIdFlat(pApslId) ((*(pApslId)) - 1)
#define mSurLineIdFlat(pLineId) ((*(pLineId)) - 1)
#define mSurStsIdFlat(pStsId)   (((pStsId)->localId) - 1)
#define mSurAuIdFlat(pStsId)    (SurAuIdFlat(pStsId))
#define mSurTu3IdFlat(pTu3Id)   (SurTu3IdFlat(pTu3Id))
#define mSurVtIdFlat(pVtId)     ((((pVtId)->vtg - 1) * 4) + ((pVtId)->vt - 1))
#define mSurTuIdFlat(pTuId)     ((((pTuId)->tug2 - 1) * 4) + ((pTuId)->tu - 1))
#define mSurDe3IdFlat(pDe3Id)   ((*(pDe3Id)) - 1)
#define mSurDe1IdFlat(pDe1Id)   ((((pDe1Id)->de2Id - 1) * 4) + ((pDe1Id)->de1Id - 1))
#define mSurImaLinkIdFlat(pImaLink)   ((*(pImaLink)) - 1)
#define mSurImaGrpIdFlat(pImaGrp)   ((*(pImaGrp)) - 1)
#define mSurPwIdFlat(pPwId)   ((*(pPwId)) - 1)
/*------------------------------------------------------------------------------
Prototype : mSurVtIdUnFlat(flatNum, pVtId)     
            mSurDe1IdUnFlat(flatNum, pDe1Id)   
Purpose   : These macros are used to convert a flat number to VT ID and DS1/E1 
            ID
Inputs    : flatNum                  - flat number
Outputs   : pVtId                    - VT ID
Return    : None
------------------------------------------------------------------------------*/
#define mSurVtIdUnFlat(flatNum, pVtId)                                         \
    (pVtId)->vtg = ((flatNum) / 4) + 1;                                        \
    (pVtId)->vt  = ((flatNum) % 4) + 1;
#define mSurDe1IdUnFlat(flatNum, pDe1Id)                                       \
    (pDe1Id)->de2Id = ((flatNum) / 4) + 1;                                     \
    (pDe1Id)->de1Id = ((flatNum) % 4) + 1;
    
/*------------------------------------------------------------------------------
Prototype : mSurApslInfo(pSurInfo, pApslId, pChnInfo)
Purpose   : This macro is used to get information of a Linear APS engine
Inputs    : pSurInfo                 - device database
            pApslId                  - Linear APS engine ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurApslInfo(pSurInfo, pApslId, pChnInfo)                              \
    {                                                                          \
    if ((*(pApslId)) <= cSurMaxApslEng)                                        \
        {                                                                      \
        (pChnInfo) = mChnDb(pSurInfo).pApsl[mSurApslIdFlat(pApslId)];          \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (pChnInfo) = null;                                                     \
        }                                                                      \
    }
    
/*------------------------------------------------------------------------------
Prototype : mSurLineInfo(pSurInfo, pLineId, pChnInfo)
Purpose   : This macro is used to get information of a line
Inputs    : pSurInfo                 - device database
            pLineId                  - line ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurLineInfo(pSurInfo, pLineId, pChnInfo)                              \
    {                                                                          \
    if ((*(pLineId)) <= cSurMaxLine)                                           \
        {                                                                      \
        (pChnInfo) = mChnDb(pSurInfo).pLine[mSurLineIdFlat(pLineId)]->pInfo;   \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (pChnInfo) = null;                                                     \
        }                                                                      \
    }
    
/*------------------------------------------------------------------------------
Prototype : mSurStsInfo(pSurInfo, pStsId, pChnInfo)
Purpose   : This macro is used to get information of a STS
Inputs    : pSurInfo                 - device database
            pStsId                   - STS ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurStsInfo(pSurInfo, pStsId, pChnInfo)                                    \
	{																		  	   \
	if (mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pStsId)->line))] != null)	       \
		{                                                                          \
		if (mSurStsIdFlat(pStsId) <                                                \
			mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pStsId)->line))]->numSts)     \
			{                                                                      \
			if (mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pStsId)->line))]->ppStsDb[mSurStsIdFlat(pStsId)] != null) \
				{																   \
				(pChnInfo) = mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pStsId)->line))]->ppStsDb[mSurStsIdFlat(pStsId)]->pInfo;\
				}																   \
			}                                                                      \
		else                                                                       \
			{                                                                      \
			(pChnInfo) = null;                                                     \
			}																	   \
		}																		   \
	}
/*------------------------------------------------------------------------------
Prototype : mSurVtInfo(pSurInfo, pVtId, pChnInfo)
Purpose   : This macro is used to get information of a VT
Inputs    : pSurInfo                 - device database
            pVtId                    - VT ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurVtInfo(pSurInfo, pVtId, pChnInfo)                                  \
    {                                                                          \
	if (mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pVtId)->sts.line))] != null)	       \
		{                                                                          \
		if (mSurStsIdFlat(&((pVtId)->sts)) <                                       \
			mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pVtId)->sts.line))]->numSts)  \
			{                                                                      \
			if (mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pVtId)->sts.line))]->ppStsDb[mSurStsIdFlat(&((pVtId)->sts))] != null) \
				{																   \
				pChnInfo = mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pVtId)->sts.line))]->ppStsDb[mSurStsIdFlat(&((pVtId)->sts))]->ppVtInfo[mSurVtIdFlat(pVtId)]; \
				}																   \
			}                                                                      \
		else                                                                       \
			{                                                                      \
			pChnInfo = null;                                                       \
			}                                                                      \
		}																		   \
    }

/*------------------------------------------------------------------------------
Prototype : mSurAuInfo(pSurInfo, pAuId, pChnInfo)
Purpose   : This macro is used to get information of a AU-n
Inputs    : pSurInfo                 - device database
            pAuId                    - AU-n ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurAuInfo(pSurInfo, pAuId, pChnInfo)                                  \
    {                                                                          \
    tSurStsId _stsId;                                                          \
    SurAu2Sts(pAuId, &_stsId, null);                                           \
    mSurStsInfo(pSurInfo, &(_stsId), pChnInfo)                                 \
    }

/*------------------------------------------------------------------------------
Prototype : mSurTu3Info(pSurInfo, pTu3Id, pChnInfo)
Purpose   : This macro is used to get information of a TU-3
Inputs    : pSurInfo                 - device database
            pTu3Id                   - TU-3 ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurTu3Info(pSurInfo, pTu3Id, pChnInfo)                                \
    {                                                                          \
    if (mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pTu3Id)->au.line))] != null)     \
		{																		   \
		if (mSurTu3IdFlat(pTu3Id) <                                                \
			mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pTu3Id)->au.line))]->numSts)     \
			{                                                                      \
			if (mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pTu3Id)->au.line))]->ppStsDb[mSurTu3IdFlat(pTu3Id)] != null) \
				{																   \
				(pChnInfo) = mChnDb(pSurInfo).pLine[mSurLineIdFlat(&((pTu3Id)->au.line))]->ppStsDb[mSurTu3IdFlat(pTu3Id)]->pTu3Info; \
				}																   \
			}                                                                      \
		else                                                                       \
			{                                                                      \
			(pChnInfo) = null;                                                     \
			}                                                                      \
		}																		   \
    }

/*------------------------------------------------------------------------------
Prototype : mSurTuInfo(pSurInfo, pTuId, pChnInfo)
Purpose   : This macro is used to get information of a TU-m
Inputs    : pSurInfo                 - device database
            pTuId                    - TU-m ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurTuInfo(pSurInfo, pTuId, pChnInfo)                                  \
    {                                                                          \
    tSurVtId _vtId;                                                            \
    SurTu2Vt(pTuId, &_vtId);                                                   \
    mSurVtInfo(pSurInfo, &_vtId, pChnInfo);                                    \
    }

/*------------------------------------------------------------------------------
Prototype : mSurDe3Info(pSurInfo, pDe3Id, pChnInfo)
Purpose   : This macro is used to get information of a DS3/E3
Inputs    : pSurInfo                 - device database
            pDe3Id                   - DS3/E3 ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurDe3Info(pSurInfo, pDe3Id, pChnInfo)                                \
    {                                                                          \
    if ((*(pDe3Id)) <= cSurMaxDe3Line)                                         \
        {                                                                      \
        (pChnInfo) = mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(pDe3Id)]->pInfo;      \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (pChnInfo) = null;                                                     \
        }                                                                      \
    }
    
/*------------------------------------------------------------------------------
Prototype : mSurDe1Info(pSurInfo, pDe1Id, pChnInfo)
Purpose   : This macro is used to get information of a DS1/E1
Inputs    : pSurInfo                 - device database
            pDe1Id                   - DS1/E1 ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurDe1Info(pSurInfo, pDe1Id, pChnInfo)                                \
	{																		   \
	if (mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(&((pDe1Id)->de3Id))] != null)	   \
		{																	   \
		(pChnInfo) = mChnDb(pSurInfo).pDe3[mSurDe3IdFlat(&((pDe1Id)->de3Id))]->pDe1Db[mSurDe1IdFlat(pDe1Id)]; \
		}																	   \
	}

/*------------------------------------------------------------------------------
Prototype : mSurImaLinkInfo(pSurInfo, pImaLinkId, pChnInfo)
Purpose   : This macro is used to get information of a IMA link
Inputs    : pSurInfo                 - device database
            pImaLinkId               - IMA Link ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurImaLinkInfo(pSurInfo, pImaLinkId, pChnInfo)                                \
    {                                                                          \
    if ((*(pImaLinkId)) <= cSurMaxImaLink)                                         \
        {                                                                      \
        (pChnInfo) = mChnDb(pSurInfo).pImaLink[mSurImaLinkIdFlat(pImaLinkId)];      \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (pChnInfo) = null;                                                     \
        }                                                                      \
    }

/*------------------------------------------------------------------------------
Prototype : mSurImaGrpInfo(pSurInfo, pImaGrpId, pChnInfo)
Purpose   : This macro is used to get information of a IMA Group
Inputs    : pSurInfo                 - device database
            pImaGrpId                - IMA Group ID
Outputs   : pChnInfo                 - channel information
------------------------------------------------------------------------------*/
#define mSurImaGrpInfo(pSurInfo, pImaGrpId, pChnInfo)                                \
    {                                                                          \
    if ((*(pImaGrpId)) <= cSurMaxImaGrp)                                         \
        {                                                                      \
        (pChnInfo) = mChnDb(pSurInfo).pImaGrp[mSurImaGrpIdFlat(pImaGrpId)];      \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (pChnInfo) = null;                                                     \
        }                                                                      \
    }

/*------------------------------------------------------------------------------
Prototype : mSurPwInfo(pSurInfo, pPwId, pChnInfo)
Purpose   : This macro is used to get information of a PW
Inputs    : pSurInfo            - device database
            pPwId               - PW ID
Outputs   : pChnInfo            - channel information
------------------------------------------------------------------------------*/
#define mSurPwInfo(pSurInfo, pPwId, pChnInfo)                                \
    {                                                                          \
    if ((*(pPwId)) <= cSurMaxPw)                                         \
        {                                                                      \
        (pChnInfo) = mChnDb(pSurInfo).pPw[mSurPwIdFlat(pPwId)];      \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        (pChnInfo) = null;                                                     \
        }                                                                      \
    }

/*------------------------------------------------------------------------------
Prototype : mDevTake(device)

Purpose   : This macro is used to take semaphore of a device, if success,
            it will return database handle of device.
Inputs    : device                   - device. Its type must be tSurDev
Return    : tSurInfo                 - database handle of device or null if fail
------------------------------------------------------------------------------*/
#define mDevTake(device)                                                       \
    (((device) == null) ||                                                     \
      (OsalSemTake(&(((tSurInfo *)(device))->sem)) != cOsalOk)) ? \
     null : (tSurInfo *)(device)
   
/*------------------------------------------------------------------------------
Prototype : mDevGive(device)
Purpose   : This macro is used to give semaphore of a device
Inputs    : device                   - device.
Return    : cSurOk                   - success
            cSurErrSem               - semaphore error
------------------------------------------------------------------------------*/
#define mDevGive(device)                                                       \
    (OsalSemGive(&(((tSurInfo *)(device))->sem)) == cOsalOk) ? cSurOk : cSurErrSem
    
/*------------------------------------------------------------------------------
Prototype : mDevDb(device)
Purpose   : This macro is used to get database of a device
Inputs    : device                   - device.
Return    : tSurInfo                 - device database
------------------------------------------------------------------------------*/
#define mDevDb(device) ((tSurInfo *)(device))

/*------------------------------------------------------------------------------
Prototype : mDbTake()
            mDbGive()
Purpose   : These macros are used to take/give semaphore of database.
Return    : cSurOk                   - success
            cSurErrSem               - semaphore error
------------------------------------------------------------------------------*/
#define mDbTake() (OsalSemTake(&(surDb.sem)) != cOsalOk) ? cSurErrSem : cSurOk
#define mDbGive() (OsalSemGive(&(surDb.sem)) != cOsalOk) ? cSurErrSem : cSurOk

/*------------------------------------------------------------------------------
Prototype : mChnDb
Purpose   : These macros are used to get channel database
Return    : tChnDb                   - Channel database
------------------------------------------------------------------------------*/
#define mChnDb(pSurInfo) ((pSurInfo)->pDb->chnDb)

/*------------------------------------------------------------------------------
Prototype    : mFailStatCopy(pSrc, pDes)
Purpose      : This macro copy fail status from source to destination
Inputs       : pSrc                  - source
Outputs      : pDes                  - destination
Return       : Surveillance return code
------------------------------------------------------------------------------*/
#define mSurFailStatCopy(pSrc, pDes)                                           \
    {                                                                          \
    (pDes)->status = (pSrc)->status;                                           \
    (pDes)->time   = (pSrc)->time;                                             \
    }

/*--------------------------- Typedefs ---------------------------------------*/
/* Defect status */
typedef struct tSurDefStat
    {
    bool     status; /* [true/false] Defect status */
    dword    time;   /* Time when defect changed its status */
    }tSurDefStat;

/* PM parameter information */
typedef struct tPmParm
    {
    dword  curSec;       /* [Full] Current second register */
    word   perCnt;       /* [0..900] The 15-minute accumulation period */
    word   dayCnt;       /* [0..86,400] The daily accumulation period */
    bool   perTca;       /* [true/false] Indicate that period TCA is reported */
    bool   dayTca;       /* [true/false] Indicate that day TCA is reported */
    bool   prePerTca;    /* [true/false] Indicate that previous period TCA is reported */
    bool   preDayTca;    /* [true/false] Indicate that previous day TCA is reported */
    word   autoRep;      /* Resolution to periodically report PM data. The 
                            resolution is 1 minute. This function will be 
                            disabled if its value is zero */
    tPmReg curPer;       /* Current period register */
    tPmReg prePer;       /* Previous period register */
    tPmReg curPerNegAdj; /* Current period negative adjustment register */
    tPmReg prePerNegAdj; /* Previous period negative adjustment register */
    tPmReg curPerPosAdj; /* Current period positive adjustment register */
    tPmReg prePerPosAdj; /* Previous period positive adjustment register */
    tPmReg curDay;       /* Current day register */
    tPmReg preDay;       /* Previous day register */
    tPmReg *pRecent;     /* Recent registers */
    byte   numRecReg;    /* Number of recent registers */
    dword  perThres;     /* Period threshold */
    dword  dayThres;     /* Day threshold */
    bool   inhibit;      /* [true/false] Inhibit accumulation */
    }tPmParm;

/* DS1/E1 PM parameters */
typedef struct tSurDe1Parm
    {
    /* DS1/E1 Line Near-End */
    tPmParm cvL;     /* Code Violations-Line.  A count of BPVs and EXZs 
                        occurring over the accumulation period. */
    tPmParm esL;     /* Errored Seconds-Line.  A count of seconds during which 
                        one or more of the following has occurred: BPVs, EXZs, 
                        and LOSs. */
    tPmParm sesL;    /* Severely Errored Seconds-Line. A count of seconds 
                        during which 1544 or more BPVs or EXZs, or one or more 
                        LOS defects, have occurred. */
    tPmParm lossL;   /* Loss of Signal Seconds-Line. A count of 1-second 
                        intervals containing one or more LOS defects. */
    
    /* DS1/E1 Path Near-End */
    tPmParm cvP;     /* Code Violations-Path. For DS1-ESF paths, this 
                        parameter is a count of detected CRC-6 errors. 
                        For DS1-SF paths, since no redundancy check mechanism 
                        is incorporated into the frame format to detect errors 
                        in payload bits, FEs are used.  Thus, for DS1-SF paths, 
                        this parameter is a count of detected FEs.   */
    tPmParm esP;     /* Errored Seconds-Path. In the case of DS1 ESF, this 
                        parameter is a count of 1-second intervals containing 
                        one or more CRC-6 errors, or one or more CS events, or 
                        one or more SEF or AIS defects.  In the case of DS1 SF, 
                        this parameter is a count of 1-second intervals 
                        containing one or more FE events, or one or more CS 
                        events, or one or more SEF or AIS defects.  */
    tPmParm esaP;    /* Errored Second type A. This parameter applies to DS1 ESF 
                        paths only. It is a count of one second intervals with 
                        exactly one CRC-6 error, and no SEF or AIS defects. */
    tPmParm esbP;    /* Errored Second type B. This parameter applies to DS1 ESF 
                        paths only. It is a count of one second intervals with 
                        no less than 2 and not more than 319 CRC-6 error, and no 
                        SEF or AIS defects. */
    tPmParm sesP;    /* Severely Errored Seconds-Path. For DS1-ESF paths, this 
                        parameter is a count of seconds during which at least 
                        one of the following has occurred: 320 or more CRC-6 
                        errors, or one or more SEF or AIS defects.  For DS1-SF 
                        paths, an SES is a second containing either the 
                        occurrence of eight FEs (if both Ft and Fs bits are 
                        measured), four FEs (if only Ft bits are measured), or 
                        one or more SEF or AIS defects. */
    tPmParm aissP;   /* Alarm Indication Signal Seconds-Path. This parameter 
                        is a count of seconds containing one or more AIS 
                        defects. */
    tPmParm aissCiP; /* AIS-CI second. This parameter is a count of 1-second 
                        intervals containing one or more AIS-CI defects. */
    tPmParm sasP;    /* Severely Errored Frame/Alarm Indication Signal-Path. 
                        This parameter is a count of seconds containing one or 
                        more SEFs or one or more AIS defects. */
    tPmParm cssP;    /* Controlled Slip Seconds-Path. This parameter is a 
                        count of seconds during which a CS has occurred. 
                        Counts of CSs can be accurately made only in the path 
                        terminating NE of the DS1 signal where the CS takes 
                        place. */
    tPmParm uasP;    /* Unavailable Seconds-Path. This parameter is a count of 
                        1-second intervals during which the DS1 path is 
                        unavailable.  */
    tPmParm fcP;     /* Near-end Failure Count-Path.  This parameter is a 
                        count of the number of occurrences of near-end path 
                        failure events, with the failure event defined as 
                        follows: a near-end path failure event begins when 
                        either an LOF or AIS failure is declared and ends when 
                        both LOF and AIS failures are clear. */
    
    /* DS1/E1 Far-End */
    tPmParm esLfe;   /* Errored Seconds-Line. This parameter is a count of 
                        1-second PRM intervals containing an LV=1. */
    tPmParm sefsPfe; /* Severely Errored Frame-Path. This parameter is a count 
                        of 1- second PRM intervals containing an SE=1. */
    tPmParm cvPfe;   /* Code Violation-path. This parameter is a count of number 
                        of far-end CVs occurring during the accumulation 
                        period. */
    tPmParm esPfe;   /* Errored Seconds-Path. This parameter is a count of 
                        1-second PRM intervals containing a G1=1, G2=1, G3=1, 
                        G4=1, G5=1, G6=1, SE=1, SL=1, or an RAI signal.  */
    tPmParm esaPfe;  /* Errored Second type A. This parameter is a count of 
                        1-second PRM intervals containing a G1 = 1 and SE = 0. */
    tPmParm esbPfe;  /* Errored Second type B. This parameter is a count of 
                        1-second PRM intervals containing an SE = 0 and a 1 in 
                        any of following: G2, G3, G4, or G5. */
    tPmParm sesPfe;  /* Severely Errored Seconds-Path. This parameter is a 
                        count of 1-second PRM intervals containing a G6=1, SE=1, 
                        or an RAI signal.  */
    tPmParm cssPfe;  /* Controlled Slip Seconds-Path. This parameter is a 
                        count of 1- second PRM intervals containing an SL=1.  */
    tPmParm fcPfe;   /* Failure Count-Path. This parameter is a count of the 
                        number of occurrences of far-end path failure events, 
                        with the failure event defined as follows: a far-end 
                        path failure indication begins when an RAI failure is 
                        declared and ends when the RAI failure is cleared. */
    tPmParm uasPfe;  /* Unavailable Seconds-Path. This parameter is a count of 
                        1- second intervals during which the DS1 path is 
                        unavailable. */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    }tSurDe1Parm;

/* DS1/E1 failure indication */
typedef struct tSurDe1Fail
    {
    tSurFailStat los;   /* DS1/E1 line Loss Of Signal failure */
    tSurFailStat lof;   /* DS1/E1 path Loss Of Frame failure */
    tSurFailStat ais;   /* DS1/E1 path Alarm Indication Signal failure */
    tSurFailStat aisCi; /* DS1/E1 path Alarm Indication Signal � Customer 
                           Installation failure */
    tSurFailStat rai;   /* DS1/E1 path Remote Alarm Indication failure */
    tSurFailStat raiCi; /* DS1/E1 path Remote Alarm Indication - Customer 
                           Installation failure */
    tSurFailStat lBit;  /* L bit failure */
    tSurFailStat rBit;  /* R bit failure */
    tSurFailStat mBit;  /* M bit failure */
    }tSurDe1Fail;

/* DS1/E1 status */
typedef struct tSurDe1Stat
    {
    /* Engine state information */
    eSurEngStat state;       /* Engine state */
    dword       start;       /* Time when FM&PM are started */
    dword       stop;        /* Time when FM&PM are stopped */
    
    /* Defect status */
    tSurDefStat los;         /* DS1/E1 line Loss Of Signal */
    tSurDefStat oof;         /* DS1/E1 path Out Of Frame */
    tSurDefStat sef;         /* DS1/E1 path Severely Errored Frame */
    tSurDefStat ais;         /* DS1/E1 path Alarm Indication Signal */
    tSurDefStat aisCi;       /* DS1/E1 path Alarm Indication Signal � Customer 
                                Installation */
    tSurDefStat rai;         /* DS1/E1 path Remote Alarm Indication */
    tSurDefStat raiCi;       /* DS1/E1 path Remote Alarm Indication - Customer 
                                Installation */
    tSurDefStat lBit;        /* L bit defect */
    tSurDefStat rBit;        /* R bit defect */
    tSurDefStat mBit;        /* M bit defect */
    
    /* Unavailable status */
    bool        neUatPot;    /* [true/false] Near-End potential for 
                                             entering/exiting unavailable 
                                             time indication. */
    byte        nePotCnt;    /* Near-End potential for entering/exiting 
                                unavailable time seconds.  */
    bool        neUat;       /* [true/false] Near-End Unavailable status */
    bool        feUatPot;    /* [true/false] Far-End potential for 
                                             entering/exiting unavailable 
                                             time indication. */
    byte        fePotCnt;    /* Far-End potential for entering/exiting 
                                unavailable time seconds.  */
    bool        feUat;       /* [true/false] Far-End Unavailable status */
    }tSurDe1Stat;

/* DS1/E1 information */
typedef struct tSurDe1Info
    {
    tSurDe1Conf    conf;    /* DS1/E1 configuration */
    tSurDe1MsgInh  msgInh;  /* DS1/E1 automatic messages inhibition */
    tSurDe1Stat    status;  /* DS1/E1 status */
    tSurDe1Fail    failure; /* DS1/E1 failure indication */
    tSurDe1Parm    parm;    /* DS1/E1 PM parameters */
    tSortList      failHis; /* DS1/E1 failure history */
    tSortList      tcaHis;  /* DS1/E1 TCA history */
    }tSurDe1Info;

/* DS3/E3 PM parameters */
typedef struct tSurDe3Parm
    {
    /* DS3 Near-End Line */
    tPmParm cvL;      /* Code Violations-Line. This parameter is a count of both 
                         BPVs and EXZs occurring over the accumulation period. */
    tPmParm esL;      /* Errored Seconds-Line. This parameter is a count of 
                         seconds containing one or more BPVs, one or more EXZs, 
                         or one or more LOS defects. */
    tPmParm esaL;     /* Errored Second-Line type A. This parameter is a count 
                         of 1-second intervals containing one BPV or EXZ and no 
                         LOS defect. */
    tPmParm esbL;     /* Errored Second-Line type B. This parameter is a count 
                         of 1-second intervals containing more than one but less 
                         than 44 BPV plus EXZ and no LOS defect. */
    tPmParm sesL;     /* Severely Errored Seconds-Line. This parameter is a 
                         count of seconds during which BPVs plus EXZs exceed 44, 
                         or one or more LOS defects occur BPVs that are part of 
                         the zero substitution code are excluded. */
    tPmParm lossL;    /* LOS Second. This parameter is a count of 1-second 
                         intervals containing one or more LOS defects. */
    
    /* DS3 Near-End Path */
    tPmParm cvpP;     /* Code Violations-Path, P-bit. CVP-P is a count of P-bit 
                         parity check CVs. */
    tPmParm cvcpP;    /* Code Violations-Path, CP-Bit. Apply only for the C-Bit 
                         Parity application. CVCP-P is the count of CP-bit 
                         parity errors occurring in the accumulation period. */
    tPmParm espP;     /* Errored Seconds-Path, P-bit. ESP-P is a count of 
                         seconds containing one or more P-bit parity errors, 
                         one or more SEF defects, or one or more AIS defects. */
    tPmParm esapP;    /* Errored Second type A, P-bit parity. This parameter is 
                         the count of 1-second intervals containing exactly one 
                         P-bit parity errors and no SEF or AIS-L defect. It is 
                         defined for both DS3 application. */
    tPmParm esbpP;    /* Errored Second type B, P-bit parity. This parameter is 
                         the count of 1-second intervals containing more than 
                         one but less than 44 P-bit parity errors and no SEF or 
                         AIS-L defect. It is defined for both DS3 application. */
    tPmParm escpP;    /* Errored Seconds-Path, CP-bit. For the C-Bit Parity 
                         application, the ESCP-P parameter is a count of 
                         seconds containing one or more CP-bit parity errors, 
                         one or more SEF defects, or one or more AIS defects. */
    tPmParm esacpP;   /* Errored Second type A, CP-bit parity. This parameter is 
                         the count of 1-second intervals containing one CP-bit 
                         parity errors and no SEF or AIS-L defect. It is defined 
                         for C-bit parity DS3 application. */
    tPmParm esbcpP;   /* Errored Second type B, CP-bit parity. This parameter is 
                         the count of 1-second intervals containing more than 
                         one but less than 44 CVCP-Ps and no SEF or AIS-L 
                         defect. It is defined for C-bit parity DS3 
                         application. */
    tPmParm sespP;    /* Severely Errored Seconds-Path, P-bit. SESP-P is a 
                         count of seconds containing more than 44 P-bit parity 
                         violations, one or more SEF defects, or one or more 
                         AIS defects. */
    tPmParm sescpP;   /* Severely Errored Seconds-Path, CP-bit. For the C-Bit 
                         Parity application, the SESCP-P parameter is a count 
                         of seconds containing more than 44 CP-bit parity 
                         errors, one or more SEF defects, or one or more AIS 
                         defects. */
    tPmParm sasP;     /* SEF/AIS Seconds-Path. This parameter is a count of 
                         seconds containing one or more SEF defects or one or 
                         more AIS defects. */
    tPmParm aissP;    /* AIS Seconds-Path.  This parameter is a count of 
                         1-second intervals containing one or more AIS defects. */
    tPmParm uaspP;    /* Unavailable Seconds-Path, caused by SESP-P.  This 
                         parameter is a count of 1-second intervals during 
                         which the DS3 path is unavailable. */
    tPmParm uascpP;   /* Unavailable Seconds-Path, caused by SESCP-P.  This 
                         parameter is a count of 1-second intervals during which 
                         the DS3 path is unavailable. */
    tPmParm fcP;      /* Near-end Failure Count-Path. This parameter is a 
                         count of the number of occurrences of near-end path 
                         failure events, with the failure event defined as 
                         follows: a near-end path failure event begins when 
                         either an LOF or AIS failure is declared and ends 
                         when both LOF and AIS failures are clear. */
    
    /* DS3 Far-End */
    tPmParm cvcpPfe;  /* Code Violation. This parameter is counted when the 
                         three FEBE bits in an M-frame are not all set to 1.  */
    tPmParm escpPfe;  /* Errored Second. This parameter is a count of 1-second 
                         intervals containing one or more M-frames with the 
                         three FEBE bits not all set to one, or one or more 
                         far-end SEF/AIS defects. */
    tPmParm esacpPfe; /* Error Second type A. This parameter is a count of 
                         1-second intervals containing one M-frames with the 
                         three FEBE bits not all set to one and no far-end 
                         SEF/AIS defects. */
    tPmParm esbcpPfe; /* Error Second type B. This parameter is a count of 
                         1-second intervals containing more than one but less 
                         than 44 M-frames with the three FEBE bits not all set 
                         to one and no far-end SEF/AIS defects. */
    tPmParm sescpPfe; /* Severely Errored Second. This parameter is a count of 
                         1-second intervals containing one or more than 44 
                         M-frames with the three FEBE bits not all set to one, 
                         or one or more far-end SEF/AIS defects. */
    tPmParm sascpPfe; /* SEF/AIS Second. This parameter is a count of 1-second 
                         intervals containing one or more far-end SEF/AIS 
                         defects. */
    tPmParm uascpPfe; /* Unavailable Second. This parameter is a count of 
                         1-second intervals during which the DS3 path is 
                         unavailable. */
    tPmParm fcPfe;    /* Far-end Failure Count-Path. This parameter is a count 
                         of the number of occurrences of far-end path failure 
                         events, with the far-end failure event defined as 
                         follows: a far-end path failure event begins when an 
                         RAI failure is declared and ends when the RAI failure 
                         is cleared. The FCCP-PFE applies only to the C-Bit 
                         Parity application. */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    }tSurDe3Parm;

/* DS3/E3 failure indication */
typedef struct tSurDe3Fail
    {
    tSurFailStat los; /* DS3/E3 line Loss Of Signal failure */
    tSurFailStat lof; /* DS3/E3 path Loss Of Frame failure */
    tSurFailStat ais; /* DS3/E3 path Alarm Indication Signal failure */
    tSurFailStat rai; /* DS3/E3 path Remote Alarm Indication */
    tSurFailStat lBit; /* L bit failure */
    tSurFailStat rBit; /* R bit failure */
    tSurFailStat mBit; /* M bit failure */
    }tSurDe3Fail;

/* DS3/E3 defect status */
typedef struct tSurDe3Stat
    {
    /* Engine state information */
    eSurEngStat state;         /* Engine state */
    dword       start;         /* Time when FM&PM are started */
    dword       stop;          /* Time when FM&PM are stopped */
                               
    /* Defect status */        
    tSurDefStat los;           /* DS3/E3 line Loss Of Signal */
    tSurDefStat oof;           /* DS3/E3 path Out Of Frame */
    tSurDefStat sef;           /* DS3/E3 path Severely Errored Frame */
    tSurDefStat ais;           /* DS3/E3 path Alarm Indication Signal */
    tSurDefStat sefAisFe;      /* Far-End SEF/AIS */
    tSurDefStat rai;           /* Remote Alarm Indication */
                               
    tSurDefStat lBit;           /* L bit failure */
    tSurDefStat rBit;           /* R bit failure */
    tSurDefStat mBit;           /* M bit failure */

    /* Unavailable status */   
    bool        neUatpPot;     /* [true/false] Near-End potential for 
                                  entering/exiting unavailable time indication 
                                  (caused by SESP-P) */
    bool        nePPotCnt;     /* [true/false] Near-End potential for 
                                  entering/exiting unavailable time seconds 
                                  (caused by SESP-P). */
    bool        neUatp;        /* [true/false] Near-End Unavailable status 
                                  (caused by SESP-P). */
    
    bool        neUatcpPot;     /* [true/false] Near-End potential for 
                                   entering/exiting unavailable time indication 
                                   (caused by SESCP-P) */
    bool        neCpPotCnt;      /* [true/false] Near-End potential for 
                                    entering/exiting unavailable time seconds 
                                    (caused by SESCP-P). */
    bool        neUatcp;        /* [true/false] Near-End Unavailable status 
                                   (caused by SESCP-P). */
    
    bool        feUatcpPot;     /* [true/false] Far-End potential for 
                                   entering/exiting unavailable time indication 
                                   (caused by SESCP-PFE) */
    bool        feCpPotCnt;      /* [true/false] Far-End potential for 
                                    entering/exiting unavailable time seconds 
                                    (caused by SESCP-PFE). */
    bool        feUatcp;        /* [true/false] Far-End Unavailable status 
                                   (caused by SESCP-PFE). */
    }tSurDe3Stat;

/* DS3/E3 information */
typedef struct tSurDe3Info
    {
    tSurDe3Conf    conf;    /* DS3/E3 configuration */
    tSurDe3MsgInh  msgInh;  /* DS3/E3 automatic messages inhibition */
    tSurDe3Stat    status;  /* DS3/E3 status */
    tSurDe3Fail    failure; /* DS3/E3 failure indication */
    tSurDe3Parm    parm;    /* DS3/E3 PM parameters */
    tSortList      failHis; /* DS3/E3 failure history */
    tSortList      tcaHis;  /* DS3/E3 TCA history */
    }tSurDe3Info;

/* VT PM parameters */
typedef struct tSurVtParm
    {
    /* VT Path Near-End */
    tPmParm cvV;      /* Near-end VT Path Coding Violation. A count of BIP 
                         errors detected at the VT Path layer (i.e., using 
                         bits 1 and 2 of the V5 byte in the incoming VT Path 
                         overhead). */
    tPmParm esV;      /* Near-end VT Path Errored Second. A count of the 
                         seconds during which (at any point during the second) 
                         at least one VT Path BIP error was detected, or an 
                         AIS-V defect (or a lower-layer, traffic-related, 
                         near-end defect), an LOP-V defect or, if the VT PTE 
                         monitoring the path supports ERDI-V for that path, an 
                         UNEQ-V defect was present. */
    tPmParm sesV;     /* Near-end VT Path Severely Errored Second. A count of 
                         the seconds during which K or more VT Path BIP errors 
                         were detected, or an AIS-V defect (or a lower-layer, 
                         traffic-related, near-end defect), an LOP-V defect or
                         , if the VT PTE monitoring the path supports ERDI-V 
                         for that path, an UNEQ-V defect was present.  The 
                         number of BIP errors that cause a second to be 
                         considered an SES-V may need to be settable */
    tPmParm uasV;     /* Near-end VT Path Unavailable Second. A count of the 
                         seconds during which the VT Path was considered 
                         unavailable.  A VT Path becomes unavailable at the 
                         onset of 10 consecutive seconds that qualify as SES-Vs, 
                         and continues to be unavailable until the onset of 10 
                         consecutive seconds that do not qualify as SES-Vs. */
    tPmParm fcV;      /* Near-end VT Path Failure Count. A count of the number 
                         of near-end VT Path failure events.  A failure event 
                         begins when an AIS-V failure (or a lower-layer, 
                         traffic-related, near-end failure), an LOP-V failure 
                         or, if the VT PTE monitoring the path supports ERDI-V 
                         for that path, an UNEQ-V failure is declared.  The 
                         failure event ends when these failures are cleared.  
                         A failure event that begins in one period and ends in 
                         another period is counted only in the period in which 
                         it begins.  Note that functionally, an AIS-V failure 
                         will be declared either by receiving (and timing) an 
                         AIS-V signal from another NE, or by receiving (and 
                         timing) an internally generated AIS-V signal from STS 
                         PTE in the same NE where the VT PTE resides. */
    tPmParm ppjcVdet; /* Positive Pointer Justification Count - VT Path 
                         Detected. A count of the positive pointer 
                         justifications (i.e., valid increment operations) 
                         detected on a particular path in an incoming SONET 
                         signal. */
    tPmParm npjcVdet; /* Negative Pointer Justification Count - VT Path 
                         Detected. A count of the negative pointer 
                         justifications (i.e., valid decrement operations) 
                         detected on a particular path in an incoming SONET 
                         signal. */
    tPmParm ppjcVgen; /* Positive Pointer Justification Count - VT Path 
                         Generated. A count of the positive pointer 
                         justifications (i.e., increment operations) generated 
                         for a particular path to reconcile the frequency of 
                         the SPE with the local clock. */
    tPmParm npjcVgen; /* Negative Pointer Justification Count - VT Path 
                         Generated. A count of the negative pointer 
                         justifications (i.e., decrement operations) generated 
                         for a particular path to reconcile the frequency of 
                         the SPE with the local clock. */
    tPmParm pjcDiffV; /* Pointer Justification Count Difference - VT Path. The 
                         absolute value of the difference between the net 
                         number of detected pointer justification counts and 
                         the net number of generated pointer justification 
                         counts.  That is, PJCDiff-V is equal to 
                         |(PPJC-VGen � NPJC-VGen) � (PPJC-VDet � NPJC-VDet)|. */
    tPmParm pjcsVdet; /* Pointer Justification Count Seconds - VT Path Detect. 
                         A count of the one-second intervals containing one or 
                         more PPJC-VDet or NPJC-VDet */
    tPmParm pjcsVgen; /* Pointer Justification Count Seconds - VT Path Generate. 
                         A count of the one-second intervals containing one or 
                         more PPJC-VGen or NPJC-VGen */
    
    /* VT Path Far-End */
    tPmParm cvVfe;    /* Far-end VT Path Coding Violation. A count of the 
                         number of BIP errors detected by the far-end VT PTE 
                         and reported back to the near-end VT PTE using the 
                         REI-V indication in the VT Path overhead. */
    tPmParm esVfe;    /* Far-end VT Path Errored Second.  A count of the 
                         seconds during which (at any point during the second) 
                         at least one VT Path BIP error was reported by the far
                         -end VT PTE (using the REI-V indication), a one-bit 
                         RDI-V defect was present, or (if ERDI-V is supported) 
                         an ERDI-V Server or Connectivity defect was present. */
    tPmParm sesVfe;   /* Far-end VT Path Severely Errored Second. A count of 
                         the seconds during which K or more VT Path BIP errors 
                         were reported by the far-end VT PTE, a one-bit RDI-V 
                         defect was present, or (if ERDI-V is supported) an 
                         ERDI-V Server or Connectivity defect was present. The 
                         number of reported far-end BIP errors that cause a 
                         second to be considered an SES-VFE may need to be 
                         settable. */
    tPmParm uasVfe;   /* Far-end VT Path Unavailable Second. A count of the 
                         seconds during which the VT Path is considered 
                         unavailable at the far end.  A VT Path is considered 
                         unavailable at the far end at the onset of 10 
                         consecutive seconds that qualify as SES-VFEs, and 
                         continues to be considered unavailable until the 
                         onset of 10 consecutive seconds that do not qualify 
                         as SES-VFEs. */
    tPmParm fcVfe;    /* Far-end VT Path Failure Count. A count of the number 
                         of far-end VT Path failure events.  A failure event 
                         begins when a one-bit RFI-V failure, or (if ERDI-V is 
                         supported) an ERFI-V Server or Connectivity failure 
                         is declared. The failure event ends when the RFI-V 
                         failure is cleared.  A failure event that begins in 
                         one period and ends in another period is counted only 
                         in the period in which it begins. */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    }tSurVtParm;

/* VT failure indication */
typedef struct tSurVtFail
    {
    tSurFailStat lopv;    /* VT path Loss Of Pointer */
    tSurFailStat plmv;    /* VT Path Payload Label Mismatch */
    tSurFailStat uneqv;   /* VT Path Unequipped */
    tSurFailStat timv;    /* VT Path Trace Identifier Mismatch */
    tSurFailStat aisv;    /* VT Path Alarm Indication Signal */
    tSurFailStat rfiv;    /* VT Path Remote Failure Indication. If ERDI-V is 
                             supported, this field will be ignored. */
    tSurFailStat erfivS;  /* VT Path Server Enhanced Remote Failure Indication. 
                             If ERDI-V is not supported, this field will be 
                             ignored. */
    tSurFailStat erfivC;  /* VT Path Connectivity Enhanced Remote Failure 
                             Indication. If ERDI-V is not supported, this field 
                             will be ignored. */
    tSurFailStat erfivP;  /* VT Path Payload Enhanced Remote Failure 
                             Indication If ERDI-V is not supported, this field 
                             will be ignored. */
    tSurFailStat vtBerSf; /* VT Path BER-based Signal Failure */
    tSurFailStat vtBerSd; /* VT Path BER-based Signal Degrade */

    tSurFailStat lBit;		/* L bit error only used for PW */
    tSurFailStat rBit;		/* R bit error only used for PW */
    tSurFailStat mBit;		/* M bit error only used for PW */
    }tSurVtFail;

/* VT status */
typedef struct tSurVtStat
    {
    /* Engine state information */
    eSurEngStat state;       /* Engine state */
    dword       start;       /* Time when FM&PM are started */
    dword       stop;        /* Time when FM&PM are stopped */
    
    /* Defect status */
    tSurDefStat lopv;        /* VT path Loss Of Pointer */
    tSurDefStat plmv;        /* VT Path Payload Label Mismatch */
    tSurDefStat uneqv;       /* VT Path Unequipped */
    tSurDefStat timv;        /* VT Path Trace Identifier Mismatch */
    tSurDefStat aisv;        /* VT Path Alarm Indication Signal */
    tSurDefStat rdiv;        /* VT Path Remote Defect Indication. If ERDI-V is 
                                supported, this field will be ignored. */
    tSurDefStat erdivS;      /* VT Path Server Enhanced Remote Defect 
                                Indication. If ERDI-V is not supported, this 
                                field will be ignored. */
    tSurDefStat erdivC;      /* VT Path Connectivity Enhanced Remote Defect 
                                Indication. If ERDI-V is not supported, this 
                                field will be ignored. */
    tSurDefStat erdivP;      /* VT Path Payload Enhanced Remote Defect 
                                Indication If ERDI-V is not supported, this 
                                field will be ignored. */
    tSurDefStat vtBerSf;     /* VT Path BER-based Signal Failure */
    tSurDefStat vtBerSd;     /* VT Path BER-based Signal Degrade */
    
    tSurDefStat lBit;		/* L bit error only used for PW */
    tSurDefStat rBit;		/* R bit error only used for PW */
    tSurDefStat mBit;		/* M bit error only used for PW */
    /* Unavailable status */
    bool        neUatPot;    /* [true/false] Near-End potential for 
                                             entering/exiting unavailable 
                                             time indication. */
    byte        nePotCnt;    /* Near-End potential for entering/exiting 
                                unavailable time seconds.  */
    bool        neUat;       /* [true/false] Near-End Unavailable status */
    bool        feUatPot;    /* [true/false] Far-End potential for 
                                             entering/exiting unavailable 
                                             time indication. */
    byte        fePotCnt;    /* Far-End potential for entering/exiting 
                                unavailable time seconds.  */
    bool        feUat;       /* [true/false] Far-End Unavailable status */
    }tSurVtStat;

/* VT/TU information */
typedef struct tSurVtInfo
    {
    tSurVtConf    conf;    /* VT configuration */
    tSurVtMsgInh  msgInh;  /* VT automatic messages inhibition */
    tSurVtStat    status;  /* VT status */
    tSurVtFail    failure; /* VT failure indication */
    tSurVtParm    parm;    /* VT PM parameters */
    tSortList     failHis; /* VT failure history */
    tSortList     tcaHis;  /* VT TCA history */
    }tSurVtInfo;
typedef tSurVtInfo tSurTuInfo;

/* STS PM parameters */
typedef struct tSurStsParm
    {
    /* STS Path Near-End */
    tPmParm cvP;      /* Near-end STS Path Coding Violation. A count of BIP 
                         errors detected at the STS Path layer  */
    tPmParm esP;      /* Near-end STS Path Errored Second. A count of the 
                         seconds during which (at any point during the second) 
                         at least one STS Path BIP error was detected, or an 
                         AIS-P defect (or a lower-layer, traffic-related, near-
                         end defect), an LOP-P defect or, if the STS PTE 
                         monitoring the path supports ERDI-P for that path, an 
                         UNEQ-P or TIM-P defect was present. */
    tPmParm sesP;     /* Near-end STS Path Severely Errored Second. A count of 
                         the seconds during which K or more STS Path BIP 
                         errors were detected, or an AIS-P defect (or a lower-
                         layer, traffic-related, near-end defect), an LOP-P 
                         defect or, if the STS PTE monitoring the path 
                         supports ERDI-P for that path, an UNEQ-P or TIM-P 
                         defect was present. The number of BIP errors that 
                         cause a second to be considered an SES-P may need to 
                         be settable. */
    tPmParm uasP;     /* Near-end STS Path Unavailable Second. A count of the 
                         seconds during which the STS Path was considered 
                         unavailable. An STS Path becomes unavailable at the 
                         onset of 10 consecutive seconds that qualify as SES-Ps, 
                         and continues to be unavailable until the onset of 10 
                         consecutive seconds that do not qualify as SES-P */
    tPmParm fcP;      /* Near-end STS Path Failure Count. A count of the 
                         number of near-end STS Path failure events. A failure 
                         event begins when an AIS-P failure (or a lower-layer, 
                         traffic-related, near-end failure), an LOP-P failure 
                         or, if the STS PTE monitoring the path supports ERDI-P 
                         for that path, an UNEQ-P or TIM-P failure is declared. 
                         Note that functionally, an AIS-P failure will be 
                         declared either by receiving (and timing) an AIS-P 
                         signal from another NE, or by receiving (and timing) 
                         an internally generated AIS-P signal from LTE in the 
                         same NE where the STS PTE resides. */
    tPmParm ppjcPdet; /* Positive Pointer Justification Count - STS Path 
                         Detected. A count of the positive pointer 
                         justifications (i.e., valid increment operations) 
                         detected on a particular path in an incoming SONET 
                         signal. */
    tPmParm npjcPdet; /* Negative Pointer Justification Count - STS Path 
                         Detected. A count of the negative pointer 
                         justifications (i.e., valid decrement operations) 
                         detected on a particular path in an incoming SONET 
                         signal. */
    tPmParm ppjcPgen; /* Positive Pointer Justification Count. A count of the 
                         positive pointer justifications (i.e., increment 
                         operations) generated for a particular path to 
                         reconcile the frequency of the SPE with the local 
                         clock. */
    tPmParm npjcPgen; /* Negative Pointer Justification Count - STS Path 
                         Generated. A count of the negative pointer 
                         justifications (i.e., decrement operations) generated 
                         for a particular path to reconcile the frequency of 
                         the SPE with the local clock. */
    tPmParm pjcDiffP; /* Pointer Justification Count Difference - STS Path. 
                         The absolute value of the difference between the net 
                         number of detected pointer justification counts and 
                         the net number of generated pointer justification 
                         counts. That is, PJCDiff-P is equal to 
                         |(PPJC-PGen � NPJC-PGen) � (PPJC-PDet � NPJC-PDet)|. */
    tPmParm pjcsPdet; /* Pointer Justification Count Seconds - STS Path Detect. 
                         A count of the one-second intervals containing one or 
                         more PPJC-PDet or NPJC-PDet. */
    tPmParm pjcsPgen; /* Pointer Justification Count Seconds - STS Path 
                         Generate. A count of the one-second intervals 
                         containing one or more PPJC-PGen or NPJC-PGen. */
    
    /* STS Path Far-End */
    tPmParm cvPfe;    /* Far-end STS Path Coding Violation. A count of the 
                         number of BIP errors detected by the far-end STS PTE 
                         and reported back to the near-end STS PTE using the 
                         REI-P indication in the STS Path overhead */
    tPmParm esPfe;    /* Far-end STS Path Errored Second. A count of the 
                         seconds during which (at any point during the second) 
                         at least one STS Path BIP error was reported by the 
                         far-end STS PTE (using the REI-P indication), a one-bit 
                         RDI-P defect was present, or (if ERDI-P is supported) 
                         an ERDI-P Server or Connectivity defect was present. */
    tPmParm sesPfe;   /* Far-end STS Path Severely Errored Second. A count of 
                         the seconds during which K or more STS Path BIP 
                         errors were reported by the far-end STS PTE, a one-bit 
                         RDI-P defect was present, or (if ERDI-P is supported) 
                         an ERDI-P Server or Connectivity defect was present. 
                         The number of reported far-end BIP errors that cause 
                         a second to be considered an SES-PFE may need to be 
                         settable. */
    tPmParm uasPfe;   /* Far-end STS Path Unavailable Second. A count of the 
                         seconds during which the STS Path is considered 
                         unavailable at the far end. An STS Path is considered 
                         unavailable at the far end at the onset of 10 
                         consecutive seconds that qualify as SES-PFEs, and 
                         continues to be considered unavailable until the 
                         onset of 10 consecutive seconds that do not qualify 
                         as SES-PFEs. */
    tPmParm fcPfe;    /* Far-end STS Path Failure Count. A count of the number 
                         of far-end STS Path failure events. A failure event 
                         begins when a one-bit RFI-P failure, or (if ERDI-P is 
                         supported) an ERFI-P Server or Connectivity failure 
                         is declared. */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    }tSurStsParm;

/* STS failure indication */
typedef struct tSurStsFail
    {
    tSurFailStat lopp;      /* STS path Loss Of Pointer */
    tSurFailStat plmp;      /* STS Path Payload Label Mismatch */
    tSurFailStat uneqp;     /* STS Path Unequipped */
    tSurFailStat timp;      /* STS Path Trace Identifier Mismatch */
    tSurFailStat aisp;      /* STS Path Alarm Indication Signal */
    tSurFailStat rfip;      /* STS Path one-bit Remote Failure Indication. If 
                               ERDI-P is supported, this field will be ignored. */
    tSurFailStat erfipS;    /* STS Path Server Enhanced Remote Failure 
                               Indication. If ERDI-P is not supported, this 
                               field will be ignored. */
    tSurFailStat erfipC;    /* STS Path Connectivity Enhanced Remote Failure 
                               Indication. If ERDI-P is not supported, this 
                               field will be ignored. */
    tSurFailStat erfipP;    /* STS Path Payload Enhanced Remote Failure 
                               Indication If ERDI-P is not supported, this 
                               field will be ignored. */
    tSurFailStat pathBerSf; /* STS Path BER-based Signal Failure */
    tSurFailStat pathBerSd; /* STS Path BER-based Signal Degrade */

    tSurFailStat lBit;		/* L bit error only used for PW */
    tSurFailStat rBit;		/* R bit error only used for PW */
    tSurFailStat mBit;		/* M bit error only used for PW */
    }tSurStsFail;

/* STS status */
typedef struct tSurStsStat
    {
    /* Engine state information */
    eSurEngStat state;       /* Engine state */
    dword       start;       /* Time when FM&PM are started */
    dword       stop;        /* Time when FM&PM are stopped */
                             
    /* Defect status */      
    tSurDefStat lopp;        /* STS path Loss Of Pointer */
    tSurDefStat plmp;        /* STS Path Payload Label Mismatch */
    tSurDefStat uneqp;       /* STS Path Unequipped */
    tSurDefStat timp;        /* STS Path Trace Identifier Mismatch */
    tSurDefStat aisp;        /* STS Path Alarm Indication Signal */
    tSurDefStat rdip;        /* STS Path one-bit Remote Defect Indication. If 
                                ERDI-P is supported, this field will be ignored. */
    tSurDefStat erdipS;      /* STS Path Server Enhanced Remote Defect 
                                Indication. If ERDI-P is not supported, this 
                                field will be ignored. */
    tSurDefStat erdipC;      /* STS Path Connectivity Enhanced Remote Defect 
                                Indication. If ERDI-P is not supported, this 
                                field will be ignored. */
    tSurDefStat erdipP;      /* STS Path Payload Enhanced Remote Defect 
                                Indication If ERDI-P is not supported, this 
                                field will be ignored. */
    tSurDefStat pathBerSf;   /* STS Path BER-based Signal Failure */
    tSurDefStat pathBerSd;   /* STS Path BER-based Signal Degrade */
    
    tSurDefStat lBit;   	 /* L bit error only used for PW */
    tSurDefStat rBit;   	 /* R bit error only used for PW */
    tSurDefStat mBit;   	 /* M bit error only used for PW */

    /* Unavailable status */
    bool        neUatPot;    /* [true/false] Near-End potential for 
                                             entering/exiting unavailable 
                                             time indication. */
    byte        nePotCnt;    /* Near-End potential for entering/exiting 
                                unavailable time seconds.  */
    bool        neUat;       /* [true/false] Near-End Unavailable status */
    bool        feUatPot;    /* [true/false] Far-End potential for 
                                             entering/exiting unavailable 
                                             time indication. */
    byte        fePotCnt;    /* Far-End potential for entering/exiting 
                                unavailable time seconds.  */
    bool        feUat;       /* [true/false] Far-End Unavailable status */

    }tSurStsStat;

/* STS/TU3 information */
typedef struct tSurStsInfo
    {
    tSurStsConf    conf;    /* STS configuration */
    tSurStsMsgInh  msgInh;  /* STS automatic messages inhibition */
    tSurStsStat    status;  /* STS status */
    tSurStsFail    failure; /* STS failure indication */
    tSurStsParm    parm;    /* STS PM parameters */
    tSortList      failHis; /* STS failure history */
    tSortList      tcaHis;  /* STS TCA history */
    }tSurStsInfo;

/* AU-n information */
typedef tSurStsInfo tSurAuInfo;

/* TU-3 information */
typedef tSurStsInfo tSurTu3Info;

/* Line PM parameters */
typedef struct tSurLineParm
    {
    /* Section */
    tPmParm sefsS;   /* Section Severely Errored Framing Seconds. It is a count 
                       of the seconds during which an SEF defect was present. */
    tPmParm cvS;    /* Section Coding Violations. Count of BIP errors (B1) 
                       detected at the Section layer. */
    tPmParm esS;    /* Section Errored Second. Count of the number of seconds 
                       during which at least one Section layer BIP error was 
                       detected or an SEF or LOS defect was present. */
    tPmParm sesS;   /* Section Severely Errored Seconds. Count of the seconds 
                       during which K or more Section layer BIP errors were 
                       detected or an SEF or LOS defect was present. K values 
                       are settable. */
    
    /* Line Near-End */
    tPmParm cvL;    /* Near-End Line Coding Violation. Count of BIP errors (B2) 
                       detected at the Line layer. */
                       
    tPmParm esL;    /* Near-End Line Errored Second. Count of the seconds 
                       during which at least one Line layer BIP error was 
                       detected or an AIS-L defect (or a lower-layer, 
                       traffic-related, near-end defect) was present. */
    tPmParm sesL;   /* Near-End Line Severely Errored Second. Count of the 
                       seconds during which K or more Line layer BIP errors 
                       were detected or an AIS-L defect (or a lower-layer, 
                       traffic-related, near-end defect) was present. K values 
                       are settable. */
    tPmParm uasL;   /* Near-End Line Unavailable Second. Count of the seconds 
                       during which the Line was considered unavailable. A 
                       line becomes unavailable at the onset of 10 consecutive 
                       seconds that qualify as SES-Ls and continue to be 
                       unavailable until the onset of 10 consecutive seconds 
                       that do not qualify as SES-Ls. */
    tPmParm fcL;    /* Near-End Line Failure Count. Count of the numbers of 
                       near-end line failure events. A failure event begins 
                       when the AIS-L failure (or a lower-layer, traffic-
                       related, near-end failure) is declared, and ends when 
                       the failure is cleared. The failure event that begins 
                       in one period and ends in another period is counted 
                       only in the period in which it begins. */
    tPmParm psc;    /* Protection Switching Count. For a working line, the PSC 
                       parameter is a count of the number of times that 
                       service has been switched from the monitored line to 
                       the protection line, plus the number of timers it has 
                       been switched back to the working line. For the 
                       protection line, it is a count of the number of times 
                       that service has been switched from any working line to 
                       the protection line, plus the number of times service 
                       has been switched back to a working line. This 
                       parameter is only applicable if line level protection 
                       switching is used. */
    tPmParm psd;    /* For a working line, the PSD parameter is a count of the 
                       seconds that service was being carried on the 
                       protection line. For the protection line, it is a count 
                       of the seconds that the line was being used to carry 
                       service. This parameter is only applicable if revertive 
                       line level protection switching is used. */
    
    /* Line Far-End */
    tPmParm cvLfe;  /* Far-End Line Coding Violation. Count of the number of 
                       BIP errors detected by the far-end LTE and reported 
                       back to the near-end LTE using the REI-L indication. */
                       
    tPmParm esLfe;  /* Far-End Line Errored Second. Count of the seconds 
                       during which at least one Line BIP error was reported 
                       by the far-end LTE or an RDI-L defect was present. */
    tPmParm sesLfe; /* Far-End Line Severely Errored Second. Count of the 
                       seconds during which K or more Line BIP errors were 
                       reported by the far-end LTE or an RDI-L defect was 
                       present. */
    tPmParm uasLfe; /* Far-End Line Unavailable Second. Count of the seconds 
                       during which the Line is considered unavailable at the 
                       far-end. A line is considered unavailable at the far-
                       end at the onset of 10 consecutive seconds that qualify 
                       as SES-LFE, and continues to considered unavailable 
                       until the onset of 10 consecutive seconds that do not 
                       qualify as SES-LFE. */
    tPmParm fcLfe;  /* Far-End Line Failure Count. Count of the number of 
                       far-end line failure events. A failure event begins when 
                       the RFI-L failure is declared, and ends when the RFI-L 
                       failure is cleared. A failure event that begins in one 
                       period and ends in another period is counted only in 
                       the period in which it begins. */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    }tSurLineParm;

/* Line failure indication */
typedef struct tSurLineFail
    {
    /* Section */
    tSurFailStat los;       /* Loss Of Signal failure indication */
    tSurFailStat lof;       /* Loss Of Frame failure indication */
    tSurFailStat tims;      /* Section Trace Identifier Mismatch failure 
                               indication */
    /* Line */
    tSurFailStat lineBerSf; /* Line BER-based Signal Failure failure indication */
    tSurFailStat lineBerSd; /* Line BER-based Signal Degrade failure indication */
    tSurFailStat dcc;       /* DCC failure indication */
    tSurFailStat aisl;      /* Alarm Indication Signal failure indication */
    tSurFailStat rfil;      /* Remote Failure Indication */
    }tSurLineFail;

/* Line status */
typedef struct tSurLineStat
    {
    /* Engine state information */
    eSurEngStat state;          /* Engine state */
    dword       start;          /* Time when FM&PM are started */
    dword       stop;           /* Time when FM&PM are stopped */
    
    /* Section defects */
    tSurDefStat los;            /* Loss Of Signal status */
    tSurDefStat lof;            /* Loss Of Frame status */
    tSurDefStat sef;            /* Severely Errored Framing status */
    tSurDefStat tims;           /* Section Trace Identifier Mismatch */
    
    /* Line defects */
    tSurDefStat lineBerSf;      /* Line BER-based Signal Failure */
    tSurDefStat lineBerSd;      /* Line BER-based Signal Degrade */
    tSurDefStat dcc;            /* DCC failure */
    tSurDefStat aisl;           /* Alarm Indication Signal */
    tSurDefStat rdil;           /* Remote Defect Indication */
    
    /* Unavailable status */
    bool        neUatPot;       /* [true/false] Near-End potential for 
                                                entering/exiting unavailable 
                                                time indication. */
    byte        nePotCnt;       /* Near-End potential for entering/exiting 
                                   unavailable time seconds.  */
    bool        neUat;          /* [true/false] Near-End Unavailable status */
    bool        feUatPot;       /* [true/false] Far-End potential for 
                                                entering/exiting unavailable 
                                                time indication. */
    byte        fePotCnt;       /* Far-End potential for entering/exiting 
                                   unavailable time seconds.  */
    bool        feUat;          /* [true/false] Far-End Unavailable status */
    }tSurLineStat;

/* Line information */
typedef struct tSurLineInfo
    {
    tSurLineConf    conf;    /* Line configuration */
    tSurLineMsgInh  msgInh;  /* Line automatic messages inhibition */
    tSurLineStat    status;  /* Line status */
    tSurLineFail    failure; /* Line failure indication */
    tSurLineParm    parm;    /* Line PM parameters */
    tSortList       failHis; /* Line failure history */
    tSortList       tcaHis;  /* Line TCA history */
    }tSurLineInfo;

/* Linear APS failure indication */
typedef struct tSurApslFail
    {
    tSurFailStat kbyteFail; /* Protection Switching Byte failure indication. 
                               This failure is ignored if LTE does not 
                               support Linear APS. */
    tSurFailStat chnMis;    /* APS Channel Mismatch failure indication. This 
                               failure is ignored if LTE does not support 
                               Linear APS. */
    tSurFailStat mdMis;     /* APS Mode Mismatch failure indication. This 
                               failure is ignored if LTE does not support 
                               Linear APS. */
    tSurFailStat feFail;    /* Far-End Protection Line failure indication. 
                               This failure is ignored if LTE does not 
                               support Linear APS. */
    tSurFailStat protFail;  /* APS Protection Switching Failure indication */
    }tSurApslFail;

/* Linear APS status */
typedef struct tSurApslStat
    {
    /* Engine state information */
    eSurEngStat state;        /* Engine state */
    dword       start;        /* Time when FM&PM are started */
    dword       stop;         /* Time when FM&PM are stopped */
    
    /* Linear APS defects */
    tSurDefStat protFail;     /* Protection failure */
    tSurDefStat kbyteFail;    /* Protection Switching Byte defect. This failure 
                                 is ignored if LTE does not support Linear APS. */
    tSurDefStat chnMis;       /* APS Channel Mismatch defect. This defect is 
                                 ignored if LTE does not support Linear APS. */
    tSurDefStat mdMis;        /* APS Mode Mismatch defect. This defect is 
                                 ignored if LTE does not support Linear APS. */
    tSurDefStat feFail;       /* Far-End Protection Line defect. This defect is 
                                 ignored if LTE does not support Linear APS. */
                                 
    /* Switching status */
    byte        curWorkChn;  /* [0..14] ID of the current working channel that 
                                is utilizing the protection line. ID of working
                                channels are [1..14]. The value 0 is used to indicate
                                that the protection line is free. */
    byte        prevWorkChn; /* [0..14] ID of the previous working channel that 
                                was utilizing the protection line. ID of working
                                channels are [1..14]. The value 0 is used to indicate
                                that the current working channel does not preempt
                                any previous working channel. */
    
    }tSurApslStat;

/* Linear APS information */
typedef struct tSurApslInfo
    {
    tSurApslConf    conf;    /* Linear APS configuration */
    tSurApslMsgInh  msgInh;  /* Linear APS automatic messages inhibition */
    tSurApslStat    status;  /* Linear APS status */
    tSurApslFail    failure; /* Linear APS failure indication */
    tSortList       failHis; /* Linear APS failure history */
    }tSurApslInfo;

/* IMA Link PM parameters */
typedef struct tSurImaLinkParm
    {
    /* IMA Link Near-End */
    tPmParm iv;     	/* ICP Violations: count of errored, invalid or missing ICP cells,
						   except during seconds where a SES-IMA or UAS-IMA condition is reported */

    tPmParm oif;		/* Count OIF anomalies, except during SES-IMA or UAS-IMA conditions */

    tPmParm ses;		/* Count of one second intervals containing >= 30% of the ICP cells
						   counted as IV-IMAs, or one or more link defects (e.g.,
						   LOS, OOF/LOF, AIS or LCD), LIF, LODS defects during non-UASIMA
						   condition */
    tPmParm uas;		/* Unavailable seconds at NE: NE unavailability begins at the onset of 10
						   contiguous SES-IMA including the first 10 seconds to enter the UAS-IMA
						   condition, and ends at the onset of 10 contiguous seconds with no SES-IMA,
						   excluding the last 10 seconds to exit the UAS-IMA condition */

    tPmParm uusTx;		/* Tx Unusable seconds: count of Tx Unusable seconds at the Tx NE LSM */
    tPmParm uusRx;		/* Rx Unusable seconds: count of Rx Unusable seconds at the Rx NE LSM */
    tPmParm fcTx;		/* NE Tx link failure count: count of NE Tx link failure alarm condition
						   entrances. The possible NE Tx link failure alarm conditions are:
						   Tx-Mis-Connected and Tx-Fault. */

    tPmParm fcRx;		/* NE Rx link failure count: count of NE Rx link failure alarm condition entrances.
						   The possible NE Rx link failure alarm conditions are: LIF, LODS, Rx-Mis-
						   Connected and Rx-Fault. */

    tPmParm stufTx;		/* Count of Tx stuffing events except during SES-IMA
						   or UAS-IMA conditions */
    tPmParm stufRx;		/* Count of Rx stuffing events except during SES-IMA
						   or UAS-IMA conditions */

    /* IMA Link Far-End */
    tPmParm sesfe;		/* Count of one second intervals containing one or more RDI-IMA
						   defects, except during UAS-IMA-FE conditions. */

    tPmParm uasfe;		/* Unavailable seconds at FE: FE unavailability begins at the onset of 10
						   contiguous SES-IMA-FE including the first 10 seconds to enter the UAS-IMAFE
						   condition, and ends at the onset of 10 contiguous seconds with no SES-IMAFE,
						   excluding the last 10 seconds to exit the UAS-IMA-FE condition */

    tPmParm uusTxfe;	/* Tx Unusable seconds at FE: count seconds with Tx Unusable
						   indications from the Tx FE LSM */

    tPmParm uusRxfe;	/* Rx Unusable seconds at FE: count of seconds with Rx Unusable
						   indications from the Rx FE LSM */

    tPmParm fcTxfe;		/* FE Tx link failure count: count of FE Tx link failure alarm condition
						   entrances. The only possible link failure alarm condition is Tx-Unusable-FE. */

    tPmParm fcRxfe;		/* FE Rx link failure count: count of FE Rx link failure alarm condition
						   entrances. The possible FE Rx link failure alarm conditions are:
						   RFI-IMA, and Rx-Unusable-FE */

    dword	totalIcp;	/* Total ICP cell in second */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    } tSurImaLinkParm;


/* IMA Link status */
typedef struct tSurImaLinkStat
    {
    /* Engine state information */
    eSurEngStat state;       /* Engine state */
    dword       start;       /* Time when FM&PM are started */
    dword       stop;        /* Time when FM&PM are stopped */

    /* Defect */
    tSurDefStat lif;         /* IMA Link Loss of IMA Frame defect */
    tSurDefStat lods;        /* IMA Link Out of Delay Synchronization defect */
    tSurDefStat rdi;         /* IMA Link Remote Defect Indicator */
    tSurDefStat phyLinkDef;	 /* Physical link defect (LOS, OOF, LOF, AIS, LCD),
							    is used internally to calculate PM parameters */

    /* NE status */
    tSurDefStat txLsm;   	 /* IMA Link Tx Link State */
							 /*	000 Not In Group
								001 Unusable No reason given
								010 Unusable Fault (vendor specific)
								011 Unusable Mis-connected
								100 Unusable Inhibited (vendor specific)
								101 Unusable Failed (not currently defined)
								110 Usable
								111 Active */

    tSurDefStat rxLsm;   	 /* IMA Link Rx Link State */
							 /*	000 Not In Group
								001 Unusable No reason given
								010 Unusable Fault (vendor specific)
								011 Unusable Mis-connected
								100 Unusable Inhibited (vendor specific)
								101 Unusable Failed (not currently defined)
								110 Usable
								111 Active */

    /* FE status */
    tSurDefStat txLsmFe;   	 /* IMA Link Tx Link State */
							 /*	000 Not In Group
								001 Unusable No reason given
								010 Unusable Fault (vendor specific)
								011 Unusable Mis-connected
								100 Unusable Inhibited (vendor specific)
								101 Unusable Failed (not currently defined)
								110 Usable
								111 Active */

    tSurDefStat rxLsmFe;   	 /* IMA Link Rx Link State */
							 /*	000 Not In Group
								001 Unusable No reason given
								010 Unusable Fault (vendor specific)
								011 Unusable Mis-connected
								100 Unusable Inhibited (vendor specific)
								101 Unusable Failed (not currently defined)
								110 Usable
								111 Active */

    /* Unavailable status */
    bool        neUatPot;    /* [true/false] Near-End potential for
                                             entering/exiting unavailable
                                             time indication. */
    byte        nePotCnt;    /* Near-End potential for entering/exiting
                                unavailable time seconds.  */
    bool        neUat;       /* [true/false] Near-End Unavailable status */
    bool        feUatPot;    /* [true/false] Far-End potential for
                                             entering/exiting unavailable
                                             time indication. */
    byte        fePotCnt;    /* Far-End potential for entering/exiting
                                unavailable time seconds.  */
    bool        feUat;       /* [true/false] Far-End Unavailable status */

    } tSurImaLinkStat;

/* IMA Link failure indication */
typedef struct tSurImaLinkFail
    {
    tSurFailStat lif;   		/* IMA Link Loss of IMA Frame */
    tSurFailStat lods;  		/* IMA Link Out of Delay Synchronization */
    tSurFailStat rfi;   		/* Persistence of an RDI-IMA defect */
    tSurFailStat txMisConn;   	/* IMA Link Tx mis-connected */
    tSurFailStat rxMisConn;   	/* IMA Link Rx mis-connected */
    tSurFailStat txFault;     	/* IMA Link Tx Fault */
    tSurFailStat rxFault;     	/* IMA Link Rx Fault */
    tSurFailStat txUnusableFe; 	/* IMA Link Tx Unusable FE */
    tSurFailStat rxUnusableFe; 	/* IMA Link Rx Unusable FE */
    } tSurImaLinkFail;

/* IMA Link information */
typedef struct tSurImaLinkInfo
    {
    tSurImaLinkConf   	conf;    /* IMA Link configuration */
    tSurImaLinkMsgInh  	msgInh;  /* IMA Link automatic messages inhibition */
    tSurImaLinkStat    	status;  /* IMA Link status */
    tSurImaLinkFail    	failure; /* IMA Link failure indication */
    tSurImaLinkParm    	parm;    /* IMA Link PM parameters */
    tSortList      		failHis; /* IMA Link failure history */
    tSortList      		tcaHis;  /* IMA Link TCA history */
    } tSurImaLinkInfo;


/* IMA Group PM parameters */
typedef struct tSurImaGrpParm
    {
    /* IMA Group Near-End */
    tPmParm uas;		/* Count of one second intervals where the GTSM is Down */
    tPmParm fc;			/* NE Group Failure count: count of NE group failure condition entrances.
						   The possible NE group failure alarm conditions are: Config-Aborted and
						   Insufficient-Links */

    /* IMA Group Far-End */
    tPmParm fcfe;		/* FE Group Failure count: count of FE group failure condition entrances. The
						   possible FE group failure alarm conditions are: Start-up-FE, Config-Aborted-
						   FE, Insufficient-Links-FE, and Blocked-FE. */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    } tSurImaGrpParm;

/* IMA Group status */
typedef struct tSurImaGrpStat
    {
    /* Engine state information */
    eSurEngStat state;       /* Engine state */
    dword       start;       /* Time when FM&PM are started */
    dword       stop;        /* Time when FM&PM are stopped */

    /* Defect status */
    tSurDefStat timingMismatch; /* IMA Group Timing-Mismatch */

    /* Group State Machine status */
    tSurDefStat gsm;   	 	 /* State of IMA Near-End Group State Machine */
							 /* 0000 = Start-up,
								0001 = Start-up-Ack,
								0010 = Config-Aborted - Unsupported M,
								0011 = Config-Aborted - Incompatible Group Symmetry,
								0100 = Config-Aborted - Unsupported IMA Version,
								0101, 0110 = Reserved for Config-Aborted
								0111 = Config-Aborted - Other reasons,
								1000 = Insufficient-Links,
								1001 = Blocked,
								1010 = Operational */

    tSurDefStat gsmfe;   	 /* State of IMA Far-End Group State Machine */
							 /* 0000 = Start-up,
								0001 = Start-up-Ack,
								0010 = Config-Aborted - Unsupported M,
								0011 = Config-Aborted - Incompatible Group Symmetry,
								0100 = Config-Aborted - Unsupported IMA Version,
								0101, 0110 = Reserved for Config-Aborted
								0111 = Config-Aborted - Other reasons,
								1000 = Insufficient-Links,
								1001 = Blocked,
								1010 = Operational */

    tSurDefStat gtsm;   	 /* State of IMA Near-End Group Traffic State Machine */



    /* Unavailable status */
    bool        neUatPot;    /* [true/false] Near-End potential for
                                             entering/exiting unavailable
                                             time indication. */
    byte        nePotCnt;    /* Near-End potential for entering/exiting
                                unavailable time seconds.  */
    bool        neUat;       /* [true/false] Near-End Unavailable status */
    bool        feUatPot;    /* [true/false] Far-End potential for
                                             entering/exiting unavailable
                                             time indication. */
    byte        fePotCnt;    /* Far-End potential for entering/exiting
                                unavailable time seconds.  */
    bool        feUat;       /* [true/false] Far-End Unavailable status */

    } tSurImaGrpStat;

/* IMA Group failure indication */
typedef struct tSurImaGrpFail
    {
    tSurFailStat startupFe;   	/* IMA Group Start up FE */
    tSurFailStat cfgAborted;   	/* IMA Group Config-aborted */
    tSurFailStat cfgAbortedFe; 	/* IMA Group Config-aborted FE */
    tSurFailStat insuffLink;  	/* IMA Group Insufficient-Links */
    tSurFailStat insuffLinkFe; 	/* IMA Group Insufficient-Links FE */
    tSurFailStat blockedFe; 	/* IMA Group Blocked-FE */
    tSurFailStat timingMismatch; /* IMA Group Timing-Mismatch */
    } tSurImaGrpFail;

/* IMA Group information */
typedef struct tSurImaGrpInfo
    {
    tSurImaGrpConf   	conf;    /* IMA Group configuration */
    tSurImaGrpMsgInh  	msgInh;  /* IMA Group automatic messages inhibition */
    tSurImaGrpStat    	status;  /* IMA Group status */
    tSurImaGrpFail    	failure; /* IMA Group failure indication */
    tSurImaGrpParm    	parm;    /* IMA Group PM parameters */
    tSortList      		failHis; /* IMA Group failure history */
    tSortList      		tcaHis;  /* IMA Group TCA history */
    } tSurImaGrpInfo;

/* PW PM parameters */
typedef struct tSurPwParm
    {
    tPmParm es;		/* Errored Seconds */
    tPmParm ses;	/* Severely Errored Second */
    tPmParm uas;	/* Unavailable Second */

	tPmParm	txPkt;					/* Transmitted packet */
	tPmParm	rxPkt;					/* Received packet */
	tPmParm	missingPkt;				/* Missing packet */
	tPmParm	misorderedPkt;			/* Mis-ordered packet, and successfully re-ordered */
	tPmParm	misorderedPktDropped;	/* Mis-ordered packet dropped */
	tPmParm	playoutJitBufOverrun;	/* Play-out buffer overrun */
	tPmParm	playoutJitBufUnderrun;	/* Play-out buffer underrun*/
	tPmParm	lops;					/* Loss of Packet Synchronization */
	tPmParm	malformedPkt;			/* Malformed packet */
	tPmParm	strayPkt;				/* Stray packet */
	tPmParm	remotePktLoss;			/* Remote packet loss */
	tPmParm	txLbitPkt;				/* Transmitted L-Bit packet */

    dword	numErrDataBlock;	/* Number of data blocks. A Errored data block is defined as a block
								   of data played out to the TDM attachment circuit (RFC4553, RFC5086) */
    dword	numPkt;				/* Number of packet */

    dword	prePerStartTime;	/* Start time of previous period */
    dword	prePerEndTime;		/* End time of previous period */
    dword	curPerStartTime;	/* Start time of current period */

    dword	preDayStartTime;	/* Start time of previous day */
    dword	preDayEndTime;		/* End time of previous day */
    dword	curDayStartTime;	/* Start time of current day */

    tSurTime	curEngTime;			/* Current engine time of PM engine */
    } tSurPwParm;

/* PW status */
typedef struct tSurPwStat
    {
    /* Engine state information */
    eSurEngStat state;       /* Engine state */
    dword       start;       /* Time when FM&PM are started */
    dword       stop;        /* Time when FM&PM are stopped */

    /* Defect status */
    tSurDefStat strayPkt;          /* PW Stray packet */
    tSurDefStat malformedPkt;      /* PW Malformed Packet */
    tSurDefStat excPktLossRate;    /* PW Excessive packet loss rate */
    tSurDefStat jitBufOverrun;     /* PW Jitter Buffer overrun */
    tSurDefStat remotePktLoss;     /* PW Remote packet loss */
    tSurDefStat lofs;     		   /* PW Loss of Frame State (MEF8)
									or Loss of Packet Synchronization (RFC4842) */

    /* Unavailable status */
    bool        neUatPot;    /* [true/false] Near-End potential for
                                             entering/exiting unavailable
                                             time indication. */
    byte        nePotCnt;    /* Near-End potential for entering/exiting
                                unavailable time seconds.  */
    bool        neUat;       /* [true/false] Near-End Unavailable status */
    bool        feUatPot;    /* [true/false] Far-End potential for
                                             entering/exiting unavailable
                                             time indication. */
    byte        fePotCnt;    /* Far-End potential for entering/exiting
                                unavailable time seconds.  */
    bool        feUat;       /* [true/false] Far-End Unavailable status */

    } tSurPwStat;

/* PW failure indication */
typedef struct tSurPwFail
    {
    tSurFailStat strayPkt;          /* PW Stray packet */
    tSurFailStat malformedPkt;      /* PW Malformed Packet */
    tSurFailStat excPktLossRate;    /* PW Excessive packet loss rate */
    tSurFailStat jitBufOverrun;     /* PW Buffer overrun */
    tSurFailStat remotePktLoss;     /* PW Remote packet loss */
    tSurFailStat lofs;     			/* PW Loss of Frame State (MEF8)
									or Loss of Packet Synchronization (RFC4842) */
    } tSurPwFail;

/* PW information */
typedef struct tSurPwInfo
    {
    tSurPwConf   	conf;    /* PW configuration */
    tSurPwMsgInh  	msgInh;  /* PW automatic messages inhibition */
    tSurPwStat    	status;  /* PW status */
    tSurPwFail    	failure; /* PW failure indication */
    tSurPwParm    	parm;    /* PW PM parameters */
    tSortList      		failHis; /* PW failure history */
    tSortList      		tcaHis;  /* PW TCA history */
    } tSurPwInfo;

/* Trouble profile */
typedef struct tSurTrblProf
    {
    /* Linear APS */
    tSurTrblDesgn apsKbyteFail; /* Trouble designation of APS K-byte failure */
    tSurTrblDesgn apsChnMis;    /* Trouble designation of APS Channel Mismatch 
                                   failure */
    tSurTrblDesgn apsMdMis;     /* Trouble designation of APS Mode Mismatch 
                                   failure */
    tSurTrblDesgn apsFeFail;    /* Trouble designation of APS Far-End failure */
    tSurTrblDesgn apsSw;        /* Trouble designation of APS switching */
    tSurTrblDesgn apsFail;      /* Trouble designation of APS protection failure */
    tSurTrblDesgn apsInhibit;   /* Trouble designation of APS protection inhibition */
    
    /* Section */
    tSurTrblDesgn los;          /* Trouble designation of LOS failure */
    tSurTrblDesgn lof;          /* Trouble designation of LOF failure */
    tSurTrblDesgn tims;         /* Trouble designation of TIM-S failure */
    
    /* Line */
    tSurTrblDesgn lineBerSf;    /* Trouble designation of Line BER-SF failure */
    tSurTrblDesgn lineBerSd;    /* Trouble designation of Line BER-SD failure */
    tSurTrblDesgn dcc;          /* Trouble designation of DCC failure */
    tSurTrblDesgn aisl;         /* Trouble designation of AIS-L failure */
    tSurTrblDesgn rfil;         /* Trouble designation of RFI-L failure */
    
    /* STS Path */              
    tSurTrblDesgn lopp;         /* Trouble designation of LOP-P */
    tSurTrblDesgn plmp;         /* Trouble designation of PLM-P */
    tSurTrblDesgn uneqp;        /* Trouble designation of UNEQ-P */
    tSurTrblDesgn timp;         /* Trouble designation of TIM-P */
    tSurTrblDesgn aisp;         /* Trouble designation of AIS-P */
    tSurTrblDesgn rfip;         /* Trouble designation of RFI-P */
    tSurTrblDesgn erfipS;       /* Trouble designation of STS Path Server 
                                   Enhanced Remote Failure Indication. */
    tSurTrblDesgn erfipC;       /* Trouble designation of STS Path Connectivity 
                                   Enhanced Remote Failure Indication. */
    tSurTrblDesgn erfipP;       /* Trouble designation of STS Path Payload 
                                   Enhanced Remote Failure Indication */
    tSurTrblDesgn pathBerSf;    /* Trouble designation of STS Path BER-SF */
    tSurTrblDesgn pathBerSd;    /* Trouble designation of STS Path BER-SD */
    
    /* VT Path */
    tSurTrblDesgn lopv;         /* Trouble designation of LOP-V */
    tSurTrblDesgn plmv;         /* Trouble designation of PLM-V */
    tSurTrblDesgn uneqv;        /* Trouble designation of UNEQ-V */
    tSurTrblDesgn timv;         /* Trouble designation of TIM-V */
    tSurTrblDesgn aisv;         /* Trouble designation of AIS-V */
    tSurTrblDesgn rfiv;         /* Trouble designation of RFI-V */
    tSurTrblDesgn erfivS;       /* VT Path Server Enhanced Remote Failure 
                                   Indication. */
    tSurTrblDesgn erfivC;       /* VT Path Connectivity Enhanced Remote 
                                   Failure Indication. */
    tSurTrblDesgn erfivP;       /* VT Path Payload Enhanced Remote Failure 
                                   Indication */
    tSurTrblDesgn vtBerSf;      /* Trouble designation of VT Path BER-SF */
    tSurTrblDesgn vtBerSd;      /* Trouble designation of VT Path BER-SD */
    
    /* DS3/E3 */
    tSurTrblDesgn de3Los;       /* Trouble designation of DS3/E3 LOS */
    tSurTrblDesgn de3Lof;       /* Trouble designation of DS3/E3 LOF */
    tSurTrblDesgn de3Ais;       /* Trouble designation of DS3/E3 AIS */
    tSurTrblDesgn de3Rai;       /* Trouble designation of DS3/E3 RAI */
    
    /* DS1/E1 */
    tSurTrblDesgn de1Los;       /* Trouble designation of DS1/E1 LOS */
    tSurTrblDesgn de1Lof;       /* Trouble designation of DS1/E1 LOF */
    tSurTrblDesgn de1Ais;       /* Trouble designation of DS1/E1 AIS */
    tSurTrblDesgn de1AisCi;     /* Trouble designation of DS1/E1 AIS-CI */
    tSurTrblDesgn de1Rai;       /* Trouble designation of DS1/E1 RAI */
    tSurTrblDesgn de1RaiCi;     /* Trouble designation of DS1/E1 RAI-CI */
    
    /* PW */
    tSurTrblDesgn lBit;     	/* Trouble designation of L bit error */
    tSurTrblDesgn rBit;       	/* Trouble designation of R bit error */
    tSurTrblDesgn mBit;     	/* Trouble designation of M bit error */

    /* IMA Link */
    tSurTrblDesgn imaLinkLif;   /* Trouble designation of IMA Link Loss of IMA Frame */
    tSurTrblDesgn imaLinkLods;  /* Trouble designation of IMA Link Out of Delay Synchronization */
    tSurTrblDesgn imaLinkRfi;   /* Trouble designation of IMA Link Remote Failure Indicator */
    tSurTrblDesgn imaLinkTxMisConn;   	/* IMA Link Tx mis-connected */
    tSurTrblDesgn imaLinkRxMisConn;   	/* IMA Link Rx mis-connected */
    tSurTrblDesgn imaLinkTxFault;     	/* IMA Link Tx Fault */
    tSurTrblDesgn imaLinkRxFault;     	/* IMA Link Rx Fault */
    tSurTrblDesgn imaLinkTxUnusableFe; 	/* IMA Link Tx Unusable FE */
    tSurTrblDesgn imaLinkRxUnusableFe; 	/* IMA Link Rx Unusable FE */

    /* IMA Group */
    tSurTrblDesgn imaGrpStartupFe;   	/* IMA Group Start up FE */
    tSurTrblDesgn imaGrpCfgAborted;   	/* IMA Group Config aborted */
    tSurTrblDesgn imaGrpCfgAbortedFe; 	/* IMA Group Config aborted FE */
    tSurTrblDesgn imaGrpInsuffLink;  	/* IMA Group Insufficient-Links */
    tSurTrblDesgn imaGrpInsuffLinkFe; 	/* IMA Group Insufficient-Links FE */
    tSurTrblDesgn imaGrpBlockedFe; 		/* IMA Group Blocked-FE */
    tSurTrblDesgn imaGrpTimingMismatch; /* IMA Group Timing-Mismatch */

    /* PW */
    tSurTrblDesgn pwStrayPkt;          /* PW Stray packet */
    tSurTrblDesgn pwMalformedPkt;      /* PW Malformed Packet */
    tSurTrblDesgn pwExcPktLossRate;    /* PW Excessive packet loss rate */
    tSurTrblDesgn pwJitBufOverrun;     /* PW Buffer overrun */
    tSurTrblDesgn pwRemotePktLoss;     /* PW Remote packet loss */
    tSurTrblDesgn pwLofs;     		   /* PW LOFS */

    /* Performance Monitoring */
    tSurTrblDesgn tca;          /* Threshold Crossing Alert */
    }tSurTrblProf;

/* Device master configuration */
typedef struct tSurMastConf
    {
    tSurTrblProf  troubleProf; /* Trouble profile */
    tSurColorProf colorProf;   /* Color profile */
    tSurKThres    kThres;      /* K threshold (BIP threshold) for NE to enter 
                                  Severely Errored Second at Line, STS Path, VT 
                                  Path */
    eSurDefServMd defServMd;   /* Defect service mode */

    dword 		  decSoakTime;	/* Soaking time to declare the failure */
    dword 		  terSoakTime;	/* Soaking time to terminate the failure */
    }tSurMastConf;

/* STS database */
typedef struct tSurStsDb
    {
    tSurStsInfo *pInfo;                    /* STS information */
    tSurVtInfo  *ppVtInfo[cSurMaxVtInSts]; /* VT information */
    tSurTu3Info *pTu3Info;                 /* TU3 information */
    }tSurStsDb;

/* Line database */
typedef struct tSurLineDb
    {
    tSurLineInfo   *pInfo;     /* Line information */
    tSurStsDb     **ppStsDb;   /* STS DB of this line */
    word            numSts;    /* Number STSs in line */
    }tSurLineDb;

/* Line database */
typedef struct tSurDe3Db
    {
    tSurDe3Info *pInfo;      /* DS3/E3 information */
    tSurDe1Info *pDe1Db[cSurMaxDe3Line * cSurMaxVtInSts]; /* DS1/E1 of this line */
    }tSurDe3Db;

/* All channels database */
typedef struct tChnDb
    {
    tSurLineDb   *pLine[cSurMaxLine];    /* SONET/SDH lines database */
    tSurDe3Db    *pDe3[cSurMaxDe3Line];  /* DS3/E3 lines database */
    tSurApslInfo *pApsl[cSurMaxApslEng]; /* Linear APS engine database */
    tSurImaLinkInfo	*pImaLink[cSurMaxImaLink];	/* IMA link database */
    tSurImaGrpInfo	*pImaGrp[cSurMaxImaGrp];	/* IMA Group database */
    tSurPwInfo		*pPw[cSurMaxPw];			/* PW database */
    }tChnDb;

/* Defect queue structure */
typedef struct tSurDefQue
    {
    tLinkQueue apsl; /* Linear APS defect queue */
    tLinkQueue line; /* Line defect queue */
    tLinkQueue sts;  /* STS defect queue */
    tLinkQueue vt;   /* VT defect queue */
    tLinkQueue tu3;  /* TU3 defect queue */
    tLinkQueue de3;  /* DS3/E3 defect queue */
    tLinkQueue de1;  /* DS1/E1 defect queue */
    tLinkQueue imaLink;  /* IMA Link defect queue */
    tLinkQueue imaGrp;   /* IMA Group defect queue */
    tLinkQueue pw;   	 /* PW defect queue */
    }tSurDefQue;

/* Device database. This structure stores database for each device, include 
   master configuration and channel database */
typedef struct tSurDevDb
    {
    tSurMastConf mastConf; /* Master configuration */
    tChnDb       chnDb;    /* Database of all channels */
    tSurDefQue  *pDefQue;  /* Defect queue. This queue will be NULL when module 
                              runs in polling mode. */
    /* Engine state */
    eSurEngStat  state;    /* Engine state */
    dword        time;     /* Time when engine state changed */
    }tSurDevDb;

/* Surveillance handle. This structure stores information of each device, 
   includes device handle and its database. */
typedef struct tSurInfo
    {
    tSurDevHdl *pHdl;                  /* Device handle */
    tSurDevDb  *pDb;                   /* Device database */
    osalSemKey  sem;                   /* Semaphore */
    char        desBuf[cSurDevDesLen]; /* Description buffer */
    
    tSurDevIntf interface;             /* Device interface */
    }tSurInfo;

/* Surveillance Database. This structure stores databases of all devices that 
   this module can support. */
typedef struct tSurDb
    {
    tSurDev     devices[cSurMaxDev]; /* Array of devices */ 
    osalSemKey  sem;                 /* Semaphore */
    }tSurDb;

/*--------------------------- Entries ----------------------------------------*/
/* Database */
extern tSurDb surDb;

/*------------------------------------------------------------------------------
 * GENERAL INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to compare two failures
------------------------------------------------------------------------------*/
extern eListCmpResult SurFailCmp(void *pFail1, void *pFail2);

/*------------------------------------------------------------------------------
This function is used to compare two TCAs
------------------------------------------------------------------------------*/
extern eListCmpResult SurTcaCmp(void *pTca1, void *pTca2);

/*------------------------------------------------------------------------------
This function is used to clear failure history of a channel
------------------------------------------------------------------------------*/
extern eSurRet FailHisClear(tSortList pFailHis, bool flush);

/*------------------------------------------------------------------------------
This function is used to get a  pointer to a register.
------------------------------------------------------------------------------*/
extern  eSurRet RegGet(tPmParm *pParm, ePmRegType regType, tPmReg **ppReg);

/*------------------------------------------------------------------------------
This function is used to get reset register(s) of a parameters.
------------------------------------------------------------------------------*/
extern  eSurRet RegReset(tPmParm *pParm, ePmRegType regType);

/*------------------------------------------------------------------------------
 * LINEAR APS INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
                                   
/*------------------------------------------------------------------------------
This function is used to provision a Linear APS engine for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurApslProv(tSurDev device, tSurApslId *pApslId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one Linear APS engine. It will 
deallocated all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurApslDeProv(tSurDev device, tSurApslId *pApslId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a linear 
APS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurApslTrblNotfInhSet(tSurDev           device,
                                     const tSurApslId *pChnId, 
                                     eSurApslTrbl      trblType, 
                                     bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition for a linear
APS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurApslTrblNotfInhGet(tSurDev           device,
                                     const tSurApslId *pChnId, 
                                     eSurApslTrbl      trblType, 
                                     bool             *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get linear APS channel's trouble notification 
inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurApslChnTrblNotfInhInfoGet(tSurDev              device, 
                                            const tSurApslId    *pChnId, 
                                            tSurApslMsgInh      *pTrblInh);
                                                                                
/*------------------------------------------------------------------------------
This function is used to set linear APS trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurApslTrblDesgnSet(tSurDev              device, 
                                   eSurApslTrbl         trblType, 
                                   const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get linear APS trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurApslTrblDesgnGet(tSurDev        device, 
                                   eSurApslTrbl   trblType, 
                                   tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a linear APS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurApslEngStatSet(tSurDev             device, 
                                 const tSurApslId   *pChnId, 
                                 eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a linear APS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurApslEngStatGet(tSurDev             device, 
                                 const tSurApslId   *pChnId, 
                                 eSurEngStat        *pStat);
                                                      
/*------------------------------------------------------------------------------
This internal API is used to set linear APS channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurApslChnCfgSet(tSurDev             device, 
                                const tSurApslId   *pChnId, 
                                const tSurApslConf *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to get linear APS channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurApslChnCfgGet(tSurDev           device,
                                const tSurApslId *pChnId, 
                                tSurApslConf     *pChnCfg);  

/*------------------------------------------------------------------------------
This internal API is used to get a linear APS channel's specific failure's 
status.
------------------------------------------------------------------------------*/
extern eSurRet FmApslFailIndGet(tSurDev             device, 
                                const tSurApslId   *pChnId, 
                                eSurApslTrbl        trblType, 
                                tSurFailStat       *pFailStat);
  
/*------------------------------------------------------------------------------
This internal API is used to get a linear APS channel's failure history.
------------------------------------------------------------------------------*/ 
friend tSortList FmApslFailHisGet(tSurDev           device,
                                  const tSurApslId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a linear APS channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmApslFailHisClear(tSurDev             device, 
                                  const tSurApslId   *pChnId,
                                  bool                flush);
           
/*------------------------------------------------------------------------------
 * LINE INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
This function is used to get a pointer to a parameter.
------------------------------------------------------------------------------*/
extern eSurRet LineParmGet(tSurLineParm *pLineParm, ePmLineParm parm, tPmParm **ppParm);

/*------------------------------------------------------------------------------
This function is used to reset registers of a group of line parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllLineParmRegReset(tSurLineParm *pLineParm, 
                                   ePmRegType    regType,
                                   byte          lineParmType);
                                   
/*------------------------------------------------------------------------------
This function is used to provision a line for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurLineProv(tSurDev device, tSurLineId *pLineId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one line. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurLineDeProv(tSurDev device, tSurLineId *pLineId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a line 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurLineTrblNotfInhSet(tSurDev           device,
                                     const tSurLineId *pChnId, 
                                     eSurLineTrbl      trblType, 
                                     bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition for a line 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurLineTrblNotfInhGet(tSurDev           device,
                                     const tSurLineId *pChnId, 
                                     eSurLineTrbl      trblType, 
                                     bool             *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get line channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurLineChnTrblNotfInhInfoGet(tSurDev             device, 
                                            const tSurLineId   *pChnId, 
                                            tSurLineMsgInh     *pTrblInh);
                                                                                 
/*------------------------------------------------------------------------------
This internal API is used to set line trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurLineTrblDesgnSet(tSurDev              device, 
                                   eSurLineTrbl         trblType, 
                                   const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to get line trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurLineTrblDesgnGet(tSurDev        device, 
                                   eSurLineTrbl   trblType, 
                                   tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a line channel.
------------------------------------------------------------------------------*/
extern eSurRet SurLineEngStatSet(tSurDev             device, 
                                 const tSurLineId   *pChnId, 
                                 eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a line channel.
------------------------------------------------------------------------------*/
extern eSurRet SurLineEngStatGet(tSurDev             device, 
                                 const tSurLineId   *pChnId, 
                                 eSurEngStat        *pStat);
                                 
/*------------------------------------------------------------------------------
This internal API is used to set line channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurLineChnCfgSet(tSurDev             device, 
                                const tSurLineId   *pChnId, 
                                const tSurLineConf *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to get line channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurLineChnCfgGet(tSurDev           device,
                                const tSurLineId *pChnId, 
                                tSurLineConf     *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to get a line channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmLineFailIndGet(tSurDev             device, 
                                const tSurLineId   *pChnId, 
                                eSurLineTrbl        trblType, 
                                tSurFailStat       *pFailStat);
                                       
                                       
/*------------------------------------------------------------------------------
This internal API is used to get a line channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmLineFailHisGet(tSurDev           device,
                                        const tSurLineId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a line channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmLineFailHisClear(tSurDev             device, 
                                  const tSurLineId   *pChnId,
                                  bool                flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a line channel's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmLineRegReset(tSurDev       device, 
                              tSurLineId   *pChnId, 
                              ePmLineParm   parm, 
                              ePmRegType    regType);
                              
/*------------------------------------------------------------------------------
This internal API is used to get a line channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmLineRegGet(tSurDev             device, 
                            const tSurLineId   *pChnId, 
                            ePmLineParm         parm, 
                            ePmRegType          regType, 
                            tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a line channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmLineTcaHisGet(tSurDev device, const tSurLineId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a line channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmLineTcaHisClear(tSurDev device, const tSurLineId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a line channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmLineParmAccInhSet(tSurDev           device, 
                                   const tSurLineId *pChnId, 
                                   ePmLineParm       parm, 
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a line channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmLineParmAccInhGet(tSurDev           device, 
                                   const tSurLineId *pChnId, 
                                   ePmLineParm       parm, 
                                   bool             *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for a line channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmLineRegThresSet(tSurDev            device, 
                                 const tSurLineId  *pChnId, 
                                 ePmLineParm        parm, 
                                 ePmRegType         regType, 
                                 dword              thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of a line channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmLineRegThresGet(tSurDev            device, 
                                 const tSurLineId  *pChnId, 
                                 ePmLineParm        parm, 
                                 ePmRegType         regType, 
                                 dword             *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a line channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmLineParmNumRecRegSet(tSurDev           device, 
                                      const tSurLineId *pChnId, 
                                      ePmLineParm       parm, 
                                      byte              numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a line channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmLineParmNumRecRegGet(tSurDev           device, 
                                      const tSurLineId *pChnId, 
                                      ePmLineParm       parm, 
                                      byte             *pNumRecReg);
                                                                                                    
/*------------------------------------------------------------------------------
 * STS INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to get a pointer to a STS parameter.
------------------------------------------------------------------------------*/
extern eSurRet StsParmGet(tSurStsParm *pStsParm, ePmStsParm parm, tPmParm **ppParm);

/*------------------------------------------------------------------------------
This function is used to reset registers of a group of STS parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllStsParmRegReset(tSurStsParm  *pStsParm, 
                                  ePmRegType    regType,
                                  bool          neParm);

/*------------------------------------------------------------------------------
This functions is used to set accumulate inhibition for a group of STS parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllStsParmAccInhSet(tSurStsParm  *pStsParm, 
                                    bool         inhibit, 
                                    bool         neParm);

/*------------------------------------------------------------------------------
This functions is used to set register threshold for a group of STS parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllStsParmRegThresSet(tSurStsParm  *pStsParm, 
                                     ePmRegType    regType,
                                     dword         thresVal, 
                                     bool          neParm);                                      

/*------------------------------------------------------------------------------
This functions is used to set number of recent registers for a group of STS 
parameters.
------------------------------------------------------------------------------*/
friend eSurRet AllStsParmNumRecRegSet(tSurStsParm  *pStsParm, 
                                      byte          numRecReg, 
                                      bool          neParm);
                                                                        
/*------------------------------------------------------------------------------
This internal API is used to provision a STS for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurStsProv(tSurDev device, tSurStsId *pStsId);

/*------------------------------------------------------------------------------
This internal API is used to de-provision FM & PM of one STS. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurStsDeProv(tSurDev device, tSurStsId *pStsId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a STS 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurStsTrblNotfInhSet(tSurDev           device,
                                    const tSurStsId  *pChnId, 
                                    eSurStsTrbl       trblType, 
                                    bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a STS 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurStsTrblNotfInhGet(tSurDev          device,
                                    const tSurStsId *pChnId, 
                                    eSurStsTrbl      trblType, 
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get STS channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurStsChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurStsId    *pChnId, 
                                           tSurStsMsgInh      *pTrblInh);
                                            
/*------------------------------------------------------------------------------
This function is used to set STS path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurStsTrblDesgnSet(tSurDev              device, 
                                  eSurStsTrbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get STS path trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurStsTrblDesgnGet(tSurDev        device, 
                                  eSurStsTrbl    trblType, 
                                  tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a STS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurStsEngStatSet(tSurDev             device, 
                                const tSurStsId    *pChnId, 
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a STS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurStsEngStatGet(tSurDev             device, 
                                const tSurStsId    *pChnId, 
                                eSurEngStat        *pStat);
                                 
/*------------------------------------------------------------------------------
This internal API is used to set STS channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurStsChnCfgSet(tSurDev            device,
                               const tSurStsId   *pChnId, 
                               const tSurStsConf *pChnCfg);
                               
/*------------------------------------------------------------------------------
This internal API is used to get STS channel's configuration
------------------------------------------------------------------------------*/                        
extern eSurRet SurStsChnCfgGet(tSurDev          device,
                               const tSurStsId *pChnId, 
                               tSurStsConf     *pChnCfg);
                   
/*------------------------------------------------------------------------------
This internal API is used to get a STS channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmStsFailIndGet(tSurDev            device, 
                               const tSurStsId   *pChnId, 
                               eSurStsTrbl        trblType, 
                               tSurFailStat      *pFailStat);

/*------------------------------------------------------------------------------
This internal API is used to get a STS channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmStsFailHisGet(tSurDev          device, 
                                 const tSurStsId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a STS channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmStsFailHisClear(tSurDev            device, 
                                 const tSurStsId   *pChnId,
                                 bool               flush);
                              
/*------------------------------------------------------------------------------
This internal API is used to reset a STS channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmStsRegReset(tSurDev       device, 
                             tSurStsId    *pChnId, 
                             ePmStsParm    parm, 
                             ePmRegType    regType);

/*------------------------------------------------------------------------------
This internal API is used to get a STS channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmStsRegGet(tSurDev             device, 
                           const tSurStsId    *pChnId, 
                           ePmStsParm          parm, 
                           ePmRegType          regType, 
                           tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a STS channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmStsTcaHisGet(tSurDev device, const tSurStsId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a STS channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmStsTcaHisClear(tSurDev device, const tSurStsId *pChnId);
                       
/*------------------------------------------------------------------------------
This internal API is used to enable/disable a STS channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmStsParmAccInhSet(tSurDev          device, 
                                  const tSurStsId *pChnId, 
                                  ePmStsParm       parm, 
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a STS channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmStsParmAccInhGet(tSurDev          device, 
                                  const tSurStsId *pChnId, 
                                  ePmStsParm       parm, 
                                  bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for a STS channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmStsRegThresSet(tSurDev           device, 
                                const tSurStsId  *pChnId, 
                                ePmStsParm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of a STS channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmStsRegThresGet(tSurDev           device, 
                                const tSurStsId  *pChnId, 
                                ePmStsParm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a STS channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmStsParmNumRecRegSet(tSurDev          device, 
                                     const tSurStsId *pChnId, 
                                     ePmStsParm       parm, 
                                     byte             numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a STS channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmStsParmNumRecRegGet(tSurDev          device, 
                                     const tSurStsId *pChnId, 
                                     ePmStsParm       parm, 
                                     byte            *pNumRecReg);
                                                                                                                                                                                                                                                                                                                                                                                                           
/*------------------------------------------------------------------------------
 * AU-n INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to convert an AU-n to STS
------------------------------------------------------------------------------*/
extern eSurRet SurAu2Sts(const tSurAuId *pAuId, 
                         tSurStsId      *pStsId, 
                         eSurStsRate    *pRate);
                         
/*------------------------------------------------------------------------------
This function is used to get flat number of a AU-n
------------------------------------------------------------------------------*/
extern word SurAuIdFlat(const tSurAuId *pAuId);

/*------------------------------------------------------------------------------
This function is used to provision a AU-n for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurAuProv(tSurDev device, tSurAuId *pAuId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one AU-n. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurAuDeProv(tSurDev device, tSurAuId *pAuId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for an AU-n 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurAuTrblNotfInhSet(tSurDev           device,
                                   const tSurAuId   *pChnId, 
                                   eSurAuTrbl        trblType, 
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of an AU-n 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurAuTrblNotfInhGet(tSurDev          device,
                                   const tSurAuId  *pChnId, 
                                   eSurAuTrbl       trblType, 
                                   bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get AU-n channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurAuChnTrblNotfInhInfoGet(tSurDev             device, 
                                          const tSurAuId     *pChnId, 
                                          tSurStsMsgInh      *pTrblInh);
                                                       
/*------------------------------------------------------------------------------
This function is used to set AU-n path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurAuTrblDesgnSet(tSurDev              device, 
                                 eSurAuTrbl           trblType, 
                                 const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get AU-n path trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurAuTrblDesgnGet(tSurDev        device, 
                                 eSurAuTrbl     trblType, 
                                 tSurTrblDesgn *pTrblDesgn);                                                                     

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a AU-n channel.
------------------------------------------------------------------------------*/
extern eSurRet SurAuEngStatSet(tSurDev             device, 
                               const tSurAuId     *pChnId, 
                               eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a AU-n channel.
------------------------------------------------------------------------------*/
extern eSurRet SurAuEngStatGet(tSurDev             device, 
                               const tSurAuId     *pChnId, 
                               eSurEngStat        *pStat);
                                                                                             
/*------------------------------------------------------------------------------
This internal API is used to set STS channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurAuChnCfgSet(tSurDev            device,
                              const tSurAuId    *pChnId, 
                              const tSurStsConf *pChnCfg);
                               
/*------------------------------------------------------------------------------
This internal API is used to get AU-n channel's configuration
------------------------------------------------------------------------------*/                        
extern eSurRet SurAuChnCfgGet(tSurDev         device,
                              const tSurAuId *pChnId, 
                              tSurStsConf    *pChnCfg);
               
/*------------------------------------------------------------------------------
This internal API is used to get an AU-n channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmAuFailIndGet(tSurDev           device, 
                              const tSurAuId   *pChnId, 
                              eSurAuTrbl        trblType, 
                              tSurFailStat     *pFailStat);
                  
/*------------------------------------------------------------------------------
This internal API is used to get a AU-n channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmAuFailHisGet(tSurDev         device, 
                                const tSurAuId *pChnId);                  

/*------------------------------------------------------------------------------
This internal API is used to clear a AU-n channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmAuFailHisClear(tSurDev           device, 
                                const tSurAuId   *pChnId,
                                bool              flush);
                         
/*------------------------------------------------------------------------------
This internal API is used to reset a AU-n channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmAuRegReset(tSurDev       device, 
                            tSurAuId     *pChnId, 
                            ePmAuParm     parm, 
                            ePmRegType    regType);

/*------------------------------------------------------------------------------
This internal API is used to get a AU-n channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmAuRegGet(tSurDev             device, 
                          const tSurAuId     *pChnId, 
                          ePmAuParm           parm, 
                          ePmRegType          regType, 
                          tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a AU-n channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmAuTcaHisGet(tSurDev device, const tSurAuId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear an AU-n channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmAuTcaHisClear(tSurDev device, const tSurAuId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable an AU-n channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmAuParmAccInhSet(tSurDev          device, 
                                 const tSurAuId  *pChnId, 
                                 ePmAuParm        parm, 
                                 bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get an AU-n channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmAuParmAccInhGet(tSurDev          device, 
                                 const tSurAuId  *pChnId, 
                                 ePmAuParm        parm, 
                                 bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an AU-n channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmAuRegThresSet(tSurDev           device, 
                               const tSurAuId   *pChnId, 
                               ePmAuParm         parm, 
                               ePmRegType        regType, 
                               dword             thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of an AU-n channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmAuRegThresGet(tSurDev           device, 
                               const tSurAuId   *pChnId, 
                               ePmAuParm         parm, 
                               ePmRegType        regType, 
                               dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set an AU-n channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmAuParmNumRecRegSet(tSurDev          device, 
                                    const tSurAuId  *pChnId, 
                                    ePmAuParm        parm, 
                                    byte             numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get an AU-n channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmAuParmNumRecRegGet(tSurDev          device, 
                                    const tSurAuId  *pChnId, 
                                    ePmAuParm        parm, 
                                    byte            *pNumRecReg);
                                                                                                                                                                                                                                                                                                                                                                          
/*------------------------------------------------------------------------------
 * TU-3 INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to convert STS to TU3
------------------------------------------------------------------------------*/
extern eSurRet SurTu3FromStsGet(const tSurStsId *pStsId, tSurTu3Id *pTu3Id);

/*------------------------------------------------------------------------------
This function is used to get flat number of a TU-3
------------------------------------------------------------------------------*/
extern word SurTu3IdFlat(const tSurTu3Id *pTu3Id);

/*------------------------------------------------------------------------------
This function is used to provision a TU-3 for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3Prov(tSurDev device, tSurTu3Id *pTu3Id);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one TU-3. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3DeProv(tSurDev device, tSurTu3Id *pTu3Id);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a TU-3
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3TrblNotfInhSet(tSurDev           device,
                                    const tSurTu3Id  *pChnId, 
                                    eSurTu3Trbl       trblType, 
                                    bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a TU-3
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3TrblNotfInhGet(tSurDev          device,
                                    const tSurTu3Id *pChnId, 
                                    eSurTu3Trbl      trblType, 
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get TU-3 channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3ChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurTu3Id    *pChnId, 
                                           tSurStsMsgInh      *pTrblInh);
                                                                               
/*------------------------------------------------------------------------------
This function is used to set TU-3 path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3TrblDesgnSet(tSurDev              device, 
                                  eSurTu3Trbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get TU-3 path trouble designation of a device.
------------------------------------------------------------------------------*/
friend eSurRet SurTu3TrblDesgnGet(tSurDev        device, 
                                  eSurTu3Trbl    trblType, 
                                  tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a TU-3 channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3EngStatSet(tSurDev             device, 
                                const tSurTu3Id    *pChnId, 
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a TU-3 channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3EngStatGet(tSurDev             device, 
                                const tSurTu3Id    *pChnId, 
                                eSurEngStat        *pStat);
                                                        
/*------------------------------------------------------------------------------
This internal API is used to set TU-3 channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurTu3ChnCfgSet(tSurDev           device,
                              const tSurTu3Id   *pChnId, 
                              const tSurStsConf *pChnCfg);
                               
/*------------------------------------------------------------------------------
This internal API is used to get TU-3 channel's configuration
------------------------------------------------------------------------------*/                        
extern eSurRet SurTu3ChnCfgGet(tSurDev         device,
                              const tSurTu3Id *pChnId, 
                              tSurStsConf     *pChnCfg);
          
/*------------------------------------------------------------------------------
This internal API is used to get TU-3 channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmTu3FailIndGet(tSurDev           device, 
                               const tSurTu3Id  *pChnId, 
                               eSurTu3Trbl       trblType, 
                               tSurFailStat     *pFailStat);
                           
/*------------------------------------------------------------------------------
This internal API is used to get a TU-3 channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmTu3FailHisGet(tSurDev         device, 
                                 const tSurTu3Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a TU-3 channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmTu3FailHisClear(tSurDev           device, 
                                 const tSurTu3Id  *pChnId,
                                 bool              flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a TU-3 channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmTu3RegReset(tSurDev       device, 
                             tSurTu3Id    *pChnId, 
                             ePmTu3Parm    parm, 
                             ePmRegType    regType);

/*------------------------------------------------------------------------------
This internal API is used to get a TU-3 channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmTu3RegGet(tSurDev             device, 
                           const tSurTu3Id    *pChnId, 
                           ePmTu3Parm          parm, 
                           ePmRegType          regType, 
                           tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a TU-3 channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmTu3TcaHisGet(tSurDev device, const tSurTu3Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a TU-3 channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmTu3TcaHisClear(tSurDev device, const tSurTu3Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a TU-3 channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmTu3ParmAccInhSet(tSurDev          device, 
                                  const tSurTu3Id *pChnId, 
                                  ePmTu3Parm       parm, 
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a TU-3 channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmTu3ParmAccInhGet(tSurDev          device, 
                                  const tSurTu3Id *pChnId, 
                                  ePmTu3Parm       parm, 
                                  bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an TU-3 channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmTu3RegThresSet(tSurDev           device, 
                                const tSurTu3Id  *pChnId, 
                                ePmTu3Parm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of an TU-3 channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmTu3RegThresGet(tSurDev           device, 
                                const tSurTu3Id  *pChnId, 
                                ePmTu3Parm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a TU-3 channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmTu3ParmNumRecRegSet(tSurDev          device, 
                                     const tSurTu3Id *pChnId, 
                                     ePmTu3Parm       parm, 
                                     byte             numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a TU-3 channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmTu3ParmNumRecRegGet(tSurDev          device, 
                                     const tSurTu3Id *pChnId, 
                                     ePmTu3Parm       parm, 
                                     byte            *pNumRecReg);
                                                                                                                                                                                                                                                                                                                                                                                                                                         
/*------------------------------------------------------------------------------
 * VT INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to get a pointer to a VT parameter.
------------------------------------------------------------------------------*/
extern eSurRet VtParmGet(tSurVtParm *pVtParm, ePmVtParm parm, tPmParm **ppParm);

/*------------------------------------------------------------------------------
This function is used to reset registers of a group of VT parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllVtParmRegReset(tSurVtParm   *pVtParm, 
                                 ePmRegType    regType,
                                 bool          neParm);

/*------------------------------------------------------------------------------
This functions is used to set accumulate inhibition for a group of VT parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllVtParmAccInhSet(tSurVtParm  *pVtParm, 
                                  bool         inhibit, 
                                  bool         neParm);

/*------------------------------------------------------------------------------
This functions is used to set register threshold for a group of VT parameters.
------------------------------------------------------------------------------*/
extern eSurRet AllVtParmRegThresSet(tSurVtParm    *pVtParm, 
                                     ePmRegType    regType,
                                     dword         thresVal, 
                                     bool          neParm);

/*------------------------------------------------------------------------------
This functions is used to set number of recent registers for a  group of VT 
parameters.
------------------------------------------------------------------------------*/
friend eSurRet AllVtParmNumRecRegSet(tSurVtParm  *pVtParm, 
                                     byte         numRecReg, 
                                     bool         neParm);
                                                                                                                                           
/*------------------------------------------------------------------------------
This function is used to provision a VT for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurVtProv(tSurDev device, tSurVtId *pVtId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one VT. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurVtDeProv(tSurDev device, tSurVtId *pVtId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a VT 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurVtTrblNotfInhSet(tSurDev           device,
                                   const tSurVtId   *pChnId, 
                                   eSurVtTrbl        trblType, 
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a VT 
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurVtTrblNotfInhGet(tSurDev           device,
                                   const tSurVtId  *pChnId, 
                                   eSurVtTrbl       trblType, 
                                   bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get VT channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurVtChnTrblNotfInhInfoGet(tSurDev             device, 
                                          const tSurVtId     *pChnId, 
                                          tSurVtMsgInh       *pTrblInh);
                                           
/*------------------------------------------------------------------------------
This function is used to set VT path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurVtTrblDesgnSet(tSurDev              device, 
                                 eSurVtTrbl           trblType, 
                                 const tSurTrblDesgn *pTrblDesgn);
                                 
/*------------------------------------------------------------------------------
This function is used to get VT path trouble designation of a device.
------------------------------------------------------------------------------*/
friend eSurRet SurVtTrblDesgnGet(tSurDev        device, 
                                 eSurVtTrbl     trblType, 
                                 tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a VT channel.
------------------------------------------------------------------------------*/
extern eSurRet SurVtEngStatSet(tSurDev             device, 
                               const tSurVtId     *pChnId, 
                               eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a STS channel.
------------------------------------------------------------------------------*/
extern eSurRet SurVtEngStatGet(tSurDev             device, 
                               const tSurVtId     *pChnId, 
                               eSurEngStat        *pStat);
                                
/*------------------------------------------------------------------------------
This internal API is used to set VT channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurVtChnCfgSet(tSurDev           device,
                              const tSurVtId   *pChnId, 
                              const tSurVtConf *pChnCfg);
                               
/*------------------------------------------------------------------------------
This internal API is used to get VT channel's configuration
------------------------------------------------------------------------------*/                        
extern eSurRet SurVtChnCfgGet(tSurDev         device,
                              const tSurVtId *pChnId, 
                              tSurVtConf     *pChnCfg);
                              
/*------------------------------------------------------------------------------
This internal API is used to get VT channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmVtFailIndGet(tSurDev          device, 
                              const tSurVtId  *pChnId, 
                              eSurVtTrbl       trblType, 
                              tSurFailStat    *pFailStat);
                                  
/*------------------------------------------------------------------------------
This internal API is used to get a VT channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmVtFailHisGet(tSurDev         device, 
                                const tSurVtId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a VT channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmVtFailHisClear(tSurDev          device, 
                                const tSurVtId  *pChnId,
                                bool             flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a VT channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmVtRegReset(tSurDev       device, 
                            tSurVtId     *pChnId, 
                            ePmVtParm     parm, 
                            ePmRegType    regType);
                         
/*------------------------------------------------------------------------------
This internal API is used to get a VT channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmVtRegGet(tSurDev             device, 
                          const tSurVtId     *pChnId, 
                          ePmVtParm           parm, 
                          ePmRegType          regType, 
                          tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a VT channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmVtTcaHisGet(tSurDev device, const tSurVtId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a VT channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmVtTcaHisClear(tSurDev device, const tSurVtId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a VT channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmVtParmAccInhSet(tSurDev          device, 
                                 const tSurVtId  *pChnId, 
                                 ePmVtParm        parm, 
                                 bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a VT channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmVtParmAccInhGet(tSurDev          device, 
                                 const tSurVtId  *pChnId, 
                                 ePmVtParm        parm, 
                                 bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an VT channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmVtRegThresSet(tSurDev          device, 
                               const tSurVtId  *pChnId, 
                               ePmVtParm        parm, 
                               ePmRegType       regType, 
                               dword            thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of an VT channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmVtRegThresGet(tSurDev          device, 
                               const tSurVtId  *pChnId, 
                               ePmVtParm        parm, 
                               ePmRegType       regType, 
                               dword           *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a VT channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmVtParmNumRecRegSet(tSurDev         device, 
                                    const tSurVtId *pChnId, 
                                    ePmVtParm       parm, 
                                    byte            numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a VT channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmVtParmNumRecRegGet(tSurDev         device, 
                                    const tSurVtId *pChnId, 
                                    ePmVtParm       parm, 
                                    byte           *pNumRecReg);
                                                                                                                                                                                                                                                                                                                                                                
/*------------------------------------------------------------------------------
 * TU-m INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to convert an TU-m to VT
------------------------------------------------------------------------------*/
extern eSurRet SurTu2Vt(const tSurTuId *pTuId, tSurVtId       *pVtId);

/*------------------------------------------------------------------------------
This function is used to provision a TU-m for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurTuProv(tSurDev device, tSurTuId *pTuId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one TU-m. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurTuDeProv(tSurDev device, tSurTuId *pTuId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a TU-m
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTuTrblNotfInhSet(tSurDev           device,
                                   const tSurTuId   *pChnId, 
                                   eSurTuTrbl        trblType, 
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a TU-m
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTuTrblNotfInhGet(tSurDev           device,
                                    const tSurTuId  *pChnId, 
                                    eSurTuTrbl       trblType, 
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get TU-m channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurTuChnTrblNotfInhInfoGet(tSurDev             device, 
                                          const tSurTuId     *pChnId, 
                                          tSurVtMsgInh       *pTrblInh);
                                                                        
/*------------------------------------------------------------------------------
This function is used to set TU path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurTuTrblDesgnSet(tSurDev              device, 
                                 eSurTuTrbl           trblType, 
                                 const tSurTrblDesgn *pTrblDesgn);
                                 
/*------------------------------------------------------------------------------
This function is used to get TU path trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurTuTrblDesgnGet(tSurDev        device, 
                                 eSurTuTrbl     trblType, 
                                 tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a TU-m channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTuEngStatSet(tSurDev             device, 
                               const tSurTuId     *pChnId, 
                               eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a TU-m channel.
------------------------------------------------------------------------------*/
extern eSurRet SurTuEngStatGet(tSurDev             device, 
                               const tSurTuId     *pChnId, 
                               eSurEngStat        *pStat);
                               
/*------------------------------------------------------------------------------
This internal API is used to set TU-m channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurTuChnCfgSet(tSurDev           device,
                              const tSurTuId   *pChnId, 
                              const tSurVtConf *pChnCfg);
                               
/*------------------------------------------------------------------------------
This internal API is used to get TU-m channel's configuration.
------------------------------------------------------------------------------*/                        
extern eSurRet SurTuChnCfgGet(tSurDev         device,
                              const tSurTuId *pChnId, 
                              tSurVtConf     *pChnCfg);
                   
/*------------------------------------------------------------------------------
This internal API is used to get TU-m channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmTuFailIndGet(tSurDev          device, 
                              const tSurTuId  *pChnId, 
                              eSurTuTrbl       trblType, 
                              tSurFailStat    *pFailStat);

/*------------------------------------------------------------------------------
This internal API is used to get a TU-m channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmTuFailHisGet(tSurDev         device, 
                                const tSurTuId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a TU-m channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmTuFailHisClear(tSurDev          device, 
                                const tSurTuId  *pChnId,
                                bool             flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a TU-m channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmTuRegReset(tSurDev       device, 
                            tSurTuId     *pChnId, 
                            ePmTuParm     parm, 
                            ePmRegType    regType);

/*------------------------------------------------------------------------------
This internal API is used to get a TU-m channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmTuRegGet(tSurDev             device, 
                          const tSurTuId     *pChnId, 
                          ePmTuParm           parm, 
                          ePmRegType          regType, 
                          tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a TU-m channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmTuTcaHisGet(tSurDev device, const tSurTuId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a TU-m channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmTuTcaHisClear(tSurDev device, const tSurTuId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a TU-m channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmTuParmAccInhSet(tSurDev          device, 
                                 const tSurTuId  *pChnId, 
                                 ePmTuParm        parm, 
                                 bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a TU-m channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmTuParmAccInhGet(tSurDev          device, 
                                 const tSurTuId  *pChnId, 
                                 ePmTuParm        parm, 
                                 bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an TU-m channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmTuRegThresSet(tSurDev          device, 
                               const tSurTuId  *pChnId, 
                               ePmTuParm        parm, 
                               ePmRegType       regType, 
                               dword            thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of an Tu-m channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmTuRegThresGet(tSurDev          device, 
                               const tSurTuId  *pChnId, 
                               ePmTuParm        parm, 
                               ePmRegType       regType, 
                               dword           *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a TU-m channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmTuParmNumRecRegSet(tSurDev         device, 
                                    const tSurTuId *pChnId, 
                                    ePmTuParm       parm, 
                                    byte            numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a TU-m channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmTuParmNumRecRegGet(tSurDev         device, 
                                    const tSurTuId *pChnId, 
                                    ePmTuParm       parm, 
                                    byte           *pNumRecReg);
                                                                                                                                                                                                                                                                                                                                                                                                                      
/*------------------------------------------------------------------------------
 * DS3/E3 INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to provision a DS3/E3 for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3Prov(tSurDev device, tSurDe3Id *pDe3Id);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one DS3/E3. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3DeProv(tSurDev device, tSurDe3Id *pDe3Id);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a DS3/E3
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3TrblNotfInhSet(tSurDev          device,
                                   const tSurDe3Id  *pChnId, 
                                   eSurDe3Trbl       trblType, 
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a DS3/E3
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3TrblNotfInhGet(tSurDev          device,
                                    const tSurDe3Id *pChnId, 
                                    eSurDe3Trbl      trblType, 
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get DS3/E3 channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3ChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurDe3Id    *pChnId, 
                                           tSurDe3MsgInh      *pTrblInh);
                                                                                                                  
/*------------------------------------------------------------------------------
This function is used to set DS3/E3 path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3TrblDesgnSet(tSurDev              device, 
                                  eSurDe3Trbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn);
                                 
/*------------------------------------------------------------------------------
This function is used to get DS3/E3 path trouble designation of a device.
------------------------------------------------------------------------------*/
friend eSurRet SurDe3TrblDesgnGet(tSurDev       device, 
                                 eSurDe3Trbl    trblType, 
                                 tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a DS3/E3 channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3EngStatSet(tSurDev             device, 
                                const tSurDe3Id    *pChnId, 
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a DS3/E3 channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3EngStatGet(tSurDev             device, 
                                const tSurDe3Id    *pChnId, 
                                eSurEngStat        *pStat);
                                                              
/*------------------------------------------------------------------------------
This internal API is used to set DS3/E3 channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurDe3ChnCfgSet(tSurDev           device,
                              const tSurDe3Id   *pChnId, 
                              const tSurDe3Conf *pChnCfg);
                               
/*------------------------------------------------------------------------------
This internal API is used to get DS3/E3 channel's configuration.
------------------------------------------------------------------------------*/                        
extern eSurRet SurDe3ChnCfgGet(tSurDev         device,
                              const tSurDe3Id *pChnId, 
                              tSurDe3Conf     *pChnCfg);                                                
             
/*------------------------------------------------------------------------------
This internal API is used to get DS3/E3 channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmDe3FailIndGet(tSurDev           device, 
                               const tSurDe3Id  *pChnId, 
                               eSurDe3Trbl       trblType, 
                               tSurFailStat     *pFailStat);

/*------------------------------------------------------------------------------
This internal API is used to get a DS3/E3 channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmDe3FailHisGet(tSurDev          device, 
                                 const tSurDe3Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a DS3/E3 channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmDe3FailHisClear(tSurDev           device, 
                                 const tSurDe3Id  *pChnId,
                                 bool              flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a DS3/E3 channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmDe3RegReset(tSurDev        device, 
                             tSurDe3Id     *pChnId, 
                             ePmDe3Parm     parm, 
                             ePmRegType     regType);

/*------------------------------------------------------------------------------
This internal API is used to get a DS3/E3 channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmDe3RegGet(tSurDev             device, 
                           const tSurDe3Id    *pChnId, 
                           ePmDe3Parm          parm, 
                           ePmRegType          regType, 
                           tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This internal API is used to get a DS3/E3 channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmDe3TcaHisGet(tSurDev device, const tSurDe3Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a DS3/E3 channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmDe3TcaHisClear(tSurDev device, const tSurDe3Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a DS3/E3 channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmDe3ParmAccInhSet(tSurDev          device, 
                                  const tSurDe3Id *pChnId, 
                                  ePmDe3Parm       parm, 
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a DS3/E3 channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmDe3ParmAccInhGet(tSurDev          device, 
                                  const tSurDe3Id *pChnId, 
                                  ePmDe3Parm       parm, 
                                  bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an DS3/E3 channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmDe3RegThresSet(tSurDev           device, 
                                const tSurDe3Id  *pChnId, 
                                ePmDe3Parm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of an DS3/E3 channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmDe3RegThresGet(tSurDev           device, 
                                const tSurDe3Id  *pChnId, 
                                ePmDe3Parm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a DS3/E3 channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmDe3ParmNumRecRegSet(tSurDev          device, 
                                     const tSurDe3Id *pChnId, 
                                     ePmDe3Parm       parm, 
                                     byte             numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a DS3/E3 channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmDe3ParmNumRecRegGet(tSurDev          device, 
                                     const tSurDe3Id *pChnId, 
                                     ePmDe3Parm       parm, 
                                     byte            *pNumRecReg);
                                                                                                                                                                                                                                                                                                                                                                                                    
/*------------------------------------------------------------------------------
 * DS1/E1 INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to provision a DS1/E1 for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1Prov(tSurDev device, tSurDe1Id *pDe1Id);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one DS1/E1. It will deallocated 
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1DeProv(tSurDev device, tSurDe1Id *pDe1Id);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a DS1/E1
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1TrblNotfInhSet(tSurDev          device,
                                   const tSurDe1Id  *pChnId, 
                                   eSurDe1Trbl       trblType, 
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a DS1/E1
channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1TrblNotfInhGet(tSurDev          device,
                                    const tSurDe1Id *pChnId, 
                                    eSurDe1Trbl      trblType, 
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get DS1/E1 channel's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1ChnTrblNotfInhInfoGet(tSurDev             device, 
                                           const tSurDe1Id    *pChnId, 
                                           tSurDe1MsgInh      *pTrblInh);
                                                                                                                   
/*------------------------------------------------------------------------------
This function is used to set DS1/E1 path trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1TrblDesgnSet(tSurDev              device, 
                                  eSurDe1Trbl          trblType, 
                                  const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get DS1/E1 path trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1TrblDesgnGet(tSurDev        device, 
                                  eSurDe1Trbl    trblType, 
                                  tSurTrblDesgn *pTrblDesgn);
                                 
/*------------------------------------------------------------------------------
This internal API is used to set DS1/E1 channel's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1ChnCfgSet(tSurDev           device,
                              const tSurDe1Id   *pChnId, 
                              const tSurDe1Conf *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a DS1/E1 channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1EngStatSet(tSurDev             device, 
                                const tSurDe1Id    *pChnId, 
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a DS1/E1 channel.
------------------------------------------------------------------------------*/
extern eSurRet SurDe1EngStatGet(tSurDev             device, 
                                const tSurDe1Id    *pChnId, 
                                eSurEngStat        *pStat);
                                
/*------------------------------------------------------------------------------
This internal API is used to get DS1/E1 channel's configuration.
------------------------------------------------------------------------------*/                        
extern eSurRet SurDe1ChnCfgGet(tSurDev         device,
                              const tSurDe1Id *pChnId, 
                              tSurDe1Conf     *pChnCfg);
                              
/*------------------------------------------------------------------------------
This internal API is used to get DS1/E1 channel's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmDe1FailIndGet(tSurDev           device, 
                               const tSurDe1Id  *pChnId, 
                               eSurDe1Trbl       trblType, 
                               tSurFailStat     *pFailStat);
                                                                 
/*------------------------------------------------------------------------------
This internal API is used to get a DS1/E1 channel's failure history.
------------------------------------------------------------------------------*/ 
extern tSortList FmDe1FailHisGet(tSurDev          device, 
                                 const tSurDe1Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a DS1/E1 channel's failure history.
------------------------------------------------------------------------------*/ 
extern eSurRet FmDe1FailHisClear(tSurDev           device, 
                                 const tSurDe1Id  *pChnId,
                                 bool              flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a DS1/E1 channel 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmDe1RegReset(tSurDev        device, 
                             tSurDe1Id     *pChnId, 
                             ePmDe1Parm     parm, 
                             ePmRegType     regType);
                                           
/*------------------------------------------------------------------------------
This API is used to get a DS1/E1 channel's parameter's register's value.
------------------------------------------------------------------------------*/                              
extern eSurRet PmDe1RegGet(tSurDev             device, 
                           const tSurDe1Id    *pChnId, 
                           ePmDe1Parm          parm, 
                           ePmRegType          regType, 
                           tPmReg             *pRetReg);
                            
/*------------------------------------------------------------------------------
This API is used to get a DS1/E1 channel's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmDe1TcaHisGet(tSurDev device, const tSurDe1Id *pChnId);
                                                                                                                                                                 
/*------------------------------------------------------------------------------
This internal API is used to clear a DS1/E1 channel's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmDe1TcaHisClear(tSurDev device, const tSurDe1Id *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a DS1/E1 channel's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmDe1ParmAccInhSet(tSurDev          device, 
                                  const tSurDe1Id *pChnId, 
                                  ePmDe1Parm       parm, 
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a DS1/E1 channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmDe1ParmAccInhGet(tSurDev          device, 
                                  const tSurDe1Id *pChnId, 
                                  ePmDe1Parm       parm, 
                                  bool            *pInhibit);
                                  
/*------------------------------------------------------------------------------
This internal API is used to set threshold for an DS1/E1 channel's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmDe1RegThresSet(tSurDev           device, 
                                const tSurDe1Id  *pChnId, 
                                ePmDe1Parm        parm, 
                                ePmRegType        regType, 
                                dword             thresVal);
                                 
/*------------------------------------------------------------------------------
This internal API is used to get threshold of an DS1/E1 channel's paramter's
register.
------------------------------------------------------------------------------*/ 
extern eSurRet PmDe1RegThresGet(tSurDev           device, 
                                const tSurDe1Id  *pChnId, 
                                ePmDe1Parm        parm, 
                                ePmRegType        regType, 
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a DS1/E1 channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmDe1ParmNumRecRegSet(tSurDev          device, 
                                     const tSurDe1Id *pChnId, 
                                     ePmDe1Parm       parm, 
                                     byte             numRecReg);
                                      
/*------------------------------------------------------------------------------
This internal API is used to get a DS1/E1 channel's parameter's number of 
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmDe1ParmNumRecRegGet(tSurDev          device, 
                                     const tSurDe1Id *pChnId, 
                                     ePmDe1Parm       parm, 
                                     byte            *pNumRecReg);

/*------------------------------------------------------------------------------
 * IMA Link INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to provision a IMA Link for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkProv(tSurDev device, tSurImaLinkId *pImaLinkId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one IMA Link. It will deallocated
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkDeProv(tSurDev device, tSurImaLinkId *pImaLinkId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a IMA Link
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkTrblNotfInhSet(tSurDev          device,
                                   const tSurImaLinkId  *pChnId,
                                   eSurImaLinkTrbl       trblType,
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a IMA Link
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkTrblNotfInhGet(tSurDev          device,
                                    const tSurImaLinkId *pChnId,
                                    eSurImaLinkTrbl      trblType,
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get IMA Link's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkChnTrblNotfInhInfoGet(tSurDev             device,
                                           const tSurImaLinkId    *pChnId,
                                           tSurImaLinkMsgInh      *pTrblInh);

/*------------------------------------------------------------------------------
This function is used to set IMA Link trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkTrblDesgnSet(tSurDev              device,
                                  eSurImaLinkTrbl          trblType,
                                  const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get IMA Link trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkTrblDesgnGet(tSurDev        device,
                                  eSurImaLinkTrbl    trblType,
                                  tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set IMA Link's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkChnCfgSet(tSurDev           device,
                              const tSurImaLinkId   *pChnId,
                              const tSurImaLinkConf *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a IMA Link.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkEngStatSet(tSurDev             device,
                                const tSurImaLinkId    *pChnId,
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a IMA Link.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkEngStatGet(tSurDev             device,
                                const tSurImaLinkId    *pChnId,
                                eSurEngStat        *pStat);

/*------------------------------------------------------------------------------
This internal API is used to get IMA Link's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurImaLinkChnCfgGet(tSurDev         device,
                              const tSurImaLinkId *pChnId,
                              tSurImaLinkConf     *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to get IMA Link's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmImaLinkFailIndGet(tSurDev           device,
                               const tSurImaLinkId  *pChnId,
                               eSurImaLinkTrbl       trblType,
                               tSurFailStat     *pFailStat);

/*------------------------------------------------------------------------------
This internal API is used to get a IMA Link's failure history.
------------------------------------------------------------------------------*/
extern tSortList FmImaLinkFailHisGet(tSurDev          device,
                                 const tSurImaLinkId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a IMA Link's failure history.
------------------------------------------------------------------------------*/
extern eSurRet FmImaLinkFailHisClear(tSurDev           device,
                                 const tSurImaLinkId  *pChnId,
                                 bool              flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a IMA Link 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkRegReset(tSurDev        device,
                             tSurImaLinkId     *pChnId,
                             ePmImaLinkParm     parm,
                             ePmRegType     regType);

/*------------------------------------------------------------------------------
This API is used to get a IMA Link's parameter's register's value.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkRegGet(tSurDev             device,
                           const tSurImaLinkId    *pChnId,
                           ePmImaLinkParm          parm,
                           ePmRegType          regType,
                           tPmReg             *pRetReg);

/*------------------------------------------------------------------------------
This API is used to get a IMA Link's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmImaLinkTcaHisGet(tSurDev device, const tSurImaLinkId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a IMA Link's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkTcaHisClear(tSurDev device, const tSurImaLinkId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a IMA Link's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkParmAccInhSet(tSurDev          device,
                                  const tSurImaLinkId *pChnId,
                                  ePmImaLinkParm       parm,
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a IMA Link's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkParmAccInhGet(tSurDev          device,
                                  const tSurImaLinkId *pChnId,
                                  ePmImaLinkParm       parm,
                                  bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an IMA Link's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkRegThresSet(tSurDev           device,
                                const tSurImaLinkId  *pChnId,
                                ePmImaLinkParm        parm,
                                ePmRegType        regType,
                                dword             thresVal);

/*------------------------------------------------------------------------------
This internal API is used to get threshold of an IMA Link's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkRegThresGet(tSurDev           device,
                                const tSurImaLinkId  *pChnId,
                                ePmImaLinkParm        parm,
                                ePmRegType        regType,
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a IMA Link's parameter's number of
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkParmNumRecRegSet(tSurDev          device,
                                     const tSurImaLinkId *pChnId,
                                     ePmImaLinkParm       parm,
                                     byte             numRecReg);

/*------------------------------------------------------------------------------
This internal API is used to get a IMA Link's parameter's number of
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmImaLinkParmNumRecRegGet(tSurDev          device,
                                     const tSurImaLinkId *pChnId,
                                     ePmImaLinkParm       parm,
                                     byte            *pNumRecReg);

/*------------------------------------------------------------------------------
 * IMA Group INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to provision a IMA Group for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpProv(tSurDev device, tSurImaGrpId *pImaGrpId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one IMA Group. It will deallocated
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpDeProv(tSurDev device, tSurImaGrpId *pImaGrpId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a IMA Group
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpTrblNotfInhSet(tSurDev          device,
                                   const tSurImaGrpId  *pChnId,
                                   eSurImaGrpTrbl       trblType,
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a IMA Group
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpTrblNotfInhGet(tSurDev          device,
                                    const tSurImaGrpId *pChnId,
                                    eSurImaGrpTrbl      trblType,
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get IMA Group's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpChnTrblNotfInhInfoGet(tSurDev             device,
                                           const tSurImaGrpId    *pChnId,
                                           tSurImaGrpMsgInh      *pTrblInh);

/*------------------------------------------------------------------------------
This function is used to set IMA Group trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpTrblDesgnSet(tSurDev              device,
                                  eSurImaGrpTrbl          trblType,
                                  const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get IMA Group trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpTrblDesgnGet(tSurDev        device,
                                  eSurImaGrpTrbl    trblType,
                                  tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set IMA Group's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpChnCfgSet(tSurDev           device,
                              const tSurImaGrpId   *pChnId,
                              const tSurImaGrpConf *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a IMA Group.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpEngStatSet(tSurDev             device,
                                const tSurImaGrpId    *pChnId,
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a IMA Group.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpEngStatGet(tSurDev             device,
                                const tSurImaGrpId    *pChnId,
                                eSurEngStat        *pStat);

/*------------------------------------------------------------------------------
This internal API is used to get IMA Group's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurImaGrpChnCfgGet(tSurDev         device,
                              const tSurImaGrpId *pChnId,
                              tSurImaGrpConf     *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to get IMA Group's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmImaGrpFailIndGet(tSurDev           device,
                               const tSurImaGrpId  *pChnId,
                               eSurImaGrpTrbl       trblType,
                               tSurFailStat     *pFailStat);

/*------------------------------------------------------------------------------
This internal API is used to get a IMA Group's failure history.
------------------------------------------------------------------------------*/
extern tSortList FmImaGrpFailHisGet(tSurDev          device,
                                 const tSurImaGrpId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a IMA Group's failure history.
------------------------------------------------------------------------------*/
extern eSurRet FmImaGrpFailHisClear(tSurDev           device,
                                 const tSurImaGrpId  *pChnId,
                                 bool              flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a IMA Group 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpRegReset(tSurDev        device,
                             tSurImaGrpId     *pChnId,
                             ePmImaGrpParm     parm,
                             ePmRegType     regType);

/*------------------------------------------------------------------------------
This API is used to get a IMA Group's parameter's register's value.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpRegGet(tSurDev             device,
                           const tSurImaGrpId    *pChnId,
                           ePmImaGrpParm          parm,
                           ePmRegType          regType,
                           tPmReg             *pRetReg);

/*------------------------------------------------------------------------------
This API is used to get a IMA Group's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmImaGrpTcaHisGet(tSurDev device, const tSurImaGrpId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a IMA Group's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpTcaHisClear(tSurDev device, const tSurImaGrpId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a IMA Group's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpParmAccInhSet(tSurDev          device,
                                  const tSurImaGrpId *pChnId,
                                  ePmImaGrpParm       parm,
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a IMA Group's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpParmAccInhGet(tSurDev          device,
                                  const tSurImaGrpId *pChnId,
                                  ePmImaGrpParm       parm,
                                  bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an IMA Group's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpRegThresSet(tSurDev           device,
                                const tSurImaGrpId  *pChnId,
                                ePmImaGrpParm        parm,
                                ePmRegType        regType,
                                dword             thresVal);

/*------------------------------------------------------------------------------
This internal API is used to get threshold of an IMA Group's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpRegThresGet(tSurDev           device,
                                const tSurImaGrpId  *pChnId,
                                ePmImaGrpParm        parm,
                                ePmRegType        regType,
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a IMA Group's parameter's number of
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpParmNumRecRegSet(tSurDev          device,
                                     const tSurImaGrpId *pChnId,
                                     ePmImaGrpParm       parm,
                                     byte             numRecReg);

/*------------------------------------------------------------------------------
This internal API is used to get a IMA Group's parameter's number of
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmImaGrpParmNumRecRegGet(tSurDev          device,
                                     const tSurImaGrpId *pChnId,
                                     ePmImaGrpParm       parm,
                                     byte            *pNumRecReg);




/*------------------------------------------------------------------------------
 * PW INTERNAL FUNCTIONS
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to provision a PW for FM & PM.
------------------------------------------------------------------------------*/
extern eSurRet SurPwProv(tSurDev device, tSurPwId *pPwId);

/*------------------------------------------------------------------------------
This function is used to de-provision FM & PM of one PW. It will deallocated
all resources.
------------------------------------------------------------------------------*/
extern eSurRet SurPwDeProv(tSurDev device, tSurPwId *pPwId);

/*------------------------------------------------------------------------------
This internal API is used to set trouble notification inhibition for a PW
------------------------------------------------------------------------------*/
extern eSurRet SurPwTrblNotfInhSet(tSurDev          device,
                                   const tSurPwId  *pChnId,
                                   eSurPwTrbl       trblType,
                                   bool              inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get trouble notification inhibition of a PW
------------------------------------------------------------------------------*/
extern eSurRet SurPwTrblNotfInhGet(tSurDev          device,
                                    const tSurPwId *pChnId,
                                    eSurPwTrbl      trblType,
                                    bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to get PW's trouble notification inhibition.
------------------------------------------------------------------------------*/
extern eSurRet SurPwChnTrblNotfInhInfoGet(tSurDev             device,
                                           const tSurPwId    *pChnId,
                                           tSurPwMsgInh      *pTrblInh);

/*------------------------------------------------------------------------------
This function is used to set PW trouble designation for a device.
------------------------------------------------------------------------------*/
extern eSurRet SurPwTrblDesgnSet(tSurDev              device,
                                  eSurPwTrbl          trblType,
                                  const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get PW trouble designation of a device.
------------------------------------------------------------------------------*/
extern eSurRet SurPwTrblDesgnGet(tSurDev        device,
                                  eSurPwTrbl    trblType,
                                  tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This internal API is used to set PW's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurPwChnCfgSet(tSurDev           device,
                              const tSurPwId   *pChnId,
                              const tSurPwConf *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to set engine state for a PW.
------------------------------------------------------------------------------*/
extern eSurRet SurPwEngStatSet(tSurDev             device,
                                const tSurPwId    *pChnId,
                                eSurEngStat         stat);

/*------------------------------------------------------------------------------
This internal API is used to get engine state of a PW.
------------------------------------------------------------------------------*/
extern eSurRet SurPwEngStatGet(tSurDev             device,
                                const tSurPwId    *pChnId,
                                eSurEngStat        *pStat);

/*------------------------------------------------------------------------------
This internal API is used to get PW's configuration.
------------------------------------------------------------------------------*/
extern eSurRet SurPwChnCfgGet(tSurDev         device,
                              const tSurPwId *pChnId,
                              tSurPwConf     *pChnCfg);

/*------------------------------------------------------------------------------
This internal API is used to get PW's specific failure's status
------------------------------------------------------------------------------*/
extern eSurRet FmPwFailIndGet(tSurDev           device,
                               const tSurPwId  *pChnId,
                               eSurPwTrbl       trblType,
                               tSurFailStat     *pFailStat);

/*------------------------------------------------------------------------------
This internal API is used to get a PW's failure history.
------------------------------------------------------------------------------*/
extern tSortList FmPwFailHisGet(tSurDev          device,
                                 const tSurPwId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a PW's failure history.
------------------------------------------------------------------------------*/
extern eSurRet FmPwFailHisClear(tSurDev           device,
                                 const tSurPwId  *pChnId,
                                 bool              flush);

/*------------------------------------------------------------------------------
This internal API is used to reset a PW 's parameter's resigter.
------------------------------------------------------------------------------*/
extern eSurRet PmPwRegReset(tSurDev        device,
                             tSurPwId     *pChnId,
                             ePmPwParm     parm,
                             ePmRegType     regType);

/*------------------------------------------------------------------------------
This API is used to get a PW's parameter's register's value.
------------------------------------------------------------------------------*/
extern eSurRet PmPwRegGet(tSurDev             device,
                           const tSurPwId    *pChnId,
                           ePmPwParm          parm,
                           ePmRegType          regType,
                           tPmReg             *pRetReg);

/*------------------------------------------------------------------------------
This API is used to get a PW's TCA history.
------------------------------------------------------------------------------*/
extern tSortList PmPwTcaHisGet(tSurDev device, const tSurPwId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to clear a PW's TCA history
------------------------------------------------------------------------------*/
extern eSurRet PmPwTcaHisClear(tSurDev device, const tSurPwId *pChnId);

/*------------------------------------------------------------------------------
This internal API is used to enable/disable a PW's accumlate inhibition.
------------------------------------------------------------------------------*/
extern eSurRet PmPwParmAccInhSet(tSurDev          device,
                                  const tSurPwId *pChnId,
                                  ePmPwParm       parm,
                                  bool             inhibit);

/*------------------------------------------------------------------------------
This internal API is used to get a PW's accumulate inhibition mode.
------------------------------------------------------------------------------*/
extern eSurRet PmPwParmAccInhGet(tSurDev          device,
                                  const tSurPwId *pChnId,
                                  ePmPwParm       parm,
                                  bool            *pInhibit);

/*------------------------------------------------------------------------------
This internal API is used to set threshold for an PW's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmPwRegThresSet(tSurDev           device,
                                const tSurPwId  *pChnId,
                                ePmPwParm        parm,
                                ePmRegType        regType,
                                dword             thresVal);

/*------------------------------------------------------------------------------
This internal API is used to get threshold of an PW's paramter's
register.
------------------------------------------------------------------------------*/
extern eSurRet PmPwRegThresGet(tSurDev           device,
                                const tSurPwId  *pChnId,
                                ePmPwParm        parm,
                                ePmRegType        regType,
                                dword            *pThresVal);

/*------------------------------------------------------------------------------
This internal API is used to set a PW's parameter's number of
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmPwParmNumRecRegSet(tSurDev          device,
                                     const tSurPwId *pChnId,
                                     ePmPwParm       parm,
                                     byte             numRecReg);

/*------------------------------------------------------------------------------
This internal API is used to get a PW's parameter's number of
recent registers.
------------------------------------------------------------------------------*/
extern eSurRet PmPwParmNumRecRegGet(tSurDev          device,
                                     const tSurPwId *pChnId,
                                     ePmPwParm       parm,
                                     byte            *pNumRecReg);


#endif /* _SUR_DB_HEADER_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - TRANSMISSION SURVEILLANCE
 *
 * File        : surid.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contain identifier definitions of Surveillance module
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _SUR_ID_HEADER_
#define _SUR_ID_HEADER_

/*--------------------------- Define -----------------------------------------*/
#include "suradapt.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Line identifier */
typedef word tSurLineId;

/* Linear APS engine identifier */
typedef word tSurApslId;

/* STS identifier. STS Path is identified by line ID and local STS ID in line. 
   This numbering scheme is compliant to GR253. So, structure of STS Path ID is 
   as follow */
typedef struct tSurStsId
    {
    tSurLineId line;       /* Line contains this STS */
    word       localId;    /* [1..768] STS local identifier in line */
    }tSurStsId;

/* VT identifier. VT Path is identified by STS ID, VTG ID and VT ID. This 
   numbering scheme is compliant to GR253. So, structure of VT Path ID is as 
   follow */
typedef struct tSurVtId
    {
    tSurStsId sts;  /* STS contains this VT */
    byte      vtg;  /* [1..7] VTG ID */
    byte      vt;   /* [1..4] VT ID */
    }tSurVtId;

/* AU-n identifier. AU-n numbering scheme is compliant to G707. So, the 
   structure of AU-n ID is as follow */
typedef struct tSurAuId
    {
    tSurLineId line;  /* Line that contains this AU-n */
    byte       aug64; /* [0..4] AUG-64 ID. This field will be ignored and should 
                                be zero when line rate is less than STM-256. */
    byte       aug16; /* [0..4] AUG-16 ID. This field will be ignored and should 
                                be zero when line rate is less than STM-64 */
    byte       aug4;  /* [0..4] AUG-4 ID. This field will be ignored and should 
                                be zero when line rate is less than STM-16 */
    byte       aug1;  /* [0..4] AUG-1 ID. This field will be ignored and should 
                                be zero when line rate is less than STM-4 */
    byte       au;    /* [0..3] AU-n ID. If AU-n is AU-4, this field will be 
                                zero. If AU-n is AU-3, the range of this field 
                                is from 1 to 3 */
    }tSurAuId;

/* TU-3 Path identifier. TU-3 Path numbering scheme is compliant to G707 and the 
   ID structure is as follow: */
typedef struct tSurTu3Id
    {
    tSurAuId au;  /* AU-3/AU-4 ID */
    byte     tu3; /* [1..3] TU3 ID */
    }tSurTu3Id;

/* TU-m Path identifier. TU-11, TU-12 and TU-2 numbering scheme are compliant to 
   G707. ID structure is as follow */
typedef struct tSurTuId
    {
    tSurAuId au; /* AU-3 or AU-4 ID */
    byte tug2;   /* [1..7] TUG-2 ID */
    byte tu;     /* [1..4] TU-m ID */
    }tSurTuId;

/* DS3/E3 identifier */
typedef word tSurDe3Id;

/* DS1/E1 identifier */
typedef struct tSurDe1Id
    {
    tSurDe3Id de3Id; /* DS3/E3 identifier */
    byte      de2Id; /* [1..7] DS2/E2 identifier */
    byte      de1Id; /* [1..4] DS1/E1 identifier */
    }tSurDe1Id;

/* IMA Link identifier */
typedef word tSurImaLinkId;

/* IMA Group identifier */
typedef word tSurImaGrpId;

/* PW identifier */
typedef word tSurPwId;
/*--------------------------- Entries ----------------------------------------*/

#endif /* _SUR_ID_HEADER_ */

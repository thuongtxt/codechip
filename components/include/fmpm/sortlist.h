/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : DATA STRUCTURE
 *
 * File        : sortlist.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contains definitions of sorted linked-list.
 *               Features:
 *               + List is sorted and linked list.
 *               + Support muti-threads
 *               + Can initialize and finalize list
 *               + Get/Add/remove one element at specified index.
 *               + Remove a specified element.
 *               + Flush list
 *               + Get size of list
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _SORT_LIST_HEADER_
#define _SORT_LIST_HEADER_

/*--------------------------- Define -----------------------------------------*/
#include "surtypes.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Linked, sorted list */
typedef void* tSortList;

/* Return code */
typedef enum eListRet
    {
    cListSucc = 0,      /* Success */
    cListErrSem,        /* Error on semaphore */
    cListErrEleExist,   /* Element existed */
    cListErrMem,        /* Memory error */
    cListErr            /* General error */
    }eListRet;

/* Order type */
typedef enum eListOrder
    {
    cListInc = 0,   /* Increase order */
    cListDec        /* Decrease order */
    }eListOrder;

/* Compare result when comparing two elements of list */
typedef enum eListCmpResult
    {
    cListEqual = 0, /* Equal */
    cListGreater,   /* Greater */
    cListLower,     /* Lower */
    cListCmpFail    /* Compare fail */
    }eListCmpResult;

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to create a sorted linked list. This function must be 
called before calling other functions
------------------------------------------------------------------------------*/
eListRet ListCreate(tSortList      *pList, 
                    eListOrder      order, 
                    eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2));

/*------------------------------------------------------------------------------
This function is used to destroy a sorted linked list. This function must be 
called if list is not used anymore to make sure that all allocated resources are
freed.
------------------------------------------------------------------------------*/
eListRet ListDestroy(tSortList *pList);

/*------------------------------------------------------------------------------
This function is used to set compare function
------------------------------------------------------------------------------*/
eListRet ListCmpFuncSet(tSortList       list, 
                        eListCmpResult (*pCmpFunc)(void *pEle1, void *pEle2));

/*------------------------------------------------------------------------------
This function is used to set order for a list
------------------------------------------------------------------------------*/
eListRet ListOrderSet(tSortList list, eListOrder order);

/*------------------------------------------------------------------------------
This function is used to get order of a list
------------------------------------------------------------------------------*/
eListRet ListOrderGet(tSortList list, eListOrder *pOrder);

/*------------------------------------------------------------------------------
This function is used to search and remove (if enable) a element in list
------------------------------------------------------------------------------*/
eListRet ListSearch(tSortList list, void *pEle, dword *pIndex, bool rmv);

/*------------------------------------------------------------------------------
This function is used to remove a element at specified index and return the
removed element
------------------------------------------------------------------------------*/
eListRet ListRmv(tSortList list, dword idx, void *pEle, dword size);

/*------------------------------------------------------------------------------
This function is used to add one element to list at a specified index
------------------------------------------------------------------------------*/
eListRet ListAdd(tSortList list, void *pData, dword size);

/*------------------------------------------------------------------------------
This function is used to get a element at specified index
------------------------------------------------------------------------------*/
eListRet ListGet(tSortList list, dword idx, void *pData, dword size);

/*------------------------------------------------------------------------------
This function is used to flush a list
------------------------------------------------------------------------------*/
eListRet ListFlush(tSortList list);

/*------------------------------------------------------------------------------
This function is used to get size of list
------------------------------------------------------------------------------*/
eListRet ListSize(tSortList list, dword *pSize);

#endif /* _SORT_LIST_HEADER_ */

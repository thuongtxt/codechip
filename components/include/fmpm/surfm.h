/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : FM - FAULT MONITORING
 *
 * File        : surfm.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contain definitions of Fault Monitoring
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _FM_HEADER_
#define _FM_HEADER_

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Failure information */
typedef struct tFmFailInfo
    {
    byte trblType;           /* Trouble type. It's value must be in 
                                eSurApslTrbl, eSurLineTrbl, eSurStsTrbl, 
                                eSurVtTrbl, eSurTu3Trbl, eSurAuTrbl, 
                                eSurDe3Trbl, eSurDe1Trbl depend on channel 
                                type */
    tSurTrblDesgn trblDesgn; /* Trouble designation */
    eSurColor     color;     /* Trouble color */
    bool          status;    /* Failure status */
    dword         time;      /* Time associate with status */
    }tFmFailInfo;
    
/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
This API is used to get failure indication of a channel's specific failure.
------------------------------------------------------------------------------*/
public eSurRet FmFailIndGet(tSurDev         device, 
                            eSurChnType     chnType, 
                            const void     *pChnId, 
                            byte            trblType, 
                            tSurFailStat   *pFailStat);
                            
/*------------------------------------------------------------------------------
This API is used to get a channel's failure history.
------------------------------------------------------------------------------*/
public tSortList FmFailHisGet(tSurDev      device,
                                    eSurChnType  chnType, 
                                    const void  *pChnId);                    

/*------------------------------------------------------------------------------
This API is used to clear a channel's failure history.
------------------------------------------------------------------------------*/
public eSurRet FmFailHisClear(tSurDev       device, 
                              eSurChnType   chnType, 
                              const void   *pChnId, 
                              bool          flush);

#endif /* _FM_HEADER_ */

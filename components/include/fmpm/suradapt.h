/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : suradapt.h
 * 
 * Created Date: Mar 16, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _SURADAPT_H_
#define _SURADAPT_H_

/*--------------------------- Includes ---------------------------------------*/
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <signal.h>
#include "surtypes.h"

/*--------------------------- Define -----------------------------------------*/
#define cOsalEventMaxNum        cAtEventMaxNum

/* The maximum number of message queue pools in system */
#define cOsalMaxNumMqPool       32

/* The maximum number of message queues in each message queue pool */
#define cOsalMaxNumMq           16

/* The sleeping time in event waiting operation */
#define cOsalEventWaitSleep     100     /* in microseconds */

/* The sleeping time in timeout waiting operation */
#define cOsalTimeoutWaitSleep   100     /* in microseconds */

/* The maximum number of timer pools in system */
#define cOsalTmPoolMaxNum       64

#define    cOsalPrioMin         1
#define    cOsalPrioMax         99

#define OneMicro          1000000
#define OneMilli          1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* OSAL return code */
typedef enum eOsalRet
    {
    cOsalOk  = 0,
    cOsalInval,
    cOsalFailed
    } eOsalRet;

/* Semaphore handle type */
typedef sem_t           atSemKey;
typedef atSemKey        osalSemKey;


/* Timer priority level */
typedef enum eOsalTimerPrioLevel
    {
    cOsalTimerLowPrio   = 0,    /* Low priority level */
    cOsalTimerHighPrio,         /* High priority level */
    cOsalTimerPrioNum
    }eOsalTimerPrioLevel;

/* Timer type */
typedef enum eOsalTimerType
    {
    cOsalTimerOneShot = 0,  /* One-shot timer */
    cOsalTimerPeriodic      /* Periodic timer */
    }eOsalTimerType ;

/* Byte order convert type */
typedef enum eOsalByteOrderConvType
    {
    cOsalByteOrderConvHostToNet,
    cOsalByteOrderConvNetToHost
    } eOsalByteOrderConvType;

/* The current time structure */
typedef struct tOsalCurTime
    {
    dword               sec;        /* Seconds */
    dword               usec;       /* Micorseconds */
    }tOsalCurTime;

/* Task handle type */
typedef pthread_t   osalTaskId;

/* Task attribute structure */
typedef struct tOsalTaskAttr
    {
    dword   stackSize;      /* Stack size */
    void*   stackAddr;      /* Stack address */
    dword   detachState;    /* Detach state */
    dword   schedPolicy;    /* Scheduling policy */
    dword   inheritSched;   /* Scheduling inheriting */
    dword   priority;       /* Scheduling priority */
    }tOsalTaskAttr;

/* Message queue attribute */
typedef struct tOsalMqAttr
    {
    dword   maxMsg;  /* The maximum number of messages that can be stored on the queue */
    dword   msgSize; /* The maximum size of each message on the given message queue */
    dword   curMsgs; /* Current number of messages in measage queue */
    }tOsalMqAttr;

/* EventWord structure */
typedef struct tOsalEventWord
    {
    byte        *eventWord; /* The pointer to EventWord */
    dword       numEvent;   /* The number of events needed to monitored */
    }tOsalEventWord;

/* Mutex handle type */
typedef pthread_mutex_t     osalMutexId;

/* Mutex attribute structure type */
typedef pthread_mutexattr_t tOsalMutexAttr;

/* Signal set type */
typedef sigset_t    tOsalSigSet;

/* Timer notifying mode */
typedef enum eOsalTmNotifyMode
    {
    cOsalTmSignal = 0,  /* send signal whenever timer is timeout */
    cOsalTmThread       /* create task/thread calling notifying function whenever timer is timeout */
    }eOsalTmNotifyMode;

/* Time specification structure */
typedef struct tOsalTmTimeSpec
    {
    dword   secNum; /* The number of seconds to run after calling OsalTmRun() */
    dword   usecNum; /* The number of microseconds  to run after calling OsalTmRun() */
    dword   iSecNum; /* The number of seconds to restart timer whenever it's timeout.
                        If timer is one-shot, set this field to 0.  */
    dword   iUSecNum; /* The number of microseconds  to restart timer whenever it's timeout.
                        If timer is one-shot, set this field to 0.  */
    }tOsalTmTimeSpec;

/* Notifying data  */
typedef union uOsalTmVal
    {
    dword   value;      /* Data is carried in signal */
    dword*  pValue;     /* Data is passed to  notifying function */
    }uOsalTmVal;

/* Timer attribute structure */
typedef struct tOsalTmAttr
    {
    eOsalTmNotifyMode   notifyMode; /* This value indicates the notifying mode: send signal or create task/thread */
    dword               sigNo;      /* Signal number */
    void                (*notifyFunction)(union uOsalTmVal); /* Function is called whenever timer is timeout */
    tOsalTaskAttr       *notifyAttr; /* Attribute of task created when timer is timeout */
    uOsalTmVal          notifyData; /* Value is passed in signal or task/thread's notifying function */
    }tOsalTmAttr;

/* Conditional variable */
typedef pthread_cond_t osalCondVar;

/* Conditional variable's attribute */
typedef pthread_condattr_t tOsalCondVarAttr;

/* The timer information structure */
typedef struct tAtTmInfor
    {
    tOsalCurTime        startTime;  /* The starting time */
    eBool               isCreated;  /* The flag is used to indicate timer has been created */
    eBool               isStarted;  /* The flag is used to indicate timer has been started */
    dword               duration;   /* The timer's duration */
    }tAtTmInfor;

/* The timer pool structure */
typedef struct tAtTmPool
    {
    dword               numTm;      /* The number of timers in pool */
    tAtTmInfor          *tmList;    /* The list of timers in pool */
    }tAtTmPool;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void* OsalMemAlloc(dword size);
void    OsalMemFree(void* pMem);
void    OsalMemInit(void* pMem, dword size, dword val);

void        OsalMemCpy(void *pSrc, void *pDest, dword size);
dword       OsalMemCmp(void *pMem1, void *pMem2, dword size);

/* Semaphore */
eOsalRet    OsalSemInit(osalSemKey *semKey, eBool procEn, dword initVal);
eOsalRet    OsalSemDestroy(osalSemKey *semKey);
eOsalRet    OsalSemTake(osalSemKey *semKey);
eOsalRet    OsalSemGive(osalSemKey *semKey);

/* OSAL Control API */
eOsalRet    OsalStart(void);
eOsalRet    OsalShutdown(void);

/* Task */
eOsalRet    OsalTaskCreate(osalTaskId *taskId, tOsalTaskAttr *pTaskAttr, void *(*taskHandler)(void*), void *data);
void        OsalTaskExit(void* data);
eOsalRet    OsalTaskWaitForTerm(osalTaskId taskId, void **data);
eOsalRet    OsalTaskCancel(osalTaskId taskId);
eOsalRet    OsalTaskCancelAttrSet(dword type);
void        OsalTaskCancelCheck(void);
eOsalRet    OsalTaskAttrInit(tOsalTaskAttr *pTaskAttr);
osalTaskId  OsalTaskSelf(void);

eOsalRet    OsalTmDestroy(dword  tmPoolId, dword  tmId);
eOsalRet    OsalTmStart(dword  tmPoolId, dword  tmId, dword  duration);
eOsalRet    OsalTmRestart(dword tmPoolId, dword tmId, dword  duration);
eOsalRet    OsalTmStop(dword tmPoolId, dword tmId);
eOsalRet    OsalTmIsTimeout (dword tmPoolId, dword tmId);
eOsalRet    OsalTmPoolCreate(dword numTm, dword  *pTmPoolId);
eOsalRet    OsalTmPoolDestroy(dword  tmPoolId);
eOsalRet    OsalTmCreate(dword  tmPoolId, dword  *pTmId);

#endif /* _SURADAPT_H_ */


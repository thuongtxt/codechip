/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - TRANSMISSION SURVEILLANCE
 *
 * File        : sur.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contain general definitions of Transmisstion 
 *               Surveillance module
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _SUR_HEADER_
#define _SUR_HEADER_

/*--------------------------- Includes ---------------------------------------*/
#include "surid.h"

/*--------------------------- Define -----------------------------------------*/
/* TODO: need to increase to support STM16??? */

/* General */
#define cSurMaxDev       16  /* Maximum of devices that this module can support */
#define cSurMaxLine      8   /* Maximum of lines that this module can support */
#define cSurMaxDe3Line   24 * 2  /* Maximum of DS3/E3 lines that this module can
                                support */
#define cSurMaxImaLink	 1	/* Maximum of IMA physical link that this module can support */
#define cSurMaxImaGrp	 1	/* Maximum of IMA group that this module can support */
                                
#define cSurMaxPw	 	 1024	/* Maximum of PW that this module can support */

/* Linear APS */
#define cSurMaxApslEng           1    /* Maximum of Linear APS engines that this module
                                        can support */
#define cSurApslMaxWorkLine      1   /* Maximum number of working channels a Linear APS can support */
                                
#define cSurEngPollTime          100  /* Engine polling interval (in ms) */

/* Default declare/terminate soaking timer */
#define cSurDefaultDecSoakTime		2
#define cSurDefaultTerSoakTime		10
#define cSurDefaultDe1TerSoakTime	20

#define cSurDefaultPmPeriod			15	/* Minutes, default period of PM */

/* Number of defects in defect queue. These constants define the number of 
   defects that can be stored in defect queue when the module run in service 
   mode. */
#define cSurNumLineDefInQue 100 /* Number of line defects in line defect queue */
#define cSurNumStsDefInQue  200 /* Number of STS defects in line defect queue */
#define cSurNumTu3DefInQue  200 /* Number of TU3 defects in line defect queue */
#define cSurNumVtDefInQue   500 /* Number of VT defects in line defect queue */
#define cSurNumDe3DefInQue  200 /* Number of DS3/E3 defects in line defect queue */
#define cSurNumDe1DefInQue  500 /* Number of DS1/E1 defects in line defect queue */

/* Defect bit mask definitions. These bit masks are used if module run in 
   service mode */
   
/* Linear APS defect bit masks */
#define cSurApslKByteFailMask   cBit0     /* Linear APS K-Byte Defect */
#define cSurApslChnMisMask      cBit1     /* Linear APS Channel Mismatch */
#define cSurApslMdMisMask       cBit2     /* Linear APS Mode Mismatch */
#define cSurApslFeFailMask      cBit3     /* Linear APS Far-end Defect */
#define cSurApslFailMask        cBit4     /* Linear APS Switching Failure */
#define cSurApslSwChnMask       cBit8_5   /* [0..14]ID of the working channel that 
                                             is utilizing the protection line. The
                                             value 0 indicates that the protection
                                             line is free */
#define cSurApslSwChnShift      5         /* Shifting value for insert Linear APS
                                             channel into the bit masks */

/* Section defect bit masks */
#define cSurSecLosMask    cBit0 /* Loss Of Signal */
#define cSurSecLofMask    cBit1 /* Loss Of Frame */
#define cSurSecSefMask    cBit2 /* Severely Errored Framing */
#define cSurSecTimsMask   cBit3 /* Section Trace Identifier Mismatch */

/* Line defect bit masks */
#define cSurLineBerSfMask cBit4 /* Line BER-based Signal Failure */
#define cSurLineBerSdMask cBit5 /* Line BER-based Signal Degrade */
#define cSurLineDccMask   cBit6 /* DCC failure */
#define cSurLineAislMask  cBit7 /* Alarm Indication Signal */
#define cSurLineRdilMask  cBit8 /* Remote Defect Indication */
#define cSurLineNumDef    9     /* Number line defect types */

/* STS defect bit masks */
#define cSurStsLopMask   cBit0  /* STS path Loss Of Pointer */
#define cSurStsPlmMask   cBit1  /* STS Path Payload Label Mismatch */
#define cSurStsUneqMask  cBit2  /* STS Path Unequipped */
#define cSurStsTimMask   cBit3  /* STS Path Trace Identifier Mismatch */
#define cSurStsAisMask   cBit4  /* STS Path Alarm Indication Signal */
#define cSurStsRdiMask   cBit5  /* STS Path one-bit Remote Defect Indication. 
                                   If ERDI-P is supported, this field will be 
                                   ignored. */
#define cSurStsErdiSMask cBit6  /* STS Path Server Enhanced Remote Defect 
                                   Indication. If ERDI-P is not supported, this 
                                   field will be ignored. */
#define cSurStsErdiCMask cBit7  /* STS Path Connectivity Enhanced Remote Defect 
                                   Indication. If ERDI-P is not supported, this 
                                   field will be ignored. */
#define cSurStsErdiPMask cBit8  /* STS Path Payload Enhanced Remote Defect 
                                   Indication If ERDI-P is not supported, this 
                                   field will be ignored. */
#define cSurStsBerSfMask cBit9  /* STS Path BER-based Signal Failure */
#define cSurStsBerSdMask cBit10 /* STS Path BER-based Signal Degrade */
#define cSurStsLbitMask  cBit11 	/* L bit error, only used for PW */
#define cSurStsRbitMask  cBit12 	/* R bit error, only used for PW */
#define cSurStsMbitMask  cBit13 	/* M bit error, only used for PW */
#define cSurStsNumDef    14     /* Number STS defect types */

/* VT defect bit masks */
#define cSurVtLopMask   cBit0  /* VT path Loss Of Pointer */
#define cSurVtPlmMask   cBit1  /* VT Path Payload Label Mismatch */
#define cSurVtUneqMask  cBit2  /* VT Path Unequipped */
#define cSurVtTimMask   cBit3  /* VT Path Trace Identifier Mismatch */
#define cSurVtAisMask   cBit4  /* VT Path Alarm Indication Signal */
#define cSurVtRdiMask   cBit5  /* VT Path Remote Defect Indication. If ERDI-V is 
                                  supported, this field will be ignored. */
#define cSurVtErdiSMask cBit6  /* VT Path Server Enhanced Remote Defect 
                                  Indication. If ERDI-V is not supported, this 
                                  field will be ignored. */
#define cSurVtErdiCMask cBit7  /* VT Path Connectivity Enhanced Remote Defect 
                                  Indication. If ERDI-V is not supported, this 
                                  field will be ignored. */
#define cSurVtErdiPMask cBit8  /* VT Path Payload Enhanced Remote Defect 
                                  Indication If ERDI-V is not supported, this 
                                  field will be ignored. */
#define cSurVtBerSfMask cBit9  /* VT Path BER-based Signal Failure */
#define cSurVtBerSdMask cBit10 /* VT Path BER-based Signal Degrade */

#define cSurVtLbitMask 	cBit11 /* L bit failure */
#define cSurVtRbitMask 	cBit12 /* R bit failure */
#define cSurVtMbitMask 	cBit13 /* M bit failure */

#define cSurVtNumDef    14     /* Number VT defect types */

/* DS3/E3 defect bit masks */
#define cSurDe3LosMask      cBit0 /* DS3/E3 line Loss Of Signal */
#define cSurDe3OofMask      cBit1 /* DS3/E3 path Loss Of Frame */
#define cSurDe3SefMask      cBit2 /* DS3/E3 path Severely Errored Frame */
#define cSurDe3AisMask      cBit3 /* DS3/E3 path Alarm Indication Signal */
#define cSurDe3SefAisFeMask cBit4 /* Far-End SEF/AIS */
#define cSurDe3RaiMask      cBit5 /* DS3/E3 path Remote Alarm Indication */
#define cSurDe3LbitMask    	cBit6 /* L bit failure */
#define cSurDe3RbitMask    	cBit7 /* L bit failure */
#define cSurDe3MbitMask    	cBit8 /* L bit failure */
#define cSurDe3NumDef       9     /* Number DS3/E3 defect types */

/* DS1/E1 defect bit masks */
#define cSurDe1LosMask   cBit0 /* DS1/E1 line Loss Of Signal */
#define cSurDe1OofMask   cBit1 /* DS1/E1 path Out Of Frame */
#define cSurDe1SefMask   cBit2 /* DS1/E1 path Severely Errored Frame */
#define cSurDe1AisMask   cBit3 /* DS1/E1 path Alarm Indication Signal */
#define cSurDe1AisCiMask cBit4 /* DS1/E1 path Alarm Indication Signal
                                  Customer Installation */
#define cSurDe1RaiMask   cBit5 /* DS1/E1 path Remote Alarm Indication */
#define cSurDe1RaiCiMask cBit6 /* DS1/E1 path Remote Alarm Indication - 
                                  Customer Installation */
#define cSurDe1LbitMask  cBit7 /* L bit failure */
#define cSurDe1RbitMask  cBit8 /* R bit failure */
#define cSurDe1MbitMask  cBit9 /* M bit failure */
#define cSurDe1NumDef    10     /* Number DS1/E1 defect types */

/* IMA Link defect bit masks */
#define cSurImaLinkLifMask  			cBit0  /* IMA Link Loss of IMA Frame (LIF) */
#define cSurImaLinkLodsMask 			cBit1  /* IMA Link Out of Delay Synchronization (LODS) */
#define cSurImaLinkRdiMask  			cBit2  /* IMA Link Remote Defect Indicator (RDI) */
#define cSurImaLinkTxLsmChangeMask  	cBit3  /* IMA Link Tx LSM state changing NE */
#define cSurImaLinkRxLsmChangeMask  	cBit4  /* IMA Link Rx LSM state changing NE */
#define cSurImaLinkTxLsmChangeFeMask  	cBit5  /* IMA Link Tx LSM state changing FE */
#define cSurImaLinkRxLsmChangeFeMask  	cBit6  /* IMA Link Rx LSM state changing FE */
#define cSurImaLinkPhyDefMask  			cBit7  /* Defect of physical link (LOS, OOF, LOF, AIS, LCD) */
#define cSurImaLinkNumDef   			8      /* Number IMA Link defect types */

/* IMA Group defect bit masks */
#define cSurImaGrpGsmChangeMask  		cBit0  /* IMA Near-End Group State Machine change state */
#define cSurImaGrpGtsmChangeMask 		cBit1  /* IMA Near-End Group Traffic State Machine change state */
#define cSurImaGrpGsmChangeFeMask  		cBit2  /* IMA Far-End Group State Machine change state */
#define cSurImaGrpTimingMismatchMask  	cBit3  /* IMA Group Timing-Mismatch */
#define cSurImaGrpNumDef   				4      /* Total IMA Group defect */

/* PW defect bit masks */
#define cSurPwStrayPktMask          		cBit0	/* PW Stray packet */
#define cSurPwMalformedPktMask      		cBit1	/* PW Malformed Packet */
#define cSurPwExcPktLossRateMask    		cBit2	/* PW Excessive packet loss rate */
#define cSurPwJitBufOverrunMask     		cBit3	/* PW Jitter Buffer overrun */
#define cSurPwRemotePktLossMask     		cBit4	/* PW Remote packet loss */
#define cSurPwLofsMask  					cBit5	/* PW LOFS */
#define cSurPwNumDef   						6      	/* Total PW defect */

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mColorCode(color)

Purpose   : This macro is used to get shell color code from eSurColor
------------------------------------------------------------------------------*/
#define mColorCode(color)                                                      \
    (((color) == cSurColorRed) ? 31 :                                          \
    ((((color) == cSurColorYellow) || ((color) == cSurColorAmber)) ? 33 :      \
    (((color) ==  cSurColorGreen) ? 32 :                                       \
    (((color) == cSurColorWhite) ? 37 : 0))))                                  
    
    
/*------------------------------------------------------------------------------
Prototype : mSurNumSts(lineRate)

Purpose   : This macro is used to get number of STSs corresponding with line 
            rate

Inputs    : lineRate                 - line rate

Outputs   : None

Return    : number of STSs
------------------------------------------------------------------------------*/
#define mSurNumSts(lineRate)                                                   \
    (((lineRate) == cSurOc1Stm0)     ? 1   :                                   \
    (((lineRate) == cSurOc3Stm1)     ? 3   :                                   \
    (((lineRate) == cSurOc12Stm4)    ? 12  :                                   \
    (((lineRate) == cSurOc48Stm16)   ? 48  :                                   \
    (((lineRate) == cSurOc192Stm64)  ? 192 :                                   \
    (((lineRate) == cSurOc768Stm256) ? 768 : 0))))))

/*------------------------------------------------------------------------------
Prototype : mBitStat(reg, bitMask)
Purpose   : This macro is used to check if a bit in register is 1 or 0
Inputs    : reg                      - register
            bitMask                  - bit mask
Return    : true                     - if bit value is 1
            false                    - if bit value is 0
------------------------------------------------------------------------------*/
#define mBitStat(reg, bitMask)       ((((reg) & (bitMask)) != 0) ? true : false)

/*------------------------------------------------------------------------------
Prototype : mIsNull(pPointer)
Purpose   : This macro is used to check if a pointer is null
Inputs    : pPoiner                  - pointer
Return    : true                     - if the pointer is null
            false                    - if the pointer is not null
------------------------------------------------------------------------------*/
#define mIsNull(pPointer)   ((pPointer) == null)

/*--------------------------- Typedefs ---------------------------------------*/
/* Surveillance module return code */
typedef enum eSurRet
    {
    cSurOk,             /* Successful return code */
    cSurErrSem,         /* Error on semaphore */
    cSurErrRsrNoAvai,   /* There is no resource left */
    cSurErrChnBusy,     /* Channel is busy */
    cSurErrNoSuchChn,   /* No such channel */
    cSurErrDevNotFound, /* Device not found */
    cSurErrInvlParm,    /* Invalid parameter */
    cSurErrNullParm,    /* Null handle */
    cSurErr             /* Fail return code */
    }eSurRet;

/* Message type */
typedef enum eSurMsgType
    {
    cSurErrMsg  = 31,  /* Error message */
    cSurInfoMsg = 32,  /* Information message */
    cSurWarnMsg = 33,  /* Warning message */
    cSurNormMsg = 37   /* Normal message */
    }eSurMsgType;
    
/* Defect service mode */
typedef enum eSurDefServMd
    {
    cSurDefPoll, /* Module runs in polling mode. It will poll defect status of 
                    all monitored channels periodically. */
    cSurDefServ  /* Module runs in service mode. It will receive defects from 
                    other tasks. */
    }eSurDefServMd;
    
/* Surveillance engine state */
typedef enum eSurEngStat
    {
    cSurStop,   /* Engine is in stop state, PM and FM are disabled. */
    cSurStart   /* Engine is in start state, PM and FM are in progress. */
    }eSurEngStat;

/* Failure status */
typedef struct tSurFailStat
    {
    bool     status; /* [true/false] Failure status */
    dword    time;   /* Time when failure changed its status */
    }tSurFailStat;
       
/* Line rate */
typedef enum eSurLineRate
    {
    cSurOc1Stm0,    /* OC-1/STM-0 rate */
    cSurOc3Stm1,    /* OC-3/STM1 */
    cSurOc12Stm4,   /* OC-12/STM-4 */
    cSurOc48Stm16,  /* OC-48/STM-16 */
    cSurOc192Stm64, /* OC-192/STM-64 */
    cSurOc768Stm256 /* OC-768/STM-256 */
    }eSurLineRate;
    
/* STS rate */
typedef enum eSurStsRate
    {
    cSurSts1,   /* STS-1 */
    cSurSts3c,  /* STS-3c */
    cSurSts12c, /* STS-12c */
    cSurSts48c, /* STS-48c */
    cSurSts192c,/* STS-192c */
    cSurSts768c /* STS-192c */
    }eSurStsRate;

/* VT rate */    
typedef enum eSurVtRate
    {
    cSurVt15, /* VT1.5 */
    cSurVt2,  /* VT2 */
    cSurVt3,  /* VT3 */
    cSurVt6   /* VT6 */
    }eSurVtRate;

/* Linear APS architecture */
typedef enum eSurApslArch
    {
    cSurLinear11, /* Linear APS 1+1 */
    cSurLinear1n  /* Linear APS 1:n */
    }eSurApslArch;

/* Linear APS directional mode */
typedef enum eSurApslDir
    {
    cSurUniDir,     /* Uni-directional */
    cSurBiDir       /* Bi-directional */
    }eSurApslDir;

/* Linear APS revertive mode */
typedef enum eSurApslRev
    {
    cSurNonRev,     /* Non-revertive mode */
    cSurRev         /* Revertive mode */
    }eSurApslRev;

/* Linear APS trouble & event type */
typedef enum eSurApslTrbl
    {
    /* Linear APS */
    cFmApslKbyteFail, /* Protection Switching Byte failure indication. This 
                         failure is ignored if LTE does not support Linear APS. */
    cFmApslChnMis,    /* APS Channel Mismatch failure indication. This failure 
                         is ignored if LTE does not support Linear APS. */
    cFmApslMdMis,     /* APS Mode Mismatch failure indication. This failure is 
                         ignored if LTE does not support Linear APS. */
    cFmApslFeFail,    /* Far-End Protection Line failure indication. This 
                         failure is ignored if LTE does not support Linear APS. */
    cFmLineApsFail,   /* Line APS switching failure */
    cFmApslAllFail,   /* All Linear APS failures */
    
    cEvLineApsSw0,    /* This event indicates that the protection line is freed. */
    cEvLineApsSw1,    /* Linear APS switching event for working channel #1 */
    cEvLineApsSw2,    /* Linear APS switching event for working channel #2 */
    cEvLineApsSw3,    /* Linear APS switching event for working channel #3 */
    cEvLineApsSw4,    /* Linear APS switching event for working channel #4 */
    cEvLineApsSw5,    /* Linear APS switching event for working channel #5 */
    cEvLineApsSw6,    /* Linear APS switching event for working channel #6 */
    cEvLineApsSw7,    /* Linear APS switching event for working channel #7 */
    cEvLineApsSw8,    /* Linear APS switching event for working channel #8 */
    cEvLineApsSw9,    /* Linear APS switching event for working channel #9 */
    cEvLineApsSw10,   /* Linear APS switching event for working channel #10 */
    cEvLineApsSw11,   /* Linear APS switching event for working channel #11 */
    cEvLineApsSw12,   /* Linear APS switching event for working channel #12 */
    cEvLineApsSw13,   /* Linear APS switching event for working channel #13 */
    cEvLineApsSw14   /* Linear APS switching event for working channel #14 */
    
    }eSurApslTrbl;

/* Linear APS automatic messages inhibition */
typedef struct tSurApslMsgInh
    {
    bool apsl;      /* [true/false] Inhibiting automatic messages of Linear APS */
    bool kbyteFail; /* [true/false] Inhibition of Linear APS K-byte failure automatic 
                                    message */
    bool chnMis;    /* [true/false] Inhibition of Linear APS Channel Mismatch failure 
                                    automatic message */
    bool mdMis;     /* [true/false] Inhibition of Linear APS Mode Mismatch failure 
                                    automatic message */
    bool feFail;    /* [true/false] Inhibition of Linear APS Far-End failure automatic 
                                    message */
    bool protFail;  /* [true/false] Inhibition of Linear APS switching failure */
    bool swEvent;   /* [true/false] Inhibition of Linear APS switching event */
    }tSurApslMsgInh;

/* Linear APS configuration */
typedef struct tSurApslConf
    {
    eSurApslArch arch;                          /* Linear APS architecture */
    eSurApslDir  dir;                           /* Directional mode */
    eSurApslRev  rev;                           /* Revertive mode */        
    byte         numWorkLine;                   /* Number of working line(s) */
    tSurLineId   protLine;                      /* Protecting line */
    tSurLineId   workLine[cSurApslMaxWorkLine]; /* Working line(s) */

    dword		 decSoakTime;		   /* Declare soaking timer of failure */
    dword		 terSoakTime;		   /* Terminate soaking timer of failure */
    }tSurApslConf;
    
/* SONET/SDH Line/Section trouble type */
typedef enum eSurLineTrbl
    {
    /* Section */
    cFmSecLos,      /* Loss Of Signal status */
    cFmSecLof,      /* Loss Of Frame status */
    cFmSecTim,      /* Section Trace Identifier Mismatch */
    cFmSecAllFail,  /* All Section failures */
    
    /* Line */      
    cFmLineAis,     /* Alarm Indication Signal failure indication */
    cFmLineBerSf,   /* Line BER-based Signal Failure failure indication */
    cFmLineBerSd,   /* Line BER-based Signal Degrade failure indication */
    cFmLineRfi,     /* Remote failure Indication */
    cFmLineDccFail, /* DCC failure indication */
    cFmLineAllFail, /* All Line failures */
    
    /* Other defects */
    cFmSecSef,      /* Severely Errored Framing */
    cFmLineRdi,     /* Remote defect indication */
    
    /* Performance Monitoring */
    cPmLineTca      /* Threshold Crossing Alert of SONET/SDH Line parameters */
    }eSurLineTrbl;

/* Line automatic messages inhibition */
typedef struct tSurLineMsgInh
    {
    /* Section */
    bool section;       /* [true/false] Inhibiting automatic messages of 
                                        Section */
    bool los;           /* [true/false] Inhibition of LOS failure automatic 
                                        message */
    bool lof;           /* [true/false] Inhibition of LOF failure automatic 
                                        message */
    bool tim;           /* [true/false] Inhibition of TIM-S failure automatic 
                                        message */
    /* Line */
    bool line;          /* [true/false] Inhibiting automatic messages of Line */
    bool berSf;         /* [true/false] Inhibition of Line BER-SF failure 
                                        automatic message */
    bool berSd;         /* [true/false] Inhibition of Line BER-SD failure 
                                        automatic message */
    bool dcc;           /* [true/false] Inhibition of DCC failure automatic 
                                        message */
    bool aisl;          /* [true/false] Inhibition of AIS-L failure automatic 
                                        message */
    bool rfil;          /* [true/false] Inhibition of RFI-L failure automatic 
                                        message */
    
    /* Performance Monitoring */
    bool tca;           /* [true/false] Inhibition of TCA automatic message */
    }tSurLineMsgInh;

/* Line configuration */
typedef struct tSurLineConf
    {
    eSurLineRate rate;           /* Line rate */
    dword		 decSoakTime;		   /* Declare soaking timer of failure */
    dword		 terSoakTime;		   /* Terminate soaking timer of failure */
    }tSurLineConf;
        
/* STS Path trouble type */
typedef enum eSurStsTrbl
    {
    /* Defect/Failure declarations */
    cFmStsAis,      /* STS Path Alarm Indication Signal */
    cFmStsLop,      /* STS path Loss Of Pointer */
    cFmStsBerSf,    /* STS Path BER-based Signal Failure */
    cFmStsBerSd,    /* STS Path BER-based Signal Degrade */
    cFmStsUneq,     /* STS Path Unequipped */
    cFmStsTim,      /* STS Path Trace Identifier Mismatch */
    cFmStsPlm,      /* STS Path Payload Label Mismatch */
    cFmStsRfi,      /* STS Path one-bit Remote Failure Indication. */
    cFmStsErfiS,    /* STS Path Server Enhanced Remote Failure Indication. */
    cFmStsErfiC,    /* STS Path Connectivity Enhanced Remote Failure Indication. */
    cFmStsErfiP,    /* STS Path Payload Enhanced Remote Failure Indication */
    cFmStsAllFail,  /* STS Path all failures */
    
    /* Other defects */
    cFmStsRdi,      /* STS Path one-bit Remote Defect Indication. */
    cFmStsErdiS,    /* STS Path Server Enhanced Remote Defect Indication. */
    cFmStsErdiC,    /* STS Path Connectivity Enhanced Remote Defect Indication. */
    cFmStsErdiP,    /* STS Path Payload Enhanced Remote Defect Indication */

    cFmStsLbit,     /* L bit error only used for PW */
    cFmStsRbit,     /* R bit error only used for PW */
    cFmStsMbit,     /* M bit error only used for PW */
    /* Performance Monitoring */
    cPmStsTca         /* Threshold Crossing Alert of SONET/SDH STS Path parameters */
    }eSurStsTrbl;

/* TU3 trouble type */
typedef eSurStsTrbl eSurTu3Trbl;

/* AU-n trouble type */
typedef eSurStsTrbl eSurAuTrbl;

/* STS automatic messages inhibition */
typedef struct tSurStsMsgInh
    {
    bool sts;       /* [true/false] Inhibiting automatic messages of STS Path */
    bool lopp;      /* [true/false] Inhibition of LOP-P failure automatic 
                                    message */
    bool plmp;      /* [true/false] Inhibition of PLM-P failure automatic 
                                    message */
    bool uneqp;     /* [true/false] Inhibition of UNEQ-P failure automatic 
                                    message */
    bool timp;      /* [true/false] Inhibition of TIM-P failure automatic 
                                    message */
    bool aisp;      /* [true/false] Inhibition of AIS-P failure automatic 
                                    message */
    bool rfip;      /* [true/false] Inhibition of RFI-P/ERFI-P failure automatic 
                                    message */
    bool pathBerSf; /* [true/false] Inhibition of STS Path BER-SF failure 
                                    automatic message */
    bool pathBerSd; /* [true/false] Inhibition of STS Path BER-SD failure 
                                    automatic message */
    bool lBit; 		/* [true/false] Inhibition of L bit failure automatic message */
    bool rBit; 		/* [true/false] Inhibition of R bit failure automatic message */
    bool mBit; 		/* [true/false] Inhibition of M bit failure automatic message */
    
    /* Performance Monitoring */
    bool tca;       /* [true/false] Inhibition of TCA automatic message */

    }tSurStsMsgInh;

/* STS configuration */
typedef struct tSurStsConf
    {
    eSurStsRate rate;           /* STS rate */
    bool        erdiEn;         /* [true/false] Enable ERDI-P */
    dword		 decSoakTime;	/* Declare soaking timer of failure */
    dword		 terSoakTime;	/* Terminate soaking timer of failure */
    }tSurStsConf;
    
/* VT trouble type */
typedef enum eSurVtTrbl
    {
    /* Defect/Failure definitions */
    cFmVtAis,     /* VT Path Alarm Indication Signal */
    cFmVtLop,     /* VT path Loss Of Pointer */
    cFmVtBerSf,   /* VT Path BER-based Signal Failure */
    cFmVtBerSd,   /* VT Path BER-based Signal Degrade */
    cFmVtUneq,    /* VT Path Unequipped */
    cFmVtTim,     /* VT Path Trace Identifier Mismatch */
    cFmVtPlm,     /* VT Path Payload Label Mismatch */
    cFmVtRfi,     /* VT Path Remote Failure Indication */
    cFmVtErfiS,   /* VT Path Server Enhanced Remote Failure Indication. */
    cFmVtErfiC,   /* VT Path Connectivity Enhanced Remote Failure Indication. */
    cFmVtErfiP,   /* VT Path Payload Enhanced Remote Failure Indication */

    cFmVtLbit,     /* L bit error only used for PW */
    cFmVtRbit,     /* R bit error only used for PW */
    cFmVtMbit,     /* M bit error only used for PW */

    cFmVtAllFail, /* VT Path all failures */
    
    /* Other defects */
    cFmVtRdi,     /* VT Path Remote Defect Indication */
    cFmVtErdiS,   /* VT Path Server Enhanced Remote Defect Indication. */
    cFmVtErdiC,   /* VT Path Connectivity Enhanced Remote Defect Indication. */
    cFmVtErdiP,   /* VT Path Payload Enhanced Remote Defect Indication */
    
    /* Performance Monitoring */
    cPmVtTca      /* Threshold Crossing Alert of SONET/SDH VT Path parameters */
    }eSurVtTrbl;
    
/* TU-m trouble type */
typedef eSurVtTrbl eSurTuTrbl;

/* VT automatic messages inhibition */
typedef struct tSurVtMsgInh
    {
    bool vt;      /* [true/false] Inhibiting automatic messages of VT Path */
    bool lopv;    /* [true/false] Inhibition of LOP-V failure automatic message */
    bool plmv;    /* [true/false] Inhibition of PLM-V failure automatic message */
    bool uneqv;   /* [true/false] Inhibition of UNEQ-V failure automatic message */
    bool timv;    /* [true/false] Inhibition of TIM-V failure automatic message */
    bool aisv;    /* [true/false] Inhibition of AIS-V failure automatic message */
    bool rfiv;    /* [true/false] Inhibition of RFI-V failure automatic message */
    bool vtBerSf; /* [true/false] Inhibition of VT Path BER-SF failure automatic 
                                  message */
    bool vtBerSd; /* [true/false] Inhibition of VT Path BER-SD failure automatic 
                                  message */
    bool lBit; 	  /* [true/false] Inhibition of L bit failure automatic message */
    bool rBit; 	  /* [true/false] Inhibition of R bit failure automatic message */
    bool mBit; 	  /* [true/false] Inhibition of M bit failure automatic message */
    
    /* Performance Monitoring */
    bool tca;     /* [true/false] Inhibition of TCA automatic message */
    }tSurVtMsgInh;

/* VT configuration */
typedef struct tSurVtConf
    {
    eSurVtRate rate;           /* VT rate */
    bool       erdiEn;         /* [true/false] Enable ERDI-V */
    dword	   decSoakTime;	   /* Declare soaking timer of failure */
    dword	   terSoakTime;	   /* Terminate soaking timer of failure */
    }tSurVtConf;
        
/* DS3/E3 trouble type */
typedef enum eSurDe3Trbl
    {
    /* Defect/Failure definition */
    cFmDe3Los,       /* DS3/E3 line Loss Of Signal failure */
    cFmDe3Lof,       /* DS3/E3 path Loss Of Frame failure */
    cFmDe3Ais,       /* DS3/E3 path Alarm Indication Signal failure */
    cFmDe3Rai,       /* DS3/E3 path Remote Alarm Indication */
    cFmDe3AllFail,   /* DS3/E3 all failures */
    
    /* Other defects */
    cFmDe3Oof,       /* DS3/E3 path Out of Frame */
    cFmDe3Sef,       /* DS3/E3 path Severely Errored Frame */
    cFmDe3SefAisFar, /* DS3/E3 path Far-End SEF/AIS */
    
    cFmDe3Lbit, 	 /* L bit failure */
    cFmDe3Rbit, 	 /* R bit failure */
    cFmDe3Mbit, 	 /* M bit failure */

    /* Performance Monitoring */
    cPmDe3Tca        /* Threshold Crossing Alert of PDH DS3/E3 parameters */
    }eSurDe3Trbl;

/* DS3/E3 automatic messages inhibition */
typedef struct tSurDe3MsgInh
    {
    bool de3; /* [true/false] Inhibit automatic messages of DS3/E3 */
    bool los; /* [true/false] Inhibition of DS3/E3 line Loss Of Signal failure 
                              automatic message */
    bool lof; /* [true/false] Inhibition of DS3/E3 path Loss Of Frame failure 
                              automatic message */
    bool ais; /* [true/false] Inhibition of DS3/E3 path Alarm Indication Signal 
                              failure automatic message */
    bool rai; /* [true/false] Inhibition of DS3/E3 path Remote Alarm Indication 
                              automatic message */
    bool lBit; /* [true/false] Inhibition of L bit failure automatic message */
    bool rBit; /* [true/false] Inhibition of R bit failure automatic message */
    bool mBit; /* [true/false] Inhibition of M bit failure automatic message */
    
    /* Performance Monitoring */
    bool tca; /* [true/false] Inhibition of TCA automatic message */
    }tSurDe3MsgInh;

/* DS3/E3 configuration */
typedef struct tSurDe3Conf
    {
    /* Nothing yet */
    dword		 decSoakTime;	/* Declare soaking timer of failure */
    dword		 terSoakTime;	/* Terminate soaking timer of failure */
    }tSurDe3Conf;
        
/* DS1/E1 trouble type */
typedef enum eSurDe1Trbl
    {
    /* Defect/Failure definitions */
    cFmDe1Los,        /* DS1/E1 line Loss Of Signal failure */
    cFmDe1Lof,        /* DS1/E1 path Loss Of Frame failure */
    cFmDe1Ais,        /* DS1/E1 path Alarm Indication Signal failure */
    cFmDe1AisCi,      /* DS1/E1 path Alarm Indication Signal -
                         Customer Installation failure */
    cFmDe1Rai,        /* DS1/E1 path Remote Alarm Indication failure */
    cFmDe1RaiCi,      /* DS1/E1 path Remote Alarm Indication - 
                         Customer Installation failure */

    cFmDe1AllFail,    /* DS1/E1 all failures */
    
    /* Other defects */
    cFmDe1Oof,        /* DS1/E1 path Out of Frame */
    cFmDe1Sef,        /* DS1/E1 path Severely Errored Frame */
    
    cFmDe1Lbit,       /* L bit failure */
    cFmDe1Rbit,       /* R bit failure */
    cFmDe1Mbit,       /* M bit failure */


    /* Performance Monitoring */
    cPmDe1Tca         /* Threshold Crossing Alert of PDH DS1/E1 parameters */
    }eSurDe1Trbl;

/* DS1/E1 automatic messages inhibition */
typedef struct tSurDe1MsgInh
    {
    bool de1;   /* [true/false] Inhibiting automatic messages of DS1/E1 Path */
    bool los;   /* [true/false] Inhibition of DS1/E1 line Loss Of Signal 
                   failure */
    bool lof;   /* [true/false] Inhibition of DS1/E1 path Loss Of Frame 
                   failure */
    bool ais;   /* [true/false] Inhibition of DS1/E1 path Alarm Indication 
                   Signal failure */
    bool aisCi; /* [true/false] Inhibition of DS1/E1 path 
                   Alarm Indication Signal � Customer Installation failure */
    bool rai;   /* [true/false] Inhibition of DS1/E1 path Remote Alarm 
                   Indication failure */
    bool raiCi; /* [true/false] Inhibition of DS1/E1 path 
                   Remote Alarm Indication - Customer Installation failure */
    
    bool lBit;  /* [true/false] L bit failure */
    bool rBit;  /* [true/false] R bit failure */
    bool mBit;  /* [true/false] M bit failure */

    /* Performance Monitoring */
    bool tca;   /* [true/false] Inhibition of TCA automatic message */

    }tSurDe1MsgInh;

/* DS1 framing type */
typedef enum eSurDs1FrmType
    {
    cSurDs1FrmSf,  /* DS1 SF framing */
    cSurDs1FrmEsf /* DS1 ESF framing */
    }eSurDs1FrmType;

/* DS1/E1 configuration */
typedef struct tSurDe1Conf
    {
    eSurDs1FrmType frmType;        /* DS1 framing type */
    bool           measureFs;      /* This field is used for DS1-SF to indicate 
                                      that Fs is measured. */
    bool		   overSonetSdh;	   /* If DS1/E1 is over SONET/SDH, set to cAtTrue,
									  else set to cAtFalse */
    dword		   decSoakTime;	/* Declare soaking timer of failure */
    dword		   terSoakTime;	/* Terminate soaking timer of failure */
    }tSurDe1Conf;

/* IMA Link trouble type */
typedef enum eSurImaLinkTrbl
    {
    /* Defect/Failure definitions */
    cFmImaLinkLif,        	/* IMA Link Loss of IMA Frame failure */
    cFmImaLinkLods,       	/* IMA Link Out of Delay Synchronization failure */

    /* Failure */
    cFmImaLinkRfi,        	/* IMA Link Remote Failure Indicator */
    cFmImaLinkTxMisConn,   	/* IMA Link Tx mis-connected */
    cFmImaLinkRxMisConn,   	/* IMA Link Rx mis-connected */
    cFmImaLinkTxFault,     	/* IMA Link Tx Fault */
    cFmImaLinkRxFault,     	/* IMA Link Rx Fault */
    cFmImaLinkTxUnusableFe, /* IMA Link Tx Unusable FE */
    cFmImaLinkRxUnusableFe, /* IMA Link Rx Unusable FE */

    cFmImaLinkAllFail,    	/* IMA Link all failures */

    /* Defects */
    cFmImaLinkRdi,        	/* IMA Link Remote Defect Indicator */

    cFmImaLinkPhyDef, 		/* Physical link defect (LOS, OOF, LOF, AIS, LCD),
							   is used internally to calculate PM parameters */

    cFmImaLinkTxLsmChange, 		/* IMA Link Near-End Tx Link State Machine change state */
    cFmImaLinkRxLsmChange, 		/* IMA Link Near-End Rx Link State Machine change state */
    cFmImaLinkTxLsmChangeFe, 	/* IMA Link Far-End Tx Link State Machine change state */
    cFmImaLinkRxLsmChangeFe, 	/* IMA Link Far-End Rx Link State Machine change state */

    /* Performance Monitoring */
    cPmImaLinkTca         /* Threshold Crossing Alert of PDH DS1/E1 parameters */
    } eSurImaLinkTrbl;

/* State of IMA Link State Machine */
typedef enum eSurImaLinkState
	{
	cFmImaLinkNotInGrp 			= 0,	/* Not in group */
	cFmImaLinkUnusableNoReason 	= 1,	/* Unusable No Reason */
	cFmImaLinkUnusableFault 	= 2,	/* Unusable Fault */
	cFmImaLinkUnusableMisConn 	= 3,	/* Unusable Mis Connected */
	cFmImaLinkUnusableInhibited = 4,	/* Unusable Inhibited */
	cFmImaLinkUnusableFailed 	= 5,	/* Unusable Failed */
	cFmImaLinkUsable 			= 6,	/* Usable */
	cFmImaLinkActive 			= 7  	/* Active */
	} eSurImaLinkState;


/* IMA Link automatic messages inhibition */
typedef struct tSurImaLinkMsgInh
    {
    bool imaLink;   /* [true/false] Inhibiting automatic messages of IMA link */
    bool lif;   	/* [true/false] Inhibition of IMA Link Loss of IMA Frame
					   failure */
    bool lods;   	/* [true/false] Inhibition of IMA Link Out of Delay Synchronization
					   failure */
    bool rfi;   	/* [true/false] Inhibition of IMA Link Remote Failure Indicator
					   failure */
    bool txMisConn;   /* IMA Link Tx mis-connected */
    bool rxMisConn;   /* IMA Link Rx mis-connected */
    bool txFault;     /* IMA Link Tx Fault */
    bool rxFault;     /* IMA Link Rx Fault */
    bool txUnusableFe; /* IMA Link Tx Unusable FE */
    bool rxUnusableFe; /* IMA Link Rx Unusable FE */

    /* Performance Monitoring */
    bool tca;   	/* [true/false] Inhibition of TCA automatic message */

    }tSurImaLinkMsgInh;

/* IMA Link configuration */
typedef struct tSurImaLinkConf
    {
    dword		   decSoakTime;	/* Declare soaking timer of failure */
    dword		   terSoakTime;	/* Terminate soaking timer of failure */
    }tSurImaLinkConf;

/* IMA Group trouble type */
typedef enum eSurImaGrpTrbl
    {
    /* Failure definitions */
    cFmImaGrpStartupFe,   	/* IMA Group Start up FE */
    cFmImaGrpCfgAborted,   	/* IMA Group Config aborted */
    cFmImaGrpCfgAbortedFe, 	/* IMA Group Config aborted FE */
    cFmImaGrpInsuffLink,  	/* IMA Group Insufficient-Links */
    cFmImaGrpInsuffLinkFe, 	/* IMA Group Insufficient-Links FE */
    cFmImaGrpBlockedFe, 	/* IMA Group Blocked-FE */
    cFmImaGrpTimingMismatch, /* IMA Group Timing-Mismatch */

    cFmImaGrpAllFail,    	/* IMA Group all failures */

    /* Defects */
    cFmImaGrpGsmChange, 		/* IMA Group Near-End Group State Machine change state */
    cFmImaGrpGtsmChange, 		/* IMA Group Near-End Group Traffic State Machine change state */
    cFmImaGrpGsmChangeFe, 		/* IMA Group Far-End Group State Machine change state */

    /* Performance Monitoring */
    cPmImaGrpTca         	/* Threshold Crossing Alert of PDH DS1/E1 parameters */
    } eSurImaGrpTrbl;

/* State of IMA Group State Machine */
typedef enum eSurImaGrpGsmState
	{
	cFmImaGrpStatStartup 			= 0,	/* Start-up */
	cFmImaGrpStatStartupAck 		= 1,	/* Start-up ACK */
	cFmImaGrpStatCfgAbrtUnsuppM 	= 2,	/* Config-Aborted - Unsupported M */
	cFmImaGrpStatCfgAbrtInCompGrpSym = 3,	/* Config-Aborted - Incompatible Group Symmetry */
	cFmImaGrpStatCfgAbrtUnsuppImaVer = 4,	/* Config-Aborted - Unsupported IMA Version */
	cFmImaGrpStatCfgAbrtResvd1 		= 5,	/* Config-Aborted - Reserved 1 */
	cFmImaGrpStatCfgAbrtResvd2		= 6,	/* Config-Aborted - Reserved 2 */
	cFmImaGrpStatCfgAbrtOtherReason = 7,	/* Config-Aborted - Other reason */
	cFmImaGrpStatInsuffLink 		= 8,	/* Insufficient Link */
	cFmImaGrpStatBlocked 			= 9,	/* Blocked */
	cFmImaGrpStatOperational		= 10 	/* Operational */
	} eSurImaGrpGsmState;

/* State of IMA Group Traffic State Machine */
typedef enum eSurImaGrpGtsmState
	{
	cFmImaGrpStatGtsmDown 		= 0,	/* GTSM down is down state */
	cFmImaGrpStatGtsmUp 		= 1 	/* GTSM down is up state */
	} eSurImaGrpGtsmState;


/* IMA Group automatic messages inhibition */
typedef struct tSurImaGrpMsgInh
    {
    bool imaGrp;   		/* [true/false] Inhibiting automatic messages of IMA Group */

    bool startupFe;   	/* IMA Group Start up FE */
    bool cfgAborted;   	/* IMA Group Config aborted */
    bool cfgAbortedFe; 	/* IMA Group Config aborted FE */
    bool insuffLink;  	/* IMA Group Insufficient-Links */
    bool insuffLinkFe; 	/* IMA Group Insufficient-Links FE */
    bool blockedFe; 	/* IMA Group Blocked-FE */
    bool timingMismatch; /* IMA Group Timing-Mismatch */

    /* Performance Monitoring */
    bool tca;   	/* [true/false] Inhibition of TCA automatic message */

    }tSurImaGrpMsgInh;

/* IMA Group configuration */
typedef struct tSurImaGrpConf
    {
    dword		   decSoakTime;	/* Declare soaking timer of failure */
    dword		   terSoakTime;	/* Terminate soaking timer of failure */
    }tSurImaGrpConf;

/* PW trouble type */
typedef enum eSurPwTrbl
    {
    /* Defect/Failure definitions */
    cFmPwStrayPkt,        	/* PW Stray packet */
    cFmPwMalformedPkt,      /* PW Malformed Packet */
    cFmPwExcPktLossRate,    /* PW Excessive packet loss rate */
    cFmPwJitBufOverrun,     /* PW Jitter Buffer overrun */
    cFmPwRemotePktLoss,     /* PW Remote packet loss */
    cFmPwLofs,     			/* PW Loss of Frame State (MEF8)
							or Loss of Packet Synchronization (RFC4842) */

    cFmPwAllFail,    	/* PW all failures */

    /* Performance Monitoring */
    cPmPwTca         /* Threshold Crossing Alert of PDH DS1/E1 parameters */
    } eSurPwTrbl;

/* PW automatic messages inhibition */
typedef struct tSurPwMsgInh
    {
    bool pw;   			  	/* [true/false] Inhibiting automatic messages PW */
    bool strayPkt;   	  	/* [true/false] Inhibition of PW Stray packet failure */
    bool malformedPkt;    	/* [true/false] Inhibition of PW Malformed Packet failure */
    bool excPktLossRate;  	/* [true/false] Inhibition of PW Excessive packet loss rate failure */
    bool jitBufOverrun;   	/* [true/false] Inhibition of PW Buffer overrun */
    bool remotePktLoss;   	/* [true/false] Inhibition of PW Remote packet loss */
    bool lofs;   			/* [true/false] Inhibition of PW LOFS */

    /* Performance Monitoring */
    bool tca;   		  	/* [true/false] Inhibition of TCA automatic message */
    }tSurPwMsgInh;

/* PW configuration */
typedef struct tSurPwConf
    {
    dword		   decSoakTime;	/* Declare soaking timer of failure */
    dword		   terSoakTime;	/* Terminate soaking timer of failure */
    }tSurPwConf;

      
/* Alarm Type */
typedef enum eSurAlarmType
    {
    cSurAlarm,    /* Alarmed trouble */
    cSurNonAlarm, /* Non-Alarmed trouble */
    cSurNonReport /* Non-Reported trouble */
    }eSurAlarmType;

/* Alarm severity */
typedef enum eSurAlarmSev
    {
    cSurCr, /* Critical alarm */
    cSurMj, /* Major alarm */
    cSurMn  /* Minor alarm */
    }eSurAlarmSev;

/* Service affecting type */
typedef enum eSurServAff
    {
    cSurSa, /* Service-affection trouble */
    cSurNsa /* Non-service-affecting trouble */
    }eSurServAff;
    
/* Color */
typedef enum eSurColor
    {
    cSurColorRed,    /* Red color */
    cSurColorYellow, /* Yellow color */
    cSurColorAmber,  /* Amber color */
    cSurColorGreen,  /* Green color */
    cSurColorWhite   /* White color */
    }eSurColor;

/* Channel type */
typedef enum eSurChnType
    {
    cSurApslEng, /* Linear APS engine */
    cSurLine,    /* SONET/SDH line */
    cSurSts,     /* STS Path */
    cSurVt,      /* VT Path */
    cSurAu,      /* AU-n */
    cSurTu3,     /* TU-3 */
    cSurTu,      /* TU-m (TU11, TU12, TU2) */
    cSurDe3,     /* DS3/E3 channel */
    cSurDe1,      /* DS1/E1 channel */
    cSurImaLink,  	/* IMA Link */
    cSurImaGrp,  	/* IMA Group */
    cSurPw  		/* PW */
    }eSurChnType;

/* Surveillance device class */
typedef void* tSurDev;

/* Device handle */
typedef void tSurDevHdl;

/* Trouble designation */
typedef struct tSurTrblDesgn
    {
    eSurAlarmType alarmType;  /* Alarm type: alarmed or non-alarmed */
    eSurAlarmSev  alarmSev;   /* Alarm severity */
    eSurServAff   serviceAff; /* Service-affecting or non-service-affecting */
    }tSurTrblDesgn;

struct tFmFailInfo;

/* Device interface */
typedef struct tSurDevIntf
    {
    /* Failure notification function */
    void (*FailNotf)(tSurDevHdl        *pHdl,
                     eSurChnType        chnType, 
                     const void        *pChnId, 
                     const struct tFmFailInfo *pFailInfo);
    
    /* TCA notification function */
    void (*TcaNotf)(tSurDevHdl *hdl, eSurChnType chnType, const void *chnId, void * info);
    
    /* Function to get device description */
    void (*DevDes)(tSurDevHdl *pHdl, char *pDesBuf); 
    
    /* Function to get description of a channel */
    eSurRet (*ChnDes)(eSurChnType chnType,
                      const void *pChnId, 
                      char       *pDesBuf);
    
    /* Function to get channel's defect status. */
    eSurRet (*DefStatGet)(tSurDevHdl  *pHdl,
                          eSurChnType  chnType,
                          const void  *pChnId,
                          dword       *pDefStat);

    /* Function to get channel's anomaly counters. Type of "pAnomaly" is as 
       follow:
       - cSurLine, tSurLineAnomaly
       - cSurSts, tSurStsAnomaly
       - cSurVt, tSurVtAnomaly
       - cSurTu3, tSurTu3Anomaly 
       - cSurTu, tSurTuAnomaly 
       - cSurAu, tSurAuAnomaly 
       - cSurDe3, tSurDe3Anomaly  
       - cSurDe1, tSurDe1Anomaly */
    eSurRet (*AnomalyGet)(tSurDevHdl *pHdl,
                          eSurChnType chnType,
                          const void *pChnId,
                          void       *pAnomaly);
    }tSurDevIntf;

/* K thresholds */
typedef struct tSurKThres
    {
    /* SONET/SDH Section */
    word oc1Sec;       /* K value of OC-1 Section. Default value is 52 */
    word oc3Sec;       /* K value of OC-3 Section. Default value is 155 */
    word oc12Sec;      /* K value of OC-12 Section. Default value is 616 */
    word oc24Sec;      /* K value of OC-24 Section. Default value is 1,220 */
    word oc48Sec;      /* K value of OC-48 Section. Default value is 2,392 */
    word oc192Sec;     /* K value of OC-192 Section. Default value is 8,854 */
    word oc768Sec;     /* K value of OC-768 Section. Default value is 35,416 */

    /* SONET/SDH Line */
    word oc1Line;      /* K value of OC-1 Line. Default value is 51 */
    word oc3Line;      /* K value of OC-3 Line. Default value is 154 */
    word oc12Line;     /* K value of OC-12 Line. Default value is 615 */
    word oc24Line;     /* K value of OC-24 Line. Default value is 1,230 */
    word oc48Line;     /* K value of OC-48 Line. Default value is 2,459 */
    word oc192Line;    /* K value of OC-192 Line. Default value is 9,835 */
    word oc768Line;    /* K value of OC-768 Line. Default value is 39,340 */
                        
    /* STS Path */      
    word sts1;         /* K value of STS-1. Default value is 2,400 */
    word sts3c;        /* K value of STS-3c. Default value is 2,400 */
    word sts12c;       /* K value of STS-12c. Default value is 2,400 */
    word sts48c;       /* K value of STS-48c. Default value is 2,400 */
    word sts192c;      /* K value of STS-192c. Default value is 2,400 */
    word sts768c;      /* K value of STS-768c. Default value is 2,400 */
                        
    /* VT Path */
    word vt15;         /* K value of VT1.5. Default value is 600 */
    word vt2;          /* K value of VT2. Default value is 600 */
    word vt3;          /* K value of VT3. Default value is 600 */
    word vt6;          /* K value of VT6. Default value is 600 */
    
    /* DS3 */
    word ds3Line;      /* K value of DS3 line. Default value is 44 */
    word ds3PathPbit;  /* K value of DS3 path, used for SESP-P. Default value is 44 */
    word ds3PathCPbit; /* K value of DS3 path, used for SESCP-P. Default value is 44 */
    
    /* DS1 */
    word ds1Line;      /* K value of DS1 line. Default value is 1,544 */
    word ds1EsfPath;   /* K value of DS1-ESF path. Default value is 320 */
    word ds1SfPath;    /* K value of DS1-SF path. Default value is 4 */
    }tSurKThres;

/* Color profile */
typedef struct tSurColorProf
    {
    eSurColor criticalFail; /* Critical failure color. Default color is red. */
    eSurColor majorFail;    /* Major failure color. Default color is red. */
    eSurColor error;        /* Error color. Default color is red. */
    eSurColor danger;       /* Danger color. Default color is red. */
    eSurColor minorFail;    /* Minor failure color. Default color is yellow. */
    eSurColor nonAlarm;     /* Non-alarmed color. Default color is amber. */
    eSurColor caution;      /* Caution color. Default color is amber. */
    eSurColor warning;      /* Warning color. Default color is yellow. */
    eSurColor tmpMalfunc;   /* Temporary malfunction color. Default color is 
                               amber. */
    eSurColor success;      /* Success color. Default color is green. */
    eSurColor neutral;      /* Neutral condition color. Default color is white. */
    }tSurColorProf;

/* Time information */
typedef struct tSurTime
    {
    word year;   /* Current year */
    byte month;  /* [1..12] Month */
    byte day;    /* [1..31] Day */
    byte hour;   /* [0..23] Hour */
    byte minute; /* [0..59] Minute */
    byte second; /* [0..59] Second */
    }tSurTime;

/* Line anomaly counters */
typedef struct tSurLineAnomaly
    {
    /* Section */
    dword b1;   /* B1 errors */

    /* Line */
    dword b2;   /* B2 errors */
    dword reil; /* REI-L errors */
    }tSurLineAnomaly;

/* STS anomaly counters */
typedef struct tSurStsAnomaly
    {
    dword b3;    /* B3 errors */
    dword reip;  /* REI-P errors */
    dword piInc; /* Pointer positive adjustment - Pointer Interpreter */
    dword piDec; /* Pointer negative adjustment - Pointer Interpreter */
    dword pgInc; /* Pointer positive adjustment - Pointer Generator */
    dword pgDec; /* Pointer negative adjustment - Pointer Generator */
    }tSurStsAnomaly;

/* AU-n anomaly counters */
typedef tSurStsAnomaly tSurAuAnomaly;

/* TU-3 anomaly counters */
typedef tSurStsAnomaly tSurTu3Anomaly;

/* VT anomaly counters */
typedef struct tSurVtAnomaly
    {
    dword bipV;  /* BIP-V errors */
    dword reiV;  /* REI-V errors */
    dword piInc; /* Pointer positive adjustment - Pointer Interpreter */
    dword piDec; /* Pointer negative adjustment - Pointer Interpreter */
    dword pgInc; /* Pointer positive adjustment - Pointer Generator */
    dword pgDec; /* Pointer negative adjustment - Pointer Generator */
    }tSurVtAnomaly;

/* TU anomaly counters */
typedef tSurVtAnomaly tSurTuAnomaly;

/* DS3/E3 anomaly counters */
typedef struct tSurDe3Anomaly
    {
    dword bpv;      /* DS3/E3 Line Bipolar Violation counter */
    dword exz;      /* DS3/E3 Line Excessive Zero counter */
    dword pBitPar;  /* DS3/E3 Path P-bit Parity Error */
    dword cpBitPar; /* DS3/E3 Path CP-bit Parity Error 
                       (apply only for DS3 C-Bit Parity) */
    dword febe;     /* DS3/E3 Path Far-End Block Error 
                       (apply only for DS3 C-Bit Parity) */
    }tSurDe3Anomaly;

/* Performance Report Message */
typedef struct tSurDe1PrmMsg
    {
    bool newMsg; /* [true/false] Indicate that PRM message is the new one. This 
                                 field is cleared to false when all informations of 
                                 message are read. */
    bool g1;     /* [true/false] G1 bit. CRC Error Event = 1 */
    bool g2;     /* [true/false] G2 bit. 1 < CRC Error Events = 5 */
    bool g3;     /* [true/false] G3 bit. 5 < CRC Error Events = 10 */
    bool g4;     /* [true/false] G4 bit. 10 < CRC Error Events = 100 */
    bool g5;     /* [true/false] G5 bit. 100 < CRC Error Events = 319 */
    bool g6;     /* [true/false] G6 bit. CRC Error Events = 320 */
    bool se;     /* [true/false] SE bit. SEF Events = 1 */
    bool fe;     /* [true/false] FE bit. FE Events = 1 and SEF Event = 0 */
    bool lv;     /* [true/false] LV bit. BPV Events = 1 or EXZ Events = 1 */
    bool sl;     /* [true/false] SL bit. CS Events = 1 */
    }tSurDe1PrmMsg;
    
/* DS1/E1 anomaly counters */
typedef struct tSurDe1Anomaly
    {
    dword         bpvExz; /* DS1/E1 Line Bipolar Violation and Excessive Zero 
                             counters */
    dword         crc;    /* DS1/E1 Path Cyclic Redundancy Check counter */
    dword         fe;     /* DS1/E1 SF Path Framing (bit) Error counter */
    dword         cs;     /* DS1/E1 Path Controlled Slip counter */
    tSurDe1PrmMsg prmMsg; /* DS1/E1 ESF Path PRM message */
    }tSurDe1Anomaly;

/* IMA Link anomaly counters */
typedef struct tSurImaLinkAnomaly
    {
    dword iv;   /* Number of ICP violation cells */
    dword oif; 	/* OIF event */
    dword stuffRx;	/* Rx stuff cell */
    dword stuffTx;	/* Tx stuff cell */
    dword totalIcp;	/* Total ICP cell */
    } tSurImaLinkAnomaly;

/* PW anomaly counters */
typedef struct tSurPwAnomaly
    {
    dword 	rxRbitPkt;			/* Number of R-Bit packet */
    dword 	rxLbitPkt;			/* Number of L-Bit packet */
    dword 	rxPkt;				/* Number of packet received */
	dword	playoutJitBufUnderrun;	/* Play-out frame when buffer is underrun*/

    /* For PM only */
	dword	txPkt;					/* Transmitted packet */
	dword	missingPkt;				/* Missing packet */
	dword	misorderedPkt;			/* Mis-ordered packet, and successfully re-ordered */
	dword	misorderedPktDropped;	/* Mis-ordered packet dropped */
	dword	playoutJitBufOverrun;	/* Play-out buffer overrun */
	dword	lops;					/* Loss of Packet Synchronization */
	dword	malformedPkt;			/* Malformed packet */
	dword	strayPkt;				/* Stray packet */
	dword	remotePktLoss;			/* Remote packet loss */
	dword	txLbitPkt;				/* Transmitted L-Bit packet */
    } tSurPwAnomaly;


/* Linear APS defect information */
typedef struct tSurApslDef
    {
    tSurApslId   id;      /* Identifier */
    eSurApslTrbl defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurApslDef;

/* Line defect information */
typedef struct tSurLineDef
    {
    tSurLineId   id;      /* Identifier */
    eSurLineTrbl defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurLineDef;

/* STS defect information */
typedef struct tSurStsDef
    {
    tSurStsId    id;      /* Identifier */
    eSurStsTrbl  defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurStsDef;

/* AU-n defect information */
typedef tSurStsDef tSurAuDef;

/* TU3 defect information */
typedef struct tSurTu3Def
    {
    tSurTu3Id    id;      /* Identifier */
    eSurTu3Trbl  defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurTu3Def;

/* VT defect information */
typedef struct tSurVtDef
    {
    tSurVtId     id;      /* Identifier */
    eSurVtTrbl   defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurVtDef;

/* Tu-m defect information */
typedef tSurVtDef tSurTuDef;

/* DS3/E3 defect information */
typedef struct tSurDe3Def
    {
    tSurDe3Id    id;      /* Identifier */
    eSurDe3Trbl  defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurDe3Def;

/* DS1/E1 defect information */
typedef struct tSurDe1Def
    {
    tSurDe1Id    id;      /* Identifier */
    eSurDe1Trbl  defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurDe1Def;
        
/* IMA Link defect information */
typedef struct tSurImaLinkDef
    {
    tSurImaLinkId    id;      /* Identifier */
    eSurImaLinkTrbl  defect;  /* Defect type */
    bool         	 status;  /* Status */
    dword        	 time;    /* Time */
    }tSurImaLinkDef;

/* IMA Group defect information */
typedef struct tSurImaGrpDef
    {
    tSurImaGrpId     id;      /* Identifier */
    eSurImaGrpTrbl   defect;  /* Defect type */
    bool         	 status;  /* Status */
    dword        	 time;    /* Time */
    }tSurImaGrpDef;

/* PW defect information */
typedef struct tSurPwDef
    {
    tSurPwId     id;      /* Identifier */
    eSurPwTrbl   defect;  /* Defect type */
    bool         status;  /* Status */
    dword        time;    /* Time */
    }tSurPwDef;

/*--------------------------- Entries ----------------------------------------*/
void mSurMsgPrint(uint32 msgType, const char *format);
void mSurPrintc(uint32 surColor, const char *format) ;
void mSurDebug(uint32 msgType,const char *format);

/*------------------------------------------------------------------------------
This function is used to get version description of SUR module
------------------------------------------------------------------------------*/
public char* SurVersion(void);

/*------------------------------------------------------------------------------
This function is used to initialize Transmission Surveillance module. This
function must be called before calling any functions of this module
------------------------------------------------------------------------------*/
eSurRet SurInit(void);

/*------------------------------------------------------------------------------
This function is used to finalize Transmission Surveillance module. This
function must be called when this module is not used no more to release all
allocated resources and stop its tasks
------------------------------------------------------------------------------*/
eSurRet SurFinalize(void);

/*------------------------------------------------------------------------------
This function is used to add a new device to monitor failures and performance
------------------------------------------------------------------------------*/
eSurRet SurDevAdd(tSurDevHdl        *pHdl, 
                  tSurDev           *pDevice,
                  const tSurDevIntf *pIntf);

/*------------------------------------------------------------------------------
This function is used to remove a device from database, Fault Monitoring and 
Performance Monitoring of this device will be disabled.
------------------------------------------------------------------------------*/
eSurRet SurDevRemove(tSurDev *pDevice);

/*------------------------------------------------------------------------------
This function is used to get device handle
------------------------------------------------------------------------------*/
const tSurDevHdl *SurDevHdlGet(tSurDev device);

/*------------------------------------------------------------------------------
This function is used to set device interface.
------------------------------------------------------------------------------*/
eSurRet SurDevIntfSet(tSurDev device, const tSurDevIntf *pIntf);

/*------------------------------------------------------------------------------
This function is used to get device interface.
------------------------------------------------------------------------------*/
eSurRet SurDevIntfGet(tSurDev device, tSurDevIntf *pIntf);

/*------------------------------------------------------------------------------
This function is used to initialize Surveillance database of a device
------------------------------------------------------------------------------*/
eSurRet SurDevInit(tSurDev device);

/*------------------------------------------------------------------------------
This function is used to set state for a device (to make engine start or stop)
------------------------------------------------------------------------------*/
eSurRet SurDevStatSet(tSurDev device, eSurEngStat state);

/*------------------------------------------------------------------------------
This function is used to get state of a device to check if engine is running or 
stop
------------------------------------------------------------------------------*/
eSurRet SurDevStatGet(tSurDev device, eSurEngStat *pState);

/*------------------------------------------------------------------------------
This function is used to set defect service mode. 
- In service mode, engine will serve defects sent from other tasks and update 
  engine status
- In polling mode, engine will poll hardware periodically to detect new defects 
  and update engine status.
------------------------------------------------------------------------------*/
eSurRet SurDefServMdSet(tSurDev device, eSurDefServMd servMd);

/*------------------------------------------------------------------------------
This function is used to get defect service mode.
- In service mode, engine will serve defects sent from other tasks and update 
  engine status
- In polling mode, engine will poll hardware periodically to detect new defects 
  and update engine status.
------------------------------------------------------------------------------*/
eSurRet SurDefServMdGet(tSurDev device, eSurDefServMd *pServMd);

/*------------------------------------------------------------------------------
This function is used to set K thresholds for SONET/SDH and PDH
------------------------------------------------------------------------------*/
eSurRet SurKThresSet(tSurDev device, const tSurKThres *pKThres);

/*------------------------------------------------------------------------------
This function is used to get K thresholds of SONET/SDH and PDH
------------------------------------------------------------------------------*/
eSurRet SurKThresGet(tSurDev device, tSurKThres *pKThres);

/*------------------------------------------------------------------------------
This function is used to set color profile for a device
------------------------------------------------------------------------------*/
eSurRet SurColorProfSet(tSurDev device, const tSurColorProf *pColorProf);

/*------------------------------------------------------------------------------
This function is used to get color profile of a device
------------------------------------------------------------------------------*/
eSurRet SurColorProfGet(tSurDev device, tSurColorProf *pColorProf);

/*------------------------------------------------------------------------------
This function is used to set trouble designation for a device.
------------------------------------------------------------------------------*/
public eSurRet SurTrblDesgnSet(tSurDev              device, 
                               eSurChnType          chnType,
                               byte                 trblType, 
                               const tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This function is used to get trouble designation of a device.
------------------------------------------------------------------------------*/
public eSurRet SurTrblDesgnGet(tSurDev        device, 
                               eSurChnType    chnType,
                               byte           trblType, 
                               tSurTrblDesgn *pTrblDesgn);

/*------------------------------------------------------------------------------
This API is used to set engine state for a channel.
------------------------------------------------------------------------------*/
public eSurRet SurEngStatSet(tSurDev       device, 
                             eSurChnType   chnType, 
                             const void   *pChnId, 
                             eSurEngStat   engStat);
                      
/*------------------------------------------------------------------------------
This API is used to get engine state of a channel.
------------------------------------------------------------------------------*/
public eSurRet SurEngStatGet(tSurDev       device, 
                             eSurChnType   chnType, 
                             const void   *pChnId, 
                             eSurEngStat  *pStat);
                             
/*------------------------------------------------------------------------------
This function is used to provision a channel to monitor failures and performance. 
Note, channel's engine will not start automatically when it has just been 
provisioned.
------------------------------------------------------------------------------*/
public eSurRet SurChnProv(tSurDev device, eSurChnType chnType, const void *pChnId);

/*------------------------------------------------------------------------------
This function is used to deprovision a monitored channel. All resources of this 
channel will be deallocated.
------------------------------------------------------------------------------*/
public eSurRet SurChnDeProv(tSurDev device, eSurChnType chnType, const void *pChnId);

/*------------------------------------------------------------------------------
This function is used to set a channel's configuration.
------------------------------------------------------------------------------*/
public eSurRet SurChnCfgSet(tSurDev        device, 
                            eSurChnType    chnType, 
                            const void    *pChnId, 
                            const void    *pChnCfg);
                     
/*------------------------------------------------------------------------------
This function is used to get a channel's configuration.
------------------------------------------------------------------------------*/                     
public eSurRet SurChnCfgGet(tSurDev        device, 
                            eSurChnType    chnType, 
                            const void    *pChnId, 
                            void          *pChnCfg);

/*------------------------------------------------------------------------------
This API is used to set trouble notification inhibition for a channel. 
------------------------------------------------------------------------------*/
public eSurRet SurTrblNotfInhSet(tSurDev       device, 
                                 eSurChnType   chnType, 
                                 const void   *pChnId, 
                                 byte          trblType, 
                                 bool          inhibit);
                          
/*------------------------------------------------------------------------------
This API is used to get trouble notification inhibition of a channel. 
------------------------------------------------------------------------------*/
public eSurRet SurTrblNotfInhGet(tSurDev       device, 
                                 eSurChnType   chnType, 
                                 const void   *pChnId, 
                                 byte          trblType, 
                                 bool         *pInhibit);
                                 
/*------------------------------------------------------------------------------
This API is used to get a channel's trouble notification inhibitions.
------------------------------------------------------------------------------*/
public eSurRet SurChnTrblNotfInhInfoGet(tSurDev        device, 
                                         eSurChnType    chnType, 
                                         const void    *pChnId, 
                                         void          *pTrblInh);

/*------------------------------------------------------------------------------
This API is used to fetch a defect for the surveillance module when it is running
in service mode.
------------------------------------------------------------------------------*/
public eSurRet SurDefFetch(tSurDevHdl    *pDevHandle, 
                           eSurChnType    chnType, 
                           void          *pDefStat);

/*------------------------------------------------------------------------------
This API is used to set soaking time to declare or terminate the failure
------------------------------------------------------------------------------*/
eSurRet SurSoakTimeSet(tSurDev device, dword decSoakTime, dword terSoakTime);

/*------------------------------------------------------------------------------
This API is used to get soaking time to declare or terminate the failure
------------------------------------------------------------------------------*/
eSurRet SurSoakTimeGet(tSurDev device, dword *decSoakTime, dword *terSoakTime);

#endif /* _SUR_HEADER_ */

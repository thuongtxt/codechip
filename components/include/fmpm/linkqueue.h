/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : DATA STRUCTURE
 *
 * File        : linkqueue.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contains definitions of linked queue
 *               Features:
 *               + Queue is linked queue
 *               + Support multi-threads
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _LINK_QUEUE_HEADER_
#define _LINK_QUEUE_HEADER_

/*--------------------------- Includes -----------------------------------------*/
#include "surtypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef void* tLinkQueue;

/* Queue return codes */
typedef enum eQueRet
    {
    cQueSucc = 0, /* Successful return code */
    cQueEmpty,    /* Queue is underflow */
    cQueOver,     /* Queue is overflow */
    cQueErrSem,   /* Semaphore error */
    cQueErr       /* General error */
    }eQueRet;

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
This function is used to create a linked queue. This function must be called
before using any queue functions.
------------------------------------------------------------------------------*/
eQueRet LQueCreate(tLinkQueue *pQueue);

/*------------------------------------------------------------------------------
This function is used to destroy a linked queue. This function must be called
to make sure that all allocated memories are deallocated.
------------------------------------------------------------------------------*/
eQueRet LQueDestroy(tLinkQueue *pQueue);

/*------------------------------------------------------------------------------
This function is used to remove the first element and return the removed element
------------------------------------------------------------------------------*/
eQueRet LQueRmv(tLinkQueue queue, void *pData, dword size);

/*------------------------------------------------------------------------------
This function is used to insert a new element to queue
------------------------------------------------------------------------------*/
eQueRet LQueIns(tLinkQueue queue, void *pData, dword size);

/*------------------------------------------------------------------------------
This function is used to get (not remove) the first element of queue
------------------------------------------------------------------------------*/
eQueRet LQueFront(tLinkQueue queue, void *pData, dword size);

/*------------------------------------------------------------------------------
This function is used to get size of queue
------------------------------------------------------------------------------*/
eQueRet LQueSize(tLinkQueue queue, dword *pSize);

/*------------------------------------------------------------------------------
This function is used to flush (remove all data) queue
------------------------------------------------------------------------------*/
eQueRet LQueFlush(tLinkQueue queue);

#endif /* _LINK_QUEUE_HEADER_ */

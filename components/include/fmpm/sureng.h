/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : SUR - TRANSMISSION SURVEILLANCE
 *
 * File        : sureng.h
 *
 * Created Date: 05-Feb-09
 *
 * Description : This file contain engine definitions of Transmisstion
 *               Surveillance module
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _SUR_ENG_HEADER_
#define _SUR_ENG_HEADER_
/*--------------------------- Includes ---------------------------------------*/
/* Framework include */
#include <stdio.h>

/* Library */
#include "sortlist.h"
#include "linkqueue.h"

/* Surveillance include */
#include "sur.h"
#include "surfm.h"
#include "surpm.h"
#include "surutil.h"
#include "surdb.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mDecSoakExpr(defTime)
            mTerSoakExpr(defTime)
Purpose   : These macros are used to check if declare/terminate soaking timer
            expired
Inputs    : defTime                  - time of defect
Return    : true                     - if timer expired
------------------------------------------------------------------------------*/
#define mDecSoakExpr(defTime, decSoakTime)        (((dCurTime - (defTime)) >= (decSoakTime))  ? true : false)
#define mTerSoakExpr(defTime, terSoakTime)        (((dCurTime - (defTime)) >= (terSoakTime)) ? true : false)

/*------------------------------------------------------------------------------
Prototype : mTrblColorGet(pSurInfo, trblDesgn, pTrblColor)
Purpose   : These macros are used to get trouble color
Inputs    : pSurInfo                 - device database
            trblDesgn                - trouble designation
Outputs   : pTrblColor               - trouble color
------------------------------------------------------------------------------*/
#define mTrblColorGet(pSurInfo, trblDesgn, pTrblColor)                         \
    if ((trblDesgn).alarmType == cSurAlarm)                                    \
        {                                                                      \
        switch ((trblDesgn).alarmSev)                                          \
            {                                                                  \
            case cSurCr:                                                       \
                *(pTrblColor) = (pSurInfo)->pDb->mastConf.colorProf.criticalFail; \
                break;                                                         \
            case cSurMj:                                                       \
                *(pTrblColor) = (pSurInfo)->pDb->mastConf.colorProf.majorFail; \
                break;                                                         \
            case cSurMn:                                                       \
                *(pTrblColor) = (pSurInfo)->pDb->mastConf.colorProf.minorFail; \
                break;                                                         \
            default:                                                           \
                mSurDebug(cSurErrMsg, "Invalid alarm severity\n");             \
                break;                                                         \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        *(pTrblColor) = (pSurInfo)->pDb->mastConf.colorProf.nonAlarm;          \
        }

/*------------------------------------------------------------------------------
Prototype : mFailIndUpd(pSurInfo, pChnInfo, defName, failName, failChg)
Purpose   : This macro is used to update failure indication
Inputs    : pSurInfo				 - Surveillance device information
			pChnInfo                 - channel information
            defName                  - defect name (in structure)
            failName                 - failure name (in structure)
Outputs   : failChg                  - true if failure changed status
------------------------------------------------------------------------------*/
#define mFailIndUpd(pSurInfo, pChnInfo, defName, failName, failChg)                      \
    (failChg) = false;                                                         \
    if ((pChnInfo)->failure.failName.status != (pChnInfo)->status.defName.status)\
        {                                                                      \
        /* Set failure indication */                                           \
        if ((pChnInfo)->status.defName.status == true)                         \
            {                                                                  \
            if (mDecSoakExpr((pChnInfo)->status.defName.time, pSurInfo->pDb->mastConf.decSoakTime) == true)         \
                {                                                              \
                (pChnInfo)->failure.failName.status = true;                    \
                (pChnInfo)->failure.failName.time   = dCurTime;                \
                (failChg) = true;                                              \
                }                                                              \
            }                                                                  \
                                                                               \
        /* Clear failure indication */                                         \
        else                                                                   \
            {                                                                  \
            if (mTerSoakExpr((pChnInfo)->status.defName.time, pSurInfo->pDb->mastConf.terSoakTime) == true)         \
                {                                                              \
                (pChnInfo)->failure.failName.status = false;                   \
                (pChnInfo)->failure.failName.time   = dCurTime;                \
                (failChg) = true;                                              \
                }                                                              \
            }                                                                  \
        }

/*------------------------------------------------------------------------------
Prototype : mFailRept(pSurInfo, pChnId, pChnInfo, failInfo, trblName, trbl_Type)
Purpose   : This macro is used to notify failure
Inputs    : pSurInfo                 - device database
            pChnId                   - channel ID
            pChnInfo                 - channel information
            failInfo                 - failure information
            notify                   - true to notify failure
            trblName                 - trouble name (in structure)
            trbl_Type                - trouble type
------------------------------------------------------------------------------*/
#define mFailRept(pSurInfo, chnType, pChnId, failInd, history, failInfo, notify, trbl_Type, trbl_desgn)\
    /* Add failure to history and notify failure */                            \
    OsalMemCpy(&(trbl_desgn), &((failInfo).trblDesgn), sizeof(tSurTrblDesgn)); \
    mTrblColorGet((pSurInfo), trbl_desgn, &((failInfo).color));                \
    if ((failInd).status == false)                                             \
        {                                                                      \
        (failInfo).color = cSurColorGreen;                                     \
        }                                                                      \
    (failInfo).status   = (failInd).status;                                    \
    (failInfo).time     = (failInd).time;                                      \
    (failInfo).trblType = trbl_Type;                                           \
                                                                               \
    /* Add failure to history */                                               \
    if (ListAdd(history,                                                       \
                &(failInfo),                                                   \
                sizeof(tFmFailInfo)) != cListSucc)                             \
        {                                                                      \
        mSurDebug(cSurWarnMsg, "Cannot add failure to failure history\n");     \
        }                                                                      \
                                                                               \
    /* Notify */                                                               \
    if (((pSurInfo)->interface.FailNotf != null) && (notify))                  \
        {                                                                      \
        (pSurInfo)->interface.FailNotf((pSurInfo)->pHdl, chnType, (pChnId), &(failInfo));\
        }

/*------------------------------------------------------------------------------
Prototype : mCurPerAcc(pParm)
Purpose   : This macro is used to accumulate current period register when
            current second expired.
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mCurPerAcc(pParm)                                                      \
    if ((pParm)->inhibit == false)                                             \
        {                                                                      \
        (pParm)->curPer.value += (pParm)->curSec;                              \
        (pParm)->curDay.value += (pParm)->curSec;                              \
        }                                                                      \
    (pParm)->curSec = 0;

/*------------------------------------------------------------------------------
Prototype :
Purpose   : This macro is used to
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mTcaCheck(pParm, per_Tca, day_Tca, prePer_Tca, preDay_Tca)             \
    per_Tca    = false;                                                        \
    day_Tca    = false;                                                        \
    prePer_Tca = false;                                                        \
    preDay_Tca = false;                                                        \
                                                                               \
    if ((pParm)->perThres > 0)                                                 \
        {                                                                      \
        /* Period TCA */                                                       \
        if (((pParm)->perTca         == false) &&                              \
            ((pParm)->curPer.invalid == false) &&                              \
            ((pParm)->curPer.value   > (pParm)->perThres))                     \
            {                                                                  \
            per_Tca = true;                                                    \
            }                                                                  \
                                                                               \
        /* Previous Period TCA */                                              \
        if (((pParm)->prePerTca      == false) &&                              \
            ((pParm)->prePer.invalid == false) &&                              \
            ((pParm)->prePer.value   > (pParm)->perThres))                     \
            {                                                                  \
            prePer_Tca = true;                                                 \
            }                                                                  \
        }                                                                      \
                                                                               \
    if ((pParm)->dayThres > 0)                                                 \
        {                                                                      \
        /* Day TCA */                                                          \
        if (((pParm)->dayTca         == false) &&                              \
            ((pParm)->curDay.invalid == false) &                               \
            ((pParm)->curDay.value   > (pParm)->dayThres))                     \
            {                                                                  \
            day_Tca = true;                                                    \
            }                                                                  \
                                                                               \
        /* Previous Day TCA */                                                 \
        if (((pParm)->preDayTca      == false) &&                              \
            ((pParm)->preDay.invalid == false) &&                              \
            ((pParm)->preDay.value   > (pParm)->dayThres))                     \
            {                                                                  \
            preDay_Tca = true;                                                 \
            }                                                                  \
        }


/*------------------------------------------------------------------------------
Prototype : mStackDown(pParm)
Purpose   : This macro is used to make PM registers bahave as a push-down stack
            when current period timer expired
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mStackDown(pParm)                                                      \
    {                                                                          \
    byte _rec;                                                                 \
                                                                               \
    /* Push down recent registers */                                           \
    if ((pParm)->numRecReg > 0)                                                \
        {                                                                      \
        for (_rec = ((pParm)->numRecReg - 1); _rec >= 1; _rec--)               \
            {                                                                  \
            OsalMemCpy(&((pParm)->pRecent[_rec - 1]),                          \
                       &((pParm)->pRecent[_rec]),                              \
                       sizeof(tPmReg));                                        \
            }                                                                  \
                                                                               \
        /* Move the previous register to the first recent register */          \
        OsalMemCpy(&((pParm)->prePer), &((pParm)->pRecent[0]), sizeof(tPmReg));\
        }                                                                      \
                                                                               \
                                                                               \
    /* Move positive & negative adjustment register */                         \
    (pParm)->prePerPosAdj.value = (pParm)->curPerPosAdj.value;                 \
    (pParm)->prePerNegAdj.value = (pParm)->curPerNegAdj.value;                 \
                                                                               \
    /* Clear positive & negative adjustment register */                        \
    (pParm)->curPerPosAdj.value = 0;                                           \
    (pParm)->curPerNegAdj.value = 0;                                           \
                                                                               \
    /* Move current to previous TCA report flag */                             \
    (pParm)->prePerTca = (pParm)->perTca;                                      \
                                                                               \
    /* Clear current TCA report flag */                                        \
    (pParm)->perTca = false;                                                   \
                                                                               \
    /* Move the current period register to the previous register */            \
    OsalMemCpy(&((pParm)->curPer), &((pParm)->prePer), sizeof(tPmReg));        \
                                                                               \
    /* Clear the current period register */                                    \
    (pParm)->curPer.invalid = false;                                           \
    (pParm)->curPer.value   = 0;                                               \
    }
                                                                               
/*------------------------------------------------------------------------------
Prototype : mDayShift(pParm)
Purpose   : This macro is used to shift data of the current day register to the
            previous day register.
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mDayShift(pParm)                                                       \
    OsalMemCpy(&((pParm)->curDay), &((pParm)->preDay), sizeof(tPmReg));        \
    (pParm)->preDayTca      = (pParm)->dayTca;                                 \
    (pParm)->dayTca         = false;                                           \
    (pParm)->curDay.invalid = false;                                           \
    (pParm)->curDay.value   = 0;

/*------------------------------------------------------------------------------
Prototype : mNegInc(pParm)
            mPosInc(pParm)
Purpose   : These macros are used to increase negative and positive adjustment
            register
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mNegInc(pParm) (pParm)->curPerNegAdj.value += (pParm)->curSec
#define mPosInc(pParm) (pParm)->curPerPosAdj.value += (pParm)->curSec

/*------------------------------------------------------------------------------
Prototype : mNegClr(pParm)
            mPosClr(pParm)
Purpose   : These macros are used to clear negative and positive adjustment
            register
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mNegClr(pParm) (pParm)->curPerNegAdj.value = 0
#define mPosClr(pParm) (pParm)->curPerPosAdj.value = 0

/*------------------------------------------------------------------------------
Prototype : mCurPerNegAdj(pParm)
            mCurPerPosAdj(pParm)
            mPrePerNegAdj(pParm)
            mPrePerPosAdj(pParm)
Purpose   : These macros are used to adjust period registers when
            unavailable just changed status
Inputs    : pParm                    - PM parameter
------------------------------------------------------------------------------*/
#define mCurPerNegAdj(pParm)                                                   \
        (pParm)->curPer.value  -= (pParm)->curPerNegAdj.value;                 \
        (pParm)->curDay.value  -= (pParm)->curPerNegAdj.value;                 \
        (pParm)->curPerNegAdj.value = 0;
#define mCurPerPosAdj(pParm)                                                   \
    (pParm)->curPer.value += (pParm)->curPerPosAdj.value;                      \
    (pParm)->curDay.value += (pParm)->curPerPosAdj.value;                      \
    (pParm)->curPerPosAdj.value = 0;
#define mPrePerNegAdj(pParm)                                                   \
    (pParm)->prePer.value -= (pParm)->prePerNegAdj.value;                      \
    (pParm)->curDay.value -= (pParm)->prePerNegAdj.value;
#define mPrePerPosAdj(pParm)                                                   \
    (pParm)->prePer.value += (pParm)->prePerPosAdj.value;                      \
    (pParm)->curDay.value += (pParm)->prePerPosAdj.value

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Entriers ---------------------------------------*/
/* Timer information */
extern tSurTime curTime;   /* Current time in details */
extern dword    dCurTime;  /* Current time (in micro second) */
extern bool     blSecExpr; /* Second timer expired indication */
extern bool     blPerExpr; /* Period timer expired indication */
extern bool     blDayExpr; /* Day timer expired indication */

/* Invalid flags */
extern bool blInvlPer; /* Invalid period due to system time has been changed */
extern bool blInvlDay; /* Invalid day due to system time has been changed */

/*------------------------------------------------------------------------------
This function is used to check and report TCA for each channel's parameter
------------------------------------------------------------------------------*/
extern void SurTcaReport(tSurInfo    *pSurInfo,
                         eSurChnType  chnType,     
                         const void  *pChnId,
                         void        *pChnInfo,
                         byte         parmType,
                         tPmParm     *pParm,
                         bool         notify,
                         tPmTcaInfo  *pTcaInfo);

#endif /* _SUR_ENG_HEADER_ */


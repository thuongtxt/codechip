/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : atfmpm.h
 * 
 * Created Date: Mar 22, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFMPM_H_
#define _ATFMPM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtSdhLine.h"
#include "AtSdhChannel.h"
#include "AtSdhChannel.h"
#include "AtPdhChannel.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtSurAdaptPmRegType
    {
    cAtSurAdaptPmCurPer,       /* Current period register */
    cAtSurAdaptPmPrePer,       /* Previous period register */
    cAtSurAdaptPmCurDay,       /* Current day register */
    cAtSurAdaptPmPreDay,       /* Previous day register */
    cAtSurAdaptPmRecPer        /* Recent register 0. To indicate other registers, just
                                  start from this value. */
    }eAtSurAdaptPmRegType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtSurAdaptEngPoll(void);
eAtRet AtSurAdaptInit(AtDevice device);
eAtRet AtSurAdaptDelete(AtDevice device);

uint32 AtSurAdaptLineFailureGet(AtSdhLine line);
eAtRet AtSurAdaptLineSurEngineProvision(AtSdhLine line, eBool enable);
eAtRet AtSurAdaptLineSurEngineEnable(AtSdhLine line, eBool enable);
eBool AtSurAdaptLineSurEngineIsEnabled(AtSdhLine line);
uint32 AtSurAdaptLineFailureGet(AtSdhLine line);
uint32 AtSurAdaptLineFailureHistoryGet(AtSdhLine line);
uint32 AtSurAdaptLineFailureHistoryClear(AtSdhLine line);
uint32 AtSurAdaptLinePmValueGet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType);
uint32 AtSurAdaptLinePmValueGet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType);
eBool AtSurAdaptLinePmIsValid(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType);
void AtSurAdaptLinePmReset(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType);
eAtRet AtSurAdaptLinePeriodThresholdSet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType, uint32 threshold);
uint32 AtSurAdaptLinePeriodThresholdGet(AtSdhLine line, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhLinePmParam pmType);

eAtRet AtSurAdaptHoVcSurEngineProvision(AtSdhChannel channel, eBool enable);
eAtRet AtSurAdaptHoVcSurEngineEnable(AtSdhChannel channel, eBool enable);
eBool AtSurAdaptHoVcSurEngineIsEnabled(AtSdhChannel channel);
uint32 AtSurAdaptHoVcFailureGet(AtSdhChannel channel);
uint32 AtSurAdaptHoVcFailureHistoryGet(AtSdhChannel channel);
uint32 AtSurAdaptHoVcFailureHistoryClear(AtSdhChannel channel);
uint32 AtSurAdaptHoVcPmValueGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
void AtSurAdaptHoVcPmReset(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
eBool AtSurAdaptHoVcPmIsValid(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
eAtRet AtSurAdaptHoVcPmThresholdSet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType, uint32 threshold);
uint32 AtSurAdaptHoVcPmThresholdGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);

eAtRet AtSurAdaptTu3SurEngineProvision(AtSdhChannel channel, eBool enable);
eAtRet AtSurAdaptTu3SurEngineEnable(AtSdhChannel channel, eBool enable);
eBool AtSurAdaptTu3SurEngineIsEnabled(AtSdhChannel channel);
uint32 AtSurAdaptTu3FailureGet(AtSdhChannel channel);
uint32 AtSurAdaptTu3FailureHistoryGet(AtSdhChannel channel);
uint32 AtSurAdaptTu3FailureHistoryClear(AtSdhChannel channel);
uint32 AtSurAdaptTu3PmValueGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
void AtSurAdaptTu3PmReset(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
eBool AtSurAdaptTu3PmIsValid(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
eAtRet AtSurAdaptTu3PmThresholdSet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType, uint32 threshold);
uint32 AtSurAdaptTu3PmThresholdGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);

eAtRet AtSurAdaptTuSurEngineProvision(AtSdhChannel channel, eBool enable);
eAtRet AtSurAdaptTuSurEngineEnable(AtSdhChannel channel, eBool enable);
eBool AtSurAdaptTuSurEngineIsEnabled(AtSdhChannel channel);
uint32 AtSurAdaptTuFailureGet(AtSdhChannel channel);
uint32 AtSurAdaptTuFailureHistoryGet(AtSdhChannel channel);
uint32 AtSurAdaptTuFailureHistoryClear(AtSdhChannel channel);
uint32 AtSurAdaptTuPmValueGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
void AtSurAdaptTuPmReset(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
eBool AtSurAdaptTuPmIsValid(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
eAtRet AtSurAdaptTuPmThresholdSet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType, uint32 threshold);

eAtRet AtSurAdaptPdhDe3SurEngineProvision(AtPdhChannel channel, eBool enable);
eAtRet AtSurAdaptPdhDe3SurEngineEnable(AtPdhChannel channel, eBool enable);
eBool AtSurAdaptPdhDe3SurEngineIsEnabled(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe3FailureGet(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe3FailureHistoryGet(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe3FailureHistoryClear(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe3PmValueGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType);
void AtSurAdaptPdhDe3PmReset(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType);
eBool AtSurAdaptPdhDe3PmIsValid(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType);
eAtRet AtSurAdaptPdhDe3PmThresholdSet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType, uint32 threshold);
uint32 AtSurAdaptPdhDe3PmThresholdGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe3PmParam pmType);

eAtRet AtSurAdaptPdhDe1SurEngineProvision(AtPdhChannel channel, eBool enable);
eAtRet AtSurAdaptPdhDe1SurEngineEnable(AtPdhChannel channel, eBool enable);
eBool AtSurAdaptPdhDe1SurEngineIsEnabled(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe1FailureGet(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe1FailureHistoryGet(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe1FailureHistoryClear(AtPdhChannel channel);
uint32 AtSurAdaptPdhDe1PmValueGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType);
void AtSurAdaptPdhDe1PmReset(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType);
eBool AtSurAdaptPdhDe1PmIsValid(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType);
eAtRet AtSurAdaptPdhDe1PmThresholdSet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType, uint32 threshold);
uint32 AtSurAdaptPdhDe1PmThresholdGet(AtPdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePdhDe1PmParam pmType);

eAtRet AtSurAdaptPwSurEngineProvision(AtPw pw, eBool enable);
eAtRet AtSurAdaptPwSurEngineEnable(AtPw pw, eBool enable);
eBool AtSurAdaptPwSurEngineIsEnabled(AtPw pw);
uint32 AtSurAdaptPwFailureGet(AtPw pw);
uint32 AtSurAdaptPwFailureHistoryGet(AtPw pw);
uint32 AtSurAdaptPwFailureHistoryClear(AtPw pw);
uint32 AtSurAdaptPwPmValueGet(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType);
void AtSurAdaptPwPmReset(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType);
eBool AtSurAdaptPwPmIsValid(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType);
eAtRet AtSurAdaptPwPmThresholdSet(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType, uint32 threshold);
uint32 AtSurAdaptTuPmThresholdGet(AtSdhChannel channel, eAtSurAdaptPmRegType pmRegType, eAtSurEngineSdhPathPmParam pmType);
uint32 AtSurAdaptPwPmThresholdGet(AtPw pw, eAtSurAdaptPmRegType pmRegType, eAtSurEnginePwPmParam pmType);

#endif /* _ATFMPM_H_ */


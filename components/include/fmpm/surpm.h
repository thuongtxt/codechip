/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Block       : PM - PERFORMANCE MONITORING
 *
 * File        : surpm.h
 *
 * Created Date: 03-Jan-08
 *
 * Description : This file contain definitions of Performance Monitoring
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _PM_HEADER_
#define _PM_HEADER_

/*--------------------------- Define -----------------------------------------*/
#define cPmNumRecentReg              16     /* Default number of recent registers */
#define cPmMaxNumRecentReg           64     /* Maximum number of recent registers */
#define cPmMaxNumReg                 72     /* Maximum number of possible register in
                                               a parameter */
#define cPmPerThresVal               100    /* Default period threshold value */
#define cPmDayThresVal               1000   /* Default day threshold value */                                            
                                           
#define cPmSecParmType               0
#define cPmNeLineParmType            1
#define cPmFaLineParmType            2
/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Prototype : mPmParmInit(pPmParm)
            mPmParmFinal(pPmParm)

Purpose   : These macros are used to initialize/fianlize one PM parameter.

Inputs    : pPmParm                  - PM parameter

Outputs   : pPmParm                  - PM parameter

Return    : None
------------------------------------------------------------------------------*/
#define mPmParmInit(pPmParm)                                                   \
    OsalMemInit(pPmParm, sizeof(tPmParm), 0);                                  \
    (pPmParm)->pRecent = OsalMemAlloc(cPmNumRecentReg * sizeof(tPmReg));       \
    OsalMemInit((pPmParm)->pRecent, cPmNumRecentReg * sizeof(tPmReg), 0);      \
    (pPmParm)->numRecReg = cPmNumRecentReg;
#define mPmParmFinal(pPmParm)                                                  \
    OsalMemFree((pPmParm)->pRecent);                                           \
    OsalMemInit(pPmParm, sizeof(tPmParm), 0);

/*------------------------------------------------------------------------------
Prototype : mPmRegReset(pReg)

Purpose   : This macro is used to reset a register.

Inputs    : pReg                     - register

Outputs   : None

Return    : None
------------------------------------------------------------------------------*/
#define mPmRegReset(pReg)                                                       \
    {                                                                           \
    (pReg)->value   = cSurResetVal;                                             \
    (pReg)->invalid = false;                                                    \
    }

/*------------------------------------------------------------------------------
Prototype : define mPmRecRegCheck(numRecReg)

Purpose   : This macro is used to check if the number of recent registers is valid.

Inputs    : numRecReg                - number of recent registers

Outputs   : None

Return    : None
------------------------------------------------------------------------------*/
#define mPmRecRegCheck(numRecReg)                                               \
    {                                                                           \
    if (numRecReg > cPmMaxNumRecentReg)                                         \
        {                                                                       \
        return cSurErrInvlParm;                                                 \
        }                                                                       \
    }
    
/*------------------------------------------------------------------------------
Prototype : mPmRecRegSet(pParm, recReg)

Purpose   : This macro is used to set number of recent registers of a parameter.

Inputs    : pParm                    - parameter
            recReg                   - number of recent registers

Outputs   : None

Return    : None
------------------------------------------------------------------------------*/
#define mPmRecRegSet(pParm, recReg)                                             \
    {                                                                           \
    /* Deallocate memory of the old structure */                                \
    OsalMemFree((pParm)->pRecent);                                              \
                                                                                \
    /* Allocate memory for the new structure */                                 \
    (pParm)->pRecent = OsalMemAlloc(recReg * sizeof(tPmReg));                   \
                                                                                \
    /* Init new memory */                                                       \
    OsalMemInit((pParm)->pRecent, recReg * sizeof(tPmReg), 0);                  \
                                                                                \
    /* Set number of recent registers */                                        \
    (pParm)->numRecReg = recReg;                                                \
    }
    
/*------------------------------------------------------------------------------
Prototype : mPmDefParmSet(pParm, recReg, perThresVal, dayThresVal)

Purpose   : This macro is used to set default values for a parameter.

Inputs    : pParm                    - parameter

Outputs   : None

Return    : None
------------------------------------------------------------------------------*/
#define mPmDefParmSet(pParm, perThresVal, dayThresVal)                          \
    {                                                                           \
    (pParm)->perThres = perThresVal;                                            \
    (pParm)->dayThres = dayThresVal;                                            \
    mPmRecRegSet((pParm), cPmNumRecentReg);                                     \
    }

/*--------------------------- Typedefs ---------------------------------------*/
/* SONET/SDH Section/Line parameters */
typedef enum ePmLineParm
    {
    /* Section */
    cPmSecSefs,         /* Section Severely Errored Framing Seconds. It is a 
                           count of the seconds during which an SEF defect was 
                           present. */
    cPmSecCv,           /* Section Coding Violations. Count of BIP errors (B1) 
                           detected at the Section layer. */
    cPmSecEs,           /* Section Errored Second. Count of the number of 
                           seconds during which at least one Section layer BIP 
                           error was detected or an SEF or LOS defect was 
                           present. */
    cPmSecSes,          /* Section Severely Errored Seconds. Count of the 
                           seconds during which K or more Section layer BIP 
                           errors were detected or an SEF or LOS defect was 
                           present. K values are settable. */
    cPmSecAllParm,      /* All Section parameters */
    
    /* Line Near-End */
    cPmLineCv,          /* Near-End Line Coding Violation. Count of BIP errors 
                           (B2) detected at the Line layer. */
    cPmLineEs,          /* Near-End Line Errored Second. Count of the seconds 
                           during which at least one Line layer BIP error was 
                           detected or an AIS-L defect (or a lower-layer, 
                           traffic-related, near-end defect) was present. */
    cPmLineSes,         /* Near-End Line Severely Errored Second. Count of the 
                           seconds during which K or more Line layer BIP errors 
                           were detected or an AIS-L defect (or a lower-layer, 
                           traffic-related, near-end defect) was present. 
                           K values are settable. */
    cPmLineUas,         /* Near-End Line Unavailable Second. Count of the 
                           seconds during which the Line was considered 
                           unavailable. A line becomes unavailable at the onset 
                           of 10 consecutive seconds that qualify as SES-Ls and 
                           continue to be unavailable until the onset of 10 
                           consecutive seconds that do not qualify as SES-Ls. */
    cPmLineFc,          /* Near-End Line Failure Count. Count of the numbers of       
                           near-end line failure events. A failure event begins       
                           when the AIS-L failure (or a lower-layer, 
                           traffic-related, near-end failure) is declared,            
                           and ends when the failure is cleared. The failure          
                           event that begins in one period and ends in another        
                           period is counted only in the period in which it 
                           begins. */
    
    cPmLinePsc,         /* Protection Switching Count. For a working line, the 
                           PSC parameter is a count of the number of times that 
                           service has been switched from the monitored line to 
                           the protection line, plus the number of timers it has 
                           been switched back to the working line. For the 
                           protection line, it is a count of the number of times 
                           that service has been switched from any working line 
                           to the protection line, plus the number of times 
                           service has been switched back to a working line. 
                           This parameter is only applicable if line level 
                           protection switching is used. */
    cPmLinePsd,         /* For a working line, the PSD parameter is a count of 
                           the seconds that service was being carried on the 
                           protection line. For the protection line, it is a 
                           count of the seconds that the line was being used to 
                           carry service. This parameter is only applicable if 
                           revertive line level protection switching is used. */
    cPmLineNeAllParm,   /* All Line Near-End parameters */
    
    /* Line Far-End */
    cPmLineCvLfe,       /* Far-End Line Coding Violation. Count of the number of 
                           BIP errors detected by the far-end LTE and reported 
                           back to the near-end LTE using the REI-L 
                           indication. */
    cPmLineEsLfe,       /* Far-End Line Errored Second. Count of the seconds 
                           during which at least one Line BIP error was reported 
                           by the far-end LTE or an RDI-L defect was present. */
    cPmLineSesLfe,      /* Far-End Line Severely Errored Second. Count of the 
                           seconds during which K or more Line BIP errors were 
                           reported by the far-end LTE or an RDI-L defect was 
                           present. */
    cPmLineUasLfe,      /* Far-End Line Unavailable Second. Count of the seconds 
                           during which the Line is considered unavailable at 
                           the far-end. A line is considered unavailable at the 
                           far-end at the onset of 10 consecutive seconds that 
                           qualify as SES-LFE, and continues to considered 
                           unavailable until the onset of 10 consecutive seconds 
                           that do not qualify as SES-LFE. */
    cPmLineFcLfe,       /* Far-End Line Failure Count. Count of the number of 
                           far-end line failure events. A failure event begins 
                           when the RFI-L failure is declared, and ends when 
                           the RFI-L failure is cleared. A failure event that 
                           begins in one period and ends in another period is 
                           counted only in the period in which it begins. */
    cPmLineFeAllParm    /* All Line Far-End parameters */
    }ePmLineParm;

/* STS Path parameters */
typedef enum ePmStsParm 
    {
    /* STS Path Near-End */
    cPmStsCv,           /* Near-end STS Path Coding Violation. A count of BIP 
                           errors detected at the STS Path layer */
    cPmStsEs,           /* Near-end STS Path Errored Second. A count of the 
                           seconds during which (at any point during the second) 
                           at least one STS Path BIP error was detected, or an 
                           AIS-P defect (or a lower-layer, traffic-related, 
                           near-end defect), an LOP-P defect or, if the STS PTE 
                           monitoring the path supports ERDI-P for that path, an 
                           UNEQ-P or TIM-P defect was present. */
    cPmStsSes,          /* Near-end STS Path Severely Errored Second. A count 
                           of the seconds during which K or more STS Path BIP 
                           errors were detected, or an AIS-P defect (or a 
                           lower-layer, traffic-related, near-end defect), 
                           an LOP-P defect or, if the STS PTE monitoring the 
                           path supports ERDI-P for that path, an UNEQ-P or 
                           TIM-P defect was present. The number of BIP errors 
                           that cause a second to be considered an SES-P may 
                           need to be settable. */
    cPmStsUas,          /* Near-end STS Path Unavailable Second. A count of the 
                           seconds during which the STS Path was considered 
                           unavailable. An STS Path becomes unavailable at the 
                           onset of 10 consecutive seconds that qualify as 
                           SES-Ps, and continues to be unavailable until the 
                           onset of 10 consecutive seconds that do not qualify 
                           as SES-P */
    cPmStsFc,           /* Near-end STS Path Failure Count. A count of the 
                           number of near-end STS Path failure events. A failure 
                           event begins when an AIS-P failure (or a lower-layer, 
                           traffic-related, near-end failure), an LOP-P failure 
                           or, if the STS PTE monitoring the path supports 
                           ERDI-P for that path, an UNEQ-P or TIM-P failure is 
                           declared. Note that functionally, an AIS-P failure 
                           will be declared either by receiving (and timing) an 
                           AIS-P signal from another NE, or by receiving 
                           (and timing) an internally generated AIS-P signal 
                           from LTE in the same NE where the STS PTE resides. */
    cPmStsPpjcPdet,     /* Positive Pointer Justification Count - STS Path 
                           Detected. A count of the positive pointer 
                           justifications (i.e., valid increment operations) 
                           detected on a particular path in an incoming SONET 
                           signal. */
    cPmStsNpjcPdet,     /* Negative Pointer Justification Count - STS Path 
                           Detected. A count of the negative pointer 
                           justifications (i.e., valid decrement operations) 
                           detected on a particular path in an incoming SONET 
                           signal. */
    cPmStsPpjcPgen,     /* Positive Pointer Justification Count. A count of the 
                           positive pointer justifications (i.e., increment 
                           operations) generated for a particular path to 
                           reconcile the frequency of the SPE with the local 
                           clock. */
    cPmStsNpjcPgen,     /* Negative Pointer Justification Count - STS Path 
                           Generated. A count of the negative pointer 
                           justifications (i.e., decrement operations) generated 
                           for a particular path to reconcile the frequency of 
                           the SPE with the local clock. */
    cPmStsPjcDiff,      /* Pointer Justification Count Difference - STS Path. 
                           The absolute value of the difference between the net 
                           number of detected pointer justification counts and 
                           the net number of generated pointer justification 
                           counts. That is, PJCDiff-P is equal to 
                           |(PPJC-PGen  NPJC-PGen)  (PPJC-PDet  NPJC-PDet)|. */
    cPmStsPjcsPdet,     /* Pointer Justification Count Seconds - STS Path Detect. 
                           A count of the one-second intervals containing one or 
                           more PPJC-PDet or NPJC-PDet. */
    cPmStsPjcsPgen,     /* Pointer Justification Count Seconds - STS Path 
                           Generate. A count of the one-second intervals 
                           containing one or more PPJC-PGen or NPJC-PGen. */
    cPmStsNeAllParm,    /* All STS Path Near-End parameters */
    
    /* STS Path Far-End */
    cPmStsCvPfe,        /* Far-end STS Path Coding Violation. A count of the 
                           number of BIP errors detected by the far-end STS PTE 
                           and reported back to the near-end STS PTE using the 
                           REI-P indication in the STS Path overhead */
    
    cPmStsEsPfe,        /* Far-end STS Path Errored Second. A count of the 
                           seconds during which (at any point during the second) 
                           at least one STS Path BIP error was reported by the 
                           far-end STS PTE (using the REI-P indication), a 
                           one-bit RDI-P defect was present, or (if ERDI-P is 
                           supported) an ERDI-P Server or Connectivity defect 
                           was present. */
    cPmStsSesPfe,       /* Far-end STS Path Severely Errored Second. A count of 
                           the seconds during which K or more STS Path BIP 
                           errors were reported by the far-end STS PTE, a 
                           one-bit RDI-P defect was present, or (if ERDI-P 
                           is supported) an ERDI-P Server or Connectivity defect 
                           was present. The number of reported far-end BIP 
                           errors that cause a second to be considered an 
                           SES-PFE may need to be settable. */
    cPmStsUasPfe,       /* Far-end STS Path Unavailable Second. A count of the 
                           seconds during which the STS Path is considered 
                           unavailable at the far end. An STS Path is considered 
                           unavailable at the far end at the onset of 10 
                           consecutive seconds that qualify as SES-PFEs, and 
                           continues to be considered unavailable until the 
                           onset of 10 consecutive seconds that do not qualify 
                           as SES-PFEs. */
    cPmStsFcPfe,        /* Far-end STS Path Failure Count. A count of the number 
                           of far-end STS Path failure events. A failure event 
                           begins when a one-bit RFI-P failure, or (if ERDI-P is 
                           supported) an ERFI-P Server or Connectivity failure 
                           is declared. */
    cPmStsFeAllParm     /* All STS Path Far-End parameters */
    }ePmStsParm;

/* TU3 Path parameters */
typedef ePmStsParm ePmTu3Parm;

/* AU-n Path parameters */
typedef ePmStsParm ePmAuParm;

/* VT Path parameters */
typedef enum ePmVtParm
    {
    /* VT Path Near-End */
    cPmVtCv,            /* Near-end VT Path Coding Violation. A count of BIP 
                           errors detected at the VT Path layer (i.e., using 
                           bits 1 and 2 of the V5 byte in the incoming VT Path 
                           overhead). */
    cPmVtEs,            /* Near-end VT Path Errored Second. A count of the 
                           seconds during which (at any point during the second) 
                           at least one VT Path BIP error was detected, or an 
                           AIS-V defect (or a lower-layer, traffic-related, 
                           near-end defect), an LOP-V defect or, if the VT PTE 
                           monitoring the path supports ERDI-V for that path, 
                           an UNEQ-V defect was present. */
    cPmVtSes,           /* Near-end VT Path Severely Errored Second. A count 
                           of the seconds during which K or more VT Path BIP 
                           errors were detected, or an AIS-V defect (or a lower
                           -layer, traffic-related, near-end defect), an LOP-V 
                           defect or, if the VT PTE monitoring the path 
                           supports ERDI-V for that path, an UNEQ-V defect was 
                           present.  The number of BIP errors that cause a 
                           second to be considered an SES-V may need to be 
                           settable */
    cPmVtUas,           /* Near-end VT Path Unavailable Second. A count of the 
                           seconds during which the VT Path was considered 
                           unavailable.  A VT Path becomes unavailable at the 
                           onset of 10 consecutive seconds that qualify as 
                           SES-Vs, and continues to be unavailable until the 
                           onset of 10 consecutive seconds that do not qualify 
                           as SES-Vs. */
    cPmVtFc,            /* Near-end VT Path Failure Count. A count of the number 
                           of near-end VT Path failure events.  A failure 
                           event begins when an AIS-V failure (or a lower-layer
                           , traffic-related, near-end failure), an LOP-V 
                           failure or, if the VT PTE monitoring the path 
                           supports ERDI-V for that path, an UNEQ-V failure is 
                           declared.  The failure event ends when these 
                           failures are cleared.  A failure event that begins 
                           in one period and ends in another period is counted 
                           only in the period in which it begins.  Note that 
                           functionally, an AIS-V failure will be declared 
                           either by receiving (and timing) an AIS-V signal 
                           from another NE, or by receiving (and timing) an 
                           internally generated AIS-V signal from STS PTE in 
                           the same NE where the VT PTE resides. */
    cPmVtPpjcVdet,      /* Positive Pointer Justification Count - VT Path 
                           Detected. A count of the positive pointer 
                           justifications (i.e., valid increment operations) 
                           detected on a particular path in an incoming SONET 
                           signal. */
    cPmVtNpjcVdet,      /* Negative Pointer Justification Count - VT Path 
                           Detected. A count of the negative pointer 
                           justifications (i.e., valid decrement operations) 
                           detected on a particular path in an incoming SONET 
                           signal. */
    cPmVtPpjcVgen,      /* Positive Pointer Justification Count - VT Path 
                           Generated. A count of the positive pointer 
                           justifications (i.e., increment operations) 
                           generated for a particular path to reconcile the 
                           frequency of the SPE with the local clock. */
    cPmVtNpjcVgen,      /* Negative Pointer Justification Count - VT Path 
                           Generated. A count of the negative pointer 
                           justifications (i.e., decrement operations) 
                           generated for a particular path to reconcile the 
                           frequency of the SPE with the local clock. */
    cPmVtPjcDiff,       /* Pointer Justification Count Difference - VT Path. 
                           The absolute value of the difference between the 
                           net number of detected pointer justification counts 
                           and the net number of generated pointer 
                           justification counts.  That is, PJCDiff-V is equal 
                           to |(PPJC-VGen  NPJC-VGen)  (PPJC-VDet  NPJC-VDet)|. */
    cPmVtPjcsVdet,      /* Pointer Justification Count Seconds - VT Path Detect. A 
                           count of the one-second intervals containing one or 
                           more PPJC-VDet or NPJC-VDet */
    cPmVtPjcsVgen,      /* Pointer Justification Count Seconds - VT Path 
                           Generate. A count of the one-second intervals 
                           containing one or more PPJC-VGen or NPJC-VGen */
    cPmVtNeAllParm,     /* All VT Path Near-End parameters */
    
    /* VT Path Far-End */
    cPmVtCvVfe,         /* Far-end VT Path Coding Violation. A count of the 
                           number of BIP errors detected by the far-end VT PTE 
                           and reported back to the near-end VT PTE using the 
                           REI-V indication in the VT Path overhead. */
    cPmVtEsVfe,         /* Far-end VT Path Errored Second.  A count of the 
                           seconds during which (at any point during the second
                           ) at least one VT Path BIP error was reported by 
                           the far-end VT PTE (using the REI-V indication), a 
                           one-bit RDI-V defect was present, or (if ERDI-V is 
                           supported) an ERDI-V Server or Connectivity defect 
                           was present. */
    cPmVtSesVfe,        /* Far-end VT Path Severely Errored Second. A count of 
                           the seconds during which K or more VT Path BIP 
                           errors were reported by the far-end VT PTE, a one-bit 
                           RDI-V defect was present, or (if ERDI-V is supported
                           ) an ERDI-V Server or Connectivity defect was 
                           present. The number of reported far-end BIP errors 
                           that cause a second to be considered an SES-VFE may 
                           need to be settable. */
    cPmVtUasVfe,        /* Far-end VT Path Unavailable Second. A count of the 
                           seconds during which the VT Path is considered 
                           unavailable at the far end.  A VT Path is 
                           considered unavailable at the far end at the onset 
                           of 10 consecutive seconds that qualify as SES-VFEs, 
                           and continues to be considered unavailable until 
                           the onset of 10 consecutive seconds that do not 
                           qualify as SES-VFEs. */
    cPmVtFcVfe,         /*  Far-end VT Path Failure Count. A count of the 
                           number of far-end VT Path failure events.  A 
                           failure event begins when a one-bit RFI-V failure, 
                           or (if ERDI-V is supported) an ERFI-V Server or 
                           Connectivity failure is declared. The failure event 
                           ends when the RFI-V failure is cleared.  A failure 
                           event that begins in one period and ends in another 
                           period is counted only in the period in which it 
                           begins. */
    cPmVtFeAllParm      /* All VT Path Far-End parameters */
    }ePmVtParm;    

/* TU-m parameters */
typedef ePmVtParm ePmTuParm;

/* DS3/E3 parameters */
typedef enum ePmDe3Parm
    {
    /* DS3/E3 Near-End Line */
    cPmDe3CvL,          /* Code Violations-Line. This parameter is a count of both 
                           BPVs and EXZs occurring over the accumulation period. */
    cPmDe3EsL,          /* Errored Seconds-Line. This parameter is a count of 
                           seconds containing one or more BPVs, one or more 
                           EXZs, or one or more LOS defects. */
    cPmDe3EsaL,         /* Errored Second-Line type A. This parameter is a count 
                           of 1-second intervals containing one BPV or EXZ and 
                           no LOS defect. */
    cPmDe3EsbL,         /* Errored Second-Line type B. This parameter is a count 
                           of 1-second intervals containing more than one but 
                           less than 44 BPV plus EXZ and no LOS defect. */
    cPmDe3SesL,         /* Severely Errored Seconds-Line. This parameter is a 
                           count of seconds during which BPVs plus EXZs exceed 
                           44, or one or more LOS defects occur BPVs that are 
                           part of the zero substitution code are excluded. */
    cPmDe3LossL,        /* LOS Second. This parameter is a count of 1-second 
                           intervals containing one or more LOS defects.   */
    
    /* DS3 Near-End Path */
    cPmDe3CvpP,         /* Code Violations-Path, P-bit. CVP-P is a count of 
                           P-bit parity check CVs. */
    cPmDe3CvcpP,        /* Code Violations-Path, CP-Bit. Apply only for the 
                           C-Bit Parity application. CVCP-P is the count of 
                           CP-bit parity errors occurring in the accumulation 
                           period. */
    cPmDe3EspP,         /* Errored Seconds-Path, P-bit. ESP-P is a count of 
                           seconds containing one or more P-bit parity errors, 
                           one or more SEF defects, or one or more AIS defects. */
    cPmDe3EsapP,        /* Errored Second type A, P-bit parity. This parameter 
                           is the count of 1-second intervals containing exactly 
                           one P-bit parity errors and no SEF or AIS-L defect. 
                           It is defined for both DS3 application. */
    cPmDe3EsbpP,        /* Errored Second type B, P-bit parity. This parameter 
                           is the count of 1-second intervals containing more 
                           than one but less than 44 P-bit parity errors and no 
                           SEF or AIS-L defect. It is defined for both DS3 
                           application. */
    cPmDe3EscpP,        /* Errored Seconds-Path, CP-bit. For the C-Bit Parity 
                           application, the ESCP-P parameter is a count of 
                           seconds containing one or more CP-bit parity errors
                           , one or more SEF defects, or one or more AIS defects. */
    cPmDe3EsacpP,       /* Errored Second type A, CP-bit parity. This parameter 
                           is the count of 1-second intervals containing one 
                           CP-bit parity errors and no SEF or AIS-L defect. It 
                           is defined for C-bit parity DS3 application. */
    cPmDe3EsbcpP,       /* Errored Second type B, CP-bit parity. This parameter 
                           is the count of 1-second intervals containing more 
                           than one but less than 44 CVCP-Ps and no SEF or 
                           AIS-L defect. It is defined for C-bit parity DS3 
                           application. */
    cPmDe3SespP,        /* Severely Errored Seconds-Path, P-bit. SESP-P is a 
                           count of seconds containing more than 44 P-bit 
                           parity violations, one or more SEF defects, or one 
                           or more AIS defects. */
    cPmDe3SescpP,       /* Severely Errored Seconds-Path, CP-bit. For the 
                           C-Bit Parity application, the SESCP-P parameter is 
                           a count of seconds containing more than 44 CP-bit 
                           parity errors, one or more SEF defects, or one or 
                           more AIS defects. */
    cPmDe3SasP,         /* SEF/AIS Seconds-Path. This parameter is a count of 
                           seconds containing one or more SEF defects or one 
                           or more AIS defects. */
    cPmDe3AissP,        /* AIS Seconds-Path.  This parameter is a count of 
                           1-second intervals containing one or more AIS defects. */
    cPmDe3UaspP,        /* Unavailable Seconds-Path, caused by SESP-P.  This 
                           parameter is a count of 1-second intervals during 
                           which the DS3 path is unavailable. */
    cPmDe3UascpP,       /* Unavailable Seconds-Path, caused by SESCP-P.  This 
                           parameter is a count of 1-second intervals during 
                           which the DS3 path is unavailable. */
    cPmDe3FcP,          /* Near-end Failure Count-Path. This parameter is a 
                           count of the number of occurrences of near-end path 
                           failure events, with the failure event defined as 
                           follows: a near-end path failure event begins when 
                           either an LOF or AIS failure is declared and ends 
                           when both LOF and AIS failures are clear. */
    cPmDe3NeAllParm,    /* All DS3/E3 Near-End parameters */
    
    /* DS3 Far-End */
    cPmDe3CvcpPfe,      /* Code Violation. This parameter is counted when the 
                           three FEBE bits in an M-frame are not all set to 1.  */
    cPmDe3EscpPfe,      /* Errored Second. This parameter is a count of 1-second 
                           intervals containing one or more M-frames with the 
                           three FEBE bits not all set to one, or one or more 
                           far-end SEF/AIS defects. */
    cPmDe3EsacpPfe,     /* Error Second type A. This parameter is a count of 
                           1-second intervals containing one M-frames with the 
                           three FEBE bits not all set to one and no far-end 
                           SEF/AIS defects. */
    cPmDe3EsbcpPfe,     /* Error Second type B. This parameter is a count of 
                           1-second intervals containing more than one but less 
                           than 44 M-frames with the three FEBE bits not all set 
                           to one and no far-end SEF/AIS defects. */
    cPmDe3SescpPfe,     /* Severely Errored Second. This parameter is a count 
                           of 1-second intervals containing one or more than 
                           44 M-frames with the three FEBE bits not all set to 
                           one, or one or more far-end SEF/AIS defects. */
    cPmDe3SascpPfe,     /* SEF/AIS Second. This parameter is a count of 1-second 
                           intervals containing one or more far-end SEF/AIS 
                           defects. */
    cPmDe3UascpPfe,     /* Unavailable Second. This parameter is a count of 
                           1-second intervals during which the DS3 path is 
                           unavailable. */
    cPmDe3FcPfe,        /* Far-end Failure Count-Path. This parameter is a count 
                           of the number of occurrences of far-end path 
                           failure events, with the far-end failure event 
                           defined as follows: a far-end path failure event 
                           begins when an RAI failure is declared and ends 
                           when the RAI failure is cleared. The FCCP-PFE 
                           applies only to the C-Bit Parity application. */
    cPmDe3FeAllParm     /* All DS3/E3 Far-End parameters */
    }ePmDe3Parm;

/* DS1/E1 parameters */
typedef enum ePmDe1Parm
    {
    /* DS1/E1 Line Near-End */
    cPmDe1CvL,          /* Code Violations-Line.  A count of BPVs and EXZs 
                           occurring over the accumulation period. */
    cPmDe1EsL,          /* Errored Seconds-Line.  A count of seconds during which 
                           one or more of the following has occurred: BPVs, 
                           EXZs, and LOSs. */
    cPmDe1SesL,         /* Severely Errored Seconds-Line. A count of seconds 
                           during which 1544 or more BPVs or EXZs, or one or 
                           more LOS defects, have occurred. */
    cPmDe1LossL,        /* Loss of Signal Seconds-Line. A count of 1-second 
                           intervals containing one or more LOS defects. */

    /* DS1/E1 Path Near-End */
    cPmDe1CvP,          /* Code Violations-Path. For DS1-ESF paths, this 
                           parameter is a count of detected CRC-6 errors. For 
                           DS1-SF paths, since no redundancy check mechanism 
                           is incorporated into the frame format to detect 
                           errors in payload bits, FEs are used.  Thus, for DS1
                           -SF paths, this parameter is a count of detected FEs. */
    cPmDe1EsP,          /* Errored Seconds-Path. In the case of DS1 ESF, this 
                           parameter is a count of 1-second intervals 
                           containing one or more CRC-6 errors, or one or more 
                           CS events, or one or more SEF or AIS defects.  In 
                           the case of DS1 SF, this parameter is a count of 
                           1-second intervals containing one or more FE events
                           , or one or more CS events, or one or more SEF or 
                           AIS defects.  */
    cPmDe1EsaP,         /* Errored Second type A. This parameter applies to DS1 
                           ESF paths only. It is a count of one second intervals 
                           with exactly one CRC-6 error, and no SEF or AIS 
                           defects. */
    cPmDe1EsbP,         /* Errored Second type B. This parameter applies to DS1 
                           ESF paths only. It is a count of one second intervals 
                           with no less than 2 and not more than 319 CRC-6 error, 
                           and no SEF or AIS defects. */
    cPmDe1SesP,         /* Severely Errored Seconds-Path. For DS1-ESF paths, 
                           this parameter is a count of seconds during which 
                           at least one of the following has occurred: 320 or 
                           more CRC-6 errors, or one or more SEF or AIS defects.  
                           For DS1-SF paths, an SES is a second containing 
                           either the occurrence of eight FEs (if both Ft and 
                           Fs bits are measured), four FEs (if only Ft bits 
                           are measured), or one or more SEF or AIS defects. */
    cPmDe1AissP,        /* Alarm Indication Signal Seconds-Path. This 
                           parameter is a count of seconds containing one or 
                           more AIS defects. */
    cPmDe1AissCiP,      /* AIS-CI second. This parameter is a count of 1-second 
                           intervals containing one or more AIS-CI defects. */
    cPmDe1SasP,         /* Severely Errored Frame/Alarm Indication Signal-Path
                           . This parameter is a count of seconds containing 
                           one or more SEFs or one or more AIS defects. */
    cPmDe1CssP,         /* Controlled Slip Seconds-Path. This parameter is a 
                           count of seconds during which a CS has occurred. 
                           Counts of CSs can be accurately made only in the 
                           path terminating NE of the DS1 signal where the CS 
                           takes place. */
    cPmDe1UasP,         /* Unavailable Seconds-Path. This parameter is a count 
                           of 1-second intervals during which the DS1 path is 
                           unavailable.  */
    cPmDe1FcP,          /* Near-end Failure Count-Path.  This parameter is a 
                           count of the number of occurrences of near-end path 
                           failure events, with the failure event defined as 
                           follows: a near-end path failure event begins when 
                           either an LOF or AIS failure is declared and ends 
                           when both LOF and AIS failures are clear. */
    cPmDe1NeAllParm,    /* All DS1 Near-End parameters */

    /* DS1/E1 Far-End */
    cPmDe1EsLfe,        /* Errored Seconds-Line. This parameter is a count of 
                           1-second PRM intervals containing an LV=1. */
    cPmDe1SefsPfe,      /* Severely Errored Frame-Path. This parameter is a 
                           count of 1- second PRM intervals containing an SE=1. */
    cPmDe1CvPfe,        /* Code Violation-path. This parameter is a count of 
                           number of far-end CVs occurring during the 
                           accumulation period. */
    cPmDe1EsPfe,        /* Errored Seconds-Path. This parameter is a count of 
                           1-second PRM intervals containing a G1=1, G2=1, G3=1
                           , G4=1, G5=1, G6=1, SE=1, SL=1, or an RAI signal.  */
    cPmDe1EsaPfe,       /* Errored Second type A. This parameter is a count of 
                           1-second PRM intervals containing a G1 = 1 and 
                           SE = 0. */
    cPmDe1EsbPfe,       /* Errored Second type B. This parameter is a count of 
                           1-second PRM intervals containing an SE = 0 and a 1 
                           in any of following: G2, G3, G4, or G5. */
    cPmDe1SesPfe,       /* Severely Errored Seconds-Path. This parameter is a 
                           count of 1-second PRM intervals containing a G6=1, 
                           SE=1, or an RAI signal. */
    cPmDe1CssPfe,       /* Controlled Slip Seconds-Path. This parameter is a 
                           count of 1- second PRM intervals containing an SL=1. */
    cPmDe1FcPfe,        /* Failure Count-Path. This parameter is a count of 
                           the number of occurrences of far-end path failure 
                           events, with the failure event defined as follows: 
                           a far-end path failure indication begins when an 
                           RAI failure is declared and ends when the RAI 
                           failure is cleared. */
    cPmDe1UasPfe,       /* Unavailable Seconds-Path. This parameter is a count 
                           of 1- second intervals during which the DS1 path is 
                           unavailable. */
    cPmDe1FeAllParm     /* All DS1 Far-End parameters */
    }ePmDe1Parm;

/* IMA link parameters */
typedef enum ePmImaLinkParm
	{
    /* IMA Link Near-End */
    cPmImaLinkIv,     	/* ICP Violations: count of errored, invalid or missing ICP cells,
						   except during seconds where a SES-IMA or UAS-IMA condition is reported */

    cPmImaLinkOif,		/* Count OIF anomalies, except during SES-IMA or UAS-IMA conditions */

    cPmImaLinkSes,		/* Count of one second intervals containing >= 30% of the ICP cells
						   counted as IV-IMAs, or one or more link defects (e.g.,
						   LOS, OOF/LOF, AIS or LCD), LIF, LODS defects during non-UASIMA
						   condition */
    cPmImaLinkUas,		/* Unavailable seconds at NE: NE unavailability begins at the onset of 10
						   contiguous SES-IMA including the first 10 seconds to enter the UAS-IMA
						   condition, and ends at the onset of 10 contiguous seconds with no SES-IMA,
						   excluding the last 10 seconds to exit the UAS-IMA condition */

    cPmImaLinkUusTx,		/* Tx Unusable seconds: count of Tx Unusable seconds at the Tx NE LSM */
    cPmImaLinkUusRx,		/* Rx Unusable seconds: count of Rx Unusable seconds at the Rx NE LSM */
    cPmImaLinkFcTx,		/* NE Tx link failure count: count of NE Tx link failure alarm condition
						   entrances. The possible NE Tx link failure alarm conditions are:
						   Tx-Mis-Connected and Tx-Fault. */

    cPmImaLinkFcRx,		/* NE Rx link failure count: count of NE Rx link failure alarm condition entrances.
						   The possible NE Rx link failure alarm conditions are: LIF, LODS, Rx-Mis-
						   Connected and Rx-Fault. */

    cPmImaLinkStufTx,		/* Count of Tx stuffing events except during SES-IMA
						   or UAS-IMA conditions */
    cPmImaLinkStufRx,		/* Count of Rx stuffing events except during SES-IMA
						   or UAS-IMA conditions */

    cPmImaLinkNeAllParm,    /* All IMA Link NE parameters */

    /* IMA Link Far-End */
    cPmImaLinkSesfe,		/* Count of one second intervals containing one or more RDI-IMA
						   defects, except during UAS-IMA-FE conditions. */

    cPmImaLinkUasfe,		/* Unavailable seconds at FE: FE unavailability begins at the onset of 10
						   contiguous SES-IMA-FE including the first 10 seconds to enter the UAS-IMAFE
						   condition, and ends at the onset of 10 contiguous seconds with no SES-IMAFE,
						   excluding the last 10 seconds to exit the UAS-IMA-FE condition */

    cPmImaLinkUusTxfe,	/* Tx Unusable seconds at FE: count seconds with Tx Unusable
						   indications from the Tx FE LSM */

    cPmImaLinkUusRxfe,	/* Rx Unusable seconds at FE: count of seconds with Rx Unusable
						   indications from the Rx FE LSM */

    cPmImaLinkFcTxfe,	/* FE Tx link failure count: count of FE Tx link failure alarm condition
						   entrances. The only possible link failure alarm condition is Tx-Unusable-FE. */

    cPmImaLinkFcRxfe,	/* FE Rx link failure count: count of FE Rx link failure alarm condition
						   entrances. The possible FE Rx link failure alarm conditions are:
						   RFI-IMA, and Rx-Unusable-FE */

    cPmImaLinkFeAllParm    /* All IMA Link FE parameters */
	} ePmImaLinkParm;

/* IMA Group parameters */
typedef enum ePmImaGrpParm
	{
    /* IMA Group Near-End */
    cPmImaGrpUas,		/* Count of one second intervals where the GTSM is Down */
    cPmImaGrpFc,		/* NE Group Failure count: count of NE group failure condition entrances.
						   The possible NE group failure alarm conditions are: Config-Aborted and
						   Insufficient-Links */

    cPmImaGrpNeAllParm,    /* All IMA Group NE parameters */

    /* IMA Group Far-End */
    cPmImaGrpFcfe,		  /* FE Group Failure count: count of FE group failure condition entrances. The
						     possible FE group failure alarm conditions are: Start-up-FE, Config-Aborted-
						     FE, Insufficient-Links-FE, and Blocked-FE. */

    cPmImaGrpFeAllParm    /* All IMA Group FE parameters */
	} ePmImaGrpParm;

/* PW parameters */
typedef enum ePmPwParm
	{
    cPmPwEs,	/* Errored Seconds */
    cPmPwSes,	/* Severely Errored Second */
    cPmPwUas,	/* Unavailable Second */

	cPmPwTxPkt,							/* Transmitted packet */
	cPmPwRxPkt,                         /* Received packet */
	cPmPwMissingPkt,                    /* Missing packet */
	cPmPwMisorderedPkt,                 /* Mis-ordered packet, and successfully re-ordered */
	cPmPwMisorderedPktDrop,             /* Mis-ordered packet dropped */
	cPmPwPlayoutJitBufOverrun,          /* Play-out buffer overrun */
	cPmPwPlayoutJitBufUnderrun,         /* Play-out buffer underrun*/
	cPmPwLops,                          /* Loss of Packet Synchronization */
	cPmPwMalformedPkt,                  /* Malformed packet */
	cPmPwStrayPkt,                      /* Stray packet */
	cPmPwRemotePktLoss,                 /* Remote packet loss: is indicated by reception
										   of packets with their R bit set */
	cPmPwTxLbitPkt,                     /* Transmitted L-Bit packet */

	cPmPwNeAllParm,    /* All NE PW PM parameters */

    cPmPwFeStartParm,  /* Reserved only */
    cPmPwFeAllParm     /* Reserved only */
	} ePmPwParm;

/* PM Register type */
typedef enum ePmRegType
    {
    cPmCurPer,       /* Current period register */
    cPmPrePer,       /* Previous period register */
    cPmCurDay,       /* Current day register */
    cPmPreDay,       /* Previous day register */
    cPmRecPer,       /* Recent register 0. To indicate other registers, just 
                        start from this value. */
    cPmAllReg = cPmRecPer + cPmMaxNumRecentReg      /* All registers */

    }ePmRegType;

/* TCA information */
typedef struct tPmTcaInfo
    {
    tSurTrblDesgn trblDesgn; /* Trouble designation */
    eSurColor     color;     /* Trouble color */
    byte          parm;      /* Parameter. It's value must be in ePmLineParm,  
                                ePmStsParm, ePmTu3Parm, ePmAuParm, ePmVtParm, 
                                ePmDe3Parm, ePmDe1Parm depend on channel type */
    ePmRegType    reg;       /* Register type */
    dword         value;     /* Register's value */
    dword         thres;     /* Register's threshold */
    dword         time;      /* Time when TCA is declared */
    }tPmTcaInfo;

/* PM register */
typedef struct tPmReg
    {
    dword value;   /* [Full] Register value */
    bool  invalid; /* [true/false] Invalid flag */
    }tPmReg;

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
This API is used to reset a channel's parameter's resigter.
------------------------------------------------------------------------------*/
public eSurRet PmRegReset(tSurDev       device, 
                          eSurChnType   chnType, 
                          const void   *pChnId, 
                          byte          parm, 
                          ePmRegType    regType);

                   
/*------------------------------------------------------------------------------
This API is used to get a channel's parameter's register's value.
------------------------------------------------------------------------------*/                  
public eSurRet PmRegGet(tSurDev         device, 
                        eSurChnType     chnType, 
                        const void     *pChnId, 
                        byte            parm, 
                        ePmRegType      regType, 
                        tPmReg         *pRetReg);
  
/*------------------------------------------------------------------------------
This API is used to get TCA history of a channel.
------------------------------------------------------------------------------*/               
public tSortList PmTcaHisGet(tSurDev           device,
                                   eSurChnType       chnType, 
                                   const void       *pChnId);
                    
/*------------------------------------------------------------------------------
This API is used to clear TCA history of a channel.
------------------------------------------------------------------------------*/
public eSurRet PmTcaHisClear(tSurDev        device, 
                             eSurChnType    chnType, 
                             const void    *pChnId);

/*------------------------------------------------------------------------------
This API is used to enable/disable a channel's PM accumlate inhibition.
------------------------------------------------------------------------------*/
public eSurRet PmParmAccInhSet(tSurDev      device, 
                               eSurChnType  chnType, 
                               const void  *pChnId, 
                               byte         parm, 
                               bool         inhibit);

/*------------------------------------------------------------------------------
This API is used to get a channel's accumulate inhibition mode.
------------------------------------------------------------------------------*/
public eSurRet PmParmAccInhGet(tSurDev      device, 
                               eSurChnType  chnType, 
                               const void  *pChnId, 
                               byte         parm, 
                               bool        *pInhibit);

/*------------------------------------------------------------------------------
This API is used to set threshold for a channel's paramter's register.
------------------------------------------------------------------------------*/
public eSurRet PmRegThresSet(tSurDev        device, 
                             eSurChnType    chnType, 
                             const void    *pChnId, 
                             byte           parm, 
                             ePmRegType     regType, 
                             dword          thresVal);

/*------------------------------------------------------------------------------
This API is used to get threshold of a channel's paramter's register.
------------------------------------------------------------------------------*/
public eSurRet PmRegThresGet(tSurDev        device, 
                             eSurChnType    chnType, 
                             const void    *pChnId, 
                             byte           parm, 
                             ePmRegType     regType, 
                             dword         *pThresVal);
                                                                                                                                                     
/*------------------------------------------------------------------------------
This API is used set the number of recent registers for a channel's parameter.
------------------------------------------------------------------------------*/
public eSurRet PmParmNumRecRegSet(tSurDev       device, 
                                  eSurChnType   chnType, 
                                  const void   *pChnId, 
                                  byte          parm, 
                                  byte          numRecReg);
                           
/*------------------------------------------------------------------------------
This API is used get the number of recent registers of a channel's parameter.
------------------------------------------------------------------------------*/
public eSurRet PmParmNumRecRegGet(tSurDev       device, 
                                  eSurChnType   chnType, 
                                  const void   *pChnId, 
                                  byte          parm, 
                                  byte         *pNumRecReg);

/*------------------------------------------------------------------------------
This API is used get get a time stamp of performance parameters
------------------------------------------------------------------------------*/
public eSurRet PmTimeStampGet(tSurDev       device,
							  eSurChnType   chnType,
							  const void    *pChnId,
							  ePmRegType    timeType,
							  dword         *startTime,
							  dword         *endTime);

/*------------------------------------------------------------------------------
This API is used to get total time from starting monitoring Performance in seconds
------------------------------------------------------------------------------*/
public eSurRet PmTotalTimeGet(tSurDev       device,
							  eSurChnType   chnType,
							  const void    *pChnId,
							  dword         *totalTime);

#endif /* _PM_HEADER_ */

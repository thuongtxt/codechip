namespace eval EP {
	variable m_initialized 0
	
	# Make the procedures visible to namespace import
	namespace export initialize configurePortsWithMode configureAllPortsWithMode 
	
	# static: set master configuration for EP
	proc setMasterForDevice { deviceId } {
		# Master DS1/E1 Configuration
		# 		             devid     raimd  ataos rxclken txclken rxmute exlosen sysexlosen txclkcnl
		atsdk::ep de1 master $deviceId single dis 	dis 	dis 	dis    dis     dis        dis
		
		# Clock DS1/E1 Configuration
		#				    devid     ds1clkout e1clkout inclk recclken recportid
		atsdk::ep de1 clock $deviceId 1ds1 	    1e1 	 1e1   dis 	    1
	}
	
	# Initialize EP
	proc initialize {} {
		# Reset 
		atsdk::ep de1 reset
		
		# Set master configuration for two devices
		setMasterForDevice 1
		setMasterForDevice 2
		
		# Not to initialize more than one time
		variable m_initialized
		set m_initialized 1
	}
	
	# Set PDH DS1/E1 port to specified mode and set its default configuration
	# @param portId Port ID
	# @param mode Mode.
	# 			  - "e1": E1
	# 			  - "ds1": DS1
	proc configurePortsWithMode { portList mode } {
		# Initialize EP if it has not
		variable m_initialized
		if {$m_initialized == 0} initialize
		
		if {$mode == "ds1"} {
			atsdk::ep de1 chanel $portList en en  b8zs t1-100tp1x 100ohm
		} else {
			atsdk::ep de1 chanel $portList en en hdb3 e1-75coax 75ohm
		}
		
		atsdk::ep de1 jitterattenuator $portList tx 10hz 64bit
		atsdk::ep de1 loopback $portList noloop
	}
	
	proc allPorts {} { return 1-32 }
	
	# Set all ports to input mode with default configuration
	# @param mode port mode
	# 			  - "e1": E1
	# 			  - "ds1": DS1
	proc configureAllPortsWithMode { mode } {
		configurePortsWithMode [allPorts] $mode
	}
}

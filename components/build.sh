# !/bin/sh

originPath=$PATH
toolPath="/opt/tools"

# ==============================================================================
# Update for ZTE PTN projects
# ============================================================================== 
# Compile PPC-8540
export PATH=$toolPath/zte_nj_ptn/ppc8540_gcc4.1.2_glibc2.5.0/bin/:$PATH
export CROSS_COMPILE=powerpc-8540-linux-gnu
make clean
make COMPILE_PREFIX=powerpc-8540-linux-gnu- AT_COMPONENTS_SPECIFIC_FLAGS="-fPIC -msoft-float"
`cp -f atcomponents.a customers/zte_ptn/atcomponents_powerpc_8540.a` 
export PATH=$originPath

# Compile MIPS-64
export PATH=$toolPath/zte_nj_ptn/mips64_gcc4.5.2_glibc2.13.0_multi/bin/:$PATH
export CROSS_COMPILE=mips_n64-softfloat-linux-gnu
make clean
make COMPILE_PREFIX=mips_n64-softfloat-linux-gnu-
`cp -f atcomponents.a customers/zte_ptn/atcomponents_mips64.a`
export PATH=$originPath

# Compile MIPS-32
export PATH=$toolPath/zte_nj_ptn/mips64_gcc4.5.2_glibc2.13.0_multi/bin/:$PATH
make clean
make COMPILE_PREFIX=mips_n32-hardfloat-linux-gnu- AT_COMPONENTS_SPECIFIC_FLAGS="-fpic -pipe -G 0 -mno-branch-likely -ffunction-sections -fdata-sections -fomit-frame-pointer -fno-builtin -ffreestanding -DXLP_3XX_2XX -march=xlp"
`cp -f atcomponents.a customers/zte_ptn/atcomponents_mips32.a`
export PATH=$originPath

# ==============================================================================
# Update for H3C projects
# ============================================================================== 
# Compile for MIPS-32
export PATH=$toolPath/eldk-5.2.1/mips/sysroots/i686-eldk-linux/usr/bin:$toolPath/eldk-5.2.1/mips/sysroots/i686-eldk-linux/usr/bin/mips32-linux:$PATH
make clean
make COMPILE_PREFIX=mips-linux- AT_COMPONENTS_SPECIFIC_FLAGS="-mno-branch-likely -fno-pic -mno-abicalls -msoft-float -pipe -fdollars-in-identifiers -fno-builtin -fno-common -I$toolPath/eldk-5.2.1/mips/sysroots/mips32-linux/usr/include -mips2"
`cp -f atcomponents.a customers/h3c/atcomponents_mips32.a`
export PATH=$originPath

# Compile for MIPS-64
export PATH=$toolPath/zte_nj_ptn/mips64_gcc4.5.2_glibc2.13.0_multi/bin/:$PATH
export CROSS_COMPILE=mips_n64-softfloat-linux-gnu
make clean
make COMPILE_PREFIX=mips_n64-softfloat-linux-gnu- AT_COMPONENTS_SPECIFIC_FLAGS="-fsigned-char -Wvla -D_DEBUG_VERSION -DMODULE -mlong-calls -isystem -DCONFIG_DISTRIBUTED -D__KERNEL__ -DVMLINUX_LOAD_ADDRESS=0xffffffff80000000 -DDATAOFFSET=0 -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-builtin -fno-common -ffunction-sections -fdata-sections -falign-functions=16 -fno-tree-dominator-opts -fno-tree-switch-conversion -fno-jump-tables -fno-inline-small-functions -Wframe-larger-than=2048 -fno-stack-protector -mabi=64 -G 0 -mno-abicalls -fno-pic -pipe -msoft-float -ffreestanding -DCVMX_ENABLE_POW_CHECKS=0 -march=xlp -Wa,--trap -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign"
`cp -f atcomponents.a customers/h3c/atcomponents_mips64.a`
export PATH=$originPath

# ==============================================================================
# Update for H3C Beijing projects
# ==============================================================================
# Compile for PPC-4080
export PATH=$toolPath/eldk-5.3/powerpc-e500v2/sysroots/i686-eldk-linux/usr/bin/ppce500v2-linux-gnuspe/:$PATH

make clean
make COMPILE_PREFIX=powerpc-e500v2- AT_COMPONENTS_SPECIFIC_FLAGS="-mcpu=e500mc -mtune=e500mc"
`powerpc-e500v2-strip --strip-unneeded atcomponents.a`
`cp -f atcomponents.a customers/h3c/atcomponents_powerpc_4080_debug.a`

make clean
make COMPILE_PREFIX=powerpc-e500v2- AT_COMPONENTS_SPECIFIC_FLAGS="-mcpu=e500mc -mtune=e500mc" GDB_DEBUG_OPT=n
`powerpc-e500v2-strip --strip-unneeded atcomponents.a`
`cp -f atcomponents.a customers/h3c/atcomponents_powerpc_4080_release.a`

export PATH=$originPath

# Compile for MIPS-64
export PATH=$toolPath/zte_nj_ptn/mips64_gcc4.5.2_glibc2.13.0_multi/bin/:$PATH

make clean
make COMPILE_PREFIX=mips_n64-softfloat-linux-gnu- AT_COMPONENTS_SPECIFIC_FLAGS="-fsigned-char -Wvla -D_DEBUG_VERSION -DMODULE -mlong-calls -isystem -DCONFIG_DISTRIBUTED -D__KERNEL__ -DVMLINUX_LOAD_ADDRESS=0xffffffff80100000 -DDATAOFFSET=0 -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-builtin -fno-common -ffunction-sections -fdata-sections -falign-functions=16 -fno-tree-dominator-opts -fno-tree-switch-conversion -fno-jump-tables -fno-inline-small-functions -Wframe-larger-than=2048 -fno-stack-protector -mabi=64 -G 0 -mno-abicalls -fno-pic -pipe -msoft-float -ffreestanding -DCVMX_ENABLE_POW_CHECKS=0 -march=xlp -Wa,--trap -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -Wno-builtin-macro-redefined -mno-branch-likely -DCVMX_ENABLE_CSR_ADDRESS_CHECKING=0 -DXLS -DNLM_STORM -DNLM_HAL_LINUX_KERNEL -msym32  -DKBUILD_64BIT_SYM32 -DLOADADDR=0xffffffff80100000 -DPHYSADDR=0xffffffff80100000"
`mips_n64-softfloat-linux-gnu-strip --strip-unneeded atcomponents.a`
`cp -f atcomponents.a customers/h3c/atcomponents_mips64_XLP432_debug.a`

make clean
make COMPILE_PREFIX=mips_n64-softfloat-linux-gnu- GDB_DEBUG_OPT=n AT_COMPONENTS_SPECIFIC_FLAGS="-fsigned-char -Wvla -D_DEBUG_VERSION -DMODULE -mlong-calls -isystem -DCONFIG_DISTRIBUTED -D__KERNEL__ -DVMLINUX_LOAD_ADDRESS=0xffffffff80100000 -DDATAOFFSET=0 -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-builtin -fno-common -ffunction-sections -fdata-sections -falign-functions=16 -fno-tree-dominator-opts -fno-tree-switch-conversion -fno-jump-tables -fno-inline-small-functions -Wframe-larger-than=2048 -fno-stack-protector -mabi=64 -G 0 -mno-abicalls -fno-pic -pipe -msoft-float -ffreestanding -DCVMX_ENABLE_POW_CHECKS=0 -march=xlp -Wa,--trap -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -Wno-builtin-macro-redefined -mno-branch-likely -DCVMX_ENABLE_CSR_ADDRESS_CHECKING=0 -DXLS -DNLM_STORM -DNLM_HAL_LINUX_KERNEL -msym32  -DKBUILD_64BIT_SYM32 -DLOADADDR=0xffffffff80100000 -DPHYSADDR=0xffffffff80100000"
`mips_n64-softfloat-linux-gnu-strip --strip-unneeded atcomponents.a`
`cp -f atcomponents.a customers/h3c/atcomponents_mips64_XLP432_release.a`

export PATH=$originPath

# ==============================================================================
# Update for ZTE MW projects
# ============================================================================== 
export CROSS_COMPILE=ppc_6xx-
make clean
make COMPILE_PREFIX=$toolPath/eldk4.2/usr/bin/ppc_6xx- AT_COMPONENTS_SPECIFIC_FLAGS="-mcpu=603 -mtune=603"
`cp -f atcomponents.a customers/zte_mw/atcomponents_powerpc_603.a`
export PATH=$originPath

# ==============================================================================
# Update for FibroLAN projects
# ==============================================================================
make clean 
make COMPILE_PREFIX=$toolPath/ecos/gnutools/mipsisa32-elf/bin/mipsisa32-elf- GEN_CMD_SRC_FILE=no ADDITIONAL_FLAGS="-Wpointer-arith -Wundef -EL -mips32 -mabi=eabi -msoft-float -g -O2 -ffunction-sections -fdata-sections -fno-exceptions -G0 -I../customers/archive/fibrolan/include" LDFLAGS="-EL -r -o"
`cp -f atcomponents.o customers/fibrolan/atcomponents_mipsisa32-elf.o`

make clean
make COMPILE_PREFIX=$toolPath/ecos/vtss-cross-mips32-24kec/bin/mipsel-vtss-elf- GEN_CMD_SRC_FILE=no ADDITIONAL_FLAGS="-Wpointer-arith -Wundef -EL -mips32 -mabi=eabi -msoft-float -g -O2 -ffunction-sections -fdata-sections -fno-exceptions -G0 -I../customers/archive/fibrolan/include" LDFLAGS="-EL -r -o"
`cp -f atcomponents.o customers/fibrolan/atcomponents_mipsel-vtss-elf.o`

# ==============================================================================
# Update for LOOP projects
# ============================================================================== 
# Compile PPC-85xxDP
export PATH=$toolPath/eldk4.2/usr/bin/:$PATH
export CROSS_COMPILE=ppc_85xxDP
make clean
make COMPILE_PREFIX=ppc_85xxDP- AT_COMPONENTS_SPECIFIC_FLAGS="-fPIC -msoft-float"
`cp -f atcomponents.o customers/loop/atcomponents_powerpc_85xxDP.o` 
export PATH=$originPath

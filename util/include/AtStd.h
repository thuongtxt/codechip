/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtStd.h
 * 
 * Created Date: Feb 6, 2014
 *
 * Description : To abstract C standard library
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTD_H_
#define _ATSTD_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdarg.h>
#include "attypes.h"
#include "AtFile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtStd * AtStd;

typedef enum eAtStdCompareResult
    {
    cAtStdCompareResultLessThan    = -1,
    cAtStdCompareResultEqual       = 0,
    cAtStdCompareResultGreaterThan = 1
    }eAtStdCompareResult;

typedef struct tAtStdListener
    {
    void (*WillPrint)(AtStd self, void *userData, char *aString);
    void (*DidPrint)(AtStd self, void *userData, char *aString);
    }tAtStdListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Manage the singleton */
void AtStdSharedStdSet(AtStd std);
AtStd AtStdSharedStdGet(void);

/* Built-in standard implementation:
 * - AtStdTiny: used for applications that file operations are not available
 * - AtStdDefault: extends from AtStdTiny and work with file operations.
 */
AtStd AtStdDefaultSharedStd(void);
AtStd AtStdTinySharedStd(void);

/* String library */
char *AtStdStrcpy(AtStd self, char *dest, const char *src);
char *AtStdStrncpy(AtStd self, char *dest, const char *src, uint32 numBytes);
uint32 AtStdStrlen(AtStd self, const char *pString);
uint32 AtStdStrcmp(AtStd self, const char *pString1, const char *pString2);
uint32 AtStdStrncmp(AtStd self, const char *pString1, const char *pString2, uint32 numBytes);
char *AtStdStrstr(AtStd self, const char *pString, const char *subString);
char *AtStdStrtok(AtStd self, char *pString, const char *delim);
char *AtStdStrtok_r(AtStd self, char *pString, const char *delim, char **saveptr);
char *AtStdStrcat(AtStd self, char *dest, const char *src);
char *AtStdStrncat(AtStd self, char *dest, const char *src, uint32 numBytes);
char *AtStdStrchr(AtStd self, const char *pString, uint16 aChar);
char *AtStdStrrchr(AtStd self, const char *pString, uint16 aChar);
uint32 AtStdSprintf(AtStd self, char *str, const char *format, va_list ap);
uint32 AtStdSnprintf(AtStd self, char *str, uint32 size, const char *format, va_list ap);

/* Value conversion */
int32 AtStdAtoi(AtStd self, const char *pString);
uint32 AtStdAtol(AtStd self, const char *pString);
uint32 AtStdStrtol(AtStd self, const char *pString, char **endptr, uint32 base);
uint32 AtStdStrtoul(AtStd self, const char *pString, char **endptr, uint32 base);
uint32 AtStdAbs(AtStd self, int value);
uint64 AtStdStrtoull(AtStd self, const char *pString, char **endptr, uint32 base);

/* Random value */
uint32 AtStdRand(AtStd self);
void AtStdRandSeed(AtStd self);

/* Print */
uint32 AtStdPrintf(AtStd self, const char *format, va_list ap);
void AtStdPrintFuncSet(AtStd self, int (*printFunc)(void *appContext, char *aString));
void AtStdAppContextSet(AtStd self, void *appContext);
void AtStdColorEnable(AtStd self, eBool enabled);
eBool AtStdColorIsEnabled(AtStd self);
void AtStdPrintEnable(AtStd self, eBool enabled);
eBool AtStdPrintIsEnabled(AtStd self);

/* Global APIs */
void AtPrintEnable(eBool enable);
eBool AtPrintIsEnabled(void);
uint32 AtPrintc(uint32 color, const char *format, ...) AtAttributePrintf(2, 3);
uint32 AtPrintf(const char *, ...) AtAttributePrintf(1, 2);
uint32 AtSprintf(char *str, const char *format, ...) AtAttributePrintf(2, 3);
uint32 AtSnprintf(char *str, uint32 size, const char *format, ...) AtAttributePrintf(3, 4);
void AtSpaceIndent(uint32 indentLevel);
int AtEof(void);

/* File operation */
AtFile AtStdFileOpen(AtStd self, const char *filepath, uint8 mode);
int AtStdFileClose(AtFile file);
AtFile AtStdStandardOutput(AtStd self);
AtFile AtStdStandardError(AtStd self);
AtFile AtStdStandardInput(AtStd self);
int AtStdEof(AtStd self);
void AtStdFlush(void);

/* Listeners */
void AtStdListenerAdd(AtStd self, tAtStdListener *listener, void *userData);
void AtStdListenerRemove(AtStd self, tAtStdListener *listener, void *userData);

/* Sorting */
void AtStdSort(AtStd self,
               void *firstElement, uint32 numElements, uint32 memberSize,
               eAtStdCompareResult (*CompareFunc)(const void *, const void *));
eAtStdCompareResult AtStdUint32CompareFunction(const void *value1, const void *value2);
eAtStdCompareResult AtStdUint16CompareFunction(const void *value1, const void *value2);
eAtStdCompareResult AtStdUint8CompareFunction(const void *value1, const void *value2);
char *AtStdNumbersToString(AtStd self, char *idString, uint32 idBufSize, uint32 *numbers, uint32 numNumbers);
char *AtStdTwoLevelNumbersToString(AtStd self, char *idString, uint32 idBufSize,
                                   uint32 *level1Numbers, uint32 *level2Numbers, uint32 numNumbers, uint32 maxLevel2Number);

#endif /* _ATSTD_H_ */

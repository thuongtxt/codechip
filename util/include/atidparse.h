/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atidparse.h
 *
 * Created Date: Dec 7, 2011
 *
 * Description : This file contain definitions for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#ifndef ATIDPARSE_H_
#define ATIDPARSE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "atclib.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/
#define cOrChar             "|"
#define cSeparateChar       ","
#define cFormatSeparate     "-"
#define cFormatCheck        "."
#define cLineFormatCheck    "#"
#define mMin(a,b) (a > b ? b : a)
#define mMax(a,b) (a > b ? a : b)
#define cMaxBuf  500
#define cMaxItem 1024

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
/* This function is used to parse of string into a list of flat ID. For example.
  "0,3-6" will return [0, 3, 4, 5, 6] */
uint8 IdFlatNumParse(const char   *pStrId,
                     uint32        maxItem,
                     uint32       *pIdList,
                     uint32       *pNumItem);

/* This function is used to parse of string (hex format) into a list of flat
   ID. For example. "0x0,0x3-0x6" will return [0, 3, 4, 5, 6] */
uint8 HexIdFlatNumParse(const char  *pStrId,
                        uint32       maxItem,
                        uint32      *pIdList,
                        uint32      *pNumItem);

/* This function is used to parse a string (multiple, hierarchical format)
   into a list of flat ID. Multiple ID list strings in input string are separated
   by the "," token. Please note all ID list strings MUST have the same hierarchical
   level. */
uint8 IdComplexNumParse(char  *pStrId,
                        char  *pFormatMin,
                        char  *pFormatMax,
                        uint32  maxItem,
                        uint32 *pIdList,
                        uint32 *pNumItem);

/* This function parse the dwordList of ID into a short string with format "start1-end1,startN-endN" */
uint32 IdDwordListToString(char *retStr, uint32 strBufLength, uint32 *dwordList, uint32 listLengInDword);

#ifdef  __cplusplus
}
#endif

#endif /* ATIDPARSE_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atclib.h
 *
 * Created Date: Dec 8, 2011
 *
 * Description : This file contain definitions for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#ifndef ATCLIB_H_
#define ATCLIB_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/
#define AT_EOF              EOF
#define AT_CLOCK_REALTIME   CLOCK_REALTIME
#define AT_TIMER_ABSTIME    TIMER_ABSTIME
#define AT_CLOCK_MONOTONIC  CLOCK_MONOTONIC
#define AT_SIGRTMIN         SIGRTMIN
#define AT_SIGRTMAX         SIGRTMAX

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef void*               (*fAtTaskHdl)(void* pData);

/** Time zone, this structure is used interchangeably with POSIX 'struct timezone' */
typedef struct tAtTimeZone
    {
    int minWest;        /**< Minutes west of GMT */
    int dstTime;        /**< Nonzero if DST (Daylight Saving Time) is ever in effect */
    }tAtTimeZone;

/** Time Value, this structure is used interchangeably with POSIX 'struct timeval' */
typedef struct tAtTimeVal
    {
    int      sec;    /**< Seconds */
    int      usec;   /**< Microseconds */
    }tAtTimeVal;

/** Detail time information, returned by AtLocalTime()
    this structure is used interchangeably with POSIX 'struct tm' */
typedef struct tAtTm
    {
    int sec;        /**< [0-60] Seconds */
    int min;        /**< [0-59] Minutes */
    int hour;       /**< [0-23] Hours */
    int mday;       /**< [1-31] Day */
    int mon;        /**< [0-11] Month */
    int year;       /**< [1900- ] Year */
    int wday;       /**< [0-6] Day of week */
    int yday;       /**< [0-365] Days in year */
    int isdst;      /**< [-1/0/1] Daylight time saving */
    int gmtoff;     /**< Seconds east of UTC  */
    char *tzone;    /**< Timezone abbreviation  */
    }tAtTm;

/** Timer value, this structure is used interchangeably with POSIX 'struct timespec' */
typedef struct tAtTimeSpec
    {
    int sec;       /**< Seconds */
    int nsec;      /**< Nanoseconds */
    }tAtTimeSpec;

/** Timer value, this structure is used interchangeably with POSIX 'struct itimerspec' */
typedef struct tAtItimerSpec
    {
    tAtTimeSpec     interval; /**< Interval */
    tAtTimeSpec     value;    /**< Value */
    }tAtItimerSpec;

/**< Severity level represented in terminal colors */
typedef enum eAtSevLevel
    {
    cSevCritical   = 31,   /**< Red */
    cSevMajor      = 35,   /**< Magenta */
    cSevWarning    = 33,   /**< Yellow */
    cSevNeutral    = 37,   /**< White */
    cSevDebug      = 36,   /**< Cyan */
    cSevInfo       = 32,   /**< Green */
    cSevNormal     = 0     /**< Default color of terminal */
    } eAtSevLevel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
/* Duplicate a memory region */
void* AtMemdup(void* pMem, uint32 size);

char *AtStrdup(const char *s);
uint32 AtStrToDw(const char *pStr);
char** StrTokenGet(const char *pStr, const char *pDelim, uint32 *tokenCount);
eBool AtStrToArrayDw(const char* str, uint32 *pValue, uint32 *pNumDw);
eBool StrToDw(const char* str, char format, uint32 *value);

eBool AtIsHexDigit(int c);
eBool AtIsDigit(int c);
eBool AtIsAlnum(int c);
eBool AtIsSpace(int c);
eBool AtIsHexChar(char c);
uint32 AtString2Bytes(const char *pByteString, uint8 *pBytes, uint32 bufferSize, uint8 base);
char *AtBytes2String(const uint8 *buffer, uint32 bufferLength, uint8 base);
char *AtStringTrim(char *stringBuffer);
eBool AtStringNumberIsValid(const char *pHexString, uint8 base);
void AtStringArrayFree(char **pStringArray, uint32 numItem);

char *AtStrcpy(char *dest, const char *src);
char *AtStrncpy(char *dest, const char *src, uint32 numBytes);
uint32 AtStrlen(const char *pString);
uint32 AtStrcmp(const char *pString1, const char *pString2);
uint32 AtStrncmp(const char *pString1, const char *pString2, uint32 numBytes);
/*char *AtStrstr(const char *pString, const char *subString) AtAttributePrintf(1, 0);*/
char *AtStrstr(const char *pString, const char *subString);
char *AtStrtok(char *pString, const char *delim);
char *AtStrtok_r(char *pString, const char *delim, char **saveptr);
char *AtStrcat(char *dest, const char *src);
char *AtStrncat(char *dest, const char *src, uint32 numBytes);
char *AtStrchr(const char *pString, uint16 aChar);
char *AtStrrchr(const char *pString, uint16 aChar);

int32 AtAtoi(const char *pString);
uint32 AtAtol(const char *pString);
uint32 AtStrtol(const char *pString, char **endptr, uint32 base);
uint32 AtStrtoul(const char *pString, char **endptr, uint32 base);
uint64 AtStrtoull(const char *pString, char **endptr, uint32 base);

eBool AtOutOfRange(uint32 value, uint32 min, uint32 max);

void AtRegFieldIns(uint32 *pRegVal, uint32 mask, uint32 shift, uint32 value);
uint32 AtRegMaskToShift(uint32 mask);

#ifdef  __cplusplus
}
#endif

#endif /* ATCLIB_H_ */

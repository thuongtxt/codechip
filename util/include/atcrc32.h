/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atcrc32.h
 *
 * Created Date: Dec 7, 2011
 *
 * Description : This file contain definitions for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#ifndef ATCRC32_H_
#define ATCRC32_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
/* Calculates 32-bit CRC value for input data buffer */
uint32 AtCrc32(uint32 crc, const char* buf, unsigned int len);

#ifdef  __cplusplus
}
#endif

#endif /* ATCRC32_H_ */

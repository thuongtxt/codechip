/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : atcrc.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : To contain header source of atcrc12.c
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCRC_H_
#define _ATCRC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtCrc(const uint32* value, uint32 numBits, uint32 polyBitMask);

#ifdef __cplusplus
}
#endif
#endif /* _ATCRC_H_ */


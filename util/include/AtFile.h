/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtFile.h
 * 
 * Created Date: Sep 17, 2014
 *
 * Description : To abstract file operation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFILE_H_
#define _ATFILE_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdarg.h>
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFile *AtFile;

typedef enum eAtFileOpenMode
    {
    cAtFileOpenModeRead     = cBit0, /**< Open File for reading */
    cAtFileOpenModeWrite    = cBit1, /**< Open File for writing */
    cAtFileOpenModeAppend   = cBit2, /**< Open File for appending */
    cAtFileOpenModeTruncate = cBit3  /**< Open File for appending */
    }eAtFileOpenMode;

typedef enum eAtFileSeekPosition
    {
    cAtFileSeekPositionAt_SEEK_SET, /**< Start of file */
    cAtFileSeekPositionAt_SEEK_CUR, /**< current pointer position */
    cAtFileSeekPositionAt_SEEK_END  /**< End of file */
    }eAtFileSeekPosition;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete files */
AtFile AtFileDefaultNew(const char *filePath, uint8 mode);

const char *AtFilePath(AtFile self);
eBool AtFileEof(AtFile self);
int AtFileClose(AtFile self);
int AtFileFlush(AtFile self);
char *AtFileGets(AtFile self, char *buffer, uint32 numBytes);
int AtFileSeek(AtFile self, long int offset, eAtFileSeekPosition whence);
uint32 AtFileRead(AtFile self, void *buffer, uint32 numBytes, uint32 numMember);
uint32 AtFileWrite(AtFile self, const void *buffer, uint32 numBytes, uint32 numMember);
uint32 AtFilePrintf(AtFile self, const char *format, ...) AtAttributePrintf(2, 3);
uint32 AtFileVPrintf(AtFile self, const char *format, va_list ap) AtAttributePrintf(2, 0);
int AtFilePuts(AtFile self, const char *string);

#endif /* _ATFILE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : None
 *
 * File        : eupautil.h
 *
 * Created Date: 02-May-07
 *
 * Description : This file contain all utilities function and macro for
 *				 THALASSA device driver using.
 *				 
 * Notes       : None
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/ 


/*--------------------------- Macro -----------------------------------------*/ 


/*------------------------------------------------------------------------------
Prototype 	: mFieldIns( pRegVal , mask , shift , inVal )

Inputs      : pRegVal                 - is a pointer that points to the dword-
										value before insert inVal 	
			  mask                    - is the mask of inVal Field	shift is the 
			  							position which inVal must be shift to
			  inVal 				  - is the value wanted to be inserted			

Outputs     : pRegVal 				  - is a pointer pointing to the dword-value  
										after inVal inserted with mask and shift

Return      : None

Valid state : Active or Inactive state of the driver device state 

Side affect	: None

Purpose     : This macro is used to set a field to dword value with mask and 
  			  shift bit
------------------------------------------------------------------------------*/
#define mFieldIns(pRegVal, mask, shift, inVal) AtRegFieldIns(pRegVal, (uint32)(mask), (uint32)(shift), (uint32)(inVal))

/*------------------------------------------------------------------------------
Prototype 	: mFieldGet( regVal , mask , shift , pOutVal )

Inputs      : regVal 				  - is the dword-value which outVal got from	
			  mask                    - is the mask of outVal field
			  shift                   - is the position which outVal must be 
			                            shift to			

Outputs     : pOutVal                 - is a pointer that point to the value got 
                                        from regVal, mask and shift

Return      : None

Valid state : Active or Inactive state of the driver device state 

Side affect	: None

Purpose     : This macro is used to get a field to dword-value with mask and 
			  shift bit
------------------------------------------------------------------------------*/
#define mFieldGet( regVal , mask , shift , type, pOutVal )                     \
    do                                                                         \
        {                                                                      \
        *(pOutVal) = (type)( (uint32)( (regVal) & (mask) ) >> ((uint32)(shift)) ); \
        } while(0)

/*------------------------------------------------------------------------------
Prototype 	: mPointerNull(pParam)

Inputs		: pParam	              - The pointer want to check

Outputs		: None

Return	 	: true                    - if the pParam == NULL
			: false                   - if the pParam != NULL

Valid state : None

Side affect	: None

Purpose 	: Check pointer.
------------------------------------------------------------------------------*/
#define mPointernull(pParam) ((pParam) == NULL)


/*------------------------------------------------------------------------------
Prototype 	: mBoolToBin(boolVar)

Inputs		: boolVar	              - The bool variable

Outputs		: None

Return	 	: 1                       - if the boolVar is true
			  0                       - if the boolVar is false

Valid state : None

Side affect	: None

Purpose 	: Convert the bool variable to binary value
------------------------------------------------------------------------------*/
#define mBoolToBin(boolVar) (((boolVar) == cAtFalse) ? 0x0UL : 0x1UL)

/*------------------------------------------------------------------------------
Prototype 	: mBinToBool(binVar)

Inputs		: boolVar	              - The bool variable

Outputs		: None

Return	 	: true                    - if the binVar is 1
			  false                   - if the binVar is 0

Valid state : None

Side affect	: None

Purpose 	: Convert the binary variable to boolean value
------------------------------------------------------------------------------*/
#define mBinToBool(binVar) (((binVar) == 0) ? cAtFalse : cAtTrue)

/*------------------------------------------------------------------------------
Prototype : mBitGet(reg, shift)

Purpose   : This macro is used to get the value of bit at desired location

Inputs    : reg                     - Register value.
			shift                   - Shift bit

Outputs   : pBitVal                 - Bit value

Return    : None
------------------------------------------------------------------------------*/
#define mBitGet(reg, shift) (((reg) & (uint32)(0x1<<(shift))) ? 1 : 0)

/*------------------------------------------------------------------------------
Prototype : mBitIns(pReg, shift, bitVal)

Purpose   : This macro is used to insert the bit into desired location of register

Inputs    : pReg                    - Register value.
			shift                   - Shift bit
			bitVal                  - bit value

Outputs   : None

Return    : None
------------------------------------------------------------------------------*/
#define mBitIns(pReg, shift, bitVal) \
    {*(pReg) = ((*(pReg))&(~((uint32)((0x1)<<(shift))))) | (((bitVal) & 0x1)<<(shift));}

/*------------------------------------------------------------------------------
Prototype : mBitClear(pReg, pos)

Purpose   : This macro is used to clear the bit at input position of one register 

Inputs    : pReg                     - Register value.
            pos                      - bit position need to be cleared

Outputs   : pReg                     - Register value

Return    : None
------------------------------------------------------------------------------*/
#define mBitClear(pReg, pos)                                               \
    *(pReg) = (uint32)(*(pReg) & (~(cBit0 << (pos))))

/*------------------------------------------------------------------------------
Prototype : mBitSet(pReg, pos)

Purpose   : This macro is used to set the bit at input position of one register 

Inputs    : pReg                     - Register value.
            pos                      - bit position need to be cleared

Outputs   : pReg                     - Register value

Return    : None
------------------------------------------------------------------------------*/
#define mBitSet(pReg, pos)                                               \
    *(pReg) = (uint32)(*(pReg) | ((cBit0 << (pos))))


#define mAtStrToEnum(pTabxxxStr, pTabxxxVal, pStr, pVal, rs)                  \
        {                                                                      \
        uint32 dIdx;                                                            \
        dIdx = 0;                                                              \
        rs = cAtFalse;                                                         \
		while(dIdx < mCount(pTabxxxVal))                                       \
            {                                                                  \
            if((AtStrlen((const char*)pStr) == AtStrlen(pTabxxxStr[dIdx])) &&        \
               (AtStrncmp(pTabxxxStr[dIdx], pStr, AtStrlen((const char*)pStr)) == 0))\
                {                                                              \
                pVal = pTabxxxVal[dIdx];                                       \
			    rs = cAtTrue;                                                  \
			    dIdx = mCount(pTabxxxVal);                                     \
                }                                                              \
            dIdx++;                                                            \
            }                                                                  \
        }                

#define mAtEnumToStr(pTabxxxStr, pTabxxxVal, pEnum, pStr, rs)                  \
        {                                                                      \
        uint32 dIdx;                                                            \
        dIdx = 0;                                                              \
        rs = cAtFalse;                                                         \
		while(dIdx < mCount(pTabxxxVal))                                       \
            {                                                                  \
            if(pEnum == pTabxxxVal[dIdx])                                      \
                {                                                              \
                AtStrncpy(pStr, pTabxxxStr[dIdx], AtStrlen(pTabxxxStr[dIdx])); \
			    pStr[AtStrlen(pTabxxxStr[dIdx])] = 0;                          \
				rs = cAtTrue;                                                  \
			    dIdx = mCount(pTabxxxVal);                                     \
                }                                                              \
            dIdx++;                                                            \
            }                                                                  \
        }

#define mAtStrToBool(pStr, blVal, blRs)                                        \
        {                                                                      \
        blRs = cAtFalse;                                                       \
        blVal = cAtFalse;                                                      \
		if(AtStrcmp(pStr, "en") == 0)                                          \
            {                                                                  \
            blVal = cAtTrue;                                                   \
		    blRs = cAtTrue;                                                    \
		    }                                                                  \
		else if(AtStrcmp(pStr, "dis") == 0)                                    \
			{                                                                  \
            blVal = cAtFalse;                                                  \
		    blRs = cAtTrue;                                                    \
		    }                                                                  \
         }                

#define mAtBoolToStr(blVal, pStr, blRs)                                        \
        {                                                                      \
        eBool m_blVal = (blVal);                                               \
        blRs = cAtFalse;                                                       \
		if(m_blVal == cAtTrue)                                                 \
		    {                                                                  \
            AtStrcpy(pStr, "en");                                              \
		    blRs = cAtTrue;                                                    \
		    }                                                                  \
		else if(m_blVal == cAtFalse)                                           \
			{                                                                  \
            AtStrcpy(pStr, "dis");                                             \
		    blRs = cAtTrue;                                                    \
		    }                                                                  \
         }           

/*------------------------------------------------------------------------------
Prototype 	: mTimeIntervalInMsGet(prevTime, curTime)

Inputs      : prevTime                - is previous time (has type tOsalCurTime)
              curTime                 - is current time (has type tOsalCurTime)

Outputs     : None

Return      : Time interval between current time and previous time in milisecond

Valid state : None

Side affect	: None

Purpose     : This macro is used to get time interval between current time and
              previous time in milisecond
------------------------------------------------------------------------------*/
#define mTimeIntervalInMsGet(prevTime, curTime) AtOsalDifferenceTimeInMs(&(curTime), &(prevTime))

#define mRegField(regVal, fieldName) ((uint32)(((regVal) & fieldName##Mask) >> (fieldName##Shift)))
#define mRegFieldSet(regVal, fieldName, fieldValue)                            \
    (regVal) = (uint32)((uint32)((regVal) & (~(fieldName##Mask))) | (((uint32)(fieldValue) << ((uint32)(fieldName##Shift))) & (fieldName##Mask)))

#define mPolyRegField(regVal, module, fieldName) ((uint32)(((regVal) & (mMethodsGet(module)->fieldName##Mask(module))) >> (mMethodsGet(module)->fieldName##Shift(module))))
#define mPolyRegFieldSet(regVal, module, fieldName, fieldValue)                            \
    (regVal) = (uint32)((uint32)((regVal) & (~(mMethodsGet(module)->fieldName##Mask(module)))) | (((uint32)(fieldValue) << ((uint32)(mMethodsGet(module)->fieldName##Shift(module)))) & (mMethodsGet(module)->fieldName##Mask(module))))

/* Asssert */
#define AtAssert(expr)                                                         \
    do {                                                                       \
        if(!(expr))                                                            \
            {                                                                  \
            AtOsalTrace(cAtOsalTraceError, "nIn file %s:%d Assertion %s fail !!!n", \
                        __FILE__, __LINE__, #expr);                            \
            AtOsalPanic();                                                     \
            }                                                                  \
    }while(0)

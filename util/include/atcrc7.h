/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atcrc7.h
 *
 * Created Date: Dec 7, 2011
 *
 * Description : This file contain definitions for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/
#ifndef ATCRC7_H_
#define ATCRC7_H_
/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
void AtCrc7Ins(uint8 *pData, uint16 dataLen);

#endif /* ATCRC7_H_ */

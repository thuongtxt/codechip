/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atcrc32.h
 *
 * Created Date: Dec 7, 2011
 *
 * Description : This file contain definitions for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#ifndef ATCRC16_H_
#define ATCRC16_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
/* Calculates 32-bit CRC value for input data buffer */
uint16 AtCrc16(uint16 crc, const char* buf, unsigned int len);

#ifdef  __cplusplus
}
#endif

#endif /* ATCRC16_H_ */

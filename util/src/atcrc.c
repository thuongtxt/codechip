/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : atcrc.c
 *
 * Created Date: Jul 21, 2016
 *
 * Description : CRC calculation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atcrc.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 BitInDword(uint32 dwValue, uint8 bit)
    {
    return ((1UL << bit) & dwValue) ? 1 : 0;
    }

static uint8 BitAtPosition(const uint32* value, uint32 bit)
    {
    uint8 dwordIndex = (uint8)(bit / 32);
    uint8 bitInDwIndex = bit % 32;

    return BitInDword(value[dwordIndex], bitInDwIndex);
    }

/*
 * Get CRC mode from poly bitmask, for example:
 *
 * 100111001    : highest set bit is 8, CRC-8 with polynomial P(x) = x8+ x5+ x4+ x3+ x0, return 8
 * 100100111001 : highest set bit is 11, CRC-11 with polynomial P(x) = x11+ x8+ x5+ x4+ x3+ x0, return 11
 */
static uint8 CrcFromPolyBitMask(uint32 polyBitMask)
    {
    uint8 bit_i;
    for (bit_i = 31; bit_i > 0; bit_i--)
        {
        if (BitInDword(polyBitMask, bit_i))
            return bit_i;
        }

    return 0;
    }

/*
 * Calculate CRC value from input value
 *
 * @param value Array of value
 * @param numBits Number bits of value
 * @param polyBitMask Polynomial BitMask
 * @return CRC value
 */
uint32 AtCrc(const uint32* value, uint32 numBits, uint32 polyBitMask)
    {
    uint8 CRC[32];
    int valueBit_i;
    uint8 crcBit_i;
    uint8 crc = CrcFromPolyBitMask(polyBitMask);
    uint8 doInvert;
    uint32 crcResult;

    AtOsalMemInit(CRC, 0, sizeof(CRC));

    for (valueBit_i = (int)(numBits - 1); valueBit_i >= 0; valueBit_i--)
        {
        doInvert = (uint8)(BitAtPosition(value, (uint32)valueBit_i) ^ CRC[crc - 1]);
        for (crcBit_i = (uint8)(crc - 1); crcBit_i > 0; crcBit_i--)
            {
            if (BitInDword(polyBitMask, crcBit_i))
                CRC[crcBit_i] = (CRC[crcBit_i - 1] ^ doInvert);
            else
                CRC[crcBit_i] = CRC[crcBit_i - 1];
            }

        CRC[0] = doInvert;
        }

    /* Convert to uint32 */
    crcResult = 0;
    for (crcBit_i = 0; crcBit_i < (uint8)crc; ++crcBit_i)
        {
        uint8 crcBitValShift = crcBit_i;
        uint32 crcBitValMask = (1UL << crcBit_i);
        mRegFieldSet(crcResult, crcBitVal, CRC[crcBit_i]);
        }

    return crcResult;
    }

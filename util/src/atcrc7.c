/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atcrc7.c
 *
 * Created Date: Dec 7, 2011
 *
 * Description : This file contain implementation for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atcrc7.h"

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Functions --------------------------------------*/
/*
 * This function is used generate a CRC-7 syndromes for x^7 * of input byte
 *
 * @param val       - Input byte
 *
 * @return cAtOk    - CRC-7 syndromes for x^7 * of input byte
 *
 */
static uint8 SyndromeGen(uint8 val)
    {
    uint16 j;
    uint32 syndrome;
    static const uint8 POLYNOMINAL = 0x89;

    syndrome = (uint32)(((val & cBit7) != 0)? val ^ POLYNOMINAL : val);
    for (j = 0;  j < 7;  ++j)
        {
        if (((syndrome <<= 1) & cBit7) != 0)
            {
            syndrome ^= POLYNOMINAL;
            }
        }

    return (uint8)syndrome;
    }

/*
 * This function is used to insert CRC value computed from input
 * data. It will compute CRC from first (dataLen - 1) bytes.
 * After computing CRC, first (dataLen - 1) bytes will be shifted
 * one byte to the right and the result CRC will be inserted to the
 * first byte of data buffer.
 *
 * @param pData       - Hardware BER line interrupt bit map (SF-BER line, SD-BER line)
 * @param dataLen     - OC line identifier
 * @param pData       - Data buffer with inserted CRC
 *
 * @return cAtOk      - If success
 * cAtErrNullPointer  - If input pointer is null
 *
 */
void AtCrc7Ins(uint8 *pData, uint16 dataLen)
    {
    uint8        crc7_accum;
    int       i;

    for (i = (dataLen-1);  i >= 1;  i--)
        {
        pData[i] = pData[i - 1];
        }
    pData[0] = cBit7;

    pData[0] = pData[0] & (uint8)(~cBit6_0);
    crc7_accum  = 0;
    for (i = 0;  i < dataLen;  ++i)
        {
        crc7_accum = SyndromeGen((uint8)((crc7_accum << 1) ^ pData[i]));
        }
    pData[0] ^= crc7_accum;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : atclib.c
 *
 * Created Date: Dec 8, 2011
 *
 * Description : C library wrap-up
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cBufMaxLength 100

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Functions --------------------------------------*/
static eBool DigitIsValid(char digit, uint8 base)
    {
    if (base == 16) return AtIsHexDigit(digit);
    if (base == 10) return AtIsDigit(digit);

    return cAtFalse;
    }

static eBool StringNumberIsValid(const char *pHexString, uint8 base)
    {
    uint32 i;

    for (i = 0; i < AtStrlen(pHexString); i++)
        {
        if (!DigitIsValid(pHexString[i], base))
            return cAtFalse;
        }

    return cAtTrue;
    }

eBool AtStringNumberIsValid(const char *pHexString, uint8 base)
    {
    return StringNumberIsValid(pHexString, base);
    }

static void StringArrayFree(char **pStringArray, uint32 numItem)
    {
    uint32 itemIndex;

    for (itemIndex = 0; itemIndex < numItem; itemIndex++)
        AtOsalMemFree(pStringArray[itemIndex]);
    AtOsalMemFree(pStringArray);
    }

eBool AtIsHexDigit(int c)
    {
    return (((c <= 'f') && (c >= 'a')) ||
            ((c <= 'F') && (c >= 'A')) ||
            ((c <= '9') && (c >= '0')));
    }

eBool AtIsDigit(int c)
    {
    return (c<='9' && c>='0');
    }


eBool AtIsAlnum(int c)
    {
    return (((c >= 'a') && (c <= 'z')) ||
            ((c >= 'A') && (c <= 'Z')) ||
            ((c >= '0') && (c <= '9')));
    }

eBool AtIsSpace(int c)
    {
    /* first check gives quick exit in most cases */
    return (c <= ' ' && ((c == ' ') || ((c <= 13) && (c >= 9))));
    }

eBool AtIsHexChar(char c)
    {
    return (((c >= 'a') && (c <= 'f')) ||
            ((c >= 'A') && (c <= 'F')) ||
            ((c >= '0') && (c <= '9')));
    }

/**
 * @brief Duplicate a memory region.
 *
 * @param [in] pMem Reference to the memory region.
 * @retval [in] size Size of the memory region
 *
 * @retval void* A malloc'd pointer will be returned.
 * @retval NULL The procedure fails.
 */
void* AtMemdup(void* pMem, uint32 size)
    {
    void *pTemp;

    if (!pMem || !size) return NULL;

    if ((pTemp = AtOsalMemAlloc(size)) == NULL)
        return NULL;

    AtOsalMemCpy(pTemp, pMem, size);

    return pTemp;
    }

/**
 * Convert string to dword.
 *
 * @param pStr Null-terminated string to be converted.
 *
 * @return The converted value.
 */
uint32 AtStrToDw(const char *pStr)
    {
    uint32 value = 0;

    if(pStr != NULL)
        {
        /* Convert hex to value */
        if(AtStrlen(pStr) > 1 && (pStr[0] == '0') && (pStr[1] == 'x' || pStr[1] == 'X') )
            {
            const char *hexNumberString = pStr + 2;
            value = AtStrtoul(hexNumberString, (char**)NULL, 16);
            }
        else /* Convert to decimal value */
            {
            value = AtStrtoul(pStr, (char**)NULL, 10);
            }
        }

    return value;
    }

/* TODO: Consider to remove this function, it may be duplicate with AtStrToDw */
eBool StrToDw(const char* str, char format, uint32 *value)
    {
    uint32 len;
    uint32 i;
    eBool ret;

    ret = cAtTrue;
    len = AtStrlen(str);
    *value = 0;
    i = 0;

    switch(format)
        {
        case 'h':
            {
            if( (len > 1) && ((str[0] == '0') && (str[1] == 'x' || str[1] == 'X')) )
                {
                i = 2;
                }
            else
                {
                i = 0;
                }
            for( ; i < len; i++ )
                {
                if( !AtIsHexDigit(str[i]) )
                    {
                    ret = cAtFalse;
                    break;
                    }
                }

            if( ret == cAtTrue )
                {
                *value = AtStrtoul(str, (char**)NULL, 16);
                }
            }
            break;
        case 'd':
            {
            for( i = 0; i < len; i++ )
                {
                if( !AtIsDigit(str[i]) )
                    {
                    ret = cAtFalse;
                    break;
                    }
                else
                    {
                    *value = (uint32)((*value) * 10UL + (uint32)(str[i] - 48));
                    }
                }
            }
            break;
        case 'b':
            {
            for( i = 0; i < len; i++ )
                {
                if( str[i] != '0' && str[i] != '1' )
                    {
                    ret = cAtFalse;
                    break;
                    }
                else
                    {
                    *value = (uint32)((*value << 1) | (uint32)(str[i] - 48));
                    }
                }
            }
            break;
        default:
            {
            ret = cAtFalse;
            }
            break;
        }

    if( ret == cAtFalse )
        {
        *value = 0;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
Prototype    : public eBool AtStrToArrayDw(const char* str, dword *pValue, dword *pNumDw)

Description  : Convert string to dword array

Inputs       : str          - String to be converted
               format       - Format char

Outputs      : pValue       - Converted value
               pNumDw       - Converted value

Return Code  : cAtTrue      - If convert successfully
               cAtFalse     - If convert fail
------------------------------------------------------------------------------*/
eBool AtStrToArrayDw(const char* str, uint32 *pValue, uint32 *pNumDw)
    {
    eBool ret;
    uint32 dLength;
    uint32 dDwIndex;
    uint32 dIndex;
    uint32 dTmpIndex;
    char  pTmpDword[11];
    char                **pItemList;
    uint32               dNumItem;
    uint32               dItemIndex;

    dNumItem = 0;
    dItemIndex = 0;
    ret = cAtTrue;
    dDwIndex = 0;
    dIndex = 0;
    dTmpIndex = 0;
    *pNumDw = 0;

    if (str == NULL)
        {
        AtPrintc(cSevCritical,
                "\nERROR: str is NULL.\n");
        return cAtFalse;
        }

    dLength = AtStrlen(str);
    if  (((str[0] != '0') && ((str[1] != 'x') || (str[1] != 'X')))
         ||(dLength < 3))
        {
        AtPrintc(cSevCritical, "ERROR: Cannot see '0x' in front %s\n", str);
        return cAtFalse;
        }

    pItemList = StrTokenGet(str, ".", &dNumItem);
    if (pItemList ==  NULL)
        {
        AtPrintc(cSevCritical, "\nERROR: Split %s error.\n", str);
        return cAtFalse;
        }
    if (dNumItem > 8)
        ret = cAtFalse;
    if (ret == cAtTrue)
        {
        if (dNumItem == 1)
            {
            while (dLength > 10)
                {
                AtOsalMemInit(pTmpDword, 0, sizeof(pTmpDword));
                pTmpDword[0] = '0';
                pTmpDword[1] = 'x';
                for (dIndex = 0; dIndex < 8; dIndex ++)
                    {
                    dTmpIndex = (dLength + dIndex) - 8;
                    pTmpDword[dIndex + 2] = str[dTmpIndex];
                    }
                pValue[dDwIndex] = AtStrToDw((char*)pTmpDword);
                dDwIndex = dDwIndex + 1;
                dLength = dLength - 8;
                }
            if (dLength > 2)
                {
                AtOsalMemInit(pTmpDword, 0, sizeof(pTmpDword));
                pTmpDword[0] = '0';
                pTmpDword[1] = 'x';
                for (dIndex = 0; dIndex < dLength - 2; dIndex ++)
                    pTmpDword[dIndex + 2] = str[dIndex + 2];
                pValue[dDwIndex] = AtStrToDw((char*)pTmpDword);
                }
            }
        else
            {
            for (dItemIndex = 0; dItemIndex < dNumItem; dItemIndex ++)
                {
                pValue[dDwIndex] = AtStrtoul(pItemList[dNumItem - dDwIndex - 1], NULL, 16);
                dDwIndex = dDwIndex + 1;
                }
            }
        }

    for (dItemIndex = 0; dItemIndex < dNumItem; dItemIndex ++)
        AtOsalMemFree(pItemList[dItemIndex]);
    AtOsalMemFree(pItemList);

    *pNumDw = dDwIndex;
    return ret;
    }

/*
 * @brief This function returns a list of tokens (substrings), given a input string
 * and a delimiter.
 *
 * @param [in] pStr The input string.
 * @param [in] pStr pDelim The Delimiter.
 *
 * @param [out] tokenCount The returned token list.
 *
 * @retval 1 The procedure is executed successful.
 * @retval 0 The procedure  fails.
 */
char** StrTokenGet(const char *pStr, const char *pDelim, uint32 *tokenCount)
    {
    int i;
    uint32 tokenNum;
    char *pTokenStr;
    char *pBuff;
    char **pToken;
    eBool isInToken;

    i        = 0;
    tokenNum = 0;
    *tokenCount = 0;
    if((pStr == NULL) || ((pBuff = AtStrdup(pStr)) == NULL))
        {
        *tokenCount = 0;
        return NULL;
        }

    tokenNum = 0;
    i = 0;
    isInToken = cAtFalse;
    while (pBuff[i] != '\0')
        {
        if ((AtStrchr(pDelim, (uint16)pBuff[i]) != NULL))
            {
            if (isInToken == cAtTrue)
                {
                tokenNum++;
                isInToken = cAtFalse;
                i++;
                while (pBuff[i] != '\0')
                    {
                    if (AtStrchr(pDelim, (uint16)pBuff[i]) == NULL)
                        {
                        isInToken = cAtTrue;
                        i++;
                        break;
                        }
                    i++;
                    }
                }
            else
                {
                i++;
                }
            }
        else
            {
            isInToken = cAtTrue;
            i++;
            }
        }

    if (isInToken == cAtTrue)
        {
        tokenNum++;
        }


    if (tokenNum == 0)
        {
        *tokenCount = 0;
        return NULL;
        }

    pToken = (char**)AtOsalMemAlloc(tokenNum * sizeof(char*));
    if (pToken == NULL)
        return NULL;

    pToken[0]= AtStrdup(AtStrtok(pBuff, pDelim));
    i = 1;
    while ((pTokenStr = AtStrtok(NULL, pDelim)) != NULL)
        {
        pToken[i] = AtStrdup(pTokenStr);
        i++;
        }

    *tokenCount = tokenNum;

    AtOsalMemFree(pBuff);

    return pToken;
    }

/*
 * Some OS does not have strdup function, the following function is to replace it
 */
char *AtStrdup(const char *s)
    {
    char *newString;

    if (s == NULL)
        return NULL;

    /* Allocate memory */
    newString = AtOsalMemAlloc(AtStrlen(s) + 1);
    if (newString == NULL)
        return NULL;

    /* Copy original string and return */
    AtOsalMemCpy(newString, s, AtStrlen(s));
    newString[AtStrlen(s)] = '\0';

    return newString;
    }

/*
 * Convert string of bytes separated by '.' to array of bytes
 * @param pByteString Byte string
 * @param [out] pBytes Byte array
 * @param bufferSize Buffer size
 * @param base Base, 10 or 16
 *
 * @return Number of bytes are parsed or 0 if fail
 */
uint32 AtString2Bytes(const char *pByteString, uint8 *pBytes, uint32 bufferSize, uint8 base)
    {
    eBool isValid = cAtTrue;
    char **pItemList;
    uint32 numItem = 0, numItemsToConvert;
    uint32 itemIndex = 0;

    /* Cannot parse a NULL string */
    if (pByteString == NULL)
        return cAtFalse;

    /* Have all of byte tokens */
    pItemList = StrTokenGet(pByteString, ".", &numItem);

    /* If caller just want to know how many tokens */
    if ((pBytes == NULL) || (bufferSize == 0))
        {
        StringArrayFree(pItemList, numItem);
        return numItem;
        }

    /* Prevent overflow */
    numItemsToConvert = numItem;
    if (numItemsToConvert > bufferSize)
        numItemsToConvert = bufferSize;

    /* Convert all of them to bytes */
    for (itemIndex = 0; itemIndex < numItemsToConvert; itemIndex ++)
        {
        uint32 value;

        /* Make sure that character is valid */
        if (!StringNumberIsValid(pItemList[itemIndex], base))
            {
            isValid = cAtFalse;
            break;
            }

        /* Try to convert this value */
        value = AtStrtoul(pItemList[itemIndex], NULL, base);
        if (value > cBit7_0)
            {
            isValid = cAtFalse;
            break;
            }

        pBytes[itemIndex] = (uint8)value;
        }

    /* Release resource */
    StringArrayFree(pItemList, numItem);

    return isValid ? numItem : 0;
    }

char *AtBytes2String(const uint8 *buffer, uint32 bufferLength, uint8 base)
    {
    uint32 i;
    static char buf[cBufMaxLength];

    AtOsalMemInit((void*)buf, 0, sizeof(buf));

    if (bufferLength > cBufMaxLength)
        bufferLength = cBufMaxLength;

    for (i = 0; i < bufferLength; i++)
        {
        char cBuf[16];
        AtOsalMemInit((void*)cBuf, 0, sizeof(cBuf));

        if (base == 10) AtSprintf(cBuf, "%d", buffer[i]);
        if (base == 16) AtSprintf(cBuf, "%02x", buffer[i]);

        AtStrcat(buf, ".");
        AtStrcat(buf, cBuf);
        }

    return &buf[1];
    }

char *AtStringTrim(char *stringBuffer)
    {
    char *end;
    char *pString;

    /* Trim leading */
    pString = stringBuffer;
    while (AtIsSpace(*pString))
        pString = pString + 1;

    /* All spaces */
    if (*pString == 0)
      return pString;

    /* Trim trailing */
    end = pString + AtStrlen(pString) - 1;
    while (end > pString && AtIsSpace(*end))
        end = end - 1;

    /* Write new null terminator */
    *(end + 1) = 0;

    return pString;
    }

char *AtStrcpy(char *dest, const char *src)
    {
    return AtStdStrcpy(AtStdSharedStdGet(), dest, src);
    }

char *AtStrncpy(char *dest, const char *src, uint32 numBytes)
    {
    return AtStdStrncpy(AtStdSharedStdGet(), dest, src, numBytes);
    }

uint32 AtStrlen(const char *pString)
    {
    return AtStdStrlen(AtStdSharedStdGet(), pString);
    }

uint32 AtStrcmp(const char *pString1, const char *pString2)
    {
    return AtStdStrcmp(AtStdSharedStdGet(), pString1, pString2);
    }

uint32 AtStrncmp(const char *pString1, const char *pString2, uint32 numBytes)
    {
    return AtStdStrncmp(AtStdSharedStdGet(), pString1, pString2, numBytes);
    }

char *AtStrstr(const char *pString, const char *subString)
    {
    return AtStdStrstr(AtStdSharedStdGet(), pString, subString);
    }

char *AtStrtok(char *pString, const char *delim)
    {
    return AtStdStrtok(AtStdSharedStdGet(), pString, delim);
    }

char *AtStrtok_r(char *pString, const char *delim, char **saveptr)
    {
    return AtStdStrtok_r(AtStdSharedStdGet(), pString, delim, saveptr);
    }

char *AtStrcat(char *dest, const char *src)
    {
    return AtStdStrcat(AtStdSharedStdGet(), dest, src);
    }

char *AtStrncat(char *dest, const char *src, uint32 numBytes)
    {
    return AtStdStrncat(AtStdSharedStdGet(), dest, src, numBytes);
    }

char *AtStrchr(const char *pString, uint16 aChar)
    {
    return AtStdStrchr(AtStdSharedStdGet(), pString, aChar);
    }

char *AtStrrchr(const char *pString, uint16 aChar)
    {
    return AtStdStrrchr(AtStdSharedStdGet(), pString, aChar);
    }

int32 AtAtoi(const char *pString)
    {
    return AtStdAtoi(AtStdSharedStdGet(), pString);
    }

uint32 AtAtol(const char *pString)
    {
    return AtStdAtol(AtStdSharedStdGet(), pString);
    }

uint32 AtStrtol(const char *pString, char **endptr, uint32 base)
    {
    return AtStdStrtol(AtStdSharedStdGet(), pString, endptr, base);
    }

uint32 AtStrtoul(const char *pString, char **endptr, uint32 base)
    {
    return AtStdStrtoul(AtStdSharedStdGet(), pString, endptr, base);
    }

uint64 AtStrtoull(const char *pString, char **endptr, uint32 base)
    {
    return AtStdStrtoull(AtStdSharedStdGet(), pString, endptr, base);
    }

eBool AtOutOfRange(uint32 value, uint32 min, uint32 max)
    {
    return ((value < min) || (value > max));
    }

void AtRegFieldIns(uint32 *pRegVal, uint32 mask, uint32 shift, uint32 value)
    {
    *pRegVal = ((*pRegVal & ~mask) | ((value << shift) & mask));
    }

uint32 AtRegMaskToShift(uint32 mask)
    {
    uint32 shift;

    /* TODO: Consider its usage for performance */

    for (shift = 0; shift < 32; shift++)
        {
        if ((cBit0 << shift) & mask)
            break;
        }

    return shift;
    }

void AtStringArrayFree(char **pStringArray, uint32 numItem)
    {
    StringArrayFree(pStringArray, numItem);
    }

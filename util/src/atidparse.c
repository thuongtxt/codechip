/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : atidparse.c
 *
 * Created Date: Dec 7, 2011
 *
 * Description : This file contain implementation for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atidparse.h"
#include "../../cli/libs/include/AtIdParser.h"

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward Declarations ---------------------------*/

/*--------------------------- Functions --------------------------------------*/
static eAtStdCompareResult CompareUint32Func(const void *id1, const void *id2)
    {
    uint32 myId1 = *((const uint32*)id1);
    uint32 myId2 = *((const uint32*)id2);

    if (myId1 > myId2)
        return cAtStdCompareResultGreaterThan;
    if (myId1 < myId2)
        return cAtStdCompareResultLessThan;

    return cAtStdCompareResultEqual;
    }

/**
 * @brief This function is used to parse a string (multiple, hierarchical format)
 * into a list of flat ID. Multiple ID list strings in input string are separated
 * by the "," token. Please note all ID list strings MUST have the same hierarchical
 * level.
 *
 * @param [in] pStrId The string required parsing.
 * @param [in] pFormatMin The format of minimum value of complex number.
 * @param [in] pFormatMax The format of maximum value of complex number.
 * @param [in] maxItem Maximum number of expected returned items.
 *
 * @param [out] pIdList The returned ID list.
 * @param [out] pNumItem Number of returned items.
 *
 * @retval [1..256] Hierarchical level.
 * @retval 0 Parsing procedure fails.
 */
uint8 IdComplexNumParse(char   *pStrId,
                        char   *pFormatMin,
                        char   *pFormatMax,
                        uint32  maxItem,
                        uint32 *pIdList,
                        uint32 *pNumItem)
    {
    AtIdParser parser = AtIdParserNew(pStrId, pFormatMin, pFormatMax);
    uint32 i = 0;
    AtUnused(maxItem);

    if (pNumItem)
        *pNumItem = 0;

    if (parser == NULL)
        return 0;

    while (AtIdParserHasNextNumber(parser))
        {
        pIdList[i] = AtIdParserNextNumber(parser);
        i = i + 1;
        }

    if (pNumItem)
        *pNumItem = i;

    AtObjectDelete((AtObject)parser);

    return 1;
    } /* End of IdComplexNumParse() */

/**
 * @brief This function convert a list of ID in dword list into a string.
 *
 * @param [in] dwordList Id list.
 * @param [in] listLengInDword Number of IDs.
 * @param [in] retStr String buffer.
 * @param [in] strLength Length of the string.
 *
 * @param [out] retStr The returned ID list in string.
 *
 * @retval Real return string Length.
 */
uint32 IdDwordListToString(char *retStr, uint32 strBufLength, uint32 *dwordList, uint32 listLengInDword)
    {
    AtStd std = AtStdSharedStdGet();
    uint32 currentIdx = 0, start, end;
    char tmpStr[128];

    if (!retStr || !dwordList || (listLengInDword == 0) || (strBufLength < 2))
        return 0;

    AtStdSort(std, &dwordList[0], listLengInDword, sizeof(uint32), CompareUint32Func);
    AtOsalMemInit(retStr, 0, strBufLength);

    start = dwordList[currentIdx];
    end = dwordList[currentIdx];
    while (currentIdx < listLengInDword && (AtStrlen(retStr) + 1) < strBufLength)
        {
        while ((currentIdx + 1) < listLengInDword)
            {
            end = dwordList[currentIdx + 1];
            if ((end - dwordList[currentIdx]) == 1)
                currentIdx++;
            else
                {
                end = dwordList[currentIdx];
                break;
                }

            }
        if (start == end)
            AtSprintf(tmpStr, "%d", start);
        else
            AtSprintf(tmpStr, "%d-%d", start, end);
        if ((currentIdx + 1) < listLengInDword)
            AtStrcat(tmpStr, ",");
        if (AtStrlen(retStr) + AtStrlen(tmpStr) + 1 < strBufLength)
            AtStrcat(retStr, tmpStr);

        currentIdx++;
        start = dwordList[currentIdx];
        end = dwordList[currentIdx];
        }

    return AtStrlen(retStr);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtStdInternal.h
 * 
 * Created Date: Sep 24, 2014
 *
 * Description : To abstract C standard library
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDINTERNAL_H_
#define _ATSTDINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtStd.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumListener 16

/*--------------------------- Macros -----------------------------------------*/
/* Access methods */
#define mMethodsGet(object) (object)->methods
#define mMethodsSet(object, methods) mMethodsGet(object) = methods
#define mMethodOverride(methods, methodName) (methods).methodName = methodName

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtStdMethods
    {
    /* Console printing */
    uint32 (*Printf)(AtStd self, const char *format, va_list ap) AtAttributePrintf(2, 0);
    void (*PrintFuncSet)(AtStd self, int (*printFunc)(void *appContext, char *aString));
    void (*AppContextSet)(AtStd self, void *appContext);

    /* String library */
    char *(*Strcpy)(AtStd self, char *dest, const char *src);
    char *(*Strncpy)(AtStd self, char *dest, const char *src, uint32 numBytes);
    uint32 (*Strlen)(AtStd self, const char *pString);
    uint32 (*Strcmp)(AtStd self, const char *pString1, const char *pString2);
    uint32 (*Strncmp)(AtStd self, const char *pString1, const char *pString2, uint32 numBytes);
    char *(*Strstr)(AtStd self, const char *pString, const char *subString) AtAttributePrintf(2, 0);
    char *(*Strtok)(AtStd self, char *pString, const char *delim);
    char *(*Strtok_r)(AtStd self, char *pString, const char *delim, char **saveptr);
    char *(*Strcat)(AtStd self, char *dest, const char *src);
    char *(*Strncat)(AtStd self, char *dest, const char *src, uint32 numBytes);
    char *(*Strchr)(AtStd self, const char *pString, uint16 aChar);
    char *(*Strrchr)(AtStd self, const char *pString, uint16 aChar);
    uint32 (*Sprintf)(AtStd self, char *str, const char *format, va_list ap) AtAttributePrintf(2, 0);
    uint32 (*Snprintf)(AtStd self, char *str, uint32 size, const char *format, va_list ap) AtAttributePrintf(4, 0);

    /* Value conversion */
    int32 (*Atoi)(AtStd self, const char *pString);
    uint32 (*Atol)(AtStd self, const char *pString);
    uint32 (*Strtol)(AtStd self, const char *pString, char **endptr, uint32 base);
    uint32 (*Strtoul)(AtStd self, const char *pString, char **endptr, uint32 base);
    uint32 (*Abs)(AtStd self, int value);
    uint64 (*Strtoull)(AtStd self, const char *pString, char **endptr, uint32 base);

    /* Random value */
    uint32 (*Rand)(AtStd self);
    void (*RandSeed)(AtStd self);

    /* File management */
    AtFile (*FileCreate)(AtStd self, const char *filePath, uint8 mode);
    AtFile (*StandardOutput)(AtStd self);
    AtFile (*StandardError)(AtStd self);
    AtFile (*StandardInput)(AtStd self);

    /* Sorting */
    void (*Sort)(AtStd self, void *firstElement, uint32 numElements, uint32 memberSize,
    eAtStdCompareResult (*CompareFunc)(const void *, const void *));
    }tAtStdMethods;

typedef struct tStdListenerWrapper
    {
    tAtStdListener listener;
    void *userData;
    eBool valid;
    }tStdListenerWrapper;

typedef struct tAtStd
    {
    const tAtStdMethods *methods;
    eBool colorDisabled;
    eBool printDisabled;

    /* Array of listeners as memory allocation should not be done in STD context */
    tStdListenerWrapper listener[cMaxNumListener];
    uint32 numListeners;
    }tAtStd;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtStd AtStdObjectInit(AtStd self);

/* Notification */
void AtStdWillPrintNotify(AtStd self, char *aString);
void AtStdDidPrintNotify(AtStd self, char *aString);

#endif /* _ATSTDINTERNAL_H_ */


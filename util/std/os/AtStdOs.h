/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtStdOs.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : OS specific header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDOS_H_
#define _ATSTDOS_H_

/*--------------------------- Includes ---------------------------------------*/
#ifdef AT_NONE_OS
#include "AtStdOsNone.h"
#else
#include "AtStdOsStandard.h"
#endif

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATSTDOS_H_ */


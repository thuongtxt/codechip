/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtStdOsNone.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : To work with environment that does not have OS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDOSNONE_H_
#define _ATSTDOSNONE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern int vprintf(const char *, va_list);
extern char *strcpy(char * dst, const char * src);
extern char *strncpy(char * dst, const char * src, size_t len);
extern size_t strlen(const char *);
extern int  strcmp(const char *, const char *);
extern int strncmp(const char *, const char *, size_t);
extern char *strstr(const char *, const char *);
extern char *strtok(char *, const char *);
extern char *strtok_r(char *str, const char *delim, char **saveptr);
extern char *strcat(char *, const char *);
extern char *strncat(char *, const char *, size_t);
extern char *strchr(const char *, int);
extern char *strrchr(const char *, int);
extern int vsprintf(char *str, const char *format, va_list ap);
extern int vsnprintf(char *str, size_t size, const char *format, va_list ap);
extern long strtol(const char *str, char **endptr, int base);
extern long atol(const char *str);
extern unsigned long strtoul(const char *str, char **endptr, int base);
extern void srand(unsigned seed);
extern int rand(void);
extern int abs(int);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATSTDOSNONE_H_ */


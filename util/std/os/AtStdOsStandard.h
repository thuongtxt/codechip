/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtStdOsStandard.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : To work with environments that have OS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDOSSTANDARD_H_
#define _ATSTDOSSTANDARD_H_

/*--------------------------- Includes ---------------------------------------*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATSTDOSSTANDARD_H_ */


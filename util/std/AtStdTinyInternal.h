/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtStdTinyInternal.h
 * 
 * Created Date: Sep 24, 2014
 *
 * Description : Tiny STD
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDTINYINTERNAL_H_
#define _ATSTDTINYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtStdInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cStdMsgBufferLength 10 * 1024

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtStdTiny
    {
    tAtStd super;

    void *appContext;
    int (*printFunc)(void *appContext, char *aString);
    char sharedMsgBuffer[cStdMsgBufferLength];
    }tAtStdTiny;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtStd AtStdTinyObjectInit(AtStd self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSTDTINYINTERNAL_H_ */

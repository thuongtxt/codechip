/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtStdDefaultInternal.h
 * 
 * Created Date: Sep 24, 2014
 *
 * Description : STD Default
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDDEFAULTINTERNAL_H_
#define _ATSTDDEFAULTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtStdTinyInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtStdDefault
    {
    tAtStdTiny super;
    }tAtStdDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtStd AtStdDefaultObjectInit(AtStd self);

#endif /* _ATSTDDEFAULTINTERNAL_H_ */


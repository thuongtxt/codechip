/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : AtStdDefault.c
 *
 * Created Date: Feb 25, 2014
 *
 * Description : Default implementation of C Standard Library
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "AtOsal.h"
#include "AtStdDefaultInternal.h"
#include "file/AtFileDefault.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtStdMethods m_AtStdOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtFile FileCreate(AtStd self, const char *filePath, uint8 mode)
    {
    AtUnused(self);
    AtUnused(filePath);
    AtUnused(mode);
    return AtFileDefaultNew(filePath, mode);
    }

static AtFile StandardOutput(AtStd self)
    {
    AtUnused(self);
    return AtFileDefaultStandardOutput();
    }

static AtFile StandardError(AtStd self)
    {
    AtUnused(self);
    return AtFileDefaultStandardError();
    }

static AtFile StandardInput(AtStd self)
    {
    AtUnused(self);
    return AtFileDefaultStandardInput();
    }

static void Sort(AtStd self, void *firstElement, uint32 numElements, uint32 memberSize,
                 eAtStdCompareResult (*CompareFunc)(const void *, const void *))
    {
    AtUnused(self);
    qsort(firstElement, numElements, memberSize, CompareFunc);
    }

static void OverrideAtStd(AtStd self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtStdOverride, mMethodsGet(self), sizeof(m_AtStdOverride));

        mMethodOverride(m_AtStdOverride, FileCreate);
        mMethodOverride(m_AtStdOverride, StandardOutput);
        mMethodOverride(m_AtStdOverride, StandardError);
        mMethodOverride(m_AtStdOverride, StandardInput);
        mMethodOverride(m_AtStdOverride, Sort);
        }

    mMethodsSet(self, &m_AtStdOverride);
    }

static void Override(AtStd self)
    {
    OverrideAtStd(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtStdDefault);
    }

AtStd AtStdDefaultObjectInit(AtStd self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtStdTinyObjectInit(self) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtStd AtStdDefaultSharedStd(void)
    {
    static tAtStdDefault m_std;
    static AtStd m_sharedStd = NULL;

    if (m_sharedStd == NULL)
        m_sharedStd = AtStdDefaultObjectInit((AtStd)&m_std);

    return m_sharedStd;
    }

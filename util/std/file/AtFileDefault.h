/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : STD
 * 
 * File        : AtFileDefault.h
 * 
 * Created Date: Mar 3, 2015
 *
 * Description : Default file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFILEDEFAULT_H_
#define _ATFILEDEFAULT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFile.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFile AtFileDefaultStandardInput(void);
AtFile AtFileDefaultStandardOutput(void);
AtFile AtFileDefaultStandardError(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATFILEDEFAULT_H_ */


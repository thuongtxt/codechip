/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtFileDefaultInternal.h
 * 
 * Created Date: Sep 23, 2014
 *
 * Description : File default implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFILEDEFAULTINTERNAL_H_
#define _ATFILEDEFAULTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdio.h>
#include "AtFileInternal.h"
#include "AtFileDefault.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFileDefault * AtFileDefault;

typedef struct tAtFileDefault
    {
    tAtFile super;

    /* Private */
    FILE *file;
    char *filePath;
    }tAtFileDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFile AtFileDefaultObjectInit(AtFile self, const char *filePath, uint8 mode);

#endif /* _ATFILEDEFAULTINTERNAL_H_ */


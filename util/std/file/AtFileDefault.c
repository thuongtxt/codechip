/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtFileDefault.c
 *
 * Created Date: Sep 17, 2014
 *
 * Description : Default implementation of file operation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "../AtStdInternal.h"
#include "AtFileDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)  ((AtFileDefault)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtFileMethods m_AtFileOverride;

/* Save supper implementation */
static const tAtFileMethods *m_AtFileMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int Closed(AtFile self)
    {
    int ret = 0;

    if (mThis(self)->file)
        {
        ret = fclose(mThis(self)->file);
        AtOsalMemFree(mThis(self)->filePath);
        }

    ret |= m_AtFileMethods->Closed(self);

    return ret;
    }

static int Flush(AtFile self)
    {
    return fflush(mThis(self)->file);
    }

static char *Gets(AtFile self, char *buffer, uint32 numBytes)
    {
    return fgets(buffer, (int)numBytes, mThis(self)->file);
    }

static int SeekWhence(eAtFileSeekPosition position)
    {
    switch (position)
        {
        case cAtFileSeekPositionAt_SEEK_SET:
            return SEEK_SET;
        case cAtFileSeekPositionAt_SEEK_CUR:
            return SEEK_CUR;
        case cAtFileSeekPositionAt_SEEK_END:
            return SEEK_END;
        default:
            return position;
        }
    }

static int Seek(AtFile self, long int offset, eAtFileSeekPosition whence)
    {
    return fseek(mThis(self)->file, offset, SeekWhence(whence));
    }

static uint32 Read(AtFile self, void *buffer, uint32 numBytes, uint32 numMember)
    {
    return fread(buffer, numBytes, numMember, mThis(self)->file);
    }

static uint32 Write(AtFile self, const void *buffer, uint32 numBytes, uint32 numMember)
    {
    return fwrite(buffer, numBytes, numMember, mThis(self)->file);
    }

AtAttributePrintf(2, 0)
static uint32 VPrintf(AtFile self, const char *format, va_list ap)
    {
    return (uint32)vfprintf(mThis(self)->file, format, ap);
    }

static int Puts(AtFile self, const char *string)
    {
    return fputs(string, mThis(self)->file);
    }

static eBool Eof(AtFile self)
    {
    return (eBool)(feof(mThis(self)->file) ? cAtTrue : cAtFalse);
    }

static const char *Mode2Str(uint8 mode)
    {
    switch (mode)
        {
        case cAtFileOpenModeRead:
            return "r";
        case cAtFileOpenModeWrite:
            return "w";
        case cAtFileOpenModeAppend:
            return "a";
        default:
            break;
        }

    /* Need to separate 2 switches, below switch consider value type in case is uint8 not eAtFileOpenMode */
    switch (mode)
        {
        case (cAtFileOpenModeRead  | cAtFileOpenModeWrite):
            return "r+";
        case (cAtFileOpenModeWrite | cAtFileOpenModeTruncate):
            return "w+";
        case (cAtFileOpenModeRead  | cAtFileOpenModeAppend):
            return "a+";
        case (cAtFileOpenModeWrite | cAtFileOpenModeTruncate | cAtFileOpenModeAppend):
            return "aw+";
        default:
            break;
        }

    return NULL;
    }

static const char *Path(AtFile self)
    {
    return mThis(self)->filePath;
    }

static void OverrideAtFile(AtFile self)
    {
    if (!m_methodsInit)
        {
        m_AtFileMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtFileOverride, m_AtFileMethods, sizeof(m_AtFileOverride));

        mMethodOverride(m_AtFileOverride, Closed);
        mMethodOverride(m_AtFileOverride, Flush);
        mMethodOverride(m_AtFileOverride, Gets);
        mMethodOverride(m_AtFileOverride, Seek);
        mMethodOverride(m_AtFileOverride, Read);
        mMethodOverride(m_AtFileOverride, Write);
        mMethodOverride(m_AtFileOverride, VPrintf);
        mMethodOverride(m_AtFileOverride, Puts);
        mMethodOverride(m_AtFileOverride, Eof);
        mMethodOverride(m_AtFileOverride, Path);
        }

    mMethodsSet(self, &m_AtFileOverride);
    }

static void Override(AtFile self)
    {
    OverrideAtFile(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFileDefault);
    }

static AtFile ObjectInitWithFile(AtFileDefault self, FILE *file)
    {
    AtFileDefaultObjectInit((AtFile)self, NULL, 0);
    self->file = file;
    return (AtFile)self;
    }

AtFile AtFileDefaultObjectInit(AtFile self, const char *filePath, uint8 mode)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtFileObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Open file and catches it */
    if (filePath == NULL)
        return self;

    if (mThis(self)->file)
        fclose(mThis(self)->file);

    mThis(self)->file = fopen(filePath, Mode2Str(mode));
    if (mThis(self)->file == NULL)
        {
        AtFileClose(self);
        return NULL;
        }

    mThis(self)->filePath = AtStrdup(filePath);

    return self;
    }

AtFile AtFileDefaultNew(const char *filePath, uint8 mode)
    {
    AtFile newFile = AtOsalMemAlloc(ObjectSize());
    if (newFile == NULL)
        return NULL;

    return AtFileDefaultObjectInit(newFile, filePath, mode);
    }

AtFile AtFileDefaultStandardInput(void)
    {
    static tAtFileDefault m_file;
    static AtFile file = NULL;

    if (file == NULL)
        file = ObjectInitWithFile(&m_file, stdin);

    /* On some environments, stdin is changed for every new session, it is better to
     * always update this. Otherwise, TextUI will never work */
    else
        m_file.file = stdin;

    return file;
    }

AtFile AtFileDefaultStandardOutput(void)
    {
    static tAtFileDefault m_file;
    static AtFile file = NULL;

    if (file == NULL)
        file = ObjectInitWithFile(&m_file, stdout);

    /* On some environments, stdout is changed for every new session, it is better to
     * always update this. Otherwise, nothing will be displayed on standard output */
    else
        m_file.file = stdout;

    return file;
    }

AtFile AtFileDefaultStandardError(void)
    {
    static tAtFileDefault m_file;
    static AtFile file = NULL;

    if (file == NULL)
        file = ObjectInitWithFile(&m_file, stderr);

    /* On some environments, stderr is changed for every new session, it is better to
     * always update this. Otherwise, nothing will be displayed on standard error */
    else
        m_file.file = stderr;

    return file;
    }

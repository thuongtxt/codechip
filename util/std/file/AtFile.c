/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtFile.c
 *
 * Created Date: Sep 17, 2014
 *
 * Description : To abstract file operation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../AtStdInternal.h"
#include "AtFileInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNotImplement -1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((AtFile)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtFileMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int Closed(AtFile self)
    {
    if (self)
        AtOsalMemFree(self);
    return 0;
    }

static int Flush(AtFile self)
    {
    AtUnused(self);
    return cNotImplement;
    }

static char *Gets(AtFile self, char *buffer, uint32 numBytes)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(numBytes);
    return NULL;
    }

static int Seek(AtFile self, long int offset, eAtFileSeekPosition whence)
    {
    AtUnused(self);
    AtUnused(offset);
    AtUnused(whence);
    return -1;
    }

static uint32 Read(AtFile self, void *buffer, uint32 numBytes, uint32 numMember)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(numBytes);
    AtUnused(numMember);
    return 0;
    }

static uint32 Write(AtFile self,
                    const void *buffer,
                    uint32 numBytes,
                    uint32 numMember)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(numBytes);
    AtUnused(numMember);
    return 0;
    }

static uint32 VPrintf(AtFile self, const char *format, va_list ap)
    {
    AtUnused(self);
    AtUnused(format);
    AtUnused(ap);
    return 0;
    }

static int Puts(AtFile self, const char *string)
    {
    AtUnused(self);
    AtUnused(string);
    return cNotImplement;
    }

static eBool Eof(AtFile self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static const char *Path(AtFile self)
    {
    AtUnused(self);
    return NULL;
    }

static void MethodsInit(AtFile self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Closed);
        mMethodOverride(m_methods, Flush);
        mMethodOverride(m_methods, Gets);
        mMethodOverride(m_methods, Seek);
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, VPrintf);
        mMethodOverride(m_methods, Puts);
        mMethodOverride(m_methods, Eof);
        mMethodOverride(m_methods, Path);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFile);
    }

AtFile AtFileObjectInit(AtFile self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

int AtFileClose(AtFile self)
    {
    if (self)
        return mMethodsGet(self)->Closed(self);

    return cNotImplement;
    }

int AtFileFlush(AtFile self)
    {
    if (self)
        return mMethodsGet(self)->Flush(self);

    return cNotImplement;
    }

char *AtFileGets(AtFile self, char *buffer, uint32 numBytes)
    {
    if (self)
        return mMethodsGet(self)->Gets(self, buffer, numBytes);

    return NULL;
    }

int AtFileSeek(AtFile self, long int offset, eAtFileSeekPosition whence)
    {
    if (self)
        return mMethodsGet(self)->Seek(self, offset, whence);

    return -1;
    }

uint32 AtFileRead(AtFile self, void *buffer, uint32 numBytes, uint32 numMember)
    {
    if (self)
        return mMethodsGet(self)->Read(self, buffer, numBytes, numMember);

    return 0;
    }

uint32 AtFileWrite(AtFile self,
                   const void *buffer,
                   uint32 numBytes,
                   uint32 numMember)
    {
    if (self)
        return mMethodsGet(self)->Write(self, buffer, numBytes, numMember);

    return 0;
    }

uint32 AtFilePrintf(AtFile self, const char *format, ...)
    {
    uint32 numChar;
    va_list args;

    if (self == NULL)
        return 0;

    va_start(args, format);
    numChar = mMethodsGet(self)->VPrintf(self, format, args);
    va_end(args);

    return numChar;
    }

uint32 AtFileVPrintf(AtFile self, const char *format, va_list ap)
    {
    if (self)
        return mMethodsGet(self)->VPrintf(self, format, ap);

    return 0;
    }

int AtFilePuts(AtFile self, const char *string)
    {
    if (self)
        return mMethodsGet(self)->Puts(self, string);

    return cNotImplement;
    }

eBool AtFileEof(AtFile self)
    {
    if (self)
        return mMethodsGet(self)->Eof(self);

    return cAtTrue;
    }

const char *AtFilePath(AtFile self)
    {
    if (self)
        return mMethodsGet(self)->Path(self);
    return NULL;
    }

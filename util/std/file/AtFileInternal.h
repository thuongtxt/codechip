/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtFileInternal.h
 * 
 * Created Date: Sep 17, 2014
 *
 * Description : Abstract file operations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFILEINTERNAL_H_
#define _ATFILEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFileMethods
    {
    int (*Closed)(AtFile file);
    int (*Flush)(AtFile self);
    char *(*Gets)(AtFile self, char *buffer, uint32 numBytes);
    int (*Seek)(AtFile self, long int offset, eAtFileSeekPosition whence);
    uint32 (*Read)(AtFile self, void *buffer, uint32 numBytes, uint32 numElements);
    uint32 (*Write)(AtFile self, const void *buffer, uint32 numBytes, uint32 numElements);
    uint32 (*VPrintf)(AtFile self, const char *format, va_list ap) AtAttributePrintf(2, 0);
    int (*Puts)(AtFile self, const char *string);
    eBool (*Eof)(AtFile self);
    const char *(*Path)(AtFile self);
    }tAtFileMethods;

typedef struct tAtFile
    {
    const tAtFileMethods *methods;
    }tAtFile;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFile AtFileObjectInit(AtFile self);

#endif /* _ATFILEINTERNAL_H_ */


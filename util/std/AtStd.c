/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtStd.c
 *
 * Created Date: Feb 6, 2014
 *
 * Description : To abstract C standard library
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtStdInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtStdMethods m_methods;

static AtStd m_sharedStd = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Printf(AtStd self, const char *format, va_list ap)
    {
    AtUnused(self);
    AtUnused(format);
    AtUnused(ap);
    return 0;
    }

static char *Strcpy(AtStd self, char *dest, const char *src)
    {
    AtUnused(self);
    AtUnused(dest);
    AtUnused(src);
    return NULL;
    }

static char *Strncpy(AtStd self, char *dest, const char *src, uint32 numBytes)
    {
    AtUnused(self);
    AtUnused(dest);
    AtUnused(src);
    AtUnused(numBytes);
    return NULL;
    }

static uint32 Strlen(AtStd self, const char *pString)
    {
    AtUnused(self);
    AtUnused(pString);
    return 0;
    }

static uint32 Strcmp(AtStd self, const char *pString1, const char *pString2)
    {
    AtUnused(self);
    AtUnused(pString1);
    AtUnused(pString2);
    return 0;
    }

static uint32 Strncmp(AtStd self, const char *pString1, const char *pString2, uint32 numBytes)
    {
    AtUnused(self);
    AtUnused(pString1);
    AtUnused(pString2);
    AtUnused(numBytes);
    return 0;
    }

static char *Strstr(AtStd self, const char *pString, const char *subString)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(subString);
    return NULL;
    }

static char *Strtok(AtStd self, char *pString, const char *delim)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(delim);
    return NULL;
    }

static char *Strtok_r(AtStd self, char *pString, const char *delim, char **saveptr)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(delim);
    AtUnused(saveptr);
    return NULL;
    }

static char *Strcat(AtStd self, char *dest, const char *src)
    {
    AtUnused(self);
    AtUnused(dest);
    AtUnused(src);
    return NULL;
    }

static char *Strncat(AtStd self, char *dest, const char *src, uint32 numBytes)
    {
    AtUnused(self);
    AtUnused(dest);
    AtUnused(src);
    AtUnused(numBytes);
    return NULL;
    }

static char *Strchr(AtStd self, const char *pString, uint16 aChar)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(aChar);
    return NULL;
    }

static char *Strrchr(AtStd self, const char *pString, uint16 aChar)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(aChar);
    return NULL;
    }

static uint32 Sprintf(AtStd self, char *str, const char *format, va_list ap)
    {
    AtUnused(self);
    AtUnused(str);
    AtUnused(format);
    AtUnused(ap);
    return 0;
    }

static uint32 Snprintf(AtStd self, char *str, uint32 size, const char *format, va_list ap)
    {
    AtUnused(self);
    AtUnused(str);
    AtUnused(size);
    AtUnused(format);
    AtUnused(ap);
    return 0;
    }

static int32 Atoi(AtStd self, const char *pString)
    {
    AtUnused(self);
    AtUnused(pString);
    return 0;
    }

static uint32 Atol(AtStd self, const char *pString)
    {
    AtUnused(self);
    AtUnused(pString);
    return 0;
    }

static uint32 Strtol(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(endptr);
    AtUnused(base);
    return 0;
    }

static uint32 Strtoul(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(endptr);
    AtUnused(base);
    return 0;
    }

static uint64 Strtoull(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    AtUnused(self);
    AtUnused(pString);
    AtUnused(endptr);
    AtUnused(base);
    return 0;
    }

static AtFile FileCreate(AtStd self, const char *filePath, uint8 mode)
    {
    AtUnused(self);
    AtUnused(filePath);
    AtUnused(mode);
    return NULL;
    }

static AtFile StandardOutput(AtStd self)
    {
    AtUnused(self);
    return NULL;
    }

static AtFile StandardError(AtStd self)
    {
    AtUnused(self);
    return NULL;
    }

static AtFile StandardInput(AtStd self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 Abs(AtStd self, int value)
    {
    AtUnused(self);
    return (uint32)((value < 0) ? -value : value);
    }

static uint32 Rand(AtStd self)
    {
    AtUnused(self);
    return 0;
    }

static void RandSeed(AtStd self)
    {
    AtUnused(self);
    }

static void Sort(AtStd self, void *firstElement, uint32 numElements, uint32 memberSize,
                 eAtStdCompareResult (*CompareFunc)(const void *, const void *))
    {
    AtUnused(self);
    AtUnused(firstElement);
    AtUnused(numElements);
    AtUnused(memberSize);
    AtUnused(CompareFunc);
    }

static void PrintFuncSet(AtStd self, int (*printFunc)(void *appContext, char *aString))
    {
    AtUnused(self);
    AtUnused(printFunc);
    }

static void AppContextSet(AtStd self, void *appContext)
    {
    AtUnused(self);
    AtUnused(appContext);
    }

static uint32 PutString(AtStd std, char *idString, uint32 remainingBytes, const char *aString)
    {
    uint32 len = AtStdStrlen(std, aString);

    AtStdStrncat(std, idString, aString, remainingBytes);
    if (len > remainingBytes)
        return 0;

    return (remainingBytes - len);
    }

static uint32 PutRange(AtStd std, char *idString, uint32 remainingBytes, uint32 startNumber, uint32 lastNumber)
    {
    char rangeBuffer[32];

    if (startNumber == lastNumber)
        AtSnprintf(rangeBuffer, sizeof(rangeBuffer), "%d", lastNumber);
    else
        AtSnprintf(rangeBuffer, sizeof(rangeBuffer), "%d-%d", startNumber, lastNumber);

    return PutString(std, idString, remainingBytes, rangeBuffer);
    }

static char *NumbersToString(AtStd self, char *idString, uint32 idBufSize, uint32 *numbers, uint32 numNumbers)
    {
    uint32 number_i;
    uint32 startNumber = cInvalidUint32;
    uint32 lastNumber = cInvalidUint32;
    eBool firstPrinted = cAtTrue;
    uint32 remainBytes = idBufSize;

    /* If there is only one number, treat it in a special way, so that remaining
     * code will be simple */
    AtOsalMemInit(idString, 0, idBufSize);
    if (numNumbers == 1)
        {
        AtSnprintf(idString, idBufSize, "%d", numbers[0]);
        return idString;
        }

    /* Should sort first to have optimize output */
    AtStdSort(self, numbers, numNumbers, sizeof(uint32), AtStdUint32CompareFunction);

    /* Detect all of possible sequences */
    startNumber = numbers[0];
    lastNumber  = startNumber;
    for (number_i = 0; number_i < numNumbers; number_i++)
        {
        uint32 currentNumber = numbers[number_i];

        /* Have new sequence */
        if ((currentNumber == (lastNumber + 1)) ||
            (currentNumber == lastNumber))
            {
            lastNumber = currentNumber;
            continue;
            }

        /* Sequence is end, put this range to the ID string */
        if (firstPrinted)
            firstPrinted = cAtFalse;
        else
            remainBytes = PutString(self, idString, remainBytes, ",");

        if (remainBytes <= 1)
            return idString;

        remainBytes = PutRange(self, idString, remainBytes, startNumber, lastNumber);
        if (remainBytes <= 1)
            return idString;

        /* Prepare for new sequence */
        startNumber = currentNumber;
        lastNumber  = currentNumber;
        }

    /* Put this range to the ID string */
    if (!firstPrinted)
        remainBytes = PutString(self, idString, remainBytes, ",");
    if (remainBytes > 1)
        remainBytes = PutRange(self, idString, remainBytes, startNumber, lastNumber);

    return idString;
    }

static uint32 TwoLevelPutRange(AtStd std, char *idString, uint32 remainingBytes,
                               uint32 startLevel1Number, uint32 startLevel2Number,
                               uint32 lastLevel1Number, uint32 lastLevel2Number)
    {
    char rangeBuffer[32];

    if ((startLevel1Number == lastLevel1Number) && (startLevel2Number == lastLevel2Number))
        AtSnprintf(rangeBuffer, sizeof(rangeBuffer), "%d.%d", lastLevel1Number, lastLevel2Number);
    else
        AtSnprintf(rangeBuffer, sizeof(rangeBuffer), "%d.%d-%d.%d", startLevel1Number, startLevel2Number, lastLevel1Number, lastLevel2Number);

    return PutString(std, idString, remainingBytes, rangeBuffer);
    }

static char *TwoLevelNumbersToString(AtStd self, char *idString, uint32 idBufSize,
                                     uint32 *level1Numbers, uint32 *level2Numbers, uint32 numNumbers, uint32 maxLevel2Number)
    {
    uint32 number_i;
    uint32 startLevel2Number = cInvalidUint32;
    uint32 lastLevel2Number = cInvalidUint32;
    uint32 startLevel1Number = cInvalidUint32;
    uint32 lastLevel1Number = cInvalidUint32;
    eBool firstPrinted = cAtTrue;
    uint32 remainBytes = idBufSize;

    /* If there is only one number, treat it in a special way, so that remaining
     * code will be simple */
    AtOsalMemInit(idString, 0, idBufSize);
    if (numNumbers == 1)
        {
        AtSnprintf(idString, idBufSize, "%d.%d", level1Numbers[0], level2Numbers[0]);
        return idString;
        }

    /* Detect all of possible sequences */
    startLevel1Number = level1Numbers[0];
    lastLevel1Number  = startLevel1Number;
    startLevel2Number = level2Numbers[0];
    lastLevel2Number  = startLevel2Number;
    number_i = 0;
    while (number_i < numNumbers)
        {
        uint32 currentLevel1Number = level1Numbers[number_i];
        uint32 currentLevel2Number = level2Numbers[number_i];

        /* Have new level-1 sequence (due to roll-over level-2 sequence) */
        if ((currentLevel1Number == (lastLevel1Number + 1)) &&
            (lastLevel2Number == maxLevel2Number) &&
            (currentLevel2Number == 1))
            {
            lastLevel1Number = currentLevel1Number;
            lastLevel2Number = currentLevel2Number;
            number_i++;
            continue;
            }

        /* Have new level-2 sequence */
        if ((currentLevel1Number == lastLevel1Number) &&
            ((currentLevel2Number == (lastLevel2Number + 1)) ||
             (currentLevel2Number == lastLevel2Number)))
            {
            lastLevel2Number = currentLevel2Number;
            number_i++;
            continue;
            }

        /* Sequence is end, put this range to the ID string */
        if (firstPrinted)
            firstPrinted = cAtFalse;
        else
            remainBytes = PutString(self, idString, remainBytes, ",");

        if (remainBytes <= 1)
            return idString;

        remainBytes = TwoLevelPutRange(self, idString, remainBytes, startLevel1Number, startLevel2Number, lastLevel1Number, lastLevel2Number);
        if (remainBytes <= 1)
            return idString;

        /* Prepare for new sequence */
        startLevel1Number = currentLevel1Number;
        lastLevel1Number  = currentLevel1Number;
        startLevel2Number = currentLevel2Number;
        lastLevel2Number  = currentLevel2Number;
        number_i++;
        }

    /* Put this range to the ID string */
    if (!firstPrinted)
        remainBytes = PutString(self, idString, remainBytes, ",");
    if (remainBytes > 1)
        remainBytes = TwoLevelPutRange(self, idString, remainBytes, startLevel1Number, startLevel2Number, lastLevel1Number, lastLevel2Number);

    return idString;
    }

static eAtStdCompareResult Compare(uint32 value1, uint32 value2)
    {
    if (value1 == value2)
        return cAtStdCompareResultEqual;

    if (value1 > value2)
        return cAtStdCompareResultGreaterThan;

    return cAtStdCompareResultLessThan;
    }

static tStdListenerWrapper *EventListenerFind(AtStd self,
                                              void *userData, const tAtStdListener *listener,
                                              uint32 *listenerIndex)
    {
    uint32 listener_i;

    for (listener_i = 0; listener_i < cMaxNumListener; listener_i++)
        {
        tStdListenerWrapper *aListener = &(self->listener[listener_i]);

        if (!aListener->valid)
            continue;

        if ((aListener->userData == userData) &&
            (AtOsalMemCmp(&(aListener->listener), listener, sizeof(tAtStdListener)) == 0))
            {
            if (listenerIndex)
                *listenerIndex = listener_i;
            return aListener;
            }
        }

    return NULL;
    }

static tStdListenerWrapper *FreeListenerFind(AtStd self)
    {
    uint32 listener_i;

    for (listener_i = 0; listener_i < cMaxNumListener; listener_i++)
        {
        tStdListenerWrapper *aListener = &(self->listener[listener_i]);

        if (!aListener->valid)
            return aListener;
        }

    return NULL;
    }

static void ListenerAdd(AtStd self, tAtStdListener *listener, void *userData)
    {
    tStdListenerWrapper *wrapper;

    if (listener == NULL)
        return;

    /* Already add, ignore */
    if (EventListenerFind(self, userData, listener, NULL))
        return;

    wrapper = FreeListenerFind(self);
    if (wrapper == NULL)
        return;

    AtOsalMemCpy(&(wrapper->listener), listener, sizeof(tAtStdListener));
    wrapper->userData = userData;
    wrapper->valid = cAtTrue;
    self->numListeners = self->numListeners + 1;
    }

static void ListenerRemove(AtStd self, tAtStdListener *listener, void *userData)
    {
    tStdListenerWrapper *wrapper;

    if (listener == NULL)
        return;

    wrapper = EventListenerFind(self, userData, listener, NULL);
    if (wrapper == NULL)
        return;

    AtOsalMemInit(wrapper, 0, sizeof(tStdListenerWrapper));
    self->numListeners = self->numListeners - 1;
    }

static void WillPrintNotify(AtStd self, char *aString)
    {
    uint32 listener_i;

    if (self->numListeners == 0)
        return;

    if (aString == NULL)
        return;

    for (listener_i = 0; listener_i < cMaxNumListener; listener_i++)
        {
        tStdListenerWrapper *aListener = &(self->listener[listener_i]);

        if (!aListener->valid)
            continue;

        if (aListener->listener.WillPrint)
            aListener->listener.WillPrint(self, aListener->userData, aString);
        }
    }

static void DidPrintNotify(AtStd self, char *aString)
    {
    uint32 listener_i;

    if (self->numListeners == 0)
        return;

    if (aString == NULL)
        return;

    for (listener_i = 0; listener_i < cMaxNumListener; listener_i++)
        {
        tStdListenerWrapper *aListener = &(self->listener[listener_i]);

        if (!aListener->valid)
            continue;

        if (aListener->listener.DidPrint)
            aListener->listener.DidPrint(self, aListener->userData, aString);
        }
    }

static void MethodsInit(AtStd self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Printf);
        mMethodOverride(m_methods, PrintFuncSet);
        mMethodOverride(m_methods, AppContextSet);
        mMethodOverride(m_methods, Strcpy);
        mMethodOverride(m_methods, Strncpy);
        mMethodOverride(m_methods, Strlen);
        mMethodOverride(m_methods, Strcmp);
        mMethodOverride(m_methods, Strncmp);
        mMethodOverride(m_methods, Strstr);
        mMethodOverride(m_methods, Strtok);
        mMethodOverride(m_methods, Strtok_r);
        mMethodOverride(m_methods, Strcat);
        mMethodOverride(m_methods, Strncat);
        mMethodOverride(m_methods, Strchr);
        mMethodOverride(m_methods, Strrchr);
        mMethodOverride(m_methods, Sprintf);
        mMethodOverride(m_methods, Snprintf);
        mMethodOverride(m_methods, Atoi);
        mMethodOverride(m_methods, Atol);
        mMethodOverride(m_methods, Strtol);
        mMethodOverride(m_methods, Strtoul);
        mMethodOverride(m_methods, Strtoull);
        mMethodOverride(m_methods, FileCreate);
        mMethodOverride(m_methods, StandardOutput);
        mMethodOverride(m_methods, StandardError);
        mMethodOverride(m_methods, StandardInput);
        mMethodOverride(m_methods, Abs);
        mMethodOverride(m_methods, Rand);
        mMethodOverride(m_methods, RandSeed);
        mMethodOverride(m_methods, Sort);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtStd);
    }

AtStd AtStdObjectInit(AtStd self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    MethodsInit(self);
    self->printDisabled = cAtFalse;
    self->colorDisabled = cAtFalse;
    m_methodsInit = 1;

    return self;
    }

uint32 AtPrintf(const char *format, ...)
    {
    uint32 numChar;
    va_list args;
    AtStd std = AtStdSharedStdGet();

    if (std == NULL)
        return 0;

    if (!AtStdPrintIsEnabled(std))
        return 0;

    va_start(args, format);
    numChar = mMethodsGet(std)->Printf(std, format, args);
    va_end(args);

    return numChar;
    }

void AtPrintEnable(eBool enable)
    {
    AtStdPrintEnable(AtStdSharedStdGet(), enable);
    }

eBool AtPrintIsEnabled(void)
    {
    return AtStdPrintIsEnabled(AtStdSharedStdGet());
    }

/*
static uint32 AtPrintc(AtStd self, uint32 color, const char *format, va_list ap)
    {
    uint32 numChar;

    if (!AtStdPrintIsEnabled(self))
        return 0;

    if (AtStdColorIsEnabled(self))
        AtPrintf("\033[1;%dm", color);

    numChar = mMethodsGet(self)->Printf(self, format, ap);

    if (AtStdColorIsEnabled(self))
        AtPrintf("\033[0m");

    return numChar;
    }
*/

uint32 AtPrintc(uint32 color, const char *format, ...)
    {
    uint32 numChar;
    va_list args;
    AtStd std = AtStdSharedStdGet();

    if (std == NULL)
        return 0;

    if (!AtStdPrintIsEnabled(std))
        return 0;

    if (AtStdColorIsEnabled(std))
        AtPrintf("\033[1;%dm", color);

    va_start(args, format);
    numChar = mMethodsGet(std)->Printf(std, format, args);
    va_end(args);

    if (AtStdColorIsEnabled(std))
        AtPrintf("\033[0m");

    return numChar;
    }

uint32 AtSprintf(char *str, const char *format, ...)
    {
    va_list args;
    uint32 numChars;
    AtStd std = AtStdSharedStdGet();

    if (std == NULL)
        return 0;

    va_start(args, format);
    numChars = mMethodsGet(std)->Sprintf(std, str, format, args);
    va_end(args);

    return numChars;
    }

uint32 AtSnprintf(char *str, uint32 size, const char *format, ...)
    {
    va_list args;
    uint32 numChars;
    AtStd std = AtStdSharedStdGet();

    if (std == NULL)
        return 0;

    va_start(args, format);
    numChars = mMethodsGet(std)->Snprintf(std, str, size, format, args);
    va_end(args);

    return numChars;
    }

/*uint32 AtSnprintc(uint32 color, char *str, uint32 size, const char *format, ...)
    {
    va_list args;
    uint32 numChars;
    AtStd std = AtStdSharedStdGet();

    if (std == NULL)
        return 0;

     Need to print to stdout
    if (str == NULL)
        {

        }

    numChars = mMethodsGet(std)->Snprintf(std, str, size, format, args);
    va_end(args);

    return numChars;
    }*/

char *AtStdStrcpy(AtStd self, char *dest, const char *src)
    {
    if (self)
        return mMethodsGet(self)->Strcpy(self, dest, src);
    return NULL;
    }

char *AtStdStrncpy(AtStd self, char *dest, const char *src, uint32 numBytes)
    {
    if (self)
        return mMethodsGet(self)->Strncpy(self, dest, src, numBytes);
    return NULL;
    }

uint32 AtStdStrlen(AtStd self, const char *pString)
    {
    if (self)
        return mMethodsGet(self)->Strlen(self, pString);
    return 0;
    }

uint32 AtStdStrcmp(AtStd self, const char *pString1, const char *pString2)
    {
    if (self)
        return mMethodsGet(self)->Strcmp(self, pString1, pString2);
    return 0;
    }

uint32 AtStdStrncmp(AtStd self, const char *pString1, const char *pString2, uint32 numBytes)
    {
    if (self)
        return mMethodsGet(self)->Strncmp(self, pString1, pString2, numBytes);
    return 0;
    }

AtAttributePrintf(2, 0)
char *AtStdStrstr(AtStd self, const char *pString, const char *subString)
    {
    if (self)
        return mMethodsGet(self)->Strstr(self, pString, subString);
    return NULL;
    };

char *AtStdStrtok(AtStd self, char *pString, const char *delim)
    {
    if (self)
        return mMethodsGet(self)->Strtok(self, pString, delim);
    return NULL;
    }

char *AtStdStrtok_r(AtStd self, char *pString, const char *delim, char **saveptr)
    {
    if (self)
        return mMethodsGet(self)->Strtok_r(self, pString, delim, saveptr);
    return NULL;
    }

char *AtStdStrcat(AtStd self, char *dest, const char *src)
    {
    if (self)
        return mMethodsGet(self)->Strcat(self, dest, src);
    return NULL;
    }

char *AtStdStrncat(AtStd self, char *dest, const char *src, uint32 numBytes)
    {
    if (self)
        return mMethodsGet(self)->Strncat(self, dest, src, numBytes);
    return NULL;
    }

char *AtStdStrchr(AtStd self, const char *pString, uint16 aChar)
    {
    if (self)
        return mMethodsGet(self)->Strchr(self, pString, aChar);
    return NULL;
    }

char *AtStdStrrchr(AtStd self, const char *pString, uint16 aChar)
    {
    if (self)
        return mMethodsGet(self)->Strrchr(self, pString, aChar);
    return NULL;
    }

int32 AtStdAtoi(AtStd self, const char *pString)
    {
    if (self)
        return mMethodsGet(self)->Atoi(self, pString);
    return 0x0;
    }

uint32 AtStdAtol(AtStd self, const char *pString)
    {
    if (self)
        return mMethodsGet(self)->Atol(self, pString);
    return 0x0;
    }

uint32 AtStdStrtol(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    if (self)
        return mMethodsGet(self)->Strtol(self, pString, endptr, base);
    return 0x0;
    }

uint32 AtStdStrtoul(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    if (self)
        return mMethodsGet(self)->Strtoul(self, pString, endptr, base);
    return 0x0;
    }

uint64 AtStdStrtoull(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    if (self)
        return mMethodsGet(self)->Strtoull(self, pString, endptr, base);
    return 0x0;
    }

AtAttributePrintf(3, 0)
uint32 AtStdSprintf(AtStd self, char *str, const char *format, va_list ap)
    {
    if (self)
        return mMethodsGet(self)->Sprintf(self, str, format, ap);
    return 0;
    }

AtAttributePrintf(4, 0)
uint32 AtStdSnprintf(AtStd self, char *str, uint32 size, const char *format, va_list ap)
    {
    if (self)
        return mMethodsGet(self)->Snprintf(self, str, size, format, ap);
    return 0;
    }

void AtStdSharedStdSet(AtStd std)
    {
    m_sharedStd = std;
    }

AtStd AtStdSharedStdGet(void)
    {
    return m_sharedStd ? m_sharedStd : AtOsalStd();
    }

AtFile AtStdFileOpen(AtStd self, const char *filePath, uint8 mode)
    {
    if (self == NULL)
        return NULL;

    return mMethodsGet(self)->FileCreate(self, filePath, mode);
    }

AtFile AtStdStandardOutput(AtStd self)
    {
    if (self)
        return mMethodsGet(self)->StandardOutput(self);

    return NULL;
    }

AtFile AtStdStandardError(AtStd self)
    {
    if (self)
        return mMethodsGet(self)->StandardError(self);

    return NULL;
    }

AtFile AtStdStandardInput(AtStd self)
    {
    if (self)
        return mMethodsGet(self)->StandardInput(self);

    return NULL;
    }

int AtStdFileClose(AtFile file)
    {
    return AtFileClose(file);
    }

AtAttributePrintf(2, 0)
uint32 AtStdPrintf(AtStd self, const char *format, va_list ap)
    {
    if (self)
        return mMethodsGet(self)->Printf(self, format, ap);
    return 0;
    }

uint32 AtStdAbs(AtStd self, int value)
    {
    if (self)
        return mMethodsGet(self)->Abs(self, value);
    return 0;
    }

int AtStdEof(AtStd self)
    {
    AtUnused(self);
    return -1;
    }

int AtEof(void)
    {
    return AtStdEof(AtStdSharedStdGet());
    }

uint32 AtStdRand(AtStd self)
    {
    if (self)
        return mMethodsGet(self)->Rand(self);
    return 0;
    }

void AtStdRandSeed(AtStd self)
    {
    if (self)
        mMethodsGet(self)->RandSeed(self);
    }

void AtStdPrintFuncSet(AtStd self, int (*printFunc)(void *appContext, char *aString))
    {
    if (self)
        mMethodsGet(self)->PrintFuncSet(self, printFunc);
    }

void AtStdAppContextSet(AtStd self, void *appContext)
    {
    if (self)
        mMethodsGet(self)->AppContextSet(self, appContext);
    }

void AtStdSort(AtStd self,
               void *firstElement, uint32 numElements, uint32 memberSize,
               eAtStdCompareResult (*CompareFunc)(const void *, const void *))
    {
    if (self)
        mMethodsGet(self)->Sort(self, firstElement, numElements, memberSize, CompareFunc);
    }

void AtStdFlush(void)
    {
    AtFileFlush(AtStdStandardOutput(AtStdSharedStdGet()));
    }

void AtStdColorEnable(AtStd self, eBool enabled)
    {
    if (self)
        self->colorDisabled = enabled ? cAtFalse : cAtTrue;
    }

eBool AtStdColorIsEnabled(AtStd self)
    {
    if (self)
        return self->colorDisabled ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

void AtStdPrintEnable(AtStd self, eBool enabled)
    {
    if (self)
        self->printDisabled = enabled ? cAtFalse : cAtTrue;
    }

eBool AtStdPrintIsEnabled(AtStd self)
    {
    if (self)
        return self->printDisabled ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

eAtStdCompareResult AtStdUint32CompareFunction(const void *value1, const void *value2)
    {
    uint32 uint32Value1 = *((const uint32 *)value1);
    uint32 uint32Value2 = *((const uint32 *)value2);
    return Compare(uint32Value1, uint32Value2);
    }

eAtStdCompareResult AtStdUint16CompareFunction(const void *value1, const void *value2)
    {
    uint16 uint16Value1 = *((const uint16 *)value1);
    uint16 uint16Value2 = *((const uint16 *)value2);
    return Compare(uint16Value1, uint16Value2);
    }

eAtStdCompareResult AtStdUint8CompareFunction(const void *value1, const void *value2)
    {
    uint8 uint8Value1 = *((const uint8 *)value1);
    uint8 uint8Value2 = *((const uint8 *)value2);
    return Compare(uint8Value1, uint8Value2);
    }

char *AtStdNumbersToString(AtStd self, char *idString, uint32 idBufSize, uint32 *numbers, uint32 numNumbers)
    {
    if (self == NULL)
        return NULL;

    if ((numNumbers == 0) || (numbers == NULL))
        return NULL;

    return NumbersToString(self, idString, idBufSize, numbers, numNumbers);
    }

char *AtStdTwoLevelNumbersToString(AtStd self, char *idString, uint32 idBufSize,
                                   uint32 *level1Numbers, uint32 *level2Numbers, uint32 numNumbers, uint32 maxLevel2Number)
    {
    if (self == NULL)
        return NULL;

    if ((numNumbers == 0)|| (level1Numbers == NULL) ||  (level2Numbers == NULL))
        return NULL;

    /* Note there is a constraint that the maximum level-2 numbers must be same for all level-1 numbers. */
    return TwoLevelNumbersToString(self, idString, idBufSize, level1Numbers, level2Numbers, numNumbers, maxLevel2Number);
    }

void AtSpaceIndent(uint32 indentLevel)
    {
    uint32 numSpaces = indentLevel * 4; /* 4 spaces for one indent level */
    uint32 i;

    for (i = 0; i < numSpaces; i++)
        AtPrintf(" ");
    }

void AtStdListenerAdd(AtStd self, tAtStdListener *listener, void *userData)
    {
    if (self)
        ListenerAdd(self, listener, userData);
    }

void AtStdListenerRemove(AtStd self, tAtStdListener *listener, void *userData)
    {
    if (self)
        ListenerRemove(self, listener, userData);
    }

void AtStdWillPrintNotify(AtStd self, char *aString)
    {
    if (self)
        WillPrintNotify(self, aString);
    }

void AtStdDidPrintNotify(AtStd self, char *aString)
    {
    if (self)
        DidPrintNotify(self, aString);
    }

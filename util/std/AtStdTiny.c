/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtStdTiny.c
 *
 * Created Date: Sep 24, 2014
 *
 * Description : A STD tiny that only supports command processing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "os/AtStdOs.h"
#include "AtStdTinyInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtStdTiny *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtStdMethods m_AtStdOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldRedirectPrintf(AtStd self)
    {
    if (mThis(self)->printFunc == NULL)
        return cAtFalse;
    return cAtTrue;
    }

static char* SharedMessageBufferGet(AtStd self, uint32* bufferLength)
    {
    if (bufferLength)
        *bufferLength = cStdMsgBufferLength;

    AtOsalMemInit(mThis(self)->sharedMsgBuffer, 0, cStdMsgBufferLength);
    return mThis(self)->sharedMsgBuffer;
    }

static eBool ShouldBuildOutputBuffer(AtStd self)
    {
    if (ShouldRedirectPrintf(self))
        return cAtTrue;

    if (self->numListeners)
        return cAtTrue;

    return cAtFalse;
    }

AtAttributePrintf(2, 0)
static uint32 Printf(AtStd self, const char *format, va_list ap)
    {
    char* buffer = NULL;
    uint32 ret;

    /* Build message buffer */
    if (ShouldBuildOutputBuffer(self))
        {
        uint32 bufferSize = 0;
        va_list ap2;

        __builtin_va_copy(ap2, ap);
        buffer = SharedMessageBufferGet(self, &bufferSize);
        AtStdSnprintf(self, buffer, bufferSize - 1, format, ap2);
        buffer[bufferSize - 1] = '\0';
        va_end(ap2);
        }

    AtStdWillPrintNotify(self, buffer);

    if (ShouldRedirectPrintf(self))
        ret = (uint32)(mThis(self)->printFunc(mThis(self)->appContext, buffer));
    else
        ret = (uint32)vprintf(format, ap);

    AtStdDidPrintNotify(self, buffer);

    return ret;
    }

static void PrintFuncSet(AtStd self, int (*printFunc)(void *appContext, char *aString))
    {
    mThis(self)->printFunc = printFunc;
    }

static void AppContextSet(AtStd self, void *appContext)
    {
    mThis(self)->appContext = appContext;
    }

static char *Strcpy(AtStd self, char *dest, const char *src)
    {
    AtUnused(self);
    return strcpy(dest, src);
    }

static char *Strncpy(AtStd self, char *dest, const char *src, uint32 numBytes)
    {
    AtUnused(self);
    return strncpy(dest, src, numBytes);
    }

static uint32 Strlen(AtStd self, const char *pString)
    {
    AtUnused(self);
    return strlen(pString);
    }

static uint32 Strcmp(AtStd self, const char *pString1, const char *pString2)
    {
    AtUnused(self);
    return (uint32)strcmp(pString1, pString2);
    }

static uint32 Strncmp(AtStd self, const char *pString1, const char *pString2, uint32 numBytes)
    {
    AtUnused(self);
    return (uint32)strncmp(pString1, pString2, numBytes);
    }

static AtAttributePrintf(2, 0) char *Strstr(AtStd self, const char *pString, const char *subString)
    {
    AtUnused(self);
    return strstr(pString, subString);
    }

static char *Strtok(AtStd self, char *pString, const char *delim)
    {
    AtUnused(self);
    return strtok(pString, delim);
    }

static char *Strtok_r(AtStd self, char *pString, const char *delim, char **saveptr)
    {
    AtUnused(self);
    return strtok_r(pString, delim, saveptr);
    }

static char *Strcat(AtStd self, char *dest, const char *src)
    {
    AtUnused(self);
    return strcat(dest, src);
    }

static char *Strncat(AtStd self, char *dest, const char *src, uint32 numBytes)
    {
    AtUnused(self);
    return strncat(dest, src, numBytes);
    }

static char *Strchr(AtStd self, const char *pString, uint16 aChar)
    {
    AtUnused(self);
    return strchr(pString, aChar);
    }

static char *Strrchr(AtStd self, const char *pString, uint16 aChar)
    {
    AtUnused(self);
    return strrchr(pString, aChar);
    }

static AtAttributePrintf(3, 0) uint32 Sprintf(AtStd self, char *str, const char *format, va_list ap)
    {
    AtUnused(self);
    return (uint32)vsprintf(str, format, ap);
    }

static AtAttributePrintf(4, 0) uint32 Snprintf(AtStd self, char *str, uint32 size, const char *format, va_list ap)
    {
    AtUnused(self);
    return (uint32)vsnprintf(str, size, format, ap);
    }

static int32 Atoi(AtStd self, const char *pString)
    {
    AtUnused(self);
    return (int32)strtol(pString, (char**)NULL, 10);
    }

static uint32 Atol(AtStd self, const char *pString)
    {
    AtUnused(self);
    return (uint32)atol(pString);
    }

static uint32 Strtol(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    AtUnused(self);
    return (uint32)strtol(pString, endptr, (int)base);
    }

static uint32 Strtoul(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    AtUnused(self);
    return (uint32)strtoul(pString, endptr, (int)base);
    }

static uint64 Strtoull(AtStd self, const char *pString, char **endptr, uint32 base)
    {
    AtUnused(self);
    return (uint64)strtoul(pString, endptr, (int)base); /*FIXME: temporary use UL instead ULL because issue in vxworks 6.9*/
    }

static AtFile FileCreate(AtStd self, const char *filePath, uint8 mode)
    {
    AtUnused(self);
    AtUnused(filePath);
    AtUnused(mode);
    return NULL;
    }

static AtFile StandardOutput(AtStd self)
    {
    AtUnused(self);
    return NULL;
    }

static AtFile StandardError(AtStd self)
    {
    AtUnused(self);
    return NULL;
    }

static AtFile StandardInput(AtStd self)
    {
    AtUnused(self);
    return NULL;
    }

static void RandSeed(AtStd self)
    {
    tAtOsalCurTime curTime;
    AtUnused(self);
    AtOsalCurTimeGet(&curTime);
    srand((curTime.sec * 1000) + (curTime.usec / 1000));
    }

static uint32 Rand(AtStd self)
    {
    AtUnused(self);
    return (uint32)rand();
    }

static void OverrideAtStd(AtStd self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtStdOverride, mMethodsGet(self), sizeof(m_AtStdOverride));

        mMethodOverride(m_AtStdOverride, Printf);
        mMethodOverride(m_AtStdOverride, PrintFuncSet);
        mMethodOverride(m_AtStdOverride, AppContextSet);
        mMethodOverride(m_AtStdOverride, Strcpy);
        mMethodOverride(m_AtStdOverride, Strncpy);
        mMethodOverride(m_AtStdOverride, Strlen);
        mMethodOverride(m_AtStdOverride, Strcmp);
        mMethodOverride(m_AtStdOverride, Strncmp);
        mMethodOverride(m_AtStdOverride, Strstr);
        mMethodOverride(m_AtStdOverride, Strtok);
        mMethodOverride(m_AtStdOverride, Strtok_r);
        mMethodOverride(m_AtStdOverride, Strcat);
        mMethodOverride(m_AtStdOverride, Strncat);
        mMethodOverride(m_AtStdOverride, Strchr);
        mMethodOverride(m_AtStdOverride, Strrchr);
        mMethodOverride(m_AtStdOverride, Sprintf);
        mMethodOverride(m_AtStdOverride, Snprintf);
        mMethodOverride(m_AtStdOverride, Atoi);
        mMethodOverride(m_AtStdOverride, Atol);
        mMethodOverride(m_AtStdOverride, Strtol);
        mMethodOverride(m_AtStdOverride, Strtoul);
        mMethodOverride(m_AtStdOverride, Strtoull);
        mMethodOverride(m_AtStdOverride, FileCreate);
        mMethodOverride(m_AtStdOverride, StandardOutput);
        mMethodOverride(m_AtStdOverride, StandardError);
        mMethodOverride(m_AtStdOverride, StandardInput);
        mMethodOverride(m_AtStdOverride, Rand);
        mMethodOverride(m_AtStdOverride, RandSeed);
        }

    mMethodsSet(self, &m_AtStdOverride);
    }

static void Override(AtStd self)
    {
    OverrideAtStd(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtStdTiny);
    }

AtStd AtStdTinyObjectInit(AtStd self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtStdObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtStd AtStdTinySharedStd(void)
    {
    static tAtStdTiny m_std;
    static AtStd m_sharedStd = NULL;

    if (m_sharedStd == NULL)
        m_sharedStd = AtStdTinyObjectInit((AtStd)&m_std);

    return m_sharedStd;
    }

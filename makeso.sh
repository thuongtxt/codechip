libDir=temp/libarrivesdk
libName=libarrivesdk.so
mkdir -p $libDir

cd $libDir

# Stupid steps first (need to refactor by function)
`rm -f *.o`
`rm -f *.a`
`rm -f *.so`

`powerpc-8540-linux-gnu-ar -x ../../Debug/driver/libdriver.a`
`powerpc-8540-linux-gnu-ld -r -o libdriver.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/platform/libplatform.a`
`powerpc-8540-linux-gnu-ld -r -o libplatform.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libs/klish/.libs/libklishbin.a`
`powerpc-8540-linux-gnu-ld -r -o libklishbin.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libs/klish/.libs/liblub.a`
`powerpc-8540-linux-gnu-ld -r -o liblub.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libs/klish/.libs/libtinyrl.a`
`powerpc-8540-linux-gnu-ld -r -o libtinyrl.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libs/klish/.libs/libclish.a`
`powerpc-8540-linux-gnu-ld -r -o libclish.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libs/tcl-8.5/libtcl8.5.a`
`powerpc-8540-linux-gnu-ld -r -o libtcl8.5.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/liblistener.a`
`powerpc-8540-linux-gnu-ld -r -o liblistener.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libsdkcli.a`
`powerpc-8540-linux-gnu-ld -r -o libsdkcli.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libshowtable.a`
`powerpc-8540-linux-gnu-ld -r -o libshowtable.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libclicustomers.a`
`powerpc-8540-linux-gnu-ld -r -o libclicustomers.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/components/libcomponents.a`
`powerpc-8540-linux-gnu-ld -r -o libcomponents.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/apps/libapps.a`
`powerpc-8540-linux-gnu-ld -r -o libapps.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/debugutil/libdebugutil.a`
`powerpc-8540-linux-gnu-ld -r -o libdebugutil.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/util/libutil.a`
`powerpc-8540-linux-gnu-ld -r -o libutil.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/customers/libcustomers.a`
`powerpc-8540-linux-gnu-ld -r -o libcustomers.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ar -x ../../Debug/cli/libs/klish/.libs/libkonf.a`
`powerpc-8540-linux-gnu-ld -r -o libkonf.o_temp *.o`
`rm -f *.o`

`powerpc-8540-linux-gnu-ld -r -o arrivesdk.o *.*_temp`
`powerpc-8540-linux-gnu-gcc -shared -o $libName arrivesdk.o`
`rm -f *.o`
`powerpc-8540-linux-gnu-strip -g $libName`
`mv -f $libName ../../`
`cd ../../`
`rm -fR $libDir`

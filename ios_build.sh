# /bin/sh

rm -fr .build/atsdkall.so

export PATH=$PATH:/opt/tools/fsl-networking/QorIQ-SDK-V1.5/sysroots/x86_64-fsl_networking_sdk-linux/usr/bin/ppce5500-fsl_networking-linux
make -C components/ clean
make -C components/ COMPILE_PREFIX=powerpc-fsl_networking-linux- ADDITIONAL_FLAGS="--sysroot=/opt/tools/fsl-networking/QorIQ-SDK-V1.5/sysroots/ppce5500-fsl_networking-linux  -Wno-unused-but-set-variable -Wno-enum-compare" GEN_CMD_SRC_FILE=no -j 10 all

mv cli/cmd/customers/cisco/CliAtCiscoSlot.c cli/cmd/customers/cisco/CliAtCiscoSlot.c_
mv cli/cmd/customers/cisco/CliAtCiscoSlot.conf cli/cmd/customers/cisco/CliAtCiscoSlot.conf_
make clean
make atclitable -j 10
make -C apps/sample_app/ COMPILE_PREFIX=powerpc-fsl_networking-linux- ADDITIONAL_FLAGS="--sysroot=/opt/tools/fsl-networking/QorIQ-SDK-V1.5/sysroots/ppce5500-fsl_networking-linux -DLINUX" GEN_CMD_SRC_FILE=no AT_OS=linux -j 10 clean all
mv cli/cmd/customers/cisco/CliAtCiscoSlot.c_ cli/cmd/customers/cisco/CliAtCiscoSlot.c
mv cli/cmd/customers/cisco/CliAtCiscoSlot.conf_ cli/cmd/customers/cisco/CliAtCiscoSlot.conf

if [ -f ".build/atsdkall.so" ]
then
    pushd .
    cd .build
    rm -fr libarrive.so libarrive.so.tar.gz
    mv atsdkall.so libarrive.so
    tar -czvf libarrive.so.tar.gz libarrive.so
    popd
else
    echo "FAIL! Cannot compile IOS SDK library"
    exit 1
fi

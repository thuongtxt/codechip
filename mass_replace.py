#!/usr/bin/python

import os
import re
import sys
import glob
import getopt

# list of extensions to replace
replace_extensions = ["reg.h"]

# Check if it is register file
def try_to_replace(fname):
    for extention in replace_extensions[:]:
        if fname.lower().endswith(extention):
            return True

# Perform replace operation.
def file_replace(fname, pattern, backupOption):  
    ifile = open(fname,'r');
    content = ifile.read();
    ifile.close()
    
    replaced = False
    #Replace comment by ""
    for match in re.findall(pattern, content):
        content = content.replace(match, '');
        replaced = True
    
    if replaced == False: return
    
    # Write to temporary file
    fileWithoutCommentName = fname + ".tmp"
    fileWithoutComment = open(fileWithoutCommentName, "w")
    fileWithoutComment.write(content);
    fileWithoutComment.close()
    
    # Rename original file to .bak
    backupFile = fname + ".bak"
    if os.path.exists(backupFile):
        os.remove(backupFile)
        
    os.rename(fname, backupFile)
    
    # Output file has same name with original file 
    out_fname = fname
    out = open(out_fname, "w")
    
    fileWithoutComment = open(fileWithoutCommentName, "r")
    for line in fileWithoutComment:
        cleanedLine = line.strip()
        if cleanedLine: # is not empty
            out.writelines(cleanedLine + "\n");
            
    out.close()
    
    #Remove temporary file
    fileWithoutComment.close()
    os.remove(fileWithoutCommentName)
    
    if backupOption == False :
        os.remove(backupFile)
    
# Recursive scan directory and process register file
def mass_replace(path, backupOption):
    pattern = re.compile("/\*.*?\*/", re.DOTALL)
    
    for currentFile in glob.glob( os.path.join(path, '*') ):
        if os.path.isdir(currentFile):
            mass_replace(currentFile, backupOption)
        
        if try_to_replace(currentFile):
            print ("processing file: " + currentFile)
            file_replace(currentFile, pattern, backupOption)           

def printUsage():
    print ('usage: mass_replace.py -d <directory> -b')
    print ('       -d, --directory         root directory')
    print ('       -b, --backup=[yes|no]   backup register files')
      
def main(argv):
   inputDirectory = '.' #Default is current directory
   backupOption = False
   try:
      opts, args = getopt.getopt(argv,"hd:b",["help", "directory=","backup="])
   except getopt.GetoptError:
      printUsage()
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         printUsage()
         sys.exit()
      elif opt in ("-d", "--directory"):
         inputDirectory = arg
      elif opt in ("-b"):
         backupOption = True
      elif opt in ("--backup"):
          if arg in ("yes"):
              backupOption = True
          elif arg in ("no"):
              backupOption = False
          else:
             printUsage()
             sys.exit()
   mass_replace(inputDirectory, backupOption)
   
if __name__ == "__main__":
   main(sys.argv[1:])


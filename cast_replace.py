#!/usr/bin/python
import os
import re
import sys
import glob
import getopt

class bcolors:
    WARNING = '\033[93m'
    NORMAL = '\033[0m'
    
c_CastTypePattern = "\(\s*At\w+\s*\)|\(\s*Tha\w+\s*\)"   
m_LineNumberList = []
m_ReverseList=[]
m_Content_changed = 0
m_Debug = False
filterString = ""

# list of extensions to replace
replace_extensions = [".c"]

def function_arg(string_begin_with_function_arg):
    #First char is open parenthesis
    enterParenthesis = 1
    for i, c in enumerate(string_begin_with_function_arg):
        if i == 0: 
            continue
        if c == '(':
            enterParenthesis = enterParenthesis + 1
        if c == ')':
            enterParenthesis = enterParenthesis - 1
            if enterParenthesis == 0:
                return string_begin_with_function_arg[:i+1]

def cast_function_call(functionName, string_begin_with_function_call):
    funcArg = string_begin_with_function_call[len(functionName):]
    return functionName + function_arg(funcArg)
         
def find_line_number(fileName):
    ifile = open(fileName,'r');
    lineNumber = 1
    
    for line in ifile:
        match = re.findall(c_CastTypePattern, line)
        for i in match :
            m_LineNumberList.append(lineNumber)
        lineNumber = lineNumber + 1
    
    ifile.close()

def content_replace(kind, content, replace, by, lineNumber):
    global m_Content_changed
    content = content.replace(replace, by, 1);
    if m_Debug == True:
        print (kind + " - %4d: " % lineNumber + replace + " -> " + by)
    else:
        print (" - %4d: " % lineNumber + replace + " -> " + by)
    m_Content_changed = 1;
    return content
        
def variableSearch(subContent):
    # Search until reach +,-,*,/,;, end of line
    operators = ['=','+','*','/','&','|','&&','||','==','!=','<',';']
    enterParenthese = 0;
    for i, c in enumerate(subContent):
        if c in operators:
            return subContent[:i]
    
        if c == '-':
            if subContent[i+1] != '>':
                return subContent[:i]
            
        if c == '>':
            if subContent[i-1] != '-':
                return subContent[:i]
        
        if c == '(': enterParenthese += 1
        
        if c == ')': 
            if enterParenthese == 0:
                return subContent[:i]
            else:
                enterParenthese -= 1
                
        if c == ',' and enterParenthese == 0:
            return subContent[:i]

def function_cast_replace(type, content, subContent):
    functionCallPattern = "\s*[A-Z]\w+\s*\("
    
    functionCall = re.match(functionCallPattern, subContent)
    if functionCall == None:
        return None

    cast_func_call = cast_function_call(functionCall.group(0)[:-1], subContent)
    replace = type + cast_func_call
    by      = "m" + type[1:-1] + "(" + cast_func_call.strip() + ")"
    content = content_replace("F ", content, replace, by, m_LineNumberList.pop(0))
        
    return content

def variable_cast_replace(type, content, subContent):
    variablePattern = "\s*\w+"
  
    variableBegin = re.match(variablePattern, subContent)
    if variableBegin == None:
        return None

    variable = variableSearch(subContent)
    replace = type + variable
    by      = "m" + type[1:-1] + "(" + variable.strip() + ")"
    content = content_replace("V ", content, replace, by, m_LineNumberList.pop(0))
        
    return content
    
def simple_cast_replace(type, content, subContent):
    if re.match("\s*\(", subContent) == None:
        return None
    
    variable = variableSearch(subContent)
    replace = type + variable
    by = "m" + type[1:-1] + "(" + variable.strip() + ")"
    content = content_replace("S ", content, replace, by, m_LineNumberList.pop(0))
    
    return content
  
def not_need_replace(type, content, subContent):
    global m_ReverseList
    catch = re.match("\s*[+*/&|=<>;]", subContent)
    if catch == None:
        return None
    
    lineNumber = m_LineNumberList.pop(0)
    if m_Debug == True:
        print ("I - %4d: " % lineNumber)
    
    # Temporarily replace, after finish, replaced item will be restored    
    replace = type + catch.group(0)
    by = "m" + type[1:-1] + catch.group(0)
    
    content = content.replace(replace, by, 1);
    m_ReverseList.append((replace, by))
    return content
    
def ContentReverse(content):
    global m_ReverseList
    
    while len(m_ReverseList) > 0:
        reverseItem = m_ReverseList.pop(0)
        content = content.replace(reverseItem[1], reverseItem[0])
        
    return content
    
def make_new_file(newFileName, content):
    replacedFile = open(newFileName, "w")
    replacedFile.write(content)
    replacedFile.close()
    
def cast_replace(fileName):
    global m_Content_changed
    ifile = open(fileName,'r');
    content = ifile.read();
    
    failLines = []
    subContent = content
    m_Content_changed = 0
    
    if m_Debug == True:
        print ("Lines: " + str(m_LineNumberList))
        
    for type in re.findall(c_CastTypePattern, subContent):
        subContent = subContent[subContent.find(type) + len(type):]
        
        newContent = simple_cast_replace(type, content, subContent)
        if newContent:
            content = newContent
            continue
        
        newContent =  function_cast_replace(type, content, subContent)
        if newContent:
            content = newContent
            continue
        
        newContent =  variable_cast_replace(type, content, subContent)
        if newContent:
            content = newContent
            continue
        
        newContent = not_need_replace(type, content, subContent)
        if newContent:
            content = newContent
            continue
            
        failLines.append(m_LineNumberList.pop(0))
    
    # Reverse
    content = ContentReverse(content)
       
    if len(failLines) > 0:
        print (bcolors.WARNING + "Please solve these lines: " + str(failLines) + bcolors.NORMAL)
        
    if m_Content_changed == 0:
        return
    
    if m_Debug == True:
        # Write to temporary file
        make_new_file(fileName + ".tmp", content)
        ifile.close()
        return
    
    make_new_file(fileName, content)
    
def file_replace(fileName):
    find_line_number(fileName)
    cast_replace(fileName)
    
def filter(fileOrDir):
    if re.search(filterString, fileOrDir):
        return True
    return False

# Check if it is register file
def try_to_replace(fname):
    for extention in replace_extensions[:]:
        if fname.lower().endswith(extention):
            return True
        
def dir_replace(path):
    for currentFile in glob.glob( os.path.join(path, '*') ):
        if os.path.isdir(currentFile):
            dir_replace(currentFile)
            
        if try_to_replace(currentFile):
            if filter(currentFile):
                print ("\r\nProcessing file: " + currentFile)
                file_replace(currentFile)  

def printUsage():
    print ('usage: cast_replace.py -d <directory>')
    print ('       -d <directory>: replace all file .c in folder')
    print ('       -f <filter string> example \'abcd\'')
     
def main(argv):
    global m_Debug
    global filterString
    
    inputDirectory = None
    m_Debug = False
    
    try:
       opts, args = getopt.getopt(argv,"hd:f:v",["help"])
    except getopt.GetoptError:
       printUsage()
       sys.exit(2)
       
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit()
        elif opt == '-v':
            m_Debug = True
        elif opt == '-d':
            inputDirectory = arg
        elif opt == '-f':
            filterString = arg
    
    if '-v' in argv: 
       m_Debug = True
       
    if inputDirectory:
        dir_replace(inputDirectory)
    else:
        file_replace(argv[0])   
   
if __name__ == "__main__":
   main(sys.argv[1:])
   
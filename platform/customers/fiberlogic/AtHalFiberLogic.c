/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFiberLogic.c
 *
 * Created Date: Apr 14, 2014
 *
 * Description : Concrete HAL class for FiberLogic project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalFiberLogic.h"
#include "AtObject.h"
#include "../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalFiberLogic
    {
    tAtHalIndirectDefault super;
    }tAtHalFiberLogic;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalIndirectDefaultMethods m_AtHalIndirectDefaultOverride;

/* Save super implementation */
static const tAtHalIndirectDefaultMethods *m_AtHalIndirectDefaultMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalFiberLogic);
    }

static void Setup(AtHal self)
    {
    AtHalIndirectDefaultAddressMaskSet(self, cBit24_0);
    AtHalIndirectDefaultIndirectControl1AddressSet(self, 0x702);
    AtHalIndirectDefaultIndirectControl2AddressSet(self, 0x703);
    AtHalIndirectDefaultIndirectData1AddressSet(self, 0x704);
    AtHalIndirectDefaultIndirectData2AddressSet(self, 0x705);
    }

static uint8 IsDirectRegister(AtHal self, uint32 address)
    {
    /* Inherit AtHalIndirectDefault methods */
    if (m_AtHalIndirectDefaultMethods->IsDirectRegister(self, address))
        return cAtTrue;

    /* Add more new registers */
    if ((address >= 0x700) && (address <= 0x701))
        return cAtTrue;

    return cAtFalse;
    }

static void AtHalIndirectDefaultOverride(AtHal self)
    {
    AtHalIndirectDefault defaultHal = (AtHalIndirectDefault)self;
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalIndirectDefaultMethods = defaultHal->methods;
        AtOsalMemCpy(&m_AtHalIndirectDefaultOverride, defaultHal->methods, sizeof(m_AtHalIndirectDefaultOverride));

        mMethodOverride(m_AtHalIndirectDefaultOverride, IsDirectRegister);
        }

    mMethodsSet(defaultHal, &m_AtHalIndirectDefaultOverride);
    }

static void Override(AtHal self)
    {
    AtHalIndirectDefaultOverride(self);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Default setup */
    Setup(self);

    m_methodsInit = 1;
    return self;
    }

AtHal AtHalFiberLogicNew(uint32 baseAddress)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

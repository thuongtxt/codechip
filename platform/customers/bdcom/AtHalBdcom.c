/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtHalBdcom.c
 *
 * Created Date: Dec 10, 2013
 *
 * Description : Concrete HAL to work with BDCOM platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtHalBdcom.h"
#include "../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Control registers */
#define cControl1 0xF0
#define cControl2 0xF1
#define cControl3 0xF2
#define cControl4 0xF3

/* Data registers */
#define cData1 0xF4
#define cData2 0xF5
#define cData3 0xF6
#define cData4 0xF7

#define cInvalidValue 0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalBdcom *)((void*)self))

#define mIndirectDefault(self_) ((AtHalIndirectDefault)self_)
#define mDirectWrite(self_, address_, value_) (self_)->methods->DirectWrite(self_, address_, value_)
#define mDirectRead(self_, address_) (self_)->methods->DirectRead(self_, address_)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalBdcom
    {
    tAtHalIndirectDefault super;

    /* Private data */
    uint8 slotId;
    uint8 pageId;

    /* Save/restore HAL from interrupt context */
    uint8 control1;
    uint8 control2;
    uint8 control3;
    uint8 control4;
    uint8 data1;
    uint8 data2;
    uint8 data3;
    uint8 data4;

    AtHal mdio;

    /* FPGA direct register access functions */
    FpgaRead readFunc;
    FpgaWrite writeFunc;
    }tAtHalBdcom;

/* Request types */
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods                       m_AtHalOverride;
static tAtHalDefaultMethods                m_AtHalDefaultOverride;
static tAtHalIndirectDefaultMethods        m_AtHalIndirectDefaultOverride;

/* Save super implementation */
static const tAtHalMethods                *m_AtHalMethods                = NULL;
static const tAtHalIndirectDefaultMethods *m_AtHalIndirectDefaultMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 MakeRegOnPage(uint8 address, uint8 page)
    {
    uint16 reg = 0;

    reg = (uint16)(reg | address);
    reg = (uint16)(reg | (page & cBit2_0) << 10);

    return reg;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mThis(self)->writeFunc(mThis(self)->slotId, MakeRegOnPage((uint8)address, mThis(self)->pageId), (uint8)value);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    return mThis(self)->readFunc(mThis(self)->slotId, MakeRegOnPage((uint8)address, mThis(self)->pageId));
    }

static eBool HwDone(AtHal self, uint32 address, eRequestType requestType)
    {
    uint32 elapsedTime = 0;

    while (elapsedTime < mIndirectDefault(self)->retryTimes)
        {
        /* Done */
        if ((mDirectRead(self, cControl4) & ((uint8)cBit7)) == 0)
            {
            if (mIndirectDefault(self)->maxRetryTimes < elapsedTime)
                mIndirectDefault(self)->maxRetryTimes = elapsedTime;

            return cAtTrue;
            }

        /* Retry */
        elapsedTime = elapsedTime + 1;
        }

    /* Timeout, log debug information */
    AtPrintc(cSevCritical,
             "(%s, %d) ERROR: %s 0x%08x timeout(retry %u times)\n",
             __FILE__, __LINE__,
             (requestType == cRequestRead) ? "Read" : "Write", address,
             elapsedTime);
    if (requestType == cRequestRead)
        {
        mIndirectDefault(self)->readTimeoutCounter = mIndirectDefault(self)->readTimeoutCounter + 1;
        AtHalReadFailNotify(self);
        }
    else
        {
        mIndirectDefault(self)->writeTimeoutCounter = mIndirectDefault(self)->writeTimeoutCounter + 1;
        AtHalWriteFailNotify(self);
        }

    return cAtFalse;
    }

static uint8 Request(AtHal self, uint32 address, eRequestType requestType)
    {
    uint8 request = 0;

    /* Write address */
    mDirectWrite(self, cControl1,  address & cBit7_0);
    mDirectWrite(self, cControl2, (address & cBit15_8)  >> 8);
    mDirectWrite(self, cControl3, (address & cBit23_16) >> 16);

    /* Write request type */
    if (requestType == cRequestRead)
        request |= (uint8)cBit6;

    /* Start request */
    request |= (uint8)cBit7;
    mDirectWrite(self, cControl4, request);

    return HwDone(self, address, requestType);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    /* Make a read request on this register */
    if (Request(self, address, cRequestRead))
        {
        uint32 data = 0;

        data |=  mDirectRead(self, cData1);
        data |= (mDirectRead(self, cData2) << 8);
        data |= (mDirectRead(self, cData3) << 16);
        data |= (mDirectRead(self, cData4) << 24);

        return data;
        }

    /* Timeout */
    return cInvalidValue;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    /* Write data */
    mDirectWrite(self, cData1, (uint8) (value & cBit7_0));
    mDirectWrite(self, cData2, (uint8)((value & cBit15_8)  >> 8));
    mDirectWrite(self, cData3, (uint8)((value & cBit23_16) >> 16));
    mDirectWrite(self, cData4, (uint8)((value & cBit31_24) >> 24));

    /* Make request */
    Request(self, address, cRequestWrite);
    }

static void ShowControlRegisters(AtHal self)
    {
	AtUnused(self);
    }

static tAtHalTestRegister *RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[7];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], cControl1);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], cControl2);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], cControl3);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[3], cData1);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[4], cData2);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[5], cData3);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[6], cData4);

    return regsToTest;
    }

static uint32 TestPattern(AtHal hal)
    {
	AtUnused(hal);
    return 0xAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
	AtUnused(self);
    return 0x55;
    }

static uint8 IndirectIsInProgress(AtHal self)
    {
    uint32 requestStatus = mDirectRead(self, cControl4);
    return (requestStatus & cBit7) ? 1 : 0;
    }

static void IndirectCtrlRegisterSave(AtHal self)
    {
    mThis(self)->control1 = (uint8)mDirectRead(self, cControl1);
    mThis(self)->control2 = (uint8)mDirectRead(self, cControl2);
    mThis(self)->control3 = (uint8)mDirectRead(self, cControl3);
    mThis(self)->control4 = (uint8)mDirectRead(self, cControl4);
    mThis(self)->data1    = (uint8)mDirectRead(self, cData1);
    mThis(self)->data2    = (uint8)mDirectRead(self, cData2);
    mThis(self)->data3    = (uint8)mDirectRead(self, cData3);
    mThis(self)->data4    = (uint8)mDirectRead(self, cData4);
    }

static void StateRestore(AtHal self)
    {
    mDirectWrite(self, cData1,    mThis(self)->data1);
    mDirectWrite(self, cData2,    mThis(self)->data2);
    mDirectWrite(self, cData3,    mThis(self)->data3);
    mDirectWrite(self, cData4,    mThis(self)->data4);

    mDirectWrite(self, cControl1, mThis(self)->control1);
    mDirectWrite(self, cControl2, mThis(self)->control2);
    mDirectWrite(self, cControl3, mThis(self)->control3);
    mDirectWrite(self, cControl4, mThis(self)->control4);
    }

static void Debug(AtHal self)
    {
    m_AtHalMethods->Debug(self);

    /* Show additional information */
    AtPrintc(cSevNormal, "Slot: %d\r\n", mThis(self)->slotId);
    AtPrintc(cSevNormal, "Page: %d\r\n", mThis(self)->pageId);
    }

static uint32 DataBusMask(AtHal self)
    {
	AtUnused(self);
    return cBit7_0;
    }

static void Delete(AtHal self)
    {
    if (mThis(self)->mdio)
        AtHalDelete(mThis(self)->mdio);
    m_AtHalMethods->Delete(self);
    }

static char* BaseAddressDescriptionToString(AtHal self, char *buffer)
    {
    AtSprintf(buffer, "Slot %d, page 0x%x", mThis(self)->slotId, mThis(self)->pageId);
    return buffer;
    }

static void OverrideAtHalIndirectDefault(AtHal self)
    {
    AtHalIndirectDefault hal = (AtHalIndirectDefault)self;

    if (!m_methodsInit)
        {
        m_AtHalIndirectDefaultMethods = hal->methods;
        AtOsalMemCpy(&m_AtHalIndirectDefaultOverride, hal->methods, sizeof(m_AtHalIndirectDefaultOverride));


        m_AtHalIndirectDefaultOverride.IndirectIsInProgress     = IndirectIsInProgress;
        m_AtHalIndirectDefaultOverride.IndirectCtrlRegisterSave = IndirectCtrlRegisterSave;
        m_AtHalIndirectDefaultOverride.ShowControlRegisters     = ShowControlRegisters;
        }

    hal->methods = &m_AtHalIndirectDefaultOverride;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.BaseAddressDescriptionToString = BaseAddressDescriptionToString;
        m_AtHalDefaultOverride.RegsToTest      = RegsToTest;
        m_AtHalDefaultOverride.TestPattern     = TestPattern;
        m_AtHalDefaultOverride.TestAntiPattern = TestAntiPattern;
        m_AtHalDefaultOverride.DataBusMask     = DataBusMask;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read         = Read;
        m_AtHalOverride.Write        = Write;
        m_AtHalOverride.DirectRead   = DirectRead;
        m_AtHalOverride.DirectWrite  = DirectWrite;
        m_AtHalOverride.Debug        = Debug;
        m_AtHalOverride.Delete       = Delete;
        m_AtHalOverride.StateRestore = StateRestore;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    OverrideAtHalIndirectDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalBdcom);
    }

static AtHal ObjectInit(AtHal self, uint8 slotId, uint8 pageId, FpgaRead readFunc, FpgaWrite writeFunc)
    {
    /* Super constructor */
    AtOsalMemInit(self, 0, ObjectSize());
    if (AtHalIndirectDefaultObjectInit(self, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->slotId = slotId;
    mThis(self)->pageId = pageId;

    /* Read/write interfaces */
    mThis(self)->readFunc  = readFunc;
    mThis(self)->writeFunc = writeFunc;

    return self;
    }

AtHal AtHalBdcomNew(uint8 slotId, uint8 pageId, FpgaRead readFunc, FpgaWrite writeFunc)
    {
    AtHal newHal;

    /* Need hardware dependence interfaces */
    if ((readFunc == NULL) || (writeFunc == NULL))
        return NULL;

    newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
    	return NULL;

    return ObjectInit(newHal, slotId, pageId, readFunc, writeFunc);
    }

AtHal AtHalBdcomMdio(AtHal self)
    {
    if (self == NULL)
        return NULL;

    if (mThis(self)->mdio == NULL)
        mThis(self)->mdio = AtHalBdcomMdioNew(self);

    return mThis(self)->mdio;
    }

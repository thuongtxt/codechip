/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalBdcomMdio.c
 *
 * Created Date: Dec 18, 2013
 *
 * Description : MDIO to control GE physical
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "atclib.h"
#include "AtHalBdcomMdio.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
/* Control register and fields */
#define cControlReg 0xf00043
#define cControlClearAckMask        cBit8
#define cControlReadWriteStartMask  cBit7
#define cControlReadWriteStartShift 7
#define cControlOperationMask       cBit6_5
#define cControlOperationShift      5
#define cControlOperationWrite      1
#define cControlOperationRead       2
#define cControlRegAddressMask      cBit4_0
#define cControlRegAddressShift     0

/* Status register and fields */
#define cStatusReg  0xf00064
#define cStatusReadWriteDoneMask    cBit16
#define cStatusReadDataMask         cBit15_0

/* Register to write data */
#define cWritenDataReg              0xf00044
#define cInvalidValue               0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalBdcomMdio *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalBdcomMdio
    {
    tAtHal super;

    /* Private data */
    AtHal hal;
    uint8 retryInMs;
    }tAtHalBdcomMdio;

/* Request types */
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalBdcomMdio);
    }

static void Delete(AtHal self)
    {
    AtOsalMemFree(self);
    }

static uint32 ReadControl(AtHal self, uint32 controlRegister)
    {
    return AtHalRead(mThis(self)->hal, controlRegister);
    }

static void WriteControl(AtHal self, uint32 controlRegister, uint32 value)
    {
    AtHalWrite(mThis(self)->hal, controlRegister, value);
    }

static void ClearAck(AtHal self)
    {
    WriteControl(self, cControlReg, cControlClearAckMask);
    }

static eBool HwDone(AtHal self, uint32 address, eRequestType requestType)
    {
    uint32 elapsedTime = 0;
    tAtOsalCurTime startTime, currentTime;

    AtOsalCurTimeGet(&startTime);
    AtOsalCurTimeGet(&currentTime);
    while (elapsedTime < mThis(self)->retryInMs)
        {
        /* Done */
        if (ReadControl(self, cStatusReg) & cStatusReadWriteDoneMask)
            return cAtTrue;

        /* Retry */
        AtOsalCurTimeGet(&currentTime);
        elapsedTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    /* Timeout, log debug information */
    AtPrintc(cSevCritical,
             "(%s, %d) ERROR: %s 0x%08x timeout(startTime: %u.%u, stopTime: %u.%u)\n",
             __FILE__, __LINE__,
             (requestType == cRequestRead) ? "Read" : "Write", address,
             startTime.sec, startTime.usec,
             currentTime.sec, currentTime.usec);

    return cAtFalse;
    }

static void MakeRequest(AtHal self, uint32 address, eRequestType requestType)
    {
    uint32 regVal = 0;

    address = address & cBit4_0;
    mRegFieldSet(regVal, cControlOperation, (requestType == cRequestRead) ? cControlOperationRead : cControlOperationWrite);
    mRegFieldSet(regVal, cControlRegAddress, (address & cBit4_0));
    mRegFieldSet(regVal, cControlReadWriteStart, 1);
    WriteControl(self, cControlReg, regVal);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    ClearAck(self);

    address = address & cBit4_0;
    WriteControl(self, cWritenDataReg, value & cBit15_0);
    MakeRequest(self, address, cRequestWrite);

    (void)HwDone(self, address, cRequestWrite);
    ClearAck(self);
    WriteControl(self, cControlReg, 0);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 regVal;

    ClearAck(self);

    MakeRequest(self, address, cRequestRead);
    if (HwDone(self, address, cRequestRead))
        regVal = ReadControl(self, cStatusReg) & cStatusReadDataMask;
    else
        regVal = cInvalidValue;

    ClearAck(self);
    WriteControl(self, cControlReg, 0);

    return regVal;
    }

static void Debug(AtHal self)
    {
    AtHalDebug(mThis(self)->hal);
    }

static void Lock(AtHal self)
    {
	AtUnused(self);
    }

static void UnLock(AtHal self)
    {
	AtUnused(self);
    }

static const char* ToString(AtHal self)
    {
    return AtHalToString(mThis(self)->hal);
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_AtHalOverride, 0, sizeof(m_AtHalOverride));
        m_AtHalOverride.Delete   = Delete;
        m_AtHalOverride.Debug    = Debug;
        m_AtHalOverride.Write    = Write;
        m_AtHalOverride.Read     = Read;
        m_AtHalOverride.Lock     = Lock;
        m_AtHalOverride.UnLock   = UnLock;
        m_AtHalOverride.ToString = ToString;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static AtHal ObjectInit(AtHal self, AtHal hal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->hal       = hal;
    mThis(self)->retryInMs = 10;

    return self;
    }

AtHal AtHalBdcomMdioNew(AtHal hal)
    {
	AtHal halMdio;
    if (hal == NULL)
        return NULL;

    halMdio = AtOsalMemAlloc(ObjectSize());
    if (halMdio == NULL)
    	return NULL;

    return ObjectInit(halMdio, hal);
    }

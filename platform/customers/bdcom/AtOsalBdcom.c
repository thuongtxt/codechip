/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalBdcom.c
 *
 * Created Date: Aug 1, 2012
 *
 * Description : VxWorks OSAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_methodsInit = cAtFalse;

/* Override */
static tAtOsalMethods m_AtOsalOverride;

/* Super implementation */
static const tAtOsalMethods *m_AtOsalMethods = NULL;

static tAtOsal m_osal;

/*--------------------------- Forward declarations ---------------------------*/
extern void * sys_mem_malloc(uint32 size);
extern void sys_mem_free(void *ptr);
AtOsal AtOsalBdcom(void);

/*--------------------------- Implementation ---------------------------------*/
static void* MemAlloc(AtOsal self, uint32 size)
    {
	AtUnused(self);
    return sys_mem_malloc(size);
    }

static void MemFree(AtOsal self, void* pMem)
    {
	AtUnused(self);
    sys_mem_free(pMem);
    }

AtOsal AtOsalBdcom(void)
    {
    AtOsal vxWorks = AtOsalVxWorks();

    /* Initialize implementation */
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtOsalMethods = vxWorks->methods;
        m_AtOsalMethods->MemCpy(vxWorks, &m_AtOsalOverride, m_AtOsalMethods, sizeof(m_AtOsalOverride));

        /* Override memory operations */
        m_AtOsalOverride.MemAlloc = MemAlloc;
        m_AtOsalOverride.MemFree  = MemFree;
        }

    m_osal.methods = &m_AtOsalOverride;
    m_methodsInit  = 1;

    /* Return VxWorks OSAL */
    return &m_osal;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalZtePtn.c
 *
 * Created Date: Jun 4, 2013
 *
 * Description : Concrete HAL class for ZTE PTN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalZtePtnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods                m_AtHalOverride;
static tAtHalIndirectDefaultMethods m_AtHalIndirectDefaultOverride;

/* Super's implementation */
static const tAtHalMethods                *m_AtHalMethods                = NULL;
static const tAtHalIndirectDefaultMethods *m_AtHalIndirectDefaultMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalZtePtn);
    }

static uint8 IsDirectRegister(AtHal self, uint32 address)
    {
    if (m_AtHalIndirectDefaultMethods->IsDirectRegister(self, address))
        return cAtTrue;

    if ((address >= 0x700) && (address <= 0x705))
        return cAtTrue;

    return cAtFalse;
    }

static eBool VersionMatch(AtHal self)
    {
    const uint32 cVersionRegister = 0x700;
    const uint32 cVersion         = 0xAAF6;
    uint32 version = AtHalDirectRead(self, cVersionRegister);
    eBool match = (version == cVersion) ? cAtTrue : cAtFalse;

    if (!match)
        AtPrintc(cSevCritical, "ERROR: Version mismatch, expect 0x%x but got 0x%x\r\n", cVersion, version);

    return match;
    }

static eBool MemTest(AtHal self)
    {
    eBool testSuccess;

    if (!VersionMatch(self))
        return cAtFalse;

    testSuccess = m_AtHalMethods->MemTest(self);
    return testSuccess;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        m_AtHalOverride.MemTest = MemTest;
        }

    self->methods = &m_AtHalOverride;
    }

static void OverrideAtHalIndirectDefault(AtHal self)
    {
    AtHalIndirectDefault hal = (AtHalIndirectDefault)self;

    if (!m_methodsInit)
        {
        m_AtHalIndirectDefaultMethods = hal->methods;
        AtOsalMemCpy(&m_AtHalIndirectDefaultOverride, hal->methods, sizeof(m_AtHalIndirectDefaultOverride));

        m_AtHalIndirectDefaultOverride.IsDirectRegister = IsDirectRegister;
        }

    hal->methods = &m_AtHalIndirectDefaultOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalIndirectDefault(self);
    }

static void Setup(AtHal self)
    {
    AtHalIndirectDefaultAddressMaskSet(self, cBit24_0);
    AtHalIndirectDefaultIndirectControl1AddressSet(self, 0x702);
    AtHalIndirectDefaultIndirectControl2AddressSet(self, 0x703);
    AtHalIndirectDefaultIndirectData1AddressSet(self, 0x704);
    AtHalIndirectDefaultIndirectData2AddressSet(self, 0x705);

    /* Let's enable when it is required */
    AtHalDefaultFailureDetectEnable(self, cAtFalse);
    }

AtHal AtHalZtePtnObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* Additional configuration */
    Setup(self);

    return self;
    }

AtHal AtHalZtePtnNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return AtHalZtePtnObjectInit(newHal, baseAddress);
    }

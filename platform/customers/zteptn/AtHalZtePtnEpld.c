/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalZtePtnEpld.c
 *
 * Created Date: Jun 21, 2013
 *
 * Description : HAL for STM card of ZTE PTN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalZtePtnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtHalZtePtnEpld)self)
#define mRealAddress(self, address) ((address) << 1)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static uint32 DirectRead(AtHal self, uint32 address)
    {
    uint16 value;
    mThis(self)->directReadFunc(AtHalZtePtnEpldSlotId(self), (uint16)mRealAddress(self, address), &value);
    return (uint16)value;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mThis(self)->directWriteFunc(AtHalZtePtnEpldSlotId(self), (uint16)mRealAddress(self, address), (uint16)value);
    }

static char* BaseAddressDescriptionToString(AtHal self, char *buffer)
    {
    AtSprintf(buffer, "Slot %u", AtHalZtePtnEpldSlotId(self));
    return buffer;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.BaseAddressDescriptionToString = BaseAddressDescriptionToString;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        m_AtHalOverride.DirectRead  = DirectRead;
        m_AtHalOverride.DirectWrite = DirectWrite;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHalDefault(self);
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalZtePtnEpld);
    }

static AtHal ObjectInit(AtHal self, uint32 slotId,
                        BSPFpgaRead16 directReadFunc,
                        BSPFpgaWrite16 directWriteFunc)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super construction, but consider slotId is base address */
    if (AtHalZtePtnObjectInit(self, slotId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->directReadFunc  = directReadFunc;
    mThis(self)->directWriteFunc = directWriteFunc;

    return self;
    }

AtHal AtHalZtePtnEpldNew(uint32 slotId,
                         BSPFpgaRead16 directReadFunc,
                         BSPFpgaWrite16 directWriteFunc)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());

    if (newHal == NULL)
		return NULL;

    /* Construct it */
    return ObjectInit(newHal, slotId, directReadFunc, directWriteFunc);
    }

uint32 AtHalZtePtnEpldSlotId(AtHal self)
    {
    return AtHalBaseAddressGet(self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalZtePtnInternal.h
 * 
 * Created Date: Jul 16, 2013
 *
 * Description : HAL for ZTE PTN products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALZTEPTNINTERNAL_H_
#define _ATHALZTEPTNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../hal/AtHalIndirectDefaultInternal.h"
#include "AtHalZtePtn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalZtePtnEpld * AtHalZtePtnEpld;

typedef struct tAtHalZtePtn
    {
    tAtHalIndirectDefault super;

    /* Private data */
    }tAtHalZtePtn;

typedef struct tAtHalZtePtnEpld
    {
    tAtHalZtePtn super;

    /* Private data */
    BSPFpgaRead16 directReadFunc;
    BSPFpgaWrite16 directWriteFunc;
    }tAtHalZtePtnEpld;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtHal AtHalZtePtnObjectInit(AtHal self, uint32 baseAddress);
AtHal AtHalZtePtnEpldObjectInit(AtHal self, uint32 slotId, BSPFpgaRead16 directReadFunc, BSPFpgaWrite16 directWriteFunc);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALZTEPTNINTERNAL_H_ */


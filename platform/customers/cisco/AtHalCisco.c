/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalCisco.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : HAL for Cisco projects
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtObject.h"
#include "AtHalCisco.h"
#include "../../hal/AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/
#define cIndirectEnableRegister  0x07FF000

#define cIndirectControlRegister 0x07FF001
#define cIndirectRequestMask     cBit31
#define cIndirectRequestShift    31
#define cIndirectOperationMask   cBit30
#define cIndirectOperationShift  30
#define cIndirectOperationRead   1
#define cIndirectOperationWrite  0
#define cIndirectAddressMask     cBit24_0
#define cIndirectAddressShift    0

#define cIndirectWriteDataRegister  0x07FF002
#define cIndirectReadDataRegister   0x07FF003
#define cInvalidValue               0xDEADC0FE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalCisco *)self)
#define mRealAddress(self, localAddress) AtHalBaseAddressGet(self) + (localAddress << 2)
#define mDirectWrite(self, address, value) mThis(self)->writeFunc(mRealAddress(self, address), value)
#define mDirectRead(self, address) mThis(self)->readFunc(mRealAddress(self, address))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalCisco
    {
    tAtHalDefault super;

    /* Private data */
    WriteFunc writeFunc;
    ReadFunc  readFunc;

    /* IMPORTANT: this variable is used to check if indirect mode is enabled or not.
     * Reading from hardware to determine is fine but it will slow down read/write
     * operations */
    eBool indirectAccessEnabled;

    /* For debugging */
    uint32 maxRetryTimes;
    }tAtHalCisco;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mDirectWrite(self, address, value);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    return mDirectRead(self, address);
    }

static eBool RequestAndWait(AtHal self, uint32 address, uint8 operation)
    {
    static const uint32 cRetryTimes = 1000000;
    uint32 retryTimes = 0;
    uint32 regVal;

    /* Make request */
    regVal = cIndirectRequestMask;
    mRegFieldSet(regVal, cIndirectOperation, operation);
    mRegFieldSet(regVal, cIndirectAddress, address);
    mDirectWrite(self, cIndirectControlRegister, regVal);

    /* Wait */
    while (retryTimes < cRetryTimes)
        {
        if ((mDirectRead(self, cIndirectControlRegister) & cIndirectRequestMask) == 0)
            {
            if (mThis(self)->maxRetryTimes < retryTimes)
                mThis(self)->maxRetryTimes = retryTimes;
            return cAtTrue;
            }

        /* Or retry */
        retryTimes = retryTimes + 1;
        }

    return cAtFalse;
    }

static void IndirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mDirectWrite(self, cIndirectWriteDataRegister, value);

    if (RequestAndWait(self, address, cIndirectOperationWrite))
        return;

    AtPrintc(cSevCritical,
             "ERROR: Write 0x%08x, value 0x%08x timeout\r\n",
             address,
             value);
    }

static uint32 IndirectRead(AtHal self, uint32 address)
    {
    if (RequestAndWait(self, address, cIndirectOperationRead))
        return mDirectRead(self, cIndirectReadDataRegister);

    AtPrintc(cSevCritical, "ERROR: Read 0x%08x timeout\r\n",address);
    return cInvalidValue;
    }

static uint32 HwRead(AtHal self, uint32 address)
    {
    if (mThis(self)->indirectAccessEnabled)
        return IndirectRead(self, address);
    return mDirectRead(self, address);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 regVal = HwRead(self, address);

    if ((regVal == cInvalidReadValue) && AtHalDebugIsEnabled(self))
        {
        AtPrintc(cSevWarning, "(%s, %d) Read 0x%08x, value 0x%08x (FPGA at 0x%08x)\n",
                 __FILE__, __LINE__,
                 address, regVal,
                 AtHalDefaultBaseAddress(self));
        }

    return regVal;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    if (mThis(self)->indirectAccessEnabled)
        IndirectWrite(self, address, value);
    else
        mDirectWrite(self, address, value);
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[8];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], 0xF00040);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], 0xF00041);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], 0xF00042);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[3], 0xF00043);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[4], 0xF00044);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[5], 0xF00045);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[6], 0xF00046);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[7], 0xF00047);

    return regsToTest;
    }

static uint32 DataBusMask(AtHal self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 TestPattern(AtHal hal)
    {
    AtUnused(hal);
    return 0xAAAAAAAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
    AtUnused(self);
    return 0x55555555;
    }

static uint32 MaxAddress(AtHal self)
    {
    AtUnused(self);
    return cBit24_0;
    }

static void IndirectAccessEnable(AtHal self, eBool enable)
    {
    mDirectWrite(self, cIndirectEnableRegister, enable ? 1 : 0);
    mThis(self)->indirectAccessEnabled = enable;
    m_AtHalMethods->IndirectAccessEnable(self, enable);
    }

static eBool IndirectAccessIsEnabled(AtHal self)
    {
    return mThis(self)->indirectAccessEnabled;
    }

static void Debug(AtHal self)
    {
    m_AtHalMethods->Debug(self);
    AtPrintc(cSevNormal, "* Max retry times: %u\r\n", mThis(self)->maxRetryTimes);
    }

static eBool NeedRestoreTestRegValues(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        mMethodOverride(m_AtHalDefaultOverride, RegsToTest);
        mMethodOverride(m_AtHalDefaultOverride, DataBusMask);
        mMethodOverride(m_AtHalDefaultOverride, TestPattern);
        mMethodOverride(m_AtHalDefaultOverride, TestAntiPattern);
        mMethodOverride(m_AtHalDefaultOverride, NeedRestoreTestRegValues);
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        mMethodOverride(m_AtHalOverride, Read);
        mMethodOverride(m_AtHalOverride, Write);
        mMethodOverride(m_AtHalOverride, DirectRead);
        mMethodOverride(m_AtHalOverride, DirectWrite);
        mMethodOverride(m_AtHalOverride, MaxAddress);
        mMethodOverride(m_AtHalOverride, Debug);
        mMethodOverride(m_AtHalOverride, IndirectAccessEnable);
        mMethodOverride(m_AtHalOverride, IndirectAccessIsEnabled);
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalCisco);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->writeFunc = writeFunc;
    mThis(self)->readFunc  = readFunc;

    return self;
    }

/*
 * Create a new HAL that works on Cisco platform
 *
 * @param baseAddress Base address. Application must know.
 *
 * @return HAL object
 */
AtHal AtHalCiscoNew(uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress, writeFunc, readFunc);
    }

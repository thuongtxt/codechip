/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalAlu.c
 *
 * Created Date: Aug 8, 2014
 *
 * Description : Hal for ALU
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalAlu.h"
#include "../../hal/AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalAlu *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalAlu
    {
    tAtHalDefault super;

    /* Private data */
    WriteFunc writeFunc;
    ReadFunc  readFunc;
    }tAtHalAlu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define mRealAddress(self, localAddress) AtHalBaseAddressGet(self) + (localAddress << 2)

static void Write(AtHal self, uint32 address, uint32 value)
    {
    mThis(self)->writeFunc(mRealAddress(self, address), value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    return mThis(self)->readFunc(mRealAddress(self, address));
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[1];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], 0xF00041);
    return regsToTest;
    }

static uint32 DataBusMask(AtHal self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 ReadAddressFromWriteAddress(AtHal hal, uint32 address)
    {
    AtUnused(hal);
    if (address == 0xF00041)
        return 0xF0006E;
    return 0xCAFECAFE;
    }

static uint32 ReadValueFromWriteValue(AtHal hal, uint32 value)
    {
    AtUnused(hal);
    return ~value;
    }

static uint32 TestPattern(AtHal hal)
    {
    AtUnused(hal);
    return 0xAAAAAAAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
    AtUnused(self);
    return 0x55555555;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.RegsToTest      = RegsToTest;
        m_AtHalDefaultOverride.DataBusMask     = DataBusMask;
        m_AtHalDefaultOverride.TestPattern     = TestPattern;
        m_AtHalDefaultOverride.TestAntiPattern = TestAntiPattern;
        m_AtHalDefaultOverride.ReadAddressFromWriteAddress = ReadAddressFromWriteAddress;
        m_AtHalDefaultOverride.ReadValueFromWriteValue     = ReadValueFromWriteValue;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Read        = Read;
        m_AtHalOverride.Write       = Write;
        m_AtHalOverride.DirectRead  = Read;
        m_AtHalOverride.DirectWrite = Write;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalAlu);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->writeFunc = writeFunc;
    mThis(self)->readFunc  = readFunc;

    return self;
    }

/*
 * Create a new HAL that works on ALU platform
 *
 * @param baseAddress Base address. It can be: bsw_Archimede1BaseAddress[3]
 *
 * @return HAL object
 */
AtHal AtHalAluNew(uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress, writeFunc, readFunc);
    }

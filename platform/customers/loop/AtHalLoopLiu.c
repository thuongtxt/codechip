/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalLoopLiu.c
 *
 * Created Date: Mar 12, 2014
 *
 * Description : HAL to control LIU
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../hal/AtHalDefault.h"
#include "AtHalLoop.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalLoopLiu *)self)
#define mBaseAddress(self) (mThis(self)->baseAddress)
#define mRealAddress(self, address) (((address) & cBit10_0) + (mBaseAddress(self)))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalLoopLiu
    {
    tAtHal super;

    /* Private data */
    AtHal hal;
    uint32 baseAddress;

    }tAtHalLoopLiu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return 0xF42000;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    AtHalWrite(mThis(self)->hal, mRealAddress(self, address), value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    return AtHalRead(mThis(self)->hal, mRealAddress(self, address));
    }

static void Debug(AtHal self)
    {
    AtHalDebug(mThis(self)->hal);
    }

static void Lock(AtHal self)
    {
	AtUnused(self);
    }

static void UnLock(AtHal self)
    {
	AtUnused(self);
    }

static void Delete(AtHal self)
    {
    AtOsalMemFree(self);
    }

static const char* ToString(AtHal self)
    {
    return AtHalToString(mThis(self)->hal);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalLoopLiu);
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_AtHalOverride, 0, sizeof(m_AtHalOverride));
        m_AtHalOverride.Write    = Write;
        m_AtHalOverride.Read     = Read;
        m_AtHalOverride.Delete   = Delete;
        m_AtHalOverride.Debug    = Debug;
        m_AtHalOverride.Lock     = Lock;
        m_AtHalOverride.UnLock   = UnLock;
        m_AtHalOverride.ToString = ToString;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static AtHal ObjectInit(AtHal self, AtHal hal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->hal         = hal;
    mThis(self)->baseAddress = BaseAddress();
    return self;
    }

AtHal AtHalLoopLiuNew(AtHal hal)
    {
	AtHal halLiu;

    if (hal == NULL)
        return NULL;

    halLiu = AtOsalMemAlloc(ObjectSize());
    if (halLiu == NULL)
    	return NULL;

    return ObjectInit(halLiu, hal);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalLoopSerdes.c
 *
 * Created Date: Mar 10, 2014
 *
 * Description : HAl to control ETH Port SERDES
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "atclib.h"
#include "AtOsal.h"
#include "AtHalLoop.h"
#include "AtHalLoopSerdesInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Control register and fields */
#define cControlPortReg             0x000010
#define cControlPortAddressMask     cBit4_0
#define cControlPortAddressShift    0

#define cControlReg                 0x000012
#define cControlReadWriteStartMask  cBit7
#define cControlReadWriteStartShift 7
#define cControlOperationMask       cBit6_5
#define cControlOperationShift      5
#define cControlOperationWrite      1
#define cControlOperationRead       2
#define cControlRegAddressMask      cBit4_0
#define cControlRegAddressShift     0

/* ACK */
#define cStatusAckReg               0x000000
#define cStatusClearAckMask         cBit0

#define cControlAckReg              0x000011
#define cControlClearAckMask        cBit0

/* Register to read/write data */
#define cReadWriteDatasReg          0x000020
#define cWriteDataMask              cBit31_16
#define cWriteDataShift             16
#define cReadDataMask               cBit15_0

#define cInvalidValue               0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)        ((tAtHalLoopSerdes *)self)
#define mBaseAddress(self) (mThis(self)->methods->SerdesBaseAddress(self))

/*--------------------------- Local typedefs ---------------------------------*/
/* Request types */
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalLoopSerdesMethods m_methods;

/* Override */
static tAtHalMethods m_AtHalOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalLoopSerdes);
    }

static void Delete(AtHal self)
    {
    AtOsalMemFree(self);
    }

static uint32 ReadControl(AtHal self, uint32 controlRegister)
    {
    return AtHalRead(mThis(self)->hal, controlRegister);
    }

static void WriteControl(AtHal self, uint32 controlRegister, uint32 value)
    {
    AtHalWrite(mThis(self)->hal, controlRegister, value);
    }

static void ClearAck(AtHal self)
    {
    WriteControl(self, cControlAckReg + mBaseAddress(self), cControlClearAckMask);
    }

static eBool HwDone(AtHal self, uint32 address, eRequestType requestType)
    {
    uint32 elapsedTime = 0;
    tAtOsalCurTime startTime, currentTime;
    uint32 ackReg = cStatusAckReg + mBaseAddress(self);

    AtOsalCurTimeGet(&startTime);
    AtOsalCurTimeGet(&currentTime);
    while (elapsedTime < mThis(self)->retryInMs)
        {
        /* Done */
        if (ReadControl(self, ackReg) & cStatusClearAckMask)
            return cAtTrue;

        /* Retry */
        AtOsalCurTimeGet(&currentTime);
        elapsedTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    /* Timeout, log debug information */
    AtPrintc(cSevCritical,
             "(%s, %d) ERROR: %s 0x%08x timeout(startTime: %u.%u, stopTime: %u.%u)\n",
             __FILE__, __LINE__,
             (requestType == cRequestRead) ? "Read" : "Write", address,
             startTime.sec, startTime.usec,
             currentTime.sec, currentTime.usec);

    return cAtFalse;
    }

static uint8 PortAddress(AtHal self)
    {
    return (uint8)(mThis(self)->portId + 1);
    }

static void PortAddressSet(AtHal self)
    {
    WriteControl(self, cControlPortReg + mBaseAddress(self), PortAddress(self) & cBit4_0);
    }

static void MakeRequest(AtHal self, uint32 address, eRequestType requestType)
    {
    uint32 regVal = 0;
    PortAddressSet(self);
    mRegFieldSet(regVal, cControlOperation, (requestType == cRequestRead) ? cControlOperationRead : cControlOperationWrite);
    mRegFieldSet(regVal, cControlRegAddress, (address & cBit4_0));
    mRegFieldSet(regVal, cControlReadWriteStart, 1);
    WriteControl(self, cControlReg + mBaseAddress(self), regVal);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    uint32 regVal = 0;
    uint32 baseAddress = mBaseAddress(self);

    ClearAck(self);

    address = address & cBit4_0;
    mRegFieldSet(regVal, cWriteData, value & cBit15_0);
    WriteControl(self, cReadWriteDatasReg + baseAddress, regVal);
    MakeRequest(self, address, cRequestWrite);

    (void)HwDone(self, address, cRequestWrite);
    ClearAck(self);
    WriteControl(self, cControlReg + baseAddress, 0);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 regVal;
    uint32 baseAddress = mBaseAddress(self);

    ClearAck(self);

    MakeRequest(self, address, cRequestRead);
    if (HwDone(self, address, cRequestRead))
        regVal = ReadControl(self, cReadWriteDatasReg + baseAddress) & cReadDataMask;
    else
        regVal = cInvalidValue;

    ClearAck(self);
    WriteControl(self, cControlReg + baseAddress, 0);

    return regVal;
    }

static void Debug(AtHal self)
    {
    AtHalDebug(mThis(self)->hal);
    }

static void Lock(AtHal self)
    {
	AtUnused(self);
    }

static void UnLock(AtHal self)
    {
	AtUnused(self);
    }

static const char* ToString(AtHal self)
    {
    return AtHalToString(mThis(self)->hal);
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_AtHalOverride, 0, sizeof(m_AtHalOverride));
        m_AtHalOverride.Delete   = Delete;
        m_AtHalOverride.Debug    = Debug;
        m_AtHalOverride.Write    = Write;
        m_AtHalOverride.Read     = Read;
        m_AtHalOverride.Lock     = Lock;
        m_AtHalOverride.UnLock   = UnLock;
        m_AtHalOverride.ToString = ToString;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 SerdesBaseAddress(AtHal self)
    {
	AtUnused(self);
    return 0xF51000;
    }

static void MethodsInit(AtHal self)
    {
    AtHalLoopSerdes hal = (AtHalLoopSerdes)self;

    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.SerdesBaseAddress = SerdesBaseAddress;
        }

    hal->methods = &m_methods;
    }

AtHal AtHalLoopSerdesObjectInit(AtHal self, AtHal hal, uint8 portId)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->hal       = hal;
    mThis(self)->retryInMs = 10;
    mThis(self)->portId    = portId;

    return self;
    }

AtHal AtHalLoopSerdesNew(AtHal hal, uint8 portId)
    {
	AtHal halSerdes;

    if (hal == NULL)
        return NULL;

    halSerdes = AtOsalMemAlloc(ObjectSize());
    if (halSerdes == NULL)
    	return NULL;

    return AtHalLoopSerdesObjectInit(halSerdes, hal, portId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalLoop.c
 *
 * Created Date: Feb 24, 2014
 *
 * Description : HAL for LOOP projects
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../hal/AtHalDefault.h"
#include "AtHalLoop.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalLoop *)((void*)self))
#define mRealAddress(address) (address & cBit23_0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalLoop
    {
    tAtHalDefault super;

    /* Private data */
    FpgaRead readFunc;
    FpgaWrite writeFunc;
    AtHal liuHal;
    AtHal *serdesHals;
    }tAtHalLoop;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Write(AtHal self, uint32 address, uint32 value)
    {
    mThis(self)->writeFunc(mRealAddress(address), value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    unsigned long value;
    mThis(self)->readFunc(mRealAddress(address), &value);
    return (uint32)value;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalLoop);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    return Read(self, address);
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    Write(self, address, value);
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[4];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], 0xf00040);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], 0xf00041);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], 0xf00042);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[3], 0xf00043);

    return regsToTest;
    }

static uint32 TestPattern(AtHal hal)
    {
	AtUnused(hal);
    return 0xAAAAAAAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
	AtUnused(self);
    return 0x55555555;
    }

static uint32 DataBusMask(AtHal self)
    {
	AtUnused(self);
    return cBit23_0;
    }

static uint8 MaxNumEthPorts(AtHal self)
    {
	AtUnused(self);
    return 1;
    }

static void GeSerdesHalsDelete(AtHal self)
    {
    uint8 i;
    if (mThis(self)->serdesHals == NULL)
        return;

    for (i = 0; i < MaxNumEthPorts(self); i++)
        AtHalDelete(mThis(self)->serdesHals[i]);

    AtOsalMemFree(mThis(self)->serdesHals);
    }

static AtHal GeSerdesHalCreate(AtHal self, uint8 portId)
    {
    uint32 productCode = AtProductCodeGetByHal(self);

    if (productCode == 0x60070041)
        return AtHalLoopSerdesNew(self, portId);

    if (productCode == 0x60070061)
        return AtHalLoopSerdesStmNew(self, portId);

    return NULL;
    }

static AtHal GeSerdesHal(AtHal self, uint8 ethPortId)
    {
    uint8 numSerdes = MaxNumEthPorts(self);

    if (ethPortId >= numSerdes)
        return NULL;

    /* Create array to hold HALs */
    if (mThis(self)->serdesHals == NULL)
        {
        uint32 memorySize = sizeof(AtHal) * numSerdes;
        AtHal *hals = AtOsalMemAlloc(memorySize);
        AtOsalMemInit(hals, 0, memorySize);
        mThis(self)->serdesHals = hals;
        }

    /* Memory maybe not allocated successfully */
    if (mThis(self)->serdesHals == NULL)
        return NULL;

    /* Create HAL for SERDES if it has not been created yet */
    if (mThis(self)->serdesHals[ethPortId] == NULL)
        mThis(self)->serdesHals[ethPortId] = GeSerdesHalCreate(self, ethPortId);

    return mThis(self)->serdesHals[ethPortId];
    }

static void Delete(AtHal self)
    {
    GeSerdesHalsDelete(self);
    AtHalDelete(mThis(self)->liuHal);
    m_AtHalMethods->Delete(self);
    }

static AtHal LiuHalGet(AtHal self)
    {
    return AtHalLoopLiu(self);
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        m_AtHalOverride.Read        = Read;
        m_AtHalOverride.Write       = Write;
        m_AtHalOverride.DirectRead  = DirectRead;
        m_AtHalOverride.DirectWrite = DirectWrite;
        m_AtHalOverride.Delete      = Delete;
        m_AtHalOverride.LiuHalGet   = LiuHalGet;
        }

    self->methods = &m_AtHalOverride;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.RegsToTest      = RegsToTest;
        m_AtHalDefaultOverride.TestPattern     = TestPattern;
        m_AtHalDefaultOverride.TestAntiPattern = TestAntiPattern;
        m_AtHalDefaultOverride.DataBusMask     = DataBusMask;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static AtHal ObjectInit(AtHal self, FpgaRead readFunc, FpgaWrite writeFunc)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtHalDefaultObjectInit(self, 0) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->readFunc  = readFunc;
    mThis(self)->writeFunc = writeFunc;

    return self;
    }

AtHal AtHalLoopNew(FpgaRead readFunc, FpgaWrite writeFunc)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
    	return NULL;

    return ObjectInit(newHal, readFunc, writeFunc);
    }

AtHal AtHalLoopGeSerdes(AtHal self, uint8 ethPortId)
    {
    if (self)
        return GeSerdesHal(self, ethPortId);
    return NULL;
    }

AtHal AtHalLoopLiu(AtHal self)
    {
    if (self == NULL)
        return NULL;

    if (mThis(self)->liuHal == NULL)
        mThis(self)->liuHal = AtHalLoopLiuNew(self);

    return mThis(self)->liuHal;
    }

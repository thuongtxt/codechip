/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalLoopStmSerdes.c
 *
 * Created Date: Dec 10, 2014
 *
 * Description : This file contains implementations of serdes of AF6LTT0061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalLoopSerdesInternal.h"
#include "AtHalLoop.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalLoopSerdesStm
    {
    tAtHalLoopSerdes super;
    } tAtHalLoopSerdesStm;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalLoopSerdesMethods m_AtHalLoopSerdesOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalLoopSerdesStm);
    }

static uint32 SerdesBaseAddress(AtHal self)
    {
	AtUnused(self);
    return 0xF40000;
    }

static void OverrideAtHalLoopSerdes(AtHal self)
    {
    AtHalLoopSerdes serdes = (AtHalLoopSerdes)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalLoopSerdesOverride, serdes->methods, sizeof(m_AtHalLoopSerdesOverride));

        m_AtHalLoopSerdesOverride.SerdesBaseAddress = SerdesBaseAddress;
        }

    serdes->methods = &m_AtHalLoopSerdesOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHalLoopSerdes(self);
    }

static AtHal ObjectInit(AtHal self, AtHal hal, uint8 portId)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super initialization */
    if (AtHalLoopSerdesObjectInit(self, hal, portId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalLoopSerdesStmNew(AtHal hal, uint8 portId)
    {
    AtHal newHal;

    if (hal == NULL)
        return NULL;

    newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    return ObjectInit(newHal, hal, portId);
    }

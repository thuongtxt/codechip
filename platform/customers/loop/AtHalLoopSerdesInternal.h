/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalLoopSerdesInternal.h
 * 
 * Created Date: Dec 11, 2014
 *
 * Description : This file contains declarations of serdes of AF6LTT0061
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALLOOPSERDESINTERNAL_H_
#define _ATHALLOOPSERDESINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../hal/AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalLoopSerdes * AtHalLoopSerdes;

typedef struct tAtHalLoopSerdesMethods
    {
    uint32 (*SerdesBaseAddress)(AtHal self);
    } tAtHalLoopSerdesMethods;

typedef struct tAtHalLoopSerdes
    {
    tAtHal super;
    const tAtHalLoopSerdesMethods *methods;

    /* Private data */
    AtHal hal;
    uint8 retryInMs;
    uint8 portId;
    }tAtHalLoopSerdes;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalLoopSerdesObjectInit(AtHal self, AtHal hal, uint8 portId);

#endif /* _ATHALLOOPSERDESINTERNAL_H_ */


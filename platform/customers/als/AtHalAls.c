/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalAls.c
 *
 * Created Date: Apr 9, 2013
 *
 * Description : HAL for ALS project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalAls.h"
#include "../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

#define mRealAddress(self, address) (AtSize)((address << 2) + AtHalDefaultBaseAddress(self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalAls * AtHalAls;

typedef struct tAtHalAls
    {
    tAtHalIndirectDefault super;

    /* Private data */
    }tAtHalAls;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalAls);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    uint16 value;

    value = (uint16)(*((volatile uint32 *)mRealAddress(self, address)));
    mAsm("sync");

    return value;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    *((volatile uint32 *)mRealAddress(self, address)) = (uint16)value;
    mAsm("sync");
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        m_AtHalOverride.DirectRead  = DirectRead;
        m_AtHalOverride.DirectWrite = DirectWrite;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalAlsNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
    	return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

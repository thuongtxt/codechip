/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFibroLan.c
 *
 * Created Date: Jan 9, 2015
 *
 * Description : Hal for FibroLan project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtHalFibroLan.h"
#include "../../hal/AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegControl1 0xF000F0
#define cRegControl2 0xF000F1
#define cRegControl3 0xF000F2
#define cRegControl4 0xF000F3

#define cRegData1    0xF000F4
#define cRegData2    0xF000F5
#define cRegData3    0xF000F6
#define cRegData4    0xF000F7

#define cInvalidReadValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalFibroLan *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalFibroLan
    {
    tAtHalDefault super;

    /* Private data */
    WriteFunc writeFunc;
    ReadFunc  readFunc;
    uint32    retryTimes; /* Retry time to wait for HW to finish accessing register */
    uint32    maxRetryTimes; /* Maximum retry times while software wait to read/write FPGA */
    uint32    addressMask;
    }tAtHalFibroLan;

/* Request types */
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mThis(self)->writeFunc(address, (uint8)value);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    return mThis(self)->readFunc(address);
    }

static uint32 DataBusMask(AtHal self)
    {
	AtUnused(self);
    return cBit7_0;
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[7];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], cRegControl1);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], cRegControl2);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], cRegControl3);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[3], cRegData1);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[4], cRegData2);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[5], cRegData3);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[6], cRegData4);

    return regsToTest;
    }

static uint8 HwDone(AtHal self)
    {
    volatile uint32 retryTimes = 0;

   /* Check if hardware done */
    while (retryTimes < mThis(self)->retryTimes)
        {
        if ((DirectRead(self, cRegControl4) & cBit7) == 0)
            {
            if (mThis(self)->maxRetryTimes < retryTimes)
                mThis(self)->maxRetryTimes = retryTimes;
            return 1;
            }

        /* Or retry */
        retryTimes = retryTimes + 1;
        }

    return 0;
    }

static uint8 Request(AtHal self, uint32 address, eRequestType requestType)
    {
    uint8 request = 0;

    /* Write address */
    DirectWrite(self, cRegControl1,  address & cBit7_0);
    DirectWrite(self, cRegControl2, (address & cBit15_8)  >> 8);
    DirectWrite(self, cRegControl3, (address & cBit23_16) >> 16);

    /* Write request type */
    if (requestType == cRequestRead)
        request |= (uint8)cBit6;

    /* Start request */
    request |= (uint8)cBit7;
    DirectWrite(self, cRegControl4, request);

    return HwDone(self);
    }

static uint8 IsDirectRegister(AtHal self, uint32 address)
    {
	AtUnused(self);
    if ((address == cRegControl1) ||
        (address == cRegControl2) ||
        (address == cRegControl3) ||
        (address == cRegControl4))
        return cAtTrue;

    if ((address == cRegData1) ||
        (address == cRegData2) ||
        (address == cRegData3) ||
        (address == cRegData4))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ReadData(AtHal self)
    {
    uint32 data = 0;

    data |= DirectRead(self, cRegData1);
    data |= DirectRead(self, cRegData2) << 8;
    data |= DirectRead(self, cRegData3) << 16;
    data |= DirectRead(self, cRegData4) << 24;

    return data;
    }

static uint32 Read(AtHal self, uint32 address)
    {
    /* Read a control register */
    if (IsDirectRegister(self, address))
        return DirectRead(self, address);

    if (Request(self, address, cRequestRead))
        return ReadData(self);

    return cInvalidReadValue;
    }

static void WriteData(AtHal self, uint32 value)
    {
    DirectWrite(self, cRegData1, (uint8) (value & cBit7_0));
    DirectWrite(self, cRegData2, (uint8)((value & cBit15_8)  >> 8));
    DirectWrite(self, cRegData3, (uint8)((value & cBit23_16) >> 16));
    DirectWrite(self, cRegData4, (uint8)((value & cBit31_24) >> 24));
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    if (IsDirectRegister(self, address))
        {
        DirectWrite(self, address, value);
        return;
        }

    WriteData(self, value);
    if (!Request(self, address, cRequestWrite))
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Write 0x%08x, value 0x%08x timeout\n", __FILE__, __LINE__, address, value);
    }

static void Debug(AtHal self)
    {
    AtPrintc(cSevNormal, "AddressMask     : 0x%08x\r\n", mThis(self)->addressMask);
    AtPrintc(cSevNormal, "retryTimes      : %d(times)\r\n", mThis(self)->retryTimes);
    AtPrintc(cSevNormal, "maxRetryTimes   : %d(times)\r\n", mThis(self)->maxRetryTimes);

    mThis(self)->maxRetryTimes = 0;
    }

static eBool IndirectAccessIsEnabled(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));
        m_AtHalOverride.Read                    = Read;
        m_AtHalOverride.Write                   = Write;
        m_AtHalOverride.DirectRead              = DirectRead;
        m_AtHalOverride.DirectWrite             = DirectWrite;
        m_AtHalOverride.Debug                   = Debug;
        m_AtHalOverride.IndirectAccessIsEnabled = IndirectAccessIsEnabled;
        }

    self->methods = &m_AtHalOverride;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.RegsToTest  = RegsToTest;
        m_AtHalDefaultOverride.DataBusMask = DataBusMask;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalFibroLan);
    }

static void Setup(AtHal self, WriteFunc writeFunc, ReadFunc readFunc)
    {
    mThis(self)->retryTimes    = 1000000;
    mThis(self)->maxRetryTimes = 0;
    mThis(self)->addressMask   = cBit24_0;
    mThis(self)->writeFunc     = writeFunc;
    mThis(self)->readFunc      = readFunc;
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    Setup(self, writeFunc, readFunc);

    return self;
    }

/**
 * Create a new HAL that works on FibroLan platform
 *
 * @param baseAddress Base address.
 *
 * @return HAL object
 */
AtHal AtHalFibroLanNew(uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress, writeFunc, readFunc);
    }

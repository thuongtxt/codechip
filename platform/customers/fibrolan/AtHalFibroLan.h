/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalFibroLan.h
 * 
 * Created Date: Jan 9, 2015
 *
 * Description : HAL for FibroLan project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALFIBROLAN_H_
#define _ATHALFIBROLAN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Direct access functions which depend on specific platform */
typedef void (*WriteFunc)(uint32 realAddress, uint8 value);
typedef uint8 (*ReadFunc)(uint32 realAddress);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalFibroLanNew(uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALFIBROLAN_H_ */


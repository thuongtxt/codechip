/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFhw16Bit.c
 *
 * Created Date: May 9, 2013
 *
 * Description : HAL for FHW product, 16-bit registers
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../hal/AtHalIndirectDefaultInternal.h"
#include "AtHalFhwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cIndirectControl1 0x7FFF02
#define cIndirectControl2 0x7FFF03
#define cIndirectData     0x7FFF04

#define cThaReadTopHoldBit31_16Adr  0x0F0041
#define cThaReadHoldBit31_16Adr     0x000021
#define cThaWriteTopHoldBit31_16Adr 0x0F0040
#define cThaWriteHoldBit31_16Adr    0x000011

/*--------------------------- Macros -----------------------------------------*/
/* Direct read/write */
#define mRealAddress(self, address) (AtSize)(((address) << 2) + AtHalDefaultBaseAddress(self))
#define mDirectWrite(self_, address_, value_) (self_)->methods->DirectWrite(self_, address_, value_)
#define mDirectRead(self_, address_) (self_)->methods->DirectRead(self_, address_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8  m_methodsInit = 0;
static const uint32 m_retryInMs   = 5;

/* Override */
static tAtHalMethods        m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 IsDirectRegister(uint32 address)
    {
    return ((address >= 0x7FFF02) && (address <= 0x7FFF04));
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    return *(volatile uint32 *)mRealAddress(self, address);
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    *((volatile uint32 *)mRealAddress(self, address)) = (uint32)value;
    }

static uint32 Register16BitRead(AtHal self, uint32 address)
    {
    uint32 elapsedTime = 0;
    uint32 regVal;
    tAtOsalCurTime startTime, currentTime;

    /* Read a control register */
    address = address & cBit23_0;
    if (IsDirectRegister(address))
        return mDirectRead(self, address);

    regVal  = address;
    regVal |= cBit30; /* Read request */
    regVal |= cBit31; /* Request hardware */
    mDirectWrite(self, cIndirectControl1, regVal & cBit15_0);
    mDirectWrite(self, cIndirectControl2, (regVal >> 16));

    /* Wait for hardware ready */
    AtOsalCurTimeGet(&startTime);
    while (elapsedTime < m_retryInMs)
        {
        /* If hardware is ready, return data */
        regVal = mDirectRead(self, cIndirectControl2);
        if ((regVal & cBit15) == 0)
            return mDirectRead(self, cIndirectData);

        /* Or retry */
        AtOsalCurTimeGet(&currentTime);
        elapsedTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    /* Timeout */
    AtPrintc(cSevCritical, "(%s, %d): ERROR: Read 0x%08x timeout\n", __FILE__, __LINE__, address);

    return cInvalidReadValue;
    }

static void Register16BitWrite(AtHal self, uint32 address, uint32 value)
    {
    uint32 dIndrRegVal;
    uint32 elapsedTime = 0;
    tAtOsalCurTime startTime, currentTime;

    /* Direct register */
    address = address & cBit23_0;
    if (IsDirectRegister(address))
        {
        mDirectWrite(self, address, value);
        return;
        }

    /* Write data, address then make request */
    mDirectWrite(self, cIndirectData, value);
    dIndrRegVal  = address;
    dIndrRegVal |= cBit31;
    mDirectWrite(self, cIndirectControl1, dIndrRegVal & cBit15_0);
    mDirectWrite(self, cIndirectControl2, (dIndrRegVal >> 16));

    /* Check if hardware done */
    AtOsalCurTimeGet(&startTime);
    while (elapsedTime < m_retryInMs)
        {
        dIndrRegVal = mDirectRead(self, cIndirectControl2);
        if ((dIndrRegVal & cBit15) == 0)
            return;

        /* Or retry */
        AtOsalCurTimeGet(&currentTime);
        elapsedTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    AtPrintc(cSevCritical, "(%s, %d): ERROR: Write 0x%08x, value 0x%08x timeout\n", __FILE__, __LINE__, address, value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 field31_16, field15_0;
    uint32 regVal;

    field15_0 = Register16BitRead(self, address);
    if ((address & cBit23_16) == 0xF0000)
        field31_16 = Register16BitRead(self, cThaReadTopHoldBit31_16Adr);
    else
        field31_16 = Register16BitRead(self, cThaReadHoldBit31_16Adr);

    regVal = ((field31_16 & cBit15_0) << 16) | (field15_0 & cBit15_0);

    return regVal;
    }

static uint16 Read16(AtHal self, uint32 address)
    {
    return ((Register16BitRead(self, address)) & cBit15_0);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    if (((address) & cBit23_16) == 0xF0000)
        Register16BitWrite(self, cThaWriteTopHoldBit31_16Adr, value >> 16);
    else
        Register16BitWrite(self, cThaWriteHoldBit31_16Adr, value >> 16);

    Register16BitWrite(self, address, value);
    }

static void Write16(AtHal self, uint32 address, uint16 value)
    {
    Register16BitWrite(self, address, value);
    }

static void StateSave(AtHal self)
    {
	AtUnused(self);

    }

static void StateRestore(AtHal self)
    {
	AtUnused(self);

    }

static eBool MemTest(AtHal self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 DirectOamOffsetGet(AtHal self)
    {
	AtUnused(self);
    return 0;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read               = Read;
        m_AtHalOverride.Write              = Write;
        m_AtHalOverride.Read16             = Read16;
        m_AtHalOverride.Write16            = Write16;
        m_AtHalOverride.DirectRead         = DirectRead;
        m_AtHalOverride.DirectWrite        = DirectWrite;
        m_AtHalOverride.StateSave          = StateSave;
        m_AtHalOverride.StateRestore       = StateRestore;
        m_AtHalOverride.MemTest            = MemTest;
        m_AtHalOverride.DirectOamOffsetGet = DirectOamOffsetGet;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalFhw);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalFhw16BitNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

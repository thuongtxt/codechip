/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFhwInternal.h
 *
 * Created Date: May 28, 2014
 *
 * Description : Indirect HAL for FHW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALFHWINTERNAL_H_
#define _ATHALFHWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "commacro.h"
#include "AtHalFhw.h"
#include "../../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalFhw * AtHalFhw;

typedef struct tAtHalFhw
    {
    tAtHalIndirectDefault super;
    }tAtHalFhw;

AtHal AtHalFhwObjectInit(AtHal self, uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALFHWINTERNAL_H_ */

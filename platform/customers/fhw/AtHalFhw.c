/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFhw.c
 *
 * Created Date: Jun 4, 2013
 *
 * Description : Concrete HAL class for FHW project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "AtHalFhwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalIndirectDefaultMethods m_AtHalIndirectDefaultOverride;

/* Save super implementation */
static const tAtHalIndirectDefaultMethods *m_AtHalIndirectDefaultMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalFhw);
    }

static void Setup(AtHal self)
    {
    AtHalIndirectDefaultAddressMaskSet(self, cBit24_0);
    AtHalIndirectDefaultIndirectControl1AddressSet(self, 0x702);
    AtHalIndirectDefaultIndirectControl2AddressSet(self, 0x703);
    AtHalIndirectDefaultIndirectData1AddressSet(self, 0x704);
    AtHalIndirectDefaultIndirectData2AddressSet(self, 0x705);
    }

static uint8 IsDirectRegister(AtHal self, uint32 address)
    {
    /* Inherit AtHalIndirectDefault methods */
    if (m_AtHalIndirectDefaultMethods->IsDirectRegister(self, address))
        return cAtTrue;

    /* Add more new registers */
    if ((address >= 0x700) && (address <= 0x701))
        return cAtTrue;

    return cAtFalse;
    }

static void AtHalIndirectDefaultOverride(AtHal self)
    {
    AtHalIndirectDefault defaultHal = (AtHalIndirectDefault)self;
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalIndirectDefaultMethods = defaultHal->methods;
        AtOsalMemCpy(&m_AtHalIndirectDefaultOverride, defaultHal->methods, sizeof(m_AtHalIndirectDefaultOverride));

        mMethodOverride(m_AtHalIndirectDefaultOverride, IsDirectRegister);
        }

    mMethodsSet(defaultHal, &m_AtHalIndirectDefaultOverride);
    }

static void Override(AtHal self)
    {
    AtHalIndirectDefaultOverride(self);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Default setup */
    Setup(self);

    m_methodsInit = 1;
    return self;
    }

AtHal AtHalFhwNew(uint32 baseAddress)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

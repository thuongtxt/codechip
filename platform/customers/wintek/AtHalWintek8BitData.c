/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalWintek8BitData.c
 *
 * Created Date: May 7, 2015
 *
 * Description : 8bit address, 8 bit data Wintek HAL implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Control registers */
#define cControl1 0xF2
#define cControl2 0xF3
#define cControl3 0xF4
#define cControl4 0xF5

/* Data registers */
#define cData1    0xF6
#define cData2    0xF7
#define cData3    0xF8
#define cData4    0xF9

#define cInvalidValue    0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/
#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

#define mThis(self) ((tAtHalWintek8BitData *)(self))

#define mIndirectDefault(self_) ((AtHalIndirectDefault)self_)
#define mDefaultHal(self_) ((AtHalDefault)self_)
#define mDirectWrite(self_, address_, value_) (self_)->methods->DirectWrite(self_, address_, value_)
#define mDirectRead(self_, address_) (self_)->methods->DirectRead(self_, address_)
#define mRealAddress(self, address) (AtSize)((address) + AtHalDefaultBaseAddress(self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalWintek8BitData
    {
    tAtHalIndirectDefault super;

    /* To store content of indirect 8 Bits registers */
    uint8 control1;
    uint8 control2;
    uint8 control3;
    uint8 control4;
    uint8 data1;
    uint8 data2;
    uint8 data3;
    uint8 data4;
    }tAtHalWintek8BitData;

/* Request types */
typedef enum eRequestHwType
    {
    cRequestHwRead,
    cRequestHwWrite
    }eRequestHwType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;
static tAtHalIndirectDefaultMethods m_AtHalIndirectDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;
static const tAtHalIndirectDefaultMethods *m_AtHalIndirectDefaultMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
AtHal AtHalWintek8BitDataNew(uint32 baseAddress);

/*--------------------------- Implementation ---------------------------------*/
static eBool HwDone(AtHal self, uint32 ctrlAddress, uint16 readyBitMask)
    {
    uint32 elapsedTime = 0;

    while (elapsedTime < mIndirectDefault(self)->retryTimes)
        {
        /* Done */
        if ((mDirectRead(self, ctrlAddress) & readyBitMask) == 0)
            {
            if (mIndirectDefault(self)->maxRetryTimes < elapsedTime)
                mIndirectDefault(self)->maxRetryTimes = elapsedTime;

            return cAtTrue;
            }

        /* Retry */
        elapsedTime = elapsedTime + 1;
        }

    return cAtFalse;
    }

static eBool Request(AtHal self, uint32 address, eRequestHwType request)
    {
    uint8 requestVal = 0;

    /* Write address */
    mDirectWrite(self, cControl1,  address & cBit7_0);
    mDirectWrite(self, cControl2, (address & cBit15_8)  >> 8);
    mDirectWrite(self, cControl3, (address & cBit23_16) >> 16);

    /* Write request type */
    if (request == cRequestHwRead)
        requestVal |= (uint8)cBit6;

    /* Start request */
    requestVal |= (uint8)cBit7;
    mDirectWrite(self, cControl4, requestVal);

    return HwDone(self, cControl4, cBit7);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    uint8 value = (uint8)(*(volatile uint8 *)mRealAddress(self, address));
    mAsm("sync");
    return value;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    *(volatile uint8*)(mRealAddress(self, address)) = (uint8)(value & cBit7_0);
    mAsm("sync");
    }

static uint32 Read(AtHal self, uint32 address)
    {
    /* Make a read request on this register */
    if (Request(self, address, cRequestHwRead))
        {
        uint32 data = 0;

        data |=  mDirectRead(self, cData1);
        data |= (mDirectRead(self, cData2) << 8);
        data |= (mDirectRead(self, cData3) << 16);
        data |= (mDirectRead(self, cData4) << 24);

        return data;
        }

    /* Timeout */
    return cInvalidValue;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    mDirectWrite(self, cData1, (uint8) (value & cBit7_0));
    mDirectWrite(self, cData2, (uint8)((value & cBit15_8)  >> 8));
    mDirectWrite(self, cData3, (uint8)((value & cBit23_16) >> 16));
    mDirectWrite(self, cData4, (uint8)((value & cBit31_24) >> 24));

    /* Make request */
    Request(self, address, cRequestHwWrite);
    }

static void StateRestore(AtHal self)
    {
    mDirectWrite(self, cData1, mThis(self)->data1);
    mDirectWrite(self, cData2, mThis(self)->data2);
    mDirectWrite(self, cData3, mThis(self)->data3);
    mDirectWrite(self, cData4, mThis(self)->data4);

    mDirectWrite(self, cControl1, mThis(self)->control1);
    mDirectWrite(self, cControl2, mThis(self)->control2);
    mDirectWrite(self, cControl3, mThis(self)->control3);
    mDirectWrite(self, cControl4, mThis(self)->control4);
    }

static void IndirectCtrlRegisterSave(AtHal self)
    {
    mThis(self)->control1 = (uint8)mDirectRead(self, cControl1);
    mThis(self)->control2 = (uint8)mDirectRead(self, cControl2);
    mThis(self)->control3 = (uint8)mDirectRead(self, cControl3);
    mThis(self)->control4 = (uint8)mDirectRead(self, cControl4);
    mThis(self)->data1    = (uint8)mDirectRead(self, cData1);
    mThis(self)->data2    = (uint8)mDirectRead(self, cData2);
    mThis(self)->data3    = (uint8)mDirectRead(self, cData3);
    mThis(self)->data4    = (uint8)mDirectRead(self, cData4);
    }

static uint8 IndirectIsInProgress(AtHal self)
    {
    uint32 requestStatus = mDirectRead(self, cControl4);
    return (requestStatus & cBit7) ? 1 : 0;
    }

static uint32 DataBusMask(AtHal self)
    {
    AtUnused(self);
    return cBit7_0;
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[7];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], cControl1);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], cControl2);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], cControl3);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[3], cData1);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[4], cData2);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[5], cData3);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[6], cData4);

    return regsToTest;
    }

static uint32 TestPattern(AtHal self)
    {
    AtUnused(self);
    return 0xAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
    AtUnused(self);
    return 0x55;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault hal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, hal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.RegsToTest      = RegsToTest;
        m_AtHalDefaultOverride.DataBusMask     = DataBusMask;
        m_AtHalDefaultOverride.TestPattern     = TestPattern;
        m_AtHalDefaultOverride.TestAntiPattern = TestAntiPattern;
        }

    hal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHalIndirectDefault(AtHal self)
    {
    AtHalIndirectDefault hal = (AtHalIndirectDefault)self;
    if (!m_methodsInit)
        {
        m_AtHalIndirectDefaultMethods = hal->methods;
        AtOsalMemCpy(&m_AtHalIndirectDefaultOverride, hal->methods, sizeof(m_AtHalIndirectDefaultOverride));

        m_AtHalIndirectDefaultOverride.IndirectIsInProgress     = IndirectIsInProgress;
        m_AtHalIndirectDefaultOverride.IndirectCtrlRegisterSave = IndirectCtrlRegisterSave;
        }

    hal->methods = &m_AtHalIndirectDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read         = Read;
        m_AtHalOverride.Write        = Write;
        m_AtHalOverride.DirectRead   = DirectRead;
        m_AtHalOverride.DirectWrite  = DirectWrite;
        m_AtHalOverride.StateRestore = StateRestore;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    OverrideAtHalIndirectDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalWintek8BitData);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalWintek8BitDataNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

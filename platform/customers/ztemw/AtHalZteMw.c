/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalZteMw.c
 *
 * Created Date: Jun 4, 2013
 *
 * Description : Concrete HAL class for ZTE PTN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalZteMw.h"
#include "../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Setup(AtHal self)
    {
    AtHalIndirectDefaultAddressMaskSet(self, cBit23_0);
    AtHalIndirectDefaultIndirectControl1AddressSet(self, 0x702);
    AtHalIndirectDefaultIndirectControl2AddressSet(self, 0x703);
    AtHalIndirectDefaultIndirectData1AddressSet(self, 0x704);
    AtHalIndirectDefaultIndirectData2AddressSet(self, 0x705);

    /* Let's enable when it is required */
    AtHalDefaultFailureDetectEnable(self, cAtFalse);
    }

AtHal AtHalZteMwNew(uint32 baseAddress)
    {
    AtHal newHal = AtHalIndirectDefaultNew(baseAddress);
    Setup(newHal);

    return newHal;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFhnSpi.c
 *
 * Created Date: Jan 13, 2013
 *
 * Description : Concrete HAL SPI for FHN project
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "AtHalFhnSpi.h"
#include "../../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((tAtHalFhnSpi *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalFhnSpi
    {
    tAtHalIndirectDefault super;

    /* Private data */
    SpiRead  directReadFunc;
    SpiWrite directWriteFunc;
    }tAtHalFhnSpi;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods                        m_AtHalOverride;
static tAtHalIndirectDefaultMethods         m_AtHalIndirectDefaultOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalFhnSpi);
    }

static void Setup(AtHal self)
    {
    AtHalIndirectDefaultAddressMaskSet(self, cBit23_0);
    AtHalIndirectDefaultIndirectControl1AddressSet(self, 0x2);
    AtHalIndirectDefaultIndirectControl2AddressSet(self, 0x3);
    AtHalIndirectDefaultIndirectData1AddressSet(self, 0x4);
    AtHalIndirectDefaultIndirectData2AddressSet(self, 0x5);
    }

static uint8 IsDirectRegister(AtHal self, uint32 address)
    {
	AtUnused(self);
    return((address >= 0x2) && (address <= 0x5)) ? cAtTrue : cAtFalse;
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    uint16 value;
    mThis(self)->directReadFunc((uint8)address, &value);
    return value;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mThis(self)->directWriteFunc((uint8)address, (uint16)value);
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        AtOsalMemCpy(&m_AtHalOverride, self->methods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.DirectRead         = DirectRead;
        m_AtHalOverride.DirectWrite        = DirectWrite;
        }

    self->methods = &m_AtHalOverride;
    }

static void AtHalIndirectDefaultOverride(AtHal self)
    {
    AtHalIndirectDefault defaultHal = (AtHalIndirectDefault)self;
    if (!m_methodsInit)
        {
        /* Save super implementation */
        AtOsalMemCpy(&m_AtHalIndirectDefaultOverride, defaultHal->methods, sizeof(m_AtHalIndirectDefaultOverride));

        mMethodOverride(m_AtHalIndirectDefaultOverride, IsDirectRegister);
        }

    mMethodsSet(defaultHal, &m_AtHalIndirectDefaultOverride);
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    AtHalIndirectDefaultOverride(self);
    }

static AtHal ObjectInit(AtHal self, SpiRead spiReadFunc, SpiWrite spiWriteFunc)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalIndirectDefaultObjectInit(self, 0) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Default setup */
    Setup(self);

    /* Private data */
    mThis(self)->directReadFunc  = spiReadFunc;
    mThis(self)->directWriteFunc = spiWriteFunc;

    m_methodsInit = 1;
    return self;
    }

AtHal AtHalFhnSpiNew(SpiRead spiReadFunc, SpiWrite spiWriteFunc)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());

    /* Construct it */
    return ObjectInit(newHal, spiReadFunc, spiWriteFunc);
    }

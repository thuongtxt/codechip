/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalFhn.c
 *
 * Created Date: Apr 23, 2013
 *
 * Description : Concrete HAL for FHN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalFhn.h"
#include "../hal/AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtHal AtHalFhnNew(uint32 baseAddress)
    {
    AtHal hal = AtHalIndirectDefaultNew(baseAddress);

    /* Let's enable when it is required */
    AtHalDefaultFailureDetectEnable(hal, cAtFalse);

    return hal;
    }

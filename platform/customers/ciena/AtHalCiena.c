/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtHalCiena.c
 *
 * Created Date: Nov 7, 2016
 *
 * Description : Ciena specific HAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "AtHalCiena.h"
#include "../hal/AtHalDefaultWithHandlerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cStartRegAddress 0xF0004A
#define cStopRegAddress  0xF0004F
#define cNumRegs         (cStopRegAddress - cStartRegAddress + 1)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalCiena
    {
    tAtHalDefaultWithHandler super;
    }tAtHalCiena;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    uint32 reg_i;
    static tAtHalTestRegister regsToTest[cNumRegs];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    for (reg_i = 0; reg_i < cNumRegs; reg_i++)
        AtHalDefaultTestRegisterSetup(self, &regsToTest[reg_i], cStartRegAddress + reg_i);

    return regsToTest;
    }

static eBool NeedRestoreTestRegValues(AtHal self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        mMethodOverride(m_AtHalDefaultOverride, RegsToTest);
        mMethodOverride(m_AtHalDefaultOverride, NeedRestoreTestRegValues);
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHalDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalCiena);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultWithHandlerObjectInit(self, baseAddress, writeHandler, readHandler) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalCienaNew(uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress, writeHandler, readHandler);
    }

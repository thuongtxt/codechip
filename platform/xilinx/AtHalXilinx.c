/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalXilinx.c
 *
 * Created Date: May 27, 2015
 *
 * Description : HAL to work on XILINX platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalXilinx.h"
#include "atclib.h"
#include "AtOsal.h"
#include "commacro.h"
#include "../ep5/AtHalEp5Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalXilinx
    {
    tAtHalEp5 super;
    }tAtHalXilinx;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Override(AtHal self)
    {
    /* TODO: may add code in the future */
    AtUnused(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalXilinx);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalEp5ObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalXilinxNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

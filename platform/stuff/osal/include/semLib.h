/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : semLib.h
 * 
 * Created Date: Dec 11, 2013
 *
 * Description : To make VxWorks OSAL be compiled successfully
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _SEMLIB_H_
#define _SEMLIB_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef void *  SEM_ID;

typedef enum
    {
    SEM_EMPTY,
    SEM_FULL
    } SEM_B_STATE;

typedef int STATUS;

#define NO_WAIT     0
#define WAIT_FOREVER    (-1)

#define SEM_Q_FIFO         0x00   /* first in first out queue */
#define SEM_Q_PRIORITY     0x1
#define SEM_DELETE_SAFE    0x4
#define SEM_INVERSION_SAFE 0x8

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
SEM_ID semBCreate(int options, SEM_B_STATE initialState);
SEM_ID semMCreate(int options);
STATUS semDelete(SEM_ID semId);
STATUS semTake(SEM_ID semId, int timeout);
STATUS semGive(SEM_ID semId);

#endif /* _SEMLIB_H_ */


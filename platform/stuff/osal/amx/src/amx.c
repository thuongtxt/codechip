/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : amx.c
 *
 * Created Date: May 9, 2014
 *
 * Description : Just to pass linking
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cjzzz.h"

/*--------------------------- Define -----------------------------------------*/
#define CJ_UNUSED(x) 	(void)(x)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
CJ_ERRST CJ_CCPP    cjmmcreate(CJ_ID *mpidp, char *tag, void *mp, unsigned long msize)
    {
	CJ_UNUSED(tag);
	CJ_UNUSED(mp);
	CJ_UNUSED(msize);
    *mpidp = 1;
    return CJ_EROK;
    }

CJ_ERRST CJ_CCPP    cjmmget(CJ_ID id, unsigned long size, void *mempp, unsigned long *sizep)
    {
	CJ_UNUSED(id);
	CJ_UNUSED(sizep);
    *((char**)mempp) = malloc(size);
    return CJ_EROK;
    }

CJ_ERRST CJ_CCPP    cjmmfree(void *mp)
    {
    free(mp);
    return CJ_EROK;
    }

CJ_TIME CJ_CCPP cjtmconvert(unsigned long ms)
    {
	CJ_UNUSED(ms);
    return 0;
    }

CJ_ERRST CJ_CCPP    cjtkdelay(CJ_TIME tmr)
    {
	CJ_UNUSED(tmr);
    return CJ_EROK;
    }

CJ_T32U CJ_CCPP     cjtmtick(void)
    {
    return 0;
    }

CJ_ERRST CJ_CCPP    cjsmdelete(CJ_ID id)
    {
	CJ_UNUSED(id);
    return CJ_EROK;
    }

CJ_ERRST CJ_CCPP    cjsmwait(CJ_ID id, int pr, CJ_TIME tmr)
    {
	CJ_UNUSED(id);
	CJ_UNUSED(pr);
	CJ_UNUSED(tmr);
    return CJ_EROK;
    }

CJ_ERRST CJ_CCPP    cjsmsignal(CJ_ID id)
    {
	CJ_UNUSED(id);
    return CJ_EROK;
    }

CJ_ERRST CJ_CCPP    cjsmcreate(CJ_ID *smidp, char *tag, int v)
    {
	CJ_UNUSED(smidp);
	CJ_UNUSED(tag);
	CJ_UNUSED(v);
    return CJ_EROK;
    }

CJ_ERRST CJ_CCPP    cjtkkill(CJ_ID taskid)
    {
	CJ_UNUSED(taskid);
    return CJ_EROK;
    }

CJ_ID CJ_CCPP cjtkid(void)
    {
    return 0;
    }

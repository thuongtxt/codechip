/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : cjzzz.h
 * 
 * Created Date: May 9, 2014
 *
 * Description : AMX stuff
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CJZZZ_H_
#define _CJZZZ_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef unsigned int CJ_ID;     /* General Object id        */
#define CJ_IDNULL 0         /* Null id          */

#define CJ_ERRST int  /* Error status     */
#define CJ_CCPP       /* C param passing keyword not supported*/
typedef long CJ_TIME;           /* Timer interval       */

typedef unsigned long CJ_T32U;          /* unsigned 32-bit int  */
typedef CJ_T32U CJ_TYTAG;           /* Private tags     */
typedef struct cjxtag {char xtag[2*sizeof(CJ_TYTAG)]; } CJ_TAGDEF;

/* Semaphore Definition Structure                   */
struct cjxsmdef {
    CJ_TAGDEF   xsmdtag;    /* Semaphore tag        */
    int     xsmdvalue;  /* Initial semaphore value  */
#if (CJ_CCISIZE == 2)
    CJ_T16      xsmdrsv;    /* Reserved for alignment   */
#endif
    };

struct cjxmpdef {
    CJ_TAGDEF   xmpdtag;    /* Memory pool tag      */
    void        *xmpdmemp;  /* Memory pointer       */
    unsigned long   xmpdmsize;  /* Size of memory       */
    };

/* Semaphore Status Block Structure                 */

struct cjxsmsts {
    CJ_TYTAG    xsmstag;    /* Semaphore tag        */
    int     xsmsvalue;  /* Semaphore initial value  */
    int     xsmscount;  /* Semaphore value      */
                    /* >0 free          */
                    /* 0  owned         */
                    /* -n owned; n tasks waiting    */
    int     xsmsnest;   /* Resource nest level      */
    CJ_ID       xsmsowner;  /* Task id of resource owner    */
                    /* CJ_IDNULL if not a resource  */
    };

#define CJ_EROK     0   /* Call successful          */

/* Semaphore Manager Services */
CJ_ERRST CJ_CCPP    cjsmbuild(CJ_ID *smidp, struct cjxsmdef *smdefp);
CJ_ERRST CJ_CCPP    cjsmcreate(CJ_ID *smidp, char *tag, int v);
CJ_ERRST CJ_CCPP    cjsmdelete(CJ_ID id);
CJ_ERRST CJ_CCPP    cjsmrls(CJ_ID id, int n);
CJ_ERRST CJ_CCPP    cjsmsignal(CJ_ID id);
CJ_ERRST CJ_CCPP    cjsmstatus(CJ_ID id, struct cjxsmsts *stsp);
CJ_ERRST CJ_CCPP    cjsmwait(CJ_ID id, int pr, CJ_TIME tmr);

/* Memory Manager Services                      */
CJ_ERRST CJ_CCPP    cjmmbuild(CJ_ID *mpidp, struct cjxmpdef *mpdefp);
CJ_ERRST CJ_CCPP    cjmmcreate(CJ_ID *mpidp, char *tag,
                void *mp, unsigned long msize);
CJ_ERRST CJ_CCPP    cjmmdelete(CJ_ID id);
CJ_ERRST CJ_CCPP    cjmmfree(void *mp);
CJ_ERRST CJ_CCPP    cjmmget(CJ_ID id, unsigned long size,
                void *mempp, unsigned long *sizep);
CJ_ERRST CJ_CCPP    cjmmid(CJ_ID *mpidp, void *mp);
CJ_ERRST CJ_CCPP    cjmmresize(void *mp, unsigned long size);
CJ_ERRST CJ_CCPP    cjmmsection(CJ_ID id, void *mp, unsigned long size);
CJ_ERRST CJ_CCPP    cjmmsize(void *mp, unsigned long *sizep);
CJ_ERRST CJ_CCPP    cjmmuse(void *mp, int cnt);

CJ_TIME CJ_CCPP     cjtmconvert(unsigned long ms);
CJ_ERRST CJ_CCPP    cjtkdelay(CJ_TIME tmr);
CJ_T32U CJ_CCPP     cjtmtick(void);
CJ_ERRST CJ_CCPP    cjtkkill(CJ_ID taskid);
CJ_ID CJ_CCPP cjtkid(void);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _CJZZZ_H_ */


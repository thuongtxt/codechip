/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : semLib.c
 *
 * Created Date: Dec 11, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "semLib.h"
#include <stdlib.h>

/*--------------------------- Define -----------------------------------------*/
#define VxWUnused(x)	(void)(x)
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef int clockid_t;
struct timespec;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void* sys_mem_malloc(size_t size);
void sys_mem_free(void *ptr);

/*--------------------------- Implementation ---------------------------------*/
SEM_ID semBCreate(int options, SEM_B_STATE initialState)
    {
	VxWUnused(options);
	VxWUnused(initialState);
    return NULL;
    }

SEM_ID semMCreate(int options)
    {
	VxWUnused(options);
    return NULL;
    }

STATUS semDelete(SEM_ID semId)
    {
	VxWUnused(semId);
    return 0;
    }

STATUS semTake(SEM_ID semId, int timeout)
    {
	VxWUnused(semId);
	VxWUnused(timeout);
    return 0;
    }

STATUS semGive(SEM_ID semId)
    {
	VxWUnused(semId);
    return 0;
    }

/*int clock_gettime (clockid_t clock_id, struct timespec *tp)
    {
	VxWUnused(clock_id);
	VxWUnused(tp);
    return 0;
    }*/

void* sys_mem_malloc(size_t size)
    {
    return malloc(size);
    }

void sys_mem_free(void *ptr)
    {
    free(ptr);
    }

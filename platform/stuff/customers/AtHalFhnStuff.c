/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtHalFhnStuff.c
 *
 * Created Date: Jan 21, 2015
 *
 * Description : Stuff function to pass compiling
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
int spi_read_stm1(uint8 addr,uint16 *buf);
int spi_write_stm1(uint8 addr,uint16 data);

/*--------------------------- Implementation ---------------------------------*/
int spi_read_stm1(uint8 addr,uint16 *buf)
    {
	AtUnused(buf);
	AtUnused(addr);
    return 0xDEAD;
    }

int spi_write_stm1(uint8 addr,uint16 data)
    {
	AtUnused(data);
	AtUnused(addr);
    return 0xDEAD;
    }

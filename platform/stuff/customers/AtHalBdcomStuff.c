/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtHalBdcomStuff.c
 *
 * Created Date: Dec 11, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
uint8 fpgaex_read_fpga(int slot, uint16 reg);
int fpgaex_write_fpga(int slot, uint16 reg, uint8 value);

/*--------------------------- Implementation ---------------------------------*/
uint8 fpgaex_read_fpga(int slot, uint16 reg)
    {
	AtUnused(reg);
	AtUnused(slot);
    return 0;
    }

int fpgaex_write_fpga(int slot, uint16 reg, uint8 value)
    {
	AtUnused(value);
	AtUnused(reg);
	AtUnused(slot);
    return 0;
    }

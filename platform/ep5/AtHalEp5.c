/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalEp5.c
 *
 * Created Date: May 28, 2014
 *
 * Description : HAL to work on EP5
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "commacro.h"
#include "AtHalEp5Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalEp5);
    }

AtHal AtHalEp5ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalEp5New(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return AtHalEp5ObjectInit(newHal, baseAddress);
    }

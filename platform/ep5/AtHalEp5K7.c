/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalEp5K7.c
 *
 * Created Date: May 28, 2014
 *
 * Description : HAL to work on EP5-K7
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "commacro.h"
#include "AtHalEp5K7Internal.h"

/*--------------------------- Define -----------------------------------------*/
#define mBaseAddress(self) (((AtHalDefault)self)->baseAddress)
#define mRealAddress(self, address) ((((address) & cBit26_0) << 2) + mBaseAddress(self))

#define cEp5K7HalAddressBit25 0xF00042

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void K7ReadWriteBit25(AtHal self, uint32 address)
    {
    if (address != cEp5K7HalAddressBit25)
        *((uint32 *)mRealAddress(self, cEp5K7HalAddressBit25)) = address & cBit24 ? 1 : 0;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    /* Process for bit 25 */
    K7ReadWriteBit25(self, address);

    m_AtHalMethods->Write(self, address, value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    /* Process for bit 25 */
    K7ReadWriteBit25(self, address);

    return m_AtHalMethods->Read(self, address);
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Read               = Read;
        m_AtHalOverride.Write              = Write;
        m_AtHalOverride.DirectRead         = Read;
        m_AtHalOverride.DirectWrite        = Write;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalEp5K7);
    }

AtHal AtHalEp5K7ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    /* Setup class */
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalEp5K7New(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return AtHalEp5K7ObjectInit(newHal, baseAddress);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalEp5K7Internal.h
 * 
 * Created Date: Jul 5, 2015
 *
 * Description : HAL to work on EP5
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _PLATFORM_EP5K7_ATHALEP5INTERNAL_H_
#define _PLATFORM_EP5K7_ATHALEP5INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../hal/AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalEp5K7
    {
    tAtHalDefault super;
    }tAtHalEp5K7;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalEp5K7ObjectInit(AtHal self, uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _PLATFORM_EP5K7_ATHALEP5INTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHalBdcom.h
 * 
 * Created Date: Dec 10, 2013
 *
 * Description : Concrete HAL to work with BDCOM platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALBDCOM_H_
#define _ATHALBDCOM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h" /* Super class */
#include "AtHalBdcomMdio.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef uint8 (*FpgaRead)(uint8 slot, uint16 reg);
typedef uint8 (*FpgaWrite)(uint8 slot, uint16 reg, uint8 value);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalBdcomNew(uint8 slotId, uint8 pageId, FpgaRead readFunc, FpgaWrite writeFunc);
AtHal AtHalBdcomMdio(AtHal self);

/* Debug */
AtHal AtHalBdcomSimNew(uint8 slot, uint8 page);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALBDCOM_H_ */


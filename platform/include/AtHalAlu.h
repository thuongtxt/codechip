/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalAlu.h
 * 
 * Created Date: Aug 8, 2014
 *
 * Description : Hal for ALU
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALALU_H_
#define _ATHALALU_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Direct access functions which depend on specific platform */
typedef void (*WriteFunc)(uint32 realAddress, uint32 value);
typedef uint32 (*ReadFunc)(uint32 realAddress);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalAluNew(uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALALU_H_ */


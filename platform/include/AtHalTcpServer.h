/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalTcpServer.h
 * 
 * Created Date: Feb 7, 2015
 *
 * Description : HAL TCP server
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALTCPSERVER_H_
#define _ATHALTCPSERVER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConnection.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalTcpServerNew(uint16 registerAccessPort, uint16 eventNotifyPort, AtHal realHal);

eAtRet AtHalTcpServerRun(AtHal self);
AtHal AtHalTcpServerRealHal(AtHal self);

eAtRet AtHalTcpServerInterruptEventSend(AtHal self);
eBool AtHalTcpServerInterruptEventCanSend(AtHal self);

/* Connections */
AtConnection AtHalTcpServerAccessConnection(AtHal self);
AtConnection AtHalTcpServerEventConnection(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALTCPSERVER_H_ */


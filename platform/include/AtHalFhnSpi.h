/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalFhnSpi.h
 * 
 * Created Date: Jan 13, 2015
 *
 * Description : Concrete HAL for FHN SPI project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALFHNSPI_H_
#define _ATHALFHNSPI_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef int (*SpiRead)(uint8 addr, uint16 *buf);
typedef int (*SpiWrite)(uint8 addr, uint16 data);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalFhnSpiNew(SpiRead spiReadFunc, SpiWrite spiWriteFunc);

#endif /* _ATHALFHNSPI_H_ */


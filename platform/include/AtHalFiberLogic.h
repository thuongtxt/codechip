/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalFiberLogic.h
 * 
 * Created Date: Apr 3, 2015
 *
 * Description : Fiber Logic HAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALFIBERLOGIC_H_
#define _ATHALFIBERLOGIC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalFiberLogicNew(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALFIBERLOGIC_H_ */


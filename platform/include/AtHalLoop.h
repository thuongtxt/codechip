/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalLoop.h
 * 
 * Created Date: Feb 24, 2014
 *
 * Description : HAL for LOOP projects. Usage:
 *               - AtHalLoopNew: Create HAL object to be used for this project.
 *                 After this object is created, it has following associated HALs:
 *                 + AtHalLoopGeSerdes: To control GE SERDES
 *                 + AtHalLoopLiu: To control LIU
 *----------------------------------------------------------------------------*/
#ifndef _ATHALLOOP_H_
#define _ATHALLOOP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef int (*FpgaRead) (unsigned long address, unsigned long *value);
typedef int (*FpgaWrite)(unsigned long address, unsigned long value);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalLoopNew(FpgaRead readFunc, FpgaWrite writeFunc);
AtHal AtHalLoopSerdesNew(AtHal hal, uint8 portId);
AtHal AtHalLoopSerdesStmNew(AtHal hal, uint8 portId);
AtHal AtHalLoopLiuNew(AtHal hal);

AtHal AtHalLoopGeSerdes(AtHal self, uint8 ethPortId);
AtHal AtHalLoopLiu(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALLOOP_H_ */


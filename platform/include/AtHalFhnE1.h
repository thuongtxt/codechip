/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalFhnE1.h
 * 
 * Created Date: Jan 21, 2015
 *
 * Description : Concrete HAL for FHN E1 PW project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALFHNE1_H_
#define _ATHALFHNE1_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalFhnE1New(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALFHNE1_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalAls.h
 * 
 * Created Date: Dec 10, 2013
 *
 * Description : HAL for ALS project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALALS_H_
#define _ATHALALS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalAlsNew(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALALS_H_ */


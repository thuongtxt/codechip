/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHalXilinx.h
 * 
 * Created Date: May 27, 2015
 *
 * Description : HAL that work on XILINX platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALXILINX_H_
#define _ATHALXILINX_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalXilinxNew(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALXILINX_H_ */


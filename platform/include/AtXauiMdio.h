/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module HAL
 * 
 * File        : AtXauiMdio.h
 * 
 * Created Date: Nov 26, 2014
 *
 * Description : Xaui access functions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATXAUIMDIO_H_
#define _ATXAUIMDIO_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "commacro.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtXauiMdioWrite(AtHal hal, uint32 page, uint32 address, uint32 value);
uint32 AtXauiMdioRead(AtHal hal, uint32 page, uint32 address);

eAtRet AtXauiReset(AtHal hal, eBool reset);
uint32 AtXauiStatusRead2Clear(AtHal hal, eBool clear);

#endif /* _ATXAUIMDIO_H_ */


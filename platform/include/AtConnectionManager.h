/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IP
 * 
 * File        : AtConnectionManager.h
 * 
 * Created Date: Feb 12, 2015
 *
 * Description : Abstract connection manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONNECTIONMANAGER_H_
#define _ATCONNECTIONMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConnectionClasses.h"
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Connection factory */
AtConnection AtConnectionManagerServerConnectionCreate(AtConnectionManager self, const char *bindAddress, uint16 port);
AtConnection AtConnectionManagerClientConnectionCreate(AtConnectionManager self, const char *ipAddress, uint16 port);

/* Network utils */
uint32 AtConnectionManagerHtonl(AtConnectionManager self, uint32 hostlong);
uint32 AtConnectionManagerNtohl(AtConnectionManager self, uint32 netlong);

/* Built-in connection manager */
AtConnectionManager AtConnectionManagerDefault(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATCONNECTIONMANAGER_H_ */


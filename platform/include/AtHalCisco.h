/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalCisco.h
 * 
 * Created Date: Apr 10, 2015
 *
 * Description : HAL for Cisco projects
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALCISCO_H_
#define _ATHALCISCO_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Direct access functions which depend on specific platform */
typedef void (*WriteFunc)(uint32 realAddress, uint32 value);
typedef uint32 (*ReadFunc)(uint32 realAddress);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalCiscoNew(uint32 baseAddress, WriteFunc writeFunc, ReadFunc readFunc);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALCISCO_H_ */


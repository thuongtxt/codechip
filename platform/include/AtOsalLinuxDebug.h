/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalLinuxDebug.h
 * 
 * Created Date: Jun 13, 2013
 *
 * Description : OSAL debug version for Linux
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALLINUXDEBUG_H_
#define _ATOSALLINUXDEBUG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "../osal/debug/AtOsalLeakTracker.h"
#include "../osal/debug/AtOsalStackTracer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsal AtOsalLinuxDebug(void);
AtOsal AtOsalLinuxTestbench(void);
void AtOsalLinuxDebugCleanUp(AtOsal self);

AtOsalLeakTracker AtOsalLinuxDebugLeakTracker(AtOsal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALLINUXDEBUG_H_ */


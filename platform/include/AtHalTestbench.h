/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHalTestbench.h
 * 
 * Created Date: Sep 20, 2016
 *
 * Description : Testbench HAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALTESTBENCH_H_
#define _ATHALTESTBENCH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete */
AtHal AtHalTestbenchTcpNew(const char *serverIpAddress, uint16 connectedPort);
AtHal AtHalTestbenchNew(uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALTESTBENCH_H_ */


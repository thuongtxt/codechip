/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalZteRouter.h
 *
 * Created Date: May 20, 2015
 *
 * Description : HAL for ZTE PTN project
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATHALZTEROUTER_H_
#define _ATHALZTEROUTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete HALs */
AtHal AtHalZteRouterNew(uint32 baseAddress);
AtHal AtHalZteRouterStmNew(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALZTEROUTER_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtOsal.h
 * 
 * Created Date: Aug 1, 2012
 *
 * Description : OS Abstraction Layer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _OSAL_H_
#define _OSAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtStd.h"
#include "AtConnectionManager.h"
#include "attypes.h"
#include <stdarg.h>
#include "AtTaskManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtOsal
 * @{
 */
/**
 * @brief OS Abstraction Layer class
 */
typedef struct tAtOsal * AtOsal;

/**
 * @brief OSAL return code
 */
typedef enum eAtOsalRet
    {
    cAtOsalOk,     /**< Success */

    /* Other return codes go here */

    cAtOsalFailed  /**< Fail */
    }eAtOsalRet;

/**
 * @brief Osal Trace Level
 */
typedef enum eAtOsalTraceLevel
    {
    cAtOsalTraceEmerg = 0, /**< Emergency Trace Level */
    cAtOsalTraceAlert,     /**< Alert Trace Level */
    cAtOsalTraceCrit,      /**< Critical Trace Level */
    cAtOsalTraceError,     /**< Error Trace Level */
    cAtOsalTraceWarning,   /**< Warning Trace Level */
    cAtOsalTraceNotice,    /**< Notice Trace Level */
    cAtOsalTraceInfo,      /**< Info Trace Level */
    cAtOsalTraceDebug,     /**< Debug Trace Level */
    cAtOsalTraceMax        /**< Max Trace Level */
    } eAtOsalTraceLevel;

/**
 * @brief Semaphore class
 */
typedef struct tAtOsalSem * AtOsalSem;

/**
 * @brief Mutex class
 */
typedef struct tAtOsalMutex * AtOsalMutex;

/**
 * @}
 */

/**
 * @addtogroup AtOsalTime
 * @{
 */
/** @brief The current time structure */
typedef struct tAtOsalCurTime
    {
    uint32 sec;  /**< Seconds */
    uint32 usec; /**< Microseconds */
    }tAtOsalCurTime;
/**
 * @}
 */

/**
 * @addtogroup AtOsalSem
 * @{
 */
/** @brief Semaphore methods */
typedef struct tAtOsalSemMethods
    {
    eAtOsalRet (*Delete)(AtOsalSem self);  /**< Delete */
    eAtOsalRet (*Take)(AtOsalSem self);    /**< Take */
    eAtOsalRet (*TryTake)(AtOsalSem self); /**< Try take */
    eAtOsalRet (*Give)(AtOsalSem self);    /**< Give */
    AtOsal (*OsalGet)(AtOsalSem self);     /**< OSAL that creates this semaphore */
    }tAtOsalSemMethods;

/** @brief Semaphore class representation */
typedef struct tAtOsalSem
    {
    const tAtOsalSemMethods *methods; /**< Methods */

    /* Private */
    AtOsal osal; /**< OSAL */
    }tAtOsalSem;

/**
 * @}
 */

/**
 * @addtogroup AtOsalMutex
 * @{
 */

/** @brief Mutex methods */
typedef struct tAtOsalMutexMethods
    {
    eAtOsalRet (*Delete)(AtOsalMutex self);  /**< Delete */
    eAtOsalRet (*Lock)(AtOsalMutex self);    /**< Lock */
    eAtOsalRet (*TryLock)(AtOsalMutex self); /**< Try lock */
    eAtOsalRet (*UnLock)(AtOsalMutex self);  /**< Unlock */
    AtOsal (*OsalGet)(AtOsalMutex self);     /**< OSAL that creates this mutex */
    }tAtOsalMutexMethods;

/**< Mutex class representation */
typedef struct tAtOsalMutex
    {
    const tAtOsalMutexMethods *methods; /**< Methods */

    /* Private data */
    AtOsal osal; /**< OSAL */
    }tAtOsalMutex;

/**
 * @}
 */

/**
 * @addtogroup AtOsal
 * @{
 */

/** @brief OSAL methods */
typedef struct tAtOsalMethods
    {
    /* Memory operations */
    void* (*MemAlloc)(AtOsal self, uint32 size); /**< Allocate memory */
    void (*MemFree)(AtOsal self, void* pMem);    /**< Free memory */
    void* (*MemInit)(AtOsal self, void* pMem, uint32 val, uint32 size);  /**< Initialize memory */
    void* (*MemCpy)(AtOsal self, void *pDest, const void *pSrc, uint32 size);  /**< Copy memory */
    void* (*MemMove)(AtOsal self, void *pDest, const void *pSrc, uint32 size);  /**< Move memory */
    int (*MemCmp)(AtOsal self, const void *pMem1, const void *pMem2, uint32 size); /**< Compare memory */

    /* Semaphore */
    AtOsalSem (*SemCreate)(AtOsal self, eBool procEn, uint32 initVal);   /**< Create semaphore */

    /* Mutex */
    AtOsalMutex (*MutexCreate)(AtOsal self); /**< Create mutex */

    /* Delay functions */
    void (*Sleep)(AtOsal self, uint32 timeToSleep);  /**< Sleep in seconds */
    void (*USleep)(AtOsal self, uint32 timeToSleep); /**< Sleep in microseconds */

    /* Time util */
    void (*CurTimeGet)(AtOsal self, tAtOsalCurTime *currentTime); /**< Get current time */
    uint32 (*DifferenceTimeInMs)(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime); /**< Calculate different time in miliseconds */
    uint32 (*DifferenceTimeInUs)(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime); /**< Calculate different time in miliseconds */

    /* Util */
    void (*Panic)(AtOsal self); /**< Crash */

    /* Manage time */
    const char* (*DateTimeGet)(AtOsal self);      /**< Get current time in string */
    const char* (*DateTimeInUsGet)(AtOsal self);  /**< Get current time in string */

    /* Managers */
    eBool (*IsMultitasking)(AtOsal self); /**< To determine if OSAL is multitasking environment */
    AtTaskManager (*TaskManagerGet)(AtOsal self); /**< Task manager */
    AtConnectionManager (*ConnectionManager)(AtOsal self); /**< Connection manager */
    AtStd (*Std)(AtOsal self); /**< Get C standard library abstraction */
    }tAtOsalMethods;

/** @brief OS Abstraction Layer interfaces */
typedef struct tAtOsal
    {
    const tAtOsalMethods *methods; /**< Methods */
    }tAtOsal;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Supported OSALs */
AtOsal AtOsalLinux(void);
AtOsal AtOsalVxWorks(void);
AtOsal AtOsalAmx(void);
AtOsal AtOsalWindows(void);

/* OSAL singleton object */
AtOsal AtOsalSharedGet(void);
eAtOsalRet AtOsalSharedSet(AtOsal osal);

/* Memory operations */
void* AtOsalMemAlloc(uint32 size);
void * AtOsalMemRealloc(void *oldPtr, uint32 oldSize, uint32 newSize);
void AtOsalMemFree(void* pMem);
void* AtOsalMemInit(void* pMem, uint32 val, uint32 size);
void* AtOsalMemCpy(void *pDest, const void *pSrc, uint32 size);
void* AtOsalMemMove(void *pDest, const void *pSrc, uint32 size);
int AtOsalMemCmp(const void *pMem1, const void *pMem2, uint32 size);

/* Semaphore */
AtOsalSem AtOsalSemCreate(atbool procEn, uint32 initVal);
eAtOsalRet AtOsalSemDestroy(AtOsalSem sem);
eAtOsalRet AtOsalSemTake(AtOsalSem sem);
eAtOsalRet AtOsalSemTryTake(AtOsalSem sem);
eAtOsalRet AtOsalSemGive(AtOsalSem sem);

/* Mutex */
AtOsalMutex AtOsalMutexCreate(void);
eAtOsalRet AtOsalMutexDestroy(AtOsalMutex mutex);
eAtOsalRet AtOsalMutexTryLock(AtOsalMutex mutex);
eAtOsalRet AtOsalMutexLock(AtOsalMutex mutex);
eAtOsalRet AtOsalMutexUnLock(AtOsalMutex mutex);

/* Delay functions */
void AtOsalSleep(uint32 timeToSleep);
void AtOsalUSleep(uint32 timeToSleep);

/* System time */
void AtOsalCurTimeGet(tAtOsalCurTime *currentTime);
uint32 AtOsalDifferenceTimeInMs(const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime);
uint32 AtOsalDifferenceTimeInUs(const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime);

/* Trace */
void AtOsalTraceRedirectSet(void (*redirect_func)(const char *level, const char *fmt, va_list args));
void AtOsalTrace(eAtOsalTraceLevel traceLevel, const char *fmt, ...) AtAttributePrintf(2, 3);

/* Internal constructors */
AtOsalMutex AtOsalMutexObjectInit(AtOsalMutex self, AtOsal osal);
AtOsalSem AtOsalSemObjectInit(AtOsalSem self, AtOsal osal);
AtStd AtOsalStd(void);

void AtOsalPanic(void);
eBool AtOsalIsMultitasking(void);
AtTaskManager AtOsalTaskManagerGet(void);
AtConnectionManager AtOsalConnectionManager(void);
const char* AtOsalDateTimeGet(void);
const char* AtOsalDateTimeInUsGet(void);

#ifdef __cplusplus
}
#endif
#endif /* _OSAL_H_ */


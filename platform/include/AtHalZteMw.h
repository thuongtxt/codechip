/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalZteMw.h
 * 
 * Created Date: Mar 10, 2014
 *
 * Description : HAL for ZTE MW project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALZTEMW_H_
#define _ATHALZTEMW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalZteMwNew(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALZTEMW_H_ */


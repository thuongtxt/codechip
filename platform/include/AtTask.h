/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtTask.h
 * 
 * Created Date: Jun 25, 2015
 *
 * Description : Abstract OS task
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTASK_H_
#define _ATTASK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTask * AtTask;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char *AtTaskNameGet(AtTask self);
void *AtTaskUserDataGet(AtTask self);

/* Task operations */
eAtRet AtTaskStart(AtTask self);
eAtRet AtTaskStop(AtTask self);
eAtRet AtTaskJoin(AtTask self);
void AtTaskTestCancel(AtTask self);

/* Concretes (private used only) */
AtTask AtLinuxTaskNew(const char *taskName, void *userData, void *(*taskHandler)(void *userData));

#ifdef __cplusplus
}
#endif
#endif /* _ATTASK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IP
 * 
 * File        : AtConnection.h
 * 
 * Created Date: Feb 12, 2015
 *
 * Description : Abstract connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONNECTION_H_
#define _ATCONNECTION_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConnectionClasses.h"
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtConnectionNoTimeout 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtConnectionRet
    {
    cAtConnectionOk,
    cAtConnectionErrorCreateFail,
    cAtConnectionErrorAcceptFail,
    cAtConnectionErrorListenFail,
    cAtConnectionErrorBindFail,
    cAtConnectionErrorInvalidAddress,
    cAtConnectionError
    }eAtConnectionRet;

typedef enum eAtConnectionTransport
    {
    cAtConnectionTransportTcp,
    cAtConnectionTransportUdp
    }eAtConnectionTransport;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtConnectionTransport AtConnectionTransportGet(AtConnection self);
eAtConnectionRet AtConnectionDelete(AtConnection self);

eAtConnectionRet AtConnectionSetup(AtConnection self);

int AtConnectionSend(AtConnection self, const char *dataBuffer, uint32 numBytes);
int AtConnectionReceive(AtConnection self);
eAtConnectionRet AtConnectionReceiveTimeoutSet(AtConnection self, uint32 receiveTimeoutInSeconds);
uint32 AtConnectionReceiveTimeoutGet(AtConnection self);
char *AtConnectionDataBuffer(AtConnection self);
uint32 AtConnectionDataBufferSize(AtConnection self);

const char *AtConnectionRemoteAddressGet(AtConnection self);
uint16 AtConnectionConnectedPortGet(AtConnection self);

void AtConnectionDebug(AtConnection self);
const char *AtConnectionErrorDescription(AtConnection self);

/* For server connection */
const char *AtConnectionServerBoundAddress(AtConnection self);
uint16 AtConnectionServerListeningPort(AtConnection self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCONNECTION_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtMdio.h
 * 
 * Created Date: Jul 17, 2015
 *
 * Description : MDIO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMDIO_H_
#define _ATMDIO_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtRet.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtMdio * AtMdio;

typedef struct tAtMdioAddressCalculator
    {
    uint32 (*PortAddress)(AtMdio self, uint32 selectedPortId);
    }tAtMdioAddressCalculator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMdio AtMdioNew(uint32 baseAddress, AtHal hal);
AtMdio AtMdioSgmiiNew(uint32 baseAddress, AtHal hal);
void AtMdioDelete(AtMdio self);
const char *AtMdioToString(AtMdio self);

AtHal AtMdioHalGet(AtMdio self);
void AtMdioHalSet(AtMdio self, AtHal hal);

uint32 AtMdioRead(AtMdio self, uint32 page, uint32 address);
eAtRet AtMdioWrite(AtMdio self, uint32 page, uint32 address, uint32 value);

/* Manage ports */
void AtMdioPortDeselect(AtMdio self);
void AtMdioPortSelect(AtMdio self, uint32 portId);
uint32 AtMdioSelectedPort(AtMdio self);

/* For debugging */
void AtMdioDebug(AtMdio self);
void AtMdioSimulateEnable(AtMdio self, eBool enable);
eBool AtMdioSimulateIsEnabled(AtMdio self);

/* To change address calculator */
void AtMdioAddressCalculatorSet(AtMdio self, const tAtMdioAddressCalculator *calculator);
const tAtMdioAddressCalculator *AtMdioAddressCalculatorGet(AtMdio self);
uint32 AtMdioBaseAddress(AtMdio self);

/* SGMII */
eAtRet AtMdioSgmiiIsolateControlEnable(AtMdio self, eBool enable);
eAtRet AtMdioSgmiiAutoNegEnable(AtMdio self, eBool enable);
void AtMdioSgmiiTimeoutMsSet(AtMdio self, uint32 timeoutMs);

#ifdef __cplusplus
}
#endif
#endif /* _ATMDIO_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalFhn.h
 * 
 * Created Date: Dec 10, 2013
 *
 * Description : Concrete HAL for FHN project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALFHN_H_
#define _ATHALFHN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalFhnNew(uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALFHN_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalBdcomMdio.h
 * 
 * Created Date: Dec 18, 2013
 *
 * Description : MDIO to control GE physical
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBDCOMMDIO_H_
#define _ATBDCOMMDIO_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalBdcomMdioNew(AtHal hal);

#ifdef __cplusplus
}
#endif
#endif /* _ATBDCOMMDIO_H_ */


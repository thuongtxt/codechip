/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtCpld.h
 * 
 * Created Date: Jun 28, 2016
 *
 * Description : CPLD control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCPLD_H_
#define _ATCPLD_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCpld* AtCpld;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCpld AtCpldNew(uint32 baseAddress, AtHal hal);

const char *AtCpldToString(AtCpld self);
void AtCpldDelete(AtCpld self);
uint32 AtCpldVersion(AtCpld self);
eBool AtCpldIsDownloaded(AtCpld self);
eAtRet AtCpldDs1LoopbackSet(AtCpld self, eAtLoopbackMode loopbackMode);
eAtLoopbackMode AtCpldDs1LoopbackGet(AtCpld self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCPLD_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalTcp.h
 * 
 * Created Date: Feb 7, 2015
 *
 * Description : HAL that works through TCP connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALTCP_H_
#define _ATHALTCP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtHal.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtHalTcpDefaultPort     8000
#define cAtHalTcpEventListenPort 8001

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalTcpListener
    {
    tAtHalListener super;

    /* Additional methods */
    void (*DidReceiveInterruptMessage)(AtHal self, void *listener);
    }tAtHalTcpListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalTcpNew(const char *serverIpAddress, uint16 connectedPort);

/* To listen on events from the connected server. An event can be interrupt
 * event or other kinds. */
eAtRet AtHalTcpEventListen(AtHal self, uint16 listenPort);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALTCP_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Task Manager
 * 
 * File        : AtTaskManager.h
 * 
 * Created Date: Feb 6, 2015
 *
 * Description : Task manager header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTASKMANAGER_H_
#define _ATTASKMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTask.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTaskManager * AtTaskManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete managers */
AtTaskManager AtLinuxTaskManager(void);

/* Task management */
AtTask AtTaskManagerTaskCreate(AtTaskManager self, const char *taskName, void *userData, void* (*taskHandler)(void *userData));
eAtRet AtTaskManagerTaskDelete(AtTaskManager self, AtTask task);

#endif /* _ATTASKMANAGER_H_ */


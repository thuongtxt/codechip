/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHal.h
 * 
 * Created Date: Jul 31, 2012
 *
 * Author      : namnn
 * 
 * Description : Hardware Abstraction Layer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _HAL_H_
#define _HAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtHalSimDefaultMemorySizeInMbyte 256

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtHal
 * @{
 */

/** @brief HAL class */
typedef struct tAtHal * AtHal;

/** @brief To listen on HAL events */
typedef struct tAtHalListener
    {
    /* Read/write fail notification
     * IMPORTANT: Do not read/write inside the context of these callbacks to
     * avoid forever loop when device is ejected. */
    void (*ReadFail)(AtHal self, void *listener);  /**< Called when read operation fail */
    void (*WriteFail)(AtHal self, void *listener); /**< Called when write operation fail */

    /* For testing */
    void (*DidReadRegister)(AtHal self, void *listener, uint32 address, uint32 value);  /**< Called after reading a register */
    void (*DidWriteRegister)(AtHal self, void *listener, uint32 address, uint32 value); /**< Called after writing a register */
    void (*DidReadEnable)(AtHal self, void *listener, eBool enabled);  /**< Called after enabling/disabling read operation */
    void (*DidWriteEnable)(AtHal self, void *listener, eBool enabled); /**< Called after enabling/disabling write operation */
    }tAtHalListener;

/** @brief Direct write function which depends on specific platform */
typedef void (*AtHalDirectWriteHandler)(uint32 realAddress, uint32 value);

/** @brief Direct read function which depends on specific platform */
typedef uint32 (*AtHalDirectReadHandler)(uint32 realAddress);

/**
 * @}
 */

/* Private include */
#include "../hal/AtHalInternal.h"

/*--------------------------- Entries ----------------------------------------*/
/* Built-in simulation HALs */
AtHal AtHalSimNew(uint32 sizeInMbyte);
AtHal AtHalSimGlobalHoldNew(uint32 sizeInMbyte);
AtHal AtHalSimCreateForProductCode(uint32 codeChip, uint32 sizeInMbyte);

/* Built-in real HALs */
AtHal AtHalDefaultNew(uint32 baseAddress);
AtHal AtHalDefaultWithHandlerNew(uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler);
AtHal AtHalIndirectDefaultNew(uint32 baseAddress);

/* Common methods */
void AtHalDelete(AtHal self);
uint32 AtHalBaseAddressGet(AtHal self);
const char *AtHalToString(AtHal self);
uint32 AtHalMaxAddress(AtHal self);
void AtHalMaxAddressSet(AtHal self, uint32 maxAddress);

/* Listeners */
void AtHalListenerAdd(AtHal self, void *listener, const tAtHalListener *callbacks);
void AtHalListenerRemove(AtHal self, void *listener, const tAtHalListener *callbacks);

/* Default access registers methods. Indirect read/write is encapsulated by
 * these APIs. See AtHalDirectRead/AtHalDirectWrite for always direct accessing.
 * Note, these methods always lock. */
void AtHalWrite(AtHal self, uint32 address, uint32 value);
uint32 AtHalRead(AtHal self, uint32 address);
void AtHalWrite16(AtHal self, uint32 address, uint16 value);
uint16 AtHalRead16(AtHal self, uint32 address);

/* Direct read/write APIs */
void AtHalDirectWrite(AtHal self, uint32 address, uint32 value);
uint32 AtHalDirectRead(AtHal self, uint32 address);

/* Show read/write */
void AtHalShowRead(AtHal self, eBool enable);
void AtHalShowWrite(AtHal self, eBool enable);

/* For debug */
eBool AtHalDiagMemTest(AtHal self);
void AtHalDebug(AtHal self);
void AtHalDebugEnable(AtHal self, eBool enable);
eBool AtHalDebugIsEnabled(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _HAL_H_ */

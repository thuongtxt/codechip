/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Connection
 * 
 * File        : AtConnectionClasses.h
 * 
 * Created Date: May 30, 2016
 *
 * Description : Connection classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONNECTIONCLASSES_H_
#define _ATCONNECTIONCLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtConnection * AtConnection;
typedef struct tAtConnectionManager * AtConnectionManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATCONNECTIONCLASSES_H_ */


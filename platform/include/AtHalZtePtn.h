/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalZtePtn.h
 * 
 * Created Date: Jun 21, 2013
 *
 * Description : HAL for ZTE PTN project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALZTEPTN_H_
#define _ATHALZTEPTN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalZtePtn * AtHalZtePtn;

/* Helpers to directly read/write FPGA registers */
typedef unsigned int (*BSPFpgaRead16) (unsigned int dwSlot, unsigned short wAddr, unsigned short *pwData);
typedef unsigned int (*BSPFpgaWrite16)(unsigned int dwSlot, unsigned short wAddr, unsigned short wData);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete HALs */
AtHal AtHalZtePtnNew(uint32 baseAddress);
AtHal AtHalZtePtnEpldNew(uint32 slotId, BSPFpgaRead16 directReadFunc, BSPFpgaWrite16 directWriteFunc);

/* Additional APIs */
uint32 AtHalZtePtnEpldSlotId(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALZTEPTN_H_ */


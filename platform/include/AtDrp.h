/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtDrp.h
 * 
 * Created Date: Oct 27, 2015
 *
 * Description : DRP HAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDRP_H_
#define _ATDRP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDrp * AtDrp;

typedef struct tAtDrpAddressCalculator
    {
    uint32 (*PortOffset)(AtDrp self, uint32 selectedPortId);
    uint32 (*PortSelectRegisterAddress)(AtDrp self);
    uint32 (*PortSelectRegisterMask)(AtDrp self);
    uint32 (*PortSelectRegisterShift)(AtDrp self);
    }tAtDrpAddressCalculator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDrp AtDrpNew(uint32 baseAddress, AtHal hal);
void AtDrpDelete(AtDrp self);
AtHal AtDrpHalGet(AtDrp self);
const char *AtDrpToString(AtDrp self);

uint32 AtDrpBaseAddress(AtDrp self);
uint32 AtDrpRead(AtDrp self, uint32 address);
eAtRet AtDrpWrite(AtDrp self, uint32 address, uint32 value);

uint32 AtDrpSelectedPort(AtDrp self);
void AtDrpPortSelect(AtDrp self, uint32 portId);
void AtDrpPortDeselect(AtDrp self);

/* To change address calculator */
void AtDrpAddressCalculatorSet(AtDrp self, const tAtDrpAddressCalculator *calculator);

#ifdef __cplusplus
}
#endif
#endif /* _ATDRP_H_ */

#include "../hal/drp/AtDrpInternal.h"

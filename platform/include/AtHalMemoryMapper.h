/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalMemoryMapper.h
 * 
 * Created Date: Jul 15, 2013
 *
 * Description : Memory mapper
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALMEMORYMAPPER_H_
#define _ATHALMEMORYMAPPER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalMemoryMapper * AtHalMemoryMapper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHalMemoryMapper AtHalMemoryMapperLinuxNew(uint32 offset, uint32 memorySize);
void AtHalMemoryMapperDelete(AtHalMemoryMapper self);

eAtRet AtHalMemoryMapperMap(AtHalMemoryMapper self);
eAtRet AtHalMemoryMapperUnMap(AtHalMemoryMapper self);
uint32 AtHalMemoryMapperMemorySize(AtHalMemoryMapper self);
uint32 AtHalMemoryMapperOffset(AtHalMemoryMapper self);
uint32 AtHalMemoryMapperBaseAddress(AtHalMemoryMapper self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALMEMORYMAPPER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalDefault.h
 * 
 * Created Date: Nov 15, 2012
 *
 * Description : HAL default implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALDEFAULT_H_
#define _ATHALDEFAULT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumContinuousAccessFailToDeclareFailure 10
#define cInvalidReadValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mAtHalDefault(_self)       ((AtHalDefault)((void*)_self))
#define mHalFail(self_)            ((!mAtHalDefault(self_)->halFailureDetectDisabled) && \
                                    (mAtHalDefault(self_)->numContinuousFailures >= 20))
#define mHalFailureIncrease(self_)  mAtHalDefault(self_)->numContinuousFailures++
#define mHalFailureReset(self_)     mAtHalDefault(self_)->numContinuousFailures = 0
#define mHalFailureHandle(self_)                                               \
    do {                                                                       \
        mHalFailureIncrease(self_);                                            \
        if (mHalFail(self_))                                                   \
            AtHalDefaultFailureReport(self_);                                  \
    }while(0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalDefault * AtHalDefault;
typedef struct tAtHalTestRegister * AtHalTestRegister;
typedef struct tAtHalHa * AtHalHa;

typedef struct tAtHalTestRegister
    {
    uint32 address;
    uint32 writableMask;
    }tAtHalTestRegister;
    
typedef struct tAtHalDefaultMethods
    {
    const char *(*DescriptionBuild)(AtHal self, char *buffer, uint32 bufferSize);
    char* (*BaseAddressDescriptionToString)(AtHal self, char *buffer);

    /* Self-test */
    AtHalTestRegister (*RegsToTest)(AtHal self, uint32 *numRegs);
    uint32 (*TestPattern)(AtHal hal);
    uint32 (*TestAntiPattern)(AtHal hal);
    void (*ShowControlRegisters)(AtHal self);
    uint32 (*DataBusMask)(AtHal self);
    uint32 (*ReadAddressFromWriteAddress)(AtHal hal, uint32 address);
    uint32 (*ReadValueFromWriteValue)(AtHal hal, uint32 value);
    eBool (*NeedRestoreTestRegValues)(AtHal self);
    }tAtHalDefaultMethods;

/* Default implementation of HAL */
typedef struct tAtHalDefault
    {
    tAtHal super;
    const tAtHalDefaultMethods *methods;

    /* Private variables */
    uint32 baseAddress;
    AtOsalMutex mutex;
    uint8 isIsrContext;
    uint32 addressMask;

    /* To hold value of test registers before doing memory testing. Original
     * values will be written after testing */
    uint32 *testRegValues;

    char description[128];
    eBool descriptionIsValid;

    /* To detect HAL failure */
    uint32 numContinuousFailures;
    eBool failureNotified;
    eBool halFailureDetectDisabled;
    }tAtHalDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalDefaultObjectInit(AtHal self, uint32 baseAddress);
AtHal AtHalDefaultWithHandlerObjectInit(AtHal self, uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler);

uint32 AtHalDefaultBaseAddress(AtHal self);
void AtHalDefaultAddressMaskSet(AtHal self, uint32 addressMask);

void AtHalDefaultTestRegisterSetup(AtHal self, AtHalTestRegister regToTest, uint32 address);
void AtHalDefaultTestRegisterSetupWithMask(AtHal self, AtHalTestRegister regToTest, uint32 address, uint32 regMask);

void AtHalDefaultFailureReport(AtHal self);
void AtHalDefaultFailureDetectEnable(AtHal self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALDEFAULT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform - HAL
 *
 * File        : AtHalSim.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Source file of HAL Simulation,
 *               the Ram-based simulation of Hardware Abstraction Layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "atclib.h"
#include "AtHalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Simulate 24 bit address space of 32bit registers : 4 * 2^24 = 64MB
 * To avoid error in allocating a big memory, divide it to 16 blocks 4MB */
#define cAtHalSimOneMega        0x100000
#define cAtNumHoldRegs          3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtHalSim)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalSimMethods m_methods;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BlockSize(void)
    {
    return 1024;
    }

static uint32 NumBlocks(AtHalSim self)
    {
    return (self->sizeInMbytes * 1024 * 1024) / BlockSize();
    }

static uint32 BlockOfAddress(AtHalSim self, uint32 address)
    {
    uint32 blockId = address / BlockSize();
    AtAssert(blockId < NumBlocks(self));
    return blockId;
    }

static uint32 LocalAddress(uint32 address)
    {
    return address % BlockSize();
    }

static tAtHalSimRegister **MemoryBlocks(AtHalSim self)
    {
    uint32 memorySize;

    if (mThis(self)->memoryBlocks)
        return mThis(self)->memoryBlocks;

    memorySize = sizeof(unsigned long *) * NumBlocks(self);
    self->memoryBlocks = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(self->memoryBlocks, 0, memorySize);

    return mThis(self)->memoryBlocks;
    }

static tAtHalSimRegister *MemoryBlock(AtHalSim self, uint32 address)
    {
    uint32 blockMemorySize;
    uint32 blockId = BlockOfAddress(self, address);
    tAtHalSimRegister *block = MemoryBlocks(self)[blockId];

    if (block)
        return block;

    blockMemorySize = BlockSize() * sizeof(tAtHalSimRegister);
    block = (tAtHalSimRegister *)AtOsalMemAlloc(blockMemorySize);
    AtOsalMemInit(block, 0, blockMemorySize);

    MemoryBlocks(self)[blockId] = block;

    return block;
    }

static tAtHalSimRegister *Register(AtHalSim self, AtSize address)
    {
    tAtHalSimRegister *block = MemoryBlock(self, address);
    return &(block[LocalAddress(address)]);
    }

static eBool *LongRegisterIndicator(AtHalSim self, AtSize address)
    {
    return &(Register(self, address)->isLongReg);
    }

static AtSize *RegisterMemory(AtHalSim self, uint32 address)
    {
    return &(Register(self, address)->registerMemory);
    }

static eBool IsLongReg(AtHalSim self, uint32 address)
    {
    return *LongRegisterIndicator(self, address);
    }

static void LongRegMark(AtHalSim self, AtSize address, eBool isLongReg)
    {
    *LongRegisterIndicator(self, address) = isLongReg;
    }

static void RegWrite(AtHalSim self, uint32 address, uint32 value)
    {
    *RegisterMemory(self, address) = value;
    }

static uint32 RegRead(AtHalSim self, uint32 address)
    {
    return (uint32)*RegisterMemory(self, address);
    }

static uint32 LongRegRead(AtHalSim self, uint32 address, uint32 idx)
    {
    AtSize startMemory = *RegisterMemory(self, address);
    uint32 *values = (void *)startMemory;
    return values[idx];
    }

static void Lock(AtHal self)
    {
    AtHalSim hal = (AtHalSim)self;
    AtOsalMutexLock(hal->mutex);
    }

static void UnLock(AtHal self)
    {
    AtHalSim hal = (AtHalSim)self;
    AtOsalMutexUnLock(hal->mutex);
    }

static void Debug(AtHal self)
    {
    m_AtHalMethods->Debug(self);
    }

static uint32 HoldRegsForReadOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    *holdRegs = NULL;
    return 0;
    }

static uint32 HoldRegsForWriteOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    *holdRegs = NULL;
    return 0;
    }

static eBool IsReadHoldReg(AtHalSim self, uint32 regAddress)
    {
    const uint32 *holdRegs = NULL;
    uint32 numHoldRegs = self->methods->HoldRegsForReadOfLongReg(self, regAddress, &holdRegs);
    uint32 holdReg_i;

    for (holdReg_i = 0; holdReg_i < numHoldRegs; holdReg_i++)
        {
        if (regAddress == holdRegs[holdReg_i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool IsWriteHoldReg(AtHalSim self, uint32 regAddress)
    {
    const uint32 *holdRegs = NULL;
    uint32 numHoldRegs = self->methods->HoldRegsForWriteOfLongReg(self, regAddress, &holdRegs);
    uint32 holdReg_i;

    for (holdReg_i = 0; holdReg_i < numHoldRegs; holdReg_i++)
        {
        if (regAddress == holdRegs[holdReg_i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static void DeleteRegisterMemory(AtHal self)
    {
    AtHalSim hal = mThis(self);
    uint32 block_i;
    uint32 reg_i;
    uint32 numBlocks = NumBlocks(hal);

    if (hal->memoryBlocks == NULL)
        return;

    /* Free memory of registers */
    for (block_i = 0; block_i < numBlocks; block_i++)
        {
        for (reg_i = 0; reg_i < BlockSize(); reg_i++)
            {
            tAtHalSimRegister *simRegister;

            if (hal->memoryBlocks == NULL)
                continue;

            if (hal->memoryBlocks[block_i] == NULL)
                continue;

            simRegister = &(hal->memoryBlocks[block_i][reg_i]);
            if (simRegister == NULL)
                continue;

            if (simRegister->isLongReg)
                AtOsalMemFree((void *)simRegister->registerMemory);
            }

        if (hal->memoryBlocks)
            {
            AtOsalMemFree(hal->memoryBlocks[block_i]);
            hal->memoryBlocks[block_i] = NULL;
            }
        }

    AtOsalMemFree(hal->memoryBlocks);
    }

static void Delete(AtHal self)
    {
    AtHalSim hal = mThis(self);

    DeleteRegisterMemory(self);
    AtOsalMutexDestroy(hal->mutex);
    AtOsalMemFree(hal->readHoldRegisters);
    AtOsalMemFree(hal->writeHoldRegisters);

    m_AtHalMethods->Delete(self);
    }

static void WriteHoldReg(AtHal self, uint32 address, uint32 value)
    {
    RegWrite(mThis(self), address, value);
    mThis(self)->holdWritten = cAtTrue;
    }

static void WriteNormalReg(AtHal self, uint32 address, uint32 value)
    {
    RegWrite(mThis(self), address, value);
    mThis(self)->holdWritten = cAtFalse;
    }

static void Delay(void)
    {
    static uint32 cRepeatTime = 14000;
    uint32 i;

    for (i = 0; i < cRepeatTime; i++);
    }

static void LongWrite(AtHalSim hal, uint32 address, uint32 value, const uint32 *holdRegs, uint32 numHoldRegs)
    {
    uint8 hold_i;
    uint32 memAddr;
    uint32 *longRegMem;

    memAddr = RegRead(hal, address);
    longRegMem = (uint32 *)(AtSize)memAddr;
    if (longRegMem == NULL)
        return;

    longRegMem[0] = value;
    for (hold_i = 0; hold_i < numHoldRegs; hold_i++)
        longRegMem[hold_i + 1] = RegRead(hal, holdRegs[hold_i]);
    }

static void LongRegisterMemoryAllocate(AtHalSim hal, uint32 address, uint32 value, const uint32 *holdRegs, uint32 numHoldRegs)
    {
    /* If hold register was written right before, this register is assumed to be a long register.
     * Allocate memory for it and hal->regMem[][] contain address of allocated memory */
    uint32 *longRegMem, memorySize;
    uint32 hold_i;

    /* Allocate memory for hold register */
    memorySize = (uint32)(sizeof(uint32) * (numHoldRegs + 1));
    longRegMem = (uint32 *)AtOsalMemAlloc(memorySize);
    if (longRegMem == NULL)
        return;

    /* Initialize memory */
    AtOsalMemInit(longRegMem, 0, memorySize);
    longRegMem[0] = value;
    for (hold_i = 0; hold_i < numHoldRegs; hold_i++)
        longRegMem[hold_i + 1] = RegRead(hal, holdRegs[hold_i]);

    /* For long register, the first address is to store memory to all of its short registers */
    RegWrite(hal, address, (uint32)(AtSize)longRegMem);
    LongRegMark(hal, address, cAtTrue);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    AtHalSim hal = (AtHalSim)self;
    eBool longAccessInProgress;
    uint32 numHoldRegs = 0;
    const uint32 *holdRegs = NULL;

    if (self == NULL)
        return;

    if (self->slowDown)
        Delay();

    /* Writing a long register is in progress */
    if (IsWriteHoldReg(hal, address))
        {
        WriteHoldReg(self, address, value);
        return;
        }

    /* Do normal operation when long register is not in access process */
    longAccessInProgress = IsLongReg(hal, address) || hal->holdWritten;
    if (!longAccessInProgress)
        {
        WriteNormalReg(self, address, value);
        return;
        }

    numHoldRegs = (uint32)(hal->methods->HoldRegsForWriteOfLongReg(hal, address, &holdRegs));
    if ((numHoldRegs == 0) || (holdRegs == NULL))
        {
        hal->holdWritten = cAtFalse;
        return;
        }

    /* If register is long register and memory was allocated,
     * write "value" to location 0, and store all hold values to remain locations of allocated memory */
    if (IsLongReg(hal, address))
        LongWrite(hal, address, value, holdRegs, numHoldRegs);

    else if (hal->holdWritten)
        LongRegisterMemoryAllocate(hal, address, value, holdRegs, numHoldRegs);

    hal->holdWritten = cAtFalse;
    }

static uint32 HoldRegRead(AtHalSim hal, uint32 address)
    {
    uint32 numHoldRegs;
    const uint32 *holdRegs;
    uint32 i;
    uint32 rdVal;

    if (hal->cachedAddress == cInvalidUint32)
        return RegRead(hal, address);

    holdRegs    = NULL;
    numHoldRegs = (uint32)(hal->methods->HoldRegsForReadOfLongReg(hal, address, &holdRegs));
    if ((numHoldRegs == 0) || (holdRegs == NULL))
        return RegRead(hal, address);

    /* If memory of long register (hal->cachedAddress) was not allocated, allocate memory for it */
    if (IsLongReg(hal, hal->cachedAddress) == cAtFalse)
        {
        uint32 memorySize = (uint32)sizeof(uint32) * (numHoldRegs + 1);
        AtSize *mem = AtOsalMemAlloc(memorySize);
        if (mem != NULL)
            {
            /* Some addresses may firstly be written as short register, then
             * it is later read as a long register, although it is actually
             * a long register.
             * For this situation, we need to copy value from short cache
             * holder to long cache holder. It is to retain the last written
             * value. */
            uint32 lastValue = RegRead(hal, hal->cachedAddress);
            uint32 *longAddrMemory;

            AtOsalMemInit(mem, 0, memorySize);
            RegWrite(hal, hal->cachedAddress, (AtSize)mem);
            LongRegMark(hal, hal->cachedAddress, cAtTrue);

            longAddrMemory = (uint32 *)*RegisterMemory(hal, hal->cachedAddress);
            longAddrMemory[0] = lastValue;
            }
        }

    /* Load value of long register to all hold registers */
    for (i = 0; i < numHoldRegs; i++)
        {
        rdVal = LongRegRead(hal, hal->cachedAddress, i + 1);
        RegWrite(hal, holdRegs[i], rdVal);
        }

    /* Return value of hold register */
    return RegRead(hal, address);
    }

static uint32 ReadAndCacheAddress(AtHalSim hal, uint32 address)
    {
    uint32 value;

    if (IsLongReg(hal, address))
        value = LongRegRead(hal, address, 0);
    else
        value = RegRead(hal, address);

    hal->cachedAddress = address;

    return value;
    }

static uint32 Read(AtHal self, uint32 address)
    {
    AtHalSim hal = mThis(self);

    if (self->slowDown)
        Delay();

    /* Cache address of normal register (not hold register) to hal->cachedAddress.
     * Assume a register (hal->cachedAddress) is long register
     * if hold register is read right after reading this register
     */
    if (IsReadHoldReg(hal, address))
        return HoldRegRead(hal, address);
    else
        return ReadAndCacheAddress(hal, address);
    }

static uint32 BaseAddressGet(AtHal self)
    {
	AtUnused(self);
    return 0;
    }

static const char* ToString(AtHal self)
    {
	AtUnused(self);
    return "Simulation";
    }

static uint32 DirectOamOffsetGet(AtHal self)
    {
	AtUnused(self);
    return 0;
    }

static eBool IsPppProduct(uint32 productCode)
    {
    if ((productCode == 0x60030022) ||
        (productCode == 0x60030023) ||
        (productCode == 0x60030051) ||
        (productCode == 0x60030101) ||
        (productCode == 0x60031032) ||
        (productCode == 0x60031035) ||
        (productCode == 0x60060011) ||
        (productCode == 0x60070013) ||
        (productCode == 0x60070023) ||
        (productCode == 0x60071032) ||
        (productCode == 0x60091132) ||
        (productCode == 0x60091135) ||
        (productCode == 0x61150011) ||
        (productCode == 0x65031032))
        return cAtTrue;
    return cAtFalse;
    }

static void RegisterValidate(AtHal self, uint32 address, eBool validated)
    {
    tAtHalSimRegister *reg = Register((AtHalSim)self, address);
    reg->validated = validated;
    }

static eBool RegisterIsValidated(AtHal self, uint32 address)
    {
    tAtHalSimRegister *reg = Register((AtHalSim)self, address);
    return reg->validated;
    }

static uint32 MaxAddress(AtHal self)
    {
    uint32 numBytes = mThis(self)->sizeInMbytes * 1024 * 1024;
    uint32 numDwords = numBytes / 4;
    return (uint32)(numDwords - 1);
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize super methods */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Delete             = Delete;
        m_AtHalOverride.Read               = Read;
        m_AtHalOverride.Write              = Write;
        m_AtHalOverride.DirectRead         = Read;
        m_AtHalOverride.DirectWrite        = Write;
        m_AtHalOverride.BaseAddressGet     = BaseAddressGet;
        m_AtHalOverride.DirectOamOffsetGet = DirectOamOffsetGet;
        m_AtHalOverride.Lock               = Lock;
        m_AtHalOverride.UnLock             = UnLock;
        m_AtHalOverride.Debug              = Debug;
        m_AtHalOverride.ToString           = ToString;
        m_AtHalOverride.MaxAddress         = MaxAddress;
        }

    AtHalMethodsSet((AtHal)self, &m_AtHalOverride);
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalSim);
    }

static void MethodsInit(AtHalSim self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.HoldRegsForReadOfLongReg  = HoldRegsForReadOfLongReg;
        m_methods.HoldRegsForWriteOfLongReg = HoldRegsForWriteOfLongReg;
        }

    AtHalSimMethodsSet(self, &m_methods);
    }

static void InitPrivateData(AtHalSim self, uint32 sizeInMbyte)
    {
    /* Initialize private data */
    self->sizeInMbytes = sizeInMbyte;
    self->cachedAddress = 0xFFFFFFFF;
    self->holdWritten = cAtFalse;
    self->memoryBlocks = NULL;

    /* Create mutex to protect shared memory */
    self->mutex = AtOsalMutexCreate();
    }

AtHal AtHalSimObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtHalSim)self);
    m_methodsInit = 1;

    /* Private data */
    InitPrivateData(mThis(self), sizeInMbyte);

    return self;
    }

const tAtHalSimMethods *AtHalSimMethodsGet(AtHalSim self)
    {
    return self ? self->methods : NULL;
    }

void AtHalSimMethodsSet(AtHalSim self, const tAtHalSimMethods *methods)
    {
    if (self)
        self->methods = methods;
    }

AtHal AtHalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return AtHalSimObjectInit(hal, sizeInMbyte);
    }

void AtHalSimRegisterValidate(AtHal self, uint32 address, eBool validated)
    {
    if (self)
        RegisterValidate(self, address, validated);
    }

eBool AtHalSimRegisterIsValidated(AtHal self, uint32 address)
    {
    if (self)
        return RegisterIsValidated(self, address);
    return cAtFalse;
    }

/**
 * @addtogroup AtHal
 * @{
 */

/**
 * Create HAL simulation for specific product code
 *
 * @param productCode Product code
 * @param sizeInMbyte Size in MB
 *
 * @return HAL simulation instance
 */
AtHal AtHalSimCreateForProductCode(uint32 productCode, uint32 sizeInMbyte)
    {
    if (IsPppProduct(productCode))
        return AtHalSimNew(sizeInMbyte);

    if (productCode == 0x60210011)
        {
        static uint32 holdRegs[] = {0x0F00008, 0x0F00009, 0x0F0000A};
        return AtHalSimGlobalHoldWithHoldRegistersNew(holdRegs, holdRegs, 3, sizeInMbyte);
        }

    if (productCode == 0x60210051)
        return Tha60210051HalSimNew(sizeInMbyte);

    if ((productCode == 0x60210012) || (productCode == 0x60210061))
        return Tha60210012HalSimNew(sizeInMbyte);

    if ((productCode == 0x60290021))
        return Tha60290021HalSimNew(sizeInMbyte);

    if ((productCode == 0x6A290021)||(productCode == 0x6A290E21))
        return Tha6A290021HalSimNew(sizeInMbyte);

    if ((productCode == 0x60290022) ||
        (productCode == 0x6A290022) ||
        (productCode == 0x6A291022) ||
        (productCode == 0x60291022) ||
        (productCode == 0x60290051))
        return Tha60290022HalSimNew(sizeInMbyte);

    if ((productCode == 0x60290081)||
        (productCode == 0x60290081))
        return Tha60290081HalSimNew(sizeInMbyte);

    if ((productCode == 0x60290011) ||
        (productCode == 0x6A290011) ||
        (productCode == 0x60291011) ||
        (productCode == 0x60290061))
        return Tha60290011HalSimNew(sizeInMbyte);

    return AtHalSimGlobalHoldNew(sizeInMbyte);
    }

/**
 * @}
 */

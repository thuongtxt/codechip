/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Hal
 *
 * File        : AtXAuiMdio.c
 *
 * Created Date: Nov 26, 2014
 *
 * Description : XAUI access functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtMdioInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cXauiMdioAccessPageMask  cBit4_0
#define cXauiMdioAccessPageShift 0
#define cXauiMdioAccessPortMask  cBit12_8
#define cXauiMdioAccessPortShift 8

#define cXauiMdioReadWriteMask  cBit4
#define cXauiMdioReadWriteShift 4
#define cXauiMdioOperationMask  cBit1_0
#define cXauiMdioOperationShift 0

#define cXauiMdioDataMask cBit15_0

/* Note: only projects that have more than 1 XAUI ports use this register */
#define cXauiMdioPortSelectMask  cBit7_0
#define cXauiMdioPortSelectShift 0
#define cXauiMdioPortSelectNone  0
#define cXauiMdioPortSelect(portId) ((portId) + 1)

#define cPageAddressSelectCommand 0x10
#define cReadCommand 0x13
#define cWriteCommand 0x11
#define cStopCommand 0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtMdioMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtMdio self)
    {
    if (self->baseAddress == 0)
        return 0xf50000;
    return self->baseAddress;
    }

static uint32 RegisterWithOffset(AtMdio self, uint32 offset)
    {
    return BaseAddress(self) + offset;
    }

static uint32 AccessCommandRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x12);
    }

static uint32 AccessAckReadRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x0);
    }

static uint32 AccessAddressSelectRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x10);
    }

static uint32 AccessAckClearRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x11);
    }

static uint32 ReadDataRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x21);
    }

static uint32 WriteDataRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x20);
    }

static uint32 PortSelectRegister(AtMdio self)
    {
    return RegisterWithOffset(self, 0x13);
    }

static void AccessingFinish(AtMdio self)
    {
    AtHal hal = AtMdioHalGet(self);
    AtHalWrite(hal, AccessAckClearRegister(self), 0x1);
    AtHalWrite(hal, AccessCommandRegister(self), cStopCommand);
    }

static eBool HwDone(AtMdio self)
    {
    static uint32 cNumRetryTimes = 10000;
    uint32 retryTimes = 0;
    AtHal hal = AtMdioHalGet(self);

    if (AtHalRead(hal, AccessAckReadRegister(self)))
        return cAtTrue;

    if (AtMdioSimulateIsEnabled(self))
        return cAtTrue;

    /* Wait for ack from hardware */
    while (retryTimes < cNumRetryTimes)
        {
        /* If ack return */
        if (AtHalRead(hal, AccessAckReadRegister(self)))
            return cAtTrue;

        /* Or retry */
        retryTimes = retryTimes + 1;
        }

    return cAtFalse;
    }

static eBool PageAndAddressSelect(AtMdio self, uint32 page, uint32 address)
    {
    static const uint8 cPort = 1; /* HW recommended */
    uint32 regVal = 0;
    eBool result;
    AtHal hal = AtMdioHalGet(self);

    /* Select page and port */
    mRegFieldSet(regVal, cXauiMdioAccessPage, page);
    mRegFieldSet(regVal, cXauiMdioAccessPort, cPort);
    AtHalWrite(hal, AccessAddressSelectRegister(self), regVal);

    /* Select address in page */
    AtHalWrite(hal, WriteDataRegister(self), address);

    /* Send command to select page and address */
    AtHalWrite(hal, AccessCommandRegister(self), cPageAddressSelectCommand);

    /* Wait for HW finish */
    result = HwDone(self);

    /* Finish select address */
    AccessingFinish(self);

    return result;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMdio);
    }

static void HwPortDeselect(AtMdio self)
    {
    AtHal hal = AtMdioHalGet(self);
    uint32 regVal = AtHalRead(hal, PortSelectRegister(self));
    mRegFieldSet(regVal, cXauiMdioPortSelect, cXauiMdioPortSelectNone);
    AtHalWrite(hal, PortSelectRegister(self), regVal);
    }

static void HwPortSelect(AtMdio self, uint32 portId)
    {
    AtHal hal = AtMdioHalGet(self);
    uint32 regVal = AtHalRead(hal, PortSelectRegister(self));
    mRegFieldSet(regVal, cXauiMdioPortSelect, (portId + 1));
    AtHalWrite(hal, PortSelectRegister(self), regVal);
    }

static uint32 HwSelectedPort(AtMdio self)
    {
    AtHal hal = AtMdioHalGet(self);
    uint32 regVal = AtHalRead(hal, PortSelectRegister(self));
    uint32 port = mRegField(regVal, cXauiMdioPortSelect);
    if (port == cXauiMdioPortSelectNone)
        return cBit31_0;
    return port - 1;
    }

static void PortDeselect(AtMdio self)
    {
    self->selectedPort = cBit31_0;
    }

static void PortSelect(AtMdio self, uint32 portId)
    {
    self->selectedPort = portId;
    }

static void DefaultPortSelect(AtMdio self)
    {
    uint32 port = self->selectedPort;
    if (port == cBit31_0)
        port = 0;
    HwPortSelect(self, port);
    }

static uint32 SelectedPort(AtMdio self)
    {
    return self->selectedPort;
    }

static void ShowSelectedPort(uint32 selectedPort)
    {
    if (selectedPort == cBit31_0)
        AtPrintc(cSevNormal, "None\r\n");
    else
        AtPrintc(cSevInfo, "%d\r\n", selectedPort + 1);
    }

static void Debug(AtMdio self)
    {
    AtPrintc(cSevNormal, "* HW selected port: ");
    ShowSelectedPort(HwSelectedPort(self));
    AtPrintc(cSevNormal, "* SW selected port: ");
    ShowSelectedPort(self->selectedPort);
    }

static uint32 HwRead(AtMdio self, uint32 page, uint32 address)
    {
    uint32 readVal = 0xCAFECAFE;
    AtHal hal = AtMdioHalGet(self);

    /* Select page and port */
    if (!PageAndAddressSelect(self, page, address))
        return readVal;

    /* Send command to read */
    AtHalWrite(hal, AccessCommandRegister(self), cReadCommand);

    /* Wait for HW finish */
    if (HwDone(self))
        readVal = (AtHalRead(hal, ReadDataRegister(self)) & cXauiMdioDataMask);

    AccessingFinish(self);
    return readVal;
    }

static eAtRet HwWrite(AtMdio self, uint32 page, uint32 address, uint32 value)
    {
    AtHal hal = AtMdioHalGet(self);
    eAtRet ret = cAtOk;

    /* Select page and port */
    if (!PageAndAddressSelect(self, page, address))
        return cAtErrorSerdesMdioFail;

    /* Send command to write */
    AtHalWrite(hal, WriteDataRegister(self), value & cXauiMdioDataMask);
    AtHalWrite(hal, AccessCommandRegister(self), cWriteCommand);

    /* Wait for hardware finish its job */
    if (!HwDone(self))
        ret = cAtErrorSerdesMdioFail;

    AccessingFinish(self);

    return ret;
    }

static uint32 Read(AtMdio self, uint32 page, uint32 address)
    {
    uint32 value;

    DefaultPortSelect(self);
    value = HwRead(self, page, address);
    HwPortDeselect(self);

    return value;
    }

static eAtRet Write(AtMdio self, uint32 page, uint32 address, uint32 value)
    {
    eAtRet ret;

    DefaultPortSelect(self);
    ret = HwWrite(self, page, address, value);
    HwPortDeselect(self);

    return ret;
    }

static const char *ToString(AtMdio self)
    {
    static char description[64];

    AtSnprintf(description, sizeof(description), "hal: %s, baseAddress: 0x%08x",
               self->hal ? "installed" : "none",
               self->baseAddress);

    return description;
    }

static void AddressCalculatorSet(AtMdio self, const tAtMdioAddressCalculator *calculator)
    {
    self->addressCalculator = calculator;
    }

static const tAtMdioAddressCalculator *AddressCalculatorGet(AtMdio self)
    {
    return self->addressCalculator;
    }

static void MethodsInit(AtMdio self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.Write = Write;
        m_methods.Read  = Read;
        }

    self->methods = &m_methods;
    }

AtMdio AtMdioObjectInit(AtMdio self, uint32 baseAddress, AtHal hal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (hal == NULL)
        return NULL;

    MethodsInit(self);
    m_methodsInit = 1;

    self->hal = hal;
    self->baseAddress = baseAddress;
    self->selectedPort = cBit31_0;

    return self;
    }

AtMdio AtMdioNew(uint32 baseAddress, AtHal hal)
    {
    AtMdio newMdio = AtOsalMemAlloc(ObjectSize());
    if (AtMdioObjectInit(newMdio, baseAddress, hal) == NULL)
        {
        AtMdioDelete(newMdio);
        return NULL;
        }

    return newMdio;
    }

uint32 AtMdioRead(AtMdio self, uint32 page, uint32 address)
    {
    if (self)
        return self->methods->Read(self, page, address);
    return 0xDEADCAFE;
    }

eAtRet AtMdioWrite(AtMdio self, uint32 page, uint32 address, uint32 value)
    {
    if (self)
        return self->methods->Write(self, page, address, value);
    return cAtOk;
    }

void AtMdioPortDeselect(AtMdio self)
    {
    if (self)
		PortDeselect(self);
    }

void AtMdioPortSelect(AtMdio self, uint32 portId)
    {
    if (self)
        PortSelect(self, portId);
    }

uint32 AtMdioSelectedPort(AtMdio self)
    {
    if (self)
        return SelectedPort(self);
    return 0xFFFFFFFF;
    }

AtHal AtMdioHalGet(AtMdio self)
    {
    return self ? self->hal : NULL;
    }

void AtMdioHalSet(AtMdio self, AtHal hal)
    {
    if (self)
        self->hal = hal;
    }

void AtMdioDelete(AtMdio self)
    {
    AtOsalMemFree(self);
    }

void AtMdioSimulateEnable(AtMdio self, eBool enable)
    {
    if (self)
        self->simulated = enable;
    }

eBool AtMdioSimulateIsEnabled(AtMdio self)
    {
    return (eBool)(self ? self->simulated : cAtFalse);
    }

void AtMdioDebug(AtMdio self)
    {
    if (self)
        Debug(self);
    }

const char *AtMdioToString(AtMdio self)
    {
    if (self)
        return ToString(self);
    return NULL;
    }

void AtMdioAddressCalculatorSet(AtMdio self, const tAtMdioAddressCalculator *calculator)
    {
    if (self)
        AddressCalculatorSet(self, calculator);
    }

const tAtMdioAddressCalculator *AtMdioAddressCalculatorGet(AtMdio self)
    {
    if (self)
        return AddressCalculatorGet(self);
    return NULL;
    }

uint32 AtMdioBaseAddress(AtMdio self)
    {
    if (self)
        return BaseAddress(self);
    return cInvalidUint32;
    }

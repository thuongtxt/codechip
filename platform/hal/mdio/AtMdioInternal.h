/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtMdioInternal.h
 * 
 * Created Date: Jul 17, 2015
 *
 * Description : MDIO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMDIOINTERNAL_H_
#define _ATMDIOINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMdio.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtMdioMethods
    {
    uint32 (*Read)(AtMdio self, uint32 page, uint32 address);
    eAtRet (*Write)(AtMdio self, uint32 page, uint32 address, uint32 value);
    }tAtMdioMethods;

typedef struct tAtMdio
    {
    AtHal hal;
    const tAtMdioMethods *methods;

    /* Private data */
    uint32 baseAddress;
    eBool simulated;
    uint32 selectedPort;
    const tAtMdioAddressCalculator *addressCalculator;
    }tAtMdio;

typedef struct tAtMdioSgmii
    {
    tAtMdio super;

    /* Private data */
    uint32 retryInMs;
    }tAtMdioSgmii;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMdio AtMdioObjectInit(AtMdio self, uint32 baseAddress, AtHal hal);

#ifdef __cplusplus
}
#endif
#endif /* _ATMDIOINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtMdioSgmii.c
 *
 * Created Date: Jun 25, 2016
 *
 * Description : SGMII function
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtMdioInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* Control register and fields */
#define cControlPortReg             0x000010
#define cControlPortSelectMask      cBit7_4
#define cControlPortSelectShift     4
#define cControlPortAddressMask     cBit3_0 /* Default 1 */
#define cControlPortAddressShift    0

#define cControlReg                 0x000012
#define cControlReadWriteStartMask  cBit7
#define cControlReadWriteStartShift 7
#define cControlOperationMask       cBit6_5
#define cControlOperationShift      5
#define cControlOperationWrite      1
#define cControlOperationRead       2
#define cControlRegAddressMask      cBit4_0
#define cControlRegAddressShift     0

/* ACK */
#define cStatusAckReg               0x000000
#define cStatusIsolateMask         cBit10
#define cStatusIsolateShift            10
#define cStatusClearAckMask         cBit0
#define cStatusAutoNegMask          cBit12
#define cStatusAutoNegShift         12

#define cControlAckReg              0x000011
#define cControlClearAckMask        cBit0

/* Register to read/write data */
#define cReadWriteDatasReg          0x000020
#define cWriteDataMask              cBit31_16
#define cWriteDataShift             16
#define cReadDataMask               cBit15_0

#define cInvalidValue               0xDEADCAFE

#define mThis(self)        ((tAtMdioSgmii *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtMdioMethods m_AtMdioOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtMdioSgmii);
    }

static uint32 BaseAddress(AtMdio self)
    {
    return AtMdioBaseAddress(self);
    }

static uint32 ReadControl(AtMdio self, uint32 controlRegister)
    {
    return AtHalRead(AtMdioHalGet(self), controlRegister);
    }

static void WriteControl(AtMdio self, uint32 controlRegister, uint32 value)
    {
    AtHalWrite(AtMdioHalGet(self), controlRegister, value);
    }

static void ClearAck(AtMdio self)
    {
    WriteControl(self, cControlAckReg + BaseAddress(self), cControlClearAckMask);
    }

static eBool HwDone(AtMdio self, uint32 address, eRequestType requestType)
    {
    uint32 elapsedTime = 0;
    tAtOsalCurTime startTime, currentTime;
    uint32 ackReg = cStatusAckReg + BaseAddress(self);

    AtOsalCurTimeGet(&startTime);
    AtOsalCurTimeGet(&currentTime);
    while (elapsedTime < mThis(self)->retryInMs)
        {
        uint32 regVal = ReadControl(self, ackReg);

        if (AtMdioSimulateIsEnabled(self))
            return cAtTrue;

        /* Done */
        if (regVal & cStatusClearAckMask)
            return cAtTrue;

        /* Retry */
        AtOsalCurTimeGet(&currentTime);
        elapsedTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    /* Timeout, log debug information */
    AtPrintc(cSevCritical, "(%s, %d) ERROR: %s 0x%08x timeout(startTime: %u.%u, stopTime: %u.%u)\n", AtSourceLocation,
             (requestType == cRequestRead) ? "Read" : "Write", address, startTime.sec, startTime.usec, currentTime.sec, currentTime.usec);

    return cAtFalse;
    }

static uint32 PortAddress(AtMdio self)
    {
    const tAtMdioAddressCalculator *addressCalculator = AtMdioAddressCalculatorGet(self);
    uint32 selectedPort = AtMdioSelectedPort(self);

    if (addressCalculator && addressCalculator->PortAddress)
        return addressCalculator->PortAddress(self, selectedPort);

    switch (selectedPort)
        {
        case 3: return 1;
        case 4: return 2;
        case 5: return 4;
        case 6: return 8;
        default:
            return 0;
        }
    }

static void PortAddressSet(AtMdio self)
    {
    uint32 regVal = 0;
    mFieldIns(&regVal, cControlPortSelectMask, cControlPortSelectShift, PortAddress(self));
    mFieldIns(&regVal, cControlPortAddressMask, cControlPortAddressShift, 0x1);
    WriteControl(self, cControlPortReg + BaseAddress(self), regVal);
    }

static void MakeRequest(AtMdio self, uint32 address, eRequestType requestType)
    {
    uint32 regVal = 0;
    PortAddressSet(self);
    mRegFieldSet(regVal, cControlOperation, (requestType == cRequestRead) ? cControlOperationRead : cControlOperationWrite);
    mRegFieldSet(regVal, cControlRegAddress, (address & cBit4_0));
    mRegFieldSet(regVal, cControlReadWriteStart, 1);
    WriteControl(self, cControlReg + BaseAddress(self), regVal);
    }

static eAtRet Write(AtMdio self, uint32 page, uint32 address, uint32 value)
    {
    uint32 regVal, baseAddress;
    eAtRet ret = cAtOk;

    AtUnused(page);
    regVal = 0;
    baseAddress = BaseAddress(self);
    ClearAck(self);

    mRegFieldSet(regVal, cWriteData, value & cBit15_0);
    WriteControl(self, cReadWriteDatasReg + baseAddress, regVal);
    MakeRequest(self, address, cRequestWrite);

    if (!HwDone(self, address, cRequestWrite))
        ret = cAtErrorSerdesMdioFail;

    ClearAck(self);
    WriteControl(self, cControlReg + baseAddress, 0);

    return ret;
    }

static uint32 Read(AtMdio self, uint32 page, uint32 address)
    {
    uint32 regVal, baseAddress;

    AtUnused(page);
    baseAddress = BaseAddress(self);
    ClearAck(self);

    MakeRequest(self, address, cRequestRead);
    if (HwDone(self, address, cRequestRead))
        regVal = ReadControl(self, cReadWriteDatasReg + baseAddress) & cReadDataMask;
    else
        regVal = cInvalidValue;

    ClearAck(self);
    WriteControl(self, cControlReg + baseAddress, 0);

    return regVal;
    }

static eAtRet MdioIsolateBitCfg(AtMdio self, eBool enable)
    {
    uint32 regAddr = cStatusAckReg;
    uint32 regVal = Read(self, 1, regAddr);

    mFieldIns(&regVal, cStatusIsolateMask, cStatusIsolateShift, enable);
    Write(self, 1, regAddr, regVal);

    return cAtOk;
    }

static eAtRet MdioAutoNegEnable(AtMdio self, eBool enable)
    {
    uint32 regAddr = cStatusAckReg;
    uint32 regVal = Read(self, 1, regAddr);

    mRegFieldSet(regVal, cStatusAutoNeg, (enable) ? 1 : 0);
    Write(self, 1, regAddr, regVal);

    return cAtOk;
    }

static void OverrideAtMdio(AtMdio self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtMdioOverride, self->methods, sizeof(m_AtMdioOverride));

        m_AtMdioOverride.Write = Write;
        m_AtMdioOverride.Read = Read;
        }

    self->methods = &m_AtMdioOverride;
    }

static void Override(AtMdio self)
    {
    OverrideAtMdio(self);
    }

static uint32 DefaultTimeoutMs(void)
    {
    return 20;
    }

static AtMdio ObjectInit(AtMdio self, uint32 baseAddress, AtHal hal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super initialization */
    if (AtMdioObjectInit(self, baseAddress, hal) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    /* Set default timeout mdio */
    mThis(self)->retryInMs = DefaultTimeoutMs();

    return self;
    }

AtMdio AtMdioSgmiiNew(uint32 baseAddress, AtHal hal)
    {
    AtMdio newMdio = AtOsalMemAlloc(ObjectSize());
    if (ObjectInit(newMdio, baseAddress, hal) == NULL)
        {
        AtMdioDelete(newMdio);
        return NULL;
        }

    return newMdio;
    }

eAtRet AtMdioSgmiiIsolateControlEnable(AtMdio self, eBool enable)
    {
    if (self)
        return MdioIsolateBitCfg(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet AtMdioSgmiiAutoNegEnable(AtMdio self, eBool enable)
    {
    if (self)
        return MdioAutoNegEnable(self, enable);
    return cAtErrorNullPointer;
    }

void AtMdioSgmiiTimeoutMsSet(AtMdio self, uint32 timeoutMs)
    {
    if (self)
        mThis(self)->retryInMs = timeoutMs;
    }

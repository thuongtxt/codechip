/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform - HAL
 *
 * File        : hal.c
 *
 * Created Date: Jul 31, 2012
 *
 * Author      : namnn
 *
 * Description : Hardware Abstraction Layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalDefault.h"
#include "AtOsal.h"
#include "atclib.h"
#include "AtList.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalid32BitValue 0xDEADCAFE
#define cInvalid16BitValue 0xDEAD
#define cTimeOutReadValue  0xC0CAC0CA

/*--------------------------- Macros -----------------------------------------*/
/* IMPORTAN: The previous version does not have mNoLockWrite and mNoLockRead,
 * their contents are put in AtHalNoLockWrite/AtHalNoLockRead correspondingly.
 * And also in previous version AtHalWrite/AtHalRead also call
 * AtHalNoLockWrite/AtHalNoLockRead.
 *
 * AtHalWrite/AtHalRead are use at everywhere when hardware access takes
 * place, they are used in none interrupt context and AtHalNoLockWrite/AtHalNoLockRead
 * are used for interrupt context.
 *
 * To reduce context switching, contents of AtHalNoLockWrite/AtHalNoLockRead are
 * now moved the following macros and they are all used in
 * AtHalNoLockWrite/AtHalNoLockRead and AtHalWrite/AtHalRead. So AtHalWrite/AtHalRead
 * do not need to call AtHalNoLockWrite/AtHalNoLockRead.
 */
#define mNoLockWrite(self, address, _value)                                    \
    if (!self->writeDisabled)                                                  \
        {                                                                      \
        self->methods->Write(self, address, _value);                           \
        self->numWrite += 1;                                                   \
        if (AtHalWriteShown(self))                                             \
            {                                                                  \
            AtPrintc(cSevWarning, "Write ");                                   \
            AtPrintc(cSevNormal, "0x%08x, value 0x%08x (FPGA at %s)\r\n", address, _value, AtHalToString(self)); \
            }                                                                  \
        }

#define mAtHalListenerCall(self, callback, params...)                          \
    do  {                                                                      \
        tAtHalEventListener *allListeners;                                     \
        uint32 i;                                                              \
                                                                               \
        if ((self->allEventListeners == NULL))                                 \
            return;                                                            \
                                                                               \
        allListeners = AllListenersGet(self);                                  \
                                                                               \
        AtOsalMutexLock(allListeners->listenersLock);                          \
        for (i = 0; i < AtListLengthGet(allListeners->listeners); i++)         \
            {                                                                  \
            tEventListener *listener = (tEventListener *)((void*)AtListObjectGet(allListeners->listeners, i)); \
            if (listener->callbacks->callback)                                 \
                listener->callbacks->callback(self, listener->listener, ##params);\
            }                                                                  \
        AtOsalMutexUnLock(allListeners->listenersLock);                        \
    } while (0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalEventListener
    {
    AtList listeners;
    AtOsalMutex listenersLock;
    }tAtHalEventListener;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddressGet(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    return cInvalid32BitValue;
    }

static uint32 DirectOamOffsetGet(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    return cInvalid32BitValue;
    }

static void AllListenersDelete(AtHal self)
    {
    tAtHalEventListener *allListeners = self->allEventListeners;
    void *listener = NULL;

    if (allListeners == NULL)
        return;

    while ((listener = AtListObjectRemoveAtIndex(allListeners->listeners, 0)) != NULL)
        AtOsalMemFree(listener);

    AtObjectDelete((AtObject)allListeners->listeners);
    AtOsalMutexDestroy(allListeners->listenersLock);
    AtOsalMemFree(allListeners);
    self->allEventListeners = NULL;
    }

static void Delete(AtHal self)
    {
    AllListenersDelete(self);
    AtOsalMemFree(self);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
	AtUnused(value);
	AtUnused(address);
	AtUnused(self);
    /* Concrete must do */
    }

static uint32 Read(AtHal self, uint32 address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Concrete must do */
    return cInvalid32BitValue;
    }

static void Write16(AtHal self, uint32 address, uint16 value)
    {
	AtUnused(value);
	AtUnused(address);
	AtUnused(self);
    /* Concrete must do */
    }

static uint16 Read16(AtHal self, uint32 address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Concrete must do */
    return cInvalid16BitValue;
    }

static void Lock(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    }

static void UnLock(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    }

static void StateRestore(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    }

static void StateSave(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    }

static void IsrContextEnter(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    }

static void IsrContextExit(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    }

static eBool MemTest(AtHal self)
    {
	AtUnused(self);
    /* Concrete must do */
    return cAtFalse;
    }

static eBool RegistersTest(AtHal self, uint32 *regList, uint32 regNum, uint32 mask)
    {
    AtUnused(self);
    AtUnused(regList);
    AtUnused(regNum);
    AtUnused(mask);

    /* Concrete must do */
    return cAtFalse;
    }
    
static void Debug(AtHal self)
    {
    uint32 value;

    AtPrintc(cSevNormal, "HAL: %s\r\n", AtHalToString(self));

    AtPrintc(cSevNormal, "* Normal read enabled: ");
    AtPrintc(self->readDisabled ? cSevCritical : cSevInfo, "%s\r\n", self->readDisabled ? "no" : "yes");
    AtPrintc(cSevNormal, "* Normal write enabled: ");
    AtPrintc(self->writeDisabled ? cSevCritical : cSevInfo, "%s\r\n", self->writeDisabled ? "no" : "yes");

    mMethodsGet(self)->ShowAccessStatistic(self);

    value = self->numOverRangeReadAddress;
    AtPrintc(cSevNormal, "* Read address over range: ");
    AtPrintc(value ? cSevCritical : cSevInfo, "%u operations\r\n", value);

    value = self->invalidReadAddress;
    AtPrintc(cSevNormal, "* Number of read return 0xCAFECAFE: ");
    AtPrintc(value ? cSevCritical : cSevInfo, "%u operations\r\n", value);

    value = self->timeoutReadAddress;
    AtPrintc(cSevNormal, "* Number of read return 0xC0CAC0CA: ");
    AtPrintc(value ? cSevCritical : cSevInfo, "%u operations\r\n", value);

    value = self->numOverRangeWriteAddress;
    AtPrintc(cSevNormal, "* Write address over range: ");
    AtPrintc(value ? cSevCritical : cSevInfo, "%u operations\r\n", value);

    AtPrintc(cSevNormal, "* Indirect access: %s\r\n", AtHalIndirectAccessIsEnabled(self) ? "enable" : "disable");

    AtPrintc(cSevNormal, "* Access time : %d (ms)\r\n", AtHalSlowAccessTimeUsGet(self));
    AtPrintc(cSevNormal, "* Caching     : %s\r\n", AtHalCacheIsEnabled(self) ? "enabled" : "disabled");

    /* Clear debug statistic */
    self->numOverRangeReadAddress  = 0;
    self->numOverRangeWriteAddress = 0;
    self->invalidReadAddress = 0;
    self->timeoutReadAddress = 0;
    self->numReads  = 0;
    self->numWrite  = 0;

    }

static const char* ToString(AtHal self)
    {
	AtUnused(self);
    return NULL;
    }

static tAtHalEventListener *AllListenersCreate(void)
    {
    tAtHalEventListener *listener = AtOsalMemAlloc(sizeof(tAtHalEventListener));
    if (listener == NULL)
        return NULL;

    listener->listeners = AtListCreate(0);
    listener->listenersLock = AtOsalMutexCreate();
    return listener;
    }

static tAtHalEventListener *AllListenersGet(AtHal self)
    {
    if (self->allEventListeners == NULL)
        self->allEventListeners = AllListenersCreate();
    return self->allEventListeners;
    }

static AtList Listeners(AtHal self)
    {
    tAtHalEventListener *allEventListeners = AllListenersGet(self);
    if (allEventListeners == NULL)
        return NULL;

    return allEventListeners->listeners;
    }

static tEventListener *EventListenerCreate(void* listener, const tAtHalListener *callbacks)
    {
    uint32 memorySize = sizeof(tEventListener);
    tEventListener *newListener = AtOsalMemAlloc(memorySize);
    if (newListener == NULL)
        return NULL;

    /* Save listener information */
    newListener->callbacks = callbacks;
    newListener->listener  = listener;

    return newListener;
    }

static eBool IsFail(AtHal self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Reset(AtHal self)
    {
    AtUnused(self);
    return;
    }

static AtHal LiuHalGet(AtHal self)
    {
	AtUnused(self);
    return NULL;
    }

static void NotifyRegisterDidRead(AtHal self, uint32 address, uint32 value)
    {
    mAtHalListenerCall(self, DidReadRegister, address, value);
    }

static void NotifyRegisterDidWrite(AtHal self, uint32 address, uint32 value)
    {
    mAtHalListenerCall(self, DidWriteRegister, address, value);
    }

static void DidReadEnableNotify(AtHal self, eBool enabled)
    {
    mAtHalListenerCall(self, DidReadEnable, enabled);
    }

static void DidWriteEnableNotify(AtHal self, eBool enabled)
    {
    mAtHalListenerCall(self, DidWriteEnable, enabled);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHal);
    }

static uint32 MaxAddress(AtHal self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static void IndirectAccessEnable(AtHal self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    }

static eBool IndirectAccessIsEnabled(AtHal self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NoLockReadForce(AtHal self, uint32 address, eBool forced)
    {
    uint32 value;

    if (!forced)
        return 0;

    value = self->methods->Read(self, address);
    self->numReads += 1;
    if (AtHalReadShown(self))
        {
        AtPrintc(cSevInfo, "Read  ");
        AtPrintc(cSevNormal, "0x%08x, value 0x%08x (FPGA at %s)\r\n", address, value, AtHalToString(self));
        }

    /* Need to cache fail operations */
    switch (value)
        {
        case cInvalidReadValue:
            self->invalidReadAddress = self->invalidReadAddress + 1;
            break;
        case cTimeOutReadValue:
            self->timeoutReadAddress = self->timeoutReadAddress + 1;
            break;
        default:
            break;
        }

    return value;
    }

static uint32 ReadForce(AtHal self, uint32 address, eBool forced)
    {
    uint32 value = 0;

    if (address > AtHalMaxAddress(self))
        {
        self->numOverRangeReadAddress = self->numOverRangeReadAddress + 1;
        return 0xDEADCAFE;
        }

    self->methods->Lock(self);
    value = NoLockReadForce(self, address, forced);
    self->methods->UnLock(self);

    NotifyRegisterDidRead(self, address, value);

    return value;
    }

static eBool IsSlow(AtHal self)
    {
    return self->slowDown;
    }

static void ShowAccessStatistic(AtHal self)
    {
    AtPrintc(cSevNormal, "* Read: %d (registers)\r\n", self->numReads);
    AtPrintc(cSevNormal, "* Write: %d (registers)\r\n", self->numWrite);
    AtPrintc(cSevNormal, "* Total read/write: %d (registers)\r\n", self->numReads + self->numWrite);
    }

static eBool TwoListenersAreIdentical(const tAtHalListener *me, const tAtHalListener *other)
    {
    if ((me->ReadFail         != other->ReadFail)        ||
        (me->WriteFail        != other->WriteFail)       ||
        (me->DidReadRegister  != other->DidReadRegister) ||
        (me->DidWriteRegister != other->DidWriteRegister))
        return cAtFalse;

    return cAtTrue;
    }

static tEventListener *EventListenerFind(AtHal self, void* listener, const tAtHalListener *callbacks, uint32 *listenerIndex)
    {
    uint32 i;
    tAtHalEventListener *allListeners = AllListenersGet(self);

    if (allListeners == NULL)
        return NULL;

    for (i = 0; i < AtListLengthGet(allListeners->listeners); i++)
        {
        tEventListener *aListener = (tEventListener *)((void*)(AtListObjectGet(allListeners->listeners, i)));

        if (aListener->listener != listener)
            continue;

        if (TwoListenersAreIdentical(aListener->callbacks, callbacks))
            {
            if (listenerIndex)
                *listenerIndex = i;
            return aListener;
            }
        }

    return NULL;
    }

static void ListenerAdd(AtHal self, void *listener, const tAtHalListener *callbacks)
    {
    tAtHalEventListener *allListeners = AllListenersGet(self);

    /* Already add, ignore */
    if (EventListenerFind(self, listener, callbacks, NULL))
        return;

    AtOsalMutexLock(allListeners->listenersLock);
    AtListObjectAdd(allListeners->listeners, (AtObject)EventListenerCreate(listener, callbacks));
    AtOsalMutexUnLock(allListeners->listenersLock);
    }

static void ListenerRemove(AtHal self, void *listener, const tAtHalListener *callbacks)
    {
    tAtHalEventListener *allListeners = AllListenersGet(self);
    uint32 listenerIndex;
    tEventListener* eventListener;

    eventListener = EventListenerFind(self, listener, callbacks, &listenerIndex);
    if (eventListener)
        {
        AtOsalMutexLock(allListeners->listenersLock);
        AtListObjectRemoveAtIndex(allListeners->listeners, listenerIndex);
        AtOsalMemFree(eventListener);
        AtOsalMutexUnLock(allListeners->listenersLock);
        }
    }

static void MethodsInit(AtHal self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.BaseAddressGet     = BaseAddressGet;
        m_methods.DirectOamOffsetGet = DirectOamOffsetGet;
        m_methods.Delete             = Delete;
        m_methods.Write              = Write;
        m_methods.Read               = Read;
        m_methods.Write16            = Write16;
        m_methods.Read16             = Read16;
        m_methods.Lock               = Lock;
        m_methods.UnLock             = UnLock;
        m_methods.StateRestore       = StateRestore;
        m_methods.StateSave          = StateSave;
        m_methods.IsrContextEnter    = IsrContextEnter;
        m_methods.IsrContextExit     = IsrContextExit;
        m_methods.MemTest            = MemTest;
        m_methods.Debug              = Debug;
        m_methods.ToString           = ToString;
        m_methods.IsFail             = IsFail;
        m_methods.Reset              = Reset;
        m_methods.LiuHalGet          = LiuHalGet;
        m_methods.MaxAddress         = MaxAddress;
        m_methods.IsSlow             = IsSlow;
        m_methods.IndirectAccessEnable    = IndirectAccessEnable;
        m_methods.IndirectAccessIsEnabled = IndirectAccessIsEnabled;
        m_methods.RegistersTest           = RegistersTest;
        m_methods.ShowAccessStatistic     = ShowAccessStatistic;
        }

    self->methods = &m_methods;
    }

void AtHalNoLockWrite(AtHal self, uint32 address, uint32 value)
    {
    if (self)
        {
        mNoLockWrite(self, address, value);
        NotifyRegisterDidWrite(self, address, value);
        }
    }

uint32 AtHalNoLockRead(AtHal self, uint32 address)
    {
    uint32 value = 0;

    if (self == NULL)
        return 0xCAFECAFE;

    value = NoLockReadForce(self, address, !self->readDisabled);
    NotifyRegisterDidRead(self, address, value);
    return value;
    }

void AtHalNoLockWrite16(AtHal self, uint32 address, uint16 value)
    {
    /* Cannot write a NULL HAL */
    if ((self == NULL) || self->writeDisabled)
        return;

    self->methods->Write16(self, address, value);

    /* Show write detail */
    if (AtHalWriteShown(self))
        {
        AtPrintc(cSevWarning, "Write ");
        AtPrintc(cSevNormal, "0x%08x, value 0x%08x (FPGA at %s)\r\n", address, value, AtHalToString(self));
        }
    }

uint16 AtHalNoLockRead16(AtHal self, uint32 address)
    {
    uint16 value = 0;

    /* Cannot read a NULL HAL */
    if ((self == NULL) || self->readDisabled)
        return 0;

    /* Read its register then show detail */
    value = self->methods->Read16(self, address);
    if (AtHalReadShown(self))
        {
        AtPrintc(cSevInfo, "Read  ");
        AtPrintc(cSevNormal, "0x%08x, value 0x%08x (FPGA at %s)\r\n", address, value, AtHalToString(self));
        }

    return value;
    }

void AtHalDebug(AtHal self)
    {
    if (self)
        self->methods->Debug(self);
    }

AtHal AtHalObjectInit(AtHal self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtHalWriteFailNotify(AtHal self)
    {
    if (self == NULL)
        return;

    mAtHalListenerCall(self, WriteFail);
    }

void AtHalReadFailNotify(AtHal self)
    {
    if (self == NULL)
        return;

    mAtHalListenerCall(self, ReadFail);
    }

void *AtHalAllEventListeners(AtHal self)
    {
    if (self)
        return Listeners(self);
    return NULL;
    }

void AtHalSlowDown(AtHal self, eBool slowDown)
    {
    self->slowDown = slowDown;
    }

eBool AtHalIsSlow(AtHal self)
    {
    if (self)
        return mMethodsGet(self)->IsSlow(self);
    return cAtFalse;
    }

eBool AtHalIsSlowDown(AtHal self)
    {
    return self->slowDown;
    }

void AtHalIndirectAccessEnable(AtHal self, eBool enable)
    {
    if (self)
        mMethodsGet(self)->IndirectAccessEnable(self, enable);
    }

eBool AtHalIndirectAccessIsEnabled(AtHal self)
    {
    if (self)
        return mMethodsGet(self)->IndirectAccessIsEnabled(self);
    return cAtFalse;
    }

eBool AtHalReadShown(AtHal self)
    {
    return (eBool)(self ? self->showRead : cAtFalse);
    }

eBool AtHalWriteShown(AtHal self)
    {
    return (eBool)(self ? self->showWrite : cAtFalse);
    }

uint32 AtHalNumReadGet(AtHal self)
    {
    return (self == NULL) ? 0 : self->numReads;
    }

uint32 AtHalNumWriteGet(AtHal self)
    {
    return (self == NULL) ? 0 : self->numWrite;
    }

void AtHalNumReadSet(AtHal self, uint32 number)
    {
    if (self != NULL)
        self->numReads = number;
    }

void AtHalNumWriteSet(AtHal self, uint32 number)
    {
    if (self != NULL)
        self->numWrite = number;
    }

void AtHalSlowAccessTimeUsSet(AtHal self, uint32 accessTimeUs)
    {
    if (self)
        self->accessTimeUs = accessTimeUs;
    }

uint32 AtHalSlowAccessTimeUsGet(AtHal self)
    {
    if (self)
        return self->accessTimeUs;
    return 0;
    }

void AtHalCacheEnable(AtHal self, eBool enabled)
    {
    if (self)
        self->cachingEnabled = enabled;
    }

eBool AtHalCacheIsEnabled(AtHal self)
    {
    if (self)
        return self->cachingEnabled;
    return cAtFalse;
    }

void AtHalCachedHalSimSet(AtHal self, AtHal cachedHalSim)
    {
    if (self)
        self->cachedHalSim = cachedHalSim;
    }

AtHal AtHalCachedHalSimGet(AtHal self)
    {
    if (self)
        return self->cachedHalSim;
    return NULL;
    }

void AtHalCachedSimulate(AtHal self, eBool simulated)
    {
    if (self)
        self->cachingSimulated = simulated;
    }

eBool AtHalCachedIsSimulated(AtHal self)
    {
    if (self)
        return self->cachingSimulated;
    return cAtFalse;
    }

/**
 * @addtogroup AtHal
 * @{
 */

/**
 * Create new default HAL
 *
 * @param baseAddress Base address
 *
 * @return Default HAL
 */
AtHal AtHalNew(uint32 baseAddress)
    {
    return AtHalDefaultNew(baseAddress);
    }

/**
 * Delete a HAL
 *
 * @param self This HAL
 */
void AtHalDelete(AtHal self)
    {
    if (self)
        self->methods->Delete(self);
    }

/**
 * Write a register
 *
 * @param self This HAL
 * @param address Local address of device
 * @param value Written value
 */
void AtHalWrite(AtHal self, uint32 address, uint32 value)
    {
    if (self == NULL)
        return;

    if (address > AtHalMaxAddress(self))
        {
        self->numOverRangeWriteAddress = self->numOverRangeWriteAddress + 1;
        return;
        }

    self->methods->Lock(self);
    mNoLockWrite(self, address, value);
    self->methods->UnLock(self);

    NotifyRegisterDidWrite(self, address, value);
    }

/**
 * Write a register with value 16bit
 *
 * @param self This HAL
 * @param address Local address of device
 * @param value Written value
 */
void AtHalWrite16(AtHal self, uint32 address, uint16 value)
    {
    if (self == NULL)
        return;

    self->methods->Lock(self);
    AtHalNoLockWrite16(self, address, value);
    self->methods->UnLock(self);
    }

/**
 * Get base address of HAL
 *
 * @param self This HAL
 *
 * @return Base address
 */
uint32 AtHalBaseAddressGet(AtHal self)
    {
    if (self)
        return self->methods->BaseAddressGet(self);

    return 0;
    }

/**
 * Get HAL description
 *
 * @param self This HAL
 *
 * @return HAL description
 */
const char *AtHalToString(AtHal self)
    {
    if (self)
        return self->methods->ToString(self);

    return "NULL";
    }

/**
 * Read a register
 *
 * @param self This HAL
 * @param address Local address of device
 *
 * @return Register value
 */
uint32 AtHalRead(AtHal self, uint32 address)
    {
    if (self == NULL)
        return 0xCAFECAFE;

    return ReadForce(self, address, !self->readDisabled);
    }

/**
 * Read a register with 16bit data
 *
 * @param self This HAL
 * @param address Local address of device
 *
 * @return Register value
 */
uint16 AtHalRead16(AtHal self, uint32 address)
    {
    uint16 value;

    if (self == NULL)
        return 0xCAFE;

    self->methods->Lock(self);
    value = AtHalNoLockRead16(self, address);
    self->methods->UnLock(self);

    return value;
    }

/**
 * Get methods
 * @param self
 * @return
 */
const tAtHalMethods *AtHalMethodsGet(AtHal self)
    {
    return self->methods;
    }

/**
 * Set methods
 * @param self
 * @param methods
 */
void AtHalMethodsSet(AtHal self, const tAtHalMethods *methods)
    {
    self->methods = methods;
    }

/**
 * Enable displaying reading detail
 *
 * @param self This HAL
 * @param enable Enable/disable displaying reading detail
 */
void AtHalShowRead(AtHal self, eBool enable)
    {
    self->showRead = enable;
    }

/**
 * Enable displaying writing detail
 *
 * @param self This HAL
 * @param enable Enable/disable displaying writing detail
 */
void AtHalShowWrite(AtHal self, eBool enable)
    {
    self->showWrite = enable;
    }

/**
 * Save current state
 *
 * @param self This HAL
 */
void AtHalStateSave(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->StateSave == NULL)
        return;

    self->methods->StateSave(self);
    }

/**
 * Restore previous internal state
 *
 * @param self This HAL
 */
void AtHalStateRestore(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->StateRestore == NULL)
        return;

    self->methods->StateRestore(self);
    }

/**
 * To enter ISR context
 *
 * @param self This HAL
 */
void AtHalIsrContextEnter(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->IsrContextEnter == NULL)
        return;

    self->methods->IsrContextEnter(self);
    }

/**
 * Exit ISR context, back to normal context
 *
 * @param self This HAL
 */
void AtHalIsrContextExit(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->IsrContextExit == NULL)
        return;

    self->methods->IsrContextExit(self);
    }

/**
 * Lock HAL to prevent other read/write accesses
 *
 * @param self This HAL
 */
void AtHalLock(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->Lock)
        self->methods->Lock(self);
    }

/**
 * Unlock HAL.
 *
 * @param self This HAL
 */
void AtHalUnLock(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->UnLock)
        self->methods->UnLock(self);
    }

/**
 * Test HAL
 *
 * @param self This HAL
 * @return cAtTrue if success, cAtFalse if fail
 */
eBool AtHalDiagMemTest(AtHal self)
    {
    if (self == NULL)
        return cAtFalse;

    if (self->methods->MemTest == NULL)
        return cAtTrue;

    return self->methods->MemTest(self);
    }

/**
 * Enable or disable write operation.
 *
 * @param self This HAL
 * @param enable cAtTrue to enable, cAtFalse to disable
 */
void AtHalWriteOperationEnable(AtHal self, eBool enable)
    {
    if (self == NULL)
        return;
    self->writeDisabled = enable ? cAtFalse : cAtTrue;
    DidWriteEnableNotify(self, enable);
    }

/**
 * Check if write operation is enabled or not
 *
 * @param self This HAL
 * @return cAtTrue if enabled, cAtFalse if disabled
 */
eBool AtHalWriteOperationIsEnabled(AtHal self)
    {
    if (self)
        return self->writeDisabled ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

/**
 * Enable or disable read operation.
 *
 * @param self This HAL
 * @param enable cAtTrue to enable, cAtFalse to disable
 */
void AtHalReadOperationEnable(AtHal self, eBool enable)
    {
    if (self == NULL)
        return;
    self->readDisabled = enable ? cAtFalse : cAtTrue;
    DidReadEnableNotify(self, enable);
    }

/**
 * Check if read operation is enabled or not
 *
 * @param self This HAL
 * @return cAtTrue if enabled, cAtFalse if disabled
 */
eBool AtHalReadOperationIsEnabled(AtHal self)
    {
    if (self)
        return self->readDisabled ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

/**
 * Enable or disable debug operation.
 *
 * @param self This HAL
 * @param enable cAtTrue to enable, cAtFalse to disable
 */
void AtHalDebugEnable(AtHal self, eBool enable)
    {
    if (self == NULL)
        return;
    self->debugDisabled = enable ? cAtFalse : cAtTrue;
    }

/**
 * Check if HAL debug is enabled or not
 *
 * @param self This HAL
 * @return cAtTrue if debugging is enabled, cAtFalse if disabled
 */
eBool AtHalDebugIsEnabled(AtHal self)
    {
    if (self)
        return self->debugDisabled ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

/**
 * Add a listener
 *
 * @param self This HAL
 * @param listener Listener
 * @param callbacks Callbacks
 */
void AtHalListenerAdd(AtHal self, void *listener, const tAtHalListener *callbacks)
    {
    if (self && callbacks)
        ListenerAdd(self, listener, callbacks);
    }

/**
 * Remove a listener
 *
 * @param self This HAL
 * @param listener Listener
 * @param callbacks Callbacks
 */
void AtHalListenerRemove(AtHal self, void *listener, const tAtHalListener *callbacks)
    {
    if (self && callbacks)
        ListenerRemove(self, listener, callbacks);
    }

/**
 * Directly write a register
 *
 * @param self This HAL
 * @param address Register address
 * @param value Value to write
 */
void AtHalDirectWrite(AtHal self, uint32 address, uint32 value)
    {
    if (self == NULL)
        return;

    self->methods->DirectWrite(self, address, value);
    }

/**
 * Directly read a register
 *
 * @param self This HAL
 * @param address Register address
 *
 * @return Register value
 */
uint32 AtHalDirectRead(AtHal self, uint32 address)
    {
    if (self == NULL)
        return cInvalid32BitValue;
    return self->methods->DirectRead(self, address);
    }

/**
 * Check if HAL is in failure state
 *
 * @param self This HAL.
 * @retval cAtTrue if HAL is fail
 * @retval cAtFalse if HAL is working normally
 *
 * @note When HAL is in failure state, read/write actions make no affect and 0 is
 *       returned for read operation. There is only one way to force HAL exit
 *       this state is calling AtHalReset() API.
 */
eBool AtHalIsFail(AtHal self)
    {
    if (self)
        return self->methods->IsFail(self);
    return cAtFalse;
    }

/**
 * To explicitly reset HAL and bring it to working state.
 *
 * @param self This HAL.
 */
void AtHalReset(AtHal self)
    {
    if (self)
        self->methods->Reset(self);
    }

/**
 * Get LIU HAL to configure LIU
 *
 * @param self This HAL
 *
 * @return LIU HAL if supported or NULL if not supported.
 */
AtHal AtHalLiuHalGet(AtHal self)
    {
    if (self == NULL)
        return NULL;
    return self->methods->LiuHalGet(self);
    }

/**
 * Get max address
 *
 * @param self This HAL
 * @return Max address
 */
uint32 AtHalMaxAddress(AtHal self)
    {
    if (self == NULL)
    return 0x0;

    if (self->maxAddress)
        return self->maxAddress;

    return mMethodsGet(self)->MaxAddress(self);
    }

/**
 * Explicitly specify max address
 *
 * @param self This HAL
 * @param maxAddress Max address
 */
void AtHalMaxAddressSet(AtHal self, uint32 maxAddress)
    {
    if (self)
        self->maxAddress = maxAddress;
    }

/**
 * Test a register or a range of registers
 *
 * @param self This HAL
 * @param regList Tested register list
 * @param regNum Number of tested registers
 * @param mask Tested mask
 *
* @return cAtTrue if passed, cAtFalse if fail
 */
eBool AtHalDiagRegistersTest(AtHal self, uint32 *regList, uint32 regNum, uint32 mask)
    {
    if (self == NULL)
        return cAtFalse;
    return self->methods->RegistersTest(self, regList, regNum, mask);
    }
    
/**
 * @}
 */

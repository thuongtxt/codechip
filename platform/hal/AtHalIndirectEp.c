/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalIndirectEp.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : EP6 C1 V2 - HAL implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "atclib.h"
#include "AtHalIndirectEpInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cInvalidReadValue        0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtHalIndirectEp)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalIndirectEpMethods m_methods;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super's implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

static const uint32 m_retryTimes = 600000;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalIndirectEp);
    }

static void IsrContextEnter(AtHal self)
    {
    mThis(self)->isInIsrContext = 1;
    }

static void IsrContextExit(AtHal self)
    {
    mThis(self)->isInIsrContext = 0;
    }

static eBool IsControlRegister(AtHalIndirectEp self, uint32 address)
    {
    return ((address == self->methods->RegisterIndirectControl(self)) ||
            (address == self->methods->RegisterIndirectData(self)));
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    uint32 regVal;
    volatile uint32 elapseTime = 0;
    AtHalIndirectEp epHal = mThis(self);
    uint32 controlRegister;

    if (mHalFail(self))
        return;

    /* Write control registers */
    if (IsControlRegister(mThis(self), address))
        {
        m_AtHalMethods->DirectWrite(self, address, value);
        return;
        }

    /* Write other registes */
    self->methods->DirectWrite(self, epHal->methods->RegisterIndirectData(epHal), value);

    /* Make write request */
    regVal  = address; /* Bit 30 is 0 for read action */
    regVal |= cBit31;  /* Bit 31 is to make request */
    controlRegister = epHal->methods->RegisterIndirectControl(epHal);
    self->methods->DirectWrite(self, controlRegister, regVal);

    /* Wait for finish, only do this when it has not */
    regVal = self->methods->DirectRead(self, controlRegister);
    if ((regVal & cBit31) == 0)
        {
        mHalFailureReset(self);
        return;
        }

    /* Timeout waiting for hardware finish job */
    while (elapseTime < m_retryTimes)
        {
        /* Check if hardware done */
        regVal = self->methods->DirectRead(self, controlRegister);
        if ((regVal & cBit31) == 0)
            {
            if (mThis(self)->maxRetry < elapseTime)
                mThis(self)->maxRetry = elapseTime;
            mHalFailureReset(self);
            return;
            }

        /* Retry */
        elapseTime = elapseTime + 1;
        }

    /* Timeout, cancel this progress */
    self->methods->DirectWrite(self, controlRegister, 0);
    AtPrintc(cSevCritical, "ERROR: Indirect write 0x%x with value 0x%x fail (FPGA at 0x%08x)\n",
             address, value, AtHalDefaultBaseAddress(self));
    AtHalWriteFailNotify(self);
    mHalFailureHandle(self);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 regVal;
    volatile uint32 elapseTime = 0;
    AtHalIndirectEp epHal = mThis(self);
    uint32 controlRegister;

    if (mHalFail(self))
        return 0x0;

    /* Read a control register */
    if (IsControlRegister(mThis(self), address))
        return self->methods->DirectRead(self, address);

    /* Make a read request for normal register */
    regVal  = address;
    regVal |= cBit30; /* For read action */
    regVal |= cBit31; /* To make request */
    controlRegister = epHal->methods->RegisterIndirectControl(epHal);
    self->methods->DirectWrite(self, controlRegister, regVal);

    /* Hardware finishes its job, return its value */
    if ((self->methods->DirectRead(self, controlRegister) & cBit31) == 0)
        {
        regVal = self->methods->DirectRead(self, epHal->methods->RegisterIndirectData(epHal));
        if ((regVal == cInvalidReadValue) && AtHalDebugIsEnabled(self))
            AtPrintc(cSevWarning, "(%s, %d) Read 0x%08x, value 0x%08x (FPGA at 0x%08x)\n",
                     __FILE__, __LINE__,
                     address, regVal,
                     AtHalDefaultBaseAddress(self));
        mHalFailureReset(self);
        return regVal;
        }

    /* Timeout waiting for it */
    while(elapseTime < m_retryTimes)
        {
        /* Done, return hardware value */
        if ((self->methods->DirectRead(self, controlRegister) & cBit31) == 0)
            {
            regVal = self->methods->DirectRead(self, epHal->methods->RegisterIndirectData(epHal));
            if ((regVal == cInvalidReadValue) && AtHalDebugIsEnabled(self))
                AtPrintc(cSevWarning, "(%s, %d) Read 0x%08x, value 0x%08x (FPGA at 0x%08x)\n",
                         __FILE__, __LINE__,
                         address, regVal,
                         AtHalDefaultBaseAddress(self));

            if (mThis(self)->maxRetry < elapseTime)
                mThis(self)->maxRetry = elapseTime;

            mHalFailureReset(self);

            return regVal;
            }

        /* Calculate elapse time */
        elapseTime = elapseTime + 1;
        }

    /* Timeout, cancel this action and return an invalid value */
    self->methods->DirectWrite(self, controlRegister, 0);
    AtPrintc(cSevCritical, "ERROR: Indirect read 0x%x timeout (FPGA at 0x%08x)\n", address, AtHalDefaultBaseAddress(self));
    AtHalReadFailNotify(self);
    mHalFailureHandle(self);

    return cInvalidReadValue;
    }

static uint8 IndirectIsInProgress(uint32 controlReg)
    {
    return (controlReg & cBit31) ? 1 : 0;
    }

static void IndirectCtrlRegisterRead(AtHal self)
    {
    AtHalIndirectEp halIndirect = mThis(self);
    uint32 registerIndirectControl = halIndirect->methods->RegisterIndirectControl(halIndirect);
    uint32 registerIndirectData = halIndirect->methods->RegisterIndirectData(halIndirect);
    halIndirect->indirectControl = self->methods->DirectRead(self, registerIndirectControl);
    halIndirect->indirectData = self->methods->DirectRead(self, registerIndirectData);
    }

/* Save and restore indirect control registers */
static void StateSave(AtHal self)
    {
    uint32 timeOut;
    AtHalIndirectEp halIndirect = mThis(self);
    uint32 controlReg;

    IndirectCtrlRegisterRead(self);

    /* If HW is NOT being request, save and return */
    if (!IndirectIsInProgress(halIndirect->indirectControl))
        return;

    /* HW is being requested, need to wait until HW finish before save content */
    /* Timeout waiting for hardware finish job */
    timeOut = 0;
    while (timeOut < m_retryTimes)
        {
        uint32 registerIndirectControl = halIndirect->methods->RegisterIndirectControl(halIndirect);
        uint32 registerIndirectData = halIndirect->methods->RegisterIndirectData(halIndirect);

        /* Check if hardware done, save and return */
        controlReg = self->methods->DirectRead(self, registerIndirectControl);
        if (!IndirectIsInProgress(controlReg))
            {
            /* Read again to get new values */
            halIndirect->indirectControl = controlReg;
            halIndirect->indirectData = self->methods->DirectRead(self, registerIndirectData);
            return;
            }

        /* Calculate elapse time */
        timeOut = timeOut + 1;
        }
    }

static void StateRestore(AtHal self)
    {
    AtHalIndirectEp epHal = mThis(self);
    uint32 registerIndirectControl = epHal->methods->RegisterIndirectControl(epHal);
    uint32 registerIndirectData = epHal->methods->RegisterIndirectData(epHal);
    self->methods->DirectWrite(self, registerIndirectData, epHal->indirectData);
    self->methods->DirectWrite(self, registerIndirectControl, epHal->indirectControl);
    }

static void Debug(AtHal self)
    {
    if (mHalFail(self))
        {
        AtPrintc(cSevCritical, "HAL is fail\r\n");
        return;
        }

    AtPrintc(cSevNormal, "maxRetry : %u(times)\r\n", mThis(self)->maxRetry);
    mThis(self)->maxRetry = 0;
    }

static uint32 RegisterIndirectControl(AtHalIndirectEp self)
    {
	AtUnused(self);
    return 0xF00002;
    }

static uint32 RegisterIndirectData(AtHalIndirectEp self)
    {
	AtUnused(self);
    return 0xF00003;
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[2];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], mThis(self)->methods->RegisterIndirectControl(mThis(self)));
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], mThis(self)->methods->RegisterIndirectData(mThis(self)));

    return regsToTest;
    }

static uint32 DataBusMask(AtHal self)
    {
	AtUnused(self);
    return cBit23_0;
    }

static uint32 TestPattern(AtHal hal)
    {
	AtUnused(hal);
    return 0xAAAAAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
	AtUnused(self);
    return 0x555555;
    }

static eBool IndirectAccessIsEnabled(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read            = Read;
        m_AtHalOverride.Write           = Write;
        m_AtHalOverride.StateSave       = StateSave;
        m_AtHalOverride.StateRestore    = StateRestore;
        m_AtHalOverride.IsrContextExit  = IsrContextExit;
        m_AtHalOverride.IsrContextEnter = IsrContextEnter;
        m_AtHalOverride.Debug           = Debug;
        m_AtHalOverride.IndirectAccessIsEnabled = IndirectAccessIsEnabled;
        }

    self->methods = &m_AtHalOverride;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault hal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, hal->methods, sizeof(m_AtHalDefaultOverride));

        /* Override read/write function */
        m_AtHalDefaultOverride.RegsToTest       = RegsToTest;
        m_AtHalDefaultOverride.DataBusMask      = DataBusMask;
        m_AtHalDefaultOverride.TestPattern      = TestPattern;
        m_AtHalDefaultOverride.TestAntiPattern  = TestAntiPattern;
        }

    hal->methods = &m_AtHalDefaultOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static void MethodsInit(AtHal self)
    {
    AtHalIndirectEp halIndirect = (AtHalIndirectEp)self;
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.RegisterIndirectControl = RegisterIndirectControl;
        m_methods.RegisterIndirectData    = RegisterIndirectData;
        }

    halIndirect->methods = &m_methods;
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalIndirectEpNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

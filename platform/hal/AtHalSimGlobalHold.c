/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtHalSimGlobalHold.c
 *
 * Created Date: May 24, 2013
 *
 * Description : HAL simulation for product that use global hold registers
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation datum */
static uint8 m_methodsInit = 0;
static tAtHalSimMethods m_AtHalSimOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultHoldRegsForReadOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static const uint32 holdRegistersCore1[] = {0x21, 0x22, 0x23};
    static const uint32 holdRegistersCore2[] = {0x21 | cBit24, 0x22 | cBit24, 0x23 | cBit24};
    static const uint32 numHoldRegs = 3;
	AtUnused(self);

    *holdRegs = (address & cBit24) ? holdRegistersCore2 : holdRegistersCore1;
    return numHoldRegs;
    }

static uint32 DefaultHoldRegsForWriteOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static const uint32 holdRegistersCore1[] = {0x11, 0x12, 0x13};
    static const uint32 holdRegistersCore2[] = {0x11 | cBit24, 0x12 | cBit24, 0x13 | cBit24};
    static const uint32 numHoldRegs = 3;
	AtUnused(self);

    *holdRegs = (address & cBit24) ? holdRegistersCore2 : holdRegistersCore1;

    return numHoldRegs;
    }

static uint32 HoldRegsForReadOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (self->readHoldRegisters == NULL)
        return DefaultHoldRegsForReadOfLongReg(self, address, holdRegs);

    *holdRegs = self->readHoldRegisters;
    return self->numHoldRegs;
    }

static uint32 HoldRegsForWriteOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (self->writeHoldRegisters == NULL)
        return DefaultHoldRegsForWriteOfLongReg(self, address, holdRegs);

    *holdRegs = self->writeHoldRegisters;
    return self->numHoldRegs;
    }

static void SaveHoldRegisters(AtHal self, uint32 *readHoldRegisters, uint32 *writeHoldRegisters, uint16 numHoldRegisters)
    {
    uint8 i;
    AtHalSim hal = (AtHalSim)self;

    if ((readHoldRegisters == NULL) || (writeHoldRegisters == NULL))
        return;

    if ((hal->readHoldRegisters) || (hal->writeHoldRegisters))
        return;

    hal->readHoldRegisters  = AtOsalMemAlloc(numHoldRegisters * sizeof(uint32));
    hal->writeHoldRegisters = AtOsalMemAlloc(numHoldRegisters * sizeof(uint32));

    for (i = 0; i < numHoldRegisters; i++)
        {
        hal->readHoldRegisters[i]  = readHoldRegisters[i];
        hal->writeHoldRegisters[i] = writeHoldRegisters[i];
        }

    hal->numHoldRegs = numHoldRegisters;
    }

static void OverrideAtHalSim(AtHal self)
    {
    AtHalSim hal = (AtHalSim)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalSimOverride, hal->methods, sizeof(m_AtHalSimOverride));
        m_AtHalSimOverride.HoldRegsForReadOfLongReg  = HoldRegsForReadOfLongReg;
        m_AtHalSimOverride.HoldRegsForWriteOfLongReg = HoldRegsForWriteOfLongReg;
        }

    AtHalSimMethodsSet(hal, &m_AtHalSimOverride);
    }

static void Override(AtHal self)
    {
    OverrideAtHalSim(self);
    }

AtHal AtHalSimGlobalHoldObjectInit(AtHal self, uint32 *readHoldRegisters, uint32 *writeHoldRegisters, uint16 numHoldRegisters, uint32 sizeInMbyte)
    {
    if (AtHalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    SaveHoldRegisters(self, readHoldRegisters, writeHoldRegisters, numHoldRegisters);
    return self;
    }

AtHal AtHalSimGlobalHoldWithHoldRegistersNew(uint32 *readHoldRegisters, uint32 *writeHoldRegisters, uint16 numHoldRegisters, uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(sizeof(tAtHalSim));
    if (hal == NULL)
        return NULL;

    return AtHalSimGlobalHoldObjectInit(hal, readHoldRegisters, writeHoldRegisters, numHoldRegisters, sizeInMbyte);
    }

AtHal AtHalSimGlobalHoldNew(uint32 sizeInMbyte)
    {
    return AtHalSimGlobalHoldWithHoldRegistersNew(NULL, NULL, 0, sizeInMbyte);
    }

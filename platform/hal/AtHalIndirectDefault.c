/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalIndirectDefault.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : Indirect 24-bit address
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalIndirectDefaultInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Indirect registers */
#define cInvalidReadValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

/* Direct read/write */
#define mThis(self_) ((AtHalIndirectDefault)self_)
#define mRealAddress(self, address) (AtSize)(((address) << 1) + ((AtHalDefault)self)->baseAddress)
#define mDirectWrite(self_, address_, value_) (self_)->methods->DirectWrite(self_, address_, value_)
#define mDirectRead(self_, address_) (self_)->methods->DirectRead(self_, address_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalIndirectDefaultMethods m_methods;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
AtHal AtHalIndirect24BitNew(uint32 baseAddress);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalIndirectDefault);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    uint16 value;

    value = (uint16)(*((volatile uint16 *)mRealAddress(self, address)));
    mAsm("sync");

    return value;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    *((volatile uint16 *)mRealAddress(self, address)) = (uint16)value;
    mAsm("sync");
    }

static uint8 IsDirectRegister(AtHal self, uint32 address)
    {
    if ((address == mThis(self)->indirectControl1Address) ||
        (address == mThis(self)->indirectControl2Address) ||
        (address == mThis(self)->indirectData1Address)    ||
        (address == mThis(self)->indirectData2Address))
        return cAtTrue;

    /* Additional register */
    return ((address >= 0xFFF00) && (address <= 0xFFFFF));
    }

static void ShowControlRegisters(AtHal self)
    {
    AtPrintc(cSevNormal, "IndirectData1   : 0x%08x\r\n", mThis(self)->indirectData1Address);
    AtPrintc(cSevNormal, "IndirectData2   : 0x%08x\r\n", mThis(self)->indirectData2Address);
    AtPrintc(cSevNormal, "IndirectControl1: 0x%08x\r\n", mThis(self)->indirectControl1Address);
    AtPrintc(cSevNormal, "IndirectControl2: 0x%08x\r\n", mThis(self)->indirectControl2Address);
    }

static void Debug(AtHal self)
    {
    AtPrintc(cSevNormal, "AddressMask     : 0x%08x\r\n", mThis(self)->addressMask);
    mThis(self)->methods->ShowControlRegisters(self);
    AtPrintc(cSevNormal, "retryTimes      : %u(times)\r\n", mThis(self)->retryTimes);
    AtPrintc(cSevNormal, "maxRetryTimes   : %u(times)\r\n", mThis(self)->maxRetryTimes);
    AtPrintc(cSevNormal, "readTimeout     : %u\r\n", mThis(self)->readTimeoutCounter);
    AtPrintc(cSevNormal, "writeTimeout    : %u\r\n", mThis(self)->writeTimeoutCounter);
    if (mHalFail(self))
        AtPrintc(cSevCritical, "HAL is fail\r\n");

    mThis(self)->maxRetryTimes       = 0;
    mThis(self)->readTimeoutCounter  = 0;
    mThis(self)->writeTimeoutCounter = 0;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    uint16 tmpValue;
    uint32 regValue = 0;
    uint32 addressMask = mThis(self)->addressMask;
    volatile uint32 retryTimes = 0;

    if (mHalFail(self))
        return;

    /* Write data */
    address = address & addressMask;
    if (mThis(self)->methods->IsDirectRegister(self, address))
        {
        mDirectWrite(self, address, value);
        return;
        }

    mDirectWrite(self, mThis(self)->indirectData1Address, (value & cBit15_0));
    mFieldGet(value, cBit31_16, 16, uint16, &tmpValue);
    mDirectWrite(self, mThis(self)->indirectData2Address, tmpValue);

    /* Write address */
    mDirectWrite(self, mThis(self)->indirectControl1Address, address & cBit15_0);

    addressMask = addressMask & cBit31_16;
    mFieldGet(address, addressMask, 16, uint16, &tmpValue);
    regValue = tmpValue;
    mFieldIns(&regValue, cBit15, 15, 1);
    mDirectWrite(self, mThis(self)->indirectControl2Address, regValue);

    /* Check if hardware done */
    while (retryTimes < mThis(self)->retryTimes)
        {
        regValue = mDirectRead(self, mThis(self)->indirectControl2Address);
        if ((regValue & cBit15) == 0)
            {
            if (mThis(self)->maxRetryTimes < retryTimes)
                mThis(self)->maxRetryTimes = retryTimes;

            /* It backs to work */
            mHalFailureReset(self);

            return;
            }

        /* Or retry */
        retryTimes = retryTimes + 1;
        }

    AtPrintc(cSevCritical, "(%s, %d) ERROR: Write 0x%08x, value 0x%08x timeout\n", __FILE__, __LINE__, address, value);
    mThis(self)->writeTimeoutCounter = mThis(self)->writeTimeoutCounter + 1;
    AtHalWriteFailNotify(self);
    mHalFailureHandle(self);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint16 tmpValue;
    uint32 regVal = 0;
    uint32 data = 0;
    uint32 addrMask = mThis(self)->addressMask;
    volatile uint32 retryTimes = 0;

    if (mHalFail(self))
        return 0x0;

    /* Read a control register */
    address = address & addrMask;
    if (mThis(self)->methods->IsDirectRegister(self, address))
        return mDirectRead(self, address);

    /* Make read request */
    mDirectWrite(self, mThis(self)->indirectControl1Address, address & cBit15_0);
    regVal |= cBit15;
    regVal |= cBit14;
    addrMask = addrMask & cBit31_16;
    mFieldGet(address, addrMask, 16, uint16, &tmpValue);
    mFieldIns(&regVal, addrMask >> 16, 0, tmpValue);
    mDirectWrite(self, mThis(self)->indirectControl2Address, regVal);

    /* Wait for hardware ready */
    while (retryTimes < mThis(self)->retryTimes)
        {
        /* If hardware is ready, return data */
        regVal = mDirectRead(self, mThis(self)->indirectControl2Address);
        if ((regVal & cBit15) == 0)
            {
            regVal = mDirectRead(self, mThis(self)->indirectData1Address);
            data |= (regVal & cBit15_0);
            mFieldIns(&data, cBit31_16, 16, mDirectRead(self, mThis(self)->indirectData2Address));

            if (mThis(self)->maxRetryTimes < retryTimes)
                mThis(self)->maxRetryTimes = retryTimes;

            /* It backs to work */
            mHalFailureReset(self);

            return data;
            }

        /* Or retry */
        retryTimes = retryTimes + 1;
        }

    /* Timeout */
    AtPrintc(cSevCritical, "(%s, %d) ERROR: Read 0x%08x timeout\n", __FILE__, __LINE__, address);
    mThis(self)->readTimeoutCounter = mThis(self)->readTimeoutCounter + 1;
    AtHalReadFailNotify(self);
    mHalFailureHandle(self);

    return cInvalidReadValue;
    }

static uint8 IndirectIsInProgress(AtHal self)
    {
    uint32 requestStatus = mDirectRead(self, mThis(self)->indirectControl2Address);
    return (requestStatus & cBit15) ? 1 : 0;
    }

static void IndirectCtrlRegisterSave(AtHal self)
    {
    AtHalIndirectDefault halIndirect = (AtHalIndirectDefault)self;

    halIndirect->indirectControl1 = mDirectRead(self, mThis(self)->indirectControl1Address);
    halIndirect->indirectControl2 = mDirectRead(self, mThis(self)->indirectControl2Address);
    halIndirect->indirectData1    = mDirectRead(self, mThis(self)->indirectData1Address);
    halIndirect->indirectData2    = mDirectRead(self, mThis(self)->indirectData2Address);
    }

static void StateSave(AtHal self)
    {
    volatile uint32 elapseTime = 0;

    /* If HW is NOT being requested, save and return */
    if (!mThis(self)->methods->IndirectIsInProgress(self))
        {
        mThis(self)->methods->IndirectCtrlRegisterSave(self);
        return;
        }

    /* HW is being requested, need to wait until HW finish before save content */
    /* Timeout waiting for hardware finish job */
    while (elapseTime < mThis(self)->retryTimes)
        {
        /* Check if hardware done, save and return */
        if (!mThis(self)->methods->IndirectIsInProgress(self))
            {
            mThis(self)->methods->IndirectCtrlRegisterSave(self);
            return;
            }

        /* Retry */
        elapseTime = elapseTime + 1;
        }
    }

static void StateRestore(AtHal self)
    {
    AtHalIndirectDefault halIndirect = (AtHalIndirectDefault)self;

    mDirectWrite(self, mThis(self)->indirectData1Address,    halIndirect->indirectData1);
    mDirectWrite(self, mThis(self)->indirectData2Address,    halIndirect->indirectData2);
    mDirectWrite(self, mThis(self)->indirectControl1Address, halIndirect->indirectControl1);
    mDirectWrite(self, mThis(self)->indirectControl2Address, halIndirect->indirectControl2);
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[3];
    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], mThis(self)->indirectControl1Address);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], mThis(self)->indirectData1Address);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], mThis(self)->indirectData2Address);

    return regsToTest;
    }

static uint32 DirectOamOffsetGet(AtHal self)
    {
	AtUnused(self);
    return 0xFFFD0;
    }

static eBool IndirectAccessIsEnabled(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read                    = Read;
        m_AtHalOverride.Write                   = Write;
        m_AtHalOverride.DirectRead              = DirectRead;
        m_AtHalOverride.DirectWrite             = DirectWrite;
        m_AtHalOverride.StateSave               = StateSave;
        m_AtHalOverride.StateRestore            = StateRestore;
        m_AtHalOverride.DirectOamOffsetGet      = DirectOamOffsetGet;
        m_AtHalOverride.Debug                   = Debug;
        m_AtHalOverride.IndirectAccessIsEnabled = IndirectAccessIsEnabled;
        }

    self->methods = &m_AtHalOverride;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.RegsToTest  = RegsToTest;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static void MethodsInit(AtHalIndirectDefault self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.IsDirectRegister         = IsDirectRegister;
        m_methods.IndirectIsInProgress     = IndirectIsInProgress;
        m_methods.IndirectCtrlRegisterSave = IndirectCtrlRegisterSave;
        m_methods.ShowControlRegisters     = ShowControlRegisters;
        }

    self->methods = &m_methods;
    }

static void Setup(AtHal self)
    {
    mThis(self)->retryTimes    = 1000000;
    mThis(self)->maxRetryTimes = 0;
    mThis(self)->addressMask   = cBit24_0;
    mThis(self)->indirectControl1Address = 0xfff02;
    mThis(self)->indirectControl2Address = 0xfff03;
    mThis(self)->indirectData1Address    = 0xfff04;
    mThis(self)->indirectData2Address    = 0xfff05;
    }

AtHal AtHalIndirectDefaultObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((AtHalIndirectDefault)self);
    m_methodsInit = 1;

    /* Default setup */
    Setup(self);

    return self;
    }

AtHal AtHalIndirectDefaultNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return AtHalIndirectDefaultObjectInit(newHal, baseAddress);
    }

/* To be compatible with previous projects that are using AtHalIndirect24BitNew API */
AtHal AtHalIndirect24BitNew(uint32 baseAddress)
    {
    return AtHalIndirectDefaultNew(baseAddress);
    }

void AtHalIndirectDefaultAddressMaskSet(AtHal self, uint32 addressMask)
    {
    mThis(self)->addressMask = addressMask;
    }

void AtHalIndirectDefaultIndirectControl1AddressSet(AtHal self, uint32 address)
    {
    mThis(self)->indirectControl1Address = address;
    }

void AtHalIndirectDefaultIndirectControl2AddressSet(AtHal self, uint32 address)
    {
    mThis(self)->indirectControl2Address = address;
    }

void AtHalIndirectDefaultIndirectData1AddressSet(AtHal self, uint32 address)
    {
    mThis(self)->indirectData1Address = address;
    }

void AtHalIndirectDefaultIndirectData2AddressSet(AtHal self, uint32 address)
    {
    mThis(self)->indirectData2Address = address;
    }

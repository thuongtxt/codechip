/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHalInternal.h
 * 
 * Created Date: Jan 27, 2014
 *
 * Description : HAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALINTERNAL_H_
#define _ATHALINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mHalMethods(self) ((self)->methods)

/*--------------------------- Typedefs ---------------------------------------*/
/* @brief HAL methods */
typedef struct tAtHalMethods
    {
    uint32 (*BaseAddressGet)(AtHal self);
    uint32 (*DirectOamOffsetGet)(AtHal self);
    uint32 (*MaxAddress)(AtHal self);

    /* Delete HAL */
    void (*Delete)(AtHal self);

    /* Read/write 32-bit registers */
    void (*Write)(AtHal self, uint32 address, uint32 value);
    uint32 (*Read)(AtHal self, uint32 address);

    /* Read/write 16-bit registers */
    void (*Write16)(AtHal self, uint32 address, uint16 value);
    uint16 (*Read16)(AtHal self, uint32 address);

    /* Direct read/write */
    uint32 (*DirectRead)(AtHal self, uint32 address);
    void (*DirectWrite)(AtHal self, uint32 address, uint32 value);

    /* Lock/unlock */
    void (*Lock)(AtHal self);
    void (*UnLock)(AtHal self);

    /* For interrupt */
    void (*StateRestore)(AtHal self);
    void (*StateSave)(AtHal self);
    void (*IsrContextEnter)(AtHal self);
    void (*IsrContextExit)(AtHal self);

    /* For testing and debugging */
    eBool (*MemTest)(AtHal self);
    eBool (*RegistersTest)(AtHal self, uint32 *regList, uint32 regNum, uint32 mask);
    void (*Debug)(AtHal self);
    void (*ShowAccessStatistic)(AtHal self);
    const char* (*ToString)(AtHal self);
    eBool (*IsSlow)(AtHal self);

    /* To deal with HAL failure */
    eBool (*IsFail)(AtHal self);
    void (*Reset)(AtHal self);

    /* To control indirect read/write */
    void (*IndirectAccessEnable)(AtHal self, eBool enable);
    eBool (*IndirectAccessIsEnabled)(AtHal self);

    /* Get HAL to configure LIU */
    AtHal (*LiuHalGet)(AtHal self);
    }tAtHalMethods;

/* @brief HAL class representation */
typedef struct tAtHal
    {
    const tAtHalMethods *methods; /* HAL methods */

    /* Private data */
    uint8 showRead;  /* To show read action */
    uint8 showWrite; /* To show read action */
    eBool writeDisabled;
    eBool readDisabled;
    eBool debugDisabled;
    uint32 numOverRangeReadAddress;
    uint32 numOverRangeWriteAddress;
    uint32 timeoutReadAddress; /* Read that return 0xC0CAC0CA */
    uint32 invalidReadAddress; /* Read that return 0xCAFECAFE */
    uint32 maxAddress;
    uint32 numReads;
    uint32 numWrite;

    /* Caching */
    eBool slowDown;
    uint32 accessTimeUs;
    AtHal cachedHalSim;
    eBool cachingEnabled;
    eBool cachingSimulated;

    /* Listeners */
    void *allEventListeners; /* Keep it be (void*) to free circular dependencies */
    }tAtHal;

typedef struct tEventListener
    {
    const tAtHalListener *callbacks;
    void *listener;
    }tEventListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalObjectInit(AtHal self);
uint32 AtHalNumReadGet(AtHal self);
uint32 AtHalNumWriteGet(AtHal self);
void AtHalNumReadSet(AtHal self, uint32 number);
void AtHalNumWriteSet(AtHal self, uint32 number);

/* Concrete HALs which are used for LAB testing */
AtHal AtHalNew(uint32 baseAddress);
AtHal AtHalSimGlobalHoldWithHoldRegistersNew(uint32 *readHoldRegisters, uint32 *writeHoldRegisters, uint16 numHoldRegisters, uint32 sizeInMbyte);
AtHal AtHalSimE1New(uint32 sizeInMbyte);
AtHal AtHalIndirectEpNew(uint32 baseAddress);
AtHal AtHalIndirect16BitNew(uint32 baseAddress);
AtHal AtHalEp5New(uint32 baseAddress);
AtHal AtHalEp6New(void);

/* Access registers methods with no locking. */
void AtHalNoLockWrite(AtHal self, uint32 address, uint32 value);
uint32 AtHalNoLockRead(AtHal self, uint32 address);
void AtHalNoLockWrite16(AtHal self, uint32 address, uint16 value);
uint16 AtHalNoLockRead16(AtHal self, uint32 address);

/* Control read/write operations */
void AtHalWriteOperationEnable(AtHal self, eBool enable);
eBool AtHalWriteOperationIsEnabled(AtHal self);
void AtHalReadOperationEnable(AtHal self, eBool enable);
eBool AtHalReadOperationIsEnabled(AtHal self);

/* Set/Get HAL methods */
void AtHalMethodsSet(AtHal self, const tAtHalMethods *methods);
const tAtHalMethods *AtHalMethodsGet(AtHal self);

/* Save and restore control registers */
void AtHalStateSave(AtHal self);
void AtHalStateRestore(AtHal self);

/* Context switch */
void AtHalIsrContextEnter(AtHal self);
void AtHalIsrContextExit(AtHal self);

/* Lock/unlock */
void AtHalLock(AtHal self);
void AtHalUnLock(AtHal self);

/* Notify */
void AtHalWriteFailNotify(AtHal self);
void AtHalReadFailNotify(AtHal self);
void *AtHalAllEventListeners(AtHal self); /* AtList */

void AtHalIndirectAccessEnable(AtHal self, eBool enable);
eBool AtHalIndirectAccessIsEnabled(AtHal self);

/* For debugging */
eBool AtHalWriteShown(AtHal self);
eBool AtHalReadShown(AtHal self);
eBool AtHalDiagRegistersTest(AtHal self, uint32 *regList, uint32 regNum, uint32 mask);
void AtHalSlowDown(AtHal self, eBool slowDown);
void AtHalSlowAccessTimeUsSet(AtHal self, uint32 accessTimeUs);
uint32 AtHalSlowAccessTimeUsGet(AtHal self);
eBool AtHalIsSlowDown(AtHal self);
eBool AtHalIsSlow(AtHal self);

/* To deal with HAL failure */
eBool AtHalIsFail(AtHal self);
void AtHalReset(AtHal self);

/* Get LIU HAL to configure LIU (Note, not all HALs have this) */
AtHal AtHalLiuHalGet(AtHal self);

/* For caching */
void AtHalCacheEnable(AtHal self, eBool enabled);
eBool AtHalCacheIsEnabled(AtHal self);
void AtHalCachedHalSimSet(AtHal self, AtHal cachedHalSim);
AtHal AtHalCachedHalSimGet(AtHal self);
void AtHalCachedSimulate(AtHal self, eBool simulated);
eBool AtHalCachedIsSimulated(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalMemoryMapperLinux.c
 *
 * Created Date: Jul 15, 2013
 *
 * Description : Linux memory mapper
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "atclib.h"
#include "AtHalMemoryMapperInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalMemoryMapperLinux
    {
    tAtHalMemoryMapper super;
    }tAtHalMemoryMapperLinux;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMemoryMapperMethods m_AtHalMemoryMapperOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Map(AtHalMemoryMapper self)
    {
    void *address;
    int32 fd;

    fd = open("/dev/mem", O_RDWR);
    if (fd < 0)
        {
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cAtError;
        }

    address = mmap(NULL, self->memorySize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (off_t)(self->offset));
    if (address == ((void *)-1))
        {
        close(fd);
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cAtError;
        }

    self->baseAddress = (uint32)((size_t)address);
    close(fd);

    return cAtOk;

    }

static eAtRet UnMap(AtHalMemoryMapper self)
    {
    int32 fd;
    eAtRet ret;

    fd = open("/dev/mem", O_RDWR);
    if (fd == -1)
        return cAtOk;

    ret = (munmap((void *)((size_t)self->baseAddress), self->memorySize) < 0) ? cAtError : cAtOk;

    close(fd);

    return ret;
    }

static void OverrideAtHalMemoryMapper(AtHalMemoryMapper self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalMemoryMapperOverride, self->methods, sizeof(m_AtHalMemoryMapperOverride));

        m_AtHalMemoryMapperOverride.Map   = Map;
        m_AtHalMemoryMapperOverride.UnMap = UnMap;
        }

    self->methods = &m_AtHalMemoryMapperOverride;
    }

static void Override(AtHalMemoryMapper self)
    {
    OverrideAtHalMemoryMapper(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalMemoryMapperLinux);
    }

static AtHalMemoryMapper ObjectInit(AtHalMemoryMapper self, uint32 offset, uint32 memorySize)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtHalMemoryMapperObjectInit(self, offset, memorySize) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHalMemoryMapper AtHalMemoryMapperLinuxNew(uint32 offset, uint32 memorySize)
    {
    AtHalMemoryMapper newMapper = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newMapper, offset, memorySize);
    }

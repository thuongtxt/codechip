/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtHalIndirect16Bit16Bit.c
 *
 * Created Date: Nov 16, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalDefault.h"
#include "AtOsal.h"
#include "commacro.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegisterIndirectControl1 0x7FFF03
#define cRegisterIndirectControl2 0x7FFF02
#define cRegisterIndirectData  0x7FFF04

#define cThaReadTopHoldBit31_16Adr      0x0F0041
#define cThaReadHoldBit31_16Adr         0x000021
#define cThaWriteTopHoldBit31_16Adr     0x0F0040
#define cThaWriteHoldBit31_16Adr        0x000011

#define cIndrAccessTimeOut 1 /* milisecond */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalIndirect16Bit
    {
    tAtHalDefault super;

    /* Private data may be added in the future */
    }tAtHalIndirect16Bit;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalIndirect16Bit);
    }

static void Indirect16BitsWrite(AtHal self, uint32 address, uint32 value)
    {
    uint32 regVal;
    uint32 timeout_ms;
    tAtOsalCurTime curTime, startTime;

    m_AtHalMethods->Write(self, cRegisterIndirectData, value);

    /* Make write request */
    regVal = (address) & cBit23_0; /* Bit30 is 0, it is write request */
    regVal |= cBit31;              /* To request */
    m_AtHalMethods->Write(self, cRegisterIndirectControl2, regVal & cBit15_0);
    m_AtHalMethods->Write(self, cRegisterIndirectControl1, regVal >> 16);

    /* Check if hardware done so that do not need to wait */
    if ((m_AtHalMethods->Read(self, cRegisterIndirectControl1) & cBit15) == 0)
        return;

    /* Timeout waiting for hardware */
    timeout_ms = 0;
    AtOsalCurTimeGet(&startTime);
    while(timeout_ms < cIndrAccessTimeOut)
        {
        /* Hardware done */
        if ((m_AtHalMethods->Read(self, cRegisterIndirectControl1) & cBit15) == 0)
            return;

        /* Calculate elapse time */
        AtOsalCurTimeGet(&curTime);
        timeout_ms = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Timeout, cancel this action */
    m_AtHalMethods->Write(self, cRegisterIndirectControl1, 0);
    AtPrintc(cSevCritical, "ERROR: Write timeout\n");
    }

static uint32 Indirect16BitsRead(AtHal self, uint32 address)
    {
    uint32 regVal;
    uint32 timeout_ms;
    tAtOsalCurTime curTime, startTime;

    /* Make read request */
    regVal  = (address) & cBit23_0;
    regVal |= cBit30; /* Read */
    regVal |= cBit31; /* Request */
    m_AtHalMethods->Write(self, cRegisterIndirectControl2, regVal & cBit15_0);
    m_AtHalMethods->Write(self, cRegisterIndirectControl1, (regVal >> 16));

    /* Hardware finishes, just return value */
    if ((m_AtHalMethods->Read(self, cRegisterIndirectControl1) & cBit15) == 0)
        return m_AtHalMethods->Read(self, cRegisterIndirectData);

    /* Timeout waiting for it */
    timeout_ms = 0;
    AtOsalCurTimeGet(&startTime);
    while (timeout_ms < cIndrAccessTimeOut)
        {
        /* Hardware finishes, return its data */
        if ((m_AtHalMethods->Read(self, cRegisterIndirectControl1) & cBit15) == 0)
            return m_AtHalMethods->Read(self, cRegisterIndirectData);

        /* Calculate elapse time */
        AtOsalCurTimeGet(&curTime);
        timeout_ms = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Timeout, cancel this action and return an invalid value */
    m_AtHalMethods->Write(self, cRegisterIndirectControl1, 0);
    AtPrintc(cSevCritical, "ERROR: Read timeout\n");

    return cInvalidReadValue;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    if (((address) & cBit23_16) == 0xF0000)
        Indirect16BitsWrite(self, cThaWriteTopHoldBit31_16Adr, (value >> 16));
    else
        Indirect16BitsWrite(self, cThaWriteHoldBit31_16Adr, (value >> 16));

    Indirect16BitsWrite(self, address, (uint16)value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 msb, lsb;

    lsb = Indirect16BitsRead(self, address);
    if ((address & cBit23_16) == 0xF0000)
        msb = Indirect16BitsRead(self, cThaReadTopHoldBit31_16Adr);
    else
        msb = Indirect16BitsRead(self, cThaReadHoldBit31_16Adr);

    return (((uint32)(msb & cBit15_0)) << 16) | (lsb & cBit15_0);
    }

static eBool IndirectAccessIsEnabled(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read  = Read;
        m_AtHalOverride.Write = Write;
        m_AtHalOverride.IndirectAccessIsEnabled = IndirectAccessIsEnabled;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalIndirect16BitNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress);
    }

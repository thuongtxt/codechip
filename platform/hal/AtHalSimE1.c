/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform - HAL
 *
 * File        : AtHalSim16E1.c
 *
 * Created Date: Sep 17, 2012
 *
 * Description : HAL Simulation of 16 E1 FPGA images
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation datum */
static uint8 m_methodsInit = 0;
static tAtHalSimMethods m_AtHalSimOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MpigHoldRegsForReadOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static const uint32 holdRegisters[] = {0x840010, 0x840011, 0x840012, 0x840013};
	AtUnused(self);

    if ((address < 0x840000) || (address > 0x86FFFF))
        return 0;

    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static uint32 HoldRegsOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    uint32 numRegs;

    if ((numRegs = MpigHoldRegsForReadOfLongReg(self, address, holdRegs)) > 0)
        return numRegs;

    /* TODO: Other module */

    return 0;
    }

static uint32 HoldRegsForReadOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    return HoldRegsOfLongReg(self, address, holdRegs);
    }

static uint32 HoldRegsForWriteOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    return HoldRegsOfLongReg(self, address, holdRegs);
    }

static void Override(AtHal self)
    {
    /* Initialize super methods */
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalSimOverride, self->methods, sizeof(m_AtHalSimOverride));
        m_AtHalSimOverride.HoldRegsForReadOfLongReg  = HoldRegsForReadOfLongReg;
        m_AtHalSimOverride.HoldRegsForWriteOfLongReg = HoldRegsForWriteOfLongReg;
        }

    AtHalSimMethodsSet((AtHalSim)self, &m_AtHalSimOverride);
    }

static AtHal ObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    if (AtHalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalSimE1New(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(sizeof(tAtHalSim));
    if (hal == NULL)
        return NULL;

    ObjectInit(hal, sizeInMbyte);
    return hal;
    }

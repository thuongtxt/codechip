/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalTcp.c
 *
 * Created Date: Feb 7, 2015
 *
 * Description : HAL TCP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtList.h"
#include "AtConnection.h"
#include "AtConnectionManager.h"
#include "AtHalTcpInternal.h"
#include "../AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalTcp *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalTcp
    {
    tAtHalDefault super;

    /* Private data */
    AtConnection accessConnection;
    AtConnection eventConnection;
    char *data;
    uint32 length;
    uint32 readCount;
    uint32 writeCount;

    /* Listen task */
    AtTask eventListenTask;
    }tAtHalTcp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DeleteListenTask(AtHal self)
    {
    AtTask task = mThis(self)->eventListenTask;

    if (task == 0)
        return;

    if (AtTaskStop(task) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Stop event listener task fail\r\n");
        return;
        }

    if (AtTaskJoin(task) != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot wait for event listener task from stop\r\n");

    AtTaskManagerTaskDelete(AtOsalTaskManagerGet(), task);
    mThis(self)->eventListenTask = NULL;
    }

static void Delete(AtHal self)
    {
    AtConnectionDelete(mThis(self)->accessConnection);
    DeleteListenTask(self);
    AtConnectionDelete(mThis(self)->eventConnection);

    mThis(self)->accessConnection = NULL;
    mThis(self)->eventConnection  = NULL;

    m_AtHalMethods->Delete(self);
    }

static int Send(AtHal self, void *buffer, uint32 bufferSize)
    {
    return AtConnectionSend(mThis(self)->accessConnection, buffer, bufferSize);
    }

static tAtHalTcpRegisterMessage *SendRegisterRequest(AtHal self, uint32 address, uint32 data, eAtHalTcpMessage type)
    {
    tAtHalTcpRegisterMessage message;
    tAtHalTcpRegisterMessage *response;
    int numBytes;

    /* Make request */
    message.request.messageType = type;
    message.address = address;
    message.data    = data;

    AtHalTcpEncodeMessage((tAtHalTcpMessage *)&message);

    /* Send it */
    numBytes = Send(self, &message, sizeof(message));
    if (numBytes != sizeof(message))
        return NULL;

    /* Wait server response */
    numBytes = AtConnectionReceive(mThis(self)->accessConnection);
    if (numBytes < 0)
        return NULL;

    /* This can be a failure response */
    response = (tAtHalTcpRegisterMessage *)(void *)AtConnectionDataBuffer(mThis(self)->accessConnection);
    AtHalTcpDecodeMessage((tAtHalTcpMessage *)response);
    if (response->request.messageType != type)
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Server failure response\r\n", __FILE__, __LINE__);
        return NULL;
        }

    /* It can be a good message */
    mThis(self)->length = (uint32)numBytes;
    return response;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    tAtHalTcpRegisterMessage *response;

    response = SendRegisterRequest(self, address, value, cAtHalTcpMessageWrite);
    if (response == NULL)
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Write fail\r\n", __FILE__, __LINE__);
        return;
        }

    /* Make sure the response is valid */
    if (mThis(self)->length != sizeof(tAtHalTcpRegisterMessage))
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Server return unknown message with %d bytes\r\n", __FILE__, __LINE__, mThis(self)->length);
        return;
        }

    /* To be more safe */
    if (response->address != address)
        {
        AtPrintc(cSevCritical,
                 "(%s, %d) ERROR: Server return a response of other register. Expect 0x%08x, actual 0x%08x\r\n", __FILE__, __LINE__,
                 address,
                 response->address);
        return;
        }

    mThis(self)->writeCount = mThis(self)->writeCount + 1;
    }

static uint32 Read(AtHal self, uint32 address)
    {
    tAtHalTcpRegisterMessage *response = SendRegisterRequest(self, address, cInvalidReadValue, cAtHalTcpMessageRead);
    if (response == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Read 0x%08x fail, network timeout may happen\r\n", address);
        return cInvalidReadValue;
        }

    /* Make sure the response is valid */
    if (mThis(self)->length != sizeof(tAtHalTcpRegisterMessage))
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Server return unknown message with %d bytes\r\n", __FILE__, __LINE__, mThis(self)->length);
        return cInvalidReadValue;
        }

    /* To be more safe */
    if (response->address != address)
        {
        AtPrintc(cSevCritical,
                 "(%s, %d) ERROR: Server return a value of other register. Expect 0x%08x, actual 0x%08x\r\n", __FILE__, __LINE__,
                 address,
                 response->address);
        return cInvalidReadValue;
        }

    mThis(self)->readCount = mThis(self)->readCount + 1;

    return response->data;
    }

static const char* ToString(AtHal self)
    {
    static char description[64];
    AtSprintf(description, "%s:%d",
              AtConnectionRemoteAddressGet(mThis(self)->accessConnection),
              AtConnectionConnectedPortGet(mThis(self)->accessConnection));
    return description;
    }

static eBool IsRegisterMessage(tAtHalTcpMessage *message)
    {
    eAtHalTcpMessage type = message->messageType;
    if ((type == cAtHalTcpMessageRead) ||
        (type == cAtHalTcpMessageWrite))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet SetupConnection(AtHal self, const char *serverIpAddress, uint16 connectedPort)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    if (connectedPort == 0)
        connectedPort = cAtHalTcpDefaultPort;
    mThis(self)->accessConnection = AtConnectionManagerClientConnectionCreate(manager, serverIpAddress, connectedPort);

    return cAtOk;
    }

static AtConnection CreateEventConnection(AtHal self, uint16 listenPort)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    AtUnused(self);
    return AtConnectionManagerServerConnectionCreate(manager, NULL, listenPort);
    }

static void NotifyInterruptEvent(AtHal self)
    {
    AtList listeners;
    uint32 i;

    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    listeners = AtHalAllEventListeners(self);

    for (i = 0; i < AtListLengthGet(listeners); i++)
        {
        tEventListener *listener = (tEventListener *)AtListObjectGet(listeners, i);
        const tAtHalTcpListener *callbacks = (const tAtHalTcpListener *)listener->callbacks;
        if (callbacks && callbacks->DidReceiveInterruptMessage)
            callbacks->DidReceiveInterruptMessage(self, listener->listener);
        }
    }

static eAtRet ProcessEvent(AtHal self)
    {
    tAtHalTcpMessage *message = (tAtHalTcpMessage *)(void *)mThis(self)->data;

    if (mThis(self)->length < sizeof(tAtHalTcpMessage))
        {
        AtPrintc(cSevCritical, "ERROR: Receive too small message (%d bytes) from server\r\n", mThis(self)->length);
        return cAtErrorHalFail;
        }

    AtHalTcpDecodeMessage(message);
    switch (message->messageType)
        {
        case cAtHalTcpMessageInterrupt:
            NotifyInterruptEvent(self);
            break;
        default:
            AtPrintc(cSevWarning, "Just got unknown message with type %d from server\r\n", message->messageType);
            break;
        }

    return cAtOk;
    }

static void *ListenTaskHandler(void *self)
    {
    if (mThis(self)->eventListenTask == NULL)
        return NULL;

    /* Do this run loop */
    while (1)
        {
        int numBytes;

        AtTaskTestCancel(mThis(self)->eventListenTask);

        numBytes = AtConnectionReceive(mThis(self)->eventConnection);
        if (numBytes)
            {
            mThis(self)->data   = AtConnectionDataBuffer(mThis(self)->eventConnection);
            mThis(self)->length = (uint32)numBytes;
            ProcessEvent(self);
            }
        }

    return NULL;
    }

static eAtRet CreateListenTask(AtHal self)
    {
    if (mThis(self)->eventListenTask)
        return cAtOk;

    if (AtConnectionSetup(mThis(self)->eventConnection) != cAtConnectionOk)
        return cAtErrorHalFail;

    mThis(self)->eventListenTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "HAL TCP", self, ListenTaskHandler);
    if (mThis(self)->eventListenTask == NULL)
        return cAtErrorOsalFail;

    if (AtTaskStart(mThis(self)->eventListenTask) != cAtOk)
        return cAtErrorOsalFail;

    return cAtOk;
    }

static eAtRet EventListen(AtHal self, uint16 listenPort)
    {
    if (mThis(self)->eventConnection)
        return cAtOk;

    if (listenPort == 0)
        listenPort = cAtHalTcpEventListenPort;

    mThis(self)->eventConnection = CreateEventConnection(self, listenPort);
    if (mThis(self)->eventConnection == NULL)
        return cAtErrorRsrcNoAvail;

    return CreateListenTask(self);
    }

static void Debug(AtHal self)
    {
    m_AtHalMethods->Debug(self);

    AtPrintc(cSevNormal, "* Read: %d (registers)\r\n", mThis(self)->readCount);
    AtPrintc(cSevNormal, "* Write: %d (registers)\r\n", mThis(self)->writeCount);
    AtPrintc(cSevNormal, "* Total read/write: %d (registers)\r\n", mThis(self)->readCount + mThis(self)->writeCount);

    mThis(self)->readCount  = 0;
    mThis(self)->writeCount = 0;
    }

static eBool IsSlow(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Delete     = Delete;
        m_AtHalOverride.Read       = Read;
        m_AtHalOverride.Write      = Write;
        m_AtHalOverride.ToString   = ToString;
        m_AtHalOverride.Debug      = Debug;
        m_AtHalOverride.DirectRead = Read;
        m_AtHalOverride.IsSlow     = IsSlow;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalTcp);
    }

static AtHal ObjectInit(AtHal self, const char *serverIpAddress, uint16 connectedPort)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, cBit31_0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    SetupConnection(self, serverIpAddress, connectedPort);

    return self;
    }

AtHal AtHalTcpNew(const char *serverIpAddress, uint16 connectedPort)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newHal, serverIpAddress, connectedPort);
    }

void AtHalTcpEncodeMessage(tAtHalTcpMessage *message)
    {
    if (IsRegisterMessage(message))
        {
        tAtHalTcpRegisterMessage *regMessage = (tAtHalTcpRegisterMessage *)message;
        regMessage->address = AtConnectionManagerHtonl(AtOsalConnectionManager(), regMessage->address);
        regMessage->data    = AtConnectionManagerHtonl(AtOsalConnectionManager(), regMessage->data);
        }

    message->messageType = AtConnectionManagerHtonl(AtOsalConnectionManager(), message->messageType);
    }

void AtHalTcpDecodeMessage(tAtHalTcpMessage *message)
    {
    message->messageType = AtConnectionManagerNtohl(AtOsalConnectionManager(), message->messageType);

    if (IsRegisterMessage(message))
        {
        tAtHalTcpRegisterMessage *regMessage = (tAtHalTcpRegisterMessage *)message;
        regMessage->address = AtConnectionManagerNtohl(AtOsalConnectionManager(), regMessage->address);
        regMessage->data    = AtConnectionManagerNtohl(AtOsalConnectionManager(), regMessage->data);
        }
    }

eAtHalTransport AtHalDefaultTransport(void)
    {
    return cAtHalTransportUdp;
    }

eAtRet AtHalTcpEventListen(AtHal self, uint16 listenPort)
    {
    if (self)
        return EventListen(self, listenPort);
    return cAtErrorNullPointer;
    }

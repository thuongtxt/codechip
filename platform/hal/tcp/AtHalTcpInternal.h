/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalTcpInternal.h
 * 
 * Created Date: Feb 7, 2015
 *
 * Description : HAL TCP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALTCPINTERNAL_H_
#define _ATHALTCPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalTcp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtHalTcpMessage
    {
    cAtHalTcpMessageRead,
    cAtHalTcpMessageWrite,
    cAtHalTcpMessageInterrupt,
    cAtHalTcpMessageFailure
    }eAtHalTcpMessage;

typedef struct tAtHalTcpMessage
    {
    uint32 messageType;
    }tAtHalTcpMessage;

typedef struct tAtHalTcpRegisterMessage
    {
    tAtHalTcpMessage request;
    uint32 address;
    uint32 data;
    }tAtHalTcpRegisterMessage;

typedef enum eAtHalTransport
    {
    cAtHalTransportTcp,
    cAtHalTransportUdp
    }eAtHalTransport;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtHalTcpEncodeMessage(tAtHalTcpMessage *message);
void AtHalTcpDecodeMessage(tAtHalTcpMessage *message);

eAtHalTransport AtHalDefaultTransport(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALTCPINTERNAL_H_ */


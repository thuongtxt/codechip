/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalTcpServer.c
 *
 * Created Date: Feb 7, 2015
 *
 * Description : HAL TCP server
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtCommon.h"
#include "AtOsal.h"
#include "AtHalTcpInternal.h"
#include "AtConnectionManager.h"
#include "AtHalTcpServer.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtHalTcpServer *)self)

/*--------------------------- Macros -----------------------------------------*/
#define mRegMessage(data) ((tAtHalTcpRegisterMessage *)(void *)data)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalTcpServer
    {
    tAtHal super;

    /* Private data */
    AtHal realHal;
    AtConnection accessConnection;
    char *data;
    uint32 length;

    /* For event notification */
    AtConnection eventConnection;
    uint16 eventNotifyPort;
    }tAtHalTcpServer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int Send(AtHal self, void *buffer, uint32 bufferSize)
    {
    return AtConnectionSend(mThis(self)->accessConnection, buffer, bufferSize);
    }

static eAtRet SendFailureResponse(AtHal self)
    {
    tAtHalTcpMessage message = {.messageType = cAtHalTcpMessageFailure};
    int numBytes;

    AtHalTcpEncodeMessage(&message);

    numBytes = Send(self, &message, sizeof(message));
    if (numBytes != sizeof(message))
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Cannot response on failure\r\n", __FILE__, __LINE__);
        return cAtErrorHalFail;
        }

    return cAtOk;
    }

static eAtRet ResponseRegisterAccess(AtHal self, eAtHalTcpMessage type)
    {
    tAtHalTcpMessage *message = (tAtHalTcpMessage *)(void *)mThis(self)->data;
    int numBytes;

    message->messageType = type;
    AtHalTcpEncodeMessage(message);
    numBytes = Send(self, mThis(self)->data, mThis(self)->length);
    if (numBytes != (int)mThis(self)->length)
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: Response fail\r\n", __FILE__, __LINE__);
        return cAtErrorHalFail;
        }

    return cAtOk;
    }

static eAtRet ProcessReadRequest(AtHal self)
    {
    tAtHalTcpRegisterMessage *request = mRegMessage(mThis(self)->data);

    if (mThis(self)->length != sizeof(tAtHalTcpRegisterMessage))
        return SendFailureResponse(self);
    request->data = AtHalRead(mThis(self)->realHal, request->address);
    return ResponseRegisterAccess(self, cAtHalTcpMessageRead);
    }

static eAtRet ProcessWriteRequest(AtHal self)
    {
    tAtHalTcpRegisterMessage *request = mRegMessage(mThis(self)->data);

    if (mThis(self)->length != sizeof(tAtHalTcpRegisterMessage))
        return SendFailureResponse(self);

    AtHalWrite(mThis(self)->realHal, request->address, request->data);
    return ResponseRegisterAccess(self, cAtHalTcpMessageWrite);
    }

static eAtRet ProcessReceiveData(AtHal self, char *dataBuffer, int numBytes)
    {
    tAtHalTcpMessage *request = (tAtHalTcpMessage *)(void *)dataBuffer;

    AtHalTcpDecodeMessage(request);

    mThis(self)->data   = dataBuffer;
    mThis(self)->length = (uint32)numBytes;

    switch (request->messageType)
        {
        case cAtHalTcpMessageRead :
            return ProcessReadRequest(self);

        case cAtHalTcpMessageWrite:
            return ProcessWriteRequest(self);

        default:
            AtPrintc(cSevCritical, "(%s, %d) ERROR: Unknown request type: %d\r\n", __FILE__, __LINE__, request->messageType);
            return SendFailureResponse(self);
        }
    }

static eAtRet Run(AtHal self)
    {
    int numBytes = AtConnectionReceive(mThis(self)->accessConnection);

    /* Client may disconnected. Need to destroy event connection so that it can
     * be recreated next time new client connects. */
    if (numBytes <= 0)
        {
        AtConnectionDelete(mThis(self)->eventConnection);
        mThis(self)->eventConnection = NULL;
        }

    return ProcessReceiveData(self, AtConnectionDataBuffer(mThis(self)->accessConnection), numBytes);
    }

static void Delete(AtHal self)
    {
    AtConnectionDelete(mThis(self)->accessConnection);
    AtConnectionDelete(mThis(self)->eventConnection);

    mThis(self)->accessConnection = NULL;
    mThis(self)->eventConnection  = NULL;

    m_AtHalMethods->Delete(self);
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        m_AtHalOverride.Delete = Delete;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalTcpServer);
    }

static AtConnection AnyConnectionCreate(void)
    {
    static const uint16 cNumPorts = 1024;
    uint16 port_i;
    AtConnectionManager connectionManager = AtOsalConnectionManager();

    for (port_i = 0; port_i < cNumPorts; port_i++)
        {
        uint16 port = (uint16)(cAtHalTcpDefaultPort + port_i);
        AtConnection newConnection = AtConnectionManagerServerConnectionCreate(connectionManager, NULL, port);
        eAtConnectionRet ret = AtConnectionSetup(newConnection);
        if (ret == cAtConnectionOk)
            return newConnection;

        AtConnectionDelete(newConnection);
        }

    return NULL;
    }

static AtConnection SetupConnection(AtHal self, uint16 port)
    {
    eAtConnectionRet ret = cAtConnectionOk;
    AtConnectionManager connectionManager;
    AtConnection newConnection;

    /* If the port is not specified, create any */
    if (port == 0)
        {
        mThis(self)->accessConnection = AnyConnectionCreate();
        return mThis(self)->accessConnection;
        }

    connectionManager = AtOsalConnectionManager();
    newConnection = AtConnectionManagerServerConnectionCreate(connectionManager, NULL, port);
    ret = AtConnectionSetup(newConnection);
    if (ret != cAtConnectionOk)
        {
        AtPrintc(cSevCritical, "ERROR: %s\r\n", AtConnectionErrorDescription(newConnection));
        AtConnectionDelete(newConnection);
        newConnection = NULL;
        }
    mThis(self)->accessConnection = newConnection;

    return mThis(self)->accessConnection;
    }

static AtConnection CreateEventConnection(AtHal self)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    const char *clientIpAddress = AtConnectionRemoteAddressGet(mThis(self)->accessConnection);
    if (clientIpAddress == NULL)
        return NULL;

    return AtConnectionManagerClientConnectionCreate(manager, clientIpAddress, mThis(self)->eventNotifyPort);
    }

static eAtRet InterruptEventSend(AtHal self)
    {
    tAtHalTcpMessage message = {.messageType = cAtHalTcpMessageInterrupt};

    if (mThis(self)->eventConnection == NULL)
        mThis(self)->eventConnection = CreateEventConnection(self);

    /* Ignore if client has not been connected yet */
    if (mThis(self)->eventConnection == NULL)
        return cAtOk;

    /* Encode the message and send */
    AtHalTcpEncodeMessage(&message);
    if (AtConnectionSend(mThis(self)->eventConnection, (char *)&message, sizeof(message)) != sizeof(message))
        return cAtErrorHalFail;

    return cAtOk;
    }

static AtHal ObjectInit(AtHal self, uint16 registerAccessPort, uint16 eventNotifyPort, AtHal realHal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    if (SetupConnection(self, registerAccessPort) == NULL)
        return NULL;

    /* The real HAL */
    mThis(self)->realHal = realHal;

    /* The port that events will be sent */
    if (eventNotifyPort == 0)
        eventNotifyPort = cAtHalTcpEventListenPort;
    mThis(self)->eventNotifyPort = eventNotifyPort;

    return self;
    }

AtHal AtHalTcpServerNew(uint16 registerAccessPort, uint16 eventNotifyPort, AtHal realHal)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());

    if (ObjectInit(newHal, registerAccessPort, eventNotifyPort, realHal))
        return newHal;

    AtHalDelete(newHal);
    return NULL;
    }

eAtRet AtHalTcpServerRun(AtHal self)
    {
    if (self)
        return Run(self);
    return cAtErrorNullPointer;
    }

AtHal AtHalTcpServerRealHal(AtHal self)
    {
    return self ? mThis(self)->realHal : NULL;
    }

eAtRet AtHalTcpServerInterruptEventSend(AtHal self)
    {
    if (self)
        return InterruptEventSend(self);
    return cAtErrorNullPointer;
    }

eBool AtHalTcpServerInterruptEventCanSend(AtHal self)
    {
    if (self == NULL)
        return cAtFalse;

    return mThis(self)->accessConnection ? cAtTrue : cAtFalse;
    }

AtConnection AtHalTcpServerAccessConnection(AtHal self)
    {
    return self ? mThis(self)->accessConnection : NULL;
    }

AtConnection AtHalTcpServerEventConnection(AtHal self)
    {
    return self ? mThis(self)->eventConnection : NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalMemoryMapper.c
 *
 * Created Date: Jul 15, 2013
 *
 * Description : Memory maper
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtHalMemoryMapperInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalMemoryMapperMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Map(AtHalMemoryMapper self)
    {
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet UnMap(AtHalMemoryMapper self)
    {
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static void Delete(AtHalMemoryMapper self)
    {
    AtHalMemoryMapperUnMap(self);
    AtOsalMemFree(self);
    }

static void MethodsInit(AtHalMemoryMapper self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.Map    = Map;
        m_methods.UnMap  = UnMap;
        m_methods.Delete = Delete;
        }

    self->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalMemoryMapper);
    }

AtHalMemoryMapper AtHalMemoryMapperObjectInit(AtHalMemoryMapper self, uint32 offset, uint32 memorySize)
    {
	if (self == NULL)
		return NULL;

    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->offset     = offset;
    self->memorySize = memorySize;

    return self;
    }

void AtHalMemoryMapperDelete(AtHalMemoryMapper self)
    {
    if (self)
        self->methods->Delete(self);
    }

eAtRet AtHalMemoryMapperMap(AtHalMemoryMapper self)
    {
    if (self)
        return self->methods->Map(self);
    return cAtError;
    }

eAtRet AtHalMemoryMapperUnMap(AtHalMemoryMapper self)
    {
    if (self)
        return self->methods->UnMap(self);
    return cAtError;
    }

uint32 AtHalMemoryMapperMemorySize(AtHalMemoryMapper self)
    {
    return self->memorySize;
    }

uint32 AtHalMemoryMapperOffset(AtHalMemoryMapper self)
    {
    return self->offset;
    }

uint32 AtHalMemoryMapperBaseAddress(AtHalMemoryMapper self)
    {
    return self->baseAddress;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDefault.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : Default implementation of HAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtObject.h"
#include "AtHalDefault.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThisHal(self) ((AtHalDefault)self)
#define mBaseAddress(self) (((AtHalDefault)self)->baseAddress)
#define mAddressMask(self) (((AtHalDefault)self)->addressMask)
#define mRealAddress(self, address) ((((address) & mAddressMask(self)) << 2) + mBaseAddress(self))
#define mDirectWrite(self_, address_, value_) (self_)->methods->DirectWrite(self_, address_, value_)
#define mDirectRead(self_, address_) (self_)->methods->DirectRead(self_, address_)

/*--------------------------- Local typedefs ---------------------------------*/
typedef void (*TestWriteFunc)(AtHal hal, uint32 address, uint32 value);
typedef uint32 (*TestReadFunc)(AtHal hal, uint32 address);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation data structure */
static uint8 m_methodsInit = 0;
static tAtHalDefaultMethods m_methods;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtHal self)
    {
    AtOsalMutexDestroy(mThisHal(self)->mutex);
    AtOsalMemFree(mThisHal(self)->testRegValues);
    m_AtHalMethods->Delete(self);
    }

static void Delay(void)
    {
    static uint32 cRepeatTime = 500;
    uint32 i;
    for (i = 0; i < cRepeatTime; i++);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    *((uint32 *)mRealAddress(self, address)) = value;
    if (self->slowDown)
        Delay();
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 regVal = *((uint32 *)mRealAddress(self, address));
    if (self->slowDown)
        Delay();
    if ((regVal == cInvalidReadValue) && AtHalDebugIsEnabled(self))
        {
        AtPrintc(cSevWarning, "(%s, %d) Read 0x%08x, value 0x%08x (FPGA at 0x%08x)\n",
                 __FILE__, __LINE__,
                 address, regVal,
                 AtHalDefaultBaseAddress(self));
        }

    return regVal;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalDefault);
    }

static uint32 BaseAddressGet(AtHal self)
    {
    return ((AtHalDefault)self)->baseAddress;
    }

static char* BaseAddressDescriptionToString(AtHal self, char *buffer)
    {
    AtSprintf(buffer, "0x%x", AtHalBaseAddressGet(self));
    return buffer;
    }

static const char *DescriptionBuild(AtHal self, char *buffer, uint32 bufferSize)
    {
    AtUnused(bufferSize);
    mThisHal(self)->methods->BaseAddressDescriptionToString(self, buffer);
    return buffer;
    }

static const char* ToString(AtHal self)
    {
    uint32 bufferSize;

    if (mThisHal(self)->descriptionIsValid)
        return mThisHal(self)->description;

    bufferSize = sizeof(mThisHal(self)->description);
    AtOsalMemInit(mThisHal(self)->description, 0, bufferSize);
    mMethodsGet(mThisHal(self))->DescriptionBuild(self, mThisHal(self)->description, bufferSize);
    mThisHal(self)->descriptionIsValid = cAtTrue;

    return mThisHal(self)->description;
    }

static uint32 DirectOamOffsetGet(AtHal self)
    {
	AtUnused(self);
    return 0xD00000;
    }

static void Lock(AtHal self)
    {
    AtOsalMutex mutex;

    if (self == NULL)
        return;

    /* Do not need to lock/unlock in ISR context */
    if (mThisHal(self)->isIsrContext)
        return;
	
	/* Let's loss encapsulation to reduce context switching */
    mutex = mThisHal(self)->mutex;
    if (mutex)
        mutex->methods->Lock(mutex);
    }

static void UnLock(AtHal self)
    {
    AtOsalMutex mutex;

    if (self == NULL)
        return;

    /* Do not need to lock/unlock in ISR context */
    if (mThisHal(self)->isIsrContext)
        return;

	/* Let's loss encapsulation to reduce context switching */
    mutex = mThisHal(self)->mutex;
    if (mutex)
        mutex->methods->UnLock(mutex);
    }

static void IsrContextEnter(AtHal self)
    {
    mThisHal(self)->isIsrContext = 1;
    }

static void IsrContextExit(AtHal self)
    {
    mThisHal(self)->isIsrContext = 0;
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
	AtUnused(self);
    *numRegs = 0;
    return NULL;
    }

static AtHalTestRegister RegAtIndex(AtHalTestRegister allRegs, uint32 regIndex)
    {
    return allRegs + regIndex;
    }

static uint32 DataBusMask(AtHal self)
    {
	AtUnused(self);
    return cBit15_0;
    }

static uint32 ReadAddressFromWriteAddress(AtHal hal, uint32 address)
    {
    AtUnused(hal);
    return address;
    }

static uint32 ReadValueFromWriteValue(AtHal hal, uint32 value)
    {
    AtUnused(hal);
    return value;
    }

static void WriteRegTest(AtHal hal, AtHalTestRegister reg, uint32 pattern, TestWriteFunc writeFunc)
    {
    writeFunc(hal, reg->address, pattern & reg->writableMask);
    }

static eBool ReadRegTestAndCompare(AtHal hal, AtHalTestRegister reg, uint32 pattern, TestReadFunc readFunc, uint32 *actualValue)
    {
    uint32 realVal = readFunc(hal, ((AtHalDefault)hal)->methods->ReadAddressFromWriteAddress(hal, reg->address));
    *actualValue = (realVal & reg->writableMask);

    if ((*actualValue) == (pattern & reg->writableMask))
        return cAtTrue;

    return cAtFalse;
    }

static void PrintError(const char* file, int line, AtHalTestRegister reg, uint32 pattern, uint32 readValue)
    {
    AtPrintc(cSevCritical, "(%s, %d) ERROR: Address 0x%08x, Write 0x%08x, Read 0x%08x\r\n",
             file, line,
             reg->address,
             pattern & reg->writableMask,
             readValue);
    }
    
static int DataBusTestOnReg(AtHal hal, AtHalTestRegister reg, TestReadFunc readFunc, TestWriteFunc writeFunc)
    {
    uint32 pattern;
    uint32 readValue;
    uint32 mask = mThisHal(hal)->methods->DataBusMask(hal);

    /* Perform a walking 1's test at the given address. */
    for (pattern = 1; (pattern & mask) != 0; pattern <<= 1)
        {
        /* Write the test pattern. */
        WriteRegTest(hal, reg, pattern, writeFunc);

        /* Read it back (immediately is okay for this test). */
        if (!ReadRegTestAndCompare(hal, reg, pattern, readFunc, &readValue))
            {
            PrintError(__FILE__, __LINE__, reg, pattern, readValue);
            return -1;
            }
        }

    return (0);
    }

static int DataBusTest(AtHal hal, uint32 numRegs, AtHalTestRegister allRegs, TestReadFunc readFunc, TestWriteFunc writeFunc)
    {
    uint32 i;

    for (i = 0; i < numRegs; i++)
        {
        if (DataBusTestOnReg(hal, RegAtIndex(allRegs, i), readFunc, writeFunc) != 0)
            return -1;
        }

    return 0;
    }
    
static int AddressBusTest(AtHal hal, uint32 numRegs, AtHalTestRegister allRegs, TestReadFunc readFunc, TestWriteFunc writeFunc)
    {
    uint32 pattern     = mThisHal(hal)->methods->TestPattern(hal);
    uint32 antipattern = mThisHal(hal)->methods->TestAntiPattern(hal);
    uint32 readValue;
    uint32 offset, testOffset;

    if (numRegs == 0)
        return 0;

    /* Write default pattern */
    for (offset = 0; offset < numRegs; offset++)
        WriteRegTest(hal, RegAtIndex(allRegs, offset), pattern, writeFunc);

    /* Check for address bits stuck high. */
    testOffset = 0;
    WriteRegTest(hal, RegAtIndex(allRegs, testOffset), antipattern, writeFunc);
    for (offset = 1; offset < numRegs; offset++)
        {
        if (!ReadRegTestAndCompare(hal, RegAtIndex(allRegs, offset), pattern, readFunc, &readValue))
            {
            PrintError(__FILE__, __LINE__, RegAtIndex(allRegs, offset), pattern, readValue);
            return -1;
            }
        }
    WriteRegTest(hal, RegAtIndex(allRegs, testOffset), pattern, writeFunc);

    /* Check for address bits stuck low or shorted. */
    for (testOffset = 0; testOffset < numRegs; testOffset++)
        {
        WriteRegTest(hal, RegAtIndex(allRegs, testOffset), antipattern, writeFunc);
        for (offset = 0; offset < numRegs; offset++)
            {
            if ((offset != testOffset) && (!ReadRegTestAndCompare(hal, RegAtIndex(allRegs, offset), pattern, readFunc, &readValue)))
                {
                AtPrintc(cSevCritical, "Written to address 0x%08x: ",
                         RegAtIndex(allRegs, testOffset)->address);
                PrintError(__FILE__, __LINE__, RegAtIndex(allRegs, offset), pattern, readValue);
                return -1;
                }
            }

        WriteRegTest(hal, RegAtIndex(allRegs, testOffset), pattern, writeFunc);
        }

    return 0;
    }

static int DeviceTest(AtHal hal, uint32 numRegs, AtHalTestRegister allRegs, TestReadFunc readFunc, TestWriteFunc writeFunc)
    {
    uint32 offset;
    uint32 pattern;
    uint32 antipattern;
    uint32 readValue;
    uint32 dataMask = mThisHal(hal)->methods->DataBusMask(hal);

    if (numRegs == 0)
        return 0;

    /* Fill memory with a known pattern. */
    for (pattern = 1, offset = 0; offset < numRegs; pattern++, offset++)
        WriteRegTest(hal, RegAtIndex(allRegs, offset), pattern, writeFunc);

    /* Check each location and invert it for the second pass. */
    for (pattern = 1, offset = 0; offset < numRegs; pattern++, offset++)
        {
        if (!ReadRegTestAndCompare(hal, RegAtIndex(allRegs, offset), pattern, readFunc, &readValue))
            {
            PrintError(__FILE__, __LINE__, RegAtIndex(allRegs, offset), pattern, readValue);
            return -1;
            }

        antipattern = (~pattern) & dataMask;
        WriteRegTest(hal, RegAtIndex(allRegs, offset), antipattern, writeFunc);
        }

    /* Check each location for the inverted pattern and zero it. */
    for (pattern = 1, offset = 0; offset < numRegs; pattern++, offset++)
        {
        antipattern = (~pattern) & dataMask;
        if (!ReadRegTestAndCompare(hal, RegAtIndex(allRegs, offset), antipattern, readFunc, &readValue))
            {
            PrintError(__FILE__, __LINE__, RegAtIndex(allRegs, offset), pattern, readValue);
            return -1;
            }
        }

    return 0;
    }

static uint32 *TestRegisterValuesCreate(AtHal self)
    {
    AtHalDefault hal = mThisHal(self);
    uint32 numRegs = 0;
    AtHalTestRegister testRegs = mMethodsGet(hal)->RegsToTest(self, &numRegs);
    uint32 memorySize = numRegs * sizeof(uint32);
    uint32 *testRegValues;

    AtUnused(testRegs);

    testRegValues = AtOsalMemAlloc(memorySize);
    if (testRegValues)
        AtOsalMemInit(testRegValues, 0, memorySize);

    return testRegValues;
    }

static uint32 *TestRegisterValues(AtHal self)
    {
    AtHalDefault hal = mThisHal(self);
    if (hal->testRegValues == NULL)
        hal->testRegValues = TestRegisterValuesCreate(self);
    return hal->testRegValues;
    }

static void SaveTestRegisterValues(AtHal self)
    {
    uint32 numRegs, reg_i;
    AtHalDefault hal = mThisHal(self);
    uint32 *testRegValues = TestRegisterValues(self);
    AtHalTestRegister testRegs = mMethodsGet(hal)->RegsToTest(self, &numRegs);

    for (reg_i = 0; reg_i < numRegs; reg_i++)
        testRegValues[reg_i] = mDirectRead(self, testRegs[reg_i].address);
    }

static void RestoreTestRegisterValues(AtHal self)
    {
    uint32 numRegs, reg_i;
    AtHalDefault hal = mThisHal(self);
    uint32 *testRegValues = TestRegisterValues(self);
    AtHalTestRegister testRegs = mMethodsGet(hal)->RegsToTest(self, &numRegs);

    for (reg_i = 0; reg_i < numRegs; reg_i++)
        mDirectWrite(self, testRegs[reg_i].address, testRegValues[reg_i]);
    }

static eBool NeedRestoreTestRegValues(AtHal self)
    {
    /* Just to keep working logic, need to disable this as default. */
    AtUnused(self);
    return cAtFalse;
    }

static eBool MemTest(AtHal self)
    {
    int ret = 0;
    uint32 numRegs;
    eBool needRestoreRegs = mMethodsGet(mThisHal(self))->NeedRestoreTestRegValues(self);
    AtHalTestRegister allRegs = mMethodsGet(mThisHal(self))->RegsToTest(self, &numRegs);
    TestWriteFunc writeFunc = self->methods->DirectWrite;
    TestReadFunc readFunc = self->methods->DirectRead;

    if (needRestoreRegs)
        SaveTestRegisterValues(self);

    ret |= DataBusTest(self, numRegs, allRegs, readFunc, writeFunc);
    ret |= AddressBusTest(self, numRegs, allRegs, readFunc, writeFunc);
    ret |= DeviceTest(self, numRegs, allRegs, readFunc, writeFunc);

    if (needRestoreRegs)
        RestoreTestRegisterValues(self);

    return (ret < 0) ? cAtFalse : cAtTrue;
    }

static AtHalTestRegister AllTestRegsCreate(uint32 *regList, uint32 regNum, uint32 mask)
    {
    uint32 reg_i;
    uint32 memSize = regNum * sizeof(tAtHalTestRegister);
    AtHalTestRegister allRegs = AtOsalMemAlloc(memSize);

    if (allRegs == NULL)
        return NULL;

    for (reg_i = 0; reg_i < regNum; reg_i++)
        {
        allRegs[reg_i].address = regList[reg_i];
        allRegs[reg_i].writableMask = mask;
        }

    return allRegs;
    }

static void AllTestRegsDelete(AtHalTestRegister allRegs)
    {
    AtOsalMemFree(allRegs);
    }

static eBool RegisterTestReturn(eBool result, AtHalTestRegister allRegs)
    {
    AllTestRegsDelete(allRegs);
    return result;
    }

static eBool RegistersTest(AtHal self, uint32 *regList, uint32 regNum, uint32 mask)
    {
    AtHalTestRegister allRegs = AllTestRegsCreate(regList, regNum, mask);
    TestWriteFunc writeFunc = AtHalWrite;
    TestReadFunc readFunc = AtHalRead;

    if (allRegs == NULL)
        return cAtFalse;

    if (DataBusTest(self, regNum, allRegs, readFunc, writeFunc) != 0)
        return RegisterTestReturn(cAtFalse, allRegs);

    if (AddressBusTest(self, regNum, allRegs, readFunc, writeFunc) != 0)
        return RegisterTestReturn(cAtFalse, allRegs);

    if (DeviceTest(self, regNum, allRegs, readFunc, writeFunc) != 0)
        return RegisterTestReturn(cAtFalse, allRegs);

    return RegisterTestReturn(cAtTrue, allRegs);
    }

static uint32 TestPattern(AtHal hal)
    {
	AtUnused(hal);
    return 0xAAAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
	AtUnused(self);
    return 0x5555;
    }

static eBool IsFail(AtHal self)
    {
    return mHalFail(self);
    }

static void Reset(AtHal self)
    {
    mHalFailureReset(self);
    mThisHal(self)->failureNotified = cAtFalse;
    }

static uint32 MaxAddress(AtHal self)
    {
    AtUnused(self);
    /* Almost products uses 25-bits address */
    return cBit24_0;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Delete             = Delete;
        m_AtHalOverride.Read               = Read;
        m_AtHalOverride.Write              = Write;
        m_AtHalOverride.DirectRead         = Read;
        m_AtHalOverride.DirectWrite        = Write;
        m_AtHalOverride.DirectOamOffsetGet = DirectOamOffsetGet;
        m_AtHalOverride.BaseAddressGet     = BaseAddressGet;
        m_AtHalOverride.Lock               = Lock;
        m_AtHalOverride.UnLock             = UnLock;
        m_AtHalOverride.IsrContextEnter    = IsrContextEnter;
        m_AtHalOverride.IsrContextExit     = IsrContextExit;
        m_AtHalOverride.ToString           = ToString;
        m_AtHalOverride.MemTest            = MemTest;
        m_AtHalOverride.RegistersTest      = RegistersTest;
        m_AtHalOverride.IsFail             = IsFail;
        m_AtHalOverride.Reset              = Reset;
        m_AtHalOverride.MaxAddress         = MaxAddress;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static void MethodsInit(AtHal self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));
        m_methods.BaseAddressDescriptionToString = BaseAddressDescriptionToString;
        m_methods.RegsToTest                  = RegsToTest;
        m_methods.TestPattern                 = TestPattern;
        m_methods.TestAntiPattern             = TestAntiPattern;
        m_methods.DataBusMask                 = DataBusMask;
        m_methods.ReadAddressFromWriteAddress = ReadAddressFromWriteAddress;
        m_methods.ReadValueFromWriteValue     = ReadValueFromWriteValue;
        m_methods.NeedRestoreTestRegValues    = NeedRestoreTestRegValues;
        m_methods.DescriptionBuild            = DescriptionBuild;
        }

    mThisHal(self)->methods = &m_methods;
    }

AtHal AtHalDefaultObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    mThisHal(self)->baseAddress = baseAddress;
    mThisHal(self)->mutex = AtOsalMutexCreate();
    mThisHal(self)->addressMask = MaxAddress(self);
    if (AtOsalIsMultitasking() && (mThisHal(self)->mutex == NULL))
        return NULL;

    return self;
    }

/*
 * Create new default HAL
 *
 * @param baseAddress Base address
 * @return Default HAL
 */
AtHal AtHalDefaultNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return AtHalDefaultObjectInit(newHal, baseAddress);
    }

/*
 * Get base address
 *
 * @param self This HAL
 * @return Base address
 */
uint32 AtHalDefaultBaseAddress(AtHal self)
    {
    return ((AtHalDefault)self)->baseAddress;
    }

void AtHalDefaultAddressMaskSet(AtHal self, uint32 addressMask)
    {
    if (self)
        ((AtHalDefault)self)->addressMask = addressMask;
    }

void AtHalDefaultFailureReport(AtHal self)
    {
    if (!AtHalIsFail(self))
        return;

    if (mThisHal(self)->failureNotified)
        return;

    AtPrintc(cSevCritical,
             "ERROR: HAL is fail (%d continuous fail accessings), need to reset it to bring it back to work.\r\n"
             "       Use AtHalReset API or \"hal reset\" CLI to reset.\r\n",
             mThisHal(self)->numContinuousFailures);
    mThisHal(self)->failureNotified = cAtTrue;
    }

void AtHalDefaultFailureDetectEnable(AtHal self, eBool enable)
    {
    if (self)
        mThisHal(self)->halFailureDetectDisabled = enable ? cAtFalse : cAtTrue;
    }

void AtHalDefaultTestRegisterSetup(AtHal self, AtHalTestRegister regToTest, uint32 address)
    {
    AtHalDefault halDefault = (AtHalDefault)self;

    regToTest->address = address;
    regToTest->writableMask = halDefault->methods->DataBusMask(self);
    }
    
void AtHalDefaultTestRegisterSetupWithMask(AtHal self, AtHalTestRegister regToTest, uint32 address, uint32 regMask)
    {
    AtUnused(self);
    regToTest->address = address;
    regToTest->writableMask = regMask;
    }

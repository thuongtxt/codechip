/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform - HAL
 *
 * File        : AtHalSimInternal.h
 *
 * Created Date: Sep 11, 2012
 *
 * Description : Header file of HAL Simulation,
 *               the Ram-based simulation of Hardware Abstraction Layer
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalSim *AtHalSim;

typedef struct tAtHalSimMethods
    {
    uint32 (*HoldRegsForReadOfLongReg)(AtHalSim self, uint32 address, const uint32 **holdRegs);
    uint32 (*HoldRegsForWriteOfLongReg)(AtHalSim self, uint32 address, const uint32 **holdRegs);
    }tAtHalSimMethods;

typedef struct tAtHalSimRegister
    {
    AtSize registerMemory;
    uint8 isLongReg;
    uint8 validated;
    }tAtHalSimRegister;

typedef struct tAtHalSim
    {
    tAtHal super;
    const tAtHalSimMethods *methods;

    /* Private variables */
    tAtHalSimRegister **memoryBlocks;
    uint32 sizeInMbytes;
    AtSize cachedAddress; /* Cache address (not hold address) in read operation.
                             Used to read long register */
    eBool  holdWritten; /* Set to cAtTrue if hold register is written.
                              Used to write on long register */
    AtOsalMutex mutex;         /* Mutex to protect shared memory */
    uint32 *readHoldRegisters;
    uint32 *writeHoldRegisters;
    uint16 numHoldRegs;
    }tAtHalSim;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Constructor of HAL Simulation */
AtHal AtHalSimObjectInit(AtHal self, uint32 sizeInMbyte);
AtHal AtHalSimGlobalHoldObjectInit(AtHal self, uint32 *readHoldRegisters, uint32 *writeHoldRegisters, uint16 numHoldRegisters, uint32 sizeInMbyte);

/* Set/Get methods of HAL Simulation */
const tAtHalSimMethods *AtHalSimMethodsGet(AtHalSim self);
void AtHalSimMethodsSet(AtHalSim self, const tAtHalSimMethods *methods);

/* To manage caching */
void AtHalSimRegisterValidate(AtHal self, uint32 address, eBool validated);
eBool AtHalSimRegisterIsValidated(AtHal self, uint32 address);

/* Concrete HALs */
AtHal Tha60210051HalSimNew(uint32 sizeInMbyte);
AtHal Tha60210012HalSimNew(uint32 sizeInMbyte);
AtHal Tha60290021HalSimNew(uint32 sizeInMbyte);
AtHal Tha60290022HalSimNew(uint32 sizeInMbyte);
AtHal Tha60290011HalSimNew(uint32 sizeInMbyte);
AtHal Tha6A290021HalSimNew(uint32 sizeInMbyte);
AtHal Tha60290081HalSimNew(uint32 sizeInMbyte);

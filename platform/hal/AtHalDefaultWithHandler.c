/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDefaultWithHandler.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : HAL for product that needs to handle direct read/write at
 *               application layer.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtObject.h"
#include "AtHalSimInternal.h"
#include "AtHalDefaultWithHandlerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue               0xDEADC0FE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalDefaultWithHandler *)self)
#define mAddressMask(self)  (((AtHalDefault)self)->addressMask)
#define mRealAddress(self, localAddress) AtHalBaseAddressGet(self) + ((localAddress & mAddressMask(self)) << 2)
#define mDirectWrite(self, address, value) mThis(self)->writeFunc(mRealAddress(self, address), value)
#define mDirectRead(self, address) mThis(self)->readFunc(mRealAddress(self, address))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void SlowSimulate(AtHal self)
    {
    if (AtHalSlowAccessTimeUsGet(self))
        AtOsalUSleep(AtHalSlowAccessTimeUsGet(self));
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    mDirectWrite(self, address, value);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    return mDirectRead(self, address);
    }

static eBool ShouldRead(AtHal self, uint32 address)
    {
    AtHal halSim = AtHalCachedHalSimGet(self);

    if (halSim == NULL)
        return cAtTrue;

    if (AtHalCacheIsEnabled(self))
        return AtHalSimRegisterIsValidated(halSim, address) ? cAtFalse : cAtTrue;

    return cAtTrue;
    }

static eBool ShouldWrite(AtHal self, uint32 address, uint32 value)
    {
    AtHal halSim;

    if (!AtHalCacheIsEnabled(self))
        return cAtTrue;

    halSim = AtHalCachedHalSimGet(self) ;
    if (halSim == NULL)
        return cAtTrue;

    /* The same value is being applied */
    if (AtHalSimRegisterIsValidated(halSim, address))
        return (AtHalRead(halSim, address) == value) ? cAtFalse : cAtTrue;

    /* Should write when register cache has not been validated */
    return cAtTrue;
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 value;
    AtHal halSim = AtHalCachedHalSimGet(self);

    /* Return from cache rather than read from hardware */
    if (!ShouldRead(self, address))
        return AtHalRead(halSim, address);

    /* Read from hardware and cache */
    if (AtHalCachedIsSimulated(self))
        {
        SlowSimulate(self);
        value = AtHalRead(halSim, address);
        }
    else
        value = mMethodsGet(self)->DirectRead(self, address);

    AtHalWrite(halSim, address, value);
    AtHalSimRegisterValidate(halSim, address, cAtTrue);

    mThis(self)->readCount = mThis(self)->readCount + 1;

    return value;
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    AtHal halSim = AtHalCachedHalSimGet(self);

    if (!ShouldWrite(self, address, value))
        return;

    if (AtHalCachedIsSimulated(self))
        SlowSimulate(self);

    AtHalWrite(halSim, address, value);
    AtHalSimRegisterValidate(halSim, address, cAtTrue);

    if (!AtHalCachedIsSimulated(self))
        mMethodsGet(self)->DirectWrite(self, address, value);

    mThis(self)->writeCount = mThis(self)->writeCount + 1;
    }

static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest[6];

    if (numRegs)
        *numRegs = mCount(regsToTest);

    AtHalDefaultTestRegisterSetup(self, &regsToTest[0], 0xF0004A);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[1], 0xF0004B);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[2], 0xF0004C);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[3], 0xF0004D);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[4], 0xF0004E);
    AtHalDefaultTestRegisterSetup(self, &regsToTest[5], 0xF0004F);

    return regsToTest;
    }

static uint32 DataBusMask(AtHal self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 TestPattern(AtHal hal)
    {
    AtUnused(hal);
    return 0xAAAAAAAA;
    }

static uint32 TestAntiPattern(AtHal self)
    {
    AtUnused(self);
    return 0x55555555;
    }

static uint32 MaxAddress(AtHal self)
    {
    AtUnused(self);
    return cBit24_0;
    }

static eBool NeedRestoreTestRegValues(AtHal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void ShowAccessStatistic(AtHal self)
    {
    if (!AtHalCacheIsEnabled(self))
        return m_AtHalMethods->ShowAccessStatistic(self);

    AtPrintc(cSevNormal, "* Read        : %d (operations)\r\n", mThis(self)->readCount);
    AtPrintc(cSevNormal, "* Write       : %d (operations)\r\n", mThis(self)->writeCount);
    AtPrintc(cSevNormal, "* Total access: %d (operations)\r\n", mThis(self)->readCount + mThis(self)->writeCount);
    mThis(self)->readCount  = 0;
    mThis(self)->writeCount = 0;
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        mMethodOverride(m_AtHalDefaultOverride, RegsToTest);
        mMethodOverride(m_AtHalDefaultOverride, DataBusMask);
        mMethodOverride(m_AtHalDefaultOverride, TestPattern);
        mMethodOverride(m_AtHalDefaultOverride, TestAntiPattern);
        mMethodOverride(m_AtHalDefaultOverride, NeedRestoreTestRegValues);
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        mMethodOverride(m_AtHalOverride, Read);
        mMethodOverride(m_AtHalOverride, Write);
        mMethodOverride(m_AtHalOverride, DirectRead);
        mMethodOverride(m_AtHalOverride, DirectWrite);
        mMethodOverride(m_AtHalOverride, MaxAddress);
        mMethodOverride(m_AtHalOverride, ShowAccessStatistic);
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalDefaultWithHandler);
    }

AtHal AtHalDefaultWithHandlerObjectInit(AtHal self, uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->writeFunc = writeHandler;
    mThis(self)->readFunc  = readHandler;

    return self;
    }

AtHal AtHalDefaultWithHandlerNew(uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return AtHalDefaultWithHandlerObjectInit(newHal, baseAddress, writeHandler, readHandler);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtHalTestbench.c
 *
 * Created Date: May 15, 2015
 *
 * Description : HAL to work with testbench
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>

#include "atclib.h"
#include "AtHalTestbench.h"
#include "../AtHalDefaultWithHandlerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtHalTbRetryNum 100
#define cAtHalTbCmdLen   256

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalTestbenchTcp *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalTestbenchTcp
    {
    tAtHalDefaultWithHandler super;

    /* Private data */
    int sock;
    int port;
    char ip[32];
    }tAtHalTestbenchTcp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern int inet_aton (__const char *__cp, struct in_addr *__inp);

/*--------------------------- Implementation ---------------------------------*/
static int Send(int* sock, const char* ipAddr, int port, const char* pCmd)
    {
    struct sockaddr_in serverAddr;
    int _sock;
    int sendRet;

    /* Initialize */
    _sock    = -1;
    sendRet = -1;

    if(1)
        {
        inet_aton(ipAddr, &serverAddr.sin_addr);
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_port = htons((uint16_t)port);

        /* Create socket to connect to Testbench machine */
        if ( (_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
            {

            AtPrintc(cSevCritical, "Cannot create socket to connect to Testbench machine\n");
            *sock = _sock;
            }
        else if( connect(_sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0 )
            {

            AtPrintc(cSevCritical, "Cannot connect to Testbench machine\n");
            close(_sock);
            _sock = -1;
            *sock = _sock;
            }
        else
            {
            sendRet = send(_sock, pCmd, AtStrlen(pCmd) + 1, 0);
            *sock = _sock;
            }
        }
    else
        {
        sendRet = send(_sock, pCmd, AtStrlen(pCmd) + 1, 0);
        }

    return sendRet;
    }

static int Recv(int sock, char *recvStr, uint32 size)
    {
    int retRecv;
    retRecv = recv(sock, recvStr, size, 0);
    return retRecv;
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    char cmdRead[cAtHalTbCmdLen];
    char resultStr[128];
    int retry;
    int sendRet= -1;
    atbool blSucc;
    uint32 value;

    /* Initialize */
    blSucc = cAtFalse;
    value = 0xDEADCAFE;

    /* Prepare command to send */
    AtSprintf(cmdRead, "%u %x", 1, address);

    /* Request */
    for (retry = 0; retry < cAtHalTbRetryNum; retry++)
        {
        sendRet = Send(&(mThis(self)->sock), mThis(self)->ip, mThis(self)->port, cmdRead);

        if (sendRet != -1)
            {
            uint32 returnAddress;
            int numBytes = Recv(mThis(self)->sock, resultStr, sizeof(resultStr));

            close(mThis(self)->sock);

            if (numBytes > 0)
                {
                resultStr[numBytes] = '\0';
                sscanf(resultStr, "RD32[0x%x]->0x%x", &returnAddress, &value);
                blSucc = cAtTrue;
                }
            else
                blSucc = cAtFalse;

            break;
            }
        else /* Connect successful, but can't send */
            {
            /* Close sock */
            if(mThis(self)->sock >= 0)
                {
                close(mThis(self)->sock);
                }
            }
        }

    /* Request failed */
    if (blSucc == cAtFalse)
        {
        AtPrintc(cSevCritical, "Cannot read value: 0x%1x from address: 0x%1x\n", value, address);
        return value;
        }

    if ((value == cInvalidReadValue) && AtHalDebugIsEnabled(self))
        {
        AtPrintc(cSevWarning, "(%s, %d) Read 0x%08x, value 0x%08x (FPGA at %s)\n",
                 __FILE__, __LINE__,
                 address, value,
                 AtHalToString(self));
        }

    return value;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    char    cmdWrite[cAtHalTbCmdLen];
    char    resultStr[128];
    int     retry;
    atbool   blSucc;
    int     sendRet= -1;

    /* Initialize */
    blSucc = cAtFalse;

    /* Prepare command to send */
    AtSprintf(cmdWrite, "%u %x %x", 0, address, value);

    /* Request */
    for (retry = 0; retry < cAtHalTbRetryNum; retry ++)
        {
        sendRet = Send(&(mThis(self)->sock), mThis(self)->ip, mThis(self)->port, cmdWrite);

        if (sendRet != -1)
            {
            Recv(mThis(self)->sock, resultStr, sizeof(resultStr));

            close(mThis(self)->sock);

            blSucc = cAtTrue;
            break;
            }
        else/* Connect successful, but can't send */
            {

            AtPrintc(cSevCritical, "%-20s%s:%d\n", "Write-Send", "[FAIL]", sendRet);
            /* Close sock */
            if(mThis(self)->sock >= 0)
                {
                close(mThis(self)->sock);
                }
            }
        }

    /* Request failed */
    if (blSucc == cAtFalse)
        AtPrintc(cSevCritical, "Cannot write value: 0x%1x to address: 0x%1x\n", (int)value, (int)address);
    }

static const char* ToString(AtHal self)
    {
    static char description[64];
    AtSprintf(description, "%s:%d", mThis(self)->ip, mThis(self)->port);
    return description;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        m_AtHalOverride.DirectRead  = DirectRead;
        m_AtHalOverride.DirectWrite = DirectWrite;
        m_AtHalOverride.ToString    = ToString;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalTestbenchTcp);
    }

static AtHal ObjectInit(AtHal self, const char *serverIpAddress, uint16 connectedPort)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultWithHandlerObjectInit(self, cBit31_0, NULL, NULL) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    AtStrcpy(mThis(self)->ip, serverIpAddress);
    if (connectedPort == 0)
        connectedPort = 55555;
    mThis(self)->port = (int)connectedPort;

    return self;
    }

AtHal AtHalTestbenchTcpNew(const char *serverIpAddress, uint16 connectedPort)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newHal, serverIpAddress, connectedPort);
    }

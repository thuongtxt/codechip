/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtHalTestbench.c
 *
 * Created Date: Sep 20, 2016
 *
 * Description : HAL to work with testbench
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "atclib.h"
#include "AtHalTestbench.h"
#include "../AtHalSimInternal.h"
#include "../AtHalDefaultWithHandlerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtHalTestbench *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalTestbench
    {
    tAtHalDefaultWithHandler super;
    }tAtHalTestbench;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods        m_AtHalOverride;
static tAtHalDefaultMethods m_AtHalDefaultOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHalTestRegister RegsToTest(AtHal self, uint32 *numRegs)
    {
    static tAtHalTestRegister regsToTest;

    if (numRegs)
        *numRegs = 1;

    AtHalDefaultTestRegisterSetup(self, &regsToTest, 0xF00041);

    return &regsToTest;
    }

static uint32 DataBusMask(AtHal self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static void DirectWrite(AtHal self, uint32 address, uint32 value)
    {
    tAtHalDefaultWithHandler *hal = (tAtHalDefaultWithHandler *)self;
    hal->writeFunc(address, value);
    }

static uint32 DirectRead(AtHal self, uint32 address)
    {
    tAtHalDefaultWithHandler *hal = (tAtHalDefaultWithHandler *)self;
    return hal->readFunc(address);
    }

static void OverrideAtHalDefault(AtHal self)
    {
    AtHalDefault defaultHal = (AtHalDefault)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtHalDefaultOverride, defaultHal->methods, sizeof(m_AtHalDefaultOverride));

        m_AtHalDefaultOverride.RegsToTest  = RegsToTest;
        m_AtHalDefaultOverride.DataBusMask = DataBusMask;
        }

    defaultHal->methods = &m_AtHalDefaultOverride;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(m_AtHalOverride));

        mMethodOverride(m_AtHalOverride, DirectRead);
        mMethodOverride(m_AtHalOverride, DirectWrite);
        mMethodOverride(m_AtHalOverride, DirectWrite);
        mMethodOverride(m_AtHalOverride, DirectRead);
        }

    mMethodsSet(self, &m_AtHalOverride);
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    OverrideAtHalDefault(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalTestbench);
    }

static AtHal ObjectInit(AtHal self, uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalDefaultWithHandlerObjectInit(self, baseAddress, writeHandler, readHandler) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalTestbenchNew(uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler)
    {
    /* Allocate memory */
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHal, baseAddress, writeHandler, readHandler);
    }

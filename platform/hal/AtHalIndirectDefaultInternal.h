/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalIndirectDefaultInternal.h
 * 
 * Created Date: Apr 9, 2013
 *
 * Description : Indirect 24-bit address
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALINDIRECT24BITINTERNAL_H_
#define _ATHALINDIRECT24BITINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "commacro.h"
#include "AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalIndirectDefault * AtHalIndirectDefault;

typedef struct tAtHalIndirectDefaultMethods
    {
    uint8 (*IsDirectRegister)(AtHal self, uint32 address);
    void (*IndirectCtrlRegisterSave)(AtHal self);
    uint8 (*IndirectIsInProgress)(AtHal self);

    /* Self-test */
    void (*ShowControlRegisters)(AtHal self);
    }tAtHalIndirectDefaultMethods;

typedef struct tAtHalIndirectDefault
    {
    tAtHalDefault super;
    const tAtHalIndirectDefaultMethods *methods;

    /* Private data */
    uint32 addressMask;

    /* Indirect control registers */
    uint32 indirectControl1Address;
    uint32 indirectControl2Address;
    uint32 indirectData1Address;
    uint32 indirectData2Address;

    /* To store content of indirect registers */
    uint32 indirectControl1;
    uint32 indirectControl2;
    uint32 indirectData1;
    uint32 indirectData2;

    uint32 retryTimes; /* Retry time to wait for HW to finish accessing register */

    /* Debug */
    uint32 maxRetryTimes; /* Maximum retry times while software wait to read/write FPGA */
    uint32 readTimeoutCounter;
    uint32 writeTimeoutCounter;
    }tAtHalIndirectDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalIndirectDefaultObjectInit(AtHal self, uint32 baseAddress);

/* Set/get time wait for HW to finish accessing register */
uint32 AtHalIndirectDefaultRetryInMsGet(AtHal self);

/* Configure HAL */
void AtHalIndirectDefaultAddressMaskSet(AtHal self, uint32 addressMask);
void AtHalIndirectDefaultIndirectControl1AddressSet(AtHal self, uint32 address);
void AtHalIndirectDefaultIndirectControl2AddressSet(AtHal self, uint32 address);
void AtHalIndirectDefaultIndirectData1AddressSet(AtHal self, uint32 address);
void AtHalIndirectDefaultIndirectData2AddressSet(AtHal self, uint32 address);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALINDIRECT24BITINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalMemoryMapperInternal.h
 * 
 * Created Date: May 8, 2014
 *
 * Description : Memory mapper
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALMEMORYMAPPERINTERNAL_H_
#define _ATHALMEMORYMAPPERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalMemoryMapper.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalMemoryMapperMethods
    {
    eAtRet (*Map)(AtHalMemoryMapper self);
    eAtRet (*UnMap)(AtHalMemoryMapper self);
    void (*Delete)(AtHalMemoryMapper self);
    }tAtHalMemoryMapperMethods;

typedef struct tAtHalMemoryMapper
    {
    const tAtHalMemoryMapperMethods *methods;

    /* Private data */
    uint32 memorySize;
    uint32 offset;
    uint32 baseAddress;
    }tAtHalMemoryMapper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHalMemoryMapper AtHalMemoryMapperObjectInit(AtHalMemoryMapper self, uint32 offset, uint32 memorySize);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALMEMORYMAPPERINTERNAL_H_ */


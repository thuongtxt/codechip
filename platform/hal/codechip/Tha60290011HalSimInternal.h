/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : Tha60290011HalSimInternal.h
 * 
 * Created Date: Jun 19, 2018
 *
 * Description : Implementation for 60290011 sim HAL and 1:N protection sub device HAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011HALSIMINTERNAL_H_
#define _THA60290011HALSIMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "commacro.h"
#include "Tha60210051HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011HalSim
    {
    tAtHalSim super;
    }tTha60290011HalSim;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal Tha60290011HalSimObjectInit(AtHal self, uint32 sizeInMbyte);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011HALSIMINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : Tha60290021HalSimInternal.h
 * 
 * Created Date: Apr 26, 2017
 *
 * Description : HAL SIM for 60290021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021HALSIMINTERNAL_H_
#define _THA60290021HALSIMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60210051HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021HalSim
    {
    tTha60210051HalSim super;
    }tTha60290021HalSim;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal Tha60290021HalSimObjectInit(AtHal self, uint32 sizeInMbyte);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021HALSIMINTERNAL_H_ */


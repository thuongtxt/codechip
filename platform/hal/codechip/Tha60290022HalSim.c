/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : Tha60290022HalSim.c
 *
 * Created Date: Apr 26, 2017
 *
 * Description : HAL SIM for 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "Tha60290022HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPohBaseAddress            0x200000
#define cAf6Reg_pohvtpohgrb_Base   0x022000
#define cAf6Reg_pohvtpohgrb1_Base  0x026000
#define cAf6Reg_pohstspohgrb_Base  0x020000
#define cAf6Reg_pohstspohgrb1_Base 0x024000
#define cStsMax   47
#define cVtMax    27
#define cSliceMax 3
#define cCdrV3Base 0x0E00000
#define cAf6Reg_cdr_acr_cpu_hold_ctrl_Base 0x070000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

typedef struct tRegRange
    {
    uint32 start;
    uint32 stop;
    }tRegRange;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;

/* Override */
static tTha60210051HalSimMethods m_Tha60210051HalSimOverride;

/* Save super implementation */
static const tTha60210051HalSimMethods *m_Tha60210051HalSimMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsPlaAddress(uint32 address)
    {
    if ((address >= 0x0400000) && (address <= 0x04FFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PlaHoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegister[] = {0x470000, 0x470001, 0x470002};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegister;
    return mCount(holdRegister);
    }

static eBool IsPohstspohgrbWithRegBase(Tha60210051HalSim self, uint32 localAddress, uint32 regBase)
    {
    uint32 startAddr = regBase + cPohBaseAddress;
    uint32 stopAddr = startAddr + cStsMax + cSliceMax * (cStsMax + 1);
    AtUnused(self);
    return mInRange(localAddress, startAddr, stopAddr) ? cAtTrue : cAtFalse;
    }

static eBool IsPohstspohgrb(Tha60210051HalSim self, uint32 localAddress)
    {
    if (IsPohstspohgrbWithRegBase(self, localAddress, cAf6Reg_pohstspohgrb_Base) ||
        IsPohstspohgrbWithRegBase(self, localAddress, cAf6Reg_pohstspohgrb1_Base))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsPohvtpohgrbWithRegBase(Tha60210051HalSim self, uint32 localAddress, uint32 regBase)
    {
    uint32 startAddr = regBase + cPohBaseAddress;
    uint32 stopAddr  = startAddr + (cSliceMax * 32) + (cStsMax * 128) + cVtMax;
    AtUnused(self);
    return mInRange(localAddress, startAddr, stopAddr) ? cAtTrue : cAtFalse;
    }

static eBool IsPohvtpohgrb(Tha60210051HalSim self, uint32 localAddress)
    {
    if (IsPohvtpohgrbWithRegBase(self, localAddress, cAf6Reg_pohvtpohgrb_Base) ||
        IsPohvtpohgrbWithRegBase(self, localAddress, cAf6Reg_pohvtpohgrb1_Base))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsGrabberAddress(Tha60210051HalSim self, uint32 localAddress)
    {
    if (IsPohstspohgrb(self, localAddress) || IsPohvtpohgrb(self, localAddress))
        return cAtTrue;
    return cAtFalse;
    }

static eBool PohAddressBelongTo155Part(Tha60210051HalSim self, uint32 address)
    {
    if (IsGrabberAddress(self, address))
        return cAtFalse;

    return cAtTrue;
    }

static tRegRange *MapRegisterRanges(AtHalSim self, uint32 *numSlices)
    {
    static tRegRange ranges[] = {{0x0800000, 0x083FFFF},
                                 {0x0840000, 0x087FFFF},
                                 {0x0880000, 0x08BFFFF},
                                 {0x08C0000, 0x08FFFFF},
                                 {0x0900000, 0x093FFFF},
                                 {0x0940000, 0x097FFFF},
                                 {0x0980000, 0x09BFFFF},
                                 {0x09C0000, 0x09FFFFF}};
    AtUnused(self);
    if (numSlices)
        *numSlices = mCount(ranges);
    return ranges;
    }

static uint32 MapRegisterSliceId(AtHalSim self, uint32 address)
    {
    uint32 slice_i;
    uint32 numSlices;
    tRegRange *ranges = MapRegisterRanges(self, &numSlices);

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        if (mInRange(address, ranges[slice_i].start, ranges[slice_i].stop))
            return slice_i;
        }

    return cInvalidUint32;
    }

static eBool IsMapRegister(AtHalSim self, uint32 address)
    {
    return (MapRegisterSliceId(self, address) == cInvalidUint32) ? cAtFalse : cAtTrue;
    }

static uint32 MapSliceBaseAddress(AtHalSim self, uint32 sliceId)
    {
    return MapRegisterRanges(self, NULL)[sliceId].start;
    }

static uint32 MapHoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static eBool holdInit = cAtFalse;
    static uint32 holdRegisters[8][3];
    uint32 sliceId = MapRegisterSliceId(self, address);

    if (!holdInit)
        {
        uint32 slice_i, hold_i;
        for (slice_i = 0; slice_i < 8; slice_i++)
            {
            uint32 sliceBase = MapSliceBaseAddress(self, slice_i);
            for (hold_i = 0; hold_i < 3; hold_i++)
                holdRegisters[slice_i][hold_i] = 0x00F000 + sliceBase + hold_i;
            }

        holdInit = cAtTrue;
        }

    *holdRegs = (const uint32 *)&(holdRegisters[sliceId]);

    return 3;
    }

static eBool IsCdrV3Address(AtHalSim self, uint32 address)
    {
    AtUnused(self);
    return mInRange(address, 0x0E00000, 0x0E7FFFF);
    }

static uint32 CdrV3HoldRegs(AtHalSim self, const uint32 **holdRegs)
    {
    static uint32 holdAddresses[] = {cCdrV3Base + cAf6Reg_cdr_acr_cpu_hold_ctrl_Base + 0,
                                     cCdrV3Base + cAf6Reg_cdr_acr_cpu_hold_ctrl_Base + 1,
                                     cCdrV3Base + cAf6Reg_cdr_acr_cpu_hold_ctrl_Base + 2};
    *holdRegs = holdAddresses;
    AtUnused(self);
    return mCount(holdAddresses);
    }

static eBool IsPtpFaceplateAddress(uint32 address)
    {
    if ((address >= 0xD0000) && (address <= 0xDAFFF))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PtpFaceplateHoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegister[] = {0xD0000, 0xD0001, 0xD0002};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegister;
    return mCount(holdRegister);
    }

static eBool IsPtpBackplaneAddress(uint32 address)
    {
    if ((address >= 0xDB000) && (address <= 0xDFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PtpBackplaneHoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegister[] = {0xDB020, 0xDB021, 0xDB022};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegister;
    return mCount(holdRegister);
    }

static eBool IsPtpAddress(AtHalSim self, uint32 address)
    {
    AtUnused(self);
    if (IsPtpFaceplateAddress(address) || IsPtpBackplaneAddress(address))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PtpHoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (IsPtpFaceplateAddress(address))
        return PtpFaceplateHoldRegs(self, address, holdRegs);

    if (IsPtpBackplaneAddress(address))
        return PtpBackplaneHoldRegs(self, address, holdRegs);

    return 0;
    }

static uint32 HoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (IsPlaAddress(address))
        return PlaHoldRegs(self, address, holdRegs);

    if (IsMapRegister(self, address))
        return MapHoldRegs(self, address, holdRegs);

    if (IsCdrV3Address(self, address))
        return CdrV3HoldRegs(self, holdRegs);

    if (IsPtpAddress(self, address))
        return PtpHoldRegs(self, address, holdRegs);

    return m_Tha60210051HalSimMethods->HoldRegs(self, address, holdRegs);
    }

static eBool IsPohRegister(Tha60210051HalSim self, uint32 localAddress)
    {
    AtUnused(self);
    return mInRange(localAddress, 0x0200000, 0x02FFFFF) ? cAtTrue : cAtFalse;
    }

static void OverrideTha60210051HalSim(AtHal self)
    {
    Tha60210051HalSim hal = (Tha60210051HalSim)self;

    if (!m_methodsInit)
        {
        m_Tha60210051HalSimMethods = hal->methods;
        AtOsalMemCpy(&m_Tha60210051HalSimOverride, m_Tha60210051HalSimMethods, sizeof(m_Tha60210051HalSimOverride));

        m_Tha60210051HalSimOverride.HoldRegs  = HoldRegs;
        m_Tha60210051HalSimOverride.PohAddressBelongTo155Part = PohAddressBelongTo155Part;
        m_Tha60210051HalSimOverride.IsPohRegister = IsPohRegister;
        }

    hal->methods = &m_Tha60210051HalSimOverride;
    }

static void Override(AtHal self)
    {
    OverrideTha60210051HalSim(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HalSim);
    }

AtHal Tha60290022HalSimObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (Tha60290021HalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal Tha60290022HalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return Tha60290022HalSimObjectInit(hal, sizeInMbyte);
    }


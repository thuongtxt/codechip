/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : Tha60290021HalSim.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : HAL simulation for product 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "Tha6A290021HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_Pmc_Base_Address 0x1F00000UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;

/* Override */
static tTha60210051HalSimMethods m_Tha60210051HalSimOverride;

/* Save super implementation */
static const tTha60210051HalSimMethods *m_Tha60210051HalSimMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsPrbsRegister(uint32 localAddress)
    {
    if ((localAddress >= 0x0500000) && (localAddress <= 0x05FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (IsPrbsRegister(address))
        return 0;

    return m_Tha60210051HalSimMethods->HoldRegs(self, address, holdRegs);
    }

static void OverrideTha60210051HalSim(AtHal self)
    {
    Tha60210051HalSim hal = (Tha60210051HalSim)self;
    if (!m_methodsInit)
        {
        m_Tha60210051HalSimMethods = hal->methods;
        AtOsalMemCpy(&m_Tha60210051HalSimOverride, m_Tha60210051HalSimMethods, sizeof(m_Tha60210051HalSimOverride));

        m_Tha60210051HalSimOverride.HoldRegs  = HoldRegs;
        }

    hal->methods = &m_Tha60210051HalSimOverride;
    }

static void Override(AtHal self)
    {
    OverrideTha60210051HalSim(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021HalSim);
    }

AtHal Tha6A290021HalSimObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    if (Tha60290021HalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal Tha6A290021HalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return Tha6A290021HalSimObjectInit(hal, sizeInMbyte);
    }

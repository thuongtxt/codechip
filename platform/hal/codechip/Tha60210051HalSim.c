/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : Tha60210051HalSim.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : HAL simulation for product 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumLoSlice              12
#define cLoNumHoldRegister       3
#define cPweNumLongRegisterAccess 4
#define cPweNumHoldRegister       4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051HalSim)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;
static tTha60210051HalSimMethods m_methods;

/* Override */
static tAtHalSimMethods m_AtHalSimOverride;

/* Save super implementation */
static const tAtHalSimMethods *m_AtHalSimMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CdrAddressBelongToHoPart(uint32 localAddress, uint8 *loSlice)
    {
    uint32 baseAddress = localAddress & cBit23_16;

    if (baseAddress == 0x300000)
        return cAtTrue;

    *loSlice = (uint8)(((baseAddress - 0xC00000) >> 16) / 4);
    if (*loSlice >= cNumLoSlice) /* Out of range, force to use hi-order */
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *LoHoldRegistersGet(uint16 *numberOfHoldRegisters, uint8 loSlice)
    {
    static uint32 holdRegisters[] = {0xC30000, 0xC30001, 0xC30002,
                                     0xC70000, 0xC70001, 0xC70002,
                                     0xCB0000, 0xCB0001, 0xCB0002,
                                     0xCF0000, 0xCF0001, 0xCF0002,
                                     0xD30000, 0xD30001, 0xD30002,
                                     0xD70000, 0xD70001, 0xD70002,
                                     0xDB0000, 0xDB0001, 0xDB0002,
                                     0xDF0000, 0xDF0001, 0xDF0002,
                                     0xE30000, 0xE30001, 0xE30002,
                                     0xE70000, 0xE70001, 0xE70002,
                                     0xEB0000, 0xEB0001, 0xEB0002,
                                     0xEF0000, 0xEF0001, 0xEF0002};
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = cLoNumHoldRegister;

    return holdRegisters + loSlice * cLoNumHoldRegister;
    }

static uint32 CdrHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    uint8 loSlice;
    uint16 numHolds;

    AtUnused(self);

    if (CdrAddressBelongToHoPart(address, &loSlice))
        {
        static uint32 holdRegs_[] = {0x330000, 0x330001, 0x330002};
        *holdRegs = holdRegs_;
        return mCount(holdRegs_);
        }

    *holdRegs = LoHoldRegistersGet(&numHolds, loSlice);
    return numHolds;
    }

static eBool PohAddressBelongTo155Part(Tha60210051HalSim self, uint32 address)
    {
    uint32 localAddr = address & cBit19_0;

    AtUnused(self);

    if ((localAddr >= 0x24000) && (localAddr <= 0x27FFF))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 PohClk155HoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x20000A, 0x20000B};
    AtUnused(address);
    AtUnused(self);
    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static uint32 PohClk311HoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x21000A, 0x21000B};
    AtUnused(address);
    AtUnused(self);
    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static eBool IsPohRegister(Tha60210051HalSim self, uint32 localAddress)
    {
    AtUnused(self);

    if ((localAddress >= 0x0200000) && (localAddress <= 0x02BFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PohHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (mThis(self)->methods->PohAddressBelongTo155Part(mThis(self), address))
        return PohClk155HoldReg(self, address, holdRegs);

    return PohClk311HoldReg(self, address, holdRegs);
    }

static eBool IsCdrRegister(Tha60210051HalSim self, uint32 localAddress)
    {
    AtUnused(self);
    if (((localAddress >= 0x0300000) && (localAddress <= 0x033FFFF)) || /* ho part */
        ((localAddress >= 0x0C00000) && (localAddress <= 0x0EFFFFF)))   /* lo part */
        return cAtTrue;

    return cAtFalse;
    }

static eBool PweAddressBelongToPlaPart(uint32 localAddress, uint8 *pweIndex)
    {
    uint32 baseAddress = localAddress & cBit23_20;

    if (baseAddress == 0x400000)
        return cAtTrue;

    baseAddress = localAddress & cBit19_16;
    *pweIndex = (uint8)((baseAddress - 0x70000) >> 16);
    if (*pweIndex >= cPweNumLongRegisterAccess)  /* Out of range, force to use PLA */
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PlaLongRegisterAccessCreate(Tha60210051HalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x48C000, 0x48C001, 0x48C002};
    *holdRegs = holdRegisters;
    AtUnused(address);
    AtUnused(self);
    return mCount(holdRegisters);
    }

static uint32 PweLongRegisterAccess(AtHalSim self, uint32 address, const uint32 **holdRegs, uint8 pweIndex)
    {
    static uint32 holdRegisters[] = {0x7C00A, 0x7C00B, 0x7C00C,
                                     0x8C00A, 0x8C00B, 0x8C00C,
                                     0x9C00A, 0x9C00B, 0x9C00C,
                                     0xAC00A, 0xAC00B, 0xAC00C};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegisters + pweIndex * cPweNumHoldRegister;
    return cPweNumHoldRegister;
    }

static uint32 PweHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    uint8 pweIndex;

    if (PweAddressBelongToPlaPart(address, &pweIndex))
        return mThis(self)->methods->PlaLongRegisterAccessCreate(mThis(self), address, holdRegs);

    return PweLongRegisterAccess(self, address, holdRegs, pweIndex);
    }

static eBool IsPweRegister(uint32 localAddress)
    {
    if (((localAddress >= 0x0400000) && (localAddress <= 0x04FFFFF)) ||
        ((localAddress >= 0x0070000) && (localAddress <= 0x00AFFFF)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsClaRegister(uint32 localAddress)
    {
    if ((localAddress >= 0x0600000) && (localAddress <= 0x06FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ClaHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x060000A, 0x060000B, 0x060000C};

    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static uint32 PdaHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x500000, 0x500001, 0x500002};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegisters;
    return mCount(holdRegs);
    }

static eBool IsPdaRegister(uint32 localAddress)
    {
    if ((localAddress >= 0x0500000) && (localAddress <= 0x05FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if ((mThis(self))->methods->IsCdrRegister(mThis(self), address))
        return CdrHoldReg(self, address, holdRegs);
    if (mThis(self)->methods->IsPohRegister(mThis(self), address))
        return PohHoldReg(self, address, holdRegs);
    if (IsPweRegister(address))
        return PweHoldReg(self, address, holdRegs);
    if (IsClaRegister(address))
        return ClaHoldReg(self, address, holdRegs);
    if (IsPdaRegister(address))
        return PdaHoldReg(self, address, holdRegs);

    return 0;
    }

static uint32 HoldRegsForReadOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    uint32 numHolds = mThis(self)->methods->HoldRegs(self, address, holdRegs);
    if (numHolds)
        return numHolds;

    return m_AtHalSimMethods->HoldRegsForReadOfLongReg(self, address, holdRegs);
    }

static uint32 HoldRegsForWriteOfLongReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    uint32 numHolds =  mThis(self)->methods->HoldRegs(self, address, holdRegs);
    if (numHolds)
        return numHolds;

    return m_AtHalSimMethods->HoldRegsForWriteOfLongReg(self, address, holdRegs);
    }

static void OverrideAtHalSim(AtHal self)
    {
    AtHalSim hal = (AtHalSim)self;

    if (!m_methodsInit)
        {
        m_AtHalSimMethods = hal->methods;
        AtOsalMemCpy(&m_AtHalSimOverride, hal->methods, sizeof(m_AtHalSimOverride));

        m_AtHalSimOverride.HoldRegsForReadOfLongReg  = HoldRegsForReadOfLongReg;
        m_AtHalSimOverride.HoldRegsForWriteOfLongReg = HoldRegsForWriteOfLongReg;
        }

    AtHalSimMethodsSet(hal, &m_AtHalSimOverride);
    }

static void Override(AtHal self)
    {
    OverrideAtHalSim(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051HalSim);
    }

static void MethodsInit(Tha60210051HalSim self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.IsCdrRegister  = IsCdrRegister;
        m_methods.HoldRegs  = HoldRegs;
        m_methods.PlaLongRegisterAccessCreate  = PlaLongRegisterAccessCreate;
        m_methods.PohAddressBelongTo155Part = PohAddressBelongTo155Part;
        m_methods.IsPohRegister = IsPohRegister;
        }

    mThis(self)->methods = &m_methods;
    }

AtHal Tha60210051HalSimObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    if (AtHalSimGlobalHoldObjectInit(self, NULL, NULL, 0, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtHal Tha60210051HalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return Tha60210051HalSimObjectInit(hal, sizeInMbyte);
    }

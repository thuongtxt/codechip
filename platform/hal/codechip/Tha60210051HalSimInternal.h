/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : Tha60210051HalSimInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : HAL simulation for product 60210051
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051HALSIMINTERNAL_H_
#define _THA60210051HALSIMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../AtHalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051HalSim *Tha60210051HalSim;

typedef struct tTha60210051HalSimMethods
    {
    eBool (*IsCdrRegister)(Tha60210051HalSim self, uint32 localAddress);
    uint32 (*HoldRegs)(AtHalSim self, uint32 address, const uint32 **holdRegs);
    uint32 (*PlaLongRegisterAccessCreate)(Tha60210051HalSim self, uint32 address, const uint32 **holdRegs);
    eBool (*PohAddressBelongTo155Part)(Tha60210051HalSim self, uint32 address);
    eBool (*IsPohRegister)(Tha60210051HalSim self, uint32 address);
    }tTha60210051HalSimMethods;

typedef struct tTha60210051HalSim
    {
    tAtHalSim super;
    const tTha60210051HalSimMethods *methods;
    }tTha60210051HalSim;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal Tha60210051HalSimObjectInit(AtHal self, uint32 sizeInMbyte);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051HALSIMINTERNAL_H_ */


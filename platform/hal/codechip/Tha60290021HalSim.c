/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : Tha60290021HalSim.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : HAL simulation for product 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "Tha60290021HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_Pmc_Base_Address 0x1F00000UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;

/* Override */
static tTha60210051HalSimMethods m_Tha60210051HalSimOverride;

/* Save super implementation */
static const tTha60210051HalSimMethods *m_Tha60210051HalSimMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsTohOverEthRegister(uint32 localAddress)
    {
    if (mInRange(localAddress, 0x1EC0000, 0x1EFFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TohOverEthHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x1EC000A, 0x1EC000B};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static eBool IsEthBackPlanePktAnalyzerRegister(uint32 localAddress)
    {
    if ((localAddress >= 0x80000) && (localAddress <= 0x80801))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 EthBackPlanePktAnalyzerHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegister[] = {0x80800, 0x80801};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegister;
    return mCount(holdRegister);
    }

static eBool IsEthPassThroughRegister(uint32 localAddress)
    {
    return mInRange(localAddress, 0x00C0000, 0x00CFFFF) ? cAtTrue : cAtFalse;
    }

static uint32 EthPassThroughHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegister = 0x00C0080;

    AtUnused(self);
    AtUnused(address);

    *holdRegs = &holdRegister;
    return 1;
    }

static eBool IsPdhRegister(uint32 localAddress)
    {
    return mInRange(localAddress, 0x1000000, 0x1BFFFFF) ? cAtTrue : cAtFalse;
    }

static uint32 PdhHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegister[] = {0x1000400, 0x1000401, 0x1000402};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegister;
    return mCount(holdRegs);
    }

static uint32 PmcHoldReg(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x3B319 + cAf6Reg_Pmc_Base_Address};
    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static eBool IsPmcRegister(uint32 localAddress)
    {
    if (mInRange(localAddress, 0x1F00000, 0x1FFFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (IsTohOverEthRegister(address))
        return TohOverEthHoldReg(self, address, holdRegs);

    if (IsEthBackPlanePktAnalyzerRegister(address))
    	return EthBackPlanePktAnalyzerHoldReg(self, address, holdRegs);

    if (IsEthPassThroughRegister(address))
        return EthPassThroughHoldReg(self, address, holdRegs);

    if (IsPdhRegister(address))
        return PdhHoldReg(self, address, holdRegs);

    if (IsPmcRegister(address))
        return PmcHoldReg(self, address, holdRegs);

    return m_Tha60210051HalSimMethods->HoldRegs(self, address, holdRegs);
    }

static void OverrideTha60210051HalSim(AtHal self)
    {
    Tha60210051HalSim hal = (Tha60210051HalSim)self;
    if (!m_methodsInit)
        {
        m_Tha60210051HalSimMethods = hal->methods;
        AtOsalMemCpy(&m_Tha60210051HalSimOverride, m_Tha60210051HalSimMethods, sizeof(m_Tha60210051HalSimOverride));

        m_Tha60210051HalSimOverride.HoldRegs  = HoldRegs;
        }

    hal->methods = &m_Tha60210051HalSimOverride;
    }

static void Override(AtHal self)
    {
    OverrideTha60210051HalSim(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021HalSim);
    }

AtHal Tha60290021HalSimObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    if (Tha60210051HalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal Tha60290021HalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return Tha60290021HalSimObjectInit(hal, sizeInMbyte);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : Tha60290081HalSimInternal.h
 * 
 * Created Date: Aug 14, 2019
 *
 * Description : HAL SIM for 60290022
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022HALSIMINTERNAL_H_
#define _THA60290022HALSIMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022HalSim
    {
    tTha60290021HalSim super;
    }tTha60290022HalSim;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal Tha60290022HalSimObjectInit(AtHal self, uint32 sizeInMbyte);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022HALSIMINTERNAL_H_ */


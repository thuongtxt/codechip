/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : Tha60290081HalSim.c
 *
 * Created Date: Aug 14, 2019
 *
 * Description : HAL SIM for 60290081
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "Tha60290022HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081HalSim
    {
    tTha60290022HalSim super;
    }tTha60290081HalSim;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;

/* Override */
static tTha60210051HalSimMethods m_Tha60210051HalSimOverride;

/* Save super implementation */
static const tTha60210051HalSimMethods *m_Tha60210051HalSimMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsClaAddress(uint32 address)
    {
    if ((address >= 0x0600000) && (address <= 0x06FFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 ClaHoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x0600000, 0x0600001, 0x0600002};

    AtUnused(self);
    AtUnused(address);

    *holdRegs = holdRegisters;
    return mCount(holdRegisters);
    }

static uint32 HoldRegs(AtHalSim self, uint32 address, const uint32 **holdRegs)
    {
    if (IsClaAddress(address))
        return ClaHoldRegs(self, address, holdRegs);

    return m_Tha60210051HalSimMethods->HoldRegs(self, address, holdRegs);
    }

static void OverrideTha60210051HalSim(AtHal self)
    {
    Tha60210051HalSim hal = (Tha60210051HalSim)self;

    if (!m_methodsInit)
        {
        m_Tha60210051HalSimMethods = hal->methods;
        AtOsalMemCpy(&m_Tha60210051HalSimOverride, m_Tha60210051HalSimMethods, sizeof(m_Tha60210051HalSimOverride));

        m_Tha60210051HalSimOverride.HoldRegs  = HoldRegs;
        }

    hal->methods = &m_Tha60210051HalSimOverride;
    }

static void Override(AtHal self)
    {
    OverrideTha60210051HalSim(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081HalSim);
    }

static AtHal ObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (Tha60290022HalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal Tha60290081HalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return ObjectInit(hal, sizeInMbyte);
    }


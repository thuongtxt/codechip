/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : Tha60210012HalSim.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : HAL simulation for product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051HalSimInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012HalSim
    {
    tTha60210051HalSim super;
    }tTha60210012HalSim;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;

/* Override */
static tTha60210051HalSimMethods m_Tha60210051HalSimOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsCdrRegister(Tha60210051HalSim self, uint32 localAddress)
    {
    AtUnused(self);
    if (((localAddress >= 0x0300000) && (localAddress <= 0x033FFFF)) || /* ho part */
        ((localAddress >= 0x0C00000) && (localAddress <= 0x0DFFFFF)))   /* lo part */
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PlaLongRegisterAccessCreate(Tha60210051HalSim self, uint32 address, const uint32 **holdRegs)
    {
    static uint32 holdRegisters[] = {0x421000, 0x421001, 0x421002};
    *holdRegs = holdRegisters;
    AtUnused(address);
    AtUnused(self);
    return mCount(holdRegisters);
    }

static void OverrideTha60210051HalSim(AtHal self)
    {
    Tha60210051HalSim hal = (Tha60210051HalSim)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_Tha60210051HalSimOverride, hal->methods, sizeof(m_Tha60210051HalSimOverride));

        m_Tha60210051HalSimOverride.IsCdrRegister  = IsCdrRegister;
        m_Tha60210051HalSimOverride.PlaLongRegisterAccessCreate  = PlaLongRegisterAccessCreate;
        }

    hal->methods = &m_Tha60210051HalSimOverride;
    }

static void Override(AtHal self)
    {
    OverrideTha60210051HalSim(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012HalSim);
    }

static AtHal ObjectInit(AtHal self, uint32 sizeInMbyte)
    {
    if (Tha60210051HalSimObjectInit(self, sizeInMbyte) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal Tha60210012HalSimNew(uint32 sizeInMbyte)
    {
    AtHal hal = AtOsalMemAlloc(ObjectSize());
    if (hal == NULL)
        return NULL;

    return ObjectInit(hal, sizeInMbyte);
    }

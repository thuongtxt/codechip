/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CPLD Module
 *
 * File        : AtCpld.c
 *
 * Created Date: Jun 28, 2016
 *
 * Description : Control CPLD
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCpld.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/
/* Registers CPLD */
#define cCpldBaseAddress        0xf00000

#define cCpldVerion             0x6e
#define cCpldVersionMask        cBit3_0
#define cCpldVersionShift       0
#define cCpldDownloadedMask     cBit5_4
#define cCpldDownloadedShift    4

#define cCpldLoopback                0x42
#define cCpldLoopbackDs1RemoteMask   cBit4
#define cCpldLoopbackDs1RemoteShift  4
#define cCpldLoopbackDs1LocalMask    cBit0
#define cCpldLoopbackDs1LocalShift   0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtCpld
    {
    AtHal hal;
    uint32 baseAddress;
    }tAtCpld;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHal HalGet(AtCpld self)
    {
    return self ? self->hal : NULL;
    }

static uint32 BaseAddress(AtCpld self)
    {
    if (self->baseAddress == 0)
        return cCpldBaseAddress;

    return self->baseAddress;
    }

static uint32 Version(AtCpld self)
    {
    AtHal hal = HalGet(self);
    uint32 regAddr = BaseAddress(self) + cCpldVerion;
    uint32 regVal = AtHalRead(hal, regAddr);

    return mRegField(regVal, cCpldVersion);
    }

static eBool Downloaded(AtCpld self)
    {
    AtHal hal = HalGet(self);
    uint32 regAddr = BaseAddress(self) + cCpldVerion;
    uint32 regVal = AtHalRead(hal, regAddr);
    uint32 downloaded = mRegField(regVal, cCpldDownloaded);

    if (downloaded == 2)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Ds1LoopbackSet(AtCpld self, eAtLoopbackMode loopbackMode)
    {
    AtHal hal = HalGet(self);
    uint32 regAddr = BaseAddress(self) + cCpldLoopback;
    uint32 regVal = AtHalRead(hal, regAddr);

    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            mFieldIns(&regVal, cCpldLoopbackDs1LocalMask, cCpldLoopbackDs1LocalShift, 0x1);
            mFieldIns(&regVal, cCpldLoopbackDs1RemoteMask, cCpldLoopbackDs1RemoteShift, 0x0);
            break;

        case cAtLoopbackModeRemote:
            mFieldIns(&regVal, cCpldLoopbackDs1RemoteMask, cCpldLoopbackDs1RemoteShift, 0x1);
            mFieldIns(&regVal, cCpldLoopbackDs1LocalMask, cCpldLoopbackDs1LocalShift, 0x0);
            break;

        case cAtLoopbackModeDual:
            mFieldIns(&regVal, cCpldLoopbackDs1LocalMask, cCpldLoopbackDs1LocalShift, 0x1);
            mFieldIns(&regVal, cCpldLoopbackDs1RemoteMask, cCpldLoopbackDs1RemoteShift, 0x1);
            break;

        case cAtLoopbackModeRelease:
            mFieldIns(&regVal, cCpldLoopbackDs1LocalMask, cCpldLoopbackDs1LocalShift, 0x0);
            mFieldIns(&regVal, cCpldLoopbackDs1RemoteMask, cCpldLoopbackDs1RemoteShift, 0x0);
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    AtHalWrite(hal, regAddr, regVal);

    return cAtOk;
    }

static eAtLoopbackMode Ds1LoopbackGet(AtCpld self)
    {
    uint32 regAddr = BaseAddress(self) + cCpldLoopback;
    uint32 regVal = AtHalRead(HalGet(self), regAddr);
    eBool localLoop = mBinToBool(mRegField(regVal, cCpldLoopbackDs1Local));
    eBool remoteLoop = mBinToBool(mRegField(regVal, cCpldLoopbackDs1Remote));

    if (localLoop && remoteLoop)
        return cAtLoopbackModeDual;

    if (localLoop)
        return cAtLoopbackModeLocal;

    if (remoteLoop)
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static const char *ToString(AtCpld self)
    {
    static char description[32];
    AtSnprintf(description, sizeof(description), "cpld by HAL: %s", AtHalToString(self->hal));
    return description;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCpld);
    }

static AtCpld ObjectInit(AtCpld self, uint32 baseAddress, AtHal hal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (hal == NULL)
        return NULL;

    self->hal = hal;
    self->baseAddress = baseAddress;

    return self;
    }

AtCpld AtCpldNew(uint32 baseAddress, AtHal hal)
    {
    AtCpld newCpld = AtOsalMemAlloc(ObjectSize());

    if (ObjectInit(newCpld, baseAddress, hal) == NULL)
        AtCpldDelete(newCpld);

    return newCpld;
    }

void AtCpldDelete(AtCpld self)
    {
    AtOsalMemFree(self);
    }

uint32 AtCpldVersion(AtCpld self)
    {
    if (self)
        return Version(self);
    return 0x0;
    }

eBool AtCpldIsDownloaded(AtCpld self)
    {
    if (self)
        return Downloaded(self);
    return cAtFalse;
    }

eAtRet AtCpldDs1LoopbackSet(AtCpld self, eAtLoopbackMode loopbackMode)
    {
    if (self)
        return Ds1LoopbackSet(self, loopbackMode);
    return cAtErrorNullPointer;
    }

eAtLoopbackMode AtCpldDs1LoopbackGet(AtCpld self)
    {
    if (self)
        return Ds1LoopbackGet(self);
    return cAtLoopbackModeRelease;
    }

const char *AtCpldToString(AtCpld self)
    {
    if (self)
        return ToString(self);
    return NULL;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : HAL
 * 
 * File        : AtHalIndirectEpInternal.h
 * 
 * Created Date: Sep 24, 2014
 *
 * Description : HAL to work on EP platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALINDIRECTEPINTERNAL_H_
#define _ATHALINDIRECTEPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalIndirectEp * AtHalIndirectEp;

typedef struct tAtHalIndirectEpMethods
    {
    uint32 (*RegisterIndirectControl)(AtHalIndirectEp self);
    uint32 (*RegisterIndirectData)(AtHalIndirectEp self);
    }tAtHalIndirectEpMethods;

typedef struct tAtHalIndirectEp
    {
    tAtHalDefault super;
    const tAtHalIndirectEpMethods *methods;

    /* To store content of indirect registers */
    uint32 indirectControl;
    uint32 indirectData;
    uint8  isInIsrContext;

    /* Debug */
    uint32 maxRetry; /* Maximum retry times while software wait to read/write FPGA */
    }tAtHalIndirectEp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalIndirectEpObjectInit(AtHal self, uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALINDIRECTEPINTERNAL_H_ */


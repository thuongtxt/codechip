/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHalDefaultWithHandlerInternal.h
 * 
 * Created Date: Nov 7, 2016
 *
 * Description : HAL for product that needs to handle direct read/write at
 *               application layer.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALDEFAULTWITHHANDLERINTERNAL_H_
#define _ATHALDEFAULTWITHHANDLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalDefaultWithHandler
    {
    tAtHalDefault super;

    /* Private data */
    AtHalDirectWriteHandler writeFunc;
    AtHalDirectReadHandler  readFunc;

    uint32 readCount;
    uint32 writeCount;
    }tAtHalDefaultWithHandler;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalDefaultWithHandlerObjectInit(AtHal self, uint32 baseAddress, AtHalDirectWriteHandler writeHandler, AtHalDirectReadHandler readHandler);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALDEFAULTWITHHANDLERINTERNAL_H_ */


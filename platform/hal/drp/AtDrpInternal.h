/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtDrpInternal.h
 * 
 * Created Date: Oct 22, 2016
 *
 * Description : DRP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDRPINTERNAL_H_
#define _ATDRPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDrp.h"
#include "AtObject.h" /* For object macros */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDrpMethods
    {
    uint32 (*Read)(AtDrp self, uint32 address);
    eAtRet (*Write)(AtDrp self, uint32 address, uint32 value);
    }tAtDrpMethods;

typedef struct tAtDrp
    {
    const tAtDrpMethods *methods;

    /* Private data */
    AtHal hal;
    uint32 baseAddress;
    uint32 portId;
    eBool needSelectPort;
    uint32 maxPortNum;
    const tAtDrpAddressCalculator *addressCalculator;
    }tAtDrp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDrp AtDrpObjectInit(AtDrp self, uint32 baseAddress, AtHal hal);

void AtDrpNumberOfSerdesControllersSet(AtDrp self, uint32 maxNumSerdesControllers);
void AtDrpNeedSelectPortSet(AtDrp self, eBool needSelectPort);

#ifdef __cplusplus
}
#endif
#endif /* _ATDRPINTERNAL_H_ */


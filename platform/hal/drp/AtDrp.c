/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtDrp.c
 *
 * Created Date: Oct 27, 2015
 *
 * Description : DRP implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDrpInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue  0xCAFECAFE
#define cInvalidPort   0xFFFFFFFF

#define cDrpPortSelectMask  cBit3_0
#define cDrpPortSelectShift 0
#define cDrpPortSelectNone  0
#define cDrpPortSelect(portId) ((portId) + 1)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((tAtDrp *)self)
#define mRegisterWithOffset(self, address)  (mThis(self)->baseAddress + SelectedPortOffset(self) + (address & cBit15_0))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtDrpMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PortIsValid(AtDrp self, uint32 portId)
    {
    return (portId > (self->maxPortNum)) ? cAtFalse : cAtTrue;
    }

static uint32 DefaultPortOffset(AtDrp self, uint32 selectedPortId)
    {
    AtUnused(self);

    if ((selectedPortId == 0) || (selectedPortId == 1))
        return 0xC0000;

    if ((selectedPortId == 2) || (selectedPortId == 3))
        return 0xD0000;

    return 0x0;
    }

static uint32 PortOffset(AtDrp self, uint32 selectedPortId)
    {
    if (self->addressCalculator && self->addressCalculator->PortOffset)
        return self->addressCalculator->PortOffset(self, selectedPortId);

    return DefaultPortOffset(self, selectedPortId);
    }

static uint32 SelectedPortOffset(AtDrp self)
    {
    uint32 selectedPortId;

    if (self == NULL)
        return 0x0;

    selectedPortId = AtDrpSelectedPort(self);
    if (self->needSelectPort)
        {
        if (!PortIsValid(self, selectedPortId))
            return 0x0;
        }

    return PortOffset(self, selectedPortId);
    }

static uint32 PortSelectRegister(AtDrp self)
    {
    if (self->addressCalculator && self->addressCalculator->PortSelectRegisterAddress)
        return self->addressCalculator->PortSelectRegisterAddress(self);

    return mThis(self)->baseAddress + 0x43;
    }

static uint32 PortSelectRegisterMask(AtDrp self)
    {
    if (self->addressCalculator && self->addressCalculator->PortSelectRegisterMask)
        return self->addressCalculator->PortSelectRegisterMask(self);

    return cDrpPortSelectMask;
    }

static uint32 PortSelectRegisterShift(AtDrp self)
    {
    if (self->addressCalculator && self->addressCalculator->PortSelectRegisterShift)
        return self->addressCalculator->PortSelectRegisterShift(self);

    return cDrpPortSelectShift;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDrp);
    }

static AtHal Hal(AtDrp self)
    {
    return AtDrpHalGet(self);
    }

static void PortDeselect(AtDrp self)
    {
    uint32 regVal, mask, shift, address, value;

    if (!self->needSelectPort)
        return;

    address = PortSelectRegister(self);
    mask    = PortSelectRegisterMask(self);
    shift   = PortSelectRegisterShift(self);
    regVal  = AtHalRead(Hal(self), address);
    value   = cDrpPortSelectNone;
    mFieldIns(&regVal, mask, shift, value);
    AtHalWrite(Hal(self), address, regVal);
    }

static uint32 PortToSelect(AtDrp self)
    {
    uint32 portId = mThis(self)->portId;

    if (portId == cInvalidValue)
        portId = 0;

    return portId;
    }

static void PortSelect(AtDrp self, uint32 portId)
    {
    uint32 regVal, mask, shift, address, value;

    if (!self->needSelectPort)
        return;

    address = PortSelectRegister(self);
    mask    = PortSelectRegisterMask(self);
    shift   = PortSelectRegisterShift(self);
    regVal  = AtHalRead(Hal(self), address);
    value   = portId + 1;
    mFieldIns(&regVal, mask, shift, value);
    AtHalWrite(Hal(self), address, regVal);
    }

static eAtRet Write(AtDrp self, uint32 address, uint32 value)
    {
    PortSelect(self, PortToSelect(self));

    AtHalWrite(Hal(self), mRegisterWithOffset(self, address), value);

    /* Need to de-select port because DRP is parallel interface.
     * But no need to clear selected port in database in this case */
    PortDeselect(self);

    return cAtOk;
    }

static uint32 Read(AtDrp self, uint32 address)
    {
    uint32 value;

    PortSelect(self, PortToSelect(self));
    value = AtHalRead(Hal(self), mRegisterWithOffset(self, address));

    /* Need to de-select port because DRP is parallel interface.
     * But no need to clear selected port in database in this case */
    PortDeselect(self);
    return value;
    }

static uint32 SelectedPort(AtDrp self)
    {
    uint32 regVal;
    uint32 port;

    if (!self->needSelectPort)
        return mThis(self)->portId;

    regVal = AtHalRead(Hal(self), PortSelectRegister(self));
    port = mRegField(regVal, cDrpPortSelect);

    if (port == cDrpPortSelectNone)
        return mThis(self)->portId;

    return port - 1;
    }

static void AddressCalculatorSet(AtDrp self, const tAtDrpAddressCalculator *calculator)
    {
    self->addressCalculator = calculator;
    }

static void MethodsInit(AtDrp self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        }

    mMethodsSet(self, &m_methods);
    }

AtDrp AtDrpObjectInit(AtDrp self, uint32 baseAddress, AtHal hal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    if (hal == NULL)
        return NULL;

    self->hal            = hal;
    self->baseAddress    = baseAddress;
    self->portId         = cInvalidPort;
    self->needSelectPort = cAtTrue;
    return self;
    }

static const char *ToString(AtDrp self)
    {
    static char description[64];

    AtSnprintf(description, sizeof(description), "hal: %s, baseAddress: 0x%08x",
               self->hal ? "installed" : "none",
               self->baseAddress);

    return description;
    }

static uint32 BaseAddress(AtDrp self)
    {
    return self->baseAddress;
    }

AtDrp AtDrpNew(uint32 baseAddress, AtHal hal)
    {
    AtDrp drp = AtOsalMemAlloc(ObjectSize());
    if (drp == NULL)
        return NULL;

    if (AtDrpObjectInit(drp, baseAddress, hal) == NULL)
        AtDrpDelete(drp);

    return drp;
    }

void AtDrpDelete(AtDrp self)
    {
    if (self)
        AtOsalMemFree(self);
    }

void AtDrpPortDeselect(AtDrp self)
    {
    if (self == NULL)
        return;

    PortDeselect(self);
    mThis(self)->portId = cInvalidPort;
    }

void AtDrpPortSelect(AtDrp self, uint32 portId)
    {
    if ((self == NULL) || !PortIsValid(self, portId))
        return;

    PortSelect(self, portId);
    mThis(self)->portId = portId;
    }

uint32 AtDrpSelectedPort(AtDrp self)
    {
    if (self)
        return SelectedPort(self);
    return cInvalidValue;
    }

uint32 AtDrpRead(AtDrp self, uint32 address)
    {
    if (self)
        return mMethodsGet(self)->Read(self, address);
    return cInvalidValue;
    }

eAtRet AtDrpWrite(AtDrp self, uint32 address, uint32 value)
    {
    if (self)
        return mMethodsGet(self)->Write(self, address, value);
    return cAtErrorNullPointer;
    }

AtHal AtDrpHalGet(AtDrp self)
    {
    if (self)
        return mThis(self)->hal;
    return NULL;
    }

void AtDrpAddressCalculatorSet(AtDrp self, const tAtDrpAddressCalculator *calculator)
    {
    if (self)
        AddressCalculatorSet(self, calculator);
    }

void AtDrpNumberOfSerdesControllersSet(AtDrp self, uint32 maxNumSerdesControllers)
    {
    if (self)
        self->maxPortNum = maxNumSerdesControllers;
    }

void AtDrpNeedSelectPortSet(AtDrp self, eBool needSelectPort)
    {
    if (self)
        self->needSelectPort = needSelectPort;
    }

const char *AtDrpToString(AtDrp self)
    {
    if (self)
        return ToString(self);
    return NULL;
    }

uint32 AtDrpBaseAddress(AtDrp self)
    {
    if (self)
        return BaseAddress(self);
    return cInvalidUint32;
    }

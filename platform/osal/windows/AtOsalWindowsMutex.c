/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalWindowsMutex.c
 *
 * Created Date: Aug 2, 2014
 *
 * Description : Mutex
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsalWindows.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalWindowsMutex
    {
    tAtOsalMutex super;
    }tAtOsalWindowsMutex;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtOsalMutexMethods m_AtOsalMutexOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtOsalRet Delete(AtOsalMutex self)
    {
    AtOsalMemFree(self);
    return cAtOsalOk;
    }

static eAtOsalRet Lock(AtOsalMutex sem)
    {
	AtUnused(sem);
    return cAtOsalOk;
    }

static eAtOsalRet TryLock(AtOsalMutex sem)
    {
	AtUnused(sem);
    return cAtOsalOk;
    }

static eAtOsalRet UnLock(AtOsalMutex sem)
    {
	AtUnused(sem);
    return cAtOsalOk;
    }

static void OverrideAtOsalMutex(AtOsalMutex self)
    {
    AtOsalMutex sem = (AtOsalMutex)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalMutexOverride, &(self->methods), sizeof(m_AtOsalMutexOverride));

        m_AtOsalMutexOverride.Delete  = Delete;
        m_AtOsalMutexOverride.Lock    = Lock;
        m_AtOsalMutexOverride.TryLock = TryLock;
        m_AtOsalMutexOverride.UnLock  = UnLock;
        }

    sem->methods = &m_AtOsalMutexOverride;
    }

static void Override(AtOsalMutex self)
    {
    OverrideAtOsalMutex(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalWindowsMutex);
    }

static AtOsalMutex ObjectInit(AtOsalMutex self, AtOsal osal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalMutexObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtOsalMutex AtOsalWindowsMutexNew(AtOsal osal)
    {
    AtOsalMutex newMutex = AtOsalMemAlloc(ObjectSize());
    if (newMutex == NULL)
        return NULL;

    return ObjectInit(newMutex, osal);
    }

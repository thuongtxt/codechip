/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalBasic.c
 *
 * Created Date: Aug 1, 2012
 *
 * Author      : namnn
 *
 * Description : OSAL - Simulate OSAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsalWindows.h"

#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtOsalMethods m_AtOsalOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Memory operations */
static void* MemAlloc(AtOsal self, uint32 size)
    {
	AtUnused(self);
    return malloc(size);
    }

static void MemFree(AtOsal self, void* pMem)
    {
	AtUnused(self);
    free(pMem);
    }

static void* MemInit(AtOsal self, void* pMem, uint32 val, uint32 size)
    {
	AtUnused(self);
    return memset(pMem, (int)val, size);
    }

static void* MemCpy(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
	AtUnused(self);
    return memcpy(pDest, pSrc, size);
    }

static int MemCmp(AtOsal self, const void *pMem1, const void *pMem2, uint32 size)
    {
	AtUnused(self);
    return memcmp(pMem1, pMem2, size);
    }

static void USleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(timeToSleep);
	AtUnused(self);
    }

static void Sleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(timeToSleep);
	AtUnused(self);
    }

static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
    struct timeval tm;
	AtUnused(self);

    if (currentTime == NULL)
        return;

    gettimeofday(&tm, NULL);
    currentTime->sec  = (uint32)tm.tv_sec;
    currentTime->usec = (uint32)tm.tv_usec;
    }

static uint32 DifferenceTimeInMs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
	AtUnused(self);
    return ((((currentTime->sec - previousTime->sec) * 1000) + (currentTime->usec / 1000)) - (previousTime->usec / 1000));
    }

static uint32 DifferenceTimeInUs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    AtUnused(self);
    return ((currentTime->sec - previousTime->sec) * 1000000) + currentTime->usec - previousTime->usec;
    }

static AtOsalSem SemCreate(AtOsal self, eBool procEn, uint32 initVal)
    {
	AtUnused(initVal);
	AtUnused(procEn);
    return AtOsalWindowsSemNew(self);
    }

static AtOsalMutex MutexCreate(AtOsal self)
    {
    return AtOsalWindowsMutexNew(self);
    }

static AtAttributeNoReturn void Panic(AtOsal self)
    {
	AtUnused(self);
    assert(0);
    }

static AtStd Std(AtOsal self)
    {
    AtUnused(self);
    return AtStdDefaultSharedStd();
    }

static eBool IsMultitasking(AtOsal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtTaskManager TaskManagerGet(AtOsal self)
    {
    /* This has not been implemented. But having this is optional as SDK
     * internal implementation will not create any task */
    AtUnused(self);
    return NULL;
    }

static AtConnectionManager ConnectionManager(AtOsal self)
    {
    /* This has not been implemented. But having this is optional as SDK
     * internal implementation will not depend on network connection */
    AtUnused(self);
    return NULL;
    }

static void OverrideAtOsal(AtOsal self)
    {
    if (!m_methodsInit)
        {
        /* Memory operations */
        m_AtOsalOverride.MemAlloc = MemAlloc;
        m_AtOsalOverride.MemFree  = MemFree;
        m_AtOsalOverride.MemInit  = MemInit;
        m_AtOsalOverride.MemCpy   = MemCpy;
        m_AtOsalOverride.MemCmp   = MemCmp;

        /* Semaphore */
        m_AtOsalOverride.SemCreate = SemCreate;

        /* Mutex */
        m_AtOsalOverride.MutexCreate = MutexCreate;

        /* Delay functions */
        m_AtOsalOverride.Sleep      = Sleep;
        m_AtOsalOverride.USleep     = USleep;
        m_AtOsalOverride.CurTimeGet = CurTimeGet;
        m_AtOsalOverride.DifferenceTimeInMs = DifferenceTimeInMs;
        m_AtOsalOverride.DifferenceTimeInUs = DifferenceTimeInUs;

        /* Utils */
        m_AtOsalOverride.Panic = Panic;
        m_AtOsalOverride.Std   = Std;
        m_AtOsalOverride.IsMultitasking    = IsMultitasking;
        m_AtOsalOverride.TaskManagerGet    = TaskManagerGet;
        m_AtOsalOverride.ConnectionManager = ConnectionManager;
        }

    self->methods = &m_AtOsalOverride;
    }

static AtOsal ObjectInit(AtOsal self)
    {
    OverrideAtOsal(self);
    m_methodsInit = 1;

    return self;
    }

AtOsal AtOsalWindows(void)
    {
    static tAtOsal m_osal;
    static AtOsal  m_sharedOsal = NULL;

    if (m_sharedOsal)
        return m_sharedOsal;

    m_sharedOsal = ObjectInit((AtOsal)&m_osal);
    return m_sharedOsal;
    }

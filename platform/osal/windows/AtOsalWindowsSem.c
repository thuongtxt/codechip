/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalWindowsSem.c
 *
 * Created Date: Aug 2, 2014
 *
 * Description : Semaphore
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsalWindows.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtOsalWindowsSem *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalWindowsSem
    {
    tAtOsalSem super;

    uint32 count;
    }tAtOsalWindowsSem;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtOsalSemMethods m_AtOsalSemOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtOsalRet SemDelete(AtOsalSem self)
    {
    AtOsalMemFree(self);
    return cAtOsalOk;
    }

static eAtOsalRet SemTake(AtOsalSem self)
    {
    while (mThis(self)->count > 0);

    mThis(self)->count += 1;

    return cAtOsalOk;
    }

static eAtOsalRet SemTryTake(AtOsalSem self)
    {
    if (mThis(self)->count == 0)
        mThis(self)->count += 1;

    return cAtOsalOk;
    }

static eAtOsalRet SemGive(AtOsalSem self)
    {
    mThis(self)->count -= 1;
    return cAtOsalOk;
    }

static void OverrideAtOsalSem(AtOsalSem self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalSemOverride, self->methods, sizeof(m_AtOsalSemOverride));

        m_AtOsalSemOverride.Delete  = SemDelete;
        m_AtOsalSemOverride.Take    = SemTake;
        m_AtOsalSemOverride.TryTake = SemTryTake;
        m_AtOsalSemOverride.Give    = SemGive;
        }

    self->methods = &m_AtOsalSemOverride;
    }

static void Override(AtOsalSem self)
    {
    OverrideAtOsalSem(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalWindowsSem);
    }

static AtOsalSem ObjectInit(AtOsalSem self, AtOsal osal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalSemObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtOsalSem AtOsalWindowsSemNew(AtOsal osal)
    {
    AtOsalSem newSem = AtOsalMemAlloc(ObjectSize());
    if (newSem == NULL)
        return NULL;

    return ObjectInit(newSem, osal);
    }

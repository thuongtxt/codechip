/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalWindows.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Windows OSAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALWINDOWS_H_
#define _ATOSALWINDOWS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsalSem AtOsalWindowsSemNew(AtOsal osal);
AtOsalMutex AtOsalWindowsMutexNew(AtOsal osal);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALWINDOWS_H_ */


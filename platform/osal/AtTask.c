/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Task Manager
 *
 * File        : AtTask.c
 *
 * Created Date: Feb 6, 2015
 *
 * Description : Task abstraction
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtObject.h"
#include "AtTaskInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtTaskMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Start(AtTask self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet Stop(AtTask self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet Join(AtTask self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static void TestCancel(AtTask self)
    {
    /* Let concrete class do */
    AtUnused(self);
    }

static eAtRet Delete(AtTask self)
    {
    AtOsalMemFree(self);
    return cAtOk;
    }

static void MethodsInit(AtTask self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Start);
        mMethodOverride(m_methods, Stop);
        mMethodOverride(m_methods, Join);
        mMethodOverride(m_methods, TestCancel);
        mMethodOverride(m_methods, Delete);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTask);
    }

AtTask AtTaskObjectInit(AtTask self, const char *taskName, void *userData, void *(*taskHandler)(void *userData))
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;
    self->taskHandler = taskHandler;
    self->userData = userData;
    self->taskName = taskName;

    return self;
    }

eAtRet AtTaskDelete(AtTask self)
    {
    if (self)
        return mMethodsGet(self)->Delete(self);
    return cAtOk;
    }

/**
 * @addtogroup AtTask
 * @{
 */

/**
 * Start a task
 *
 * @param self Task
 *
 * @return AT return code
 */
eAtRet AtTaskStart(AtTask self)
    {
    return (self) ? mMethodsGet(self)->Start(self) : cAtErrorNullPointer;
    }

/**
 * Stop a task
 *
 * @param self Task
 *
 * @return AT return code
 */
eAtRet AtTaskStop(AtTask self)
    {
    return (self) ? mMethodsGet(self)->Stop(self) : cAtErrorNullPointer;
    }

/**
 * Wait for a task finish its jobs
 *
 * @param self Task
 *
 * @return AT return code
 */
eAtRet AtTaskJoin(AtTask self)
    {
    return (self) ? mMethodsGet(self)->Join(self) : cAtErrorNullPointer;
    }

/**
 * Test cancel
 *
 * @param self Task
 *
 * @return None
 */
void AtTaskTestCancel(AtTask self)
    {
    if (self)
        mMethodsGet(self)->TestCancel(self);
    }


/**
 * Get task Name
 *
 * @param self Task
 *
 * @return Task name string
 */
const char *AtTaskNameGet(AtTask self)
    {
    return (self) ? self->taskName : NULL;
    }

/**
 * Get task user data
 *
 * @param self This task
 * @return User data
 */
void *AtTaskUserDataGet(AtTask self)
    {
    return self ? self->userData : NULL;
    }

/**
 * @}
 */

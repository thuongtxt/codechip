/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalLinuxDebug.c
 *
 * Created Date: Jun 13, 2013
 *
 * Description : OSAL Linux used for debugging purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdlib.h>
#include "AtOsalLinuxDebugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtOsalLinuxDebugClass)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalLinuxDebugClass * AtOsalLinuxDebugClass;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtOsalLinuxDebugClass m_osal;
static uint8 m_methodsInit = 0;
static AtOsal m_sharedOsal = NULL;

/* Override */
static tAtOsalMethods m_AtOsalOverride;

/* Save super's implementation */
static const tAtOsalMethods *m_AtOsalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtOsalLeakTracker LeakTracker(AtOsal self)
    {
    if (mThis(self)->leakTracker == NULL)
        {
        mThis(self)->leakTracker = AtOsalLeakTrackerSharedTracker();
        mThis(self)->leakTrackerLock = AtOsalMutexCreate();

        AtOsalMutexLock(mThis(self)->leakTrackerLock);
        AtOsalLeakTrackerUnTrack(mThis(self)->leakTracker, mThis(self)->leakTrackerLock);
        AtOsalMutexUnLock(mThis(self)->leakTrackerLock);
        }

    return mThis(self)->leakTracker;
    }

static void* MemAlloc(AtOsal self, uint32 size)
    {
    void *memory = m_AtOsalMethods->MemAlloc(self, size);
    AtOsalLeakTracker leakTracker = LeakTracker(self);

    AtOsalMutexLock(mThis(self)->leakTrackerLock);
    AtOsalLeakTrackerTrack(leakTracker, memory, size);
    AtOsalMutexUnLock(mThis(self)->leakTrackerLock);

    return memory;
    }

static void MemFree(AtOsal self, void* pMem)
    {
    if (mThis(self)->leakTrackerLock == pMem)
        m_AtOsalMethods->MemFree(self, pMem);
    else
        {
        AtOsalLeakTracker leakTracker = LeakTracker(self);
        m_AtOsalMethods->MemFree(self, pMem);
        AtOsalMutexLock(mThis(self)->leakTrackerLock);
        AtOsalLeakTrackerUnTrack(leakTracker, pMem);
        AtOsalMutexUnLock(mThis(self)->leakTrackerLock);
        }
    }

static void OverrideAtOsal(AtOsal self)
    {
    if (!m_methodsInit)
        {
        m_AtOsalMethods = self->methods;
        self->methods->MemCpy(self, &m_AtOsalOverride, m_AtOsalMethods, sizeof(m_AtOsalOverride));

        m_AtOsalOverride.MemAlloc  = MemAlloc;
        m_AtOsalOverride.MemFree   = MemFree;
        }

    self->methods = &m_AtOsalOverride;
    }

AtOsal AtOsalLinuxDebugObjectInit(AtOsal self)
    {
    /* Super constructor */
    if (AtOsalLinuxObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtOsal(self);
    m_methodsInit = 1;

    return self;
    }

AtOsal AtOsalLinuxDebug(void)
    {
    if (m_sharedOsal == NULL)
        m_sharedOsal = AtOsalLinuxDebugObjectInit((AtOsal)&m_osal);

    return m_sharedOsal;
    }

void AtOsalLinuxDebugCleanUp(AtOsal self)
    {
    AtOsalLeakTrackerCleanUp(mThis(self)->leakTracker);
    if (mThis(self)->leakTrackerLock)
        AtOsalMutexDestroy(mThis(self)->leakTrackerLock);
    }

AtOsalLeakTracker AtOsalLinuxDebugLeakTracker(AtOsal self)
    {
    return LeakTracker(self);
    }

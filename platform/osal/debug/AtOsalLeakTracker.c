/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalLeakTracker.c
 *
 * Created Date: Jun 13, 2013
 *
 * Description : Leak tracker
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "commacro.h"
#include "AtOsalLeakTracker.h"
#include "AtOsalLinuxDebug.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalLeakTracker
    {
    uint32 totalAlloc;
    uint32 totalFree;
    eBool disabled;
    }tAtOsalLeakTracker;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtOsalLeakTracker m_sharedTracker = NULL;
static tAtOsalLeakTracker m_globalTracker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtOsal Osal(void)
    {
    return AtOsalLinuxDebug();
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalLeakTracker);
    }

static AtOsalLeakTracker ObjectInit(AtOsalLeakTracker self)
    {
    AtOsal osal = Osal();
    osal->methods->MemInit(osal, self, 0, ObjectSize());
    return self;
    }

AtOsalLeakTracker AtOsalLeakTrackerSharedTracker(void)
    {
    if (m_sharedTracker == NULL)
        m_sharedTracker = ObjectInit(&m_globalTracker);
    return m_sharedTracker;
    }

eAtRet AtOsalLeakTrackerTrack(AtOsalLeakTracker self, void *memory, uint32 size)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* Not track itself */
    if (memory == self)
        return cAtOk;

    /* Or do not track NULL memory */
    if (memory == NULL)
        return cAtOk;

    /* Force memory size must be greater than 0 */
    AtAssert(size > 0);

    if (self->disabled)
        return cAtOk;

    /* Track */
    self->totalAlloc = self->totalAlloc + 1;

    return cAtOk;
    }

eAtRet AtOsalLeakTrackerUnTrack(AtOsalLeakTracker self, void *memory)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    if (memory == self)
        return cAtOk;
    if (memory == NULL)
        return cAtOk;

    if (self->disabled)
        return cAtOk;

    self->totalFree = self->totalFree + 1;

    return cAtOk;
    }

void AtOsalLeakTrackerCleanUp(AtOsalLeakTracker self)
    {
    self->totalAlloc = 0;
    self->totalFree  = 0;
    }

void AtOsalLeakTrackerReport(AtOsalLeakTracker self)
    {
    /* No leak */
    if (!AtOsalLeakTrackerIsLeak(self))
        {
        AtPrintc(cSevCritical, "No leak is found\r\n");
        return;
        }

    /* Detail report */
    AtPrintc(cSevCritical, "MEMORY LEAK, detail:\r\n");
    AtPrintc(cSevNormal, "* Malloc   : %u addresses\r\n", self->totalAlloc);
    AtPrintc(cSevNormal, "* Free     : %u addresses\r\n", self->totalFree);
    AtPrintc(cSevNormal, "* Reachable: %u addresses\r\n", self->totalAlloc - self->totalFree);
    }

eBool AtOsalLeakTrackerIsLeak(AtOsalLeakTracker self)
    {
    return (self->totalAlloc != self->totalFree) ? cAtTrue : cAtFalse;
    }

void AtOsalLeakTrackerTest(void)
    {
    AtOsalLeakTracker tracker = AtOsalLeakTrackerSharedTracker();
    AtOsal osal, backupOsal;
    void *memory;

    AtOsalLeakTrackerCleanUp(tracker);
    osal = AtOsalLinuxDebug();
    backupOsal = AtOsalSharedGet();

    AtOsalSharedSet(osal);
    memory = AtOsalMemAlloc(8);
    AtOsalLeakTrackerReport(tracker);
    AtOsalMemFree(memory);
    AtOsalLeakTrackerReport(tracker);

    AtOsalSharedSet(backupOsal);
    }

eBool AtOsalLeakTrackerIsEnabled(AtOsalLeakTracker self)
    {
    if (self == NULL)
        return cAtFalse;

    return self->disabled ? cAtFalse : cAtTrue;
    }

eAtRet AtOsalLeakTrackerEnable(AtOsalLeakTracker self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    self->disabled = enable ? cAtFalse : cAtTrue;
    return cAtOk;
    }

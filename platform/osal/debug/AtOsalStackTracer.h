/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalStackTracer.h
 * 
 * Created Date: Aug 24, 2015
 *
 * Description : Stack tracer utility
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _PLATFORM_OSAL_DEBUG_ATOSALSTACKTRACER_H_
#define _PLATFORM_OSAL_DEBUG_ATOSALSTACKTRACER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtOsalStackTracer * AtOsalStackTracer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsalStackTracer AtOsalStackTracerCreate(uint8 size);
void AtOsalStackTracerDelete(AtOsalStackTracer self);
char* AtOsalStackTracerPop(AtOsalStackTracer self);

#ifdef __cplusplus
}
#endif
#endif /* _PLATFORM_OSAL_DEBUG_ATOSALSTACKTRACER_H_ */


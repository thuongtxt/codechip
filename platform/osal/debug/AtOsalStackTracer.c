/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalStackTracer.c
 *
 * Created Date: Aug 24, 2015
 *
 * Description : Stack tracer, note that this tracer has not supported thread-safe yet.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <execinfo.h>
#include "atclib.h"
#include "commacro.h"
#include "AtOsalLinuxDebug.h"
#include <stdlib.h>

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalStackTracer
    {
    void **buffer;
    char **strings;

    int currentIndex;
    int numberOfFuntion;
    uint8 size;

    }tAtOsalStackTracer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtOsal Osal(void)
    {
    return AtOsalLinuxDebug();
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalStackTracer);
    }

static AtOsalStackTracer ObjectInit(AtOsalStackTracer self, uint8 size)
    {
    AtOsal osal = Osal();
    osal->methods->MemInit(osal, self, 0, ObjectSize());
    self->size = size;

    return self;
    }

static uint8 SizeGet(AtOsalStackTracer self)
    {
    const uint8 cDefaultStackSize = 10;
    if (self->size == 0)
        self->size = cDefaultStackSize;

    return self->size;
    }

static void** BufferGet(AtOsalStackTracer self)
    {
    AtOsal osal = Osal();
    if (self->buffer == NULL)
        self->buffer = osal->methods->MemAlloc(osal, SizeGet(self) * sizeof(void*));

    return self->buffer;
    }

static char** StringsArrayGet(AtOsalStackTracer self, uint8 numString)
    {
    AtOsal osal = Osal();

    if (self->strings != NULL)
        osal->methods->MemFree(osal, self->strings);

    self->strings = osal->methods->MemAlloc(osal, numString * sizeof(char*));
    return self->strings;
    }

static int Trace(AtOsalStackTracer self)
    {
    int size, i;
    void** buffer = BufferGet(self);
    char** strings;
    size = SizeGet(self);

    if (buffer == NULL)
        return 0;

    self->numberOfFuntion = backtrace(buffer, size);
    self->currentIndex = 4;

    strings = backtrace_symbols(buffer, self->numberOfFuntion);
    self->strings = StringsArrayGet(self, (uint8)self->numberOfFuntion);
    if (self->strings == NULL)
        {
        free(strings);
        return 0;
        }

    for (i = 0; i < self->numberOfFuntion; i++)
        self->strings[i] = AtStrdup(strings[i]);

    free(strings);
    return self->numberOfFuntion;
    }

static char* FunctionNameExtract(const char* traceMessage)
    {
    char* functionName;
    char* closeParenthesis = AtStrstr(traceMessage, ")");

    *closeParenthesis = '\0';
    functionName = AtStrstr(traceMessage, "(") + 1;
    if (AtStrlen(functionName) == 0)
        return NULL;

    return functionName;
    }

char* AtOsalStackTracerPop(AtOsalStackTracer self)
    {
    char* retString;
    char* functionName;

    if (self->currentIndex >= self->numberOfFuntion)
        return NULL;

    if (self->strings == NULL)
        return NULL;

    retString = self->strings[self->currentIndex];
    self->currentIndex++;

    functionName = FunctionNameExtract(retString);
    if (functionName)
        return functionName;

    return AtOsalStackTracerPop(self);
    }

static void FreeAllString(AtOsalStackTracer self)
    {
    int i;
    AtOsal osal = Osal();

    if (self->strings == NULL)
        return;

    for (i = 0; i < self->numberOfFuntion; i++)
        osal->methods->MemFree(osal, self->strings[i]);

    osal->methods->MemFree(osal, self->strings);
    }

AtOsalStackTracer AtOsalStackTracerCreate(uint8 size)
    {
    AtOsal osal = Osal();
    AtOsalStackTracer newTracer = osal->methods->MemAlloc(osal, ObjectSize());

    ObjectInit(newTracer, size);
    Trace(newTracer);

    return newTracer;
    }

void AtOsalStackTracerDelete(AtOsalStackTracer self)
    {
    AtOsal osal = Osal();
    if (self->strings)
        FreeAllString(self);
    if (self->buffer)
        osal->methods->MemFree(osal, self->buffer);
    osal->methods->MemFree(osal, self);
    }

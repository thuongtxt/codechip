/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalLinuxDebugInternal.h
 * 
 * Created Date: Sep 15, 2016
 *
 * Description : OSAL for debug on Linux
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALLINUXDEBUGINTERNAL_H_
#define _ATOSALLINUXDEBUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../linux/AtOsalLinuxInternal.h"
#include "AtOsalLeakTracker.h"
#include "AtOsalLinuxDebug.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtOsalLinuxDebugClass
    {
    tAtOsalLinux super;

    /* Private data */
    AtOsalLeakTracker leakTracker;
    AtOsalMutex       leakTrackerLock;
    }tAtOsalLinuxDebugClass;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsal AtOsalLinuxDebugObjectInit(AtOsal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALLINUXDEBUGINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalLeakTracker.h
 * 
 * Created Date: Jun 13, 2013
 *
 * Description : Leak tracker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALLEAKTRACKER_H_
#define _ATOSALLEAKTRACKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtOsalLeakTracker * AtOsalLeakTracker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsalLeakTracker AtOsalLeakTrackerSharedTracker(void);
void AtOsalLeakTrackerCleanUp(AtOsalLeakTracker self);

eAtRet AtOsalLeakTrackerEnable(AtOsalLeakTracker self, eBool enable);
eBool AtOsalLeakTrackerIsEnabled(AtOsalLeakTracker self);

/* Memory tracking */
eAtRet AtOsalLeakTrackerTrack(AtOsalLeakTracker self, void *memory, uint32 size);
eAtRet AtOsalLeakTrackerUnTrack(AtOsalLeakTracker self, void *memory);

/* Status query */
eBool AtOsalLeakTrackerIsLeak(AtOsalLeakTracker self);
void AtOsalLeakTrackerReport(AtOsalLeakTracker self);

void AtOsalLeakTrackerTest(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALLEAKTRACKER_H_ */


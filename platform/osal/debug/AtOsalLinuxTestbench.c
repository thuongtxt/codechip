/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalLinuxTestbench.c
 *
 * Created Date: Jun 13, 2013
 *
 * Description : OSAL Linux used for debugging purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/time.h>
#include "AtOsalLinuxDebugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalLinuxTestbench
    {
    tAtOsalLinuxDebugClass super;
    }tAtOsalLinuxTestbench;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtOsalLinuxTestbench m_osal;
static uint8 m_methodsInit = 0;
static AtOsal m_sharedOsal = NULL;

/* Override */
static tAtOsalMethods m_AtOsalOverride;

/* Save super's implementation */
static const tAtOsalMethods *m_AtOsalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
    struct timeval tm;
    AtUnused(self);

    if (currentTime == NULL)
        return;

    gettimeofday(&tm, NULL);
    currentTime->sec  = (uint32)tm.tv_sec;
    currentTime->usec = (uint32)tm.tv_usec;
    }

static void OverrideAtOsal(AtOsal self)
    {
    if (!m_methodsInit)
        {
        m_AtOsalMethods = self->methods;
        self->methods->MemCpy(self, &m_AtOsalOverride, m_AtOsalMethods, sizeof(m_AtOsalOverride));

        m_AtOsalOverride.CurTimeGet = CurTimeGet;
        }

    self->methods = &m_AtOsalOverride;
    }

static AtOsal ObjectInit(AtOsal self)
    {
    /* Super constructor */
    if (AtOsalLinuxDebugObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtOsal(self);
    m_methodsInit = 1;

    return self;
    }

AtOsal AtOsalLinuxTestbench(void)
    {
    if (m_sharedOsal == NULL)
        m_sharedOsal = ObjectInit((AtOsal)&m_osal);

    return m_sharedOsal;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalAmx.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : AMX OSAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALAMX_H_
#define _ATOSALAMX_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsalMutex AtOsalAmxMutexNew(AtOsal osal);
AtOsalSem AtOsalAmxSemNew(AtOsal osal, uint32 initValue);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALAMX_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalAmxSem.c
 *
 * Created Date: May 6, 2013
 *
 * Description : AMX semaphore
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "cjzzz.h"
#include "AtOsalAmx.h"

/*--------------------------- Define -----------------------------------------*/
#define cWaitForever      0
#define cNoWait          -1
#define cHighestPriority  0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtOsalAmxSem)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalAmxSem * AtOsalAmxSem;

typedef struct tAtOsalAmxSem
    {
    tAtOsalSem super;

    /* Private data */
    CJ_ID semId;
    }tAtOsalAmxSem;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtOsalSemMethods m_AtOsalSemOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalAmxSem);
    }

static eAtOsalRet Delete(AtOsalSem self)
    {
    eAtOsalRet ret = cAtOsalOk;

    if (cjsmdelete(mThis(self)->semId) != CJ_EROK)
        ret = cAtOsalFailed;

    AtOsalMemFree(self);

    return ret;
    }

static eAtOsalRet Take(AtOsalSem self)
    {
    if (cjsmwait(mThis(self)->semId, cHighestPriority, cWaitForever) == CJ_EROK)
        return cAtOsalOk;
    return cAtOsalFailed;
    }

static eAtOsalRet TryTake(AtOsalSem self)
    {
    if (cjsmwait(mThis(self)->semId, cHighestPriority, cNoWait) == CJ_EROK)
        return cAtOsalOk;
    return cAtOsalFailed;
    }

static eAtOsalRet Give(AtOsalSem self)
    {
    if (cjsmsignal(mThis(self)->semId) == CJ_EROK)
        return cAtOsalOk;
    return cAtOsalFailed;
    }

static char *SemName(void)
    {
    static char name[16];
    AtSprintf(name, "ASEM");
    return name;
    }

static void OverrideAtOsalSem(AtOsalSem self)
    {
    AtOsalSem sem = (AtOsalSem)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalSemOverride, self->methods, sizeof(m_AtOsalSemOverride));

        m_AtOsalSemOverride.Delete  = Delete;
        m_AtOsalSemOverride.Take    = Take;
        m_AtOsalSemOverride.TryTake = TryTake;
        m_AtOsalSemOverride.Give    = Give;
        }

    sem->methods = &m_AtOsalSemOverride;
    }

static void Override(AtOsalSem self)
    {
    OverrideAtOsalSem(self);
    }

static AtOsalSem ObjectInit(AtOsalSem self, AtOsal osal, uint32 initValue)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalSemObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create real AMX semaphore */
    if (cjsmcreate(&(mThis(self)->semId), SemName(), (int)initValue) != CJ_EROK)
        return NULL;

    return self;
    }

AtOsalSem AtOsalAmxSemNew(AtOsal osal, uint32 initValue)
    {
    AtOsalSem newSem = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newSem, osal, initValue);
    }

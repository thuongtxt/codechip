/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalAmx.c
 *
 * Created Date: Aug 1, 2012
 *
 * Description : AMX OSAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <string.h>
#include "AtOsalAmx.h"
#include "cjzzz.h"

/*--------------------------- Define -----------------------------------------*/
#define cMallocBufferSizeInMBytes 2
#define cMallocBufferSizeInBytes  (cMallocBufferSizeInMBytes * 1024 * 1024)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtOsalMethods m_methods;

static tAtOsal m_osal;
static AtOsal  m_sharedOsal = NULL;

/* Memory pool */
static eBool m_memoryPoolCreated = cAtFalse;
static CJ_ID m_memoryPoolId      = 0;
static long  m_memoryPoolBuffer[cMallocBufferSizeInBytes / sizeof(long)];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static CJ_ID MemoryPoolCreate(void)
    {
    static char name[16];
    CJ_ID poolId;

    AtSprintf(name, "ATMP");
    if (cjmmcreate(&poolId, name, m_memoryPoolBuffer, cMallocBufferSizeInBytes) != CJ_EROK)
        return CJ_IDNULL;
    return poolId;
    }

static CJ_ID MemoryPoolId(void)
    {
    if (m_memoryPoolCreated)
        return m_memoryPoolId;

    /* Create one */
    m_memoryPoolId      = MemoryPoolCreate();
    m_memoryPoolCreated = cAtTrue;

    return m_memoryPoolId;
    }

static eBool MemoryPoolCreatedFail(void)
    {
    return ((MemoryPoolId() == 0) ? cAtTrue : cAtFalse);
    }

static void* MemAlloc(AtOsal self, uint32 size)
    {
    unsigned long usize;
    void *mem;
	AtUnused(self);

    if (size == 0)
        return NULL;

    if (MemoryPoolCreatedFail())
        return NULL;

    if (cjmmget(MemoryPoolId(), size, &mem, &usize) != CJ_EROK)
        return NULL;

    return mem;
    }

static void MemFree(AtOsal self, void* pMem)
    {
	AtUnused(self);
    if (pMem == NULL)
        return;
    if (MemoryPoolCreatedFail())
        return;

    cjmmfree(pMem);
    }

static void* MemInit(AtOsal self, void* pMem, uint32 val, uint32 size)
    {
	AtUnused(self);
    return memset(pMem, (int)val, size);
    }

static void* MemCpy(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
	AtUnused(self);
    return memcpy(pDest, pSrc, size);
    }

static int MemCmp(AtOsal self, const void *pMem1, const void *pMem2, uint32 size)
    {
	AtUnused(self);
    return memcmp(pMem1, pMem2, size);
    }

static AtOsalSem SemCreate(AtOsal self, eBool procEn, uint32 initVal)
    {
    AtUnused(initVal);
    AtUnused(procEn);
    return AtOsalAmxSemNew(self, initVal);
    }

static AtOsalMutex MutexCreate(AtOsal self)
    {
    return AtOsalAmxMutexNew(self);
    }

static uint32 Us2Ticks(uint32 microSeconds)
    {
    uint32 ticks;
    uint32 ms;

    /* At least 1ms */
    ms = microSeconds / 1000;
    if (ms == 0)
        ms = 1;

    ticks = (uint32)cjtmconvert(ms);
    if (ticks == 0)
        ticks = 1;

    if ((microSeconds % 1000) >= 500)
        ticks = ticks + 1;

    return ticks;
    }

static void Sleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(self);
    cjtkdelay((CJ_TIME)Us2Ticks(timeToSleep * 1000));
    }

static void USleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(self);
    cjtkdelay((CJ_TIME)Us2Ticks(timeToSleep));
    }

static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
	AtUnused(self);
    currentTime->sec = cjtmtick();
    }

static uint32 DifferenceTimeInMs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    uint32 diffTicks = 0;
	AtUnused(self);

    /* Calculate different ticks, also handle roll over case */
    if (currentTime->sec >= previousTime->sec)
        diffTicks = currentTime->sec - previousTime->sec;
    else
        diffTicks = (cBit31_0 - previousTime->sec) + currentTime->sec + 1;

    if (diffTicks == 0)
        return 0;

    return diffTicks / Us2Ticks(1000);
    }

static uint32 DifferenceTimeInUs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    AtUnused(self);
    AtUnused(currentTime);
    AtUnused(previousTime);
    /* TODO: to be implemented. */
    return 0;
    }

static void Panic(AtOsal self)
    {
	AtUnused(self);
    cjtkkill(cjtkid());
    }

static AtStd Std(AtOsal self)
    {
    AtUnused(self);
    return AtStdDefaultSharedStd();
    }

static eBool IsMultitasking(AtOsal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtTaskManager TaskManagerGet(AtOsal self)
    {
    /* This has not been implemented. But having this is optional as SDK
     * internal implementation will not create any task */
    AtUnused(self);
    return NULL;
    }

static AtConnectionManager ConnectionManager(AtOsal self)
    {
    /* This has not been implemented. But having this is optional as SDK
     * internal implementation will not depend on network connection */
    AtUnused(self);
    return NULL;
    }

/**
 * Get VxWorks implementation of OSAL
 *
 * @return VxWorks OSAL
 */
AtOsal AtOsalAmx(void)
    {
    if (m_sharedOsal)
        return m_sharedOsal;

    /* Initialize implementation */
    if (!m_methodsInit)
        {
        /* Memory operations */
        m_methods.MemAlloc = MemAlloc;
        m_methods.MemFree  = MemFree;
        m_methods.MemInit  = MemInit;
        m_methods.MemCpy   = MemCpy;
        m_methods.MemCmp   = MemCmp;

        /* Semaphore */
        m_methods.SemCreate = SemCreate;

        /* Mutex */
        m_methods.MutexCreate = MutexCreate;

        /* Delay functions */
        m_methods.Sleep              = Sleep;
        m_methods.USleep             = USleep;
        m_methods.CurTimeGet         = CurTimeGet;
        m_methods.DifferenceTimeInMs = DifferenceTimeInMs;
        m_methods.DifferenceTimeInUs = DifferenceTimeInUs;

        m_methods.Panic = Panic;
        m_methods.Std   = Std;
        m_methods.IsMultitasking    = IsMultitasking;
        m_methods.TaskManagerGet    = TaskManagerGet;
        m_methods.ConnectionManager = ConnectionManager;
        }

    m_osal.methods = &m_methods;
    m_methodsInit  = 1;
    m_sharedOsal   = &m_osal;

    return m_sharedOsal;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalAmxMutex.c
 *
 * Created Date: Apr 28, 2014
 *
 * Description : AMX mutex
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsalAmx.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtOsalAmxMutex)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalAmxMutex * AtOsalAmxMutex;

typedef struct tAtOsalAmxMutex
    {
    tAtOsalMutex super;

    /* AMX does not support MUTEX, let's use binary for instead */
    AtOsalSem binarySemaphore;
    }tAtOsalAmxMutex;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtOsalMutexMethods m_AtOsalMutexOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalAmxMutex);
    }

static eAtOsalRet Delete(AtOsalMutex self)
    {
    AtOsalSemDestroy(mThis(self)->binarySemaphore);
    AtOsalMemFree(self);
    return cAtOsalOk;
    }

static eAtOsalRet Lock(AtOsalMutex self)
    {
    return AtOsalSemTake(mThis(self)->binarySemaphore);
    }

static eAtOsalRet TryLock(AtOsalMutex self)
    {
    return AtOsalSemTryTake(mThis(self)->binarySemaphore);
    }

static eAtOsalRet UnLock(AtOsalMutex self)
    {
    return AtOsalSemGive(mThis(self)->binarySemaphore);
    }

static void OverrideAtOsalMutex(AtOsalMutex self)
    {
    AtOsalMutex sem = (AtOsalMutex)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalMutexOverride, &(self->methods), sizeof(m_AtOsalMutexOverride));

        m_AtOsalMutexOverride.Delete  = Delete;
        m_AtOsalMutexOverride.Lock    = Lock;
        m_AtOsalMutexOverride.TryLock = TryLock;
        m_AtOsalMutexOverride.UnLock  = UnLock;
        }

    sem->methods = &m_AtOsalMutexOverride;
    }

static void Override(AtOsalMutex self)
    {
    OverrideAtOsalMutex(self);
    }

static AtOsalMutex ObjectInit(AtOsalMutex self, AtOsal osal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalMutexObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Just create an internal binary semaphore */
    mThis(self)->binarySemaphore = AtOsalAmxSemNew(osal, 1);
    if (mThis(self)->binarySemaphore == NULL)
        return NULL;

    return self;
    }

AtOsalMutex AtOsalAmxMutexNew(AtOsal osal)
    {
    AtOsalMutex newMutex = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newMutex, osal);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalSem.c
 *
 * Created Date: Apr 29, 2014
 *
 * Description : OSAL semaphore
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtOsalSemMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtOsalRet Delete(AtOsalSem self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static eAtOsalRet Take(AtOsalSem self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static eAtOsalRet TryTake(AtOsalSem self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static eAtOsalRet Give(AtOsalSem self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static AtOsal OsalGet(AtOsalSem self)
    {
    return self->osal;
    }

static void MethodsInit(AtOsalSem self)
    {
    AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

    if (!m_methodsInit)
        {
        m_methods.Delete  = Delete;
        m_methods.Take    = Take;
        m_methods.TryTake = TryTake;
        m_methods.Give    = Give;
        m_methods.OsalGet = OsalGet;
        }

    self->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalSem);
    }

AtOsalSem AtOsalSemObjectInit(AtOsalSem self, AtOsal osal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->osal = osal;

    return self;
    }

/**
 * @addtogroup AtOsalSem
 * @{
 */

/**
 * Create a semaphore
 *
 * @param [in] procEn The flag is used to indicate the semaphore is shared between
 *                    processes or not.
                      - true: shared between processes
                      - false: shared between threads/tasks
 * @param [in] initVal The initiated value of semaphore
 *
 * @return AtOsalSem object on success or NULL on failure
 */
AtOsalSem AtOsalSemCreate(atbool procEn, uint32 initVal)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	return osal->methods->SemCreate(osal, procEn, initVal);

    return NULL;
    }

/**
 * Destroy a semaphore
 *
 * @param [in] self Semaphore
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalSemDestroy(AtOsalSem self)
    {
    if (self)
        return self->methods->Delete(self);
    return cAtOsalOk;
    }

/**
 * Try Wait on a semaphore
 *
 * @param [in] self Semaphore
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalSemTryTake(AtOsalSem self)
    {
    if (self)
        return self->methods->TryTake(self);
    return cAtOsalFailed;
    }

/**
 * Wait on a semaphore
 *
 * @param [in] self Semaphore
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalSemTake(AtOsalSem self)
    {
    if (self)
        return self->methods->Take(self);
    return cAtOsalFailed;
    }

/**
 * Give a semaphore
 *
 * @param [in] self Semaphore
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalSemGive(AtOsalSem self)
    {
    if (self)
        return self->methods->Give(self);
    return cAtOsalFailed;
    }

/**
 * @}
 */

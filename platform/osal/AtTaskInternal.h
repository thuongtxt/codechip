/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OS
 * 
 * File        : AtTaskInternal.h
 * 
 * Created Date: Jun 25, 2015
 *
 * Description : Abstract task
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTASKINTERNAL_H_
#define _ATTASKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTask.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTaskMethods
    {
    eAtRet (*Start)(AtTask self);
    eAtRet (*Stop)(AtTask self);
    eAtRet (*Join)(AtTask self);
    void (*TestCancel)(AtTask self);
    eAtRet (*Delete)(AtTask self);
    }tAtTaskMethods;

typedef struct tAtTask
    {
    const tAtTaskMethods *methods;

    /* Private data */
    void *(*taskHandler)(void *userData);
    void *userData;
    const char *taskName;
    }tAtTask;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTask AtTaskObjectInit(AtTask self, const char *taskName, void *userData, void *(*taskHandler)(void *userData));
eAtRet AtTaskDelete(AtTask self);

#ifdef __cplusplus
}
#endif
#endif /* _ATTASKINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalVxWork.c
 *
 * Created Date: Aug 1, 2012
 *
 * Description : VxWorks OSAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "AtOsalVxWork.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtOsalMethods m_methods;

static tAtOsal m_osal;
static AtOsal  m_sharedOsal = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void* MemAlloc(AtOsal self, uint32 size)
    {
	AtUnused(self);
    return malloc(size);
    }

static void MemFree(AtOsal self, void* pMem)
    {
	AtUnused(self);
    free(pMem);
    }

static void* MemInit(AtOsal self, void* pMem, uint32 val, uint32 size)
    {
	AtUnused(self);
    return memset(pMem, (int)val, size);
    }

static void* MemCpy(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
	AtUnused(self);
    return memcpy(pDest, pSrc, size);
    }

static int MemCmp(AtOsal self, const void *pMem1, const void *pMem2, uint32 size)
    {
    AtUnused(self);
    return (uint8)memcmp(pMem1, pMem2, size);
    }

static AtOsalSem SemCreate(AtOsal self, eBool procEn, uint32 initVal)
    {
    AtUnused(initVal);
    AtUnused(procEn);
    return AtOsalVxWorksSemNew(self);
    }

static AtOsalMutex MutexCreate(AtOsal self)
    {
    return AtOsalVxWorksMutexNew(self);
    }

static void _nanosleep(unsigned long secs, unsigned long nsecs)
    {
    struct timespec time_delay;

    time_delay.tv_sec  = (time_t)secs;
    time_delay.tv_nsec = (long)nsecs;
    nanosleep(&time_delay, NULL);
    }

/* Delay functions */
static void Sleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(self);
    _nanosleep(timeToSleep, 0);
    }

static void USleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(self);
    _nanosleep(timeToSleep / 1000000, (timeToSleep % 1000000) * 1000);
    }

static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
    struct timespec tm;
	AtUnused(self);

    clock_gettime(CLOCK_REALTIME, &tm);
    currentTime->sec  = (uint32)(tm.tv_sec);
    currentTime->usec = (uint32)(tm.tv_nsec / 1000);
    }

static uint32 DifferenceTimeInMs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
	AtUnused(self);
    return ((((currentTime->sec - previousTime->sec) * 1000) + (currentTime->usec / 1000)) - (previousTime->usec / 1000));
    }

static uint32 DifferenceTimeInUs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    AtUnused(self);
    return ((currentTime->sec - previousTime->sec) * 1000000) + currentTime->usec - previousTime->usec;
    }

static AtAttributeNoReturn void Panic(AtOsal self)
    {
    AtUnused(self);
    exit(1);
    }

static AtConnectionManager ConnectionManager(AtOsal self)
    {
    AtUnused(self);
    return AtConnectionManagerDefault();
    }

static AtStd Std(AtOsal self)
    {
    AtUnused(self);
    return AtStdDefaultSharedStd();
    }

static eBool IsMultitasking(AtOsal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtTaskManager TaskManagerGet(AtOsal self)
    {
    /* This has not been implemented. But having this is optional as SDK
     * internal implementation will not create any task */
    AtUnused(self);
    return NULL;
    }

/**
 * Get VxWorks implementation of OSAL
 *
 * @return VxWorks OSAL
 */
AtOsal AtOsalVxWorks(void)
    {
    if (m_sharedOsal)
        return m_sharedOsal;

    /* Initialize implementation */
    if (!m_methodsInit)
        {
        /* Memory operations */
        m_methods.MemAlloc = MemAlloc;
        m_methods.MemFree  = MemFree;
        m_methods.MemInit  = MemInit;
        m_methods.MemCpy   = MemCpy;
        m_methods.MemCmp   = MemCmp;

        /* Semaphore */
        m_methods.SemCreate = SemCreate;

        /* Mutex */
        m_methods.MutexCreate = MutexCreate;

        /* Delay functions */
        m_methods.Sleep              = Sleep;
        m_methods.USleep             = USleep;
        m_methods.CurTimeGet         = CurTimeGet;
        m_methods.DifferenceTimeInMs = DifferenceTimeInMs;
        m_methods.DifferenceTimeInUs = DifferenceTimeInUs;

        /* Managers */
        m_methods.ConnectionManager  = ConnectionManager;
        m_methods.IsMultitasking     = IsMultitasking;
        m_methods.TaskManagerGet     = TaskManagerGet;

        /* Util */
        m_methods.Panic = Panic;
        m_methods.Std   = Std;
        }

    m_osal.methods = &m_methods;
    m_methodsInit  = 1;
    m_sharedOsal   = &m_osal;

    return m_sharedOsal;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalVxWorksMutex.c
 *
 * Created Date: Apr 28, 2014
 *
 * Description : VxWork mutex
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <semLib.h>

#include "AtOsalVxWork.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThisSem(self) ((AtOsalVxWorksMutex)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalVxWorksMutex * AtOsalVxWorksMutex;

typedef struct tAtOsalVxWorksMutex
    {
    tAtOsalMutex super;

    /* Private data */
    /*
     * VxWorks OS provides three types of semaphores:
     *  - binary   : the state of semaphore can take the value 1 or 0
     *  - counting : the state of semaphore can take the integer value
     *  - mutex    : the state of semaphore can take the value 1 or 0,
     *               optimized for mutual exclusive access.
     *               This semaphore must be returned (semGive) by the same task
     *               which has taken it (by semTake).
    */
    SEM_ID semM; /* Here we use semaphores type mutex */
    }tAtOsalVxWorksMutex;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtOsalMutexMethods m_AtOsalMutexOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalVxWorksMutex);
    }

static eAtOsalRet Delete(AtOsalMutex self)
    {
    if(self == NULL)
        return cAtOsalFailed;

    /* Destroy semaphore */
    if(semDelete(mThisSem(self)->semM) < 0)
        return cAtOsalFailed;

    AtOsalMemFree(self);

    return cAtOsalOk;
    }

static eAtOsalRet Lock(AtOsalMutex self)
    {
    return (semTake(mThisSem(self)->semM, WAIT_FOREVER) == -1) ? cAtOsalFailed : cAtOsalOk;
    }

static eAtOsalRet TryLock(AtOsalMutex sem)
    {
	AtUnused(sem);
    /* Do not need this interface */
    return cAtOsalFailed;
    }

static eAtOsalRet UnLock(AtOsalMutex self)
    {
    return (semGive(mThisSem(self)->semM) == -1) ? cAtOsalFailed : cAtOsalOk;
    }

static void OverrideAtOsalMutex(AtOsalMutex self)
    {
    AtOsalMutex sem = (AtOsalMutex)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalMutexOverride, self->methods, sizeof(m_AtOsalMutexOverride));

        m_AtOsalMutexOverride.Delete  = Delete;
        m_AtOsalMutexOverride.Lock    = Lock;
        m_AtOsalMutexOverride.TryLock = TryLock;
        m_AtOsalMutexOverride.UnLock  = UnLock;
        }

    sem->methods = &m_AtOsalMutexOverride;
    }

static void Override(AtOsalMutex self)
    {
    OverrideAtOsalMutex(self);
    }

static AtOsalMutex ObjectInit(AtOsalMutex self, AtOsal osal)
    {
    SEM_ID semId;
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalMutexObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create a binary semaphore that
     * - has prioritized access (SEM_Q_PRIORITY),
     * - ensures that the holding thread cannot be deleted while holding
     *   the mutex (SEM_DELETE_SAFE),
     * - protects against priority inversion (SEM_INVERSION_SAFE)
     * These properties are important
     */
    semId = semMCreate(SEM_Q_PRIORITY | SEM_DELETE_SAFE | SEM_INVERSION_SAFE);
    if (semId == NULL)
        return NULL;

    /* Update private data */
    mThisSem(self)->semM = semId;

    return self;
    }

AtOsalMutex AtOsalVxWorksMutexNew(AtOsal osal)
    {
    AtOsalMutex newMutex = AtOsalMemAlloc(ObjectSize());
    if (newMutex == NULL)
		return NULL;

    return ObjectInit(newMutex, osal);
    }

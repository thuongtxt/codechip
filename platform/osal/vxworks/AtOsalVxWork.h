/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtOsalVxWork.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : VxWorks OSAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALVXWORK_H_
#define _ATOSALVXWORK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsalMutex AtOsalVxWorksMutexNew(AtOsal osal);
AtOsalSem AtOsalVxWorksSemNew(AtOsal osal);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALVXWORK_H_ */


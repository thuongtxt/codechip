/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalVxWorksSem.c
 *
 * Created Date: May 6, 2013
 *
 * Description : VxWorks semaphore
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <semLib.h>

#include "AtOsalVxWork.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtOsalVxWorksSem)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalVxWorksSem * AtOsalVxWorksSem;

typedef struct tAtOsalVxWorksSem
    {
    tAtOsalSem super;

    /* Private data */
    SEM_ID semaphore;
    }tAtOsalVxWorksSem;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtOsalSemMethods m_AtOsalSemOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalVxWorksSem);
    }

static eAtOsalRet Delete(AtOsalSem self)
    {
    if(self == NULL)
        return cAtOsalFailed;

    /* Destroy semaphore */
    if(semDelete(mThis(self)->semaphore) < 0)
        return cAtOsalFailed;

    AtOsalMemFree(self);

    return cAtOsalOk;
    }

static eAtOsalRet Take(AtOsalSem self)
    {
    return (semTake(mThis(self)->semaphore, WAIT_FOREVER) == -1) ? cAtOsalFailed : cAtOsalOk;
    }

static eAtOsalRet TryTake(AtOsalSem sem)
    {
	AtUnused(sem);
    /* Do not need this interface */
    return cAtOsalFailed;
    }

static eAtOsalRet Give(AtOsalSem self)
    {
    return (semGive(mThis(self)->semaphore) == -1) ? cAtOsalFailed : cAtOsalOk;
    }

static void OverrideAtOsalSem(AtOsalSem self)
    {
    AtOsalSem sem = (AtOsalSem)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalSemOverride, self->methods, sizeof(m_AtOsalSemOverride));

        m_AtOsalSemOverride.Delete  = Delete;
        m_AtOsalSemOverride.Take    = Take;
        m_AtOsalSemOverride.TryTake = TryTake;
        m_AtOsalSemOverride.Give    = Give;
        }

    sem->methods = &m_AtOsalSemOverride;
    }

static void Override(AtOsalSem self)
    {
    OverrideAtOsalSem(self);
    }

static AtOsalSem ObjectInit(AtOsalSem self, AtOsal osal)
    {
    SEM_ID semId;

    if(self == NULL)
    	return NULL;

    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalSemObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create real VxWorks semaphore */
    semId = semBCreate(SEM_Q_FIFO, SEM_FULL);
    if (semId == NULL)
        return NULL;

    /* Update private data */
    mThis(self)->semaphore = semId;

    return self;
    }

AtOsalSem AtOsalVxWorksSemNew(AtOsal osal)
    {
    AtOsalSem newSem = AtOsalMemAlloc(ObjectSize());
    if (newSem == NULL)
        return NULL;

    return ObjectInit(newSem, osal);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Task Manager
 * 
 * File        : AtTaskManagerInternal.h
 * 
 * Created Date: Feb 6, 2015
 *
 * Description : Task manager header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTASKMANAGERINTERNAL_H_
#define _ATTASKMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtTaskManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTaskManagerMethods
    {
    AtTask (*TaskCreate)(AtTaskManager self, const char *taskName, void *userData, void *(*taskHandler)(void *userData));
    eAtRet (*TaskDelete)(AtTaskManager self, AtTask task);
    }tAtTaskManagerMethods;

typedef struct tAtTaskManager
    {
    const tAtTaskManagerMethods *methods;
    }tAtTaskManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTaskManager AtTaskManagerObjectInit(AtTaskManager self);

#endif /* _ATTASKMANAGERINTERNAL_H_ */


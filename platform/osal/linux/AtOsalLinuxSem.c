/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalLinuxSem.c
 *
 * Created Date: Apr 29, 2014
 *
 * Description : Linux Semaphore
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309
#endif

#include <semaphore.h>
#include <errno.h>

#include "AtOsalLinux.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtOsalLinuxSem *)self)
#define NO_EINTR(stmt, _ret)                    \
	do                                          \
		{                                       \
		_ret = (stmt);                          \
		}while (_ret == -1 && errno == EINTR);

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalLinuxSem
    {
    tAtOsalSem super;

    /* Private data */
    sem_t semaphore;
    }tAtOsalLinuxSem;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_methodsInit = 0;

/* Override */
static tAtOsalSemMethods m_AtOsalSemOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtOsalRet SemDelete(AtOsalSem self)
    {
    eAtOsalRet ret = (sem_destroy(&(mThis(self)->semaphore)) == 0) ? cAtOsalOk : cAtOsalFailed;
    AtOsalMemFree(self);

    return ret;
    }

static eAtOsalRet SemTake(AtOsalSem self)
    {
    int ret;

    NO_EINTR(sem_wait(&(mThis(self)->semaphore)), ret);
    return ret ? cAtOsalFailed : cAtOsalOk;
    }

static eAtOsalRet SemTryTake(AtOsalSem self)
    {
    int ret;

    NO_EINTR(sem_trywait(&(mThis(self)->semaphore)), ret);
    return (ret ?  cAtOsalFailed :  cAtOsalOk);
    }

static eAtOsalRet SemGive(AtOsalSem self)
    {
    return (sem_post(&(mThis(self)->semaphore)) == 0) ? cAtOsalOk : cAtOsalFailed;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalLinuxSem);
    }

static void OverrideAtOsalSem(AtOsalSem self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalSemOverride, self->methods, sizeof(m_AtOsalSemOverride));

        m_AtOsalSemOverride.Delete  = SemDelete;
        m_AtOsalSemOverride.Take    = SemTake;
        m_AtOsalSemOverride.TryTake = SemTryTake;
        m_AtOsalSemOverride.Give    = SemGive;
        }

    self->methods = &m_AtOsalSemOverride;
    }

static void Override(AtOsalSem self)
    {
    OverrideAtOsalSem(self);
    }

static AtOsalSem ObjectInit(AtOsalSem self, AtOsal osal, eBool processEnable, uint32 initVal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalSemObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create semaphore object */
    if (sem_init(&(mThis(self)->semaphore), processEnable, initVal) != 0)
        return NULL;

    return self;
    }

AtOsalSem AtOsalLinuxSemNew(AtOsal osal, eBool processEnable, uint32 initVal)
    {
    AtOsalSem newSem = AtOsalMemAlloc(ObjectSize());
    if (newSem == NULL)
        return NULL;

    return ObjectInit(newSem, osal, processEnable, initVal);
    }

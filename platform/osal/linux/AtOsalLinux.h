/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalLinux.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Linux OSAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOSALLINUX_H_
#define _ATOSALLINUX_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsalMutex AtOsalLinuxMutexNew(AtOsal osal);
AtOsalSem AtOsalLinuxSemNew(AtOsal osal, eBool processEnable, uint32 initVal);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALLINUX_H_ */


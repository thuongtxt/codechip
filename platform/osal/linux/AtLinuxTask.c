/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Task Manager
 *
 * File        : AtLinuxTask.c
 *
 * Created Date: Feb 6, 2015
 *
 * Description : Task Linux implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <signal.h>
#include <pthread.h>

#include "AtOsal.h"
#include "AtObject.h"
#include "../AtTaskInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtLinuxTask *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLinuxTask
    {
    tAtTask super;

    /* Private data */
    pthread_t threadId;
    }tAtLinuxTask;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTaskMethods m_AtTaskOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void SignalMask(void)
    {
    sigset_t sigSet;

    /* Mask signal SIGINT, SIGTERM, SIGSEGV, SIGBUS, and SIGHUP in thread.
     * Only Main process handle these signals */
    sigemptyset(&sigSet);
    sigaddset(&sigSet, SIGTERM);
    sigaddset(&sigSet, SIGSEGV);
    sigaddset(&sigSet, SIGBUS);
    sigaddset(&sigSet, SIGINT);
    sigaddset(&sigSet, SIGQUIT);
    sigaddset(&sigSet, SIGHUP);
    pthread_sigmask(SIG_BLOCK, &sigSet, NULL);
    }

static void TaskCommonSetup(void)
    {
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
    SignalMask();
    }

static void *TaskHandlerWrapper(void *context)
    {
    AtTask task = context;
    TaskCommonSetup();
    return task->taskHandler(AtTaskUserDataGet(context));
    }

static eAtRet Start(AtTask self)
    {
    return (pthread_create(&(mThis(self)->threadId), NULL, TaskHandlerWrapper, self) == 0) ? cAtOk : cAtError;
    }

static eAtRet Stop(AtTask self)
    {
    if (mThis(self)->threadId <= 0)
        return cAtOk;
    return (pthread_cancel(mThis(self)->threadId) == 0) ? cAtOk : cAtError;
    }

static eAtRet Join(AtTask self)
    {
    if (mThis(self)->threadId <= 0)
        return cAtOk;
    return (pthread_join(mThis(self)->threadId, NULL) == 0) ? cAtOk : cAtError;
    }

static void TestCancel(AtTask self)
    {
    AtUnused(self);
    pthread_testcancel();
    }

static void OverrideAtTask(AtTask self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtTaskOverride, self->methods, sizeof(m_AtTaskOverride));

        mMethodOverride(m_AtTaskOverride, Start);
        mMethodOverride(m_AtTaskOverride, Stop);
        mMethodOverride(m_AtTaskOverride, Join);
        mMethodOverride(m_AtTaskOverride, TestCancel);
        }

    self->methods = &m_AtTaskOverride;
    }

static void Override(AtTask self)
    {
    return OverrideAtTask(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLinuxTask);
    }

static AtTask ObjectInit(AtTask self, const char *taskName, void *userData, void *(*taskHandler)(void *userData))
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtTaskObjectInit(self, taskName, userData, taskHandler) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTask AtLinuxTaskNew(const char *taskName, void *userData, void *(*taskHandler)(void *userData))
    {
    AtTask newTask = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newTask, taskName, userData, taskHandler);
    }

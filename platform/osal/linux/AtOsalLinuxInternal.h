/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OSAL
 * 
 * File        : AtOsalInternal.h
 * 
 * Created Date: Jun 13, 2013
 *
 * Description : OSAL class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _ATOSALINTERNAL_H_
#define _ATOSALINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

#include <semaphore.h>
#include <pthread.h>

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtOsalLinux
    {
    tAtOsal super;

    /* Private data */
    }tAtOsalLinux;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtOsal AtOsalLinuxObjectInit(AtOsal self);

AtOsalSem AtOsalLinuxSemNew(AtOsal osal, eBool processEnable, uint32 initVal);
AtOsalMutex AtOsalLinuxMutexNew(AtOsal osal);

#ifdef __cplusplus
}
#endif
#endif /* _ATOSALINTERNAL_H_ */

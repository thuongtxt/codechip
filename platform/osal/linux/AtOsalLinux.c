/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalLinux.c
 *
 * Created Date: Aug 1, 2012
 *
 * Author      : namnn
 *
 * Description : OSAL - Linux implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309
#endif

#include "AtOsalLinuxInternal.h"
#include "AtOsalLinux.h"

#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <time.h>
#include <assert.h>
#include <stdio.h>

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_methodsInit = 0;

/* Override */
static tAtOsalMethods m_AtOsalOverride;

/* This OSAL */
static tAtOsalLinux m_osal;
static AtOsal       m_sharedOsal = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void* MemAlloc(AtOsal self, uint32 size)
    {
	AtUnused(self);
    return malloc(size);
    }

static void MemFree(AtOsal self, void* pMem)
    {
	AtUnused(self);
    free(pMem);
    }

static void* MemInit(AtOsal self, void* pMem, uint32 val, uint32 size)
    {
	AtUnused(self);
    return memset(pMem, (int)val, size);
    }

static void* MemCpy(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
	AtUnused(self);
    return memcpy(pDest, pSrc, size);
    }

static void* MemMove(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
    AtUnused(self);
    return memmove(pDest, pSrc, size);
    }

static int MemCmp(AtOsal self, const void *pMem1, const void *pMem2, uint32 size)
    {
    AtUnused(self);
    return memcmp(pMem1, pMem2, size);
    }

static AtOsalSem SemCreate(AtOsal self, eBool procEn, uint32 initVal)
    {
    return AtOsalLinuxSemNew(self, procEn, initVal);
    }

static AtOsalMutex MutexCreate(AtOsal self)
    {
    return AtOsalLinuxMutexNew(self);
    }

static void _nanosleep(unsigned long secs, unsigned long nsecs)
    {
    struct timespec time2Sleep;
    struct timespec remain;
    int ret;

    memset(&time2Sleep, 0, sizeof(time2Sleep));
    memset(&remain, 0, sizeof(remain));
    remain.tv_sec = (time_t)secs;
    remain.tv_nsec = (long)nsecs;
    do
        {
        time2Sleep.tv_sec = remain.tv_sec;
        time2Sleep.tv_nsec = remain.tv_nsec;
        ret = nanosleep(&time2Sleep, &remain);
        }while(ret == -1 && EINTR == errno);
    }

static void Sleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(self);
    _nanosleep(timeToSleep, 0);
    }

static void USleep(AtOsal self, uint32 timeToSleep)
    {
	AtUnused(self);
    _nanosleep(timeToSleep/1000000, (timeToSleep % 1000000)*1000);
    }

static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
    struct timespec tm;
	AtUnused(self);

    if (currentTime == NULL)
        return;

    clock_gettime(CLOCK_REALTIME, &tm);
    currentTime->sec  = (uint32)tm.tv_sec;
    currentTime->usec = (uint32)(tm.tv_nsec / 1000);
    }

static uint32 DifferenceTimeInMs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    uint32 diffInUs = ((currentTime->sec - previousTime->sec) * 1000000) + currentTime->usec - previousTime->usec;
    AtUnused(self);
    return diffInUs / 1000;
    }

static uint32 DifferenceTimeInUs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    AtUnused(self);
    return ((currentTime->sec - previousTime->sec) * 1000000) + currentTime->usec - previousTime->usec;
    }

static AtAttributeNoReturn void Panic(AtOsal self)
    {
    AtUnused(self);
    assert(0);
    }

static AtTaskManager TaskManagerGet(AtOsal self)
    {
    AtUnused(self);
    return AtLinuxTaskManager();
    }

static const char* DateTimeGet(AtOsal self)
    {
    time_t dateTime = time(NULL);
    AtUnused(self);
    return ctime(&dateTime);
    }

static const char* DateTimeInUsGet(AtOsal self)
    {
    time_t dateTime = time(NULL);
    struct tm* tm_info;
    struct timeval tm;
    static char strTime[35];
    AtUnused(self);

    tm_info = localtime(&dateTime);
    gettimeofday(&tm, NULL);
    strftime(strTime, 26, "%Y-%m-%d %H:%M:%S", tm_info);
    AtSnprintf(&strTime[strlen(strTime)], 8, ".%06ld", (long)tm.tv_usec);
    return strTime;
    }

static AtConnectionManager ConnectionManager(AtOsal self)
    {
    AtUnused(self);
    return AtConnectionManagerDefault();
    }

static AtStd Std(AtOsal self)
    {
    AtUnused(self);
    return AtStdDefaultSharedStd();
    }

static eBool IsMultitasking(AtOsal self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtOsal(AtOsal self)
    {
    if (!m_methodsInit)
        {
        /* Memory operations */
        m_AtOsalOverride.MemAlloc = MemAlloc;
        m_AtOsalOverride.MemFree  = MemFree;
        m_AtOsalOverride.MemInit  = MemInit;
        m_AtOsalOverride.MemCpy   = MemCpy;
        m_AtOsalOverride.MemMove  = MemMove;
        m_AtOsalOverride.MemCmp   = MemCmp;

        /* Semaphore */
        m_AtOsalOverride.SemCreate = SemCreate;

        /* Mutex */
        m_AtOsalOverride.MutexCreate = MutexCreate;

        /* Delay functions */
        m_AtOsalOverride.Sleep              = Sleep;
        m_AtOsalOverride.USleep             = USleep;
        m_AtOsalOverride.CurTimeGet         = CurTimeGet;
        m_AtOsalOverride.DifferenceTimeInMs = DifferenceTimeInMs;
        m_AtOsalOverride.DifferenceTimeInUs = DifferenceTimeInUs;

        /* Utils */
        m_AtOsalOverride.Panic = Panic;

        /* Managers */
        m_AtOsalOverride.IsMultitasking = IsMultitasking;
        m_AtOsalOverride.TaskManagerGet = TaskManagerGet;
        m_AtOsalOverride.ConnectionManager = ConnectionManager;

        /* Time */
        m_AtOsalOverride.DateTimeGet = DateTimeGet;
        m_AtOsalOverride.DateTimeInUsGet = DateTimeInUsGet;

        m_AtOsalOverride.Std = Std;
        }

    self->methods = &m_AtOsalOverride;
    }

AtOsal AtOsalLinuxObjectInit(AtOsal self)
    {
    OverrideAtOsal(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * Get Linux implementation of OSAL
 *
 * @return Linux OSAL
 */
AtOsal AtOsalLinux(void)
    {
    if (m_sharedOsal)
        return m_sharedOsal;

    m_sharedOsal = AtOsalLinuxObjectInit((AtOsal)&m_osal);
    return m_sharedOsal;
    }

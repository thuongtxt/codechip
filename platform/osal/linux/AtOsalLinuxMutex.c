/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalLinuxMutex.c
 *
 * Created Date: Apr 29, 2014
 *
 * Description : Linux Mutex
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309
#endif

#include <pthread.h>
#include <errno.h>

#include "AtOsalLinux.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtOsalLinuxMutex *)self)

#define NO_EINTR(stmt, _ret)                  \
	do                                        \
		{                                     \
		_ret = (stmt);                        \
		}while (_ret == -1 && errno == EINTR)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtOsalLinuxMutex
    {
    tAtOsalMutex super;

    /* Private data */
    pthread_mutex_t mutex;
    eBool mutexHasInit;
    }tAtOsalLinuxMutex;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtOsalMutexMethods m_AtOsalMutexOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtOsalRet Delete(AtOsalMutex self)
    {
    tAtOsalLinuxMutex *mutex = (tAtOsalLinuxMutex *)self;
    eAtOsalRet ret = (pthread_mutex_destroy(&(mutex->mutex)) == 0) ? cAtOsalOk : cAtOsalFailed;
    AtOsalMemFree(self);

    return ret;
    }

static eAtOsalRet Lock(AtOsalMutex mutex)
    {
    int ret;

    NO_EINTR(pthread_mutex_lock(&(((tAtOsalLinuxMutex *)mutex)->mutex)), ret);
    return ret ? cAtOsalFailed : cAtOsalOk;
    }

static eAtOsalRet TryLock(AtOsalMutex mutex)
    {
    int ret;

    NO_EINTR(pthread_mutex_trylock(&(((tAtOsalLinuxMutex *)mutex)->mutex)), ret);
    return (ret ?  cAtOsalFailed :  cAtOsalOk);
    }

static eAtOsalRet UnLock(AtOsalMutex mutex)
    {
    return (pthread_mutex_unlock(&(((tAtOsalLinuxMutex *)mutex)->mutex)) == 0) ? cAtOsalOk : cAtOsalFailed;
    }

static void OverrideAtOsalMutex(AtOsalMutex self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtOsalMutexOverride, self->methods, sizeof(m_AtOsalMutexOverride));

        m_AtOsalMutexOverride.Delete  = Delete;
        m_AtOsalMutexOverride.Lock    = Lock;
        m_AtOsalMutexOverride.TryLock = TryLock;
        m_AtOsalMutexOverride.UnLock  = UnLock;
        }

    self->methods = &m_AtOsalMutexOverride;
    }

static void Override(AtOsalMutex self)
    {
    OverrideAtOsalMutex(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalLinuxMutex);
    }

static AtOsalMutex ObjectInit(AtOsalMutex self, AtOsal osal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtOsalMutexObjectInit(self, osal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create internal Linux mutex */
    if (mThis(self)->mutexHasInit)
        pthread_mutex_destroy(&(mThis(self)->mutex));

    if (pthread_mutex_init(&(mThis(self)->mutex), NULL) != 0)
        {
        mThis(self)->mutexHasInit = cAtFalse;
        return NULL;
        }

    mThis(self)->mutexHasInit = cAtTrue;
    return self;
    }

AtOsalMutex AtOsalLinuxMutexNew(AtOsal osal)
    {
    AtOsalMutex newMutex = AtOsalMemAlloc(ObjectSize());
    if (newMutex == NULL)
        return NULL;

    return ObjectInit(newMutex, osal);
    }

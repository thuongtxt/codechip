/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Task Manager
 *
 * File        : AtLinuxTaskManager.c
 *
 * Created Date: Feb 6, 2015
 *
 * Description : Task Manager Linux implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtObject.h"
#include "../AtTaskManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLinuxTaskManager
    {
    tAtTaskManager super;
    }tAtLinuxTaskManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTaskManagerMethods m_AtTaskManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtTask TaskCreate(AtTaskManager self, const char *taskName, void *userData, void *(*taskHandler)(void *userData))
    {
    AtUnused(self);
    return AtLinuxTaskNew(taskName, userData, taskHandler);
    }

static void OverrideAtTaskManager(AtTaskManager self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtTaskManagerOverride, mMethodsGet(self), sizeof(m_AtTaskManagerOverride));

        mMethodOverride(m_AtTaskManagerOverride, TaskCreate);
        }

    mMethodsSet(self, &m_AtTaskManagerOverride);
    }

static void Override(AtTaskManager self)
    {
    return OverrideAtTaskManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLinuxTaskManager);
    }

static AtTaskManager ObjectInit(AtTaskManager self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtTaskManagerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTaskManager AtLinuxTaskManager(void)
    {
    static tAtLinuxTaskManager m_taskManager;
    static AtTaskManager  m_sharedTaskManager = NULL;
    
    if (m_sharedTaskManager)
        return m_sharedTaskManager;

    m_sharedTaskManager = ObjectInit((AtTaskManager)(&m_taskManager));
    return m_sharedTaskManager;
    }

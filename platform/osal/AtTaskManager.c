/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Task Manager
 *
 * File        : AtTaskManager.c
 *
 * Created Date: Feb 6, 2015
 *
 * Description : Task Manager class abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtObject.h"
#include "AtTaskManagerInternal.h"
#include "AtTaskInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtTaskManagerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtTask TaskCreate(AtTaskManager self, const char *taskName, void *userData, void *(*taskHandler)(void *userData))
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(taskName);
    AtUnused(userData);
    AtUnused(taskHandler);
    return NULL;
    }

static eAtRet TaskDelete(AtTaskManager self, AtTask task)
    {
    AtUnused(self);
    return AtTaskDelete(task);
    }

static void MethodsInit(AtTaskManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TaskCreate);
        mMethodOverride(m_methods, TaskDelete);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTaskManager);
    }

AtTaskManager AtTaskManagerObjectInit(AtTaskManager self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtTaskManager
 * @{
 */

/**
 * Create a task
 *
 * @param self Task manager
 * @param taskName Task name
 * @param userData User data
 * @param taskHandler Task handler
 *
 * @return Task object
 */
AtTask AtTaskManagerTaskCreate(AtTaskManager self, const char *taskName, void *userData, void *(*taskHandler)(void *userData))
    {
    if (self == NULL)
        return NULL;
    return mMethodsGet(self)->TaskCreate(self, taskName, userData, taskHandler);
    }

/**
 * Delete a task
 *
 * @param self Task manager
 * @param task Task object
 *
 * @return Task object
 */
eAtRet AtTaskManagerTaskDelete(AtTaskManager self, AtTask task)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    return mMethodsGet(self)->TaskDelete(self, task);
    }

/**
 * @}
 */

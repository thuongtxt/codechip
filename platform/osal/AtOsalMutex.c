/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalMutex.c
 *
 * Created Date: Apr 29, 2014
 *
 * Description : OSAL Mutex
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtOsalMutexMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtOsalRet Delete(AtOsalMutex self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static eAtOsalRet Lock(AtOsalMutex self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static eAtOsalRet TryLock(AtOsalMutex self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static eAtOsalRet UnLock(AtOsalMutex self)
    {
	AtUnused(self);
    return cAtOsalFailed;
    }

static AtOsal OsalGet(AtOsalMutex self)
    {
    return self->osal;
    }

static void MethodsInit(AtOsalMutex self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.Delete  = Delete;
        m_methods.Lock    = Lock;
        m_methods.TryLock = TryLock;
        m_methods.UnLock  = UnLock;
        m_methods.OsalGet = OsalGet;
        }

    self->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtOsalMutex);
    }

AtOsalMutex AtOsalMutexObjectInit(AtOsalMutex self, AtOsal osal)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->osal = osal;

    return self;
    }

/**
 * @addtogroup AtOsalMutex
 * @{
 */

/**
 * Create a mutex
 *
 * @return AtOsalMutex object or NULL on failure
 */
AtOsalMutex AtOsalMutexCreate(void)
    {
    AtOsal osal = AtOsalSharedGet();
    return osal->methods->MutexCreate(osal);
    }

/**
 * Destroy a mutex
 *
 * @param [in] self Mutex
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalMutexDestroy(AtOsalMutex self)
    {
    if (self)
        return self->methods->Delete(self);
    return cAtOsalOk;
    }

/**
 * Try locking a mutex
 *
 * @param [in] self Mutex
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalMutexTryLock(AtOsalMutex self)
    {
    if (self)
        return self->methods->TryLock(self);
    return cAtOsalFailed;
    }

/**
 * Lock a mutex
 *
 * @param [in] self Mutex
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalMutexLock(AtOsalMutex self)
    {
    if (self)
        return self->methods->Lock(self);
    return cAtOsalFailed;
    }

/**
 * Unlock a mutex
 *
 * @param [in] self Mutex
 *
 * @retval cAtOsalOk On success
 * @retval cAtOsalFail On failure
 */
eAtOsalRet AtOsalMutexUnLock(AtOsalMutex self)
    {
    if (self)
        return self->methods->UnLock(self);
    return cAtOsalFailed;
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Plaform
 *
 * File        : AtOsal.c
 *
 * Created Date: Aug 30, 2012
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtOsal m_sharedOsal = NULL;

/* Trace level descriptions */
static const char * const m_traceLevelStrings[cAtOsalTraceMax] =
        {
        "<0> Emergency ",
        "<1> Alert ",
        "<2> Critical ",
        "<3> Error ",
        "<4> Warning ",
        "<5> Notice ",
        "<6> Info ",
        "<7> Debug ",
        };

/* Redirect function */
static void (*m_redirectFunction)(const char *level, const char *fmt, va_list args);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void AtOsalPanic(void)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal == NULL)
        return;
    if (osal->methods->Panic)
        osal->methods->Panic(osal);
    }

/**
 * @addtogroup AtOsal
 * @{
 */
/**
 * Get singleton OSAL object
 *
 * @return Singleton OSAL
 */
AtOsal AtOsalSharedGet(void)
    {
    /* Return a customized OSAL if it is set */
    if (m_sharedOsal)
        return m_sharedOsal;

    /* Or return built-in OSAL */
#ifdef LINUX
    m_sharedOsal = AtOsalLinux();
#endif

#ifdef VXWORKS
    m_sharedOsal = AtOsalVxWorks();
#endif

    return m_sharedOsal;
    }

/**
 * Change OSAL singleton object to a customized OSAL
 *
 * @param osal A customized OSAL
 *
 * @return OSAL return code
 */
eAtOsalRet AtOsalSharedSet(AtOsal osal)
    {
    m_sharedOsal = osal;
    return cAtOsalOk;
    }

/**
 * Redirect output
 * @param redirect_func
 */
void AtOsalTraceRedirectSet(void (*redirect_func)(const char *level, const char *fmt, va_list args))
    {
    m_redirectFunction = redirect_func;
    }

/**
 * @brief Osal Trace
 *
 * @param [in] traceLevel Trace Level
 * @param [in] fmt Format String
 * @param [in] ...
 */
void AtOsalTrace(eAtOsalTraceLevel traceLevel, const char *fmt, ...)
    {
    va_list args;

    va_start(args, fmt);
    if (!m_redirectFunction)
        {
        if (traceLevel < cAtOsalTraceMax)
            AtPrintf("%s", m_traceLevelStrings[traceLevel]);
        AtStdPrintf(AtStdSharedStdGet(), fmt, args);
        }

    else
        {
        if (traceLevel < cAtOsalTraceMax)
            m_redirectFunction(m_traceLevelStrings[traceLevel], fmt, args);
        else
            m_redirectFunction("<8> ", fmt, args);
        }

    va_end(args);
    }

/**
 * @}
 */

/**
 * @addtogroup AtOsalMem
 * @{
 */

/**
 * Allocate a memory area
 * @param [in] size Size of memory
 *
 * @return Pointer to allocated memory
 */
void* AtOsalMemAlloc(uint32 size)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	return osal->methods->MemAlloc(osal, size);

    return NULL;
    }

/**
 * Wrap realloc C function
 *
 * @param oldPtr Old memory pointer
 * @param oldSize Old memory size
 * @param newSize New memory size
 *
 * @return Pointer to new memory
 */
void * AtOsalMemRealloc(void *oldPtr, uint32 oldSize, uint32 newSize)
    {
    void *newMemory;
    uint32 sizeToCopy;

    /* Allocate and initialize new memory */
    newMemory = AtOsalMemAlloc(newSize);
    if (newMemory == NULL)
        return NULL;
    AtOsalMemInit(newMemory, 0, newSize);

    /* Copy old memory */
    sizeToCopy = (oldSize < newSize) ? oldSize : newSize;
    AtOsalMemCpy(newMemory, oldPtr, sizeToCopy);

    /* Free the old memory */
    AtOsalMemFree(oldPtr);

    return newMemory;
    }

/**
 * Free a memory area
 *
 * @param [in] pMem Memory to be free
 */
void AtOsalMemFree(void* pMem)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	osal->methods->MemFree(osal, pMem);
    }

/**
 * Initialize a memory area
 *
 * @param [out] pMem The pointer to the memory area
 * @param [in] val The initial value
 * @param [in] size The size of the memory area needed to be initiated, in bytes
 */
void* AtOsalMemInit(void* pMem, uint32 val, uint32 size)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	return osal->methods->MemInit(osal, pMem, val, size);

    return NULL;
    }

/**
 * Copy a memory area from source to destination area
 *
 * @param [out] pDest The pointer to the destination memory area
 * @param [in] pSrc The pointer to the source memory area
 * @param [in] size The size of memory area needed to be copied, in bytes
 *
 * @return Pointer to pDest
 */
void* AtOsalMemCpy(void *pDest, const void *pSrc, uint32 size)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	return osal->methods->MemCpy(osal, pDest, pSrc, size);

    return NULL;
    }

/**
 * Move memory, works just like memmove()
 *
 * @param [out] pDest The pointer to the destination memory area
 * @param [in] pSrc The pointer to the source memory area
 * @param [in] size The size of memory area needed to be moved, in bytes
 *
 * @return Pointer to pDest
 */
void* AtOsalMemMove(void *pDest, const void *pSrc, uint32 size)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
        return osal->methods->MemMove(osal, pDest, pSrc, size);

    return NULL;
    }

/**
 * Compare memory areas
 *
 * @param [in] pMem1 The pointer to the memory area 1
 * @param [in] pMem2 The pointer to the memory area 2
 * @param [in] size The size of memory area needed to be compared, in bytes
 *
 * @retval 0 If 2 memory areas are same
 * @retval 1 If 2 memory areas are different
 */
int AtOsalMemCmp(const void *pMem1, const void *pMem2, uint32 size)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	return osal->methods->MemCmp(osal, pMem1, pMem2, size);

    return 1;
    }

/**
 * @}
 */

/**
 * @addtogroup AtOsalTime
 * @{
 */

/**
 * Delay in seconds
 *
 * @param [in] timeToSleep Time to sleep in seconds
 */
void AtOsalSleep(uint32 timeToSleep)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	osal->methods->Sleep(osal, timeToSleep);
    }

/**
 * Delay in microseconds
 *
 * @param [in] timeToSleep Time to sleep in microseconds
 */
void AtOsalUSleep(uint32 timeToSleep)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	osal->methods->USleep(osal, timeToSleep);
    }

/**
 * Get system current time
 *
 * @param [out] currentTime Current time
 */
void AtOsalCurTimeGet(tAtOsalCurTime *currentTime)
    {
    AtOsal osal = AtOsalSharedGet();

    if (osal == NULL)
        return;

    if (currentTime == NULL)
        return;

    osal->methods->MemInit(osal, currentTime, 0, sizeof(tAtOsalCurTime));
    osal->methods->CurTimeGet(osal, currentTime);
    }

/**
 * Calculate difference time in milliseconds
 *
 * @param currentTime Current time
 * @param previousTime Previous time that less than "currentTime"
 *
 * @return Difference time in milliseconds
 */
uint32 AtOsalDifferenceTimeInMs(const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
    	return osal->methods->DifferenceTimeInMs(osal, currentTime, previousTime);

    return cBit31_0;
    }

/**
 * Calculate difference time in microseconds
 *
 * @param currentTime Current time
 * @param previousTime Previous time that less than "currentTime"
 *
 * @return Difference time in milliseconds
 */
uint32 AtOsalDifferenceTimeInUs(const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal)
        return osal->methods->DifferenceTimeInUs(osal, currentTime, previousTime);

    return cBit31_0;
    }

/**
 * Get task manager
 *
 * @return Task Manager
 */
AtTaskManager AtOsalTaskManagerGet(void)
    {
    AtOsal osal = AtOsalSharedGet();
    return osal->methods->TaskManagerGet(osal);
    }

/**
 * Get connection manager
 *
 * @return Connection manager
 */
AtConnectionManager AtOsalConnectionManager(void)
    {
    AtOsal osal = AtOsalSharedGet();
    return osal->methods->ConnectionManager(osal);
    }

/**
 * Get date time string
 * @return Date time string
 */
const char* AtOsalDateTimeGet(void)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal->methods->DateTimeGet)
        return osal->methods->DateTimeGet(osal);

    return "";
    }

/**
 * Get custom date time string in "yyyy-mm-dd hh:mm:ss.ssssss"
 * This method is not thread-safe.
 *
 * @return Date time string
 */
const char* AtOsalDateTimeInUsGet(void)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal->methods->DateTimeInUsGet)
        return osal->methods->DateTimeInUsGet(osal);

    return "";
    }

/**
 * Get C standard library abstraction
 *
 * @return C standard library abstraction
 */
AtStd AtOsalStd(void)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal->methods->Std)
        return osal->methods->Std(osal);
    return NULL;
    }

/**
 * To check if OSAL is in multitasking environment
 *
 * @retval cAtTrue if multitasking
 * @retval cAtFalse if no multitasking
 */
eBool AtOsalIsMultitasking(void)
    {
    AtOsal osal = AtOsalSharedGet();
    if (osal->methods->IsMultitasking)
        return osal->methods->IsMultitasking(osal);
    return cAtFalse;
    }

/**
 * @}
 */

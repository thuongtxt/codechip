/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IP
 *
 * File        : AtDefaultConnectionClient.c
 *
 * Created Date: Feb 12, 2015
 *
 * Description : Connection that is initiated by client
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDefaultConnectionInternal.h"
#include "os/AtDefaultConnectionOs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtDefaultConnectionClient *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDefaultConnectionClient
    {
    tAtDefaultConnection super;

    /* Private data */
    char *serverIpAddress;
    struct sockaddr_in serverAddress;
    int socket;
    uint32 timeoutInSeconds;
    }tAtDefaultConnectionClient;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtConnectionMethods        m_AtConnectionOverride;
static tAtDefaultConnectionMethods m_AtDefaultConnectionOverride;

/* Save super implementation */
static const tAtConnectionMethods *m_AtConnectionMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static struct sockaddr_in *RemoteSocketAddress(AtDefaultConnection self)
    {
    return &(mThis(self)->serverAddress);
    }

static eAtConnectionRet LookupServerAddress(AtConnection self)
    {
    struct hostent *server = gethostbyname(mThis(self)->serverIpAddress);

    if (server == NULL)
        {
        AtPrintc(cSevCritical, "(%s, %d) ERROR: No such host\r\n", __FILE__, __LINE__);
        return cAtConnectionErrorInvalidAddress;
        }

    AtOsalMemInit(&(mThis(self)->serverAddress), 0, sizeof(mThis(self)->serverAddress));
    mThis(self)->serverAddress.sin_family = AF_INET;
    AtOsalMemCpy(&(mThis(self)->serverAddress).sin_addr.s_addr, (char *)server->h_addr_list[0], (uint32)server->h_length);
    mThis(self)->serverAddress.sin_port = htons(AtConnectionConnectedPortGet(self));

    return cAtConnectionOk;
    }

static int Connect(AtConnection self)
    {
    if (AtConnectionTransportGet(self) == cAtConnectionTransportTcp)
        return connect(mThis(self)->socket,
                       (struct sockaddr *)&(mThis(self)->serverAddress),
                       sizeof(mThis(self)->serverAddress));
    return 0;
    }

static void CloseAllSockets(AtConnection self)
    {
    if (mThis(self)->socket > 0)
        close(mThis(self)->socket);
    mThis(self)->socket = 0;
    }

static eAtConnectionRet Setup(AtConnection self)
    {
    int newSocket;
    eAtConnectionRet ret;

    /* Already setup */
    if (mThis(self)->socket)
        return cAtConnectionOk;

    /* Server that needs to connect to */
    ret = LookupServerAddress(self);
    if (ret != cAtConnectionOk)
        return ret;

    /* Create new socket */
    newSocket = AtDefaultConnectionSocketCreate(self, AtConnectionReceiveTimeoutGet(self));
    if (newSocket < 0)
        {
        AtPrintc(cSevCritical, "ERROR: Create socket fail: %s\r\n", AtConnectionErrorString(self));
        return cAtConnectionErrorCreateFail;
        }
    mThis(self)->socket = newSocket;

    /* Connect */
    if (Connect(self) < 0)
        {
        AtPrintc(cSevCritical, "ERROR: Fail to connect to server: %s\r\n", AtConnectionErrorString(self));
        CloseAllSockets(self);
        return cAtConnectionError;
        }

    /* Connected */
    return cAtConnectionOk;
    }

static int Socket(AtConnection self)
    {
    return mThis(self)->socket;
    }

static const char *RemoteAddressGet(AtConnection self)
    {
    return mThis(self)->serverIpAddress;
    }

static int Send(AtConnection self, const char *dataBuffer, uint32 numBytes)
    {
    int ret = -1;

    if (Setup(self) == cAtConnectionOk)
        ret = m_AtConnectionMethods->Send(self, dataBuffer, numBytes);

    if (ret == (int)numBytes)
        return (int)numBytes;

    /* Fail, reset connection */
    CloseAllSockets(self);

    AtPrintc(cSevCritical, "(%s, %d) ERROR: Send fail\r\n", __FILE__, __LINE__);

    return -1;
    }

static eAtConnectionRet Delete(AtConnection self)
    {
    CloseAllSockets(self);
    AtOsalMemFree(mThis(self)->serverIpAddress);
    return m_AtConnectionMethods->Delete(self);
    }

static void OverrideAtDefaultConnection(AtConnection self)
    {
    AtDefaultConnection connection = (AtDefaultConnection)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtDefaultConnectionOverride, mMethodsGet(connection), sizeof(m_AtDefaultConnectionOverride));

        mMethodOverride(m_AtDefaultConnectionOverride, RemoteSocketAddress);
        }

    mMethodsSet(connection, &m_AtDefaultConnectionOverride);
    }

static void OverrideAtConnection(AtConnection self)
    {
    if (!m_methodsInit)
        {
        m_AtConnectionMethods = self->methods;
        AtOsalMemCpy(&m_AtConnectionOverride, self->methods, sizeof(m_AtConnectionOverride));

        mMethodOverride(m_AtConnectionOverride, Socket);
        mMethodOverride(m_AtConnectionOverride, Send);
        mMethodOverride(m_AtConnectionOverride, Delete);
        mMethodOverride(m_AtConnectionOverride, RemoteAddressGet);
        }

    mMethodsSet(self, &m_AtConnectionOverride);
    }

static void Override(AtConnection self)
    {
    OverrideAtConnection(self);
    OverrideAtDefaultConnection(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultConnectionClient);
    }

static AtConnection ObjectInit(AtConnection self, const char *ipAddress, uint16 port, AtConnectionManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDefaultConnectionObjectInit(self, port, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->serverIpAddress  = AtStrdup(ipAddress);

    return self;
    }

AtConnection AtDefaultConnectionClientNew(const char *ipAddress, uint16 port, AtConnectionManager manager)
    {
    AtConnection newConnection = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newConnection, ipAddress, port, manager);
    }

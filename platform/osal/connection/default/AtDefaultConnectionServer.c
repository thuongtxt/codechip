/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IP
 *
 * File        : AtConnectionServer.c
 *
 * Created Date: Feb 12, 2015
 *
 * Description : Connection that is initiated from server side
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "AtDefaultConnectionInternal.h"
#include "os/AtDefaultConnectionOs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtDefaultConnectionServer *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDefaultConnectionServer
    {
    tAtDefaultConnection super;

    /* Private data */
    int socket;
    struct sockaddr_in localAddress;

    /* Client info */
    char *bindAddress;
    uint16 bindPort;
    int clientSocket;
    struct sockaddr_in clientAddress;
    }tAtDefaultConnectionServer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtConnectionMethods        m_AtConnectionOverride;
static tAtDefaultConnectionMethods m_AtDefaultConnectionOverride;

/* Save super implementation */
static const tAtConnectionMethods *m_AtConnectionMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AddressInit(AtConnection self, uint16 port)
    {
    in_addr_t s_addr = INADDR_ANY;

    AtOsalMemInit(&(mThis(self)->localAddress), 0, sizeof(mThis(self)->localAddress));

    if (mThis(self)->bindAddress)
        s_addr = inet_addr(mThis(self)->bindAddress);

    mThis(self)->localAddress.sin_family      = AF_INET;
    mThis(self)->localAddress.sin_addr.s_addr = s_addr;
    mThis(self)->localAddress.sin_port        = htons(port);
    }

static eAtConnectionRet Listen(AtConnection self)
    {
    if (AtConnectionTransportGet(self) != cAtConnectionTransportTcp)
        return cAtConnectionOk;

    if (listen(mThis(self)->socket, 1) < 0)
        {
        AtPrintc(cSevCritical, "ERROR: Listen fail: %s\r\n", AtConnectionErrorString(self));
        return cAtConnectionErrorListenFail;
        }

    return cAtConnectionOk;
    }

static eAtConnectionRet SocketBind(AtConnection self)
    {
    int newSocket;
    static const uint32 cBlockOnReceiving = 0;

    /* Already created and bind */
    if (mThis(self)->socket)
        return cAtConnectionOk;

    /* Create socket */
    newSocket = AtDefaultConnectionSocketCreate(self, cBlockOnReceiving);
    if (newSocket < 0)
        {
        AtSnprintf(self->error, sizeof(self->error), "Create socket fail (%s)\r\n", AtConnectionErrorString(self));
        return cAtConnectionErrorCreateFail;
        }

    /* Listen on address */
    if (bind(newSocket, (struct sockaddr *)&(mThis(self)->localAddress), sizeof(mThis(self)->localAddress)) < 0)
        {
        AtSnprintf(self->error, sizeof(self->error), "Socket binding fail (%s)\r\n", AtConnectionErrorString(self));
        close(newSocket);
        return cAtConnectionErrorBindFail;
        }

    mThis(self)->socket = newSocket;
    return Listen(self);
    }

static eAtConnectionRet AcceptConnection(AtConnection self)
    {
    int clientSocket;
    AtSocketSize addressSize = sizeof(struct sockaddr_in);

    if (AtConnectionTransportGet(self) != cAtConnectionTransportTcp)
        return cAtConnectionOk;

    if (mThis(self)->clientSocket)
        return cAtConnectionOk;

    clientSocket = accept(mThis(self)->socket, (struct sockaddr *)&(mThis(self)->clientAddress), &addressSize);
    if (clientSocket < 0)
        {
        AtPrintc(cSevCritical, "Cannot accept client connections: %s\r\n", AtConnectionErrorString(self));
        return cAtConnectionErrorAcceptFail;
        }

    mMethodsGet(self)->RemoteSideConnected(self, &(mThis(self)->clientAddress));

    mThis(self)->clientSocket = clientSocket;
    return cAtConnectionOk;
    }

static eAtConnectionRet Setup(AtConnection self)
    {
    eAtConnectionRet ret = SocketBind(self);
    if (ret != cAtConnectionOk)
        return ret;
    return AcceptConnection(self);
    }

static void CloseAllSockets(AtConnection self)
    {
    if (mThis(self)->socket > 0)
        close(mThis(self)->socket);
    if (mThis(self)->clientSocket > 0)
        close(mThis(self)->clientSocket);

    mThis(self)->socket       = 0;
    mThis(self)->clientSocket = 0;
    }

static eAtConnectionRet Delete(AtConnection self)
    {
    CloseAllSockets(self);
    AtOsalMemFree(mThis(self)->bindAddress);
    return m_AtConnectionMethods->Delete(self);
    }

static int Socket(AtConnection self)
    {
    if (AtConnectionTransportGet(self) == cAtConnectionTransportTcp)
        return mThis(self)->clientSocket;
    return mThis(self)->socket;
    }

static int Receive(AtConnection self)
    {
    eAtConnectionRet ret;

    ret = AtConnectionSetup(self);
    if (ret != cAtConnectionOk)
        return -1;

    return m_AtConnectionMethods->Receive(self);
    }

static void RemoteSideDisconnected(AtConnection self)
    {
    if (mThis(self)->clientSocket > 0)
        close(mThis(self)->clientSocket);
    mThis(self)->clientSocket = 0;
    m_AtConnectionMethods->RemoteSideDisconnected(self);
    }

static struct sockaddr_in *RemoteSocketAddress(AtDefaultConnection self)
    {
    return &(mThis(self)->clientAddress);
    }

static void OverrideAtConnection(AtConnection self)
    {
    if (!m_methodsInit)
        {
        m_AtConnectionMethods = self->methods;
        AtOsalMemCpy(&m_AtConnectionOverride, self->methods, sizeof(m_AtConnectionOverride));

        mMethodOverride(m_AtConnectionOverride, Receive);
        mMethodOverride(m_AtConnectionOverride, Delete);
        mMethodOverride(m_AtConnectionOverride, Socket);
        mMethodOverride(m_AtConnectionOverride, Setup);
        mMethodOverride(m_AtConnectionOverride, RemoteSideDisconnected);
        }

    mMethodsSet(self, &m_AtConnectionOverride);
    }

static void OverrideAtDefaultConnection(AtConnection self)
    {
    AtDefaultConnection connection = (AtDefaultConnection)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtDefaultConnectionOverride, mMethodsGet(connection), sizeof(m_AtDefaultConnectionOverride));

        mMethodOverride(m_AtDefaultConnectionOverride, RemoteSocketAddress);
        }

    mMethodsSet(connection, &m_AtDefaultConnectionOverride);
    }

static void Override(AtConnection self)
    {
    OverrideAtConnection(self);
    OverrideAtDefaultConnection(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultConnectionServer);
    }

static AtConnection ObjectInit(AtConnection self, const char *bindAddress, uint16 port, AtConnectionManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDefaultConnectionObjectInit(self, port, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    if (bindAddress)
        mThis(self)->bindAddress = AtStrdup(bindAddress);
    mThis(self)->bindPort = port;
    AddressInit(self, port);

    return self;
    }

AtConnection AtDefaultConnectionServerNew(const char *bindAddress, uint16 port, AtConnectionManager manager)
    {
    AtConnection newConnection = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newConnection, bindAddress, port, manager);
    }

const char *AtConnectionServerBoundAddress(AtConnection self)
    {
    return self ? mThis(self)->bindAddress : NULL;
    }

uint16 AtConnectionServerListeningPort(AtConnection self)
    {
    return (uint16)(self ? mThis(self)->bindPort : 0);
    }

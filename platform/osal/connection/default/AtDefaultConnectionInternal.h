/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Connection
 * 
 * File        : AtDefaultConnectionInternal.h
 * 
 * Created Date: May 30, 2016
 *
 * Description : Default connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEFAULTCONNECTIONINTERNAL_H_
#define _ATDEFAULTCONNECTIONINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../generic/AtConnectionInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDefaultConnection * AtDefaultConnection;

typedef struct tAtDefaultConnectionMethods
    {
    struct sockaddr_in *(*RemoteSocketAddress)(AtDefaultConnection self);

    /* For subclass to listen */
    void (*RemoteSideConnected)(AtDefaultConnection self, struct sockaddr_in *clientAddress);
    void (*RemoteSideDisconnected)(AtDefaultConnection self);
    }tAtDefaultConnectionMethods;

typedef struct tAtDefaultConnection
    {
    tAtConnection super;
    const tAtDefaultConnectionMethods *methods;

    /* Remote address information */
    char remoteIpAddress[32];
    char *pRemoteIpAddress;
    }tAtDefaultConnection;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConnection AtDefaultConnectionObjectInit(AtConnection self, uint16 port, AtConnectionManager manager);

AtConnection AtDefaultConnectionServerNew(const char *bindAddress, uint16 port, AtConnectionManager manager);
AtConnection AtDefaultConnectionClientNew(const char *ipAddress, uint16 port, AtConnectionManager manager);

int AtDefaultConnectionSocketCreate(AtConnection self, uint32 receiveTimeoutInSeconds);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEFAULTCONNECTIONINTERNAL_H_ */


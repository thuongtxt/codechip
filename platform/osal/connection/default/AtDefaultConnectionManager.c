/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Connection
 *
 * File        : AtDefaultConnectionManager.c
 *
 * Created Date: May 30, 2016
 *
 * Description : Default connection manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <string.h>
#include <errno.h>
#include "../generic/AtConnectionManagerInternal.h"
#include "AtDefaultConnectionInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDefaultConnectionManager
    {
    tAtConnectionManager super;
    }tAtDefaultConnectionManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtConnectionManagerMethods m_AtConnectionManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConnection ServerConnectionCreate(AtConnectionManager self, const char *bindAddress, uint16 port)
    {
    AtUnused(self);
    return AtDefaultConnectionServerNew(bindAddress, port, self);
    }

static AtConnection ClientConnectionCreate(AtConnectionManager self, const char *ipAddress, uint16 port)
    {
    AtUnused(self);
    return AtDefaultConnectionClientNew(ipAddress, port, self);
    }

static const char * OsError2String(AtConnectionManager self)
    {
    AtUnused(self);
    return strerror(errno);
    }

static uint32 Htonl(AtConnectionManager self, uint32 hostlong)
    {
    AtUnused(self);
    return htonl(hostlong);
    }

static uint32 Ntohl(AtConnectionManager self, uint32 netlong)
    {
    AtUnused(self);
    return ntohl(netlong);
    }

static void OverrideAtConnectionManager(AtConnectionManager self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtConnectionManagerOverride, mMethodsGet(self), sizeof(m_AtConnectionManagerOverride));

        mMethodOverride(m_AtConnectionManagerOverride, ServerConnectionCreate);
        mMethodOverride(m_AtConnectionManagerOverride, ClientConnectionCreate);
        mMethodOverride(m_AtConnectionManagerOverride, OsError2String);
        mMethodOverride(m_AtConnectionManagerOverride, Htonl);
        mMethodOverride(m_AtConnectionManagerOverride, Ntohl);
        }

    mMethodsSet(self, &m_AtConnectionManagerOverride);
    }

static void Override(AtConnectionManager self)
    {
    OverrideAtConnectionManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultConnectionManager);
    }

static AtConnectionManager ObjectInit(AtConnectionManager self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtConnectionManagerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConnectionManager AtConnectionManagerDefault(void)
    {
    static tAtDefaultConnectionManager m_manager;
    static AtConnectionManager manager = NULL;

    if (manager == NULL)
        manager = ObjectInit((AtConnectionManager)&m_manager);

    return manager;
    }

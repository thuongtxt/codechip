/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IP
 *
 * File        : AtConnection.c
 *
 * Created Date: Feb 12, 2015
 *
 * Description : Abstract connection
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtDefaultConnectionInternal.h"
#include "os/AtDefaultConnectionOs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtDefaultConnection)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtDefaultConnectionMethods m_methods;

/* Override */
static tAtConnectionMethods m_AtConnectionOverride;

/* Save super implementation */
static const tAtConnectionMethods *m_AtConnectionMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int TcpSend(AtConnection self, int sentSocket, const char *dataBuffer, uint32 numBytes)
    {
	AtUnused(self);
	return write(sentSocket, mBufCatch(dataBuffer), numBytes);
    }

static int UdpSend(AtConnection self, int sentSocket, const char *dataBuffer, uint32 numBytes)
    {
    struct sockaddr_in *address = mMethodsGet(mThis(self))->RemoteSocketAddress(mThis(self));
    return sendto(sentSocket, mBufCatch(dataBuffer), numBytes, 0,
                  (struct sockaddr*)address,
                  sizeof(struct sockaddr_in));
    }

static int TcpReceive(AtConnection self, int receivedSocket)
    {
    return read(receivedSocket, AtConnectionDataBuffer(self), AtConnectionDataBufferSize(self));
    }

static int UdpReceive(AtConnection self, int receivedSocket)
    {
    AtSocketSize addressSize = sizeof(struct sockaddr_in);
    struct sockaddr_in *remoteAddress = mMethodsGet(mThis(self))->RemoteSocketAddress(mThis(self));
    int numBytes = recvfrom(receivedSocket,
                            AtConnectionDataBuffer(self),
                            AtConnectionDataBufferSize(self),
                            0,
                            (struct sockaddr *)remoteAddress,
                            &addressSize);

    if (numBytes)
        mMethodsGet(self)->RemoteSideConnected(self, remoteAddress);

    return numBytes;
    }

static int Socket(AtConnection self)
    {
	AtUnused(self);
    return -1;
    }

static eAtConnectionRet Delete(AtConnection self)
    {
    AtOsalMemFree(self);
    return cAtConnectionOk;
    }

static int SocketCreate(AtConnection self)
    {
    if (AtConnectionTransportGet(self) == cAtConnectionTransportTcp)
        return socket(AF_INET, SOCK_STREAM, 0);
    else
        return socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    }

static int ConfigureSocket(AtConnection self, int aSocket, uint32 receiveTimeoutInSeconds)
    {
    struct timeval tv;
	AtUnused(self);

    if (receiveTimeoutInSeconds == 0)
        return aSocket;

    tv.tv_sec  = (time_t)receiveTimeoutInSeconds;
    tv.tv_usec = 0;

    if (setsockopt(aSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval)) != 0)
        return -1;

    return aSocket;
    }

static eAtConnectionRet ReceiveTimeoutSet(AtConnection self, uint32 receiveTimeoutInSeconds)
    {
    int sock = mMethodsGet(self)->Socket(self);

    if (ConfigureSocket(self, sock, receiveTimeoutInSeconds) != sock)
        return cAtConnectionError;

    return m_AtConnectionMethods->ReceiveTimeoutSet(self, receiveTimeoutInSeconds);
    }

static const char *RemoteAddressGet(AtConnection self)
    {
    return mThis(self)->pRemoteIpAddress;
    }

static void RemoteSideConnected(AtConnection self, void *clientAddress)
    {
    struct sockaddr_in *sockClientAddress = clientAddress;
    in_addr_t address = sockClientAddress->sin_addr.s_addr;
    AtSprintf(mThis(self)->remoteIpAddress,
              "%d.%d.%d.%d",
               address & 0xFF,
              (address & 0xFF00)     >> 8,
              (address & 0xFF0000)   >> 16,
              (address & 0xFF000000) >> 24);
    mThis(self)->pRemoteIpAddress = mThis(self)->remoteIpAddress;
    }

static struct sockaddr_in *RemoteSocketAddress(AtDefaultConnection self)
    {
    AtUnused(self);
    return NULL;
    }

static void RemoteSideDisconnected(AtConnection self)
    {
    mThis(self)->pRemoteIpAddress = NULL;
    }

static void MethodsInit(AtDefaultConnection self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, RemoteSocketAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtConnection(AtConnection self)
    {
    if (!m_methodsInit)
        {
        m_AtConnectionMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtConnectionOverride, mMethodsGet(self), sizeof(m_AtConnectionOverride));

        mMethodOverride(m_AtConnectionOverride, Socket);
        mMethodOverride(m_AtConnectionOverride, TcpSend);
        mMethodOverride(m_AtConnectionOverride, TcpReceive);
        mMethodOverride(m_AtConnectionOverride, UdpSend);
        mMethodOverride(m_AtConnectionOverride, UdpReceive);
        mMethodOverride(m_AtConnectionOverride, Delete);
        mMethodOverride(m_AtConnectionOverride, ReceiveTimeoutSet);
        mMethodOverride(m_AtConnectionOverride, RemoteSideConnected);
        mMethodOverride(m_AtConnectionOverride, RemoteSideDisconnected);
        mMethodOverride(m_AtConnectionOverride, RemoteAddressGet);
        }

    mMethodsSet(self, &m_AtConnectionOverride);
    }

static void Override(AtConnection self)
    {
    OverrideAtConnection(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultConnection);
    }

AtConnection AtDefaultConnectionObjectInit(AtConnection self, uint16 port, AtConnectionManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtConnectionObjectInit(self, port, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

int AtDefaultConnectionSocketCreate(AtConnection self, uint32 receiveTimeoutInSeconds)
    {
    int newSocket = -1;

    if (self == NULL)
        return -1;

    newSocket = SocketCreate(self);
    if (newSocket < 0)
        return -1;

    if (ConfigureSocket(self, newSocket, receiveTimeoutInSeconds) != newSocket)
        {
        close(newSocket);
        newSocket = -1;
        }

    return newSocket;
    }

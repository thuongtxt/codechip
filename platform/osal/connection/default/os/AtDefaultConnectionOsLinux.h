/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Connection
 * 
 * File        : AtDefaultConnectionLinux.h
 * 
 * Created Date: Jun 5, 2016
 *
 * Description : Linux definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEFAULTCONNECTIONLINUX_H_
#define _ATDEFAULTCONNECTIONLINUX_H_

/*--------------------------- Includes ---------------------------------------*/
#include <netdb.h>
#include <sys/time.h>

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mBufCatch(buffer) buffer

/*--------------------------- Typedefs ---------------------------------------*/
typedef socklen_t AtSocketSize;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATDEFAULTCONNECTIONLINUX_H_ */


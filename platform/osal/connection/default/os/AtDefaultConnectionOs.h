/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Connection
 * 
 * File        : AtDefaultConnectionLinuxInclude.h
 * 
 * Created Date: Jun 5, 2016
 *
 * Description : Work with different OS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEFAULTCONNECTIONLINUXINCLUDE_H_
#define _ATDEFAULTCONNECTIONLINUXINCLUDE_H_

/*--------------------------- Includes ---------------------------------------*/
#ifdef VXWORKS
#include "AtDefaultConnectionOsVxWorks.h"
#else
#include "AtDefaultConnectionOsLinux.h"
#endif

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATDEFAULTCONNECTIONLINUXINCLUDE_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Connection
 * 
 * File        : AtDefaultConnectionVxWorks.h
 * 
 * Created Date: Jun 5, 2016
 *
 * Description : VxWorks definitions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEFAULTCONNECTIONVXWORKS_H_
#define _ATDEFAULTCONNECTIONVXWORKS_H_

/*--------------------------- Includes ---------------------------------------*/
#include <netdb.h>
#include <sockLib.h>
#include <hostLib.h>
#include <netinet/in.h>

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mBufCatch(buffer) (char *)buffer

/*--------------------------- Typedefs ---------------------------------------*/
typedef int AtSocketSize;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATDEFAULTCONNECTIONVXWORKS_H_ */


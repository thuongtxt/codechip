/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IP
 * 
 * File        : AtConnectionInternal.h
 * 
 * Created Date: Feb 12, 2015
 *
 * Description : Abstract connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONNECTIONINTERNAL_H_
#define _ATCONNECTIONINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConnection.h"
#include "AtList.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtConnectionMethods
    {
    eAtConnectionRet (*Setup)(AtConnection self);
    eAtConnectionRet (*Delete)(AtConnection self);

    int (*TcpSend)(AtConnection self, int sentSocket, const char *dataBuffer, uint32 numBytes);
    int (*TcpReceive)(AtConnection self, int receivedSocket);
    int (*UdpSend)(AtConnection self, int sentSocket, const char *dataBuffer, uint32 numBytes);
    int (*UdpReceive)(AtConnection self, int receivedSocket);

    const char *(*RemoteAddressGet)(AtConnection self);

    int (*Send)(AtConnection self, const char *dataBuffer, uint32 numBytes);
    int (*Receive)(AtConnection self);
    eAtConnectionRet (*ReceiveTimeoutSet)(AtConnection self, uint32 receiveTimeoutInSeconds);
    uint32 (*ReceiveTimeoutGet)(AtConnection self);

    /* Internal */
    int (*Socket)(AtConnection self);

    /* For subclass to listen */
    void (*RemoteSideConnected)(AtConnection self, void *clientAddress);
    void (*RemoteSideDisconnected)(AtConnection self);
    }tAtConnectionMethods;

typedef struct tAtConnection
    {
    const tAtConnectionMethods *methods;

    /* Private data */
    char data[10 * 1024];
    uint32 dataLength;
    uint16 port;
    uint32 receiveTimeoutInSeconds;
    AtConnectionManager manager;

    /* For debugging */
    uint32 numTxPackets, numTxBytes;
    uint32 numTxFailPackets;
    uint32 numRxPackets, numRxBytes;
    char error[128];
    }tAtConnection;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConnection AtConnectionObjectInit(AtConnection self, uint16 port, AtConnectionManager manager);

char *AtConnectionDataBuffer(AtConnection self);
uint32 AtConnectionDataBufferSize(AtConnection self);
AtConnectionManager AtConnectionManagerGet(AtConnection self);
const char *AtConnectionErrorString(AtConnection self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCONNECTIONINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IP
 *
 * File        : AtConnectionManager.c
 *
 * Created Date: Feb 12, 2015
 *
 * Description : Abstract connection manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtConnection.h"
#include "AtOsal.h"
#include "AtConnectionManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtConnectionManagerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConnection ServerConnectionCreate(AtConnectionManager self, const char *bindAddress, uint16 port)
    {
    AtUnused(self);
    AtUnused(bindAddress);
    AtUnused(port);
    return NULL;
    }

static AtConnection ClientConnectionCreate(AtConnectionManager self, const char *ipAddress, uint16 port)
    {
    AtUnused(self);
    AtUnused(ipAddress);
    AtUnused(port);
    return NULL;
    }

static const char * OsError2String(AtConnectionManager self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 Htonl(AtConnectionManager self, uint32 hostlong)
    {
    AtUnused(self);
    return hostlong;
    }

static uint32 Ntohl(AtConnectionManager self, uint32 netlong)
    {
    AtUnused(self);
    return netlong;
    }

static void MethodsInit(AtConnectionManager self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ClientConnectionCreate);
        mMethodOverride(m_methods, ServerConnectionCreate);
        mMethodOverride(m_methods, OsError2String);
        mMethodOverride(m_methods, Htonl);
        mMethodOverride(m_methods, Ntohl);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtConnectionManager);
    }

AtConnectionManager AtConnectionManagerObjectInit(AtConnectionManager self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtConnection AtConnectionManagerServerConnectionCreate(AtConnectionManager self, const char *bindAddress, uint16 port)
    {
    if (self)
        return self->methods->ServerConnectionCreate(self, bindAddress, port);
    return NULL;
    }

AtConnection AtConnectionManagerClientConnectionCreate(AtConnectionManager self, const char *ipAddress, uint16 port)
    {
    if (self)
        return self->methods->ClientConnectionCreate(self, ipAddress, port);
    return NULL;
    }

const char * AtConnectionManagerOsError2String(AtConnectionManager self)
    {
    if (self)
        return mMethodsGet(self)->OsError2String(self);
    return NULL;
    }

uint32 AtConnectionManagerHtonl(AtConnectionManager self, uint32 hostlong)
    {
    if (self)
        return mMethodsGet(self)->Htonl(self, hostlong);
    return hostlong;
    }

uint32 AtConnectionManagerNtohl(AtConnectionManager self, uint32 netlong)
    {
    if (self)
        return mMethodsGet(self)->Ntohl(self, netlong);
    return netlong;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IP
 * 
 * File        : AtConnectionManagerInternal.h
 * 
 * Created Date: Feb 13, 2015
 *
 * Description : Connection manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONNECTIONMANAGERINTERNAL_H_
#define _ATCONNECTIONMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConnectionManager.h"
#include "AtOsal.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtConnectionManagerMethods
    {
    AtConnection (*ServerConnectionCreate)(AtConnectionManager self, const char *bindAddress, uint16 port);
    AtConnection (*ClientConnectionCreate)(AtConnectionManager self, const char *ipAddress, uint16 port);
    const char * (*OsError2String)(AtConnectionManager self);
    uint32 (*Htonl)(AtConnectionManager self, uint32 hostlong);
    uint32 (*Ntohl)(AtConnectionManager self, uint32 netlong);
    }tAtConnectionManagerMethods;

typedef struct tAtConnectionManager
    {
    const tAtConnectionManagerMethods *methods;
    }tAtConnectionManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConnectionManager AtConnectionManagerObjectInit(AtConnectionManager self);

const char * AtConnectionManagerOsError2String(AtConnectionManager self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCONNECTIONMANAGERINTERNAL_H_ */


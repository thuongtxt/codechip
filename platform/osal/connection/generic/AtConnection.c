/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IP
 *
 * File        : AtConnection.c
 *
 * Created Date: Feb 12, 2015
 *
 * Description : Abstract connection
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "AtConnectionInternal.h"
#include "AtConnectionManagerInternal.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtConnection)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtConnectionMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int TcpSend(AtConnection self, int sentSocket, const char *dataBuffer, uint32 numBytes)
    {
    AtUnused(self);
    AtUnused(sentSocket);
    AtUnused(dataBuffer);
    AtUnused(numBytes);
    return -1;
    }

static int UdpSend(AtConnection self, int sentSocket, const char *dataBuffer, uint32 numBytes)
    {
    AtUnused(self);
    AtUnused(sentSocket);
    AtUnused(dataBuffer);
    AtUnused(numBytes);
    return -1;
    }

static int Send(AtConnection self, const char *dataBuffer, uint32 numBytes)
    {
    int sentSocket = self->methods->Socket(self);
    int ret = -1;

    if (AtConnectionTransportGet(self) == cAtConnectionTransportTcp)
        ret = mMethodsGet(self)->TcpSend(self, sentSocket, dataBuffer, numBytes);
    else
        ret = mMethodsGet(self)->UdpSend(self, sentSocket, dataBuffer, numBytes);

    if (ret != (int)numBytes)
        AtPrintc(cSevCritical, "ERROR: Send message fail: %s\r\n", AtConnectionErrorString(self));

    return ret;
    }

static int TcpReceive(AtConnection self, int receivedSocket)
    {
    AtUnused(self);
    AtUnused(receivedSocket);
    return -1;
    }

static int UdpReceive(AtConnection self, int receivedSocket)
    {
    AtUnused(self);
    AtUnused(receivedSocket);
    return -1;
    }

static int Receive(AtConnection self)
    {
    tAtOsalCurTime startTime, curTime;
    int receivedSocket = self->methods->Socket(self);
    int numBytes = -1;
    static const uint32 cTimeoutMs = 500;
    uint32 elapseTimeMs = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        if (AtConnectionTransportGet(self) == cAtConnectionTransportTcp)
            numBytes = mMethodsGet(self)->TcpReceive(self, receivedSocket);
        else
            numBytes = mMethodsGet(self)->UdpReceive(self, receivedSocket);

        if (numBytes > 0)
            return numBytes;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Print error */
    AtPrintc(cSevCritical, "ERROR: Receiving fail: %s\r\n", AtConnectionErrorString(self));
    mMethodsGet(self)->RemoteSideDisconnected(self);

    return numBytes;
    }

static int Socket(AtConnection self)
    {
	AtUnused(self);
    return -1;
    }

static eAtConnectionRet Delete(AtConnection self)
    {
    AtOsalMemFree(self);
    return cAtConnectionOk;
    }

static eAtConnectionRet Setup(AtConnection self)
    {
	AtUnused(self);
    return cAtConnectionOk;
    }

static void RemoteSideDisconnected(AtConnection self)
    {
    AtUnused(self);
    }

static uint32 ReceiveTimeoutGet(AtConnection self)
    {
    return self->receiveTimeoutInSeconds;
    }

static eAtConnectionRet ReceiveTimeoutSet(AtConnection self, uint32 receiveTimeoutInSeconds)
    {
    self->receiveTimeoutInSeconds = receiveTimeoutInSeconds;
    return cAtOk;
    }

static void Debug(AtConnection self)
    {
    AtPrintc(cSevNormal, "* numTxPackets     : %u\r\n", self->numTxPackets);
    AtPrintc(cSevNormal, "* numTxBytes       : %u\r\n", self->numTxBytes);
    AtPrintc(cSevNormal, "* numTxFailPackets : %u\r\n", self->numTxFailPackets);
    AtPrintc(cSevNormal, "* numRxPackets     : %u\r\n", self->numRxPackets);
    AtPrintc(cSevNormal, "* numRxBytes       : %u\r\n", self->numRxBytes);

    /* Clear counters */
    self->numTxPackets = 0;
    self->numTxBytes = 0;
    self->numTxFailPackets = 0;
    self->numRxPackets = 0;
    self->numRxBytes = 0;
    }

static void RemoteSideConnected(AtConnection self, void *clientAddress)
    {
    AtUnused(self);
    AtUnused(clientAddress);
    }

static const char *RemoteAddressGet(AtConnection self)
    {
    AtUnused(self);
    return NULL;
    }

static void MethodsInit(AtConnection self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Socket);
        mMethodOverride(m_methods, TcpSend);
        mMethodOverride(m_methods, TcpReceive);
        mMethodOverride(m_methods, UdpSend);
        mMethodOverride(m_methods, UdpReceive);
        mMethodOverride(m_methods, Send);
        mMethodOverride(m_methods, Delete);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, RemoteSideConnected);
        mMethodOverride(m_methods, RemoteSideDisconnected);
        mMethodOverride(m_methods, ReceiveTimeoutSet);
        mMethodOverride(m_methods, ReceiveTimeoutGet);
        mMethodOverride(m_methods, RemoteAddressGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtConnection);
    }

AtConnection AtConnectionObjectInit(AtConnection self, uint16 port, AtConnectionManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->port = port;
    mThis(self)->manager = manager;
    self->receiveTimeoutInSeconds = 60;

    return self;
    }

char *AtConnectionDataBuffer(AtConnection self)
    {
    return self ? self->data : NULL;
    }

uint32 AtConnectionDataBufferSize(AtConnection self)
    {
    return self ? sizeof(self->data) : 0;
    }

eAtConnectionRet AtConnectionDelete(AtConnection self)
    {
    if (self)
        return mMethodsGet(self)->Delete(self);
    return cAtConnectionOk;
    }

eAtConnectionTransport AtConnectionTransportGet(AtConnection self)
    {
	AtUnused(self);
    return cAtConnectionTransportUdp;
    }

int AtConnectionSend(AtConnection self, const char *dataBuffer, uint32 numBytes)
    {
    int numSentBytes;

    if (self == NULL)
        return -1;

    numSentBytes = mMethodsGet(self)->Send(self, dataBuffer, numBytes);

    /* Update counters */
    if (numSentBytes == (int)numBytes)
        {
        self->numTxPackets = self->numTxPackets + 1;
        self->numTxBytes   = self->numTxBytes + numBytes;
        }
    else
        self->numTxFailPackets = self->numTxFailPackets + 1;

    return numSentBytes;
    }

int AtConnectionReceive(AtConnection self)
    {
    int numBytes;

    if (self == NULL)
        return -1;

    numBytes = Receive(self);
    if (numBytes > 0)
        {
        self->numRxPackets = self->numRxPackets + 1;
        self->numRxBytes   = self->numRxBytes + (uint32)numBytes;
        }

    return numBytes;
    }

const char *AtConnectionRemoteAddressGet(AtConnection self)
    {
    if (self)
        return mMethodsGet(self)->RemoteAddressGet(self);
    return NULL;
    }

uint16 AtConnectionConnectedPortGet(AtConnection self)
    {
    return (uint16)(self ? self->port : cBit15_0);
    }

const char *AtConnectionErrorDescription(AtConnection self)
    {
    return self ? self->error : NULL;
    }

eAtConnectionRet AtConnectionSetup(AtConnection self)
    {
    if (self)
        return mMethodsGet(self)->Setup(self);
    return cAtConnectionError;
    }

eAtConnectionRet AtConnectionReceiveTimeoutSet(AtConnection self, uint32 receiveTimeoutInSeconds)
    {
    if (self)
        return mMethodsGet(self)->ReceiveTimeoutSet(self, receiveTimeoutInSeconds);
    return cAtConnectionError;
    }

uint32 AtConnectionReceiveTimeoutGet(AtConnection self)
    {
    if (self)
        return mMethodsGet(self)->ReceiveTimeoutGet(self);
    return 0;
    }

void AtConnectionDebug(AtConnection self)
    {
    if (self)
        Debug(self);
    }

AtConnectionManager AtConnectionManagerGet(AtConnection self)
    {
    return self ? self->manager : NULL;
    }

const char *AtConnectionErrorString(AtConnection self)
    {
    AtConnectionManager manager = AtConnectionManagerGet(self);
    return AtConnectionManagerOsError2String(manager);
    }

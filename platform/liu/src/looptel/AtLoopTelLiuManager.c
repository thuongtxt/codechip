/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : AtLiuManager.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : LIU to work on Loop platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../default/AtLiuManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLoopTelLiuManager
    {
    tAtLiuManager super;
    }tAtLoopTelLiuManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtLiuManagerMethods m_AtLiuManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/
AtLiuManager AtLoopTelLiuManagerNew(void);

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtLiuManager self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 PartOffset(AtLiuManager self, uint8 deviceId)
    {
	AtUnused(self);
    return deviceId * 0x400UL;
    }

static eAtLiuDe1EqcMode DefaultCableLengthMode(AtLiuManager self)
    {
	AtUnused(self);
    return cAtLiuE1Eqc120Tp;
    }

static eAtLiuDe1TerImp DefaultTerminateImpedance(AtLiuManager self)
    {
	AtUnused(self);
    return cAtLiuDe1Ter120Ohm;
    }

static eAtLiuDe1FifoMode DefaultFifoMode(AtLiuManager self)
    {
	AtUnused(self);
    return cAtLiuDe1Fifo32Bit;
    }

static void OverrideAtLiuManager(AtLiuManager self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtLiuManagerOverride, mMethodsGet(self), sizeof(m_AtLiuManagerOverride));

        mMethodOverride(m_AtLiuManagerOverride, BaseAddress);
        mMethodOverride(m_AtLiuManagerOverride, PartOffset);
        mMethodOverride(m_AtLiuManagerOverride, DefaultCableLengthMode);
        mMethodOverride(m_AtLiuManagerOverride, DefaultTerminateImpedance);
        mMethodOverride(m_AtLiuManagerOverride, DefaultFifoMode);
        }

    mMethodsSet(self, &m_AtLiuManagerOverride);
    }

static void Override(AtLiuManager self)
    {
    OverrideAtLiuManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLoopTelLiuManager);
    }

static AtLiuManager ObjectInit(AtLiuManager self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtLiuManagerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtLiuManager AtLoopTelLiuManagerNew(void)
    {
    AtLiuManager manager = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(manager);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : AtLiuController.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : LIU controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtOsal.h"
#include "AtLiuDevice.h"
#include "AtLiuReg.h"
#include "AtLiuControllerInternal.h"
#include "AtLiuDeviceInternal.h"
#include "AtLiuManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eAtLiuDe1JitterAttenuatorBwMode
    {
    cAtLiuDe1JitterAttenuatorBw10Hz,  /* Jitter attenuator BW 10 Hz */
    cAtLiuDe1JitterAttenuatorBw1_5Hz  /* Jitter attenuator BW 1.5 Hz */
    }eAtLiuDe1JitterAttenuatorBwMode;

typedef enum eAtLiuDe1LineCode
    {
    cAtLiuDe1LineCodesHdb3, /* Mode of Encoding/Decoding is  HDB3(E1) */
    cAtLiuDe1LineCodesB8zs, /* Mode of Encoding/Decoding is B8ZS (T1) */
    cAtLiuDe1LineCodesAmi , /* Mode of Encoding/Decoding is AMI */
    }eAtLiuDe1LineCode;

typedef enum eAtLiuDe1JitterAttenuatorMode
    {
    cAtLiuDe1JitterAttenuatorModeDisable, /* Disabled */
    cAtLiuDe1JitterAttenuatorModeTxPath , /* Enable Tx path */
    cAtLiuDe1JitterAttenuatorModeRxPath   /* Enable Rx path */
    }eAtLiuDe1JitterAttenuatorMode;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 LiuId(AtLiuController self)
    {
    return self->liuId;
    }

static AtLiuDevice DeviceGet(AtLiuController self)
	{
	return self->device;
	}

static uint32 Read(AtLiuController self, uint32 regAddr)
	{
	return AtLiuDeviceRead(DeviceGet(self), regAddr);
	}

static void Write(AtLiuController self, uint32 regAddr, uint32 regVal)
	{
	AtLiuDeviceWrite(DeviceGet(self), regAddr, regVal);
	}

static uint8 JitterAttenuatorSwToHw(eAtLiuDe1JitterAttenuatorMode mode)
	{
	if (mode == cAtLiuDe1JitterAttenuatorModeDisable)    return 0;
	if (mode == cAtLiuDe1JitterAttenuatorModeTxPath) return 1;
	if (mode == cAtLiuDe1JitterAttenuatorModeRxPath) return 2;

	return 0;
	}

static void JitterAttenuatorModeSet(AtLiuController self, eAtLiuDe1JitterAttenuatorMode mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl1 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlJaSel, JitterAttenuatorSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 JitterAttenuatorBandwidthSwToHw(eAtLiuDe1JitterAttenuatorBwMode mode)
	{
	if (mode == cAtLiuDe1JitterAttenuatorBw10Hz)
		return 0;

	if (mode == cAtLiuDe1JitterAttenuatorBw1_5Hz)
		return 1;

	return 0;
	}

static void JitterAttenuatorBandwidthSet(AtLiuController self, eAtLiuDe1JitterAttenuatorBwMode mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl1 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlJaBw, JitterAttenuatorBandwidthSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 FifoModeSwToHw(eAtLiuDe1FifoMode mode)
	{
	if (mode == cAtLiuDe1Fifo32Bit)
		return 0;

	if (mode == cAtLiuDe1Fifo64Bit)
		return 1;

	return 0;
	}

static void FifoModeSet(AtLiuController self, eAtLiuDe1FifoMode mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl1 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlFifoSz, FifoModeSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 LoopbackModeSwToHw(eAtLiuDe1LoopMode mode)
	{
	if (mode == cAtLiuDe1DualLoop)
		return 4;

	if (mode == cAtLiuDe1AnalogLoop)
		return 5;

	if (mode == cAtLiuDe1RemoteLoop)
		return 6;

	if (mode == cAtLiuDe1DigitalLoop)
		return 7;

	if (mode == cAtLiuDe1NoLoop)
		return 0;

	return 0;
	}

static eAtEpRet LoopbackSet(AtLiuController self, eAtLiuDe1LoopMode mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl2 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlLoop, LoopbackModeSwToHw(mode));
    Write(self, regAddr, regVal);
    return cAtEpRetOk;
	}

static eAtEpRet Enable(AtLiuController self, eBool enable)
	{
	uint8 hwEnable = (uint8)mBoolToBin(enable);
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl2 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlTxOn, hwEnable);
    Write(self, regAddr, regVal);

    regAddr = cEpRegDs1E1TrcvChnCtrl0 + offset;
	regVal = Read(self, regAddr);
	mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlRxOn, hwEnable);
	Write(self, regAddr, regVal);

    regAddr = cEpRegDs1E1TrcvChnCtrl1 + offset;
	regVal = Read(self, regAddr);
	mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlTxtSel, hwEnable);
	mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlRxtSel, hwEnable);
	Write(self, regAddr, regVal);
    return cAtEpRetOk;
	}

static uint8 LineCodeSwToHw(eAtLiuDe1LineCode mode)
	{
	if (mode == cAtLiuDe1LineCodesHdb3)
		return 0;

	if (mode == cAtLiuDe1LineCodesB8zs)
		return 0;

	if (mode == cAtLiuDe1LineCodesAmi)
		return 1;

	return 0;
	}

static void LineCodeSet(AtLiuController self, eAtLiuDe1LineCode mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl3 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlCodes, LineCodeSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 CableLengthModeSwToHw(eAtLiuDe1EqcMode mode)
	{
	if (mode == cAtLiuDs1Eqc100Tp1x)
		return 8;

	if (mode == cAtLiuDs1Eqc100Tp2x)
		return 9;

	if (mode == cAtLiuDs1Eqc100Tp3x)
		return 0xA;

	if (mode == cAtLiuDs1Eqc100Tp4x)
		return 0xB;

	if (mode == cAtLiuDs1Eqc100Tp5x)
		return 0xC;

	if (mode == cAtLiuDs1Eqc100TpArb)
		return 0xD;

	if (mode == cAtLiuE1Eqc75Coax)
		return 0x1C;

	if (mode == cAtLiuE1Eqc120Tp)
		return 0x1D;

	return 0;
	}

static void CableLengthModeSet(AtLiuController self, eAtLiuDe1EqcMode mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl0 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlEqc, CableLengthModeSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 TerminateImpedanceSwToHw(eAtLiuDe1TerImp mode)
	{
	if (mode == cAtLiuDe1Ter100Ohm)
		return 0;

	if (mode == cAtLiuDe1Ter110Ohm)
		return 1;

	if (mode == cAtLiuDe1Ter75Ohm)
		return 2;

	if (mode == cAtLiuDe1Ter120Ohm)
		return 3;

	return 0;
	}

static void TerminateImpedanceSet(AtLiuController self, eAtLiuDe1TerImp mode)
	{
    uint32 offset = AtLiuControllerDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvChnCtrl1 + offset;
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvChnCtrlTerSel, TerminateImpedanceSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static void DefaultSet(AtLiuController self)
	{
    AtLiuManager manager = AtLiuDeviceManagerGet(AtLiuControllerDeviceGet(self));
	CableLengthModeSet(self, AtLiuManagerDefaultCableLengthMode(manager));
	FifoModeSet(self, AtLiuManagerDefaultFifoMode(manager));
	TerminateImpedanceSet(self, AtLiuManagerDefaultTerminateImpedance(manager));
    JitterAttenuatorModeSet(self, cAtLiuDe1JitterAttenuatorModeTxPath);
    JitterAttenuatorBandwidthSet(self, cAtLiuDe1JitterAttenuatorBw10Hz);
    Enable(self, cAtTrue);
    LineCodeSet(self, cAtLiuDe1LineCodesHdb3);
	}

static void RegisterReset(AtLiuController self)
    {
    uint32 offset = AtLiuControllerDefaultOffset(self);

    Write(self, cEpRegDs1E1TrcvChnCtrl0 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvChnCtrl1 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvChnCtrl2 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvChnCtrl3 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvChnCtrl4 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvChnSysIntrEn + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvChnSysTestSel + offset, 0x0);
    }

static void Init(AtLiuController self)
	{
    RegisterReset(self);
	DefaultSet(self);
	}

static void Override(AtLiuController self)
    {
	AtUnused(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLiuController);
    }

AtLiuController AtLiuControllerObjectInit(AtLiuController self, AtLiuDevice device, uint16 liuId)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->device = device;
    self->liuId  = liuId;

    return self;
    }

AtLiuController AtLiuControllerNew(AtLiuDevice device, uint16 liuId)
    {
    AtLiuController controller = AtOsalMemAlloc(ObjectSize());
    return AtLiuControllerObjectInit(controller, device, liuId);
    }

uint32 AtLiuControllerDefaultOffset(AtLiuController self)
    {
    uint32 deviceDefaultOffset;

    if (self == NULL)
        return 0;

    deviceDefaultOffset = AtLiuDeviceDefaultOffset(AtLiuControllerDeviceGet(self));
    return (uint32)((uint32)((LiuId(self) & 0xF) << 5) + deviceDefaultOffset);
    }

AtLiuDevice AtLiuControllerDeviceGet(AtLiuController self)
    {
    return self ? self->device : NULL;
    }

eAtEpRet AtLiuControllerLoopbackSet(AtLiuController self, eAtLiuDe1LoopMode mode)
    {
    if (self)
        return LoopbackSet(self, mode);
    return cAtEpRetErrNullPointer;
    }

void AtLiuControllerInit(AtLiuController self)
	{
	if (self)
		Init(self);
	}

eAtEpRet AtLiuControllerModeSet(AtLiuController self, eAtLiuMode mode)
    {
    if (self == NULL)
        return cAtEpRetErrNullPointer;

    if (mode == cAtLiuE1Mode)
        {
        CableLengthModeSet(self, cAtLiuE1Eqc75Coax);
        LineCodeSet(self, cAtLiuDe1LineCodesHdb3);
        TerminateImpedanceSet(self, cAtLiuDe1Ter75Ohm);
        }
    else
        {
        CableLengthModeSet(self, cAtLiuDs1Eqc100Tp1x);
        LineCodeSet(self, cAtLiuDe1LineCodesB8zs);
        TerminateImpedanceSet(self, cAtLiuDe1Ter100Ohm);
        }

    return cAtEpRetOk;
    }

eAtEpRet AtLiuControllerEnable(AtLiuController self, eBool enable)
    {
    if (self)
        return Enable(self, enable);
    return cAtEpRetErrNullPointer;
    }

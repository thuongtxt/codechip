/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : AtLiuControllerInternal.h
 * 
 * Created Date: Aug 15, 2014
 *
 * Description : LIU controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIUCONTROLLERINTERNAL_H_
#define _ATLIUCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtLiuController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtLiuDe1EqcMode
    {
    cAtLiuDs1Eqc100Tp1x   , /* T1 Short Haul 0 to 133 feet (0.6dB) 100Ohm TP B8ZS */
    cAtLiuDs1Eqc100Tp2x   , /* T1 Short Haul 133 to 266 feet (1.2dB) 100Ohm TP B8ZS */
    cAtLiuDs1Eqc100Tp3x   , /* T1 Short Haul 266 to 399 feet (1.8dB) 100Ohm TP B8ZS */
    cAtLiuDs1Eqc100Tp4x   , /* T1 Short Haul 399 to 533 feet (2.4dB) 100Ohm TP B8ZS */
    cAtLiuDs1Eqc100Tp5x   , /* T1 Short Haul 533 to 655 feet (3.0dB)) 100Ohm TP B8ZS */
    cAtLiuDs1Eqc100TpArb  , /* T1 Short Haul Arbitrary Pulse 100Ohm TP B8ZS */
    cAtLiuE1Eqc75Coax     , /* E1 Short Haul ITU G.703 75Ohm Coax HDB3 */
    cAtLiuE1Eqc120Tp        /* E1 Short Haul ITU G.703 120Om TP HDB3 */
    }eAtLiuDe1EqcMode;

typedef enum eAtLiuDe1TerImp
    {
    cAtLiuDe1Ter100Ohm    , /* Receive Line Impedance is 100Ohm */
    cAtLiuDe1Ter110Ohm    , /* Receive Line Impedance is 110Ohm */
    cAtLiuDe1Ter75Ohm     , /* Receive Line Impedance is 75Ohm */
    cAtLiuDe1Ter120Ohm      /* Receive Line Impedance is 120Ohm */
    }eAtLiuDe1TerImp;

typedef enum eAtLiuDe1FifoMode
    {
    cAtLiuDe1Fifo32Bit,   /* FIFO Depth is 32 bit */
    cAtLiuDe1Fifo64Bit    /* FIFO Depth is 64 bit */
    }eAtLiuDe1FifoMode;

typedef struct tAtLiuController
    {
    tAtObject super;

    /* Private data */
    AtLiuDevice device;
    uint16 liuId;
    }tAtLiuController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLiuController AtLiuControllerObjectInit(AtLiuController self, AtLiuDevice device, uint16 liuId);

uint32 AtLiuControllerDefaultOffset(AtLiuController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLIUCONTROLLERINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : AtLiuManager.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : LIU to work on Loop platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../default/AtLiuManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLiuDefaultManager
    {
    tAtLiuManager super;
    }tAtLiuDefaultManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtLiuManagerMethods m_AtLiuManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 HwDeviceId(AtLiuManager self, uint8 deviceId)
    {
	AtUnused(self);
    return deviceId ? 0: 1;
    }

static void OverrideAtLiuManager(AtLiuManager self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtLiuManagerOverride, mMethodsGet(self), sizeof(m_AtLiuManagerOverride));

        mMethodOverride(m_AtLiuManagerOverride, HwDeviceId);
        }

    mMethodsSet(self, &m_AtLiuManagerOverride);
    }

static void Override(AtLiuManager self)
    {
    OverrideAtLiuManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLiuDefaultManager);
    }

static AtLiuManager ObjectInit(AtLiuManager self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtLiuManagerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtLiuManager AtLiuDefaultManagerNew(void)
    {
    AtLiuManager manager = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(manager);
    }

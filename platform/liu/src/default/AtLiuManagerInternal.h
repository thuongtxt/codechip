/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : AtLiuManagerInternal.h
 * 
 * Created Date: Aug 15, 2014
 *
 * Description : LIU manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIUMANAGERINTERNAL_H_
#define _ATLIUMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtLiuManager.h"
#include "AtHal.h"
#include "AtLiuControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLiuManagerMethods
    {
    uint8 (*MaxNumOfLiusGet)(AtLiuManager self);
    uint32 (*BaseAddress)(AtLiuManager self);
    uint32 (*PartOffset)(AtLiuManager self, uint8 deviceId);
    uint8 (*HwDeviceId)(AtLiuManager self, uint8 deviceId);
    AtLiuDevice (*LiuDeviceObjectCreate)(AtLiuManager self, uint8 deviceId);
    eAtLiuDe1EqcMode (*DefaultCableLengthMode)(AtLiuManager self);
    eAtLiuDe1TerImp (*DefaultTerminateImpedance)(AtLiuManager self);
    eAtLiuDe1FifoMode (*DefaultFifoMode)(AtLiuManager self);
    }tAtLiuManagerMethods;

typedef struct tAtLiuManager
    {
    tAtObject super;
    const tAtLiuManagerMethods *methods;

    /* Private data */
    AtLiuDevice *liuDevices;
    AtHal hal;
    }tAtLiuManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLiuManager AtLiuManagerObjectInit(AtLiuManager self);

uint8 AtLiuManagerHwDeviceId(AtLiuManager self, uint8 deviceId);
uint32 AtLiuManagerDefaultOffset(AtLiuManager self, uint8 deviceId);
eAtLiuDe1EqcMode AtLiuManagerDefaultCableLengthMode(AtLiuManager self);
eAtLiuDe1TerImp AtLiuManagerDefaultTerminateImpedance(AtLiuManager self);
eAtLiuDe1FifoMode AtLiuManagerDefaultFifoMode(AtLiuManager self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLIUMANAGERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : AtLiuManager.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : LIU manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtLiuManagerInternal.h"
#include "AtLiuDevice.h"
#include "AtLiuController.h"
#include "AtLiuReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cEpLiuInvalidRegVal 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtLiuManagerMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtLiuManager self)
    {
	AtUnused(self);
    return 0x12000000;
    }

static uint32 PartOffset(AtLiuManager self, uint8 deviceId)
    {
	AtUnused(self);
    return (uint32)(deviceId << 24);
    }

static eAtEpRet AllLiuDeviceDelete(AtLiuManager self)
    {
    uint32 deviceId;

    if (self->liuDevices == NULL)
        return cAtEpRetOk;

    for (deviceId = 0; deviceId < AtLiuManagerNumLiuDevices(self); deviceId++)
        AtObjectDelete((AtObject)self->liuDevices[deviceId]);

    AtOsalMemFree(self->liuDevices);
    self->liuDevices = NULL;

    return cAtEpRetOk;
    }

static void Delete(AtObject self)
    {
    AtLiuManager manager = (AtLiuManager)self;
    AllLiuDeviceDelete(manager);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static uint8 MaxNumOfLiusGet(AtLiuManager self)
    {
	AtUnused(self);
    return 32;
    }

static AtLiuDevice LiuDeviceObjectCreate(AtLiuManager self, uint8 deviceId)
    {
    return AtLiuDeviceNew(self, deviceId);
    }

static eAtEpRet Init(AtLiuManager self)
	{
	uint8 deviceId;

	for (deviceId = 0; deviceId < AtLiuManagerNumLiuDevices(self); deviceId++)
		AtLiuDeviceInit(AtLiuManagerLiuDeviceGet(self, deviceId));

	return cAtEpRetOk;
	}

static eAtEpRet AllDevicesCreate(AtLiuManager self)
    {
    uint8 deviceId;

    if (self == NULL)
        return cAtEpRetErrNullPointer;

    /* Have memory to hold all devices */
    if (self->liuDevices == NULL)
        {
        uint32 memorySize = sizeof(AtLiuDevice) * AtLiuManagerNumLiuDevices(self);
        self->liuDevices = AtOsalMemAlloc(memorySize);
        if (self->liuDevices)
            AtOsalMemInit(self->liuDevices, 0, memorySize);
        }

    if (self->liuDevices == NULL)
        return cAtEpRetError;

    /* Setup all devices */
    for (deviceId = 0; deviceId < AtLiuManagerNumLiuDevices(self); deviceId++)
        {
        if (self->liuDevices[deviceId] == NULL)
            self->liuDevices[deviceId] = mMethodsGet(self)->LiuDeviceObjectCreate(self, deviceId);
        AtLiuDeviceSetUp(self->liuDevices[deviceId]);
        }

    return cAtEpRetOk;
    }

static uint8 HwDeviceId(AtLiuManager self, uint8 deviceId)
    {
	AtUnused(self);
    return deviceId;
    }

static eAtLiuDe1EqcMode DefaultCableLengthMode(AtLiuManager self)
    {
	AtUnused(self);
    return cAtLiuE1Eqc75Coax;
    }

static eAtLiuDe1TerImp DefaultTerminateImpedance(AtLiuManager self)
    {
	AtUnused(self);
    return cAtLiuDe1Ter75Ohm;
    }

static eAtLiuDe1FifoMode DefaultFifoMode(AtLiuManager self)
    {
	AtUnused(self);
    return cAtLiuDe1Fifo64Bit;
    }

static void OverrideAtObject(AtLiuManager self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtLiuManager self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtLiuManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MaxNumOfLiusGet);
        mMethodOverride(m_methods, LiuDeviceObjectCreate);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, HwDeviceId);
        mMethodOverride(m_methods, DefaultCableLengthMode);
        mMethodOverride(m_methods, DefaultTerminateImpedance);
        mMethodOverride(m_methods, DefaultFifoMode);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLiuManager);
    }

AtLiuManager AtLiuManagerObjectInit(AtLiuManager self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

uint8 AtLiuManagerHwDeviceId(AtLiuManager self, uint8 deviceId)
    {
    if (self)
        return mMethodsGet(self)->HwDeviceId(self, deviceId);
    return deviceId;
    }

uint32 AtLiuManagerDefaultOffset(AtLiuManager self, uint8 deviceId)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self) + mMethodsGet(self)->PartOffset(self, deviceId);
    return 0;
    }

uint8 AtLiuManagerMaxNumOfLiusGet(AtLiuManager self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumOfLiusGet(self);
    return 0;
    }

uint32 AtLiuManagerNumLiuDevices(AtLiuManager self)
    {
    return (self) ? 2 : 0;
    }

AtLiuDevice AtLiuManagerLiuDeviceGet(AtLiuManager self, uint8 deviceId)
    {
    if (self == NULL)
        return NULL;
    return self->liuDevices[deviceId];
    }

uint32 AtLiuManagerRead(AtLiuManager self, uint32 addr)
    {
    if (self)
        return AtHalRead(self->hal, addr);
    return cEpLiuInvalidRegVal;
    }

void AtLiuManagerWrite(AtLiuManager self, uint32 addr, uint32 value)
    {
    if (self)
        AtHalWrite(self->hal, addr, value);
    }

eAtEpRet AtLiuManagerInit(AtLiuManager self)
    {
    eAtEpRet ret = cAtEpRetOk;

    if (self == NULL)
        return cAtEpRetErrNullPointer;

    if (self->hal == NULL)
        return cAtEpRetErrNoHal;

    ret |= AllDevicesCreate(self);
    ret |= Init(self);

    return ret;
    }

AtLiuController AtLiuManagerLiuControllerGet(AtLiuManager self, uint16 liuId)
    {
    uint8 deviceId;
    uint16 localLiuId;
    if (self == NULL)
        return NULL;

    deviceId = (uint8)(liuId / AtLiuDeviceMaxNumOfLiusGet(self->liuDevices[0]));
    localLiuId = liuId % AtLiuDeviceMaxNumOfLiusGet(self->liuDevices[0]);

    return AtLiuDeviceLiuControllerGet(AtLiuManagerLiuDeviceGet(self, deviceId), localLiuId);
    }

eAtEpRet AtLiuManagerLiuModeSet(AtLiuManager self, eAtLiuMode mode)
	{
	uint16 liuPortIndex;

	for (liuPortIndex = 0; liuPortIndex < AtLiuManagerMaxNumOfLiusGet(self); liuPortIndex++)
		{
	    AtLiuController liuController = AtLiuManagerLiuControllerGet(self, liuPortIndex);
		AtLiuControllerModeSet(liuController, mode);
		}

	return cAtEpRetOk;
	}

eAtEpRet AtLiuManagerHalSet(AtLiuManager self, AtHal hal)
    {
    if (self == NULL)
        return cAtEpRetErrNullPointer;
    self->hal = hal;

    return cAtEpRetOk;
    }

AtHal AtLiuManagerHalGet(AtLiuManager self)
    {
    return self ? self->hal : NULL;
    }

eAtLiuDe1EqcMode AtLiuManagerDefaultCableLengthMode(AtLiuManager self)
    {
    return (self) ? mMethodsGet(self)->DefaultCableLengthMode(self) : cAtLiuE1Eqc75Coax;
    }

eAtLiuDe1TerImp AtLiuManagerDefaultTerminateImpedance(AtLiuManager self)
    {
    return (self) ? mMethodsGet(self)->DefaultTerminateImpedance(self) : cAtLiuDe1Ter75Ohm;
    }

eAtLiuDe1FifoMode AtLiuManagerDefaultFifoMode(AtLiuManager self)
    {
    return (self) ? mMethodsGet(self)->DefaultFifoMode(self) : cAtLiuDe1Fifo64Bit;
    }

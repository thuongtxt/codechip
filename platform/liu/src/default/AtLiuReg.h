/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ateppdhreg.h
 * 
 * Created Date: Oct 16, 2012
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEPPDHREG_H_
#define _ATEPPDHREG_H_

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaDdrRegStartAddr         0x600000
#define cEpDdr2InitReg              0x80000B

#define cHalEpRegBlock0MinAddress   0x12000000
#define cHalEpRegBlock0MaxAddress   0x1200FFFF

#define cHalEpRegBlock1MinAddress   0x13000000
#define cHalEpRegBlock1MaxAddress   0x1300FFFF

#define cHalEpRegBlock2MinAddress   0x14000000
#define cHalEpRegBlock2MaxAddress   0x1400FFFF
/*------------------------------------------------------------------------------
Reg Name: EPXXXX Version
Reg Addr: 0x800002
Reg Desc: This register is used to force Error for Ds1E1 ports.
------------------------------------------------------------------------------*/
#define cEpDs1E1FrcReg              0x800002    /* Bit 0 */
#define cEpDs1E1StatReg             0x800000
#define cEpDs1E1PpmCfgStartReg      0x800200
#define cEpDs1E1PpmCfgEndReg        0x80020F
#define cEpDs1E1PpmCfgStep          0x1
#define cEpDs1E1PpmCfgBaseVal       0x7
#define cEpDs1E1StaClrVal           0xffff
#define cEpDs1E1LosbaseAdd          0x20
#define cEpDs1E1LosbaseStep         0x5
#define cEpDs1E1LosShiftVal         0x1
#define cEpDs1E1LosMaskVal          0x2

#define cEpDs3E3LosbaseStep         0x03
#define cEpDs3E3TrcvLosAlmMask                      cBit1
#define cEpDs3E3TrcvLosAlmShift                     1

#define cEpDs3E3FrcReg              0x800002    /* Bit 1 */
#define cEpDs3E3StatReg             0x800001
#define cEpDs3E3StaClrVal           0x3

/* Register for OCN[3:0],GBE[7:4] each bit control one port */
#define cEpOcnGbeEnableTestMdReg    0x800005
#define cEpOcnGbeFrcErrReg          0x800003
#define cEpOcnStatReg               0x800004
#define cEpGbeStatReg               0x800006
/*------------------------------------------------------------------------------
Reg Name: EPXXXX Version
Reg Addr: 0x15000012
Reg Desc: This register is used to contain EPXXXX version.
------------------------------------------------------------------------------*/
#define cEpRegVersionCPLDVerMask                    cBit7_0
#define cEpRegVersionCPLDVerShift                   0

/*------------------------------------------------------------------------------
Reg Name: Group Version
Reg Addr: 0x15000012
Reg Desc: value 0xB4, read_only register. It is used for CPLD RTL identification
------------------------------------------------------------------------------*/
#define cEpRegGroupVersion                          0x1400005B

/*------------------------------------------------------------------------------
Reg Name: All Devices reset
Reg Addr: 0x14000042
Reg Desc: Read/Write this register to control reset signals of all devices
------------------------------------------------------------------------------*/
#define cEpRegAllDevReset                           0x14000042
#define cEpRegAllDevResetEnVal                      0x7F
#define cEpRegAllDevResetDisVal                     0x0

/*-----------
BitField Name: PowerPcRst
BitField Type: R/W
BitField Desc: Power-PC reset bit.
------------*/
#define cEpRegPowerPcResetMask                      cBit6
#define cEpRegPowerPcResetShift                     6
#define cEpRegPowerPcResetMaxVal                    0x1
#define cEpRegPowerPcResetMinVal                    0x0
#define cEpRegPowerPcResetRstVal                    0x0

/*-----------
BitField Name: DS1/E1TransRst
BitField Type: R/W
BitField Desc: DS1/E1 Transceiver reset bit.
------------*/
#define cEpRegDs1E1TrcvResetMask                    cBit5
#define cEpRegDs1E1TrcvResetShift                   5
#define cEpRegDs1E1TrcvResetMaxVal                  0x1
#define cEpRegDs1E1TrcvResetMinVal                  0x0
#define cEpRegDs1E1TrcvResetRstVal                  0x0


/*-----------
BitField Name: DS3/E3TransRst
BitField Type: R/W
BitField Desc: DS3/E3 Transceiver reset bit.
------------*/
#define cEpRegDs3E3TrcvResetMask                    cBit4
#define cEpRegDs3E3TrcvResetShift                   4
#define cEpRegDs3E3TrcvResetMaxVal                  0x1
#define cEpRegDs3E3TrcvResetMinVal                  0x0
#define cEpRegDs3E3TrcvResetRstVal                  0x0

/*-----------
BitField Name: FpgaStratix3Rst
BitField Type: R/W
BitField Desc: FPGA Stratix3 reset bit.
------------*/
#define cEpRegFpgaStra3ResetMask                    cBit3
#define cEpRegFpgaStra3ResetShift                   3
#define cEpRegFpgaStra3ResetMaxVal                  0x1
#define cEpRegFpgaStra3ResetMinVal                  0x0
#define cEpRegFpgaStra3ResetRstVal                  0x0

/*-----------
BitField Name: FpgaCycloneRst
BitField Type: R/W
BitField Desc: FPGA Cyclone reset bit.
------------*/
#define cEpRegFpgaCyclResetMask                     cBit2
#define cEpRegFpgaCyclResetShift                    2
#define cEpRegFpgaCyclResetMaxVal                   0x1
#define cEpRegFpgaCyclResetMinVal                   0x0
#define cEpRegFpgaCyclResetRstVal                   0x0

/*-----------
BitField Name: FpgaStratix3Rst
BitField Type: R/W
BitField Desc: FPGA Stratix3 reset bit.
------------*/
#define cEpRegFpgaStra2ResetMask                    cBit1
#define cEpRegFpgaStra2ResetShift                   1
#define cEpRegFpgaStra2ResetMaxVal                  0x1
#define cEpRegFpgaStra2ResetMinVal                  0x0
#define cEpRegFpgaStra2ResetRstVal                  0x0

/*-----------
BitField Name: FePhyRst
BitField Type: R/W
BitField Desc: FE Physical reset bit.
------------*/
#define cEpRegFePhyResetMask                        cBit0
#define cEpRegFePhyResetShift                       0
#define cEpRegFePhyResetMaxVal                      0x1
#define cEpRegFePhyResetMinVal                      0x0
#define cEpRegFePhyResetRstVal                      0x0


/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
Description: In chip
------------------------------------------------------------------------------*/
#define cAt6000RegDdrRamInit(ramId)                   (cThaDdrRegStartAddr + ((ramId) << 13) + 0xa)
#define cAt6000RegDdrRamInitVal                       1


/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
Description: In chip
------------------------------------------------------------------------------*/
#define cAt6000RegDdrRamCalib(ramId)                  (cThaDdrRegStartAddr + ((ramId)<<13) + 0x7)
#define cAt6000RegDdrRamCalibVal                      7


#define cEpDdr2InitMask                 cBit0
#define cEpDdr2InitShift                0
#define cEpDdr2InitVal                  0x1

#define cEpDDr2CalibErrMask             cBit2
#define cEpDDr2CalibWarningMask         cBit1
/*-----------
BitField Name: DDRRamCalib
BitField Type:
BitField Desc:
------------*/
#define cAt6000RegDdrRamCalibMask                      cBit2_0
#define cAt6000RegDdrRamCalibShift                     0
#define cAt6000RegDdrRamCalibdMaxVal                   0x7
#define cAt6000RegDdrRamCalibMinVal                    0x0
#define cAt6000RegDdrRamCalibRstVal                    0x0

/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
Description: In chip
------------------------------------------------------------------------------*/
#define cAt6000RegDdrRamPrbsMd(ramId)                 (cThaDdrRegStartAddr + ((ramId)<<13) + 0xF)
#define cAt6000RegDdrRamPrbsMdVal                     0xFFFF

/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
Description: In chip
------------------------------------------------------------------------------*/
#define cAt6000RegDdrRamPrbsEn(ramId)                 (cThaDdrRegStartAddr + ((ramId)<<13) + 0xE)
#define cAt6000RegDdrRam32PrbsEnVal                   0xa2
#define cAt6000RegDdrRam16PrbsEnVal                   0x22

#define cAt6000RegDdrRamPrbsRstVal                    0x0
#define cAt6000RegDdrRamFullMdPrbsEnVal               0x22
#define cAt6000RegDdrRamAddrPrbsEnVal                  0x12

/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
Description: In chip
------------------------------------------------------------------------------*/
#define cAt6000RegDdrRamErrCnt(ramId)                 (cThaDdrRegStartAddr + ((ramId)<<13) + 0x3F)
#define cAt6000RegDdrRamErrCntVal                     0xF

/*-----------
BitField Name: DDRRamErrCnt
BitField Type:
BitField Desc:
------------*/
#define cAt6000RegDdrRam32bitErrCntMask               cBit1_0
#define cAt6000RegDdrRam32bitErrCntShift              0
#define cAt6000RegDdrRam32bitErrCntdMaxVal            0x3
#define cAt6000RegDdrRam32bitErrCntMinVal             0x0
#define cAt6000RegDdrRam32bitErrCntRstVal             0x0

#define cAt6000RegDdrRam16bitErrCntMask               cBit1_0
#define cAt6000RegDdrRam16bitErrCntShift              0
#define cAt6000RegDdrRam16bitErrCntdMaxVal            0x3
#define cAt6000RegDdrRam16bitErrCntMinVal             0x0
#define cAt6000RegDdrRam16bitErrCntRstVal             0x0

/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
Description: In chip
------------------------------------------------------------------------------*/
#define cAt6000RegDdrRamPrbsData(ramId)               (cThaDdrRegStartAddr + ((ramId)<<13) + 0x29)
#define cAt6000RegDdrRamPrbsDataVal                   0xF



/*------------------------------------------------------------------------------
 XRT83VSH316 CHIP: DS1/E1 Transceiver Register
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: Device ID Register
Reg Addr: 0x120003FE,0x130003FE
Reg Desc: DS1/E1 Transceiver Device ID
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrvcDevId(devId)                 mEpDs1E1AddrGet(devId, 0x3FE)
#define cEpRegDs1E1TrvcDevIdVal                     0xE8

#define cEpRegDs1E1TrvcDevIdMask                    cBit7_0
#define cEpRegDs1E1TrvcDevIdShift                   0

/*------------------------------------------------------------------------------
Reg Name: Revision ID Register
Reg Addr: 0x120003FF,0x130003FF
Reg Desc: DS1/E1 Transceiver Revision ID
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrvcRevId(devId)                 mEpDs1E1AddrGet(devId, 0x3FF)
#define cEpRegDs1E1TrvcRevIdVal                     0x35

#define cEpRegDs1E1TrvcRevIdMask                    cBit7_0
#define cEpRegDs1E1TrvcRevIdShift                   0

/*------------------------------------------------------------------------------
Reg Name: Global Register 0
Reg Addr: 0x12000000,0x13000000
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl0                     0x0


/*-----------
BitField Name: SR/DR
BitField Type: R/W
BitField Desc: Single Rail/Dual Rail Mode
               0 = Dual Rail Mode
               1 = Single Rail Mode
------------*/
#define cEpDs1E1TrcvComCtrlSrDrMask                 cBit7
#define cEpDs1E1TrcvComCtrlSrDrShift                7
#define cEpDs1E1TrcvComCtrlSrdrRstVal               0x0


/*-----------
BitField Name: ATAOS
BitField Type: R/W
BitField Desc: Line Automatic Transmit All Ones
               0 = Disabled
               1 = Enabled
------------*/
#define cEpDs1E1TrcvComCtrlAtaosMask                cBit6
#define cEpDs1E1TrcvComCtrlAtaosShift               6
#define cEpDs1E1TrcvComCtrlAtaosRstVal              0x0


/*-----------
BitField Name: RCLKE
BitField Type: R/W
BitField Desc: Receive Clock Data
               0 = RPOS/RNEG data is updated on the rising edge of RCLK
               1 = RPOS/RNEG data is updated on the falling edge of RCLK
------------*/
#define cEpDs1E1TrcvComCtrlRclkeMask                cBit5
#define cEpDs1E1TrcvComCtrlRclkeShift               5
#define cEpDs1E1TrcvComCtrlRclkeRstVal              0x0


/*-----------
BitField Name: TCLKE
BitField Type: R/W
BitField Desc: Transmit Clock Data
               0 = TPOS/TNEG data is sampled on the falling edge of TCLK
               1 = TPOS/TNEG data is sampled on the rising edge of TCLK
------------*/
#define cEpDs1E1TrcvComCtrlTclkeMask                cBit4
#define cEpDs1E1TrcvComCtrlTclkeShift               4
#define cEpDs1E1TrcvComCtrlTclkeRstVal              0x0


/*-----------
BitField Name: DATAP
BitField Type: R/W
BitField Desc: Data Polarity
               0 = Transmit input and receive output data is active "High"
               1 = Transmit input and receive output data is active "Low"
------------*/
#define cEpDs1E1TrcvComCtrlDatapMask                cBit3
#define cEpDs1E1TrcvComCtrlDatapShift               3
#define cEpDs1E1TrcvComCtrlDatapRstVal              0x0


/*-----------
BitField Name: GIE
BitField Type: R/W
BitField Desc: Global Interrupt Enable
               The global interrupt enable is used to enable/disable all
               interrupt activity for all 16 channels.  This bit must be set
               "High" for the interrupt pin to operate.
               0 = Disable all interrupt generation
               1 = Enable interrupt generation to the individual channel registers
------------*/
#define cEpDs1E1TrcvComCtrlGlbIntrEnMask            cBit1
#define cEpDs1E1TrcvComCtrlGlbIntrEnShift           1
#define cEpDs1E1TrcvComCtrlGlbIntrEnRstVal          0x0


/*-----------
BitField Name: SRESET
BitField Type: R/W
BitField Desc: Software Reset
               Writing a "1" to this bit for more than 10�S initiates a device
               reset for all internal circuits except the microprocessor register
               bits. To reset the registers to their default setting, use the
               Hardware Reset pin (See the pin description for more details).
------------*/
#define cEpDs1E1TrcvComCtrlSwRstMask                cBit0
#define cEpDs1E1TrcvComCtrlSwRstShift               0
#define cEpDs1E1TrcvComCtrlSwRstRstVal              0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 1
Reg Addr: 0x12000001,0x13000001
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl1                     0x001


/*-----------
BitField Name: RxMUTE
BitField Type: R/W
BitField Desc: Receiver Output Mute Enable
               If RxMUTE is selected, RPOS/RNEG will be pulled "Low" for any
               channel that experiences an RLOS condition.  If an RLOS condition
               does not occur, RxMUTE will remain inactive.
               0 = Disabled
               1 = Enabled
------------*/
#define cEpDs1E1TrcvComCtrlRxMuteMask               cBit2
#define cEpDs1E1TrcvComCtrlRxMuteShift              2
#define cEpDs1E1TrcvComCtrlRxMuteRstVal             0x0


/*-----------
BitField Name: EXLOS
BitField Type: R/W
BitField Desc: Line Extended Loss of Zeros
               The number of zeros required to declare a Digital Loss of Signal is
               extended to 4,096.
               0 = Normal Operation
               1 = Enables the EXLOS function
------------*/
#define cEpDs1E1TrcvComCtrlExLosMask                cBit1
#define cEpDs1E1TrcvComCtrlExLosShift               1
#define cEpDs1E1TrcvComCtrlExLosRstVal              0x0


/*-----------
BitField Name: ICT
BitField Type: R/W
BitField Desc: In Circuit Testing
               0 = Normal Operation
               1 = Sets all output pins to "High" impedance for in circuit testing
------------*/
#define cEpDs1E1TrcvComCtrlIctMask                  cBit0
#define cEpDs1E1TrcvComCtrlIctShift                 0
#define cEpDs1E1TrcvComCtrlIctRstVal                0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 2
Reg Addr: 0x12000002,0x13000002
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl2                     0x002


/*-----------
BitField Name: RxTCNTL
BitField Type: R/W
BitField Desc: Receive Termination Select Control
               This bit sets the LIU to control the RxTSEL function with either
               the individual channel register bit or the global hardware pin.
               0 = Control of the receive termination is set to the register bits
               1 = Control of the receive termination is set to the hardware pin
------------*/
#define cEpDs1E1TrcvComCtrlRxTcntlMask              cBit6
#define cEpDs1E1TrcvComCtrlRxTcntlShift             6
#define cEpDs1E1TrcvComCtrlRxTcntlRstVal            0x0


/*-----------
BitField Name: SYS_EXLOS
BitField Type: R/W
BitField Desc: System Extended Loss of Zeros
               The number of zeros required to declare a Digital Loss of Signal
               is extended to 4,096.
               0 = Normal Operation
               1 = Enables the SYS_EXLOS function
------------*/
#define cEpDs1E1TrcvComCtrlSysExLosMask             cBit3
#define cEpDs1E1TrcvComCtrlSysExLosShift            3
#define cEpDs1E1TrcvComCtrlSysExLosRstVal           0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 3
Reg Addr: 0x12000003,0x13000003
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl3                     0x003


/*-----------
BitField Name: SL[1:0]
BitField Type: R/W
BitField Desc: Slicer Level Select
               00 = 60%
               01 = 65%
               10 = 70%
               11 = 55%
------------*/
#define cEpDs1E1TrcvComCtrlSlMask                   cBit3_2
#define cEpDs1E1TrcvComCtrlSlShift                  2
#define cEpDs1E1TrcvComCtrlSlRstVal                 0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 4
Reg Addr: 0x12000004,0x13000004
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl4                    0x004


/*-----------
BitField Name: MCLKT1out[1:0]
BitField Type: R/W
BitField Desc: MCLKT1Nout Select
               MclkT1out[1:0] is used to program the MCLKT1out pin.  By default,
               the output clock is 1.544MHz.
               00 = 1.544MHz
               01 = 3.088MHz
               10 = 6.176MHz
               11 = 12.352MHz
------------*/
#define cEpDs1E1TrcvComCtrlMclkT1OutMask            cBit7_6
#define cEpDs1E1TrcvComCtrlMclkT1OutShift           6
#define cEpDs1E1TrcvComCtrlMclkT1OutRstVal          0x0

/*-----------
BitField Name: MCLKE1out[1:0]
BitField Type: R/W
BitField Desc: MCLKE1Nout Select
               MclkE1out[1:0]  is  used  to  program  the  MCLKE1Nout  pin. By
               default, the output clock is 2.048MHz.
               00 = 2.048MHz
               01 = 4.096MHz
               10 = 8.192MHz
               11 = 16.384MHz
------------*/
#define cEpDs1E1TrcvComCtrlMclkE1OutMask            cBit5_4
#define cEpDs1E1TrcvComCtrlMclkE1OutShift           4
#define cEpDs1E1TrcvComCtrlMclkE1OutRstVal          0x0



/*------------------------------------------------------------------------------
Reg Name: Global Register 5
Reg Addr: 0x12000005,0x13000005
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl5                     0x005


/*-----------
BitField Name: LCV_OFLW
BitField Type: R/W
BitField Desc: Line Code Violation / Counter Overflow Monitor Select
               This bit is used to select the monitoring activity between the LCV
               and the counter overflow status.  When the 16-bit LCV counter
               saturates, the counter overflow condition is activated. By
               default, the LCV activity is monitored by bit D4 in register
               0xN05h.
               0 = Monitoring LCV
               1 = Monitoring the counter overflow status
------------*/
#define cEpDs1E1TrcvComCtrlLcvOflwMask              cBit7
#define cEpDs1E1TrcvComCtrlLcvOflwShift             7
#define cEpDs1E1TrcvComCtrlLcvOflwRstVal            0x0


/*-----------
BitField Name: LCVen
BitField Type: R/W
BitField Desc: Line Code Violation Counter Enable
               This  bit  is  used  to  enable  the  LCV  counters  for  all  16
               channels within the device. By default, all 16 LCV counters are
               disabled.
               0 = Disabled
               1 = LCV Counters Enabled (For all 16 Channels)
------------*/
#define cEpDs1E1TrcvComCtrlLcvEnMask                cBit4
#define cEpDs1E1TrcvComCtrlLcvEnShift               4
#define cEpDs1E1TrcvComCtrlLcvEnRstVal              0x0


/*-----------
BitField Name: LCVCH
BitField Type: R/W
BitField Desc: Line Code Violation Counter Select
               These bits are used to select which channel is to be addressed for
               reading the contents in register 0x0007h (LSB) and 0x0008 (MSB).
               It  is  also  used  to  address  the  counter  for  a  given  channel  when
               performing an update or reset on a per channel basis.  By default,
               Channel 0 is selected.
------------*/
#define cEpDs1E1TrcvComCtrlLcvChnMask               cBit3_0
#define cEpDs1E1TrcvComCtrlLcvChnShift              0
#define cEpDs1E1TrcvComCtrlLcvChnRstVal             0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 6
Reg Addr: 0x12000006,0x13000006
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl6                     0x006


/*-----------
BitField Name: allRST
BitField Type: R/W
BitField Desc: LCV Counter Reset for All Channels
               This bit is used to reset all internal LCV counters to their
               default state 0x0000h.  This bit must be set to "1" for 1mS.
               0 = Normal Operation
               1 = Resets all Counters
------------*/
#define cEpDs1E1TrcvComCtrlAllRstMask               cBit4
#define cEpDs1E1TrcvComCtrlAllRstShift              4
#define cEpDs1E1TrcvComCtrlAllRstRstVal             0x0


/*-----------
BitField Name: allUPDATE
BitField Type: R/W
BitField Desc: LCV Counter Update for All Channels
               This bit is used to latch the contents of all 16 counters into
               holding registers so that the value of each counter can be read.
               The channel is addressed by using bits D[3:0] in register 0x0005h.
               0 = Normal Operation
               1 = Updates all Counters
------------*/
#define cEpDs1E1TrcvComCtrlAllUpdMask               cBit3
#define cEpDs1E1TrcvComCtrlAllUpdShift              3
#define cEpDs1E1TrcvComCtrlAllUpdRstVal             0x0

/*-----------
BitField Name: chUPDATE
BitField Type: R/W
BitField Desc: LCV Counter Update Per Channel
               This  bit  is  used  to  latch  the  contents  of  the  counter
               for  a  given channel into a holding register so that the value
               of the counter can be read.  The channel is addressed by using
               bits D[3:0] in register 0x0005h.
               0 = Normal Operation
               1 = Updates the Selected Channel
------------*/
#define cEpDs1E1TrcvComCtrlChnUpdMask               cBit1
#define cEpDs1E1TrcvComCtrlChnUpdShift              1
#define cEpDs1E1TrcvComCtrlChnUpdRstVal             0x0

/*-----------
BitField Name: ChRST
BitField Type: R/W
BitField Desc: LCV Counter Reset Per Channel
               This bit is used to reset the LCV counter of a given channel to its
               default state 0x0000h. The channel is addressed by using bits
               D[3:0] in register 0x0005h.  This bit must be set to "1" for 1mS.
               0 = Normal Operation
               1 = Resets the Selected Channel
------------*/
#define cEpDs1E1TrcvComCtrlChnCntRstMask            cBit0
#define cEpDs1E1TrcvComCtrlChnCntRstShift           0
#define cEpDs1E1TrcvComCtrlChnCntRstRstVal          0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 7
Reg Addr: 0x12000007,0x13000007
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl7(devId)              mEpDs1E1AddrGet(devId, 0x007)


/*-----------
BitField Name: LCVCNT
BitField Type: RO
BitField Desc: Line Code Violation Byte Contents[7:0]
               These bits contain the LSB (bits [7:0]) of the LCV counter contents
               for a selected channel.  The channel is addressed by using bits
               D[3:0] in register 0x0005h.
------------*/
#define cEpDs1E1TrcvComCtrlLoLcvCntMask             cBit7_0
#define cEpDs1E1TrcvComCtrlLoLcvCntShift            0


/*------------------------------------------------------------------------------
Reg Name: Global Register 8
Reg Addr: 0x12000008,0x13000008
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl8(devId)              mEpDs1E1AddrGet(devId, 0x008)


/*-----------
BitField Name: LCVCNT
BitField Type: RO
BitField Desc: Line Code Violation Byte Contents[7:0]
               These bits contain the MSB (bits [7:0]) of the LCV counter contents
               for a selected channel.  The channel is addressed by using bits
               D[3:0] in register 0x0005h.
------------*/
#define cEpDs1E1TrcvComCtrlHiLcvCntMask             cBit7_0
#define cEpDs1E1TrcvComCtrlHiLcvCntShift            0


/*------------------------------------------------------------------------------
Reg Name: Global Register 9
Reg Addr: 0x12000009,0x13000009
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrl9                     0x009


/*-----------
BitField Name: TCLKCNL
BitField Type: R/W
BitField Desc: Transmit Clock Control
               When this bit is pulled "High" and there is no TCLK signal present
               on the transmit input path, TTIP/TRING will Transmit All "Ones"
               (TAOS). By default, TTIP/TRING will Transmit All Zeros.
               0 = All Zeros
               1 = All Ones
------------*/
#define cEpDs1E1TrcvComCtrlTclkCnlMask              cBit4
#define cEpDs1E1TrcvComCtrlTclkCnlShift             4
#define cEpDs1E1TrcvComCtrlTclkCnlRstVal            0x0

/*-----------
BitField Name: CLKSEL
BitField Type: R/W
BitField Desc: Clock Input Select
               CLKSEL[3:0] is used to select the input clock source used as the
               internal timing reference.
               0000 = 2.048 MHz
               0001 = 1.544 MHz
               1000 = 4.096 Mhz
               1001 = 3.088 Mhz
               1010 = 8.192 Mhz
               1011 = 6.176 Mhz
               1100 = 16.384 Mhz
               1101 = 12.352 Mhz
               1110 = 2.048 Mhz
               1111 = 1.544 Mhz
------------*/
#define cEpDs1E1TrcvComCtrlClkSelMask               cBit3_0
#define cEpDs1E1TrcvComCtrlClkSelShift              0
#define cEpDs1E1TrcvComCtrlClkSelRstVal             0x0


/*------------------------------------------------------------------------------
Reg Name: Global Register 10
Reg Addr: 0x1200000A-0x1200000B,0x1300000A-0x1300000B
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnIntrStat(chnId)           (mEpDs1E1AddrGet(((chnId)>>4), 0x00A) +\
                                                    (((chnId)&0xF)>>3))


/*-----------
BitField Name: GCHIS
BitField Type: RUR
BitField Desc: Global Channel Interrupt Status for Channel X
               0 = No interrupt activity from channel X
               1 = Interrupt was generated from channel X
------------*/
#define cEpDs1E1TrcvComCtrlGchisMask(chnId)         (cBit0>>((chnId)&0x7))
#define cEpDs1E1TrcvComCtrlGchisShift(chnId)        ((chnId)&0x7)


/*------------------------------------------------------------------------------
Reg Name: RECOVERED CLOCK SELECT
Reg Addr: 0x1200000C,0x1300000C
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvComCtrlRecClkSel             0x00C


/*-----------
BitField Name: RCLKOUT[4:0]
BitField Type: R/W
BitField Desc: Recovered Clock Select
               These register bits are used to select the recovered clock from
               one of the RCLK[15:0] lines and output it on the RCLKOUT pin.
------------*/
#define cEpDs1E1TrcvComCtrlRclkOutMask              cBit4_0
#define cEpDs1E1TrcvComCtrlRclkOutShift             0
#define cEpDs1E1TrcvComCtrlRclkOutRstVal            0x0


/*------------------------------------------------------------------------------
Reg Name: E1 ARBITRARY SELECT
Reg Addr: 0x1200000D,0x1300000D
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvE1ArbiSel                    0x00D


/*-----------
BitField Name: GPIODIR1
BitField Type: R/W
BitField Desc: These bits select the direction of the external GPIO pins on the
               LIU. These pins can be used for general purpose. By default, the
               hardware pins are set as inputs.
               0 = Input
               1 = Output
------------*/
#define cEpDs1E1TrcvComCtrlGpIoDir1Mask             cBit7
#define cEpDs1E1TrcvComCtrlGpIoDir1Shift            7
#define cEpDs1E1TrcvComCtrlGpIoDir1RstVal           0x0


/*-----------
BitField Name: GPIODIR0
BitField Type: R/W
BitField Desc: These bits select the direction of the external GPIO pins on the
               LIU. These pins can be used for general purpose. By default, the
               hardware pins are set as inputs.
               0 = Input
               1 = Output
------------*/
#define cEpDs1E1TrcvComCtrlGpIoDir0Mask             cBit6
#define cEpDs1E1TrcvComCtrlGpIoDir0Shift            6
#define cEpDs1E1TrcvComCtrlGpIoDir0RstVal           0x0


/*-----------
BitField Name: GPIO1
BitField Type: R/W
BitField Desc: GPIO Control Status
               These pins are used to set/monitor the GPIO pins. If the direction
               is input, then these bits monitor the GPIO hardware pins. If the
               direction is output, then these bits set the status of the output
               pins.
------------*/
#define cEpDs1E1TrcvComCtrlGpIo1Mask                cBit5
#define cEpDs1E1TrcvComCtrlGpIo1Shift               5
#define cEpDs1E1TrcvComCtrlGpIo1RstVal              0x0


/*-----------
BitField Name: GPIO0
BitField Type: R/W
BitField Desc: GPIO Control Status
               These pins are used to set/monitor the GPIO pins. If the direction
               is input, then these bits monitor the GPIO hardware pins. If the
               direction is output, then these bits set the status of the output
               pins.
------------*/
#define cEpDs1E1TrcvComCtrlGpIo0Mask                cBit4
#define cEpDs1E1TrcvComCtrlGpIo0Shift               4
#define cEpDs1E1TrcvComCtrlGpIo0RstVal              0x0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 0
Reg Addr: 0x120000020 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl0                     0x020

/*-----------
BitField Name: QRSS/PRBS
BitField Type: R/W
BitField Desc: QRSS/PRBS Select Bits
               These bits are used to select between QRSS and PRBS.
               0 = PRBS
               1 = QRSS
------------*/
#define cEpDs1E1TrcvChnCtrlQrssPrbsMask             cBit7
#define cEpDs1E1TrcvChnCtrlQrssPrbsShift            7
#define cEpDs1E1TrcvChnCtrlQrssPrbsRstVal           0x0


/*-----------
BitField Name: PRBS_Rx/Tx
BitField Type: R/W
BitField Desc: PRBS Receive/Transmit Select:
               This bit is used to select where the output of the PRBS Generator
               is directed if PRBS generation is enabled.
               0 = Normal Operation - PRBS generator is output on TTIP and TRING
               if PRBS generation is enabled.
               1 = PRBS Generator is output on RPOS; RNEG is internally grounded,
               if PRBS generation is enabled.
------------*/
#define cEpDs1E1TrcvChnCtrlPrbsRxTxSelMask          cBit6
#define cEpDs1E1TrcvChnCtrlPrbsRxTxSelShift         6
#define cEpDs1E1TrcvChnCtrlPrbsRxTxSelRstVal        0x0


/*-----------
BitField Name: RxON
BitField Type: R/W
BitField Desc: Receiver ON/OFF
               Upon power up, the receiver is powered OFF.  RxON is used to turn
               the receiver ON or OFF if the hardware pin RxON is pulled "High".
               If the hardware pin is pulled "Low", all receivers are turned off.
               0 = Receiver is Powered Off
               1 = Receiver is Powered On
------------*/
#define cEpDs1E1TrcvChnCtrlRxOnMask                 cBit5
#define cEpDs1E1TrcvChnCtrlRxOnShift                5
#define cEpDs1E1TrcvChnCtrlRxOnRstVal               0x0


/*-----------
BitField Name: EQC
BitField Type: R/W
BitField Desc: Cable Length Settings
------------*/
#define cEpDs1E1TrcvChnCtrlEqcMask                  cBit4_0
#define cEpDs1E1TrcvChnCtrlEqcShift                 0
#define cEpDs1E1TrcvChnCtrlEqcRstVal                0x0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 1
Reg Addr: 0x120000021 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl1                     0x021

/*-----------
BitField Name: RxTSEL
BitField Type: R/W
BitField Desc: Receive Termination Select
               Upon power up, the receiver is in "High" impedance. RxTSEL is
               used to switch between the internal termination and "High"
               impedance.
               0 = External Termination
               1 = Internal Termination
------------*/
#define cEpDs1E1TrcvChnCtrlRxtSelMask               cBit7
#define cEpDs1E1TrcvChnCtrlRxtSelShift              7
#define cEpDs1E1TrcvChnCtrlRxtSelRstVal             0x0


/*-----------
BitField Name: TxTSEL
BitField Type: R/W
BitField Desc: Transmit Termination Select
               Upon power up, the receiver is in "High" impedance. TxTSEL is
               used to switch between the internal termination and "High"
               impedance.
               0 = "High" Impedance
               1 = Internal Termination
------------*/
#define cEpDs1E1TrcvChnCtrlTxtSelMask               cBit6
#define cEpDs1E1TrcvChnCtrlTxtSelShift              6
#define cEpDs1E1TrcvChnCtrlTxtSelRstVal             0x0


/*-----------
BitField Name: TERSEL[1:0]
BitField Type: R/W
BitField Desc: Receive Line Impedance Select
               TERSEL[1:0] are used to select the line impedance for T1/J1/E1.
------------*/
#define cEpDs1E1TrcvChnCtrlTerSelMask               cBit5_4
#define cEpDs1E1TrcvChnCtrlTerSelShift              4
#define cEpDs1E1TrcvChnCtrlTerSelRstVal             0x0


/*-----------
BitField Name: JASEL[1:0]
BitField Type: R/W
BitField Desc: Jitter Attenuator Select
               JASEL[1:0] are used to enable the jitter attenuator in the
               receive or transmit path.  By default, the jitter attenuator is
               disabled.
------------*/
#define cEpDs1E1TrcvChnCtrlJaSelMask                cBit3_2
#define cEpDs1E1TrcvChnCtrlJaSelShift               2
#define cEpDs1E1TrcvChnCtrlJaSelRstVal              0x0


/*-----------
BitField Name: JABW
BitField Type: R/W
BitField Desc: Jitter Bandwidth (E1 Mode Only, T1 is permanently set to 3Hz)
               The jitter bandwidth is a global setting that is applied to both
               the receiver and transmitter jitter attenuator.
               0 = 10Hz
               1 = 1.5Hz
------------*/
#define cEpDs1E1TrcvChnCtrlJaBwMask                 cBit1
#define cEpDs1E1TrcvChnCtrlJaBwShift                1
#define cEpDs1E1TrcvChnCtrlJaBwRstVal               0x0


/*-----------
BitField Name: FIFOS
BitField Type: R/W
BitField Desc: FIFO Depth Select
               The FIFO depth select is used to configure the part for a 32-bit
               or 64-bit FIFO (within the jitter attenuator blocks). The delay
               of the FIFO is equal to � the FIFO depth.  This is a global
               setting that is applied to both the receiver and transmitter FIFO.
               0 = 32-Bit
               1 = 64-Bit
------------*/
#define cEpDs1E1TrcvChnCtrlFifoSzMask               cBit0
#define cEpDs1E1TrcvChnCtrlFifoSzShift              0
#define cEpDs1E1TrcvChnCtrlFifoSzRstVal             0x0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 2
Reg Addr: 0x120000022 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl2                     0x022

/*-----------
BitField Name: INVQRSS
BitField Type: R/W
BitField Desc: QRSS inversion
               INVQRSS is used to invert the transmit QRSS or PRBS pattern set
               by the TxTEST[2:0] bits.  By default (bit D7=0), INVQRSS is
               disabled for PRBS and enabled for QRSS.
               0 = Disabled for PRBS
               0 = Enabled for QRSS
               1 = Disabled for QRSS
               1 = Enabled for PRBS
------------*/
#define cEpDs1E1TrcvChnCtrlInvQrssMask              cBit7
#define cEpDs1E1TrcvChnCtrlInvQrssShift             7
#define cEpDs1E1TrcvChnCtrlInvQrssRstVal            0x0


/*-----------
BitField Name: TxTEST
BitField Type: R/W
BitField Desc: Test Code Pattern
               TxTEST[2:0] are used to select a diagnostic test pattern to the
               line side (transmit outputs). If these bits are selected, the LIU
               is automatically placed in single rail mode.
               0XX = No Pattern
               100 = Tx QRSS
               101 = Tx TAOS
               110 = Tx LOS (All Zeros)
               111 = Reserved
------------*/
#define cEpDs1E1TrcvChnCtrlTxTestMask               cBit6_4
#define cEpDs1E1TrcvChnCtrlTxTestShift              4
#define cEpDs1E1TrcvChnCtrlTxTestRstVal             0x0


/*-----------
BitField Name: TxOn
BitField Type: R/W
BitField Desc: Transmit ON/OFF
               Upon power up, the transmitters are powered off. This bit is used
               to turn the transmitter for this channel On or Off if the TxON
               pin is pulled "High".  If the TxON pin is pulled "Low", all 16
               transmitters are powered off.
               0 = Transmitter is Powered OFF
               1 = Transmitter is Powered ON
------------*/
#define cEpDs1E1TrcvChnCtrlTxOnMask                 cBit3
#define cEpDs1E1TrcvChnCtrlTxOnShift                3
#define cEpDs1E1TrcvChnCtrlTxOnRstVal               0x0

/*-----------
BitField Name: LOOP
BitField Type: R/W
BitField Desc: Loopback Diagnostic Select
               LOOP[2:0] are used to select the loopback mode.
               0XX = No Loopback
               100 = Dual Loopback
               101 = Analog Loopback
               110 = Remote Loopback
               111 = Digital Loopback
------------*/
#define cEpDs1E1TrcvChnCtrlLoopMask                 cBit2_0
#define cEpDs1E1TrcvChnCtrlLoopShift                0
#define cEpDs1E1TrcvChnCtrlLoopRstVal               0x0

/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 3
Reg Addr: 0x120000023 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl3                     0x023

/*-----------
BitField Name: RxRES
BitField Type: R/W
BitField Desc: Receive External Fixed Resistor
               RxRES[1:0] are used to select the value for a high precision
               external resistor to improve return loss.
               00 = None
               01 = 240Ohm
               10 = 210Ohm
               11 = 150Ohm
------------*/
#define cEpDs1E1TrcvChnCtrlRxResMask                cBit7_6
#define cEpDs1E1TrcvChnCtrlRxResShift               6
#define cEpDs1E1TrcvChnCtrlRxResRstVal              0x0


/*-----------
BitField Name: CODES
BitField Type: R/W
BitField Desc: Encoding/Decoding Select (Single Rail Mode Only)
               0 = HDB3 (E1), B8ZS (T1)
               1 = AMI Coding
------------*/
#define cEpDs1E1TrcvChnCtrlCodesMask                cBit5
#define cEpDs1E1TrcvChnCtrlCodesShift               5
#define cEpDs1E1TrcvChnCtrlCodesRstVal              0x0

/*-----------
BitField Name: E1Arben
BitField Type: R/W
BitField Desc: E1 Arbitrary Pulse Enable
               This bit is used to enable the Arbitrary Pulse Generator for
               shaping the transmit pulse shape when E1 mode is selected. If
               this bit is set to "1", this channel will be configured for the
               Arbitrary Mode.
               Each channel is individually controlled by programming the channel
               registers 0xN08 through 0xN0F, where n is the number of the channel.
               "0" = Disabled (Normal E1 Pulse Shape ITU G.703)
               "1" = Arbitrary Pulse Enabled
------------*/
#define cEpDs1E1TrcvChnCtrlE1ArbenMask              cBit3
#define cEpDs1E1TrcvChnCtrlE1ArbenShift             3
#define cEpDs1E1TrcvChnCtrlE1ArbenRstVal            0x0


/*-----------
BitField Name: INSBPV
BitField Type: R/W
BitField Desc: Insert Bipolar Violation
               When this bit transitions from a "0" to a "1", a bipolar violation
               will be inserted in the transmitted data from TPOS, QRSS/PRBS
               pattern.  The state of this bit will be sampled on the rising
               edge of TCLK.  To ensure proper operation, it is recommended to
               write a "0" to this bit before writing a "1".
------------*/
#define cEpDs1E1TrcvChnCtrlInsBpvMask               cBit2
#define cEpDs1E1TrcvChnCtrlInsBpvShift              2
#define cEpDs1E1TrcvChnCtrlInsBpvRstVal             0x0

/*-----------
BitField Name: INSBER
BitField Type: R/W
BitField Desc: Insert Bit Error
               When this bit transitions from a "0" to a "1", a bit error will be
               inserted in the transmitted QRSS/PRBS pattern.  The state of this
               bit will be sampled on the rising edge of TCLK.  To ensure proper
               operation, it is recommended to write a "0" to this bit before
               writing a "1".
------------*/
#define cEpDs1E1TrcvChnCtrlInsBerMask               cBit1
#define cEpDs1E1TrcvChnCtrlInsBerShift              1
#define cEpDs1E1TrcvChnCtrlInsBerRstVal             0x0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 4
Reg Addr: 0x120000024 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl4                     0x024

/*-----------
BitField Name: DMOIE
BitField Type: R/W
BitField Desc: Digital Monitor Output Interrupt Enable
               0 = Masks the DMO function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlDmoIeMask                cBit6
#define cEpDs1E1TrcvChnCtrlDmoIeShift               6
#define cEpDs1E1TrcvChnCtrlDmoIeRstVal              0x0

/*-----------
BitField Name: FLSIE
BitField Type: R/W
BitField Desc: FIFO Limit Status Interrupt Enable
               0 = Masks the FLS function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlFlsIeMask                cBit5
#define cEpDs1E1TrcvChnCtrlFlsIeShift               5
#define cEpDs1E1TrcvChnCtrlFlsIeRstVal              0x0

/*-----------
BitField Name: LCV_OFIE
BitField Type: R/W
BitField Desc: Line Code Violation / Counter Overflow Interrupt Enable
               0 = Masks the LCV_OF function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlLcvIeMask                cBit4
#define cEpDs1E1TrcvChnCtrlLcvIeShift               4
#define cEpDs1E1TrcvChnCtrlLcvIeRstVal              0x0

/*-----------
BitField Name: AISDIE
BitField Type: R/W
BitField Desc: Alarm Indication Signal Detection Interrupt Enable
               0 = Masks the AIS function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlAisdIeMask               cBit2
#define cEpDs1E1TrcvChnCtrlAisdIeShift              2
#define cEpDs1E1TrcvChnCtrlAisdIeRstVal             0x0


/*-----------
BitField Name: RLOSIE
BitField Type: R/W
BitField Desc: Receiver Loss of Signal Interrupt Enable
               0 = Masks the RLOS function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlRlosIeMask               cBit1
#define cEpDs1E1TrcvChnCtrlRlosIeShift              1
#define cEpDs1E1TrcvChnCtrlRlosIeRstVal             0x0

/*-----------
BitField Name: RLOSIE
BitField Type: R/W
BitField Desc: Quasi Random Pattern Detect Interrupt Enable
               0 = Masks the QRPD function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlQrpdIeMask               cBit0
#define cEpDs1E1TrcvChnCtrlQrpdIeShift              0
#define cEpDs1E1TrcvChnCtrlQrpdIeRstVal             0x0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 5
Reg Addr: 0x120000025 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl5                     0x025

/*-----------
BitField Name: DMO
BitField Type: RO
BitField Desc: Digital Monitor Output
               The digital monitor output is always active regardless if the
               interrupt generation is disabled.  This bit indicates the DMO
               activity.  An interrupt will not occur unless the DMOIE is set to
               "1" in the channel register 0xN04h and GIE is set to "1" in the
               global register 0x0000h.
               0 = No Alarm
               1 = Transmit output driver has failures
------------*/
#define cEpDs1E1TrcvChnCtrlDmoMask                  cBit6
#define cEpDs1E1TrcvChnCtrlDmoShift                 6
#define cEpDs1E1TrcvChnCtrlDmoRstVal                0x0


/*-----------
BitField Name: FLS
BitField Type: RO
BitField Desc: FIFO Limit Status
               The FIFO limit status is always active regardless if the interrupt
               generation is disabled.  This bit indicates whether the RD/WR
               pointers are within 3-Bits.  An interrupt will not occur unless the
               FLSIE is set to "1" in the channel register 0xN04h and GIE is set to
               "1" in the global register 0x0000h.
               0 = No Alarm
               1 = RD/WR FIFO pointers are within �3-Bits
------------*/
#define cEpDs1E1TrcvChnCtrlFlsMask                  cBit5
#define cEpDs1E1TrcvChnCtrlFlsShift                 5
#define cEpDs1E1TrcvChnCtrlFlsRstVal                0x0


/*-----------
BitField Name: LCV_OF
BitField Type: RO
BitField Desc: Line Code Violation / Counter Overflow
               This bit serves a dual purpose.  By default, this bit monitors
               the line code violation activity.  However, if bit 7 in register
               0x0005h is set to a "1", this bit monitors the overflow status of
               the internal LCV counter.  An interrupt will not occur unless the
               LCV_OFIE is set to "1" in the channel register 0xN04h and GIE is
               set to "1" in the global register 0x0000h.
               0 = No Alarm
               1 = A line code violation, bipolar violation, or excessive zeros
               has occurred
------------*/
#define cEpDs1E1TrcvChnCtrlLcvMask                  cBit4
#define cEpDs1E1TrcvChnCtrlLcvShift                 4
#define cEpDs1E1TrcvChnCtrlLcvRstval                0x0

/*
#define cEpDs1E1TrcvChnCtrlNlcdMask         cBit3
#define cEpDs1E1TrcvChnCtrlNlcdShift        3
*/

/*-----------
BitField Name: AISD
BitField Type: RO
BitField Desc: Alarm Indication Signal Detection
               The alarm indication signal detection is always active regardless if
               the interrupt generation is disabled.  This bit indicates the AIS
               activity.  An interrupt will not occur unless the AISIE is set to "1" in
               the channel register 0xN04h and GIE is set to "1" in the global
               register 0x0000h.
               0 = No Alarm
               1 = An all ones signal is detected
------------*/
#define cEpDs1E1TrcvChnCtrlAisdMask                 cBit2
#define cEpDs1E1TrcvChnCtrlAisdShift                2
#define cEpDs1E1TrcvChnCtrlAisdRstVal               0x0


/*-----------
BitField Name: RLOS
BitField Type: RO
BitField Desc: Receiver Loss of Signal
               The receiver loss of signal detection is always active regardless if
               the interrupt generation is disabled.  This bit indicates the RLOS
               activity.  An interrupt will not occur unless the RLOSIE is set to "1"
               in the channel register 0xN04h and GIE is set to "1" in the global
               register 0x0000h.
               0 = No Alarm
               1 = An RLOS condition is present
------------*/
#define cEpDs1E1TrcvChnCtrlRlosMask                 cBit1
#define cEpDs1E1TrcvChnCtrlRlosShift                1
#define cEpDs1E1TrcvChnCtrlRlosRstVal               0x0


/*-----------
BitField Name: QRPD
BitField Type: RO
BitField Desc: Quasi Random Pattern Detection
               The quasi random pattern detection is always active regardless if
               the interrupt generation is disabled.  This bit indicates that a QRPD
               has been detected.  An interrupt will not occur unless the QRPDIE
               is set to "1" in the channel register 0xN04h and GIE is set to "1" in
               the global register 0x0000h.
               0 = No Alarm
               1 = A QRP is detected
------------*/
#define cEpDs1E1TrcvChnCtrlQrpdMask                 cBit0
#define cEpDs1E1TrcvChnCtrlQrpdShift                0
#define cEpDs1E1TrcvChnCtrlQrpdRstVal               0x0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 6
Reg Addr: 0x120000026 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl6                     0x026

/*-----------
BitField Name: DMOIS
BitField Type: RUR
BitField Desc: Digital Monitor Output Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlDmoIsMask                cBit6
#define cEpDs1E1TrcvChnCtrlDmoIsShift               6


/*-----------
BitField Name: FLSIS
BitField Type: RUR
BitField Desc: FIFO Limit Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlFlsIsMask                cBit5
#define cEpDs1E1TrcvChnCtrlFlsIsShift               5

/*-----------
BitField Name: LCV_OFIS
BitField Type: RUR
BitField Desc: Line Code Violation / Overflow Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlLcvIsMask                cBit4
#define cEpDs1E1TrcvChnCtrlLcvIsShift               4
/*
#define cEpDs1E1TrcvChnCtrlNlcdIsMask       cBit3
#define cEpDs1E1TrcvChnCtrlNlcdIsShift      3
*/

/*-----------
BitField Name: AISDIS
BitField Type: RUR
BitField Desc: Alarm Indication Signal Detection Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlAisdIsMask               cBit2
#define cEpDs1E1TrcvChnCtrlAisdIsShift              2

/*-----------
BitField Name: RLOSIS
BitField Type: RUR
BitField Desc: Receiver Loss of Signal Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlRlosIsMask               cBit1
#define cEpDs1E1TrcvChnCtrlRlosIsShift              1

/*-----------
BitField Name: QRPDIS
BitField Type: RUR
BitField Desc: Quasi Random Pattern Detection Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlQrpdIsMask               cBit0
#define cEpDs1E1TrcvChnCtrlQrpdIsShift              0


/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 7
Reg Addr: 0x120000027 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
/*
#define cEpRegDs1E1TrcvChnCtrl7(chnId)              ((((chnId)>>4)<<24) + \
                                                (((chnId)&0xF)<<5) + 0x12000027)

#define cEpDs1E1TrcvChnCtrlCLosMask         cBit5_0
#define cEpDs1E1TrcvChnCtrlCLosShift        0

*/

/*------------------------------------------------------------------------------
Reg Name: Channel Control Register 8,9,10,11,12,13,14,15
Reg Addr: 0x120000028-0x12000002F + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnCtrl8(chnId, seg)         (mEpDs1E1AddrGet(((chnId)>>4), 0x028) +\
                                                    (((chnId)&0xF)<<5) + ((seg)&0x7))

/*-----------
BitField Name: SEG
BitField Type: R/W
BitField Desc: Arbitrary Pulse Generation
               The transmit output pulse is divided into 8 individual segments.
               This register is used to program the first segment which corresponds
               to the overshoot of the pulse amplitude.  There are four segments
               for the top portion of the pulse and four segments for the bottom
               portion of the pulse.  Segment number 5 corresponds to the
               undershoot of the pulse.  The MSB of each segment is the sign bit.
               Bit 6 = 0 = Negative Direction
               Bit 6 = 1 = Positive Direction
------------*/
#define cEpDs1E1TrcvChnCtrlLoSegMask                cBit6_0
#define cEpDs1E1TrcvChnCtrlLoSegShift               0
#define cEpDs1E1TrcvChnCtrlLoSegRstVal              0x0



/*------------------------------------------------------------------------------
Reg Name: SYSTEM SIDE INTERRUPT ENABLE REGISTER
Reg Addr: 0x120000030 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnSysIntrEn                 0x030

/*-----------
BitField Name: SAISDIE
BitField Type: R/W
BitField Desc: System Side Alarm Indication Signal Detection Interrupt Enable
               0 = Masks the SAIS function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlSysAisdIeMask            cBit2
#define cEpDs1E1TrcvChnCtrlSysAisdIeShift           2
#define cEpDs1E1TrcvChnCtrlSysAisdIeRstVal          0x0


/*-----------
BitField Name: SRLOSIE
BitField Type: R/W
BitField Desc: System Side Receiver Loss of Signal Interrupt Enable
               0 = Masks the SRLOS function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlSysRlosIeMask            cBit1
#define cEpDs1E1TrcvChnCtrlSysRlosIeShift           1
#define cEpDs1E1TrcvChnCtrlSysRlosIeRstVal          0x0


/*-----------
BitField Name: SQRPDIE
BitField Type: R/W
BitField Desc: System Side Quasi Random Pattern Detect Interrupt Enable
               0 = Masks the SQRPD function
               1 = Enables Interrupt Generation
------------*/
#define cEpDs1E1TrcvChnCtrlSysQrpdIeMask            cBit0
#define cEpDs1E1TrcvChnCtrlSysQrpdIeShift           0
#define cEpDs1E1TrcvChnCtrlSysQrpdIeRstVal          0x0


/*------------------------------------------------------------------------------
Reg Name: SYSTEM SIDE INTERRUPT DETECTION REGISTER
Reg Addr: 0x120000031 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnSysIntrDetect(chnId)      (mEpDs1E1AddrGet(((chnId)>>4), 0x031) +\
                                                    (((chnId)&0xF)<<5))

/*-----------
BitField Name: SAISD
BitField Type: RO
BitField Desc: System Side Alarm Indication Signal Detection
               The alarm indication signal detection is always active regardless
               if the interrupt generation is disabled.  This bit indicates the
               SAIS activity.  An interrupt will not occur unless the SAISIE is
               set to "1" in the channel register 0xN10h and GIE is set to "1"
               in the global register 0x0000h.
               0 = No Alarm
               1 = An all ones signal is detected
------------*/
#define cEpDs1E1TrcvChnCtrlSysAisdMask              cBit2
#define cEpDs1E1TrcvChnCtrlSysAisdShift             2
#define cEpDs1E1TrcvChnCtrlSysAisdRstVal            0x0


/*-----------
BitField Name: SRLOS
BitField Type: RO
BitField Desc: System Side Receiver Loss of Signal
               The transmitter loss of signal detection is always active regardless
               if the interrupt generation is disabled.  This bit indicates the
               SRLOS activity.  An interrupt will not occur unless the SRLOSIE
               is set to "1" in the channel register 0xN10h and GIE is set to
               "1" in the global register 0x0000h.
               0 = No Alarm
               1 = AN SRLOS condition is present
------------*/
#define cEpDs1E1TrcvChnCtrlSysRlosMask              cBit1
#define cEpDs1E1TrcvChnCtrlSysRlosShift             1
#define cEpDs1E1TrcvChnCtrlSysRlosRstVal            0x0


/*-----------
BitField Name: SQRPD
BitField Type: RO
BitField Desc: System Side Quasi Random Pattern Detection
               The quasi random pattern detection is always active regardless if
               the interrupt generation is disabled.  This bit indicates that a
               SQRPD has been detected.  An interrupt will not occur unless the
               SQRPDIE is set to "1" in the channel register 0xN10h and GIE is
               set to "1" in the global register 0x0000h.
               0 = No Alarm
               1 = A SQRP is detected
------------*/
#define cEpDs1E1TrcvChnCtrlSysQrpdMask              cBit0
#define cEpDs1E1TrcvChnCtrlSysQrpdShift             0
#define cEpDs1E1TrcvChnCtrlSysQrpdRstVal            0x0



/*------------------------------------------------------------------------------
Reg Name: SYSTEM SIDE INTERRUPT STATUS REGISTER
Reg Addr: 0x120000032 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnSysIntrStat(chnId)        (mEpDs1E1AddrGet(((chnId)>>4), 0x032) +\
                                                    (((chnId)&0xF)<<5))

/*-----------
BitField Name: SAISDIS
BitField Type: RUR
BitField Desc: System Side Alarm Indication Signal Detection Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlSysAisdIsMask            cBit2
#define cEpDs1E1TrcvChnCtrlSysAisdIsShift           2
#define cEpDs1E1TrcvChnCtrlSysAisdIsRstVal          0x0


/*-----------
BitField Name: SRLOSIS
BitField Type: RUR
BitField Desc: System Side Receiver Loss of Signal Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlSysRlosIsMask            cBit1
#define cEpDs1E1TrcvChnCtrlSysRlosIsShift           1
#define cEpDs1E1TrcvChnCtrlSysRlosIsRstVal          0x0


/*-----------
BitField Name: SQRPDIS
BitField Type: RUR
BitField Desc: System Side Quasi Random Pattern Detection Interrupt Status
               0 = No change
               1 = Change in status occurred
------------*/
#define cEpDs1E1TrcvChnCtrlSysQrpdIsMask            cBit0
#define cEpDs1E1TrcvChnCtrlSysQrpdIsShift           0
#define cEpDs1E1TrcvChnCtrlSysQrpdIsRstVal          0x0


/*------------------------------------------------------------------------------
Reg Name: SYSTEM SIDE TEST PATTERN SELECT REGISTER
Reg Addr: 0x120000033 + devId*0x1000000 + chnId*0x20
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs1E1TrcvChnSysTestSel                0x033

/*-----------
BitField Name: SQRSS/SPRBS
BitField Type: R/W
BitField Desc: System QRSS/PRBS Select Bits
               These bits are used to select between QRSS and PRBS for the
               system side interface.
               0 = PRBS
               1 = QRSS
------------*/
#define cEpDs1E1TrcvChnCtrlSysQrssPrbsMask          cBit7
#define cEpDs1E1TrcvChnCtrlSysQrssPrbsShift         7
#define cEpDs1E1TrcvChnCtrlSysQrssPrbsRstVal        0x0


/*-----------
BitField Name: RxTEST
BitField Type: R/W
BitField Desc: Receive System Side Test Code Pattern
               RxTEST[2:0] are used to select a diagnostic test pattern to the
               system side (receive outputs). If these bits are selected, the
               LIU is automatically placed in single rail mode.
               00 = RTip/Rring
               01 = Rx SAIS
               10 = Rx SLOS (All Zeros)
               11 = Rx SQRSS/SPRBS
------------*/
#define cEpDs1E1TrcvChnCtrlSysRxTestMask            cBit6_5
#define cEpDs1E1TrcvChnCtrlSysRxTestShift           5
#define cEpDs1E1TrcvChnCtrlSysRxTestRstVal          0x0


/*-----------
BitField Name: ALARM
BitField Type: R/W
BitField Desc: Alarm Report Output (Pin RNEG, SR mode Only)
               These bits are used to select which alram will be reported to the
               RNEG pin in single rail mode.
               000 = LCV/EXZ
               001 = Line AIS
               010 = Line QRPD
               011 = Line RLOS
               100 = System SAIS
               101 = System SQRPD/SPRPD
               110 = System SLOS
               111 = GND
------------*/
#define cEpDs1E1TrcvChnCtrlSysAlamMask              cBit4_2
#define cEpDs1E1TrcvChnCtrlSysAlamShift             2
#define cEpDs1E1TrcvChnCtrlSysAlamRstVal            0x0

/*-----------
BitField Name: SINVPRBS
BitField Type: R/W
BitField Desc: System Invert PRBS/QRSS
               This bit is used to select between a normal test pattern or
               inverted test pattern whenever the PRBS/QRSS is selected.
               0 = Normal
               1 = Inverted PRBS/QRSS
------------*/
#define cEpDs1E1TrcvChnCtrlSysInvPrbsMask           cBit1
#define cEpDs1E1TrcvChnCtrlSysInvPrbsShift          1
#define cEpDs1E1TrcvChnCtrlSysInvPrbsRstVal         0x0


/*-----------
BitField Name: SINSBER
BitField Type: R/W
BitField Desc: System Insert Bit Error
               When this bit transitions from a "0" to a "1", a bit error will
               be inserted in the Received QRSS/PRBS pattern.  The state of this
               bit will be updated on the rising edge of RCLK.  To ensure proper
               operation, it is recommended to write a "0" to this bit before
               writing a "1".
------------*/
#define cEpDs1E1TrcvChnCtrlSysInsBerMask            cBit0
#define cEpDs1E1TrcvChnCtrlSysInsBerShift           0
#define cEpDs1E1TrcvChnCtrlSysInsBerRstVal          0x0


/*------------------------------------------------------------------------------
 XRT75R03D chip: DS3/E3 Transceiver Register
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: DS3/E3 Transceiver Device/Part ID Register
Reg Addr: 0x1400003E
Reg Desc: DS3/E3 Transceiver Device ID
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvDevId                        mEpDs3E3AddrGet(0, 0x3E)
#define cEpRegDs3E3TrcvDevIdVal                     0x53

/*-----------
BitField Name: DevID
BitField Type: R/O
BitField Desc:
------------*/
#define cEpDs3E3TrcvDevIdMask                       cBit7_0
#define cEpDs3E3TrcvDevIdShift                      0


/*------------------------------------------------------------------------------
Reg Name: Chip Revision Number Register
Reg Addr: 0x1400003F
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvRegRevNo                     mEpDs3E3AddrGet(0, 0x3F)

/*-----------
BitField Name: RevNumber
BitField Type: R/O
BitField Desc:
------------*/
#define cEpDs3E3TrcvDevRevNoMask                    cBit7_0
#define cEpDs3E3TrcvDevRevNoShift                   0

/*------------------------------------------------------------------------------
Reg Name: APS/Redundancy Control Register
Reg Addr: 0x14000000
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvApsRedCtrl                   mEpDs3E3AddrGet(0, 0x00)

/*-----------
BitField Name: RxONCh
BitField Type: R/W
BitField Desc: This READ/WRITE bit-field is used to either turn on or turn off
               the Receive Section of Channel.
                0 - Shuts off the Receive Section of Channel.
                1 - Turns on the Receive Section of Channel.
------------*/
#define cEpDs3E3TrcvRxOnChMask(chn)                 (cBit4 << (chn))
#define cEpDs3E3TrcvRxOnChShift(chn)                ((chn) + 4)
#define cEpDs3E3TrcvRxOnChMaxVal                    0x1
#define cEpDs3E3TrcvRxOnChMinVal                    0x0
#define cEpDs3E3TrcvRxOnChRstVal                    0x0

/*-----------
BitField Name: TxONCh
BitField Type: R/W
BitField Desc: This READ/WRITE bit-field is used to either turn on or turn off
               the Transmit  Driver associated with Channel x. x=0,1,2.
                0 - Shuts off the Transmit Driver associated with Channel x and
                    tri-states the TTIP_2 and TRING_ 2 output pins.
                1 - Turns on or enables the Transmit Driver associated with
                    Channel x
------------*/
#define cEpDs3E3TrcvTxOnChMask(chn)                 (cBit0 << (chn))
#define cEpDs3E3TrcvTxOnChShift(chn)                (chn)
#define cEpDs3E3TrcvTxOnChMaxVal                    0x1
#define cEpDs3E3TrcvTxOnChMinVal                    0x0
#define cEpDs3E3TrcvTxOnChRstVal                    0x0


/*------------------------------------------------------------------------------
Reg Name: Block Level Interrup Enable Register
Reg Addr: 0x14000020
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvBlkIntrEn                    mEpDs3E3AddrGet(0, 0x20)

/*-----------
BitField Name: Channel Interrupt Enable
BitField Type: R/W
BitField Desc: Channel 2 Interrupt Enable Bit:
                0 - Disables all Channel 2-related Interrupt.
                1 - Enables Channel 2-related Interrupts at the Block Level.  The
                user must still enable individual Channel 2-related Interrupts at
                the source level, before they are enabled for interrupt generation.
------------*/
#define cEpDs3E3TrcvChnBlkIntrEnMask(chn)           (cBit0 << (chn))
#define cEpDs3E3TrcvChnBlkIntrEnShift(chn)          (chn)
#define cEpDs3E3TrcvChnBlkIntrEnMaxVal              0x1
#define cEpDs3E3TrcvChnBlkIntrEnMinVal              0x0
#define cEpDs3E3TrcvChnBlkIntrEnRstVal              0x0


/*------------------------------------------------------------------------------
Reg Name: Block Level Interrupt Status Register
Reg Addr: 0x14000021
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvRegBlkIntrStat               mEpDs3E3AddrGet(0, 0x21)

/*-----------
BitField Name: Channel Interrupt Status
BitField Type: R/O
BitField Desc: 0 - Indicates that there is NO Channel 2-related Interrupt awaiting
               service.
               1 - Indicates that there is at least one Channel 2-related Interrupt
               awaiting service.
------------*/
#define cEpDs3E3TrcvChnBlkIntrStatMask(chn)         (cBit0 << (chn))
#define cEpDs3E3TrcvChnBlkIntrStatShift(chn)        (chn)


/*------------------------------------------------------------------------------
Reg Name: Source Level Interrupt Enable Register
Reg Addr: 0x14000001,0x14000009,0x14000011
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvSrcIntrEn(chn)               (mEpDs3E3AddrGet(0, 0x01)\
                                                    + ((chn) << 3))
#define cEpRegDs3E3TrcvSrcIntrRstVal                0x0


/*-----------
BitField Name: Change of FL Condition Interrupt Enable
BitField Type: R/W
BitField Desc: Change of FL (FIFO Limit Alarm) Condition Interrupt Enable
               This READ/WRITE bit-field is used to either enable or disable the
               Change of FL Condition Interrupt.
               0 - Disables the Change in FL Condition Interrupt.
               1 - Enables the Change in FL Condition Interrupt.
------------*/
#define cEpDs3E3TrcvChgoFLIntrEnMask                cBit3
#define cEpDs3E3TrcvChgoFLIntrEnShift               3
#define cEpDs3E3TrcvChgoFLIntrEnMaxVal              0x1
#define cEpDs3E3TrcvChgoFLIntrEnMinVal              0x0
#define cEpDs3E3TrcvChgoFLIntrEnRstVal              0x0

/*-----------
BitField Name: Change of LOL Condition Interrupt Enable
BitField Type: R/W
BitField Desc: Change of Receive LOL (Loss of Lock) Condition Interrupt Enable
               This READ/WRITE bit-field is used to either enable or disable
               the Change of Receive LOL Condition Interrupt.
               0 - Disables the Change in Receive LOL Condition Interrupt.
               1 - Enables the Change in Receive LOL Condition Interrupt.
------------*/
#define cEpDs3E3TrcvChgoLOLIntrEnMask               cBit2
#define cEpDs3E3TrcvChgoLOLIntrEnShift              2
#define cEpDs3E3TrcvChgoLOLIntrEnMaxVal             0x1
#define cEpDs3E3TrcvChgoLOLIntrEnMinVal             0x0
#define cEpDs3E3TrcvChgoLOLIntrEnRstVal             0x0

/*-----------
BitField Name: Change of LOS Condition Interrupt Enable
BitField Type: R/W
BitField Desc: Change of Receive LOS Condition Interrupt Enable
               0 - Disables the Change in Receive LOS Condition Interrupt.
               1 - Enables the Change in Receive LOS Condition Interrupt.
------------*/
#define cEpDs3E3TrcvChgoLOSIntrEnMask               cBit1
#define cEpDs3E3TrcvChgoLOSIntrEnShift              1
#define cEpDs3E3TrcvChgoLOSIntrEnMaxVal             0x1
#define cEpDs3E3TrcvChgoLOSIntrEnMinVal             0x0
#define cEpDs3E3TrcvChgoLOSIntrEnRstVal             0x0


/*-----------
BitField Name: Change of DMO Condition Interrupt Enable
BitField Type: R/W
BitField Desc: Change of Transmit DMO (Drive Monitor Output) Condition Interrupt
               Enable
                0 - Disables the Change in the DMO Condition Interrupt.
                1 - Enables the Change in the DMO Condition Interrupt.
------------*/
#define cEpDs3E3TrcvChgoDMOIntrEnMask               cBit0
#define cEpDs3E3TrcvChgoDMOIntrEnShift              0
#define cEpDs3E3TrcvChgoDMOIntrEnMaxVal             0x1
#define cEpDs3E3TrcvChgoDMOIntrEnMinVal             0x0
#define cEpDs3E3TrcvChgoDMOIntrEnRstVal             0x0


/*------------------------------------------------------------------------------
Reg Name: Source Level Interrupt Status Register
Reg Addr: 0x14000002,0x1400000A,0x14000012
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvSrcIntrStat(chn)             (mEpDs3E3AddrGet(0, 0x02)\
                                                    +((chn) << 3))


/*-----------
BitField Name: Change of FL Condition Interrupt Status
BitField Type: RUR
BitField Desc: Change of FL (FIFO Limit Alarm) Condition Interrupt Status
                0 - Indicates that the Change of FL Condition Interrupt has
                NOT occurred since the last read of this register.
                1 - Indicates that the Change of FL Condition Interrupt has
                occurred since the last read of this register.
------------*/
#define cEpDs3E3TrcvChgoFLIntrStatMask              cBit3
#define cEpDs3E3TrcvChgoFLIntrStatShift             3


/*-----------
BitField Name: Change of LOL Condition Interrupt Status
BitField Type: RUR
BitField Desc: Change of Receive LOL (Loss of Lock) Condition Interrupt Status
               0 - Indicates that the Change of Receive LOL Condition Interrupt
               has NOT occurred.
               1 - Indicates that the Change of Receive LOL Condition Interrupt
               has occurred.
------------*/
#define cEpDs3E3TrcvChgoLOLIntrStatMask             cBit2
#define cEpDs3E3TrcvChgoLOLIntrStatShift            2

/*-----------
BitField Name: Change of LOS Condition Interrupt Status
BitField Type: RUR
BitField Desc: Change of Receive LOS (Loss of Signal) Condition Interrupt Status
               0 - Indicates that the Change of Receive LOS Defect Condition
               Interrupt has NOT occurred.
               1 - Indicates that the Change of Receive LOS Defect Condition
               Interrupt has occurred.
------------*/
#define cEpDs3E3TrcvChgoLosIntrStatMask             cBit1
#define cEpDs3E3TrcvChgoLosIntrStatShift            1

/*-----------
BitField Name: Change of DMO Condition Interrupt Status
BitField Type: RUR
BitField Desc: Change of Transmit DMO (Drive Monitor Output) Condition Interrupt
               Status
               0 - Indicates that the Change of Transmit DMO Condition
               Interrupt has NOT occurred.
               1 - Indicates that the Change of Transmit DMO Condition
               Interrupt has occurred.
------------*/
#define cEpDs3E3TrcvChgoDmoIntrStatMask             cBit0
#define cEpDs3E3TrcvChgoDmoIntrStatShift            0


/*------------------------------------------------------------------------------
Reg Name: Source Level Interrupt Status Register
Reg Addr: 0x14000003,0x1400000B,0x14000013
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvSrcAlmStat(chn)              (mEpDs3E3AddrGet(0, 0x03)\
                                                    +((chn) << 3))


/*-----------
BitField Name: Loss of PRBS Pattern Sync
BitField Type: R/O
BitField Desc:  0 - Indicates that the PRBS Receiver is currently declaring
                the PRBS Lock condition within the incoming DS3, E3 or
                STS-1 data-stream.
                1 - Indicates that the PRBS Receiver is currently declaring
                the Loss of PRBS Lock condition within the incoming DS3,
                E3 or STs-1 data-stream.
------------*/
#define cEpDs3E3TrcvLoPrbsAlmMask                   cBit6
#define cEpDs3E3TrcvLoPrbsAlmShift                  6

/*-----------
BitField Name: Digital LOS Defect Declared
BitField Type: R/O
BitField Desc:  0 - Indicates that the Digital LOS Detector is NOT declaring
                the LOS Defect Condition.
                1 - Indicates that the Digital LOS Detector is currently
                declaring the LOS Defect condition.
------------*/
#define cEpDs3E3TrcvDLosAlmMask                     cBit5
#define cEpDs3E3TrcvDLosAlmShift                    5


/*-----------
BitField Name: Analog LOS Defect Declared
BitField Type: R/O
BitField Desc:  0 - Indicates that the Analog LOS Detector is NOT declaring
                the LOS Defect Condition.
                1 - Indicates that the Analog LOS Detector is currently
                declaring the LOS Defect condition.
------------*/
#define cEpDs3E3TrcvALosAlmMask                     cBit4
#define cEpDs3E3TrcvALosAlmShift                    4


/*-----------
BitField Name: FL Alarm Declared
BitField Type: R/O
BitField Desc:  0 - Indicates that the  Jitter Attenuator block is NOT declaring
                the  FIFO Limit Alarm condition.
                1 - Indicates that the  Jitter Attenuator block is currently
                declaring the  FIFO Limit Alarm condition.
------------*/
#define cEpDs3E3TrcvFlAlmMask                       cBit3
#define cEpDs3E3TrcvFlAlmShift                      3


/*-----------
BitField Name: Receive LOL Condition Declared
BitField Type: R/O
BitField Desc: 0 - Indicates that the Receive Section of Channel_n is NOT
               currently declaring the LOL Condition.
               1 - Indicates that the Receive Section of Channel_n is cur-
               rently declaring the LOL Condition.
------------*/
#define cEpDs3E3TrcvLolAlmMask                      cBit2
#define cEpDs3E3TrcvLolAlmShift                     2


/*-----------
BitField Name: Receive LOS Defect Condition Declared
BitField Type: R/O
BitField Desc: 0 - Indicates that the Receive Section of Channel_n is NOT
               currently declaring the LOL Condition.
               1 - Indicates that the Receive Section of Channel_n is cur-
               rently declaring the LOL Condition.
------------*/



/*-----------
BitField Name: Transmit DMO Condition Declared
BitField Type: R/O
BitField Desc: 0 - Indicates that the Transmit Section of Channel_n is NOT
               currently declaring the Transmit DMO Alarm condition.
               1 - Indicates that the Transmit Section of Channel_n is currently
               declaring the Transmit DMO Alarm condition.
------------*/
#define cEpDs3E3TrcvDmoAlmMask                      cBit0
#define cEpDs3E3TrcvDmoAlmShift                     0


/*------------------------------------------------------------------------------
Reg Name: Transmit Control Register
Reg Addr: 0x14000004,0x1400000C,0x14000014
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvTxCtrl(chn)                  (mEpDs3E3AddrGet(0, 0x04)\
                                                    +((chn) << 3))

/*-----------
BitField Name: Internal Transmit Drive Monitor
BitField Type: R/W
BitField Desc: 0 - Configures the Transmit Drive Monitor to externally monitor
               the TTIP_n and TRING_n output pins for bipolar pulses.
               1 - Configures the Transmit Drive Monitor to internally monitor
               the TTIP_n and TRING_n output pins for bipolar pulses.
------------*/
#define cEpDs3E3TrcvInTransDrvMonMask               cBit5
#define cEpDs3E3TrcvInTransDrvMonShift              5


/*-----------
BitField Name: Insert PRBS Error
BitField Type: R/W
BitField Desc: A "0 to 1" transition within this bit-field configures the PRBS
               Generator (within the Transmit Section of Channel_n) to generate
               a single bit error within the outbound PRBS pattern-stream.
------------*/
#define cEpDs3E3TrcvInsPrbsErrMask                  cBit4
#define cEpDs3E3TrcvInsPrbsErrShift                 4


/*-----------
BitField Name: TAOS
BitField Type: R/W
BitField Desc: Transmit All OneS Pattern
               0 - Configures the Transmit Section to transmit the data that it
               accepts from the System-side Interface.
               1 - Configures the Transmit Section to generate and transmit the
               Unframed, All Ones pattern.
------------*/
#define cEpDs3E3TrcvTaosMask                        cBit2
#define cEpDs3E3TrcvTaosShift                       2


/*-----------
BitField Name: TxCLKINV
BitField Type: R/W
BitField Desc: Transmit Clock Invert Select
               0 - Configures the Transmit Section (within the corresponding
               channel) to sample the TPDATA_n and TNDATA_n input pins upon the
               falling edge of TxCLK_n.
               1 - Configures the Transmit Section (within the corresponding
               channel) to sample the TPDATA_n and TNDATA_n input pins upon the
               rising edge of TxCLK_n.
------------*/
#define cEpDs3E3TrcvTxClkInvMask                    cBit1
#define cEpDs3E3TrcvTxClkInvShift                   1
#define cEpDs3E3TrcvTxClkInvRiseEdge                0x1
#define cEpDs3E3TrcvTxClkInvFallEdge                0x0

/*-----------
BitField Name: TxLEV
BitField Type: R/W
BitField Desc: Transmit Line Build-Out Select
               0 - If the cable length between the Transmit Output (of the
               corresponding Channel) and the DSX-3/STSX-1 location is 225 feet
               or less.
               1 - If the cable length between the Transmit Output (of the
               corresponding Channel) and the DSX-3/STSX-1 location is 225 feet
               or more.
------------*/
#define cEpDs3E3TrcvTxLevMask                       cBit0
#define cEpDs3E3TrcvTxLevShift                      0


/*------------------------------------------------------------------------------
Reg Name: Receive Control Register
Reg Addr: 0x14000005,0x1400000D,0x14000015
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvRxCtrl(chn)                  (mEpDs3E3AddrGet(0, 0x05)\
                                                     +((chn) << 3))


/*-----------
BitField Name: Disable DLOS Detector
BitField Type: R/W
BitField Desc: 0 - Enables the Digital LOS Detector within Channel_n.
               1 - Disables the Digital LOS Detector within Channel_n.
------------*/
#define cEpDs3E3TrcvDlosDisMask                     cBit5
#define cEpDs3E3TrcvDlosDisShift                    5
#define cEpDs3E3TrcvDlosDisEnVal                    0x1
#define cEpDs3E3TrcvDlosDisDisVal                   0x0


/*-----------
BitField Name: Disable ALOS Detector
BitField Type: R/W
BitField Desc: 0 - Enables the Analog LOS Detector within Channel_n.
               1 - Disables the Analog LOS Detector within Channel_n.
------------*/
#define cEpDs3E3TrcvAlosDisMask                     cBit4
#define cEpDs3E3TrcvAlosDisShift                    4
#define cEpDs3E3TrcvAlosDisEnVal                    0x1
#define cEpDs3E3TrcvAlosDisDisVal                   0x0


/*-----------
BitField Name: RxCLKINV
BitField Type: R/W
BitField Desc: Receive Clock Invert Select
               0 - Configures the Receive Section (within the corresponding
               channel) to output the recovered data via the RPOS_n and RNEG_n
               output pins upon the rising edge of RCLK_n.
               1 - Configures the Receive Section (within the corresponding
               channel) to output the recovered data via the RPOS_n and RNEG_n
               output pins upon the falling edge of RCLK_n.
------------*/
#define cEpDs3E3TrcvRxClkInvMask                    cBit3
#define cEpDs3E3TrcvRxClkInvShift                   3
#define cEpDs3E3TrcvRxClkInvFallEdge                0x1
#define cEpDs3E3TrcvRxClkInvRiseEdge                0x0


/*-----------
BitField Name: LOSMUT Enable
BitField Type: R/W
BitField Desc: Muting upon LOS Enable
               0 - Disables the Muting upon LOS feature. In this setting the
               Receive Section will NOT automatically mute the Recovered Data
               whenever it is declaring the LOS defect condition.
               1 - Enables the Muting upon LOS feature. In this setting the
               Receive Section will automatically mute the Recovered Data
               whenever it is declaring the LOS defect condition.
------------*/
#define cEpDs3E3TrcvLosMutEnMask                    cBit2
#define cEpDs3E3TrcvLosMutEnShift                   2


/*-----------
BitField Name: Receive Monitor Mode Enable
BitField Type: R/W
BitField Desc: 0 - Configures the corresponding channel to operate in the Normal
               Mode.
               1 - Configure the corresponding channel to operate in the Receive
               Monitor Mode.
------------*/
#define cEpDs3E3TrcvRxMonMdEnMask                   cBit1
#define cEpDs3E3TrcvRxMonMdEnShift                  1


/*-----------
BitField Name: Receive Equalizer Enable
BitField Type: R/W
BitField Desc: 0 - Disables the Receive Equalizer within the corresponding channel.
               1 - Enables the Receive Equalizer within the corresponding channel.
------------*/
#define cEpDs3E3TrcvRxEquaEnMask                    cBit0
#define cEpDs3E3TrcvRxEquaEnShift                   0



/*------------------------------------------------------------------------------
Reg Name: Channel Control Register
Reg Addr: 0x14000006,0x1400000E,0x14000016
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvChnCtrl(chn)                 (mEpDs3E3AddrGet(0, 0x06)\
                                                     +((chn) << 3))

/*-----------
BitField Name: PRBS Enable
BitField Type: R/W
BitField Desc: PRBS Generator and Receiver Enable
               0 - Disables both the PRBS Generator and PRBS Receiver within the
               corresponding channel.
               1 - Enables both the PRBS Generator and PRBS Receiver within the
               corresponding channel.
------------*/
#define cEpDs3E3TrcvChnPrbsEnMask                   cBit5
#define cEpDs3E3TrcvChnPrbsEnShift                  5


/*-----------
BitField Name: Loop-Back Select Mode
BitField Type: R/W
BitField Desc: RLB & LLB Bit-field
------------*/
#define cEpDs3E3TrcvChnLbsMask                      cBit4_3
#define cEpDs3E3TrcvChnLbsShift                     3

#define cEpDs3E3TrcvChnLbsNormVal                   0x0
#define cEpDs3E3TrcvChnLbsAlbVal                    0x1
#define cEpDs3E3TrcvChnLbsRlbVal                    0x2
#define cEpDs3E3TrcvChnLbsDlbVal                    0x3

/*-----------
BitField Name: E3_n
BitField Type: R/W
BitField Desc: E3 Mode Select
               0 - Configures Channel_n to operate in either the DS3 or STS-1
               Modes, depending upon the state of Bit 1 (STS-1/DS3_n) within
               this same register.
               1- Configures Channel_n to operate in the E3 Mode.
------------*/
#define cEpDs3E3TrcvChnDe3MdMask                    cBit2
#define cEpDs3E3TrcvChnDe3MdShift                   2


/*-----------
BitField Name: STS-1/DS3_n
BitField Type: R/W
BitField Desc: STS-1/DS3 Mode Select
               0 - Configures Channel_n to operate in the DS3 Mode (provided by
               Bit 2 [E3_n], within this same register) has been set to "0").
               1 - Configures Channel_n to operate in the STS-1 Mode (provided
               that Bit 2 [E3_n], within the same register) has been set to "0".
------------*/
#define cEpDs3E3TrcvChnStsDs3MdMask                 cBit1
#define cEpDs3E3TrcvChnStsDs3MdShift                1
#define cEpDs3E3TrcvChnDs3MdVal                     0x0

/*-----------
BitField Name: SR/DR_n
BitField Type: R/W
BitField Desc: Single-Rail/Dual-Rail Select
               0 - Configures Channel_n to operate in the Dual-Rail Mode.
               1 - Configures Channel_n to operate in the Single-Rail Mode.
------------*/
#define cEpDs3E3TrcvChnDrSrMdMask                   cBit0
#define cEpDs3E3TrcvChnDrSrMdShift                  0



/*------------------------------------------------------------------------------
Reg Name: Jitter Attenuator Control Register
Reg Addr: 0x14000007,0x1400000F,0x14000017
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegDs3E3TrcvJitAttCtrl(chn)              (mEpDs3E3AddrGet(0, 0x07)\
                                                     +((chn) << 3))


/*-----------
BitField Name: SONET APS Recovery Time Disable Ch_n
BitField Type: R/W
BitField Desc: SONET APS Recovery Time Mode Disable - Channel n:
               0 - Enables the "SONET APS Recovery Time" Mode.
               1 - Disables the "SONET APS Recovery Time" Mode.
------------*/
#define cEpDs3E3TrcvSonetApsRecMask                 cBit4
#define cEpDs3E3TrcvSonetApsRecShift                4


/*-----------
BitField Name: JA RESET Ch_n
BitField Type: R/W
BitField Desc: Jitter Attenuator RESET - Channel_n
               Writing a "0 to 1" transition within this bit-field will configure
               the Jitter Attenuator (within Channel_n) to execute a RESET operation.
------------*/
#define cEpDs3E3TrcvJaResetMask                     cBit3
#define cEpDs3E3TrcvJaResetShift                    3


/*-----------
BitField Name: JA1 Ch_n & JA0 Ch_n
BitField Type: R/W
BitField Desc: Jitter Attenuator Configuration Select Input
               JA0 JA1  Jitter Attenuator Mode
               0     0  FIFO Depth = 16 bits
               0     1  FIFO Depth = 32 bits
               1     0  SONET/SDH De-Sync Mode
               1     1  Jitter Attenuator Disabled
------------*/
#define cEpDs3E3TrcvJaCfgMask                       (cBit2 | cBit0)
#define cEpDs3E3TrcvJaCfgShift                      0
#define cEpDs3E3TrcvJaCfgSdhDesyncMd                0x1


/*-----------
BitField Name: JA in Tx Path Ch_n
BitField Type: R/W
BitField Desc: Jitter Attenuator in Transmit/Receive Path Select Bit
               0 - Configures the Jitter Attenuator (within Channel_n) to
               operate in the Receive Path.
               1 - Configures the Jitter Attenuator (within Channel_n) to
               operate in the Transmit Path.
------------*/
#define cEpDs3E3TrcvJaSelMask                       cBit1
#define cEpDs3E3TrcvJaSelShift                      1
#define cEpDs3E3TrcvJaSelTxPath                     0x1
#define cEpDs3E3TrcvJaSelRxPath                     0x0




/*------------------------------------------------------------------------------
Reg Name: LED Latch device 1st
Reg Addr: 0x14000081
Reg Desc: Read/Write this register to control led status.
------------------------------------------------------------------------------*/
#define cThaRegLEDLatdevice1st                      0x14000081

#define cThaRegLEDLatdevice1stRstVal                0x0001FF11
#define cThaRegLEDLatdevice1stRwMsk                 0xFFFFFFFF
#define cThaRegLEDLatdevice1stRwcMsk                0x00000000
/*--------------------------------------
BitField Name: DS1E1T1_LED [77-00]
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaDs1s1t1GreenLEDMask4(portId)                (cBit31 >> ((31 - portId)*2))
#define cThaDs1s1t1GreenLEDShift4(portId)               (31 - ((31 - portId)*2))
#define cThaDs1s1t1GreenLEDMaxVal4                      0x1
#define cThaDs1s1t1GreenLEDMinVal4                      0x0
#define cThaDs1s1t1GreenLEDRstVal4                      0x1

#define cThaDs1s1t1YellowLEDMask4(portId)               (cBit30 >> ((31 - portId)*2))
#define cThaDs1s1t1YellowLEDShift4(portId)              (30 - ((31 - portId)*2))
#define cThaDs1s1t1YellowLEDMaxVal4                     0x1
#define cThaDs1s1t1YellowLEDMinVal4                     0x0
#define cThaDs1s1t1YellowLEDRstVal4                     0x1

/*--------------------------------------
BitField Name: SYSALM_STALED#
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaSYSALMSTALEDMask                           cBit15
#define cThaSYSALMSTALEDShift                          15
#define cThaSYSALMSTALEDMaxVal                         0x1
#define cThaSYSALMSTALEDMinVal                         0x0
#define cThaSYSALMSTALEDRstVal                         0x1

/*--------------------------------------
BitField Name: ACT/STBY_STALED#
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: ACT/STBY_STALED# */
#define cThaACTSTBYSTALEDMask                          cBit14
#define cThaACTSTBYSTALEDShift                         14
#define cThaACTSTBYSTALEDMaxVal                        0x1
#define cThaACTSTBYSTALEDMinVal                        0x0
#define cThaACTSTBYSTALEDRstVal                        0x1
/*
Special Character ',' '/' ':' '-'.....: ACT/STBY_STALED# */

#define cThaDs3E3TrANSLATcHSTATLEDRedMask(portId)     (cBit9 << (portId * 2))
#define cThaDs3E3TrANSLATcHSTATLEDRedShift(portId)    (9 + (portId * 2))
#define cThaDs3E3TrANSLATcHSTATLEDRedMaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDRedMinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDRedRstVal           0x1

#define cThaDs3E3TrANSLATcHSTATLEDGRNMask(portId)     (cBit8 << (portId * 2))
#define cThaDs3E3TrANSLATcHSTATLEDGRNShift(portId)    (8 + (portId * 2))
#define cThaDs3E3TrANSLATcHSTATLEDGRNMaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDGRNMinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDGRNRstVal           0x1

/*--------------------------------------
BitField Name: DS3E3TRANS_LATCH/STAT_LEDGRN_2
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDGRN_2 */
#define cThaDs3E3TrANSLATcHSTATLEDGRN2Mask             cBit13
#define cThaDs3E3TrANSLATcHSTATLEDGRN2Shift            13
#define cThaDs3E3TrANSLATcHSTATLEDGRN2MaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDGRN2MinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDGRN2RstVal           0x1
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDGRN_2 */

/*--------------------------------------
BitField Name: DS3E3TRANS_LATCH/STAT_LEDRED_2
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDRED_2 */
#define cThaDs3E3TrANSLATcHSTATLEDRed2Mask             cBit12
#define cThaDs3E3TrANSLATcHSTATLEDRed2Shift            12
#define cThaDs3E3TrANSLATcHSTATLEDRed2MaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDRed2MinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDRed2RstVal           0x1
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDRED_2 */

/*--------------------------------------
BitField Name: DS3E3TRANS_LATCH/STAT_LEDGRN_1
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDGRN_1 */
#define cThaDs3E3TrANSLATcHSTATLEDGRN1Mask             cBit11
#define cThaDs3E3TrANSLATcHSTATLEDGRN1Shift            11
#define cThaDs3E3TrANSLATcHSTATLEDGRN1MaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDGRN1MinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDGRN1RstVal           0x1
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDGRN_1 */

/*--------------------------------------
BitField Name: DS3E3TRANS_LATCH/STAT_LEDRED_1
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDRED_1 */
#define cThaDs3E3TrANSLATcHSTATLEDRed1Mask             cBit10
#define cThaDs3E3TrANSLATcHSTATLEDRed1Shift            10
#define cThaDs3E3TrANSLATcHSTATLEDRed1MaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDRed1MinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDRed1RstVal           0x1
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDRED_1 */

/*--------------------------------------
BitField Name: DS3E3TRANS_LATCH/STAT_LEDGRN_0
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDGRN_0 */
#define cThaDs3E3TrANSLATcHSTATLEDGRN0Mask             cBit9
#define cThaDs3E3TrANSLATcHSTATLEDGRN0Shift            9
#define cThaDs3E3TrANSLATcHSTATLEDGRN0MaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDGRN0MinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDGRN0RstVal           0x1
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDGRN_0 */

/*--------------------------------------
BitField Name: DS3E3TRANS_LATCH/STAT_LEDRED_0
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDRED_0 */
#define cThaDs3E3TrANSLATcHSTATLEDRed0Mask             cBit8
#define cThaDs3E3TrANSLATcHSTATLEDRed0Shift            8
#define cThaDs3E3TrANSLATcHSTATLEDRed0MaxVal           0x1
#define cThaDs3E3TrANSLATcHSTATLEDRed0MinVal           0x0
#define cThaDs3E3TrANSLATcHSTATLEDRed0RstVal           0x1
/*
Special Character ',' '/' ':' '-'.....: DS3E3TRANS_LATCH/STAT_LEDRED_0 */

/*--------------------------------------
BitField Name: OC12/OPT[8:5]_LATCH/STAT_LEDGRN
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: OC12/OPT */
#define cThaOc12OPTStatLedMask(portId)                        (cBit4 << portId)
#define cThaOc12OPTStatLedShift(portId)                       (4 + portId)
#define cThaOc12OPTStatLedMaxVal                              0x1
#define cThaOc12OPTStatLedMinVal                              0x0
#define cThaOc12OPTStatLedRstVal                              0x1
/*
Special Character ',' '/' ':' '-'.....: OC12/OPT */

/*--------------------------------------
BitField Name: GBE/OPT[4:1]_LATCH/STAT_LEDGRN
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: GBE/OPT */
#define cThaGBEOPTStatLedMask(portId)                         (cBit0 << portId)
#define cThaGBEOPTStatLedShift(portId)                        (portId)
#define cThaGBEOPTStatLedMaxVal                               0x1
#define cThaGBEOPTStatLedMinVal                               0x0
#define cThaGBEOPTStatLedRstVal                               0x1
/*
Special Character ',' '/' ':' '-'.....: GBE/OPT */




/*------------------------------------------------------------------------------
Reg Name: LED Latch device 2nd
Reg Addr: 0x14000082
Reg Desc: Read/Write this register to initial configure Fast Ethernet PHY
          device
------------------------------------------------------------------------------*/
#define cThaRegLEDLatdevice2nd                      0x14000082

#define cThaRegLEDLatdevice2ndRstVal                0x0000FFFF
#define cThaRegLEDLatdevice2ndRwMsk                 0x00007FFF
#define cThaRegLEDLatdevice2ndRwcMsk                0x00000000
/*
#define cThaUnusedMask                                 cBit31_16
#define cThaUnusedShift                                16
#define cThaUnusedRstVal                               0x0
*/

/*
#define cThaUnusedMask                                 cBit15
#define cThaUnusedShift                                15
#define cThaUnusedRstVal                               0x1
*/

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_llb_0
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_llb_0 */
#define cThaDs3e3t1latchstatllb0Mask                   cBit14
#define cThaDs3e3t1latchstatllb0Shift                  14
#define cThaDs3e3t1latchstatllb0MaxVal                 0x1
#define cThaDs3e3t1latchstatllb0MinVal                 0x0
#define cThaDs3e3t1latchstatllb0RstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_llb_0 */

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_rlb_0
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_rlb_0 */
#define cThaDs3e3t1latchstatrlb0Mask                   cBit13
#define cThaDs3e3t1latchstatrlb0Shift                  13
#define cThaDs3e3t1latchstatrlb0MaxVal                 0x1
#define cThaDs3e3t1latchstatrlb0MinVal                 0x0
#define cThaDs3e3t1latchstatrlb0RstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_rlb_0 */

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_llb_1
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_llb_1 */
#define cThaDs3e3t1latchstatllb1Mask                   cBit12
#define cThaDs3e3t1latchstatllb1Shift                  12
#define cThaDs3e3t1latchstatllb1MaxVal                 0x1
#define cThaDs3e3t1latchstatllb1MinVal                 0x0
#define cThaDs3e3t1latchstatllb1RstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_llb_1 */

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_rlb_1
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_rlb_1 */
#define cThaDs3e3t1latchstatrlb1Mask                   cBit11
#define cThaDs3e3t1latchstatrlb1Shift                  11
#define cThaDs3e3t1latchstatrlb1MaxVal                 0x1
#define cThaDs3e3t1latchstatrlb1MinVal                 0x0
#define cThaDs3e3t1latchstatrlb1RstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_rlb_1 */

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_llb_2
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_llb_2 */
#define cThaDs3e3t1latchstatllb2Mask                   cBit10
#define cThaDs3e3t1latchstatllb2Shift                  10
#define cThaDs3e3t1latchstatllb2MaxVal                 0x1
#define cThaDs3e3t1latchstatllb2MinVal                 0x0
#define cThaDs3e3t1latchstatllb2RstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_llb_2 */

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_rlb_2
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_rlb_2 */
#define cThaDs3e3t1latchstatrlb2Mask                   cBit9
#define cThaDs3e3t1latchstatrlb2Shift                  9
#define cThaDs3e3t1latchstatrlb2MaxVal                 0x1
#define cThaDs3e3t1latchstatrlb2MinVal                 0x0
#define cThaDs3e3t1latchstatrlb2RstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_rlb_2 */

/*--------------------------------------
BitField Name: Ds3e3t1_latch/stat_Sr/Dr#
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_Sr/Dr# */
#define cThaDs3e3t1latchstatSrDrMask                   cBit8
#define cThaDs3e3t1latchstatSrDrShift                  8
#define cThaDs3e3t1latchstatSrDrMaxVal                 0x1
#define cThaDs3e3t1latchstatSrDrMinVal                 0x0
#define cThaDs3e3t1latchstatSrDrRstVal                 0x1
/*
Special Character ',' '/' ':' '-'.....: Ds3e3t1_latch/stat_Sr/Dr# */

/*--------------------------------------
BitField Name: Fephy_Modesel1
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyModesel1Mask                          cBit7
#define cThaFephyModesel1Shift                         7
#define cThaFephyModesel1MaxVal                        0x1
#define cThaFephyModesel1MinVal                        0x0
#define cThaFephyModesel1RstVal                        0x1

/*--------------------------------------
BitField Name: Fephy_Modesel0
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyModesel0Mask                          cBit6
#define cThaFephyModesel0Shift                         6
#define cThaFephyModesel0MaxVal                        0x1
#define cThaFephyModesel0MinVal                        0x0
#define cThaFephyModesel0RstVal                        0x1

/*--------------------------------------
BitField Name: Fephy_Preasel
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyPreaselMask                           cBit5
#define cThaFephyPreaselShift                          5
#define cThaFephyPreaselMaxVal                         0x1
#define cThaFephyPreaselMinVal                         0x0
#define cThaFephyPreaselRstVal                         0x1

/*--------------------------------------
BitField Name: Fephy_Linkhold
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyLinkholdMask                          cBit4
#define cThaFephyLinkholdShift                         4
#define cThaFephyLinkholdMaxVal                        0x1
#define cThaFephyLinkholdMinVal                        0x0
#define cThaFephyLinkholdRstVal                        0x1

/*--------------------------------------
BitField Name: Fephy_Cfg3
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyCfg3Mask                              cBit3
#define cThaFephyCfg3Shift                             3
#define cThaFephyCfg3MaxVal                            0x1
#define cThaFephyCfg3MinVal                            0x0
#define cThaFephyCfg3RstVal                            0x1

/*--------------------------------------
BitField Name: Fephy_Cfg2
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyCfg2Mask                              cBit2
#define cThaFephyCfg2Shift                             2
#define cThaFephyCfg2MaxVal                            0x1
#define cThaFephyCfg2MinVal                            0x0
#define cThaFephyCfg2RstVal                            0x1

/*--------------------------------------
BitField Name: Fephy_Cfg1
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyCfg1Mask                              cBit1
#define cThaFephyCfg1Shift                             1
#define cThaFephyCfg1MaxVal                            0x1
#define cThaFephyCfg1MinVal                            0x0
#define cThaFephyCfg1RstVal                            0x1

/*--------------------------------------
BitField Name: Fephy_Pwrdwn
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFephyPwrdwnMask                            cBit0
#define cThaFephyPwrdwnShift                           0
#define cThaFephyPwrdwnMaxVal                          0x1
#define cThaFephyPwrdwnMinVal                          0x0
#define cThaFephyPwrdwnRstVal                          0x1




/*------------------------------------------------------------------------------
Reg Name: LED Latch device 3rd
Reg Addr: 0x14000083
Reg Desc: Read/Write this register to control led status.
------------------------------------------------------------------------------*/
#define cThaRegLEDLatdevice3rd                      0x14000083

#define cThaRegLEDLatdevice3rdRstVal                0x00010001
#define cThaRegLEDLatdevice3rdRwMsk                 0xFFFFFFFF
#define cThaRegLEDLatdevice3rdRwcMsk                0x00000000
/*--------------------------------------
BitField Name: Ds1e1t2led [77-00]
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaDs1e1t2ledMask                             cBit31_16
#define cThaDs1e1t2ledShift                            16
#define cThaDs1e1t2ledMaxVal                           0xFFFF
#define cThaDs1e1t2ledMinVal                           0x0
#define cThaDs1e1t2ledRstVal                           0x1

/*--------------------------------------
BitField Name: Ds1e1t1led [1515-88]
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaDs1e1t1ledMask                             cBit15_0
#define cThaDs1e1t1ledShift                            0
#define cThaDs1e1t1ledMaxVal                           0xFFFF
#define cThaDs1e1t1ledMinVal                           0x0
#define cThaDs1e1t1ledRstVal                           0x1


#define cThaDs1s1t1GreenLEDMask3(portId)               (cBit15 >> ((23 - portId)*2))
#define cThaDs1s1t1GreenLEDShift3(portId)              (15 - ((23 - portId)*2))
#define cThaDs1s1t1GreenLEDMaxVal3                     0x1
#define cThaDs1s1t1GreenLEDMinVal3                     0x0
#define cThaDs1s1t1GreenLEDRstVal3                     0x1

#define cThaDs1s1t1YellowLEDMask3(portId)              (cBit14 >> ((23 - portId)*2))
#define cThaDs1s1t1YellowLEDShift3(portId)             (14 - ((23 - portId)*2))
#define cThaDs1s1t1YellowLEDMaxVal3                    0x1
#define cThaDs1s1t1YellowLEDMinVal3                    0x0
#define cThaDs1s1t1YellowLEDRstVal3                    0x1

#define cThaDs1s1t1GreenLEDMask2(portId)               (cBit31 >> ((15 - portId)*2))
#define cThaDs1s1t1GreenLEDShift2(portId)              (31 - ((15 - portId)*2))
#define cThaDs1s1t1GreenLEDMaxVal2                     0x1
#define cThaDs1s1t1GreenLEDMinVal2                     0x0
#define cThaDs1s1t1GreenLEDRstVal2                     0x1

#define cThaDs1s1t1YellowLEDMask2(portId)              (cBit30 >> ((15 - portId)*2))
#define cThaDs1s1t1YellowLEDShift2(portId)             (30 - ((15 - portId)*2))
#define cThaDs1s1t1YellowLEDMaxVal2                    0x1
#define cThaDs1s1t1YellowLEDMinVal2                    0x0
#define cThaDs1s1t1YellowLEDRstVal2                    0x1


/*------------------------------------------------------------------------------
Reg Name: LED Latch device 4th
Reg Addr: 0x14000084
Reg Desc: Read/Write this register to control led status.
------------------------------------------------------------------------------*/
#define cThaRegLEDLatdevice4th                      0x14000084

#define cThaRegLEDLatdevice4thRstVal                0x00010000
#define cThaRegLEDLatdevice4thRwMsk                 0xFFFF0000
#define cThaRegLEDLatdevice4thRwcMsk                0x00000000
/*--------------------------------------
BitField Name: Ds1e1t2led [1515-88]
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
Ds1e1t2led in 1.4.LED Latch device 4th
 have declare in 1.3. LED Latch device 3rd
*/
#define cThaDs1e1t2ledMask                             cBit31_16
#define cThaDs1e1t2ledShift                            16
#define cThaDs1e1t2ledMaxVal                           0xFFFF
#define cThaDs1e1t2ledMinVal                           0x0
#define cThaDs1e1t2ledRstVal                           0x1

#define cThaDs1s1t1GreenLEDMask1(portId)               (cBit31 >> ((7 - portId)*2))
#define cThaDs1s1t1GreenLEDShift1(portId)              (31 - ((7 - portId)*2))
#define cThaDs1s1t1GreenLEDMaxVal1                     0x1
#define cThaDs1s1t1GreenLEDMinVal1                     0x0
#define cThaDs1s1t1GreenLEDRstVal1                     0x1

#define cThaDs1s1t1YellowLEDMask1(portId)              (cBit30 >> ((7 - portId)*2))
#define cThaDs1s1t1YellowLEDShift1(portId)             (30 - ((7 - portId)*2))
#define cThaDs1s1t1YellowLEDMaxVal1                    0x1
#define cThaDs1s1t1YellowLEDMinVal1                    0x0
#define cThaDs1s1t1YellowLEDRstVal1                    0x1

/*
#define cThaUnusedMask                                 cBit15_0
#define cThaUnusedShift                                0
#define cThaUnusedRstVal                               0x0
*/




/*------------------------------------------------------------------------------
Reg Name: LED Latch device 5th
Reg Addr: 0x14000085
Reg Desc: Read/Write this register to control led status.
------------------------------------------------------------------------------*/
#define cThaRegLEDLatdevice5th                      0x14000085

#define cThaRegLEDLatdevice5thRstVal                0x0000FF11
#define cThaRegLEDLatdevice5thRwMsk                 0x0000FFFF
#define cThaRegLEDLatdevice5thRwcMsk                0x00000000
/*
#define cThaUnusedMask                                 cBit31_16
#define cThaUnusedShift                                16
#define cThaUnusedRstVal                               0x0
*/

#define cThaGBEOptLosMask(portId)                         (cBit15 >> portId)
#define cThaGBEOptLosShift(portId)                        (15 - portId)
#define cThaGBEOptLosMaxVal                               0x1
#define cThaGBEOptLosMinVal                               0x0
#define cThaGBEOptLosRstVal                               0x1

/*--------------------------------------
BitField Name: GBE/Opt[1]_lost of signal
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */
#define cThaGBEOpt1LosMask                                 cBit15
#define cThaGBEOpt1LosShift                                15
#define cThaGBEOpt1LosMaxVal                               0x1
#define cThaGBEOpt1LosMinVal                               0x0
#define cThaGBEOpt1LosRstVal                               0x1
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */

/*--------------------------------------
BitField Name: GBE/Opt[2]_lost of signal
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
GBEOpt in 1.5.LED Latch device 5th
 have declare in 1.5. LED Latch device 5th
*/
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */
#define cThaGBEOpt2LosMask                                 cBit14
#define cThaGBEOpt2LosShift                                14
#define cThaGBEOpt2LosMaxVal                               0x1
#define cThaGBEOpt2LosMinVal                               0x0
#define cThaGBEOpt2LosRstVal                               0x1
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */

/*--------------------------------------
BitField Name: GBE/Opt[3]_lost of signal
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
GBEOpt in 1.5.LED Latch device 5th
 have declare in 1.5. LED Latch device 5th
*/
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */
#define cThaGBEOpt3LosMask                                 cBit13
#define cThaGBEOpt3LosShift                                13
#define cThaGBEOpt3LosMaxVal                               0x1
#define cThaGBEOpt3LosMinVal                               0x0
#define cThaGBEOpt3LosRstVal                               0x1
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */

/*--------------------------------------
BitField Name: GBE/Opt[4]_lost of signal
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
GBEOpt in 1.5.LED Latch device 5th
 have declare in 1.5. LED Latch device 5th
*/
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */
#define cThaGBEOpt4LosMask                                 cBit12
#define cThaGBEOpt4LosShift                                12
#define cThaGBEOpt4LosMaxVal                               0x1
#define cThaGBEOpt4LosMinVal                               0x0
#define cThaGBEOpt4LosRstVal                               0x1
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */

/*--------------------------------------
BitField Name: OC12/Opt[1]_lost of signal
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*
Special Character ',' '/' ':' '-'.....: OC12/Opt */
#define cThaOc12OptLosMask(portId)                        (cBit11 >> portId)
#define cThaOc12OptLosShift(portId)                       (11 - portId)
#define cThaOc12OptLosMaxVal                              0x1
#define cThaOc12OptLosMinVal                              0x0
#define cThaOc12OptLosRstVal                              0x1
/*
Special Character ',' '/' ':' '-'.....: OC12/Opt */

/*--------------------------------------
BitField Name: OC12/Opt[8:5]_latch/stat_ledred
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
Oc12Opt in 1.5.LED Latch device 5th
 have declare in 1.5. LED Latch device 5th
*/
/*
Special Character ',' '/' ':' '-'.....: OC12/Opt */
#define cThaOc12OptLosLedMask(portId)                        (cBit4 << portId)
#define cThaOc12OptLosLedShift(portId)                       (4 + portId)
#define cThaOc12OptLosLedMaxVal                              0x1
#define cThaOc12OptLosLedMinVal                              0x0
#define cThaOc12OptLosLedRstVal                              0x1
/*
Special Character ',' '/' ':' '-'.....: OC12/Opt */

/*--------------------------------------
BitField Name: GBE/Opt[4:1]_latch/stat_ledred
BitField Type: R_W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
GBEOpt in 1.5.LED Latch device 5th
 have declare in 1.5. LED Latch device 5th
*/
/*
Special Character ',' '/' ':' '-'.....: GBE/Opt */
#define cThaGBEOptLosLedMask(portId)                         (cBit0 << portId)
#define cThaGBEOptLosLedShift(portId)                        (portId)
#define cThaGBEOptLosLedMaxVal                               0x1
#define cThaGBEOptLosLedMinVal                               0x0
#define cThaGBEOptLosLedRstVal                               0x1


/*Str3 FPGA version */
#define cEpRegStr3FPGAVersion  0xbffffff
/*------------------------------------------------------------------------------
Reg Name: PRBS Enable for OC, GbE ports' diagnosis
Reg Addr: 0xfffff00
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegPrbsEnOcGbeDiag                       0xfffff00
#define cEpRegPrbsEnOcGbeDiagEnVal                  0xff
#define cEpRegPrbsEnOcGbeDiagDisVal                 0x0

/*------------------------------------------------------------------------------
Reg Name: OC, GbE ports' status
Reg Addr: 0xfffff03
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegOcGbeStat                             0xfffff03
#define cEpRegOcGbeStatClrVal                       0xff
#define cEpRegOcStatClrVal                          0xf0
#define cEpRegGbeStatClrVal                         0xf

/*------------------------------------------------------------------------------
Reg Name: OC, GbE ports' lost of sync status
Reg Addr: 0xfffff0f
Reg Desc:
------------------------------------------------------------------------------*/
#define cEpRegOcGbeLosyncStat                       0xfffff0f

/*Reg Name: E1 Ports Status*/
#define cEpSlotId                           1
#define cEpLiuPrbsEnId                      1
#define cEpCyclId                           1
#define cEpLiu1DevId                        2
#define cEpLiu2DevId                        3

#define cEpSingleRailVal                    0x80
#define cEpSingleRailAddr                   0x0

#define cEpE1ClkInputAddr                   0x9
#define cEpE1ClkOutputAddr                  0x4
#define cEp2MhzE1Val                        0x0
#define cEpE1SelAddr                        0x1
#define cEpE1LineCodeAddr                   0x3
#define cEpE1LoopBackMdAddr                 0x2
#define cEpE1LosDetectAddr                  0x5
#define cEpE1PrbsAddr                       0xd0
#define cEpE1DeReg                          0xde
#define cEpE1D3Reg                          0xd3
#define cEpE1D6Reg                          0xd6
#define cEpE1D7Reg                          0xd7


#define cEpE1ForceErrVal                    0x1
#define cEpE1ForceErr0Val                   0x0
#define cEpE3ForceErrVal                    0xE0
#define cEpE3ForceErr0Val                   0x00

#define cEpE1SelVal                         0xc5
#define cEpE1LineCodeHDB3Val                0x0
#define cEpE1ShortHaulRXVal                 0x3c
#define cEpE1LoopBackDigitalMdVal           0xf
#define cEpE1LoopBackAnalogMdVal            0xd
#define cEpE1LoopBackCableMdVal             0x8
#define cEpE1PrbsEnVal                      0x1
#define cEpE1PrbsDisVal                     0x0
#define cEpE3PrbsDisVal                     0x00
#define cEpE1ClrVal                         0xff /*1111.1111*/

/*Lost of signal register address for all port*/
#define cEpE1LosAddrChn0                    0x025
#define cEpE1LosAddrChn1                    0x045
#define cEpE1LosAddrChn2                    0x065
#define cEpE1LosAddrChn3                    0x085
#define cEpE1LosAddrChn4                    0x0a5
#define cEpE1LosAddrChn5                    0x0c5
#define cEpE1LosAddrChn6                    0x0e5
#define cEpE1LosAddrChn7                    0x105
#define cEpE1LosAddrChn8                    0x125
#define cEpE1LosAddrChn9                    0x145
#define cEpE1LosAddrChn10                   0x165
#define cEpE1LosAddrChn11                   0x185
#define cEpE1LosAddrChn12                   0x1a5
#define cEpE1LosAddrChn13                   0x1c5
#define cEpE1LosAddrChn14                   0x1e5
#define cEpE1LosAddrChn15                   0x205


/*Register for E3 Dianosis */
#define cEpLiu4DevId                        4
#define cEpLiuE3ConfAddr                    0x0
#define cEpLiuE3EnVal                       0x77
#define cEpLiuE3PrbsEnVal                   0xE0
#define cEpLiuE3StatClVal                   0x07

#define cEpLiuE3Port0Addr                   0x6
#define cEpLiuE3Port1Addr                   0xe
#define cEpLiuE3Port2Addr                   0x16
#define cEpLiuE3StatAddr                    0xd8

#define cEpLiuE3DigitalLoopBackMd           0x1d
#define cEpLiuE3AnalogLoopBackMd            0xd
#define cEpLiuE3NormalLoopBackMd            0x5



#ifdef __cplusplus
}
#endif
#endif /* _ATEPPDHREG_H_ */


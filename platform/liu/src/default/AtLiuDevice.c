/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : AtLiuDevice.c
 *
 * Created Date: Jul 2, 2014
 *
 * Description : LIU device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtOsal.h"
#include "AtLiuDeviceInternal.h"
#include "AtLiuManagerInternal.h"
#include "AtLiuController.h"
#include "AtLiuReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eAtLiuDe1ClkInMode
    {
    cAtLiuDe1Clk15Mx1, /* The input clock is 1.544MHz. */
    cAtLiuDe1Clk2Mx1,  /* The input clock is 2.048MHz. */
    cAtLiuDe1Clk2Mx2,  /* The input clock is 4.096MHz. */
    cAtLiuDe1Clk15Mx2, /* The input clock is 3.088MHz. */
    cAtLiuDe1Clk2Mx4,  /* The input clock is 8.192MHz. */
    cAtLiuDe1Clk15Mx4, /* The input clock is 6.176MHz. */
    cAtLiuDe1Clk2Mx8,  /* The input clock is 16.384MHz. */
    cAtLiuDe1Clk15Mx8  /* The input clock is 12.352MHz. */
    }eAtLiuDe1ClkInMode;

typedef enum eAtLiuDe1RailMode
    {
    cAtLiuDe1RailDual, /* Dual rail */
    cAtLiuDe1RailSing  /* Single rail */
    }eAtLiuDe1RailMode;

typedef enum eAtLiuDs1ClkOutMode
    {
    cAtLiuDs1Clk15Mx1, /* The output clock is 1.544MHz. */
    cAtLiuDs1Clk15Mx2, /* The output clock is 3.088MHz. */
    cAtLiuDs1Clk15Mx4, /* The output clock is 6.176MHz. */
    cAtLiuDs1Clk15Mx8  /* The output clock is 12.352MHz. */
    }eAtLiuDs1ClkOutMode;

typedef enum eAtLiuE1ClkOutMode
    {
    cAtLiuE1Clk2Mx1, /* The output clock is 2.048MHz. */
    cAtLiuE1Clk2Mx2, /* The output clock is 4.096MHz. */
    cAtLiuE1Clk2Mx4, /* The output clock is 8.192MHz. */
    cAtLiuE1Clk2Mx8  /* The output clock is 16.384MHz. */
    }eAtLiuE1ClkOutMode;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtLiuController AtLiuControllerNew(AtLiuDevice device, uint16 liuId);

/*--------------------------- Implementation ---------------------------------*/
static uint8 HwDeviceId(AtLiuDevice self)
    {
    return AtLiuManagerHwDeviceId(self->manager, self->deviceId);
    }

static eAtEpRet AllLiuControllerDelete(AtLiuDevice self)
    {
    uint32 maxNumOfliuControllers;
    uint32 liuId;
    if (self->liuControllers == NULL)
        return cAtEpRetOk;

    maxNumOfliuControllers = AtLiuDeviceMaxNumOfLiusGet(self);

    for (liuId = 0; liuId < maxNumOfliuControllers; liuId++)
        AtObjectDelete((AtObject)self->liuControllers[liuId]);

    AtOsalMemFree(self->liuControllers);
    self->liuControllers = NULL;

    return cAtEpRetOk;
    }

static AtLiuController LiuControllerObjectCreate(AtLiuDevice self, uint16 liuId)
    {
    return AtLiuControllerNew(self, liuId);
    }

static void Delete(AtObject self)
    {
    AllLiuControllerDelete((AtLiuDevice)self);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static uint32 Read(AtLiuDevice self, uint32 regAddr)
    {
    return AtLiuManagerRead(AtLiuDeviceManagerGet(self), regAddr);
    }

static void Write(AtLiuDevice self, uint32 regAddr, uint32 regVal)
    {
    AtLiuManagerWrite(AtLiuDeviceManagerGet(self), regAddr, regVal);
    }

static uint8 RailModeSwToHw(eAtLiuDe1RailMode mode)
	{
	if (mode == cAtLiuDe1RailDual) return 0;
	if (mode == cAtLiuDe1RailSing) return 1;

	return 0;
	}

static void RailModeSet(AtLiuDevice self, eAtLiuDe1RailMode mode)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl0 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlSrDr, RailModeSwToHw(mode));
    Write(self, regAddr, regVal);
    }

static void AutomaticLineEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl0 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlAtaos, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static void RxClockFallingEdgeModeEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl0 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlRclke, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static void TxClockFallingEdgeModeEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl0 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlTclke, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static void RxMuteEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl1 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlRxMute, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static void ExtendedLossOfZerosEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl1 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlExLos, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static void SystemExtendedLossOfZerosEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl2 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlSysExLos, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static void TxClockTransmitAllOnesModeEnable(AtLiuDevice self, eBool enable)
    {
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl2 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlRxTcntl, mBoolToBin(enable));
    Write(self, regAddr, regVal);
    }

static uint8 Ds1ClockModeSwToHw(eAtLiuDs1ClkOutMode mode)
	{
	if (mode == cAtLiuDs1Clk15Mx1) return 0;
	if (mode == cAtLiuDs1Clk15Mx2) return 1;
	if (mode == cAtLiuDs1Clk15Mx4) return 2;
	if (mode == cAtLiuDs1Clk15Mx8) return 3;

	return 0;
	}

static void Ds1ClockModeSet(AtLiuDevice self, eAtLiuDs1ClkOutMode mode)
	{
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl4 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlMclkT1Out, Ds1ClockModeSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 E1ClockModeSwToHw(eAtLiuE1ClkOutMode mode)
	{
	if (mode == cAtLiuE1Clk2Mx1) return 0;
	if (mode == cAtLiuE1Clk2Mx2) return 1;
	if (mode == cAtLiuE1Clk2Mx4) return 2;
	if (mode == cAtLiuE1Clk2Mx8) return 3;

	return 0;
	}

static void E1ClockModeSet(AtLiuDevice self, eAtLiuE1ClkOutMode mode)
	{
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl4 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlMclkE1Out, E1ClockModeSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static uint8 InputClockModeSwToHw(eAtLiuDe1ClkInMode mode)
	{
	if (mode == cAtLiuDe1Clk15Mx1) return 1;
	if (mode == cAtLiuDe1Clk2Mx1)  return 0;
	if (mode == cAtLiuDe1Clk2Mx2)  return 8;
	if (mode == cAtLiuDe1Clk15Mx2) return 9;
	if (mode == cAtLiuDe1Clk2Mx4)  return 0xA;
	if (mode == cAtLiuDe1Clk15Mx4) return 0xB;
	if (mode == cAtLiuDe1Clk2Mx8)  return 0xC;
	if (mode == cAtLiuDe1Clk15Mx8) return 0xD;

	return 0;
	}

static void InputClockModeSet(AtLiuDevice self, eAtLiuDe1ClkInMode mode)
	{
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrl9 + offset;
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlClkSel, InputClockModeSwToHw(mode));
    Write(self, regAddr, regVal);
	}

static void RecoverClockEnable(AtLiuDevice self, uint8 refClockPortId, eBool enable)
	{
    uint32 offset  = AtLiuDeviceDefaultOffset(self);
    uint32 regAddr = cEpRegDs1E1TrcvComCtrlRecClkSel + offset;
    uint32 regVal  = Read(self, regAddr);
	mRegFieldSet(regVal, cEpDs1E1TrcvComCtrlRclkOut, (mBoolToBin(enable) << 4) | (refClockPortId & 0xF));
    Write(self, regAddr, regVal);
	}

static void RegisterReset(AtLiuDevice self)
    {
    uint32 offset = AtLiuManagerDefaultOffset(self->manager, self->deviceId);

    Write(self, cEpRegDs1E1TrcvComCtrl0 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl1 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl2 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl3 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl4 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl5 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl6 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrl9 + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvComCtrlRecClkSel + offset, 0x0);
    Write(self, cEpRegDs1E1TrcvE1ArbiSel + offset, 0x0);
    }

static void DefaultSet(AtLiuDevice self)
	{
    RegisterReset(self);

	RailModeSet(self, cAtLiuDe1RailSing);
	AutomaticLineEnable(self, cAtFalse);
	RxClockFallingEdgeModeEnable(self, cAtFalse);
	TxClockFallingEdgeModeEnable(self, cAtFalse);
	RxMuteEnable(self, cAtFalse);
	ExtendedLossOfZerosEnable(self, cAtFalse);
	SystemExtendedLossOfZerosEnable(self, cAtFalse);
	TxClockTransmitAllOnesModeEnable(self, cAtFalse);
	Ds1ClockModeSet(self, cAtLiuDs1Clk15Mx1);
	E1ClockModeSet(self, cAtLiuE1Clk2Mx1);
	InputClockModeSet(self, cAtLiuDe1Clk2Mx1);
	RecoverClockEnable(self, 0x0, cAtFalse);
	}

static void Init(AtLiuDevice self)
	{
	uint16 liuId;
	DefaultSet(self);
	for (liuId = 0; liuId < AtLiuDeviceMaxNumOfLiusGet(self); liuId++)
		AtLiuControllerInit(AtLiuDeviceLiuControllerGet(self, liuId));
	}

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLiuDevice);
    }

static void OverrideAtObject(AtLiuDevice self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtLiuDevice self)
    {
    OverrideAtObject(self);
    }

AtLiuDevice AtLiuDeviceObjectInit(AtLiuDevice self, AtLiuManager manager, uint8 deviceId)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->manager  = manager;
    self->deviceId = deviceId;

    return self;
    }

AtLiuDevice AtLiuDeviceNew(AtLiuManager manager, uint8 deviceId)
    {
    AtLiuDevice device = AtOsalMemAlloc(ObjectSize());
    return AtLiuDeviceObjectInit(device, manager, deviceId);
    }

uint32 AtLiuDeviceDefaultOffset(AtLiuDevice self)
    {
    if (self == NULL)
        return 0;
    return AtLiuManagerDefaultOffset(AtLiuDeviceManagerGet(self), HwDeviceId(self));
    }

uint16 AtLiuDeviceMaxNumOfLiusGet(AtLiuDevice self)
    {
    if (self == NULL)
        return 0;
    return (uint16)(AtLiuManagerMaxNumOfLiusGet(AtLiuDeviceManagerGet(self)) / AtLiuManagerNumLiuDevices(AtLiuDeviceManagerGet(self)));
    }

AtLiuController AtLiuDeviceLiuControllerGet(AtLiuDevice self, uint16 liuId)
    {
    if (self == NULL)
        return NULL;
    return self->liuControllers[liuId];
    }

static eAtEpRet AllControllersCreate(AtLiuDevice self)
    {
    uint16 numLius;
    uint16 liuId;

    numLius = AtLiuDeviceMaxNumOfLiusGet(self);
    if (numLius == 0)
        return cAtEpRetOk;

    if (self->liuControllers == NULL)
        {
        uint32 memorySize = sizeof(AtLiuController) * numLius;
        self->liuControllers = AtOsalMemAlloc(memorySize);
        if (self->liuControllers)
            AtOsalMemInit(self->liuControllers, 0, memorySize);
        }

    if (self->liuControllers == NULL)
        return cAtEpRetError;

    for (liuId = 0; liuId < numLius; liuId++)
        {
        if (self->liuControllers[liuId] == NULL)
            self->liuControllers[liuId] = LiuControllerObjectCreate(self, liuId);
        }

    return cAtEpRetOk;
    }

eAtEpRet AtLiuDeviceSetUp(AtLiuDevice self)
    {
    if (self == NULL)
        return cAtEpRetErrNullPointer;

    return AllControllersCreate(self);
    }

uint32 AtLiuDeviceRead(AtLiuDevice self, uint32 regAddr)
    {
    return Read(self, regAddr);
    }

void AtLiuDeviceWrite(AtLiuDevice self, uint32 regAddr, uint32 regVal)
    {
    Write(self, regAddr, regVal);
    }

void AtLiuDeviceInit(AtLiuDevice self)
	{
	if (self)
		Init(self);
	}

uint8 AtLiuDeviceId(AtLiuDevice self)
    {
    return (uint8)(self ? self->deviceId : 0xFF);
    }

AtLiuManager AtLiuDeviceManagerGet(AtLiuDevice self)
    {
    return self->manager;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : AtLiuDeviceInternal.h
 * 
 * Created Date: Aug 15, 2014
 *
 * Description : LIU device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIUDEVICEINTERNAL_H_
#define _ATLIUDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtLiuDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLiuDevice
    {
    tAtObject super;

    /* Private data */
    uint8 deviceId;
    AtLiuManager manager;
    AtLiuController *liuControllers;
    }tAtLiuDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLiuDevice AtLiuDeviceObjectInit(AtLiuDevice self, AtLiuManager manager, uint8 deviceId);

uint32 AtLiuDeviceDefaultOffset(AtLiuDevice self);
AtLiuManager AtLiuDeviceManagerGet(AtLiuDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLIUDEVICEINTERNAL_H_ */


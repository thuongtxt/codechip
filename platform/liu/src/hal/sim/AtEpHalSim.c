/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : atspihalsim.c
 *
 * Created Date: Oct 30, 2012
 *
 * Author      : thuynt
 *
 * Description : HAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "AtHal.h"
#include "AtLiuManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cHalSimBlockNum 3

#define cHalEpRegBlock0MinAddress   0x12000000
#define cHalEpRegBlock0MaxAddress   0x1200FFFF

#define cHalEpRegBlock1MinAddress   0x13000000
#define cHalEpRegBlock1MaxAddress   0x1300FFFF

#define cHalEpRegBlock2MinAddress   0x14000000
#define cHalEpRegBlock2MaxAddress   0x1400FFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtEpHalSim *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tHalEpRegBlock
    {
    uint32 startAddr;
    uint32 endAddr;
    }tHalEpRegBlock;

typedef struct tAtEpHalSim
    {
    tAtHal super;

    /* Private data */
    uint32* pHalSimMemBlock[cHalSimBlockNum];
    }tAtEpHalSim;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *SimMemBlock(AtHal self, uint32 blockIndex)
    {
    return (blockIndex < cHalSimBlockNum) ? mThis(self)->pHalSimMemBlock[blockIndex] : NULL;
    }

static const tHalEpRegBlock *RegBlockAtIndex(uint32 blockIndex)
    {
    static const tHalEpRegBlock m_regBlock[cHalSimBlockNum] = {
                                               {cHalEpRegBlock0MinAddress, cHalEpRegBlock0MaxAddress},
                                               {cHalEpRegBlock1MinAddress, cHalEpRegBlock1MaxAddress},
                                               {cHalEpRegBlock2MinAddress, cHalEpRegBlock2MaxAddress},
                                               };
    return (blockIndex < cHalSimBlockNum) ? &(m_regBlock[blockIndex]) : NULL;
    }

static uint32 Read(AtHal self, uint32 addr)
    {
    uint32 block_i;

    for (block_i = 0; block_i < cHalSimBlockNum; block_i++)
        {
        if(addr >= RegBlockAtIndex(block_i)->startAddr && addr <= RegBlockAtIndex(block_i)->endAddr)
            return (uint32) SimMemBlock(self, block_i)[addr - RegBlockAtIndex(block_i)->startAddr];
        }

    AtPrintc(cSevCritical, "ERROR Address is out of range: 0x%X\r\n", addr);

    return 0xCAFECAFE;
    }

static void Write(AtHal self, uint32 addr, uint32 value)
    {
    uint32 block_i;

    for (block_i = 0; block_i < cHalSimBlockNum; block_i++)
        {
        if(addr >= RegBlockAtIndex(block_i)->startAddr && addr <= RegBlockAtIndex(block_i)->endAddr)
            {
            SimMemBlock(self, block_i)[addr - RegBlockAtIndex(block_i)->startAddr] = value;
            return;
            }
        }

    AtPrintc(cSevCritical, "ERROR Address is out of range: 0x%X\r\n", addr);
    }

static void FreeAllMemoryBlocks(AtHal self)
    {
    uint32 block_i;

    for (block_i = 0; block_i < cHalSimBlockNum; block_i++)
        {
        if (SimMemBlock(self, block_i) != NULL)
            AtOsalMemFree(SimMemBlock(self, block_i));
        }
    }

static void Delete(AtHal self)
    {
    FreeAllMemoryBlocks(self);
    m_AtHalMethods->Delete(self);
    }

static eBool HalInit(AtHal self)
    {
    uint32 block_i;

    for (block_i = 0; block_i < cHalSimBlockNum; block_i++)
        mThis(self)->pHalSimMemBlock[block_i] = NULL;

    for (block_i = 0; block_i < cHalSimBlockNum; block_i++)
        {
        uint32 memorySize = (uint32)((RegBlockAtIndex(block_i)->endAddr - RegBlockAtIndex(block_i)->startAddr + 1) * 4);
        mThis(self)->pHalSimMemBlock[block_i] = (uint32*)AtOsalMemAlloc(memorySize);
        if(mThis(self)->pHalSimMemBlock[block_i] == NULL)
            {
            FreeAllMemoryBlocks(self);
            return cAtFalse;
            }
        AtOsalMemInit(mThis(self)->pHalSimMemBlock[block_i], 0, memorySize);
        }

    return cAtTrue;
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, self->methods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Read        = Read;
        m_AtHalOverride.Write       = Write;
        m_AtHalOverride.DirectRead  = Read;
        m_AtHalOverride.DirectWrite = Write;
        m_AtHalOverride.Delete      = Delete;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEpHalSim);
    }

static AtHal ObjectInit(AtHal self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    if (!HalInit(self))
        return NULL;

    return self;
    }

AtHal AtEpHalSimNew(void)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    return ObjectInit(newHal);
    }

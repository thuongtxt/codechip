/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SPI
 *
 * File        : spi.c
 *
 * Created Date: Oct 23, 2012
 *
 * Description : SPI
 *
 * Notes       : 
 *
 *    DE1 R/W: read=0b1, write=0b0
 *     8-bits address        8-bits address      8-bit data
 *    __________________   __________________   __________________
 *   /                  \ /                  \ /                  \
 *  X ADDR[0]-ADDR[7]    X ADDR[8,9],RW,XXXXX X DATA[0]-DATA[7]    X
 *   \__________________/ \__________________/ \__________________/
 *
 *
 *    DE3 R/W: read=0b1, write=0b0
 *     8-bits address,rw      8-bits data
 *    __________________   __________________
 *   /                  \ /                  \
 *  X R/W, A[0]-A[5], 0  X    D[0]-D[7]       X
 *   \__________________/ \__________________/
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "spi.h"

/*--------------------------- Defines ----------------------------------------*/
#define cReadCmd  0x1
#define cWriteCmd 0x0

/*--------------------------- Macros -----------------------------------------*/
#define mAsm(asmCmd) __asm__(asmCmd)

#define mDe1AddrBuild(addr, rw) \
    (unsigned short)((((addr) & 0xFF) << 8) | (((addr) & 0x300) >> 8) | (((rw) & 0x1) << 2))

#define mDe3BuildAddr(addr, rw) \
    (unsigned char)((((addr) & 0x3F) << 1) | ((rw) & 0x1))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static int m_de1Fd = -1;
static int m_de3Fd = -1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int De1ReadWrite(int fd, int addr, int cmd, unsigned char *value)
    {
    struct spi_ioc_transfer transfer[2];
    int status;
    volatile unsigned long cmdBuf;
    volatile unsigned long txBuf0 = 0x0;

    cmdBuf = (unsigned long)((mDe1AddrBuild(addr, cmd) << 16) | (*value & 0xFF) << 8);

    memset(&transfer, 0, sizeof transfer);

    transfer[0].tx_buf    = (unsigned long) &txBuf0;
    transfer[0].len       = 1;
    transfer[0].cs_change = 0x0;

    transfer[1].tx_buf    = (unsigned long) &cmdBuf;
    transfer[1].len       = 3;
    transfer[1].rx_buf    = (unsigned long) &cmdBuf;
    transfer[1].cs_change = 0x1;

    status = ioctl(fd, SPI_IOC_MESSAGE(2), transfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return (-1);
        }

    if (cmd == cReadCmd)
        *value = (cmdBuf >> 8) & 0xFF;

    return 0;
    }

static int De3ReadWrite(int fd, int addr, int cmd, unsigned char *value)
    {
    struct spi_ioc_transfer transfer;
    int status;
    volatile unsigned short cmdBuf;

    cmdBuf = (unsigned short)((mDe3BuildAddr(addr, cmd) << 8) | (*value & 0xFF));

    memset(&transfer, 0, sizeof transfer);

    transfer.tx_buf    = (unsigned long) &cmdBuf;
    transfer.len       = 2;
    transfer.rx_buf    = (unsigned long) &cmdBuf;
    transfer.cs_change = 1;

    status = ioctl(fd, SPI_IOC_MESSAGE(1), transfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return (-1);
        }

    if (cmd == cReadCmd)
        *value = (unsigned char)(cmdBuf & 0xFF);

    return 0;
    }

int SpiDe1Read(int addr, unsigned char *value)
    {
    if (m_de1Fd < 0)
        return (-1);

    return (De1ReadWrite(m_de1Fd, addr, cReadCmd, value));
    }

int SpiDe1Write(int addr, unsigned char value)
    {
    if (m_de1Fd < 0)
        return (-1);

    return (De1ReadWrite(m_de1Fd, addr, cWriteCmd, &value));
    }

int SpiDe3Read(int addr, unsigned char *value)
    {
    if (m_de3Fd < 0)
        return (-1);
    return (De3ReadWrite(m_de3Fd, addr, cReadCmd, value));
    }

int SpiDe3Write(int addr, unsigned char value)
    {
    if (m_de3Fd < 0)
        return (-1);

    return (De3ReadWrite(m_de3Fd, addr, cWriteCmd, &value));
    }

int SpiInit(void)
    {
    m_de1Fd = open("/dev/spidev.de1", O_RDWR);
    m_de3Fd = open("/dev/spidev.de3", O_RDWR);

    if (m_de1Fd < 0 || m_de3Fd < 0)
        {
        perror("Open spi device fail \r\n");
        return (-1);
        }

    return (0);
    }

void SpiShutdown(void)
    {
    if (m_de3Fd >= 0)
        close(m_de3Fd);
    if (m_de1Fd >= 0)
        close(m_de1Fd);
    }

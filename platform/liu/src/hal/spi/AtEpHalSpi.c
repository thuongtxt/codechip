/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : atspiintf.c
 *
 * Created Date: Oct 30, 2012
 *
 * Description : SPI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHal.h"
#include "spi.h"
#include "atclib.h"
#include "AtLiuManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAsm(asmCmd) __asm__(asmCmd)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtEpHalSpi
    {
    tAtHal super;
    }tAtEpHalSpi;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;

/* Save super implementation */
static const tAtHalMethods *m_AtHalMethods = NULL;

/* To control SPI */
static uint8 m_spiInit       = 0;
static uint32 m_numInstances = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void SpiOpen(void)
    {
    if (!m_spiInit)
        SpiInit();
    }

static uint32 Read(AtHal hal, uint32 addr)
    {
    uint32 ret;
    uint8 val;

    AtUnused(hal);
    ret = (uint32)SpiDe1Read((int)addr, (unsigned char *)&val);
    mAsm("sync");

    if (ret != 0)
        {
        AtPrintc(cSevCritical, "ERROR: Failed to read  at address = %X, return value = %X\r\n", addr, ret);
        return 0xCAFECAFE;
        }

    return val;
    }

static void Write(AtHal hal, uint32 addr, uint32 value)
    {
    int ret = 0;

    AtUnused(hal);
    ret = SpiDe1Write((int)addr, (unsigned char)value);
    mAsm("sync");

    if (ret != 0)
        {
        AtPrintc(cSevCritical,"Failed to write to addr =%X value =%X\r\n", addr, ret);
        return;
        }
    }

static void SpiClose(void)
    {
    m_numInstances = m_numInstances - 1;
    if (m_numInstances == 0)
        SpiShutdown();
    }

static void Delete(AtHal self)
    {
    SpiClose();
    m_AtHalMethods->Delete(self);
    }

static void OverrideAtHal(AtHal self)
    {
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, self->methods, sizeof(m_AtHalOverride));

        m_AtHalOverride.Read        = Read;
        m_AtHalOverride.Write       = Write;
        m_AtHalOverride.DirectRead  = Read;
        m_AtHalOverride.DirectWrite = Write;
        m_AtHalOverride.Delete      = Delete;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEpHalSpi);
    }

static AtHal ObjectInit(AtHal self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtHalObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Open SPI */
    SpiOpen();

    return self;
    }

AtHal AtEpHalSpiNew(void);
AtHal AtEpHalSpiNew(void)
    {
    AtHal newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    if (ObjectInit(newHal))
        m_numInstances = m_numInstances + 1;

    return newHal;
    }

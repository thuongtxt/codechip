/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : EP
 * 
 * File        : spi.h
 * 
 * Created Date: Aug 15, 2014
 *
 * Description : SPI
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _SPI_H_
#define _SPI_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
int SpiInit(void);
void SpiShutdown(void);
int SpiDe1Read(int addr, unsigned char *value);
int SpiDe1Write(int addr, unsigned char value) ;
int SpiDe3Read(int addr, unsigned char *value);
int SpiDe3Write(int addr, unsigned char value);

#endif /* _SPI_H_ */


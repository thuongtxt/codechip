/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : AtLiuManager.h
 * 
 * Created Date: Jul 1, 2014
 *
 * Description : LIU manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIUMANAGER_H_
#define _ATLIUMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLiuManager    * AtLiuManager;
typedef struct tAtLiuController * AtLiuController;
typedef struct tAtLiuDevice     * AtLiuDevice;

typedef enum eAtLiuMode
    {
    cAtLiuE1Mode, /**< E1 mode */
    cAtLiuDs1Mode /**< DS1 mode */
    }eAtLiuMode;

typedef enum eAtEpRet
    {
    cAtEpRetOk,
    cAtEpRetFail,
    cAtEpRetPllNotLocked,
    cAtEpRetErrInvlParam,
    cAtEpRetErrModeNotSupp,
    cAtEpRetErrOutOfRangParm,
    cAtEpRetErrNullPointer,
    cAtEpRetErrDe1Fail,
    cAtEpRetErrDe3Fail,
    cAtEpRetErrAccessTimeout,
    cAtEpRetErrNoHal,
    cAtEpRetErrRsrcNoAvail,
    cAtEpRetError,
    }eAtEpRet;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete manager */
AtLiuManager AtLiuDefaultManagerNew(void);

/* Global setup */
eAtEpRet AtLiuManagerInit(AtLiuManager self);
eAtEpRet AtLiuManagerLiuModeSet(AtLiuManager self, eAtLiuMode mode);

/* HAL setup */
eAtEpRet AtLiuManagerHalSet(AtLiuManager self, AtHal hal);
AtHal AtLiuManagerHalGet(AtLiuManager self);

/* Access LIU devices */
uint32 AtLiuManagerNumLiuDevices(AtLiuManager self);
AtLiuDevice AtLiuManagerLiuDeviceGet(AtLiuManager self, uint8 deviceId);

/* Access each LIU */
uint8 AtLiuManagerMaxNumOfLiusGet(AtLiuManager self);
AtLiuController AtLiuManagerLiuControllerGet(AtLiuManager self, uint16 liuId);

/* Utils to access hardware */
uint32 AtLiuManagerRead(AtLiuManager self, uint32 addr);
void AtLiuManagerWrite(AtLiuManager self, uint32 addr, uint32 value);

/* Built-in HALs */
AtHal AtEpHalSimNew(void);
AtHal AtEpHalSpiNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATLIUMANAGER_H_ */


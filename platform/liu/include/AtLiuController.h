/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : AtLiuController.h
 * 
 * Created Date: Jul 1, 2014
 *
 * Description : LIU controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIUCONTROLLER_H_
#define _ATLIUCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtLiuManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtLiuDe1LoopMode
    {
    cAtLiuDe1DualLoop,    /**< Dual loopback */
    cAtLiuDe1AnalogLoop,  /**< Analog loopback */
    cAtLiuDe1RemoteLoop,  /**< Remote loopback */
    cAtLiuDe1DigitalLoop, /**< Digital loopback */
    cAtLiuDe1NoLoop       /**< No loopback */
    }eAtLiuDe1LoopMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLiuController AtLiuControllerNew(AtLiuDevice device, uint16 liuId);

void AtLiuControllerInit(AtLiuController self);
eAtEpRet AtLiuControllerModeSet(AtLiuController self, eAtLiuMode mode);
AtLiuDevice AtLiuControllerDeviceGet(AtLiuController self);
eAtEpRet AtLiuControllerLoopbackSet(AtLiuController self, eAtLiuDe1LoopMode mode);
eAtEpRet AtLiuControllerEnable(AtLiuController self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _ATLIUCONTROLLER_H_ */


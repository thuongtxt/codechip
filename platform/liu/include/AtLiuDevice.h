/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : AtLiuDevice.h
 * 
 * Created Date: Jul 2, 2014
 *
 * Description : LIU device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIUDEVICE_H_
#define _ATLIUDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtLiuManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLiuDevice AtLiuDeviceNew(AtLiuManager manager, uint8 deviceId);

/* Global setup */
eAtEpRet AtLiuDeviceSetUp(AtLiuDevice self);
void AtLiuDeviceInit(AtLiuDevice self);
uint8 AtLiuDeviceId(AtLiuDevice self);

/* Access LIU controllers */
uint16 AtLiuDeviceMaxNumOfLiusGet(AtLiuDevice self);
AtLiuController AtLiuDeviceLiuControllerGet(AtLiuDevice self, uint16 liuId);

/* To access hardware */
uint32 AtLiuDeviceRead(AtLiuDevice self, uint32 regAddr);
void AtLiuDeviceWrite(AtLiuDevice self, uint32 regAddr, uint32 regVal);

#ifdef __cplusplus
}
#endif
#endif /* _ATLIUDEVICE_H_ */


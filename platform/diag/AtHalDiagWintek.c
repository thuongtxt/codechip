/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DIAG
 *
 * File        : AtHalDiagWintek.c
 *
 * Created Date: May 7, 2015
 *
 * Description : This file is used to diagnose HAL of Wintek
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <stddef.h>

/*--------------------------- Define -----------------------------------------*/
/* Control registers */
#define cControl1 0xF2
#define cControl2 0xF3
#define cControl3 0xF4
#define cControl4 0xF5

/* Data registers */
#define cData1 0xF6
#define cData2 0xF7
#define cData3 0xF8
#define cData4 0xF9

/* Bit masks */
#define cBit6     0x00000040L
#define cBit7     0x00000080L
#define cBit2_0   0x00000007L
#define cBit5_0   0x0000003FL
#define cBit7_0   0x000000FFL
#define cBit15_8  0x0000FF00L
#define cBit23_16 0x00FF0000L
#define cBit31_24 0xFF000000L

#define cInvalidValue 0xDEADCAFE


/*--------------------------- Macros -----------------------------------------*/
#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

#define mRealAddress(localAddress, baseAddress) (size_t)((localAddress) + baseAddress)

#define mFieldGet( regVal , mask , shift , type, pOutVal )                     \
    do                                                                         \
        {                                                                      \
        *(pOutVal) = (type)( (uint32)( (regVal) & (mask) ) >> ((uint32)(shift)) ); \
        } while(0)

#define mFieldIns( pRegVal , mask , shift , inVal )                            \
    do                                                                         \
        {                                                                      \
        *(pRegVal) =  (uint32)( (uint32)( ( *(pRegVal) ) & ( ~( mask ) ) )       \
                        | ( ( (uint32)( inVal ) << ((uint32)(shift)) )  & ( mask ) ) \
                      );                                                       \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
/* Basic data types */
typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;

/* Request types */
typedef enum eRequestHwType
    {
    cRequestHwRead,
    cRequestHwWrite
    }eRequestHwType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_baseAddress = 0x0;
static uint32 m_retry = 20000;

/*--------------------------- Forward declarations ---------------------------*/
uint32 AtHalDiagRead(uint32 address);
void AtHalDiagWrite(uint32 address, uint32 value);
void AtHalDiagRetryTimeSet(uint32 retryTime);
void AtHalDiagBaseAddressSet(uint32 baseAddress);
void drrd(uint32 address);
void drwr(uint32 address, uint32 value);
void drd(uint32 address);
void dwr(uint32 address, uint32 value);
void showdiaginfo(void);

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return m_baseAddress;
    }

static uint32 RetryTime(void)
    {
    return m_retry;
    }

static uint16 _Read(uint32 address)
    {
    uint16 value;

    value = (uint8)(*((volatile uint8 *)mRealAddress(address, BaseAddress())));
    mAsm("sync");

    return value;
    }

static void _Write(uint32 address, volatile uint16 value)
    {
    *((volatile uint8 *)mRealAddress(address, BaseAddress())) = (uint8)(value & cBit7_0);
    mAsm("sync");
    }

static uint8 HwDone(void)
    {
    uint32 retryCount = 0;

    while (retryCount < RetryTime())
        {
        /* Hardware Done */
        if ((_Read(cControl4) & ((uint8)cBit7)) == 0)
            return 1;

        /* Retry */
        retryCount = retryCount + 1;
        }

    return 0;
    }

static uint8 Request(uint32 address, eRequestHwType requestType)
    {
    uint8 request = 0;

    /* Write address */
    _Write(cControl1,  address & cBit7_0);
    _Write(cControl2, (uint16)((address & cBit15_8)  >> 8));
    _Write(cControl3, (uint16)((address & cBit23_16) >> 16));

    /* Write request type */
    if (requestType == cRequestHwRead)
        request |= (uint8)cBit6;

    /* Start request */
    request |= (uint8)cBit7;
    _Write(cControl4, request);

    return HwDone();
    }

uint32 AtHalDiagRead(uint32 address)
    {
    /* Make a read request on this register */
    if (Request(address, cRequestHwRead))
        {
        uint32 data = 0;

        data |=  _Read(cData1);
        data |= (uint32)(_Read(cData2) << 8);
        data |= (uint32)(_Read(cData3) << 16);
        data |= (uint32)(_Read(cData4) << 24);

        return data;
        }

    /* Timeout */
    printf("ERROR: Read 0x%08x timeout\n", address);
    return cInvalidValue;
    }

void AtHalDiagWrite(uint32 address, uint32 value)
    {
    /* Write data */
    _Write(cData1, (uint8) (value & cBit7_0));
    _Write(cData2, (uint8)((value & cBit15_8)  >> 8));
    _Write(cData3, (uint8)((value & cBit23_16) >> 16));
    _Write(cData4, (uint8)((value & cBit31_24) >> 24));

    /* Make request */
    if (!Request(address, cRequestHwWrite))
        printf("ERROR: Write 0x%08x, value 0x%08x timeout\n", address, value);
    }

void AtHalDiagRetryTimeSet(uint32 retryTime)
    {
    m_retry = retryTime;
    }

void AtHalDiagBaseAddressSet(uint32 baseAddress)
    {
    m_baseAddress = baseAddress;
    }

void drrd(uint32 address)
    {
    uint32 value = _Read(address);
    printf("Address 0x%08x, value 0x%08x\r\n", address, value);
    }

void drwr(uint32 address, uint32 value)
    {
    _Write(address, (uint16)value);
    }

void drd(uint32 address)
    {
    uint32 value = AtHalDiagRead(address);
    printf("Address 0x%08x, value 0x%08x\r\n", address, value);
    }

void dwr(uint32 address, uint32 value)
    {
    AtHalDiagWrite(address, value);
    }

void showdiaginfo(void)
    {
    printf("Base Address: %d\r\n", m_baseAddress);
    printf("Retry times: %d\r\n", m_retry);
    printf("Wintek HAL information:\r\n");
    printf("* 128 E1 board physical base addresses\r\n");
    printf("    + 1st CES: 0x70040000\r\n");
    printf("    + 2nd CES: 0x70050000\r\n");
    printf("    + 3rd CES: 0x70060000\r\n");
    printf("    + 4th CES: 0x70070000\r\n");
    printf("CPU information: MPC8247 Motorola PowerQUICC II Processor\r\n");
    }

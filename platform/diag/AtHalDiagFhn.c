/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDiagFhw.c
 *
 * Created Date: Apr 08, 2013
 *
 * Description : Diagnostic HAL for FHW project. This source file is used to
 *               diagnostic hardware read/write. Usage
 *               - Set base address: AtHalDiagBaseAddressSet(baseAddress)
 *               - Read: AtHalDiagRead(address)
 *               - Write: AtHalDiagWrite(address, value)
 *
 *               Example:
 *               AtHalDiagBaseAddressSet(0x3200000)
 *               AtHalDiagRead(0x0)
 *               AtHalDiagWrite(0x40000, 0x2)
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <time.h>
#include <string.h>

/*--------------------------- Define -----------------------------------------*/
/* Indirect registers */
#define cIndirectControl1 0x2
#define cIndirectControl2 0x3
#define cIndirectData1    0x4
#define cIndirectData2    0x5
#define cInvalidReadValue 0xCAFECAFE

/* Bit fields */
#define cBit23_0       0x00FFFFFFL
#define cBit15_0       0x0000FFFFL
#define cBit15         0x00008000L
#define cBit14         0x00004000L
#define cBit23_16      0x00FF0000L
#define cBit7_0        0x000000FFL
#define cBit31_16      0xFFFF0000L

/*--------------------------- Macros -----------------------------------------*/
#define mFieldGet( regVal , mask , shift , type, pOutVal )                     \
    do                                                                         \
        {                                                                      \
        *(pOutVal) = (type)( (uint32)( (regVal) & (mask) ) >> ((uint32)(shift)) ); \
        } while(0)

#define mFieldIns( pRegVal , mask , shift , inVal )                            \
    do                                                                         \
        {                                                                      \
        *(pRegVal) =  (uint32)( (uint32)( ( *(pRegVal) ) & ( ~( mask ) ) )       \
                        | ( ( (uint32)( inVal ) << ((uint32)(shift)) )  & ( mask ) ) \
                      );                                                       \
        }while(0)

#define mTimeIntervalInUsGet(prevTime, curTime) \
  (((((curTime).sec - (prevTime).sec) * 1000000) + ((curTime).usec)) - ((prevTime).usec))

/*--------------------------- Local typedefs ---------------------------------*/
typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;

typedef struct tAtOsalCurTime
    {
    uint32 sec;  /**< Seconds */
    uint32 usec; /**< Microseconds */
    }tAtOsalCurTime;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_retry = 20000;

/*--------------------------- Forward declarations ---------------------------*/
void AtHalDiagRetryTimeSet(uint32 retryTime);
uint32 AtHalDiagRead(uint32 address);
void AtHalDiagWrite(uint32 address, uint32 value);
void rd(uint32 address);
void wr(uint32 address, uint32 value);
void drd(uint32 address);
void dwr(uint32 address, volatile uint16 value);
void MeasureRead(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void MeasureWrite(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void MeasureDirectRead(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void MeasureDirectWrite(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void msrd(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void msdrd(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void mswr(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);
void msdwr(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes);

/*--------------------------- Implementation ---------------------------------*/
static uint32 RetryTime(void)
    {
    return m_retry;
    }

static uint16 _Read(uint8 address)
    {
    extern int spi_read_stm1(uint8 addr,uint16 *buf);
    uint16 value;

    spi_read_stm1(address, &value);
    return value;
    }

static void _Write(uint8 address, volatile uint16 value)
    {
    extern int spi_write_stm1(uint8 addr,uint16 data);
    spi_write_stm1(address, value);
    }

void AtHalDiagRetryTimeSet(uint32 retryTime)
    {
    m_retry = retryTime;
    }

uint32 AtHalDiagRead(uint32 address)
    {
    uint16 regVal = 0, tmpValue;
    uint32 data = 0;
    uint32 retryCount = 0;

    /* Read a control register */
    address = address & cBit23_0;

    /* Make read request */
    _Write(cIndirectControl1, address & cBit15_0);
    regVal |= cBit15;
    regVal |= cBit14;
    mFieldGet(address, cBit23_16, 16, uint16, &tmpValue);
    mFieldIns(&regVal, cBit7_0, 0, tmpValue);
    _Write(cIndirectControl2, regVal);

    /* Wait for hardware ready */
    while (retryCount < RetryTime())
        {
        /* If hardware is ready, return data */
        regVal = _Read(cIndirectControl2);
        if ((regVal & cBit15) == 0)
            {
            regVal = _Read(cIndirectData1);
            data |= (regVal & cBit15_0);
            mFieldIns(&data, cBit31_16, 16, _Read(cIndirectData2));

            return data;
            }

        /* Or retry */
        retryCount = retryCount + 1;
        }

    /* Timeout */
    printf("ERROR: Read 0x%08x timeout\n", address);

    return cInvalidReadValue;
    }

void AtHalDiagWrite(uint32 address, uint32 value)
    {
    uint16 tmpValue;
    uint16 regValue = 0;
    uint32 retryCount = 0;

    /* _Write data */
    address = address & cBit23_0;

    _Write(cIndirectData1, (value & cBit15_0));
    mFieldGet(value, cBit31_16, 16, uint16, &tmpValue);
    _Write(cIndirectData2, tmpValue);

    /* _Write address */
    _Write(cIndirectControl1, address & cBit15_0);

    mFieldGet(address, cBit23_16, 16, uint16, &tmpValue);
    regValue = tmpValue;
    mFieldIns(&regValue, cBit15, 15, 1);
    _Write(cIndirectControl2, regValue);

    /* Check if hardware done */
    while (retryCount < RetryTime())
        {
        regValue = _Read(cIndirectControl2);
        if ((regValue & cBit15) == 0)
            return;

        /* Or retry */
        retryCount = retryCount + 1;
        }

    printf("ERROR: Write 0x%08x, value 0x%08x timeout\n", address, value);
    }

/* To type easily to read/write FPGA register */
void rd(uint32 address)
    {
    uint32 value = AtHalDiagRead(address);
    printf("Address 0x%08x, value 0x%08x\r\n", address, value);
    }

void wr(uint32 address, uint32 value)
    {
    AtHalDiagWrite(address, value);
    }

void drd(uint32 address)
    {
    uint16 value = _Read(address);
    printf("Address 0x%08x, value 0x%04x\r\n", address, value);
    }

void dwr(uint32 address, volatile uint16 value)
    {
    _Write(address, value);
    }

static void CurTimeGet(tAtOsalCurTime *currentTime)
    {
    struct timespec tm;

    if (currentTime == NULL)
        return;

    clock_gettime(CLOCK_REALTIME, &tm);
    currentTime->sec  = tm.tv_sec;
    currentTime->usec = tm.tv_nsec / 1000;
    }

static void MeasureReadHelper(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes, uint32 (*ReadFunction)(uint32 address))
    {
    uint32 address;
    uint32 repeat;
    uint32 elapsedTime_us = 0;
    uint32 numRegs;
    tAtOsalCurTime curTime, startTime;

    memset(&curTime, 0, sizeof(tAtOsalCurTime));
    memset(&startTime, 0, sizeof(tAtOsalCurTime));

    if (toAddr < fromAddr)
        {
        printf("Error: toAddr is less than fromAddr\r\n");
        return;
        }

    numRegs = (toAddr - fromAddr + 1) * repeatTimes;;
    CurTimeGet(&startTime);
    for (repeat = 0; repeat < repeatTimes; repeat++)
        {
        for (address = fromAddr; address <= toAddr; address++)
            ReadFunction(address);
        }

    /* Average elapsed microsecond for reading one address */
    CurTimeGet(&curTime);
    elapsedTime_us = mTimeIntervalInUsGet(startTime, curTime) / numRegs;
    printf("Read: %d microseconds\r\n", elapsedTime_us);
    }

static void MeasureWriteHelper(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes, void (WriteFunction)(uint32 address, uint32 value))
    {
    uint32 address;
    uint32 repeat;
    tAtOsalCurTime curTime, startTime;
    uint32 numRegs;
    uint32 elapsedTime_us;

    memset(&curTime, 0, sizeof(tAtOsalCurTime));
    memset(&startTime, 0, sizeof(tAtOsalCurTime));

    if (toAddr < fromAddr)
        {
        printf("Error: toAddr is less than fromAddr\r\n");
        return;
        }

    numRegs = (toAddr - fromAddr + 1) * repeatTimes;;
    CurTimeGet(&startTime);
    for (repeat = 0; repeat < repeatTimes; repeat++)
        {
        for (address = fromAddr; address <= toAddr; address++)
            WriteFunction(address, 0xABCD);
        }

    /* Average elapsed microsecond for reading one address */
    CurTimeGet(&curTime);
    elapsedTime_us = mTimeIntervalInUsGet(startTime, curTime) / numRegs;
    printf("Write: %d microseconds\n", elapsedTime_us);
    }

void MeasureRead(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureReadHelper(fromAddr, toAddr, repeatTimes, AtHalDirectRead);
    }

void MeasureWrite(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureWriteHelper(fromAddr, toAddr, repeatTimes, AtHalDiagWrite);
    }

void MeasureDirectRead(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureReadHelper(fromAddr, toAddr, repeatTimes, _Read);
    }

void MeasureDirectWrite(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureWriteHelper(fromAddr, toAddr, repeatTimes, _Write);
    }

void msrd(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureRead(fromAddr, toAddr, repeatTimes);
    }

void msdrd(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureDirectRead(fromAddr, toAddr, repeatTimes);
    }

void mswr(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureWrite(fromAddr, toAddr, repeatTimes);
    }

void msdwr(uint32 fromAddr, uint32 toAddr, uint32 repeatTimes)
    {
    MeasureDirectWrite(fromAddr, toAddr, repeatTimes);
    }

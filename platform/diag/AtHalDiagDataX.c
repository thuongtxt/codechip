/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDiagDataX.c
 *
 * Created Date: Dec 09, 2013
 *
 * Description : Diagnostic HAL for DataX project. This source file is used to
 *               diagnostic hardware read/write.
 *
 *               There are two basic functions to read/write FPGA registers:
 *               - AtHalDiagRead
 *               - AtHalDiagWrite
 *
 *               Because there are two board types, one is 4xE1s and one is
 *               16xE1s, SPI read/write operations are different on them. This
 *               source file works on 4xE1s board as default, to change to 16xE1s,
 *               the function AtHalDiagBoardTypeSet() is used. For example:
 *
 *               AtHalDiagBoardTypeSet(cBoardType16E1s)
 *
 *               and the following calling will change board type to 4xE1s
 *               AtHalDiagBoardTypeSet(cBoardType4E1s)
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>

/*--------------------------- Define -----------------------------------------*/
/* Bit masks */
#define cBit6     0x40
#define cBit7     0x80
#define cBit2_0   0x07
#define cBit5_0   0x3F
#define cBit7_0   0xFF
#define cBit14    0x00004000L
#define cBit15    0x00008000L
#define cBit15_8  0x0000FF00L
#define cBit15_0  0x0000FFFFL
#define cBit23_16 0x00FF0000L
#define cBit31_24 0xFF000000L
#define cBit31_16 0xFFFF0000L

#define cInvalidValue 0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/
#define cRegControl1 0xF0
#define cRegControl2 0xF1
#define cRegControl3 0xF2
#define cRegControl4 0xF3

#define cRegData1_16E1s 0xF2
#define cRegData1_4E1s  0xF4
#define cRegData2_16E1s 0xF3
#define cRegData2_4E1s  0xF5

#define cRegData3    0xF6
#define cRegData4    0xF7

/*--------------------------- Local typedefs ---------------------------------*/
/* Basic data types */
typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;

typedef enum eBoardType
    {
    cBoardType4E1s,
    cBoardType16E1s
    }eBoardType;

/* Request types */
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/* To work on different board types */
typedef uint8 (*RequestFunc)(uint32 address, eRequestType requestType);
typedef uint32 (*ReadDataFunc)(void);
typedef void (*WriteDataFunc)(uint32 data);
typedef void (*SpiWriteFunc)(uint16 address, uint16 data);
typedef uint16 (*SpiReadFunc)(uint16 address);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_retry = 20000; /* Not too long like this, it is just for the worst case */
static uint32 m_maxRetryTimes = 0;

/* To work on different board types */
static RequestFunc m_RequestFunc     = NULL;
static ReadDataFunc m_ReadDataFunc   = NULL;
static WriteDataFunc m_WriteDataFunc = NULL;
static SpiWriteFunc m_SpiWriteFunc   = NULL;
static SpiReadFunc m_SpiReadFunc     = NULL;

/*--------------------------- Forward declarations ---------------------------*/
void AtHalDiagRetryTimeSet(uint32 retryTime);
uint32 AtHalDiagRead(uint32 address);
void AtHalDiagWrite(uint32 address, uint32 value);
void ard(uint32 address);
void awr(uint32 address, uint32 value);
void AtHalDiagShow(void);
void AtHalDiagBoardTypeSet(eBoardType boardType);

extern void bsp_SpiWrite(unsigned int Addr, unsigned char Data);
extern unsigned char bsp_SpiRead(unsigned int Addr);
extern void spi_write(unsigned short Addr, unsigned short Data);
extern unsigned short spi_read(unsigned short Addr);

/*--------------------------- Implementation ---------------------------------*/
static uint32 RetryTime(void)
    {
    return m_retry;
    }

static void SpiWrite_8Bits(uint16 address, uint16 data)
    {
    bsp_SpiWrite(address, (unsigned char)data);
    }

static uint16 SpiRead_8Bits(uint16 address)
    {
    return bsp_SpiRead(address);
    }

static void SpiWrite_16Bits(uint16 address, uint16 data)
    {
    spi_write(address, data);
    }

static uint16 SpiRead_16Bits(uint16 address)
    {
    return spi_read(address);
    }

static uint16 SpiRead(uint16 address)
    {
    if (m_SpiReadFunc == NULL)
        m_SpiReadFunc = SpiRead_8Bits;
    return m_SpiReadFunc(address);
    }

static void SpiWrite(uint16 address, uint16 data)
    {
    if (m_SpiWriteFunc == NULL)
        m_SpiWriteFunc = SpiWrite_8Bits;
    m_SpiWriteFunc(address, data);
    }

static void DirectWrite(uint16 address, uint16 value)
    {
    SpiWrite(address, value);
    }

static uint16 DirectRead(uint16 address)
    {
    return SpiRead(address);
    }

static uint8 HwDone(uint8 controlReg, uint16 readyBitMask)
    {
    uint32 retryCount = 0;

    while (retryCount < RetryTime())
        {
        /* Done */
        if ((DirectRead(controlReg) & readyBitMask) == 0)
            {
            if (retryCount > m_maxRetryTimes)
                m_maxRetryTimes = retryCount;

            return 1;
            }

        /* Retry */
        retryCount = retryCount + 1;
        }

    return 0;
    }

static uint8 Request_8Bits(uint32 address, eRequestType requestType)
    {
    uint8 request = 0;

    /* Write address */
    DirectWrite(cRegControl1,  address & cBit7_0);
    DirectWrite(cRegControl2, (uint16)((address & cBit15_8)  >> 8));
    DirectWrite(cRegControl3, (uint16)((address & cBit23_16) >> 16));

    /* Write request type */
    if (requestType == cRequestRead)
        request |= (uint8)cBit6;

    /* Start request */
    request |= (uint8)cBit7;
    DirectWrite(cRegControl4, request);

    return HwDone(cRegControl4, cBit7);
    }

static uint32 ReadData_8Bits(void)
    {
    uint32 data = 0;

    data = (uint32)(data |  DirectRead(cRegData1_4E1s));
    data = (uint32)(data | (uint32)(DirectRead(cRegData2_4E1s) << 8));
    data = (uint32)(data | (uint32)(DirectRead(cRegData3) << 16));
    data = (uint32)(data | (uint32)(DirectRead(cRegData4) << 24));

    return data;
    }

static void WriteData_8Bits(uint32 value)
    {
    DirectWrite(cRegData1_4E1s, (uint8) (value & cBit7_0));
    DirectWrite(cRegData2_4E1s, (uint8)((value & cBit15_8)  >> 8));
    DirectWrite(cRegData3, (uint8)((value & cBit23_16) >> 16));
    DirectWrite(cRegData4, (uint8)((value & cBit31_24) >> 24));
    }

static uint8 Request_16Bits(uint32 address, eRequestType requestType)
    {
    uint16 request = 0;

    DirectWrite(cRegControl1,  address & cBit15_0);

    request = (uint16)((address & cBit23_16) >> 16);
    if (requestType == cRequestRead)
        request |= (uint16)cBit14;

    /* Start request */
    if (requestType == cRequestRead)
        request |= (uint16)cBit15;
    DirectWrite(cRegControl2, request);

    return HwDone(cRegControl2, cBit15);
    }

static uint32 ReadData_16Bits(void)
    {
    uint32 data = 0;

    data = data |  DirectRead(cRegData1_16E1s);
    data = (uint32)(data | (uint32)(DirectRead(cRegData2_16E1s) << 16));

    return data;
    }

static void WriteData_16Bits(uint32 value)
    {
    DirectWrite(cRegData1_16E1s, (value & cBit15_0));
    DirectWrite(cRegData2_16E1s, (uint16)((value & cBit31_16) >> 16));
    }

static uint8 Request(uint32 address, eRequestType requestType)
    {
    if (m_RequestFunc == NULL)
        m_RequestFunc = Request_8Bits;
    return m_RequestFunc(address, requestType);
    }

static uint32 ReadData(void)
    {
    if (m_ReadDataFunc == NULL)
        m_ReadDataFunc = ReadData_8Bits;
    return m_ReadDataFunc();
    }

static void WriteData(uint32 value)
    {
    if (m_WriteDataFunc == NULL)
        m_WriteDataFunc = WriteData_8Bits;
    m_WriteDataFunc(value);
    }

void AtHalDiagRetryTimeSet(uint32 retryTime)
    {
    m_retry = retryTime;
    }

void AtHalDiagBoardTypeSet(eBoardType boardType)
    {
    if (boardType == cBoardType4E1s)
        {
        m_RequestFunc   = Request_8Bits;
        m_ReadDataFunc  = ReadData_8Bits;
        m_WriteDataFunc = WriteData_8Bits;
        m_SpiWriteFunc  = SpiWrite_8Bits;
        m_SpiReadFunc   = SpiRead_8Bits;
        }
    else
        {
        m_RequestFunc   = Request_16Bits;
        m_ReadDataFunc  = ReadData_16Bits;
        m_WriteDataFunc = WriteData_16Bits;
        m_SpiWriteFunc  = SpiWrite_16Bits;
        m_SpiReadFunc   = SpiRead_16Bits;
        }
    }

uint32 AtHalDiagRead(uint32 address)
    {
    if (Request(address, cRequestRead))
        return ReadData();

    /* Timeout */
    printf("ERROR: Read 0x%08x timeout\n", address);
    return cInvalidValue;
    }

void AtHalDiagWrite(uint32 address, uint32 value)
    {
    WriteData(value);
    if (!Request(address, cRequestWrite))
        printf("ERROR: Write 0x%08x, value 0x%08x timeout\n", address, value);
    }

/* To type easily to read/write FPGA register */
void ard(uint32 address)
    {
    uint32 value = AtHalDiagRead(address);
    printf("Address 0x%08x, value 0x%08x\r\n", address, value);
    }

void awr(uint32 address, uint32 value)
    {
    AtHalDiagWrite(address, value);
    }

void AtHalDiagShow(void)
    {
    printf("Board type: %s\r\n", (m_SpiReadFunc == SpiRead_16Bits) ? "16xE1s" : "4xE1s");
    printf("Max retry times: %d\r\n", m_maxRetryTimes);
    m_maxRetryTimes = 0;
    }

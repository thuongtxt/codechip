/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDiagBdcom.c
 *
 * Created Date: Dec 09, 2013
 *
 * Description : Diagnostic HAL for BDCOM project. This source file is used to
 *               diagnostic hardware read/write.
 *               There are two basic functions to read/write FPGA registers:
 *               - AtHalDiagRead
 *               - AtHalDiagWrite
 *
 *               To change slot ID and page, the function AtHalDiagSlotAndPageSet
 *               is used. Slot 1 and page 0 are set as default.
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>

/*--------------------------- Define -----------------------------------------*/
/* Control registers */
#define cControl1 0xF0
#define cControl2 0xF1
#define cControl3 0xF2
#define cControl4 0xF3

/* Data registers */
#define cData1 0xF4
#define cData2 0xF5
#define cData3 0xF6
#define cData4 0xF7

/* Bit masks */
#define cBit6     0x00000040L
#define cBit7     0x00000080L
#define cBit2_0   0x00000007L

#define cBit7_0   0x000000FFL
#define cBit15_8  0x0000FF00L
#define cBit23_16 0x00FF0000L
#define cBit31_24 0xFF000000L

#define cInvalidValue 0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
/* Basic data types */
typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;

/* Request types */
typedef enum eRequestType
    {
    cRequestRead,
    cRequestWrite
    }eRequestType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_retry  = 20000;

/* Default slot and page */
static uint8  m_slotId = 1;
static uint8  m_page   = 0;

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 fpgaex_read_fpga(int slot, uint16 reg);
extern int fpgaex_write_fpga(int slot, uint16 reg, uint8 value);

void AtHalDiagRetryTimeSet(uint32 retryTime);
uint32 AtHalDiagRead(uint32 address);
void AtHalDiagWrite(uint32 address, uint32 value);
void AtHalDiagSlotAndPageSet(uint8 slot, uint8 page);
void ard(uint32 address);
void awr(uint32 address, uint32 value);

/*--------------------------- Implementation ---------------------------------*/
static uint32 RetryTime(void)
    {
    return m_retry;
    }

static uint16 MakeRegOnPage(uint8 address, uint8 page)
    {
    uint16 reg = 0;

    reg = (uint16)(reg | address);
    reg = (uint16)(reg | (page & cBit2_0) << 10);

    return reg;
    }

static void DirectWrite(uint8 address, uint8 value)
    {
    fpgaex_write_fpga(m_slotId, MakeRegOnPage(address, m_page), value);
    }

static uint8 DirectRead(uint8 address)
    {
    return fpgaex_read_fpga(m_slotId, MakeRegOnPage(address, m_page));
    }

static uint8 HwDone(void)
    {
    uint32 retryCount = 0;

    while (retryCount < RetryTime())
        {
        /* Done */
        if ((DirectRead(cControl4) & ((uint8)cBit7)) == 0)
            return 1;

        /* Retry */
        retryCount = retryCount + 1;
        }

    return 0;
    }

static uint8 Request(uint32 address, eRequestType requestType)
    {
    uint8 request = 0;

    /* Write address */
    DirectWrite(cControl1,  address & cBit7_0);
    DirectWrite(cControl2, (uint8)((address & cBit15_8)  >> 8));
    DirectWrite(cControl3, (uint8)((address & cBit23_16) >> 16));

    /* Write request type */
    if (requestType == cRequestRead)
        request |= (uint8)cBit6;

    /* Start request */
    request |= (uint8)cBit7;
    DirectWrite(cControl4, request);

    return HwDone();
    }

void AtHalDiagRetryTimeSet(uint32 retryTime)
    {
    m_retry = retryTime;
    }

void AtHalDiagSlotAndPageSet(uint8 slot, uint8 page)
    {
    m_slotId = slot;
    m_page   = page;
    }

uint32 AtHalDiagRead(uint32 address)
    {
    /* Make a read request on this register */
    if (Request(address, cRequestRead))
        {
        uint32 data = 0;

        data =  DirectRead(cData4); data <<= 8;
        data |= DirectRead(cData3); data <<= 8;
        data |= DirectRead(cData2); data <<= 8;
        data |= DirectRead(cData1);

        return data;
        }

    /* Timeout */
    printf("ERROR: Read 0x%08x timeout\n", address);
    return cInvalidValue;
    }

void AtHalDiagWrite(uint32 address, uint32 value)
    {
    /* Write data */
    DirectWrite(cData1, (uint8) (value & cBit7_0));
    DirectWrite(cData2, (uint8)((value & cBit15_8)  >> 8));
    DirectWrite(cData3, (uint8)((value & cBit23_16) >> 16));
    DirectWrite(cData4, (uint8)((value & cBit31_24) >> 24));

    /* Make request */
    if (!Request(address, cRequestWrite))
        printf("ERROR: Write 0x%08x, value 0x%08x timeout\n", address, value);
    }

/* To type easily to read/write FPGA register */
void ard(uint32 address)
    {
    uint32 value = AtHalDiagRead(address);
    printf("Address 0x%08x, value 0x%08x\r\n", address, value);
    }

void awr(uint32 address, uint32 value)
    {
    AtHalDiagWrite(address, value);
    }

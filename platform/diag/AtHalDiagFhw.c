/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDiagFhw.c
 *
 * Created Date: Apr 08, 2013
 *
 * Description : Diagnostic HAL for FHW project. This source file is used to
 *               diagnostic hardware read/write. Usage
 *               - Set base address: AtHalDiagBaseAddressSet(baseAddress)
 *               - Read: AtHalDiagRead(address)
 *               - Write: AtHalDiagWrite(address, value)
 *
 *               Example:
 *               AtHalDiagBaseAddressSet(0x3200000)
 *               AtHalDiagRead(0x0)
 *               AtHalDiagWrite(0x40000, 0x2)
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <stddef.h>

/*--------------------------- Define -----------------------------------------*/
/* Indirect registers */
#define cIndirectControl1 0x702
#define cIndirectControl2 0x703
#define cIndirectData1    0x704
#define cIndirectData2    0x705
#define cInvalidReadValue 0xCAFECAFE

/* Bit fields */
#define cBit23_0       0x00FFFFFFL
#define cBit15_0       0x0000FFFFL
#define cBit15         0x00008000L
#define cBit14         0x00004000L
#define cBit23_16      0x00FF0000L
#define cBit7_0        0x000000FFL
#define cBit31_16      0xFFFF0000L

/*--------------------------- Macros -----------------------------------------*/
#define mRealAddress(localAddress, baseAddress) ((size_t)(((localAddress) << 1) + baseAddress))

#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

#define mFieldGet( regVal , mask , shift , type, pOutVal )                     \
    do                                                                         \
        {                                                                      \
        *(pOutVal) = (type)( (uint32)( (regVal) & (mask) ) >> ((uint32)(shift)) ); \
        } while(0)

#define mFieldIns(pRegVal, mask, shift, inVal) AtRegFieldIns(pRegVal, (uint32)(mask), (uint32)(shift), (uint32)(inVal))

/*--------------------------- Local typedefs ---------------------------------*/
typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_baseAddress = 0x70000000;
static uint32 m_retry = 20000;

/*--------------------------- Forward declarations ---------------------------*/
void AtHalDiagRetryTimeSet(uint32 retryTime);
void AtHalDiagBaseAddressSet(uint32 baseAddress);
uint32 AtHalDiagRead(uint32 address);
void AtHalDiagWrite(uint32 address, uint32 value);
void rd(uint32 address);
void wr(uint32 address, uint32 value);
void drd(uint32 address);
void dwr(uint32 address, volatile uint16 value);

/*--------------------------- Implementation ---------------------------------*/
static void AtRegFieldIns(uint32 *pRegVal, uint32 mask, uint32 shift, uint32 value)
    {
    *pRegVal = ((*pRegVal & ~mask) | ((value << shift) & mask));
    }

static uint8 IsDirectRegister(uint32 address)
    {
    return ((address >= 0xFFF00) && (address <= 0xFFFFF));
    }

static uint32 BaseAddress(void)
    {
    return m_baseAddress;
    }

static uint32 RetryTime(void)
    {
    return m_retry;
    }

static uint16 _Read(uint32 address)
    {
    uint16 value;

    value = (uint16)(*((volatile uint16 *)mRealAddress(address, BaseAddress())));
    mAsm("sync");

    return value;
    }

static void _Write(uint32 address, volatile uint16 value)
    {
    *((volatile uint16 *)mRealAddress(address, BaseAddress())) = value;
    mAsm("sync");
    }

void AtHalDiagRetryTimeSet(uint32 retryTime)
    {
    m_retry = retryTime;
    }

void AtHalDiagBaseAddressSet(uint32 baseAddress)
    {
    m_baseAddress = baseAddress;
    }

uint32 AtHalDiagRead(uint32 address)
    {
    uint32 regVal = 0, tmpValue;
    uint32 data = 0;
    uint32 retryCount = 0;

    /* Read a control register */
    address = address & cBit23_0;
    if (IsDirectRegister(address))
        return _Read(address);

    /* Make read request */
    _Write(cIndirectControl1, address & cBit15_0);
    regVal |= cBit15;
    regVal |= cBit14;
    mFieldGet(address, cBit23_16, 16, uint16, &tmpValue);
    mFieldIns(&regVal, cBit7_0, 0, tmpValue);
    _Write(cIndirectControl2, (uint16)regVal);

    /* Wait for hardware ready */
    while (retryCount < RetryTime())
        {
        /* If hardware is ready, return data */
        regVal = _Read(cIndirectControl2);
        if ((regVal & cBit15) == 0)
            {
            regVal = _Read(cIndirectData1);
            data |= (regVal & cBit15_0);
            mFieldIns(&data, cBit31_16, 16, _Read(cIndirectData2));

            return data;
            }

        /* Or retry */
        retryCount = retryCount + 1;
        }

    /* Timeout */
    printf("ERROR: Read 0x%08x timeout\n", address);

    return cInvalidReadValue;
    }

void AtHalDiagWrite(uint32 address, uint32 value)
    {
    uint16 tmpValue;
    uint32 regValue = 0;
    uint32 retryCount = 0;

    /* _Write data */
    address = address & cBit23_0;
    if (IsDirectRegister(address))
        {
        _Write(address, (uint16)value);
        return;
        }

    _Write(cIndirectData1, (value & cBit15_0));
    mFieldGet(value, cBit31_16, 16, uint16, &tmpValue);
    _Write(cIndirectData2, tmpValue);

    /* _Write address */
    _Write(cIndirectControl1, address & cBit15_0);

    mFieldGet(address, cBit23_16, 16, uint16, &tmpValue);
    regValue = tmpValue;
    mFieldIns(&regValue, cBit15, 15, 1);
    _Write(cIndirectControl2, (uint16)regValue);

    /* Check if hardware done */
    while (retryCount < RetryTime())
        {
        regValue = _Read(cIndirectControl2);
        if ((regValue & cBit15) == 0)
            return;

        /* Or retry */
        retryCount = retryCount + 1;
        }

    printf("ERROR: Write 0x%08x, value 0x%08x timeout\n", address, value);
    }

/* To type easily to read/write FPGA register */
void rd(uint32 address)
    {
    uint32 value = AtHalDiagRead(address);
    printf("Address 0x%08x, value 0x%08x\r\n", address, value);
    }

void wr(uint32 address, uint32 value)
    {
    AtHalDiagWrite(address, value);
    }

void drd(uint32 address)
    {
    uint16 value = _Read(address);
    printf("Address 0x%08x, value 0x%04x\r\n", address, value);
    }

void dwr(uint32 address, volatile uint16 value)
    {
    _Write(address, value);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies. 
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : DEVICE CONTROL
 *
 * File        : devctrl.h
 *
 * Created Date: 05-Jul-06
 *
 * Description : This file contains all definition of DEVICE CONTROL component, 
 *				 which is used to control character device 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/

#ifndef DEV_CTRL
#define DEV_CTRL

/*--------------------------- Include files ----------------------------------*/ 
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/ 

#define cCmdMsgBufLen		500	/* Size of buffer for transfer data between kernel and user space. */

/*--------------------------- Typedefs ---------------------------------------*/ 
typedef struct tDevDescriptor
	{
	int			  fd;
	unsigned long phyAddr;		/* The physical addresses on CPU bus */
	unsigned long size;
	void		  *pVirtualAddr;		/* The virtual addresses are retuner by mmap */
	unsigned int  majorNum;
	} tDevDescriptor;

/*--------------------------- Entries ----------------------------------------*/ 

#ifdef __KERNEL__

/*-----------------------------------------------------------------------------
This function is used to register the character device
------------------------------------------------------------------------------*/
eBool DevRegister(const char* devName, 
				  unsigned int majorNum, 
				  eBool (*pUsrToKer)(char *, int len, int id), 
			      eBool (*pKerToUsr)(char *, int *len, int id));
/*-----------------------------------------------------------------------------
This function is used to unregister the character device
------------------------------------------------------------------------------*/
eBool DevUnregister(const char* devName, 
				   unsigned int majorNum);

#else /* USER SPACE */
/*-----------------------------------------------------------------------------
This function is used to open character device
------------------------------------------------------------------------------*/
eBool DevOpen(const char* devName, 
			  unsigned long phyAddr, 
			  unsigned long devMemSize, 
			  unsigned int  majorNum,
			  tDevDescriptor *pDevDescr);

/*-----------------------------------------------------------------------------
This function is used to close character device
------------------------------------------------------------------------------*/
eBool DevClose(tDevDescriptor *pDevDescr);

/*-----------------------------------------------------------------------------
This function is used to set the command for character device
------------------------------------------------------------------------------*/
eBool DevCtrlSet(tDevDescriptor *pDevDescr, 
				 char *pDataBuf, 
				 int len, 
				 int cmdId);

/*-----------------------------------------------------------------------------
This function is used to get the command for character device
------------------------------------------------------------------------------*/
eBool DevCtrlGet(tDevDescriptor *pDevDescr, 
				 char *pDataBuf, 
				 int *len, 
				 int cmdId);

/*-----------------------------------------------------------------------------
This function is used to get command from or set command to character device
------------------------------------------------------------------------------*/
eBool DevCtrlSetGet(tDevDescriptor *pDevDescr, 
					char *pDataBuf, 
					int *len, 
					int cmdId);

#endif /* USER SPACE */

#endif /* DEV_CTRL */

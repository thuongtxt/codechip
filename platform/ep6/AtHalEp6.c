/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalEp6.c
 *
 * Created Date: Dec 03, 2012
 *
 * Description : Implementation of HAL on EP6 board
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../hal/AtHalDefault.h"
#include "AtOsal.h"
#include "devctrl.h"

/*--------------------------- Define -----------------------------------------*/
#define cHalEpPhysicalAddr      0x80000000      /* Physical address on CPU bus */
#define cHalEpMemMapSize        0x59000000      /* 1408M */
#define cHalEpDevName           "ep"
#define cHalEpDevMajorNum       80              /* Major number of EP */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHalMethods m_AtHalOverride;
static const tAtHalMethods *m_AtHalMethods = NULL;

static uint32 baseAddress = 0;
static tDevDescriptor epDevDescr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static atbool MemMap(void)
    {
    atbool ret;

    ret = cAtTrue;

    if (DevOpen(cHalEpDevName, cHalEpPhysicalAddr, cHalEpMemMapSize, cHalEpDevMajorNum, &epDevDescr) == 0)
        {
        ret = cAtFalse;
        }
     else
        {
        baseAddress = (unsigned long)epDevDescr.pVirtualAddr;
        }

    return ret;
    }

static atbool MemUnmap(void)
    {
    return DevClose(&epDevDescr);
    }

static void Delete(AtHal self)
    {
    AtOsal osal;

    if (self != NULL)
        {
        MemUnmap();
        }

    osal = AtOsalSharedGet();
    osal->methods->MemFree(osal, self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalDefault);
    }

static uint32 DirectOamOffsetGet(AtHal self)
    {
	AtUnused(self);
    return 0xD00000;
    }

static void OverrideAtHal(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_AtHalMethods = self->methods;
        AtOsalMemCpy(&m_AtHalOverride, m_AtHalMethods, sizeof(tAtHalMethods));

        m_AtHalOverride.Delete             = Delete;
        m_AtHalOverride.Read               = self->methods->Read;
        m_AtHalOverride.Write              = self->methods->Write;
        m_AtHalOverride.DirectOamOffsetGet = DirectOamOffsetGet;
        }

    self->methods = &m_AtHalOverride;
    }

static void Override(AtHal self)
    {
    OverrideAtHal(self);
    }

/**
 * Initialize HAL EP6
 * @param self
 * @return
 */
static AtHal ObjectInit(AtHal self)
    {
    if (self != NULL)
        {
        /* Construct it */
        AtHalDefaultObjectInit(self, baseAddress);

        /* Override */
        Override(self);
        m_methodsInit = 1;
        }

    return self;
    }

/*
 * Create new EP6 HAL
 *
 * @return EP6 HAL
 */
AtHal AtHalEp6New(void)
    {
	AtHal newHal;
	
    if (!MemMap())
        return NULL;

    /* Allocate memory */
    newHal = AtOsalMemAlloc(ObjectSize());
    if (newHal == NULL)
        return NULL;

    return ObjectInit(newHal);
    }


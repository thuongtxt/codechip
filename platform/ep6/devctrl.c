/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEVICE CONTROL
 *
 * File        : devctrl.c
 *
 * Created Date: 05-Jul-06
 *
 * Description : This file contains source code for controlling character device 
 * 
 * Notes       : If we use this component for single task system -> no problem. 
                 In case we use this component for multi-task system, we must protect 
                 concurrent access to this component by using semaphore, and this semaphore 
                 must be outside of the this component codes (this semaphore must NOT be 
                 in ukcom.c). If we put semaphore in the source code of UKCOM, 
                 it still works, but error will happen.
 *----------------------------------------------------------------------------*/


/*--------------------------- File description -------------------------------
 *
 *
 *----------------------------------------------------------------------------*/ 

/*--------------------------- Include files ----------------------------------*/ 



#ifdef __KERNEL__

#include <linux/module.h>		/* Specifically, a module    */
#include <linux/version.h>
#include <linux/kernel.h>		/* We're doing kernel work   */
#include <linux/fs.h>
#include <asm/uaccess.h>  		/* for get_user and put_user */
#include <asm/io.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include "devctrl.h"

#else

#include "stdio.h"				/* sprintf() */
#include <sys/types.h>			/* open()    */
#include <sys/stat.h>			/* open()    */
#include <fcntl.h>				/* open()    */
#include <unistd.h>				/* close()   */
#include <sys/ioctl.h>			/* ioctl()   */
#include <stdlib.h>				/* system()  */
#include <sys/mman.h>			/* mmap() */
#include "devctrl.h"

#endif

/*--------------------------- Define -----------------------------------------*/ 
#define cSuccess				0	/* Must be zero, other values don't work */

#define IOCTL_SET_DATA(major)			_IOR(major,  0, char *)
#define IOCTL_SET_LEN(major)			_IOR(major,  1, int)
#define IOCTL_SEND_SET_ID(major)		_IOR(major,  2, int)

#define IOCTL_GET_DATA(major)			_IOR(major,  3, char *)
#define IOCTL_GET_LEN(major)			_IOWR(major, 4, int)
#define IOCTL_SEND_GET_ID(major)		_IOR(major,  5, int)

#define IOCTL_SETGET_DATA(major)		_IOR(major,  6, char *)
#define IOCTL_SET_SETGET_LEN(major)		_IOR(major,  7, int)
#define IOCTL_GET_SETGET_LEN(major)		_IOWR(major, 8, int)
#define IOCTL_SEND_SETGET_ID(major)		_IOR(major,  9, int)

#define IOCTL_SET_DEVBASEADDR(major)	_IOR(major,  10, unsigned long)
#define IOCTL_SET_DEVMEMSIZE(major)		_IOR(major,  11, unsigned long)

/*--------------------------- Typedefs ---------------------------------------*/ 

/*--------------------------- Global variables -------------------------------*/ 

/*--------------------------- Local variables --------------------------------*/ 
static char strDevFileName[20];

#ifdef __KERNEL__

static spinlock_t 		devLock;

/* This structure will hold the functions to be called 
 * when a process does something to the device we 
 * created. Since a pointer to this structure is kept in 
 * the devices table, it can't be local to
 * init_module. NULL is for unimplemented functions. 
 */

static int epdev_ioctl(
    			struct inode *inode,
    			struct file *file,
    			unsigned int ioctl_num,		/* The number of the ioctl */
    			unsigned long ioctl_param);	/* The parameter to it */
static int epdev_mmap(struct file * filp, struct vm_area_struct * vma);
static int epdev_open(struct inode *inode, struct file *file);
static int epdev_release(struct inode *inode, struct file *file);
static ssize_t epdev_read(
    			struct file *file,
    			char *buffer, 				/* The buffer to fill with the data */   
    			size_t length,     			/* The length of the buffer */
    			loff_t *offset); 			/* offset to the file */
    			
static ssize_t epdev_write(struct file *file,
				const char *buffer,
				size_t length,
				loff_t *offset);
    				
static struct file_operations fileOps = 	
	{
	write	: epdev_write,
	ioctl	: epdev_ioctl,   				/* ioctl */
	open	: epdev_open,		
	read	: epdev_read,
	release	: epdev_release,  				/* a.k.a. close */
	mmap	: epdev_mmap
	};

static	char	pTransitBuf[cCmdMsgBufLen];

static	int		setDataLen;
static	int		setId;

static	int		getDataLen;
static	int		getId;

static	int		setGetDataLen;
static	int		setGetId;

static 	int 	deviceOpen	= 0;
static  eBool	isInitOk	= cAtFalse;

eBool	(*pKerComGet)(char *pBuf, int len, int id);
eBool	(*pKerComSet)(char *pBuf, int *len, int id);

static	unsigned long	devBaseAddr;/* The physical address of device */
static	unsigned long	devMemMapSize;

#else

static char strTmpFileName[20];

#endif

/*--------------------------- Entries ----------------------------------------*/ 

/*--------------------------- Forward declaration ----------------------------*/ 

#ifdef __KERNEL__

/*----------------------------------------------------------------------------
*************************** KERNEL SPACE CODE *********************************
------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Prototype    : eBool DevRegister(const char* devName, 
								 unsigned int majorNum, 
								 eBool (*pUsrToKer)(char *, int len, int id), 
								 eBool (*pKerToUsr)(char *, int *len, int id))

Description  : This function is used to register the character device

Input        : devName					- The name of device
			   majorNum					- Major number of device
			   pUsrToKer                - Pointer to function which is used to 
										  process command from user space
               pKerToUsr                - Pointer to function which is used to 
										  process command from kernel space
               
Output       : None 

Return Code  : cAtTrue                     - If success
               cAtFalse                    - If failed

------------------------------------------------------------------------------*/
eBool DevRegister(const char* devName, 
				  unsigned int majorNum, 
				  eBool (*pUsrToKer)(char *, int len, int id), 
				  eBool (*pKerToUsr)(char *, int *len, int id))
	{
	int retVal;
	
	/* Register the character device (atleast try) */
	
	setDataLen		= 0;
	getDataLen		= 0;
	setGetDataLen	= 0;
	setId			= 0;
	getId			= 0;
	setGetId		= 0;
	
	pKerComGet	= pUsrToKer;
	pKerComSet	= pKerToUsr;
	isInitOk	= cAtFalse;
	sprintf(strDevFileName, "/dev/%s", devName);
	
	retVal = register_chrdev(majorNum, devName, &fileOps);
	
	/* Negative values signify an error */
	if (retVal < 0) 
		{
		isInitOk = cAtFalse;
		printk ("Register EP device is failed. ERROR CODE: %d\n", retVal);
		return cAtFalse;
		}

	isInitOk = cAtTrue;

	spin_lock_init(&devLock);
	
	return cAtTrue;
	}

/*-----------------------------------------------------------------------------
Prototype    : eBool DevUnregister(const char* devName, 
							       unsigned int majorNum)

Description  : This function is used to unregister character device

Input        : None
               
Output       : None

Return Code  : cAtTrue                     - If success
               cAtFalse                    - If failed

------------------------------------------------------------------------------*/
eBool DevUnregister(const char* devName, 
				    unsigned int majorNum)
	{
	/* Unregister the device */
	unregister_chrdev(majorNum, devName);
    /*if (unregister_chrdev(majorNum, devName) < 0)
		{
		return cAtFalse;
		}*/

	return cAtTrue;
	}  

/*-----------------------------------------------------------------------------
Prototype    : epdev_ioctl()

Description  :  This function is called whenever a process tries to 
                do an ioctl on our device file. We get two extra 
                parameters (additional to the inode and file 
                structures, which all device functions get): the number
                of the ioctl called and the parameter given to the 
                ioctl function.
                If the ioctl is write or read/write (meaning output 
                is returned to the calling process), the ioctl call 
                returns the output of this function.

Input        : None
               
Output       : None

Return Code  : None

------------------------------------------------------------------------------*/

static int epdev_ioctl(
    		struct inode *inode,
    		struct file *file,
    		unsigned int ioctl_num,		/* The number of the ioctl */
    		unsigned long ioctl_param)	/* The parameter to it */
	{
	unsigned int majorNum;

	majorNum = imajor(inode);

	/*------------------------ SET OPERATION ------------------------*/
	if (ioctl_num == IOCTL_SET_LEN(majorNum))
		{
		setDataLen = (int)ioctl_param;
		}
	else if (ioctl_num == IOCTL_SEND_SET_ID(majorNum))
		{
		setId = (int )ioctl_param;
		}
	else if (ioctl_num == IOCTL_SET_DATA(majorNum))
		{
		copy_from_user((char *)pTransitBuf, (char *)ioctl_param, setDataLen);
		if (pKerComGet != NULL)
			{
			pKerComGet(pTransitBuf, setDataLen, setId);
			}
		}
	/*------------------------ GET OPERATION ------------------------*/
	else if (ioctl_num == IOCTL_GET_LEN(majorNum))
		{
		return getDataLen;
		}	
	else if (ioctl_num == IOCTL_SEND_GET_ID(majorNum))
		{
		getId = (int )ioctl_param;
		}		
	else if (ioctl_num == IOCTL_GET_DATA(majorNum))
		{
		if (pKerComSet != NULL)
			{
			if (pKerComSet(pTransitBuf, &getDataLen, getId) == cAtTrue)
				{
				copy_to_user((char *)ioctl_param, pTransitBuf, getDataLen);
				}
			else
				{
				getDataLen = 0;
				}
			}			
		}	
	/*------------------------ SETGET OPERATION ---------------------*/
	else if (ioctl_num == IOCTL_SET_SETGET_LEN(majorNum))
		{
		setGetDataLen = (int )ioctl_param;
		}		
	else if (ioctl_num == IOCTL_GET_SETGET_LEN(majorNum))
		{
		return setGetDataLen;
		}		
	else if (ioctl_num == IOCTL_SEND_SETGET_ID(majorNum))
		{
		setGetId = (int )ioctl_param;
		}	
	else if (ioctl_num == IOCTL_SETGET_DATA(majorNum))
		{
		copy_from_user((char *)pTransitBuf, (char *)ioctl_param, setGetDataLen);
		
		if (pKerComSet != NULL)
			{
			if (pKerComSet(pTransitBuf, &setGetDataLen, setGetId) == cAtTrue)
				{
				copy_to_user((char *)ioctl_param, pTransitBuf, setGetDataLen);
				}
			else
				{
				setGetDataLen = 0;
				}
			}			
		}
	/*------------------------ SET MAPPED ADDRESS ---------------------*/	
	else if (ioctl_num == IOCTL_SET_DEVBASEADDR(majorNum))
		{
		devBaseAddr = ioctl_param;
		}
	else if (ioctl_num == IOCTL_SET_DEVMEMSIZE(majorNum))
		{
		devMemMapSize = ioctl_param;
		}
	else
		{
		printk("epdev_ioctl: Unknown message\n");
		return -1;
		}
	
	return cSuccess;
	}
 
static int epdev_mmap(struct file * filp, struct vm_area_struct * vma)
	{
	int 			ret;
	unsigned long 	addr;
	unsigned long 	size;
	
	addr = devBaseAddr;
	size = devMemMapSize;
	
	spin_lock(&devLock);
	
	if ((vma->vm_pgoff << PAGE_SHIFT) > size)
		{
		spin_unlock(&devLock);
		return -ENXIO;
		}
	addr = vma->vm_pgoff + (addr >> PAGE_SHIFT);

	if (vma->vm_end - (vma->vm_start + (vma->vm_pgoff << PAGE_SHIFT)) > size)
		size = vma->vm_end - (vma->vm_start + (vma->vm_pgoff << PAGE_SHIFT));
	
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	ret = io_remap_pfn_range(vma, vma->vm_start, addr, size, vma->vm_page_prot);		   
	
	if(ret != 0)
		{
		spin_unlock(&devLock);
		return -EAGAIN;
		}
	
	spin_unlock(&devLock);
	return 0;
	} 
 
/*-----------------------------------------------------------------------------
Prototype    : epdev_open()

Description  : This function is called whenever a process attempts 
               to open the device file 

Input        : inode
               file
               
Output       : None

Return Code  : -1                       - If init failed
               -EBUSY                   - If can not open device
               cSuccess                 - If success

------------------------------------------------------------------------------*/
 
static int epdev_open(struct inode *inode, struct file *file)
	{
	/* We don't want to talk to two processes at the 
	 * same time 
	 */
	 if (isInitOk != cAtTrue)
		{
		return -1;
		}
	if (deviceOpen > 0)
		{
		printk("%s %d\n", __FILE__, __LINE__);
		return -EBUSY;
		}
	
	/* If this was a process, we would have had to be 
	* more careful here, because one process might have 
	* checked Device_Open right before the other one 
	* tried to increment it. However, we're in the 
	* kernel, so we're protected against context switches.
	*
	* This is NOT the right attitude to take, because we
	* might be running on an SMP box, but we'll deal with
	* SMP in a later chapter.
	*/ 
	
	deviceOpen++;
	
	setDataLen = 0;
	getDataLen = 0;

#if ( ((LINUX_VERSION_CODE >> 8) << 8) == KERNEL_VERSION(2,4,0) )
	MOD_INC_USE_COUNT;
#elif ( ((LINUX_VERSION_CODE >> 8) << 8) == KERNEL_VERSION(2,6,0) )
	try_module_get(THIS_MODULE);
#endif
	
	return cSuccess;
	}


/*-----------------------------------------------------------------------------
Prototype    : epdev_release()

Description  : This function is called whenever a process attempts 
               to release the device file 

Input        : inode
               file
               
Output       : None

Return Code  : cSuccess                 - If success

------------------------------------------------------------------------------*/

static int epdev_release(struct inode *inode, struct file *file)
	{
	#ifdef DEBUG
	printk ("epdev_release(%p,%p)\n", inode, file);
	#endif
	
	/* We're now ready for our next caller */
	deviceOpen--;
	
#if ( ((LINUX_VERSION_CODE >> 8) << 8) == KERNEL_VERSION(2,4,0) )
	MOD_DEC_USE_COUNT;
#elif ( ((LINUX_VERSION_CODE >> 8) << 8) == KERNEL_VERSION(2,6,0) )
	module_put(THIS_MODULE);
#endif
	
	return cSuccess;
	}

/*-----------------------------------------------------------------------------
Prototype    : epdev_read()

Description  : This function is called whenever a process attempts 
               to read data from the device 

Input        : file                     - Pointer to the device
               length                   - The length of the buffer
               offset                   - offset to the file
               
Output       : buffer                   - The buffer to fill with the data

Return Code  : cSuccess                 - If success

------------------------------------------------------------------------------*/

static ssize_t epdev_read(
    				struct file *file,
    				char *buffer, 		/* The buffer to fill with the data */   
    				size_t length,     	/* The length of the buffer */
    				loff_t *offset) 	/* offset to the file */
	{
		
	#ifdef DEBUG
	printk("epdev_read(%p,%p,%d)\n", file, buffer, length);
	#endif
	
	return -1;
	}

/*-----------------------------------------------------------------------------
Prototype    : epdev_write()

Description  : This function is called whenever a process attempts 
               to write data to the device 

Input        : file                     - Pointer to the device
               length                   - The length of the buffer
               offset                   - offset to the file
               
Output       : buffer                   - The buffer to fill with the data

Return Code  : cSuccess                 - If success

------------------------------------------------------------------------------*/

static ssize_t epdev_write(struct file *file,
                            const char *buffer,
                            size_t length,
                            loff_t *offset)
	{
	#ifdef DEBUG
	printk("epdev_write(%p,%p,%d)\n", file, buffer, length);
	#endif
	
	return 0;
	}


#else /* USER SPACE */


/*----------------------------------------------------------------------------
*************************** USER SPACE CODE ***********************************
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
Prototype    : static eBool IsDevFileExist(char *pDevFileName, 
											char *pTmpFileName, 
											unsigned int majorNum)

Description  : This function is used to check if a file is existed in the 
               filesystem or not

Input        : pDevFileName             - Pointer to device file
               pTmpFileName             - Pointer to temporary file
			   majorNum					- Major number of device
               
Output       : None 

Return Code  : cAtTrue                  - If success
               cAtFalse                 - If failed

------------------------------------------------------------------------------*/
static eBool IsDevFileExist(char *pDevFileName, 
							char *pTmpFileName, 
							unsigned int majorNum)
	{
	char cmd[200];
	FILE *pTmpFile;
	
	sprintf(cmd, "ls -all %s | grep %u | cat > %s", pDevFileName, majorNum, pTmpFileName);
	if (system(cmd) < 0)
		{
		return cAtFalse;
		}
		
	pTmpFile = fopen( pTmpFileName, "r");
	if (pTmpFile == NULL)
		{
		return cAtFalse;
		}

	fseek(pTmpFile, 0L, SEEK_END);
	if (ftell(pTmpFile) == 0)
		{
		fclose(pTmpFile);
		return cAtFalse;
		}

	sprintf(cmd, "rm -f %s", pTmpFileName);	
	system(cmd);

	return cAtTrue;
	}

/*-----------------------------------------------------------------------------
Prototype    : eBool DevOpen(const char* devName, 
							 unsigned long phyAddr, 
							 unsigned long devMemSize, 
							 unsigned int  majorNum,
							 tDevDescriptor *pDevDescr)

Description  : This function is used to open character device

Input        : devName				- The device name
			   phyAddr				- The physical address
			   devMemSize			- The size of device memory
			   majorNum				- The major number of device
               
Output       : pDevDescr			- The device information 

Return Code  : cAtTrue              - If success
               cAtFalse             - If failed

------------------------------------------------------------------------------*/
eBool DevOpen(const char* devName, 
			  unsigned long phyAddr, 
			  unsigned long devMemSize, 
			  unsigned int  majorNum,
			  tDevDescriptor *pDevDescr)
	{	
	char createDev[100];
	
	if (pDevDescr == NULL)
		{
		return cAtFalse;
		}
	
	sprintf(strDevFileName, "/dev/%s", devName);
	sprintf(strTmpFileName, "/tmp/%s", devName);
	
	if (IsDevFileExist(strDevFileName, strTmpFileName, majorNum) != cAtTrue)
		{
		sprintf(createDev,"mknod %s c %u 0", strDevFileName, majorNum);
		system(createDev);
		}
		
	pDevDescr->fd = open(strDevFileName, O_RDWR);
	if (pDevDescr->fd < 0)
		{
		fprintf(stderr, "Open device file %s is failed\n", strDevFileName);
		return cAtFalse;
		}

	pDevDescr->phyAddr	= phyAddr; 
	pDevDescr->size		= devMemSize; 

	/*mmap*/
	ioctl(pDevDescr->fd, IOCTL_SET_DEVBASEADDR(majorNum), pDevDescr->phyAddr);
	ioctl(pDevDescr->fd, IOCTL_SET_DEVMEMSIZE(majorNum), pDevDescr->size);
	pDevDescr->pVirtualAddr	= mmap(0, pDevDescr->size, 
								   PROT_READ | PROT_WRITE ,
								   MAP_SHARED ,
								   pDevDescr->fd,
								   0);
	
	if(pDevDescr->pVirtualAddr == MAP_FAILED)
		{	
		perror("mmap");
		return cAtFalse;
		}
	
	return cAtTrue;
	}

/*-----------------------------------------------------------------------------
Prototype    : eBool DevClose(tDevDescriptor *pDevDescr)

Description  : This function is used to close character device

Input        : pDevDescr			- The device information
               
Output       : None 

Return Code  : cAtTrue              - If success
               cAtFalse             - If failed
------------------------------------------------------------------------------*/
eBool DevClose(tDevDescriptor *pDevDescr)
	{
	if (pDevDescr == NULL)
		{
		return cAtFalse;
		}

	if( pDevDescr->fd >= 0 )
		{
		if(pDevDescr->pVirtualAddr != NULL 
			&& pDevDescr->pVirtualAddr != MAP_FAILED 
			&& pDevDescr->size)
			{
			munmap(pDevDescr->pVirtualAddr, pDevDescr->size);
			}

		close(pDevDescr->fd);
		}

	return cAtTrue;
	}

/*-----------------------------------------------------------------------------
Prototype    : eBool DevCtrlSet(tDevDescriptor *pDevDescr, 
								char *pDataBuf, 
								int len, 
								int cmdId)

Description  : This function is used to set the command for character device

Input        : pDevDescr			- The device information
			   pDataBuf				- The data buffer
			   len					- The length of data buffer
			   cmdId				- The command identifier
               
Output       : None 

Return Code  : cAtTrue              - If success
               cAtFalse             - If message (pDataBuf)is longer than 
                                      buf (cCmdMsgBufLen) Or Calling 
                                      ioctl() failed.

------------------------------------------------------------------------------*/

eBool DevCtrlSet(tDevDescriptor *pDevDescr, 
				 char *pDataBuf, 
				 int len, 
				 int cmdId)
	{
	int retVal;
	eBool result;
	
	/* Send Set ID to kernel*/
	result = cAtTrue;
	retVal = ioctl(pDevDescr->fd, IOCTL_SEND_SET_ID(pDevDescr->majorNum), cmdId);
	if (retVal < 0)
		{
		fprintf(stderr, "DevCtrlSet(), Send set ID failed\n");
		result = cAtFalse;
		}
	else if ((len > cCmdMsgBufLen) || (len < 0))
		{
		fprintf(stderr, "DevCtrlSet(), Message length is invalid\n");
		result = cAtFalse;
		}
	else
		{
		retVal = ioctl(pDevDescr->fd, IOCTL_SET_LEN(pDevDescr->majorNum), len);
		if (retVal < 0)
			{
			fprintf(stderr, "DevCtrlSet(), Send length of set message failed\n");
			result = cAtFalse;
			}
		else
			{
			retVal = ioctl(pDevDescr->fd, IOCTL_SET_DATA(pDevDescr->majorNum), pDataBuf);
			if (retVal < 0)
				{
				fprintf(stderr, "DevCtrlSet(), Send set message failed\n");
				result = cAtFalse;
				}
			}
		}
	return result;
	}


/*-----------------------------------------------------------------------------
Prototype    : eBool DevCtrlGet(tDevDescriptor *pDevDescr, 
								char *pDataBuf, 
								int *len, 
								int cmdId)

Description  : This function is used to get command from character device

Input        : pDevDescr			- The device information
			   cmdId				- The command identifier
               
Output       : pDataBuf				- The data buffer
			   len					- The length of data buffer

Return Code  : cAtTrue              - If success
               cAtFalse             - If message (pDataBuf)is longer than 
                                      transit buf (cCmdMsgBufLen) Or Calling 
                                      ioctl() failed.

------------------------------------------------------------------------------*/

eBool DevCtrlGet(tDevDescriptor *pDevDescr, 
				 char *pDataBuf, 
				 int *len, 
				 int cmdId)
	{
	int retVal;
	eBool result;
	
	/* Send ID to kernel*/
	result = cAtTrue;
	retVal = ioctl(pDevDescr->fd, IOCTL_SEND_GET_ID(pDevDescr->majorNum), cmdId);
	if (retVal < 0)
		{
		fprintf(stderr, "DevCtrlGet(), Send get ID failed\n");
		result = cAtFalse;
		}
	else
		{
		retVal = ioctl(pDevDescr->fd, IOCTL_GET_DATA(pDevDescr->majorNum), pDataBuf);
		if (retVal < 0)
			{
			fprintf(stderr, "DevCtrlGet(), get data failed\n");
			result = cAtFalse;
			}
		else
			{
			/* Get len must be after get data.*/
			*len = ioctl(pDevDescr->fd, IOCTL_GET_LEN(pDevDescr->majorNum), 0);
			if (*len < 0)
				{
				fprintf(stderr, "DevCtrlGet(), Get length of message failed\n");
				result = cAtFalse;
				}
			}
		}
	return result;
	}


/*-----------------------------------------------------------------------------
Prototype    : eBool DevCtrlSetGet(tDevDescriptor *pDevDescr, 
								   char *pDataBuf, 
								   int *len, 
								   int cmdId)

Description  : This function is used to get command from or 
			   set command to character device

Input        : pDevDescr			- The device information
		       pDataBuf				- The data buffer
			   len					- The length of data buffer
			   cmdId				- The command identifier
               
Output       : pDataBuf				- The data buffer
			   len					- The length of data buffer

Return Code  : cAtTrue              - If success
               cAtFalse             - If message (pDataBuf)is longer than 
                                      transit buf (cCmdMsgBufLen) Or Calling 
                                      ioctl() failed.

------------------------------------------------------------------------------*/

eBool DevCtrlSetGet(tDevDescriptor *pDevDescr, 
					char *pDataBuf, 
					int *len, 
					int cmdId)
	{
	int retVal;	
	eBool result;	
	
	/* Send ID to kernel*/
	result = cAtTrue;
	retVal = ioctl(pDevDescr->fd, IOCTL_SEND_SETGET_ID(pDevDescr->majorNum), cmdId);
	if (retVal < 0)
		{
		fprintf(stderr, "DevCtrlSetGet(), Send SetGet ID failed\n");
		result = cAtFalse;
		}
	else
		{
		/* Send len to kernel*/
		retVal = ioctl(pDevDescr->fd, IOCTL_SET_SETGET_LEN(pDevDescr->majorNum), *len);
		if (retVal < 0)
			{
			fprintf(stderr, "DevCtrlSetGet(), Send length of get message failed\n");
			result = cAtFalse;
			}
		else
			{
			retVal = ioctl(pDevDescr->fd, IOCTL_SETGET_DATA(pDevDescr->majorNum), pDataBuf);
			if (retVal < 0)
				{
				fprintf(stderr, "DevCtrlSetGet(), get data failed\n");
				result = cAtFalse;
				}
			else
				{
				/* Get len must be after get data.*/
				*len = ioctl(pDevDescr->fd, IOCTL_GET_SETGET_LEN(pDevDescr->majorNum), 0);
				if (*len < 0)
					{
					fprintf(stderr, "DevCtrlSetGet(), Get length of message failed\n");
					result = cAtFalse;
					}
				}
			}
		}

	return result;
	}

#endif	/* USER SPACE */


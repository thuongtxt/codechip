/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : main.c
 *
 * Created Date: Nov 13, 2014
 *
 * Description : CLI application
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAlu.h"
#include "AtAluPci.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
int main(int argc, char* argv[])
    {
	AtHal hal;

	/* Need OSAL for memory operation */
	AtOsalSharedSet(AtOsalLinux());
    hal = AtAluHalCreate();

    /* Start CLI app. This will block and wait user input CLI. */
    AtAluCliStart(hal);

    /* The following lines will run when "exit" CLI is executed */
    AtAluCliCleanup();
    AtHalDelete(hal);

    return 0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AppHalListener.h
 * 
 * Created Date: Mar 28, 2017
 *
 * Description : Application HAL listener
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _APPHALLISTENER_H_
#define _APPHALLISTENER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AppHalListenerEnable(AtHal hal, eBool enabled);
eBool AppHalListenerHasInvalidAccesses(void);
const char *AppHalListenerLogFileName(void);

#ifdef __cplusplus
}
#endif
#endif /* _APPHALLISTENER_H_ */


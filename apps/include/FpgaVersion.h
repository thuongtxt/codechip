/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : FpgaVersion.h
 * 
 * Created Date: May 25, 2016
 *
 * Description : To handle FPGA version
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _FPGAVERSION_H_
#define _FPGAVERSION_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AppFpgaVersionParse(char *fpgaVersion);
const char *AppFpgaVersionFormat(void);
uint32 AppFpgaVersionNumber(void);
uint32 AppFpgaBuiltNumber(void);

#ifdef __cplusplus
}
#endif
#endif /* _FPGAVERSION_H_ */


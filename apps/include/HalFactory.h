/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : HalFactory.h
 * 
 * Created Date: Apr 8, 2015
 *
 * Description : HAL factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _HALFACTORY_H_
#define _HALFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal HalFactoryCreateHalSim(uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _HALFACTORY_H_ */


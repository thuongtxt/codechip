/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO module name
 *
 * File        : epapp.h
 *
 * Created Date: Nov 1, 2012
 *
 * Description : TODO Description
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _EPAPP_H_
#define _EPAPP_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cMsgCliShellConnect      1
#define cMsgCliShellDisconnect   2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * Message in message queue contains a message header and data
 */
typedef struct tMsgHeader
    {
    uint32 msgId;
    uint32 msgLen;
    pid_t  senderPid;
    } tMsgHeader;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Entries ----------------------------------------*/
extern void CliClientExit(void);

#endif /* _EPAPP_H_ */


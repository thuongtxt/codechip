/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : MemoryMap.h
 * 
 * Created Date: Jan 17, 2017
 *
 * Description : Memory map
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _MEMORYMAP_H_
#define _MEMORYMAP_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
char *AppMemoryMapDeviceGet(void);
void AppMemoryMapDeviceSet(const char *deviceName);
void AppMemoryMapPhysicalOffsetSet(long long offset);
long long AppMemoryMapPhysicalOffsetGet(void);
void AppMemoryMapMemorySizeInBytesSet(long long size);
long long AppMemoryMapMemorySizeInBytesGet(void);
void AppMemoryMapCleanup(void);
AtHal AppMemoryMapHalCreate(void);
eBool AppMemoryMapParseInput(char *information);

#ifdef __cplusplus
}
#endif
#endif /* _MEMORYMAP_H_ */


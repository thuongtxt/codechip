/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : tasks.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Task management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _TASKS_H_
#define _TASKS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtTaskInterruptPeriodUs 1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
atbool AppAllTasksCreate(void);
atbool AppAllTasksDelete(void);

eBool AppPollingTaskIsEnabled(void);
void  AppPollingTaskEnable(eBool enable);
void AppPollingTaskPeriodInMsSet(uint32 periodMs);

eBool AppForcePointerTaskIsEnabled(void);
void  AppForcePointerTaskEnable(eBool enable);
void  AppForcePointerTaskPeriodInMsSet(uint32 periodMs);

eBool AppInterruptTaskIsEnabled(void);
void  AppInterruptTaskEnable(eBool enable);

void AppEyeScanTaskEnable(eBool enable);

void AppInterruptMessageSend(void);
eBool AppInterruptMessageCanSend(void);
AtHal AppHalServer(void);

#endif /* _TASKS_H_ */


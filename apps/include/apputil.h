/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : apputil.h
 * 
 * Created Date: Feb 3, 2013
 *
 * Description : App Util
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _APPUTIL_H_
#define _APPUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtHal.h"
#include "AppHalListener.h"

/*--------------------------- Define -----------------------------------------*/
#define cStrPlatformEp5               "ep5"
#define cStrPlatformEp6               "ep6"
#define cStrPlatformEp6C1             "ep6c1"
#define cStrPlatform16BitEp6C1        "ep6c1_16bit"
#define cStrPlatformEp6C2             "ep6c2"
#define cStrPlatformH3C               "h3c"
#define cStrPlatformLoop16E1          "loop_16e1"
#define cStrPlatformLoop32E1          "loop_32e1"
#define cStrPlatformLoopStm           "loop_stm"
#define cStrPlatformZtePtn            "zte_ptn"
#define cStrPlatformAlu               "alu"
#define cStrPlatformZteRouterStm      "zte_router_stm"
#define cStrPlatformFiberLogic        "fiberlogic"
#define cStrPlatformXilinx            "xilinx"
#define cStrPlatformAnna            "anna"
#define cStrPlatformAll               "["cStrPlatformEp5" | "cStrPlatformEp6" | "cStrPlatformEp6C1" | "cStrPlatform16BitEp6C1" | "cStrPlatformEp6C2 \
                                      " | "cStrPlatformH3C" | "cStrPlatformLoop16E1" | "cStrPlatformLoop32E1" | "cStrPlatformLoopStm"  | "cStrPlatformZtePtn\
                                      " | "cStrPlatformFiberLogic" | "cStrPlatformZteRouterStm" | "cStrPlatformAlu" | "cStrPlatformXilinx" | "cStrPlatformAnna"]"
#define cStrInputPlatform           "Please input platform: "cStrPlatformAll".\nUse option -h for more help\n"

#define cMsgMaxLength      100
#define cMsgQueueName            "/comm_queue"

/*--------------------------- Macros -----------------------------------------*/
#define mPrintMsg(color_, typeStr, ...)                                        \
    do                                                                         \
        {                                                                      \
        AtPrintc(color_, typeStr": ");                                         \
        AtPrintc(color_, __VA_ARGS__);                                         \
        AtPrintc(cSevNormal, "\r\n");                                          \
        }while(0)

#define mPrintError(...)   mPrintMsg(cSevCritical, "ERROR", __VA_ARGS__)
#define mPrintWarning(...) mPrintMsg(cSevWarning, "WARNING", __VA_ARGS__)
#define mPrintInfo(...)    mPrintMsg(cSevInfo, "INFO", __VA_ARGS__)
#define mPrint(...)        AtPrintf(__VA_ARGS__)

/*--------------------------- Typedefs ---------------------------------------*/
/* Image type to use for simulation purpose */
typedef enum ePlatform
    {
    cPlatformUnknown,
    cPlatformEp6,
    cPlatformEp6C1,
    cPlatform16BitEp6C1,
    cPlatformEp6C2,
    cPlatformEp5,
    cPlatformH3C,
    cPlatformLoop16E1,
    cPlatformLoop32E1,
    cPlatformLoopStm,
    cPlatformZtePtn,
    cPlatformAlu,
    cPlatformZteRouterStm,
    cPlatformFiberLogic,
    cPlatformSimul,
    cPlatformRemote,
    cPlatformTestbench,
    cPlatformXilinx,
    cPlatformAnna
    } ePlatform;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
atbool AppOptionParse(int argc, char* argv[]);
atbool AppOptionParseWithFormat(int argc, char* argv[], const char *format);
const char *AppOptionParseDefaultFormat(void);

AtHal AppCreateHal(uint32 baseAddress, ePlatform plat);
uint32 AppBaseAddressOfCore(uint8 coreId);
const char *AppName(void);
ePlatform AppPlatform(void);
eBool AppIsDeamon(void);
eBool AppNeedToDisplayHelp(void);
eBool AppIsSimulationPlatform(ePlatform platform);
uint32 AppProductCodeGet(ePlatform plat);
uint32 AppListenPortGet(void);

/* Product code translation */
uint32 AppSpecifiedProductCode(void);
uint32 AppSimulationProductCode(uint32 realProductCode);
uint32 AppEpProductCode(uint32 realProductCode);

int SocketListen(uint32 listenPort);
int SocketCreate(void);
atbool SocketConnect(int sock, const char * serverIpAddr, int port);
uint32 SocketSend(int sock, char *pMsg, uint32 msgLen);
uint32 SocketRecv(int sock, char *msgBuf, uint32 msgSize);
int SocketClose(int sock);
int SocketAccept(int listenSocket);

uint32 AppHalListenPortGet(void);
uint32 AppHalEventNotifyPortGet(void);
eBool AppDoNotAddDevice(void);
uint16 AppCliServerListenPortGet(void);

void AppAllCoresInterruptListen(AtDevice device);
void AppInterruptFloodingWatch(eBool watched);
void AppInterruptFloodingContinuousThresholdSet(uint32 threshold);
void AppInterruptFloodingTotalThresholdSet(uint32 threshold);
void AppInterruptFloodingShow(void);

eBool AppIsFileLogger(void);

#endif /* _APPUTIL_H_ */


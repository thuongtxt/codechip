/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LIU
 * 
 * File        : LiuManager.h
 * 
 * Created Date: Mar 3, 2015
 *
 * Description : LIU manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _LIUMANAGER_H_
#define _LIUMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "apputil.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool LiuManagerSetup(AtDevice device, ePlatform platform);
void LiuManagerDelete(void);

#ifdef __cplusplus
}
#endif
#endif /* _LIUMANAGER_H_ */


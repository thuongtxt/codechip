/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : appsignal.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Signal processing
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _APPSIGNAL_H_
#define _APPSIGNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
atbool AppSignalSetup();
atbool SignalHandlerInstall(int sigNum, void (*sigHandler)(int), int saFlags);

#endif /* _APPSIGNAL_H_ */


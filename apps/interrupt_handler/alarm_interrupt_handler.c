/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : interrupt_handler.c
 *
 * Created Date: Jan 15, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
/*
 * Standard in kernel modules
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <asm/uaccess.h>        /* for get_user and put_user */
#include <asm/io.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <asm/siginfo.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/version.h>

#include "attypes.h"
#include "hal_kernel/AtHal.h"
#include "commacro.h"

MODULE_LICENSE("GPL");

/*--------------------------- Define -----------------------------------------*/
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,39) /* Board EP6C1 */
#define cIrqHwNumber 17
#else
#define cIrqHwNumber 0 /* Board EP6K1 */
#endif

#define cDeviceBaseAddress      0xD0000000

#define cEpappProcFileName      "epapp"
#define PROCFS_MAX_SIZE         11
#define cDeviceAlarmInterruptSignal   SIGUSR1

/*--------------------------- Macros -----------------------------------------*/
#define mPrintError(FMT, PARMS...)     printk("ERROR: "FMT"\n", ## PARMS)
#define mPrintWarning(FMT, PARMS...)   printk("WARNING: "FMT"\n", ## PARMS)
#define mPrintInfo(FMT, PARMS...)      printk(FMT"\n", ## PARMS)
#define mPrint(FMT, PARMS...)          printk(FMT"\n", ## PARMS)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHal halk;
static unsigned int irq_number = 0;
static int hal_show = 0;

static struct proc_dir_entry *epapp_proc_file = NULL;
static char epapp_pid_string[PROCFS_MAX_SIZE];
static int epapp_pid = 0;

static struct siginfo signal_info;
static struct task_struct *epapp_taskstruct;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * This function services interrupts
 */
irqreturn_t irq_handler(int irq, void *dev_id)
    {
    /* Save indirect state */
    AtHalStateSave(halk);

     /* Disable global interrupt */
    AtHalWrite(halk, 0x3, 0x0);
    mPrint("Handling IRQ : %d!\n", irq);

    if (epapp_pid != 0)
        {
        if (send_sig_info(cDeviceAlarmInterruptSignal, &signal_info, epapp_taskstruct) < 0)
            mPrintError("Sending signal is FAIL\n");

        mPrint("Send interrupt signal to user space app\n");
        }

    /* Restore indirect state */
    AtHalStateRestore(halk);

    return IRQ_HANDLED;
    }

static int read_proc(char *buffer,
                     char **start,
                     off_t offset,
                     int count,
                     int *eof,
                     void *data)
    {
    int len;

    if (offset > 0)
        {
        return 0;
        }
    else
        {
        sprintf(epapp_pid_string, "%d\n", epapp_pid);
        len = strlen(epapp_pid_string);
        memcpy(buffer, epapp_pid_string, len);
        }

    return len;
    }

static int write_proc(struct file *file,
                    const char *buffer,
                    unsigned long count,
                    void *data)
    {
    if (count > (PROCFS_MAX_SIZE - 1) )
        {
        mPrintError("Invalid pid = %ld\n", count);
        return (EINVAL);
        }

    /* Get epapp pid */
    if (copy_from_user(epapp_pid_string, buffer, count) != 0)
        {
        return (EFAULT);
        }

    sscanf(epapp_pid_string, "%d", &epapp_pid);
    mPrint("epapp_pid = %d\n", epapp_pid);

    if (epapp_pid != 0)
        {
        rcu_read_lock();
        /*epapp_taskstruct = find_task_by_pid_type(PIDTYPE_PID, epapp_pid);*/
        epapp_taskstruct = pid_task(find_pid_ns(epapp_pid, &init_pid_ns), PIDTYPE_PID);
        if(epapp_taskstruct == NULL)
            {
            mPrintError("No such epapp pid = %d\n", epapp_pid);
            rcu_read_unlock();

            epapp_pid = 0;
            return (ENODEV);
            }
        rcu_read_unlock();
        }

    return count;
    }

static int EpappProcFileCreate(void)
    {
    epapp_proc_file = create_proc_entry(cEpappProcFileName, 0644, NULL);

    if (epapp_proc_file == NULL)
        {
        remove_proc_entry(cEpappProcFileName, NULL);
        mPrintError("Could not initialize /proc/%s\n", cEpappProcFileName);
        return (ENOMEM);
        }

    /*epapp_proc_file->owner = THIS_MODULE;*/
    epapp_proc_file->write_proc = write_proc;
    epapp_proc_file->read_proc = read_proc;

    /* Initialize signal to be ready */
    memset(&signal_info, 0, sizeof(struct siginfo));
    signal_info.si_signo = cDeviceAlarmInterruptSignal;
    signal_info.si_code = SI_KERNEL;

    memset(epapp_pid_string, 0, PROCFS_MAX_SIZE);

    return (0);
    }

static void EpappProcFileRemove(void)
    {
    if (epapp_proc_file != NULL)
        {
        remove_proc_entry(cEpappProcFileName, NULL);
        mPrint("/proc/%s is removed\n", cEpappProcFileName);
        }
    }

int init_module( void )
    {
    int retval = 0;
    int irq_return = 0;

    if ((retval = EpappProcFileCreate()) != 0)
        return (retval);

    /**
     * Create HAL indirect
     */
    halk = AtHalIndirectNew(cDeviceBaseAddress);
    if (halk == NULL)
        return EBUSY;

    if (hal_show)
        {
        AtHalShowRead(halk, cAtTrue);
        AtHalShowWrite(halk, cAtTrue);
        }

    /**
     * irq_create_mapping() - Map a hardware interrupt into linux irq space
     * @domain: domain owning this hardware interrupt or NULL for default domain
     * @hwirq: hardware irq number in that domain space
     *
     * Only one mapping per hardware interrupt is permitted. Returns a linux
     * irq number.
     * If the sense/trigger is to be specified, set_irq_type() should be called
     * on the number returned from that call.
     */
    irq_number = irq_create_mapping(NULL, cIrqHwNumber);
    mPrint("IRQ number %d\n", irq_number);

    if (irq_number > 0)
        {
        irq_return = request_irq(irq_number,
                                 irq_handler, /* our handler */
                                 IRQF_PROBE_SHARED, "alarm_interrupt_handler",
                                 NULL);
        mPrint("Request IRQ return %d\n", irq_return);
        }

    if (irq_return != 0)
        return EBUSY;

    mPrint("Init interrupt handler is OK\n");

    return (retval);
    }

void cleanup_module( void )
    {
    EpappProcFileRemove();

    if (irq_number > 0)
        {
        free_irq(irq_number, NULL);

        /**
         * irq_dispose_mapping() - Unmap an interrupt
         * @virq: linux irq number of the interrupt to unmap
         */
        irq_dispose_mapping(irq_number);
        }

    if (halk)
        AtHalDelete(halk);

    return;
    }



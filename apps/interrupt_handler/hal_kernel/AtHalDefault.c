/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDefault.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : Default implementation of HAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <linux/slab.h>
#include "AtHalDefault.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mBaseAddress(self) (((tAtHalDefault *)self)->baseAddress)
#define mRealAddress(self, localAddress) ((((localAddress) & cBit24_0) << 2) + mBaseAddress(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation data structure */
static uint8 m_methodsInit = 0;
static tAtHalMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtHal self)
    {
    kfree(self);
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    *((uint32 *)mRealAddress(self, address)) = value;
    }

static uint32 Read(AtHal self, uint32 address)
    {
    return *((uint32 *)mRealAddress(self, address));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalDefault);
    }

static void Override(AtHal self)
    {
    /* Initialize implementation */
    if (!m_methodsInit)
        {
        m_methods.Delete = Delete;
        m_methods.Read = Read;
        m_methods.Write = Write;
        }

    self->methods = &m_methods;
    }

AtHal AtHalDefaultObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* Save base address */
    ((AtHalDefault)self)->baseAddress = baseAddress;

    return self;
    }

/*
 * Create new default HAL
 *
 * @param baseAddress Base address
 * @return Default HAL
 */
AtHal AtHalDefaultNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = kmalloc(ObjectSize(), GFP_KERNEL);

    /* Construct it */
    return AtHalDefaultObjectInit(newHal, baseAddress);
    }

/*
 * Get base address
 *
 * @param self This HAL
 * @return Base address
 */
uint32 AtHalDefaultBaseAddress(AtHal self)
    {
    return ((AtHalDefault)self)->baseAddress;
    }

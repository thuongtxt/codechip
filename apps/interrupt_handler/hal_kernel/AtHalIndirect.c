/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalIndirect.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : EP6 C1 V2 - HAL implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <linux/slab.h>
#include <linux/time.h>
#include "AtHalDefault.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegisterIndirectControl 0xF00002
#define cRegisterIndirectData    0xF00003
#define cInvalidReadValue        0xCAFECAFE
#define cIndirectTimeoutInMs     2 /* milisecond */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalIndirect * AtHalIndirect;

typedef struct tAtHalIndirect
    {
    tAtHalDefault super;

    /* To store content of indirect registers */
    uint32 indirectControl;
    uint32 indirectData;
    }tAtHalIndirect;

/** @brief The current time structure */
typedef struct tAtOsalCurTime
    {
    uint32 sec;  /**< Seconds */
    uint32 usec; /**< Microseconds */
    }tAtOsalCurTime;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHalMethods m_AtHalOverride;
static const tAtHalMethods *m_AtHalMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtHalIndirect);
    }

static void CurTimeGet(tAtOsalCurTime *currentTime)
    {
    struct timeval tm;

    if (currentTime == NULL)
        return;

    do_gettimeofday(&tm);
    currentTime->sec = tm.tv_sec;
    currentTime->usec = tm.tv_usec;
    }

static uint32 AtOsalDifferenceTimeInMs(const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    return ((((currentTime->sec - previousTime->sec) * 1000) + (currentTime->usec / 1000)) - (previousTime->usec / 1000));
    }

static void Write(AtHal self, uint32 address, uint32 value)
    {
    uint32 regVal;
    uint32 timeOut;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;

    regVal  = 0;
    timeOut = 0;
    address = address & cBit23_0;
    memset(&curTime, sizeof(tAtOsalCurTime), 0);
    memset(&startTime, sizeof(tAtOsalCurTime), 0);

    /* Write control registers */
    if ((address == cRegisterIndirectControl) || (address == cRegisterIndirectData))
        {
        m_AtHalMethods->Write(self, address, value);
        return;
        }

    /* Write other registes */
    m_AtHalMethods->Write(self, cRegisterIndirectData, value);

    /* Make write request */
    regVal  = address; /* Bit 30 is 0 for read action */
    regVal |= cBit31;  /* Bit 31 is to make request */
    m_AtHalMethods->Write(self, cRegisterIndirectControl, regVal);

    /* Wait for finish, only do this when it has not */
    regVal = m_AtHalMethods->Read(self, cRegisterIndirectControl);
    if ((regVal & cBit31) == 0)
        {
        return;
        }

    /* Timeout waiting for hardware finish job */
    timeOut = 0;
    CurTimeGet(&startTime);
    while (timeOut < cIndirectTimeoutInMs)
        {
        /* Check if hardware done */
        regVal = m_AtHalMethods->Read(self, cRegisterIndirectControl);
        if ((regVal & cBit31) == 0)
            {
            return;
            }

        /* Calculate elapse time */
        CurTimeGet(&curTime);
        timeOut = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Timeout, cancel this progress */
    m_AtHalMethods->Write(self, cRegisterIndirectControl, 0);
    printk("ERROR: Indirect write 0x%x with value 0x%x fail\n", address, value);
    }

static uint32 Read(AtHal self, uint32 address)
    {
    uint32 regVal;
    uint32 timeout_ms;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;

    /* Write to control reg */
    regVal = 0;
    timeout_ms    = 0;
    memset(&curTime, sizeof(tAtOsalCurTime), 0);
    memset(&startTime, sizeof(tAtOsalCurTime), 0);

    /* Read a control register */
    address = address & cBit23_0;
    if (address == cRegisterIndirectControl || address == cRegisterIndirectData)
        return m_AtHalMethods->Read(self, address);

    /* Make a read request for normal register */
    regVal  = address;
    regVal |= cBit30; /* For read action */
    regVal |= cBit31; /* To make request */
    m_AtHalMethods->Write(self, cRegisterIndirectControl, regVal);

    /* Hardware finishes its job, return its value */
    if ((m_AtHalMethods->Read(self, cRegisterIndirectControl) & cBit31) == 0)
        {
        return m_AtHalMethods->Read(self, cRegisterIndirectData);
        }

    /* Timeout waiting for it */
    timeout_ms = 0;
    CurTimeGet(&startTime);
    while(timeout_ms < cIndirectTimeoutInMs)
        {
        /* Done, return hardware value */
        if ((m_AtHalMethods->Read(self, cRegisterIndirectControl) & cBit31) == 0)
            {
            regVal = m_AtHalMethods->Read(self, cRegisterIndirectData);
            if (regVal == 0xCAFECAFE)
                printk("(%s, %d) Read 0x%08x, value 0x%08x\n", __FILE__, __LINE__, address, regVal);

            return regVal;
            }

        /* Calculate elapse time */
        CurTimeGet(&curTime);
        timeout_ms = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Timeout, cancel this action and return an invalid value */
    m_AtHalMethods->Write(self, cRegisterIndirectControl, 0);
    printk("ERROR: Indirect read 0x%x timeout\n", address);

    return cInvalidReadValue;
    }

static void Delete(AtHal self)
    {
    /* Fully delete itself */
    m_AtHalMethods->Delete(self);
    }

static uint8 IndirectIsInProgress(uint32 controlReg)
    {
    return (controlReg & cBit31) ? 1 : 0;
    }

static void IndirectCtrlRegisterRead(AtHal self)
    {
    AtHalIndirect halIndirect = (AtHalIndirect)self;
    halIndirect->indirectControl = m_AtHalMethods->Read(self, cRegisterIndirectControl);
    halIndirect->indirectData = m_AtHalMethods->Read(self, cRegisterIndirectData);
    }


/* Save and restore indirect control registers */
static void StateSave(AtHal self)
    {
    uint32 timeOut;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtHalIndirect halIndirect = (AtHalIndirect)self;
    uint32 controlReg;

    IndirectCtrlRegisterRead(self);

    /* If HW is NOT being request, save and return */
    if (!IndirectIsInProgress(halIndirect->indirectControl))
        return;

    /* HW is being requested, need to wait until HW finish before save content */
    /* Timeout waiting for hardware finish job */
    timeOut = 0;
    CurTimeGet(&startTime);
    while (timeOut < cIndirectTimeoutInMs)
        {
        /* Check if hardware done, save and return */
        controlReg = m_AtHalMethods->Read(self, cRegisterIndirectControl);
        if (!IndirectIsInProgress(controlReg))
            {
            /* Read again to get new values */
            halIndirect->indirectControl = controlReg;
            halIndirect->indirectData = m_AtHalMethods->Read(self, cRegisterIndirectData);
            return;
            }

        /* Calculate elapse time */
        CurTimeGet(&curTime);
        timeOut = mTimeIntervalInMsGet(startTime, curTime);
        }
    /* Time out, just return */
    printk("ERROR: State save timeout\n");
    }

static void StateRestore(AtHal self)
    {
    AtHalIndirect halIndirect = (AtHalIndirect)self;
    m_AtHalMethods->Write(self, cRegisterIndirectData, halIndirect->indirectData);
    m_AtHalMethods->Write(self, cRegisterIndirectControl, halIndirect->indirectControl);
    }

static void Override(AtHal self)
    {
    if (!m_methodsInit)
        {
        /* Save super implementation */
        m_AtHalMethods = self->methods;
        memcpy(&m_AtHalOverride, (void *)m_AtHalMethods, sizeof(tAtHalMethods));

        /* Override read/write function */
        m_AtHalOverride.Read   = Read;
        m_AtHalOverride.Write  = Write;
        m_AtHalOverride.Delete = Delete;
        m_AtHalOverride.StateSave = StateSave;
        m_AtHalOverride.StateRestore = StateRestore;
        }

    self->methods = &m_AtHalOverride;
    }

AtHal AtHalIndirectObjectInit(AtHal self, uint32 baseAddress)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Reuse super construction */
    if (AtHalDefaultObjectInit(self, baseAddress) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHal AtHalIndirectNew(uint32 baseAddress)
    {
    /* Allocate memory */
    AtHal newHal = kmalloc(ObjectSize(), GFP_KERNEL);

    /* Construct it */
    return AtHalIndirectObjectInit(newHal, baseAddress);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : AtHal.h
 * 
 * Created Date: Jul 31, 2012
 *
 * Author      : namnn
 * 
 * Description : Hardware Abstraction Layer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _HAL_H_
#define _HAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*#define printk(FMT, PARMS...) trace_printk(FMT, ## PARMS)*/
#define printk    trace_printk

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief HAL class
 */
typedef struct tAtHal * AtHal;

/**
 * @brief HAL methods
 */
typedef struct tAtHalMethods
    {
    /* Delete HAL */
    void (*Delete)(AtHal self); /**< Delete HAL */

    /* Read/write interfaces. Note, address is local address of device */
    void (*Write)(AtHal self, uint32 address, uint32 value); /**< Write value to local register */
    uint32 (*Read)(AtHal self, uint32 address);              /**< Read local register */

    /* Save and restore indirect control registers */
    void (*StateSave)(AtHal self);
    void (*StateRestore)(AtHal self);
    }tAtHalMethods;

/**
 * @brief HAL class representation
 */
typedef struct tAtHal
    {
    const tAtHalMethods *methods;

    /* Private data */
    uint8 showRead;  /* To show read action */
    uint8 showWrite; /* To show read action */
    }tAtHal;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default implementation of HAL */
AtHal AtHalNew(uint32 baseAddress);
void AtHalDelete(AtHal self);

/* Access registers */
void AtHalWrite(AtHal self, uint32 address, uint32 value);
uint32 AtHalRead(AtHal self, uint32 address);

/* Show read/write */
void AtHalShowRead(AtHal self, eBool enable);
void AtHalShowWrite(AtHal self, eBool enable);

/* Set/Get HAL methods */
void AtHalMethodsSet(AtHal self, const tAtHalMethods *methods);
const tAtHalMethods *AtHalMethodsGet(AtHal self);

/* Other built-in HALs */
AtHal AtHalDefaultNew(uint32 baseAddress);
AtHal AtHalIndirectNew(uint32 baseAddress);
AtHal AtHalIndirect16BitNew(uint32 baseAddress);
AtHal AtHalIndirect24BitNew(uint32 baseAddress);
AtHal AtHalEp6New(void);

/* Save and restore control registers */
void AtHalStateSave(AtHal self);
void AtHalStateRestore(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _HAL_H_ */


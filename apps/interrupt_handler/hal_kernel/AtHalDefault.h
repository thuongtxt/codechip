/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtHalDefault.h
 * 
 * Created Date: Nov 15, 2012
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHALDEFAULT_H_
#define _ATHALDEFAULT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHalDefault * AtHalDefault;

typedef struct tAtHalDefaultMethods
    {
    char* (*BaseAddressDescriptionToString)(AtHal self, char *buffer);

    /* Self-test */
    uint32 *(*RegsToTest)(AtHal self, uint32 *numRegs);
    uint32 (*TestPattern)(AtHal hal);
    uint32 (*TestAntiPattern)(AtHal hal);
    void (*ShowControlRegisters)(AtHal self);
    uint32 (*DataBusMask)(AtHal self);
    }tAtHalDefaultMethods;

/* Default implementation of HAL */
typedef struct tAtHalDefault
    {
    tAtHal super;
    const tAtHalDefaultMethods *methods;

    /* Private variables */
    uint32 baseAddress;
    }tAtHalDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtHalDefaultObjectInit(AtHal self, uint32 baseAddress);
AtHal AtHalDefaultNew(uint32 baseAddress);
uint32 AtHalDefaultBaseAddress(AtHal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHALDEFAULT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform - HAL
 *
 * File        : hal.c
 *
 * Created Date: Jul 31, 2012
 *
 * Author      : namnn
 *
 * Description : Hardware Abstraction Layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <linux/kernel.h>
#include "AtHalDefault.h"
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * @addtogroup AtHal
 * @{
 */

/**
 * Create new default HAL
 *
 * @param baseAddress Base address
 *
 * @return Default HAL
 */
AtHal AtHalNew(uint32 baseAddress)
    {
    return AtHalDefaultNew(baseAddress);
    }

/**
 * Delete a HAL
 *
 * @param self This HAL
 */
void AtHalDelete(AtHal self)
    {
    if (self)
        self->methods->Delete(self);
    }

/**
 * Write a register
 *
 * @param self This HAL
 * @param address Local address of device
 * @param value Written value
 */
void AtHalWrite(AtHal self, uint32 address, uint32 value)
    {
    /* Cannot write a NULL HAL */
    if (self == NULL)
        return;

    self->methods->Write(self, address, value);

    /* Show write detail */
    if (self->showWrite)
        printk("Write 0x%08x, value 0x%08x\n", address, value);
    }

/**
 * Read a register
 *
 * @param self This HAL
 * @param address Local address of device
 *
 * @return Register value
 */
uint32 AtHalRead(AtHal self, uint32 address)
    {
    uint32 value = 0;

    /* Cannot read a NULL HAL */
    if (self == NULL)
        return 0;

    /* Read its register then show detail */
    value = self->methods->Read(self, address);

    if (self->showRead)
        printk("Read 0x%08x, value 0x%08x\n", address, value);

    return value;
    }

/**
 * Get methods
 * @param self
 * @return
 */
const tAtHalMethods *AtHalMethodsGet(AtHal self)
    {
    return self->methods;
    }

/**
 * Set methods
 * @param self
 * @param methods
 */
void AtHalMethodsSet(AtHal self, const tAtHalMethods *methods)
    {
    self->methods = methods;
    }

/**
 * Enable displaying reading detail
 *
 * @param self This HAL
 * @param enable Enable/disable displaying reading detail
 */
void AtHalShowRead(AtHal self, eBool enable)
    {
    self->showRead = enable;
    }

/**
 * Enable displaying writing detail
 *
 * @param self This HAL
 * @param enable Enable/disable displaying writing detail
 */
void AtHalShowWrite(AtHal self, eBool enable)
    {
    self->showWrite = enable;
    }

/**
 * Save content of Indirect control registers.
 * This API is used in Interrupt handler context,
 * it saves content of Indirect control registers
 * to protect these registers' content BEFORE
 * doing some interrupt process
 *
 * @param self
 */
void AtHalStateSave(AtHal self)
    {
    if (self == NULL)
        return;

    if(self->methods->StateSave == NULL)
        return;

    return self->methods->StateSave(self);
    }

/**
 * Restore content of Indirect control registers.
 * This API is used in Interrupt handler context,
 * it restores content of Indirect registers AFTER
 * finishing interrupt process
 *
 * @param self
 */
void AtHalStateRestore(AtHal self)
    {
    if (self == NULL)
        return;

    if (self->methods->StateRestore == NULL)
        return;

    return self->methods->StateRestore(self);
    }
/**
 * @}
 */

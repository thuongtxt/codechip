/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : interrupt_handler.c
 *
 * Created Date: Jan 15, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
/*
 * Standard in kernel modules
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <asm/uaccess.h>        /* for get_user and put_user */
#include <asm/io.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/slab.h>

#include "attypes.h"
#include "hal_kernel/AtHal.h"
#include "../../driver/src/implement/default/ppp/ThaMpigReg.h"
#include "commacro.h"

MODULE_LICENSE("GPL");

/*--------------------------- Define -----------------------------------------*/
#define cIrqHwNumber 17
#define cDeviceBaseAddress      0xD0000000
#define cNumServedOamsAtOneTime 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHal halk;
static unsigned int irq_number = 0;
static int hal_show = 0;
static int oam_num = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define cThaLongRegMaxSize 8
#define mChannelHwRead(hal, address) AtHalRead(hal, address)
#define mChannelHwWrite(hal, address, value) AtHalWrite(hal, address, value)
#define cOamReadyTimeOut         100  /* ms*/
#define cThaNumDwordsInLongReg   4
#define cThaMaxNumberReaddEngine 4
#define cAtErrorDevFail 1
#define cAtOk 0

/* Supported protocols */
#define cAtPppProtIdLcp  0xc021
#define cAtPppProtIdBcp  0x8031
#define cAtPppProtIdIpcp 0x8021

#define mKernelTimeIntervalInMsGet(prevTime, curTime) \
  (((((curTime).tv_sec - (prevTime).tv_sec) * 1000) + ((curTime).tv_usec/1000)) - ((prevTime).tv_usec/1000))

eBool ThaPppLinkHwIsReady(AtHal self, uint32 address, uint32 bitMask)
    {
    uint32  regValue;

    regValue = mChannelHwRead(self, address);
    if ((regValue & bitMask) != 0)
        return cAtFalse;

    return cAtTrue;
    }


eBool ThaHwFinished(AtHal self, uint32 address, uint32 bitMask, uint32 timeOut)
    {
    uint32 regValue;
    uint32 elapseTime;
    struct timeval curTime, startTime;

    /* Hardware finish its job */
    regValue = mChannelHwRead(self, address);
    if ((regValue & bitMask) == 0)
        return cAtTrue;

    /* Timeout waiting for it */
    elapseTime = 0;
    do_gettimeofday(&startTime);

    while (elapseTime < timeOut)
        {
        regValue = mChannelHwRead(self, address);
        if ((regValue & bitMask) == 0)
            return cAtTrue;

        /* Calculate elapse time */
        do_gettimeofday(&curTime);
        elapseTime = mKernelTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x840010, 0x840011, 0x840012};

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 3;

    return holdRegisters;
    }

static uint16 HwLongReadOnCore(AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    uint16 numberOfHoldRegisters;
    uint32 *holdRegisters = HoldRegistersGet(&numberOfHoldRegisters);
    uint16 i, numberOfReadDwords;

    /* Read data */
    dataBuffer[0] = AtHalRead(hal, localAddress);
    numberOfReadDwords = 1;
    for (i = 0; i < numberOfHoldRegisters; i++)
        {
        /* Buffer has no space left */
        if (bufferLen <= numberOfReadDwords)
            break;

        dataBuffer[i + 1] = AtHalRead(hal, holdRegisters[i]);
        numberOfReadDwords = numberOfReadDwords + 1;
        }

    return numberOfReadDwords;
    }

static uint32 OamReceive(AtHal self, uint8 *buffer, uint32 bufferLength, uint16* pLinkId)
    {
    uint32 regValue[cThaLongRegMaxSize];
    uint32 address, bitMask, pktLen;
    uint16 oamBlk;
    uint8  wIndex, *pBuffer, wIdx;
    eBool  discarded;

    /* Check if OAM is available */
    regValue[0] = mChannelHwRead(self, cThaRegMPIGDATDeQueOamInfoget);
    if ((regValue[0] & cThaOamReadyMask) == 0)
        {
        pktLen = 0;
        return cAtOk;
        }

    /* Get packet information */
    mFieldGet(regValue[0], cThaOamIDMask, cThaOamIDShift, uint16, &oamBlk);
    address = cThaRegMPIGDATDeQueCpuReadCtrl;
    bitMask = cThaReqEnbMask;
    if (!ThaPppLinkHwIsReady(self, address, bitMask))
        return cAtErrorDevFail;

    regValue[0] = 0;
    mFieldIns(&regValue[0], cThaBlkIdMask, cThaBlkIdShift, oamBlk);
    mFieldIns(&regValue[0], cThaMPIGDATDeQueCpuReadSoPMask, cThaMPIGDATDeQueCpuReadSoPShift, 0x1);
    mFieldIns(&regValue[0], cThaReqEnbMask, cThaReqEnbShift, 0x1);
    mChannelHwWrite(self, cThaRegMPIGDATDeQueCpuReadCtrl, regValue[0]);

    /* wait hardware finish */
    if (!ThaHwFinished(self, address, bitMask, cOamReadyTimeOut))
        return cAtErrorDevFail;

    /* HW finished, read information */
    HwLongReadOnCore(self, cThaRegMPIGDATDeQueOamDatacache, regValue, cThaNumDwordsInLongReg);

    mFieldGet(regValue[0], cThaDataLenMask, cThaDataLenShift, uint32, &pktLen);
    mFieldGet(regValue[0], cThaOamLinkIdMask, cThaOamLinkIdShift, uint16, pLinkId);
    for (wIndex = 1; wIndex < cThaMaxNumberReaddEngine; wIndex++)
        HwLongReadOnCore(self,
                            cThaRegMPIGDATDeQueOamDatacache + wIndex,
                            regValue,
                            cThaNumDwordsInLongReg);

    pktLen++;
    discarded = cAtFalse;
    if (bufferLength < pktLen)
        {
        pktLen = 31; /* So the last segment will be read to flush this invalid OAM out of FIFO */
        discarded = cAtTrue;
        }

    /* Get packet data */
    pBuffer = buffer;
    for (wIndex = 1; wIndex<= ((pktLen / 32) + 1); wIndex++)
        {
        regValue[0] = 0;
        mFieldIns(&regValue[0], cThaSegIDMask, cThaSegIDShift, wIndex);
        mFieldIns(&regValue[0], cThaBlkIdMask, cThaBlkIdShift, oamBlk);

        if (wIndex == ((pktLen / 32) + 1))
            {
            mFieldIns(&regValue[0],
                      cThaMPIGDATDeQueCpuReadEoPMask,
                      cThaMPIGDATDeQueCpuReadEoPShift,
                      1);
            }
        else
            {
            mFieldIns(&regValue[0],
                      cThaMPIGDATDeQueCpuReadEoPMask,
                      cThaMPIGDATDeQueCpuReadEoPShift,
                      0);
            }

        mFieldIns(&regValue[0],
                  cThaMPIGDATDeQueCpuReadSoPMask,
                  cThaMPIGDATDeQueCpuReadSoPShift,
                  0);

        mFieldIns(&regValue[0],
                  cThaReqEnbMask,
                  cThaReqEnbShift,
                  1);

        mChannelHwWrite(self,
                        cThaRegMPIGDATDeQueCpuReadCtrl,
                        regValue[0]);

        /* Wait for HW finish */
        if (!ThaHwFinished(self, address, bitMask, cOamReadyTimeOut))
            return cAtErrorDevFail;

        for (wIdx = 0; wIdx < cThaMaxNumberReaddEngine; wIdx++)
            {
            HwLongReadOnCore(self,
                               cThaRegMPIGDATDeQueOamDatacache + wIdx,
                               regValue,
                               cThaNumDwordsInLongReg);

            *pBuffer = (uint8)(regValue[1] >> 24);
            pBuffer++;
            *pBuffer = (uint8)(regValue[1] >> 16);
            pBuffer++;
            *pBuffer = (uint8)(regValue[1] >> 8);
            pBuffer++;
            *pBuffer = (uint8)(regValue[1]);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0] >> 24);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0] >> 16);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0] >> 8);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0]);
            pBuffer++;
            }
        }

    if (discarded == cAtTrue)
        {
        pktLen = 0;
        return cAtErrorDevFail;
        }

    return cAtOk;
    }

/*
 * Get byte
 */
static uint8 GetByte(const uint8 *p, const uint8 **pNext)
    {
    uint8 value = *((uint8*)p);
    if (pNext)
        *pNext = p + 1;
    return value;
    }

/*
 * Get word
 */
static uint16 GetWord(const uint8 *p, const uint8 **pNext)
    {
    uint16 value = *((uint16*)p);
    if (pNext)
        *pNext = p + 2;
    return value;
    }

/*
 * Get dword
 */
static uint32 GetDword(const uint8 *p, const uint8 **pNext)
    {
    uint32 value = *((uint32*)p);
    if (pNext)
        *pNext = p + 4;
    return value;
    }

static const char *ProtocolCode2String(uint16 code)
    {
    int i;
    static char buf[16];
    struct
        {
        uint16 code;
        const char *name;
        }codes[] = {
                    {0x0021, "Internet Protocol"},
                    {0x0023, "OSI Network Layer"},
                    {0x0025, "Xerox NS IDP"},
                    {0x0027, "DECnet Phase IV"},
                    {0x0029, "Appletalk"},
                    {0x002b, "Novell IPX"},
                    {0x002d, "Van Jacobson Compressed TCP/IP"},
                    {0x002f, "Van Jacobson Uncompressed TCP/IP"},
                    {0x0031, "Bridging PDU"},
                    {0x0033, "Stream Protocol (ST-II)"},
                    {0x0035, "Banyan Vines"},
                    {0x0201, "802.1d Hello Packets"},
                    {0x0231, "Luxcom"},
                    {0x0233, "Sigma Network Systems"},
                    {cAtPppProtIdIpcp, "Internet Protocol Control Protocol"},
                    {0x8023, "OSI Network Layer Control Protocol"},
                    {0x8025, "Xerox NS IDP Control Protocol"},
                    {0x8027, "DECnet Phase IV Control Protocol"},
                    {0x8029, "Appletalk Control Protocol"},
                    {0x802b, "Novell IPX Control Protocol"},
                    {cAtPppProtIdBcp, "Bridging NCP"},
                    {0x8033, "Stream Protocol Control Protocol"},
                    {0x8035, "Banyan Vines Control Protocol"},
                    {cAtPppProtIdLcp, "Link Control Protocol"},
                    {0xc023, "Password Authentication Protocol"},
                    {0xc025, "Link Quality Report"},
                    {0xc223, "Challenge Handshake Authentication Protocol"},
                    {-1, NULL},
                   };
    for (i = 0; codes[i].name; i++)
        {
        if (code == codes[i].code)
            return codes[i].name;
        }

    sprintf(buf, "Unknown(0x%hX)", code);
    return buf;
    }

static const char *Code2String(uint8 code)
    {
    int i;
    static char buf[16];

    struct
        {
        int code;
        const char *name;
        }codes[] = {
                   {1, "Configure-Request"},
                   {2, "Configure-Ack"},
                   {3, "Configure-Nak"},
                   {4, "Configure-Reject"},
                   {5, "Terminate-Request"},
                   {6, "Terminate-Ack"},
                   {7, "Code-Reject"},
                   {8, "Protocol-Reject"},
                   {9, "Echo-Request"},
                   {10,"Echo-Reply"},
                   {11,"Discard-Request"},
                   {-1, NULL},
                   };
    for (i = 0; codes[i].name; i++)
        {
        if (code == codes[i].code)
            return codes[i].name;
        }

    sprintf(buf, "Unknown(%d)", code);
    return buf;
    }

/*
 * Dump LCP options
 */
static char *LcpOptionDump(const uint8 *buffer, const uint8 **p)
    {
    static char optionStr[64];
    uint8 len, type;
    const uint8 *next = buffer;

    type = GetByte(next, &next);
    len  = GetByte(next, &next);

    /* LCP options */
    if (type == 1)
        sprintf(optionStr, "MRU: %d", GetWord(next, &next));
    else if (type == 3)
        sprintf(optionStr, "Authentication-Protocol: %d", GetWord(next, &next));
    else if (type == 4)
        sprintf(optionStr, "Quality-Protocol: %d", GetWord(next, &next));
    else if (type == 5)
        sprintf(optionStr, "Magic-Number: %d", GetWord(next, &next));
    else if (type == 7)
        sprintf(optionStr, "Protocol-Field-Compression (PFC)");
    else if (type == 8)
        sprintf(optionStr, "Address-and-Control-Field-Compression (ACFC)");

    /* MLPPP options */
    else if (type == 17)
        sprintf(optionStr, "MRRU: %d", GetWord(next, &next));
    else if (type == 18)
        sprintf(optionStr, "Short sequence");
    else if (type == 19)
        sprintf(optionStr, "End-point Discriminator: class %d", GetByte(next, &next));
    else if (type == 26)
        sprintf(optionStr, "Prefix elision");
    else if (type == 27)
        sprintf(optionStr, "Multilink header format, code = %d, Susp Clses = %d", GetByte(next, &next), GetByte(next, &next));

    /* Unknown option */
    else
        sprintf(optionStr, "Unknown option %d", type);

    if (p)
        *p = buffer + len;
    return optionStr;
    }

/*
 * Dump BCP options
 */
static char *BcpOptionDump(const uint8 *buffer, const uint8 **p)
    {
    static char optionStr[64];
    uint8 len, type;
    const uint8 *next = buffer;

    type = GetByte(next, &next);
    len  = GetByte(next, &next);

    if (type == 1)
        sprintf(optionStr, "Bridge-Identification");
    else if (type == 2)
        sprintf(optionStr, "Line-Identification");
    else if (type == 3)
        sprintf(optionStr, "MAC-Support");
    else if (type == 7)
        sprintf(optionStr, "Spanning-Tree-Protocol");
    else if (type == 8)
        sprintf(optionStr, "IEEE-802-Tagged-Frame");
    else if (type == 9)
        sprintf(optionStr, "Management-Inline");
    else if (type == 10)
        sprintf(optionStr, "Bridge-Control-Packet-Indicator");

    /* Unknown option */
    else
        sprintf(optionStr, "Unknown option %d", type);

    if (p)
        *p = buffer + len;
    return optionStr;
    }

/*
 * Get IP address from dword value
 */
static char *IpAddrFromDword(uint32 ipAddr)
    {
    static char buf[32];
    int i;
    const uint8 *p = (const uint8 *)&ipAddr;

    buf[0] = '\0';
    for (i = 0; i < 4; i++)
        sprintf(buf, "%s%d.", buf, GetByte(p, &p));
    buf[strlen(buf) - 1] = '\0'; /* Remove the last '. */

    return buf;
    }

/*
 * Dump IPCP options
 */
static char *IpcpOptionDump(const uint8 *buffer, const uint8 **p)
    {
    static char optionStr[64];
    uint8 len, type;
    const uint8 *next = buffer;

    type = GetByte(next, &next);
    len  = GetByte(next, &next);

    if (type == 2)
        sprintf(optionStr, "IP-Compression-Protocol");
    else if (type == 3)
        sprintf(optionStr, "IP-Address: %s", IpAddrFromDword(GetDword(next, &next)));

    /* Unknown option */
    else
        sprintf(optionStr, "Unknown option %d", type);

    if (p)
        *p = buffer + len;
    return optionStr;
    }

/*
 * Dump LCP packet
 */
static void LcpPacketDump(const uint8 *lcpPacketBuf)
    {
    const uint8 *p = lcpPacketBuf;
    uint8 code;
    uint16 len;

    code = GetByte(p, &p);
    printk("- Code: %s\r\n", Code2String(code));
    printk("- Identifier: %d\r\n", GetByte(p, &p));
    len = GetWord(p, &p);
    printk("- Length: %d\r\n", len);

    /* Configure-Request, Configure-Ack, Configure-Nak */
    if ((code == 1) ||
        (code == 2) ||
        (code == 3) ||
        (code == 4))
        {
        printk("- Options:\r\n");
        while ((p - lcpPacketBuf) < len)
            printk("  + %s\r\n", LcpOptionDump(p, &p));
        }
    }

/*
 * Dump BCP packet
 */
static void BcpPacketDump(const uint8 *lcpPacketBuf)
    {
    const uint8 *p = lcpPacketBuf;
    uint8 code;
    uint16 len;

    code = GetByte(p, &p);
    printk("- Code: %s\r\n", Code2String(code));
    printk("- Identifier: %d\r\n", GetByte(p, &p));
    len = GetWord(p, &p);
    printk("- Length: %d\r\n", len);

    /* Configure-Request, Configure-Ack, Configure-Nak */
    if ((code == 1) ||
        (code == 2) ||
        (code == 3) ||
        (code == 4))
        {
        printk("- Options:\r\n");
        while ((p - lcpPacketBuf) < len)
            printk("  + %s\r\n", BcpOptionDump(p, &p));
        }
    }

/*
 * Dump IPCP packet
 */
static void IpcpPacketDump(const uint8 *lcpPacketBuf)
    {
    const uint8 *p = lcpPacketBuf;
    uint8 code;
    uint16 len;

    code = GetByte(p, &p);
    printk("- Code: %s\r\n", Code2String(code));
    printk("- Identifier: %d\r\n", GetByte(p, &p));
    len = GetWord(p, &p);
    printk("- Length: %d\r\n", len);

    /* Configure-Request, Configure-Ack, Configure-Nak */
    if ((code == 1) ||
        (code == 2) ||
        (code == 3) ||
        (code == 4))
        {
        printk("- Options:\r\n");
        while ((p - lcpPacketBuf) < len)
            printk("  + %s\r\n", IpcpOptionDump(p, &p));
        }
    }

/*
 * To show the meaningful format
 *
 * @param packetBuf Packet buffer
 */
void PppPacketDump(uint16 linkId, const uint8 *packetBuf)
    {
    const uint8 *p = packetBuf;
    uint16 protocol;

    protocol = GetWord(p, &p);
    printk("(Link %d)protocol: %s\r\n", linkId, ProtocolCode2String(protocol));;

    if (protocol == cAtPppProtIdLcp)
        LcpPacketDump(p);
    if (protocol == cAtPppProtIdBcp)
        BcpPacketDump(p);
    if (protocol == cAtPppProtIdIpcp)
        IpcpPacketDump(p);
    }

/*
 * This function services interrupts
 */
irqreturn_t irq_handler(int irq, void *dev_id)
    {
    uint32 enabledInterrupt, i;
    static uint8 messageBuffer[128];
    uint16 linkId = 0;

    /* Save indirect state */
    AtHalStateSave(halk);

     /* Disable global interrupt */
    enabledInterrupt = AtHalRead(halk, 0xf);
    AtHalWrite(halk, 0xf, 0x0);
    printk("Interrupt %d acknowledge!\n", irq);

    if (oam_num == 0)
        oam_num = cNumServedOamsAtOneTime;

    for (i = 0; i < oam_num; i++)
        {
        /* Get OAM packet and print it out */
        OamReceive(halk, messageBuffer, sizeof(messageBuffer), &linkId);
        PppPacketDump(linkId, messageBuffer);
        }

    /* Clear sticky and clear OAM interrupt if there is no packet */
    AtHalWrite(halk, 0x844005, 0xf);

    /* Enable global interrupt */
    AtHalWrite(halk, 0xf, enabledInterrupt);

    /* Restore indirect state */
    AtHalStateRestore(halk);

    return IRQ_HANDLED;
    }

module_param(hal_show, int, S_IRUGO);
module_param(oam_num, int, S_IRUGO);

int init_module( void )
    {
    int irq_return = 0;

    /**
     * Create HAL indirect
     */
    halk = AtHalIndirectNew(0xD0000000);
    if (halk == NULL)
        return EBUSY;

    if (hal_show)
        {
        AtHalShowRead(halk, cAtTrue);
        AtHalShowWrite(halk, cAtTrue);
        }

    /**
     * irq_create_mapping() - Map a hardware interrupt into linux irq space
     * @domain: domain owning this hardware interrupt or NULL for default domain
     * @hwirq: hardware irq number in that domain space
     *
     * Only one mapping per hardware interrupt is permitted. Returns a linux
     * irq number.
     * If the sense/trigger is to be specified, set_irq_type() should be called
     * on the number returned from that call.
     */
    irq_number = irq_create_mapping(NULL, cIrqHwNumber);
    printk("IRQ number %d\n", irq_number);

    if (irq_number > 0)
        {
        irq_return = request_irq(irq_number,
                                 irq_handler, /* our handler */
                                 IRQF_DISABLED , "AT_interrupt_handler",
                                 NULL);
        printk("Request IRQ return %d\n", irq_return);
        }

    if (irq_return != 0)
        return EBUSY;

    return 0;
    }

void cleanup_module( void )
    {
    if (irq_number > 0)
        {
        free_irq(irq_number, NULL);

        /**
         * irq_dispose_mapping() - Unmap an interrupt
         * @virq: linux irq number of the interrupt to unmap
         */
        irq_dispose_mapping(irq_number);
        }

    if (halk)
        AtHalDelete(halk);

    return;
    }


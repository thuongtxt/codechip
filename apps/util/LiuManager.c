/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : LiuManager.c
 *
 * Created Date: Jul 11, 2014
 *
 * Description : LIU Manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtLiuManager.h"
#include "AtHalLoop.h"
#include "apputil.h"
#include "../../cli/cmd/platform/liu/CliAtLiu.h"
#include "LiuManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtLiuManager m_liuManager = NULL;

static AtHal m_createdHal = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtLiuManager AtLoopTelLiuManagerNew(void);

/*--------------------------- Implementation ---------------------------------*/
static AtLiuManager ManagerCreate(ePlatform platform)
    {
    if (platform == cPlatformLoop32E1)
        {
        return AtLoopTelLiuManagerNew();
        }

    if ((platform == cPlatformEp6C1) || AppIsSimulationPlatform(platform))
        return AtLiuDefaultManagerNew();

    return NULL;
    }

static AtHal LiuHal(AtDevice device, ePlatform platform)
    {
    if (platform == cPlatformLoop32E1)
        return AtHalLoopLiu(AtDeviceIpCoreHalGet(device, 0));

    AtHalDelete(m_createdHal);

    if (platform == cPlatformEp6C1)
        {
#ifdef __NO_SPI__
        return NULL;
#endif

#ifdef __ARCH_POWERPC__
        m_createdHal = AtEpHalSpiNew();
        return m_createdHal;
#else
        return NULL;
#endif
        }

    if (AppIsSimulationPlatform(platform))
        {
        m_createdHal = AtEpHalSimNew();
        return m_createdHal;
        }

    return NULL;
    }

eBool LiuManagerSetup(AtDevice device, ePlatform aPlatform)
    {
    eBool success;

    /* Try creating LIU manager */
    if (m_liuManager == NULL)
        m_liuManager = ManagerCreate(aPlatform);
    if (m_liuManager == NULL)
        return cAtTrue;

    /* Setup HAL and initialize LIU */
    AtLiuManagerHalSet(m_liuManager, LiuHal(device, aPlatform));
    success = (AtLiuManagerInit(m_liuManager) == cAtEpRetOk) ? cAtTrue : cAtFalse;

    CliAtLiuManagerSet(m_liuManager);

    return success;
    }

void LiuManagerDelete(void)
    {
    AtObjectDelete((AtObject)m_liuManager);
    AtHalDelete(m_createdHal);
    }

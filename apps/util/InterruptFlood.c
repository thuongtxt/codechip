/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : App util
 *
 * File        : InterruptFlood.c
 *
 * Created Date: Feb 25, 2017
 *
 * Description : To manage interrupt flooding
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtIpCore.h"
#include "tasks.h"
#include "commacro.h"
#include "apputil.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumInterruptsInOneSecond (1000000 / cAtTaskInterruptPeriodUs)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tInterruptProfile
    {
    eBool happened;
    tAtOsalCurTime startTime;
    tAtOsalCurTime changeStartTime;
    uint32 totalCount;
    uint32 contiguousCount;

    tAtOsalCurTime curTime;
    tAtOsalCurTime previousReportedTime; /* To check if pooling task is running */
    }tInterruptProfile;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_watched = cAtTrue;
static uint32 m_continuousInterruptThreshold = cMaxNumInterruptsInOneSecond / 10;
static uint32 m_totalInterruptThreshold = cMaxNumInterruptsInOneSecond / 5;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HappenFrequently(tInterruptProfile *info)
    {
    eBool frequently = cAtFalse;

    if ((info->previousReportedTime.sec == 0) && (info->previousReportedTime.usec == 0))
        {
        AtOsalMemCpy(&info->previousReportedTime, &info->curTime, sizeof(tAtOsalCurTime));
        return frequently;
        }

    if (mTimeIntervalInMsGet(info->previousReportedTime, info->curTime) <= 500)
        frequently = cAtTrue;

    AtOsalMemCpy(&info->previousReportedTime, &info->curTime, sizeof(tAtOsalCurTime));

    return frequently;
    }

static void UpdateInterruptCounters(tInterruptProfile *info)
    {
    info->totalCount = info->totalCount + 1;
    info->contiguousCount = info->contiguousCount + 1;
    }

static void FloodingCheck(tInterruptProfile *info)
    {
    uint32 elapseTimeMs = mTimeIntervalInMsGet(info->changeStartTime, info->curTime);
    uint32 numInterruptCountConstrain = m_continuousInterruptThreshold;
    eBool persist = (elapseTimeMs >= 1000) ? cAtTrue : cAtFalse;

    /* Note, this is || not && */
    if (persist || (info->contiguousCount >= numInterruptCountConstrain))
        {
        AtPrintc(cSevCritical, "[CRITICAL] ");
        AtPrintc(cSevNormal, "(%s: %d) Interrupt happens continuously within %d(ms), %d times\r\n", AtSourceLocation, elapseTimeMs, info->contiguousCount);

        /* To start new counter */
        if (info->contiguousCount >= numInterruptCountConstrain)
            info->contiguousCount = 0;

        /* To start new soaking time */
        if (persist)
            AtOsalMemCpy(&info->changeStartTime, &info->curTime, sizeof(tAtOsalCurTime));
        }
    }

static void InterruptCountConstrainCheck(tInterruptProfile *info)
    {
    uint32 elapseTimeMs = mTimeIntervalInMsGet(info->startTime, info->curTime);

    /* Although interrupt may not happen contiguously, but too many interrupts
     * happen in a specific duration (1 second) must be small. This is to enforce
     * interrupt flooding improvement */
    if (elapseTimeMs < 1000)
        return;

    if (info->totalCount >= m_totalInterruptThreshold)
        {
        AtPrintc(cSevCritical, "[CRITICAL] ");
        AtPrintc(cSevNormal, "(%s, %d) Interrupt happens many times within %d (ms), %d times\r\n", AtSourceLocation, elapseTimeMs, info->totalCount);
        }

    info->totalCount = 0;
    AtOsalCurTimeGet(&(info->startTime));
    }

static void InterruptHappened(AtIpCore core, void *userData)
    {
    tInterruptProfile *info = userData;
    eBool frequently;
    eBool hasJustHappened;

    AtUnused(core);

    if (!m_watched)
        return;

    UpdateInterruptCounters(info);
    AtOsalCurTimeGet(&info->curTime);
    frequently = HappenFrequently(info);
    if (!frequently)
        {
        info->happened = cAtFalse;
        return;
        }

    /* It has just happened, record its start time */
    hasJustHappened = info->happened ? cAtFalse : cAtTrue;
    if (hasJustHappened)
        AtOsalMemCpy(&info->changeStartTime, &info->curTime, sizeof(tAtOsalCurTime));

    /* Check if flooding is happening */
    FloodingCheck(info);
    InterruptCountConstrainCheck(info);

    info->happened = cAtTrue;
    }

static void CoreInterruptResolved(AtIpCore core, void *userData)
    {
    tInterruptProfile *info = userData;

    if (!m_watched)
        return;

    AtOsalMemInit(info, 0, sizeof(tInterruptProfile));
    AtUnused(core);
    }

static void CoreWillRemoveListener(AtIpCore core, void *userData)
    {
    AtUnused(core);
    AtOsalMemFree(userData);
    }

static const tAtIpCoreListener *CoreListener(void)
    {
    static tAtIpCoreListener listener;
    static tAtIpCoreListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.InterruptHappened  = InterruptHappened;
    listener.InterruptResolved  = CoreInterruptResolved;
    listener.WillRemoveListener = CoreWillRemoveListener;

    pListener = &listener;

    return pListener;
    }

static void CoreListen(AtIpCore core)
    {
    tInterruptProfile *information = AtOsalMemAlloc(sizeof(tInterruptProfile));
    AtOsalMemInit(information, 0, sizeof(tInterruptProfile));
    AtOsalCurTimeGet(&(information->startTime));
    AtIpCoreListenerAdd(core, CoreListener(), information);
    }

static void DefaultThresholdSetup(AtDevice device)
    {
    uint32 productCode;

    if (device == NULL)
        return;

    productCode = AtDeviceProductCodeGet(device);
    if ((productCode == 0x60290021) ||
        (productCode == 0x60290022) ||
        (productCode == 0x60290011))
        {
        m_continuousInterruptThreshold = 15;
        m_totalInterruptThreshold      = 20;
        }
    }

void AppAllCoresInterruptListen(AtDevice device)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(device, core_i);
        CoreListen(core);
        }

    DefaultThresholdSetup(device);
    }

void AppInterruptFloodingWatch(eBool watched)
    {
    m_watched = watched;
    }

void AppInterruptFloodingContinuousThresholdSet(uint32 threshold)
    {
    m_continuousInterruptThreshold = threshold;
    }

void AppInterruptFloodingTotalThresholdSet(uint32 threshold)
    {
    m_totalInterruptThreshold = threshold;
    }

void AppInterruptFloodingShow(void)
    {
    AtPrintc(cSevNormal, "* Watched: ");
    AtPrintc(m_watched ? cSevInfo : cSevCritical, "%s\r\n", m_watched ? "enabled" : "disabled");
    AtPrintc(cSevNormal, "* Continuous threshold: %d (interrupts/second)\r\n", m_continuousInterruptThreshold);
    AtPrintc(cSevNormal, "* Total interrupt threshold: %d (interrupts/second)\r\n", m_totalInterruptThreshold);
    }

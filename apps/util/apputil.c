/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : apputil.c
 *
 * Created Date: Feb 3, 2013
 *
 * Description : Application util
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <string.h>
#ifndef VXWORKS
#include <getopt.h>
#endif
#include "errno.h"
#include "unistd.h"
#include "sys/ioctl.h"
#include <fcntl.h>
#include "sys/types.h"
#include "sys/wait.h"
#include "sys/stat.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "atclib.h"
#include "AtOsal.h"
#include "AtDriver.h"
#include "AtHalTcp.h"
#include "commacro.h"
#include "AtTokenizer.h"

#include "tasks.h"
#include "apputil.h"
#include "MemoryMap.h"
#include "../ep5/ep5.h"
#include "../xilinx/xilinx.h"
#include "AtAluPci.h"
#include "loop.h"
#include "zte.h"
#include "AtFiberLogicPlatform.h"
#include "ZteRouter.h"
#include "AtHalFiberLogic.h"
#include "AtHalFhw.h"
#include "HalFactory.h"
#include "AtHalXilinx.h"
#include "FpgaVersion.h"
#include "AtHalTestbench.h"
#include "../customers/loop/LoopPlatform.h"

/*--------------------------- Define -----------------------------------------*/
#define cProgramStringLength      256

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static  char m_progName[cProgramStringLength];
static atbool    m_needHelp;
static atbool    m_isDaemon;
static ePlatform m_platform = cPlatformUnknown;
static uint32    m_simulateProductCode  = 0;
static uint32    m_specifiedProductCode = 0;
static uint32 socketListenPort = 8765;

static char m_remoteHalServer[128];
static uint16 m_halListenPort = 0;
static uint16 m_halEventListenPort = 0;

/* Test bench */
static char m_testbenchServer[128];
static uint32 m_testbenchPort = 0;

static eBool m_doNotAddDevice = cAtFalse;

static uint32 m_phyBaseAddress = 0;

/* Number of SDH lines */
static uint32    m_specifiedNumberOfSdhLines = 0;

/* CLI server */
static uint16 m_cliServerListenPort = 0;

static atbool m_isFileLogger = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/
extern void Tha61210011ModuleSdhNumLines(uint8 numLines);

/*--------------------------- Implementation ---------------------------------*/
static ePlatform RealPlatform(char *platformName)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(platformName, ":");
    char *platformType = AtTokenizerNextString(tokenizer);
    ePlatform platform = cPlatformUnknown;

    if (strcmp(platformType, cStrPlatformEp5)               == 0) platform = cPlatformEp5;
    if (strcmp(platformType, cStrPlatformEp6)               == 0) platform = cPlatformEp6;
    if (strcmp(platformType, cStrPlatformEp6C1)             == 0) platform = cPlatformEp6C1;
    if (strcmp(platformName, cStrPlatform16BitEp6C1)        == 0) platform = cPlatform16BitEp6C1;
    if (strcmp(platformType, cStrPlatformEp6C2)             == 0) platform = cPlatformEp6C2;
    if (strcmp(platformType, cStrPlatformH3C)               == 0) platform = cPlatformH3C;
    if (strcmp(platformType, cStrPlatformLoop16E1)          == 0) platform = cPlatformLoop16E1;
    if (strcmp(platformType, cStrPlatformLoop32E1)          == 0) platform = cPlatformLoop32E1;
    if (strcmp(platformType, cStrPlatformLoopStm)           == 0) platform = cPlatformLoopStm;
    if (strcmp(platformType, cStrPlatformZtePtn)            == 0) platform = cPlatformZtePtn;
    if (strcmp(platformName, cStrPlatformZteRouterStm)      == 0) platform = cPlatformZteRouterStm;
    if (strcmp(platformType, cStrPlatformAlu)               == 0) platform = cPlatformAlu;
    if (strcmp(platformType, cStrPlatformFiberLogic)        == 0) platform = cPlatformFiberLogic;
    if (strcmp(platformType, cStrPlatformXilinx)            == 0) platform = cPlatformXilinx;
    if (strcmp(platformType, cStrPlatformAnna)         == 0) platform = cPlatformAnna;

    if (AtTokenizerHasNextString(tokenizer))
        StrToDw(AtTokenizerNextString(tokenizer), 'h', &m_phyBaseAddress);

    return platform;
    }

static AtHal CreateHalSim(uint32 baseAddress)
    {
    AtUnused(baseAddress);
    return AtHalSimCreateForProductCode(AppProductCodeGet(cPlatformSimul), cAtHalSimDefaultMemorySizeInMbyte);
    }

static uint32 PutFamilyToCode(uint32 realProductCode, uint8 familyCode)
    {
    uint32 newCode = realProductCode;

    mFieldIns(&newCode, cBit31_28, 28, familyCode);

    return newCode;
    }

static eBool IsEpPlatform(ePlatform platform)
    {
    if ((platform == cPlatformEp5)        ||
        (platform == cPlatformEp6)        ||
        (platform == cPlatformEp6C1)      ||
        (platform == cPlatform16BitEp6C1) ||
        (platform == cPlatformEp6C2)      ||
        (platform == cPlatformXilinx))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 CorrectProductCode(uint32 realCode)
    {
    if (!IsEpPlatform(AppPlatform()))
        return realCode;

    return AppEpProductCode(realCode);
    }

const char *AppName(void)
    {
    return m_progName;
    }

ePlatform AppPlatform(void)
    {
    return m_platform;
    }

eBool AppIsDeamon(void)
    {
    return m_isDaemon;
    }

eBool AppNeedToDisplayHelp(void)
    {
    return m_needHelp;
    }

eBool AppIsSimulationPlatform(ePlatform plat)
    {
    return (plat == cPlatformSimul) ? cAtTrue : cAtFalse;
    }

AtHal AppCreateHal(uint32 baseAddress, ePlatform plat)
    {
    if ((plat == cPlatformEp6C1) || (plat == cPlatformEp6C2))
        return AtHalIndirectEpNew(baseAddress);

    if (plat == cPlatform16BitEp6C1)
        return AtHalFhw16BitNew(baseAddress); /* Use old methods of FHW */

    if (plat == cPlatformEp6)
        return AtHalEp6New();

    if (plat == cPlatformEp5)
        return AtHalEp5New(baseAddress);

    if (plat == cPlatformXilinx)
        return AtHalXilinxNew(baseAddress);

    if (plat == cPlatformLoop16E1)
        return Loop16E1HalCreate();

    if (plat == cPlatformLoop32E1)
        return Loop32E1HalCreate();

    if (plat == cPlatformLoopStm)
        return LoopStmHalCreate();

    if (plat == cPlatformZtePtn)
        return ZtePtnHalCreate();

    if (AppPlatform() == cPlatformAlu)
        return AtAluHalCreate();

    if (plat == cPlatformZteRouterStm)
        return ZteRouterStmHalCreate(baseAddress);

    if (plat == cPlatformFiberLogic)
        return AtHalFiberLogicNew(AtFiberLogicPlatformFpgaBaseAddress());

    if (plat == cPlatformRemote)
        return AtHalTcpNew(m_remoteHalServer, m_halListenPort);

    if (plat == cPlatformTestbench)
        return AtHalTestbenchTcpNew(m_testbenchServer, (uint16)m_testbenchPort);

    if (plat == cPlatformAnna)
        {
        AppMemoryMapDeviceSet("/dev/mem");
        AppMemoryMapPhysicalOffsetSet(0xfd0000000LL);
        AppMemoryMapMemorySizeInBytesSet(0x8000000);
        }

    if (AppMemoryMapDeviceGet())
        {
        AtHal hal = AppMemoryMapHalCreate();
        AtHalMaxAddressSet(hal, (uint32)(AppMemoryMapMemorySizeInBytesGet() / 4)); /* /4 for number of dwords */
        return hal;
        }

    return CreateHalSim(baseAddress);
    }

uint32 AppBaseAddressOfCore(uint8 coreId)
    {
    if (AppPlatform() == cPlatformEp5)
        return Ep5PlatformFpgaBaseAddress();
    if (AppPlatform() == cPlatformXilinx)
        return XilinxPlatformFpgaBaseAddress(m_phyBaseAddress);
    if (AppPlatform() == cPlatformZteRouterStm)
        return ZteRouterStmBaseAddressGet(coreId);

    if (AppMemoryMapDeviceGet())
        return 0; /* This does not matter */

    return (0xD0000000 + (coreId * 0x10000000UL));
    }

uint32 AppProductCodeGet(ePlatform plat)
    {
    uint32 code;
    AtHal hal;

    /* Simulate platform */
    if (AppIsSimulationPlatform(plat))
         return m_simulateProductCode;

    /* Read product code if user does not specify */
    code = AppSpecifiedProductCode();
    if (code == 0)
        {
        hal = AppCreateHal(AppBaseAddressOfCore(0), plat);
        code = AtProductCodeGetByHal(hal);
        AtHalDelete(hal);
        }

    return CorrectProductCode(code);
    }

static void ParseHalRemoteConnection(char *connection)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(connection, ":");

    m_platform = cPlatformRemote;
    AtSprintf(m_remoteHalServer, "%s", AtTokenizerNextString(tokenizer));

    if (AtTokenizerHasNextString(tokenizer))
        m_halListenPort = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));

    if (AtTokenizerHasNextString(tokenizer))
        m_halEventListenPort = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));
    }

static void ParseTestbenchConnection(char *connection)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(connection, ":");

    m_platform = cPlatformTestbench;
    AtSprintf(m_testbenchServer, "%s", AtTokenizerNextString(tokenizer));

    if (AtTokenizerHasNextString(tokenizer))
        m_testbenchPort = AtStrToDw(AtTokenizerNextString(tokenizer));
    }

const char *AppOptionParseDefaultFormat(void)
    {
    return "hDs:p:l:L:P:r:e:nt:N:u:fw:m:";
    }

atbool AppOptionParse(int argc, char* argv[])
    {
    return AppOptionParseWithFormat(argc, argv, NULL);
    }

atbool AppOptionParseWithFormat(int argc, char* argv[], const char *format)
    {
    int opt;
    const char *optString = format;
    uint32 port;

    if (optString == NULL)
        optString = AppOptionParseDefaultFormat();

    /* Get the application name */
    AtOsalMemInit((void*)m_progName, 0, sizeof(m_progName));
    AtStrncpy(m_progName, argv[0], (cProgramStringLength - 1));

    m_needHelp        = cAtFalse;
    m_isDaemon        = cAtFalse;
    m_platform        = cPlatformUnknown;

    while ((opt = getopt( argc, argv, optString)) != AtEof())
        {
        switch (opt)
            {
            case 'h':
                m_needHelp = cAtTrue;
                break;
            case 'D':
                m_isDaemon = cAtTrue;
                break;

            /* Simulate */
            case 's':
                {
                char optStr[256];

                if ((m_platform != cPlatformRemote) &&
                    (m_platform != cPlatformTestbench))
                    m_platform = cPlatformSimul;

                AtOsalMemInit((void*)optStr, 0, sizeof(optStr));
                if (sscanf(optarg, "%s", optStr) != 1)
                    {
                    mPrintError("Please input product code");
                    return cAtFalse;
                    }

                StrToDw(optStr, 'h', &m_simulateProductCode);
                }
                break;

            /* Remote control */
            case 'r':
                ParseHalRemoteConnection(optarg);
                break;

            /* Testbench */
            case 't':
                ParseTestbenchConnection(optarg);
                break;

            /* Platform */
            case 'p':
                {
                char optStr[256];
                AtOsalMemInit((void*)optStr, 0, sizeof(optStr));
                if (sscanf(optarg, "%s", optStr) != 1)
                    {
                    mPrintError(cStrInputPlatform);
                    return cAtFalse;
                    }

                /* Get platform type */
                /* Try to get real platform. If cannot get it, try detecting simulate platform type */
                m_platform = RealPlatform(optStr);
                if (m_platform == cPlatformUnknown)
                    {
                    mPrintError("Invalid platform");
                    return cAtFalse;
                    }
                }
                break;

            case 'l':
                if( sscanf( optarg, "%d", &socketListenPort ) != 1 )
                    {
                    return cAtFalse;
                    }
                break;

            case 'L':
                if( sscanf( optarg, "%d", &port) != 1 )
                    {
                    return cAtFalse;
                    }
                m_halListenPort = (uint16)port;
                break;

            case 'e':
                if( sscanf( optarg, "%d", &port) != 1 )
                    {
                    return cAtFalse;
                    }
                m_halEventListenPort = (uint16)port;
                break;

            case 'P':
                StrToDw(optarg, 'h', &m_specifiedProductCode);
                break;

            case 'n':
                m_doNotAddDevice = cAtTrue;
                break;

            case 'N':
                StrToDw(optarg, 'd', &m_specifiedNumberOfSdhLines);
                Tha61210011ModuleSdhNumLines((uint8)m_specifiedNumberOfSdhLines);
                break;

            case 'u':
                if( sscanf( optarg, "%d", &port) != 1 )
                    return cAtFalse;
                m_cliServerListenPort = (uint16)port;
                break;

            case 'f':
                m_isFileLogger = cAtTrue;
                break;

            case 'w':
                if (!AppFpgaVersionParse(optarg))
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: please input correct FPGA version with expected format: \"%s\"\r\n",
                             AppFpgaVersionFormat());
                    return cAtFalse;
                    }
                break;

            case 'm':
                if (!AppMemoryMapParseInput(optarg))
                    return cAtFalse;
                break;
            default:
                break;
            }
        }

    return cAtTrue;
    }

uint32 AppListenPortGet(void)
    {
    return socketListenPort;
    }

uint32 AppSpecifiedProductCode(void)
    {
    return m_specifiedProductCode;
    }

uint32 AppSimulationProductCode(uint32 realProductCode)
    {
    return PutFamilyToCode(realProductCode, 0xf);
    }

uint32 AppEpProductCode(uint32 realProductCode)
    {
    return PutFamilyToCode(realProductCode, 0x8);
    }

uint32 AppHalListenPortGet(void)
    {
    return m_halListenPort;
    }

uint32 AppHalEventNotifyPortGet(void)
    {
    return m_halEventListenPort;
    }

eBool AppDoNotAddDevice(void)
    {
    return m_doNotAddDevice;
    }

uint16 AppCliServerListenPortGet(void)
    {
    return m_cliServerListenPort;
    }

eBool AppIsFileLogger(void)
    {
    return m_isFileLogger;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : MemoryMap.c
 *
 * Created Date: Jan 17, 2017
 *
 * Description : To handle memory map
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "atclib.h"
#include "AtTokenizer.h"
#include "MemoryMap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_memoryMapDevice[128];
static char *p_memoryMapDevice = NULL;
static long long m_memoryMapPhysicalOffset = 0;
static long long m_memoryMapMemorySizeInBytes = 0;
static uint32 m_memoryMapVirtualBaseAddress = cInvalidUint32;

/*--------------------------- Forward declarations ---------------------------*/
extern void *mmap64 (void * addr, size_t len, int prot, int flags, int fildes, long long off);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MemoryMap(void)
    {
    int fd;
    long long offset = AppMemoryMapPhysicalOffsetGet();
    void *baseAddress;

    if (AppMemoryMapDeviceGet() == NULL)
        return cInvalidUint32;

    fd = open(AppMemoryMapDeviceGet(), O_RDWR);
    if (fd < 0)
        {
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidUint32;
        }

    baseAddress = mmap64(NULL, (size_t)AppMemoryMapMemorySizeInBytesGet(), PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
    if (baseAddress == ((void *)-1))
        {
        close(fd);
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidUint32;
        }

    close(fd);

    return (uint32)(AtSize)baseAddress;
    }

static void MemoryUnMap(void)
    {
    int fd;

    if (AppMemoryMapDeviceGet() == NULL)
        return;

    /* Not mapped yet */
    if (m_memoryMapVirtualBaseAddress == cInvalidUint32)
        return;

    fd = open(AppMemoryMapDeviceGet(), O_RDWR);
    if (fd == -1)
        return;

    munmap((void *)((size_t)m_memoryMapVirtualBaseAddress), (size_t)AppMemoryMapMemorySizeInBytesGet());
    close(fd);

    m_memoryMapVirtualBaseAddress = cInvalidUint32;
    }

static AtHal CreateHalWithMemoryMappingInfo(void)
    {
    /* Already map */
    if (m_memoryMapVirtualBaseAddress != cInvalidUint32)
        return AtHalDefaultNew(m_memoryMapVirtualBaseAddress);

    m_memoryMapVirtualBaseAddress = MemoryMap();
    if (m_memoryMapVirtualBaseAddress == cInvalidUint32)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create HAL because memory map is fail\r\n");
        return NULL;
        }

    AtPrintc(cSevInfo, "Memory map %s:0x%llx:0x%llx done with virtual base address: 0x%x\r\n",
             AppMemoryMapDeviceGet(), AppMemoryMapPhysicalOffsetGet(), AppMemoryMapMemorySizeInBytesGet(),
             m_memoryMapVirtualBaseAddress);

    return AtHalDefaultNew(m_memoryMapVirtualBaseAddress);
    }

static eBool ParseMemoryMapInformation(char *information)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(information, ":");

    if (AtTokenizerNumStrings(tokenizer) != 3)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid memory map information, expect: <deviceFile>:<physicalOffset>:<sizeInBytes>. "
                               "All numbers are in hex\r\n");
        return cAtFalse;
        }

    AtSprintf(m_memoryMapDevice, "%s", AtTokenizerNextString(tokenizer));
    p_memoryMapDevice = m_memoryMapDevice;
    m_memoryMapPhysicalOffset = strtoll(AtTokenizerNextString(tokenizer), NULL, 16);
    m_memoryMapMemorySizeInBytes = strtoll(AtTokenizerNextString(tokenizer), NULL, 16);

    return cAtTrue;
    }

char *AppMemoryMapDeviceGet(void)
    {
    return p_memoryMapDevice;
    }

long long AppMemoryMapPhysicalOffsetGet(void)
    {
    return m_memoryMapPhysicalOffset;
    }

void AppMemoryMapPhysicalOffsetSet(long long offset)
    {
    m_memoryMapPhysicalOffset = offset;
    }

long long AppMemoryMapMemorySizeInBytesGet(void)
    {
    return m_memoryMapMemorySizeInBytes;
    }

void AppMemoryMapMemorySizeInBytesSet(long long size)
    {
    m_memoryMapMemorySizeInBytes = size;
    }

void AppMemoryMapCleanup(void)
    {
    MemoryUnMap();
    }

AtHal AppMemoryMapHalCreate(void)
    {
    return CreateHalWithMemoryMappingInfo();
    }

eBool AppMemoryMapParseInput(char *information)
    {
    return ParseMemoryMapInformation(information);
    }

void AppMemoryMapDeviceSet(const char *deviceName)
    {
    if (deviceName)
        {
        AtStrncpy(m_memoryMapDevice, deviceName, AtStrlen(deviceName));
        p_memoryMapDevice = m_memoryMapDevice;
        }
    else
        p_memoryMapDevice = NULL;
    }

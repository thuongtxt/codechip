/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : socketutil.c
 *
 * Created Date: Mar 29, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include "errno.h"
#include "unistd.h"
#include "sys/ioctl.h"
#include <fcntl.h>
#include <sys/select.h>
#include "sys/types.h"
#include "sys/wait.h"
#include "sys/stat.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "atclib.h"
#include "apputil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

int SocketListen(uint32 listenPort)
    {
    struct sockaddr_in tcpAddr;
    int i;
    int socketListen;

    /* Create TCP socket to listen */
    if ((socketListen = SocketCreate()) < 0)
        {
        mPrintError("Can not create TCP socket\r\n");
        return -1;
        }
    /* Let the kernel reuse the socket address. This lets us run
     * twice in a row, without waiting for the (ip, port) tuple
     * to time out. */
    i = 1;
    setsockopt(socketListen, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));

#if 0
    /* Set socket becomes non-blocking */
    fcntl(socketListen, F_SETFL, O_NONBLOCK);
#endif
    memset(&tcpAddr, 0, sizeof(tcpAddr));
    tcpAddr.sin_family      = AF_INET;
    tcpAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    tcpAddr.sin_port        = htons(listenPort);

    if((bind(socketListen, (struct sockaddr*)&tcpAddr, sizeof(tcpAddr))) < 0)
        {
        mPrintError("Can not bind TCP socket\r\n");
        SocketClose(socketListen);
        return -1;
        }

    if (listen(socketListen, 1) < 0)
        {
        mPrintError("Can not listen TCP socket\r\n");
        SocketClose(socketListen);
        return -1;
        }

    return socketListen;
    }

int SocketCreate()
    {
    return socket(AF_INET, SOCK_STREAM, 0);
    }

atbool SocketConnect(int sock, const char * serverIpAddr, int port)
    {
    struct sockaddr_in serverAddr;
    struct hostent *host;

    host = gethostbyname(serverIpAddr);

    memcpy(&serverAddr.sin_addr.s_addr, host->h_addr_list[0], host->h_length);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);

    if( connect(sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0 )
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

uint32 SocketSend(int sock, char *pMsg, uint32 msgLen)
    {
#if 0
    fd_set              fds;
    struct timeval      tv;
    int                 retval;
#endif
    int                 len;
    int                 retlen;
    uint32              offset;

    if (socket < 0)
        {
        return 0;
        }

    if((msgLen <= 0) || (pMsg == NULL))
        {
        return 0;
        }
#if 0
    /* Check whether the accepted socket has data */
    FD_ZERO(&fds);
    FD_SET(sock, &fds);
    tv.tv_sec   = 0;
    tv.tv_usec  = 10000;
#endif
    /* Send message body */
    len     = msgLen;
    offset = 0;
    while(len > 0)
        {
        retlen  = send(sock, pMsg + offset, len, 0);
        if(retlen < 0)
            {
            if((retlen == -1) &&(errno == EAGAIN || errno == EINTR))
                {
#if 0
                retval = select(FD_SETSIZE, NULL, &fds, NULL, &tv);
                if (retval > 0)
                    {
                    if (FD_ISSET(sock, &fds))
                        {
                        continue;
                        }
                    }
#endif
                }
            return offset;
            }

        len     -= retlen;
        offset   = offset + retlen;
        }

    return offset;
    }

uint32 SocketRecv(int sock, char *msgBuf, uint32 msgSize)
    {
    int     recvCount;
    int     byteRecv = 0;
    uint32  offset;
#if 0
    fd_set  rfds;
    struct  timeval tv;
#endif

    if (sock < 0)
        {
        return 0;
        }
#if 0
    /* Check whether the accepted socket has data */
    FD_ZERO(&rfds);
    FD_SET(sock, &rfds);
    tv.tv_sec = 0;
    tv.tv_usec = 10000;

    /* Not block the program */
    if (select(FD_SETSIZE, &rfds, NULL, NULL, &tv) < 0 || !FD_ISSET(sock, &rfds))
        {
        return 0;
        }
#endif
    recvCount = msgSize;
    offset = 0;
    while(recvCount > 0)
        {
        byteRecv = recv(sock, msgBuf + offset, recvCount, 0);
        if(byteRecv <= 0)
            {
            if((byteRecv == -1) && (errno == EAGAIN || errno == EINTR))
                {
                continue;
                }
            else
                {
                return offset;
                }
            }

        offset  = offset + byteRecv;
        recvCount -= byteRecv;
        }

    return offset;
    }

int SocketClose(int sock)
    {
    return close(sock);
    }

int SocketAccept(int listenSocket)
    {
    return accept(listenSocket, NULL, NULL);
    }

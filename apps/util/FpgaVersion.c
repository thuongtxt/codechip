/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : FpgaVersion.c
 *
 * Created Date: May 25, 2016
 *
 * Description : To handle FPGA version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "FpgaVersion.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cVersionMajorMask  cBit7_4
#define cVersionMajorShift 4
#define cVersionMinorMask  cBit3_0
#define cVersionMinorShift 0
#define cVersionDDMask     cBit15_8
#define cVersionDDShift    8
#define cVersionMMMask     cBit23_16
#define cVersionMMShift    16
#define cVersionYYMask     cBit31_24
#define cVersionYYShift    24

#define mVersionNumberBuild(year, month, day, major, minor)                    \
        ((((uint32)(year)  << cVersionYYShift)    & cVersionYYMask)    |       \
         (((uint32)(month) << cVersionMMShift)    & cVersionMMMask)    |       \
         (((uint32)(day)   << cVersionDDShift)    & cVersionDDMask)    |       \
         (((uint32)(major) << cVersionMajorShift) & cVersionMajorMask) |       \
         (((uint32)(minor) << cVersionMinorShift) & cVersionMinorMask))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_versionNumber = 0;
static uint32 m_builtNumber   = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool AppFpgaVersionParse(char *fpgaVersion)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(fpgaVersion, " ");
    AtTokenizer releasedDateTokenizer, versionTokenizer;
    uint32 year, month, day;
    uint32 major, minor, built;

    if (AtTokenizerNumStrings(tokenizer) != 2)
        return cAtFalse;

    /* Parse released date */
    releasedDateTokenizer = AtTokenizerNew(AtTokenizerNextString(tokenizer), "/");
    if (AtTokenizerNumStrings(releasedDateTokenizer) != 3)
        {
        AtObjectDelete((AtObject)releasedDateTokenizer);
        return cAtFalse;
        }
    StrToDw(AtTokenizerNextString(releasedDateTokenizer), 'h', &year);
    StrToDw(AtTokenizerNextString(releasedDateTokenizer), 'h', &month);
    StrToDw(AtTokenizerNextString(releasedDateTokenizer), 'h', &day);
    AtObjectDelete((AtObject)releasedDateTokenizer);

    /* Parse hardware built number */
    versionTokenizer = AtTokenizerNew(AtTokenizerNextString(tokenizer), ".");
    if (AtTokenizerNumStrings(versionTokenizer) != 3)
        {
        AtObjectDelete((AtObject)versionTokenizer);
        return cAtFalse;
        }
    StrToDw(AtTokenizerNextString(versionTokenizer), 'h', &major);
    StrToDw(AtTokenizerNextString(versionTokenizer), 'h', &minor);
    StrToDw(AtTokenizerNextString(versionTokenizer), 'h', &built);
    AtObjectDelete((AtObject)versionTokenizer);

    /* Build hardware version */
    m_versionNumber = mVersionNumberBuild(year, month, day, major, minor);
    m_builtNumber   = built;

    return cAtTrue;
    }

const char *AppFpgaVersionFormat(void)
    {
    return "yy/mm/dd major.minor.built";
    }

uint32 AppFpgaVersionNumber(void)
    {
    return m_versionNumber;
    }

uint32 AppFpgaBuiltNumber(void)
    {
    return m_builtNumber;
    }

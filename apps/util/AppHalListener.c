/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : HalListener.c
 *
 * Created Date: Mar 28, 2017
 *
 * Description : HAL listener used for application
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/types.h>
#include <unistd.h>
#include "AppHalListener.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtFile m_halLogFile = NULL;
static eBool m_hasInvalidAccess = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *LogFileName(void)
    {
    static char m_logFileName[64];
    static char *pLogFileName = NULL;

    if (pLogFileName)
        return pLogFileName;

    AtSprintf(m_logFileName, "%d.invalid_access.log", getpid());
    pLogFileName = m_logFileName;

    return pLogFileName;
    }

static eBool LogOpen(const char *fileName)
    {
    m_halLogFile = AtStdFileOpen(AtStdSharedStdGet(), fileName, cAtFileOpenModeWrite | cAtFileOpenModeTruncate);
    return m_halLogFile ? cAtTrue : cAtFalse;
    }

static void LogClose(void)
    {
    AtStdFileClose(m_halLogFile);
    m_halLogFile = NULL;
    }

static eBool IsValidValue(uint32 value)
    {
    switch (value)
        {
        case 0xCAFECAFE: return cAtFalse;
        case 0XC0CAC0CA: return cAtFalse;
        case 0xDEADCAFE: return cAtFalse;
        case 0xDEADC0FE: return cAtFalse;
        default:
            return cAtTrue;
        }
    }

static void DidReadRegister(AtHal hal, void *listener, uint32 address, uint32 value)
    {
    AtUnused(listener);

    if (IsValidValue(value))
        return;

    if (m_halLogFile == NULL)
        LogOpen(LogFileName());

    if (m_halLogFile == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: Cannot open/create log file %s for invalid access logging. Please check access right.\r\n", LogFileName());
        return;
        }

    AtFilePrintf(m_halLogFile, "Read  0x%08x, value 0x%08x (FPGA at %s)\r\n",
                 address, value, AtHalToString(hal));
    AtFileFlush(m_halLogFile);

    m_hasInvalidAccess = cAtTrue;
    }

static tAtHalListener *HalListener(void)
    {
    static tAtHalListener listener;
    static tAtHalListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidReadRegister  = DidReadRegister;
    pListener = &listener;

    return pListener;
    }

static void Enable(AtHal hal)
    {
    AtHalListenerAdd(hal, NULL, HalListener());
    }

static void Disable(AtHal hal)
    {
    AtHalListenerRemove(hal, NULL, HalListener());
    LogClose();
    }

void AppHalListenerEnable(AtHal hal, eBool enabled)
    {
    if (enabled)
        Enable(hal);
    else
        Disable(hal);
    }

eBool AppHalListenerHasInvalidAccesses(void)
    {
    return m_hasInvalidAccess;
    }

const char *AppHalListenerLogFileName(void)
    {
    return LogFileName();
    }

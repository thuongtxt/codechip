/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : AppListenerTextUILogCommands.h
 * 
 * Created Date: May 25, 2017
 *
 * Description : App log all command input
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _APPTEXTUILISTENER_H_
#define _APPTEXTUILISTENER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AppTextUIListen(AtTextUI textui);
const char * AppTextUICliLogFileName(void);
void AppTextUICliLogFileClose(void);
void AppTextUICliLogFileDelete(void);

#ifdef __cplusplus
}
#endif
#endif /* _APPTEXTUILISTENER_H_ */


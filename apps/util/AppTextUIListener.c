/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AppListenerTextUILogCommands.c
 *
 * Created Date: May 25, 2017
 *
 * Description : App log all command input
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

#include "atclib.h"
#include "AtCliModule.h"
#include "AppTextUIListener.h"
#include "showtable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtFile m_logFile = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *LogFileName(void)
    {
    static char logFileName[64];
    static char *pLogFileName = NULL;

    if (pLogFileName)
        return pLogFileName;

    AtSprintf(logFileName, ".%d_cli.log", getpid());
    pLogFileName = logFileName;

    return pLogFileName;
    }

static eBool LogOpen(const char *fileName)
    {
    m_logFile = AtStdFileOpen(AtStdSharedStdGet(), fileName, cAtFileOpenModeWrite | cAtFileOpenModeTruncate);
    return m_logFile ? cAtTrue : cAtFalse;
    }

static void LogClose(void)
    {
    AtStdFileClose(m_logFile);
    m_logFile = NULL;
    }

static void DidExecuteCliWithResult(AtTextUI self, const char *cli, int result, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtUnused(result);

    if (m_logFile == NULL)
        LogOpen(LogFileName());

    if (m_logFile == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: Cannot open/create log file %s for CLI logging. Please check access right.\r\n", LogFileName());
        return;
        }

    if (AtStrstr(cli, ".tcl") || AtStrstr(cli, ".py"))
        {
        AtFilePrintf(m_logFile, "#%s", cli);
        AtFileFlush(m_logFile);
        }
    else
        {
        AtFilePrintf(m_logFile, "%s\n", cli);
        AtFileFlush(m_logFile);
        }
    }

static void TextUIWillExecuteCli(AtTextUI self, const char *cli, void *userData)
    {
    AtUnused(self);
    AtUnused(cli);
    AtUnused(userData);
    GlobalTableReset();
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;
    m_listener.DidExecuteCliWithResult = DidExecuteCliWithResult;
    m_listener.WillExecuteCli = TextUIWillExecuteCli;

    return &m_listener;
    }

void AppTextUIListen(AtTextUI textui)
    {
    AtTextUIListenerAdd(textui, TextUIListener(), NULL);
    }

const char *AppTextUICliLogFileName(void)
    {
    return LogFileName();
    }

void AppTextUICliLogFileClose(void)
    {
    LogClose();
    }

void AppTextUICliLogFileDelete(void)
    {
    remove(LogFileName());
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : EP APP
 *
 * File        : main.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/time.h>

#include "extAtSdkInt.h"
#include "AtHalTcp.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "AtOsalLinuxDebug.h"
#include "AtCliModule.h"
#include "AtFile.h"
#include "AtCliService.h"

#include "apputil.h"
#include "appsignal.h"
#include "tasks.h"
#include "MemoryMap.h"
#include "../ep5/ep5.h"
#include "../xilinx/xilinx.h"
#include "AtAluPci.h"
#include "LiuManager.h"
#include "FpgaVersion.h"

#include "AtFiberLogicPlatform.h"
#include "../util/AppTextUIListener.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtTextUI m_clishTextUI = NULL;
static AtTextUI m_tinyTextUI  = NULL;
static AtLogger m_fileLogger  = NULL;

static eBool m_shouldReportReplicatedSequence = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/
eBool H3CSystemInit(void);
eAtRet AtFiberLogicPlatformFinalize(void);

/* NOTE: we know this is bad, the following extern for working on LAB only */
#include "../../driver/src/generic/util/AtLongRegisterAccess.h"
#include "../../driver/src/generic/man/AtDeviceInternal.h"
extern eAtRet AtDeviceGlobalLongRegisterAccessSet(AtDevice self, AtLongRegisterAccess registerAccess);
extern AtLongRegisterAccess AtDeviceInstalledGlobalLongRegisterAccessGet(AtDevice self);
extern AtLongRegisterAccess Tha60210011LongRegisterAccessNew(void);
extern eBool CliRegisterCheckDestroy(void);

/* Additional CLIs */
eBool CmdAppConnectionShow(char argc, char **argv);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return CliDevice();
    }

static AtDriver Driver(void)
    {
    return AtDriverSharedDriverGet();
    }

/*
 * Print help information
 * @param pProgName
 */
static void HelpPrint(char *pProgName)
    {
    mPrint("Usage: %s [options]\r\n", pProgName);
    mPrint("Options: \r\n");
    mPrint("    -h                             Display help\r\n");
    mPrint("    -D                             Run in background\r\n");
    mPrint("    -s <productCode>               Simulate device specified by product code\r\n");
    mPrint("    -p <platform[:phyBaseaddress]> Run on specific platform: %s\r\n", cStrPlatformAll);
    mPrint("    -l <listeningPort>             Listen on specified port for shell connect\r\n");
    mPrint("    -L <halListeningPort>          Listen on specified port for HAL connection\r\n");
    mPrint("    -e <halEventNotifyPort>        Port to notify event\r\n");
    mPrint("    -P <productCode>               Specify product code instead of auto detecting\r\n");
    mPrint("    -r <boardIp[:port]>            Remote control the board from a local machine\r\n");
    mPrint("    -t <testbenchIp[:port]>        Connect to testbench\r\n");
    mPrint("    -n                             Start the app with no devices\r\n");
    mPrint("    -u <port>                      Explicitly specify the port for CLI server to listen on\r\n");
    mPrint("    -w <yy/mm/dd major.minor.built> Specify the firmware version\r\n");
    mPrint("    -m <deviceFile>:<physicalOffset_hex>:<sizeInBytes_hex> Specify memory mapping information\r\n");
    }

/*
 * Start program in background.
 * @return cAtTrue on success, cAtFalse on failure
 */
static int DaemonStart(int (*run)(void))
    {
    if (daemon(1, 0) != 0)
        mPrintError("Start daemon\r\n");

    return run();
    }

static void DidReceiveInterruptMessage(AtHal hal, void *listener)
    {
    AtDeviceInterruptProcess(Device());
    }

static void ListenOnInterrupt(AtHal hal)
    {
    static tAtHalTcpListener listener;

    /* Register listener */
    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidReceiveInterruptMessage = DidReceiveInterruptMessage;
    AtHalListenerAdd(hal, NULL, (const tAtHalListener *)&listener);

    /* And listen on HAL event */
    AtHalTcpEventListen(hal, AppHalEventNotifyPortGet());
    }

static const char* LoggerFilename(void)
    {
    time_t dateTime = time(NULL);
    struct tm* tm_info;
    struct timeval tm;
    static char loggerName[40];

    tm_info = localtime(&dateTime);
    gettimeofday(&tm, NULL);
    AtSprintf(loggerName, "%x-", AppProductCodeGet(AppPlatform()));
    strftime(&loggerName[AtStrlen(loggerName)], 32, "%Y%m%d", tm_info);
    AtSprintf(&loggerName[AtStrlen(loggerName)], "-log.txt");
    return loggerName;
    }

static void DriverCreate(void)
    {
    static const uint32 cMaxMessageLength = 512;
    static const uint8 cMaxNumDevices = 10;
    static const uint32 cMaxNumLoggerMessage = 1024;

    AtDriver driver = AtDriverCreate(cMaxNumDevices, AtOsalLinuxDebug());
    AtLogger logger;

    if (AppIsFileLogger())
        logger = AtFileLoggerNew(LoggerFilename(), cMaxMessageLength);
    else
        logger = AtDefaultLoggerNew(cMaxNumLoggerMessage, cMaxMessageLength);
    if (logger == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create default logger\r\n");
        return;
        }
    AtLoggerLevelEnable(logger, cAtLogLevelDebug, cAtTrue);
    AtDriverLoggerSet(driver, logger);
    AtLoggerFlush(logger);
    AtDriverDebugEnable(cAtTrue);

    m_fileLogger = logger;
    }

static eBool Tha60210011RunOnKintex(uint32 productCode)
    {
    if (AppPlatform() != cPlatformEp5)
        return cAtFalse;

    if ((productCode == 0x80210011) || (productCode == 0x81210011))
        return cAtTrue;

    return cAtFalse;
    }

/*
 * Initialize Driver
 * @return cAtTrue on success, cAtFalse on failure
 */
static atbool DriverInit(void)
    {
    uint32 productCode;

    /* Create driver */
    if (Driver() == NULL)
        DriverCreate();

    if (Driver() == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create driver\r\n");
        return cAtFalse;
        }

    /* User may want to add remote devices */
    if (AppDoNotAddDevice())
        return cAtTrue;

    /* Create device */
    productCode = AppProductCodeGet(AppPlatform());
    if (AppIsSimulationPlatform(AppPlatform()))
        productCode = AppSimulationProductCode(productCode);
    if (Device() == NULL)
        AtDriverDeviceCreate(Driver(), productCode);
    if (Device() == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create device with product code 0x%08x\r\n", productCode);
        return cAtFalse;
        }

    /* Operator may want to specify FPGA version */
    if (AppFpgaVersionNumber())
        {
        AtDeviceVersionNumberSet(Device(), AppFpgaVersionNumber());
        AtDeviceBuiltNumberSet(Device(), AppFpgaBuiltNumber());
        }

    /* Install HAL for simulation */
    if (AppIsSimulationPlatform(AppPlatform()))
        {
        uint8 coreId;
        AtHal hal;

        hal = AppCreateHal(0, AppPlatform());
        for (coreId = 0; coreId < AtDeviceNumIpCoresGet(Device()); coreId++)
            {
            AppHalListenerEnable(hal, cAtTrue);
            AtIpCoreHalSet(AtDeviceIpCoreGet(Device(), coreId), hal);
        }
        }

    /* Install HAL for real platform */
    else if ((AppPlatform() == cPlatformEp6)          ||
             (AppPlatform() == cPlatformEp6C1)        ||
             (AppPlatform() == cPlatform16BitEp6C1)   ||
             (AppPlatform() == cPlatformEp6C2)        ||
             (AppPlatform() == cPlatformEp5)          ||
             (AppPlatform() == cPlatformLoop16E1)     ||
             (AppPlatform() == cPlatformLoop32E1)     ||
             (AppPlatform() == cPlatformLoopStm)      ||
             (AppPlatform() == cPlatformZtePtn)       ||
             (AppPlatform() == cPlatformZteRouterStm) ||
             (AppPlatform() == cPlatformFiberLogic)   ||
             (AppPlatform() == cPlatformAlu)          ||
             (AppPlatform() == cPlatformRemote)       ||
             (AppPlatform() == cPlatformTestbench)    ||
             (AppPlatform() == cPlatformXilinx)       ||
             (AppPlatform() == cPlatformAnna)         ||
             ((AppPlatform() == cPlatformUnknown) && (AppMemoryMapDeviceGet() != NULL)))
        {
        uint8 coreId;

        if (AppPlatform() == cPlatformTestbench)
            AtDeviceTestbenchEnable(Device(), cAtTrue);

        for (coreId = 0; coreId < AtDeviceNumIpCoresGet(Device()); coreId++)
            {
            AtHal hal = AppCreateHal(AppBaseAddressOfCore(coreId), AppPlatform());
            AtIpCoreHalSet(AtDeviceIpCoreGet(Device(), coreId), hal);
            if (AppPlatform() == cPlatformRemote)
                ListenOnInterrupt(hal);

            AppHalListenerEnable(hal, cAtTrue);
            }
        }

    if ((AppPlatform() == cPlatformXilinx) || Tha60210011RunOnKintex(productCode))
        AtDeviceGlobalLongRegisterAccessSet(Device(), Tha60210011LongRegisterAccessNew());

    AppAllCoresInterruptListen(Device());

    return cAtTrue;
    }

/*
 * Initialize CLI
 * @return cAtTrue on success, cAtFalse on failure
 */
static atbool CliInit(void)
    {
    return cAtTrue;
    }

/*
 * Initialize all resources
 * @return cAtTrue on success, cAtFalse on failure
 */
static atbool Init(void)
    {
    atbool success;

    if (!AppSignalSetup())
        {
        mPrintError("Install signal handler\r\n");
        return cAtFalse;
        }

    if (AppPlatform() == cPlatformH3C)
        success = H3CSystemInit();
    else
        success = DriverInit();
    if (!success)
        return success;

    if (!CliInit())
        {
        mPrintError("CLI Init\r\n");
        return cAtFalse;
        }

    if (!AppAllTasksCreate())
        {
        mPrintError("Thread create\r\n");
        return cAtFalse;
        }

    return LiuManagerSetup(Device(), AppPlatform());
    }

static void ShowReplicateSequence(void)
    {
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevCritical, "NOTICE: Logged messages/memory leak/invalid accesses may happen, please send file %s to software team to replicate\r\n", AppTextUICliLogFileName());
    }

static void LeakReport(void)
    {
    AtOsalLeakTracker tracker = AtOsalLinuxDebugLeakTracker(AtOsalLinuxDebug());
    if (!AtOsalLeakTrackerIsLeak(tracker))
        {
        AtPrintc(cSevInfo, "No memory leak found\r\n");
        return;
        }

    AtOsalLeakTrackerReport(tracker);
    m_shouldReportReplicatedSequence = cAtTrue;
    }

static void OsalCleanup(void)
    {
    LeakReport();

    if (m_shouldReportReplicatedSequence)
        ShowReplicateSequence();
    else
        AppTextUICliLogFileDelete();

    AtOsalLinuxDebugCleanUp(AtOsalLinuxDebug());
    }

/* Save all HALs installed to this device */
static void SaveDeviceHals(AtDevice device, AtList hals)
    {
    uint8 coreId;

    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }
    }

static uint32 NumProblemLoggedMessages(AtLogger logger)
    {
    tAtLoggerCounters counters;
    AtLoggerCountersGet(logger, &counters);
    return (counters.levelCritical +
            counters.levelWarning +
            counters.levelOthers);
    }

static void ReportLogger(AtDriver driver)
    {
    AtLogger logger;

    logger = AtSharedDriverLoggerGet();
    if (NumProblemLoggedMessages(logger) == 0)
        {
        AtPrintc(cSevInfo, "No problem logged messages\r\n");
        return;
        }

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevWarning, "====================================================\r\n");
    AtPrintc(cSevWarning, "WARNING: there are %d logged messages, please review\r\n"
                          "them and report to software team for any warning/critical\r\n"
                          "messages\r\n", AtLoggerNumMessages(logger));
    AtPrintc(cSevWarning, "==================== Start of logger ===============\r\n");
    AtLoggerShow(logger);
    AtPrintc(cSevWarning, "==================== End of logger   ===============\r\n");
    AtPrintc(cSevNormal, "\r\n");
    m_shouldReportReplicatedSequence = cAtTrue;
    }

static void InvalidAccessReport(void)
    {
    if (AppHalListenerHasInvalidAccesses())
        {
        AtPrintc(cSevCritical, "There are invalid accesses reported in the file %s. "
                               "Please send it to software team along with the whole CLI sequences to analyze\r\n",
                               AppHalListenerLogFileName());
        m_shouldReportReplicatedSequence = cAtTrue;
        return;
        }

    AtPrintc(cSevInfo, "No invalid access reported\r\n");
    }

/*
 * Sample driver clean up function.
 *
 * Application create HALs and install them to AT Driver, so application must be
 * responsible for deleting these HALs. But, during device deleting, driver may
 * use HAL objects for special purposes, so HAL should be deleted after device
 * or driver is deleted.
 *
 * The following sample code will delete all added device. For each device, all
 * of installed HALs are saved in a list "hals". When driver is deleted, all of
 * HAL objects in this list are deleted.
 */
static eAtRet DriverCleanup(AtDriver driver)
    {
    AtList hals;
    uint8 numDevices, i;
    uint32 deletedTimeMs;
    eBool slowDeleting;
    tAtOsalCurTime prevTime, curTime;

    if (driver == NULL)
        return cAtOk;

    /* All HALs that are installed so far */
    hals = AtListCreate(0);

    /* Delete all devices */
    AtOsalCurTimeGet(&prevTime);
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        AtObject longRegRegisterAccess;

        SaveDeviceHals(device, hals);
        longRegRegisterAccess = (AtObject)AtDeviceInstalledGlobalLongRegisterAccessGet(device);
        AtDriverDeviceDelete(driver, device);
        AtObjectDelete(longRegRegisterAccess);
        }

    ReportLogger(driver);

    /* Delete driver and profile it */
    AtDriverDelete(driver);
    AtOsalCurTimeGet(&curTime);
    deletedTimeMs = mTimeIntervalInMsGet(prevTime, curTime);
    slowDeleting = (deletedTimeMs > 1000) ? cAtTrue : cAtFalse;
    AtPrintc(slowDeleting ? cSevWarning : cSevInfo, "Device deleting take %d (ms)\r\n", deletedTimeMs);

    /* Delete all saved HALs */
    while (AtListLengthGet(hals) > 0)
        {
        AtHal hal = (AtHal)AtListObjectRemoveAtIndex(hals, 0);
        AppHalListenerEnable(hal, cAtFalse);
        AtHalDelete(hal);
        }
    AtObjectDelete((AtObject)hals);

    AtAluPciCleanup();
    AtObjectDelete((AtObject)m_fileLogger);
    AppMemoryMapCleanup();

    InvalidAccessReport();

    return cAtOk;
    }

/*
 * Clean-up CLI
 * @return cAtTrue on success, cAtFalse on failure
 */
static atbool CliCleanup(void)
    {
    AtCliServerStop();
    AtTextUIDelete(m_clishTextUI);
    AtTextUIDelete(m_tinyTextUI);
    CliRegisterCheckDestroy();

    return cAtTrue;
    }

/*
 * Clean-up all resources
 * @return cAtTrue on success, cAtFalse on failure
 */
atbool AppCleanup(void)
    {
    atbool ret;

    if (!(ret = AppAllTasksDelete()))
        {
        mPrintError("Thread delete\r\n");
        }

    DriverCleanup(Driver());

    if (!(ret = CliCleanup()))
        {
        mPrintError("CLI Cleanup\r\n");
        }

    LiuManagerDelete();
    Ep5PlatformFinalize();
    AtFiberLogicPlatformFinalize();
    XilinxPlatformFinalize();

    OsalCleanup();

    return ret;
    }

static void CmdDidExecute(unsigned long evtMask, const void *evtObj)
    {
    if (!AtTextUIResultIsValid(AtCliSharedTextUI()))
        return;

    AtSdkTclSetIntegerResult(AtTextUIIntegerResultGet(AtCliSharedTextUI()));
    }

static void CliServerStart(void)
    {
    AtCliServerStart(NULL, AppCliServerListenPortGet());

    /* When CLI server start, it configures CPU rest time and slow down CLI
     * processing when many channels are input. Need to reset this attribute for
     * LAB testing */
    AtCliCpuRestTimeMsSet(0);
    }

/*
 * CLI shell
 */
static void CliStart(void)
    {
    extern int AtCliStartWithTextUI(AtTextUI textUI);
    m_tinyTextUI  = AtDefaultTinyTextUINew();
    m_clishTextUI = AtClishTextUINew(m_tinyTextUI);

    AtTinyTextUICmdAdd(m_tinyTextUI, "show app connection", CmdAppConnectionShow, 3, 0);

    AppTextUIListen(m_clishTextUI);
    CliServerStart();
    AtSdkTclEventRegister(CmdDidExecute, cAtSdkTclEventCmdEnd);
    AtTextUICmdBufferSizeSet(m_clishTextUI, 1024 * 1024);

    /* Make show table be faster */
    TableRestTimeMsSet(0); /* It is default to 10ms */

    AtCliStartWithTextUI(m_clishTextUI);
    AppTextUICliLogFileClose();
    }

/*
 * Initialize system environment
 */
static void SystemInit(void)
    {
    struct rlimit coreFileLimit;

    /* Set maximum size of core file */
    coreFileLimit.rlim_cur = 8192;
    coreFileLimit.rlim_max = coreFileLimit.rlim_cur;
    setrlimit(RLIMIT_CORE, &coreFileLimit);
    }

/*
 * Run main task
 * @return EXIT_SUCCESS on success, otherwise return EXIT_FAILURE
 */
static int Run(void)
    {
    if (!Init())
        {
        mPrintError("Init\r\n");

        AppCleanup();
        return EXIT_FAILURE;
        }

    CliStart();
    AppCleanup();

    return EXIT_SUCCESS;
    }

/*
 * Main process
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char* argv[])
    {
    /* Initialize system environment */
    SystemInit();

    /* Get arguments for program's options */
    if (AppOptionParse(argc, argv) == cAtFalse)
        return EXIT_FAILURE;

    if (AppNeedToDisplayHelp())
        {
        HelpPrint((char *)AppName());
        return EXIT_SUCCESS;
        }

    if (AppIsDeamon())
        return DaemonStart(Run);

    return Run();
    }


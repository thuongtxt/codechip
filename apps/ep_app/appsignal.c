/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : signal.c
 *
 * Created Date: Sep 16, 2013
 *
 * Description : Signal processing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <signal.h>
#include <unistd.h>
#include <execinfo.h>
#include <stdio.h>
#include <string.h>

#include "attypes.h"
#include "atclib.h"
#include "apputil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define sStartStack "================= Start of backtrace ===============\r\n"
#define sEndStack   "================== End of backtrace ================\r\n"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 signalStackMem[SIGSTKSZ];

/*--------------------------- Forward declarations ---------------------------*/
extern atbool AppCleanup(void);

/*--------------------------- Implementation ---------------------------------*/
static char *StackStringBuild(char *execPath, void *stackPointer)
    {
    FILE *fp;
    static char lineBuffer[2048];
    char command[64];

    AtSprintf(command,"addr2line -f -e %s %p", execPath, stackPointer);

    /* Open the command for reading. */
    fp = popen(command, "r");
    if (fp == NULL)
        {
        mPrintError("Cannot run \"%s\"\r\n", command);
        return NULL;
        }

    AtOsalMemInit(lineBuffer, 0, 2048);

    /* Read the output */
    while (fgets(lineBuffer, sizeof(lineBuffer), fp) != NULL);

    pclose(fp);
    return lineBuffer;
    }

static void SignalBacktrace(int sigNum)
    {
    const int maxPointers = 100;
    void *stackPointers[maxPointers];
    int numPointers, j;
    char execPath[32] = {0};
    char reportPath[] = "backtrace.txt";
    FILE *logger;

    sprintf(execPath, "/proc/%d/exe", getpid());

    numPointers = backtrace(stackPointers, maxPointers);

    logger = fopen(reportPath, "w");

    AtPrintc(cSevWarning, "====================================================\r\n");
    AtPrintc(cSevWarning, "WARNING: The \"%s\" has just happened.\r\n", strsignal(sigNum));
    AtPrintc(cSevWarning, "Please report the log file %s to software team.     \r\n", reportPath);
    AtPrintc(cSevWarning, "====================================================\r\n");

    AtPrintf(sStartStack);
    if (logger)
        fprintf(logger, "%s", sStartStack);

    for (j = 0; j < numPointers; j++)
        {
        char *stackStr = StackStringBuild(execPath, stackPointers[j]);
        AtPrintf("[%3d]: %s", j, stackStr);
        if (logger)
            fprintf(logger, "[%3d]: %s", j, stackStr);
        }

    AtPrintf(sEndStack);
    if (logger)
        {
        fprintf(logger, "%s", sEndStack);

        fflush(logger);
        fclose(logger);
        }
    }

/*
 * Install handler for a signal
 * @return cAtTrue on success, cAtFalse on failure
 */
atbool SignalHandlerInstall(int sigNum, void (*sigHandler)(int), int saFlags)
    {
    struct sigaction action;

    /* Register signal handler for sigNum */
    sigemptyset(&action.sa_mask);
    sigaddset(&action.sa_mask, sigNum);
    action.sa_flags = saFlags;
    action.sa_handler = sigHandler;

    if(sigaction(sigNum, &action, NULL) != 0)
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

/*
 * Cleanup on signal received and activate signal's default action
 */
static void SignalCleanupWithDefaultAction(int sigNum)
    {
    AppCleanup();

    /* Reset signal's default handler */
    SignalHandlerInstall(sigNum, SIG_DFL, SA_ONSTACK);

    /* Re-send signal to force it activates the default action */
    kill(getpid(), sigNum);
    }

/*
 * Handle on signal received with backtrace dump and system default action
 */
static void SignalHandlerWithBacktrace(int sigNum)
    {
    /* Dump backtrace */
    SignalBacktrace(sigNum);

    /* Reset signal's default handler */
    SignalHandlerInstall(sigNum, SIG_DFL, SA_ONSTACK);

    /* Re-send signal to force it activates the default action */
    kill(getpid(), sigNum);
    }

/*
 * Handler of signal SIGTERM
 * @param sigNum Signal number
 */
static void SIGTERM_Handler(int sigNum)
    {
    SignalCleanupWithDefaultAction(sigNum);
    }

/*
 * Handler of signal SIGINT
 * @param sigNum Signal number
 */
static void SIGINT_Handler(int sigNum)
    {
    kill(getpid(), SIGTERM);
    }

/*
 * Handler of signal SIGSEGV
 * @param sigNum Signal number
 */
static void SIGSEGV_Handler(int sigNum)
    {
    /*  Program will not be cleanup to avoid nest lock and make the application cannot exit*/
    mPrintError("Segmentation fault (SIGSEGV).\r\n");
    SignalHandlerWithBacktrace(sigNum);
    }

/*
 * Handler of signal SIGFPE
 * @param sigNum Signal number
 */
static void SIGFPE_Handler(int sigNum)
    {
    mPrintError("Floating point exception (SIGFPE).\r\n");
    SignalHandlerWithBacktrace(sigNum);
    }

/*
 * Handler of signal SIGBUS
 * @param sigNum Signal number
 */
static void SIGBUS_Handler(int sigNum)
    {
    mPrintError("Bus error (SIGBUS). Clean-up and exit program\r\n");
    SignalCleanupWithDefaultAction(sigNum);
    }

atbool AppSignalSetup()
    {
    /* Create new alternate stack to handle signal */
    stack_t newSignalStack;

    newSignalStack.ss_sp = signalStackMem;
    newSignalStack.ss_size = SIGSTKSZ;
    newSignalStack.ss_flags = 0;

    if (sigaltstack(&newSignalStack, NULL) != 0)
        return cAtTrue;

    /* Register signal handler for SIGINT, SIGTERM, SIGSEGV, SIGBUS, SIGHUP */
    if (SignalHandlerInstall(SIGSEGV, SIGSEGV_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }
    else if (SignalHandlerInstall(SIGBUS, SIGBUS_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }
    else if (SignalHandlerInstall(SIGTERM, SIGTERM_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }
    else if (SignalHandlerInstall(SIGINT, SIGINT_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }
    else if (SignalHandlerInstall(SIGQUIT, SIGINT_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }
    else if (SignalHandlerInstall(SIGHUP, SIGINT_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }
    else if (SignalHandlerInstall(SIGFPE, SIGFPE_Handler, SA_ONSTACK) != cAtTrue)
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

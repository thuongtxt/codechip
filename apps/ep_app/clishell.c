/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : EP APP
 *
 * File        : clishell.c
 *
 * Created Date: Nov 1, 2012
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "stdlib.h"
#include "stdio.h"
#include "fcntl.h"
#include "unistd.h"
#include "string.h"
#include "errno.h"
#include <signal.h>
#include <pthread.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include "sys/types.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "attypes.h"
#include "atclib.h"
#include "epapp.h"
#include "apputil.h"

/*--------------------------- Define -----------------------------------------*/
#define cOptString           "c:"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static int cliSocket = -1;
static uint32 cliSocketPort = 8765;

/*--------------------------- Forward declarations ---------------------------*/
static atbool Cleanup(void);

/*--------------------------- Implementation ---------------------------------*/
static atbool ConnectBySocket(void)
    {
    char *  pTtyName;

    if (cliSocket < 0)
        {
        if ((cliSocket = SocketCreate()) < 0)
            {
            return cAtFalse;
            }

        if (!SocketConnect(cliSocket, "127.0.0.1", cliSocketPort))
            {
            return cAtFalse;
            }
        }

    pTtyName = ttyname(STDOUT_FILENO);
    if (pTtyName != NULL)
        {
        char msgBuf[cMsgMaxLength];
        tMsgHeader *pMsgHeader;
        uint32 msgLen;

        memset(msgBuf, 0, cMsgMaxLength);

        /* Build connect message */
        pMsgHeader = (tMsgHeader *)msgBuf;
        pMsgHeader->msgId = cMsgCliShellConnect;
        pMsgHeader->msgLen = sizeof(tMsgHeader) + strlen(pTtyName);
        pMsgHeader->senderPid = getpid();

        strncpy(&msgBuf[sizeof(tMsgHeader)], pTtyName, cMsgMaxLength - sizeof(tMsgHeader) - 1);

        msgLen = pMsgHeader->msgLen;
        /* Convert to network byte order */
        pMsgHeader->msgId = htonl(pMsgHeader->msgId);
        pMsgHeader->msgLen = htonl(pMsgHeader->msgLen);
        pMsgHeader->senderPid = htonl(pMsgHeader->senderPid);

        /* Send message */
        if (SocketSend(cliSocket, msgBuf, msgLen) != msgLen)
            {
            mPrintError("Connect\r\n");
            return cAtFalse;
            }
        }
    else
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

static atbool DisconnectBySocket(void)
    {
    char msgBuf[cMsgMaxLength];
    tMsgHeader *pMsgHeader;
    uint32 msgLen;

    if (cliSocket < 0)
        {
        if ((cliSocket = SocketCreate()) < 0)
            {
            return cAtFalse;
            }

        if (!SocketConnect(cliSocket, "127.0.0.1", cliSocketPort))
            {
            return cAtFalse;
            }
        }

    memset(msgBuf, 0, cMsgMaxLength);

    /* Build connect message */
    pMsgHeader = (tMsgHeader *)msgBuf;
    pMsgHeader->msgId = cMsgCliShellDisconnect;
    pMsgHeader->msgLen = sizeof(tMsgHeader);
    pMsgHeader->senderPid = getpid();

    msgLen = pMsgHeader->msgLen;
    /* Convert to network byte order */
    pMsgHeader->msgId = htonl(pMsgHeader->msgId);
    pMsgHeader->msgLen = htonl(pMsgHeader->msgLen);
    pMsgHeader->senderPid = htonl(pMsgHeader->senderPid);

    /* Send message */
    if (SocketSend(cliSocket, msgBuf, msgLen) != msgLen)
        {
        mPrintError("Disconnect\r\n");
        SocketClose(cliSocket);
        cliSocket = -1;
        return cAtFalse;
        }

    AtOsalUSleep(500000);
    SocketClose(cliSocket);
    cliSocket = -1;

    return cAtTrue;
    }

static atbool ServerConnect()
    {
    return ConnectBySocket();
    }

static atbool ServerDisconnect()
    {
    return DisconnectBySocket();
    }

static atbool SignalHandlerInstall(int sigNum, void (*sigHandler)(int), int saFlags)
    {
    struct sigaction action;

    /* Register signal handler for sigNum */
    sigemptyset(&action.sa_mask);
    sigaddset(&action.sa_mask, sigNum);
    action.sa_flags = saFlags;
    action.sa_handler = sigHandler;

    if(sigaction(sigNum, &action, NULL) != 0)
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

/*
 * Handler of signal
 * @param sigNum Signal number
 */
static void SignalHandler(int sigNum)
    {
    Cleanup();
    }

static atbool SignalHandle(void)
    {
    if (SignalHandlerInstall(SIGHUP, SignalHandler, 0) != cAtTrue)
        {
        return cAtFalse;
        }

    if (SignalHandlerInstall(SIGINT, SignalHandler, 0) != cAtTrue)
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

static atbool Init(void)
    {
    atbool ret;

    if (!(ret = SignalHandle()))
        mPrintError("Install signal handler\r\n");

    if (ret)
        {
        if (!(ret = ServerConnect()))
            mPrintError("Connect epapp\r\n");
        }

    return ret;
    }

static atbool Cleanup(void)
    {
    ServerDisconnect();
    return cAtTrue;
    }

static void TerminationWait(void)
    {
    sigset_t waitSigSet;

    sigemptyset(&waitSigSet);
    sigaddset(&waitSigSet, SIGTERM);

    pthread_sigmask(SIG_BLOCK, &waitSigSet, NULL);
    sigwaitinfo(&waitSigSet, NULL);
    }

static atbool OptionParse(int argc, char* argv[])
    {
    int opt;

    while ((opt = getopt( argc, argv, cOptString)) != AtEof())
        {
        switch (opt)
            {
            case 'c':
                if( sscanf( optarg, "%d", &cliSocketPort ) != 1 )
                    return cAtFalse;
                break;
            default:
                return cAtFalse;
            }
        }

    return cAtTrue;
    }

int main(int argc, char* argv[])
    {
    if (OptionParse(argc, argv) == cAtFalse)
        {
        AtPrintc(cSevInfo, "Usage: clishell -c <portToConnect>\r\n");
        return EXIT_FAILURE;
        }

    if (!Init())
        {
        mPrintError("Init\r\n");

        Cleanup();
        return EXIT_FAILURE;
        }

    TerminationWait();

    Cleanup();

    return EXIT_SUCCESS;
    }

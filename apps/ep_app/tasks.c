/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : tasks.c
 *
 * Created Date: Sep 16, 2013
 *
 * Description : Task management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <arpa/inet.h>
#include <string.h>

#include "AtDriver.h"
#include "AtModuleBer.h"
#include "AtModuleAps.h"
#include "AtCliModule.h"
#include "AtCli.h"
#include "AtEyeScanController.h"
#include "AtEyeScanLane.h"
#include "AtSdhLine.h"
#include "AtEthPort.h"
#include "AtHalTcp.h"
#include "AtHalTcpServer.h"
#include "AtModuleSur.h"

#include "apputil.h"
#include "appsignal.h"
#include "epapp.h"
#include "tasks.h"

/*--------------------------- Define -----------------------------------------*/
#define CLI_SHELL_STARTUP_STR  "********************************************\r\n" \
                               "*         Arrive Technologies              *\r\n" \
                               "*         Command Line SHELL               *\r\n" \
                               "*             AT SDK                       *\r\n" \
                               "*                                          *\r\n" \
                               "*      WARNING: Authorized Access Only     *\r\n" \
                               "********************************************\r\n"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtTask commTask = NULL;     /* Communication thread */

/* Periodic thread */
static AtTask m_pollingTask       = NULL;
static eBool m_pollingTaskEnabled = cAtFalse;
static uint32 m_taskPeriodInMs    = 100;

static AtTask m_pollingForcePointerTask            = NULL;
static eBool  m_pollingForcePointerTaskEnabled     = cAtFalse;
static uint32 m_pollingForcePointerTaskPeriodInMs  = 50;

/* FM/PM thread */
static AtTask m_FmPmTask           = NULL;
static eBool m_FmPmTaskEnabled     = cAtFalse;

/* Interrupt task */
static AtTask m_interruptTask = NULL;
static eBool m_interruptTaskEnabled = cAtFalse;




/* Eye scan task */
static AtTask m_eyescanTask = NULL;
static eBool m_eyescanTaskEnabled = cAtFalse;

/* HAL server task */
static AtTask m_halServerTask = 0;
static AtHal m_serverHal = NULL;

/* Shell */
static pid_t cliShellPid  = -1;
static int socketListen   = -1;
static int socketCliShell = -1;
static int cliShellTtyFd  = -1;

/*--------------------------- Forward declarations ---------------------------*/
atbool AppInterruptHandlerInstall(void);
extern uint32 RegisterAuditPerformPeriodicProcess(uint32 periodInMs);
extern void RegisterAuditDestroy(void);
extern void AtDeviceUpsrInterruptProcess(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device()
    {
    return CliDevice();
    }

static atbool CommunicationInit(void)
    {
    if (socketListen < 0)
        {
        socketListen = SocketListen(AppListenPortGet());
        }

    return (socketListen < 0)? cAtFalse : cAtTrue;
    }

static uint32 SocketMsgReceive(char *msgBuf, uint32 msgSize)
    {
    uint32 msgLen = 0;
    tMsgHeader *pMsgHeader;
    uint32 recvByte;

    if (socketCliShell < 0)
        {
        if ((socketCliShell = SocketAccept(socketListen)) < 0 )
            {
            return 0;
            }
        }

    /* Receive header */
    if ((recvByte = SocketRecv(socketCliShell, msgBuf, sizeof(tMsgHeader))) < sizeof(tMsgHeader))
        return recvByte;
    msgLen += recvByte;

    /* Convert from network byte order to host byte order */
    pMsgHeader = (tMsgHeader *)msgBuf;
    pMsgHeader->msgId = ntohl(pMsgHeader->msgId);
    pMsgHeader->msgLen = ntohl(pMsgHeader->msgLen);
    pMsgHeader->senderPid = ntohl(pMsgHeader->senderPid);

    /* Receive payload */
    recvByte = SocketRecv(socketCliShell, &msgBuf[recvByte], pMsgHeader->msgLen - sizeof(tMsgHeader));
    msgLen += recvByte;

    return msgLen;
    }

static uint32 CommunicationMsgReceive(char *msgBuf, uint32 msgSize)
    {
    return SocketMsgReceive(msgBuf, msgSize);
    }

static void StdIoRedirect(const char *newStdIoName)
    {
    extern void ClishStdIoRedirect(int stdIoFd);
    static eBool firstConnected = cAtTrue;

    cliShellTtyFd = open(newStdIoName, O_RDWR);
    if (cliShellTtyFd >= 0)
        {
        ClishStdIoRedirect(cliShellTtyFd);

        /* Print start up string */
        if (firstConnected)
            firstConnected = cAtFalse;
        else
            mPrint(CLI_SHELL_STARTUP_STR);
        }
    }

static void StdIoRestore(void)
    {
    extern void ClishStdIoRestore(void);

    ClishStdIoRestore();

    if (cliShellTtyFd >= 0)
        {
        close(cliShellTtyFd);
        cliShellTtyFd = -1;
        }
    }

static void CommunicationHandle(void)
    {
    static char msgBuf[cMsgMaxLength];
    static tMsgHeader *pMsgHeader;

    pMsgHeader = (tMsgHeader *)msgBuf;

    AtOsalMemInit(msgBuf, 0, cMsgMaxLength);

    if (CommunicationMsgReceive(msgBuf, cMsgMaxLength) < sizeof(tMsgHeader))
        return;

    switch (pMsgHeader->msgId)
        {
        /* If there is shell connected, send signal to shell to terminate it */
        case cMsgCliShellConnect:
            if (cliShellPid >= 0)
                kill(cliShellPid, SIGTERM);

            cliShellPid = pMsgHeader->senderPid;
            StdIoRedirect(&msgBuf[sizeof(tMsgHeader)]);

            break;

        /* Disconnect */
        case cMsgCliShellDisconnect:
            StdIoRestore();
            cliShellPid = -1;
            if (socketCliShell >= 0)
                {
                if (SocketClose(socketCliShell) < 0)
                    mPrintError("Cannot close CLI socket\r\n");
                socketCliShell = -1;
                }

            break;

        default:
            break;
        }
    }

static void *CommThreadHandler(void *pData)
    {
    CommunicationInit();
    while (1)
        {
        AtTaskTestCancel(commTask);
        AtOsalUSleep(100000);
        CommunicationHandle();
        }

    return NULL;
    }

extern uint32 RegisterMonPerformPeriodicProcess(uint32 periodInMs);
static void *PeriodicThreadHandler(void *pData)
    {
    uint32 elapseTime;

    while (1)
        {
        AtTaskTestCancel(m_pollingTask);
        if (!AppPollingTaskIsEnabled())
            {
            AtOsalUSleep(m_taskPeriodInMs * 1000);
            continue;
            }

        elapseTime = RegisterMonPerformPeriodicProcess(m_taskPeriodInMs);
        /* BER processing. Just sleep in remain time, to make sure task period */
        elapseTime += AtModuleBerPeriodicProcess((AtModuleBer)AtDeviceModuleGet(Device(), cAtModuleBer), m_taskPeriodInMs);
        if (elapseTime < m_taskPeriodInMs)
            AtOsalUSleep((m_taskPeriodInMs - elapseTime) * 1000);

#if 0 /* Keep it to open later if need */
        elapseTime += RegisterAuditPerformPeriodicProcess(m_taskPeriodInMs);
        AtModuleApsPeriodicProcess((AtModuleAps)AtDeviceModuleGet(Device(), cAtModuleAps), m_taskPeriodInMs);
        AtModulePeriodicProcess(AtDeviceModuleGet(Device(), cAtModulePdh), m_taskPeriodInMs);
#endif
        }

    return NULL;
    }

eBool AppFmPmTaskIsEnabled(void)
    {
    return m_FmPmTaskEnabled;
    }

eBool AppForcePointerTaskIsEnabled(void)
    {
    return m_pollingForcePointerTaskEnabled;
    }

static void *PeriodicForcePointerThreadHandler(void *pData)
    {
    while (1)
        {
        AtTaskTestCancel(m_pollingForcePointerTask);
        if (!AppForcePointerTaskIsEnabled())
            {
            AtOsalUSleep(m_pollingForcePointerTaskPeriodInMs);
            continue;
            }

        /* TIE processing. Just sleep in remain time, to make sure task period */
        AtOsalUSleep(m_pollingForcePointerTaskPeriodInMs* 1000);
        AtModuleForcePointerPeriodicProcess(AtDeviceModuleGet(Device(), cAtModuleSdh), m_pollingForcePointerTaskPeriodInMs);
        }

    return NULL;
    }

static void *PeriodicdFmPmThreadHandler(void *pData)
    {
    AtModule module = (AtModule)AtDeviceModuleGet(Device(), cAtModuleSur);
    uint32 taskPeriod = AtModuleRecommendedPeriodInMs(module);

    while (1)
        {
        AtTaskTestCancel(m_FmPmTask);

        if (AppFmPmTaskIsEnabled())
            AtModulePeriodicProcess(module, taskPeriod);

        AtOsalUSleep(taskPeriod * 1000);
        }

    return NULL;
    }

static void *InterruptThreadHandler(void *pData)
    {
    while (1)
        {
        AtTaskTestCancel(m_interruptTask);

        if (!AppInterruptTaskIsEnabled())
            {
            AtOsalUSleep(cAtTaskInterruptPeriodUs);
            continue;
            }

        /* Interrupt processing */
        AtDeviceInterruptProcess(Device());
        AtDeviceUpsrInterruptProcess(Device());
        AtOsalUSleep(cAtTaskInterruptPeriodUs);
        }

    return NULL;
    }

static eBool CreatePollingTask()
    {
    m_pollingTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "polling task", NULL, PeriodicThreadHandler);
    if (m_pollingTask == NULL)
        {
        mPrintError("Create Periodic task\r\n");
        return cAtFalse;
        }

    return (AtTaskStart(m_pollingTask) == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool CreateFmPmTask()
    {
    m_FmPmTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "pm fm task", NULL, PeriodicdFmPmThreadHandler);
    if (m_FmPmTask == NULL)
        {
        mPrintError("Create FM/PM task\r\n");
        return cAtFalse;
        }

    if (AtTaskStart(m_FmPmTask) != cAtOk)
        {
        mPrintError("Start FM/PM task\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool CreateForcePointerTask()
    {
    m_pollingForcePointerTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "force pointer task", NULL, PeriodicForcePointerThreadHandler);
    if (m_pollingForcePointerTask == NULL)
        {
        mPrintError("Create Force Pointer task\r\n");
        return cAtFalse;
        }

    if (AtTaskStart(m_pollingForcePointerTask) != cAtOk)
        {
        mPrintError("Start ForcePointer task\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool CreateInterruptTask()
    {
    m_interruptTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "interrupt task", NULL, InterruptThreadHandler);
    if (m_interruptTask == NULL)
        {
        mPrintError("Create Interrupt task\r\n");
        return cAtFalse;
        }

    return (AtTaskStart(m_interruptTask) == cAtOk) ? cAtTrue : cAtFalse;
    }

static void *HalServerTheadHandler(void *pData)
    {
    while (1)
        {
        AtTaskTestCancel(m_halServerTask);
        AtHalTcpServerRun(AppHalServer());
        }

    return NULL;
    }

static eBool CreateHalServerTask()
    {
    if (AppPlatform() == cPlatformRemote)
        return cAtTrue;

    if (AppHalServer() == NULL)
        {
        AtPrintc(cSevWarning, "HAL server will not be started\r\n");
        return cAtTrue;
        }

    m_halServerTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "HAL server", NULL, HalServerTheadHandler);
    if (m_halServerTask == NULL)
        {
        mPrintError("Create HAL server task\r\n");
        return cAtFalse;
        }

    return (AtTaskStart(m_halServerTask) == cAtOk) ? cAtTrue : cAtFalse;
    }

static void SocketCleanup()
    {
    if (socketListen >= 0)
        {
        if (SocketClose(socketListen) < 0)
            mPrintError("Cannot close listen socket\r\n");
        socketListen = -1;
        }

    if (socketCliShell >= 0)
        {
        if (SocketClose(socketCliShell) < 0)
            mPrintError("Cannot close CLI socket\r\n");
        socketCliShell = -1;
        }
    }

static void CommunicationCleanup(void)
    {
    SocketCleanup();
    }

void CliClientExit(void)
    {
    StdIoRestore();
    if (cliShellPid < 0)
        return;

    kill(cliShellPid, SIGTERM);
    cliShellPid = -1;
    }

atbool AppAllTasksCreate(void)
    {
    eBool ret = cAtTrue;
    if (AppIsDeamon())
        {
        if (commTask != NULL)
            {
            mPrintInfo("Main task ID: %u", getpid());
            return cAtTrue;
            }

        commTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "Communication task", NULL, CommThreadHandler);
        if (commTask == NULL)
            {
            mPrintError("Create communication task\r\n");
            return cAtFalse;
            }

        if (AtTaskStart(commTask) != cAtOk)
            return cAtFalse;
        }

    mPrintInfo("Main task ID: %u", getpid());
    ret = CreateHalServerTask();

    AppInterruptHandlerInstall();

    return ret;
    }

static eBool TaskDelete(AtTask task)
    {
    if (task == NULL)
        return cAtTrue;

    if (AtTaskStop(task) != cAtOk)
        {
        AtTaskManagerTaskDelete(AtOsalTaskManagerGet(), task);
        return cAtFalse;
        }

    if (AtTaskJoin(task) != cAtOk)
        {
        AtTaskManagerTaskDelete(AtOsalTaskManagerGet(), task);
        return cAtFalse;
        }

    return (AtTaskManagerTaskDelete(AtOsalTaskManagerGet(), task) == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool CommTaskDelete()
    {
    eBool success = TaskDelete(commTask);
    commTask = NULL;
    if (!success)
        {
        mPrintError("Cleanup Communication task\r\n");
        }

    CommunicationCleanup();

    return success;
    }

static eBool PollingTaskDelete()
    {
    eBool success = TaskDelete(m_pollingTask);
    m_pollingTask = NULL;
    if (!success)
        {
        mPrintError("Cleanup Periodic task\r\n");
        }

    return success;
    }

static eBool PollingForcePointerTaskDelete()
    {
    eBool success = TaskDelete(m_pollingForcePointerTask);
    m_pollingForcePointerTask = NULL;
    if (!success)
        {
        mPrintError("Cleanup Force Pointer Periodic task\r\n");
        }

    return success;
    }


static eBool FmPmTaskDelete()
    {
    eBool success = TaskDelete(m_FmPmTask);

    if (!success)
        {
        mPrintError("Cleanup Periodic task\r\n");
        }

    return success;
    }


static eBool InterruptTaskDelete()
    {
    eBool success = TaskDelete(m_interruptTask);
    m_interruptTask = NULL;
    if (!success)
        {
        mPrintError("Cleanup interrupt task\r\n");
        }

    return success;
    }

static eBool SdhLinesEyeScan(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    eBool complete = cAtTrue;
    uint8 line_i;

    for (line_i = 0; line_i < AtModuleSdhMaxLinesGet(sdhModule); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, line_i);
        AtEyeScanController controller = AtSdhLineEyeScanControllerGet(line);
        eAtRet ret = AtEyeScanControllerScan(controller);

        if (!AtEyeScanControllerFinished(controller))
            complete = cAtFalse;

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Eye-scan fail on %s with ret = %s\r\n",
                     AtObjectToString((AtObject)controller), AtRet2String(ret));
            }
        }

    return complete;
    }

static eBool EthPortsEyeScan(void)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
    eBool complete = cAtTrue;
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(ethModule); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(ethModule, port_i);
        AtEyeScanController controller = AtEthPortEyeScanControllerGet(port);
        eAtRet ret = AtEyeScanControllerScan(controller);

        if (!AtEyeScanControllerFinished(controller))
            complete = cAtFalse;

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Eye-scan fail on %s with ret = %s\r\n",
                     AtObjectToString((AtObject)controller), AtRet2String(ret));
            }
        }

    return complete;
    }

static eBool ModulesEyeScan(void)
    {
    eBool eyeScanCompleted = cAtTrue;

    if (!SdhLinesEyeScan())
        eyeScanCompleted = cAtFalse;
    if (!EthPortsEyeScan())
        eyeScanCompleted = cAtFalse;

    return eyeScanCompleted;
    }

static eBool SerdesManagerEyeScan(void)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(Device());
    eBool complete = cAtTrue;
    uint8 serdes_i;

    for (serdes_i = 0; serdes_i < AtSerdesManagerNumSerdesControllers(serdesManager); serdes_i++)
        {
        eAtRet ret;
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(serdesManager, serdes_i);
        AtEyeScanController controller = AtSerdesControllerEyeScanControllerGet(serdes);

        if (controller == NULL)
            continue;

        ret = AtEyeScanControllerScan(controller);

        if (!AtEyeScanControllerFinished(controller))
            complete = cAtFalse;

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Eye-scan fail on %s with ret = %s\r\n",
                     AtObjectToString((AtObject)controller), AtRet2String(ret));
            }
        }

    return complete;
    }

static void *EyeScanThreadHandler(void *pData)
    {
    while (1)
        {
        eBool eyeScanCompleted = cAtTrue;

        AtTaskTestCancel(m_eyescanTask);

        if (!m_eyescanTaskEnabled)
            break;

        if (AtDeviceSerdesManagerGet(Device()))
            eyeScanCompleted = SerdesManagerEyeScan();
        else
            eyeScanCompleted = ModulesEyeScan();

        if (eyeScanCompleted)
            break;
        }

    AtTaskManagerTaskDelete(AtOsalTaskManagerGet(), m_eyescanTask);
    m_eyescanTask = NULL;

    return NULL;
    }

static eBool CreateEyeScanTask(void)
    {
    m_eyescanTask = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "eye-scan", NULL, EyeScanThreadHandler);
    if (m_eyescanTask == NULL)
        {
        mPrintError("Create eye scan task\r\n");
        return cAtFalse;
        }

    if (AtTaskStart(m_eyescanTask) != cAtOk)
        {
        mPrintError("Start eye scan task\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool EyeScanTaskDelete(void)
    {
    eBool success = TaskDelete(m_eyescanTask);
    m_eyescanTask = 0;

    if (!success)
        {
        mPrintError("Cleanup eyescan task\r\n");
        }

    return success;
    }

static eBool HalServerTaskDelete()
    {
    eBool success = TaskDelete(m_halServerTask);

    m_halServerTask = 0;

    if (!success)
        {
        mPrintError("Cleanup HAL server task\r\n");
        }

    AtHalDelete(m_serverHal);
    m_serverHal = NULL;

    return success;
    }

atbool AppAllTasksDelete(void)
    {
    atbool ret = cAtTrue;
    RegisterAuditDestroy();
    ret |= CommTaskDelete();
    ret |= PollingTaskDelete();
    ret |= InterruptTaskDelete();
    ret |= EyeScanTaskDelete();
    ret |= HalServerTaskDelete();
    ret |= FmPmTaskDelete();
    ret |= PollingForcePointerTaskDelete();
    return ret;
    }

eBool AppPollingTaskIsEnabled()
    {
    return m_pollingTaskEnabled;
    }

eBool AppInterruptTaskIsEnabled()
    {
    return m_interruptTaskEnabled;
    }

void AppPollingTaskEnable(eBool enable)
    {
    if (m_pollingTask == NULL)
        CreatePollingTask();

    m_pollingTaskEnabled = enable;
    }

void AppFmPmTaskEnable(eBool enable)
    {
    if (m_FmPmTask == 0)
        CreateFmPmTask();

    m_FmPmTaskEnabled = enable;
    }

void AppForcePointerTaskPeriodInMsSet(uint32 periodMs)
    {
    m_pollingForcePointerTaskPeriodInMs = periodMs;
    }

void AppForcePointerTaskEnable(eBool enable)
    {
    if (m_pollingForcePointerTask == NULL)
        CreateForcePointerTask();

    m_pollingForcePointerTaskEnabled = enable;
    }

void AppPollingTaskPeriodInMsSet(uint32 periodMs)
    {
    m_taskPeriodInMs = periodMs;
    }

void AppInterruptTaskEnable(eBool enable)
    {
    if (m_interruptTask == NULL)
        CreateInterruptTask();

    m_interruptTaskEnabled = enable;
    }

void AppEyeScanTaskEnable(eBool enable)
    {
    if (m_eyescanTask == 0)
        CreateEyeScanTask();

    m_eyescanTaskEnabled = enable;
    }

/*
 * Handler of device interrupt
 * @param sigNum Signal number
 */
static void DeviceInterruptSignalHandler(int sigNum)
    {
    /* If client connect, it will control interrupt processing, just let it know */
    if (AppInterruptMessageCanSend())
        AppInterruptMessageSend();

    /* Process interrupt if client has not connected */
    else
        AtDeviceInterruptProcess(Device());
    }

atbool AppInterruptHandlerInstall(void)
    {
    int fd_proc;
    char buf[11];
    static const int cDeviceAlarmInterruptSignal = SIGUSR1;
    static const char *cEpappProcFileName = "/proc/epapp";

    fd_proc = open(cEpappProcFileName, O_WRONLY);
    if (fd_proc < 0)
        {
        AtPrintc(cSevWarning, "Open file %s fail, ignore kernel interrupt\r\n", cEpappProcFileName);
        return cAtFalse;
        }

    sprintf(buf, "%i", getpid());
    if (write(fd_proc, buf, strlen(buf) + 1) < 0)
        {
        mPrintError("Can't write file %s a pid=%s\r\n", cEpappProcFileName, buf);
        return cAtFalse;
        }

    if (SignalHandlerInstall(cDeviceAlarmInterruptSignal, DeviceInterruptSignalHandler, SA_SIGINFO) != cAtTrue)
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool AppInterruptMessageCanSend(void)
    {
    return AtHalTcpServerInterruptEventCanSend(m_serverHal);
    }

void AppInterruptMessageSend(void)
    {
    if (m_serverHal)
        AtHalTcpServerInterruptEventSend(m_serverHal);
    }

AtHal AppHalServer(void)
    {
    AtHal realHal;
    AtDevice device;

    if (m_serverHal)
        return m_serverHal;

    device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    realHal = AtDeviceIpCoreHalGet(device, 0);
    m_serverHal = AtHalTcpServerNew(AppHalListenPortGet(), AppHalEventNotifyPortGet(), realHal);
    return m_serverHal;
    }


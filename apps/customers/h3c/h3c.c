/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : h3c.c
 *
 * Created Date: Dec 27, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "apputil.h"
#include "AtDriver.h"
#include "h3c.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDriver Driver(void)
    {
    return AtDriverSharedDriverGet();
    }

static uint32 BaseAddressOfSlot(uint8 slotId)
    {
    /* Note: to debug by "devmem", the base address 0xB0000000 is used for instead */
    static const uint32 CPU_BASE_ADDRESS = 0xD0000000;

    if (slotId == 0)
        return CPU_BASE_ADDRESS + (0x100000 << 1);
    if (slotId == 1)
        return CPU_BASE_ADDRESS + (0x200000 << 1);

    return 0;
    }

AtHal H3CCreateHalForSlot(uint8 slotId)
    {
    return AtHalIndirectDefaultNew(BaseAddressOfSlot(slotId));
    }

uint32 H3CProductCodeOfSlot(uint8 slotId)
    {
    AtHal hal;
    uint32 code;

    code = AppSpecifiedProductCode();
    if (code)
        return code;

    hal = H3CCreateHalForSlot(slotId);
    code = AtHalRead(hal, 0);
    AtHalDelete(hal);

    return code;
    }

static AtDevice H3CSlotSetup(uint8 slotId)
    {
    uint8 coreId;
    AtHal hal;
    uint32 code;
    AtDevice slot;

    /* Create device for this slot */
    code = H3CProductCodeOfSlot(slotId);
    if (code == 0)
        return NULL;

    slot = AtDriverDeviceCreate(Driver(), code);
    if (slot == NULL)
        return NULL;

    /* Setup HAL */
    hal = H3CCreateHalForSlot(slotId);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(slot); coreId++)
        AtIpCoreHalSet(AtDeviceIpCoreGet(slot, coreId), hal);

    return slot;
    }

static AtDevice H3CSlot(uint8 slotId)
    {
    uint8 numAddedDevices;
    AtDevice *devices;

    devices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    if (slotId >= numAddedDevices)
        return NULL;

    return devices[slotId];
    }

/* To be used in the future */
#if 0
static uint8 PortId2VodId(uint8 portId)
    {
    if (portId == 0) return 0;
    if (portId == 1) return 1;
    if (portId == 2) return 4;
    if (portId == 3) return 8;

    return 0;
    }

void SetStm4VodForPort(uint8 portId)
    {
    DiagHalWrite1(0xf28008, PortId2VodId(portId)); /* Select port */
    DiagHalWrite1(0xf2800B, 0x0);    /* Address */
    DiagHalWrite1(0xf2800C, 0x3f);   /* (Data) STM-4: 0x3f, STM-1: 0x08 */
    DiagHalWrite1(0xf2800A, 0x1);    /* Control: 1 - write, 0 - read */
    }

void SetStm1VodForPort(uint8 portId)
    {
    DiagHalWrite1(0xf28008, PortId2VodId(portId)); /* Select port */
    DiagHalWrite1(0xf2800B, 0x0);    /* Address */
    DiagHalWrite1(0xf2800C, 0x8);   /* (Data) STM-4: 0x3f, STM-1: 0x08 */
    DiagHalWrite1(0xf2800A, 0x1);    /* Control: 1 - write, 0 - read */
    }
#endif

static void VodInitForSlot(uint8 slotId)
    {
    /* Get handle of this slot */
    AtDevice slot = H3CSlot(slotId);
    AtHal hal;
    if (slot == NULL)
        return;

    /* Only apply for STM slot */
    if (AtDeviceProductCodeGet(slot) != 0x60070023)
        return;

    hal = AtDeviceIpCoreHalGet(slot, 0);
    AtHalWrite(hal, 0xf28008, 0x0);
    AtHalWrite(hal, 0xf2800B, 0x0);
    AtHalWrite(hal, 0xf2800C, 0x3f);
    AtHalWrite(hal, 0xf2800A, 0x1);

    AtHalWrite(hal, 0xf28008, 0x1);
    AtHalWrite(hal, 0xf2800B, 0x0);
    AtHalWrite(hal, 0xf2800C, 0x3f);
    AtHalWrite(hal, 0xf2800A, 0x1);

    AtHalWrite(hal, 0xf28008, 0x4);
    AtHalWrite(hal, 0xf2800B, 0x0);
    AtHalWrite(hal, 0xf2800C, 0x3f);
    AtHalWrite(hal, 0xf2800A, 0x1);

    AtHalWrite(hal, 0xf28008, 0x5);
    AtHalWrite(hal, 0xf2800B, 0x0);
    AtHalWrite(hal, 0xf2800C, 0x3f);
    AtHalWrite(hal, 0xf2800A, 0x1);

    AtHalWrite(hal, 0xf28008, 0x8);
    AtHalWrite(hal, 0xf2800B, 0x0);
    AtHalWrite(hal, 0xf2800C, 0x3f);
    AtHalWrite(hal, 0xf2800A, 0x1);

    AtHalWrite(hal, 0xf28008, 0x9);
    AtHalWrite(hal, 0xf2800B, 0x0);
    AtHalWrite(hal, 0xf2800C, 0x3f);
    AtHalWrite(hal, 0xf2800A, 0x1);
    }

static void VodInit(void)
    {
    VodInitForSlot(0);
    VodInitForSlot(1);
    }

eBool H3CSystemInit(void)
    {
    uint8 numAddedSlots;

    /* Create driver */
    if (Driver() == NULL)
        AtDriverCreate(2, AtOsalSharedGet());

    /* Create device */
    if (Driver() != NULL)
        {
        uint8 slot_i;

        for (slot_i = 0; slot_i < 2; slot_i++)
            {
            if (H3CSlotSetup(slot_i))
                AtPrintc(cSevInfo, "Device at slot %d is added\n", slot_i + 1);
            else
                AtPrintc(cSevInfo, "No device at slot %d\n", slot_i + 1);
            }
        }

    AtDriverAddedDevicesGet(Driver(), &numAddedSlots);
    if (numAddedSlots == 0)
        AtPrintc(cSevCritical, "No slot is added\n");

    /* Initialize slots */
    VodInit();

    return numAddedSlots ? cAtTrue : cAtFalse;
    }


#define _LARGEFILE64_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int arrive_32e1_fpga_read(unsigned long address, unsigned long *value);
int arrive_32e1_fpga_write(unsigned long address, unsigned long value);

#define ARRIVE_FPGA_BASE    0x90000000

/*! Read a Arrive FPGA register
 * settings.
 * \Parameters
 *      \param[in] unsigned long address: FPGA register address
 *      \param[out] unsigned long *value: Read value for FPGA register
 * \Return
 *      \retval 0 for success, -1 for fail
 */
int
arrive_32e1_fpga_read(unsigned long address, unsigned long *value)
{
    unsigned char buffer[4];
    int fd;
    off64_t offset;
    int readn;

    offset=ARRIVE_FPGA_BASE+address*4;

    if((fd=open("/dev/mem", O_RDONLY))<0)
        return(-1);

    if(lseek64(fd, offset, SEEK_SET)!=offset)
    {
        close(fd);
        return(-1);
    }

    readn=read(fd, buffer, sizeof(buffer));
    if(readn<=0)
    {
        close(fd);
        return(-1);
    }

    *value=(unsigned long)(buffer[0]|(buffer[1]<<8)|(buffer[2]<<16)|(buffer[3]<<24));

    close(fd);

    return(0);
}

/*! Write a Arrive FPGA register
 * settings.
 * \Parameters
 *      \param[in] unsigned long address: FPGA register address
 *      \param[int] unsigned long value: Value to write
 * \Return
 *      \retval 0 for success, -1 for fail
 */
int
arrive_32e1_fpga_write(unsigned long address, unsigned long value)
{
    unsigned char *buffer;
    unsigned long val32;
    int fd;
    off64_t offset;

    offset=ARRIVE_FPGA_BASE+address*4;

    if((fd=open("/dev/mem", O_RDWR))<0)
        return(-1);

    if(lseek64(fd, offset, SEEK_SET)!=offset)
    {
        close(fd);
        return(-1);
    }

    buffer=(unsigned char *)&value;
    val32=(unsigned long)(buffer[0]|(buffer[1]<<8)|(buffer[2]<<16)|(buffer[3]<<24));
    if(write(fd, &val32, sizeof(val32))!=sizeof(val32))
    {
        close(fd);
        return(-1);
    }

    close(fd);

    return(0);
}

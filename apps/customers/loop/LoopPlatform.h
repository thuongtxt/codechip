/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Loop
 * 
 * File        : LoopPlatform.h
 * 
 * Created Date: May 13, 2015
 *
 * Description : Work with Loop platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _LOOPPLATFORM_H_
#define _LOOPPLATFORM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal Loop16E1HalCreate(void);
AtHal Loop32E1HalCreate(void);
AtHal LoopStmHalCreate(void);

#ifdef __cplusplus
}
#endif
#endif /* _LOOPPLATFORM_H_ */


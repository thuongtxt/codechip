/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : LoopPlatform.c
 *
 * Created Date: Feb 24, 2014
 *
 * Description : To work with Loop platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalLoop.h"
#include "loop.h"
#include "LoopPlatform.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern int arrive_16e1_fpga_read(unsigned long address, unsigned long *value);
extern int arrive_16e1_fpga_write(unsigned long address, unsigned long value);
extern int arrive_32e1_fpga_read(unsigned long address, unsigned long *value);
extern int arrive_32e1_fpga_write(unsigned long address, unsigned long value);
extern int arrive_stm_fpga_read(unsigned long address, unsigned long *value);
extern int arrive_stm_fpga_write(unsigned long address, unsigned long value);

/*--------------------------- Implementation ---------------------------------*/
AtHal Loop16E1HalCreate(void)
    {
    return AtHalLoopNew(arrive_16e1_fpga_read, arrive_16e1_fpga_write);
    }

AtHal Loop32E1HalCreate(void)
    {
    return AtHalLoopNew(arrive_32e1_fpga_read, arrive_32e1_fpga_write);
    }

AtHal LoopStmHalCreate(void)
    {
    return AtHalLoopNew(arrive_stm_fpga_read, arrive_stm_fpga_write);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : arrive_fpga.h
 * 
 * Created Date: Mar 3, 2015
 *
 * Description : LOOP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ARRIVE_FPGA_H_
#define _ARRIVE_FPGA_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
int arrive_fpga_read(unsigned long address, unsigned long *value);
int arrive_fpga_write(unsigned long address, unsigned long value);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ARRIVE_FPGA_H_ */


#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define ARRIVE_FPGA_BASE	0x90000000
#define ARRIVE_FPGA_SIZE	0x08000000

int arrive_stm_fpga_read(unsigned long address, unsigned long *value);
int arrive_stm_fpga_write(unsigned long address, unsigned long value);

static int pciefd=0;
static volatile unsigned long *pciebase=0;

/*! Read a Arrive FPGA register
 * settings.
 * \Parameters
 *		\param[in] unsigned long address: FPGA register address
 *		\param[out] unsigned long *value: Read value for FPGA register
 * \Return
 * 		\retval 0 for success, -1 for fail
 */
int
arrive_stm_fpga_read(unsigned long address, unsigned long *value)
{
	if(pciebase==0)
	{
		int fd;
		void *base;
		
		if((fd=open("/dev/mypcie", O_RDWR))<0)
			return(-1);

		base=mmap((void *)ARRIVE_FPGA_BASE, ARRIVE_FPGA_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_FIXED, fd, 0);
		if(base==MAP_FAILED)
		{
			close(fd);			
			return(-1);
		}
		
		pciefd=fd;
		pciebase=base;
	}
	
	*value=*(pciebase+address);
	
	return(0);
}

/*! Write a Arrive FPGA register
 * settings.
 * \Parameters
 *		\param[in] unsigned long address: FPGA register address
 *		\param[int] unsigned long value: Value to write
 * \Return
 * 		\retval 0 for success, -1 for fail
 */
int
arrive_stm_fpga_write(unsigned long address, unsigned long value)
{
	if(pciebase==0)
	{
		int fd;
		void *base;
		
		if((fd=open("/dev/mypcie", O_RDWR))<0)
			return(-1);

		base=mmap((void *)ARRIVE_FPGA_BASE, ARRIVE_FPGA_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_FIXED, fd, 0);
		if(base==MAP_FAILED)
		{
			close(fd);			
			return(-1);
		}
		
		pciefd=fd;
		pciebase=base;
	}
	
	*(pciebase+address)=value;

	return(0);
}

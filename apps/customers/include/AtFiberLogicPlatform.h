/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtFiberLogicPlatform.h
 * 
 * Created Date: Apr 14, 2015
 *
 * Description : To work with FiberLogic platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFIBERLOGICPLATFORM_H_
#define _ATFIBERLOGICPLATFORM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtFiberLogicPlatformFpgaBaseAddress(void);
eAtRet AtFiberLogicPlatformFinalize(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATFIBERLOGICPLATFORM_H_ */


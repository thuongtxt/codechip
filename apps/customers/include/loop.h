/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : loop.h
 * 
 * Created Date: Mar 3, 2015
 *
 * Description : Loop
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _LOOP_H_
#define _LOOP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalLoop.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal LoopHalCreate(void);

#ifdef __cplusplus
}
#endif
#endif /* _LOOP_H_ */


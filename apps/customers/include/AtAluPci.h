/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PCI
 * 
 * File        : AtAluPci.h
 * 
 * Created Date: Sep 30, 2014
 *
 * Description : Wrap ALU's pci function
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATALUPCI_H_
#define _ATALUPCI_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalAlu.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal AtAluHalCreate(void);
void AtAluPciCleanup(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATCUSTOMER_H_ */

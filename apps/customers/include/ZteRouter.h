/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : ZteRouter.h
 * 
 * Created Date: Jun 4, 2015
 *
 * Description : To work with ZTE Router platforms
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _APPS_CUSTOMERS_INCLUDE_ZTEROUTER_H_
#define _APPS_CUSTOMERS_INCLUDE_ZTEROUTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ZteRouterStmBaseAddressGet(uint8 coreId);
AtHal ZteRouterStmHalCreate(uint32 baseAdr);

#ifdef __cplusplus
}
#endif
#endif /* _APPS_CUSTOMERS_INCLUDE_ZTEROUTER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : zte.h
 * 
 * Created Date: Mar 3, 2015
 *
 * Description : ZTE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ZTE_H_
#define _ZTE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHalZtePtn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHal ZtePtnHalCreate(void);

#ifdef __cplusplus
}
#endif
#endif /* _ZTE_H_ */


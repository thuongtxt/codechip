/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtFhw.c
 *
 * Created Date: Oct 2, 2013
 *
 * Description : To work with FHW VxWorks platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtList.h"
#include "AtHalFhw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_baseAddress = 0x70000000;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice AddedDevice()
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    return (numAddedDevices == 0) ? NULL : addedDevices[0];
    }

static uint32 BaseAddressOfFpga(uint8 fpgaId)
    {
    return m_baseAddress;
    }

static AtHal CreateHal(uint32 baseAddress)
    {
    return AtHalFhwNew(baseAddress);
    }

static uint32 ReadProductCode(uint32 baseAddress)
    {
    uint32 productCode;
    AtHal hal;

    hal = CreateHal(baseAddress);
    productCode = AtProductCodeGetByHal(hal);
    AtHalDelete(hal);

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    return productCode;
    }

/* To setup HALs for all of IP Cores */
static void DeviceSetup(AtDevice device)
    {
    uint8 fpga_i;

    for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
        {
        uint32 fpgaBaseAddress = BaseAddressOfFpga(fpga_i);
        AtIpCoreHalSet(AtDeviceIpCoreGet(AddedDevice(), 0), CreateHal(fpgaBaseAddress));
        }
    }

static eAtRet DriverInit()
    {
    /* Create driver with one device */
    AtDriverCreate(1, AtOsalSharedGet());
    AtDriverDeviceCreate(AtDriverSharedDriverGet(), ReadProductCode(BaseAddressOfFpga(0)));
    if (AddedDevice() == NULL)
        return cAtErrorRsrcNoAvail;

    /* And setup it */
    DeviceSetup(AddedDevice());

    /* Optional: initialize device */
    AtDeviceInit(AddedDevice());

    return cAtOk;
    }

/* Save all HALs installed to this device */
static AtList AllDeviceHals(AtDevice device)
    {
    uint8 coreId;
    AtList hals;

    if (device == NULL)
        return NULL;

    hals = AtListCreate(0);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }

    return hals;
    }

/*
 * Sample device clean up function.
 *
 * Application create HALs and install them to AtDevice, so application must be
 * responsible for deleting these HALs. But, during device deleting, driver may
 * use HAL objects for special purposes, so HAL should be deleted after device
 * is deleted.
 *
 * The following sample code will backup all HALs that installed to device,
 * after deleting device, all of them are deleted
 */
static eAtRet DeviceCleanUp(AtDevice device)
    {
    AtList hals;

    if (device == NULL)
        return cAtOk;

    /* Save all HALs that are installed so far */
    hals = AllDeviceHals(device);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

    /* Delete all HALs */
    while (AtListLengthGet(hals) > 0)
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
    AtObjectDelete((AtObject)hals);

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    uint8 numDevices, i;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        DeviceCleanUp(device);
        }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);

    return cAtOk;
    }

static AtTextUI TinyTextUI()
    {
    extern tCmdConf cmdConf[];
    extern int cmdConfCount;

    return AtDefaultTinyTextUIGet(cmdConf, cmdConfCount);
    }

static void Write(uint32 address, uint16 value)
    {
    *((volatile uint16 *)address) = (uint16)value;
    }

/* Reset CES */
static void FpgaReset()
    {
    Write(0x20000022, 0x111);
    }

static void OpticalEnable()
    {
    Write(0x2000003e, 0);
    }

static void DisableResetLosAlarmOnSysFpga()
    {
    Write(0x20000a00, 1);
    }

static void DisableResetOhBus()
    {
    Write(0x20000800, 1);
    }

static void BoardSetup()
    {
    extern void HardInitBmu8308(void);
    extern void HardDownSysFpga(void);
    extern void HardResetSysFpga(void);
    extern void HardResetClkChip(void);
    extern void HardInitI2cChip(void);

    HardInitBmu8308();
    HardDownSysFpga();
    HardResetSysFpga();
    HardResetClkChip();
    HardInitI2cChip();
    FpgaReset();
    OpticalEnable();
    DisableResetLosAlarmOnSysFpga();
    DisableResetOhBus();
    }

void atsdk(eBool initBoard)
    {
    eAtRet ret;

    if (initBoard)
        BoardSetup();

    ret = DriverInit();
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot initialize driver, ret = %s\r\n", AtRet2String(ret));

    AtCliStartWithTextUI(TinyTextUI());

    /* Cleanup */
    DriverCleanup(AtDriverSharedDriverGet());
    AtCliStop();
    }

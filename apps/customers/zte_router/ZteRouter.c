/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : ZteRouter.c
 *
 * Created Date: May 20, 2015
 *
 * Description : ZTE application
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "atclib.h"
#include "AtHalZteRouter.h"
#include "ZteRouter.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void *mmap64 (void * addr, size_t len, int prot, int flags, int fildes, long long off);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MemoryMap(uint8 coreId)
    {
    void *address;
    int32 fd;
    const uint32 cZteRouterMapSize = 0x3DC0000;
    const uint32 cInvalidAddress = 0xFFFFFFFF;

    fd = open("/dev/mem", O_RDWR);
    if (fd < 0)
        {
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidAddress;
        }

    if (coreId == 0)
        address = mmap64(NULL, cZteRouterMapSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0xFF0000000ULL);
    else if (coreId == 1)
        address = mmap64(NULL, cZteRouterMapSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0xFE0000000ULL);
    else
        return cInvalidAddress;

    if (address == ((void *)-1))
        {
        close(fd);
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidAddress;
        }

    close(fd);

    return (uint32)(AtSize)address;
    }

uint32 ZteRouterStmBaseAddressGet(uint8 coreId)
    {
    return MemoryMap(coreId);
    }

AtHal ZteRouterStmHalCreate(uint32 baseAdr)
    {
    return AtHalZteRouterStmNew(baseAdr);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtFiberLogicPlatform.c
 *
 * Created Date: Apr 3, 2015
 *
 * Description : To work with FiberLogic platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHalMemoryMapper.h"
#include "AtFiberLogicPlatform.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHalMemoryMapper m_memoryMapper = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHalMemoryMapper MemoryMapper(void)
    {
    if (m_memoryMapper)
        return m_memoryMapper;

    m_memoryMapper = AtHalMemoryMapperLinuxNew(0xD0009000UL, 0xFFFFFFF);
    AtHalMemoryMapperMap(m_memoryMapper);

    return m_memoryMapper;
    }

uint32 AtFiberLogicPlatformFpgaBaseAddress(void)
    {
    return AtHalMemoryMapperBaseAddress(MemoryMapper());
    }

eAtRet AtFiberLogicPlatformFinalize(void)
    {
    AtHalMemoryMapperDelete(m_memoryMapper);
    m_memoryMapper = NULL;
    return cAtOk;
    }

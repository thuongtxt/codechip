/*
 * vim: set softtabstop=4 expandtab :
 *
 * the previous line configure vim when the file is edited
 ******************************************************************************/
/**
 * \file pci_cfg_map.h
 *
 * \brief  Declarations and functions to interact with FPGA devices on PCI bus.
 *
 * This module contains functions to check if a specified PCI device
 * (referenced by its VENDOR:DEVICE id) is present in the system, to map the
 * device BAR memory area into user space and to logically switch off the
 * device (operation needed before really detaching the device from the bus).
 *
 * \note
 * The codes are maintained by the OND Control Platform Lab and all assigned
 * values are available on its web site (http://platforms.tsd.alcatel.it/).
 *
 * \author Angelo Spada, Francesco Marletta
 *
 * \version 1.5
 *
 * \date 14/01/2010
 *
 */

#ifndef __PCI_CFG_MAP_IF_H__
#define __PCI_CFG_MAP_IF_H__

#ifdef __cplusplus
  extern "C" {
#endif

/*!
 * A struct containing the information needed to map a PCI device BAR memory
 * area into user space address.
 */
typedef struct pci_cfg_map_data_t {
    unsigned int vend_id;           //!< The PCI device \e vendor id
    unsigned int dev_id;            //!< The PCI device \e device id
    unsigned int bus_dev;           //!< The PCI bus device number
    unsigned int function;          //!< The PCI function device
    unsigned int bus_number;        //!< The PCI bus number
    unsigned int bar_num;           //!< The PCI \e BAR number to map

    unsigned int size_mapped;       //!< The size of the mapped memory area
    unsigned long long physical_addr;    //!< The physical address mapped by map
    volatile unsigned long* map;    //!< The pointer to the mapped memory

    unsigned long map_offset;       //!< The offset in the bar ares to start mapping from
    unsigned int  map_size;         //!< The size of the area that has to be mapped

    unsigned long long bar_addr;         //!< The physical address of the BAR area
    unsigned int  bar_size;         //!< The size of the whole BAR area
} pci_cfg_map_data;


/*!
 * This is a simple remap of the old name of the struct type, to assure legacy
 * code continue compiling.
 */
#define     PCI_CFG_MAP         pci_cfg_map_data


/*!
 * A value used to specify, in the following functions and data structs, to
 * process pci entries regardless of the pci bus device number.
 */
#define     BUS_DEVICE_ANY      (unsigned int)-1
#define     BUS_NUMBER_ANY      (unsigned int)-1

struct pci_bar_t {
  unsigned long long bar_addr;     //!< The BAR address
  unsigned long bar_size;     //!< The BAR size
};
typedef   struct pci_bar_t   pci_bar;


//int sysfs_pci_dev_base_addresses(unsigned short vendor_id, unsigned short device_id, unsigned int bus_device,
//                                 unsigned int function, pci_bar_array bar_data);


/*!
 * \brief   Maps the PCI device BAR memory into user address space.
 *
 * This function maps the whole BAR memory area indicated by \c bar_num into
 * \e pci_cfg_struct for the PCI device which vendor and device ids are
 * referenced by the \e vend_id and \e dev_id fields of \e pci_cfg_struct.
 *
 * \param   pci_cfg_struct  The struct containing the device references and
 *                          the mapped area.
 *
 * \return  \b 0 un success, a \b negative value on error.
 *
 *
 * The possible negative return values are:
 *  - \b -1  function failure due to missing element
 *  - \b -2  missing parameters in function invocation (or parameter void)
 *  - \b -3  function parameters outside correct values range
 *  - \b -4  file system problem during function execution
 *  - \b -5  a NULL pointer parameter has been passed or a memory
 *              allocation failed
 *  - \b -6  sys fs file for the PCI device has not been found
 *  - \b -7  a wrong parameter has been provided (maybe a jolly value
 *              provided to one of the fields)
 *
 */
int pci_cfg_map(pci_cfg_map_data *pci_cfg_struct);

/*!
 * \brief   Maps the PCI device BAR memory into user address space.
 *
 * This function maps a part of the BAR memory area indicated by \c bar_num 
 * into \e pci_cfg_struct for the PCI device which vendor and device ids are
 * referenced by the \e vend_id and \e dev_id fields of \e pci_cfg_struct.
 * 
 * The BAR area is restricted by the fields \c bar_offset and \c bar_size
 * inside the \e pci_cfg_struct parameter. If both values are set to 0, this
 * function behaves like pci_cfg_map.
 * 
 * A strict check is made on the values of these two fields, to ensure that 
 * the requested area is inside the BAR memory region. If the check is not
 * satisfied, the function returns with the exit code for wrong parameters.
 *
 * \param   pci_cfg_struct  The struct containing the device references and
 *                          the mapped area.
 *
 * \return  \b 0 un success, a \b negative value on error.
 *
 *
 * The possible negative return values are:
 *  - \b -1  function failure due to missing element
 *  - \b -2  missing parameters in function invocation (or parameter void)
 *  - \b -3  function parameters outside correct values range
 *  - \b -4  file system problem during function execution
 *  - \b -5  a NULL pointer parameter has been passed or a memory
 *              allocation failed
 *  - \b -6  sys fs file for the PCI device has not been found
 *  - \b -7  a wrong parameter has been provided (maybe a jolly value
 *              provided to one of the fields)
 *
 */
int pci_cfg_map_partial(pci_cfg_map_data *pci_cfg_struct);


/*!
 * \brief   Unmaps the memory area mapped by \c pci_cfg_map().
 *
 * This function performs a simple unmap for the memory area mapped by
 * pci_cfg_map() using the information stored inside \e pci_cfg_struct.
 *
 * \param   pci_cfg_struct  The struct containing the memory mapped area
 *                          references.
 *
 * \return  \b 0 on success, a \b negative value on error (see pci_cfg_map()
 *              for details on negative return values).
 *
 */
int pci_cfg_unmap(pci_cfg_map_data *pci_cfg_struct);


/*!
 * \brief   Check the sys fs for device with a given vendor_id:device_id pair.
 *
 * This function scan the sys virtual filesystem directory containing the PCI
 * devices the kernel is aware of (\c /sys/bus/pci/devices) and for each device
 * in this directory checks the vendor and device attributes with the specified
 * values.
 *
 * \param vendor_id     The \a vendor attribute to search for.
 * \param device_id     The \a device attribute to search for.
 * \param bus_device    The \a bus_device number to search for,
 *                      or \c BUS_DEVICE_ANY.
 *
 * \return      The function returns \b 0 if no device is found for the given
 *              input parameters, or a \b positive value reporting the number
 *              of devices that matches the vendor_id:device_id pair (when the
 *              parameter bus_device is BUS_DEVICE_ANY at most one device can
 *              match), or a \b negative value if some error has occurred
 *              during the search (see pci_cfg_map() for details on negative
 *              return values).
 *
 */
int pci_device_present_extended(unsigned int vendor_id,
                                unsigned int device_id,
                                unsigned int bus_device,
                                unsigned int function,
                                unsigned int bus_number);
#define pci_device_present(vendor_id,device_id,bus_device) \
        pci_device_present_extended(vendor_id,device_id,bus_device,-1, BUS_NUMBER_ANY);
#define pci_device_present_int(vendor_id,device_id,bus_device, function) \
        pci_device_present_extended(vendor_id,device_id,bus_device, function, BUS_NUMBER_ANY);


/*!
 * \brief   Release the pci device with a given vendor_id:device_id pair.
 *
 * This function searchs the sys virtual filesystem for the device with the
 * given vendor_id:device_id pair, and logically release it by invoking the
 * device \e release function (writing the correct value depenfing the value
 * of the \c rel_and_rem parameter).
 *
 * \param vendor_id     The \a vendor attribute to search for.
 * \param device_id     The \a device attribute to search for.
 * \param bus_device    The \a bus_device number to search for,
 *                      or \c BUS_DEVICE_ANY.
 * \param rel_and_rem   Specify if the device must be only released from the
 *                      driver (value 0) or if it must be released from the
 *                      driver and removed from the PCI bus (value 1).
 *
 * \return      The function returns \b 0 when the operation is successfully
 *              executed, a \b positive if the device is not found in the
 *              system or exists but don't have the \e release_driver attribute
 *              and a \b negative value if some error has occurred during the
 *              process (see pci_cfg_map() for details on negative return
 *              values).
 *
 */
int pci_device_release_extended(unsigned int vendor_id,
                                unsigned int device_id,
                                unsigned int bus_device,
                                unsigned int function,
                                unsigned int bus_number,
                                unsigned int rel_and_rem);
#define pci_device_release_int(vendor_id,device_id,bus_device, function, rel_and_rem) \
        pci_device_release_extended (vendor_id,device_id,bus_device,function ,BUS_NUMBER_ANY,rel_and_rem)
#define pci_device_release(vendor_id,device_id,bus_device,rel_and_rem) \
        pci_device_release_extended (vendor_id,device_id,bus_device,-1,BUS_NUMBER_ANY,rel_and_rem)


/*!
 * \brief   Execute a rescan of the PCI bus by triggering the pcirescan
 *          module functionality.
 *
 * This function triggers the \e cplab_pcirescan module functionality of
 * PCI bus rescan by writing something into the \c /proc/rescan entry.
 *
 * \return      \b 0 when the operation succeed, \b 1 when there is an error
 *              in writing into the proc entry (like the entry is not present
 *              since the module is not loaded for some reason).
 */
int pci_bus_rescan();

#ifdef __cplusplus
  }
#endif


#endif /* __PCI_CFG_MAP_IF_H__ */

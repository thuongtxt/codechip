#ifndef __CPLAB_IO_H_
#define __CPLAB_IO_H_

#if __cplusplus
    extern "C" {
#endif

/* function to ioremap and iounmap memory */
extern volatile void * ioremap(unsigned long long physaddr, unsigned size);
extern int iounmap(volatile void *start, size_t length);
extern int ks_sd_system(const char *cmd);
extern int cplab_bus_check_address (unsigned char* address);
extern int diskCheckTSS320H (void);
extern int diskCheckTSS320 (void);

extern int checkDiskTSS320H (void);
extern int checkDiskTSS320 (void);

/* Define ini/out macro so the appl doesn't need to include asm/io.h */
static inline int in_8(volatile unsigned char *addr)
{
        int ret;

        __asm__ __volatile__(
                "lbz%U1%X1 %0,%1;\n"
                "twi 0,%0,0;\n"
                "isync" : "=r" (ret) : "m" (*addr));
        return ret;
}

static inline int in_be16(volatile unsigned short *addr)
{
        int ret;

        __asm__ __volatile__("lhz%U1%X1 %0,%1;\n"
                             "twi 0,%0,0;\n"
                             "isync" : "=r" (ret) : "m" (*addr));
        return ret;
}

static inline int in_le16(volatile unsigned short *addr)
{
        int ret;

        __asm__ __volatile__("lhbrx %0,0,%1;\n"
                             "twi 0,%0,0;\n"
                             "isync" : "=r" (ret) :
                              "r" (addr), "m" (*addr));
        return ret;
}

static inline unsigned in_le32(volatile unsigned long *addr)
{
        unsigned ret;

        __asm__ __volatile__("lwbrx %0,0,%1;\n"
                             "twi 0,%0,0;\n"
                             "isync" : "=r" (ret) :
                             "r" (addr), "m" (*addr));
        return ret;
}

static inline unsigned in_be32(volatile unsigned long *addr)
{
        unsigned ret;

        __asm__ __volatile__("lwz%U1%X1 %0,%1;\n"
                             "twi 0,%0,0;\n"
                             "isync" : "=r" (ret) : "m" (*addr));
        return ret;
}

static inline void out_8(volatile unsigned char *addr, unsigned val)
{
        __asm__ __volatile__("stb%U0%X0 %1,%0; eieio" : "=m" (*addr) : "r" (val));
}

static inline void out_le16(volatile unsigned short *addr, int val)
{
        __asm__ __volatile__("sthbrx %1,0,%2; eieio" : "=m" (*addr) :
                              "r" (val), "r" (addr));
}

static inline void out_be16(volatile unsigned short *addr, int val)
{
        __asm__ __volatile__("sth%U0%X0 %1,%0; eieio" : "=m" (*addr) : "r" (val));
}


static inline void out_le32(volatile unsigned long *addr, int val)
{
        __asm__ __volatile__("stwbrx %1,0,%2; eieio" : "=m" (*addr) :
                             "r" (val), "r" (addr));
}

static inline void out_be32(volatile unsigned long *addr, int val)
{
        __asm__ __volatile__("stw%U0%X0 %1,%0; eieio" : "=m" (*addr) : "r" (val));
}


static inline int IN_LE16(unsigned short *addr)
{
        int ret;
        asm volatile ("lhbrx %0, %y1" : "=r"(ret) : "m"(*addr));
        return ret;
}

static inline int IN_LE32(unsigned long *addr)
{
        int ret;
        asm volatile ("lwbrx %0, %y1" : "=r"(ret) : "m"(*addr));
        return ret;
}


static inline void OUT_LE16(unsigned short *addr, int val)
{
        asm volatile("sthbrx %1,%y0": "=m"(*addr): "r"(val): "memory" );
}

static inline void OUT_LE32(unsigned long *addr, int val)
{
        asm volatile("stwbrx %1,%y0": "=m"(*addr): "r"(val): "memory" );
}

#if __cplusplus
    }
#endif

#endif

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PCI
 *
 * File        : AtAluPci.c
 *
 * Created Date: Sep 30, 2014
 *
 * Description : Wrap ALU's pci function
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAluPci.h"

#ifdef CUSTOMER_ALU
#include <stddef.h>
#include <string.h>
#include "attypes.h"
#include "include/pci_cfg_map.h"
#include "include/cplab_io.h"
#include "stdio.h"

/*--------------------------- Define -----------------------------------------*/
#define VENDOR_ID_ARRIVE    0x1846
#define DEVICE_ID_ARRIVE    0x0006
#define BUS_NUM_ARRIVE      3 //   BUS_NUM_HAMILTON1
#define BUS_DEV_ARRIVE      0
#define BAR_NUM_ARRIVE      0

#define cInvalidBaseAddress cBit31_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef unsigned long   ulong;
typedef unsigned int    uint;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_baseAddress = cInvalidBaseAddress;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static PCI_CFG_MAP* PciArchStructGet(void)
    {
    static PCI_CFG_MAP pci_Arch1_struct;
    memset(&(pci_Arch1_struct), 0, sizeof(PCI_CFG_MAP)); /* avoid rescan problem (by platform guys)*/

    pci_Arch1_struct.vend_id    = VENDOR_ID_ARRIVE;
    pci_Arch1_struct.dev_id     = DEVICE_ID_ARRIVE;
    pci_Arch1_struct.bus_number = BUS_NUM_ARRIVE;
    pci_Arch1_struct.bus_dev    = BUS_DEV_ARRIVE;
    pci_Arch1_struct.bar_num    = BAR_NUM_ARRIVE;
    pci_Arch1_struct.function   = (uint)-1;

    return &pci_Arch1_struct;
    }

static eBool PciMapped(void)
    {
    return (m_baseAddress == cInvalidBaseAddress) ? cAtFalse : cAtTrue;
    }

static void Write(uint32 realAddress, uint32 value)
    {
    out_be32((volatile unsigned long*)realAddress, (int)value);
    }

static uint32 Read(uint32 realAddress)
    {
    return in_be32((volatile unsigned long*)realAddress);
    }

static uint32 PciMap(void)
    {
    int ret;
    PCI_CFG_MAP *pci_Arch1_struct;
    ulong bsw_Arch1phyBAR;

    if (PciMapped())
        return m_baseAddress;

    pci_Arch1_struct = PciArchStructGet();
    ret = pci_cfg_map(pci_Arch1_struct);
    m_baseAddress = (uint32)pci_Arch1_struct->map;
    bsw_Arch1phyBAR = (ulong)pci_Arch1_struct->physical_addr;

    return m_baseAddress;
    }

static uint32 PciUnMap(void)
    {
    int ret = pci_cfg_unmap(PciArchStructGet());
    m_baseAddress = cInvalidBaseAddress;
    return ret;
    }

AtHal AtAluHalCreate(void)
    {
    return AtHalAluNew(PciMap(), Write, Read);
    }

void AtAluPciCleanup(void)
    {
    PciUnMap();
    }

#else

AtHal AtAluHalCreate(void)
    {
    return AtHalAluNew(0, NULL, NULL);
    }

void AtAluPciCleanup(void)
    {
    }

#endif

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : ZtePtn.c
 *
 * Created Date: Mar 28, 2014
 *
 * Description : ZTE application
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "atclib.h"
#include "AtHalZtePtn.h"
#include "zte.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void *mmap64 (void * addr, size_t len, int prot, int flags, int fildes, long long off);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MemoryMap(void)
    {
    void *address;
    int32 fd;
    const uint32 cInvalidAddress = 0xFFFFFFFF;

    fd = open("/dev/mem", O_RDWR);
    if (fd < 0)
        {
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidAddress;
        }

    address = mmap64(NULL, 0xFFFFFFF, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0xFE0000000ULL);
    if (address == ((void *)-1))
        {
        close(fd);
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidAddress;
        }

    close(fd);

    return (uint32)(AtSize)address;
    }

AtHal ZtePtnHalCreate(void)
    {
    return AtHalZtePtnNew(MemoryMap());
    }

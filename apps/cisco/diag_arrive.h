/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO
 * 
 * File        : diag_arrive.h
 * 
 * Created Date: Aug 3, 2015
 *
 * Description : TODO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef APPS_CISCO_DIAG_ARRIVE_H_
#define APPS_CISCO_DIAG_ARRIVE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define FAILED -1
#define PASSED 0
#define FOX_TREE_MAGIC_WORD 0

#define cPsnUdpIp 0
#define cPsnMpls 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef uint8 tSdhLineNum;
typedef uint16 tDe1LineNum;
typedef uint16 tDe3LineNum;

typedef struct tCep
    {
    uint8 psnType;
    uint8 sdhLineNum;
    }tCep;

typedef struct tLiuSatopDe1
    {
    uint8 psnType;
    }tLiuSatopDe1;

typedef struct tLiuSatopDe3
    {
    uint8 psnType;
    }tLiuSatopDe3;

/*--------------------------- Forward declarations ---------------------------*/
AtDevice CliDeviceSet(AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
uint8 im_get_current_slot_no(void);
eAtRet SetupCepUdp(uint32, uint32);
eAtRet SetupCepMpls(uint32, uint32);
int setup_cep(tCep cep);
int enable_cep_line(tSdhLineNum sdhLineId);
int disable_cep_line(tSdhLineNum sdhLineId);
int enable_ces_de1_line(tDe1LineNum de1ineId);
int disable_ces_de1_line(tDe1LineNum de1ineId);
int enable_ces_de3_line(tDe3LineNum de3ineId);
int disable_ces_de3_line(tDe3LineNum de3ineId);
int setup_liu_de1(tLiuSatopDe1 de1);
int setup_liu_de3(tLiuSatopDe3 de3);
int arrive_fpag_init(void);
eAtRet EnableCepPwChannel(uint32, uint32);
int arrive_fpag_exit(void);
int arrive_fpag_invoke_cli(void);

#ifdef __cplusplus
}
#endif
#endif /* APPS_CISCO_DIAG_ARRIVE_H_ */


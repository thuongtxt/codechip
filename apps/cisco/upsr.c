/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : upsr.c
 *
 * Created Date: Nov 2, 2015
 *
 * Description : To show how to use UPSR APIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <stdlib.h>

#include "AtCiscoUpsr.h"

/*--------------------------- Define -----------------------------------------*/
#define AT_UINT32_MAX     (4294967295U)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void *mmap64 (void * addr, size_t len, int prot, int flags, int fildes, long long off);

/*--------------------------- Implementation ---------------------------------*/
static at_uint32 MemoryMap(const char *device, long long phyAddress, at_uint32 memorySize)
    {
    int fd;
    void *baseAddress;

    fd = open(device, O_RDWR);
    if (fd < 0)
        {
        printf("ERROR: Cannot open device %s, error = %s\r\n", device, strerror(errno));
        return AT_UINT32_MAX;
        }

    baseAddress = mmap64(NULL, memorySize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, phyAddress);
    if (baseAddress == ((void *)-1))
        {
        close(fd);
        printf("ERROR: Memory mapping fail, error = %s\r\n", strerror(errno));
        return AT_UINT32_MAX;
        }

    close(fd);

    return (at_uint32)baseAddress;
    }

static void MemoryUnMap(const char* device, at_uint32 baseAddress, at_uint32 memorySize)
    {
    int ret;
    int fd;

    fd = open(device, O_RDWR);
    if (fd == -1)
        {
        printf("ERROR: Memory un-mapping fail, error = %s\r\n", strerror(errno));
        return;
        }

    ret = munmap((void *)((size_t)baseAddress), memorySize);
    if (ret < 0)
        printf("ERROR: Memory un-mapping fail, error = %s\r\n", strerror(errno));

    close(fd);
    }

static void VtUpsrExample(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 vtRowId)
    {
    /* Try VT mask */
    AtCiscoUpsrVtMaskWrite(self, table, vtRowId, 0);
    assert(AtCiscoUpsrVtMaskRead(self, table, vtRowId) == 0);
    AtCiscoUpsrVtMaskWrite(self, table, vtRowId, 1);
    assert(AtCiscoUpsrVtMaskRead(self, table, vtRowId) == 1);

    /* VT status */
    printf("* VT row %d status: \r\n", AtCiscoUpsrVtStatusRead(self, table, vtRowId));
    }

static void StsUpsrExample(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 stsRowId)
    {
    /* Try STS mask */
    AtCiscoUpsrStsMaskWrite(self, table, stsRowId, 0);
    assert(AtCiscoUpsrStsMaskRead(self, table, stsRowId) == 0);
    AtCiscoUpsrStsMaskWrite(self, table, stsRowId, 1);
    assert(AtCiscoUpsrStsMaskRead(self, table, stsRowId) == 1);

    /* STS status */
    printf("* STS row %d status: \r\n", AtCiscoUpsrStsStatusRead(self, table, stsRowId));
    }

static void UpsrExample(AtCiscoUpsr self, eAtCiscoUpsrAlarm table)
    {
    /* Try accessing MARS mask */
    AtCiscoUpsrMASRMaskWrite(self, 3);

    /* Access MARS status */
    printf("  * MASR: 0x%08x\r\n", AtCiscoUpsrMASRStatusRead(self, table));

    /* STS SASR register */
    printf("  * SASRSts: %d\r\n", AtCiscoUpsrSASRStsRead(self, table, 0));

    /* Vt SASR registers */
    printf("  * SASRVt[00]: %d\r\n", AtCiscoUpsrSASRVtRead(self, table, 0));
    printf("  * SASRVt[01]: %d\r\n", AtCiscoUpsrSASRVtRead(self, table, 1));
    printf("  * SASRVt[..]: ...\r\n");

    /* See if STS/VT can work */
    StsUpsrExample(self, table, 2); /* Use STS ID 2 */
    VtUpsrExample(self, table, 3);  /* Use VT ID 3 */
    }

static void GroupExample(AtCiscoUpsr self)
    {
    at_uint32 apsGroupId = 2;
    at_uint32 hsGroupId  = 3;

    /* See if APS group can be enabled/disabled normally */
    AtCiscoApsGroupEnable(self, apsGroupId, 0);
    assert(AtCiscoApsGroupIsEnabled(self, apsGroupId) == 0);
    AtCiscoApsGroupEnable(self, apsGroupId, 1);
    assert(AtCiscoApsGroupIsEnabled(self, apsGroupId) == 1);

    /* See if HS group can switch label normally */
    AtCiscoHsGroupTxLabelSetSelect(self, hsGroupId, cAtCiscoPwGroupLabelSetPrimary);
    assert(AtCiscoHsGroupTxSelectedLabelGet(self, hsGroupId) == cAtCiscoPwGroupLabelSetPrimary);
    AtCiscoHsGroupRxLabelSetSelect(self, hsGroupId, cAtCiscoPwGroupLabelSetBackup);
    assert(AtCiscoHsGroupRxSelectedLabelGet(self, hsGroupId) == cAtCiscoPwGroupLabelSetBackup);
    }

static  at_uint32 CanReadRegisterExample(at_uint32 baseAddress, at_uint32 localAddress, void *userData)
    {
    return 1;
    }

static at_uint32 CanWriteRegisterExample(at_uint32 baseAddress, at_uint32 localAddress, at_uint32 value, void *userData)
    {
    return 1;
    }

int main(int argc, char* argv[])
    {
    long long metaBaseAddress;
    long long upsrBaseAddress;
    unsigned long long int physicalAddress;

    at_uint32 metaAddress, upsrAddress;

    const char *device;
    AtCiscoUpsr upsrObject;
    tAtCiscoUpsrDelegate delegate = {.CanReadRegister = CanReadRegisterExample, .CanWriteRegister = CanWriteRegisterExample};

    /* Need physical base address */
    if (argc < 3)
        {
        printf("Usage: %s <device> <physicalAddress>\r\n", argv[0]);
        return -1;
        }

    /* Memory mapping */
    device = argv[1];
    physicalAddress = strtoull(argv[2], NULL, 16);
    metaBaseAddress = physicalAddress + (AtCiscoUpsrMetaStartOffset() << 2);
    metaAddress  = MemoryMap(device, metaBaseAddress, AtCiscoUpsrMetaSizeInBytes());
    upsrObject = AtCiscoUpsrCreate(metaAddress, &delegate, NULL);

    MemoryUnMap(device, metaAddress, AtCiscoUpsrMetaSizeInBytes());

    if (upsrObject == NULL)
        {
        printf("Error: Can not create UPSR controller\r\n");
        return -1;
        }

    upsrBaseAddress = physicalAddress + (AtCiscoUpsrDwordStartOffset(upsrObject) << 2);
    upsrAddress = MemoryMap(device, upsrBaseAddress, AtCiscoUpsrMemorySizeInBytes(upsrObject));

    if (upsrAddress == AT_UINT32_MAX)
        {
        printf("ERROR: Memory mapping device \"%s\" with base address 0x%llx: FAIL\r\n", device, upsrBaseAddress);
        return -1;
        }

    AtCiscoUpsrBaseAddressSet(upsrObject, upsrAddress);

    /* Let's see if UPSR can be controlled */
    printf("\r\nUPSR Info:\r\n");
    printf("* Version           : %s\r\n", AtCiscoUpsrVersion(upsrObject));
    printf("* Base address      : 0x%08X\r\n", AtCiscoUpsrBaseAddress(upsrObject));
    printf("* Product code      : 0x%08X\r\n", AtCiscoUpsrProductCodeGet(upsrObject));
    printf("* FPGA version      : 0x%08X\r\n", AtCiscoUpsrFpgaVersion(upsrObject));
    printf("* FPGA built number : 0x%08X\r\n", AtCiscoUpsrFpgaBuiltNumber(upsrObject));

    printf("\r\nUPSR capacity:\r\n");
    printf("* Maximum APS groups: %u\r\n", AtCiscoUpsrMaxApsGroupsGet(upsrObject));
    printf("* Maximum HW groups : %u\r\n", AtCiscoUpsrMaxHsGroupsGet(upsrObject));
    printf("* Maximum STS       : %u\r\n", AtCiscoUpsrMaxStsGet(upsrObject));
    printf("* Maximum VT in STS : %u\r\n", AtCiscoUpsrMaxVtInOneStsGet(upsrObject));

    printf("\r\nUPSR tables:\r\n");
    printf("* SD table handling :\r\n");
    UpsrExample(upsrObject, cAtCiscoUpsrAlarmSd);
    printf("* SF table handling :\r\n");
    UpsrExample(upsrObject, cAtCiscoUpsrAlarmSf);

    /* Let's see if groups can be controlled */
    GroupExample(upsrObject);

    AtCiscoUpsrDelete(upsrObject);

    MemoryUnMap(device, upsrAddress, AtCiscoUpsrMemorySizeInBytes(upsrObject));
    return 0;
    }

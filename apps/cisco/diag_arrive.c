/*------------------------------------------------------------------
 * diag_arrive.c :  C file for Arrive CEM/CES FPGA support.
 *
 * May 2015, Yasdixit
 *
 * Copyright (c) 2015-2016 by Cisco Systems, Inc.
 *
 *------------------------------------------------------------------
 */

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include "AtCliModule.h"
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"
#include "AtHalCisco.h"
	
#include "diag_arrive.h"
#include "diag_im_tests.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumSlots 6

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef unsigned short      uint16_t;	/* 16-bit */
typedef unsigned char       uint8_t;	/* 8-bit */
typedef unsigned int        uint32_t;	/* 32-bit */
typedef short               int16_t;
typedef int                 int32_t;

typedef struct tCiscoIm
    {
    AtDevice device;

    /* Further information may be added in the future if necessary */
    }tCiscoIm;

typedef tCiscoIm * CiscoIm;

/*--------------------------- External variables -------------------------------*/
#if 0
extern void Tfi5Prbs(AtSdhLine ,  eAtPrbsMode );
#endif

extern volatile uint8_t *adrspc_virt_im_0_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_1_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_2_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_3_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_4_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_5_bar_2_3; /* 512 MB */

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tCiscoIm m_ims[cMaxNumSlots];

/* For command processing */
static AtTextUI m_textUI = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static CiscoIm ImAtSlot(uint8 slotIndex)
    {
    return &(m_ims[slotIndex]);
    }

static AtDevice DeviceAtSlot(uint8 slotId)
    {
    return ImAtSlot(slotId)->device;
    }

static uint32 SlotBaseAddress(uint8 slotId)
    {
    switch (slotId)
        {
        case 0: return (uint32)adrspc_virt_im_0_bar_2_3;
        case 1: return (uint32)adrspc_virt_im_1_bar_2_3;
        case 2: return (uint32)adrspc_virt_im_2_bar_2_3;
        case 3: return (uint32)adrspc_virt_im_3_bar_2_3;
        case 4: return (uint32)adrspc_virt_im_4_bar_2_3;
        case 5: return (uint32)adrspc_virt_im_5_bar_2_3;

        /* Impossible, but... */
        default: return 0;
        }
    }

static void PCIeWriteFunc(uint32 realAddress, uint32 value)
    {
    volatile unsigned int *address;
    address = (volatile unsigned int *) (realAddress);
    *address = value;
    (void) (*address);
    }

static uint32 PCIeReadFunc(uint32 realAddress)
    {
    volatile unsigned int *address;
    address = (volatile unsigned int *) (realAddress);
    return (*address);
    }

static AtHal CreateHal(uint8 slotId)
    {
    return AtHalCiscoNew(SlotBaseAddress(slotId), PCIeWriteFunc, PCIeReadFunc);
    }

static eAtRet DriverInit(void)
    {
    AtDriverCreate(cMaxNumSlots, AtOsalLinux());

    if (AtDriverSharedDriverGet() == NULL)
        AtPrintc(cSevCritical, "ERROR: Cannot create driver\r\n");

    return cAtOk;
    }

static eAtRet SlotSetup(uint8 slotId)
    {
    CiscoIm im = ImAtSlot(slotId);
    AtHal hal = CreateHal(slotId);
    uint32 productCode = AtProductCodeGetByHal(hal);
    AtDevice newDevice;

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        {
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);
        return cAtErrorInvlParm;
        }

    newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
    AtDeviceIpCoreHalSet(newDevice, 0, hal);
    im->device = newDevice;

    return cAtOk;
    }

static eAtRet DeviceCleanUp(AtDevice device)
    {
    AtHal hal;

    if (device == NULL)
        return cAtOk;

    hal = AtDeviceIpCoreHalGet(device, 0);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);
    AtHalDelete(hal);

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    uint8 numDevices, i;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        DeviceCleanUp(device);
        }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);

    return cAtOk;
    }

static AtTextUI TinyTextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
    }

static int EnterSlot(uint8 slotId)
    {
    eAtRet ret;
    AtDevice device = DeviceAtSlot(slotId);

    /* Device has been added, just select it */
    if (device)
        {
        CliDeviceSet(device);
        return PASSED;
        }

    /* Create a new one */
    ret = SlotSetup(slotId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "Cannot initialize device, ret = %s\r\n",
                 AtRet2String(ret));
        return FAILED;
        }

    /* Select it */
    CliDeviceSet(DeviceAtSlot(slotId));

    return PASSED;
    }

int arrive_fpag_init(void)
    {
    eAtRet ret;

    /* Initialize driver */
    ret = DriverInit();
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "Cannot initialize driver, ret = %s\r\n",
                 AtRet2String(ret));
        return FAILED;
        }

    return PASSED;
    }


int arrive_fpag_exit(void)
    {
    DriverCleanup(AtDriverSharedDriverGet());
    AtTextUIDelete(TinyTextUI());
    return PASSED;
    }

int arrive_fpag_invoke_cli(void)
    {
    uint8 slotNumber = (uint8)im_get_current_slot_no();

    if (slotNumber >= cMaxNumSlots)
        {
        AtPrintc(cSevCritical, "ERROR: Slot number must be from 0 to %u\r\n", cMaxNumSlots - 1);
        return FAILED;
        }

    EnterSlot(slotNumber);
    AtCliStartWithTextUI(TinyTextUI());

    return PASSED;
    }

#if 0
int testFunc(int show_menu)
    {
#if 0
    AtHal hal = AtHalCiscoNew(0, PCIeWriteFunc, PCIeReadFunc);

    uint32 value;
    int ret_val = PASSED;

    if (show_menu == FOX_TREE_MAGIC_WORD)
        {
        return (ret_val);
        }

    printf("\n0xF00001 = 0x%08x\r\n", AtHalRead(hal, 0xF00001));
    printf("0xF00040 = 0x%08x\r\n", AtHalRead(hal, 0xF00040));

    AtHalWrite(hal, 0xF00040, 0xAAAAAAAA);
    value = AtHalRead(hal, 0xF00040);
    if (value != 0xAAAAAAAA)
    printf("Write 0xAAAAAAAA but got 0x%08x\r\n", value);

    AtHalWrite(hal, 0xF00040, 0x55555555);
    value = AtHalRead(hal, 0xF00040);
    if (value != 0x55555555)
    printf("Write 0x55555555 but got 0x%08x\r\n", value);

    AtHalDelete(hal);

    printf("\nval at 0xF00001 : 0x%x\n", PCIeReadFunc(0xF00001));
#endif
    int ret_val = PASSED;
    if (show_menu == FOX_TREE_MAGIC_WORD)
        {
        return (ret_val);
        }

    AtHal hal;
    uint32 time_i;
    eBool success = cAtTrue;
    uint32 runningTimes = 5;
    printf("\n");
    runningTimes = getdec_answer("Enter runningTimes  (0 - 100000): ",
                                 0,
                                 0,
                                 100000);
    AtOsalSharedSet(AtOsalLinux()); /* Call this if we have not set it before */
    hal = AtHalCiscoNew(0, PCIeWriteFunc, PCIeReadFunc);
    for (time_i = 0; time_i < runningTimes; time_i++)
        {
        success = AtHalDiagMemTest(hal);
        if (!success)
            break;
        }

    AtHalDelete(hal);

    printf("HAL test: %s\r\n", success ? "PASS" : "FAIL");
    return success ? PASSED : FAILED;
    }
#endif

#if 0
static int setup_cep_udp(tSdhLineNum sdhLineId) {

    eAtRet ret;

    ret = SetupCepUdp( sdhLineId, 1);

    if( ret != cAtOk) {
        printf("\n Cep udp setup failed : error code : %d \n", (uint32)ret);
        return FAILED;
    }

    return PASSED;
}

static int setup_cep_mpls(tSdhLineNum sdhLineId) {

    eAtRet ret;

    ret = SetupCepMpls( sdhLineId, 1);

    if( ret != cAtOk) {
        printf("\n Cep MPLS setup failed : error code : %d \n", (uint32)ret);
        return FAILED;
    }

    return PASSED;
}
#endif

int setup_cep(tCep cep)
    {

    eAtRet ret;

    if (cep.psnType == cPsnUdpIp)
        {
        ret = SetupCepUdp(cep.sdhLineNum, 1);
        }
    else if (cep.psnType == cPsnMpls)
        {
        ret = SetupCepMpls(cep.sdhLineNum, 1);
        }
    else
        {
        printf("\n Cep setup failed : Invalid PSN type : %d \n",
               (uint32) cep.psnType);
        }

    if (ret != cAtOk)
        {
        printf("\n Cep udp setup failed : error code : %d \n", (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int enable_cep_line(tSdhLineNum sdhLineId)
    {

    eAtRet ret;

    ret = EnableCepPwChannel(sdhLineId, 1);

    if (ret != cAtOk)
        {
        printf("\n Enable Cep port FAILED : error code : %d \n", (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int disable_cep_line(tSdhLineNum sdhLineId)
    {

    eAtRet ret;

    ret = DisableCepPwChannel(sdhLineId, 1);

    if (ret != cAtOk)
        {
        printf("\n Disable Cep port FAILED : error code : %d \n", (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int enable_ces_de1_line(tDe1LineNum de1ineId)
    {

    eAtRet ret;

    ret = EnableLiuSAToPMplsPwChannel(de1ineId);

    if (ret != cAtOk)
        {
        printf("\n Enable CES DS1 port FAILED : error code : %d \n",
               (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int disable_ces_de1_line(tDe1LineNum de1ineId)
    {

    eAtRet ret;

    ret = DisableLiuSAToPMplsPwChannel(de1ineId);

    if (ret != cAtOk)
        {
        printf("\n Disable CES DS1 port FAILED : error code : %d \n",
               (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int enable_ces_de3_line(tDe3LineNum de3ineId)
    {

    eAtRet ret;

    ret = EnableLiuSAToPMplsPwChannel(de3ineId);

    if (ret != cAtOk)
        {
        printf("\n Enable CES DS3 port FAILED : error code : %d \n",
               (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int disable_ces_de3_line(tDe3LineNum de3ineId)
    {

    eAtRet ret;

    ret = DisableLiuSAToPMplsPwChannel(de3ineId);

    if (ret != cAtOk)
        {
        printf("\n Disable CES DS3 port FAILED : error code : %d \n",
               (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int setup_liu_de1(tLiuSatopDe1 de1)
    {

    eAtRet ret;

    if (de1.psnType == cPsnUdpIp)
        {
        ret = SetupLiuSAToPMplsDe1(de1);
        }
    else if (de1.psnType == cPsnMpls)
        {
        ret = SetupLiuSAToPMplsDe1(de1);
        }
    else
        {
        printf("\n CES DE1  setup failed : Invalid PSN type : %d \n",
               (uint32) de1.psnType);
        }

    if (ret != cAtOk)
        {
        printf("\n CES DE1  setup failed : error code : %d \n", (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int setup_liu_de3(tLiuSatopDe3 de3)
    {

    eAtRet ret;

    if (de3.psnType == cPsnUdpIp)
        {
        ret = SetupLiuSAToPMplsDe3(de3);
        }
    else if (de3.psnType == cPsnMpls)
        {
        ret = SetupLiuSAToPMplsDe3(de3);
        }
    else
        {
        printf("\n CES DE3  setup failed : Invalid PSN type : %d \n",
               (uint32) de3.psnType);
        }

    if (ret != cAtOk)
        {
        printf("\n CES DE3  setup failed : error code : %d \n", (uint32) ret);
        return FAILED;
        }

    return PASSED;
    }

int diag_cem_fpga_device_id_test(int show_menu)
    {
    if (show_menu == FOX_TREE_MAGIC_WORD)
        {
        return (PASSED);
        }

    return (PASSED);
    }

int diag_cem_fpga_prbs_test(int show_menu)
    {
    if (show_menu == FOX_TREE_MAGIC_WORD)
        {
        return (PASSED);
        }
    uint32 sdhLineId;
    sdhLineId = getdec_answer("Enter SDH Lin  (0 - 7): ", 0, 0, 7);

#if 0
    AtDevice device = DeviceGet();
    AtModulePw pwModule = PwModule(device);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);

    Tfi5Prbs(1, cAtPrbsModePrbs11);
#endif
    return (PASSED);
    }

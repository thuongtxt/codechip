/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtTestbench.h
 * 
 * Created Date: Jul 1, 2015
 *
 * Description : Work with testbench
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTESTBENCH_H_
#define _ATTESTBENCH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
int AtTestbenchMain(unsigned int productCode);
int AtTestbenchExit(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATTESTBENCH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : test.c
 *
 * Created Date: Jul 1, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHal.h"
#include "AtTestbench.h"
#include "AtOsalLinuxDebug.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHal m_halSim = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void HalSetup(void)
    {
    AtOsalLeakTracker tracker = AtOsalLinuxDebugLeakTracker(AtOsalLinuxDebug());
    eBool trackEnabled = AtOsalLeakTrackerIsEnabled(tracker);
    AtOsalLeakTrackerEnable(tracker, cAtFalse);

    m_halSim = AtHalSimGlobalHoldNew(cAtHalSimDefaultMemorySizeInMbyte);
    AtHalWrite(m_halSim, 0, 0x60031021);

    AtOsalLeakTrackerEnable(tracker, trackEnabled);
    }

static AtHal Hal(void)
    {
    return m_halSim;
    }

static uint32 Read(uint32 address)
    {
    return AtHalRead(Hal(), address);
    }

static void Write(uint32 address, uint32 data)
    {
    return AtHalWrite(Hal(), address, data);
    }

uint32 AtTbRead(uint32 address)
    {
    return AtHalRead(Hal(), address);
    }

void AtTbWrite(uint32 address, uint32 data)
    {
    return AtHalWrite(Hal(), address, data);
    }

int main(int argc, char* argv[])
    {
    int ret;

    AtOsalSharedSet(AtOsalLinuxDebug());
    HalSetup();

    ret = AtTestbenchMain(0x60210011);
    AtTestbenchDelete();
    AtHalDelete(m_halSim);
    m_halSim = NULL;

    return ret;
    }

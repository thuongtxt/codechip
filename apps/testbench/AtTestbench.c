/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : EP APP
 *
 * File        : main.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <unistd.h>
#include <stdlib.h>

#include "extAtSdkInt.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "AtOsalLinuxDebug.h"
#include "AtCliModule.h"
#include "AtHalTestbench.h"
#include "AtCliService.h"

#include "apputil.h"
#include "AtTestbench.h"

/* Privates */
#include "../../driver/src/generic/man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return CliDevice();
    }

static AtDriver Driver(void)
    {
    return AtDriverSharedDriverGet();
    }

static void DriverCreate(void)
    {
    static const uint8 cMaxNumDevices = 10;
    AtDriverCreate(cMaxNumDevices, AtOsalLinuxTestbench());
    }

static uint32 TestbenchRead(uint32 realAddress)
    {
    extern int AtTbRead (unsigned int _a1, unsigned int *_a2);
    unsigned int ret = 0;
    AtTbRead (realAddress,  &ret);
    return ret;
    }

static void TestbenchWrite(uint32 realAddress, uint32 value)
    {
    extern int AtTbWrite (unsigned int _a1, unsigned int _a2);
    AtTbWrite(realAddress, value);
    }

static AtHal CreateHal(uint32 productCode)
    {
    static const uint32 cBaseAddress = 0;
    return AtHalTestbenchNew(cBaseAddress, TestbenchWrite, TestbenchRead);
    }

static eBool DriverInit(uint32 productCode)
    {
    AtHal hal = CreateHal(productCode);
    AtDevice device;

    /* Create driver */
    if (Driver() == NULL)
        DriverCreate();

    if (Driver() == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create driver\r\n");
        return cAtFalse;
        }

    /* Create device */
    device = AtDriverDeviceCreate(Driver(), productCode);
    if (device == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create device with product code 0x%08x\r\n", productCode);
        return cAtFalse;
        }

    AtIpCoreHalSet(AtDeviceIpCoreGet(Device(), 0), hal);
    AtDeviceTestbenchEnable(device, cAtTrue);

    return cAtTrue;
    }

static const char *HistoryFileName(void)
    {
    return ".atsdk_history";
    }

static void ShowHistory(void)
    {
    char cmd[32];

    AtSprintf(cmd, "cat %s", HistoryFileName());
    system(cmd);

    AtSprintf(cmd, "rm -f %s", HistoryFileName());
    system(cmd);
    }

static void LeakReport(void)
    {
    AtOsalLeakTracker tracker = AtOsalLinuxDebugLeakTracker(AtOsalLinuxDebug());
    if (!AtOsalLeakTrackerIsLeak(tracker))
        return;

    AtOsalLeakTrackerReport(tracker);

    /* Also need to report CLI sequence */
    AtPrintc(cSevCritical, "Please report this sequence to software team");
    AtPrintc(cSevNormal, "\r\n");
    ShowHistory();
    }

static void OsalCleanup(void)
    {
    LeakReport();
    AtOsalLinuxDebugCleanUp(AtOsalLinuxDebug());
    }

static void SaveDeviceHals(AtDevice device, AtList hals)
    {
    uint8 coreId;

    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    AtList hals;
    uint8 numDevices, i;
    AtLogger logger;

    if (driver == NULL)
        return cAtOk;

    /* All HALs that are installed so far */
    hals = AtListCreate(0);

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        SaveDeviceHals(device, hals);
        AtDriverDeviceDelete(driver, device);
        }

    /* Need to cache logger for deleting later */
    logger = AtDriverLoggerGet(driver);

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);
    while (AtListLengthGet(hals) > 0)
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
    AtObjectDelete((AtObject)hals);

    AtObjectDelete((AtObject)logger);

    return cAtOk;
    }

static eBool CliCleanup(void)
    {
    AtCliServerStop();
    return cAtTrue;
    }

static eBool AppCleanup(void)
    {
    eBool ret = cAtTrue;

    DriverCleanup(Driver());
    CliCleanup();
    OsalCleanup();

    return ret;
    }

static void CliServerStart(uint16 port)
    {
    AtCliServerStart(NULL, port);
    AtCliCpuRestTimeMsSet(0);
    }

int AtTestbenchMain(unsigned int productCode)
    {
    if (!DriverInit(productCode))
        {
        AtPrintc(cSevCritical, "ERROR: Initialize driver fail\r\n");
        AppCleanup();
        return 1;
        }

    CliServerStart(5678);

    AtPrintc(cSevInfo, "Start SDK background successfully. Connect to port 5678\r\n");

    return 0;
    }

int AtTestbenchExit(void)
    {
    AppCleanup();
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Testbench
 *
 * File        : AtTestbenchStuff.c
 *
 * Created Date: Sep 20, 2016
 *
 * Description : To fix linking issues
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void AppInterruptTaskEnable(eBool enable)
    {
    }

int arrive_e1_fpga_write(unsigned long address, unsigned long value)
    {
    return 0;
    }

int arrive_e1_fpga_read(unsigned long address, unsigned long *value)
    {
    return 0;
    }

void AppPollingTaskPeriodInMsSet(uint32 periodMs)
    {
    }

void CliClientExit(void)
    {
    }

void  AppPollingTaskEnable(eBool enable)
    {
    }

eBool AppSignalSetup()
    {
    return cAtFalse;
    }

void AppInterruptMessageSend(void)
    {
    }

void AppEyeScanTaskEnable(eBool enable)
    {
    }

void AppFmPmTaskEnable(eBool enable)
    {
    }

void AppInterruptFloodingWatch(eBool watched);
void AppInterruptFloodingWatch(eBool watched){AtUnused(watched);}

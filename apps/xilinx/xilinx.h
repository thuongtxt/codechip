/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : xilinx.h
 * 
 * Created Date: May 27, 2015
 *
 * Description : To work with XILINX platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _XILINX_H_
#define _XILINX_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 XilinxPlatformFpgaBaseAddress(uint32 phyBaseAddress);
eAtRet XilinxPlatformFinalize(void);

#ifdef __cplusplus
}
#endif
#endif /* _XILINX_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : xilinx.c
 *
 * Created Date: May 27, 2015
 *
 * Description : To work with XILINX board
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "xilinx.h"
#include "AtHalMemoryMapper.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHalMemoryMapper m_fpgaMemoryMapper     = NULL;
static uint32 m_phyBaseAddress = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    if (m_phyBaseAddress == 0)
        return 0xc0000000;
    return m_phyBaseAddress;
    }

static uint32 MemorySize(void)
    {
    return 0x20000000UL;
    }

static AtHalMemoryMapper FpgaMemoryMapper(void)
    {
    if (m_fpgaMemoryMapper == NULL)
        {
        m_fpgaMemoryMapper = AtHalMemoryMapperLinuxNew(BaseAddress(), MemorySize());
        AtHalMemoryMapperMap(m_fpgaMemoryMapper);
        }

    return m_fpgaMemoryMapper;
    }

eAtRet XilinxPlatformFinalize(void)
    {
    AtHalMemoryMapperDelete(m_fpgaMemoryMapper);
    return cAtOk;
    }

uint32 XilinxPlatformFpgaBaseAddress(uint32 phyBaseAddress)
    {
    m_phyBaseAddress = phyBaseAddress;
    return AtHalMemoryMapperBaseAddress(FpgaMemoryMapper());
    }

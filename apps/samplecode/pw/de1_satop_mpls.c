/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : satop.c
 *
 * Created Date: Sep 5, 2014
 *
 * Description : This source file is to show how a full E1 is emulated on PSN by
 *               using SAToP Pseudowire.
 *
 *               In this example, 1 E1 unframe is mapped to SDH VC-12 and SAToP PW
 *               is used to emulate it. Just for simple demo purpose, the first
 *               VC-12 is used.
 *
 *               All of these PWs will use MPLS PSN and Ethernet Port 1 is used
 *               as Ethernet physical.
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void SAToPExample(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtSdhLine Line1(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
    }

/*
 * Mapping detail for line 1
 * - AUG-1#1: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 <--> E1 unframe
 */
static void SetupSdhMappingToMapE1(void)
    {
    AtSdhLine line1 = Line1();
    uint8 aug1Id = 0;
    AtSdhChannel aug1, vc4, tug3, tug2, vc12;
    AtPdhChannel de1;

    /* Setup mapping for AUG-1#1 to map AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line1, aug1Id, 0, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);

    /* VC-12 maps E1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);

    /* And configure E1 un-frame. Application can configure any frame mode since
     * the whole E1 will be emulated on SAToP. */
    de1 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet(de1, cAtPdhE1UnFrm);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

void SAToPExample(void)
    {
    AtSdhChannel vc12;
    AtChannel de1;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtSdhLine line1 = Line1();

    /* Setup mapping to have E1 mapped to VC-12 */
    SetupSdhMappingToMapE1();

    /* Create a SAToP to emulate this E1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, 0, 0, 0, 0);
    de1  = AtSdhChannelMapChannelGet(vc12);
    pw   = (AtPw)AtModulePwSAToPCreate(pwModule, 0);
    AtPwCircuitBind(pw, de1);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*

CLIs to configure this flow:

# Set mapping to DE1
sdh map aug1.1.1 vc4
sdh map vc4.1.1 3xtug3s
sdh map tug3.1.1.1 7xtug2s
sdh map tug2.1.1.1.1 tu12
sdh map vc1x.1.1.1.1.1 de1

# E1 un-frame
pdh de1 framing 1.1.1.1.1 e1_unframed

# Emulate this E1 unframe by SAToP. MPLS is used as PSN.
pw create satop 1
pw circuit bind 1 de1.1.1.1.1.1
pw ethport 1 1
pw psn 1 mpls
pw mpls innerlabel 1 1.0.255
pw mpls outerlabel add 1 1.0.255
pw mpls expectedlabel 1 1
pw ethheader 1 CA.FE.CA.FE.CA.FE 0.0.1 none
pw enable 1

*/

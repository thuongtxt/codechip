/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : hdlc_pw.c
 *
 * Created Date: Mar 29, 2016
 *
 * Description : Sample code of HDLC pw
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"

#include "AtModulePdh.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"

#include "AtModuleEncap.h"
#include "AtEncapChannel.h"
#include "AtModuleFr.h"
#include "AtHdlcChannel.h"
#include "AtFrLink.h"

#include "AtPw.h"
#include "AtPwPsn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Vc3_Ds1_Hdlc_Over_Mpls(void);
void Vc3_Ds1_Ppp_Over_Mpls(void);
void Vc3_Ds1_Fr_Over_Mpls_Port_Mode(void);
void Vc3_Ds1_Fr_Over_Mpls_1To1_Mode(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    return AtDriverDeviceGet(driver, 0);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

void Vc3_Ds1_Hdlc_Over_Mpls(void)
    {
    AtModulePw pwModule;
    AtPw pw;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwHdlcCreate(pwModule, encapChannel);
    SetupPw(pw);

    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void Vc3_Ds1_Ppp_Over_Mpls(void)
    {
    AtModulePw pwModule;
    AtPw pw;
    AtPppLink pppLink;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 1); /* Use the second one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate HDLC-PPP channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* PPP link from HDLC channel */
    pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwPppCreate(pwModule, pppLink);
    SetupPw(pw);

    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void Vc3_Ds1_Fr_Over_Mpls_Port_Mode(void)
    {
    AtModulePw pwModule;
    AtPw pw;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 2); /* Use the third one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate HDLC-PPP channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* FR link from HDLC channel */
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwHdlcCreate(pwModule, encapChannel);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void Vc3_Ds1_Fr_Over_Mpls_1To1_Mode(void)
    {
    AtModulePw pwModule;
    AtPw pw;
    AtFrLink frLink;
    AtFrVirtualCircuit frVc;
    uint32 dlci;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 2); /* Use the third one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate HDLC-PPP channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* FR link from HDLC channel */
    frLink = (AtFrLink)AtHdlcChannelHdlcLinkGet(encapChannel);

    dlci = 1000;
    frVc = AtFrLinkVirtualCircuitCreate(frLink, dlci);
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwFrCreate(pwModule, frVc);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

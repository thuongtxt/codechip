Although there are five PW types mentioned in the SDK programming guide, but for 
TDM PWs, only SAToP/CESoP/CEP are used. HDLC/PPP PW has not been supported yet 
and ATM PW is only ATM products. The purposes of these PWs are as following:
- CEP  : to emulate SDH VCs (VC-4/VC3/VC-11/VC-12) on Packet Switch Network.
- SAToP: to emulate the whole E1 on Packet Switch Network.
- CESoP: to emulate NxDS0 groups in E1 frame on Packet Switch Network.

And there are 3 sample source files in this package to demo API usage for each 
PW types. CLIs used to configure are also added at the end of each sample source 
file.

- cep.c: This source file is to show how a CEP PW is setup to emulate SDH VC 
  (VC-4/VC-3/VC-1x) to Packet Switch Network. 
  For the sake of simplicity, this source file makes a default mapping on 
  SDH layer to carry all kinds of VC mentioned above. And Line#1 is used.
 
  There are 16 AUG-1s in STM-16 and 3 AUG-1s are used in this example. 
  There mappings are as following:
  - AUG-1#1: VC-4
  - AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
  - AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
 
  And this source file will emulate 3 VCs onto 3 CEP PW, they are:
  - VC-4: which is contained in AUG-1#1
  - The first VC-3  in AUG-1#2
  - The first VC-12 in AUG-1#3
 
  All of these PWs will use MPLS PSN and Ethernet Port 1 is used as Ethernet physical.

- satop.c: This source file is to show how a full E1 is emulated on PSN by using 
  SAToP Pseudowire.
    
  In this example, 1 E1 unframe is mapped to SDH VC-12 and SAToP PW
  is used to emulate it. Just for simple demo purpose, the first
  VC-12 is used.
 
  All of these PWs will use MPLS PSN and Ethernet Port 1 is used
  as Ethernet physical.

- cesop.c: This source file is to show how NxDS0 groups are emulated on PSN by
  using CESoP Pseudowire.
 
  To have NxDs0 groups, the E1 must be configured to E1 frame.
  having a framed E1, application can create NxDS0 groups to be
  emulated and bind them to CESoP PWs.
 
  To make the sample code be simple to understand, the first VC-12
  of SDH Line#1 is configured to map E1 basic frame. Then two
  NxDS0 groups are created and bound to two CESoP PWs.
 
  All of these PWs will use MEF-8 PSN and Ethernet Port 1 is used
  as Ethernet physical.

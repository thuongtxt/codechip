/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : satop.c
 *
 * Created Date: Sep 5, 2014
 *
 * Description : This source file is to show how a full DS3/E3 is emulated on PSN by
 *               using SAToP Pseudowire.
 *
 *               In this example, 1 DS3 unframe is mapped to SDH VC-3 and SAToP PW
 *               is used to emulate it. Just for simple demo purpose, the first
 *               VC-3 is used.
 *
 *               UDP/IPv4 is used in this example
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtPw.h"
#include "AtPdhDe3.h"
#include "AtPwPsn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void De3SAToPExample(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtSdhLine Line1(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
    }

/*
 * Mapping detail for line 1
 * - AUG-1#1: 3xAU-3 <--> VC-3 <--> DS3 unframe
 */
static void SetupSdhMappingToMapDs3(void)
    {
    AtSdhLine line1 = Line1();
    uint8 aug1Id = 0;
    AtSdhChannel aug1, vc3;
    AtPdhChannel de3;

    /* Setup mapping for 3xAU-3 <--> VC-3 <--> DS3 unframe */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3);

    /* And configure DS3 un-frame. Application can configure any frame mode since
     * the whole DS3 will be emulated on SAToP. */
    de3 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc3);
    AtPdhChannelFrameTypeSet(de3, cAtPdhDs3Unfrm);
    }

static void SetupPwUdpIpv4(AtPw pw)
    {
    AtPwUdpPsn udp;
    AtPwIpV4Psn ipv4;
    uint16 udpPort;
    uint8 destIpAddress[4]   = {172, 33, 35, 2};
    uint8 sourceIpAddress[4] = {172, 33, 35, 3};

    /* Build IP layer */
    ipv4 = AtPwIpV4PsnNew();
    AtPwIpPsnDestAddressSet((AtPwIpPsn)ipv4, destIpAddress);
    AtPwIpPsnSourceAddressSet((AtPwIpPsn)ipv4, sourceIpAddress);
    AtPwIpV4PsnTimeToLiveSet(ipv4, 255);

    /* Build UDP layer.
     * Just for simple, the PW ID is used as source and destination port. For
     * direction from PSN to TDM, the PW ID is also used as expected port. */
    udp = AtPwUdpPsnNew();
    udpPort = (uint16)AtChannelIdGet((AtChannel)pw);
    AtPwUdpPsnDestPortSet(udp, udpPort);
    AtPwUdpPsnSourcePortSet(udp, udpPort);
    AtPwUdpPsnExpectedPortSet(udp, udpPort);

    /* Link them to make PSN layer structure, in this case, it is UDP/IPv4 */
    AtPwPsnLowerPsnSet((AtPwPsn)udp, (AtPwPsn)ipv4);

    /* Assign this PSN to PW and destroy these objects since internal
     * implementation already clone them. */
    AtPwPsnSet(pw, (AtPwPsn)udp);
    AtObjectDelete((AtObject)udp);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwUdpIpv4(pw);
    SetupPwMac(pw);
    }

void De3SAToPExample(void)
    {
    AtSdhChannel vc3;
    AtChannel de3;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtSdhLine line1 = Line1();

    /* Setup mapping to have Ds3 mapped to VC-3 */
    SetupSdhMappingToMapDs3();

    /* Create a SAToP to emulate this DS3 */
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line1, 0, 0);
    de3 = AtSdhChannelMapChannelGet(vc3);
    pw  = (AtPw)AtModulePwSAToPCreate(pwModule, 0);
    AtPwCircuitBind(pw, de3);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*

CLIs to configure this flow:

# Set mapping to DE1
sdh map aug1.1.1 3xvc3s
sdh map vc3.1.1.1 de3

# DS3 un-frame
pdh de3 framing 1.1.1 ds3_unframed

# Emulate this DS3 unframe by SAToP. MPLS is used as PSN.
pw create satop 1
pw circuit bind 1 de3.1.1.1.1.1
pw ethport 1 1

# Use UDP/IPv4
pw psn 1 udp.ipv4

# Configure IPv4 layer
pw ipv4 source 1 172.33.35.2
pw ipv4 dest   1 172.33.35.3
pw ipv4 ttl    1 255

# Configure UDP layer
pw udp source 1 1
pw udp dest   1 1
pw udp expect 1 1

pw ethheader 1 CA.FE.CA.FE.CA.FE 0.0.1 none
pw enable 1
*/

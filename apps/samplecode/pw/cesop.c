/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : cesop.c
 *
 * Created Date: Sep 5, 2014
 *
 * Description : This source file is to show how NxDS0 groups are emulated on PSN by
 *               using CESoP Pseudowire.
 *
 *               To have NxDs0 groups, the E1 must be configured to E1 frame.
 *               having a framed E1, application can create NxDS0 groups to be
 *               emulated and bind them to CESoP PWs.
 *
 *               To make the sample code be simple to understand, the first VC-12
 *               of SDH Line#1 is configured to map E1 basic frame. Then two
 *               NxDS0 groups are created and bound to two CESoP PWs.
 *
 *               All of these PWs will use MEF-8 PSN and Ethernet Port 1 is used
 *               as Ethernet physical.
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void CESoPExample(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtSdhLine Line1(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
    }

/*
 * Mapping detail for line 1
 * - AUG-1#1: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 <--> E1 unframe
 */
static void SetupSdhMappingToMapE1(void)
    {
    AtSdhLine line1 = Line1();
    uint8 aug1Id = 0;
    AtSdhChannel aug1, vc4, tug3, tug2, vc12;
    AtPdhChannel de1;

    /* Setup mapping for AUG-1#1 to map AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line1, aug1Id, 0, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);

    /* VC-12 maps E1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);

    /* And configure E1 basic frame so that NxDS0 groups can be created. */
    de1 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet(de1, cAtPdhE1Frm);
    }

static void SetupPwMef8(AtPw pw)
    {
    AtPwMefPsn mef8Psn = AtPwMefPsnNew();
    uint32 ecId = AtChannelIdGet((AtChannel)pw); /* For simple demo, the PW ID is used as EC ID */

    /* EC ID for MEF-8 packets sent to ETH side */
    AtPwMefPsnTxEcIdSet(mef8Psn, ecId);

    /* For direction from ETH to TDM, when MEF-8 packets are received, their
     * EC ID are used to look for Pseudowire that they belong to.
     * The following line is to setup it. */
    AtPwMefPsnExpectedEcIdSet(mef8Psn, ecId);

    /* Apply this MEF-8 for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mef8Psn);
    AtObjectDelete((AtObject)mef8Psn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMef8(pw);
    SetupPwMac(pw);
    }

void CESoPExample(void)
    {
    AtSdhChannel vc12;
    AtPdhNxDS0 nxDs0;
    AtPdhDe1 de1;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtSdhLine line1 = Line1();

    /* Setup mapping to have E1 mapped to VC-12 */
    SetupSdhMappingToMapE1();
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, 0, 0, 0, 0);
    de1  = (AtPdhDe1)AtSdhChannelMapChannelGet(vc12);

    /* Create NxDS0 group with timeslot 1-3 and emulate it by CESoP PW */
    nxDs0 = AtPdhDe1NxDs0Create(de1, cBit3_1);
    pw = (AtPw)AtModulePwCESoPCreate(pwModule, 0, cAtPwCESoPModeBasic);
    AtPwCircuitBind(pw, (AtChannel)nxDs0);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);

    /* Create NxDS0 group with timeslot 17-31 and emulate it by CESoP PW */
    nxDs0 = AtPdhDe1NxDs0Create(de1, cBit31_17);
    pw = (AtPw)AtModulePwCESoPCreate(pwModule, 1, cAtPwCESoPModeBasic);
    AtPwCircuitBind(pw, (AtChannel)nxDs0);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*
CLIs to configure this flow:

# Set mapping to DE1
sdh map aug1.1.1 vc4
sdh map vc4.1.1 3xtug3s
sdh map tug3.1.1.1 7xtug2s
sdh map tug2.1.1.1.1 tu12
sdh map vc1x.1.1.1.1.1 de1

# Use E1 basic frame so that NxDS0 groups can be created
pdh de1 framing 1.1.1.1.1 e1_basic

# Emulate NxD0 with DS0 timeslots 1-3 by CESoP PW. Use MEF as PSN
pw create cesop 1 basic
pdh de1 nxds0create 1.1.1.1.1 1-3
pw circuit bind 1 nxds0.1.1.1.1.1.1-3
pw ethport 1 1
pw psn 1 mef
pw mef ecid transmit 1 1
pw mef ecid expect   1 1
pw ethheader 1 CA.FE.CA.FE.CA.FE 0.0.1 none
pw enable 1

# Emulate NxD0 with DS0 timeslots 17-31 by CESoP PW. Use MEF as PSN
pw create cesop 2 basic
pdh de1 nxds0create 1.1.1.1.1 17-31
pw circuit bind 2 nxds0.1.1.1.1.1.17-31
pw ethport 2 1
pw psn 2 mef
pw mef ecid transmit 2 2
pw mef ecid expect   2 2
pw ethheader 2 CA.FE.CA.FE.CA.FE 0.0.2 none
pw enable 2

*/

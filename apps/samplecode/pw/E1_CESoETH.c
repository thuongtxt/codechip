/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : mef_e1_ces.c
 *
 * Created Date: Oct 28, 2014
 *
 * Description : This source file is to show how a full E1 is emulated on MEF-8 by
 *               using SAToP Pseudowire.
 *
 *               In this example, 1 E1 unframe is mapped to SDH VC-12 and SAToP PW
 *               is used to emulate it. Just for simple demo purpose, the first
 *               VC-12 is used.
 *
 *               In this example the MEF-8/T-MPLS is used.
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void E1_CESoETH_Example(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtSdhLine Line1(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
    }

/*
 * Mapping detail for line 1
 * - AUG-1#1: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 <--> E1 unframe
 */
static void SetupSdhMappingToMapE1(void)
    {
    AtSdhLine line1 = Line1();
    uint8 aug1Id = 0;
    AtSdhChannel aug1, vc4, tug3, tug2, vc12;
    AtPdhChannel de1;

    /* Setup mapping for AUG-1#1 to map AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line1, aug1Id, 0, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);

    /* VC-12 maps E1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);

    /* And configure E1 un-frame. Application can configure any frame mode since
     * the whole E1 will be emulated on SAToP. */
    de1 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet(de1, cAtPdhE1UnFrm);
    }

static AtPwMefPsn CreateMef8ForPw(AtPw pw)
    {
    AtPwMefPsn mef8Psn  = AtPwMefPsnNew();
    uint32 ecId = AtChannelIdGet((AtChannel)pw); /* For simple demo, the PW ID is used as EC ID */
    uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    tAtEthVlanTag vlan;

    AtEthVlanTagConstruct(0, 0, AtChannelIdGet((AtChannel)pw), &vlan);

    /* EC ID for MEF-8 packets sent to ETH side */
    AtPwMefPsnTxEcIdSet(mef8Psn, ecId);

    /* For direction from ETH to TDM, when MEF-8 packets are received, their
     * EC ID are used to look for Pseudowire that they belong to.
     * The following line is to setup it. */
    AtPwMefPsnExpectedEcIdSet(mef8Psn, ecId);

    /* Configure MAC. Note, the following APIs are not available in SDK version <= 2.4.5.1 */
    AtPwMefPsnSourceMacSet(mef8Psn, mac);
    AtPwMefPsnDestMacSet(mef8Psn, mac);
    AtPwMefPsnVlanTagAdd(mef8Psn, 0x8100 /* cVLAN */, &vlan);

    return mef8Psn;
    }

static AtPwMplsPsn CreateMplsForPw(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack. Use only label for demo purpose */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    return mplsPsn;
    }

static void SetupPwMef8_Mpls(AtPw pw)
    {
    AtPwMefPsn  mef8Psn;
    AtPwMplsPsn mplsPsn;

    /* Create MEF-8 and MPLS. Then build PSN hierarchy. In this case, MEF-8 will
     * work over MPLS */
    mef8Psn = CreateMef8ForPw(pw);
    mplsPsn = CreateMplsForPw(pw);
    AtPwPsnLowerPsnSet((AtPwPsn)mef8Psn, (AtPwPsn)mplsPsn);

    /* Apply this MEF-8 for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mef8Psn);
    AtObjectDelete((AtObject)mef8Psn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MEF-8/MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMef8_Mpls(pw);
    SetupPwMac(pw);
    }

void E1_CESoETH_Example(void)
    {
    AtSdhChannel vc12;
    AtChannel de1;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtSdhLine line1 = Line1();

    /* Setup mapping to have E1 mapped to VC-12 */
    SetupSdhMappingToMapE1();

    /* Create a SAToP to emulate this E1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, 0, 0, 0, 0);
    de1  = AtSdhChannelMapChannelGet(vc12);
    pw   = (AtPw)AtModulePwSAToPCreate(pwModule, 0);
    AtPwCircuitBind(pw, de1);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : cep.c
 *
 * Created Date: Sep 5, 2014
 *
 * Description : This source file is to show how a CEP PW is setup to emulate
 *               SDH VC (VC-4/VC-3/VC-1x) to Packet Switch Network
 *
 *               For the sake of simplicity, this source file makes a default
 *               mapping on SDH layer to carry all kinds of VC mentioned above.
 *               And Line#1 is used.
 *
 *               There are 16 AUG-1s in STM-16 and 3 AUG-1s are used in this
 *               example. There mappings are as following:
 *               - AUG-1#1: VC-4
 *               - AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
 *               - AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
 *
 *               And this source file will emulate 3 VCs onto 3 CEP PW, they are:
 *               - VC-4: which is contained in AUG-1#1
 *               - The first VC-3  in AUG-1#2
 *               - The first VC-12 in AUG-1#3
 *
 *               All of these PWs will use MPLS PSN and Ethernet Port 1 is used
 *               as Ethernet physical.
 *
 * Notes       : CLIs to configure this flow are added at the end of this file
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void CepExample(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtSdhLine Line1(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
    }

/*
 * Mapping detail for line 1
 * - AUG-1#1: VC-4
 * - AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
 * - AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
 */
static void SetupSdhMapping(void)
    {
    AtSdhLine line1 = Line1();
    uint8 aug1Id;
    AtSdhChannel aug1, vc4, vc3, vc12, tug3, tug2;

    /* Setup mapping for AUG-1#1 to map VC-4 */
    aug1Id = 0;
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4MapC4);

    /* Setup mapping for AUG-1#2 to map AU-4 <--> VC-4 <--> TUG-3 <--> VC-3 */
    aug1Id = 1;
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3);
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapC3);

    /* Setup mapping for AUG-1#3 to map AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    aug1Id = 2;
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line1, aug1Id, 0, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapC1x);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    AtPwEthExpectedCVlanSet(pw, &cVlan);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

void CepExample(void)
    {
    AtChannel vc4, vc3, vc12;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtSdhLine line1 = Line1();

    /* Setup mapping */
    SetupSdhMapping();

    /* Emulate the VC-4 in AUG1#1. CEP basic is used to emulate the whole VC
     * content. */
    vc4 = (AtChannel)AtSdhLineVc4Get(line1, 0);
    pw  = (AtPw)AtModulePwCepCreate(pwModule, 0, cAtPwCepModeBasic);
    AtPwCircuitBind(pw, vc4);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);

    /* Emulate the first VC-3 in AUG1#2. CEP basic is used to emulate the whole VC
     * content. */
    vc3 = (AtChannel)AtSdhLineVc3Get(line1, 1, 0);
    pw  = (AtPw)AtModulePwCepCreate(pwModule, 1, cAtPwCepModeBasic);
    AtPwCircuitBind(pw, vc3);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);

    /* Emulate the first VC-12 in AUG1#3. CEP basic is used to emulate the whole VC
     * content. */
    vc12 = (AtChannel)AtSdhLineVc1xGet(line1, 2, 0, 0, 0);
    pw   = (AtPw)AtModulePwCepCreate(pwModule, 2, cAtPwCepModeBasic);
    AtPwCircuitBind(pw, vc12);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*
CLIs to configure this flow:

# AUG-1#1: VC-4
sdh map aug1.1.1 vc4
sdh map vc4.1.1 c4

# AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
sdh map aug1.1.2 vc4
sdh map vc4.1.2 3xtug3s
sdh map tug3.1.2.1 vc3
sdh map vc3.1.2.1 c3

# AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
sdh map aug1.1.3 vc4
sdh map vc4.1.3 3xtug3s
sdh map tug3.1.3.1 7xtug2s
sdh map tug2.1.3.1.1 tu12
sdh map vc1x.1.3.1.1.1 c1x

# Create CEP, setup it and bind it to vc4.1.1
pw create cep 1 basic
pw circuit bind 1 vc4.1.1
pw ethport 1 1
pw psn 1 mpls
pw mpls innerlabel 1 1.0.255
pw mpls outerlabel add 1 1.0.255
pw mpls expectedlabel 1 1
pw ethheader 1 CA.FE.CA.FE.CA.FE 0.0.1 none
pw enable 1

# Create CEP, setup it and bind it to vc3.1.2.1
pw create cep 2 basic
pw circuit bind 2 vc3.1.2.1
pw ethport 2 1
pw psn 2 mpls
pw mpls innerlabel 2 2.0.255
pw mpls outerlabel add 2 2.0.255
pw mpls expectedlabel 2 2
pw ethheader 2 CA.FE.CA.FE.CA.FE 0.0.2 none
pw enable 2

# Create CEP, setup it and bind it to vc1x.1.3.1.1.1
pw create cep 3 basic
pw circuit bind 3 vc1x.1.3.1.1.1
pw ethport 3 1
pw psn 3 mpls
pw mpls innerlabel 3 3.0.255
pw mpls outerlabel add 3 3.0.255
pw mpls expectedlabel 3 3
pw ethheader 3 CA.FE.CA.FE.CA.FE 0.0.3 none
pw enable 3

 */

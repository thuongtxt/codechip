/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : pwgroup.c
 *
 * Created Date: Mar 24, 2015
 *
 * Description : PW Group example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPwGroup.h"
#include "AtPw.h"
#include "AtCisco.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void PwGroupExample(AtModulePw pwModule);

/*--------------------------- Implementation ---------------------------------*/
void PwGroupExample(AtModulePw pwModule)
    {
    uint32 groupId = 0;
    AtPwGroup hsGroup;
    AtPw pw1 = AtModulePwGetPw(pwModule, 0);
    AtPw pw2 = AtModulePwGetPw(pwModule, 1);

    /* PW module must support a number of groups */
    AtAssert(AtModulePwMaxHsGroupsGet(pwModule) > 0);

    /* Assume group has not been created, there would be no such instance */
    AtAssert(AtModulePwHsGroupGet(pwModule, groupId) == NULL);

    /* After group is created, there would be one instance */
    AtAssert(AtModulePwHsGroupCreate(pwModule, groupId) != NULL);
    AtAssert((hsGroup = AtModulePwHsGroupGet(pwModule, groupId)) != NULL);

    /* Let assume that PW 1 and 2 already exist, they can be added to group by
     * the following callings */
    AtAssert(AtPwGroupPwAdd(hsGroup, pw1) == cAtOk);
    AtAssert(AtPwGroupPwAdd(hsGroup, pw2) == cAtOk);

    /* All PWs in group can be retrieved */
    AtAssert(AtPwGroupNumPws(hsGroup) == 2);
    AtAssert(AtPwGroupContainsPw(hsGroup, pw1));
    AtAssert(AtPwGroupContainsPw(hsGroup, pw2));
    AtAssert(AtPwGroupPwAtIndex(hsGroup, 0) == pw1);
    AtAssert(AtPwGroupPwAtIndex(hsGroup, 1) == pw2);

    /* And from PW, group that it belongs to can be also retrieved */
    AtAssert(AtPwHsGroupGet(pw1) == hsGroup);

    /* This is HSPW group, it do not have control to enable or disable. So the
     * internal implementation will consider it is enabled and there is no way
     * to disable it */
    AtAssert(AtPwGroupEnable(hsGroup, cAtFalse) != cAtOk);
    AtAssert(AtPwGroupIsEnabled(hsGroup) == cAtTrue);

    /* This is HS group, application can switch label sets */
    /* Label switching for direction from TDM to PSN */
    AtAssert(AtPwGroupTxLabelSetSelect(hsGroup, cAtPwGroupLabelSetPrimary) == cAtOk);
    AtAssert(AtPwGroupTxSelectedLabelGet(hsGroup) == cAtPwGroupLabelSetPrimary);
    AtAssert(AtPwGroupTxLabelSetSelect(hsGroup, cAtPwGroupLabelSetBackup) == cAtOk);
    AtAssert(AtPwGroupTxSelectedLabelGet(hsGroup) == cAtPwGroupLabelSetBackup);

    /* Label switching for direction from PSN to TDM */
    AtAssert(AtPwGroupRxLabelSetSelect(hsGroup, cAtPwGroupLabelSetPrimary) == cAtOk);
    AtAssert(AtPwGroupRxSelectedLabelGet(hsGroup) == cAtPwGroupLabelSetPrimary);
    AtAssert(AtPwGroupRxLabelSetSelect(hsGroup, cAtPwGroupLabelSetBackup) == cAtOk);
    AtAssert(AtPwGroupRxSelectedLabelGet(hsGroup) == cAtPwGroupLabelSetBackup);

    /* Finally, a PW group can be deleted when all of PWs are removed out of groups */
    AtAssert(AtModulePwHsGroupDelete(pwModule, groupId) != cAtOk); /* Fail because group is containing PWs */
    AtAssert(AtPwGroupPwRemove(hsGroup, pw1) == cAtOk);
    AtAssert(AtPwGroupPwRemove(hsGroup, pw2) == cAtOk);
    AtAssert(AtModulePwHsGroupDelete(pwModule, groupId) == cAtOk);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : liu_de1_satop_mpls.c
 *
 * Created Date: May 12, 2015
 *
 * Description : This source file is to show how a full E1 is emulated on PSN by
 *               using SAToP Pseudowire.
 *
 *               In this example, 1 E1 unframe from LIU interface
 *               is used to emulate it.
 *               This PW will use MPLS PSN and Ethernet Port 1 is used
 *               as Ethernet physical.
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtPw.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void LiuSAToPExample(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtModulePdh PdhModule(void)
    {
    return (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    }

static AtPdhDe1 PdhDe1Get(uint32 de1Id)
    {
    return AtModulePdhDe1Get(PdhModule(), de1Id);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

static uint32 De1Id(void)
    {
    /* Use first DE1 for demonstration purpose */
    return 0;
    }

static eAtPdhDe1FrameType DefaultFrameType(void)
    {
    return cAtPdhE1UnFrm;
    }

void LiuSAToPExample(void)
    {
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtPdhChannel de1 = (AtPdhChannel)PdhDe1Get(De1Id());

    /* Set default frame type for DE1 */
    AtPdhChannelFrameTypeSet(de1, DefaultFrameType());

    /* Create a SAToP to emulate this E1 */
    pw = (AtPw)AtModulePwSAToPCreate(pwModule, 0);
    AtPwCircuitBind(pw, (AtChannel)de1);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*

CLIs to configure this flow:

# E1 un-frame
pdh de1 framing 1.1.1.1.1 e1_unframed

# Emulate this E1 unframe by SAToP. MPLS is used as PSN.
pw create satop 1
pw circuit bind 1 de1.1
pw ethport 1 1
pw psn 1 mpls
pw mpls innerlabel 1 1.0.255
pw mpls outerlabel add 1 1.0.255
pw mpls expectedlabel 1 1
pw ethheader 1 C0.CA.c0.CA.C0.CA 0.0.1 none
pw enable 1

*/

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : gebypass.c
 *
 * Created Date: Aug 18, 2016
 *
 * Description : Sample code for GE bypass
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"
#include "AtCisco.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void GeByPassToQsgmii(void);
void GeByPassToXfi(void);
void FirstSerdesToStm16(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    return AtDriverDeviceGet(driver, 0);
    }

static AtModuleEth ModuleEth(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
    }

static void XfiPtchSetup(AtEthPort gePort)
    {
    uint16 ingressPtch = 0xAAAA;
    uint16 egressPtch  = 0xAA;

    /* PTCH service is obtained from module Ethernet and GE port */
    AtPtchService ptchService = AtModuleEthGeBypassPtchServiceGet(ModuleEth(), gePort);

    /* Enabling */
    AtAssert(AtPtchServiceEnable(ptchService, cAtTrue) == cAtOk);

    /* Ingress two-bytes PTCH (direction from GE to XFI) */
    AtAssert(AtPtchServiceIngressPtchSet(ptchService, ingressPtch) == cAtOk);

    /* Egress one-byte PTCH (direction from XFI to GE) */
    AtAssert(AtPtchServiceEgressPtchSet(ptchService, egressPtch) == cAtOk);
    }

/*
 * There are 4 ports which can work in OCN or GE mode depend on mode of Serdes,
 * by default, all 4 SERDESes work in mode OCN.
 *
 * There are 6 ethernet MACs in this product with following ID
 * 0    10G MAC
 * 1..4 SGMII
 * 5    QSGMII
 * QSGMII MAC contains 4 sub-ports with ID from 0..3 which can be retrieved by API AtEthPortSubPortGet
 */
static AtEthPort First1GMACGetFromSerdesController(void)
    {
    uint32 geSerdes0 = 0;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(Device());
    AtSerdesController firstGeSerdes = AtSerdesManagerSerdesControllerGet(serdesManager, geSerdes0);
    eAtRet ret;

    /* Configure SERDES to GE mode (it's defaulted to be OCN mode) */
    ret = AtSerdesControllerModeSet(firstGeSerdes, cAtSerdesModeEth1G);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Can not set serdes to Ge mode\r\n");
        return NULL;
        }

    /* After switching to GE mode, Ethernet port object could be retrieved */
    return (AtEthPort)AtSerdesControllerPhysicalPortGet(firstGeSerdes);
    }

void GeByPassToQsgmii(void)
    {
    uint8 qsgmiiPortId = 5;
    AtEthPort qsgmiiPort = AtModuleEthPortGet(ModuleEth(), qsgmiiPortId);
    AtEthPort qsgmiiSubPort = AtEthPortSubPortGet(qsgmiiPort, 0);
    eAtRet ret;

    /* In order to bypass GE traffic to QSGMII SERDES, get corresponding GE port and configure
     * bypass SERDES for it, after bypass SERDES is configured all ethernet traffic will be pass-through between
     * 2 SERDESes, without any special action */
    AtEthPort gePort = First1GMACGetFromSerdesController();
    ret = AtCiscoGePortBypassPortSet(gePort, qsgmiiSubPort);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Can not set bypass port for Ge port %s\r\n", AtChannelIdString((AtChannel)gePort));

    AtAssert(AtCiscoGePortBypassPortGet(gePort) == qsgmiiSubPort);
    }

void GeByPassToXfi(void)
    {
    uint8 port10GId = 0; /* As mentioned above, 10G port ID is 0 */
    AtEthPort port10G = AtModuleEthPortGet(ModuleEth(), port10GId);
    eAtRet ret;

    /* In order to bypass GE traffic to QSGMII SERDES, get corresponding GE port and configure
     * bypass SERDES for it */
    AtEthPort gePort = First1GMACGetFromSerdesController();
    ret = AtCiscoGePortBypassPortSet(gePort, port10G);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Can not set bypass serdes for Ge port %s\r\n", AtChannelIdString((AtChannel)gePort));

    AtAssert(AtCiscoGePortBypassPortGet(gePort) == port10G);

    /*
     * At direction from GE to XFI, PTCH is inserted
     * At direction from XFI to GE, PTCH is used for classifying
     * Setup PTCH service
     */
    XfiPtchSetup(gePort);
    }

void FirstSerdesToStm16(void)
    {
    uint32 serdes0 = 0;
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);
    AtSerdesController firstSerdes = AtSerdesManagerSerdesControllerGet(serdesManager, serdes0);

    /* Configure SERDES to STM-16 mode */
    eAtRet ret = AtSerdesControllerModeSet(firstSerdes, cAtSerdesModeStm16);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Can not set SERDES to STM-16/OC-48 mode\r\n");
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : App
 *
 * File        : aps.c
 *
 * Created Date: Jan 16, 2015
 *
 * Description : APS example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDriver.h"
#include "AtModuleAps.h"
#include "AtApsLinearEngine.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtSerdesController.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void LinearApsExample(void);
void ProductOnePort_EthSerdesAps(void);
void ProductTwoPort_EthSerdesAps(void);
void ProductFourPort_EthSerdesAps(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

void LinearApsExample(void)
    {
    /* Have all of necessary modules */
    AtModuleAps apsModule = (AtModuleAps)AtDeviceModuleGet(Device(), cAtModuleAps);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);

    /* Use line 1 as working and line 2 as protection line */
    AtSdhLine workingLine    = AtModuleSdhLineGet(sdhModule, 0);
    AtSdhLine protectionLine = AtModuleSdhLineGet(sdhModule, 1);

    /* Use the first engine */
    uint8 engineId = 0;
    AtSdhLine lines[] = {protectionLine, workingLine};
    AtApsEngine engine = AtModuleApsLinearEngineCreate(apsModule, engineId, lines, 2, cAtApsLinearArch11);
    AtApsLinearEngine linearEngine = (AtApsLinearEngine)engine;

    /* Start it */
    AtAssert(AtApsEngineStart(engine) == cAtOk);

    /* If working line and protection line have no alarms, there would be no
     * switched line. There would be no external command issued when engine is
     * first started */
    AtAssert(AtApsLinearEngineSwitchLineGet(linearEngine) == NULL);
    AtAssert(AtApsLinearEngineExtCmdGet(linearEngine) == cAtApsLinearExtCmdClear);
    AtAssert(AtApsLinearEngineExtCmdAffectedLineGet(linearEngine) == NULL);

    /* Engine is running ...  */
    /*...*/
    /*...*/

    /* The following lines will issue Force Switch command on working line and
     * traffic will be switched to protection line */
    AtAssert(AtApsLinearEngineExtCmdSet(linearEngine, cAtApsLinearExtCmdFs, workingLine) == cAtOk);
    AtAssert(AtApsLinearEngineExtCmdGet(linearEngine) == cAtApsLinearExtCmdFs);
    AtAssert(AtApsLinearEngineExtCmdAffectedLineGet(linearEngine) == workingLine);
    }

/* ETH 1+1 APS */
static void EthPortSerdesBridgeSwitch(AtEthPort port, AtSerdesController working, AtSerdesController protection)
	{
	AtAssert(AtEthPortSerdesController(port) == working);
	AtSerdesControllerEnable(protection, cAtTrue);
	AtEthPortTxSerdesBridge(port, protection);
	AtAssert(AtEthPortTxBridgedSerdes(port) == protection);
	AtEthPortRxSerdesSelect(port, protection);
	AtAssert(AtEthPortRxSelectedSerdes(port) == protection);
	AtSerdesControllerEnable(working, cAtFalse);
	}

void ProductOnePort_EthSerdesAps(void)
	{
	AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
	AtEthPort port0 = (AtEthPort)AtModuleEthPortGet(ethModule, 0);
	AtSerdesController controller[] = {AtModuleEthSerdesController(ethModule, 0),
									   AtModuleEthSerdesController(ethModule, 1)};
	EthPortSerdesBridgeSwitch(port0, controller[0], controller[1]);
	}

void ProductTwoPort_EthSerdesAps(void)
	{
	AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
	AtEthPort port[] = {AtModuleEthPortGet(ethModule, 0),
						AtModuleEthPortGet(ethModule, 1)
	                    };
	AtSerdesController controllers[2][2] = {{AtModuleEthSerdesController(ethModule, 0), AtModuleEthSerdesController(ethModule, 1)},
										  {AtModuleEthSerdesController(ethModule, 2), AtModuleEthSerdesController(ethModule, 3)}
										 };
	int i;
	for (i = 0; i < 2; i++)
		{
		EthPortSerdesBridgeSwitch(port[i], controllers[i][0], controllers[i][1]);
		}
	}

void ProductFourPort_EthSerdesAps(void)
	{
	AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
	AtEthPort port[] = {AtModuleEthPortGet(ethModule, 0),
						AtModuleEthPortGet(ethModule, 1),
						AtModuleEthPortGet(ethModule, 2),
						AtModuleEthPortGet(ethModule, 3)
	                   };
	AtSerdesController controllers[4][2] = {{AtModuleEthSerdesController(ethModule, 0), AtModuleEthSerdesController(ethModule, 4)},
										  {AtModuleEthSerdesController(ethModule, 1), AtModuleEthSerdesController(ethModule, 5)},
										  {AtModuleEthSerdesController(ethModule, 2), AtModuleEthSerdesController(ethModule, 6)},
										  {AtModuleEthSerdesController(ethModule, 3), AtModuleEthSerdesController(ethModule, 7)}
										 };
	int i;
	for (i = 0; i < 4; i++)
		{
		EthPortSerdesBridgeSwitch(port[i], controllers[i][0], controllers[i][1]);
		}
	}

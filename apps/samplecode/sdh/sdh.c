/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : sdh.c
 *
 * Created Date: Mar 26, 2015
 *
 * Description : SDH example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"

#include "commacro.h"
#include "AtCisco.h"
#include "AtChannel.h"

#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#define c10GNumSts192 1
#define c20GNumSts192 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Oc48LineAccess(AtModuleSdh sdhModule);
void Ec1Mapping(AtModuleSdh sdhModule);
void Oc48Mapping(AtModuleSdh sdhModule);
void Ec1Stm0LineAccess(AtModuleSdh sdhModule);
void Oc48LineOverhead(void);
void Ec1Stm0LineOverhead(void);

void Tfi5Loopback(AtModuleSdh sdhModule);
void FirstLineToStm16(AtModuleSdh sdhModule);

/* Mapping functions */
AtSdhLine DeviceSonetLineGet(AtDevice device, uint8 lineId);
AtSdhAug DeviceLineAug16Set(AtDevice device, uint8 lineId);
AtSdhVc DeviceLineSts48c_c4_16cSet(AtDevice device, uint8 lineId);
void DeviceLine4xAug4sSet(AtDevice device, uint8 lineId);
AtSdhVc DeviceLineSts12c_c4_4cSet(AtDevice device, uint8 lineId, uint8 aug4Id);
void DeviceLineAug4_4xAug1sSet(AtDevice device, uint8 lineId, uint8 aug4Id);
AtSdhVc DeviceLineAug1_Sts3c_c4Set(AtDevice device, uint8 lineId, uint8 aug1Id);
void DeviceLineAug1_Sts1Set(AtDevice device, uint8 lineId, uint8 aug1Id);
AtSdhVc DeviceLineAug1_Sts1_c3Set(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3);
AtPdhDe3 DeviceLineAug1_Sts1_de3Set(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3);
void DeviceLineAug1_Sts1_7vtgsSet(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3);
void DeviceLineAug1_Sts1_Vtg_Vt1_5(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId);
void DeviceLineAug1_Sts1_Vtg_Vt2(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId);
AtSdhVc DeviceLineAug1_Sts1_Vtg_Vc1x_c1xSet(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId, uint8 vc1xId);
AtPdhDe1 DeviceLineAug1_Sts1_Vtg_Vc1x_de1Set(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId, uint8 vc1xId);

/*MRO*/
AtSdhVc Line_Stm64_vc4_64c(AtModuleSdh sdhModule, uint8 lineId, eBool isC4_64c);
void Mro10G_sts192c_setup(AtModuleSdh sdhModule);
void Mro20G_sts192c_setup(AtModuleSdh sdhModule);

/*--------------------------- Implementation ---------------------------------*/
void Oc48LineAccess(AtModuleSdh sdhModule)
    {
    AtSdhLine tfi5Line;

    /* This product support 8 TFI-5 Lines */
    AtAssert(AtModuleSdhMaxLinesGet(sdhModule)  == 8);
    AtAssert(AtModuleSdhNumTfi5Lines(sdhModule) == 8);

    /* Access the first TFI-5 Line */
    AtAssert((tfi5Line = AtModuleSdhTfi5LineGet(sdhModule, 0)));

    /* There assigned ID is as following */
    AtAssert(AtChannelIdGet((AtChannel)tfi5Line) == 0);

    /* And these lines can be also access by its assigned flat ID */
    AtAssert(tfi5Line == AtModuleSdhLineGet(sdhModule, 0));
    }

/* get a sonet line object from line index */
AtSdhLine DeviceSonetLineGet(AtDevice device, uint8 lineId)
    {
    AtModule sdhModule = AtDeviceModuleGet(device, cAtModuleSdh);
    return AtModuleSdhLineGet((AtModuleSdh)sdhModule, lineId);
    }

/* set a oc48 line to aug16 and return the aug16 */
AtSdhAug DeviceLineAug16Set(AtDevice device, uint8 lineId)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    return AtSdhLineAug16Get(line, 0);
    }

/* set a sonet line to sts48c and return a sts48c (vc4_16c)*/
AtSdhVc DeviceLineSts48c_c4_16cSet(AtDevice device, uint8 lineId)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhAug aug16 = DeviceLineAug16Set(device, 0);
    AtSdhVc vc4_16c;
    AtSdhChannelMapTypeSet((AtSdhChannel)aug16, cAtSdhAugMapTypeAug16MapVc4_16c);
    vc4_16c = AtSdhLineVc4_16cGet(line, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc4_16c, cAtSdhVcMapTypeVc4_16cMapC4_16c);
    return vc4_16c;
    }
/* get AU for vc4-16c for pointer operation like force/unforce ais: use AtSdhLineAu4_16cGet */

/* set a sonet line to 4 aug4s, a preparation before setting sts12c or sts3c or sts1 or ds3/e3 or vt1.5/vt2 or ds1/e1*/
void DeviceLine4xAug4sSet(AtDevice device, uint8 lineId)
    {
    AtSdhAug aug16 = DeviceLineAug16Set(device, lineId);
    AtSdhChannelMapTypeSet((AtSdhChannel)aug16, cAtSdhAugMapTypeAug16Map4xAug4s);
    }

/* set an aug4 to sts12c and get it, asummed the AUG16 configured to cAtSdhAugMapTypeAug16Map4xAug4s before*/
AtSdhVc DeviceLineSts12c_c4_4cSet(AtDevice device, uint8 lineId, uint8 aug4Id)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
    AtSdhVc vc4_4c;

    AtSdhChannelMapTypeSet((AtSdhChannel)aug4, cAtSdhAugMapTypeAug4MapVc4_4c);
    vc4_4c = AtSdhLineVc4_4cGet(line, aug4Id);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc4_4c, cAtSdhVcMapTypeVc4_4cMapC4_4c);
    return vc4_4c;
    }

/* get au for vc4-4c for pointer operation like force AIS, use AtSdhLineAu4_4cGet */

/* set an aug4 to aug1 for sts3c/sts1/vt/ds3/e3/ds1/e1. It is asummed that the aug16 map to 4xaug4s before */
void DeviceLineAug4_4xAug1sSet(AtDevice device, uint8 lineId, uint8 aug4Id)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);

    AtSdhChannelMapTypeSet((AtSdhChannel)aug4, cAtSdhAugMapTypeAug4Map4xAug1s);
    }

/* set sts3c for a aug1/sts3*/
AtSdhVc DeviceLineAug1_Sts3c_c4Set(AtDevice device, uint8 lineId, uint8 aug1Id)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
    AtSdhVc vc4;

    AtSdhChannelMapTypeSet((AtSdhChannel)aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = AtSdhLineVc4Get(line, aug1Id);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc4, cAtSdhVcMapTypeVc4MapC4);
    return vc4;
    }
/* get au4 for pointer operation like ais force: AtSdhLineAu4Get(line, aug1Id) */

/* set a aug1 to 3 sts1. It is assumed AUG4 is configured to 4xaug1s before */
void DeviceLineAug1_Sts1Set(AtDevice device, uint8 lineId, uint8 aug1Id)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);

    AtSdhChannelMapTypeSet((AtSdhChannel)aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    }

/*set st1 carry c3. it is assumed that its aug1 configure to 3xVC3 before*/
AtSdhVc DeviceLineAug1_Sts1_c3Set(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhVc vc3;

    vc3 = AtSdhLineVc3Get(line, aug1Id, sts1InSts3);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3MapC3);
    return vc3;
    }
/* get au3 for pointer operation, use: AtSdhLineAu3Get(line, aug1Id, sts1InSts3)*/

/* set sts1 carry de3, and get its de3. It is assumed that its aug1 is configure to 3xvc3 before */
AtPdhDe3 DeviceLineAug1_Sts1_de3Set(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhVc vc3;

    vc3 = AtSdhLineVc3Get(line, aug1Id, sts1InSts3);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3MapDe3);
    return (AtPdhDe3)AtSdhChannelMapChannelGet((AtSdhChannel)vc3);
    }

/* set sts1 carry 7vtg. it is assumed its aug1 configure as 3xvc3 before */
void DeviceLineAug1_Sts1_7vtgsSet(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhVc vc3;

    vc3 = AtSdhLineVc3Get(line, aug1Id, sts1InSts3);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3Map7xTug2s);
    }

/* set a vtg to vt1.5 or vt2. Assume the STS1 is set to 7xtug2 before */
void DeviceLineAug1_Sts1_Vtg_Vt1_5(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, sts1InSts3, vtgId);
    AtSdhChannelMapTypeSet((AtSdhChannel)tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    }
void DeviceLineAug1_Sts1_Vtg_Vt2(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, sts1InSts3, vtgId);
    AtSdhChannelMapTypeSet((AtSdhChannel)tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    }
AtSdhVc DeviceLineAug1_Sts1_Vtg_Vc1x_c1xSet(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId, uint8 vc1xId)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhVc vc1x = AtSdhLineVc1xGet(line, aug1Id, sts1InSts3, vtgId, vc1xId);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc1x, cAtSdhVcMapTypeVc1xMapC1x);
    return vc1x;
    }
/* get tu1x for pointer operation, use: AtSdhLineTu1xGet(line, aug1Id, sts1InSts3, vtgId, tuIdInVtg);*/

/* set vc1x map de1, then get the de1. It is assumed that its vtg/tug2 is configured before */
AtPdhDe1 DeviceLineAug1_Sts1_Vtg_Vc1x_de1Set(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 sts1InSts3, uint8 vtgId, uint8 vc1xId)
    {
    AtSdhLine line = DeviceSonetLineGet(device, lineId);
    AtSdhVc vc1x = AtSdhLineVc1xGet(line, aug1Id, sts1InSts3, vtgId, vc1xId);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc1x, cAtSdhVcMapTypeVc1xMapDe1);
    return (AtPdhDe1)AtSdhChannelMapChannelGet((AtSdhChannel)vc1x);
    }

/* Big notes: after a channel bound to PW, to change mapping/framing for a channel, it must be unbound from its PW before changing mapping.*/

void Oc48Mapping(AtModuleSdh sdhModule)
    {
    uint8 lineId = 0, aug1 = 0, tug3 = 0, tug2, tu;
    AtSdhLine tfi5line = AtModuleSdhLineGet(sdhModule, lineId);

    /* Get AUG-4 of this line and map it to 4xAUG-1 */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineAug4Get(tfi5line, 0), cAtSdhAugMapTypeAug4Map4xAug1s);

    /* Map desired AUG-1 to VC-4 */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineAug1Get(tfi5line, aug1), cAtSdhAugMapTypeAug1MapVc4);

    /* Map this VC-4 to 3xTUG-3s */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineVc4Get(tfi5line, aug1), cAtSdhVcMapTypeVc4Map3xTug3s);

    /* Map the first TUG-3 to VC-3 */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug3Get(tfi5line, aug1, tug3), cAtSdhTugMapTypeTug3MapVc3);

    /* Configure the second TUG-3 to carry E1s */
    /* Map the second TUG-3 to 7xTUG-2s */
    tug3 = 1;
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug3Get(tfi5line, aug1, tug3), cAtSdhTugMapTypeTug3Map7xTug2s);

    /* Map all of TUG-2s in this TUG-3 to TU-12 */
    for (tug2 = 0; tug2 < 7; tug2++)
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug2Get(tfi5line, aug1, tug3, tug2), cAtSdhTugMapTypeTug2Map3xTu12s);

    /* Just for demo purpose, just configure TU-12s of one TUG-2 to carry E1s */
    tug2 = 0;
    for (tu = 0; tu < 3; tu++) /* There are 3 TU-12s in TUG-2 */
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineVc1xGet(tfi5line, aug1, tug3, tug2, tu), cAtSdhVcMapTypeVc1xMapDe1);
    }

void Ec1Stm0LineAccess(AtModuleSdh sdhModule)
    {
    AtSdhLine ec1Stm0Line;

    /* There are 48 lines */
    AtAssert(AtModuleSdhMaxLinesGet(sdhModule) == 48);

    /* Can access any EC-1/STM-0 line */
    ec1Stm0Line = AtModuleSdhLineGet(sdhModule, 3);
    AtAssert(ec1Stm0Line != NULL);
    AtAssert(AtChannelIdGet((AtChannel)ec1Stm0Line) == 3);

    /* Its rate can be access as following. Note, line rate cannot be changed */
    AtAssert(AtSdhLineRateGet(ec1Stm0Line) == cAtSdhLineRateStm0);
    AtAssert(AtSdhLineRateSet(ec1Stm0Line, cAtSdhLineRateStm1) != cAtOk);
    AtAssert(AtSdhLineRateSet(ec1Stm0Line, cAtSdhLineRateStm4) != cAtOk);
    }

void Oc48LineOverhead(void)
    {
    const char * ttiMessage = "Line#0";
    tAtSdhTti tti;

    /* Get SDH module first */
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Get line object, its rate should be STM-16 */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 0);
    AtAssert(AtSdhLineRateGet(line) == cAtSdhLineRateStm16);

    /* As default, the line mode is SDH, and application can change to SONET */
    AtSdhChannelModeSet((AtSdhChannel)line, cAtSdhChannelModeSonet);

    /* Configure TTI, both transmitted and expected are not necessary for TFI-5
     * product and APIs will report error */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, (const uint8 *)ttiMessage, (uint8)AtStrlen(ttiMessage), &tti);
    AtAssert(AtSdhChannelTxTtiSet((AtSdhChannel)line, &tti) != cAtOk);
    AtAssert(AtSdhChannelExpectedTtiSet((AtSdhChannel)line, &tti) != cAtOk);
    }

void Ec1Stm0LineOverhead(void)
    {
    const char * ttiMessage = "Line#0";
    tAtSdhTti tti;

    /* Get SDH module first */
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Get any EC-1/STM-0, its rate must be STM-0 */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 4);
    AtAssert(AtSdhLineRateGet(line) == cAtSdhLineRateStm0);

    /* As default, the line mode is SDH, and application can change to SONET */
    AtSdhChannelModeSet((AtSdhChannel)line, cAtSdhChannelModeSonet);

    /* Configure TTI, both transmitted and expected must be successful */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, (const uint8 *)ttiMessage, (uint8)AtStrlen(ttiMessage), &tti);
    AtAssert(AtSdhChannelTxTtiSet((AtSdhChannel)line, &tti) == cAtOk);
    AtAssert(AtSdhChannelExpectedTtiSet((AtSdhChannel)line, &tti) == cAtOk);

    /* Application can access K-Bytes */
    AtAssert(AtSdhLineTxK1Set(line, 0x1) == cAtOk);
    AtAssert(AtSdhLineTxK1Get(line) == 0x1);
    AtAssert(AtSdhLineTxK2Set(line, 0x2) == cAtOk);
    AtAssert(AtSdhLineTxK2Get(line) == 0x2);

    /* And it can also access RX overhead */
    AtPrintc(cSevNormal, "- RX K1: 0x%x\r\n", AtSdhLineRxK1Get(line));
    AtPrintc(cSevNormal, "- RX K2: 0x%x\r\n", AtSdhLineRxK2Get(line));
    /* So on, ... */
    }

void Ec1Mapping(AtModuleSdh sdhModule)
    {
    /* Get associated VC-3 */
    uint8 ec1LineId = 0, aug1 = 0, au3 = 0, tug2, tu;
    AtSdhLine ec1Line = AtModuleSdhLineGet(sdhModule, ec1LineId);
    AtSdhChannel vc3 = (AtSdhChannel)AtSdhLineVc3Get(ec1Line, aug1, 0);

    /* Map VC-3 to TUG-2s */
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);

    /* Map all of TUG-2s in this VC-3 to TU-12 */
    for (tug2 = 0; tug2 < 7; tug2++)
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug2Get(ec1Line, aug1, au3, tug2), cAtSdhTugMapTypeTug2Map3xTu12s);

    /* Just for demo purpose, just configure TU-12s of one TUG-2 to carry E1s */
    tug2 = 0;
    for (tu = 0; tu < 3; tu++) /* There are 3 TU-12s in TUG-2 */
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineVc1xGet(ec1Line, aug1, au3, tug2, tu), cAtSdhVcMapTypeVc1xMapDe1);
    }

/* do remote loopback for 8 tfi-5 serdes*/
void Tfi5Loopback(AtModuleSdh sdhModule)
	{
	AtSdhLine lines[8];
	AtSerdesController serdes[8];
	eAtLoopbackMode loopMode = cAtLoopbackModeRemote;
	uint8 i;
	for (i = 0; i < 8; i++)
		{
		lines[i] = AtModuleSdhLineGet(sdhModule, i);
		serdes[i] = AtSdhLineSerdesController(lines[i]);
		AtSerdesControllerLoopbackSet(serdes[i], loopMode);
		}
	}

void FirstLineToStm16(AtModuleSdh sdhModule)
    {
    AtSdhLine firstLine;

    /* This product support 4 SONET/SDH framers */
    AtAssert(AtModuleSdhMaxLinesGet(sdhModule)  == 4);

    /* Access the first Line */
    AtAssert((firstLine = AtModuleSdhLineGet(sdhModule, 0)));

    /* There assigned ID is as following */
    AtAssert(AtChannelIdGet((AtChannel)firstLine) == 0);

    /* Set this line to work in rate STM-16/OC-48 */
    AtAssert(AtSdhLineRateSet(firstLine, cAtSdhLineRateStm16) == cAtOk);
    }

AtSdhVc Line_Stm64_vc4_64c(AtModuleSdh sdhModule, uint8 lineId, eBool isC4_64c)
    {
    AtSdhLine line = NULL;
    AtSdhAug aug64 = NULL;
    AtSdhVc  vc4_64c = NULL;

    /* Access the first Line */
    line = AtModuleSdhLineGet(sdhModule, lineId);

    /* Set this line to work in rate STM-16/OC-48 */
    AtAssert(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
    aug64 = AtSdhLineAug64Get(line, 0);
    AtAssert(AtSdhChannelMapTypeSet((AtSdhChannel)aug64, cAtSdhAugMapTypeAug64MapVc4_64c) == cAtOk);
    vc4_64c = AtSdhLineVc4_64cGet(line, 0);
    if (isC4_64c)
        AtAssert(AtSdhChannelMapTypeSet((AtSdhChannel)vc4_64c, cAtSdhVcMapTypeVc4_64cMapC4_64c) == cAtOk);
    return vc4_64c;
    }

void Mro20G_sts192c_setup(AtModuleSdh sdhModule)
    {
    AtSdhVc face_sts192s[c20GNumSts192] = {Line_Stm64_vc4_64c(sdhModule, 0, cAtFalse), Line_Stm64_vc4_64c(sdhModule, 8, cAtFalse)};
    AtSdhVc mate_tfi5_sts192s[c20GNumSts192] = {Line_Stm64_vc4_64c(sdhModule, 16, cAtFalse), Line_Stm64_vc4_64c(sdhModule, 20, cAtFalse)};
    AtSdhVc term_sts192s[c20GNumSts192] = {Line_Stm64_vc4_64c(sdhModule, 24, cAtTrue), Line_Stm64_vc4_64c(sdhModule, 28, cAtTrue)};
    uint8 i;
    for (i = 0; i < c20GNumSts192; i++)
        {
        AtAssert(face_sts192s[i]);
        AtAssert(mate_tfi5_sts192s[i]);
        AtAssert(term_sts192s[i]);
        }
    }

void Mro10G_sts192c_setup(AtModuleSdh sdhModule)
    {
    /*
    1x10G port [1]
    4x 2.5G ports [1-5-9-13]
    16xOC12/3 ports [1-16]
    16xGE ports [1-16]
    4xTFI-5 Mate lines [17-20]
    4x2.5G termiated lines [25-28]
    */
    AtSdhVc face_sts192s[c10GNumSts192] = {Line_Stm64_vc4_64c(sdhModule, 0, cAtFalse)};
    AtSdhVc mate_tfi5_sts192s[c10GNumSts192] = {Line_Stm64_vc4_64c(sdhModule, 16, cAtFalse)};
    AtSdhVc term_sts192s[c10GNumSts192] = {Line_Stm64_vc4_64c(sdhModule, 24, cAtTrue)};
    uint8 i;
    for (i = 0; i < c10GNumSts192; i++)
        {
        AtAssert(face_sts192s[i]);
        AtAssert(mate_tfi5_sts192s[i]);
        AtAssert(term_sts192s[i]);
        }
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : sur.c
 *
 * Created Date: Jun 12, 2015
 *
 * Description : Surveillance Monitoring example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void PathFailureNotify(AtSurEngine surEngine,
                              void *appContext,
                              uint32 failureType,
                              uint32 currentStatus)
    {
    AtChannel path = AtSurEngineChannelGet(surEngine);

    /* Show the path that has failures change status */
    AtPrintc(cSevInfo, "Failure change status on path: %s.%s\r\n", AtChannelTypeString(path), AtChannelIdString(path));

    if (failureType & cAtSdhPathAlarmAis)
        {
        uint32 raise = currentStatus & cAtSdhPathAlarmAis;
        AtPrintc(raise ? cSevCritical : cSevInfo, "* AIS: %s\r\n", raise ? "raise" : "clear");
        }

    if (failureType & cAtSdhPathAlarmLop)
        {
        uint32 raise = currentStatus & cAtSdhPathAlarmLop;
        AtPrintc(raise ? cSevCritical : cSevInfo, "* LOP: %s\r\n", raise ? "raise" : "clear");
        }

    /* So on ... */
    }

static void PmParamShow(AtPmParam param)
    {
    AtPmRegister pmRegister;

    /* Name of this parameter */
    AtPrintc(cSevInfo,   "* Param: %s\r\n", AtPmParamName(param));

    /* Current period register */
    pmRegister = AtPmParamCurrentPeriodRegister(param);
    AtPrintc(cSevNormal,
             "  - Current period: %u, %s\r\n",
             AtPmRegisterValue(pmRegister),
             AtPmRegisterIsValid(pmRegister) ? "valid" : "invalid");

    /* Previous period register */
    pmRegister = AtPmParamPreviousPeriodRegister(param);
    AtPrintc(cSevNormal,
             "  - Previous period: %u, %s\r\n",
             AtPmRegisterValue(pmRegister),
             AtPmRegisterIsValid(pmRegister) ? "valid" : "invalid");
    }


/* For simple demonstration, the following example code show how to access the
 * STS K threshold pool and set all of its entries to values set by Table 6-11
 * of GR-253. It also try to cover all of related pool APIs */
void KThresholdPoolSetup(AtModuleSur module)
    {
    AtPmThresholdPool stsThresholdPool = AtModuleSurStsThresholdPoolGet(module);
    uint32 numEntries, entry_i;

    /* Iterate all of entries */
    AtAssert((numEntries = AtPmThresholdPoolNumEntries(stsThresholdPool)) > 0);
    /* follow the table 6-8, gr253: entry 0 for STS1 */
    AtAssert(AtPmThresholdPoolEntryValueSet(stsThresholdPool, 0, 2400) == cAtOk);
    if (numEntries > 1)
    	/* entry 1 for STS-nc */
    	AtAssert(AtPmThresholdPoolEntryValueSet(stsThresholdPool, 1, 2400) == cAtOk);
    }

/*
 * The following function show how to configure K threshold for an engine. For
 * simple demonstration, this example use the first VC-4 of the input line
 */
void KThresholdSettingExample(AtSdhLine line)
    {
    AtChannel firstVc4 = (AtChannel)AtSdhLineVc4Get(line, 0);
    AtSurEngine engine = AtChannelSurEngineGet(firstVc4);
    AtModuleSur surModule = AtSurEngineModuleGet(engine);

    /* Just simply use the entry 1 for sts-nc k threshold setup above in threshold pool */
    AtAssert(AtSurEngineSesThresholdPoolEntrySet(engine, 1));
    AtAssert(AtSurEngineSesThresholdPoolEntryGet(engine) == 1);
    }

void PathSurEngineSetup(AtSdhPath path)
    {
    void *appContext = NULL;
    AtSurEngine engine = AtChannelSurEngineGet((AtChannel)path);
    static tAtSurEngineListener listener = {.FailureNotify = PathFailureNotify,
                                            .TcaNotify     = NULL};

    /* Register failure notification listener and enable notification */
    AtAssert(AtSurEngineListenerAdd(engine, appContext, &listener) == cAtOk);
    AtAssert(AtSurEngineFailureNotificationEnable(engine, cAtTrue) == cAtOk);
    AtAssert(AtSurEngineFailureNotificationIsEnabled(engine));

    /* Start running */
    AtAssert(AtSurEngineEnable(engine, cAtTrue) == cAtOk);
    AtAssert(AtSurEngineIsEnabled(engine));
    }

void PathPmParamShow(AtSdhPath path)
    {
    AtSurEngine engine = AtChannelSurEngineGet((AtChannel)path);

    /* Request hardware latch all of registers */
    AtAssert(AtSurEngineAllCountersLatch(engine) == cAtOk);

    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPpjcPdet));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamNpjcPdet));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPpjcPgen));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamNpjcPgen));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcDiff));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcsPdet));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamPjcsPgen));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamCvP));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamEsP));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamSesP));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamUasP));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamFcP));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamCvPfe));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamEsPfe));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamSesPfe));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamUasPfe));
    PmParamShow(AtSurEnginePmParamGet(engine, cAtSurEngineSdhPathPmParamFcPfe));
    }

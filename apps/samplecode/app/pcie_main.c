/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : main.c
 *
 * Created Date: Feb 9, 2018
 *
 * Description : Sample source code to bring up driver. For the sake of simplicity,
 *               and make it be easy to understand, this sample code is to
 *               handle driver with one device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "atclib.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* The SDK will use bit[31:28] = 0xF to see if device is running in simulation mode */
#define mSimulatedProductCode(realProductCode) ((realProductCode & ~(cBit31_28)) | (0xFUL << 28))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* For simulation. The m_isSimulate must be set to cAtTrue in order to run in
 * simulation mode, the m_simulateProductCode variable must also be set to
 * correct product code of the device want to simulate */
static eBool  m_isSimulate          = cAtFalse; /* TODO: Set this to cAtTrue to run on simulation */
static uint32 m_simulateProductCode = 0x60290022;

/* For command processing */
static AtTextUI m_textUI = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern void *mmap64 (void * addr, size_t len, int prot, int flags, int fildes, long long off);

/*--------------------------- Implementation ---------------------------------*/
static void PCIeWrite(uint32 realAddress, uint32 value)
    {
    *((uint32 *)realAddress) = value;
    }

static uint32 PCIeRead(uint32 realAddress)
    {
    return *((uint32 *)realAddress);
    }

static uint32 PCIeMemoryMap(void)
    {
    int fd;
    long long offset = 0xfd0000000LL;
    void *baseAddress;
    size_t memorySizeInBytes = 0x8000000; /* 128MB */

    fd = open("/dev/mem", O_RDWR);
    if (fd < 0)
        {
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidUint32;
        }

    baseAddress = mmap64(NULL, (size_t)memorySizeInBytes, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
    if (baseAddress == ((void *)-1))
        {
        close(fd);
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidUint32;
        }

    close(fd);

    return (uint32)(AtSize)baseAddress;
    }

static AtHal CreateHal(uint32 baseAddress)
    {
    if (m_isSimulate)
        return AtHalSimCreateForProductCode(m_simulateProductCode, cAtHalSimDefaultMemorySizeInMbyte);

    return AtHalDefaultWithHandlerNew(baseAddress, PCIeWrite, PCIeRead);
    }

static eAtRet DriverInit(void)
    {
    uint32 baseAddress;
    AtDevice device;
    AtHal hal;
    uint32 productCode;

    /* Create driver with one device */
    AtDriverCreate(1, AtOsalLinux());

    /* Create HAL to access registers */
    baseAddress = PCIeMemoryMap();
    hal = CreateHal(baseAddress);

    /* Read product code to create device instance */
    if (m_isSimulate)
        productCode = mSimulatedProductCode(m_simulateProductCode);
    else
        productCode = AtProductCodeGetByHal(hal);

    /* Create device */
    device = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
    if (device == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Product code 0x%08x is not supported\r\n", productCode);
        return cAtErrorRsrcNoAvail;
        }

    /* There is only one IP core, and need to give HAL to it so that registes
     * can be accessed */
    AtDeviceIpCoreHalSet(device, 0 /* only one core */, hal);

    /* Optional: initialize device */
    /*AtDeviceInit(device);*/

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    AtDevice device = AtDriverDeviceGet(driver, 0);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0); /* Save HAL to delete later */
    AtDriverDeviceDelete(driver, device);
    AtHalDelete(hal);
    AtDriverDelete(driver);
    return cAtOk;
    }

static AtTextUI TinyTextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
    }

int main(int argc, char* argv[])
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    /* Initialize driver */
    ret = DriverInit();
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot initialize driver, ret = %s\r\n", AtRet2String(ret));

    /* Start CLI. */
    AtCliStartWithTextUI(TinyTextUI());

    /* Cleanup */
    DriverCleanup(AtDriverSharedDriverGet());
    AtTextUIDelete(TinyTextUI());

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : samplecode
 *
 * File        : card_1_N_protection.c
 *
 * Created Date: Jun 14, 2018
 *
 * Description : example code for application of PDH card 1:N protection.
 * There are 2 kinds of PDH cards: Normal working PDH card (1) and 1:N protection PDH card (2).
 *
 * The normal working PDH card include 2 CEM FPGA physical devices(device#1 and device#2)
 * and its sdk driver include 2 logical devices corresponding to the 2 physical devices.
 *
 * The 1:N protection PDH card also include the 2 CEM FPGA physical devices
 * and its sdk driver include 2 logical devices corresponding to the 2 physical devices.
 * They are calls logical master devices which can play the same role as 2 devices in the normal PDH card.
 *
 * The 1:N protection PDH card support 5 profiles per physical device.
 * So in 1:N protection PDH card, there are 5 profiles corresponding to 5 device#1s in 5 normal working PDH cards
 * and there are also 5 other profiles corresponding to 5 device#2s in the 5 normal working PDH cards.
 *
 * In 1:N protection card, each profile is a profile_logical_device.
 * This profile logical device is created by the master logical device.
 * For master devices, application initially just setup the 2 master devices, set hal for them, init them onetime when system boot up.
 * After that, for profile logical device, application use it as device handle for any API call
 * which is the same call at the device in the normal PDH card protected by this profile.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumProfilePerPhyDevice 5
#define cNumPhyDevice 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtDriver m_AtDriver = NULL;
static AtDevice m_Master_Devices[cNumPhyDevice] = {NULL, NULL};
static const m_Phy_Base_Address[cNumPhyDevice] = {0xfc0000000UL, 0xfd0000000UL};
static AtDevice m_Profile_Devices[cNumPhyDevice][cNumProfilePerPhyDevice] = {
                                               {NULL, NULL, NULL, NULL, NULL},
                                               {NULL, NULL, NULL, NULL, NULL}};

/*--------------------------- Forward declarations ---------------------------*/
eAtRet DriverInit(void);
eAtRet DriverCleanup(AtDriver driver);

/*--------------------------- Implementation ---------------------------------*/

static AtHal CreateHal(uint32 baseAddress)
    {
    if (m_isSimulate)
        return AtHalSimCreateForProductCode(m_simulateProductCode, cAtHalSimDefaultMemorySizeInMbyte);

    return AtHalDefaultWithHandlerNew(baseAddress, PCIeWrite, PCIeRead);
    }

eAtRet DriverInit(void)
    {
    AtHal hal[cNumPhyDevice];
    uint32 productCode;
    uint32 device_idx, profile_idx;
    eAtRet ret = cAtOk;

    /* Create driver with one device */
    m_AtDriver = AtDriverCreate(cNumPhyDevice, AtOsalLinux());

    /* Create HAL to access registers */
    for (device_idx = 0; device_idx < cNumPhyDevice; device_idx++)
        {
        hal[device_idx] = CreateHal(m_Phy_Base_Address[device_idx]);
        productCode = AtProductCodeGetByHal(hal[device_idx]);
        /* Create device */
        m_Master_Devices[device_idx] = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
        if (m_Master_Devices[device_idx] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Product code 0x%08x is not supported\r\n", productCode);
            return cAtErrorRsrcNoAvail;
            }

        /* There is only one IP core, and need to give HAL to it so that registes
         * can be accessed */
        ret |= AtDeviceIpCoreHalSet(m_Master_Devices[device_idx], 0 , hal[device_idx]);
        ret |= AtDeviceInit(m_Master_Devices[device_idx]);
        }

    for (device_idx = 0; device_idx < cNumPhyDevice; device_idx++)
        for (profile_idx = 0; profile_idx < cNumProfilePerPhyDevice; profile_idx++)
            m_Profile_Devices[device_idx][profile_idx] = AtDeviceSubDeviceGet(m_Master_Devices[device_idx], profile_idx);
    /* Then application can use the m_Profile_Devices[device_idx][profile_idx] as a device handle for all API calls */

    return ret;
    }

eAtRet DriverCleanup(void)
    {
    AtHal hal[cNumPhyDevice];
    uint32 productCode;
    uint32 device_idx;
    eAtRet ret = cAtOk;
    AtHal tmpHal;
    AtDevice tmpDevice;

    for (device_idx = 0; device_idx < cNumPhyDevice; device_idx++)
        {
        productCode = AtProductCodeGetByHal(hal[device_idx]);
        /* Create device */

        tmpDevice = AtDriverDeviceGet(m_AtDriver, device_idx);
        tmpHal = AtDeviceIpCoreHalGet(tmpDevice, 0);
        AtDriverDeviceDelete(m_AtDriver, tmpDevice);
        AtHalDelete(hal);
        }

    AtDriverDelete(m_AtDriver);
    return cAtOk;
    }

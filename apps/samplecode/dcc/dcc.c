/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : dcc.c
 *
 * Created Date: Oct 18, 2016
 *
 * Description : Sample code of DCC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"
#include "AtSdhLine.h"
#include "AtPw.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void DccChannelsSetup(void);
void DccChannelVlanConfigure(void);
void ControlFlowSetup(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtSdhLine Line1(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, double tags are used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwIngressVlan(AtPw pw)
    {
    static uint8 destMac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static uint8 srcMac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};  /* Application has to know, too */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, destMac, &cVlan, NULL);
    AtPwEthSrcMacSet(pw, srcMac);
    }

/* PW need to be configured expected VLAN so that
 * it can base on the VLAN field(12-bits) to route packets to the appropriate HDLC channel
 * in this case, most inner VLAN tag (C-VLAN) is used */
static void SetupPwEgressVlan(AtPw pw)
    {
    static tAtEthVlanTag expectedVlan;

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &expectedVlan);

    AtPwEthExpectedCVlanSet(pw, &expectedVlan);
    }

void DccChannelsSetup(void)
    {
    AtSdhLine line1 = Line1();
    AtHdlcChannel sdcc = AtSdhLineDccChannelCreate(line1, cAtSdhLineDccLayerSection);
    AtHdlcChannel ldcc = AtSdhLineDccChannelCreate(line1, cAtSdhLineDccLayerLine);

    if ((sdcc == NULL) || (ldcc == NULL))
        {
        AtPrintc(cSevCritical, "Can not create SDCC or LDCC\r\n");
        return;
        }

    /* Both or either SDCC and LDCC can be enabled */
    AtChannelEnable((AtChannel)sdcc, cAtTrue);
    AtChannelEnable((AtChannel)ldcc, cAtTrue);

    /* SDCC and LDCC can be configured encapsulation type separately */
    AtHdlcChannelFrameTypeSet(sdcc, cAtHdlcFrmCiscoHdlc);
    AtHdlcChannelFrameTypeSet(ldcc, cAtHdlcFrmPpp);

    /* FCS mode is 32 bytes or 16 bytes */
    AtHdlcChannelFcsModeSet(sdcc, cAtHdlcFcsModeFcs16);
    AtHdlcChannelFcsModeSet(ldcc, cAtHdlcFcsModeFcs32);
    }

void DccChannelVlanConfigure(void)
    {
    /* Get device instance first */
    AtDevice device = Device();
    AtSdhLine line1 = Line1();
    AtModulePw pwModule;
    AtPw pw;

    /* Assume that DCC channels have been already created */
    AtHdlcChannel sdcc = AtSdhLineDccChannelGet(line1, cAtSdhLineDccLayerSection);
    if (sdcc == NULL)
        {
        AtPrintc(cSevCritical, "DCC channel has not been created\r\n");
        return;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwHdlcCreate(pwModule, sdcc);

    /* Configure ingress and egress VLAN for PW */
    SetupPwIngressVlan(pw);
    SetupPwEgressVlan(pw);

    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void ControlFlowSetup(void)
    {
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag circuitVlan;
    AtEthFlow oamFlow;
    AtSdhLine sdhLine = Line1();
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtSdhLineDccChannelGet(sdhLine, cAtSdhLineDccLayerSection);

    /* Get link from HDLC channel */
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet(hdlcChannel);

    /* Configure flow for control packets */
    oamFlow = AtHdlcLinkOamFlowGet(link);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &circuitVlan), &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);
    }

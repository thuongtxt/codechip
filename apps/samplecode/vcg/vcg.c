/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : vcg.c
 *
 * Created Date: Feb 17, 2016
 *
 * Description : VCG sample code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"

#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"

#include "AtPdhChannel.h"
#include "AtPdhDe1.h"

#include "AtVcg.h"
#include "AtConcateMember.h"

#include "AtEncapChannel.h"
#include "AtGfpChannel.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"

#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Eth_Gfp_Ds1_3v_SetupExample(void);
void Eth_Laps_Ds1_3v_SetupExample(void);
void Lcas_Vc3_3v_Gfp_SetupExample(void);

/*--------------------------- Implementation ---------------------------------*/
static AtConcateGroup Vc3Ds1VcatVcgSetupExample(uint32 vcgId)
    {
    uint8 lineId = 0, aug1Id = 0;

    AtModuleSdh sdhModule;
    AtSdhLine line;
    AtSdhChannel aug4, aug1;
    AtSdhVc vc3_0, vc3_1, vc3_2;
    AtSdhTug tug2_0, tug2_1, tug2_2;
    AtSdhVc vc1x_0, vc1x_1, vc1x_2;
    AtPdhChannel ds1_0, ds1_1, ds1_2;

    AtModuleConcate concateModule;
    AtConcateGroup vcg;
    AtConcateMember skMember1, skMember2, skMember3, soMember1, soMember2, soMember3;

    /* Get device instance first */
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);

    /* Create Sonet VC-3s. */
    sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    line = (AtSdhLine)AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhLineRateSet(line, cAtSdhLineRateStm16);
    AtSdhChannelModeSet((AtSdhChannel)line, cAtSdhChannelModeSonet);
    aug4 = (AtSdhChannel)AtSdhLineAug4Get(line, aug1Id);
    AtSdhChannelMapTypeSet(aug4, cAtSdhAugMapTypeAug4Map4xAug1s);
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    vc3_0 = AtSdhLineVc3Get(line, aug1Id, 0);
    vc3_1 = AtSdhLineVc3Get(line, aug1Id, 1);
    vc3_2 = AtSdhLineVc3Get(line, aug1Id, 2);

    /* Create DS1s */
    AtSdhChannelMapTypeSet((AtSdhChannel)vc3_0, cAtSdhVcMapTypeVc3Map7xTug2s);
    tug2_0 = AtSdhLineTug2Get(line, aug1Id, 0, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)tug2_0, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc1x_0 = AtSdhLineVc1xGet(line, aug1Id, 0, 0, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc1x_0, cAtSdhVcMapTypeVc1xMapDe1);
    ds1_0 = (AtPdhChannel)AtSdhChannelMapChannelGet((AtSdhChannel)vc1x_0);
    AtPdhChannelFrameTypeSet(ds1_0, cAtPdhDs1FrmEsf);

    AtSdhChannelMapTypeSet((AtSdhChannel)vc3_1, cAtSdhVcMapTypeVc3Map7xTug2s);
    tug2_1 = AtSdhLineTug2Get(line, aug1Id, 1, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)tug2_1, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc1x_1 = AtSdhLineVc1xGet(line, aug1Id, 1, 0, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc1x_1, cAtSdhVcMapTypeVc1xMapDe1);
    ds1_1 = (AtPdhChannel)AtSdhChannelMapChannelGet((AtSdhChannel)vc1x_1);
    AtPdhChannelFrameTypeSet(ds1_1, cAtPdhDs1FrmEsf);

    AtSdhChannelMapTypeSet((AtSdhChannel)vc3_2, cAtSdhVcMapTypeVc3Map7xTug2s);
    tug2_2 = AtSdhLineTug2Get(line, aug1Id, 2, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)tug2_2, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc1x_2 = AtSdhLineVc1xGet(line, aug1Id, 2, 0, 0);
    AtSdhChannelMapTypeSet((AtSdhChannel)vc1x_2, cAtSdhVcMapTypeVc1xMapDe1);
    ds1_2 = (AtPdhChannel)AtSdhChannelMapChannelGet((AtSdhChannel)vc1x_2);
    AtPdhChannelFrameTypeSet(ds1_2, cAtPdhDs1FrmEsf);

    /* Create a VCAT VCG and also enable LCAS mode */
    concateModule = (AtModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    vcg = AtModuleConcateGroupCreate(concateModule, vcgId, cAtConcateMemberTypeDs1, cAtConcateGroupTypeVcat);

    /* Provision 3xVC-3s to VCG at Sink side */
    skMember1 = AtConcateGroupSinkMemberProvision(vcg, (AtChannel)ds1_0);
    skMember2 = AtConcateGroupSinkMemberProvision(vcg, (AtChannel)ds1_1);
    skMember3 = AtConcateGroupSinkMemberProvision(vcg, (AtChannel)ds1_2);
    /* Provision 3xVC-3s to VCG at Source side */
    soMember1 = AtConcateGroupSourceMemberProvision(vcg, (AtChannel)ds1_0);
    soMember2 = AtConcateGroupSourceMemberProvision(vcg, (AtChannel)ds1_1);
    soMember3 = AtConcateGroupSourceMemberProvision(vcg, (AtChannel)ds1_2);

    AtPrintc(cSevInfo, "Member 1 Source SQ : %d\r\n", AtConcateMemberSourceSequenceGet(soMember1));
    AtPrintc(cSevInfo, "Member 1 Sink SQ : %d\r\n", AtConcateMemberSinkSequenceGet(skMember1));
    AtPrintc(cSevInfo, "Member 2 Source SQ : %d\r\n", AtConcateMemberSourceSequenceGet(soMember2));
    AtPrintc(cSevInfo, "Member 2 Sink SQ : %d\r\n", AtConcateMemberSinkSequenceGet(skMember2));
    AtPrintc(cSevInfo, "Member 3 Source SQ : %d\r\n", AtConcateMemberSourceSequenceGet(soMember3));
    AtPrintc(cSevInfo, "Member 3 Sink SQ : %d\r\n", AtConcateMemberSinkSequenceGet(skMember3));

    return vcg;
    }

static void EthFlowSetupExample(AtEthFlow flow)
    {
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;

    AtEthFlowVlanDescConstruct(ethPortId, AtEthVlanTagConstruct(1, 0, 1, &sVlan), AtEthVlanTagConstruct(1, 0, 2, &cVlan), &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow, &vlanTrafficDesc);
    }

void Eth_Gfp_Ds1_3v_SetupExample(void)
    {
    AtModuleEth ethModule;
    AtEthFlow flow;
    AtEncapChannel encapChannel;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);
    uint32 vcgId = 0;

    /* Setup VCG */
    AtConcateGroup vcg = Vc3Ds1VcatVcgSetupExample(vcgId);

    /* Each group has an associated GFP channel, just retrieve
    * it and configure*/
    AtConcateGroupEncapTypeSet(vcg, cAtEncapGfp);
    encapChannel = AtConcateGroupEncapChannelGet(vcg);

    /* Create Flow and bind with GFP Channel */
    flow = AtEncapChannelBoundFlowGet(encapChannel);
    if (flow == NULL)
        {
        ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
        flow = AtModuleEthFlowCreate(ethModule, AtModuleEthFreeFlowIdGet(ethModule));
        EthFlowSetupExample(flow);
        AtEncapChannelFlowBind(encapChannel, flow);
        }
    }

void Eth_Laps_Ds1_3v_SetupExample(void)
    {
    AtModuleEth ethModule;
    AtEthFlow flow;
    AtHdlcChannel hdlcChannel;
    AtHdlcLink hdlcLink;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);
    uint32 vcgId = 0;

    /* Setup VCG */
    AtConcateGroup vcg = Vc3Ds1VcatVcgSetupExample(vcgId);


    /* Create HDLC channel and bind VCG */
    AtConcateGroupEncapTypeSet(vcg, cAtEncapHdlc);
    hdlcChannel = (AtHdlcChannel)AtConcateGroupEncapChannelGet(vcg);
    hdlcLink = AtHdlcChannelHdlcLinkGet(hdlcChannel);

    /* Create Flow and bind with LAPS-Link */
    flow = AtHdlcLinkBoundFlowGet(hdlcLink);
    if (flow == NULL)
        {
        ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
        flow = AtModuleEthFlowCreate(ethModule, AtModuleEthFreeFlowIdGet(ethModule));
        EthFlowSetupExample(flow);
        AtHdlcLinkFlowBind(hdlcLink, flow);
        }
    }

void Lcas_Vc3_3v_Gfp_SetupExample(void)
    {
    uint8 lineId = 24, aug1Id = 0;
    uint8 vcgId  = 0;
    AtConcateMember skMember[3];
    AtConcateMember soMember[3];
    uint8 id;

    /* Get device instance first */
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);

    /* Create a VCAT VCG and also enable LCAS mode */
    AtModuleConcate mapModule = (AtModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    AtVcg vcg = (AtVcg)AtModuleConcateGroupCreate(mapModule, vcgId, cAtConcateMemberTypeVc3, cAtConcateGroupTypeVcat);

    /* Create SDH VC-3s. */
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line        = (AtSdhLine)AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel aug4     = (AtSdhChannel)AtSdhLineAug4Get(line, aug1Id);
    AtSdhChannel aug1     = (AtSdhChannel)AtSdhLineAug1Get(line, aug1Id);

    AtSdhChannelMapTypeSet(aug4, cAtSdhAugMapTypeAug4Map4xAug1s);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);

    AtAssert(AtConcateGroupEncapTypeSet((AtConcateGroup)vcg, cAtEncapGfp));
    AtAssert(AtConcateGroupEncapChannelGet((AtConcateGroup)vcg) != NULL);

    AtAssert(AtVcgLcasEnable(vcg, cAtTrue));

    /* Provision 3xVC-3s to VCG at Sink side */
    for (id = 0; id < 3; id++)
        skMember[id] = AtConcateGroupSinkMemberProvision((AtConcateGroup)vcg, (AtChannel)AtSdhLineVc3Get(line, aug1Id, id));

    /* Provision 3xVC-3s to VCG at Source side */
    for (id = 0; id < 3; id++)
        soMember[id] = AtConcateGroupSourceMemberProvision((AtConcateGroup)vcg, (AtChannel)AtSdhLineVc3Get(line, aug1Id, id));

    /* Add Sink members */
    for (id = 0; id < 3; id++)
        AtAssert(AtVcgSinkMemberAdd(vcg, skMember[id]) == cAtOk);

    /* Add Source members */
    for (id = 0; id < 3; id++)
        AtAssert(AtVcgSourceMemberAdd(vcg, soMember[id]) == cAtOk);

    /* Retrieve Source state */
    AtPrintc(cSevInfo, "Source STATE: %d\r\n", AtConcateMemberSourceStateGet(soMember[0]));
    AtPrintc(cSevInfo, "Source SQ   : %d\r\n", AtConcateMemberSourceSequenceGet(soMember[0]));
    AtPrintc(cSevInfo, "Source MST  : %d\r\n", AtConcateMemberSourceMstGet(soMember[0]));
    AtPrintc(cSevInfo, "Source CTRL : %d\r\n", AtConcateMemberSourceControlGet(soMember[0]));

    /* Retrieve Sink state */
    AtPrintc(cSevInfo, "Sink STATE  : %d\r\n", AtConcateMemberSinkStateGet(skMember[0]));
    AtPrintc(cSevInfo, "Sink SQ     : %d\r\n", AtConcateMemberSinkSequenceGet(skMember[0]));
    AtPrintc(cSevInfo, "Sink MST    : %d\r\n", AtConcateMemberSinkMstGet(skMember[0]));
    AtPrintc(cSevInfo, "Sink CTRL   : %d\r\n", AtConcateMemberSinkControlGet(skMember[0]));
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : datalink.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : Data link example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Ds1DataLinkExample(AtPdhDe1 de1);
void E1DataLinkExample(AtPdhDe1 de1, uint8 saBits);
void MdlControllerCountersDisplay(AtPdhDe1 de1);

/*--------------------------- Implementation ---------------------------------*/
static void OnLapdChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint32 byte_i, length;
    static uint8 dataLinkBuffer[1024]; /* Just an example, application should have its own buffer */

    AtUnused(currentStatus);

    /* Just care LAPD change event */
    if ((changedAlarms & cAtPdhDs1AlarmLapdChange) == 0)
        return;

    /* Show the channel that receives this message */
    AtPrintc(cSevNormal, "\r\nNewMsg is received at %s.%s\r\n",
             AtChannelTypeString(channel),
             AtChannelIdString(channel));

    /* Display this message */
    length = AtPdhChannelDataLinkReceive((AtPdhChannel) channel, dataLinkBuffer, sizeof(dataLinkBuffer));
    for (byte_i = 0; byte_i < length; byte_i++)
        {
        AtPrintc(cSevInfo, " %02x", dataLinkBuffer[byte_i]);
        if (((byte_i + 1) % 16) == 0)
            AtPrintc(cSevInfo, "\r\n");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static tAtChannelEventListener *DataLinkListener(void)
    {
    static tAtChannelEventListener de1DataLinkListener;
    AtOsalMemInit(&de1DataLinkListener, 0, sizeof(de1DataLinkListener));
    de1DataLinkListener.AlarmChangeState = OnLapdChanged;
    return &de1DataLinkListener;
    }

void Ds1DataLinkExample(AtPdhDe1 de1)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)de1;

    /* Enable data link */
    AtAssert(AtPdhChannelDataLinkEnable(pdhChannel, cAtTrue) == cAtOk);
    AtAssert(AtPdhChannelDataLinkIsEnabled(pdhChannel) == cAtTrue);

    /* Register listener for new LAPD */
    AtAssert(AtChannelEventListenerAdd((AtChannel)de1, DataLinkListener()) == cAtOk);
    }

void E1DataLinkExample(AtPdhDe1 de1, uint8 saBits)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)de1;

    /* Enable data link */
    AtAssert(AtPdhChannelDataLinkEnable(pdhChannel, cAtTrue) == cAtOk);
    AtAssert(AtPdhChannelDataLinkIsEnabled(pdhChannel) == cAtTrue);

    /* Application can specify SA Bit options */
    AtAssert(AtPdhDe1DataLinkSaBitsMaskSet(de1, saBits) == cAtOk);
    AtAssert(AtPdhDe1DataLinkSaBitsMaskGet(de1) == saBits);

    /* Register listener for new LAPD */
    AtAssert(AtChannelEventListenerAdd((AtChannel) de1, DataLinkListener()) == cAtOk);
    }

void MdlControllerCountersDisplay(AtPdhDe1 de1)
    {
    AtPrintc(cSevInfo, " TxPkt     : %d \r\n", AtPdhChannelDataLinkCounterGet((AtPdhChannel) de1, cAtPdhDataLinkCounterTypeTxPkt));
    AtPrintc(cSevInfo, " TxBytes   : %d \r\n", AtPdhChannelDataLinkCounterGet((AtPdhChannel) de1, cAtPdhDataLinkCounterTypeTxByte));
    AtPrintc(cSevInfo, " TxAbortPkt: %d \r\n", AtPdhChannelDataLinkCounterGet((AtPdhChannel) de1, cAtPdhDataLinkCounterTypeTxAbortPkt));

    /* So on... */
    }

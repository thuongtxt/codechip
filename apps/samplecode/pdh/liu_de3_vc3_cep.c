/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : liu_de3_vc3_cep.c
 *
 * Created Date: DEC 23, 2015
 *
 * Description : Liu De3 over VC3 CEP example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"
#include "AtPw.h"
#include "AtPdhSerialLine.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void SetupLiuOverVc3Mapping(uint32 lineId);
void SetupLiuOverVc3CepExample(uint32 lineId);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtModulePdh PdhModule(void)
    {
    return (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    }

static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

void SetupLiuOverVc3Mapping(uint32 lineId)
    {
    AtSdhLine sdhLine;
    AtSdhVc vc3;
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(PdhModule(), lineId);
    AtAssert(serialLine != NULL);

    /* Set DS3 LIU mode */
    AtAssert(AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeDs3) == cAtOk);
    AtAssert(AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeDs3);

    /* Set Mapping from LIU to VC#3, it is special code to ADM DS3/E3 Liu to VC3 */
    sdhLine = AtModuleSdhLineGet(SdhModule(), (uint8)(lineId + AtModuleSdhStartEc1LineIdGet(SdhModule())));
    vc3 = AtSdhLineVc3Get(sdhLine, 0, 0);
    AtAssert(AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3MapDe3)== cAtOk);
    }

void SetupLiuOverVc3CepExample(uint32 lineId)
    {
    AtPw pw;
    AtSdhVc vc3;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtSdhLine sdhLine = AtModuleSdhLineGet(SdhModule(), (uint8)(lineId + AtModuleSdhStartEc1LineIdGet(SdhModule())));

    /* Setup mapping */
    SetupLiuOverVc3Mapping(lineId);

    /* Get VC#3 and create PW CEP */
    vc3 = AtSdhLineVc3Get(sdhLine, 0, 0);
    pw  = (AtPw)AtModulePwCepCreate(pwModule, 0, cAtPwCepModeBasic);
    AtPwCircuitBind(pw, (AtChannel)vc3);
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    /* Enable EPAR */
    AtPwCepEparEnable((AtPwCep)pw, cAtTrue);
    }


/********
device init
pdh serline mode 1-48 ds3
sdh map vc3.1-48 de3
eth port srcmac 1 C0.CA.C0.CA.C0.CA
eth port ipv4 1 172.33.34.35
eth port loopback 1 release
pw create cep 1 basic
pw circuit bind 1 vc3.1
pw ethport 1 1
pw psn 1 mpls
pw enable 1
pw cep epar enable 1
*********/

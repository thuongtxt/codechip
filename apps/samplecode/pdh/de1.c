/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : de1.c
 *
 * Created Date: Feb 3, 2015
 *
 * Description : This example is to show how to configure the DE1 instance, access
 *               its alarms and statistic information.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPdhDe1.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtModulePrbs.h"
#include "AtPrbsEngine.h"


/*--------------------------- Define -----------------------------------------*/
#define cDurationInSecondMax  1800 /* Seconds*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void ShowDe1Status(AtChannel de1);
void De1Example(AtPdhDe1 de1);
void De1BertExample(AtPdhDe1 de1);
void De1BertDestroy(AtPdhDe1 de1);
void De1BertStart(AtPdhDe1 de1);
void De1BertStop(AtPdhDe1 de1);
void BertStatisticStart(void);
void BertStatisticShow(void);
void De1BertPerformance(AtPdhDe1 de1, eAtPrbsMode mode, eBool inv, uint32 userdefine, uint32 durationInSeconds);

/*--------------------------- Implementation ---------------------------------*/
static void ShowDe1Alarm(uint32 alarms)
    {
    if (alarms == 0)
        {
        AtPrintf("None");
        return;
        }

    if (alarms & cAtPdhDe1AlarmLos) AtPrintf("LOS ");
    if (alarms & cAtPdhDe1AlarmLof) AtPrintf("LOF ");
    if (alarms & cAtPdhDe1AlarmAis) AtPrintf("AIS ");

    /* So on... */
    AtPrintf("\r\n");
    }

void ShowDe1Status(AtChannel de1)
    {
    /* Show current alarm status */
    AtPrintf("Alarm: ");
    ShowDe1Alarm(AtChannelAlarmGet(de1));

    /* Show alarm change history status (read then clear) */
    AtPrintf("Alarm history: ");
    ShowDe1Alarm(AtChannelAlarmInterruptClear(de1));

    /* Show counter information (read to clear mode).
     * The API AtChannelCounter() can be used for read only mode.
     */
    AtPrintf("BPV EXZ counter  : %d\r\n", AtChannelCounterClear(de1, cAtPdhDe1CounterBpvExz));
    AtPrintf("CRR error counter: %d\r\n", AtChannelCounterClear(de1, cAtPdhDe1CounterCrc));
    AtPrintf("FE counter       : %d\r\n", AtChannelCounterClear(de1, cAtPdhDe1CounterFe));
    AtPrintf("REI counter      : %d\r\n", AtChannelCounterClear(de1, cAtPdhDe1CounterRei));
    }

void De1Example(AtPdhDe1 de1)
    {
    /* Just simply set frame type */
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm) == cAtOk);

    /* And display its status */
    ShowDe1Status((AtChannel)de1);
    }

/* Configure Patterns */
static void BertPattern(AtChannel channel, eAtPrbsMode mode, eBool inv, uint32 transmit_userdefine, uint32 expect_userdefine)
    {
    uint32 ret;
    AtPrbsEngine engine = AtChannelPrbsEngineGet(channel);
    if (!engine)
        {
        AtPrintc(cSevCritical,
                 "ERROR: channel %s.%s has not created PRBS engine! \r\n",
                 AtChannelTypeString(channel),
                 AtChannelIdString(channel));
        }
    ret = AtPrbsEngineInit(engine);
    ret |= AtPrbsEngineModeSet(engine, mode);
    ret |= AtPrbsEngineInvert(engine, inv);
    ret |= AtPrbsEngineTxFixedPatternSet(engine, transmit_userdefine);
    ret |= AtPrbsEngineRxFixedPatternSet(engine, expect_userdefine);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Error: when set pattern mode %d \r\n", mode);
    }

/* Enable or Disable BERT for DS1/E1 channel */
static void BertEnable(AtChannel channel, eBool enable)
    {
    AtPrbsEngine engine = AtChannelPrbsEngineGet(channel);
    if (!engine)
        {
        AtPrintc(cSevCritical,
                 "ERROR: channel %s.%s has not created PRBS engine! \r\n",
                 AtChannelTypeString(channel),
                 AtChannelIdString(channel));
        }
    AtPrbsEngineEnable(engine, enable);
    }

/* Destroy BERT for a DS1/E1 channel */
void De1BertDestroy(AtPdhDe1 de1)
    {
    AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)de1);
    if (engine)
        return AtObjectDelete((AtObject)engine);

    AtPrintc(cSevCritical,
             "ERROR: channel %s.%s has not created PRBS engine! \r\n",
             AtChannelTypeString((AtChannel) de1),
             AtChannelIdString((AtChannel) de1));
    }

/* Start BERT */
void De1BertStart(AtPdhDe1 de1)
    {
    BertEnable((AtChannel)de1, cAtTrue);
    }

/* Stop BERT */
void De1BertStop(AtPdhDe1 de1)
    {
    BertEnable((AtChannel)de1, cAtFalse);
    }

/* Counter statistics */
static eBool  isStart;
static uint32 txFrmsCnt;
static uint32 rxFrmsCnt;
static uint32 txBitsCnt;
static uint32 rxBitsCnt;
static uint32 rxBitsErrCnt;
static uint32 rxBitsLastSync;
static uint32 rxBitsErrLastSync;
static uint32 rxSyncCnt;

void BertStatisticStart(void)
    {
    txFrmsCnt = 0;
    rxFrmsCnt = 0;
    txBitsCnt = 0;
    rxBitsCnt = 0;
    rxBitsErrCnt = 0;
    rxBitsLastSync = 0;
    rxBitsErrLastSync = 0;
    rxSyncCnt = 0;
    isStart = cAtTrue;
    }

void BertStatisticShow(void)
    {
    AtPrintc(cSevInfo,"=======================================\r\n");
    AtPrintc(cSevInfo,"BERT test result (%s) \r\n", isStart ? "running":"stop");
    AtPrintc(cSevInfo,"=======================================\r\n");
    AtPrintc(cSevInfo,"txFrmsCnt        : %08d \r\n", txFrmsCnt);
    AtPrintc(cSevInfo,"rxFrmsCnt        : %08d \r\n", rxFrmsCnt);
    AtPrintc(cSevInfo,"TxBitsCnt        : %08d \r\n", txBitsCnt);
    AtPrintc(cSevInfo,"rxBitsCnt        : %08d \r\n", rxBitsCnt);
    AtPrintc(cSevInfo,"rxBitsErrCnt     : %08d \r\n", rxBitsErrCnt);
    AtPrintc(cSevInfo,"rxBitsLastSync   : %08d \r\n", rxBitsLastSync);
    AtPrintc(cSevInfo,"rxBitsErrLastSync: %08d \r\n", rxBitsErrLastSync);
    AtPrintc(cSevInfo,"rxSyncCnt        : %08d \r\n", rxSyncCnt);
    }

/* DS1/E1 BERT Performance Testing */
void De1BertPerformance(AtPdhDe1 de1, eAtPrbsMode mode, eBool inv, uint32 userdefine, uint32 durationInSeconds)
    {
    AtOsal osal = AtOsalSharedGet();
    uint32 counter_i;
    De1BertExample(de1);
    BertPattern((AtChannel)de1, mode, inv, userdefine, userdefine);
    De1BertStart(de1);
    De1BertStop(de1);
    if (durationInSeconds >= cDurationInSecondMax)
        {
        AtPrintc(cSevCritical,
                 "Error: duration time is too big!, support under 1800 seconds \r\n");
        }

    BertStatisticStart();
    for (counter_i = 0; counter_i < durationInSeconds; counter_i++)
        {
        AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) de1);
        /* Sleep in 1 Seconds */
        osal->methods->Sleep(osal, 1);

        txFrmsCnt += AtPrbsEngineCounterGet(engine,
                                            cAtPrbsEngineCounterTxFrame);
        rxFrmsCnt += AtPrbsEngineCounterGet(engine,
                                            cAtPrbsEngineCounterRxFrame);
        txBitsCnt += AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterTxBit);
        rxBitsCnt += AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxBit);
        rxBitsErrCnt += AtPrbsEngineCounterGet(engine,
                                               cAtPrbsEngineCounterRxBitError);
        rxBitsLastSync +=
                AtPrbsEngineCounterGet(engine,
                                       cAtPrbsEngineCounterRxBitLastSync);
        rxBitsErrLastSync +=
                AtPrbsEngineCounterGet(engine,
                                       cAtPrbsEngineCounterRxBitErrorLastSync);
        rxSyncCnt += AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxSync);

        }
    isStart = cAtFalse;
    De1BertDestroy(de1);
    }

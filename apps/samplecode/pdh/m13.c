/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : m13.c
 *
 * Created Date: Feb 3, 2015
 *
 * Description : This example code to show how M13/E13 is configured.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void ShowDe1Status(AtChannel de1);
void M13Example(AtPdhDe3 de3);

/*--------------------------- Implementation ---------------------------------*/
void M13Example(AtPdhDe3 de3)
    {
    uint8 de2_i;

    /* Configure M13 */
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhDs3FrmM13Chnl28Ds1s) == cAtOk);

    /* Access hierarchy */
    for (de2_i = 0; de2_i < AtPdhChannelNumberOfSubChannelsGet((AtPdhChannel)de3); de2_i++)
        {
        uint8 de1_i;
        AtPdhDe2 de2 = AtPdhDe3De2Get(de3, de2_i);
        for (de1_i = 0; de1_i < AtPdhChannelNumberOfSubChannelsGet((AtPdhChannel)de2); de2_i++)
            {
            AtPdhDe1 de1 = AtPdhDe3De1Get(de3, de2_i, de1_i);
            ShowDe1Status((AtChannel)de1);
            }
        }
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : bert.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : BERT example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDevice.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void De1BertExample(AtPdhDe1 de1);
void ShowBertCounters(AtPrbsEngine engine);
void BertStatusFlush(AtPrbsEngine engine);

/*--------------------------- Implementation ---------------------------------*/
void De1BertExample(AtPdhDe1 de1)
    {
    /* Create engine */
    AtDevice device = AtChannelDeviceGet((AtChannel)de1);
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    uint32 engineId = 0; /* Assume that this engine has not been used yet */
    AtPrbsEngine engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, de1);
    uint32 success = cAtOk;

    /* Can access this engine after creating as following */
    AtAssert(engine != NULL);
    AtAssert(AtPrbsEngineIdGet(engine) == engineId);
    AtAssert(AtModulePrbsEngineGet(prbsModule, engineId) == engine);

    /* Both these objects have reference to each other */
    AtAssert(AtPrbsEngineChannelGet(engine) == (AtChannel)de1);
    AtAssert(AtChannelPrbsEngineGet((AtChannel)de1) == engine);

    /* Generating and monitoring sides are set to TDM as default */
    AtAssert(AtPrbsEngineGeneratingSideGet(engine) == cAtPrbsSideTdm);
    AtAssert(AtPrbsEngineMonitoringSideGet(engine) == cAtPrbsSideTdm);

    /* Let's assume that this DE1 is being bound to a PW, application can change
     * these sides */
    if (AtChannelBoundPwGet((AtChannel)de1))
        {
        AtAssert(AtPrbsEngineGeneratingSideSet(engine, cAtPrbsSidePsn) == success);
        AtAssert(AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSidePsn) == success);
        }

    /* Configure inversion */
    AtAssert(AtPrbsEngineTxInvert(engine, cAtTrue) == success);
    AtAssert(AtPrbsEngineTxIsInverted(engine) == cAtTrue);
    AtAssert(AtPrbsEngineRxInvert(engine, cAtTrue) == success);
    AtAssert(AtPrbsEngineRxIsInverted(engine) == cAtTrue);

    /* Configure the PRBS mode */
    AtAssert(AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) == success);
    AtAssert(AtPrbsEngineTxModeGet(engine) == cAtPrbsModePrbs23);
    AtAssert(AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) == success);
    AtAssert(AtPrbsEngineRxModeGet(engine) == cAtPrbsModePrbs23);

    /* Then enable the engine */
    AtAssert(AtPrbsEngineRxEnable(engine, cAtTrue));
    AtAssert(AtPrbsEngineRxIsEnabled(engine) == cAtTrue);
    AtAssert(AtPrbsEngineTxEnable(engine, cAtTrue));
    AtAssert(AtPrbsEngineTxIsEnabled(engine) == cAtTrue);

    /* Can specify the error rate to force */
    AtAssert(AtPrbsEngineTxErrorRateSet(engine, cAtBerRate1E5) == success);
    AtAssert(AtPrbsEngineTxErrorRateGet(engine) == cAtBerRate1E5);

    /* Or stop forcing */
    AtAssert(AtPrbsEngineTxErrorRateSet(engine, cAtBerRateUnknown) == success);

    /* Can also query current error rate at RX side */
    AtPrintc(cSevInfo, "RX Error rate: %d\r\n", AtPrbsEngineRxErrorRateGet(engine));

    /* When PRBS is not used any more, application may want to disable and destroy
     * this engine to save resource as following */
    AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == success);
    AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == success);
    AtAssert(AtModulePrbsEngineGet(prbsModule, engineId) == NULL);

    /* All of associations would be invalid after this */
    AtAssert(AtPrbsEngineChannelGet(engine) == NULL);
    AtAssert(AtChannelPrbsEngineGet((AtChannel)de1) == NULL);
    }

void ShowBertCounters(AtPrbsEngine engine)
    {
    /* It is best practice to always latch counters before reading. */
    AtPrbsEngineAllCountersLatch(engine);

    /* Show counters directly on the console */
    AtPrintc(cSevNormal, "TxFrame            : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterTxFrame));
    AtPrintc(cSevNormal, "RxFrame            : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxFrame));
    AtPrintc(cSevNormal, "TxBit              : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterTxBit));
    AtPrintc(cSevNormal, "RxBit              : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxBit));
    AtPrintc(cSevNormal, "RxBitError         : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxBitError));
    AtPrintc(cSevNormal, "RxBitLastSync      : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxBitLastSync));
    AtPrintc(cSevNormal, "RxBitErrorLastSync : %d\r\n", AtPrbsEngineCounterGet(engine, cAtPrbsEngineCounterRxBitErrorLastSync));
    }

void BertStatusFlush(AtPrbsEngine engine)
    {
    AtPrbsEngineAllCountersLatch(engine);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterTxFrame);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxFrame);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterTxBit);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBit);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitError);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitLastSync);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitErrorLastSync);
    AtPrbsEngineAlarmHistoryClear(engine);
    }

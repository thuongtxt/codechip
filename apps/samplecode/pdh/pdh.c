/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : pdh.c
 *
 * Created Date: Apr 1, 2015
 *
 * Description : PDH example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void SerialLineExample(AtModulePdh pdhModule);

/*--------------------------- Implementation ---------------------------------*/
void SerialLineExample(AtModulePdh pdhModule)
    {
    AtPdhSerialLine serialLine;

    /* There must be 48 Serial Lines */
    AtAssert(AtModulePdhNumberOfDe3SerialLinesGet(pdhModule) == 48);

    /* Any of Serial Line can be accessed */
    serialLine = AtModulePdhDe3SerialLineGet(pdhModule, 3);
    AtAssert(serialLine != NULL);

    /* Default working mode must be valid */
    AtAssert(AtPdhSerialLineModeGet(serialLine) != cAtPdhDe3SerialLineModeUnknown);

    /* And application can change working mode to EC-1 */
    AtAssert(AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeEc1) == cAtOk);
    AtAssert(AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeEc1);

    /* Or it can change to DS3 */
    AtAssert(AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeDs3) == cAtOk);
    AtAssert(AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeDs3);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : nxds0.c
 *
 * Created Date: Feb 3, 2015
 *
 * Description : NxDS0 example code.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPdhDe1.h"
#include "AtPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void NxDs0Example(AtPdhDe1 de1);

/*--------------------------- Implementation ---------------------------------*/
void NxDs0Example(AtPdhDe1 de1)
    {
    AtIterator nxDs0Iterator;
    AtPdhNxDS0 nxDs0;

    /* If DS1/E1 framing mode is unframe, NxDS0 group cannot be created. Try
     * creating NxDS group with timeslot 1 and 2 (timeslot ID starts from 0) and
     * NULL must be returned */
    if ((AtPdhChannelFrameTypeGet((AtPdhChannel)de1) == cAtPdhDs1J1UnFrm) ||
        (AtPdhChannelFrameTypeGet((AtPdhChannel)de1) == cAtPdhE1UnFrm))
        AtAssert(AtPdhDe1NxDs0Create(de1, cBit2_1) == NULL);

    /* Need to configure the framer of DS1/E1 to framed so that DS0 timeslots
     * can be accessed. Then NxDS0 group can be created. The following code creates
     * two groups:
     * - Group 1: with timeslot 1 and 2
     * - Group 2: with timeslot 3 to 9 */
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmEsf) == cAtOk);
    AtAssert(AtPdhDe1NxDs0Create(de1, cBit2_1) != NULL);
    AtAssert(AtPdhDe1NxDs0Create(de1, cBit9_3) != NULL);

    /* After creating, NxDS0 object can be access via bit mask, such as following */
    AtAssert(AtPdhDe1NxDs0Get(de1, cBit2_1) != NULL);
    AtAssert(AtPdhDe1NxDs0Get(de1, cBit9_3) != NULL);

    /* Try to access NxDs0 groups that have not been created will have NULL object */
    AtAssert(AtPdhDe1NxDs0Get(de1, cBit12_10) == NULL);
    AtAssert(AtPdhDe1NxDs0Get(de1, cBit3_2)   == NULL);

    /* All of created groups can be iterated by using iterator. The following
     * code just simply iterates all of created nxDS0 and print their mask */
    nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate(de1);
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator)) != NULL)
        AtPrintf("NxDs0 mask: 0x%08x\r\n", AtPdhNxDS0BitmapGet(nxDs0));
    AtObjectDelete((AtObject)nxDs0Iterator);

    /* NxDS0 groups can then be deleted as following */
    AtAssert(AtPdhDe1NxDs0Delete(de1, AtPdhDe1NxDs0Get(de1, cBit2_1)) == cAtOk);
    AtAssert(AtPdhDe1NxDs0Delete(de1, AtPdhDe1NxDs0Get(de1, cBit9_3)) == cAtOk);
    }

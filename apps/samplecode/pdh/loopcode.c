/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : loopcode.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : Loopcode example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void InterruptDe1DeviceSetup(AtDevice device);
void *InterruptThreadHandler(void *data);
void *PeriodicThreadHandler(void *data);
void PeriodicExit(void);
void InterruptThreadExit(void);

void InterruptDe3DeviceSetup(AtDevice device);
eAtRet BomSend(AtChannel channel, uint8 bom);
eAtRet InbandLoopcodeStartSending(AtChannel channel, eAtPdhDe1Loopcode loopCode);
eAtRet InbandLoopcodeStopSending(AtChannel channel);
eAtRet FEACLoopActiveSend(AtChannel channel, uint8 linePattern);
/*--------------------------- Implementation ---------------------------------*/
/*
Table 10-13. ESF Data Link Unscheduled Messages
GR-499-CORE TSGR: Common Requirements
Issue 2, December 1998 Signal Formats

Function Messagea
Priority Messages
RAI 00000000 11111111
Loopback Retention and acknowledge 00101010 11111111
RAI-CI 00111110 11111111

Function Messagea
Command and Response Messages
Line Loopback Activate 00001110 11111111
Line Loopback Deactivate 00111000 11111111
Payload Loopback Activate 00010100 11111111
Payload Loopback Deactivate 00110010 11111111
Reserved for Network Use ( Loopback Activate) 00010010 11111111
Universal Loopback (Deactivate) 00100100 11111111
ISDN Line Loopback (NT2) 00101110 11111111
CI/CSU Line Loopback (NT1) 00100000 11111111
For network use (indication of NT1 power off) 00011100 11111111
Protection Switch Line 1b 01000010 11111111
Protection Switch Line 2 01000100 11111111
Protection Switch Line 3 01000110 11111111
Protection Switch Line 4 01001000 11111111
Protection Switch Line 5 01001010 11111111
Protection Switch Line 6 01001100 11111111
Protection Switch Line 7 01001110 11111111
Protection Switch Line 8 01010000 11111111
Protection Switch Line 9 01010010 11111111
Protection Switch Line 10 01010100 11111111
Protection Switch Line 11 01010110 11111111
Protection Switch Line 12 01011000 11111111
Protection Switch Line 13 01011010 11111111
Protection Switch Line 14 01011100 11111111
Protection Switch Line 15 01011110 11111111
Protection Switch Line 16 01100000 11111111
Protection Switch Line 17 01100010 11111111
Protection Switch Line 18 01100100 11111111
Protection Switch Line 19 01100110 11111111
Protection Switch Line 20 01101000 11111111
Protection Switch Line 21 01101010 11111111
Protection Switch Line 22 01101100 11111111
Protection Switch Line 23 01101110 11111111
Protection Switch Line 24 01110000 11111111
Protection Switch Line 25 01110010 11111111
Protection Switch Line 26 01110100 11111111
Protection Switch Line 27 01110110 11111111
Protection Switch Acknowledge 00011000 11111111
Protection Switch Release 00100110 11111111
Do Not for Synchronization 00110000 11111111
Stratum 2 Traceable 00001100 11111111
SONET Minimum Clock Traceable 00100010 11111111
Stratum 4 Traceable 00101000 11111111
Stratum 1 Traceable 00000100 11111111
Synchronization Traceability Unknown 00001000 11111111
Stratum 3 Traceable 00010000 11111111
Reserved for Network Synchronization 01000000 11111111
Transmit Node Clock (TNC) 01111000 11111111
Stratum 3E Traceable 01111100 11111111
Under study for maintenance 00101100 11111111
Under study for maintenance 00110100 11111111
Reserved for network use 00010110 11111111
Reserved for network use 00011010 11111111
Reserved for network use 00011110 11111111
Reserved for network use 00111010 11111111
Reserved for customer 00000110 11111111
Reserved for customer 00001010 11111111
Reserved for customer 00000010 11111111
Reserved for customer 00110110 11111111
Notes:
a. The right-most bit is transmitted first.
b. The �Protection Switch Line� codes of the form 01XXXXX0 11111111 use the five X-bits to
indicate the number of the line, 1 through 27, to be switched to a protection line.
*/
/*BOM binary pattern: 0abcdef011111111. The following is fedcba value*/
#define cHwTxOutBandLineLoopUp        0x38
#define cHwTxOutBandLineLoopDown      0x0E
#define cHwTxOutBandPayloadLoopUp     0x14
#define cHwTxOutBandPayloadLoopDown   0x26
#define cHwTxOutBandNetWorkLoopUp     0x24
#define cHwTxOutBandNetWorkLoopDown   0x12
/* TX BOM send */
eAtRet BomSend(AtChannel channel, uint8 bom)
    {
    return AtPdhDe1TxBomWithModeSet((AtPdhDe1)channel, bom, cAtPdhBomSentModeContinuous);
    }

/* RX BOM receive handler */
static void OnBomChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint32 loopcode;

    AtUnused(currentStatus);

    /* Only care BOM change event */
    if ((changedAlarms & cAtPdhDs1AlarmBomChange) == 0)
        return;

    loopcode = AtPdhDe1BomReceive((AtPdhDe1) channel);
    switch (loopcode)
        {
        /* Active remote line loop */
        case cHwTxOutBandLineLoopUp:
            AtChannelLoopbackSet(channel, cAtPdhLoopbackModeRemoteLine);
            break;

        /* Active remote payload loop */
        case cHwTxOutBandPayloadLoopUp:
            AtChannelLoopbackSet(channel, cAtPdhLoopbackModeRemotePayload);
            break;

        /* Or can release the loopback */
        case cHwTxOutBandLineLoopDown:
        case cHwTxOutBandPayloadLoopDown:
            AtChannelLoopbackSet(channel, cAtPdhLoopbackModeRelease);
            break;

        default:
            break;
        }
    }

/* TX inband loopcode send */
eAtRet InbandLoopcodeStartSending(AtChannel channel, eAtPdhDe1Loopcode loopCode)
    {
    eAtRet ret = AtPdhDe1LoopcodeEnable((AtPdhDe1)channel, cAtTrue);
    if (ret != cAtOk)
        return ret;
    ret = AtPdhDe1TxLoopcodeSet((AtPdhDe1)channel, loopCode);
    return ret;
    }

eAtRet InbandLoopcodeStopSending(AtChannel channel)
    {
    return AtPdhDe1TxLoopcodeSet((AtPdhDe1)channel, cAtPdhDe1LoopcodeInbandDisable);
    }

/* RX inband loopcode handler */
static void OnInbandLoopcodeChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
	uint32 loopcode;

    AtUnused(currentStatus);

    /* Only care BOM change event */
    if ((changedAlarms & cAtPdhDs1AlarmInbandLoopCodeChange) == 0)
        return;

    loopcode = AtPdhDe1LoopcodeReceive((AtPdhDe1) channel);
    switch (loopcode)
        {
        /* Active remote line loop */
        case cAtPdhDe1LoopcodeLineActivate:
            AtChannelLoopbackSet(channel, cAtPdhLoopbackModeRemoteLine);
            break;

        /* Or can release the loopback */
        case cAtPdhDe1LoopcodeLineDeactivate:
            AtChannelLoopbackSet(channel, cAtPdhLoopbackModeRelease);
            break;

        default:
            break;
        }
    }

/* Register the handler to a de1 object */
static eAtRet De1Listen(AtPdhDe1 de1)
    {
	eAtRet ret = cAtOk;
    static tAtChannelEventListener bomListener = {.AlarmChangeState = OnBomChanged, .OamReceived = NULL};
    static tAtChannelEventListener inbandLoopcodeListener = {.AlarmChangeState = OnInbandLoopcodeChanged, .OamReceived = NULL};
    ret = AtChannelEventListenerAdd((AtChannel) de1, &bomListener);
    if (ret == cAtOk)
    	ret = AtChannelEventListenerAdd((AtChannel) de1, &inbandLoopcodeListener);
    return ret;
    }

static void DeviceInterruptEnable(AtDevice device, eBool enable)
	{
    AtIterator coreIterator = AtDeviceCoreIteratorCreate(device);
    AtIpCore core;
    eAtRet ret = cAtOk;

    while ((core = (AtIpCore)AtIteratorNext(coreIterator)) != NULL)
        ret |= AtIpCoreInterruptEnable(core, enable);

    AtObjectDelete((AtObject)coreIterator);

	}

/* Enable interrupt */
void InterruptDe1DeviceSetup(AtDevice device)
	{
	AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
	uint32 id, numDe1 = AtModulePdhNumberOfDe1sGet(pdhModule);
	AtPdhDe1 de1 = NULL;
	uint32 interruptmask = 0;

	DeviceInterruptEnable(device, cAtTrue);
	AtModuleInterruptEnable((AtModule)pdhModule, cAtTrue);
	for (id = 0; id < numDe1; id++)
		{
		de1 = AtModulePdhDe1Get(pdhModule, id);
		De1Listen(de1);
		/* enable interrupt for rx BOM and rx inband loopcode */
		interruptmask = cAtPdhDs1AlarmBomChange | cAtPdhDs1AlarmInbandLoopCodeChange;
		AtChannelInterruptMaskSet((AtChannel)de1, interruptmask, interruptmask);
		AtPdhDe1LoopcodeEnable(de1, cAtTrue);/* this function check framing mode if it is ds1 then it accept.
		changing frame mode from ds1 to E1 the loopcode function is automatically disable
		but from e1 to ds1, this function should be called explicitly */
		}
	}

/*Install interrupt processing to scan defects and events: this function is either called every times CPU got interrupt
 * from the device or periodically called*/
static AtTask m_interruptTask = NULL;
static eBool m_InterruptThreadExit = cAtFalse;

void InterruptThreadExit(void)
    {
    m_InterruptThreadExit = cAtTrue;
    }

void *InterruptThreadHandler(void *pData)
    {
	AtDevice device = (AtDevice)pData;
    static const uint32 taskPeriodInMs = 100;
    while (1)
        {
        AtTaskTestCancel(m_interruptTask);
        if (m_InterruptThreadExit)
        	break;
        /* Interrupt processing */
        AtDeviceInterruptProcess(device);
        AtOsalUSleep(taskPeriodInMs * 1000);
        }

    return NULL;
    }

/*For inband ds1 loopcode, it take for atleast 5s, so the PDH module periodic must be called periodically,
 * if periodic is 0.1s then inband loopcode detection time precision is 5s +/- 0.1s */
static AtTask m_periodicTask = NULL;
static eBool m_periodicExit = cAtFalse;

void PeriodicExit(void)
    {
    m_periodicExit = cAtTrue;
    }

void *PeriodicThreadHandler(void *pData)
    {
	AtDevice device = (AtDevice)pData;
    static const uint32 taskPeriodInMs = 100;
    AtModule pdhModule = AtDeviceModuleGet(device, cAtModulePdh);
    while (1)
        {
        AtTaskTestCancel(m_periodicTask);
        if (m_periodicExit)
            break;

        /* Interrupt processing */
        AtModulePeriodicProcess(pdhModule, taskPeriodInMs);
        AtOsalUSleep(taskPeriodInMs * 1000);
        }

    return NULL;
    }

/*DS3 FEAC*/
/*GR-499-CORE TSGR: Common Requirements
Issue 2, December 1998 Signal Formats

 Table 10-15. Assigned Alarm and Status FEAC Codewords
Alarm/Status Condition Codeword1
DS3 Equipment Failure (Service Affecting)2 0 011001 0 11111111
DS3 Loss of Signal (LOS) 0 001110 0 11111111
DS3 Out-of-Frame 0 000000 0 11111111
DS3 AIS Received 0 010110 0 11111111
DS3 Idle Signal Received 0 011010 0 11111111
DS3 Equipment Failure (Non-Service Affecting)3 0 001111 0 11111111
Common Equipment Failure (Non-Service Affecting)3 0 011101 0 11111111
Multiple DS1 LOS4 0 010101 0 11111111
DS1 Equipment Failure (Service Affecting)2, 4 0 000101 0 11111111
Single DS1 LOS4 0 011110 0 11111111
DS1 Equipment Failure (Non-Service Affecting)3, 4 0 000011 0 11111111
Notes:
1. Right-most bit transmitted first.
2. Service Affecting (SA) is a Type-I equipment failure. Type I is an out-of-service
state that indicates a defect requiring immediate attention.
3. Non-Service Affecting (NSA) is a Type-II equipment failure. Type II is an
equipment state such as suspended service, not activated, or not available for use.
4. For unchannelized applications, these codes are unassigned.

Table 10-16. Assigned Loopback Control Codewords
Function Codeword [Note 1]
Line Loopback Activate [Note 2] 0 000111 0 11111111
Line Loopback Deactivate [Note 2] 0 011100 0 11111111
DS3 Line 0 011011 0 11111111
DS1 Line Number 1 [Note 3, 4] 0 100001 0 11111111
DS1 Line Number 2 0 100010 0 11111111
DS1 Line Number 3 0 100011 0 11111111
DS1 Line Number 4 0 100100 0 11111111
DS1 Line Number 5 0 100101 0 11111111
DS1 Line Number 6 0 100110 0 11111111
DS1 Line Number 7 0 100111 0 11111111
DS1 Line Number 8 0 101000 0 11111111
DS1 Line Number 9 0 101001 0 11111111
DS1 Line Number 10 0 101010 0 11111111
DS1 Line Number 11 0 101011 0 11111111
DS1 Line Number 12 0 101100 0 11111111
DS1 Line Number 13 0 101101 0 11111111
DS1 Line Number 14 0 101110 0 11111111
DS1 Line Number 15 0 101111 0 11111111
DS1 Line Number 16 0 110000 0 11111111
DS1 Line Number 17 0 110001 0 11111111
DS1 Line Number 18 0 110010 0 11111111
DS1 Line Number 19 0 110011 0 11111111
DS1 Line Number 20 0 110100 0 11111111
DS1 Line Number 21 0 110101 0 11111111
DS1 Line Number 22 0 110110 0 11111111
DS1 Line Number 23 0 110111 0 11111111
DS1 Line Number 24 0 111000 0 11111111
DS1 Line Number 25 0 111001 0 11111111
DS1 Line Number 26 0 111010 0 11111111
DS1 Line Number 27 0 111011 0 11111111
DS1 Line Number 28 0 111100 0 11111111
DS1 Line - All 0 010011 0 11111111
Notes:
1. Right-most bit transmitted first.
2. To activate or deactivate loopbacks, the appropriate activate or deactivate
codeword is transmitted 10 times, followed immediately by 10 repetitions of the DS3
or DS1 line codeword. Thus, the total message is 40 octets long.
3. The DS1 line codewords (01xxxxxx011111111) use the bits labeled x to indicate
the number of the line (1 through 28) to be looped back.
4. For unchannelized DS3 applications, DS1s are unassigned.

 */

/* few lineId pattern for API input */
#define cDs3cbitFeacLoopbackLine 0x36 /*revert from the DS3 Line 6-bit-pattern (011011) above */
#define cDs3cbitFeacLoopbackDs1Line28 0x0F /*revert from the DS1 Line Number#28 6-bit-pattern (111100) above */

/* TX FEAC send loopcode */
eAtRet FEACLoopActiveSend(AtChannel channel, uint8 linePattern)
    {
    return AtPdhDe3TxFeacWithModeSet((AtPdhDe3)channel, cAtPdhDe3FeacTypeLoopActive, linePattern, cAtPdhBomSentModeOneShot);
    }

static uint8 FeacCodeRevert(uint8 rxFeacCode)
    {
    int ret = 0, i;
    for (i = 0; i < 6; i++)
	ret |= ((rxFeacCode >> (6-i)) & 1) << i;
    return (uint8)ret;
    }

static void FeacCodewordLoopAction(AtChannel channel, eAtPdhDe3FrameType frame, uint8 feacCode, eBool isLoop)
    {
    uint8 revertFeacCode = FeacCodeRevert(feacCode);

    if (feacCode == cDs3cbitFeacLoopbackLine)
	AtChannelLoopbackSet(channel, isLoop ? cAtPdhLoopbackModeRemoteLine : cAtPdhLoopbackModeRelease);
    else if (revertFeacCode > 0 && revertFeacCode < 29 && frame != cAtPdhDs3FrmCbitUnChn)
		{
		uint8 numDe1InDe2 = frame == cAtPdhDs3FrmCbitChnl28Ds1s ? 4 : 3;
		uint8 de2Id =  (uint8)((revertFeacCode-1)/numDe1InDe2), de1Id = (uint8)((revertFeacCode-1)%numDe1InDe2);
		AtPdhChannel de2 = AtPdhChannelSubChannelGet((AtPdhChannel)channel, de2Id);
		AtPdhChannel de1 = AtPdhChannelSubChannelGet((AtPdhChannel)de2, de1Id);
		AtChannelLoopbackSet((AtChannel)de1, isLoop ? cAtPdhLoopbackModeRemoteLine : cAtPdhLoopbackModeRelease);
		}
    }

static void FeacAlarmAction(AtChannel channel, uint8 feacCode)
    {
	AtUnused(channel);
	AtUnused(feacCode);
    /* action follows the alarm table comments above */
    }

static void OnFeacChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    eAtPdhDe3FeacType  feacType = 0;
    uint8 feacCode = 0;
    eAtPdhDe3FrameType frame = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);

    AtUnused(currentStatus);
    if ((changedAlarms & cAtPdhDs3AlarmFeacChange) == 0)
    	return;
    if (!AtPdhDe3IsDs3CbitFrameType(frame))
    	return;

    AtPdhDe3RxFeacGet((AtPdhDe3)channel, &feacType, &feacCode);
    AtPrintc(cSevNormal,
		 "\r\n De3: %s.%s detect control = %d, new loopcode = %d\r\n",
		 AtChannelTypeString(channel), AtChannelIdString(channel), feacType, feacCode);
    if (feacType == cAtPdhDe3FeacTypeLoopActive)
		{
		FeacCodewordLoopAction(channel, frame, feacCode, cAtTrue);
		}
    else if (feacType == cAtPdhDe3FeacTypeLoopDeactive)
		{
		FeacCodewordLoopAction(channel, frame, feacCode, cAtFalse);
		}
    else if (feacType == cAtPdhDe3FeacTypeAlarm)
		{
		FeacAlarmAction(channel, feacCode);
		}
    }

static void De3Listen(AtPdhDe3 de3)
    {
    static tAtChannelEventListener feacListener = {.AlarmChangeState = OnFeacChanged, .OamReceived = NULL};
    AtChannelEventListenerAdd((AtChannel) de3, &feacListener);
    }

/* Enable interrupt */
void InterruptDe3DeviceSetup(AtDevice device)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint32 id, numDe3 = AtModulePdhNumberOfDe3sGet(pdhModule);
    AtPdhDe3 de3 = NULL;
    uint32 interruptmask = 0;

    DeviceInterruptEnable(device, cAtTrue);
    AtModuleInterruptEnable((AtModule)pdhModule, cAtTrue);
    for (id = 0; id < numDe3; id++)
        {
        de3 = AtModulePdhDe3Get(pdhModule, id);
        De3Listen(de3);
        /* enable interrupt for rx FEAC */
        interruptmask = cAtPdhDs3AlarmFeacChange;
        AtChannelInterruptMaskSet((AtChannel)de3, interruptmask, interruptmask);
        }
    }

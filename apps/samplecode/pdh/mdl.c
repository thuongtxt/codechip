/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : mdl.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : Maintenance Data link example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#define sEicDefault             "Eic"
#define sLicDefault             "Lic"
#define sFicDefault             "Fic"
#define sUinitDefault           "Unit"
#define sPfiDefault             "Pfi"
#define sPortDefault            "Port"
#define sGenDefault             "Gen"


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef uint16 (*DataElementGet)(AtPdhMdlController , eAtPdhMdlDataElement, uint8 *, uint16);
typedef uint32 (*CounterFuncGet)(AtPdhMdlController, uint32);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Ds3MdlSetUpExample(AtPdhDe3 de3);
void MdlControllerElementDisplay(AtPdhDe3 de3, eAtPdhMdlControllerType type, eBool txDir);
void De3MdlControllerCountersDisplay(AtPdhDe3 de3, eAtPdhMdlControllerType type, eBool r2c);

/*--------------------------- Implementation ---------------------------------*/
static const char *MsgTypeString(uint32 type)
    {
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage:    return "path";
        case cAtPdhMdlControllerTypeIdleSignal:     return "idle-signal";
        case cAtPdhMdlControllerTypeTestSignal:     return "test-signal";
        default :                                   return "unknown";
        }
    }

static AtPdhMdlController MdlControllerGet(AtPdhDe3 de3, uint32 type)
    {
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage:    return AtPdhDe3PathMessageMdlController(de3);
        case cAtPdhMdlControllerTypeIdleSignal:     return AtPdhDe3IdleSignalMdlController(de3);
        case cAtPdhMdlControllerTypeTestSignal:     return AtPdhDe3TestSignalMdlController(de3);
        default :                                   return NULL;
        }
    }

static uint32 LastDataElementValueGet(AtPdhMdlController controller)
    {
    uint32 type = AtPdhMdlControllerTypeGet(controller);
    switch(type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAtPdhMdlDataElementPfi;
        case cAtPdhMdlControllerTypeIdleSignal : return cAtPdhMdlDataElementIdleSignalPortNumber;
        case cAtPdhMdlControllerTypeTestSignal : return cAtPdhMdlDataElementTestSignalGeneratorNumber;
        default: return cBit31_0;
        }
    }

static const char* LastDataElemenStringGet(AtPdhMdlController controller)
    {
    uint32 type = AtPdhMdlControllerTypeGet(controller);
    switch(type)
        {
        case cAtPdhMdlControllerTypePathMessage:    return "PFI";
        case cAtPdhMdlControllerTypeIdleSignal :    return "PORT";
        case cAtPdhMdlControllerTypeTestSignal :    return "GEN";
        default:                                    return NULL;
        }
    }

static uint16 ElementSizeGet(uint32 element)
    {
    switch(element)
        {
        case cAtPdhMdlDataElementEic:       return cMdlEICStringSize;
        case cAtPdhMdlDataElementLic:       return cMdlLICStringSize;
        case cAtPdhMdlDataElementFic:       return cMdlFICStringSize;
        case cAtPdhMdlDataElementUnit:      return cMdlUINTStringSize;
        default:                            return cMdlPortPfiGenStringSize;
        }
    }

static char* ElementStringGet(AtPdhMdlController controller,
                              DataElementGet dataElementGet,
                              eAtPdhMdlDataElement element)
    {
    static uint8 buffer[64];
    static char elementStrs[64];

    AtOsalMemInit(buffer, 0, sizeof(buffer));
    AtOsalMemInit(elementStrs, 0, sizeof(elementStrs));
    dataElementGet(controller, element, buffer, ElementSizeGet(element));
    AtSprintf(elementStrs, "%s", buffer);
    return elementStrs;
    }

static const char* DirectionString(eBool txDir)
    {
    return txDir ? "Tx" : "RX";
    }

void MdlControllerElementDisplay(AtPdhDe3 de3, eAtPdhMdlControllerType type, eBool txDir)
    {
    DataElementGet dataElementGet = (txDir) ?
            AtPdhMdlControllerTxDataElementGet:AtPdhMdlControllerRxDataElementGet;
    AtPdhMdlController controller = MdlControllerGet(de3, type);

    AtPrintc(cSevInfo, " %s.EIC       : %s \r\n",
             DirectionString(txDir),
             ElementStringGet(controller, dataElementGet, cAtPdhMdlDataElementEic));

    AtPrintc(cSevInfo, " %s.LIC       : %s \r\n",
             DirectionString(txDir),
             ElementStringGet(controller, dataElementGet, cAtPdhMdlDataElementLic));

    AtPrintc(cSevInfo, " %s.FIC       : %s \r\n",
             DirectionString(txDir),
             ElementStringGet(controller, dataElementGet, cAtPdhMdlDataElementFic));

    AtPrintc(cSevInfo, " %s.UNIT      : %s \r\n",
             DirectionString(txDir),
             ElementStringGet(controller, dataElementGet, cAtPdhMdlDataElementUnit));

    AtPrintc(cSevInfo, " %s.%s   : %s \r\n",
             DirectionString(txDir),
             LastDataElemenStringGet(controller),
             ElementStringGet(controller, dataElementGet, LastDataElementValueGet(controller)));
    }

static void OnMdlChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    AtPdhMdlController controller;
    AtUnused(currentStatus);

    /* Just care MDL change event */
    if ((changedAlarms & cAtPdhDs3AlarmMdlChange) == 0)
        return;

    /* Get current controller have events */
    controller = AtPdhDe3RxCurrentMdlControllerGet((AtPdhDe3)channel);
    if (controller == NULL)
        {
        AtPrintc(cSevCritical, "\r\n MDL can not get controller at %s.%s\r\n",
                 AtChannelTypeString(channel),
                 AtChannelIdString(channel));
        return;
        }

    /* Show the channel that receives this message */
    AtPrintc(cSevNormal, "\r\nNewMDLMsg is received at %s.%s controller=%s\r\n",
             AtChannelTypeString(channel),
             AtChannelIdString(channel),
             MsgTypeString(AtPdhMdlControllerTypeGet(controller)));

    /* Display each Elememt of Rx MDL message */
    MdlControllerElementDisplay((AtPdhDe3)channel, AtPdhMdlControllerTypeGet(controller), cAtFalse);
    }

static tAtChannelEventListener *MdlListener(void)
    {
    static tAtChannelEventListener ds3MdlListener;
    AtOsalMemInit(&ds3MdlListener, 0, sizeof(ds3MdlListener));
    ds3MdlListener.AlarmChangeState = OnMdlChanged;
    return &ds3MdlListener;
    }

static uint8* ElementDefaultGet(uint32 element)
    {
    static uint8 txBuffer[64];
    AtOsalMemInit(txBuffer, 0, sizeof(txBuffer));
    switch(element)
        {
        case cAtPdhMdlDataElementEic:
            AtOsalMemCpy(txBuffer, sEicDefault, AtStrlen(sEicDefault));
            break;
        case cAtPdhMdlDataElementLic:
            AtOsalMemCpy(txBuffer, sLicDefault, AtStrlen(sLicDefault));
            break;
        case cAtPdhMdlDataElementFic:
            AtOsalMemCpy(txBuffer, sFicDefault, AtStrlen(sFicDefault));
            break;
        case cAtPdhMdlDataElementUnit:
            AtOsalMemCpy(txBuffer, sUinitDefault, AtStrlen(sUinitDefault));
            break;
        case cAtPdhMdlDataElementPfi:
            AtOsalMemCpy(txBuffer, sPfiDefault, AtStrlen(sPfiDefault));
            break;
        case cAtPdhMdlDataElementIdleSignalPortNumber:
            AtOsalMemCpy(txBuffer, sPortDefault, AtStrlen(sPortDefault));
            break;
        case cAtPdhMdlDataElementTestSignalGeneratorNumber:
            AtOsalMemCpy(txBuffer, sGenDefault, AtStrlen(sGenDefault));
            break;
        default:
            break;
        }
    return txBuffer;
    }

void Ds3MdlSetUpExample(AtPdhDe3 de3)
    {
    eAtPdhMdlControllerType type;
    AtChannel channel = (AtChannel)de3;
    AtPdhChannel pdhChannel = (AtPdhChannel)de3;

    /* Enable data link Before enable each MDL Type Controller,
     * Datalink can be enabled for DS3-Cbit framing type for Un-Channelize and Channelize */
    AtAssert(AtPdhChannelDataLinkEnable(pdhChannel, cAtTrue) == cAtOk);
    AtAssert(AtPdhChannelDataLinkIsEnabled(pdhChannel) == cAtTrue);


    for (type = cAtPdhMdlControllerTypePathMessage; type <= cAtPdhMdlControllerTypeTestSignal; type++)
        {
        eAtRet ret;
        uint32 element;
        AtPdhMdlController controller = MdlControllerGet(de3, type);

        /* Default enable for MDL Controller */
        ret = AtPdhMdlControllerEnable(controller, cAtTrue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "\r\n MDL can not enable controller=%s at %s.%s\r\n",
                     MsgTypeString(AtPdhMdlControllerTypeGet(controller)),
                     AtChannelTypeString(channel),
                     AtChannelIdString(channel));
            return;
            }

        /* Default Tx Data Element for MDL Controller */
        element = cAtPdhMdlDataElementEic;
        ret |= AtPdhMdlControllerTxDataElementSet(controller, element, ElementDefaultGet(element), ElementSizeGet(element));

        element = cAtPdhMdlDataElementLic;
        ret |= AtPdhMdlControllerTxDataElementSet(controller, element, ElementDefaultGet(element), ElementSizeGet(element));

        element = cAtPdhMdlDataElementFic;
        ret |= AtPdhMdlControllerTxDataElementSet(controller, element, ElementDefaultGet(element), ElementSizeGet(element));

        element = cAtPdhMdlDataElementUnit;
        ret |= AtPdhMdlControllerTxDataElementSet(controller, element, ElementDefaultGet(element), ElementSizeGet(element));

        element = LastDataElementValueGet(controller);
        ret |= AtPdhMdlControllerTxDataElementSet(controller, element, ElementDefaultGet(element), ElementSizeGet(element));
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "\r\n MDL can not enable controller=%s at %s.%s\r\n",
                     MsgTypeString(AtPdhMdlControllerTypeGet(controller)),
                     AtChannelTypeString(channel),
                     AtChannelIdString(channel));
            return;
            }
        }

    /* Register listener for new MDL, per DS3 Channel */
    AtAssert(AtChannelEventListenerAdd((AtChannel)de3, MdlListener()) == cAtOk);
    }

void De3MdlControllerCountersDisplay(AtPdhDe3 de3, eAtPdhMdlControllerType type, eBool r2c)
    {
    CounterFuncGet counterGet = (r2c) ? AtPdhMdlControllerCountersClear:AtPdhMdlControllerCountersGet;
    AtPdhMdlController controller = MdlControllerGet(de3, type);

    AtPrintc(cSevInfo, " TxPkt       : %d \r\n",
             counterGet(controller, cAtPdhMdlControllerCounterTypeTxPkts));

    AtPrintc(cSevInfo, " TxBytes     : %d \r\n",
             counterGet(controller, cAtPdhMdlControllerCounterTypeTxBytes));

    AtPrintc(cSevInfo, " RxPkt       : %d \r\n",
             counterGet(controller, cAtPdhMdlControllerCounterTypeRxPkts));

    AtPrintc(cSevInfo, " RxBytes     : %d \r\n",
             counterGet(controller, cAtPdhMdlControllerCounterTypeRxBytes));

    AtPrintc(cSevInfo, " RxDropPkt   : %d \r\n",
             counterGet(controller, cAtPdhMdlControllerCounterTypeRxDropPkts));
    }

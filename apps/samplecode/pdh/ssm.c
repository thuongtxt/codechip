/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SSM sample code
 *
 * File        : ssm.c
 *
 * Created Date: Dec 21, 2015
 *
 * Description : This files provide SSM sample code for T1-ESF/E1-CRC, E3G832/T3-CBIT
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eAtRet E1CrcSsmSetup(AtPdhDe1 self);
eAtRet E1CrcSsmListen(AtPdhDe1 de1);
eAtRet De3SsmSetup(AtChannel self);
eAtRet De3SsmListen(AtPdhDe3 de3);

/*--------------------------- Implementation ---------------------------------*/
/*
Table 5D/G.704 gives the numbering of the San(n = 4, 5, 6, 7, 8) bits. A Sanbit is organized as a 4-bit nibble
San1to San4. San1is the most significant bit, San4is the least significant bit.
NOTE The message set in San1to San4is a copy of the set defined in SDH bits 5 to 8 of byte S1.

Table 5C/G.704  Synchronization Status Message (SSM) bit allocation for 2048 kbit/s
QL San1, San2, San3, San4 (Note1)Synchronization Quality Level (QL) description
0     0000                        Quality unknown (existing synchronization network)
1     0001                        Reserved
2     0010                        Rec. G.811
3     0011                        Reserved
4     0100                        SSU-A (Note 2)
5     0101                        Reserved
6     0110                        Reserved
7     0111                        Reserved
8     1000                        SSU-B (Note 2)
9     1001                        Reserved
10    1010                        Reserved
11    1011                        Synchronous Equipment Timing Source (SETS)
12    1100                        Reserved
13    1101                        Reserved
14    1110                        Reserved
15    1111                        Do not use for synchronization
NOTE 1 n = 4, 5, 6, 7 or 8 (i.e. one Sabit only) depending on operator selection.
NOTE 2  In a previous version of this Recommendation, the terms "G.812 transit" and "G.812 local"
were used. These terms were changed to Synchronization Supply Unit (SSU), respectively type A and
type B, to align with the clock definitions in Recommendation G.812.
*/
eAtRet E1CrcSsmSetup(AtPdhDe1 self)
    {
    eAtRet ret = cAtOk;
    uint8 level = 0;

    AtPdhChannel pdhSelf = (AtPdhChannel)self;
    /*Enalbe Slip buffer*/
    ret = AtPdhChannelTxSlipBufferEnable(pdhSelf, cAtTrue);
    if (ret != cAtOk)
        return ret;
    /*Enable SSM*/
    ret = AtPdhChannelSsmEnable(pdhSelf, cAtTrue);
    if (ret != cAtOk)
        return ret;

    ret = AtPdhDe1SsmSaBitsMaskSet((AtPdhDe1)self, cAtPdhE1Sa4);
    if (ret != cAtOk)
        return ret;

    ret = AtPdhChannelTxSsmSet(pdhSelf, level);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

/* RX Ssm receive handler */
static void OnE1CrcSsmChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 ssm;

    AtUnused(currentStatus);

    /* Only care BOM change event */
    if ((changedAlarms & cAtPdhE1AlarmSsmChange) == 0)
        return;

    ssm = AtPdhChannelRxSsmGet((AtPdhChannel) channel);
    AtPrintc(cSevInfo, "%s got RX ssm = %d\n", AtObjectToString((AtObject)channel), ssm);
    }

eAtRet E1CrcSsmListen(AtPdhDe1 de1)
    {
    eAtRet ret = cAtOk;
    static tAtChannelEventListener e1crcSsmListener = {.AlarmChangeState = OnE1CrcSsmChanged, .OamReceived = NULL};
    ret = AtChannelEventListenerAdd((AtChannel) de1, &e1crcSsmListener);

    return ret;
    }

/*SSM for T3 CBIT is carried over UDL. E3G832 carry SSM over MA Timing Marker bit*/
eAtRet De3SsmSetup(AtChannel self)
    {
    eAtRet ret = cAtOk;
    uint8 level = 0;

    AtPdhChannel pdhSelf = (AtPdhChannel)self;
    /*Enalbe Slip buffer*/
    ret = AtPdhChannelTxSlipBufferEnable(pdhSelf, cAtTrue);
    if (ret != cAtOk)
        return ret;

    /*Enable SSM*/
    ret = AtPdhChannelSsmEnable(pdhSelf, cAtTrue);
    if (ret != cAtOk)
        return ret;

    ret = AtPdhChannelTxSsmSet(pdhSelf, level);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

/* RX Ssm receive handler */
static void OnDe3SsmChanged(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 ssm;

    AtUnused(currentStatus);

    /* Only care BOM change event */
    if ((changedAlarms & cAtPdhDe3AlarmSsmChange) == 0)
        return;

    ssm = AtPdhChannelRxSsmGet((AtPdhChannel) channel);
    AtPrintc(cSevInfo, "%s got RX ssm = %d\n", AtObjectToString((AtObject)channel), ssm);
    }

eAtRet De3SsmListen(AtPdhDe3 de3)
    {
    eAtRet ret = cAtOk;
    static tAtChannelEventListener de3SsmListener = {.AlarmChangeState = OnDe3SsmChanged, .OamReceived = NULL};
    ret = AtChannelEventListenerAdd((AtChannel) de3, &de3SsmListener);

    return ret;
    }


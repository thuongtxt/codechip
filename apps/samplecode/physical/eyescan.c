/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : eyescan.c
 *
 * Created Date: Jun 11, 2015
 *
 * Description : Example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtEthPort.h"
#include "AtDriver.h"
#include "AtEyeScanController.h"
#include "AtEyeScanLane.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void EyeScan(AtEyeScanController controller);
void EthPortEyescan(void);

/*--------------------------- Implementation ---------------------------------*/
/* This callback will be called before eye-scan on a lane is started. Use this
 * callback to display lane information */
static void EyeScanWillStart(AtEyeScanLane lane, void *userData)
    {
    uint32 numTaps = AtEyeScanLaneNumHorizontalTaps(lane);
    float laneRateGbps = AtEyeScanLaneSpeedInGbps(lane);
    float timeTapPs = (float)((1000.0 / laneRateGbps) / (float)numTaps);

    AtUnused(userData);
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "================================================================================\r\n");
    AtPrintc(cSevInfo, "* %s: \r\n", AtObjectToString((AtObject)lane));
    AtPrintc(cSevInfo, "* Horizontal range: %d taps  (%.1f ps, %.4f GHz)\r\n", numTaps, (1000 / laneRateGbps), laneRateGbps);
    AtPrintc(cSevInfo, "  (1 tap = %.3f ps)\r\n", timeTapPs);
    AtPrintc(cSevInfo, "\r\n");
    }

static float TimeTapPs(AtEyeScanLane lane)
    {
    uint32 numTaps = AtEyeScanLaneNumHorizontalTaps(lane);
    float laneRateGbps = AtEyeScanLaneSpeedInGbps(lane);
    return (1000.0f / laneRateGbps) / (float)numTaps;
    }

/* This callback will be called when eye-scan finishes on a lane. Use this
 * callback to show measured information */
static void EyeScanDidFinish(AtEyeScanLane lane, void *userData)
    {
    uint32 measuredWidth = AtEyeScanLaneMeasuredWidth(lane);
    float time_tap_ps = TimeTapPs(lane);
    uint32 elapsedTimeMs = AtEyeScanLaneElapsedTimeMs(lane);

    AtUnused(userData);
    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "* Measured width: %d(%.1f ps)\r\n", measuredWidth, (float)measuredWidth * time_tap_ps);
    AtPrintc(cSevInfo, "* Measured swing: %d (%d mV)\r\n", AtEyeScanLaneMeasuredSwing(lane), AtEyeScanLaneMeasuredSwingInMilliVolt(lane));
    AtPrintc(cSevInfo, "* Elapsed time  : %.3f s\r\n", (float)elapsedTimeMs / 1000);
    AtPrintc(cSevInfo, "================================================================================\r\n");
    }

/* Visualize pixel by its number of errors */
static const char *CharacterForError(float numErrors, float maxErrors)
    {
    if ((uint32)numErrors == 0)
        return "_";
    if ((uint32)numErrors == (uint32)maxErrors)
        return "X";
    return "x";
    }

/* This callback is called when a pixel is finished scanning. Use this callback
 * to visualize pixel or save to file. In this example, it is used to visualize
 * pixel directly on the console */
static void EyeScanDidScanPixel(AtEyeScanLane lane,
                                int32 verticalOffset, int32 horizontalOffset,
                                uint32 numErrors, uint32 maxNumErrors, float ber, void *userData)
    {
    AtUnused(lane);
    AtUnused(horizontalOffset);
    AtUnused(verticalOffset);
    AtUnused(ber);
    AtUnused(userData);
    AtPrintf("%s", CharacterForError((float)numErrors, (float)maxNumErrors));
    }

/* This callback is called when eye-scan finishes on one row. It is used in
 * this example to start a new row */
static void EyeScanDidScanRow(AtEyeScanLane lane, int32 verticalOffset, void *userData)
    {
    AtUnused(lane);
    AtUnused(verticalOffset);
    AtUnused(userData);
    AtPrintf("\r\n");
    }

/* This callback is called before a row is scanned. In this example, it is used
 * to display vertical offset in millivolts */
static void EyeScanWillScanRow(AtEyeScanLane lane, int32 verticalOffset, void *userData)
    {
    int32 voltage = AtEyeScanLaneVerticalOffsetToMillivolt(lane, verticalOffset);
    AtUnused(userData);
    AtPrintc(cSevNormal, "(%4d mV)  ", voltage);
    }

static tAtEyeScanLaneListener *LaneListener(void)
    {
    static tAtEyeScanLaneListener m_listener;
    static tAtEyeScanLaneListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.EyeScanWillStart     = EyeScanWillStart;
    m_listener.EyeScanDidFinish     = EyeScanDidFinish;
    m_listener.EyeScanDidScanPixel  = EyeScanDidScanPixel;
    m_listener.EyeScanDidScanRow    = EyeScanDidScanRow;
    m_listener.EyeScanWillScanRow   = EyeScanWillScanRow;
    pListener = &m_listener;

    return pListener;
    }

/* Main function that start eye-scanning on the input controller */
void EyeScan(AtEyeScanController controller)
    {
    uint8 numLanes = AtEyeScanControllerNumLanes(controller);
    uint8 lane_i;
    eAtRet ret = cAtOk;

    /* Setup all lanes: setup listener, enable them */
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        /* Listen on eye-scan events and also enable eye-scan */
        AtEyeScanLane lane = (AtEyeScanLane)AtEyeScanControllerLaneGet(controller, lane_i);
        AtAssert(AtEyeScanLaneListenerAdd(lane, LaneListener(), NULL) == cAtOk);
        AtAssert(AtEyeScanLaneEnable(lane, cAtTrue) == cAtOk);
        }

    /* Now, start scanning */
    ret = AtEyeScanControllerScan(controller);

    /* It may be fail for some reasons:
     * - Measured width is small
     * - Measure swing is small.
     * - Eye-scan IP state machine fail.
     * Use logger to see detail failure */
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Eye-scan fail, see logs below: \r\n");
        AtLoggerShow(AtDriverLoggerGet(AtDriverSharedDriverGet()));
        }
    else
        AtPrintc(cSevInfo, "Eye-scan success\r\n");

    /* Application may want to stop listening or disable eye-scan on lanes by the
     * following example */
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtEyeScanControllerLaneGet(controller, lane_i);
        AtEyeScanLaneListenerRemove(lane, LaneListener(), NULL);
        AtAssert(AtEyeScanLaneEnable(lane, cAtFalse) == cAtOk);
        }
    }

void EthPortEyescan(void)
    {
    AtModuleEth ethModule = NULL;

    AtEthPort xaui = AtModuleEthPortGet(ethModule, 0); /* The first port */
    AtEyeScanController eyescanController = AtEthPortEyeScanControllerGet(xaui);
    EyeScan(eyescanController);
    }

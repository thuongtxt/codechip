/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : seu.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : SEU controller example
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtSemController.h"
#include "AtUart.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
const char* ebdfiles[] = {"/media/ram/file_1.ebd", "/media/ram/file_2.ebd", "/media/ram/file_3.ebd"};
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eAtRet SemSetup(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
/*
device interrupt enable
device sem interrupt enable
device sem interruptmask 1-3 idle|initialization|observation|injection|classification|correction|fatal-error|essential|correctable|uncorrectable en
device sem notification enable 1-3
 *
 */
static const char* SlrToEbdFilePath(uint32 slr)
    {
    if (slr < mCount(ebdfiles))
        return (const char*)ebdfiles[slr];

    return NULL;
    }

static void EssentialErrorInReportCheck(AtSemController self, const char *report, uint32 length)
    {
    eBool isEssential;
    isEssential = AtSemControllerErrorIsEssential(self, report, length);
    AtPrintc(cSevInfo, "Sem#%u: The error is %s\r\n", AtSemControllerIdGet(self) + 1, isEssential ? "essential" : "non-essential");
    }

static void* BufferAllocate(uint32 *size)
    {
    *size = 2048;
    return AtOsalMemAlloc(*size);
    }

static void BufferDeallocate(void *buf)
    {
    AtOsalMemFree(buf);
    }

static void UartErrorReportPrint(uint32 semIdx, const char *report, uint32 length)
    {
    uint32 i;
    AtPrintc(cSevInfo, "Sem#%u UART report:\r\n", semIdx + 1);
    for (i = 0; i < length; i++)
        {
        if (report[i] == '\r')
            AtPrintc(cSevInfo, "\r\n");
        else
            AtPrintc(cSevInfo, "%c", report[i]);
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static void CliAtDeviceUartInterruptHandler(AtSemController self, uint32 interruptStatus, uint32 currenAlarm)
    {
    uint32 size = 0;
    char *buffer = BufferAllocate(&size);
    uint32 bufferLength = size, uartReportLength;
    AtUart uart = AtSemControllerUartGet(self);

    if (interruptStatus & currenAlarm & cAtSemControllerAlarmErrorCorrectable)
        {
        uartReportLength = AtUartReceive(uart, buffer, bufferLength);
        UartErrorReportPrint(AtSemControllerIdGet(self), buffer, uartReportLength);
        EssentialErrorInReportCheck(self, buffer, uartReportLength);
        }

    if (interruptStatus & currenAlarm & cAtSemControllerAlarmErrorNoncorrectable)
        {
        uartReportLength = AtUartReceive(uart, buffer, bufferLength);
        UartErrorReportPrint(AtSemControllerIdGet(self), buffer, uartReportLength);
        }

    BufferDeallocate(buffer);
    }

static eAtRet DeviceAndSemInterruptEnable(AtDevice self)
    {
    AtIterator coreIterator = AtDeviceCoreIteratorCreate(self);
    AtIpCore core;
    eAtRet ret = cAtOk;

    while ((core = (AtIpCore)AtIteratorNext(coreIterator)) != NULL)
        {
        ret |= AtIpCoreInterruptEnable(core, cAtTrue);
        ret |= AtIpCoreSemInterruptEnable(core, cAtTrue);
        }

    AtObjectDelete((AtObject)coreIterator);
    return ret;
    }

static eAtRet SemsInterruptMaskEnable(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint32 numSem = AtDeviceNumSemControllersGet(self), i;
    uint32 alarmMask = cAtSemControllerAlarmInitialization   |
                       cAtSemControllerAlarmObservation      |
                       cAtSemControllerAlarmCorrection       |
                       cAtSemControllerAlarmClassification   |
                       cAtSemControllerAlarmInjection        |
                       cAtSemControllerAlarmIdle             |
                       cAtSemControllerAlarmFatalError       |
                       cAtSemControllerAlarmErrorEssential   |
                       cAtSemControllerAlarmErrorCorrectable |
                       cAtSemControllerAlarmErrorNoncorrectable |
                       cAtSemControllerAlarmHeartbeatSlr1       |
                       cAtSemControllerAlarmHeartbeatSlr2       |
                       cAtSemControllerAlarmActiveSlr1          |
                       cAtSemControllerAlarmActiveSlr2;

    for (i = 0; i < numSem; i++)
        {
        AtSemController sem = AtDeviceSemControllerGetByIndex(self, (uint8)i);
        if (sem != NULL)
            ret |= AtSemControllerInterruptMaskSet(sem, alarmMask, alarmMask);
        }
    return ret;
    }

static void SemAlarmChangeStateNotify(AtSemController controller,
                                      uint32 changedAlarms,
                                      uint32 currentAlarms,
                                      void *listener)
    {
    AtUart uart = AtSemControllerUartGet(controller);
    AtUnused(listener);

    if (uart != NULL)
        CliAtDeviceUartInterruptHandler(controller, changedAlarms, currentAlarms);
    }

static tAtSemControllerEventListener *Listener(void)
    {
    static tAtSemControllerEventListener listener;
    static tAtSemControllerEventListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.AlarmChangeStateNotify = SemAlarmChangeStateNotify;

    pListener = &listener;
    return pListener;
    }

static eBool NotificationEnable(AtDevice self, eBool enable)
    {
    eAtRet ret;
    eBool success = cAtTrue;
    uint32 numSemControllers = AtDeviceNumSemControllersGet(self);
    tAtSemControllerEventListener *listener = Listener();
    uint8 sem_i;


    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        AtSemController sem = AtDeviceSemControllerGetByIndex(self, sem_i);
        if (enable)
            ret = AtSemControllerEventListenerAdd(sem, listener, NULL);
        else
            ret = AtSemControllerEventListenerRemove(sem, listener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Listener %s fail on %s, ret = %s\r\n",
                     enable ? "add" : "remove",
                     AtObjectToString((AtObject)sem),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eAtRet EbdFilePathsSet(AtDevice self)
    {
    uint32 sem_i, numSemControllers = AtDeviceNumSemControllersGet(self);
    eAtRet ret = cAtOk;

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        AtSemController sem = AtDeviceSemControllerGetByIndex(self, (uint8)sem_i);
        uint8 slr, numSlr = AtSemControllerNumSlr(sem);
        for (slr = 0; slr < numSlr; slr++)
            ret |= AtSemControllerEbdFilePathSet(sem, slr, SlrToEbdFilePath(slr+sem_i));
        }
    return ret;
    }

eAtRet SemSetup(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= EbdFilePathsSet(self);
    ret |= DeviceAndSemInterruptEnable(self);
    ret |= SemsInterruptMaskEnable(self);
    ret |= NotificationEnable(self, cAtTrue);

    return ret;
    }

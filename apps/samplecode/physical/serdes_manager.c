/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : physical
 *
 * File        : serdes_manager.c
 *
 * Created Date: Oct 6, 2016
 *
 * Description : Serdes manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtSerdesController.h"
#include "AtDevice.h"
#include "AtDriver.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void SerdesPrbsCheck(uint32 serdesId);
void Rsp3Mac10GSerdesSelect(AtModuleEth moduleEth);
void Rsp2Mac10GSerdesSelect(AtModuleEth moduleEth);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    return AtDriverDeviceGet(driver, 0);
    }

/*
 * QSGMII SERDESes support following PRBS mode:
 * - cAtPrbsModePrbs15
 * - cAtPrbsModePrbsSeq
 * - cAtPrbsModePrbsFixedPattern1Byte
 * - cAtPrbsModePrbs23
 *
 * SGMII SERDESes support following PRBS mode:
 * - cAtPrbsModePrbs15
 * - cAtPrbsModePrbsSeq
 * - cAtPrbsModePrbsFixedPattern1Byte
 *
 * XFI/RXAUI SERDESes support following PRBS mode:
 * - cAtPrbsModePrbsFixedPattern4Bytes
 * - cAtPrbsModePrbs31
 */
static eAtPrbsMode ModeBySerdesId(uint32 serdesId)
    {
    if (serdesId <= 3)
        return cAtPrbsModePrbs15;

    if ((serdesId == 4) || (serdesId == 5))
        return cAtPrbsModePrbs31;

    if ((serdesId == 6) || (serdesId == 7))
        return cAtPrbsModePrbs31;

    return cAtPrbsModePrbs15;
    }
/*
 * All SERDESes are managed by AtSerdesManager with following id:
 * 0..3 4xSGMII at line side
 * 4..5 RXAUI
 * 6..7 XFI
 * 8..9 QSGMII
 *
 * Note: At line side, SERDESes need to be configured to 1G mode first, PRBS engine
 * only can retrieved when SERDES is in 1G mode
 */
void SerdesPrbsCheck(uint32 serdesId)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(Device());
    AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(serdesManager, serdesId);
    AtPrbsEngine prbsEngine = AtSerdesControllerPrbsEngine(serdes);
    eAtRet ret;
    uint32 alarm;

    if (prbsEngine == NULL)
        {
        AtPrintc(cSevCritical, "Can not get PRBS engine from SERDES %u\r\n", serdesId);
        return;
        }

    /* Configure PRBS engine */
    ret = AtPrbsEngineEnable(prbsEngine, cAtTrue);
    ret |= AtPrbsEngineModeSet(prbsEngine, ModeBySerdesId(serdesId));
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Can not configure PRBS engine of SERDES %u\r\n", serdesId);
        return;
        }

    /* Check PRBS engine status */
    alarm = AtPrbsEngineAlarmGet(prbsEngine);
    if (alarm & cAtPrbsEngineAlarmTypeError)
        AtPrintc(cSevCritical, "At least one error happen\r\n");

    if (alarm & cAtPrbsEngineAlarmTypeLossSync)
        AtPrintc(cSevCritical, "Sync lost\r\n");
    }

void Rsp3Mac10GSerdesSelect(AtModuleEth moduleEth)
    {
    uint32 workingSerdesId = 6;
    uint32 protectSerdesId = 7;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(Device());
    AtSerdesController workingXfi = AtSerdesManagerSerdesControllerGet(serdesManager, workingSerdesId);
    AtSerdesController protectXfi = AtSerdesManagerSerdesControllerGet(serdesManager, protectSerdesId);
    AtEthPort mac10G = AtModuleEthPortGet(moduleEth, 0); /* Index of MAC 10G is 0 */
    eAtRet ret;

    /* Receive and transmit to working SERDES */
    ret  = AtEthPortTxSerdesSet(mac10G, workingXfi);
    ret |= AtEthPortRxSerdesSelect(mac10G, workingXfi);

    /* Bridge to protection SERDES */
    ret |= AtEthPortTxSerdesBridge(mac10G, protectXfi);

    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Could not configure SERDES for 10G MAC\r\n");
    }

void Rsp2Mac10GSerdesSelect(AtModuleEth moduleEth)
    {
    uint32 workingSerdesId = 4;
    uint32 protectSerdesId = 5;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(Device());
    AtSerdesController workingRxaui = AtSerdesManagerSerdesControllerGet(serdesManager, workingSerdesId);
    AtSerdesController protectRxaui = AtSerdesManagerSerdesControllerGet(serdesManager, protectSerdesId);
    AtEthPort mac10G = AtModuleEthPortGet(moduleEth, 0); /* Index of MAC 10G is 0 */
    eAtRet ret;

    /* Receive and transmit to working SERDES */
    ret  = AtEthPortTxSerdesSet(mac10G, workingRxaui);
    ret |= AtEthPortRxSerdesSelect(mac10G, workingRxaui);

    /* Bridge to protection SERDES */
    ret |= AtEthPortTxSerdesBridge(mac10G, protectRxaui);

    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Could not configure SERDES for 10G MAC\r\n");
    }

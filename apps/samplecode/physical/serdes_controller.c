/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : physical
 *
 * File        : serdes_controller.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtSerdesController.h"
#include "AtDevice.h"
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eBool IsLaneSerdesAlarmLinkDown(AtDevice self, uint32 serdesId, uint32 laneIdx);

/*--------------------------- Implementation ---------------------------------*/

static AtSerdesController SerdesControllerGet(AtDevice self, uint32 serdesId)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    AtSerdesController controler = AtModuleEthSerdesController(ethModule, serdesId);
    return controler;
    }

eBool IsLaneSerdesAlarmLinkDown(AtDevice self, uint32 serdesId, uint32 laneIdx)
    {
    uint32 alarm, numLanes;
    AtSerdesController controller = SerdesControllerGet(self, serdesId);

    /* Show all of its sub lanes if any */
    numLanes = AtSerdesControllerNumLanes(controller);
    if (laneIdx < numLanes)
        {
        AtSerdesController lane = AtSerdesControllerLaneGet(controller, laneIdx);
        if (lane == NULL)
            return cAtFalse;

        alarm = AtSerdesControllerAlarmGet(lane);
        if (alarm & cAtSerdesAlarmTypeLinkDown)
            return cAtTrue;
        }

    return cAtFalse;
    }

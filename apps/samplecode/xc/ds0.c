/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : ds0.c
 *
 * Created Date: Feb 7, 2015
 *
 * Description : DS0 example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDriver.h"
#include "AtCrossConnectDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Ds0Example(void);

/*--------------------------- Implementation ---------------------------------*/
/* The following example code will cross-connect between timeslot 2 of one
 * the first DS1/E1 and timeslot 6 of the second DS1/E1 */
void Ds0Example(void)
    {
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtPdhDe1 destDe1, sourceDe1;
    uint8 sourceTimeslot, destTimeslot;
    uint8 secondTimeSlot = 2;
    uint8 sixthTimeSlot  = 6;

    /* Get the first and second DE1 */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtPdhDe1 firstDe1  = AtModulePdhDe1Get(pdhModule, 0);
    AtPdhDe1 secondDe1 = AtModulePdhDe1Get(pdhModule, 1);

    /* Get DS0 cross-connect controller */
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    AtCrossConnectDs0 ds0Xc = AtModuleXcDs0CrossConnectGet(xcModule);

    /* Connect timeslot 2 of first DE1 to the timeslot 6 of the second one */
    AtAssert((eAtRet)AtCrossConnectDs0Connect(ds0Xc, firstDe1, secondTimeSlot, secondDe1, sixthTimeSlot) == cAtOk);
    AtAssert((eAtRet)AtCrossConnectDs0Connect(ds0Xc, secondDe1, sixthTimeSlot, firstDe1, secondTimeSlot) == cAtOk);

    /* Can also query destination information of timeslot 2 of the first DE1 */
    AtAssert(AtCrossConnectDs0NumDestChannelsGet(ds0Xc, firstDe1, secondTimeSlot) == 1);
    destDe1 = AtCrossConnectDs0DestChannelGetByIndex(ds0Xc, firstDe1, secondTimeSlot, 0, &destTimeslot);
    AtAssert(destDe1 == secondDe1);
    AtAssert(destTimeslot == sixthTimeSlot);

    /* And from the timeslot 6 of second DE1, the source timeslot can be also
     * queried as following */
    sourceDe1 = AtCrossConnectDs0SourceChannelGet(ds0Xc, secondDe1, sixthTimeSlot, &sourceTimeslot);
    AtAssert(sourceDe1 == firstDe1);
    AtAssert(sourceTimeslot == secondTimeSlot);

    /* And these timeslots can be disconnected as following */
    AtAssert((eAtRet)AtCrossConnectDs0Disconnect(ds0Xc, firstDe1, secondTimeSlot, secondDe1, sixthTimeSlot) == cAtOk);
    AtAssert((eAtRet)AtCrossConnectDs0Disconnect(ds0Xc, secondDe1, sixthTimeSlot, firstDe1, secondTimeSlot) == cAtOk);
    }

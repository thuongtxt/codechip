/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : bridge_and_roll.c
 *
 * Created Date: Apr 12, 2015
 *
 * Description : Bridge and roll example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtDevice.h"
#include "AtModuleXc.h"
#include "AtCrossConnect.h"
#include "AtCisco.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void BridgeAndRoleExample_Tse(AtDevice device);
void BridgeAndRoleExample_Arrive(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
static void TseBridge(void)
    {
    }

static void TseRoll(void)
    {
    }

static void TseConnectTimeslot3ToSonetFramer(void)
    {
    }

void BridgeAndRoleExample_Tse(AtDevice device)
    {
    /* SDH and XC modules will be used */
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* TFI-5 Lane 3 channels */
    AtSdhLine tfi5Line3 = AtModuleSdhTfi5LineGet(sdhModule, 2);
    AtSdhChannel tfi5Line3Vc3 = (AtSdhChannel)AtSdhLineVc3Get(tfi5Line3, 0, 0);

    /*  TFI-5 Lane 2 channels */
    AtSdhLine tfi5Line2 = AtModuleSdhTfi5LineGet(sdhModule, 1);
    AtSdhChannel tfi5Line2Vc3 = (AtSdhChannel)AtSdhLineVc3Get(tfi5Line2, 0, 2);

    /* Bridge. This is application job that control the TSE block to bycast
     * traffic. After this calling, the same traffic will be available at STS/VC
     * XC block on timeslot 3 */
    TseBridge();

    /* Roll:
     * 1. Need to switch dropped traffic from lane 3 to lane 2
     * 2. Using TSE to disconnect connection to timeslot 1 of TFI-5 Line 3.
     * ==> Then Rolling processing completes.
     */
    AtAssert((eAtRet)AtSdhChannelRxTrafficSwitch(tfi5Line3Vc3, tfi5Line2Vc3) == cAtOk);
    TseRoll();
    }

void BridgeAndRoleExample_Arrive(AtDevice device)
    {
    /* SDH and XC modules will be used */
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* TFI-5 Lane 3 channels */
    AtSdhLine tfi5Line3 = AtModuleSdhTfi5LineGet(sdhModule, 2);
    AtSdhChannel tfi5Line3Vc3 = (AtSdhChannel)AtSdhLineVc3Get(tfi5Line3, 0, 0);

    /*  TFI-5 Lane 2 channels */
    AtSdhLine tfi5Line2 = AtModuleSdhTfi5LineGet(sdhModule, 1);
    AtSdhChannel tfi5Line2Vc3 = (AtSdhChannel)AtSdhLineVc3Get(tfi5Line2, 0, 2);

    /* Bridge. */
    AtAssert((eAtRet)AtSdhChannelTxTrafficBridge(tfi5Line3Vc3, tfi5Line2Vc3) == cAtOk);

    /* Roll:
     * 1. Using TSE to connect timeslot 3 of TFI-5 Line 3 to SONET framer.
     * 2. Using AtSdhChannelTxTrafficCut to cut traffic that is being added to lane 3
     * ==> Then Rolling processing completes.
     */
    TseConnectTimeslot3ToSonetFramer();
    AtAssert((eAtRet)AtSdhChannelTxTrafficCut(tfi5Line3Vc3) == cAtOk);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : ram.c
 *
 * Created Date: Feb 4, 2015
 *
 * Description : RAM testing example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtModuleRam.h"
#include "AtRam.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void AllDdrsTest(void);
void AllDdrsTestWithDurationInMs(uint32 durationMs);
void RamStatusShow(AtRam ram);

/*--------------------------- Implementation ---------------------------------*/
void AllDdrsTest(void)
    {
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(device, cAtModuleRam);
    uint8 ddr_i;

    /* Test all of DDRs */
    for (ddr_i = 0; ddr_i < AtModuleRamNumDdrGet(ramModule); ddr_i++)
        {
        static const uint32 cAnyAddress   = 0x2345;
        static const uint32 cStartAddress = 0;
        eAtRet ret;
        AtRam ram = AtModuleRamDdrGet(ramModule, ddr_i);
        uint32 endAddress = AtRamMaxAddressGet(ram);
        uint32 firstErrorAddress;

        AtAssert(AtRamAddressBusTest(ram) == cAtOk);
        AtAssert(AtRamDataBusTest(ram, cAnyAddress) == cAtOk);

        /* Memory testing is longer and the result may need to be examined */
        ret = AtRamMemoryTest(ram, cStartAddress, endAddress, &firstErrorAddress);
        if (ret != cAtOk)
            AtPrintf("First error address: 0x%08x\r\n", firstErrorAddress);
        AtAssert(ret == cAtOk);
        }
    }

void AllDdrsTestWithDurationInMs(uint32 durationMs)
    {
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(device, cAtModuleRam);
    uint8 ddr_i;

    /* Test all of DDRs */
    for (ddr_i = 0; ddr_i < AtModuleRamNumDdrGet(ramModule); ddr_i++)
        {
        AtRam ram = AtModuleRamDdrGet(ramModule, ddr_i);

        /* Application may want to do each test separately, as following */
        AtAssert(AtRamAddressBusTestWithDuration(ram, durationMs) == cAtOk);
        AtAssert(AtRamDataBusTestWithDuration(ram, durationMs)    == cAtOk);
        AtAssert(AtRamMemoryTestWithDuration(ram, durationMs)     == cAtOk);

        /* And it is recommend to use the following API to run all of these
         * tests, it would be faster */
        AtAssert(AtRamTestWithDuration(ram, durationMs) == cAtOk);
        }
    }

void RamStatusShow(AtRam ram)
    {
    uint32 alarms;

    /* Check if RAM encounters problems so far */
    alarms = AtRamAlarmHistoryClear(ram); /* Read and then clear RAM history */
    if (alarms & cAtRamAlarmEccCorrectable)
        AtPrintc(cSevWarning, "* RAM ECC correctable\r\n");
    if (alarms & cAtRamAlarmEccUnnorrectable)
        AtPrintc(cSevCritical, "* RAM ECC uncorrectable\r\n");
    if (alarms & cAtRamAlarmCrcError)
        AtPrintc(cSevCritical, "* RAM CRC error\r\n");

    /* Show counters */
    AtPrintc(cSevWarning,  "* Number of correctable errors  : %u\r\n",
             AtRamCounterClear(ram, cAtRamCounterTypeEccCorrectableErrors));
    AtPrintc(cSevCritical, "* Number of uncorrectable errors: %u\r\n",
             AtRamCounterClear(ram, cAtRamCounterTypeEccUncorrectableErrors));
    AtPrintc(cSevCritical, "* Number of CRC errors          : %u\r\n",
             AtRamCounterClear(ram, cAtRamCounterTypeCrcErrors));
    }

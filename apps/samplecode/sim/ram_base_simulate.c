/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : main.c
 *
 * Created Date: Jun 04, 2016
 *
 * Description : Sample code to work with RAM base simulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/
/* List of products. IMPORTANT: all of product codes below are not officially
 * assigned. They are only used for demo purpose */
#define cDs3ProductCode 0x60210031 /* 48 DS3s/EC1s LIU */
#define cDs1ProductCode 0x60210021 /* 48 DS1 LIU */
#define cOcnProductCode 0x60210051 /* 4 OC-48s */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* For command processing */
static AtTextUI m_textUI = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHal CreateHalSim(uint32 realProductCode)
    {
    return AtHalSimCreateForProductCode(realProductCode, cAtHalSimDefaultMemorySizeInMbyte);
    }

static AtTextUI TinyTextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
    }

static uint32 SimulatedProductCode(uint32 realProductCode)
    {
    /* The SDK internally uses bit[31:28] = 0xF to know of this device should
     * run in simulation mode, so that hardware timeout can be ignored */
    return (uint32)((realProductCode & (~cBit31_28)) | (0xFUL << 28));
    }

int SimulateWithProductCode(uint32 productCode);
int SimulateWithProductCode(uint32 productCode)
    {
    eAtRet ret = cAtOk;

    /* Create driver with one device */
    AtDriver driver = AtDriverCreate(1, AtOsalVxWorks());
    AtHal hal = CreateHalSim(productCode);
    AtDevice device = AtDriverDeviceCreate(driver, SimulatedProductCode(productCode));

    /* Give the new created device HAL and initialize it */
    AtDeviceIpCoreHalSet(device, 0, hal); /* All of devices have one core (FPGA) */
    ret = AtDeviceInit(device);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device initializing fail with ret = %s\r\n", AtRet2String(ret));
        return -1;
        }

    /* Start CLI */
    AtCliStartWithTextUI(TinyTextUI());

    /* Cleanup */
    AtDriverDeviceDelete(driver, device);
    AtHalDelete(hal);
    AtDriverDelete(driver);
    AtTextUIDelete(TinyTextUI());

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : ppp-mlppp.c
 *
 * Created Date: Feb 19, 2016
 *
 * Description : PPP/MLPPP sample code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"

#include "AtModulePdh.h"

#include "AtModuleSdh.h"
#include "AtSdhLine.h"

#include "AtModuleEncap.h"
#include "AtEncapChannel.h"
#include "AtModulePpp.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"

#include "AtHdlcBundle.h"
#include "AtMpBundle.h"

#include "AtModuleEth.h"
#include "AtEthFlow.h"

#include "AtPw.h"
#include "AtPwPsn.h"
#include "AtPwHdlc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Vc3_Ds1_Ppp_Pw_Mpls(void);
void Vc3_Ds1_Ppp_Termination_IPv4_Routing(void);
void Vc3_Ds1_Ppp_Pw_IPv4_Interworking(void);
void De1_Ppp_EthFlow_Example(void);
void De1_Ppp_Mlppp_EthFlow_Example(void);
void PppOam(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    return AtDriverDeviceGet(driver, 0);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

/*
 * DA(programmable) + SA(programmable) + VLAN(PW) + MPLS Label (circuit) + 0x8847 + PW Control Word (programmable and enable/disable CW work ) + PPP/FR
 */
void Vc3_Ds1_Ppp_Pw_Mpls(void)
    {
    AtModulePw pwModule;
    AtPw pw;
    AtPppLink pppLink;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc15 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc15);

    /* Allocate HDLC-PPP channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* PPP link from HDLC channel */
    pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwPppCreate(pwModule, pppLink);

    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void PppOam(void)
    {
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag cVlan;
    AtEthFlow oamFlow;
    uint8 ethPortId = 0;
    uint16 channelId = 0;

    /* Get device instance first */
    AtDevice device = Device();

    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channelId);

    /* Get PPP link from HDLC channel */
    AtPppLink pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet(hdlcChannel);

    /* Configure flow for control packets */
    oamFlow = AtHdlcLinkOamFlowGet((AtHdlcLink)pppLink);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &cVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);
    }

/*
 * Control:
 * DA(programmable) + SA(programmable) +VLAN(control pkt)+ VLAN(circuit - per DLCI if FR) + 0x8857(may be programmable) + PPP/FR + Payload
 *
 * Data:
 * DA( programmable) + SA (programmable) + VLAN(circuit - per DLCI if FR) + type (derived from ipv4/ipv6/... type) + IPv4/IPv6/...
 *
 */
void Vc3_Ds1_Ppp_Termination_IPv4_Routing(void)
    {
    tAtEthVlanDesc dataTrafficDesc;
    tAtEthVlanTag cVlan;
    AtModuleEth ethModule;
    AtPppLink pppLink;
    AtEthFlow dataFlow;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc15 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 1); /* Use the second DS1 for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc15);

    /* Allocate PPP HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Get PPP link from HDLC channel */
    pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet(encapChannel);

    /* Termination with IPv4 routing */
    AtHdlcLinkPduTypeSet((AtHdlcLink)pppLink, cAtHdlcPduTypeIPv4);

    /* Create flow for data packets and setup VLAN traffic descriptor for two directions */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    dataFlow = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 3, &cVlan), &dataTrafficDesc);
    AtEthFlowIngressVlanAdd(dataFlow, &dataTrafficDesc);
    AtEthFlowEgressVlanAdd(dataFlow, &dataTrafficDesc);

    /* Bring this flow to PPP link */
    AtHdlcLinkFlowBind((AtHdlcLink)pppLink, dataFlow);

    /* Traffic on link is disabled at both direction as default.
     * Application will determine when to enable traffic flow like below */
    AtChannelTxEnable((AtChannel)pppLink, cAtTrue);
    AtChannelRxEnable((AtChannel)pppLink, cAtTrue);
    }

/*
 * Control:
 * DA(programmable) + SA(programmable) +VLAN(control pkt)+ VLAN(circuit - per DLCI if FR) + Ethernet type (programmable) + PPP/FR + Payload
 *
 * Data:
 * DA(programmable) + SA(programmable) + VLAN(PW) + MPLS Label (circuit) + 0x8847 + PW CW (programmable and enable/disable CW word) + IPv4/IPV6/MPLS
 */
void Vc3_Ds1_Ppp_Pw_IPv4_Interworking(void)
    {
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag cVlan;
    AtModulePw pwModule;
    AtPw pw;
    AtPppLink pppLink;
    AtEthFlow oamFlow;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc15 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 2); /* Use the third one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc15);

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Get PPP link from HDLC channel */
    pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet(encapChannel);

    /* Configure flow for control packets */
    oamFlow = AtHdlcLinkOamFlowGet((AtHdlcLink)pppLink);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &cVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);

    /* Create HDLC PW */
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwPppCreate(pwModule, pppLink);

    /* Configure PPP PW to payload mode to IPv4, so that only information field (assume it is IPv4) of PPP is encapsulated on PW */
    AtHdlcLinkPduTypeSet((AtHdlcLink)pppLink, cAtHdlcPduTypeIPv4);
    AtPwHdlcPayloadTypeSet((AtPwHdlc)pw, cAtPwHdlcPayloadTypePdu);

    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void De1_Ppp_EthFlow_Example(void)
    {
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;
    uint8 de1Id = 0;
    AtModuleEth ethModule;
    AtEthFlow flow;
    AtPppLink link;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1/E1 as physical channel */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)AtModulePdhDe1Get(pdhModule, de1Id));

    /* Create flow and setup VLAN traffic descriptor for two directions */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    flow = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, AtEthVlanTagConstruct(1, 0, 1, &sVlan), AtEthVlanTagConstruct(1, 0, 2, &cVlan), &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow, &vlanTrafficDesc);

    /* Bring this flow to this link */
    link = (AtPppLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    AtHdlcLinkFlowBind((AtHdlcLink)link, flow);

    /* Traffic on link is disabled at both direction as default. Application PPP
    * protocol will determine when to enable traffic flow like below */
    AtChannelTxEnable((AtChannel)link, cAtTrue);
    AtChannelRxEnable((AtChannel)link, cAtTrue);
    }

void De1_Ppp_Mlppp_EthFlow_Example(void)
    {
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;
    AtModulePpp pppModule;
    AtMpBundle bundle;
    AtModuleEth ethModule;
    AtEthFlow flow1, flow2;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);

    /* Allocate HDLC channels, must be PPP/HDLC */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel1 = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtHdlcChannel encapChannel2 = AtModuleEncapHdlcPppChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1s/E1s as physical interfaces */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel1, (AtChannel)AtModulePdhDe1Get(pdhModule, 0));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel2, (AtChannel)AtModulePdhDe1Get(pdhModule, 1));

    /* Create MLPPP bundle and add these links */
    pppModule = (AtModulePpp)AtDeviceModuleGet(device, cAtModulePpp);
    bundle = (AtMpBundle)AtModulePppMpBundleCreate(pppModule, 0); /* Use the first bundle */
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel1));
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel2));

    /* Enable traffic on links */
    AtChannelTxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtChannelRxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtChannelTxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);
    AtChannelRxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);

    /* Create traffic flow 1 */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    flow1 = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, AtEthVlanTagConstruct(1, 0, 1, &sVlan), AtEthVlanTagConstruct(1, 0, 2, &cVlan), &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow1, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow1, &vlanTrafficDesc);

    /* Create traffic flow 2 with only one VLAN */
    flow2 = AtModuleEthFlowCreate(ethModule, 1);
    AtEthFlowVlanDescConstruct(ethPortId, null,  AtEthVlanTagConstruct(1, 0, 3, &cVlan),  &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow2, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow2, &vlanTrafficDesc);

    /* Bring these flows to this bundle multi-class supported */
    AtMpBundleFlowAdd(bundle, flow1, 1); /* Add and assign flow 1 to class 1 */
    AtMpBundleFlowAdd(bundle, flow2, 2); /* Add and assign flow 2 to class 2 */
    }

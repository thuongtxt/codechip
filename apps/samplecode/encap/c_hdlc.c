/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : hdlc_pw.c
 *
 * Created Date: Mar 29, 2016
 *
 * Description : Sample code of HDLC pw
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"

#include "AtModulePdh.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"

#include "AtModuleEncap.h"
#include "AtEncapChannel.h"
#include "AtModuleFr.h"
#include "AtHdlcChannel.h"
#include "AtFrLink.h"

#include "AtPw.h"
#include "AtPwPsn.h"
#include "AtPwHdlc.h"
#include "AtCisco.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Vc3_Ds1_cHdlc_Pw_Mpls(void);
void Vc3_Ds1_cHdlc_Termination_IPv4_Routing(void);
void Vc3_Ds1_cHdlc_Pw_IPv4_Interworking(void);
void CommonOamFlowHeaderSetup(void);
void OamFlowSetup(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    return AtDriverDeviceGet(driver, 0);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

void CommonOamFlowHeaderSetup(void)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static uint16 ethernetType = 0x8857;
    static tAtVlan controlVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtCiscoControlFlowEgressDestMacSet(ethModule, mac);
    AtCiscoControlFlowEgressSrcMacSet(ethModule, mac);
    AtCiscoControlFlowEgressEthTypeSet(ethModule, ethernetType);

    /* Construct Control VLAN */
    controlVlan.tpid     = 0x88a8;
    controlVlan.cfi      = 0;
    controlVlan.priority = 0;
    controlVlan.vlanId   = 1000;

    AtCiscoControlFlowEgressControlVlanSet(ethModule, &controlVlan);
    AtCiscoControlFlowIngressControlVlanSet(ethModule, &controlVlan);

    /* TPID of circuit VLAN */
    AtCiscoControlFlowEgressCircuitVlanTPIDSet(ethModule, 0x8100);
    }

/*
 * DA(programmable) + SA(programmable) + VLAN(PW) + MPLS Label (circuit) + 0x8847 + PW Control Word (programmable and enable/disable CW work) + cHDLC frame
 */
void Vc3_Ds1_cHdlc_Pw_Mpls(void)
    {
    AtModulePw pwModule;
    AtPw pw;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc15 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc15);

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapCiscoHdlcChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* In HDLC PW, there's no special treatment for control packets, data and control packets are encapsulated with same PW header */
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwHdlcCreate(pwModule, encapChannel);

    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*
 * Control:
 * DA(programmable) + SA(programmable) + VLAN(control packet) + VLAN(circuit) + 0x8857(may be programmable) + cHDLC + Payload
 *
 * Data:
 * DA(programmable) + SA (programmable) + VLAN(circuit) + EthernetType (derived from ipv4/ipv6/... type) + IPv4/IPv6/...
 */
void Vc3_Ds1_cHdlc_Termination_IPv4_Routing(void)
    {
    tAtEthVlanDesc dataTrafficDesc;
    tAtEthVlanTag cVlan;
    AtModuleEth ethModule;
    AtHdlcLink hdlcLink;
    AtEthFlow dataFlow;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc15 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 1); /* Use the second one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc15);

    /* Allocate Cisco HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapCiscoHdlcChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Get HDLC link from HDLC channel */
    hdlcLink = AtHdlcChannelHdlcLinkGet(encapChannel);

    /* Termination with IPv4 routing */
    AtHdlcLinkPduTypeSet(hdlcLink, cAtHdlcPduTypeIPv4);

    /* Create flow for data packets and setup VLAN traffic descriptor for two directions */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    dataFlow = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 3, &cVlan), &dataTrafficDesc);
    AtEthFlowIngressVlanAdd(dataFlow, &dataTrafficDesc);
    AtEthFlowEgressVlanAdd(dataFlow, &dataTrafficDesc);

    /* Bring this flow to HDLC link */
    AtHdlcLinkFlowBind(hdlcLink, dataFlow);

    /* Traffic on link is disabled at both direction as default.
     * Application will determine when to enable traffic flow like below */
    AtChannelTxEnable((AtChannel)hdlcLink, cAtTrue);
    AtChannelRxEnable((AtChannel)hdlcLink, cAtTrue);
    }

/*
 * Control:
 * DA(programmable) + SA(programmable) + VLAN(control pkt) + VLAN(circuit) + Ethertype (programmable) + cHDLC +Payload -> Link/bundle oam flow
 *
 * Data:
 * DA(programmable) + SA(programmable) + VLAN(PW) + MPLS Label (circuit) + 0x8847 + PW CW (programmable and enable/disable CW word) + IPv4/IPV6/MPLS -> PW
 */
void Vc3_Ds1_cHdlc_Pw_IPv4_Interworking(void)
    {
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag cVlan;
    AtModulePw pwModule;
    AtPw pw;
    AtHdlcLink hdlcLink;
    AtEthFlow oamFlow;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc15 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 2); /* Use the third one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc15);

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapCiscoHdlcChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Get HDLC link from HDLC channel */
    hdlcLink = AtHdlcChannelHdlcLinkGet(encapChannel);

    /* Only accept IPv4 */
    AtHdlcLinkPduTypeSet(hdlcLink, cAtHdlcPduTypeIPv4);

    /* Configure flow for control packets */
    oamFlow = AtHdlcLinkOamFlowGet(hdlcLink);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &cVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);

    /* Create HDLC PW */
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwHdlcCreate(pwModule, encapChannel);

    /* Configure HDLC PW to payload IPv4, so that only information field (assume it is IPv4) of HDLC is encapsulated on PW */
    AtHdlcLinkPduTypeSet(hdlcLink, cAtHdlcPduTypeIPv4);
    AtPwHdlcPayloadTypeSet((AtPwHdlc)pw, cAtPwHdlcPayloadTypePdu);

    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void OamFlowSetup(void)
    {
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag circuitVlan;
    AtEthFlow oamFlow;
    uint8 ethPortId = 0;
    uint16 channelId = 0;

    /* Get device instance first */
    AtDevice device = Device();

    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channelId);

    /* Get link from HDLC channel */
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet(hdlcChannel);

    /* Configure flow for control packets */
    oamFlow = AtHdlcLinkOamFlowGet(link);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &circuitVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : fr_mfr.c
 *
 * Created Date: Feb 19, 2016
 *
 * Description : Sample code of HDLC pw
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "commacro.h"
#include "AtDevice.h"

#include "AtModulePdh.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"

#include "AtModuleEncap.h"
#include "AtEncapChannel.h"
#include "AtModuleFr.h"
#include "AtHdlcChannel.h"
#include "AtFrLink.h"

#include "AtHdlcBundle.h"
#include "AtMfrBundle.h"

#include "AtModuleEth.h"
#include "AtEthFlow.h"

#include "AtPw.h"
#include "AtPwPsn.h"
#include "AtPwHdlc.h"
#include "AtFrVirtualCircuit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Vc3_Ds1_Fr_Pw_Port_Mode_Mpls(void);
void Vc3_Ds1_Fr_Termination_IPv4_Routing(void);
void Vc3_Ds1_Fr_Pw_IPv4_Interworking(void);
void Ds1_Fr_EthFlow_Example(void);
void Ds1_Mfr_EthFlow_Example(void);
void OamFlowSetup(void);
/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    return AtDriverDeviceGet(driver, 0);
    }

static void SetupPwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw)
    {
    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw)
    {
    SetupPwMpls(pw);
    SetupPwMac(pw);
    }

/*
 * DA(programmable) + SA(programmable) + VLAN(PW) + MPLS Label (circuit) + 0x8847 + PW Control Word (programmable and enable/disable CW work ) + PPP/FR
 */
void Vc3_Ds1_Fr_Pw_Port_Mode_Mpls(void)
    {
    AtModulePw pwModule;
    AtPw pw;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate Frame relay channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Frame relay PW port mode */
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwHdlcCreate(pwModule, encapChannel);

    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

/*
 * Control:
 * DA(programmable) + SA(programmable) +VLAN(control pkt)+ VLAN(circuit - per DLCI if FR) + 0x8857(may be programmable) + PPP/FR + Payload
 *
 * Data:
 * DA( programmable) + SA (programmable) + VLAN(circuit - per DLCI if FR) + type (derived from ipv4/ipv6/... type) + IPv4/IPv6/...
 *
 */
void Vc3_Ds1_Fr_Termination_IPv4_Routing(void)
    {
    tAtEthVlanDesc dataTrafficDesc;
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag cVlan;
    AtModuleEth ethModule;
    AtFrLink frLink;
    AtEthFlow dataFlow;
    AtEthFlow oamFlow;
    uint8 ethPortId = 0;
    uint32 dlci;
    AtFrVirtualCircuit frVirtualCircuit;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 1); /* Use the second one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate Frame relay channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Get FR link from HDLC channel */
    frLink = (AtFrLink)AtHdlcChannelHdlcLinkGet(encapChannel);

    /* Termination with IPv4 routing */
    AtHdlcLinkPduTypeSet((AtHdlcLink)frLink, cAtHdlcPduTypeIPv4);

    /* Create FR virtual circuit for specific dlci . */
    dlci = 1234;
    frVirtualCircuit = AtFrLinkVirtualCircuitCreate(frLink, dlci);

    /* Configure flow for control packets */
    oamFlow = AtFrVirtualCircuitOamFlowGet(frVirtualCircuit);

    /* Construct VLAN descriptor with 1 VLAN tag */
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &cVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);

    /* Create flow for data packets and setup VLAN traffic descriptor for two directions */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    dataFlow = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 3, &cVlan), &dataTrafficDesc);
    AtEthFlowIngressVlanAdd(dataFlow, &dataTrafficDesc);
    AtEthFlowEgressVlanAdd(dataFlow, &dataTrafficDesc);

    /* Bring this flow to FR virtual circuit */
    AtFrVirtualCircuitFlowBind(frVirtualCircuit, dataFlow);
    }

/*
 * Control:
 * DA(programmable) + SA(programmable) +VLAN(control pkt)+ VLAN(circuit - per DLCI if FR) + Ethernet type (programmable) + PPP/FR + Payload
 *
 * Data:
 * DA(programmable) + SA(programmable) + VLAN(PW) + MPLS Label (circuit) + 0x8847 + PW CW (programmable and enable/disable CW word) + IPv4/IPV6/MPLS
 */
void Vc3_Ds1_Fr_Pw_IPv4_Interworking(void)
    {
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag cVlan;
    AtModulePw pwModule;
    AtPw pw;
    AtFrLink frLink;
    AtEthFlow oamFlow;
    uint8 ethPortId = 0;
    AtFrVirtualCircuit frVirtualCircuit;
    uint32 dlci;

    /* Get device instance first */
    AtDevice device = Device();

    /* Assume that all AUG-1s in STM16 line have been mapped to DS1
     * Get DS1 */
    uint8 aug1Id = 0;
    uint8 lineId = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhChannel vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 2); /* Use the third one for this example */
    AtPdhDe1 ds1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);

    /* Allocate Frame relay channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)ds1);

    /* Get FR link from HDLC channel */
    frLink = (AtFrLink)AtHdlcChannelHdlcLinkGet(encapChannel);

    /* Create FR virtual circuit for specific dlci . */
    dlci = 1234;
    frVirtualCircuit = AtFrLinkVirtualCircuitCreate(frLink, dlci);

    /* Configure flow for control packets */
    oamFlow = AtFrVirtualCircuitOamFlowGet(frVirtualCircuit);

    /* Construct VLAN descriptor with 1 VLAN tag */
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &cVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);

    /* Create FR PW */
    pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwFrVcCreate(pwModule, frVirtualCircuit);

    /* Configure FR PW to payload mode to IPv4, so that only information field (assume it is IPv4) of Frame Relay is encapsulated on PW */
    AtHdlcLinkPduTypeSet((AtHdlcLink)frLink, cAtHdlcPduTypeIPv4);
    AtPwHdlcPayloadTypeSet((AtPwHdlc)pw, cAtPwHdlcPayloadTypePdu);

    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
    }

void Ds1_Fr_EthFlow_Example(void)
    {
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;
    uint8 de1Id = 0;
    AtModuleEth ethModule;
    AtEthFlow flow;
    AtFrLink link;
    AtFrVirtualCircuit virtualCircuit;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1/E1 as physical channel */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)AtModulePdhDe1Get(pdhModule, de1Id));

    /* Create flow and setup VLAN traffic descriptor for two directions */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    flow = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, AtEthVlanTagConstruct(1, 0, 1, &sVlan), AtEthVlanTagConstruct(1, 0, 2, &cVlan), &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow, &vlanTrafficDesc);

    /* Bring this flow to this link */
    link = (AtFrLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    virtualCircuit = AtFrLinkVirtualCircuitCreate(link, 0);/* DLCI */
    AtFrVirtualCircuitFlowBind(virtualCircuit, flow);

    /* Traffic on link is disabled at both direction as default. Application PPP
    * protocol will determine when to enable traffic flow like below */
    AtChannelTxEnable((AtChannel)link, cAtTrue);
    AtChannelRxEnable((AtChannel)link, cAtTrue);
    }

void Ds1_Mfr_EthFlow_Example(void)
    {
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;
    AtModuleFr frModule;
    AtMfrBundle bundle;
    AtModuleEth ethModule;
    AtEthFlow flow1, flow2;
    AtFrVirtualCircuit virtualCircuit1, virtualCircuit2;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);

    /* Allocate HDLC channels, must be PPP/HDLC */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel1 = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));
    AtHdlcChannel encapChannel2 = AtModuleEncapFrChannelCreate(encapModule, (uint16)AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1s/E1s as physical interfaces */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel1, (AtChannel)AtModulePdhDe1Get(pdhModule, 0));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel2, (AtChannel)AtModulePdhDe1Get(pdhModule, 1));

    /* Create MFR bundle and add these links */
    frModule = (AtModuleFr)AtDeviceModuleGet(device, cAtModuleFr);
    bundle = (AtMfrBundle)AtModuleFrMfrBundleCreate(frModule, 0); /* Use the first bundle */
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel1));
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel2));

    /* Enable traffic on links */
    AtChannelTxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtChannelRxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtChannelTxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);
    AtChannelRxEnable((AtChannel)AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);

    /* Create traffic flow 1 */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    flow1 = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, AtEthVlanTagConstruct(1, 0, 1, &sVlan), AtEthVlanTagConstruct(1, 0, 2, &cVlan), &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow1, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow1, &vlanTrafficDesc);

    /* Create traffic flow 2 with only one VLAN */
    flow2 = AtModuleEthFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId, null, AtEthVlanTagConstruct(1, 0, 3, &cVlan), &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow2, &vlanTrafficDesc);
    AtEthFlowEgressVlanAdd(flow2, &vlanTrafficDesc);

    /* Bring these flows to this virtual circuit */
    virtualCircuit1 = AtMfrBundleVirtualCircuitCreate(bundle, 0);
    virtualCircuit2 = AtMfrBundleVirtualCircuitCreate(bundle, 1);
    AtFrVirtualCircuitFlowBind(virtualCircuit1, flow1); /* Add and assign flow 1 to DLCI 1 */
    AtFrVirtualCircuitFlowBind(virtualCircuit2, flow2); /* Add and assign flow 2 to DLCI 2 */
    }

void OamFlowSetup(void)
    {
    uint8 ethPortId = 0;
    uint16 channelId = 0;
    uint32 dlci = 1000;
    tAtEthVlanDesc controlTrafficDesc;
    tAtEthVlanTag circuitVlan;
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverDeviceGet(driver, 0);

    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channelId);
    AtFrLink frLink = (AtFrLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    AtFrVirtualCircuit dlciCircuit = AtFrLinkVirtualCircuitGet(frLink, dlci);

    AtEthFlow oamFlow = AtFrVirtualCircuitOamFlowGet(dlciCircuit);
    AtEthFlowVlanDescConstruct(ethPortId, NULL, AtEthVlanTagConstruct(1, 0, 2, &circuitVlan), &controlTrafficDesc);
    AtEthFlowIngressVlanAdd(oamFlow, &controlTrafficDesc);
    AtEthFlowEgressVlanAdd(oamFlow, &controlTrafficDesc);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : textui.c
 *
 * Created Date: Nov 3, 2014
 *
 * Description : Text-UI example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtTextUI m_textUI = NULL; /* Cache it till to avoid recreating */

/*--------------------------- Forward declarations ---------------------------*/
void ArriveTextUIStart(void);
void ArriveTextUIFree(void);

/*--------------------------- Implementation ---------------------------------*/
static AtTextUI TextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
    }

/*
 * To start Text-UI with a prompt so that CLI can be input
 */
void ArriveTextUIStart(void)
    {
    AtCliStartWithTextUI(TextUI());
    }

void ArriveTextUIFree(void)
    {
    AtTextUIDelete(m_textUI);
    m_textUI = NULL;
    }

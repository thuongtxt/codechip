/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : logger.c
 *
 * Created Date: Sep 19, 2016
 *
 * Description : Logger example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void LoggerDidLogMessage(AtLogger self, eAtLogLevel level, const char *message, void *userData)
    {
    /* User data can be accessed here */
    /* ... */

    /* Application can log message by its own way */
    AtPrintf("[%d] %s\r\n", level, message);
    }

static const tAtLoggerListener *Listener(void)
    {
    static tAtLoggerListener listener;
    static tAtLoggerListener *pListener = NULL;

    if (pListener)
        return pListener;

    /* Setup callback */
    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidLogMessage = LoggerDidLogMessage;
    pListener = &listener;

    return pListener;
    }

void LoggerListen(void)
    {
    static uint32 anyAppContext = 0; /* Any application context, will input when callback is called */
    AtLogger logger = AtSharedDriverLoggerGet();

    AtLoggerListenerAdd(logger, Listener(), &anyAppContext);
    }

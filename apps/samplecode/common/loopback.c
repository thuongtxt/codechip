/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : loopback.c
 *
 * Created Date: Feb 4, 2015
 *
 * Description : Loopback example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "commacro.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void SdhLineLoopbackExample(AtSdhLine line);

/*--------------------------- Implementation ---------------------------------*/
void SdhLineLoopbackExample(AtSdhLine line)
    {
    AtChannel lineChannel = (AtChannel)line;
    AtSerdesController serdesController = AtSdhLineSerdesController(line);

    /* The following lines show how to perform each loopback mode on line */
    AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeLocal)   == cAtOk);
    AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeRemote)  == cAtOk);
    AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeRelease) == cAtOk);

    /* And the following lines show how to perform each loopback mode on line's SERDES */
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeLocal)   == cAtOk);
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeRemote)  == cAtOk);
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeRelease) == cAtOk);
    }

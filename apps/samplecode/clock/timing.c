/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : timing.c
 *
 * Created Date: Mar 27, 2015
 *
 * Description : Timing configuration
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "commacro.h"
#include "AtChannel.h"
#include "AtDevice.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void TimingConfigureExample(AtChannel tdmChannel);
void DcrGlobalSetup(AtModulePw pwModule);

/*--------------------------- Implementation ---------------------------------*/
void TimingConfigureExample(AtChannel tdmChannel)
    {
    AtPw pw;

    /* Can check whether a timing mode is supported */
    AtAssert(AtChannelTimingModeIsSupported((tdmChannel), cAtTimingModeSys) == cAtTrue);

    /* Some timing mode does not require specified timing source. They can be
     * used as following examples */
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModeSys, NULL)     == cAtOk);
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModeLoop, NULL)    == cAtOk);
    /* The following is 3 modes are equivalent to 3 extern clock sources from 3 different Hw input PINs */
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModePrc, NULL) == cAtOk);
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModeExt1Ref, NULL) == cAtOk);
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModeExt2Ref, NULL) == cAtOk);

    /* Assume that this TDM channel is bound to a PW, ACR/DCR timing mode can be
     * configured as following */
    pw = AtChannelBoundPwGet(tdmChannel);
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModeAcr, (AtChannel)pw) == cAtOk);

    /* And application can also query timing mode along with timing source as following */
    AtAssert(AtChannelTimingModeGet(tdmChannel) == cAtTimingModeAcr);
    AtAssert(AtChannelTimingSourceGet(tdmChannel) == (AtChannel)pw);

    /* Application can also configure DCR as following. Note: For DCR global
     * configuration such as clock source, frequency and RTP timestamp, The SDK
     * already configures default for each concrete product. See DcrGlobalSetup()
     * if application needs to configure these attributes */
    AtAssert(AtChannelTimingSet(tdmChannel, cAtTimingModeDcr, (AtChannel)pw) == cAtOk);
    AtPwRtpEnable(pw, cAtTrue);

    /* Clock state can also be retrieved as following */
    AtPrintc(cSevNormal, "Clock state: %d\r\n", AtChannelClockStateGet(tdmChannel));
    }

void DcrGlobalSetup(AtModulePw pwModule)
    {
    AtAssert(AtModulePwDcrClockSourceSet(pwModule, cAtPwDcrClockSourcePrc) == cAtOk);
    AtAssert(AtModulePwDcrClockFrequencySet(pwModule, 19440) == cAtOk);
    AtAssert(AtModulePwRtpTimestampFrequencySet(pwModule, 2048) == cAtOk);
    }

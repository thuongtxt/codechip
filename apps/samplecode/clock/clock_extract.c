/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : clock_extract.c
 *
 * Created Date: Feb 4, 2015
 *
 * Description : Clock extract example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtModuleClock.h"
#include "AtClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void ClockExtractExample(AtDevice device);
eAtRet SourceClockFromDe1LiuClockRecover(AtDevice device);
eAtRet SourceClockFromDe1CdrClockRecover(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
/*
 * This example function use two clock extractors to output two clocks recovered
 * from SDH Line and DS1/E1. Just a simple example, this function use the first
 * line and the fist DS1/E1 that is mapped to the first VC1x.
 */
void ClockExtractExample(AtDevice device)
    {
    AtModuleClock clock = (AtModuleClock)AtDeviceModuleGet(device, cAtModuleClock);
    AtClockExtractor extractor1 = AtModuleClockExtractorGet(clock, 0);
    AtClockExtractor extractor2 = AtModuleClockExtractorGet(clock, 1);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line1 = AtModuleSdhLineGet(sdhModule, 0);
    AtSdhChannel vc1x = (AtSdhChannel)AtSdhLineVc1xGet(line1, 0 , 0, 0, 0);
    AtPdhDe1 de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc1x);

    AtClockExtractorSdhLineClockExtract(extractor1, line1);
    AtClockExtractorPdhDe1ClockExtract(extractor2, de1);
    }

/* The following example for primary and secondary clocks recovering from RX DS1/E1 LIU */
eAtRet SourceClockFromDe1LiuClockRecover(AtDevice device)
	{
	eAtRet ret = cAtOk;
	AtModuleClock clock = (AtModuleClock)AtDeviceModuleGet(device, cAtModuleClock);
	AtClockExtractor primary = AtModuleClockExtractorGet(clock, 0);
	AtClockExtractor secondary = AtModuleClockExtractorGet(clock, 1);
	AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
	AtPdhDe1 primaryde1 = (AtPdhDe1)AtModulePdhDe1Get(pdhModule, 10);
	AtPdhDe1 secondaryde1 = (AtPdhDe1)AtModulePdhDe1Get(pdhModule, 15);

	ret = AtClockExtractorPdhDe1LiuClockExtract(primary, primaryde1);
	if (ret == cAtOk) ret = AtClockExtractorEnable(primary, cAtTrue);
	if (ret == cAtOk) ret = AtClockExtractorPdhDe1LiuClockExtract(secondary, secondaryde1);
	if (ret == cAtOk) ret = AtClockExtractorEnable(secondary, cAtTrue);

	return ret;
	}

/* The following example for primary and secondary clocks recovering from DS1/E1 CDR clock: ACR/DCR/Loop/system */
eAtRet SourceClockFromDe1CdrClockRecover(AtDevice device)
	{
	eAtRet ret = cAtOk;
	AtModuleClock clock = (AtModuleClock)AtDeviceModuleGet(device, cAtModuleClock);
	AtClockExtractor primary = AtModuleClockExtractorGet(clock, 0);
	AtClockExtractor secondary = AtModuleClockExtractorGet(clock, 1);
	AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
	AtPdhDe1 primaryde1 = (AtPdhDe1)AtModulePdhDe1Get(pdhModule, 10);
	AtPdhDe1 secondaryde1 = (AtPdhDe1)AtModulePdhDe1Get(pdhModule, 15);

	ret = AtClockExtractorPdhDe1ClockExtract(primary, primaryde1);
	if (ret == cAtOk) ret = AtClockExtractorEnable(primary, cAtTrue);
	if (ret== cAtOk) ret = AtClockExtractorPdhDe1ClockExtract(secondary, secondaryde1);
	if (ret == cAtOk) ret = AtClockExtractorEnable(primary, cAtTrue);

	return ret;
	}


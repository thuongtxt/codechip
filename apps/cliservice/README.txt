This package is to support CLI server and client. The CLI server will listen on 
a specified port and interface for CLI requests. When clients send CLIs to server,
the server will execute CLIs and redirect all of outputs to client. 

Source files:
- AtCliClient.c: implement CLI server
- AtCliServer.c: implement CLI client
- AtCliService.h: header that contains public APIs that application will use. 
  Function usages are also described in this file.

Sample code:
- main/cliclient.c: example program for CLI client
- main/cliserver.c: example program for CLI server

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : cliserver.c
 *
 * Created Date: Jun 4, 2015
 *
 * Description : CLI server example code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <getopt.h>
#include <stdlib.h>

#include "atclib.h"
#include "AtStd.h"
#include "AtTokenizer.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtList.h"
#include "AtCliService.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* For simulation. The m_isSimulate must be set to cAtTrue in order to run in
 * simulation mode, the m_simulateProductCode variable must also be set to
 * correct product code of the device want to simulate */
static eBool  m_isSimulate          = cAtTrue;
static uint32 m_simulateProductCode = 0xF0031031;

/* For command processing */
static AtTextUI m_textUI = NULL;

/* Listen information */
static char m_ipAddress[32];
static char *pIpAddress = NULL;
static uint16 m_port = 5678;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice AddedDevice(void)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    return (numAddedDevices == 0) ? NULL : addedDevices[0];
    }

/* TODO: Return correct base address of FPGA */
static uint32 BaseAddressOfFpga(uint8 fpgaId)
    {
    AtUnused(fpgaId);
    return 0;
    }

static AtHal CreateHal(uint32 baseAddress)
    {
    if (m_isSimulate)
        return AtHalSimGlobalHoldNew(cAtHalSimDefaultMemorySizeInMbyte);

    /* TODO: user correct HAL for specific hardware platform. The following code is
     * just example */
    return AtHalIndirectDefaultNew(baseAddress);
    }

static uint32 ReadProductCode(uint32 baseAddress)
    {
    uint32 productCode;
    AtHal hal;

    if (m_isSimulate)
        return m_simulateProductCode;

    hal = CreateHal(baseAddress);
    productCode = AtProductCodeGetByHal(hal);
    AtHalDelete(hal);

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    return productCode;
    }

/* To setup HALs for all of IP Cores */
static void DeviceSetup(AtDevice device)
    {
    uint8 fpga_i;

    for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
        {
        uint32 fpgaBaseAddress = BaseAddressOfFpga(fpga_i);
        AtIpCoreHalSet(AtDeviceIpCoreGet(AddedDevice(), 0), CreateHal(fpgaBaseAddress));
        }
    }

static eAtRet DriverInit(void)
    {
    /* Create driver with one device */
    AtDriverCreate(1, AtOsalLinux());
    AtDriverDeviceCreate(AtDriverSharedDriverGet(), ReadProductCode(BaseAddressOfFpga(0)));
    if (AddedDevice() == NULL)
        return cAtErrorRsrcNoAvail;

    /* And setup it */
    DeviceSetup(AddedDevice());

    /* Optional: initialize device */
    /*AtDeviceInit(AddedDevice());*/

    return cAtOk;
    }

/* Save all HALs installed to this device */
static AtList AllDeviceHals(AtDevice device)
    {
    uint8 coreId;
    AtList hals;

    if (device == NULL)
        return NULL;

    hals = AtListCreate(0);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }

    return hals;
    }

/*
 * Sample device clean up function.
 *
 * Application create HALs and install them to AtDevice, so application must be
 * responsible for deleting these HALs. But, during device deleting, driver may
 * use HAL objects for special purposes, so HAL should be deleted after device
 * is deleted.
 *
 * The following sample code will backup all HALs that installed to device,
 * after deleting device, all of them are deleted
 */
static eAtRet DeviceCleanUp(AtDevice device)
    {
    AtList hals;

    if (device == NULL)
        return cAtOk;

    /* Save all HALs that are installed so far */
    hals = AllDeviceHals(device);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

    /* Delete all HALs */
    while (AtListLengthGet(hals) > 0)
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
    AtObjectDelete((AtObject)hals);

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    uint8 numDevices, i;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        DeviceCleanUp(device);
        }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);

    return cAtOk;
    }

static AtTextUI TinyTextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
    }

static void ParseConnection(char *connectionInfo)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(connectionInfo, ":");
    char *string;

    /* Bind address is optionally input, need to handle properly */
    string = AtTokenizerNextString(tokenizer);
    if (AtTokenizerHasNextString(tokenizer))
        {
        AtSprintf(m_ipAddress, "%s", string);
        m_port = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));
        pIpAddress = m_ipAddress;
        }

    /* Just port is input */
    else
        m_port = (uint16)AtStrToDw(string);
    }

static void OptionParse(int argc, char* argv[])
    {
    int opt;
    const char *cOptString = "hl:";

    while ((opt = getopt( argc, argv, cOptString)) != AtEof())
        {
        switch (opt)
            {
            case 'h':
                AtPrintc(cSevInfo, "Usage: %s -h -l <[listentAddress:]port>\r\n", argv[0]);
                exit(0);
                break;
            case 'l':
                ParseConnection(optarg);
                break;

            default:
                break;
            }
        }
    }

int main(int argc, char* argv[])
    {
    eAtRet ret;

    AtOsalSharedSet(AtOsalLinux());
    OptionParse(argc, argv);

    /* Initialize driver */
    ret = DriverInit();
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot initialize driver, ret = %s\r\n", AtRet2String(ret));

    AtCliServerStart(pIpAddress, m_port);

    /* Start CLI */
    AtCliStartWithTextUI(TinyTextUI());

    /* Cleanup */
    DriverCleanup(AtDriverSharedDriverGet());
    AtTextUIDelete(TinyTextUI());

    AtCliServerStop();

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : clirichclient.c
 *
 * Created Date: Aug 28, 2015
 *
 * Description : Rich CLI client
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>

#include "atclib.h"
#include "AtCliService.h"
#include "attypes.h"
#include "AtOsal.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/
#define cLoopbackAddress "127.0.0.1"
#define cDefaultPort 5678

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_ipAddress[32];
static char *pIpAddress = NULL;
static uint16 m_port = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *IpAddress(void)
    {
    if (pIpAddress == NULL)
        return cLoopbackAddress;
    return pIpAddress;
    }

static uint16 Port(void)
    {
    if (m_port == 0)
        return cDefaultPort;
    return m_port;
    }

static void ParseConnection(char *connectionInfo)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(connectionInfo, ":");
    AtSprintf(m_ipAddress, "%s", AtTokenizerNextString(tokenizer));
    if (AtTokenizerHasNextString(tokenizer))
        m_port = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));
    pIpAddress = m_ipAddress;
    }

static void OptionParse(int argc, char* argv[])
    {
    int opt;
    const char *cOptString = "hc:";

    while ((opt = getopt( argc, argv, cOptString)) != AtEof())
        {
        switch (opt)
            {
            case 'h':
                AtPrintc(cSevInfo, "Usage: %s -h -c <ipAddress:port>\r\n", argv[0]);
                exit(0);
                break;
            case 'c':
                ParseConnection(optarg);
                break;

            default:
                break;
            }
        }
    }

int main(int argc, char* argv[])
    {
    AtOsalSharedSet(AtOsalLinux());
    OptionParse(argc, argv);

    AtCliClientRichStart(IpAddress(), Port());

    return 0;
    }

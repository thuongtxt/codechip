/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI service
 *
 * File        : clistuff.c
 *
 * Created Date: Mar 3, 2017
 *
 * Description : Stuff to work with current build system
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHal.h"
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void AppInterruptTaskEnable(eBool enable);
void AppInterruptTaskEnable(eBool enable){AtUnused(enable);}
void AppPollingTaskEnable(eBool enable);
void AppPollingTaskEnable(eBool enable){AtUnused(enable);}
void AppPollingTaskPeriodInMsSet(uint32 periodMs);
void AppPollingTaskPeriodInMsSet(uint32 periodMs){AtUnused(periodMs);}
void AppForcePointerTaskEnable(eBool enable);
void AppForcePointerTaskEnable(eBool enable){AtUnused(enable);}
void AppForcePointerTaskPeriodInMsSet(uint32 periodMs);
void AppForcePointerTaskPeriodInMsSet(uint32 periodMs){AtUnused(periodMs);}
void CliClientExit(void);
void CliClientExit(void){}
void AppFmPmTaskEnable(eBool enable);
void AppFmPmTaskEnable(eBool enable){AtUnused(enable);}
void AppEyeScanTaskEnable(eBool enable);
void AppEyeScanTaskEnable(eBool enable){AtUnused(enable);}
void AppInterruptMessageSend(void);
void AppInterruptMessageSend(void){}
AtHal AppHalServer(void);
AtHal AppHalServer(void){return NULL;}

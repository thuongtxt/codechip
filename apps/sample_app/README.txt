1) How to compile the SDK in simulation mode

- In the apps/sample_app/main.c, just change m_isSimulate variable to cAtTrue.
- Standing at apps/sample_app, run: make
- After compiling, a new executable is created in apps/sample_app/. Just simply 
  run it. 

2) Sample scripts and common CLIs 
For a simple demo how PW service is configured by the ATSDK, two scripts are used:
- satop.script: For SAToP Pseudowire
- cesop.script: For CESoP Pseudowire

And for easier to understand the configuration sequence, only 1 PW is used in 
each script.

In the two scripts, only configuration CLIs are used, so the following text is 
to list all of common CLIs used to show configuration and status information.

- Show DS1/E1 configuration: show pdh de1 <de1List>
- Show DS1/E1 alarm: show pdh de1 alarm <de1List>
- Show DS1/E1 counters: show pdh de1 counters <de1List> <r2c/ro>

- Show PW configuration: show pw <pwList>
- Show PW alarm: show pw alarm <pwList>
- Show PW counters: show pw counters <pwList> <r2c/ro>

To see all of CLIs of each module, refer atsdk/docs/cli_manual

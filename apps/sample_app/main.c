/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : main.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description : Sample source code to bring up driver. For the sake of simplicity,
 *               and make it be easy to understand, this sample code is to
 *               handle driver with one device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* The SDK will use bit[31:28] = 0xF to see if device is running in simulation mode */
#define mSimulatedProductCode(realProductCode) ((realProductCode & ~(cBit31_28)) | (0xFUL << 28))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* For simulation. The m_isSimulate must be set to cAtTrue in order to run in
 * simulation mode, the m_simulateProductCode variable must also be set to
 * correct product code of the device want to simulate */
static eBool  m_isSimulate          = cAtTrue;
static uint32 m_simulateProductCode = 0x60150011;

/* For command processing */
static AtTextUI m_textUI = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice AddedDevice(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

/* TODO: Return correct base address of FPGA */
static uint32 BaseAddressOfFpga(uint8 fpgaId)
    {
    AtUnused(fpgaId);
    return 0;
    }

static void PCIeWrite(uint32 realAddress, uint32 value)
    {
    /* TODO: logic to deal with PCIe will be here */
    AtUnused(realAddress);
    AtUnused(value);
    }

static uint32 PCIeRead(uint32 realAddress)
    {
    /* TODO: logic to deal with PCIe will be here */
    AtUnused(realAddress);
    return 0xDEADCAFE;
    }

static AtHal CreateHal(uint32 baseAddress)
    {
    if (m_isSimulate)
        return AtHalSimCreateForProductCode(m_simulateProductCode, cAtHalSimDefaultMemorySizeInMbyte);

    return AtHalDefaultWithHandlerNew(baseAddress, PCIeWrite, PCIeRead);
    }

static uint32 ReadProductCode(uint32 baseAddress)
    {
    uint32 productCode;
    AtHal hal;

    if (m_isSimulate)
        return mSimulatedProductCode(m_simulateProductCode);

    hal = CreateHal(baseAddress);
    productCode = AtProductCodeGetByHal(hal);
    AtHalDelete(hal);

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    return productCode;
    }

/* To setup HALs for all of IP Cores */
static void DeviceSetup(AtDevice device)
    {
    uint8 fpga_i;

    for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
        {
        uint32 fpgaBaseAddress = BaseAddressOfFpga(fpga_i);
        AtIpCoreHalSet(AtDeviceIpCoreGet(AddedDevice(), 0), CreateHal(fpgaBaseAddress));
        }
    }

static eAtRet DriverInit(void)
    {
    /* Create driver with one device */
    AtDriverCreate(1, AtOsalLinux());
    AtDriverDeviceCreate(AtDriverSharedDriverGet(), ReadProductCode(BaseAddressOfFpga(0)));
    if (AddedDevice() == NULL)
        return cAtErrorRsrcNoAvail;

    /* And setup it */
    DeviceSetup(AddedDevice());

    /* Optional: initialize device */
    AtDeviceInit(AddedDevice());

    return cAtOk;
    }

/* Save all HALs installed to this device */
static AtList AllDeviceHals(AtDevice device)
    {
    uint8 coreId;
    AtList hals;

    if (device == NULL)
        return NULL;

    hals = AtListCreate(0);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }

    return hals;
    }

/*
 * Sample device clean up function.
 *
 * Application create HALs and install them to AtDevice, so application must be
 * responsible for deleting these HALs. But, during device deleting, driver may
 * use HAL objects for special purposes, so HAL should be deleted after device
 * is deleted.
 *
 * The following sample code will backup all HALs that installed to device,
 * after deleting device, all of them are deleted
 */
static eAtRet DeviceCleanUp(AtDevice device)
    {
    AtList hals;

    if (device == NULL)
        return cAtOk;

    /* Save all HALs that are installed so far */
    hals = AllDeviceHals(device);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

    /* Delete all HALs */
    while (AtListLengthGet(hals) > 0)
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
    AtObjectDelete((AtObject)hals);

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    uint8 numDevices, i;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        DeviceCleanUp(device);
        }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);

    return cAtOk;
    }

#ifdef AT_NONE_OS

static char *ReadLine(char *buffer, int size)
    {
    extern char *gets(char *s);
    char *line = gets(buffer);
    line[AtStrlen(line)] = '\n';
    return line;
    }

static AtTextUI TextUICreate(void)
    {
    return AtTinyTextUIWithReadLineNew(ReadLine);
    }

#else
static AtTextUI TextUICreate(void)
    {
    return AtDefaultTinyTextUINew();
    }
#endif

static AtTextUI TinyTextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = TextUICreate();
    return m_textUI;
    }

/*
 * Application specific command (for demo purpose).
 * Example: hello name Arrive Technologies
 *          + hello name: command name which has two levels
 *          + Arrive: value of parameter 1
 *          + Technologies: value of parameter 2
 *          + Result on console: Hello Arrive Technologies
 */
static eBool CmdHelloWith2Params(char argc, char **argv)
    {
    AtPrintc(cSevInfo, "Hello %s %s\r\n", argv[0], argv[1]);
    AtUnused(argc);
    return cAtTrue;
    }

static void InstallExtendCommands(void)
    {
    AtTinyTextUICmdAdd(TinyTextUI(), "hello name", CmdHelloWith2Params, 2, 2);
    }

int main(int argc, char* argv[])
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    /* Initialize driver */
    ret = DriverInit();
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot initialize driver, ret = %s\r\n", AtRet2String(ret));

    /* Start CLI */
    InstallExtendCommands();
    AtCliStartWithTextUI(TinyTextUI());

    /* Cleanup */
    DriverCleanup(AtDriverSharedDriverGet());
    AtTextUIDelete(TinyTextUI());

    return 0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : ep5.h
 * 
 * Created Date: Nov 7, 2013
 *
 * Description : To work with EP5 platform
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _EP5_H_
#define _EP5_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Ep5PlatformFinalize(void);
uint32 Ep5PlatformFpgaBaseAddress(void);
AtHal Ep5PlatformPdhAdaptHal(void);

#endif /* _EP5_H_ */


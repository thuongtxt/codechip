/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : ep5.c
 *
 * Created Date: Nov 7, 2013
 *
 * Description : To work with EP5
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ep5.h"
#include "AtHalMemoryMapper.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHalMemoryMapper m_fpgaMemoryMapper     = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return 0xd0000000;
    }

static uint32 MemorySize(void)
    {
    return 64 << 21;
    }

static AtHalMemoryMapper FpgaMemoryMapper(void)
    {
    if (m_fpgaMemoryMapper == NULL)
        {
        m_fpgaMemoryMapper = AtHalMemoryMapperLinuxNew(BaseAddress(), MemorySize());
        AtHalMemoryMapperMap(m_fpgaMemoryMapper);
        }

    return m_fpgaMemoryMapper;
    }

eAtRet Ep5PlatformFinalize(void)
    {
    AtHalMemoryMapperDelete(m_fpgaMemoryMapper);
    return cAtOk;
    }

uint32 Ep5PlatformFpgaBaseAddress(void)
    {
    return AtHalMemoryMapperBaseAddress(FpgaMemoryMapper());
    }

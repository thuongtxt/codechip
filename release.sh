#!/bin/bash

projectHome=`pwd`
releaseHome="$projectHome/release"
releaseFolder="$releaseHome/atsdk"
compress=0

# TODO: Change this to 0 if Arrive Component is not released
# Note, APS may not need to release for some products. Need to update correspond 
# customer branch
releaseComponent=0
releaseAps=$releaseComponent
releaseBer=$releaseComponent

# Cleanup
make -C "$projectHome" clean

# Copy driver source code
releaseSourceFolder="$releaseFolder/atsdk"
`rm -fR $releaseSourceFolder`
`mkdir -p $releaseSourceFolder`
`mkdir -p $releaseSourceFolder/driver`
`cp -fR $projectHome/driver/include $releaseSourceFolder/driver`
`cp -fR $projectHome/driver/src $releaseSourceFolder/driver`
`rm -fR $releaseSourceFolder/driver/src/generic/framerelay`
`rm -fR $releaseSourceFolder/driver/src/generic/ima`
`rm -fR $releaseSourceFolder/driver/include/att`
`rm -fR $releaseSourceFolder/driver/src/generic/att`

# If component is not released, need to remove related source files
if [ $releaseBer == 0 ]
then
	`rm -fR $releaseSourceFolder/driver/src/generic/ber/*Soft*`
	`rm -fR $releaseSourceFolder/driver/src/implement/default/ber/*`
	`cp -f $projectHome/driver/src/implement/default/ber/ThaModuleBer.h $releaseSourceFolder/driver/src/implement/default/ber/`
fi

if [ $releaseAps == 0 ]
then
	`rm -fR $releaseSourceFolder/driver/src/generic/aps/*Soft*`
    `rm -fR $releaseSourceFolder/driver/src/implement/default/aps/*Soft*`
fi

# Copy component files
if [ $releaseComponent == 1 ]
then
    `mkdir -p $releaseSourceFolder/components`
    `cp -fR $projectHome/components/include $releaseSourceFolder/components`
    
    if [ $releaseAps == 0 ]
    then
    	`rm -fR $releaseSourceFolder/components/include/aps`
    	`rm -fR $releaseSourceFolder/components/src/aps`
    fi
fi

# Copy customers folder
`cp -fR $projectHome/customers $releaseSourceFolder`
`rm -fR $releaseSourceFolder/customers/archive`
`rm -fR $releaseSourceFolder/customers/Makefile.*`

# TODO: Check and keep only necessary code
`rm -fR $releaseSourceFolder/customers/src/At*Bdcom*.*`
`rm -fR $releaseSourceFolder/customers/src/AtDataX*.*`
`rm -fR $releaseSourceFolder/customers/src/AtZte*Ptn*.*`
`rm -fR $releaseSourceFolder/customers/src/AtZte*Router*.*`
`rm -fR $releaseSourceFolder/customers/src/AtLoop*.*`
`rm -fR $releaseSourceFolder/customers/src/AtFhn*.*`
`rm -fR $releaseSourceFolder/customers/src/AtCisco*.*`
`rm -fR $releaseSourceFolder/customers/src/At*FiberLogic*.*`
`rm -fR $releaseSourceFolder/customers/include/AtDataX.*`
`rm -fR $releaseSourceFolder/customers/include/AtBdcom.*`
`rm -fR $releaseSourceFolder/customers/include/AtZte*Ptn*.*`
`rm -fR $releaseSourceFolder/customers/include/AtZte*Router*.*`
`rm -fR $releaseSourceFolder/customers/include/AtFhn*.*`
`rm -fR $releaseSourceFolder/customers/include/AtLoop*.*`
`rm -fR $releaseSourceFolder/customers/include/AtCisco*.*`
`rm -fR $releaseSourceFolder/customers/include/At*FiberLogic*.*`
`rm -fR $releaseSourceFolder/customers/example/loop`
`rm -fR $releaseSourceFolder/customers/example/loop`

# Copy platform source code
`cp -fR $projectHome/platform $releaseSourceFolder`
`rm -fR $releaseSourceFolder/platform/Makefile*`
`rm -fR $releaseSourceFolder/platform/ep6`
`rm -fR $releaseSourceFolder/platform/diag`
`rm -fR $releaseSourceFolder/platform/ep5`
`rm -fR $releaseSourceFolder/platform/hal/AtHalIndirectEp.c`
`rm -fR $releaseSourceFolder/platform/hal/AtHalMemoryMapper*.*`
`rm -fR $releaseSourceFolder/platform/liu`
`rm -fR $releaseSourceFolder/platform/osal/debug`
`rm -fR $releaseSourceFolder/platform/osal/windows`
`rm -fR $releaseSourceFolder/platform/osal/amx`
`rm -fR $releaseSourceFolder/platform/osal/vxworks`

# Normally, we do not release HAL TCP
`rm -fR $releaseSourceFolder/platform/connection`
`rm -fR $releaseSourceFolder/platform/hal/tcp`
`rm -fR $releaseSourceFolder/platform/include/*HalTcp*`

# We also do not release HAL that works with testbench
`rm -fR $releaseSourceFolder/platform/hal/testbench`

# Copy debug util source code
`mkdir -p $releaseSourceFolder/debugutil`
`cp -fR $projectHome/debugutil $releaseSourceFolder`
`rm -f $releaseSourceFolder/debugutil/Makefile.*`

# Copy CLI source code
`mkdir -p $releaseSourceFolder/cli`
`mkdir -p $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/cmd $releaseSourceFolder/cli`
`rm -fR $releaseSourceFolder/cli/cmd/clish`
`rm -fR $releaseSourceFolder/cli/cmd/platform/ep6c1`
`rm -fR $releaseSourceFolder/cli/cmd/epapp`
`cp -fR $projectHome/cli/include $releaseSourceFolder/cli`
`cp -fR $projectHome/cli/start $releaseSourceFolder/cli`
`cp -fR $projectHome/cli/libs/include $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/listener $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/showtable $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/exprcalc $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/stdtable $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/text-ui $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/idparser $releaseSourceFolder/cli/libs`
`cp -fR $projectHome/cli/libs/attcl $releaseSourceFolder/cli/libs`
`rm -fR $releaseSourceFolder/cli/libs/exprcalc`
`rm -fR $releaseSourceFolder/cli/libs/text-ui/src/AtClishTextUI.c`
`rm -fR $releaseSourceFolder/cli/cmd/platform/liu`
`rm -fR $releaseSourceFolder/cli/cmd/att`
`rm -fR $releaseSourceFolder/cli/cmd/lab`

# TODO: Consider if TCL lib should be removed because of the target application cannot support
`rm -fR $releaseSourceFolder/cli/libs/attcl/src/jim`
`rm -fR $releaseSourceFolder/cli/libs/attcl/src/AtDefaultTcl.c`
`rm -fR $releaseSourceFolder/cli/libs/attcl/src/AtJimTcl.c`
`rm -fR $releaseSourceFolder/cli/libs/attcl/src/AtJimTcl.h`

# TODO: Just keep necessary foders
`rm -fR $releaseSourceFolder/cli/cmd/customers/bdcom`
`rm -fR $releaseSourceFolder/cli/cmd/customers/loop`
`rm -fR $releaseSourceFolder/cli/cmd/customers/zte_ptn`
`rm -fR $releaseSourceFolder/cli/cmd/customers/zte_router`
`rm -fR $releaseSourceFolder/cli/cmd/customers/cisco`
`rm -fR $releaseSourceFolder/cli/cmd/customers/alu`
`rm -fR $releaseSourceFolder/cli/cmd/customers/fiberlogic`

# Copy application source code
`mkdir -p $releaseSourceFolder/apps`
`cp -fR $projectHome/apps/sample_app $releaseSourceFolder/apps`
`rm -fR $releaseSourceFolder/apps/sample_app/Makefile.*`

# TODO: Copy sample code
`mkdir -p $releaseSourceFolder/apps/samplecode`
`cp -fR $projectHome/apps/samplecode/aps $releaseSourceFolder/apps/samplecode/`
`cp -fR $projectHome/apps/samplecode/clock $releaseSourceFolder/apps/samplecode/`
`cp -fR $projectHome/apps/samplecode/common $releaseSourceFolder/apps/samplecode/`
`cp -fR $projectHome/apps/samplecode/pw $releaseSourceFolder/apps/samplecode/`
`cp -fR $projectHome/apps/samplecode/ram $releaseSourceFolder/apps/samplecode/`
`cp -fR $projectHome/apps/samplecode/xc $releaseSourceFolder/apps/samplecode/`

# Copy util source code
`cp -fR $projectHome/util $releaseSourceFolder`
`rm -f $releaseSourceFolder/util/Makefile.*`

# Copy makefiles
`cp -f $projectHome/Makefile $releaseSourceFolder`
`cp -f $projectHome/Rules.Make $releaseSourceFolder`
`cp -f $projectHome/Rules.d $releaseSourceFolder`

# Copy tools
`mkdir -p $releaseSourceFolder/tools/`
`cp -f $projectHome/tools/eyescan $releaseSourceFolder/tools/`

# Copy documents
docsFolder="$releaseFolder/docs"
`mkdir -p $docsFolder`
`cp -f $projectHome/driver/docs/af6_sdk_manual.chm $docsFolder`
`cp -f $projectHome/driver/docs/Release_note.txt $docsFolder`
`cp -f $projectHome/driver/docs/af6_sdk_cli_manual.pdf $docsFolder`
`cp -f $projectHome/driver/docs/af6_sdk_programming_guide.pdf $docsFolder`

# Generate CLI table
`cp -fR $projectHome/cli/Rules.d $releaseSourceFolder/cli/Rules.d`
make -C "$releaseSourceFolder/" atclitable "GENERATE_CLI_MANUAL=no"
make -C "$releaseSourceFolder/" clean
sleep 10
`rm -fR $releaseSourceFolder/cli/libs/text-ui/confgen`
`cp -fR $projectHome/cli/Rules_release.d $releaseSourceFolder/cli/Rules.d`

# Strip all register descriptions
`./mass_replace.py -d $releaseSourceFolder/`

# Do not need the *.conf files
`find -P $releaseSourceFolder/cli/cmd -iname "*.conf" -exec rm {} +`

# TODO: Should remove specific source code of other customers, just keep neccessary folders
# Remove platform customer specific source code
`rm -fR $releaseSourceFolder/platform/customers/*`
#`cp -fR $projectHome/platform/customers/xxx $releaseSourceFolder/platform/customers/`
`rm -fR $releaseSourceFolder/platform/include/AtHalAls.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalAlu.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalBdcom.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalBdcomMdio.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalFhn.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalFhnSpi.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalFhnE1.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalFhw.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalLoop.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalZteMw.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalZtePtn.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalZteRouter.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalFiberLogic.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalDataX.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalCisco.h`
`rm -fR $releaseSourceFolder/platform/include/AtHalMemoryMapper.h`
`rm -fR $releaseSourceFolder/platform/include/AtOsalLinuxDebug.h`
`rm -fR $releaseSourceFolder/platform/hal/AtHalIndirect16Bit.c`

# TODO: Remove all of applicable products, the list can be easily updated by command:
# ls driver/src/implement/codechip/* -d | column -c 1
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60000031`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60001031`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60020011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030022`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030023`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030051`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030080`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030081`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030101`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030111`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031021`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031021Ep`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031031`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031031Ep`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031032`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031033`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031035`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031071`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031131`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60035011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60035021`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60050061`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60060011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60070013`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60070023`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60070041`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60070051`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60070061`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60071011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60071021`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60071032`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60091023`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60091132`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60091135`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha600a0011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60200011`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60220031`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60220041`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60240021`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha61031031`
`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha65031032`

# Product 60031031 is so common, just remove files relate to component, not the whole codechip
if [ $releaseBer == 0 ]
then
	`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031031/man/Tha60031031Device.c`
    `rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031031/ber`
    `rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha600a0011/man/Tha600a0011Device.c`
    `rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60070023/man/Tha60070023Device.c`
    `rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60030051/man/Tha60030051Device.c`
    `rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60035021/man/Tha60035021Device.c`
    `rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60071021/man/Tha60071021Device.c`
fi 

if [ $releaseAps == 0 ]
then
	`rm -fR $releaseSourceFolder/driver/src/implement/codechip/Tha60031031Ep/man/Tha60031031EpDevice.c`
fi

# Compile components
atcomponents="atcomponents.o"
if [ $releaseComponent == 1 ]
then
	params="$projectHome/components AT_COMPONENTS_OBJ=$projectHome/components/$atcomponents"
	make -C "$projectHome/components" clean
	make -C $params
	`cp -f $projectHome/components/$atcomponents $releaseSourceFolder/components/`
fi

# Try to make to see if it works
buildDir="$releaseSourceFolder/.build"
echo "$releaseSourceFolder/apps/sample_app"
make -C "$releaseSourceFolder/apps/sample_app" clean

if [ $releaseComponent == 1 ]
then
	params="$releaseSourceFolder/apps/sample_app AT_COMPONENTS_OBJ=$releaseSourceFolder/components/$atcomponents"
else
    params="$releaseSourceFolder/apps/sample_app" AT_COMPONENTS_LIB=""
fi

make -j 10 -C $params

if [ -f "$buildDir/apps/sample_app/sampleapp" ] 
then
	# Clean all
	make -C "$releaseSourceFolder/apps/sample_app" clean
	`rm -fR $buildDir`
	sleep 3
	
	# Copy library
	`rm -fr $releaseSourceFolder/components/$atcomponents`
	`cp -fR $projectHome/components/customers/loop/atcomponents_powerpc_85xxDP.o $releaseSourceFolder/components/`
	
	# Remove stuff
	`rm -fR $releaseSourceFolder/platform/stuff`
	
	# Compress
	if [ $compress == 1 ]
	then
		date=`date +%b_%d_%Y` # Example: Oct_26_2012
	    output="$releaseHome/atsdk_$date.zip"
	    zip -r $output $releaseFolder 
	    `rm -fR $releaseFolder`
	    
	    # Done
        echo "Release DONE with $output"
	else
        echo "Release DONE with $releaseHome"
	fi
	
else
    echo "Release FAIL! Cannot compile executable file"
    exit 1
fi

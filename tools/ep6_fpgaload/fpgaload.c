/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : fpgaload.c
 *
 * Created Date: Nov 30, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/time.h>
#include "stdlib.h"
#include "stdio.h"

#include "devctrl.h"

/*--------------------------- Define -----------------------------------------*/
#define cBit0          0x00000001L
#define cBit1          0x00000002L
#define cBit2          0x00000004L
#define cBit3          0x00000008L
#define cBit4          0x00000010L
#define cBit5          0x00000020L
#define cBit6          0x00000040L
#define cBit7          0x00000080L
#define cBit8          0x00000100L
#define cBit9          0x00000200L
#define cBit10         0x00000400L
#define cBit11         0x00000800L
#define cBit12         0x00001000L
#define cBit13         0x00002000L
#define cBit14         0x00004000L
#define cBit15         0x00008000L

#define nCONFIG        cBit1
#define CONF_DONE_SPI  cBit15
#define nSTATUS_SPI    cBit14
#define MOD_SEL_SPI    cBit0
#define CS_SIGNAL_SPI  cBit3
#define DCLOCK         cBit2


#define cHalFpgaProgramTime         5           /* Second */
#define cHalFpgaProgramTime1        4000000

#define cHalStratix3FpgaBufSize         4048 * 2
#define cEpStratix3FpgaDevCfgReg        0x018
#define cHalEpPhysicalAddr      0x80000000      /* Physical address on CPU bus */
#define cHalEpMemMapSize        0x59000000      /* 1408M */
#define cHalEpDevName           "ep"
#define cHalEpDevMajorNum       80              /* Major number of EP */

/*--------------------------- Local typedefs ---------------------------------*/
typedef unsigned long long  uint64;
typedef unsigned int        uint32;
typedef unsigned short      uint16;
typedef unsigned char       uint8;
typedef signed long int     int32;
typedef signed short        int16;
typedef signed char         int8;
typedef unsigned char       atbool;
#ifndef NULL
#define NULL                ((void*)0)
#endif

#define cAtFalse            0
#define cAtTrue             1

/*--------------------------- Macros -----------------------------------------*/
#define mEpReadDw(addr, pvalue) \
                do{ \
                *pvalue          = *((uint32 *)((addr << 2) + epVirtAddr));\
                __asm__("sync");\
                }while(0)
#define mEpWriteDw(addr, value) \
                do{ \
                *((uint32 *)((addr << 2) + epVirtAddr)) = value;\
                __asm__("sync");\
                }while(0)

#define (AtGetAddr)slot, dev, addr             \
                do{                             \
                addr += slot*0x10000000 + dev*0x01000000;\
                /*printf("mAtGetAddr: addr = 0x%08x\n", addr);*/\
                }while(0);

#define mPrintError(FMT, PARMS...)     printf("ERROR: "FMT"\n", ## PARMS)
#define mPrintWarning(FMT, PARMS...)   printf("WARNING: "FMT"\n", ## PARMS)
#define mPrintInfo(FMT, PARMS...)      printf(FMT"\n", ## PARMS)
#define mPrint(FMT, PARMS...)          printf(FMT"\n", ## PARMS)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 epVirtAddr;
static tDevDescriptor epDevDescr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

/*-----------------------------------------------------------------------------
Function Name:  HalEpWdRead

Description  :  Read value of register of any device on Ep in word

Input        :  inSlotId        - Slot identifier of device
                inDevId         - Device identifier
                inAddr          - Address of register

Output       :  pValue          - Value of register

Return Code  :  cAtTrue         - If reading is successful
                cAtFalse            - If reading is failed
------------------------------------------------------------------------------*/
static eBool HalEpWdRead(uint8 slotId, uint8 devId, uint32 addr, uint16 *pValue)
    {
    uint32   dwvalue;

    (AtGetAddr)slotId, devId, addr;

    mEpReadDw(addr, &dwvalue);
    __asm__("sync");

    *pValue = (uint16)dwvalue;

    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
Function Name:  HalEpWdWrite

Description  :  Write value to register of any device on Ep in word

Input        :  inSlotId        - Slot identifier of device
                inDevId         - Device identifier
                inAddr          - Address of register
                value           - Value to write

Output       :  None

Return Code  :  cAtTrue         - If writting is successful
                cAtFalse            - If writting is failed
------------------------------------------------------------------------------*/
static eBool HalEpWdWrite(uint8 slotId, uint8 devId, uint32 addr, uint16 value)
    {
    uint32   dwvalue;

    (AtGetAddr)slotId, devId, addr;

    dwvalue = (uint32)value;
    mEpWriteDw(addr, dwvalue);
    __asm__("sync");

    return cAtTrue;
    }

static atbool Ep6Stratix3FpgaLoad(char* fileName)
    {
    uint16 wTemp;
    uint16 wTemp1;
    uint16 wrdata;
    int i;
    uint32 dIndex;
    int e = 1;
    FILE        *pFile = NULL;
    int         length;
    atbool       result;
    uint8        *fpgaBuffer;
    struct timeval  sTime;
    struct timeval  eTime;
    int             times;

    wTemp1      = 0;
    fpgaBuffer = malloc(cHalStratix3FpgaBufSize);

    if (fpgaBuffer == NULL)
        {
        return cAtFalse;
        }

    /* Read/write the configuration register */
#define (AtRead)pdata  HalEpWdRead(1, 5, cEpStratix3FpgaDevCfgReg, pdata)
#define (AtWrite)data  HalEpWdWrite(1, 5, cEpStratix3FpgaDevCfgReg, data)
#define MAX_TIME        10
#define MAX_BYTE        0x32000

    for(times = 0; times < MAX_TIME ;times++)
        {
        /* Open file */
        e = 1;
        pFile = fopen(fileName, "rb");
        if (!pFile)
            {
            mPrintError("Can not open FPGA file!!!");
            free(fpgaBuffer);
            return cAtFalse;
            }

        result = cAtTrue;

        /* Chip-select */
        (AtRead)&wrdata;
        __asm__("sync");
        /*(AtWrite)(wrdata & ~CS_SIGNAL_SPI) | DCLOCK;*/
        (AtWrite)(wrdata & ~CS_SIGNAL_SPI);
        __asm__("sync");

        /* Pull high MOD_SEL */
        (AtRead)&wrdata;
        __asm__("sync");
        (AtWrite)(wrdata | MOD_SEL_SPI);
        __asm__("sync");

        /* Pull low  nCONFIG for reset */
        (AtRead)&wrdata;
        __asm__("sync");
        (AtWrite)(wrdata & ~nCONFIG);
        __asm__("sync");

        /* Waiting for nSTATUS go low or wait for tCFG */
        /*sTime.tv_sec  = 0;
        sTime.tv_usec = 0;*/
        (AtRead) &wTemp;
        gettimeofday(&sTime, NULL);
        while ( (wTemp & nSTATUS_SPI))
            {
            (AtRead) &wTemp;
            __asm__("sync");
            gettimeofday(&eTime, NULL);
            /*if((eTime.tv_sec - sTime.tv_sec) >= cHalFpgaProgramTime)
                {
                break;
                }*/
            if(((eTime.tv_sec * 1000 + eTime.tv_usec / 1000) -
                (sTime.tv_sec * 1000 + sTime.tv_usec / 1000)) >= cHalFpgaProgramTime)
                {
                break;
                }
            }

        (AtRead) &wTemp;
        if (wTemp & nSTATUS_SPI)
            {
            mPrintError("\t nSTATUS does not go low");
            result = cAtFalse;
            }
        else if ( wTemp & CONF_DONE_SPI )
            {
            mPrintError("\t CONF_DONE does not go low");
            result = cAtFalse;
            }
        else
            {
            /*(AtDebug)cDebugNormal, "\tnSTATUS go low: Done\n";
            (AtDebug)cDebugNormal, "\tCONF_DONE go low: Done\n";*/

            /* Wait for a while */
            i = 1;
            while(i <= cHalFpgaProgramTime1)
                {
                i++;
                }
            /* Pull high  nCONFIG for configuring */
            (AtRead)&wTemp;
            __asm__("sync");
            (AtWrite)(wTemp | nCONFIG);
            __asm__("sync");

            /* Waiting for nSTATUS go high */

            /*sTime.tv_sec  = 0;
            sTime.tv_usec = 0;*/
            (AtRead) &wTemp;
            gettimeofday(&sTime, NULL);
            while (!(wTemp & nSTATUS_SPI))
                {
                (AtRead) &wTemp;
                __asm__("sync");

                /*if((eTime.tv_sec - sTime.tv_sec) >= cHalFpgaProgramTime)
                    {
                    break;
                    }*/
                gettimeofday(&eTime, NULL);
                if(((eTime.tv_sec * 1000 + eTime.tv_usec / 1000) -
                    (sTime.tv_sec * 1000 + sTime.tv_usec / 1000)) >= cHalFpgaProgramTime)
                    {
                    break;
                    }
                }

            (AtRead) &wTemp;
            if (!(wTemp & nSTATUS_SPI))
                {
                mPrintError("\t nSTATUS does not go high");
                result = cAtFalse;
                }
            else
                {
                /*(AtDebug)cDebugNormal,"\tnSTATUS go high: Done\n";
                (AtDebug)cDebugNormal,"\tWriting the high CS_SIGNAL\n";*/

                (AtRead) &wTemp;
                __asm__("sync");
                (AtWrite)(wTemp | CS_SIGNAL_SPI);
                __asm__("sync");

                (AtRead) &wTemp;
                __asm__("sync");

                /* Load data into FPGA */
                mPrint("Loading FPGA data, wait ....");

                do{
                    length = fread(fpgaBuffer, 1, cHalStratix3FpgaBufSize, pFile);
                    for (dIndex = 0; dIndex < length; dIndex += 1 )
                        {
                        /* Write data */
                        wTemp = (uint16)fpgaBuffer[dIndex];
                        /*mEpWriteDw(0, wTemp);*/
                        mEpWriteDw(0x15000019, wTemp);
                        }
                }while((length > 0) /*&& (e > 0)*/);

                (AtRead) &wTemp;
                __asm__("sync");
                (AtWrite)(wTemp & ~CS_SIGNAL_SPI & ~MOD_SEL_SPI);
                __asm__("sync");

                if(!e)
                    {
                    fclose(pFile);
                    continue;
                    }

                /* Wait for CONF_DONE go high */
                /*(AtDebug)cDebugNormal,"\n\tWait for CONF_DONE go high\n";*/
                i = 1;
                while(i <= cHalFpgaProgramTime1)
                    {
                    i++;
                    }

                /*sTime.tv_sec  = 0;
                sTime.tv_usec = 0;*/
                (AtRead) &wTemp;
                gettimeofday(&sTime, NULL);
                for (i = 0; !(wTemp & CONF_DONE_SPI); i++)
                    {
                    (AtRead) &wTemp;
                    __asm__("sync");
                    gettimeofday(&eTime, NULL);
                    if(((eTime.tv_sec * 1000 + eTime.tv_usec / 1000) -
                        (sTime.tv_sec * 1000 + sTime.tv_usec / 1000)) >= cHalFpgaProgramTime)
                        {
                        break;
                        }
                    }
                (AtRead) &wTemp;
                if (wTemp & CONF_DONE_SPI)
                    {
                    /*(AtDebug)cDebugNormal, "\tCONF_DONE is high\n";*/
                    result = cAtTrue;

                    (AtRead) &wTemp;
                    __asm__("sync");
                    (AtWrite)(wTemp & ~CS_SIGNAL_SPI & ~MOD_SEL_SPI);
                    __asm__("sync");
                    }
                else
                    {
                    mPrintError("\t CONF_DONE does not go high");
                    result = cAtFalse;
                    }
                }
            }
        fclose(pFile);
        if(result == cAtTrue)
            {
            break;
            }
        }
#undef      mAtRead
#undef      mAtWrite

    free(fpgaBuffer);

    return result;
    }

static atbool Init(void)
    {
    atbool ret;

    ret = cAtTrue;

    if (DevOpen(cHalEpDevName, cHalEpPhysicalAddr, cHalEpMemMapSize, cHalEpDevMajorNum, &epDevDescr) == 0)
        {
        ret = cAtFalse;
        }
     else
        {
        epVirtAddr = (unsigned long)epDevDescr.pVirtualAddr;
        }

    return ret;
    }

static atbool Cleanup(void)
    {
    return DevClose(&epDevDescr);
    }

/*
 * Main process
 * @param argc
 * @param argv[0] FPGA file
 * @return
 */
int main(int argc, char* argv[])
    {
    if (argc <= 1)
        {
        mPrint("Usage : fpgaload <FPGA file>");
        return EXIT_SUCCESS;
        }

    if (!Init())
        {
        mPrintError("Init");

        Cleanup();
        return EXIT_FAILURE;
        }

    if (Ep6Stratix3FpgaLoad(argv[1]) == cAtTrue)
        {
        mPrint("Loading FPGA is DONE");
        }
    else
        {
        mPrint("Loading FPGA is FAILED");
        }

    Cleanup();

    return EXIT_SUCCESS;
    }

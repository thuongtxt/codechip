#!/usr/bin/python

import sys
import os
import shutil
import re
import subprocess
import zipfile

version_major = 0
version_minor = 0
tool_directory = ''
day = 0
month = 0
year = 0

class bcolors:
    WARNING = '\033[93m'
    NORMAL = '\033[0m'
    INFO = '\033[92m'

def PrintWarning(msg):
    print (bcolors.WARNING + msg + bcolors.NORMAL)
    
def PrintInfo(msg):
    print (bcolors.INFO + msg + bcolors.NORMAL)
    
def IsAlphaOrDigit(char):
    if char.isalpha():
        return True;
    
    if char.isdigit():
        return True;
    
    return False
    
def FileNameIsAcceptable(fileName):
    if re.match("^[A-Za-z0-9]*$", fileName):
        return True
        
    return False

def DeriveAcceptableFileNameFromRealName(fileName):
    acceptableName = ''
    for i, c in enumerate(fileName):
        if IsAlphaOrDigit(c):
            acceptableName = acceptableName + c;
        if c == '.':
            break
        
    return acceptableName

def CreateAcceptableTmpFile(filePath, fileName):
    acceptName = DeriveAcceptableFileNameFromRealName(fileName)
    shutil.copy(filePath + "/" + fileName + '.hex', filePath + "/" + acceptName + '.hex')
    return acceptName;

def CreateZipPacket(filePath, inputFileName, outputFileName):
    zipFile = inputFileName + '.zip'
    tmpZipFile = inputFileName + '.tmp'
    zipf = zipfile.ZipFile(tmpZipFile, 'w')
    zipf.write(outputFileName + '.fpga')
    zipf.write(outputFileName + '.ver')
    zipf.close()
    
    # Move temp file to destination folder
    if os.getcwd() != os.path.dirname(os.path.abspath(filePath + "/" + tmpZipFile)):
        shutil.copy(tmpZipFile, filePath + "/" + tmpZipFile)
        os.remove(tmpZipFile)

    # Delete existing file
    if os.path.exists(filePath + '/' + zipFile):
        os.remove(filePath + '/' + zipFile)
        
    os.rename(filePath + '/' + tmpZipFile, filePath + '/' + zipFile)
    
    # Remove temp files    
    os.remove(outputFileName + ".h")
    os.remove(outputFileName + ".fpga")
    os.remove(outputFileName + ".ver")
    
def FailWithMessage(createdTmpFile, tmpFile, msg):
    PrintWarning(msg)
    if createdTmpFile:
        os.remove(tmpFile)
        
    return False

def CorrectDayInHeaderFile(fileName):
    global day
    fileWithExtend = fileName + ".h"
    
    file = open(fileWithExtend,'r+');
    copy = open(fileWithExtend + ".tmp",'wt');
    lineNumber = 0
    for line in file:
        if lineNumber == 7:
            copy.write("#define " + fileName.upper() + "_VER_2 0x" + str(day) + "\r\n")
        else:
            copy.write(line)
        lineNumber = lineNumber + 1
    
    file.close()
    copy.close()
    os.remove(fileWithExtend)
    os.rename(fileWithExtend + ".tmp", fileWithExtend)
    
def Hex2Fpga(inputFileFullName, outputFileName):
    global year
    global month
    global tool_directory
    
    inputFilePath, inputFileName = os.path.split(inputFileFullName)
    createdTmpFile = False
    if not inputFilePath:
        inputFilePath = '.'
        
    # Remove .hex
    inputFileName = inputFileName[:inputFileName.index(".")]
    
    if not FileNameIsAcceptable(inputFileName):
        tempFileName = CreateAcceptableTmpFile(inputFilePath, inputFileName)
        createdTmpFile = True;
    else:
        tempFileName = inputFileName
           
    if len(outputFileName) == 0:
        outputFileName = tempFileName;
        
    # Generate
    tempFullFileName = inputFilePath + "/" + tempFileName + '.hex'
    command = tool_directory + '/conv_fpga_hex_define_version_linux ' + tempFullFileName + ' ' + outputFileName + ' ' + str(year) + ' ' + str(month)
    print (command)
    os.system(command)
    
    if not os.path.exists(outputFileName + ".h"):
        return FailWithMessage(createdTmpFile, tempFullFileName, "Can not generate file .h")
        
    CorrectDayInHeaderFile(outputFileName)
    command = tool_directory + '/fpga_parser_linux ' + outputFileName + '.h'
    print (command)
    os.system(command)
    if not os.path.exists(outputFileName + ".fpga") or not os.path.exists(outputFileName + ".ver"):
        os.remove(outputFileName + ".h")
        return FailWithMessage(createdTmpFile, tempFullFileName, "Can not generate file .fpga or .ver")
 
    CreateZipPacket(inputFilePath, inputFileName, outputFileName)
    if createdTmpFile:
        os.remove(tempFullFileName)
    
    PrintInfo("Generated packet: " + inputFilePath + "/" + inputFileName + ".zip")    
    return True

def InputFileIsExist(inputFileFullName):
    if not os.path.exists(inputFileFullName):
        return False
    
    return True

def InputFileNameIsInRightFormat(inputFileFullName):
    global version_major
    global version_minor
    inputFilePath, inputFileName = os.path.split(inputFileFullName)
    match = re.findall("^af6ali0011_stm16ces_v([1-9]+)(_([0-9]+))?\.hex$", inputFileName)
    if not match:
        return False
    
    version_major = match[0][0]
    version_minor = match[0][2]
    
    if not version_minor:
        version_minor = 0
    
    return True

def PrintUsage():
    print ('+===================================================================+')
    print ('| Syntax: <path>/hex2fpga.py <input_file.hex> <year> <month> <day>  |')
    print ('|                                                                   |')
    print ('| Format input file: af6ali0011_stm16ces_v' + bcolors.INFO + 'xxx' + bcolors.NORMAL + '_' + bcolors.INFO + 'yyy' + bcolors.NORMAL + '.hex               |')
    print ('|       xxx: Major version number, it must exist                    |')
    print ('|       yyy: Minor version number, it is OPTIONAL, 0 as default     |')
    print ('|                                                                   |')
    print ('| Example: ./hex2fpga.py af6ali0011_stm16ces_v7_9.hex 15 03 27      |')
    print ('+===================================================================+')
        
def main(argv):
    outputFileName = ""
    inputFileFullName = ""
    global tool_directory
    global year
    global month
    global day
    
    tool_directory, inputFileName = os.path.split(argv[0])
    if len(argv) >= 2:
        inputFileFullName = argv[1];
    
    if len(argv) >= 3:
        year = argv[2];
        
    if len(argv) >= 4:
        month = argv[3];
        
    if len(argv) >= 5:
        day = argv[4];
        
    if not InputFileNameIsInRightFormat(inputFileFullName):
        PrintWarning('Invalid file name format');
        PrintUsage()
        sys.exit(2)

    if not InputFileIsExist(inputFileFullName):
        PrintWarning('File does not exist.');
        sys.exit(2)
                
    if len(outputFileName) > 0:
        if not FileNameIsAcceptable(outputFileName):
            PrintWarning('Invalid output file name, accept only characters in [a-z, A-Z, 0-9]');
            sys.exit(2)
        
    print ("INFO: \r\n" + "- File: " + inputFileFullName + "\r\n- Year: " + str(year) + "\r\n- Month: " + str(month) + "\r\n- Day: " + str(day))
    # Process input file
    Hex2Fpga(inputFileFullName, outputFileName)
         
if __name__ == "__main__":
   main(sys.argv)
   
   

- Required format of .hex file name: af6ali0011_stm16ces_vxxx_yyy.hex
     + xxx: Major version number, it must exist
     + yyy: Minor version number, it is OPTIONAL, 0 as default
	 
- Syntax: <path>/hex2fpga.py <input_file.hex> <year> <month> <day>
- Parameters:
	+ <input_file.hex>: is input file .hex. File name (not include path) must follow required format
    + <output>: is optional, if exists it should contains less than 20 characters and make sure that only characters in [a-z, A-Z, 0-9] are allowed. 
	            If there is no parameter <output>, the ZIP file itself will have same name with input file .hex, 
			    and files .fpga and .ver will have name derived from input file .hex
- Result: the ZIP file contains two files .fpga and .ver in the same directory as input file .hex

- Example: ./hex2fpga.py /home/ATVN_Eng/ntnguyen/stm16_fpga/af6ali0011_stm16ces_v1.hex 15 03 27

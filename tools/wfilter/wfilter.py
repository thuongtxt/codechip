#!/usr/bin/python

import os
import os.path
import sys
import getopt
import re

#Warning types
unused_parameter = 'unused-parameter'

warn_function = "c: In function"

static_pre_regex = '^static[\s]+[A-Za-z][A-Za-z0-9]+[\s*]+'
static_const_pre_regex = '^static[\s]+const[\s]+[A-Za-z][A-Za-z0-9]+[\s*]+'
const_pre_regex = '^const[\s]+[A-Za-z][A-Za-z0-9]+[\s*]+'
public_pre_regex = '^[A-Za-z][A-Za-z0-9]+[\s*]+'
suf_regex = '\([A-Za-z][A-Za-z0-9]+[\s*]+[a-z][A-Za-z0-9]+'
verbose = False

var_decl_regex = '^[\s\t]+[A-Za-z][A-Za-z0-9_]+[\s*]+[a-z][A-Za-z0-9_]+[\[]*[A-Za-z0-9_]*[\]]*[\s=,;]+'

def lookup(x):
    return {
        unused_parameter : "warning: unused parameter",
    }[x]

""" Filter make.log file with a specific warning code """
def filter(logfile, warning):
    logfile_tmp = ''.join((logfile, '__'))
    
    old = open(logfile, 'r')
    new = open(logfile_tmp, 'w') 
    
    warning_code = lookup(warning)
    
    for line in old:
        if warn_function in line:
            new.write('\n')
            new.write(line)
        elif warning_code in line:
            new.write(line)
    old.close()
    new.close()
    os.rename(logfile_tmp, logfile)
    
""" Scan filtered make.log file """
def logscan(logfile, warning):
    file = open(logfile, 'r')
    isfunc = False
    srcfile = ''
    function = ''
    param = []
    
    warning_code = lookup(warning)
    warning_count = 0
    
    for line in file:
        if warn_function in line:
            srcfile = re.split(':', line)[0]
            function = re.split('[\s\xe2\x80\x99\x98]+', line)[3]
        elif warning_code in line:
            warning_count += 1
            var = re.split('[\s\xe2\x80\x99\x98:]+', line)  #@ Unportable issue.
            param.append(var[len(var)-2])
        else:
            if warning_count > 0:
                repair(srcfile, function, param)
            param_sz = 0
            warning_count = 0
                
    file.close()
    
""" Repair the C source file from warnings. """
def repair(srcfile, function, varlist):
    global verbose
    srcfile_w = ''.join((srcfile, '__'))
    file = open(srcfile, 'r')
    file_w = open(srcfile_w, 'w')

    public_regex = ''.join((public_pre_regex, function, suf_regex))
    static_regex = ''.join((static_pre_regex, function, suf_regex))
    staconst_regex  = ''.join((static_const_pre_regex, function, suf_regex))
    const_regex  = ''.join((const_pre_regex, function, suf_regex))
    list_sz = len(varlist)
    
    local_var = False
    fn_found = False
    
    for line in file:
        if (list_sz > 0):
            if re.match(public_regex, line) != None or \
               re.match(static_regex, line) != None or \
               re.match(const_regex, line) != None or \
               re.match(staconst_regex, line) != None:
                if verbose == True:
                    print srcfile,':', len(varlist), ':',line
                fn_found = True
                
            elif fn_found == True:
                if (re.match('^[\s\t]+{[\s\t]$', prev_line) != None or \
                    re.match(var_decl_regex, prev_line) != None) \
                    and \
                   ((re.match('^[\s\t]+{[\s\t]$', line) == None and \
                     re.match(var_decl_regex, line) == None) \
                     or \
                     re.match('^[\s\t]+if', line) or re.match('^[\s\t]+return', line)):
                    for i in range(len(varlist)):
                        file_w.write(''.join(('\tAtUnused(', varlist.pop(), ');\n')))
                        list_sz -= 1
                    fn_found = False

        prev_line = line
        file_w.write(line)
        
    if list_sz > 0:
        sys.exit("Error: %s varsize %d" % (function, list_sz))
        
    file.close()
    file_w.close()
    os.rename(srcfile_w, srcfile)
    return

def usage():
    print ('usage: wfilter.py [-f <path/to/file>] [-w <warning-type>]')
    print ('       -v                 Verbose')
    print ('       -f <path/to/file>: Compile log file.')
    print ('       -w <warning-type>: Supported warning types as')
    print ('                          \"%s\"' % (unused_parameter))
    
def main(argv):
    global verbose
    logfile = ''
    warning = ''
    try:
       opts, args = getopt.getopt(argv, "hf:w:v", ["help", "file=", "warning="])
    except getopt.GetoptError:
       usage()
       sys.exit(1)
       
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-f", "--file="):
            logfile = arg
        elif opt in ("-w", "--warning="):
            warning = arg
        elif opt == '-v':
            verbose = True
    
    filter(logfile, warning)
    logscan(logfile, warning)
   
if __name__ == "__main__":
   main(sys.argv[1:])

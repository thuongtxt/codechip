#!/usr/bin/python
import sys
import os
import operator
import shutil

##################################################################################
# FUNCTIONS
##################################################################################
class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
    
def f5(seq, idfun=None): 
	# order preserving
	if idfun is None:
		def idfun(x): return x
		
	seen = {}
	result = []
	for item in seq:
		marker = idfun(item)
       # in old Python versions:
       # if seen.has_key(marker)
       # but in new ones:
		if marker in seen: continue
		seen[marker] = 1
		result.append(item)
	return result

def PrintRegisterAccessResult(input_content, message):
	register_list = []
	stripped_register_list = []
	result_dict = {}
		
	# Get all register address to list
	for lineNumber,input_line in enumerate(input_content):
		list = input_line.split( )
		register_list.append(list[1][:-1])
		
	# Remove duplicate register address
	stripped_register_list = register_list
	stripped_register_list = f5(stripped_register_list)
	
	# Add register address and num times access to dictionary
	for index in range(len(stripped_register_list)):
		result_dict[stripped_register_list[index]] = register_list.count(stripped_register_list[index])
	
	sortedRegisterList = sorted(result_dict.items(), key=operator.itemgetter(1))
	
	for index in range(len(sortedRegisterList)):
		print sortedRegisterList[index]
		
	sum = 0
	for keys,values in result_dict.items():
		sum = sum + values
		
	print message + str(sum)

##################################################################################
# MAIN CODE
##################################################################################	
def main():
	input_file_path = raw_input("Enter path to file need be sorted: ")
	if len(input_file_path) == 0:
		print "Next time please enter something"
		sys.exit()
	try:
		input_file_handle_read = open(input_file_path, 'r')
		input_content = input_file_handle_read.readlines()
	except IOError:
		print "There was an error reading file"
		sys.exit()
		
	# Copy original file
	file_path_read_register_only = input_file_path + "_readonly.txt"
	file_path_write_register_only = input_file_path + "_writeonly.txt"
	os.system ("cp %s %s" %(input_file_path, file_path_read_register_only))
	os.system ("cp %s %s" %(input_file_path, file_path_write_register_only))
	
	# Remove unneccessary line read register only file
	file_handle_register_read_register_only = open(file_path_read_register_only, 'r')
	input_content_register_readonly = file_handle_register_read_register_only.readlines()
	file_handle_register_read_register_only.close()
	file_handle_register_read_register_only = open(file_path_read_register_only, 'w')
	for line in input_content_register_readonly:
		if "Write" in line:
			continue
		file_handle_register_read_register_only.write(line)
	file_handle_register_read_register_only.close()
	
	# Remove unneccessary line write register only file
	file_handle_register_write_register_only = open(file_path_write_register_only, 'r')
	input_content_register_writeonly = file_handle_register_write_register_only.readlines()
	file_handle_register_write_register_only.close()
	file_handle_register_write_register_only = open(file_path_write_register_only, 'w')
	for line in input_content_register_writeonly:
		if "Read" in line:
			continue
		file_handle_register_write_register_only.write(line)
	file_handle_register_write_register_only.close()
		
	# Call function for both read/write
	print bcolors.OKGREEN + "REGISTER ACCESS RESULT (INCLUDE BOTH READ/WRITE)" + bcolors.ENDC
	PrintRegisterAccessResult(input_content, "Total number of access register: ")
	input_file_handle_read.close()
	
	# Call function for read
	print bcolors.OKGREEN + "REGISTER READ RESULT" + bcolors.ENDC
	file_handle_register_read_register_only = open(file_path_read_register_only, 'r')
	input_content_register_readonly = file_handle_register_read_register_only.readlines()
	PrintRegisterAccessResult(input_content_register_readonly, "Total number of read register: ")
	
	# Call function for write
	print bcolors.OKGREEN + "REGISTER WRITE RESULT" + bcolors.ENDC
	file_handle_register_write_register_only = open(file_path_write_register_only, 'r')
	input_content_register_writeonly = file_handle_register_write_register_only.readlines()
	PrintRegisterAccessResult(input_content_register_writeonly, "Total number of write register: ")
		
	# Remove temp files	
	os.system ("rm %s" %(file_path_read_register_only))
	os.system ("rm %s" %(file_path_write_register_only))
		
if __name__ == "__main__":
	main()
   
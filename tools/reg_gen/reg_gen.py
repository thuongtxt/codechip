#! /usr/bin/python

"""
Created on Feb 23, 2013

@author: cuongpt
"""

# ===== IMPORT SECTION =====

import os.path
import fnmatch
import re
import sys
import datetime
import io
import textwrap
import math
import getopt
from reg_py import RegisterClassToPythonClassesGenerator

# sys.path.insert(0, './in/format')

# from md_format import *


# ===== CLASS SECTION =====

class Color:
    GRAY = '\033[1;30m'
    RED = '\033[1;31m'
    GREEN = '\033[1;32m'
    YELLOW = '\033[1;33m'
    BLUE = '\033[1;34m'
    MAGENTA = '\033[1;35m'
    CYAN = '\033[1;36m'
    WHITE = '\033[1;37m'
    CRIMSON = '\033[1;38m'
    BOLD = '\033[1m'
    END = '\033[1;m'
    pass


class NamePrefix:
    COM_NAME = ""
    PRO_LINE = ""
    PRO_NAME = ""
    BLK_NAME = ""
    REG_DEF_PREFIX = ""
    REG_FIELD_DEF_PREFIX = ""

    def __init__(self):
        pass


    def set_com_name(self, _val):
        NamePrefix.COM_NAME = _val
        pass


    def get_com_name(self):
        return NamePrefix.COM_NAME

    def set_pro_line(self, _val):
        NamePrefix.PRO_LINE = _val

    def get_pro_line(self):
        return NamePrefix.PRO_LINE

    def set_pro_name(self, _val):
        NamePrefix.PRO_NAME = _val

    def get_pro_name(self):
        return NamePrefix.PRO_NAME

    def set_blk_name(self, _val):
        NamePrefix.BLK_NAME = _val

    def get_blk_name(self):
        return NamePrefix.BLK_NAME

class RegisterFormulaVariableDetail:
    """
    This class content all information of one variable in formula
    """

    val_name = ""  # Name
    val_min = ""  # Min value
    val_max = ""  # Max value
    val_des = ""  # Variable description

    def __init__(self):
        """
        Constructor
        """

    def set_val_name(self, _val_name):
        """
        """
        self.val_name = _val_name

    def get_val_name(self):
        """
        """
        return self.val_name

    def set_val_min(self, _val_min):
        """
        """
        self.val_min = _val_min

    def get_val_min(self):
        """
        """
        return self.val_min

    def set_val_max(self, _val_max):
        """
        """
        self.val_max = _val_max

    def get_val_max(self):
        """
        """
        return self.val_max

    def set_val_des(self, _val_des):
        """
        """
        self.val_des = _val_des

    def get_val_des(self):
        """
        """
        return self.val_des

    def show(self):
        """
        Show all variable of class
        """
        print("Name: ", self.get_val_name())
        print("Min : ", self.get_val_min())
        print("Max : ", self.get_val_max())
        print("Des : ", self.get_val_des())
        pass

    def __del__(self):
        """
        Delete object
        """

def _removeUnicode(string):
    return string.decode('unicode_escape').encode('ascii', 'ignore')

class RegisterFieldDetail:
    """
    Register field detail
    """

    # All variable is string
    field_name = ""  # name of field
    field_bit = ""  # bit from -> to ([10:2])
    field_bit_left = ""  # bit left (2)
    field_bit_right = ""  # bit right (10)
    field_val_min = ""  # min value
    field_val_max = ""  # max value
    field_des = ""  # description
    field_ex = []  # list of line example
    field_type = ""  # type of field (RO/RW...)
    field_val_reset = ""  # reset value
    field_val_def = ""  # default value

    def __init__(self):
        """
        Constructor
        """

    def set_field_name(self, _field_name):
        """
        """
        self.field_name = _field_name

    def get_field_name(self):
        """
        """
        name = self.field_name
        name = name.replace(" ", "_")
        name = name.replace("[", "_")
        name = name.replace("]", "_")
        name = name.replace("#", "_")
        name = name.replace("(", "_")
        name = name.replace(")", "_")
        name = name.replace(":", "_")
        return name

    def set_field_bit(self, _field_bit):
        """
        """
        self.field_bit = _field_bit

    def get_field_bit(self):
        """
        """
        return self.field_bit

    def set_field_bit_left(self, _field_bit_left):
        """
        """
        self.field_bit_left = _field_bit_left

    def get_field_bit_left(self):
        """
        """
        return self.field_bit_left

    def set_field_bit_right(self, _field_bit_right):
        """
        """
        self.field_bit_right = _field_bit_right

    def get_field_bit_right(self):
        """
        """
        return self.field_bit_right

    def set_field_val_min(self, _field_val_min):
        """
        """
        self.field_val_min = _field_val_min

    def get_field_val_min(self):
        """
        """
        return self.field_val_min

    def set_field_val_max(self, _field_val_max):
        """
        """
        self.field_val_max = _field_val_max

    def get_field_val_max(self):
        """
        """
        return self.field_val_max

    def set_field_des(self, _field_des):
        """
        """
        self.field_des = _field_des

    def get_field_description(self):
        """
        """
        return _removeUnicode(self.field_des)

    def set_field_example(self, _field_ex):
        """
        """
        self.field_ex = _field_ex[:]

    def get_field_example(self):
        """
        """
        return self.field_ex

    def set_field_type(self, _field_type):
        """
        """
        self.field_type = _field_type

    def get_field_type(self):
        """
        """
        return self.field_type

    def set_field_val_reset(self, _field_val_reset):
        """
        """
        self.field_val_reset = _field_val_reset

    def get_field_val_reset(self):
        """
        """
        return self.field_val_reset

    def set_field_val_default(self, _field_val_def):
        """
        """
        self.field_val_def = _field_val_def

    def get_field_val_def(self):
        """
        """
        return self.field_val_def

    def __del__(self):
        pass

class RegisterBlockDetail:
    """
    All information of one register block
    """

    reg_name_long = ""  # long name/ full name
    reg_name_short = ""  # short name
    reg_add = ""  # register address
    reg_add_start = ""  # address start
    reg_add_end = ""  # address end
    reg_add_formula = ""  # address formula
    reg_val_dis = []
    reg_val_dis_list = []  # list of RegisterFormulaVariableDetail
    reg_des = ""
    reg_des_list = []  # list of line description
    reg_width = ""
    reg_type = ""
    reg_field_num = 0
    reg_field_list = []  # List of RegisterFieldDetail
    reg_field_raw_list = []

    def __init__(self):
        """
        Constructor
        """

    def set_reg_name_long(self, _reg_name_long):
        """
        """
        self.reg_name_long = _reg_name_long

    def get_reg_name_long(self):
        """
        """
        return self.reg_name_long

    def set_reg_name_short(self, _reg_name_short):
        """
        """
        self.reg_name_short = _reg_name_short.replace(" ", "_")

    def _makeValidIdentifier(self, string):
        string = string.replace(" ", "_")
        string = string.replace("-", "_")
        return string

    def get_reg_name_short(self):
        """
        """
        name = self.reg_name_short
        if name is None or len(name) == 0:
            name = self.reg_name_long
            
        return self._makeValidIdentifier(name)

    def set_reg_add(self, _reg_add):
        """
        """
        self.reg_add = _reg_add

    def get_reg_add(self):
        """
        """
        return self.reg_add

    def set_reg_add_start(self, _reg_add_start):
        """
        """
        self.reg_add_start = _reg_add_start

    def get_reg_add_start(self):
        """
        """
        return self.reg_add_start

    def set_reg_add_end(self, _reg_add_end):
        """
        """
        self.reg_add_end = _reg_add_end

    def get_reg_add_end(self):
        """
        """
        return self.reg_add_end

    def set_reg_add_formula(self, _reg_add_formula):
        """
        """
        self.reg_add_formula = _reg_add_formula

    def get_reg_add_formula(self):
        """
        """
        return self.reg_add_formula

    def set_reg_val_dis(self, _reg_val_dis):
        """
        """
        self.reg_val_dis = _reg_val_dis[:]
        # self.reg_val_dis = copy.copy(_reg_val_dis)
        # self.reg_val_dis = list(_reg_val_dis)

    def get_reg_val_dis(self):
        """
        """
        return self.reg_val_dis

    def set_reg_val_dis_list(self, _reg_val_dis_list):
        """
        """
        self.reg_val_dis_list = _reg_val_dis_list[:]

    def get_reg_val_dis_list(self):
        """
        """
        return self.reg_val_dis_list

    def set_reg_des(self, _reg_des):
        """
        """
        self.reg_des = _reg_des

    def get_reg_des(self):
        """
        """
        return r"%s" % self.reg_des

    def set_reg_des_list(self, _reg_des):
        """
        """
        self.reg_des_list = _reg_des[:]

    def get_reg_des_list(self):
        """
        """
        return self.reg_des_list

    def set_reg_width(self, _reg_width):
        """
        """
        self.reg_width = _reg_width

    def get_reg_width(self):
        """
        """
        if self.reg_width is None or len(self.reg_width) == 0:
            return "32"
        return self.reg_width

    def set_reg_type(self, _reg_type):
        """
        """
        self.reg_type = _reg_type

    def get_reg_type(self):
        """
        """
        return self.reg_type

    def set_reg_field_num(self, _reg_field_num):
        """
        """
        self.reg_field_num = _reg_field_num

    def get_field_num(self):
        """
        """
        return self.reg_field_num

    def set_reg_field_list(self, _reg_field_list):
        """
        """
        self.reg_field_list = _reg_field_list[:]

    def get_field_list(self):
        """
        """
        return self.reg_field_list

    def set_reg_field_raw_list(self, _reg_field_raw_list):
        """
        """
        self.reg_field_raw_list = _reg_field_raw_list[:]

    def get_field_raw_list(self):
        """
        """
        return self.reg_field_raw_list

    def all_val_show(self):
        print("Full Name      : ", self.get_reg_name_long())
        print("Short Name     : ", self.get_reg_name_short())
        print("Reg Add        : ", self.get_reg_add())
        print("Reg Add Formula: ", self.get_reg_add_formula())
        print("Reg Val Des    : ", self.get_reg_val_dis())
        print("Reg Des        : ", self.get_reg_des())
        print("Reg Width      : ", self.get_reg_width())
        print("Reg Type       : ", self.get_reg_type())
        print("Field\n")
        for _item in self.get_field_list():
            print(_item.get_field_bit(),
                  _item.get_field_name(),
                  _item.get_field_description(),
                  _item.get_field_type(),
                  _item.get_field_val_reset(),
                  _item.get_field_val_def(), "\n")

    def __del__(self):
        pass

# ===== DEFINE SECTION =====

# Mapping all abbreviation convention
AbbConvertHash = dict(Abort='Abr', Accumulate='Acc')

# List all file with pattern in "folder" (recursive or not)
# Open file
# Find Header Block
# Find Guideline Block
# Find Rules Block
# Find Register define Block

# How to find one BLOCK
# start_BLOCK: "##########"
# end_BLOCK  : "##########" or "End of file"

# To convert register offset, also recognize register block name
H_REG_BLK_OFFSET = [
    ["GLB_OAM", "0x200000"],
    ["GLB", "0x000000"],
    ["BIT", "0x000800"],
    ["WAN", "0x001000"],
    ["GMac_RMON", "0x080000"],
    ["GMac1", "0x080000"],
    ["GMac2", "0x081000"],
    ["GMac", "0x080000"],
    ["L2L3", "0x100000"],
    ["RCP_OAM", "0x200000"],
    ["TX_OAM", "0x200000"],
    ["OAM_FILTER", "0x200000"],
    ["OAMFILTER", "0x200000"],
    ["LM_OAM", "0x200000"],
    ["OAM_RXDMMON", "0x200000"],
    ["TST_OAM", "0x200000"],
    ["OAM", "0x200000"],
    ["SWF", "0x300000"],
    ["TM", "0x400000"],
    ["EDI", "0x500000"],
    ["XMac1_RMON", "0x0A0000"],
    ["XMac2_RMON", "0x0A0000"],
    ["XMac1", "0x0A0000"],
    ["XMac2", "0x0A0000"],
    ["RMON", "0x600000"],
    ["PTP", "0x700000"],
    ["APS", "0x740000"],
    ["LAG", "0x780000"],
    ["DSAM", "0x7C0000"],
    ["PWE", "0x800000"],
    ["ATT", "0x000000"],
    ["PORT", "0x082000"],
    ["SKEY", "0x000000"],
    ["SYNCE", "0x001000"],
    ["CDR", "0x001000"],
	["PM", "0x001000"],
    ["OCN", "0x001000"],
    ["MAP_HO", "0x001000"],
    ["MAP", "0x001000"],
    ["PLA", "0x001000"],
    ["PDH_DLK", "0x001000"],
    ["PDH_LOOPCODE", "0x001000"],
    ["PDH_LPC", "0x001000"],
    ["PDH_MDLPRM", "0x001000"],
    ["PDH", "0x001000"]]

PRODUCT_LINE = [
    ["AF5", "0x000000"],
    ["AF6", "0x000800"]]

PRODUCT_NAME = [
    ["CE24", "0x000000"],
    ["CE08_D", "0x000800"],
    ["CE08", "0x000800"],
    ["CE32", "0x000800"],
    ["CE50", "0x000800"],
    ["CCI0021", "0x000800"],
    ["CCI0011", "0x000800"], ]

RTL_EXT = '.atreg'
FOLDER_INPUT = ".\\in\\reg\\"  # Folder input .\\in\\
RTL_REG_EXT = "*.atreg"  # RTL register file extension
RTL_REG_BLK_START_01 = "// *Register Full Name"  # RTL register file block start
RTL_REG_BLK_END_01 = "##########"
RTL_REG_BLK_END_02 = "\n"
RTL_REG_START_BLOCK = 1
RTL_REG_END_BLOCK = 2
RTL_REG_BLOCK_MAX = 1000
RTL_REG_DELIMITER = "%%"
RET_REG_STRIP_L = ","
COM_START_PAT_01 = "^#"
COM_START_PAT_02 = "^//#"
COM_START_PAT_03 = "^//---"
REG_FULL_NAME_PAT = "Register Full Name.*:"
REG_SHORT_NAME_PAT = "RTL Instant Name.*:"
REG_ADD_PAT = "Address\s*:"
REG_ADD_FOR_PAT = "Formula\s*:"
REG_ADD_FOR_PAT_NA = "N/A"
REG_ADD_FOR_VAL_PAT = "Where\s*:"
REG_DES_PAT = "Description\s*:"
REG_WIDTH_PAT = "Width\s*:"
REG_TYPE_PAT = "Register Type\s*:"
REG_FIELD_PAT = "Field[x]?\s*:"
REG_FIELD_UNUSED = "unused"
REG_FIELD_RESERVED = "reserved"
REG_FIELD_RESERVE = "reserve"
REG_FIELD_R_W = "R/W"
REG_FIELD_R_O = "RO"
REG_FIELD_R2C = "R2C"
REG_FIELD_R_W_C = "R/W/C"
REG_FIELD_W_O = "WO"
REG_FIELD_W2C = "W2C"
MIN_VAL_DEFAULT = "0x0"

IS_REG_FULL_NAME = 1
IS_REG_SHORT_NAME = 2
IS_REG_ADD = 3
IS_REG_ADD_FOR = 4
IS_REG_ADD_FOR_VAL = 5
IS_REG_DES = 6
IS_REG_WIDTH = 7
IS_REG_TYPE = 8
IS_REG_FIELD = 9
IS_START = 0

# For .md file
MD_EXT = ".md"
MD_FOLDER = ".\\out\\md"

PY_FOLDER = None

# For .h file
TCL_EXT = ".tcl"
H_EXT = ".h"
R_EXT = ".r"
H_FOLDER = ".\\out\\h"  # "..\\out\\h\\"
H_HEADER = \
    '/*------------------------------------------------------------------------------\n' \
    ' *                                                                              \n' \
    ' * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  \n' \
    ' *                                                                              \n' \
    ' * The information contained herein is confidential property of Arrive          \n' \
    ' * Technologies. The use, copying, transfer or disclosure of such information   \n' \
    ' * is prohibited except by express written agreement with Arrive Technologies.  \n' \
    ' *                                                                              \n' \
    ' * Module      :                                                                \n' \
    ' *                                                                              \n' \
    ' * File        :                                                                \n' \
    ' *                                                                              \n' \
    ' * Created Date:                                                                \n' \
    ' *                                                                              \n' \
    ' * Description : This file contain all constance definitions of  block.         \n' \
    ' *                                                                              \n' \
    ' * Notes       : None                                                           \n' \
    ' *----------------------------------------------------------------------------*/'

H_REG_NEW = "\n\n"
H_REG_DEF_START = "/*--------------------------- Define -----------------------------------------*/"
H_REG_NAME_COM_START = "/*------------------------------------------------------------------------------"
H_REG_INFO_NAME = "Reg Name   : "
H_REG_INFO_ADD = "Reg Addr   : "
H_REG_INFO_ADD_FOR = "Reg Formula: "
H_REG_INFO_ADD_VAL = "    Where  : "
H_REG_INFO_ADD_VAL_L = "           + "
H_REG_INFO_DES = "Reg Desc   : "
H_REG_NAME_COM_END = "------------------------------------------------------------------------------*/"
H_REG_DEF_WRITE_MASK_SUFFIX = "_WriteMask"
H_REG_DEF_WRITE_MAX_SUFFIX = "_MaxVal"
H_REG_DEF_WRITE_MIN_SUFFIX = "_MinVal"
H_REG_DEF_WRITE_WIDTH_SUFFIX = "_WidthVal"
H_REG_DEF_FORMAT = "%-70s%40s\n"
H_REG_FIELD_NEW = "\n"
H_REG_FIELD_START = "/*--------------------------------------"
H_REG_FIELD_NAME = "BitField Name: "
H_REG_FIELD_TYPE = "BitField Type: "
H_REG_FIELD_DES = "BitField Desc: "
H_REG_FIELD_BIT = "BitField Bits: "
H_REG_FIELD_END = "--------------------------------------*/"
H_REG_FIELD_DEF_MASK_SUFFIX = "_Mask"
H_REG_FIELD_DEF_SHIFT_SUFFIX = "_Shift"
H_REG_FIELD_DEF_MAXVAL_SUFFIX = "_MaxVal"
H_REG_FIELD_DEF_MINVAL_SUFFIX = "_MinVal"
H_REG_FIELD_DEF_RSTVAL_SUFFIX = "_RstVal"
H_REG_FIELD_DEF_BIT_START_SUFFIX = "_Bit_Start"
H_REG_FIELD_DEF_BIT_END_SUFFIX = "_Bit_End"

# For .log file
LOG_EXT = ".log"
LOG_FOLDER = ".\\out\\log"

# For _MEM.h
H_MEM_EXT = "*_MEM.h"  # RTL register file extension

REG_FIELD_EXT = ".r"


MD_HEADER = '## Revision History [Revision History]\n\n' \
            '|Revision|Date|Author|Description|\n' \
            '|--------|----|------|-----------|\n'

MD_HEADING_L1 = "#"
MD_HEADING_L2 = "##"
MD_HEADING_L3 = "###"
MD_HEADING_L4 = "####"
MD_REG_FULL_NAME = ""
MD_REG_SHORT_NAME = "* **RTL Instant Name**    : "
MD_REG_ADD = "* **Address**             : "
MD_REG_ADD_FOR = "* **Formula**             : "
MD_REG_ADD_FOR_VAL = "* **Where**               : "
MD_REG_ADD_FOR_VAL_DETAIL = "    * "
MD_REG_DES = "* **Description**           "
MD_REG_WIDTH = "* **Width**               : "
MD_REG_TYPE = "* **Register Type**       : "
MD_REG_FIELD = "* **Bit Field Detail**    : "
MD_NEW_LINE = "* "
MD_REG_FIELD_TABLE_HEADER = "|Bit Field |Name|Description|Type|Reset|Default|\n" \
                            "|----------|----|-----------|----|-----|-------|"

# ===== FUNCTION SECTION =====


def message(_out):
    print(_out)
    pass


def reg_def_prefix_get(_file_name):
    return "#define cAf5Reg_"


def company_prefix_get(_file_name):
    return "At".title()


def product_line_prefix_get(_file_name):
    product_line = ""

    name = _file_name.lower()
    for x in PRODUCT_LINE:
        if name.find(x[0].lower()) != -1:
            product_line = x[0].lower()
            break

    return product_line.title()

def product_name_prefix_get(_file_name):
    product_name = ""

    name = _file_name.lower()
    for x in PRODUCT_NAME:
        if name.find(x[0].lower()) != -1:
            product_name = x[0].lower()
            break

    return product_name.title()


def block_function_prefix_get(_file_name):
    block_name = ""

    name = _file_name.lower()
    for x in H_REG_BLK_OFFSET:
        if name.find(x[0].lower()) != -1:
            block_name = x[0].lower()
            break

    return block_name.title()


def str_strip(_str, _list_strip_param):
    """Strip all character in [_list_strip_param]
    :param _str: input string
    :param _list_strip_param: list all character will strip
    :return: str after strip
    """
    for _strip_chara in _list_strip_param:
        _str = _str.replace(_strip_chara, "")

    return _str


def cal(_expr):
    """Calculate string to value
    :param _expr: formula
    :return: result after
    """
    if _expr.isdigit():
        return float(_expr)
    for c in ('*', '/', '+', '-'):
        l, ops, r = _expr.partition(c)
        if ops == '*':
            return cal(l) * cal(r)
        elif ops == '/':
            return cal(l) / cal(r)
        elif ops == '+':
            return cal(l) + cal(r)
        elif ops == '-':
            return cal(l) - cal(r)


def expression_format(_expr):
    """Format expression in correct style
    :param _expr: formula string
    :return:
    """
    _expr = str_strip(_expr, " ")
    for c in ("*", "/", "+", "-"):
        _ope = " " + c + " "
        _expr = _expr.replace(c, _ope)

    return _expr


def safe_eval(expr, symbols={}):
    return eval(expr, dict(__builtins__=None), symbols)


def calc(expr):
    return safe_eval(expr, vars(math))


def is_list_empty(list_param):
    """Check list is empty or not
    :param list_param: Is a list
    :return: True is empty, other is not empty
    """
    _cnt = 0
    for _ in list_param:
        _cnt += 1

    if _cnt:
        return False
    else:
        return True


def list_len(list_param):
    """Counter number entry in list
    :param list_param: Is a list
    :return:
    """
    _cnt = 0
    for _ in list_param:
        _cnt += 1

    return _cnt


def is_none_formula(formula):
    """Check the string is a formula or not
    :param formula: A string
    :return: True if not formula, other is a formula
    """
    _formula = formula.lower()
    if _formula.find("n/a") != -1:
        return True
    else:
        return True


def empty_item_rmv(list_param):
    """Remove empty entry in list
    :param list_param: A list
    :return: A list after remove all empty entry
    """
    for item in list_param:
        if len(item) == 0:
            list_param.remove(item)
    return list_param


def is_str_empty(_str):
    """Check a line is empty or not
    :param _str: A string
    :return: True if empty
    """
    if _str.strip().__len__():
        return False
    else:
        return True


def is_comment_line(_line):
    """Check a line is comment or not
    :param _line:
    :return:
    """
    _line.lstrip()
    if re.match(COM_START_PAT_01, _line, 0) or \
            re.match(COM_START_PAT_02, _line, 0) or \
            re.match(COM_START_PAT_03, _line, 0):
        return True
    else:
        return False


# ===== UTILITY FUNCTION FOR REGISTER =====

def is_reg_full_name_line(_line):
    """Recognize a line is register fullname
    :param _line: A line
    :return:
    """
    if re.findall(REG_FULL_NAME_PAT, _line, 0):
        return True
    else:
        return False


def reg_full_name_get(_line):
    """Get name content in full_name line
    :param _line: Is full name line
    :return: Full name
    """
    line_split = re.split(":", _line, 1)
    return line_split[1].strip()


def is_reg_short_name_line(_line):
    """Recognize a line is register short name
    :param _line: Is a line
    :return:
    """
    if re.findall(REG_SHORT_NAME_PAT, _line, 0):
        return True
    else:
        return False


def reg_short_name_get(_line):
    """Get short name content in short_name line
    :param _line: Short name line
    :return: Short name
    """
    line_split = re.split(":", _line, 1)
    return line_split[1].strip()


def is_reg_add_line(_line):
    """Recognize a line is register address
    :param _line: A line
    :return:
    """
    if re.findall(REG_ADD_PAT, _line, 0):
        return True
    else:
        return False


def reg_add_get(_line):
    """Get address from register address line
    :param _line: A register address
    :return: Address
    """
    line_split = re.split(":", _line, 1)
    return line_split[1].strip()


def reg_add_start_get(add):
    """Get start address from register address
    :param add: Address [0x1234-0x5678]
    :return: Start address [0x1234]
    """
    start_add = add
    if add.find("-"):
        _split = re.split("-", add, 1)
        start_add = _split[0]

    if start_add.find("\("):
        _split = re.split("\(", start_add, 1)
        start_add = _split[0]

    return start_add.strip()


def reg_add_end_get(add):
    """Get end address from register address
    :param add: Address [0x1234-0x5678]
    :return: End address [0x5678]
    """
    _a_list = re.split("-", add, 1)
    if list_len(_a_list) > 1:
        _end_add = re.split("\(", _a_list[1].strip(), 1)
        return _end_add[0].strip()
    else:
        return ""


def is_reg_formula_line(_line):
    """Recognize a line is formula
    :param _line: A register formula
    :return:
    """
    if re.findall(REG_ADD_FOR_PAT, _line, 0):
        return True
    else:
        return False


def reg_formula_get(_line):
    """Get formula from register formula line
    :param _line: Formula line
    :return:
    """
    _a_list = re.split(":", _line, 1)
    return _a_list[1].strip().lstrip("{").rstrip("}")


def is_reg_formula_val_line(_line):
    """Recognize a line is formula variable line
    :param _line: A line
    :return:
    """
    if re.findall(REG_ADD_FOR_VAL_PAT, _line, 0):
        return True
    else:
        return False


def reg_formula_val_line_get(_line):
    """Get variable from variable line
    :param _line: A variable line
    :return: Variable
    """
    _a_list = re.split(":", _line, 1)
    return str_strip(_a_list[1].strip(), "{}")


def is_reg_des_line(_line):
    """Recognize a line is description line
    :param _line: A line
    :return:
    """
    if re.findall(REG_DES_PAT, _line, 0):
        return True
    else:
        return False


def reg_des_line_get(_line):
    """Get description
    :param _line: A description line
    :return:
    """
    _a_list = re.split(":", _line, 1)
    return _a_list[1].strip()
    pass


def is_reg_width_line(_line):
    """Recognize a line is width line
    :param _line: A line
    :return:
    """
    if re.findall(REG_WIDTH_PAT, _line, 0):
        return True
    else:
        return False


def reg_width_line_get(_line):
    """Get width
    :param _line: A width line
    :return: Width
    """
    _a_list = re.split(":", _line, 1)
    return _a_list[1].strip()


def is_reg_type_line(_line):
    """Recognize a line is type
    :param _line:
    :return:
    """
    if re.findall(REG_TYPE_PAT, _line, 0):
        return True
    else:
        return False


def reg_type_line_get(_line):
    """Get type
    :param _line: A type line
    :return:
    """
    _a_list = re.split(":", _line, 1)
    return _a_list[1].strip().lstrip("{").rstrip("}")


def is_reg_field_line(_line):
    """Recognize a line is field
    :param _line:
    :return:
    """
    if re.findall(REG_FIELD_PAT, _line, 0):
        return True
    else:
        return False


def reg_field_line_get(_line):
    """Get field
    :param _line: A field line
    :return:
    """
    _a_list = re.split(":", _line, 1)
    return _a_list[1].strip()


def reg_field_block_2_str(_field_block_param):
    """Field block string,
    :param _field_block_param:
    :return:
    """
    _field_block_str = ""
    for item in _field_block_param:
        _field_block_str += item.strip()
        _field_block_str += " "

    return _field_block_str


def rtl_reg_is_block_delimitation(_line):
    """ Function rtl_reg_is_block_delimitation
    @param : A line
    @return: is rtl register block delimitation or not
    """
    if re.match(RTL_REG_BLK_START_01, _line, 0):
        return True
    else:
        return False


def reg_field_block_process(_field_block_param):
    """Field block process
    :param _field_block_param:
    :return: Field block class
    """
    _field_block_str = ""
    for item in _field_block_param:
        _field_block_str += item.strip()
        _field_block_str += " "

    _field_block_strip = []
    _field_block = re.split(RTL_REG_DELIMITER, _field_block_str)

    for item in _field_block:
        _field_block_strip.append(item.strip())

    # All to Field block class
    field_block_class = RegisterFieldDetail()
    try:
        # Set bit field
        field_block_class.set_field_bit(
            _field_block_strip[0].replace("[", "").replace("]", "").replace(" ", ""))

        # Set field left and right
        _field_lr = re.split(":", field_block_class.get_field_bit())

        if list_len(_field_lr) == 1:
            field_block_class.set_field_bit_right(_field_lr[0])
        else:
            field_block_class.set_field_bit_right(_field_lr[1])

        field_block_class.set_field_bit_left(_field_lr[0])

        # Set field name
        field_block_class.set_field_name(str_strip(_field_block_strip[1], RET_REG_STRIP_L))

        # Set Field description and example
        _field_des = re.split("{", _field_block_strip[2])

        cnt = 0
        for _ in _field_des:
            cnt += 1

        _field_des_ex = []

        for idx in range(cnt):
            if idx != 0:
                pass
                _field_des[idx] = "{" + _field_des[idx]
                _field_des_ex.append(_field_des[idx])

        field_block_class.set_field_des(_field_des[0].strip())
        field_block_class.set_field_example(_field_des_ex)

        # Set Field type
        field_block_class.set_field_type(_field_block_strip[3])

        # Set Field value reset
        field_block_class.set_field_val_reset(_field_block_strip[4])

    except Exception as e:
        print(_field_block_str)
        print(str(e))

    # Set Field value min
    field_block_class.set_field_val_min("0x0")

    # Set Field value max
    try:
        val_max = 0xFFFFFFFF
        bit_left = int(field_block_class.get_field_bit_left())
        if bit_left >= 32:
            bit_left = 31
        num_bit = bit_left - int(field_block_class.get_field_bit_right()) + 1
        val_max >>= (32 - num_bit)
        field_block_class.set_field_val_max(hex(val_max))
    except Exception as e:
        print(str(e))

    # Set Field value default
    try:
        field_block_class.set_field_val_default(_field_block_strip[5])
    except:
        pass

    return field_block_class


def reg_field_block_unused_process(_reg_field_block_list):
    """ Function reg_field_block_unused_process
    @param : A field block
    @return: Field block class list
    """
    idx = 0
    hold = ""
    _list = []
    for field in _reg_field_block_list:
        if idx == 0:
            hold = field.get_field_bit_right()
        else:
            _list.append(hold + "-" + field.get_field_bit_left())
            hold = field.get_field_bit_right()
        idx += 1

    field_block_list = []
    i_pos = []
    idx = 0
    for item in _list:
        try:
            if cal(item) != 1:
                item_split = item.split("-")
                l = int(item_split[0]) - 1
                r = int(item_split[1]) + 1
                # New field
                field_block = RegisterFieldDetail()
                field_block.set_field_bit(str(l) + ":" + str(r))
                field_block.set_field_bit_left(str(l))
                field_block.set_field_bit_right(str(r))
                field_block.set_field_name("unused")
                field_block.set_field_des("unused")
                field_block.set_field_example("")
                field_block.set_field_type("R/W")
                field_block.set_field_val_reset("0x0")
                field_block.set_field_val_default("0x0")
                field_block_list.append(field_block)
                i_pos.append(idx)

        except:
            print(item)
        idx += 1

    num = 0
    for pos in i_pos:
        _reg_field_block_list.insert(int(pos + num + 1), field_block_list[num])
        num += 1

    return _reg_field_block_list


def reg_formula_val_block_process(_formula_val_block_param):
    """ Function reg_formula_val_block_process
    @param : A formula variable detail block
    @return:
    """

    _formula_val_block_str = ""
    for item in _formula_val_block_param:
        _formula_val_block_str += item.strip()
        _formula_val_block_str += " "

    _formula_val_block_strip = []
    _formula_val_block = re.split(RTL_REG_DELIMITER, _formula_val_block_str)

    for item in _formula_val_block:
        _formula_val_block_strip.append(item.strip())

    _formula_val_block_strip = empty_item_rmv(_formula_val_block_strip)

    return _formula_val_block_strip


def reg_formula_val_block_2_class_process(_formula_val_block_param):
    """ Function reg_formula_val_block_process
    @param : A formula variable detail block
    @return:
    """

    _formula_val_block_str = ""
    for item in _formula_val_block_param:
        _formula_val_block_str += item.strip()
        _formula_val_block_str += " "

    _formula_val_block_strip = []
    _formula_val_block = re.split(RTL_REG_DELIMITER, _formula_val_block_str)

    for item in _formula_val_block:
        _formula_val_block_strip.append(item.strip())

    formula_val_block_class_list = []

    for item in _formula_val_block_strip:
        _formula_val_block_strip_split = re.split(":", item)

        # Formula string to class
        val_name_min_max_split = re.split("\(", _formula_val_block_strip_split[0])
        val_name = val_name_min_max_split[0].lstrip("$")
        try:
            val_min_max_split = re.split("-", val_name_min_max_split[1])
            val_min = val_min_max_split[0]
            val_max = str_strip(val_min_max_split[1], ")  ")
        except:
            val_min = ""
            val_max = ""

        formula_val_block_class = RegisterFormulaVariableDetail()

        try:
            formula_val_block_class.set_val_des(_formula_val_block_strip_split[1])
        except:
            formula_val_block_class.set_val_des("")

        formula_val_block_class.set_val_name(val_name.strip())
        formula_val_block_class.set_val_min(val_min.strip())
        formula_val_block_class.set_val_max(val_max.strip())
        formula_val_block_class_list.append(formula_val_block_class)

    return formula_val_block_class_list


def set_field(_field, _info, _block_class):

    if _field == IS_REG_FULL_NAME:
        _str = ""
        for index in range(len(_info)):
            if index < (len(_info) - 1):
                _str += _info[index]
                _str += " "
            else:
                _str += _info[index]
        _block_class.set_reg_name_long(_str)
        pass

    elif _field == IS_REG_SHORT_NAME:
        _str = ""
        for index in range(len(_info)):
            if index < (len(_info) - 1):
                _str += _info[index]
                _str += " "
            else:
                _str += _info[index]
        _block_class.set_reg_name_short(_str)
        pass

    elif _field == IS_REG_ADD:
        _str = ""
        for index in range(len(_info)):
            if index < (len(_info) - 1):
                _str += _info[index]
                _str += " "
            else:
                _str += _info[index]
        _block_class.set_reg_add(_str)
        pass

    elif _field == IS_REG_ADD_FOR:
        _block_class.set_reg_add_formula(str_strip(_info[0], "()"))
        pass

    elif _field == IS_REG_ADD_FOR_VAL:
        formula_val = empty_item_rmv(_info)

        # Block variable
        _block_class.set_reg_val_dis(reg_formula_val_block_process(formula_val))

        # Class variable list
        if is_list_empty(formula_val):
            pass
        else:
            _block_class.set_reg_val_dis_list(reg_formula_val_block_2_class_process(formula_val))
        pass

    elif _field == IS_REG_DES:
        _str = ""
        for index in range(len(_info)):
            if index < (len(_info) - 1):
                _str += _info[index]
                _str += " "
            else:
                _str += _info[index]
        _block_class.set_reg_des(_str)
        _block_class.set_reg_des_list(_info)
        pass

    elif _field == IS_REG_WIDTH:
        _str = ""
        for index in range(len(_info)):
            if index < (len(_info) - 1):
                _str += _info[index]
                _str += " "
            else:
                _str += _info[index]
        _block_class.set_reg_width(_str)
        pass

    elif _field == IS_REG_TYPE:
        _str = ""
        for index in range(len(_info)):
            if index < (len(_info) - 1):
                _str += _info[index]
                _str += " "
            else:
                _str += _info[index]
        _block_class.set_reg_type(_str)
        pass
    elif _field == IS_REG_FIELD:
        pass
    else:
        pass

    pass


def rtl_reg_register_block_process_v1(_block_param):
    """ Function rtl_reg_register_block_process
    @param : A block
    @return: A block class
    """
    _rtl_reg_block_class = RegisterBlockDetail()

    _last_process = IS_START
    field_info = []
    field_info.__init__()
    reg_field_block_list = []
    reg_field_block_list_raw = []

    for _line in _block_param:
        if is_str_empty(_line):
            pass
        elif is_comment_line(_line):
            pass
        else:  # is Register information
            # Should check current field in process for more flexible
            if is_reg_full_name_line(_line):
                if _last_process != IS_REG_FULL_NAME:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_FULL_NAME
                    field_info.__init__()
                    field_info.append(reg_full_name_get(_line))

            elif is_reg_short_name_line(_line):
                if _last_process != IS_REG_SHORT_NAME:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_SHORT_NAME
                    field_info.__init__()
                    field_info.append(reg_short_name_get(_line))

            elif is_reg_add_line(_line):
                if _last_process != IS_REG_ADD:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_ADD
                    field_info.__init__()
                    field_info.append(reg_add_get(_line))

            elif is_reg_formula_line(_line):
                if _last_process != IS_REG_ADD_FOR:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_ADD_FOR
                    field_info.__init__()
                    field_info.append(reg_add_get(_line))

            elif is_reg_formula_val_line(_line):
                if _last_process != IS_REG_ADD_FOR_VAL:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_ADD_FOR_VAL
                    field_info.__init__()
                    field_info.append(reg_formula_val_line_get(_line))

            elif is_reg_des_line(_line):
                if _last_process != IS_REG_DES:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_DES
                    field_info.__init__()
                    field_info.append(reg_des_line_get(_line))
            elif is_reg_width_line(_line):
                if _last_process != IS_REG_WIDTH:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_WIDTH
                    field_info.__init__()
                    field_info.append(reg_width_line_get(_line))

            elif is_reg_type_line(_line):
                if _last_process != IS_REG_TYPE:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_TYPE
                    field_info.__init__()
                    field_info.append(reg_type_line_get(_line))

            elif is_reg_field_line(_line):
                if _last_process != IS_REG_FIELD:
                    set_field(_last_process, field_info, _rtl_reg_block_class)
                    _last_process = IS_REG_FIELD
                    field_info.__init__()
                    field_info.append(reg_field_line_get(_line))
                else:
                    reg_field_block_list.append(reg_field_block_process(field_info))
                    reg_field_block_list_raw.append(reg_field_block_2_str(field_info))
                    _last_process = IS_REG_FIELD
                    field_info.__init__()
                    field_info.append(reg_field_line_get(_line))

            else:
                field_info.append(_line.strip("//").strip())

    # The last field block
    reg_field_block_list.append(reg_field_block_process(field_info))
    reg_field_block_list_raw.append(reg_field_block_2_str(field_info))

    # Process reg field block with unused field
    # try:
    # reg_field_block_list = reg_field_block_unused_process(reg_field_block_list)
    # except :
    # print("ERR REG: " + _rtl_reg_block_class.get_reg_name_long())

    _rtl_reg_block_class.set_reg_field_list(reg_field_block_list)
    _rtl_reg_block_class.set_reg_field_raw_list(reg_field_block_list_raw)

    # Strip
    _rtl_reg_block_class.set_reg_name_long(_rtl_reg_block_class.get_reg_name_long().lstrip().rstrip())
    _rtl_reg_block_class.set_reg_name_short(_rtl_reg_block_class.get_reg_name_short().lstrip().rstrip())
    _rtl_reg_block_class.set_reg_add(_rtl_reg_block_class.get_reg_add().lstrip().rstrip())
    _rtl_reg_block_class.set_reg_add_formula(_rtl_reg_block_class.get_reg_add_formula().lstrip("{").rstrip("}"))
    _rtl_reg_block_class.set_reg_des(_rtl_reg_block_class.get_reg_des().lstrip().rstrip())
    _rtl_reg_block_class.set_reg_name_long(_rtl_reg_block_class.get_reg_name_long().lstrip().rstrip())

    return _rtl_reg_block_class


def rtl_reg_block_2_md(_file_des, _reg_block_class):
    _file_des.write("\n")
    _file_des.write(MD_HEADING_L3 + _reg_block_class.get_reg_name_long() + "\n")
    _file_des.write("\n")
    _file_des.write(MD_REG_DES + "\n\n")
    for item in _reg_block_class.get_reg_des_list():
        if _reg_block_class.get_reg_des_list().index(item) == 0:
            _file_des.write(item + "\n\n")
        else:
            _file_des.write(item + "\n\n")

    _file_des.write("\n")

    _file_des.write(MD_REG_SHORT_NAME + "`" + _reg_block_class.get_reg_name_short() + "`" + "\n\n")
    _file_des.write(MD_REG_ADD + "`" + _reg_block_class.get_reg_add() + "`" + "\n\n")

    if len(_reg_block_class.get_reg_add_formula()) == 0:
        _file_des.write(MD_REG_ADD_FOR + "*" + "n/a" + "*" + "\n\n")
        _file_des.write(MD_REG_ADD_FOR_VAL + "*" + "n/a" + "*" + "\n\n")
    else:
        _file_des.write(MD_REG_ADD_FOR + "`" + _reg_block_class.get_reg_add_formula() + "`" + "\n\n")
        _file_des.write(MD_REG_ADD_FOR_VAL + "\n\n")
        for item in _reg_block_class.get_reg_val_dis():
            _file_des.write(MD_REG_ADD_FOR_VAL_DETAIL + "`" + item + "`" + "\n\n")

    if not is_str_empty(_reg_block_class.get_reg_width()):
        _file_des.write(MD_REG_WIDTH + "`" + _reg_block_class.get_reg_width() + "`" + "\n")
    else:
        _file_des.write(MD_REG_WIDTH + "`" + "n/a" + "`" + "\n")

    if not is_str_empty(_reg_block_class.get_reg_type()):
        _file_des.write(MD_REG_TYPE + "`" + _reg_block_class.get_reg_type() + "`" + "\n")
    else:
        _file_des.write(MD_REG_TYPE + "`" + "n/a" + "`" + "\n")

    _file_des.write(MD_REG_FIELD + "\n\n")
    _file_des.write(MD_REG_FIELD_TABLE_HEADER + "\n")

    for item in _reg_block_class.get_field_list():
        _file_des.write("|" + "`" + "[" + item.get_field_bit() + "]" + "`")
        _file_des.write("|" + "`" + item.get_field_name().lower() + "`")

        reg_field_des = ""

        if (item.get_field_name().lower() == REG_FIELD_UNUSED) or \
                (item.get_field_name().lower() == REG_FIELD_RESERVED) or \
                (item.get_field_name().lower() == REG_FIELD_RESERVE):

            reg_field_des += "*" + "n/a" + "*"
            reg_field_des += "<br>"
        else:
            reg_field_des += item.get_field_description()
            reg_field_des += "<br>"

        for item_ in item.get_field_example():
            if not is_str_empty(item_):
                reg_field_des += item_
                reg_field_des += "<br>"
        reg_field_des = reg_field_des.rstrip("br>")
        reg_field_des = reg_field_des.rstrip("<")
        _file_des.write("| " + reg_field_des)

        if not is_str_empty(item.get_field_type()):
            _file_des.write("| " + "`" + item.get_field_type() + "`")
        else:
            _file_des.write("| " + "*" + "n/a" + "*")

        if not is_str_empty(item.get_field_val_reset()):
            _file_des.write("| " + "`" + item.get_field_val_reset() + "`")
        else:
            _file_des.write("| " + "*" + "n/a" + "*")

        if not is_str_empty(item.get_field_val_def()):
            _file_des.write("| " + "`" + item.get_field_val_def() + "`" + "|\n")
        else:
            _file_des.write("| " + "*" + "n/a" + "*" + "|\n")


def rtl_bit_field_less_32_suffix(_item, reg_field, bit_left, bit_right, suffix, h_reg_short_name):
    reg_write_mask = 0x0
    field_name_str = h_reg_short_name + "_" + _item.get_field_name()

    if bit_left > 32:
        bit_left %= 32
    if bit_right >= 32:
        bit_right %= 32

    if bit_left == bit_right:
        bit_mask = "cBit" + str(bit_right)
    else:
        bit_mask = "cBit" + str(bit_left) + "_" + str(bit_right)

    try:
        if (_item.get_field_type().upper() != REG_FIELD_R_O) and \
                (_item.get_field_name().lower() != REG_FIELD_UNUSED):
            reg_write_mask |= (int(_item.get_field_val_max(), 0) << bit_right)

    except Exception as e:
        print(str(e) + _item.get_field_name())

    writeIOString(reg_field, H_REG_DEF_FORMAT %
                    (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + suffix + H_REG_FIELD_DEF_MASK_SUFFIX,
                     bit_mask))
    writeIOString(reg_field, H_REG_DEF_FORMAT %
                    (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + suffix + H_REG_FIELD_DEF_SHIFT_SUFFIX,
                     str(bit_right)))
    pass


def rtl_bit_field_less_32_01(_item, _reg_field, _bit_left, _bit_right, _h_reg_short_name):
    reg_write_mask = 0x0
    field_name_str = _h_reg_short_name + "_" + _item.get_field_name()

    if _bit_left > 32:
        _bit_left %= 32
    if _bit_right >= 32:
        _bit_right %= 32

    if _bit_left == _bit_right:
        bit_mask = "cBit" + str(_bit_right)
    else:
        bit_mask = "cBit" + str(_bit_left) + "_" + str(_bit_right)

    try:
        if (_item.get_field_type().upper() != REG_FIELD_R_O) and \
                (_item.get_field_name().lower() != REG_FIELD_UNUSED):
            reg_write_mask |= (int(_item.get_field_val_max(), 0) << _bit_right)
    except Exception as e:
        print(str(e) + _item.get_field_name())

    writeIOString(_reg_field, H_REG_DEF_FORMAT %
                     (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + H_REG_FIELD_DEF_MASK_SUFFIX,
                      bit_mask))
    writeIOString(_reg_field, H_REG_DEF_FORMAT %
                     (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + H_REG_FIELD_DEF_SHIFT_SUFFIX,
                      str(_bit_right)))
    pass


def rtl_bit_field_less_32_02(_item, _reg_field, _bit_left, _bit_right, _h_reg_short_name):
    # < 31 but in 2 area
    bit_left_01 = 31
    bit_right_01 = _bit_right % 32
    bit_left_02 = _bit_left % 32
    bit_right_02 = 0

    if bit_left_01 == bit_right_01:
        bit_mask = "cBit" + str(bit_right_01)
    else:
        bit_mask = "cBit" + str(bit_left_01) + "_" + str(bit_right_01)

    field_name_str = _h_reg_short_name + "_" + _item.get_field_name()
    writeIOString(_reg_field, H_REG_DEF_FORMAT %
                     (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + "_01" + H_REG_FIELD_DEF_MASK_SUFFIX,
                      bit_mask))
    writeIOString(_reg_field, H_REG_DEF_FORMAT %
                     (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + "_01" + H_REG_FIELD_DEF_SHIFT_SUFFIX,
                      str(bit_right_01)))

    if bit_left_02 == bit_right_02:
        bit_mask = "cBit" + str(bit_right_02)
    else:
        bit_mask = "cBit" + str(bit_left_02) + "_" + str(bit_right_02)

    writeIOString(_reg_field, H_REG_DEF_FORMAT %
                     (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + "_02" + H_REG_FIELD_DEF_MASK_SUFFIX,
                      bit_mask))
    writeIOString(_reg_field, H_REG_DEF_FORMAT %
                     (NamePrefix.REG_FIELD_DEF_PREFIX + field_name_str + "_02" + H_REG_FIELD_DEF_SHIFT_SUFFIX,
                      str(bit_right_02)))
    pass

def writeIOString(ioString, string):
    if not isinstance(string, unicode):
        string = unicode(string)
    ioString.write(string)

def add_reg_block_2_h(_file_des, _reg_block_class):
    """register to .h file
    :param _file_des:
    :param _reg_block_class:
    :return:
    """

    _file_des.write(H_REG_NEW)
    _file_des.write(H_REG_NAME_COM_START + "\n")
    _file_des.write(H_REG_INFO_NAME + _reg_block_class.get_reg_name_long() + "\n")
    _file_des.write(H_REG_INFO_ADD + _reg_block_class.get_reg_add() + "\n")
    _file_des.write(H_REG_INFO_ADD_FOR + _reg_block_class.get_reg_add_formula() + "\n")
    _file_des.write(H_REG_INFO_ADD_VAL + "\n")

    for item in _reg_block_class.get_reg_val_dis():
        _file_des.write(H_REG_INFO_ADD_VAL_L + item + "\n")

    reg_des = H_REG_INFO_DES + "\n"
    for line_des in _reg_block_class.get_reg_des_list():
        reg_des += (line_des + "\n")

    # Old format
    # reg_des_line = H_REG_INFO_DES + _reg_block_class.get_reg_des()
    # _file_des.write(textwrap.fill(reg_des_line, width=80))
    _file_des.write(reg_des)
    _file_des.write("\n")

    _file_des.write(H_REG_NAME_COM_END + "\n")
    h_reg_short_name = _reg_block_class.get_reg_name_short()
    _file_des.write(H_REG_DEF_FORMAT %
                            (NamePrefix.REG_DEF_PREFIX + h_reg_short_name + "_Base",
                             (_reg_block_class.get_reg_add_start()).replace('_', '')))

    try:
        if int(_reg_block_class.get_reg_width()) % 32:
            _file_des.write(H_REG_DEF_FORMAT %
                            (NamePrefix.REG_DEF_PREFIX + h_reg_short_name + H_REG_DEF_WRITE_WIDTH_SUFFIX,
                             int((int(int(_reg_block_class.get_reg_width()) / 32) + 1) * 32)))
    except Exception as e:
        print(str(e) + _reg_block_class.get_reg_width())

    reg_field = io.StringIO()

    for item in _reg_block_class.get_field_list():
        if item.get_field_name().lower() != REG_FIELD_UNUSED \
                and item.get_field_name().lower() != REG_FIELD_RESERVED \
                and item.get_field_name().lower() != REG_FIELD_RESERVE:
            writeIOString(reg_field, H_REG_FIELD_NEW)
            writeIOString(reg_field, H_REG_FIELD_START + "\n")
            writeIOString(reg_field, H_REG_FIELD_NAME + item.get_field_name() + "\n")
            writeIOString(reg_field, H_REG_FIELD_TYPE + item.get_field_type() + "\n")
            reg_field_des = H_REG_FIELD_DES + item.get_field_description()
            writeIOString(reg_field, textwrap.fill(reg_field_des, width=80))
            writeIOString(reg_field, "\n")
            writeIOString(reg_field, H_REG_FIELD_BIT + "[" + item.get_field_bit() + "]" + "\n")
            writeIOString(reg_field, H_REG_FIELD_END + "\n")

            bit_left = int(item.get_field_bit_left())
            bit_right = int(item.get_field_bit_right())

            if bit_left > 256:
                writeIOString(reg_field, "NOT SUPPORT UPTO NOW\n")

            elif (bit_left - bit_right) > 31:
                num_part = int((bit_left - bit_right) / 32) + 1
                # shift bit
                bit_right %= 32
                bit_left -= (int(bit_right / 32)) * 32
                if num_part == 2:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 32, "_02", h_reg_short_name)
                if num_part == 3:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 63, 32, "_02", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 64, "_03", h_reg_short_name)
                if num_part == 4:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 63, 32, "_02", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 95, 64, "_03", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 96, "_04", h_reg_short_name)
                if num_part == 5:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 63, 32, "_02", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 95, 64, "_03", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 127, 96, "_04", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 128, "_05", h_reg_short_name)
                if num_part == 6:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 63, 32, "_02", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 95, 64, "_03", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 127, 96, "_04", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 159, 128, "_05", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 160, "_06", h_reg_short_name)
                if num_part == 7:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 63, 32, "_02", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 95, 64, "_03", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 127, 96, "_04", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 159, 128, "_05", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 191, 160, "_06", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 192, "_07", h_reg_short_name)
                if num_part == 8:
                    rtl_bit_field_less_32_suffix(item, reg_field, 31, bit_right, "_01", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 63, 32, "_02", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 95, 64, "_03", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 127, 96, "_04", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 159, 128, "_05", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 191, 160, "_06", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, 223, 192, "_07", h_reg_short_name)
                    rtl_bit_field_less_32_suffix(item, reg_field, bit_left, 224, "_08", h_reg_short_name)
            # < 31 but in 2 area
            elif (bit_left - bit_right) <= 31 and (int(bit_left / 32) != int(bit_right / 32)):
                rtl_bit_field_less_32_02(item, reg_field, bit_left, bit_right, h_reg_short_name)
            # < 31 but in 1 area
            else:
                rtl_bit_field_less_32_01(item, reg_field, bit_left, bit_right, h_reg_short_name)

    _file_des.write(reg_field.getvalue())

    reg_field.close()


def error_2_report(error, reg, at):
    # Log file format
    # ERROR:
    # RULE_01
    # POS:
    # Register:
    # Field or Formula...

    report = ""
    report.append("\nERROR:\n")
    report.append(error)
    report.append("\n")
    report.append("POS:\n")
    report.append("    Register:" + reg)
    report.append("\n")
    report.append("    At:" + at)
    report.append("\n")

    return report


RULE_01 = "RULE_01: Bit field row must have 6 tokens"
RULE_02 = "RULE_02: Start add in Formula must = start in Address"
RULE_03 = "RULE_03: Access type must is: R/W or R/W/C or R_O or R2C"
RULE_04 = "RULE_04: RTL Instant Name must different in doc"


def rtl_reg_syn_check(_reg_filename, _log_pathname, _rtl_reg_block_class_list):
    """ Function rtl_reg_syn_check
    This function will check all syntax of rtl file and out to log file
    @param : file_des -> Log file
    @param : reg_block_class_list -> List class block
    @return: True is all OK
    """

    # Rule number 1
    # Bit field row must have 6 tokens
    # Rule number 2
    # Start add in Formula must = start in Address
    # Rule number 3
    # Access type must is: R/W - R/W/C - R_O - R2C
    # Rule number 4
    # RTL Instant Name must different in doc

    # Log file format
    # ERROR:
    # RULE_01
    # POS:
    # Register:
    # Field or Formula...

    return True

    # Open .log file to w
    try:
        log_file_des = open(_log_pathname, "w")
    except IOError:
        print("%s -> File could not be opened" % _log_pathname)
        sys.exit()

    for x in _rtl_reg_block_class_list:
        print(x)
        for y in x.get_field_raw_list():
            print(x.get_field_raw_list())
            # Check rule 1
            token = re.split(RTL_REG_DELIMITER, y)

            if list_len(token) < 6:
                print("FAIL")
                log_file_des.write(error_2_report(RULE_01, x.reg_name_long(), y))

    # Close
    try:
        log_file_des.close()

    except IOError:
        print("File could not be closed")
        sys.exit()

    return True


def get_basename(_i_filename):
    # path_filename = _i_filename.replace("\\", "/")
    # dirname = os.path.dirname(_filename_param)
    return os.path.basename(_i_filename)


def get_pathname_md(_i_filename):

    filename_split = _i_filename.split(".")
    _filename = filename_split[0] + MD_EXT
    _o_pathname = MD_FOLDER + "\\" + _filename
    return _o_pathname.replace("\\", "/")

def get_pathname_py(_i_filename):
    filename_split = _i_filename.split(".")
    _filename = filename_split[0] + ".py"
    _o_pathname = PY_FOLDER + "\\" + _filename
    return _o_pathname.replace("\\", "/")

def get_pathname_h(_i_filename):
    """Get .h pathname
    [COM][PRODUCT_NAME][block].h
    :param _i_filename:
    :return: h pathname
    """
    filename_split = _i_filename.split(".")
    filename_lower = filename_split[0].lower()
    
    # Not use PRODUCT_LINE
    h_filename = "At"

    for x in PRODUCT_NAME:
        if filename_lower.find(x[0].lower()) != -1:
            h_filename += x[0].upper()
            break

    for block in H_REG_BLK_OFFSET:
        if filename_lower.find(block[0].lower()) != -1:
            h_filename += block[0]
            h_filename += "Reg"
            h_filename += R_EXT
            h_filename = h_filename.replace("_", "")
            break

    h_filename = filename_split[0]
    h_filename += R_EXT
    h_filename = h_filename.replace("_", "")
    h_pathname = H_FOLDER + "\\" + h_filename
    return h_pathname.replace("\\", "/")


def get_pathname_mem(_i_filename):
    filename_split = _i_filename.split(".")
    h_filename_1 = filename_split[0] + "_MEM" + H_EXT
    h_pathname_1 = H_FOLDER + "\\" + h_filename_1
    return h_pathname_1.replace("\\", "/")


def get_pathname_tcl(_i_filename):
    filename_split = _i_filename.split(".")
    tcl_filename = filename_split[0] + "_TCL" + TCL_EXT
    tcl_pathname = H_FOLDER + "\\" + tcl_filename
    return tcl_pathname.replace("\\", "/")


def get_pathname_log(_i_filename):
    log_filename = "rtl" + LOG_EXT
    log_pathname = LOG_FOLDER + "\\" + log_filename
    return log_pathname.replace("\\", "/")


def get_pathname_ref(_i_filename):
    """Get ref filename
    [AF5][PRODUCT][BLOCK][REG][REG][.r]
    :param _i_filename:
    :return:
    """
    filename_split = _i_filename.split(".")
    filename_lower = filename_split[0].lower()

    # Original filename
    reg_des_filename = filename_lower

    # Shorter filename
    for pro_line in PRODUCT_LINE:
        if filename_lower.find(pro_line[0].lower()) != -1:
            reg_des_filename = pro_line[0].upper() + "_"
            break
        else:
            reg_des_filename = "AT"

    for x in PRODUCT_NAME:
        if filename_lower.find(x[0].lower()) != -1:
            reg_des_filename += x[0].upper()
            reg_des_filename += "_REG_"
            break

    for block in H_REG_BLK_OFFSET:
        if filename_lower.find(block[0].lower()) != -1:
            reg_des_filename += block[0].upper()
            reg_des_filename += "_REF"
            reg_des_filename += REG_FIELD_EXT
            break

    reg_des_pathname = H_FOLDER + "\\" + reg_des_filename
    return reg_des_pathname.replace("\\", "/")


def rtl_reg_file_parse(_reg_pathname):
    """Process all file, output is list off block register
    :param _reg_pathname: Filename
    :return: A list all block register, include comment line
    """

    # Open rtl register file to read
    try:
        rtl_file_hdl = open(_reg_pathname, "r")
    except IOError:
        print("%s -> File could not be opened" % _reg_pathname)
        sys.exit()

    # File rtl process
    block_number = 0  # Number block in file
    rtl_reg_block_list = []  # RTL register block list
    is_reg_block = False

    for _line in rtl_file_hdl:
        if rtl_reg_is_block_delimitation(_line):
            is_reg_block = True
            block_number += 1
            rtl_reg_block_list.append([])
            rtl_reg_block_list[block_number - 1].append(_line)
            continue
        else:
            if is_reg_block:  # This for prevent the first of file is not register block
                rtl_reg_block_list[block_number - 1].append(_line)

    try:
        rtl_file_hdl.close()
    except IOError:
        print("%s -> File could not be close" % _reg_pathname)

    return rtl_reg_block_list

    pass


def reg_table_get(_reg_block_class_l):
    # Put all reg to reg_table (begin of .md file)
    reg_table = ["|Name|Address|\n|-----|-----|"]

    for block in _reg_block_class_l:
        reg_table.append("|" + "`" + block.get_reg_name_long() + "`" + "|" + "`" + block.get_reg_add() + "`" + "|")

    return reg_table

    pass


def rtl_reg_block_2_block_class(_rtl_reg_block_l):
    """Process all block, correct something
    :param _rtl_reg_block_l:
    "return: A list of register block class (RegisterBlockDetail)
    """

    rtl_reg_block_class_list = []
    block_number = list_len(_rtl_reg_block_l)

    # Block process
    for _idx in range(block_number):
        # is Register define Block
        rtl_reg_block_class = rtl_reg_register_block_process_v1(_rtl_reg_block_l[_idx])
        rtl_reg_block_class_01 = rtl_reg_register_block_process_v1(_rtl_reg_block_l[_idx])
        rtl_reg_block_class_02 = rtl_reg_register_block_process_v1(_rtl_reg_block_l[_idx])

        # process this class
        add = re.split(RTL_REG_DELIMITER, rtl_reg_block_class.get_reg_add())
        formula = rtl_reg_block_class.get_reg_add_formula()
        # strip formula
        formula.strip()
        name_short = rtl_reg_block_class.get_reg_name_short()
        if list_len(add) == 1:
            # Update Address in formula
            rtl_reg_block_class.set_reg_add_start(reg_add_start_get(rtl_reg_block_class.get_reg_add()))
            rtl_reg_block_class.set_reg_add_end(reg_add_end_get(rtl_reg_block_class.get_reg_add()))
            if formula == REG_ADD_FOR_PAT_NA:
                rtl_reg_block_class.set_reg_add_formula("")
            else:
                rtl_reg_block_class.set_reg_add_formula(
                    (rtl_reg_block_class.get_reg_add_formula()).replace("Address",
                                                                        rtl_reg_block_class.get_reg_add_start()))
            rtl_reg_block_class_list.append(rtl_reg_block_class)

        else:
            # Update Address in formula
            add[0] = add[0].strip()
            reg_type = re.split("\(", add[0].rstrip("\)"), 1)
            rtl_reg_block_class_01.set_reg_name_short(
                name_short + "_" + reg_type[1].lower().replace("/", "_").replace("\\", "_"))
            rtl_reg_block_class_01.set_reg_add(add[0])
            rtl_reg_block_class_01.set_reg_add_start(reg_add_start_get(rtl_reg_block_class_01.get_reg_add()))
            rtl_reg_block_class_01.set_reg_add_end(reg_add_end_get(add[0]))

            if formula == REG_ADD_FOR_PAT_NA or is_str_empty(formula.lower().strip("address")):
                rtl_reg_block_class_01.set_reg_add_formula("")
            else:
                rtl_reg_block_class_01.set_reg_add_formula(
                    formula.replace("Address", rtl_reg_block_class_01.get_reg_add_start()))

            rtl_reg_block_class_list.append(rtl_reg_block_class_01)

            # Update Address in formula
            add[1] = add[1].strip()
            reg_type = re.split("\(", add[1].rstrip("\)"), 1)
            rtl_reg_block_class_02.set_reg_name_short(
                name_short + "_" + reg_type[1].lower().replace("/", "_").replace("\\", "_"))
            rtl_reg_block_class_02.set_reg_add(add[1])
            rtl_reg_block_class_02.set_reg_add_start(reg_add_start_get(rtl_reg_block_class_02.get_reg_add()))
            rtl_reg_block_class_02.set_reg_add_end(reg_add_end_get(add[1]))

            if formula == REG_ADD_FOR_PAT_NA or is_str_empty(formula.lower().strip("address")):
                rtl_reg_block_class_02.set_reg_add_formula("")
            else:
                rtl_reg_block_class_02.set_reg_add_formula(
                    formula.replace("Address", rtl_reg_block_class_02.get_reg_add_start()))

            rtl_reg_block_class_list.append(rtl_reg_block_class_02)

    return rtl_reg_block_class_list
    pass


def md_file_generate(_reg_filename, _md_pathname, _rtl_reg_block_class_list):

    # Open .md file to w
    try:
        md_file_des = open(_md_pathname, "w")
    except IOError:
        print("%s -> File could not be opened" % _md_pathname)
        sys.exit()

    reg_name = _reg_filename.split(".")

    # Write header to file .md
    # Get datetime to put to history
    now = datetime.datetime.now()
    md_file_des.write(MD_HEADER)
    md_file_des.write("|1.0|" + now.strftime("%Y-%m-%d") + "|" + NamePrefix.PRO_LINE.upper() + "Project|Initial version|\n\n\n\n\n")
    md_file_des.write(MD_HEADING_L2 + reg_name[0] + "\n")
    md_file_des.write(MD_HEADING_L4 + "Register Table" "\n\n")

    reg_table = reg_table_get(_rtl_reg_block_class_list)

    for item in reg_table:
        md_file_des.write(item + "\n")
    md_file_des.write("\n")

    # Register block to .md file
    for block in _rtl_reg_block_class_list:
        md_file_des.write("\n")
        md_file_des.write(MD_HEADING_L3 + block.get_reg_name_long() + "\n")
        md_file_des.write("\n")
        md_file_des.write(MD_REG_DES + "\n\n")
        for item in block.get_reg_des_list():
            if block.get_reg_des_list().index(item) == 0:
                md_file_des.write(item + "\n\n")
            else:
                md_file_des.write(item + "\n\n")

        md_file_des.write("\n")

        md_file_des.write(MD_REG_SHORT_NAME + "`" + block.get_reg_name_short() + "`" + "\n\n")
        md_file_des.write(MD_REG_ADD + "`" + block.get_reg_add() + "`" + "\n\n")

        if len(block.get_reg_add_formula()) == 0:
            md_file_des.write(MD_REG_ADD_FOR + "*" + "n/a" + "*" + "\n\n")
            md_file_des.write(MD_REG_ADD_FOR_VAL + "*" + "n/a" + "*" + "\n\n")
        else:
            md_file_des.write(MD_REG_ADD_FOR + "`" + block.get_reg_add_formula() + "`" + "\n\n")
            md_file_des.write(MD_REG_ADD_FOR_VAL + "\n\n")
            for item in block.get_reg_val_dis():
                md_file_des.write(MD_REG_ADD_FOR_VAL_DETAIL + "`" + item + "`" + "\n\n")

        if not is_str_empty(block.get_reg_width()):
            md_file_des.write(MD_REG_WIDTH + "`" + block.get_reg_width() + "`" + "\n")
        else:
            md_file_des.write(MD_REG_WIDTH + "`" + "n/a" + "`" + "\n")

        if not is_str_empty(block.get_reg_type()):
            md_file_des.write(MD_REG_TYPE + "`" + block.get_reg_type() + "`" + "\n")
        else:
            md_file_des.write(MD_REG_TYPE + "`" + "n/a" + "`" + "\n")

        md_file_des.write(MD_REG_FIELD + "\n\n")
        md_file_des.write(MD_REG_FIELD_TABLE_HEADER + "\n")

        for item in block.get_field_list():
            md_file_des.write("|" + "`" + "[" + item.get_field_bit() + "]" + "`")
            md_file_des.write("|" + "`" + item.get_field_name().lower() + "`")

            reg_field_des = ""

            if (item.get_field_name().lower() == REG_FIELD_UNUSED) or \
                    (item.get_field_name().lower() == REG_FIELD_RESERVED) or \
                    (item.get_field_name().lower() == REG_FIELD_RESERVE):

                reg_field_des += "*" + "n/a" + "*"
                reg_field_des += "<br>"
            else:
                reg_field_des += item.get_field_description()
                reg_field_des += "<br>"

            for item_ in item.get_field_example():
                if not is_str_empty(item_):
                    reg_field_des += item_
                    reg_field_des += "<br>"
            reg_field_des = reg_field_des.rstrip("br>")
            reg_field_des = reg_field_des.rstrip("<")
            md_file_des.write("| " + reg_field_des)

            if not is_str_empty(item.get_field_type()):
                md_file_des.write("| " + "`" + item.get_field_type() + "`")
            else:
                md_file_des.write("| " + "*" + "n/a" + "*")

            if not is_str_empty(item.get_field_val_reset()):
                md_file_des.write("| " + "`" + item.get_field_val_reset() + "`")
            else:
                md_file_des.write("| " + "*" + "n/a" + "*")

            if not is_str_empty(item.get_field_val_def()):
                md_file_des.write("| " + "`" + item.get_field_val_def() + "`" + "|\n")
            else:
                md_file_des.write("| " + "*" + "n/a" + "*" + "|\n")

    # Close
    try:
        md_file_des.close()
    except IOError:
        print("File could not be closed")
        sys.exit()

    pass


def h_file_generate(_reg_filename, _h_pathname, _rtl_reg_block_class_list):

    # Open .h file to w
    try:
        h_file_des = open(_h_pathname, "w")
    except IOError:
        print("%s -> File could not be opened" % _h_pathname)
        sys.exit()

    reg_name = _reg_filename.split(".")

    # Header
    h_file_des.write(H_HEADER)
    h_file_des.write("\n")
    h_file_des.write("#ifndef " + "_" + NamePrefix.PRO_LINE.upper() + "_REG_" + reg_name[0] + "_H_")
    h_file_des.write("\n")
    h_file_des.write("#define " + "_" + NamePrefix.PRO_LINE.upper() + "_REG_" + reg_name[0] + "_H_")
    h_file_des.write("\n")
    h_file_des.write("\n")
    h_file_des.write(H_REG_DEF_START)
    h_file_des.write("\n")

    # Body
    for block in _rtl_reg_block_class_list:
        add_reg_block_2_h(h_file_des, block)

    # Footer
    h_file_des.write("\n")
    h_file_des.write("#endif /* " + "_" + NamePrefix.PRO_LINE.upper() + "_REG_" + reg_name[0] + "_H_" + " */")
    h_file_des.write("\n")

    try:
        h_file_des.close()
    except IOError:
        print("File could not be closed")
        sys.exit()

    pass


def block_2_detail_ref_add(_file_name, _file_des, _reg_block_class):
    if _reg_block_class.get_reg_name_long().lower().find("hold") != -1:
        return

    mul_32 = 1

    if int(_reg_block_class.get_reg_width()) % 32:
        mul_32 = int(int(_reg_block_class.get_reg_width()) / 32) + 1
    else:
        mul_32 = int(_reg_block_class.get_reg_width()) / 32

    add = _reg_block_class.get_reg_add_start()

    add_start = add
    if add.lower().find("\("):
        split = re.split("\(", add, 1)
        add_start = split[0]

    add_end = _reg_block_class.get_reg_add_end()
    if is_str_empty(_reg_block_class.get_reg_add_end()):
        add_end = add_start

    add_start = str_strip(add_start, "_")
    add_end = str_strip(add_end, "_")

    reg_val_init = "{0x0, 0x0, 0x0, 0x0, 0x0, 0x0}"

    block_name_short = ""
    product_name_short = "ce24"

    name = _file_name.lower()
    for x in H_REG_BLK_OFFSET:
        if name.find(x[0].lower()) != -1:
            block_name_short = x[0].lower()
            break

    for x in PRODUCT_NAME:
        if name.find(x[0].lower()) != -1:
            product_name_short = x[0].lower()
            break

    max_field = 0
    field_ref = product_name_short + "_" + block_name_short + "_" + _reg_block_class.get_reg_name_short()
    field_str_ref = product_name_short + "_" + block_name_short + "_" + _reg_block_class.get_reg_name_short() + "_str"

    reg_field_p_init = "{null}"
    reg_name = _reg_block_class.get_reg_name_long()
    if is_str_empty(_reg_block_class.get_reg_add_formula()):
        reg_formula = "\"" + "(none)" + "\""
    else:
        reg_formula = _reg_block_class.get_reg_add_formula()
        reg_formula = str_strip(reg_formula, "_")
        reg_formula = expression_format(reg_formula)
        reg_formula = "\"" + reg_formula + "\""

    if is_str_empty(_reg_block_class.get_reg_add_formula()):
        reg_formula_detail = ""
    else:
        _val_num = 0;
        reg_formula_var_detail = ""
        for _val in _reg_block_class.get_reg_val_dis_list():
            _val_num += 1
            reg_formula_var_str = ""
            reg_formula_var_str += "{"
            reg_formula_var_str += "\""
            reg_formula_var_str += _val.get_val_name()
            reg_formula_var_str += "\""
            reg_formula_var_str += ", "
            reg_formula_var_str += _val.get_val_min()
            reg_formula_var_str += ", "
            reg_formula_var_str += _val.get_val_max()
            reg_formula_var_str += "}"
            reg_formula_var_detail += reg_formula_var_str + ", "
        reg_formula_detail = ", {" + str(_val_num) + ", {" + reg_formula_var_detail + "}}"

    reg_formula_detail = str_strip(reg_formula_detail, "_")

    _file_des.write(
        "{" + str(int(mul_32)) + "," + add_start + "," + add_end + "," + reg_val_init + "," + str(
            max_field) + "," + field_str_ref + "," + field_ref + "," + reg_field_p_init + "," + "\"" + reg_name + "\"" + "," + reg_formula + reg_formula_detail + "},\n")


def block_2_field_add(_file_name, _file_des, _reg_block_class):
    """To all field reference to use in tool register read/write
    :param _file_name:
    :param _file_des:
    :param _reg_block_class:
    :return:
    """

    if _reg_block_class.get_reg_name_long().lower().find("hold") != -1:
        return

    block_name_short = ""
    product_name_short = "ce24"

    name = _file_name.lower()
    for x in H_REG_BLK_OFFSET:
        if name.find(x[0].lower()) != -1:
            block_name_short = x[0].lower()
            break

    for x in PRODUCT_NAME:
        if name.find(x[0].lower()) != -1:
            product_name_short = x[0].lower()
            break

    _file_des.write(
        "tRegFieldBit " + product_name_short + "_" + block_name_short + "_" +
        _reg_block_class.get_reg_name_short() + "[] = \n")
    _file_des.write("    {\n")

    for item in _reg_block_class.get_field_list():
        bit_left = int(item.get_field_bit_left())
        bit_right = int(item.get_field_bit_right())
        is_rd_wr = 0

        if item.get_field_type().upper() != REG_FIELD_R_W:
            is_rd_wr = 0
        elif item.get_field_name().lower() != REG_FIELD_UNUSED:
            is_rd_wr = 1

        _file_des.write("    {" + str(bit_left) + ", " + str(bit_right) + ", 0x0" + ", " + str(is_rd_wr) + "},\n")

    _file_des.write("    };\n")
    _file_des.write(
        "static char *" + product_name_short + "_" + block_name_short + "_" +
        _reg_block_class.get_reg_name_short() + "_str[] = \n")
    _file_des.write("    {\n")

    for item in _reg_block_class.get_field_list():
        _file_des.write("    \"" + item.get_field_name() + "\",\n")

    _file_des.write("    (void*)0,\n")
    _file_des.write("    };\n\n\n")


def ref_file_generate(_reg_filename, _ref_pathname, _rtl_reg_block_class_list):
    # Open .ref file to w
    try:
        ref_file_des = open(_ref_pathname, "w")
    except IOError:
        print("%s -> File could not be opened" % _ref_pathname)
        sys.exit()

    reg_name = _reg_filename.split(".")

    for block_reg in _rtl_reg_block_class_list:
        block_2_field_add(reg_name[0], ref_file_des, block_reg)

    product_name = NamePrefix.PRO_NAME.lower()
    block_name = NamePrefix.BLK_NAME.lower()

    ref_file_des.write("tRegFieldBit " + product_name + "_" + block_name + "_" + "default[] = \n")
    ref_file_des.write("{\n")
    ref_file_des.write("{0, 0, 0x0},\n")
    ref_file_des.write("};\n")
    ref_file_des.write("static char *" + product_name + "_" + block_name + "_" + "default_str[] = \n")
    ref_file_des.write("{\n")
    ref_file_des.write("\"unused\",\n")
    ref_file_des.write("};\n")
    ref_file_des.write("\n\n\n\n\n")
    ref_file_des.write("tAtRegDes " + product_name + "_" + block_name + "_" + "reg_p[] = \n")
    ref_file_des.write("{\n")

    # Register block to .reg_ref file
    for reg_block in _rtl_reg_block_class_list:
        block_2_detail_ref_add(reg_name[0], ref_file_des, reg_block)

    ref_file_des.write("};")
    ref_file_des.write("\n")

    try:
        ref_file_des.close()
    except IOError:
        print("File could not be closed")
        sys.exit()

    pass


def h_mem_file_generate(_reg_filename, _mem_pathname, _rtl_reg_block_class_list):
    # Open .h file to w
    try:
        mem_file_des = open(_mem_pathname, "w")
    except IOError:
        print("%s -> File could not be opened" % _mem_pathname)
        sys.exit()

    reg_name = _reg_filename.split(".")

    num_reg = 0

    offset = "0x0"
    name = _reg_filename.lower()
    for x in H_REG_BLK_OFFSET:
        if name.find(x[0].lower()) != -1:
            offset = x[1]

    offset_str = "".join("0x%08x" % int(offset, 16))

    for _reg in _rtl_reg_block_class_list:
        if is_none_formula(_reg.get_reg_add_formula()):
            num_reg += 1
        else:
            num_in_reg = 1
            for _val in _reg.get_reg_val_dis_list():
                num_in_reg *= (int(_val.get_val_max()) + 1)

            num_reg += num_in_reg

    mem_file_des.write("uint32 " + reg_name[0] + "[] = {" + "\n")

    for _reg in _rtl_reg_block_class_list:
        reg_write_mask = 0x0
        for item in _reg.get_field_list():
            if item.get_field_type().upper() == REG_FIELD_R_W \
                    and item.get_field_name().lower() != REG_FIELD_UNUSED:
                try:
                    reg_write_mask |= (int(item.get_field_val_max(), 0) << int(item.get_field_bit_right()))
                except Exception as e:
                    print(str(e) + _reg.get_reg_name_long())

        if reg_write_mask == 0x0:
            continue

        if is_list_empty(_reg.get_reg_add_formula()):
            reg_write_mask_str = "".join("0x%08x" % reg_write_mask)
            add_str = "".join("0x%08x" % int(str_strip(_reg.get_reg_add_start(), "_"), 16))
            real_add_str = "".join("0x%08x" % (int(offset_str, 16) + int(add_str, 16)))

            mem_file_des.write(real_add_str + ", " + reg_write_mask_str + ", " + "\n")
        else:
            if not is_str_empty(_reg.get_reg_add_end()):

                # For complete formula
                num_val = list_len(_reg.get_reg_val_dis_list())
                # List variable
                for_list_level = [[] for _ in range(num_val + 1)]

                # The first level in formula
                for_list_level[0].append(_reg.get_reg_add_formula())

                # Replace step by step variable in list
                level = 1
                for x in _reg.get_reg_val_dis_list():
                    if is_str_empty(x.get_val_min()) or is_str_empty(x.get_val_max()):
                        print("ERROR:")
                        print("CHECK FORMULA or WHERE or ADDRESS:")
                        print(_reg.get_reg_name_long())
                    else:
                        pass
                        _min = int(x.get_val_min())
                        _max = int(x.get_val_max())
                        if _reg.get_reg_add_formula().find("$") != -1:
                            _val_name = "$" + x.get_val_name()
                        else:
                            _val_name = x.get_val_name()

                        for y in for_list_level[level - 1]:
                            for cnt in range(_max - _min + 1):
                                for_list_level[level].append(y.replace(_val_name, str(cnt + _min)))
                        level += 1

                for x in for_list_level[num_val]:
                    reg_write_mask_str = "".join("0x%08x" % reg_write_mask)
                    add_str = "".join("0x%08x" % calc((str_strip(x, "_"))))
                    real_add_str = "".join("0x%08x" % (int(offset_str, 16) + int(add_str, 16)))
                    mem_file_des.write(real_add_str + ", " + reg_write_mask_str + ", " + "\n")
            else:
                reg_write_mask_str = "".join("0x%08x" % reg_write_mask)
                add_str = "".join("0x%08x" % int(str_strip(_reg.get_reg_add_start(), "_"), 16))
                real_add_str = "".join("0x%08x" % (int(offset_str, 16) + int(add_str, 16)))
                mem_file_des.write(real_add_str + ", " + reg_write_mask_str + ", " + "\n")

    mem_file_des.write("};")
    mem_file_des.write("\n")
    mem_file_des.write("uint32 " + reg_name[0] + "_SIZE = sizeof(" + reg_name[0] + ") / (sizeof(uint32*) * 2);" + "\n")

    # These are the end of file before
# Just try to gen MEM file
# mem_filename = "CE24_REG_MEM.c"
# mem_pathname = H_FOLDER + "\\" + mem_filename
# mem_pathname = mem_pathname.replace("\\", "/")
# try:
#     mem_file_ = open(mem_pathname, "w")
#     print("Opend -> %s" % mem_pathname)
# except IOError:
#     print("%s -> File could not be opened" % mem_pathname)
#     sys.exit()
#
# mem_file_.write("#include \"atstd.h\" \n\n")

# for root, dirs, files in os.walk(H_FOLDER):
#    for file in files:
#        if fnmatch.fnmatch(file, H_MEM_EXT):
#            temp_pathname = H_FOLDER + "\\" + file
#            temp_pathname = temp_pathname.replace("\\", "/")
#            try:
#                temp_file = open(temp_pathname, "r")
#            except IOError:
#                print("%s -> File could not be opened" % temp_pathname)
#                sys.exit()
#
#            for line in temp_file:
#                mem_file_.write(line)
#
#            mem_file_.write("\n\n")
#            temp_file.close()
#
# mem_file_.close()

    try:
        mem_file_des.close()
    except IOError:
        print("File could not be closed")
        sys.exit()


def tcl_file_generate(_reg_filename, _tcl_pathname, _rtl_reg_block_class_list):
    # Open .tcl file to w
    try:
        tcl_file_des = open(_tcl_pathname, "w")
    except IOError:
        print("%s -> File could not be opened" % _tcl_pathname)
        sys.exit()

    tcl_file_des.write("\n")
    tcl_file_des.write("namespace import atsdk::*\n")

    num_reg = 0

    offset = "0x0"
    name = _reg_filename.lower()
    for x in H_REG_BLK_OFFSET:
        if name.find(x[0].lower()) != -1:
            offset = x[1]

    offset_str = "".join("0x%08x" % int(offset, 16))

    for _reg in _rtl_reg_block_class_list:
        if is_none_formula(_reg.get_reg_add_formula()):
            num_reg += 1
        else:
            num_in_reg = 1
            for _val in _reg.get_reg_val_dis_list():
                num_in_reg *= (int(_val.get_val_max()) + 1)

            num_reg += num_in_reg

    for _reg in _rtl_reg_block_class_list:
        reg_write_mask = 0x0
        for item in _reg.get_field_list():
            if (item.get_field_type().upper() == REG_FIELD_R_W) \
                    and (item.get_field_name().lower() != REG_FIELD_UNUSED):
                try:
                    reg_write_mask |= (int(item.get_field_val_max(), 0) << int(item.get_field_bit_right()))
                except Exception as e:
                    print(str(e) + _reg.get_reg_name_long())

        if reg_write_mask == 0x0:
            continue

        if is_list_empty(_reg.get_reg_add_formula()):
            reg_write_mask_str = "".join("0x%08x" % reg_write_mask)
            add_str = "".join("0x%08x" % int(str_strip(_reg.get_reg_add_start(), "_"), 16))
            real_add_str = "".join("0x%08x" % (int(offset_str, 16) + int(add_str, 16)))

            tcl_file_des.write("wr " + real_add_str + " " + reg_write_mask_str + "\n")
            tcl_file_des.write("rd " + real_add_str + "\n")
        else:
            if not is_str_empty(_reg.get_reg_add_end()):

                # For complete formula
                num_val = list_len(_reg.get_reg_val_dis_list())
                # List variable
                for_list_level = [[] for _ in range(num_val + 1)]

                # The first level in formula
                for_list_level[0].append(_reg.get_reg_add_formula())

                # Replace step by step variable in list
                level = 1
                for x in _reg.get_reg_val_dis_list():
                    if is_str_empty(x.get_val_min()) or is_str_empty(x.get_val_max()):
                        print("ERROR:")
                        print("CHECK FORMULA or WHERE or ADDRESS:")
                        print(_reg.get_reg_name_long())
                    else:
                        pass
                        _min = int(x.get_val_min())
                        _max = int(x.get_val_max())
                        if _reg.get_reg_add_formula().find("$") != -1:
                            _val_name = "$" + x.get_val_name()
                        else:
                            _val_name = x.get_val_name()

                        for y in for_list_level[level - 1]:
                            for cnt in range(_max - _min + 1):
                                for_list_level[level].append(y.replace(_val_name, str(cnt + _min)))
                        level += 1

                for x in for_list_level[num_val]:
                    reg_write_mask_str = "".join("0x%08x" % reg_write_mask)
                    add_str = "".join("0x%08x" % calc((str_strip(x, "_"))))
                    real_add_str = "".join("0x%08x" % (int(offset_str, 16) + int(add_str, 16)))
                    tcl_file_des.write("wr " + real_add_str + " " + reg_write_mask_str + "\n")
                    tcl_file_des.write("rd " + real_add_str + "\n")
            else:
                reg_write_mask_str = "".join("0x%08x" % reg_write_mask)
                add_str = "".join("0x%08x" % int(str_strip(_reg.get_reg_add_start(), "_"), 16))
                real_add_str = "".join("0x%08x" % (int(offset_str, 16) + int(add_str, 16)))
                tcl_file_des.write("wr " + real_add_str + " " + reg_write_mask_str + "\n")
                tcl_file_des.write("rd " + real_add_str + "\n")

    # Close
    try:
        tcl_file_des.close()
    except IOError:
        print("File could not be closed")
        sys.exit()

    pass


def pre_process_filename(_file_name):

    NamePrefix.COM_NAME = "at".title()
    NamePrefix.PRO_LINE =  product_line_prefix_get(_file_name).title()
    NamePrefix.PRO_NAME =  product_name_prefix_get(_file_name).title()
    NamePrefix.BLK_NAME =  block_function_prefix_get(_file_name).title()
    NamePrefix.REG_DEF_PREFIX =  "#define c" + NamePrefix.PRO_LINE + "Reg_"
    NamePrefix.REG_FIELD_DEF_PREFIX = "#define c" + NamePrefix.PRO_LINE + "_"

    pass

def failString():
    return "[" + Color.RED + "FAIL" + Color.END + "]"

def doneString():
    return "[" + Color.GREEN + "DONE" + Color.END + "]"

def rtl_reg_file_process(_filename_param):
    """Process register file
    :param _filename_param:
    :return:
    """

    # Input filename parse
    reg_pathname = _filename_param.replace("\\", "/")
    basename = get_basename(_filename_param)

    # Some process on filename
    pre_process_filename(basename)

    # Create file to export (.md/.h/.ref/.log...)
    # File name process
    # .md file to w
    md_pathname = get_pathname_md(basename)

    # .h file to w
    h_pathname = get_pathname_h(basename)

    # .h file to w
    mem_pathname = get_pathname_mem(basename)

    # .tcl file to w
    tcl_pathname = get_pathname_tcl(basename)

    # .log file to w
    log_pathname = get_pathname_log(basename)

    # .ref file to w
    ref_pathname = get_pathname_ref(basename)

    # File rtl process
    rtl_reg_block_list = rtl_reg_file_parse(reg_pathname)

    # Get all lines valid (exclude all comment)
    rtl_reg_block_class_list = rtl_reg_block_2_block_class(rtl_reg_block_list)

    # Syntax check
    message('{0:s} {1:<80s}'.format("\tSyntax checking", "[START]"))
    reg_syntax_check = rtl_reg_syn_check(basename, log_pathname, rtl_reg_block_class_list)
    message('{0:s} {1:<80s}'.format("\tSyntax checking", doneString()))

    if not reg_syntax_check:
        print("RTL REGISTER FILE SYNTAX NOT GOOD\n")
        return

    try:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", md_pathname, "[START]"))
        md_file_generate(basename, md_pathname, rtl_reg_block_class_list)
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", md_pathname, doneString()))
    except Exception as e:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", md_pathname, failString()))
        message("ERROR: " + str(e))

    # Register block to .h file
    try:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", h_pathname, "[START]"))
        h_file_generate(basename, h_pathname, rtl_reg_block_class_list)
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", h_pathname, doneString()))
    except Exception as e:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", h_pathname, failString()))
        message("ERROR: " + str(e))

    # Register for test memory file
    try:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", mem_pathname, "[START]"))
        #h_mem_file_generate(basename, mem_pathname, rtl_reg_block_class_list)
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", mem_pathname, doneString()))
    except Exception as e:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", mem_pathname, failString()))
        message("ERROR: " + str(e))

    # Register for test memory file
    try:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", tcl_pathname, "[START]"))
        #tcl_file_generate(basename, tcl_pathname, rtl_reg_block_class_list)
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", tcl_pathname, doneString()))
    except Exception as e:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", tcl_pathname, failString()))
        message("ERROR: " + str(e))

    # Register block to .ref file
    try:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", ref_pathname, "[START]"))
        ref_file_generate(basename, ref_pathname, rtl_reg_block_class_list)
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", ref_pathname, doneString()))
    except Exception as e:
        message('{0:s} {1:s} {2:<80s}'.format("\tGenerator", ref_pathname, failString()))
        message("ERROR: " + str(e))

    py_pathname = get_pathname_py(basename)
    generator = RegisterClassToPythonClassesGenerator(rtl_reg_block_class_list, py_pathname)
    generator.generate()

def printUsage(appName):
    print ('usage: ' + appName + ' --directory=<input_dir> --md_dir=<md_dir> --h_dir=<h_dir> --py_dir=<py_dir>')
    print ('--directory=<root directory>')
    print ('--md_dir=<Mardown directory>')
    print ('--h_dir=<Header directory>')
    print ('--py_dir=<Python directory>')

def createDir(directory):
    try:
        os.makedirs(directory)
    except Exception:
        pass

def main(argv):
    global MD_FOLDER
    global FOLDER_INPUT
    global H_FOLDER
    global PY_FOLDER
    
    # Have user inputs
    try:
        opts, _ = getopt.getopt(argv[1:],"h",["help", "input_dir=", "md_dir=", "h_dir=", "py_dir="])
    except:
        printUsage(argv[0])
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printUsage(argv[0])
            sys.exit()
        elif opt in ("--input_dir"):
            FOLDER_INPUT = arg
        elif opt in ("--md_dir"):
            MD_FOLDER = arg
        elif opt in ("--h_dir"):
            H_FOLDER = arg
        elif opt in ("--py_dir"):
            PY_FOLDER = arg
            
    # Create directory if neccessary
    createDir(MD_FOLDER)
    createDir(H_FOLDER)
    createDir(PY_FOLDER)
            
    message("RTL Register Generator Tool: version 1.0.0")
    generatedFiles = []
    for root, _, files in os.walk(FOLDER_INPUT):
        for fileName in files:
            if fnmatch.fnmatch(fileName, RTL_REG_EXT):
                # Call function to process this fileName
                filePath = os.path.join(root, fileName)
                message(Color.BLUE + "Processing File: " + filePath + Color.END)
                try:
                    rtl_reg_file_process(filePath)
                    generatedFiles.append(fileName)
                except Exception as err:
                    message("Processing File: [" + Color.RED + "FAIL" + Color.END + "]")
                    message("ERROR: " + Color.RED + str(err) + Color.END)
        
        # Do not walk recursively, if sub folders contain same files as parent 
        # folder, then we will have unexpected output. This script needs to be 
        # improved to support recursive      
        break
    
    RegisterClassToPythonClassesGenerator.generateWraper(PY_FOLDER, generatedFiles)            
        
if __name__ == "__main__":
   main(sys.argv)

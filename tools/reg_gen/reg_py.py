import os.path
from python.jenkin.AtOs import AtOs
from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.AtRegister import cBit31_0

def _makeValidString(string):
    return string.replace("\"", "\\\"")

def _isUnused(string):
    return string in ["unused", "un-used"]

def _makeValidIdentifier(string):
    string = string.replace("(", "_")
    string = string.replace(")", "_")
    return string

class RegisterClassToPythonClassesGenerator(object):
    def __init__(self, rtl_reg_block_class_list, pyFilePath):
        self.rtl_reg_block_class_list = rtl_reg_block_class_list
        filename = "_" + _makeValidIdentifier(os.path.basename(pyFilePath))
        dirname = os.path.dirname(pyFilePath)
        self.pyFilePath = os.path.join(dirname, filename)
    
    def _generateFieldDicts(self, regClass, pyFile):
        pyFile.write("\n")
        pyFile.write("        def _allFieldDicts(self):\n")
        pyFile.write("            allFields = {}\n")
        for regField in regClass.get_field_list():
            if _isUnused(regField.get_field_name().lower()):
                continue
            pyFile.write("            allFields[\"%s\"] = %s._%s._%s()\n" % (regField.get_field_name(),
                                                                              self._className(),
                                                                              regClass.get_reg_name_short(),
                                                                              regField.get_field_name()))
        pyFile.write("            return allFields\n")
            
    def _generateRegClass(self, regClass, pyFile):
        try:
            startAddress = int(regClass.get_reg_add_start().replace("_", ""), 16)
        except:
            startAddress = cBit31_0
        
        try:
            stopAddress = int(regClass.get_reg_add_end().replace("_", ""), 16)
        except:
            stopAddress = cBit31_0
        
        classString = \
"""
    class _%s(AtRegister.AtRegister):
        def name(self):
            return "%s"
    
        def description(self):
            return "%s"
            
        def width(self):
            return %s
        
        def type(self):
            return "%s"
            
        def fomular(self):
            return "%s"
            
        def startAddress(self):
            return 0x%08x
            
        def endAddress(self):
            return 0x%08x
""" % (regClass.get_reg_name_short(),
       regClass.get_reg_name_long(), 
       regClass.get_reg_des().replace("\"", "\\\""), 
       regClass.get_reg_width(),
       regClass.get_reg_type(),
       regClass.get_reg_add_formula(),
       startAddress,
       stopAddress)

        pyFile.write(classString)
        
    def _generateRegFieldClasses(self, regClass, pyFile):
        for regField in regClass.get_field_list():
            if _isUnused(regField.get_field_name().lower()):
                continue
            
            bits = regField.get_field_bit().split(":")
            stopBit = int(bits[0])
            startBit = stopBit
            try:
                startBit = int(bits[1])
            except:
                pass
            
            resetValue = 0
            try:
                resetValue = int(regField.get_field_val_reset())
            except:
                resetValue = cBit31_0
            
            classString = \
"""
        class _%s(AtRegister.AtRegisterField):
            def stopBit(self):
                return %d
                
            def startBit(self):
                return %d
        
            def name(self):
                return "%s"
            
            def description(self):
                return "%s"
            
            def type(self):
                return "%s"
            
            def resetValue(self):
                return 0x%x
""" %   (regField.get_field_name(),
         stopBit,
         startBit,
         regField.get_field_name(),
         _makeValidString(regField.get_field_description()),
         regField.get_field_type(),
         resetValue)
            pyFile.write(classString)
    
    def _generatePyClass(self, regClass, pyFile):
        self._generateRegClass(regClass, pyFile)
        self._generateRegFieldClasses(regClass, pyFile)
        self._generateFieldDicts(regClass, pyFile)
        
    def _className(self):
        fileName = os.path.basename(self.pyFilePath)
        return os.path.splitext(fileName)[0]

    def _generateTopClass(self, pyFile):
        className = self._className()
        pyFile.write("class %s(AtRegister.AtRegisterProvider):\n" % className)
        pyFile.write("    @classmethod\n")
        pyFile.write("    def _allRegisters(cls):\n")
        pyFile.write("        allRegisters = {}\n")
        for regClass in self.rtl_reg_block_class_list:
            pyFile.write("        allRegisters[\"%s\"] = %s._%s()\n" % (regClass.get_reg_name_short(),
                                                                        className, 
                                                                        regClass.get_reg_name_short()))
        pyFile.write("        return allRegisters\n")
    
    def _testFile(self, pyFilePath):
        dirName = os.path.dirname(pyFilePath)
        fileName = os.path.basename(pyFilePath)
        className = os.path.splitext(fileName)[0]
        
        tempFilePath = os.path.join(dirName, "%s_test.py" % className)
        tempFile = open(tempFilePath, "w")
        
        code = \
"""
if __name__ == '__main__':
    from python.arrive.AtAppManager.AtColor import AtColor
    from %s import %s
    
    try:
        %s().test()
        AtColor.printColor(AtColor.GREEN, "Generate %s: DONE")
    except Exception as e:
        AtColor.printColor(AtColor.RED, "Generate %s: FAIL")
        print e
""" % (className, className, className, self.pyFilePath, self.pyFilePath)

        tempFile.write(code)
        tempFile.close()
        
        AtOs.run("python %s" % tempFilePath)
        os.remove(tempFilePath)
        
    def generate(self):
        if self.pyFilePath is None:
            return
        
        pyFile = open(self.pyFilePath, "w")
        pyFile.write("import python.arrive.atsdk.AtRegister as AtRegister\n\n")
        self._generateTopClass(pyFile)
        for regClass in self.rtl_reg_block_class_list:
            self._generatePyClass(regClass, pyFile)
        pyFile.close()
        self._testFile(self.pyFilePath)
        
    @classmethod
    def generateWraper(cls, folder, files):
        if folder is None:
            return
        
        className = "RegisterProviderFactory"
        wraperFilePath = os.path.join(folder, "%s.py" % className)
        wraperFile = open(wraperFilePath, "w")
        
        def moduleNameFromFilePath(filePath):
            fileName = os.path.basename(filePath)
            moduleName = "_" + os.path.splitext(fileName)[0]
            return _makeValidIdentifier(moduleName)
        
        wraperFile.write("import python.arrive.atsdk.AtRegister as AtRegister\n\n")
        wraperFile.write("class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):\n")
        wraperFile.write("    def _allRegisterProviders(self):\n")
        wraperFile.write("        allProviders = {}\n\n")
        for filePath in files:
            moduleName = moduleNameFromFilePath(filePath)
            wraperFile.write("        from %s import %s\n" % (moduleName, moduleName))
            wraperFile.write("        allProviders[\"%s\"] = %s()\n\n" % (moduleName, moduleName))
        
        wraperFile.write("\n")
        wraperFile.write("        return allProviders\n")    
        wraperFile.close()
        
        def _test():
            moduleName = os.path.join(folder, "%s_test.py" % className)
            tempFilePath = os.path.join(folder, "%s" % moduleName)
            tempFile = open(tempFilePath, "w")
            
            code = \
"""
if __name__ == '__main__':
    from RegisterProviderFactory import RegisterProviderFactory
    RegisterProviderFactory().test()
"""
    
            tempFile.write(code)
            tempFile.close()
            try:
                AtOs.run("python %s" % tempFilePath)
                AtColor.printColor(AtColor.GREEN, "Generate %s: DONE" % wraperFilePath)
                os.remove(tempFilePath)
            except Exception as e:
                AtColor.printColor(AtColor.RED, "Generate %s: FAIL" % wraperFilePath)
                print e
            
        _test()
        cls.cleanup(folder)
        
    @classmethod
    def cleanup(cls, folder):
        AtOs.run("rm %s/*.pyc" % folder)
#!/usr/bin/python

import os
import re
import sys
import glob
import getopt
import ntpath

def IsObjectInitFunctionDeclaration(content, lineNumber):
	if re.search(";", content[lineNumber]) != None:
		return False
	while lineNumber >= 0:
		if re.search("}", content[lineNumber]) != None:
			return True
		if re.search("{", content[lineNumber]) != None:
			return False
		lineNumber = lineNumber - 1
	return False

def FindObjectInitFunction(content):
	lineNumber = 0
	for line in content:
		match = re.search("ObjectInit\(.*", line)
		if match == None:
			lineNumber = lineNumber + 1
		else:
			# Make sure we found correct object init function
			if IsObjectInitFunctionDeclaration(content, lineNumber):
				break
			else:
				lineNumber = lineNumber + 1
	if match == None:
		return 0
	return lineNumber
	
def FindBeginKey(content):
	lineNumber = 0
	objectInitFunctionLineNumber = FindObjectInitFunction(content)
	for line in content:
		match1 = re.search("^\s*Override.*\);", line)
		match2 = re.search("^\s*MethodsInit\(.*\);", line)
		if match1 != None:
			if lineNumber > objectInitFunctionLineNumber:
				break
		if match2 != None:
			break
		lineNumber = lineNumber + 1
		
	if match1 == None and match2 == None:
		return 0
		
	return lineNumber

def FindEndKey(content):
	lineNumber = 0
	overrideLineNumber = 0
	methodInitLineNumber = 0
	for line in content:
		# Some classes do not define correct name m_MethodsInit
		match = re.search("m_.*ethodsInit = 1;", line)
		match1 = re.search("^\s*Override.*\);", line)
		match2 = re.search("^\s*MethodsInit\(.*\);", line)
		if match == None:
			lineNumber = lineNumber + 1
		else:
			break
		if match1 != None:
			overrideLineNumber = lineNumber
		if match2 != None:
			methodInitLineNumber = lineNumber
			
	# File lack of this line code, need to add this line code	
	if match == None:
		if methodInitLineNumber > overrideLineNumber:
			return methodInitLineNumber
		if methodInitLineNumber < overrideLineNumber:
			return overrideLineNumber
		
		print ("\r\nFile error, can not process")
		return 0

	return lineNumber
	
def file_content_scan(content):
	# This function return line number of begin key
	beginKeyLineNumber = FindBeginKey(content)
	if beginKeyLineNumber == 0:
		return True
	match1 = re.search("AtDriverObjectModelLock", content[beginKeyLineNumber - 1])
	
	# This function return line number of last key
	endKeyLineNumber = FindEndKey(content)
	match2 = re.search("AtDriverObjectModelUnLock", content[endKeyLineNumber + 1])
	
	if match1 == None or match2 == None:
		return False
	
	if beginKeyLineNumber >= endKeyLineNumber:
		return False
	
	# Detect if any abnormal code and need to solve manually
	while beginKeyLineNumber < endKeyLineNumber :
		match = re.search("return.*;", content[beginKeyLineNumber])
		if match != None:
			return False
		beginKeyLineNumber = beginKeyLineNumber + 1
	
	return file_content_scan(content[(endKeyLineNumber + 1):])
		
# process on file 
def file_scan(fileName):
	ifile = open(fileName,'r')
	content = ifile.readlines()
	ifile.close()

	if file_content_scan(content) == False:
		print fileName
		
# Check if it is register file
arrive_prefix = ["at", "tha", "af6"]

def isSourceCodeFile(fname):
	head, tail = ntpath.split(fname)
	for prefix in arrive_prefix[:]:
		 if tail.lower().startswith(prefix) & tail.lower().endswith(".c"):
			 return True
	return False
	
def dir_scan(path):
	ret = True
	for currentFile in glob.glob( os.path.join(path, '*') ):
		# Ignore AtDriver.c
		if re.search("AtDriver.c", currentFile):
			continue
			
		if os.path.isdir(currentFile):
			dir_scan(currentFile)
			
		if isSourceCodeFile(currentFile):
			if file_scan(currentFile) == False:
				ret = False
	return ret
				
def printUsage():
	print ('usage: object_model_protect_scan.py [file | -d <directory>]')
	print ('	   -d <directory>: scan all file .c in folder')
	
def main(argv):
	global m_Debug
	global filterString
	
	inputDirectory = None
	m_Verbose = False
	
	try:
	   opts, args = getopt.getopt(argv,"hd:f:v",["help"])
	except getopt.GetoptError:
	   printUsage()
	   sys.exit(2)
	   
	for opt, arg in opts:
		if opt == '-h':
			printUsage()
			sys.exit()
		elif opt == '-v':
			m_Debug = True
		elif opt == '-d':
			inputDirectory = arg
	
	if '-v' in argv: 
	   m_Verbose = True
	   
	if inputDirectory:
		return dir_scan(inputDirectory)
	else:
		return file_scan(argv[0])   
   
if __name__ == "__main__":
   main(sys.argv[1:])
   
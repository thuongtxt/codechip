#!/usr/bin/python

import os
import re
import sys
import glob
import getopt
import ntpath

def IsObjectInitFunctionDeclaration(content, lineNumber):
	if re.search(";", content[lineNumber]) != None:
		return False
	while lineNumber >= 0:
		if re.search("}", content[lineNumber]) != None:
			return True
		if re.search("{", content[lineNumber]) != None:
			return False
		lineNumber = lineNumber - 1
	return False

def FindObjectInitFunction(content):
	lineNumber = 0
	for line in content:
		match = re.search("ObjectInit\(.*", line)
		if match == None:
			lineNumber = lineNumber + 1
		else:
			# Make sure we found correct object init function
			if IsObjectInitFunctionDeclaration(content, lineNumber):
				break
			else:
				lineNumber = lineNumber + 1
	if match == None:
		return 0
	return lineNumber
	
def FindBeginKey(content):
	lineNumber = 0
	objectInitFunctionLineNumber = FindObjectInitFunction(content)
	for line in content:
		match1 = re.search("^\s*Override.*\);", line)
		match2 = re.search("^\s*MethodsInit\(.*\);", line)
		if match1 != None:
			if lineNumber > objectInitFunctionLineNumber:
				break
		if match2 != None:
			break
		lineNumber = lineNumber + 1
		
	if match1 == None and match2 == None:
		return 0
		
	return lineNumber

def CodeAlreadySet_m_methodsInit(content):
	flag = 0
	for line in content:
		match = re.search("m_.*ethodsInit = 1;", line)
		if match != None:
			return 1
	return 0

def FindEndKey(content):
	lineNumber = 0
	overrideLineNumber = 0
	methodInitLineNumber = 0
	for line in content:
		# Some classes do not define correct name m_MethodsInit
		match = re.search("m_.*ethodsInit = 1;", line)
		match1 = re.search("^\s*Override.*\);", line)
		match2 = re.search("^\s*MethodsInit\(.*\);", line)
		if match == None:
			lineNumber = lineNumber + 1
		else:
			break
		if match1 != None:
			overrideLineNumber = lineNumber
		if match2 != None:
			methodInitLineNumber = lineNumber
			
	# File lack of this line code, need to add this line code	
	if match == None:
		if methodInitLineNumber > overrideLineNumber:
			return methodInitLineNumber
		if methodInitLineNumber < overrideLineNumber:
			return overrideLineNumber
		
		print ("\r\nFile error, can not process")
		return 0
	
	return lineNumber
	
def AlreadyHasObjectModelProtection(content):
	for line in content:
		if re.search("AtDriverObjectModelLock", line):
			return True
	return False   

# process on file 
def file_modify(fileName):
	ifile = open(fileName,'r')
	content = ifile.readlines()
	ifile.close()
	
	if AlreadyHasObjectModelProtection(content) == True:
		return
	
	# This function return line number of begin key
	lineNumber = FindBeginKey(content)  
	if lineNumber == 0:
		return
	
	content.insert(lineNumber, "	AtDriverObjectModelLock();\r\n")
	
	# This function return line number of last key
	lineNumber = FindEndKey(content)
	if CodeAlreadySet_m_methodsInit(content) == 1:
		content.insert(lineNumber + 1, "	AtDriverObjectModelUnLock();\r\n")
	else:
		content.insert(lineNumber, "	m_methodsInit = 1;\r\n")
		content.insert(lineNumber + 1, "	AtDriverObjectModelUnLock();\r\n")
	
	# Write back to file
	modifiedFile = open(fileName, "w")
	content = "".join(content)
	modifiedFile.write(content)
	modifiedFile.close()
	
# Check if it is register file
arrive_prefix = ["at", "tha", "af6"]

def isSourceCodeFile(fname):
	head, tail = ntpath.split(fname)
	for prefix in arrive_prefix[:]:
		 if tail.lower().startswith(prefix) & tail.lower().endswith(".c"):
			 return True
	return False
	
def dir_modify(path):
	for currentFile in glob.glob( os.path.join(path, '*') ):
		if os.path.isdir(currentFile):
			dir_modify(currentFile)
			
		if isSourceCodeFile(currentFile):
			print ("\r\nProcessing file: " + currentFile)
			file_modify(currentFile)  
				
def printUsage():
	print ('usage: object_model_protect.py [file | -d <directory>]')
	print ('	   -d <directory>: modify all file .c in folder')
	
def main(argv):
	global m_Debug
	global filterString
	
	inputDirectory = None
	m_Verbose = False
	
	try:
	   opts, args = getopt.getopt(argv,"hd:f:v",["help"])
	except getopt.GetoptError:
	   printUsage()
	   sys.exit(2)
	   
	for opt, arg in opts:
		if opt == '-h':
			printUsage()
			sys.exit()
		elif opt == '-v':
			m_Debug = True
		elif opt == '-d':
			inputDirectory = arg
	
	if '-v' in argv: 
	   m_Verbose = True
	   
	if inputDirectory:
		dir_modify(inputDirectory)
	else:
		file_modify(argv[0])   
   
if __name__ == "__main__":
   main(sys.argv[1:])
   
#!/usr/bin/python

import sys
import getopt
import re
import Queue

# parser part
ADDRESS = 1
FORMULA = 2
FORMULA_DESCR = 3
REGISTER_DESCR = 4
FIELD_LAYOUT = 5
FIELD_DESCR = 6

BITS = 0
NAME = 1
TYPE = 2
RESET = 3

verbose = False
heading = 3
col_delim = '%%'

'''
Class of description of single register.
'''
class regdes():
    #in
    name = ''
    context = ''
    
    #out
    address = ''
    base_address = ''
    formula = Queue.Queue(10)
    var_name = Queue.Queue(6)
    var_range = Queue.Queue(6)
    var_descr = Queue.Queue(6)
    reg_descr = ''
    field_bits = Queue.Queue(32)
    field_name = Queue.Queue(32)
    field_descr = Queue.Queue(32)
    field_type = Queue.Queue(32)
    field_rst = Queue.Queue(32)
    output = ''
    
    def __init__(self, regname, context):
        self.name = regname
        self.context = context
        return
    
    def parser(self):
        global verbose
        qbits = Queue.Queue(32)
        qname = Queue.Queue(32)
        qtype = Queue.Queue(32)
        qrst = Queue.Queue(32)

        formula = ''
        i = 0
        part = 0
        field = BITS
        descr = ''
        row = ''
        lines = re.split('[\r\n]', self.context)
        for i in range(len(lines)):
            if re.match('^[\s]*$', lines[i]) or re.match('^[=]+', lines[i]):
                continue
            
            if re.match("'''Address:", lines[i]):
                part = ADDRESS
            elif 'Where:' in lines[i]:
                part = FORMULA_DESCR
            elif re.match("'''Description:", lines[i]):
                part = REGISTER_DESCR
            elif '| style="border-top' in lines[i] or '| style="border:' in lines[i]:
                part = FIELD_LAYOUT
            elif re.match("^'''<nowiki>", lines[i]) or re.match("^<nowiki>", lines[i]):
                part = FIELD_DESCR
            elif part == ADDRESS:
                part = FORMULA
            #else -> nochange part.

            #Filter all meaningless characters.
            lines[i] = lines[i].replace('#', '')

            if part == ADDRESS:
                addrs = re.findall('[(]*0x[A-Fa-f0-9]+[)]*', lines[i])
                self.base_address = addrs[0]
                self.address = drop_left0(addrs[0])
                if len(addrs) > 1:
                    if re.match('[(]', addrs[1]) == None: 
                        self.address = ''.join((drop_left0(addrs[0]), '-', drop_left0(addrs[1])))
                
                if verbose: 
                    print '****************************************'
                    print 0, self.name.lstrip()
                    print 1, self.address
            
            elif part == FORMULA:
                if '*' in lines[i] or '+' in lines[i]:
                    print lines[i]
                    if re.match('^The', lines[i]):
                        fx = re.findall(''.join((self.base_address, '[A-Za-z0-9\+\*\s]+')), lines[i])[-1]
                        self.formula._put(fx.replace(self.base_address, 'Address '))
                        if verbose: print 2, fx
                    else:
                        self.formula._put(lines[i])
                    
            elif part == FORMULA_DESCR:
                var = re.findall('[A-Za-z]+[:]*[\s]+\(', lines[i].replace("Where", "").replace(':',''))[0]
                if verbose: print 3, var
                self.var_name._put(re.split('[\(\s:]', var)[0])
                self.var_range._put(''.join((re.findall('\([0-9]+', lines[i])[-1], '-', \
                                             re.findall('[0-9]+\)', lines[i])[-1])))
                self.var_descr._put(re.split('[0-9]\)[.]*', lines[i])[-1].lstrip())
                    
            elif part == REGISTER_DESCR:
                lines[i] = lines[i].replace('<nowiki>', '').replace('</nowiki>', '')
                if "Description:" in lines[i]:
                    self.reg_descr = re.split("[\']+", lines[i])[-1].lstrip()
                    if verbose: print 4, self.reg_descr
                
            elif part == FIELD_LAYOUT:
                if '| style="border-top' in lines[i] or '| style="border:' in lines[i]:
                    qval = re.split('[|\s]+', lines[i])[-1]
                    
                    if field == BITS:
                        qbits._put(qval)
                    elif field == NAME:
                        if 'nowiki' not in qval:
                            qname._put(qval)
                        else:
                            qval = qval.replace('<nowiki>', '').replace('</nowiki>', '')
                            qname._put(re.split('[\[\]]', qval)[0])
                    elif field == TYPE:
                        qtype._put(qval.replace('/', '').replace('_', ''))
                    elif field == RESET:
                        qrst._put(qval)

                    if verbose: row = ''.join((row, '|', qval))

                    field += 1
                    if field > RESET:
                        field = BITS
                        if verbose: 
                            print 5, row; row = ''
                            
            elif part == FIELD_DESCR:
                if re.match("^'''<nowiki>", lines[i]) or re.match("^<nowiki>", lines[i]):
                    qval = lines[i].replace('<nowiki>', '').replace('</nowiki>', '')

                    while 1:
                        bits = qbits._get()
                        name = qname._get()
                        type = qtype._get()
                        reset = qrst._get()
                        if ''.join(('[', bits, ']')) in qval: print qval; break
                        else: print 6, '%s[%s] @DROPPED@' %(name, bits)

                    self.field_bits._put(bits)
                    self.field_name._put(name)
                    self.field_type._put(type)
                    self.field_rst._put(reset)
                    
                    if len(descr) > 0:
                        if verbose: print 6, descr
                        self.field_descr._put(descr)
                        descr = ''

                    descr = re.split("[\]a-z]:", qval.replace("'", ""))[-1].lstrip()
                    if len(descr) == 0: descr = '\t'
                else:
                    qval = lines[i].replace('<nowiki>', '').replace('</nowiki>', '').replace("'", '')
                    descr = ''.join((descr, '\n//\t', qval))
                    
        if len(descr) > 0:
            if verbose: print 6, descr
            self.field_descr._put(descr)
            
        assert self.field_bits.qsize() == self.field_name.qsize()
        assert self.field_bits.qsize() == self.field_type.qsize()
        assert self.field_bits.qsize() == self.field_rst.qsize()
        assert self.field_bits.qsize() == self.field_descr.qsize()
    
    def report(self):
        out = ''
        out = ''.join((out, '\n# ******************'))
        out = ''.join((out, '\n#', self.name))
        out = ''.join((out, '\n# ******************'))
        out = ''.join((out, '\n// Begin:'))
        out = ''.join((out, '\n// Register Full Name: ', self.name.lstrip()))
        out = ''.join((out, '\n// RTL Instant Name  : ', self.name.lstrip().rstrip().lower().replace(' ', '_')))
        out = ''.join((out, '\n//# {FunctionName,SubFunction_InstantName_Description}'))
        out = ''.join((out, '\n//# RTL Instant Name and Full Name MUST be unique within a register description file'))
        out = ''.join((out, '\n// Address: ', self.address))
        if self.formula.qsize() > 0:
            out = ''.join((out, 
                            '\n// Formula: ', self.formula._get()))
        else:
            out = ''.join((out, 
                            '\n// Formula: @missing@'))
        for i in range(self.formula.qsize()):
            out = ''.join((out, 
                            '\n//# ', self.formula._get()))
            
        if self.var_name.qsize() > 0:
            out = ''.join((out, \
                            '\n// Where: {$', self.var_name._get(), self.var_range._get(), ': ', self.var_descr._get(), '}'))
            for i in range(self.var_name.qsize()): 
                out = ''.join((out, ' %% {$', self.var_name._get(), self.var_range._get(), ': ', self.var_descr._get(), '}'))
        out = ''.join((out, '\n// Description: ', self.reg_descr))
        out = ''.join((out, '\n// Width: @missing@'))
        out = ''.join((out, '\n// Register Type: @missing@'))
        out = ''.join((out, '\n//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset'))
        for i in range(self.field_bits.qsize()): 
            out = ''.join((out, \
                            '\n// Field: [%s] %s  %s  %s  %s  %s %s  %s 0x0 %s %s' \
                                %(self.field_bits._get(), col_delim, self.field_name._get(), col_delim, self.field_descr._get(), \
                                  col_delim, self.field_type._get(), col_delim, col_delim, self.field_rst._get())))
        out = ''.join((out, '\n// End:'))
        self.output = out
        
        return self.output

def drop_left0(hexstr):
    return "0x%X" %(int(hexstr, 16))

def split_regdes(txtFile):
    file = open(txtFile, 'r')
    regdes_start = False
    regdes_new = False
    regname = ''
    regbody = ''
    regque = Queue.Queue(50)
    global heading

    for line in file:
        if re.match("^[=]{%s}[\sA-Za-z0-9/]+[=]{%s}" %(heading, heading), line):
            if regdes_new:
                regque._put(regdes(regname, regbody))
                regbody = ''
            regname = line.replace('=', '')
            regname = regname.replace('\n', '')
            regdes_new = True
            
        elif regdes_new:
            regbody = ''.join((regbody, line))
    if regdes_new:
        regque._put(regdes(regname, regbody))
    
    file.close()
    file = open(txtFile.replace('txt', 'atreg'), 'w')

    # Calculate and export
    for i in range(regque.qsize()):
        reg = regque._get()
        reg.parser()
        
        file.write(reg.report())
    
    # clear resource and exit.
    file.close()
    del regque

def usage():
    print ('usage: txt2atreg.py [-f inputFile] [-v] [-l heading-level]')
    print ('       -f    input rd.txt file (Output file will have same basename with extension .atreg)')
    print ('       -l    headings-level for register name search, from 2 to 5.')
    print ('       -v    verbose')
    
def main(argv):
    textFile = ''
    global verbose
    global heading
    
    try:
       opts, args = getopt.getopt(argv,"hf:vl:",["help"])
    except getopt.GetoptError:
       usage()
       sys.exit(2)
       
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt == '-f':
            textFile = arg
        elif opt == '-v':
            verbose = True
        elif opt == '-l':
            heading = arg
   
    split_regdes(textFile)
   
   
if __name__ == "__main__":
   main(sys.argv[1:])

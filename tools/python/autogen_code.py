#!/usr/bin/python

#import pydevd
#pydevd.settrace('172.33.47.2')

import os
import sys
import datetime
import re

# python tools/python/autogen_code.py

# Changable
m_ColorEnable = False

# Don't change
yesOptionStr   = 'y'
noOptionStr    = 'n'
exitProgramStr = 'exit'
cSourceFileType = '.c'
cHeaderFileType = '.h'
atsdkHome = '.'
lookupSourceList = ["/driver/src", "/platform", "/customers/src"]

class AtColor:
    RED    = "\033[1;31m"
    PINK   = "\033[1;35m"
    YELLOW = "\033[1;33m"
    CYAN   = "\033[1;36m"
    GREEN  = "\033[1;32m"
    CLEAR  = "\033[0m"
    
class AtClass(object):
    def __init__(self, className, classDir, superClass, returnClassType, hasHeaderFile):
        self.className  = className
        self.classDir   = classDir
        self.superClass = superClass
        self.hasHeaderFile = hasHeaderFile
        self.returnClassType = returnClassType
        self.paramsDesInInitFunc = None
        self.paramsDesInNewFunc  = None
        self.headerFileName = className + "Internal.h"
        self.overrideList = []
        
    def header_file_name_set(self, headerFileName):
        if not headerFileName:
            return
        self.headerFileName = headerFileName
        
    def header_file_name_get(self):
        return self.headerFileName
        
    def override_list_in_string_set(self, overrideListStr):
        if not overrideListStr:
            return False
        overrideList = overrideListStr.split(',')
        print overrideList
        for obj in overrideList:
            self.overrideList.append(clean_string(obj))
        return True
        
    def override_list_get(self):
        return self.overrideList
        
    def params_desc_in_init_func_set(self, paramsDesc):
        self.paramsDesInInitFunc = paramsDesc
        
    def params_desc_in_new_func_set(self, paramsDesc):
        self.paramsDesInNewFunc = paramsDesc
    
    def module_name_get(self):
        tokenList = os.path.split(self.classDir)
        return str(tokenList[len(tokenList) - 1])
    
    def can_gen_header_file(self):
        if self.hasHeaderFile != True:
            return False
        
        if not self.paramsDesInInitFunc or not self.paramsDesInNewFunc:
            return False
        
        return True
    
    def super_new_function_name_get(self):
        if not self:
            return None
        return "%sNew" % (self.superClass.className)
        
    def new_function_name_get(self):
        if not self:
            return None
        return "%sNew" % (self.className)
    
def month_name (number):
    if number == 1:
        return "Jan"
    elif number == 2:
        return "Feb"
    elif number == 3:
        return "Mar"
    elif number == 4:
        return "Apr"
    elif number == 5:
        return "May"
    elif number == 6:
        return "Jun"
    elif number == 7:
        return "Jul"
    elif number == 8:
        return "Aug"
    elif number == 9:
        return "Sep"
    elif number == 10:
        return "Oct"
    elif number == 11:
        return "Nov"
    elif number == 12:
        return "Dec"
    return "Unknown"
    
def clean_string(str):
    str = str.strip('\r')
    str = str.strip('\n')
    str = str.strip(' ')
    return str

def file_description(thisClass, fileType):
    now = datetime.datetime.now()
    thisClassName = thisClass.className
    
    if fileType == cSourceFileType:
        desc = thisClassName + " implementations"
        fileName = thisClassName + cSourceFileType
    else:
        desc = thisClassName + " declarations"
        fileName = thisClassName + "Internal" + cHeaderFileType
        
    return """/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) %u Arrive Technologies Inc..c'
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : %s
 *
 * File        : %s
 *
 * Created Date: %s %u, %u
 *
 * Description : %s
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

"""  % (now.year, thisClass.module_name_get().upper(), fileName, month_name(now.month), now.day, now.year, desc)
    
def include_desc(thisClass, fileType):  
    thisClassName = thisClass.className
    workingDir = thisClass.classDir
    hasHeaderFile = thisClass.hasHeaderFile
    superClassName = thisClass.superClass.header_file_name_get()
    superClassDir = thisClass.superClass.classDir
    
    buffer = "/*--------------------------- Include files ----------------------------------*/\r\n"
    if (hasHeaderFile == True and fileType == cSourceFileType):  
        buffer = buffer + "#include \"" + thisClassName + "Internal.h\"\r\n"
    elif hasHeaderFile == True and fileType == cHeaderFileType:
        buffer = buffer + "#include \"" + relative_path_form_path(workingDir, superClassDir) + superClassName + "\"\r\n"
    else:
        buffer = buffer + "#include \"" + relative_path_form_path(workingDir, superClassDir) + superClassName + "\"\r\n"
        
    return buffer + "\r\n"

def c_plus_plus_ifdef():
    return """#ifdef __cplusplus
extern "C" {
#endif
"""

def c_plus_plus_endif():
    return """
#ifdef __cplusplus
}
#endif
"""

def define_macro_desc(fileType):
    buffer = ' '
    if fileType == cHeaderFileType:
        buffer = c_plus_plus_ifdef()
        
    return """/*--------------------------- Define -----------------------------------------*/
%s
/*--------------------------- Macros -----------------------------------------*/

""" % (buffer)

def local_typedefs_desc(thisClass):
    buffer = "/*--------------------------- Local Typedefs ---------------------------------*/\r\n"
    if thisClass.hasHeaderFile == True:
        return buffer
    
    buffer = buffer + """typedef struct t%s 
    {
    t%s super;
    
    /* Private variables */
    }t%s;
""" % (thisClass.className, thisClass.superClass.className, thisClass.className)

    return buffer

def header_typedefs_desc(thisClass):
    thisClassName = thisClass.className
    superClassName = thisClass.superClass.className
    
    buffer = "/*--------------------------- Typedefs ---------------------------------------*/\r\n"
    buffer = buffer + "typedef struct t%s * %s;\r\n\r\n" % (thisClassName, thisClassName)
    buffer = buffer + """typedef struct t%sMethods 
    {
    uint8 dummy;
    }t%sMethods;
    
""" % (thisClassName, thisClassName)

    buffer = buffer + """typedef struct t%s 
    {
    t%s super;
    const t%sMethods *methods;
    
    /* Private variables */
    }t%s;
    
""" % (thisClassName, superClassName, thisClassName, thisClassName)

    return buffer

def global_variables_desc():
    return "\r\n/*--------------------------- Global variables -------------------------------*/\r\n\r\n"

def local_variables_desc(thisClass):
    buffer = "/*--------------------------- Local variables --------------------------------*/\r\n"
    buffer = buffer + "static uint8 m_methodsInit = 0;\r\n\r\n"
    buffer = buffer + "/* Override */\r\n"
    
    overrideList = thisClass.override_list_get()
    if overrideList:
        for obj in overrideList:
            buffer += "static t%sMethods m_%sOverride;\r\n" % (obj, obj)
    
    buffer += "\r\n\r\n"
    return buffer

def forward_declerations_desc():
    return """/*--------------------------- Forward declarations ---------------------------*/

"""

def implementation_desc():
    return """/*--------------------------- Implementation ---------------------------------*/

"""

def entries_desc():
    return """/*--------------------------- Entries ----------------------------------------*/
"""

def object_size(thisClass):
    return """static uint32 ObjectSize(void)
    {
    return sizeof(t%s);
    }
    
""" % (thisClass.className)

def override_function_on_class(thisClass, overrideClassName):
    return """static void Override%s(%s self)
    {
    %s overrideObject = (%s)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_%sOverride, mMethodsGet(overrideObject), sizeof(m_%sOverride));

        /*mMethodOverride(m_%sOverride, );*/
        }

    mMethodsSet(overrideObject, &m_%sOverride);
    }
    
""" % (overrideClassName, thisClass.returnClassType, overrideClassName, overrideClassName, overrideClassName, overrideClassName, overrideClassName, overrideClassName)

def override_functions(thisClass):
    buffer = ''
    
    overrideList = thisClass.override_list_get()
    if overrideList:
        for obj in overrideList:
            buffer += override_function_on_class(thisClass, obj)
            
    buffer += "static void Override(%s self)\r\n" % (thisClass.returnClassType);
    buffer += "    {\r\n"
    if overrideList:
        for obj in overrideList:
            buffer += "    Override%s(self);\r\n" % (obj)
    buffer += "    }\r\n\r\n"
    return buffer

def object_init_function_name(thisClass):
    functionName = "ObjectInit"
    if thisClass.hasHeaderFile == True:
        functionName = thisClass.className + functionName
    return functionName

def object_init_function_prototype(thisClass):
    returnClassType = thisClass.returnClassType
    functionPrototype = "%s %s%s" % (returnClassType, object_init_function_name(thisClass), thisClass.paramsDesInInitFunc)
    if thisClass.hasHeaderFile != True:
        functionPrototype = "static " + functionPrototype
        
    return functionPrototype

def object_init_function(thisClass):
    return object_init_function_prototype(thisClass) + """
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (%sObjectInit(self, %s) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

""" % (thisClass.superClass.className, init_param_list_input(thisClass))

def init_param_list_input(thisClass):
    paramsDesInNewFunc = thisClass.paramsDesInNewFunc
    paramsDesInNewFunc = paramsDesInNewFunc.lstrip('(')
    paramsDesInNewFunc = paramsDesInNewFunc.rstrip(')')
    paramList = clean_string(paramsDesInNewFunc).split(',')
    buffer = ''
    
    for i in range(0, len(paramList)):
        token = clean_string(paramList[i]).split(' ')
        buffer = buffer + ', ' + token[1]
    return buffer[2:]

def object_new_function(thisClass):
    returnClassType = thisClass.returnClassType
    return """%s %sNew%s
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    %s newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return %s(newObject, %s);
    }
     
""" % (returnClassType, thisClass.className, thisClass.paramsDesInNewFunc, returnClassType, object_init_function_name(thisClass), init_param_list_input(thisClass))

def color_print(color, string, hasNewLine, hasSpace):
    if hasSpace == True:
        sys.stdout.softspace=True
    else:
        sys.stdout.softspace=False
        
    if hasNewLine == True:
        if m_ColorEnable == True:
            print color + string + AtColor.CLEAR
        else:
            print string
    else:
        if m_ColorEnable == True:
            print color + string + AtColor.CLEAR,;
        else:
            print string,;

    if hasSpace == True:
        sys.stdout.softspace=True
    else:
        sys.stdout.softspace=False
    
def get_params(promptStr, validList, acceptNone=False):
    string = raw_input(promptStr + ": ")
    if not string:
        if acceptNone == True:
            return None
        else:
            return get_params(promptStr, validList)
        
    string = clean_string(string)
    if string == exitProgramStr:
        sys.exit()
        
    if not validList:
        return string
        
    if string in validList:
        return string
    
    color_print(AtColor.RED, "Invalid input! Expected: " + str(validList), True, False)
    return get_params(promptStr, validList)

def frefix_header_defines(thisClass):
    return """#ifndef _%sINTERNAL_H_
#define _%sINTERNAL_H_

""" % (thisClass.className.upper(), thisClass.className.upper())

def sufix_header_defines(thisClass):
    return """#endif /* _%sINTERNAL_H_ */
""" % (thisClass.className.upper())

def autogen_header_file(thisClass):
    if thisClass.can_gen_header_file() != True:
        color_print(AtColor.RED, "Can not gen header file for class %s" % (thisClass.className), True, False)
        return
    
    color_print(AtColor.GREEN, "Autogen header file of class %s ... " % (thisClass.className), False, False)
    fileName = thisClass.classDir + '/' + thisClass.className + "Internal.h"
    ifile = open(fileName,'w+')
    ifile.write(file_description(thisClass, ".h"))
    ifile.write(frefix_header_defines(thisClass))
    ifile.write(include_desc(thisClass, ".h"))
    ifile.write(define_macro_desc(".h"))
    ifile.write(header_typedefs_desc(thisClass))
    ifile.write(forward_declerations_desc())
    ifile.write(entries_desc())
    ifile.write(object_init_function_prototype(thisClass) + ";\r\n")
    ifile.write(c_plus_plus_endif());
    ifile.write(sufix_header_defines(thisClass))
    ifile.close()
    
    if not os.path.isfile(fileName):
        color_print(AtColor.RED, "Fail", True, False)
        return None
    
    os.system("git add " + fileName)
    color_print(AtColor.GREEN, "Done", True, False)
    return fileName
    
def autogen_source_file(thisClass):
    fileName = thisClass.classDir + '/' + thisClass.className + ".c"
    color_print(AtColor.GREEN, "Autogen source file of class %s ... " % (thisClass.className), False, False)
    
    ifile = open(fileName,'w+')
    ifile.write(file_description(thisClass, ".c"))
    ifile.write(include_desc(thisClass, ".c"))
    ifile.write(define_macro_desc(".c"))
    ifile.write(local_typedefs_desc(thisClass))
    ifile.write(global_variables_desc())
    ifile.write(local_variables_desc(thisClass))
    ifile.write(forward_declerations_desc())
    ifile.write(implementation_desc())
    ifile.write(object_size(thisClass))
    ifile.write(override_functions(thisClass))
    ifile.write(object_init_function(thisClass))
    ifile.write(object_new_function(thisClass))
    ifile.close()
    
    if not os.path.isfile(fileName):
        color_print(AtColor.RED, "Fail", True, False)
        return None
    
    os.system("git add " + fileName)
    color_print(AtColor.GREEN, "Done", True, False)
    return fileName

def init_or_new_line_process(line):
    if line == None:
        return None, None, None
    
    line = clean_string(line)
    i = line.find(' ')
    if i != -1:
        returnClassType = line[0:i]
    
    startPointer = i + 1
    i = line.find('(')
    if i != -1:
        functionName = line[startPointer:i]
    
    paramsDeclarations = line[i:len(line)]
    return returnClassType, functionName, paramsDeclarations

def init_and_new_lines_of_super_class_source_file_get(lookupDir, superClassName):
    superClassSourceFileName = superClassName + ".c"
    superClassHeaderFileName = superClassName + "Internal" + ".h"
    superInitLine = None
    superNewLine = None
    superClassDir = None
    isFound = False
    
    for root, dirs, files in os.walk(lookupDir):
        if not files:
            continue
    
        if superClassSourceFileName not in files:
            continue
        
        ifile = open(root + '/' + superClassSourceFileName,'r')
        superClassDir = root
        for line in ifile.readlines():
            if superClassName + "ObjectInit" in line:
                if isFound == False:
                    superInitLine = clean_string(line)
                    isFound = True
                    continue
    
            if superClassName + "New" in line:
                superNewLine = clean_string(line)
                ifile.close()
                return superClassDir, superInitLine, superNewLine
        ifile.close()
        if isFound == True:
            return superClassDir, superInitLine, superNewLine
        
    return superClassDir, superInitLine, superNewLine

def atsdk_infor_lookup(atsdkHome, superClassName):
    superNewLine = None
    superClassDir = None
    superInitLine = None
    for subDir in lookupSourceList:
        subLookupDir = atsdkHome + subDir
        superClassDir, superInitLine, superNewLine = init_and_new_lines_of_super_class_source_file_get(subLookupDir, superClassName)
        if superClassDir:
            break
        
    return superClassDir, superInitLine, superNewLine

def line_can_get_super_class_name(line):
    searchObj = re.search( r'\(\w+ObjectInit\(', line, re.M|re.I)
    if searchObj:
        return True
    return False

def super_class_name_from_init_line(line):
    tokenList = line.split('(')
    for token in tokenList:
        if "ObjectInit" in token:
            return token.replace("ObjectInit", '')
    return None

def c_source_file_correct(fileName, className):
    stats = os.stat(fileName)
    initLine = None
    newLine = None
    superClassName = None
    noNeedModify = False
    
    ifile = open(fileName,'r')
    content = ifile.readlines()
    for line in content:
        if line_can_get_super_class_name(line):
            superClassName = super_class_name_from_init_line(line)
            
        if className + "ObjectInit" in line: # has public init API
            initLine = line
            noNeedModify = True
        elif "ObjectInit" in line and "static" in line: # just has static init API
            initLine = line
            
        if "New(" in line:
            newLine = line
            if noNeedModify == True:
                ifile.close()
                return superClassName, initLine, newLine
    
    ifile.close()
    ifile = open(fileName + "temp",'w+')
    startIgnore = False
    initLine = initLine.replace("static", '')
    initLine = initLine.strip(' ')
    givenHederFileName = given_header_file_for_init_func_declaration_on_filename(fileName, className, initLine)
    for line in content:
        if "#include" in line and superClassName + "Internal.h" in line:
            if givenHederFileName:
                ifile.write("#include \"" + os.path.basename(givenHederFileName) + "\"\r\n")
            else:
                ifile.write("#include \"" + className + "\"\r\n")
            continue
        
        if "typedef struct t" + className in line:
            startIgnore = True
            continue
        
        if "}t" + className + ';' in line:
            startIgnore = False
            continue
        
        if startIgnore == True:
            continue
        
        if "ObjectInit(" in line and "static" in line:
            modifyLine = line.replace("ObjectInit", className + "ObjectInit")
            modifyLine = modifyLine.replace("static", '')
            modifyLine = modifyLine.strip(' ')
            initLine = modifyLine
            ifile.write(modifyLine)
            continue
        
        if "return ObjectInit(" in line:
            modifyLine = line.replace("ObjectInit", className + "ObjectInit")
            ifile.write(modifyLine)
            continue
        
        ifile.write(line)    
    ifile.close()

    os.remove(fileName) # remove old file 
    os.rename(fileName + "temp", fileName)
    os.chmod(fileName, stats.st_mode) # keep the same file mode
    os.system("git add " + fileName)
    
    return superClassName, initLine, newLine

def given_header_file_for_init_func_declaration(thisClass):
    initFuncRegEx = re.escape(thisClass.returnClassType) + r"\s+\w+ObjectInit" + re.escape(thisClass.paramsDesInInitFunc) + r";"
    for root, dirs, files in os.walk(thisClass.classDir):
        if not files:
            continue
        
        for file in files:
            if cHeaderFileType not in file:
                continue
            
            fileName = root + '/' + file
            ifile = open(fileName, "r")
            content = ifile.read()
            ifile.close()
            matchObj = re.search(initFuncRegEx, content, re.M|re.I)
            if matchObj:
                return fileName
    
    return None

def given_header_file_for_init_func_declaration_on_filename(fileName, className, initLine):
    lookupDir = os.path.dirname(fileName)
    returnClassType, superInitFuncName, paramsDesInInitFunc = init_or_new_line_process(initLine)
    initFuncRegEx = re.escape(returnClassType) + r"\s+\w*ObjectInit" + re.escape(paramsDesInInitFunc) + r";"
    for root, dirs, files in os.walk(lookupDir):
        if not files:
            continue
        
        for file in files:
            if cHeaderFileType not in file:
                continue
            
            fileName = root + '/' + file
            ifile = open(fileName, "r")
            content = ifile.read()
            ifile.close()
            matchObj = re.search(initFuncRegEx, content, re.M|re.I)
            if matchObj:
                return fileName
    
    return None

def type_def_struct(thisClass):
    return """typedef struct t%s 
    {
    t%s super;
    }t%s;
""" % (thisClass.className, thisClass.superClass.className, thisClass.className)

def add_class_declaration_in_header_file(thisClass, headerFileName):
    initFuncRegEx = re.escape(thisClass.returnClassType) + r"\s+\w+ObjectInit" + re.escape(thisClass.paramsDesInInitFunc) + r";"
    
    ifile = open(headerFileName, "r")
    content = ifile.readlines()
    ifile.close()
    
    for index in range(len(content) - 1, 0, -1):
        line = content[index]
        matchObj = re.search(initFuncRegEx, line, re.M|re.I)
        if matchObj:
            insertedLine = "%s %sObjectInit%s" % (thisClass.returnClassType, thisClass.className, thisClass.paramsDesInInitFunc) + ';'
            if not line_in_content_list_existed(insertedLine, content):
                content.insert(index + 1, insertedLine + "\r\n")
            break
    
    for index in range(len(content) - 1, 0, -1):
        line = content[index]
        if "Forward declarations" in line:
            content.insert(index, type_def_struct(thisClass) + "\r\n")
            break
        
    write_content_list_to_file(headerFileName, content)

def correct_class(atsdkHome, className):
    classSourceFileName = className + cSourceFileType
    classHeaderFileName = className + "Internal" + cHeaderFileType
    workingDir = None
    initLine = None
    newLine = None
    
    isFound = False
    for subDir in lookupSourceList:
        subLookupDir = atsdkHome + subDir
    
        if isFound == True:
            break
        
        for root, dirs, files in os.walk(subLookupDir):
            if not files:
                continue
        
            if classSourceFileName not in files:
                continue
            
            fileName = root + '/' + classSourceFileName
            if not os.path.isfile(fileName):
                continue
                            
            # Modify source file of super class
            workingDir = root
            superClassName, initLine, newLine = c_source_file_correct(fileName, className)
            isFound = True
            break
        
    superInitLine = None
    superNewLine = None
    superClassDir = None
    for subDir in lookupSourceList:
        subLookupDir = atsdkHome + subDir
        superClassDir, superInitLine, superNewLine = init_and_new_lines_of_super_class_source_file_get(subLookupDir, superClassName)
        if superInitLine and superClassDir:
            break
    
    # Modify header file of class
    if not os.path.isfile(root + '/' + classHeaderFileName):
        thisClass = create_class_to_gen_file(className, workingDir, True, superClassName, superInitLine, superNewLine, superClassDir)
        headerFileName = given_header_file_for_init_func_declaration(thisClass)
        if headerFileName:
            add_class_declaration_in_header_file(thisClass, headerFileName)
        else:
            autogen_header_file(thisClass)
        
    return workingDir, initLine, newLine
    
def path_from_token_list(tokenList, startIndex, stopIndex):
    buffer = ''
    for i in range(startIndex, stopIndex):
        if i == startIndex:
            buffer = tokenList[i] + '/'
        else:
            buffer = buffer + tokenList[i] + '/'
    return buffer

def num_couple_dots_2_string(numCoupleDots):
    buffer = ''
    for i in range(0, numCoupleDots):
        buffer = buffer + "../"
    return buffer

def relative_path_form_path(fromPath, toPath):
    if not os.path.isdir(fromPath) or not os.path.isdir(toPath):
        color_print(AtColor.YELLOW, "Can not find relative path to include", True, False)
        return "xxx/"
    
    formPathTokenList = os.path.abspath(fromPath).strip('/').split('/')
    toPathTokenList = os.path.abspath(toPath).strip('/').split('/')
    
    for i in range(len(formPathTokenList) - 1, 0, -1):
        if formPathTokenList[i] in toPathTokenList:
            if path_from_token_list(toPathTokenList, 0, i) == path_from_token_list(formPathTokenList, 0, i):
                numCoupleDots = len(formPathTokenList) - i - 1
                return num_couple_dots_2_string(numCoupleDots) + path_from_token_list(toPathTokenList, i + 1, len(toPathTokenList))
    
    color_print(AtColor.YELLOW, "Relative path is unkown", True, False)
    return "unknown/"
    
def super_class_is_valid(superClass):
    if not superClass:
        return False
    
    # must have header file
    if not os.path.isfile(superClass.classDir + '/' + superClass.header_file_name_get()):
        return False
    
    return True
    
def create_class_to_gen_file(className, workingDir, hasHeaderFile, superClassName, superInitLine, superNewLine, superClassDir, overrideListStr=None):
    returnClassType, superInitFuncName, paramsInitFuncDeclarations = init_or_new_line_process(superInitLine)
    
    if superNewLine:
        returnClassType, superNewFuncName, paramsNewFuncDeclarations = init_or_new_line_process(superNewLine)
    else:
        newFuncParam = paramsInitFuncDeclarations[paramsInitFuncDeclarations.find(', ') + 1:len(paramsInitFuncDeclarations)]
        newFuncParam = '(' + clean_string(newFuncParam)
        paramsNewFuncDeclarations = newFuncParam
        
    # Build super class
    superClass = AtClass(superClassName, superClassDir, None, returnClassType, True)
    superClass.params_desc_in_init_func_set(paramsInitFuncDeclarations)
    superClass.params_desc_in_new_func_set(paramsNewFuncDeclarations)
    headerFileName = os.path.basename(given_header_file_for_init_func_declaration(superClass))
    superClass.header_file_name_set(headerFileName)
    if super_class_is_valid(superClass) != True:
        color_print(AtColor.RED, "Super class may not have header file or public init funciton! Need to check again!", True, False)
        return None
        
    thisClass = AtClass(className, workingDir, superClass, returnClassType, hasHeaderFile)
    thisClass.params_desc_in_init_func_set(paramsInitFuncDeclarations)
    thisClass.params_desc_in_new_func_set(paramsNewFuncDeclarations)
    if overrideListStr:
        thisClass.override_list_in_string_set(overrideListStr)
        
    return thisClass
    
def class_has_header_file(classDir, className):
    headerFileName = classDir + '/' + className + "Internal.h"
    if os.path.isfile(headerFileName):
        return True
    
    # Try to find in this folder more
    initFuncDeclaration = className + "ObjectInit"
    for root, dirs, files in os.walk(classDir):
        if not files:
            continue
        
        for file in files:
            if cHeaderFileType not in file:
                continue
            
            fileName = root + '/' + file
            ifile = open(fileName, "r")
            content = ifile.read()
            ifile.close()
            if initFuncDeclaration in content:
                return True
    
    return False
    
def make_file_am_name_from_class_dir(classDir):
    if "driver" in classDir:
        return atsdkHome + "/driver/Makefile.am"
    if "platform" in classDir:
        return atsdkHome + "/platform/Makefile.am"
    if "customers" in classDir:
        return atsdkHome + "/customers/Makefile.am"
    return None
    
def line_in_content_list_existed(checkLine, content):
    for line in content:
        if checkLine in line:
            return True
    return False
    
def write_content_list_to_file(fileName, content):
    stats = os.stat(fileName)
    ifile = open(fileName + "temp",'w+')
    for line in content:
        ifile.write(line)    
    ifile.close()
    os.remove(fileName) # remove old file 
    os.rename(fileName + "temp", fileName)
    os.chmod(fileName, stats.st_mode) # keep the same file mode
    os.system("git add " + fileName)
    return fileName
    
def class_dir_in_makefile_am(thisClass):
    # TODO: Improve me
    removePatternRegex = r".*((driver)|(platform))\/"
    classDir = re.sub(removePatternRegex, '', thisClass.classDir)
    if classDir == thisClass.classDir:
        removePatternRegex = r".*(customers)\/"
        return re.sub(removePatternRegex, '', thisClass.classDir)
    return classDir

def source_file_declaration_has_existed(thisClass, content, makefileAmType):
    sourceDir = class_dir_in_makefile_am(thisClass) + '/' + thisClass.className + cSourceFileType
    if makefileAmType == "driver":
        insertedLine = "libdriver_a_SOURCES += " + sourceDir
    if makefileAmType == "platform":
        insertedLine = "libplatform_a_SOURCES += " + sourceDir
    if makefileAmType == "customers":
        insertedLine = "libcustomers_a_SOURCES += " + sourceDir
    
    for line in content:
        if insertedLine in line:
            return True
    return False

def insert_source_file_to_content(thisClass, content):
    fileName = make_file_am_name_from_class_dir(thisClass.classDir)
    dirName = class_dir_in_makefile_am(thisClass)
    insertedLine = ''
    replaceIndex = 0
    lookupDone = False
    
    while dirName != None:
        for index in range(len(content) - 1, 0, -1):
            if "driver" in fileName:
                if lookupDone == False:
                    if source_file_declaration_has_existed(thisClass, content, "driver") == True:
                        return content
                    lookupDone = True
                    
                if "libdriver_a_SOURCES +=" not in content[index]:
                    continue
                
                if replaceIndex == 0:
                    replaceIndex = index
                
                if dirName in content[index]:
                    insertedLine = "libdriver_a_SOURCES += " + class_dir_in_makefile_am(thisClass) + '/' + thisClass.className + cSourceFileType
                    if insertedLine == content[index]:
                        return content
                    content.insert(index + 1, insertedLine + "\r\n")
                    return content
                    
            if "platform" in fileName:
                if lookupDone == False:
                    if source_file_declaration_has_existed(thisClass, content, "platform") == True:
                        return content
                    lookupDone = True
                
                if "libplatform_a_SOURCES +=" not in content[index]:
                    continue
                                
                if replaceIndex == 0:
                    replaceIndex = index
                
                if dirName in content[index]:
                    insertedLine = "libplatform_a_SOURCES += " + class_dir_in_makefile_am(thisClass) + '/' + thisClass.className + cSourceFileType
                    if insertedLine == content[index]:
                        return content
                    content.insert(index + 1, insertedLine + "\r\n")
                    return content
                
            if "customers" in fileName:
                if lookupDone == False:
                    if source_file_declaration_has_existed(thisClass, content, "customers") == True:
                        return content
                    lookupDone = True
                    
                if "libcustomers_a_SOURCES += " not in content[index]:
                    continue
                                                
                if replaceIndex == 0:
                    replaceIndex = index
                
                if dirName in content[index]:
                    insertedLine = "libcustomers_a_SOURCES += " + class_dir_in_makefile_am(thisClass) + '/' + thisClass.className + cSourceFileType
                    if insertedLine == content[index]:
                        return content
                    content.insert(index + 1, insertedLine + "\r\n")
                    return content
                
        dirName = os.path.dirname(dirName)
           
    content.insert(replaceIndex + 1, insertedLine + "\r\n")
    return content
    
def autoupdate_makefile_am(thisClass):
    color_print(AtColor.GREEN, "Autoupdate makefile.am ... ", False, False)
    fileName = make_file_am_name_from_class_dir(thisClass.classDir)
    ifile = open(fileName,'r')
    content = ifile.readlines()
    ifile.close()
    content = insert_source_file_to_content(thisClass, content)
    write_content_list_to_file(fileName, content)    
    color_print(AtColor.GREEN, "Done", True, False)
    return fileName
    
def file_has_super_new_function_declaration(file, thisClass):
    if cHeaderFileType not in file:
        return None
                
    ifile = open(file,'r')
    content = ifile.readlines();
    ifile.close()
    
    for line in content:
        if thisClass.super_new_function_name_get() in line:
            return content
    
    return None
    
def new_function_prototype_build(thisClass):
    return "%s %sNew%s" % (thisClass.returnClassType, thisClass.className, thisClass.paramsDesInNewFunc)
    
def insert_class_new_function_declaration(thisClass, content):
    for line in content:
        if new_function_prototype_build(thisClass) in line:
            return content

    for line in content:
        if thisClass.super_new_function_name_get() in line:
            content.insert(content.index(line) + 1, new_function_prototype_build(thisClass) + ';\r\n')
            break
    return content

def autoupdate_header_declaration_file(thisClass):
    color_print(AtColor.GREEN, "Autoupdate declaration for function %sNew ... " % thisClass.className, False, False)
    for subDir in lookupSourceList:
        subLookupDir = atsdkHome + subDir
        for root, dirs, files in os.walk(subLookupDir):
            if not files:
                continue
        
            for file in files:
                if cHeaderFileType not in file:
                    continue
                
                fileName = root + '/' + file
                content = file_has_super_new_function_declaration(fileName, thisClass)
                if not content:
                    continue
                
                content = insert_class_new_function_declaration(thisClass, content)    
                write_content_list_to_file(fileName, content)
                color_print(AtColor.GREEN, "Done", True, False)
                return
    
def main(argv): 
    # Get working dir
    workingDir = get_params("Where to create", None)
    if not os.path.isdir(workingDir):
        needCreate = get_params("Directory does not exist. Create it? [y/n] ", [yesOptionStr, noOptionStr])
        if needCreate == yesOptionStr:
            os.system("mkdir -p " + workingDir)
            color_print(AtColor.GREEN, "%s is created" % (workingDir), True, False)
        else:
            color_print(AtColor.RED, "Can not create %s. Exit ..." % (workingDir), True, False)
            sys.exit()
        
    # Get class name and super
    className = get_params("Input class name", None)
    superClassName = get_params("Input super class name", None)
    
    # Get options
    hasHeaderFile = get_params("Need generate header file [y/n]", [yesOptionStr, noOptionStr])
    
    # Override list 
    overrideListStr = get_params("Override class list (Enter if ignore)", None, True)
    
    ## Temp
    #workingDir = "driver/src/implement/codechip/Tha60290022/ram"
    #className = "Tha60290022InternalRamPda"
    #superClassName = "Tha60210011InternalRamPda"
    #hasHeaderFile = 'n'
    #overrideListStr = "ThaInternalRam"
    #
    ## Check working dir
    #if not os.path.isdir(workingDir):
    #    color_print(AtColor.YELLOW, "%s does not exist. Try to create it ... " % (workingDir), False, False)
    #    os.system("mkdir -p " + workingDir)
    #    color_print(AtColor.GREEN, "Done", True, True)
    
    # Class name is existed
    if os.path.isfile(workingDir + '/' + className + cSourceFileType):
        color_print(AtColor.GREEN, "Class %s existed. Exit ..." % (className), True, False)
        return
    
    # General information
    superClassDir, superInitLine, superNewLine = atsdk_infor_lookup(atsdkHome, superClassName)
    if not superClassDir:
        color_print(AtColor.RED, "Can not find super class %s. Exit ..." % (superClassName), True, False)
        sys.exit()
        
    if not superInitLine or not class_has_header_file(superClassDir, superClassName):  
        color_print(AtColor.YELLOW, "Super class %s may not have header file or public init API. Try to modify this class..." % (superClassName), True, False)
        color_print(AtColor.CLEAR, "    ", False, False)
        superClassDir, superInitLine, superNewLine = correct_class(atsdkHome, superClassName)
        if not superInitLine or not superClassDir or not class_has_header_file(superClassDir, superClassName):    
            color_print(AtColor.RED, "    ... Fail", True, False)
            return
        else:
            color_print(AtColor.GREEN, "    ... Done", True, False)
            
    # Build this class
    if hasHeaderFile == yesOptionStr:
        hasHeaderFile = True
    else:
        hasHeaderFile = False
    
    # Autogenerate files
    thisClass = create_class_to_gen_file(className, workingDir, hasHeaderFile, superClassName, superInitLine, superNewLine, superClassDir, overrideListStr)
    if thisClass.can_gen_header_file() == True:
        autogen_header_file(thisClass)
    
    fileName = autogen_source_file(thisClass)
    if os.path.isfile(fileName):
        autoupdate_makefile_am(thisClass)
        autoupdate_header_declaration_file(thisClass)

if __name__ == '__main__':
    main(sys.argv)
    
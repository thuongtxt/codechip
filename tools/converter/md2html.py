#!/usr/bin/env python

'''
Created on Feb 25, 2017

@author: namnng
'''

import getopt, sys, os
import io

RED    = "\033[1;31m"
PINK   = "\033[1;35m"
YELLOW = "\033[1;33m"
CYAN   = "\033[1;36m"
GREEN  = "\033[1;32m"
CLEAR  = "\033[0m"
    
class Md2Html(object):
    def __init__(self, inputDir, outputDir=None):
        self.inputDir = inputDir
        self.outputDir = outputDir
        
        self.useGfm = False
        self.githubUsername = None
        self.githubPassword = None
        
    def _gfmConvert(self, inputFile, outputFile):
        import grip
        
        for _ in range(5):
            try:
                grip.export(inputFile, out_filename=outputFile, username=self.githubUsername, password=self.githubPassword)
                break
            except Exception as e:
                print e
                print YELLOW + "Retrying..." + CLEAR
        
    def _defaultConvert(self, inputFile, outputFile):
        import markdown
        mdFile = io.open(inputFile, "r", encoding="utf_8", errors='ignore')
        md = mdFile.read()
        mdFile.close()
        
        extensions = ['extra', 'abbr', 'attr_list', 'def_list', 'fenced_code', 
                      'footnotes', 'tables', 'smart_strong', 'admonition',
                      'codehilite', 'headerid', 'meta', 'nl2br', 'sane_lists',
                      'smarty', 'toc', 'wikilinks']
        html = markdown.markdown(md, extensions=extensions, output_format='html')
        out = open(outputFile, "w")
        out.write(html)
        
    def convertFile(self, filePath):
        fileName = os.path.basename(filePath)
        htmlFile = "%s.html" % os.path.splitext(fileName)[0]
        outputFile = "%s/%s" % (self.outputDir, htmlFile)
        
        # Create output directory if it does not exist
        if not os.path.exists(self.outputDir):
            os.makedirs(self.outputDir)
        
        print GREEN + "Converting file: " + CLEAR + "%s to %s" % (filePath, outputFile)
        
        if self.useGfm:
            return self._gfmConvert(filePath, outputFile)
        return self._defaultConvert(filePath, outputFile)
        
    def convert(self):
        for directoryName, _, fileList in os.walk(self.inputDir):
            for fileName in fileList:
                ext = None
                try:
                    ext = os.path.splitext(fileName)[1]
                    if ext != ".md":
                        continue
                except:
                    continue
    
                self.convertFile("%s/%s" % (directoryName, fileName))

def gripExist():
    # Grip does not exist, just do nothing
    try:
        import grip
        grip.__version__
        return True
    except:
        return False

if __name__ == '__main__':
    inputDir = None
    outputDir = None
    useGfm = False
    githubUserName = None
    githubPassword = None
    
    def printUsage(exitCode = None):
        print "Usage: %s --input-dir=<inputDirectory> --output-dir=<outputDirectory> [--gfm=<username>,<password>]" % sys.argv[0]
        if exitCode:
            sys.exit(exitCode)
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"",["help", "input-dir=", "output-dir=", "gfm="])
    except getopt.GetoptError:
        printUsage(exitCode=1)

    for opt, arg in opts:
        if opt == "--help":
            printUsage(exitCode=0)
            
        if opt == "--input-dir":
            inputDir = arg
            
        if opt == "--output-dir":
            outputDir = arg
            
        if opt == "--gfm":
            loginInfo = arg.split(",")
            if len(loginInfo) == 2 and gripExist():
                useGfm = True
                githubUserName = loginInfo[0]
                githubPassword = loginInfo[1]
    
    if inputDir is None or outputDir is None:
        printUsage(exitCode=1)
    
    converter = Md2Html(inputDir, outputDir)
    converter.useGfm = useGfm
    converter.githubUsername = githubUserName
    converter.githubPassword = githubPassword
    converter.convert()

#!/usr/bin/python
import sys
import os

##################################################################################
# FUNCTIONS
##################################################################################	
def check_string_valid(string):
	if len(string) == 0:
		print "Next time please enter something"
		sys.exit()

# Get the last element of the path to get file name
def full_file_name_get(path):
	list_element = path.split('/')
	list_length = len(list_element)
	return list_element[list_length - 1]
	
# Remove file type if have
def file_name_get(path):
	list_element = full_file_name_get(path).split('.')
	list_length = len(list_element)
	if (list_length == 1):
		return list_element[0].lower()
	
	if (list_length == 2):
		return list_element[0].lower()
		
	file_name = '.'.join(list_element[0:(list_length - 1)])
	return file_name.lower()
	
# Remove file name and return the path to this file
def path_get(path):
	return path.strip(full_file_name_get(path))
	
def output_file_path_get(path):
	return path_get(path) + file_name_get(path) + '.c'
	
# read file into an array of hex formatted strings
def read_binary(path):
	f = open(path,'rb')
	binlist = []
	while 1:
		c = f.read(1)
		if not c:
			break;
		binlist.append(hex(ord(c)))
		
	f.close()
	return binlist

def write_to_text_file(input_file_path, content):
	firstNumber = 0
	global fpga_array_var_name
	global fpga_version_var_name
	
	input_file_name = file_name_get(input_file_path)
	if (input_file_name.find("af6fhn0023") == 0):
		fpga_array_var_name = "af6fhn0023_fpga"
		fpga_version_var_name = "af6fhn0023_version"
	elif (input_file_name.find("af6fhw0051") == 0):
		fpga_array_var_name = "af6fhw0051_fpga"
		fpga_version_var_name = "af6fhw0051_version"
	else:
		print "This FPGA image is not supported, expect FPGA image name af6fhn0023, af6fhw0051..."
		sys.exit()
	
	f = open(output_file_path_get(input_file_path), 'w')
	
	# Write variables to file
	myString = 'static const char *' + fpga_version_var_name + ' = "' + input_file_name + '";\n'
	f.write(myString)
	f.write("static const unsigned char ")
	f.write(fpga_array_var_name)
	f.write("[] = {")
	for item in content:
		if firstNumber == 0:
			f.write("%s" % item)
		else:
			f.write(",%s" % item)	
		firstNumber = 1
	f.write("};\n")
	
	# Create function to return FPGA array
	myString = 'const unsigned char *' + fpga_array_var_name + '_get' + '(unsigned int *size)\n'
	f.write(myString)
	f.write("    {\n    *size = sizeof(%s);\n" % fpga_array_var_name)
	f.write("    return %s;\n    }\n" % fpga_array_var_name)
	
	# Create function to return FPGA version	
	myString = '\nconst char *' + fpga_version_var_name + '_get' + '(void)\n    {\n'
	f.write(myString)
	f.write("    return %s;\n    }\n" % fpga_version_var_name)
	f.close()
	
def main_file_generate(input_file_path, content):
	output_list_length = len(content)
	middle_element_index = output_list_length / 2
	path = path_get(input_file_path)
	myString = path + 'main.c'
	f = open(myString, 'w')
	f.write("#include <stdio.h>\n\n")
	f.write("extern unsigned char *%s_get(void);\n" % fpga_version_var_name)
	f.write("extern unsigned char *%s_get(unsigned int *size);\n" % fpga_array_var_name)
	f.write("int main()\n{\n")
	f.write("    int size;\n")
	f.write("    unsigned char *array = %s_get(&size);\n" % fpga_array_var_name)
	myString = '    printf("first element  = %d\\n", ' + 'array' + '[0]);\n'
	f.write(myString)	
	myString = '    printf("middle element = %d\\n", ' + 'array' + '[' + str(middle_element_index) + ']);\n'
	f.write(myString)	
	myString = '    printf("last element   = %d\\n", ' + 'array' + '[' + str(output_list_length - 1) + ']);\n'
	f.write(myString)	
	myString = '    printf(\"size of array is %d bytes\\n", size);\n' 
	f.write(myString)
	myString = '    printf(\"version of FPGA is %s\\n", ' + fpga_version_var_name + '_get());\n' 
	f.write(myString)
	f.write("    return 0;\n")
	f.write("    }\n")
	
##################################################################################
# MAIN CODE
##################################################################################	
def main():
	input_file_path = raw_input("Enter path to FPGA image: ")
	if len(input_file_path) == 0:
		print "Next time please enter something"
		sys.exit()
	try:
		output = read_binary(input_file_path)
	except IOError:
		print "There was an error reading file"
		sys.exit()

	write_to_text_file(input_file_path, output)
	main_file_generate(input_file_path, output)
	path = path_get(input_file_path)
	myString = 'gcc -o ' + path + 'bin_to_c_test ' + output_file_path_get(input_file_path) + ' ' + path + 'main.c'
	os.system(myString)
	
	print "Finish and try to run file test"
	myString = path + 'bin_to_c_test'
	os.system(myString)

if __name__ == "__main__":
	main()
   
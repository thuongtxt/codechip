#!/usr/bin/python

import re
import os
import sys
import getopt
import subprocess

def PathListToTagString(pathList, projectPath, nameFile):
    string = '_'
    tagVerSion = ''
    
    for path_i in range(len(pathList)):
        if projectPath == pathList[path_i]:
            break
    
    # Concate string path_list to tag String 
    path_i = path_i + 1
    while (path_i < len(pathList)):
        if nameFile != pathList[path_i]:
            tag_Current = pathList[path_i] + string
            tagVerSion = tagVerSion + tag_Current
        path_i = path_i + 1
        
    # Remove '_' in last string
    return tagVerSion[:(len(tagVerSion) - 1)]

def ProcessGitCommand(nameFile, fileBinary, tagString):
    dirHome = '.'
    
    fileTagString = tagString + '_' + nameFile
    subprocess.call(["cp", "-fR", fileBinary, dirHome])
    subprocess.call(["git", "add", nameFile])
    subprocess.call(["git", "tag", fileTagString])
    subprocess.call(["git", "commit", "-am", fileTagString])

def BracnhHasExist(pathList):
    subprocess.call("git branch > log.txt", shell=True)
    logFile = 'log.txt'
    inputFile = open(logFile, 'r')
    inputContent = inputFile.readlines()
    
    for lineNumber, inputFile in enumerate(inputContent):
        lineNoSpace = inputFile.rstrip()
        lineNoSpace = lineNoSpace.lstrip()
        if lineNoSpace == pathList:
            return True
    return False

def ProcessCopyBinary(nameFile, rootFile, pathInput):
    projectPath = ''
    headPath = ''
    
    # Get tring project and file directory
    fileBinary = os.path.join(rootFile, nameFile)
    headPath, projectPath = os.path.split(pathInput)

    # Get list path in file directory and Tag Sring
    pathList = fileBinary.split(os.sep)
    tagString = PathListToTagString(pathList, projectPath, nameFile)
    
    # Process git command
    for path_i in range(len(pathList)):
    	# if branch not exist, checkout new branch 
        if projectPath == pathList[path_i] and pathList[path_i + 2] != nameFile:
            if BracnhHasExist(pathList[path_i + 1]):
                subprocess.call(["git", "checkout", pathList[path_i + 1]])
            else:
                subprocess.call(["git", "checkout", "-b", pathList[path_i + 1]])
    ProcessGitCommand(nameFile, fileBinary, tagString)
    # After process on sub branch, change to main branch process next path 
    subprocess.call(["git", "checkout", "master"])
    
def ProcessBackupBinary(nameFile, path):
    for root, dirs, files in os.walk(path):
        if nameFile in files:
           ProcessCopyBinary(nameFile, root, path)

def PrintUsage():
    print ('usage: copy_binary.py -n <nameFile> -d <directory>')
    print ('       -n, --nameFile          Name file binary')
    print ('       -d, --directory         Direction which need backup')
    
def main(argv):
    nameFile = ''
    inputDirectory = '.'
    try:
        opts, args = getopt.getopt(argv,"hn:d:",["nameFile=","directory="])
    except getopt.GetoptError:
        PrintUsage()
        sys.exit(2) 
    for opt, arg in opts:
        if opt == 'h':
            PrintUsage()
            sys.exit()
        elif opt in ("-n", "--nameFile"):
            nameFile = arg
            print 'Name file:', nameFile
        elif opt in ("-d", "--directory"):
            inputDirectory = arg
            print 'Path is:', inputDirectory
        else:
            printUsage()
            sys.exit()
    ProcessBackupBinary(nameFile, inputDirectory)
    
if __name__ == "__main__":
    main(sys.argv[1:])
#! /usr/bin/python

'''
<item>         ::= <commandLine><newLine><anySpaces><commentOpen><description><commentClose>
<commandLine   ::= .*
<newLine>      ::= [\r\n|\r]*
<anySpaces>    ::= \s*
<commentOpen>  ::= /\*
<commentClose> ::= \*/
<description>  ::= ((.*\s)*)

==> (.*)[\r\n|\r]*\s*/\*((.*\s)*)\*/

'''

import os
import re
import sys
import glob
import getopt
from array import array
from collections import OrderedDict

class MarkdownElement(object):
    def markdownHeading(self):
        return None
    
    def replaceNewLinesByBreaks(self, text):
        return re.sub("\n|\r\n", "<br/>", text)
    
    def escapeSpecialMarkdownCharacters(self, text):
        text = re.sub("<", "&lt;", text)
        text = re.sub(">", "&gt;", text)
        return text
    
    def removeTrailingSpaces(self, text):
        newText = ""
        for line in text.split("\n"):
            newText += line.lstrip() + "\n"
        return newText.strip()
    
    def toCode(self, text):
        return "`" + text + "`"
    
    def toMarkdownText(self, text):
        text = self.escapeSpecialMarkdownCharacters(text)
        return self.replaceNewLinesByBreaks(text)
    
    def replaceTabBySpace(self, aString):
        pattern = re.compile("\t")
        return re.sub(pattern, "        ", aString)
    
    def makeHeading(self, headingLevel, text, referenceId = None):
        if referenceId is None:
            referenceId = self.makeAnchor(text)
        return headingLevel + " " + text + " [" + referenceId + "]\r\n\r\n"
    
    def makeAnchor(self, headingName):
        return re.sub(" |[-\(\)/]", "_", headingName.lower())
    
class AtCliGroup(MarkdownElement):
    def __init__(self, identifier, name, parentGroup=None):
        self.parentGroup = parentGroup
        self.name = name
        self.identifier = identifier
        self.subGroups = {}
        self.clis = []
        self.heading = ""
        
    def addCli(self, atCli):
        self.clis.append(atCli)
    
    def markdownHeading(self):
        if self.parentGroup is None:
            return self.heading 
        return self.parentGroup.markdownHeading() + "#"
    
    def removeSpaces(self, aString, numSpaces):
        patternString = "\s{" + `numSpaces` + "}"
        pattern = re.compile(patternString)
        newString = ""
        
        aString = self.replaceTabBySpace(aString)
        for line in re.split("(\n|\r\n)", aString):
            if len(line.strip()) == 0:
                continue
            
            newLine = re.sub(pattern, "", line, 1)
            if len(newString) == 0:
                newString = newLine
            else:
                newString = newString + "\r\n" + newLine
            
        return newString
    
    def _colonPositionIsValid(self, line):
        position = line.find(":")
        return position == 15
    
    def _checkDescription(self, cli, description):
        hasSyntax = False
        hasParameters = False
        hasDescription = False
        hasExample = False
        commandName = cli.commandName()
        for line in description.split("\n"):
            needCheckColon = False
            
            if "Syntax" in line:
                hasSyntax = True
                needCheckColon = True
            if "Parameter" in line:
                hasParameters = True
                needCheckColon = True
            if "Description" in line:
                hasDescription = True
                needCheckColon = True
            if "Example" in line:
                hasExample = True
                needCheckColon = True
            
            if not needCheckColon:
                continue
            
            if ":" not in line:
                raise Exception("Command: '" + commandName + "' has no colon position at line: \'" + line + "\'")
            if not self._colonPositionIsValid(line):
                raise Exception("Command: '" + commandName + "' has Colon position is not right at line: \'" + line + "\'")
        
        if not hasSyntax:
            raise Exception("Description of command \'" + commandName + "\' has no Syntax")
        if not hasParameters:
            raise Exception("Description of command \'" + commandName + "\' has no Parameter")
        if not hasDescription:
            raise Exception("Description of command \'" + commandName + "\' has no Description")
        if not hasExample:
            raise Exception("Description of command \'" + commandName + "\' has no Example")
        
    def _createCliFromCommandAndDescription(self, command, description):
        descriptionPattern = re.compile("\s*Syntax\s*:\s*(.+)\s*Parameter\s*:((\s|.)*?)Description\s*:((\s|.)*?)Example\s*:((\s|.)*)")
        
        # Extract CLI tokens        
        commandTokens = re.split("\s+", command)
        cli = AtCli(self)
        level = commandTokens[0]
        try:
            if int(level) < 1:
                raise Exception("\'" + command + "\' must have level")
        except ValueError:
            raise Exception("\'" + command + "\' has invalid level")
            
        for i in range(1, int(level) + 1):
            cli.commandTokens.append(commandTokens[i]);
        
        self._checkDescription(cli, description)
        
        # Extract C function that implements this CLI
        cli.handler = commandTokens[int(level) + 1]
        
        # Extract sub fields from description
        match = descriptionPattern.match(description)
        numSpaces = 17
        cli.syntax      = match.group(1).strip()
        cli.parameters  = self.removeSpaces(match.group(2).strip(), numSpaces)
        cli.description = self.removeSpaces(match.group(4).strip(), numSpaces)
        cli.example     = self.removeSpaces(match.group(6).strip(), numSpaces)
        
        commandName = cli.commandName()
        if len(cli.syntax) == 0:
            raise Exception("Syntax is empty for command \'" + commandName + "\'")
        if len(cli.parameters) == 0:
            raise Exception("Parameter is empty for command \'" + commandName + "\'")
        if len(cli.description) == 0:
            raise Exception("Description is empty for command \'" + commandName + "\'")
        if len(cli.example) == 0:
            raise Exception("Example is empty for command \'" + commandName + "\'")
        
        return cli

    def _extractClisFromFile(self, fileName):
        commandItemPattern = re.compile("((.+)[\r\n|\r]*\s*/\*((.|\s)*?)\*/)")

        inputFile = open(fileName,'r');
        content = inputFile.read();
        inputFile.close()
        
        if "\t" in content:
            raise Exception("'" + fileName + "' should not have TAB characters")
        
        for commandItemMatch in re.findall(commandItemPattern, content):
            command     = commandItemMatch[1]
            description = commandItemMatch[2]
            self.clis.append(self._createCliFromCommandAndDescription(command, description))
    
    def _searchForGroupByIdentifier(self, groupIdentifier):
        # Try access first
        try:
            group = self.subGroups[groupIdentifier]
            if group is not None:
                return group
        except:
            pass
        
        # Search for it
        for subGroupIdentifier, subGroup in self.subGroups.iteritems():
            if subGroupIdentifier == groupIdentifier:
                return subGroup
            
            foundGroup = subGroup._searchForGroupByIdentifier(groupIdentifier)
            if foundGroup is not None:
                return foundGroup
                
        return None 
    
    def containsGroup(self, group):
        try:
            self.subGroups[group.identifier]
            return True
        except:
            return False
    
    def addGroup(self, group):
        assert(group.parentGroup is None)
        if self.containsGroup(group):
            return
        self.subGroups[group.identifier] = group
        group.parentGroup = self
    
    def createMarkdownCliList(self):
        if len(self.clis) == 0:
            return None
        
        markdown =  ""
        markdown += "|CLI|Purpose|\r\n"
        markdown += "|-------------------------|\r\n"
        for cli in self.clis:
            markdown += "|[" + self.toCode(cli.syntax) +"] (#" + self.makeAnchor(cli.commandName()) + ")|" + self.toMarkdownText(cli.description) + "|\r\n"
            
        return markdown
    
    def cliManualHeadingIdentifier(self):
        return self.makeAnchor(self.name + " CLI manual")
    
    def writeCliManualToMarkdownFile(self, outputMarkdown):
        '''
        Write manual of all of CLIs in this group and it sub groups
        '''
        # Write this group to file
        outputFile = open(outputMarkdown, "a")
        outputFile.write(self.makeHeading(self.markdownHeading(), self.name, self.cliManualHeadingIdentifier()))
        for cli in self.clis:
            outputFile.write(cli.toMarkdownManual())
        outputFile.close()
        
        # And write its sub groups to file
        for _, group in self.subGroups.iteritems():
            group.writeCliManualToMarkdownFile(outputMarkdown)
            
    def cliListHeadingIdentifier(self):
        return self.makeAnchor(self.name + " CLI list")
            
    def writeCliListsToMarkdownFile(self, outputMarkdown):
        '''
        Write list of CLIs to markdown file. CLIs of its sub groups are also written
        '''
        outputFile = open(outputMarkdown, "a")
        outputFile.write(self.makeHeading(self.markdownHeading(), self.name, self.cliListHeadingIdentifier()))
        cliListMarkdown = self.createMarkdownCliList()
        if cliListMarkdown is not None:
            outputFile.write(cliListMarkdown)
            outputFile.write("\r\n")
            
        outputFile.close()
        
        for _, group in self.subGroups.iteritems():
            group.writeCliListsToMarkdownFile(outputMarkdown)
        
class AtCli(MarkdownElement):
    def __init__(self, group):
        self.commandTokens = []
        self.handler = None
        self.syntax = None
        self.parameters = None
        self.description = None
        self.example = None
        self.group = group
    
    def markdownHeading(self):
        return self.group.markdownHeading() + "#"
    
    def commandName(self):
        name = ""
        for token in self.commandTokens:
            name += token + " "
            
        return name.strip()
    
    def toMarkdownManual(self):
        assert(self.group)
        
        heading = self.markdownHeading() + " " + self.commandName() + " [" + self.makeAnchor(self.commandName()) + "]\r\n\r\n"
        
        # Build header
        table  = ""
        table += "|" + self.commandName() + "||\r\n" 
        table += "|-------------------------|\r\n"
        
        # Build attributes
        table += "|<b>Syntax<b>      |" + self.toCode(self.removeTrailingSpaces(self.syntax))  + "|\r\n"
        table += "|<b>Parameters<b>  |" + self.toMarkdownText(self.parameters)                 + "|\r\n"
        table += "|<b>Description<b> |" + self.toMarkdownText(self.description)                + "|\r\n"
        table += "|<b>Example<b>     |" + self.toMarkdownText(self.example)                    + "|\r\n"
        
        return heading + table + "\r\n"

class Cli2Markdown(MarkdownElement):
    
    def __init__(self, folder = ".", markdownFile = "markdown.md", cliConfExtensions = [".conf"]):
        self.folder = folder
        self.markdownFile = markdownFile
        self.extensions = cliConfExtensions
        self.allGroups = OrderedDict()
        self._extractAllGroups(self.folder)

    def _overviewHeading(self):
        return "Overview"

    def _overview(self):
        overviewText  = self.makeHeading("##", self._overviewHeading())
        overviewText += """
This document is for fast reference all of AT CLIs.
                        """
        return overviewText + "\r\n"
    
    def _shouldGenerateFile(self, fileName):
        for extention in self.extensions[:]:
            if fileName.lower().endswith(extention):
                return True
        return False
    
    def _commandCheck(self, command):
        if command not in ["@group", "@ingroup"]:
            raise Exception("Command: '" + command + "' is not defined")
    
    def _searchGroupByIdentifier(self, groupIdentifier):
        # try accessing before searching
        group = None
        try:
            group = self.allGroups[groupIdentifier]
            if group is not None:
                return group
        except:
            pass
        
        # Search for it
        for identifier, group in self.allGroups.iteritems():
            if identifier == groupIdentifier:
                return group
            
            foundSubGroup = group._searchForGroupByIdentifier(groupIdentifier)
            if foundSubGroup is not None:
                return foundSubGroup
            
        return None
    
    def _extractGroupToPutClisFromFile(self, fileName):
        inputFile = open(fileName,'r');
        content = inputFile.read();
        inputFile.close()
        pattern = re.compile("#\s*(@\w+)\s*?(\w+)\s*?\"(.+?)\"", re.DOTALL)
        groupToPutCli = None
        parentGroup = None
        for match in re.findall(pattern, content):
            command = match[0]
            self._commandCheck(command)
            groupIdendifier = match[1]
            try:
                groupName = match[2]
            except:
                groupName = None
            
            group = self._searchGroupByIdentifier(groupIdendifier)
            if group is None:
                group = AtCliGroup(groupIdendifier, groupName, None)
                self.allGroups[groupIdendifier] = group
            
            if command == "@group":
                groupToPutCli = group
            
            if command == "@ingroup":
                parentGroup = group
        
        assert(groupToPutCli)
        
        if groupToPutCli.name == "None":
            del(self.allGroups[groupToPutCli.identifier])
            return None
        
        if parentGroup:
            # Need to remove this group out of top groups
            if groupToPutCli.parentGroup is None:
                 del(self.allGroups[groupToPutCli.identifier])
            parentGroup.addGroup(groupToPutCli)
        
        return groupToPutCli
    
    def _extractGroupsFromFile(self, fileName):
        print "Extracting CLIs from file " + fileName
        
        group = self._extractGroupToPutClisFromFile(fileName)
        if group is not None:
            group._extractClisFromFile(fileName)
    
    def _extractAllGroups(self, path):
        for currentFile in glob.glob( os.path.join(path, '*') ):
            if os.path.isdir(currentFile):
                self._extractAllGroups(currentFile)
            
            if self._shouldGenerateFile(currentFile):
                self._extractGroupsFromFile(currentFile)
    
    def _writeToFile(self, fileName, text):
        directory = os.path.dirname(fileName)
        if not os.path.exists(directory):
            os.mkdir(directory)
        
        outputFile = open(fileName, "a")
        outputFile.write(text)
        outputFile.close()
    
    def _cliListHeading(self):
        return "CLI lists"
    
    def _cliManualHeading(self):
        return "CLI manual"
    
    def _generateCliList(self):
        heading = self._cliListHeading()
        self._writeToFile(self.markdownFile, self.makeHeading("##", heading))
        for groupId, group in self.allGroups.iteritems():
            group.heading = "###"
            group.writeCliListsToMarkdownFile(self.markdownFile)
    
    def _generateCliManual(self):
        self._writeToFile(self.markdownFile, self.makeHeading("##", self._cliManualHeading()))
        for groupId, group in self.allGroups.iteritems():
            group.heading = "###"
            group.writeCliManualToMarkdownFile(self.markdownFile)
    
    def _extractAllHeadingsFromMarkdownFile(self, markdownFile):
        inputFile = open(markdownFile,'r');
        content = inputFile.read();
        inputFile.close()
        pattern = re.compile("(#+)\s*?(.*)", re.DOTALL)
        headings = []
        for match in re.findall(pattern, content):
            heading = {}
            heading['heading'] = match[1]
            heading['name']    = match[2]
            headings.append(heading)
            
        return headings

    def _documentHistoryHeading(self):
        return "Document History"
    
    def _documentHistory(self):
        history  = self.makeHeading("##", self._documentHistoryHeading())
        history += """
|Revision    |Date         |Description            |
|--------------------------------------------------|
|1.0         |Jun 28, 2014 | Initial version       |
|1.1         |Nov 10, 2014 | Add new MEF-8 CLIs, device CLIs |
|1.2         |Dec 11, 2014 | New clock extract CLIs <br> New OSAL CLIs for debugging <br> New Encap CLIs for debugging <br> New DE1 CLIs to control auto RAI <br> New TOH PW CLIs <br> New SDH Line CLIs to control auto RDI <br> New SDH Path CLIs to control auto RDI, loopback <br> New customer specific CLIs |
                   """
        return history + "\r\n"
    
    def _makeReference(self, reference, description = None):
        if description is None:
            description = reference
        return "[" + description + "] (#" + reference + ")"
    
    def _makeBulletByLevel(self, bullet, level):
        text = ""
        for i in range(0, level):
            text += "    "
        return text + bullet
    
    def _cliManualTableOfContentOfGroup(self, group, bulletLevel):
        content = self._makeBulletByLevel("*", bulletLevel) + " " + self._makeReference(group.cliManualHeadingIdentifier(), group.name) + "\r\n"
        for _, subGroup in group.subGroups.iteritems():
            content += self._cliManualTableOfContentOfGroup(subGroup, bulletLevel + 1)
        return content
    
    def _cliListTableOfContentOfGroup(self, group, bulletLevel):
        content = self._makeBulletByLevel("*", bulletLevel) + " " + self._makeReference(group.cliListHeadingIdentifier(), group.name) + "\r\n"
        for _, subGroup in group.subGroups.iteritems():
            content += self._cliListTableOfContentOfGroup(subGroup, bulletLevel + 1)
        return content
    
    def _cliListTableOfContent(self):
        content = ""
        for _, group in self.allGroups.iteritems():
            content += self._cliListTableOfContentOfGroup(group, 1)
        return content
    
    def _cliManualTableOfContent(self):
        content = ""
        for _, group in self.allGroups.iteritems():
            content += self._cliManualTableOfContentOfGroup(group, 1)
        return content
    
    def _tableOfContent(self):
        content  = self.makeHeading("##", "Table of Contents")
        content += "* " + self._makeReference(self.makeAnchor(self._documentHistoryHeading()), self._documentHistoryHeading()) + "\r\n"
        content += "* " + self._makeReference(self.makeAnchor(self._overviewHeading()), self._overviewHeading()) + "\r\n"
        content += "* " + self._makeReference(self.makeAnchor(self._cliListHeading()), self._cliListHeading()) + "\r\n"
        content += self._cliListTableOfContent()
        content += "* " + self._makeReference(self.makeAnchor(self._cliManualHeading()), self._cliManualHeading()) + "\r\n"
        content += self._cliManualTableOfContent()
        
        return content + "\r\n"
    
    def _writeOverview(self):
        self._writeToFile(self.markdownFile, self._overview())
    
    def _writeDocumentHistory(self):
        self._writeToFile(self.markdownFile, self._documentHistory())
    
    def _writeTableOfContent(self):
        self._writeToFile(self.markdownFile, self._tableOfContent())
        
    def generate(self):
        self._writeDocumentHistory()
        self._writeTableOfContent()
        self._writeOverview()
        self._generateCliList()
        self._generateCliManual()

def printUsage(appName):
    print ('usage: ' + appName + ' -d <directory> -m <markdown_file>')
    print (' -d, --directory=<root directory>')
    print (' -m, --markdown_file=<Output markdown file>')
      
def main(argv):
    inputDirectory = '.' #Default is current directory
    outputMarkdown = 'markdown.md'
    
    # Have user inputs
    try:
        opts, args = getopt.getopt(argv[1:],"hd:m:e:",["help", "directory=", "markdown_file=", "exclude_files="])
    except getopt.GetoptError:
        printUsage(argv[0])
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printUsage(argv[0])
            sys.exit()
        elif opt in ("-d", "--directory"):
            inputDirectory = arg
        elif opt in ("-m", "--markdown_file"):
            outputMarkdown = arg
    
    # Clean up first 
    try:
        os.remove(outputMarkdown)
    except:
        pass
    
    generator = Cli2Markdown(inputDirectory, outputMarkdown, [".conf"])
    generator.generate()

if __name__ == "__main__":
   main(sys.argv)

#!/usr/bin/tclsh

proc es_host_process_dump {in_file horz_arr_a vert_arr_a utsign_arr_a sample_count_arr_a error_count_arr_a prescale_arr_a data_width} {
	upvar $horz_arr_a horz_arr
	upvar $vert_arr_a vert_arr
	upvar $utsign_arr_a utsign_arr
	upvar $sample_count_arr_a sample_count_arr
	upvar $error_count_arr_a error_count_arr
	upvar $prescale_arr_a prescale_arr
	
	set f_in [open $in_file r]
	set count 0
	
	# Ignore first 3 lines
	gets $f_in line
	gets $f_in line
	gets $f_in line
	
	while {[gets $f_in line] >= 0} {		
		# Separate to address and value fields
		set fields [split $line ":"]
		
		# horzOffset:vertOffset:errorCount:sampleCount:prescale:utSign
		set horz_offset  [lindex $fields 0]
		set vert_offset  [lindex $fields 1]
		set error_count  [lindex $fields 2]
		set sample_count [lindex $fields 3]
		set prescale     [lindex $fields 4]
		set ut_sign      [lindex $fields 5]
		
		# Get total samples by multiplying sample count with data width and 2^(prescale + 1)
		# set sample_total [expr $sample_count * wide($data_width << ( 1+$prescale )) ]
		
		# Update stored variables
		set sample_count_arr($horz_offset,$vert_offset,$ut_sign) $sample_count
		set error_count_arr($horz_offset,$vert_offset,$ut_sign) $error_count
		set prescale_arr($horz_offset,$vert_offset,$ut_sign) $prescale
		
		# Record horizontal offset, vertical offset, and ut sign values found
		set horz_arr($horz_offset) 1
		set vert_arr($vert_offset) 1
		set utsign_arr($ut_sign)   1
		
		set count [expr $count + 1]
	}
	close $f_in
}

# #####################################################################################################
# Function: es_host_gen_csv
# Description: Given arrays containing eye scan data, generates csv file that can be opened in iBERTplotter
#
# Parameters:
# 	csv_file: name of output csv file
# 	lpm_mode: equalizer mode 
# 	vert_step: vertical scan step size
# 	horz_arr_a: reference to array populated with horizontal offset values
# 	vert_arr_a: reference to array populated with vertical offset values
# 	utsign_arr_a: reference to array populated with ut-sign values
# 	sample_count_arr_a: reference to array populated with sample count values
# 	error_count_arr_a: reference to array populated with error count values
# 	prescale_arr_a: reference to array populated with prescale values
# 	data width: parallel data width
# 	rate: rate mode (e.g. full-rate, half-rate, etc)
#
# Returns: none
# #####################################################################################################
proc es_host_gen_csv {csv_file lpm_mode vert_step horz_arr_a vert_arr_a utsign_arr_a sample_count_arr_a error_count_arr_a prescale_arr_a data_width rate} {

	upvar $horz_arr_a horz_arr
	upvar $prescale_arr_a prescale_arr
	upvar $vert_arr_a vert_arr
	upvar $utsign_arr_a utsign_arr
	upvar $sample_count_arr_a sample_count_arr
	upvar $error_count_arr_a error_count_arr
	
	set f_csv [open $csv_file w]
	
	# Generate CSV Header iBERTplotter
	puts $f_csv "gt type,"
	puts $f_csv "device,"
	puts $f_csv "sw version,"
	set samp_per_ui [expr (2 * $rate) + 1]
	puts $f_csv "samples per ui, $samp_per_ui"
	puts $f_csv "ui width,1"
	puts $f_csv "date,"
	puts $f_csv "time,"
	puts $f_csv "voltage interval,$vert_step"
	puts $f_csv "sweep test settings,RX Sampling Point,,,"
	puts $f_csv "sweep test settings,TX Diff Swing,,,"
	puts $f_csv "sweep test settings,TX Pre-Cursor,,,"
	puts $f_csv "sweep test settings,TX Post-Cursor,,,"
	puts $f_csv "=========================================="
	puts $f_csv "Iteration,Elapsed Time,TX Diff Swing,TX Pre-Cursor,TX Post-Cursor,Voltage,RX Sampling Point(tap),Link,ES_VERT_OFFSET,ES_HORZ_OFFSET,BER"
	
	set ber 0
	set iter 0
	
	foreach curr_vert [lsort -integer [array names vert_arr]] {
		foreach curr_horz [lsort -integer [array names horz_arr]] {
			if {$lpm_mode == 1} {
				# In LPM mode, only process data with ut-sign of 0
				if {[info exists error_count_arr($curr_horz,$curr_vert,0)] == 1} {
					set curr_err0  $error_count_arr($curr_horz,$curr_vert,0)
					set curr_samp0 $sample_count_arr($curr_horz,$curr_vert,0)
					set curr_prescale0 $prescale_arr($curr_horz,$curr_vert,0)
					
					# Get total samples by multiplying sample count with data width and 2^(prescale + 1)
					set curr_tot_samp0 [expr wide($curr_samp0) * (wide($data_width) << (1+$curr_prescale0) )]
					set ber [format "%.2E" [expr double($curr_err0)/double($curr_tot_samp0)]]
					
					# To limit BER floor, assume 1 as minimum number of errors.
					if {$ber == 0} {
						set ber [expr 1/double($curr_tot_samp0)]
					}
					
				} else {
					#puts $f_csv "Error: X:$curr_horz, Y:$curr_vert, UT:0 data point does not exist!"
				}
			} else {
				# puts "here"
				# In DFE mode, average BER for ut-sign 0 and 1
				if {[info exists error_count_arr($curr_horz,$curr_vert,0)] == 1 && [info exists error_count_arr($curr_horz,$curr_vert,1)] == 1} {
					
					set curr_err0  $error_count_arr($curr_horz,$curr_vert,0)
					set curr_samp0 $sample_count_arr($curr_horz,$curr_vert,0)
					set curr_prescale0 $prescale_arr($curr_horz,$curr_vert,0)
					
					# Get total samples by multiplying sample count with data width and 2^(prescale + 1)
					set curr_tot_samp0 [expr wide($curr_samp0) * (wide($data_width) << (1+$curr_prescale0)) ]
					
					# Calculate BER for ut-sign of 0
					set ber0 [expr double($curr_err0)/double($curr_tot_samp0) ]
					
					set curr_err1  $error_count_arr($curr_horz,$curr_vert,1)
					set curr_samp1 $sample_count_arr($curr_horz,$curr_vert,1)
					set curr_prescale1 $prescale_arr($curr_horz,$curr_vert,1)
					
					# Get total samples by multiplying sample count with data width and 2^(prescale + 1)
					set curr_tot_samp1 [expr wide($curr_samp1) * (wide($data_width) << (1+$curr_prescale1)) ]
					
					# Calculate BER for ut-sign of 1
					set ber1 [expr double($curr_err1)/double($curr_tot_samp1) ]
					
					#puts "$curr_err0,$curr_samp0,$curr_prescale0,$curr_tot_samp0"
					
					# Calculate final BER
					set ber [format "%.2E" [expr ($ber0 + $ber1)/2]]
					
					if {$ber == 0} {
						set ber [expr 1/(double($curr_tot_samp0) + double($curr_tot_samp1))]
					}

				} else {
					#puts $f_csv "Error: X:$curr_horz, Y:$curr_vert data points do not exist or incomplete (not both UT signs present)!"
				}
			}
			
			
			puts $f_csv "$iter,NA,NA,NA,NA,$curr_vert,$curr_horz,NA,NA,NA,$ber"
			
			set iter [expr $iter + 1]

		}
	}
	
	close $f_csv
}

# #####################################################################################################
# Function: es_host_plot_ascii_eye
# Description: Given arrays containing eye scan data, generates a text file containing pass/fail ascii eye
#
# Parameters:
# 	f_out: file handle for output file
# 	lpm_mode: equalizer mode 
# 	horz_arr_a: reference to array populated with horizontal offset values
# 	vert_arr_a: reference to array populated with vertical offset values
# 	utsign_arr_a: reference to array populated with ut-sign values
# 	sample_count_arr_a: reference to array populated with sample count values
# 	error_count_arr_a: reference to array populated with error count values
#
# Returns: none
# #####################################################################################################
proc es_host_plot_ascii_eye {f_out lpm_mode horz_arr_a vert_arr_a utsign_arr_a sample_count_arr_a error_count_arr_a} {

	upvar $horz_arr_a horz_arr
	upvar $vert_arr_a vert_arr
	upvar $utsign_arr_a utsign_arr
	upvar $sample_count_arr_a sample_count_arr
	upvar $error_count_arr_a error_count_arr
	
	foreach curr_col [lsort -integer [array names vert_arr]] {
		foreach curr_row [lsort -integer [array names horz_arr]] {
			if {$lpm_mode == 1} {
				# In LPM mode, only process data with ut-sign of 0
				if {[info exists error_count_arr($curr_row,$curr_col,0)] == 1} {
					if {$error_count_arr($curr_row,$curr_col,0) == 0} {
						set curr_pixel " "
					} else {
						set curr_pixel "*"
					}
				} else {
					puts $f_out "Error: X:$curr_row, Y:$curr_col, UT:0 data point does not exist!"
				}
			} else {
				# In DFE mode, average BER for ut-sign 0 and 1
				if {[info exists error_count_arr($curr_row,$curr_col,0)] == 1 && [info exists error_count_arr($curr_row,$curr_col,1)] == 1} {
					if {$error_count_arr($curr_row,$curr_col,0) == 0 && $error_count_arr($curr_row,$curr_col,1) == 0} {
						set curr_pixel " "
					} else {
						set curr_pixel "*"
					}
				} else {
					puts $f_out "Error: X:$curr_row, Y:$curr_col data points do not exist or incomplete (not both UT signs present)!"
				}
			}
			
			if {[info exists curr_pixel] == 1} {
				puts -nonewline $f_out $curr_pixel
				puts -nonewline $curr_pixel
			}
		}
		puts $f_out ""
		puts ""
	}
}

proc plot_eye {in_file} {
	# Eye scan results will be populated into following arrays
	array set horz_arr {}
	array set vert_arr {} 
	array set utsign_arr {}
	array set sample_count_arr {}
	array set error_count_arr {}
	array set prescale_arr {}
	
	set f_in [open $in_file r]
	
	# Ignore the first comment line
	gets $f_in line
	
	# Get eye scan configuration
	gets $f_in line
	set fields [split $line ":"]
	set lpm_mode      [lindex $fields 0]
	set vertStepSize  [lindex $fields 2]
	set maxPrescale   [lindex $fields 3]
	set maxHorzOffset [lindex $fields 4]
	set dataWidth     [lindex $fields 5]
	
	set csvOutputFile   "$in_file.csv"
	set asciiOutputFile "$in_file.ascii"
	
	# Generate raw dump and ess. Extract data into arrays.
	es_host_process_dump $in_file horz_arr vert_arr utsign_arr sample_count_arr error_count_arr prescale_arr $dataWidth
    
	# Generate ASCII output
	set f_out [open $asciiOutputFile w]
	es_host_plot_ascii_eye $f_out $lpm_mode horz_arr vert_arr utsign_arr sample_count_arr error_count_arr
				
	# Generate CSV for ibertplotter
	es_host_gen_csv $csvOutputFile lpm_mode $vertStepSize horz_arr vert_arr utsign_arr sample_count_arr error_count_arr prescale_arr $dataWidth $maxHorzOffset
}

set input_file [lindex $argv 0]
plot_eye $input_file

def _checkOverride(filePath):
    import re
    overrideLines = []
    
    file = open(filePath, "r")
    overridePattern = re.compile("\s*(Override.*?)\(.*?self.*?\)\s*;\s*$")
    for line in file:
        match = re.match(overridePattern, line)
        if match is None:
            continue
        
        override = match.group(1)
        if override in overrideLines:
            print "ERROR: File %s contain duplicate overriding (%s)" % (filePath, override)
            return False
            
        overrideLines.append(override)
    
    file.close()
    return True

def checkFile(filePath):
    # More checks may be added in the future
    return _checkOverride(filePath)

def check(path):
    import os
    errorFound = False
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('.c'):
                if not checkFile("%s/%s" % (root, file)):
                    errorFound = True
                    
    if errorFound:
        return False
    
    return True

if __name__ == '__main__':
    folders = []
    import getopt
    import sys
    import os
    
    def printUsage(appName):
        print ("Usage: %s [--help] [--folders=<folders separated by comma>" % (appName))
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], None, ["help", "folders="])
    except getopt.GetoptError:
        printUsage(sys.argv[0])
        sys.exit(1)
    for opt, arg in opts:
        if opt == '--help':
            printUsage(sys.argv[0])
            sys.exit(0)
        elif opt == "--folders":
            folders = arg.split(",")
    
    if len(folders) == 0:
        folders.append(".") 
        
    print "Will check classes in following folders:"
    for folder in folders:
        if os.path.exists(folder):
            print "- " + folder
        else:
            print "- [Ignore]: " + folder
        
    issueFound = False
    for folder in folders:
        if not check(folder):
            issueFound = True
            
    if not issueFound:
        print "No issues are found"
        sys.exit(0)
    
    sys.exit(1)
    

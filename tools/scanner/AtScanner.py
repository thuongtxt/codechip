#!/usr/bin/python

from os import path
import sys
import re
import Error

CLASS_DECL      = '^typedef[\s]+struct[\s]+[A-Za-z][A-Za-z0-9]+'
CLASS_DECL_END  = '^[\s]+}[\s*]*[A-Za-z][A-Za-z0-9]+[\s*]*;'
CLASS_VAR_DECL  = '^[\s]+[A-Za-z][A-Za-z0-9]+[\s\*]+[A-Za-z][A-Za-z0-9]+'

CLOSE_PREPROCESSOR_REGEX = '^#include[\s]+\"[a-zA-Z][a-zA-Z0-9]+\.h\"'
FAR_PREPROCESSOR_REGEX = '^#include[\s]+\"[\.\/]+[a-zA-Z0-9\/]+'
CLASS_CONTRUSTOR = '^[a-zA-Z0-9\s]+ObjectInit\('

""" 
A AtScanner can help to scan and parser all data structure of an ATSDK class.
User can use this class in recursive calling to traverse whole class hierarchy.
"""
class AtScanner:
    def __init__(self, inputFile):
        self.sourceFile = inputFile
        self.representationFile = None
        self.recovered = False
        self.internalVariables = []
        self.closeIncludedHeaders = []
        self.openIncludedHeaders = []
    
    def getName(self):
        baseFile = path.basename(self.sourceFile);
        return re.split('\.', baseFile)[0]
    
    def getPrettyName(self):
        return ''.join(("[", self.getName(), "]"))

    def getPath(self):
        return path.dirname(self.sourceFile);
    
    def getRepresentationFile(self):
        if self.representationFile != None:
            return self.representationFile
        
        classPath = self.getPath()
        className = self.getName()
        
        filePrefix = ''.join((classPath, '/', className))
    
        classHeaderFile   = ''.join((filePrefix, ".h"))
        classInternalFile = ''.join((filePrefix, "Internal.h"))
        classFiles = self.sourceFile, classInternalFile, classHeaderFile
    
        for file in classFiles:
            if self.fileIsContainClassRepresentation(file):
                self.representationFile = file
                return file
        
        return None
    
    def getRepresentationFileFromIncludedHeaders(self):
        if self.representationFile != None:
            return self.representationFile
        
        filePrefix = ''.join((self.getPath(), '/'))
        
        ''' Cycle1: finding class declaration in closest headers '''
        for file in self.getClosestPreprocessorHeaders():
            file = ''.join((filePrefix, file))
            
            if self.fileIsContainClassRepresentation(file):
                self.representationFile = file
                return file

        ''' Cycle2: finding class declaration in further headers '''
        for file in self.getFurtherPreprocessorHeaders():
            file = ''.join((filePrefix, file))
            
            if self.fileIsContainClassRepresentation(file):
                self.representationFile = file
                return file
            
        return None
    
    def getSourceFile(self):
        return self.sourceFile

    def fileIsContainClassRepresentation(self, filepath):
        if not path.exists(filepath):
            return False

        file = open(filepath, 'r')
        for line in file:
            if self.isStartingClassDeclaration(line):
                file.close()
                return True
        
        file.close()
        return False
    
    def getClassDeclaration(self):
        return re.compile(''.join(('^typedef[\s]+struct[\s]+t', self.getName(), '[^a-zA-Z0-9]')))
    
    def isStartingClassDeclaration(self, line):
        if re.match(self.getClassDeclaration(), line) and not re.findall("\*", line):
            return True
        return False

    def getSuper(self):
        presentFile = self.getRepresentationFile()
        if presentFile == None:
            Error.warning(Error.getString(Error.ERROR_NO_REPRESENTATION, self.getPrettyName()))
            return None
        
        isClassDecl = False

        file = open(presentFile, 'r')
        for line in file:
            if self.isStartingClassDeclaration(line):
                isClassDecl = True
                
            if not isClassDecl:
                continue
            
            if re.findall('super;', line):
                super = re.split('[\s]+', line)[1]
                file.close()
                return super[1:]
        file.close()
        return None
        
    def recoverInternalVariables(self):
        presentFile = self.getRepresentationFile()
        if presentFile == None:
            return False
        
        isClassDecl = 0

        file = open(presentFile, 'r')
        for line in file:
            if self.isStartingClassDeclaration(line):
                isClassDecl = 1
            
            if isClassDecl == 0:
                continue

            if re.findall('super;', line) or re.findall('methods;', line):
                if isClassDecl == 1:
                    isClassDecl = 2
                continue
            
            """ After finding 'super' and/or 'methods', starting variable recovery """
            if isClassDecl == 2:
                if re.match(CLASS_VAR_DECL, line):
                    variable = re.split('[\s\*]+', line)
                    variable = re.split('[;\[\]]', variable[2]) #filter array
                    self.internalVariables.append(variable[0])
                
                elif re.findall(self.getName(), line) and re.match(CLASS_DECL_END, line): 
                    break
        
        self.recovered = True
        file.close()
        return True
    
    def hasInternalVariables(self):
        if not self.recovered:
            self.recoverInternalVariables()
            
        if len(self.internalVariables) > 0:
            return True
        else :
            return False

    def getInternalVariables(self):
        if not self.recovered:
            self.recoverInternalVariables()
        return self.internalVariables
    
    ''' Get included header in the same directory with source file. '''
    def getClosestPreprocessorHeaders(self):
        if len(self.closeIncludedHeaders) != 0:
            return self.closeIncludedHeaders
        
        file = open(self.sourceFile, 'r')
        for line in file:
            if re.match(CLOSE_PREPROCESSOR_REGEX, line):
                header = re.split('"', line)[1]
                self.closeIncludedHeaders.append(header)
        
        file.close()
        return self.closeIncludedHeaders

    ''' Get included header not in the same directory with source file. '''
    def getFurtherPreprocessorHeaders(self):
        if len(self.openIncludedHeaders) != 0:
            return self.openIncludedHeaders
        
        file = open(self.sourceFile, 'r')
        for line in file:
            if re.match(FAR_PREPROCESSOR_REGEX, line):
                header = re.split('"', line)[1]
                self.openIncludedHeaders.append(header)
        
        file.close()
        return self.openIncludedHeaders
    
    def isClass(self):
        file = open(self.sourceFile, 'r')
        for line in file:
            if re.match(CLASS_CONTRUSTOR, line):
                file.close()
                return True
        file.close()
        return False
    
    def scanPolicy(self):
        return True #Nothing to be scanned.

#!/usr/bin/python

from os import path
from os import walk
from os import listdir
import sys

def findFileRecursive(rootPath, baseFile):
    for root, dirs, files in walk(rootPath):
        for file in files:
            if file == baseFile:
                filepath = path.join(root, file)
                return filepath
    return None

def findSourceFileRecursive(rootPath):
    sourceFiles = []
    for root, dirs, files in walk(rootPath):
        for file in files:
            if file.endswith('c'):
                sourceFiles.append(file)
    return sourceFiles

def findSourceFiles(localPath):
    sourceFiles = []
    for file in listdir(localPath):
        if file.endswith('c'):
            sourceFiles.append(file)
            
    return sourceFiles

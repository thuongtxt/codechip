#!/usr/bin/python

import sys

''' Definition '''
ERROR_BAD_SOURCE_FILE = "ERROR: Bad C source file."
ERROR_BAD_DIRECTORY = "ERROR: Bad directory."
ERROR_NOT_SERIALIZED_OBJECT = "ERROR: Class is not serialized."
ERROR_NOT_SERIALIZED_VAR = "ERROR: Variable is not serialized."
ERROR_NO_FILE_FOUND = "ERROR: There is no such file found."
ERROR_NO_REPRESENTATION = "ERROR: Cannot find the class representation."

WARN_NO_SUPERCLASS = "WARNING: This class has no superclass."

_hasProblems = False

def getString(errorCode, substring):
    return ''.join((errorCode, ' ', substring))

def warning(string):
    global _hasProblems
    _hasProblems = True
    print string
        
def errors(string):
    global _hasProblems
    _hasProblems = True
    print string
    sys.exit(1)

def hasProblem():
    global _hasProblems
    return _hasProblems
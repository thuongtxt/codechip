#!/usr/bin/python

from os import path
import sys
import getopt
import re
from fileinput import filename
from AtSerializeScanner import AtSerializeScanner
import FileUtil
import Error

''' Definition '''
ATOBJECT = 'AtObject'
DEFAULT_ROOTDIR = '../../driver/'

''' Global variable '''
verbose = False
rootdir = DEFAULT_ROOTDIR
exception = True #TODO: Currently let passing current code, but bad policy.

def isDebugging():
    global verbose
    return verbose

""" Scan a class from its source file """
def scanClass(inputFile):
    scanner = AtSerializeScanner(inputFile)
    
    ''' Some source files are standalone, not an object. '''
    if scanner.isClass() == False:
        return scanner
    
    ''' 
    In case, representation file is not predictable, it should be searched
    through included headers placed in the same directory with source file.
    '''
    presentFile = scanner.getRepresentationFile()
    if presentFile == None and exception == True:
        presentFile = scanner.getRepresentationFileFromIncludedHeaders()

    if isDebugging():
        if presentFile != None: mesg = presentFile
        else:                   mesg = 'Not found.\n'
        print '1. Finding representation file', mesg
            
        if presentFile != None:
            vars = scanner.getInternalVariables()
            mesg = ''
            if len(vars) > 0: 
                for var in vars: mesg = ''.join((mesg, " [", var, "]"))
            else: mesg = '(nothing)'
            print '2. Listing variables:', mesg, '\n'

    if presentFile != None:
        scanner.scanPolicy()
    return scanner

def getDecoratedName(object):
    return ''.join(("[", object.getName(), "]"))

def scanClassHierarchy(basefile):
    global rootdir
    
    objectList = []
    
    if isDebugging():
        print '---------------------------------------------------------------------'
    while True:
        inputFile = FileUtil.findFileRecursive(rootdir, basefile)
        if inputFile == None:
            Error.errors(Error.getString(Error.ERROR_NO_FILE_FOUND, basefile))
            
        if isDebugging():
            print 'Scanning file', inputFile

        object = scanClass(inputFile)
        objectList.append(object)

        if object.isClass() == False:
            break
        
        representFile = object.getRepresentationFile()
        if representFile == None:
            Error.warning(Error.getString(Error.ERROR_NO_REPRESENTATION, getDecoratedName(object)))
            break
        
        super = object.getSuper()
        if super == None:
            Error.warning(Error.getString(Error.WARN_NO_SUPERCLASS, getDecoratedName(object)))
            break
        if super == ATOBJECT:
            break
        if super in object.getIgnoredObjects():
            break
        basefile = ''.join((super, '.c'))

    if isDebugging():
        print '---------------------------------------------------------------------\n'
        
    for object in objectList:
        del object

def usage():
    print ('usage: scanner.py [-r <rootdir>] [[-f|--file <filename>]|[-d|--dir <directory>]]')
    print ('       -v                        Verbose')
    print ('       -r|--rootdir=<rootdir>    Root directory')
    print ('       -f|--file=<filename>      C source file (in basename), e.g. AtXyzClass.c')
    print ('       -d|--dir=<directory>      Scanning all C source files located at the specified directory.')
    
def main(argv):
    global verbose
    global rootdir
    file = ''
    warning = ''
    basefile = None
    basedir = None
    try:
       opts, args = getopt.getopt(argv, "hvr:f:d:", ["help", "rootdir=", "file=", "dir="])
    except getopt.GetoptError:
       usage()
       sys.exit(1)
       
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-r", "--rootdir"):
            rootdir = arg
            
        elif opt in ("-f", "--file"):
            basefile = arg
            if not basefile.lower().endswith('.c'):
                Error.errors(Error.getString(Error.ERROR_BAD_SOURCE_FILE, ""))
                
        elif opt in ("-d", "--dir"):
            basedir = arg
            if not path.exists(basedir):
                Error.errors(Error.getString(Error.ERROR_BAD_SOURCE_FILE, ""))
                
        elif opt == '-v':
            verbose = True
    
    if basefile != None:
        scanClassHierarchy(basefile)
        
    if basedir != None:
        sourceFiles = FileUtil.findSourceFileRecursive(basedir)
        for file in sourceFiles: 
            scanClassHierarchy(file)
   
if __name__ == "__main__":
   main(sys.argv[1:])
   sys.exit(1 if Error.hasProblem() else 0)

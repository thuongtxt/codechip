#!/usr/bin/python

import sys
import re
from AtScanner import AtScanner
import Error

SERIALIZE_REGEX = '^static[\s]+void[\s]+Serialize\([a-zA-Z0-9\s]+,[a-zA-Z0-9\s]+\)'

def getPrettyFile(filename):
    return 

class AtSerializeScanner (AtScanner):
    def __init__(self, inputFile):
        AtScanner.__init__(self, inputFile)
        return
    
    ''' Return 0 if there is no Serialize() implementation '''
    def getLineOfSerialize(self):
        with open(AtScanner.getSourceFile(self), 'r') as file:
            for i, line in enumerate(file, 1):
                if re.match(SERIALIZE_REGEX, line):
                    file.close()
                    return i
        file.close()
        return 0
    
    def isSerializedObject(self):
        if self.getLineOfSerialize() != 0:
            return True
        else :
            return False
    
    def isSerializedVariable(self, variable):
        beingSerialize = False
        openBrackets = 0
        closeBrackets = 0
        
        with open(AtScanner.getSourceFile(self), 'r') as file:
            for line in file:
                if re.match(SERIALIZE_REGEX, line):
                    beingSerialize = True
                    
                if beingSerialize == False:
                    continue
                
                if re.findall('{', line): openBrackets += 1
                if re.findall('}', line): closeBrackets += 1
                
                if re.findall(variable, line):
                    return True
                
                if openBrackets > 0 and openBrackets == closeBrackets:
                    file.close()
                    return False #end of serialize()
                    
        return True
    
    def getIgnoredObjects(self):
        return ["AtHal", "AtHalHa", "AtHalDefault", "AtDrp", "Tha60290011Drp"]
    
    """ Override scanning rule. """
    def scanPolicy(self):
        if AtScanner.hasInternalVariables(self) == False:
            return True
        if self.getName() in self.getIgnoredObjects():
            return True
        
        prettyFile = ''.join(('<', AtScanner.getSourceFile(self), '>'))

        if not self.isSerializedObject():
            Error.warning(Error.getString(Error.ERROR_NOT_SERIALIZED_OBJECT, prettyFile))
            return False
        
        policyState = True
        varList = AtScanner.getInternalVariables(self)
        for var in varList:
            prettyVar = ''.join(("[", AtScanner.getName(self), '::', var, "]"))
            if not self.isSerializedVariable(var):
                Error.warning(Error.getString(Error.ERROR_NOT_SERIALIZED_VAR, prettyVar))
                policyState = False
        
        return policyState

ATSDK_HOME = .

include ${ATSDK_HOME}/Rules.Make
include ${ATSDK_HOME}/Rules.d
include ${CLI_DIR}/Rules.d
include ${ATSDK_HOME}/debugutil/Rules.d
include ${ATSDK_HOME}/customers/Rules.d

.PHONY: arriveso

AT_SDK_SO_LIB = $(ATSDK_HOME)/libarrivesdk.so
AT_SDK_SO_LIB_ALL_OBJS = $(DRIVER_SRC_OBJ)       \
						 $(PLATFORM_SRC_OBJ)     \
						 $(UTIL_SRC_OBJ)         \
						 $(CLI_ALL_OBJS)         \
						 ${AT_CUSTOMERS_SRC_OBJ} \
						 ${DEBUG_UTIL_SRC_OBJ}   \
						 $(AT_COMPONENTS_SRC_OBJ)
AT_SDK_SO_LIB_CFLAGS = ${AT_CFLAGS} ${CLI_CFLAGS} $(DEBUG_UTIL_FLAGS) ${AT_COMPONENTS_FLAGS}
arriveso: cleanarriveso
arriveso: ${AT_SDK_SO_LIB}
${AT_SDK_SO_LIB}: CFLAGS := $(AT_SDK_SO_LIB_CFLAGS) -fPIC
${AT_SDK_SO_LIB}: ${AT_SDK_SO_LIB_ALL_OBJS}
	$(LD) -G $^ -o $@
	$(STRIP) $@

cleanarriveso:
	rm -f $(AT_SDK_SO_LIB_ALL_OBJS) $(AT_SDK_SO_LIB)

clean: cleanarriveso

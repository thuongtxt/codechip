#include <stdio.h>
#include "header_stub.hpp"

bool func();

bool func()
{
    //note seems that there are 2 levels of indirection in the existing code?
    //because here it works just fine. May be a compiler bug also.
    Foo *ptrfoo = Foo::getInstance();
    return ptrfoo->init();
}

int main(int argc, char *argv[])
{

    //try to create an instance then pass it to the mock function
    Foo foo;
    Foo_Mock::getInstance_ExpectAndReturn (&foo);
    Foo_Mock::init_ExpectAndReturn (true);

    bool result = func();

    printf("init result %d\n", result);

  return 0;
}

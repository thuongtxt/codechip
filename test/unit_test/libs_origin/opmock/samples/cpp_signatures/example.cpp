#include <stdio.h>

int func1(int t1, int t2=0, int t3=0)
{
	return 2;
}


/* pas légal d'avoir cette surcharge avec
les paramètres par défaut (ambiguité sur la version à appeler) 
*/
/*int func1(int t1)
{
	return 3;
}*/

//voir surcharge de même nom, même nb de paramètres, mais des noms de paramètres différents
//c'est aussi illégal : les noms de paramètres sont transparents,
//ce sont les types qui déterminent la signature
/*int func1(int t1, int t2=0, int x3=0)
{
	return 3;
}*/


// pas légal non plus :
// changer le type des paramètres avec valeur par défaut dans une surcharge
//ce qui signifie que je dois vérifier les types des paramètres "pas par défaut"
//pour différentier 2 versions de la fonction
/*
int func1(int t1, float t2=0, int t3=0)
{
	return 2;
}
*/

//légal car paramètres sans défaut a un type !=
int func1(char * t1, float t2=0, int t3=0)
{
	return 2;
}

// pas légal : la signature d'inclut pas la valeur de retour?
//ce qui signifie que je n'ai pas besoin de tester la valeur de retour
/*float func1(char * t1, float t2=0, int t3=0)
{
	return 5;
}*/

//donc : j'ai une fonction avec des parametres avec valeur par defaut
//chercher la fonction:
//	-de même nom
//	-ayant les mêmes types pour les paramètres "normaux" (ce qui suppose donc que chaque paramètre soit marqué comme
// ayant ou pas une valeur par défaut. A sauver lors du parsing). Ou alors je sais que j'ai N param dont X avec valeur,
// j'ai donc N-X paramètres sans valeur. Je dois comparer les types de ces N-X paramètres à chaque fois.
//	-ayant le maximum de paramètres (non valeur+valeur)

// une fois trouvé cette fonction avec le maximum de paramètres avec valeur par défaut : la marquer comme normale (mettre a 0
// le nb de paramètres avec valeur par défaut). C'est la seule que je vais garder pour la génération de code
//	-puis chercher ds la liste toutes les fonctions de même nom, avec paramètres valeurs, avec paramètres non valeurs idem,
// et toutes les enlever de la liste?
// il va me rester une à enlever : meme nom, mais seulement les paramètres sans valeur (de même type)


//ds mon cas fonction présente b3 fois ds xml


int main()
{
	printf("appel func1 %d\n", func1(1,2,3));
	printf("appel func1 %d\n", func1(1));

return 0;
}

#ifndef FIZZBUZZ_TEST_H_
#define FIZZBUZZ_TEST_H_

void test_push_pop_stack();
void test_push_pop_stack2();
void test_push_pop_stack3();
void test_push_pop_stack4();
void test_verify();
void test_verify_with_matcher_cstr();
void test_verify_with_matcher_int();
void test_verify_with_matcher_float();
void test_verify_with_matcher_custom();

#endif

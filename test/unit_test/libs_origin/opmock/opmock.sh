#!/bin/bash

#set -x
# main script to run opmock code generators
# this will first run SWIG to get an XML AST for a header file,
# then the opmock jar file to generate the actual mocks.
# ./opmock.sh --input-file /home/pascal/dev/essai_mycmock/chantier1/deps.h 
# --output-path $MOCKHOME/dev/essai_mycmock/chantier1/ --use-cpp no

START_DATE=`date +%s`

SCRIPT_DIR=$(dirname $0)
SWIG=swig
OPMOCK_JAR=$SCRIPT_DIR/opmock.jar
MOCKHOME=./tmp/cfiles/

INPUT_FILE=""
OUTPUT_PATH=""
HEADER_PREFIX=""
HEADER=""
USE_CPP="no"
USE_RECURSE="no"
NO_MOCK="no"
SKIP_FUNCT=""
KEEP_ONLY=""
USE_IMPL="no"
VERBOSE_MODE="yes"

function verbose_echo() {
	if [ "$VERBOSE_MODE" = "yes" ]; then
		echo $1
	fi
}

# create MOCKHOME
mkdir -p $MOCKHOME

# analyze parameters
NB_PARAMS=$#
MAX_PARAMS=$(($NB_PARAMS - 1))

for (( i = 0 ; i < $MAX_PARAMS ; i++ ))
do
    oneParam=$1
	case "${oneParam}" in
    "--input-file") 
		shift
		INPUT_FILE=$1
	    shift
		;;
    "--output-path")
	    shift
	    OUTPUT_PATH=$1
	    shift
        ;;
    "--header-prefix")
	    shift
	    HEADER_PREFIX="--header-prefix ${1}"
	    shift
		;;
    "--use-cpp" )
	   shift
	   USE_CPP=$1
	   shift
		;;
    "--no-mock" )
	   shift
	   NO_MOCK=$1
	   shift
	   	;;
    "--skip-funct" )
	   shift
	   SKIP_FUNCT="--skip-funct ${1}"
	   shift
		;;
    "--keep-only" )
	   shift
	   KEEP_ONLY="--keep-only ${1}"
	   shift
		;;
    "--use-impl" )
	  shift
	  USE_IMPL=$1
	  shift
	   ;;
	"--silent")
	  VERBOSE_MODE="no" 
	   ;;
	*) ;;
	esac
done

# check mandatory parameters
if [ "$INPUT_FILE" = "" ]; then
    echo "ERROR : bad or missing --input-file. Please check your options."
    exit 1
fi

if [ "$OUTPUT_PATH" = "" ]; then
    echo "ERROR : bad or missing --output-path. Please check your options."
    exit 1
fi

HEADER=$(basename $INPUT_FILE)

if [ "$USE_CPP" = "yes" ]; then
    SWIG_CPP="-c++"
fi

if [ "$USE_RECURSE" = "yes" ]; then
    SWIG_RECURSE="-c++"
fi

# use the input filename as the output file name for swig
# cut the extension
swigfile=$(basename $INPUT_FILE)
swigfile=${swigfile%.*}


# call SWIG to the xml AST
verbose_echo "Calling SWIG to generate AST..."
SWIG_ADD_OPTION="$SWIG_FEATURES -DOPMOCK_ACCESS"
SWIG_FEATURES=''
export SWIG_FEATURES
#verbose_echo "$SWIG -includeall -ignoremissing -module module $SWIG_CPP -xmlout $MOCKHOME/${swigfile}.xml $INPUT_FILE"
echo $SWIG -includeall -ignoremissing -module module $SWIG_CPP -o $MOCKHOME/swigfile -xmlout $MOCKHOME/${swigfile}.xml $INPUT_FILE
$SWIG $SWIG_ADD_OPTION -includeall -ignoremissing -module module $SWIG_CPP -o $MOCKHOME/swigfile -xmlout $MOCKHOME/${swigfile}.xml $INPUT_FILE
verbose_echo "Done."

# now call opmock to generate the headers and c/cpp files
verbose_echo "Calling Opmock to generate the mocks..."
echo java -jar $OPMOCK_JAR --input-file $MOCKHOME/${swigfile}.xml --output-path $OUTPUT_PATH \
--header $HEADER --use-cpp $USE_CPP $HEADER_PREFIX --no-mock $NO_MOCK $SKIP_FUNCT $KEEP_ONLY --use-impl $USE_IMPL

java -jar $OPMOCK_JAR --input-file $MOCKHOME/${swigfile}.xml --output-path $OUTPUT_PATH \
    --header $HEADER --use-cpp $USE_CPP $HEADER_PREFIX --no-mock $NO_MOCK $SKIP_FUNCT $KEEP_ONLY --use-impl $USE_IMPL
verbose_echo "Done."

END_DATE=`date +%s`
DELTA_TIME="$(expr $END_DATE - $START_DATE)"

verbose_echo "Time to generate the mocks : $DELTA_TIME second(s)."



/* AUTOGENERATED FILE. DO NOT EDIT. */

//=======Test Runner Used To Run Each Test Below=====
#define RUN_TEST(TestFunc, TestLineNum) \
{ \
  Unity.CurrentTestName = #TestFunc; \
  Unity.CurrentTestLineNumber = TestLineNum; \
  Unity.NumberOfTests++; \
  if (TEST_PROTECT()) \
  { \
      CMock_Init(); \
      setUp(); \
      TestFunc(); \
      CMock_Verify(); \
  } \
  CMock_Destroy(); \
  if (TEST_PROTECT() && !TEST_IS_IGNORED) \
  { \
    tearDown(); \
  } \
  UnityConcludeTest(); \
}

//=======Automagically Detected Files To Include=====
#include "unity.h"
#include "cmock.h"
#include "Types.h"
#include <setjmp.h>
#include <stdio.h>
#include "MockAdcModel.h"
#include "MockAdcHardware.h"
#include "MockUsartModel.h"

//=======External Functions This Runner Calls=====
extern void setUp(void);
extern void tearDown(void);
extern void testInitShouldCallHardwareInit(void);
extern void testRunShouldNotDoAnythingIfItIsNotTime(void);
extern void testRunShouldNotPassAdcResultToModelIfSampleIsNotComplete(void);
extern void testRunShouldGetLatestSampleFromAdcAndPassItToModelAndStartNewConversionWhenItIsTime(void);
extern void testJustHereToTest_Should_ProperlyPassAStructAndVerifyIt(void);
extern void test_AdcConductor_AlsoHereToTest_Should_ProperlyReturnAStructAsExpected1(void);
extern void test_AdcConductor_AlsoHereToTest_Should_ProperlyReturnAStructAsExpected2(void);
extern void test_AdcConductor_YetAnotherTest_Should_VerifyThatPointersToStructsAreTestable(void);


//=======Mock Management=====
static void CMock_Init(void)
{
  MockAdcModel_Init();
  MockAdcHardware_Init();
  MockUsartModel_Init();
}
static void CMock_Verify(void)
{
  MockAdcModel_Verify();
  MockAdcHardware_Verify();
  MockUsartModel_Verify();
}
static void CMock_Destroy(void)
{
  MockAdcModel_Destroy();
  MockAdcHardware_Destroy();
  MockUsartModel_Destroy();
}

//=======Test Reset Option=====
void resetTest()
{
  CMock_Verify();
  CMock_Destroy();
  tearDown();
  CMock_Init();
  setUp();
}


//=======MAIN=====
int main(void)
{
  Unity.TestFile = "test.bk/TestAdcConductor.c";
  UnityBegin();
  RUN_TEST(testInitShouldCallHardwareInit, 18);
  RUN_TEST(testRunShouldNotDoAnythingIfItIsNotTime, 24);
  RUN_TEST(testRunShouldNotPassAdcResultToModelIfSampleIsNotComplete, 31);
  RUN_TEST(testRunShouldGetLatestSampleFromAdcAndPassItToModelAndStartNewConversionWhenItIsTime, 39);
  RUN_TEST(testJustHereToTest_Should_ProperlyPassAStructAndVerifyIt, 50);
  RUN_TEST(test_AdcConductor_AlsoHereToTest_Should_ProperlyReturnAStructAsExpected1, 83);
  RUN_TEST(test_AdcConductor_AlsoHereToTest_Should_ProperlyReturnAStructAsExpected2, 94);
  RUN_TEST(test_AdcConductor_YetAnotherTest_Should_VerifyThatPointersToStructsAreTestable, 105);

  return (UnityEnd());
}

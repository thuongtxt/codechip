/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCKTIMERMODEL_H
#define _MOCKTIMERMODEL_H

#include "Types.h"
#include "TimerModel.h"

void MockTimerModel_Init(void);
void MockTimerModel_Destroy(void);
void MockTimerModel_Verify(void);




#define TimerModel_UpdateTime_Expect(systemTime) TimerModel_UpdateTime_CMockExpect(__LINE__, systemTime)
void TimerModel_UpdateTime_CMockExpect(UNITY_LINE_TYPE cmock_line, uint32 systemTime);

#endif

/* AUTOGENERATED FILE. DO NOT EDIT. */
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include "unity.h"
#include "cmock.h"
#include "MockTimerModel.h"

typedef struct _CMOCK_TimerModel_UpdateTime_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  uint32 Expected_systemTime;

} CMOCK_TimerModel_UpdateTime_CALL_INSTANCE;

static struct MockTimerModelInstance
{
  CMOCK_MEM_INDEX_TYPE TimerModel_UpdateTime_CallInstance;
} Mock;

extern jmp_buf AbortFrame;

void MockTimerModel_Verify(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.TimerModel_UpdateTime_CallInstance, cmock_line, "Function 'TimerModel_UpdateTime' called less times than expected.");
}

void MockTimerModel_Init(void)
{
  MockTimerModel_Destroy();
}

void MockTimerModel_Destroy(void)
{
  CMock_Guts_MemFreeAll();
  memset(&Mock, 0, sizeof(Mock));
}

void TimerModel_UpdateTime(uint32 systemTime)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_TimerModel_UpdateTime_CALL_INSTANCE* cmock_call_instance = (CMOCK_TimerModel_UpdateTime_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.TimerModel_UpdateTime_CallInstance);
  Mock.TimerModel_UpdateTime_CallInstance = CMock_Guts_MemNext(Mock.TimerModel_UpdateTime_CallInstance);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, "Function 'TimerModel_UpdateTime' called more times than expected.");
  cmock_line = cmock_call_instance->LineNumber;
  UNITY_TEST_ASSERT_EQUAL_HEX32(cmock_call_instance->Expected_systemTime, systemTime, cmock_line, "Function 'TimerModel_UpdateTime' called with unexpected value for argument 'systemTime'.");
}

void CMockExpectParameters_TimerModel_UpdateTime(CMOCK_TimerModel_UpdateTime_CALL_INSTANCE* cmock_call_instance, uint32 systemTime)
{
  cmock_call_instance->Expected_systemTime = systemTime;
}

void TimerModel_UpdateTime_CMockExpect(UNITY_LINE_TYPE cmock_line, uint32 systemTime)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_TimerModel_UpdateTime_CALL_INSTANCE));
  CMOCK_TimerModel_UpdateTime_CALL_INSTANCE* cmock_call_instance = (CMOCK_TimerModel_UpdateTime_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, "CMock has run out of memory. Please allocate more.");
  Mock.TimerModel_UpdateTime_CallInstance = CMock_Guts_MemChain(Mock.TimerModel_UpdateTime_CallInstance, cmock_guts_index);
  cmock_call_instance->LineNumber = cmock_line;
  CMockExpectParameters_TimerModel_UpdateTime(cmock_call_instance, systemTime);
}


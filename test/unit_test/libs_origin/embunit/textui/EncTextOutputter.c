/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : EncTestOutputter.c
 *
 * Created Date: Dec 15, 2011
 *
 * Description : This file contain implementation for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "stdio.h"
#include "EncTextOutputter.h"
#include "atclib.h"

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Functions --------------------------------------*/
void EncTextOutputter_printHeader(OutputterRef self)
    {
    }

void EncTextOutputter_printStartTest(OutputterRef self,TestRef test)
    {
    AtPrintc(cSevNeutral,"\n------------------ START SUITE: %s ------------------\n",Test_name(test));
    }

void EncTextOutputter_printEndTest(OutputterRef self,TestRef test)
    {
    AtPrintc(cSevNeutral,"------------------ END SUITE: %s ------------------\n\n",Test_name(test));
    }

void EncTextOutputter_printSuccessful(OutputterRef self, TestRef test, int runCount)
    {
    AtPrintc(cSevInfo,"[%03d] OK ", runCount);
    AtPrintc(cSevNeutral,"%s\n", Test_name(test));
    }

void EncTextOutputter_printFailure(OutputterRef self,TestRef test,char *msg,int line,char *file,int runCount)
    {
    AtPrintc(cSevCritical,"[%03d] FAIL ", runCount);
    AtPrintc(cSevNeutral,"%s\n", Test_name(test));
    AtPrintc(cSevWarning,"    ===> REASON: (%s %d) %s\n", file, line, msg);
    }

void EncTextOutputter_printStatistics(OutputterRef self,TestResultRef result)
    {
    if (result->failureCount)
        {
        AtPrintc(cSevMajor,"\nSUMMARY: run %d, failures %d\n",result->runCount,result->failureCount);
        }
    else
        {
        AtPrintc(cSevDebug,"\nSUMMARY run %d, no failures\n", result->runCount);
        }
    }


static const OutputterImplement cOutputterImplement = {
    (OutputterPrintHeaderFunction)      EncTextOutputter_printHeader,
    (OutputterPrintStartTestFunction)   EncTextOutputter_printStartTest,
    (OutputterPrintEndTestFunction)     EncTextOutputter_printEndTest,
    (OutputterPrintSuccessfulFunction)  EncTextOutputter_printSuccessful,
    (OutputterPrintFailureFunction)     EncTextOutputter_printFailure,
    (OutputterPrintStatisticsFunction)  EncTextOutputter_printStatistics,
    };

static const Outputter cOutputter = {
    (OutputterImplementRef)&cOutputterImplement,
    };

OutputterRef EncTextOutputter_outputter(void)
    {
    return (OutputterRef)&cOutputter;
    }

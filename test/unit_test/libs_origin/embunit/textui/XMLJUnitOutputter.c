/*
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2003 Embedded Unit Project
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies
 * of the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written
 * authorization of the copyright holder.
 *
 * $Id: XMLJUnitOutputter.c,v 1.6 2003/09/26 16:32:01 arms22 Exp $
 */
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "XMLJUnitOutputter.h"

#include "xmloutputname.h"

static char *stylesheet_;
static char *name_;
static FILE * pFile = NULL;
fpos_t position;


void XMLJUnitOutputter_printHeader(OutputterRef self)
    {
    static char outputFileName[50];

    sprintf(outputFileName,
            REPORT_PREFIX "%u" ".xml",
            AtXmlOutputNextId());

    pFile = fopen(outputFileName, "w+");

    fgetpos (pFile, &position);

    fprintf(pFile, "<?xml version=\"1.0\" standalone='yes' ?>\n");

    if (stylesheet_)
        fprintf(pFile,
                "<?xml-stylesheet type=\"text/xsl\" href=\"%s\" ?>\n",
                stylesheet_);

    if (name_)
        fprintf(pFile, "<testsuites name=\"%s\">\n", name_);
    else
        fprintf(pFile, "<testsuites>\n");
    }

void XMLJUnitOutputter_printStartTest(OutputterRef self,
                                             TestRef test)
    {

    fprintf(pFile, "<testsuite name=\"%s\">\n", Test_name(test));
    }

void XMLJUnitOutputter_printEndTest(OutputterRef self,
                                           TestRef test)
    {
    fprintf(pFile, "</testsuite>\n");
    }

void XMLJUnitOutputter_printSuccessful(OutputterRef self,
                                              TestRef test,
                                              int runCount)
    {
    fprintf(pFile, "    <testcase name=\"%s\">\n", Test_name(test));
    fprintf(pFile, "    </testcase>\n");
    }

void XMLJUnitOutputter_printFailure(OutputterRef self,
                                           TestRef test,
                                           char *msg,
                                           int line,
                                           char *file,
                                           int runCount)
    {
    fprintf(pFile, "    <testcase name=\"%s\">\n", Test_name(test));
    fprintf(pFile, "        <failure message=\"%s\">\n", msg);
    fprintf(pFile, "            File: %s\n",file);
    fprintf(pFile, "            Line: %d\n",line);
    fprintf(pFile, "        </failure>\n");
    fprintf(pFile, "    </testcase>\n");

    }

void XMLJUnitOutputter_printStatisticsToOutput(void)
    {
    char buf[100];

    fsetpos (pFile, &position);

    if (pFile == NULL)
        perror("Error file");
    else
        {
        while (fgets(buf, 100, pFile) != NULL)
            fprintf(stdout, "%s", buf);
        }

    }

void XMLJUnitOutputter_printStatistics(OutputterRef self,
                                              TestResultRef result)
    {
    fprintf(pFile, "</testsuites>\n");
    fclose(pFile);
    }

static const OutputterImplement XMLJUnitOutputterImplement =
{ (OutputterPrintHeaderFunction) XMLJUnitOutputter_printHeader,
        (OutputterPrintStartTestFunction) XMLJUnitOutputter_printStartTest,
        (OutputterPrintEndTestFunction) XMLJUnitOutputter_printEndTest,
        (OutputterPrintSuccessfulFunction) XMLJUnitOutputter_printSuccessful,
        (OutputterPrintFailureFunction) XMLJUnitOutputter_printFailure,
        (OutputterPrintStatisticsFunction) XMLJUnitOutputter_printStatistics, };

static const Outputter XMLJUnitOutputter =
{ (OutputterImplementRef) &XMLJUnitOutputterImplement, };

void XMLJUnitOutputter_setStyleSheet(char *style)
    {
    stylesheet_ = style;
    }

void XMLJUnitOutputter_setName(char *name)
    {
    name_ = name;
    }

OutputterRef XMLJUnitOutputter_outputter()
    {
    return (OutputterRef) &XMLJUnitOutputter;
    }

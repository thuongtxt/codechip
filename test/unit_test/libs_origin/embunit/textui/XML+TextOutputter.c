/*
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2003 Embedded Unit Project
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies
 * of the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written
 * authorization of the copyright holder.
 *
 * $Id: XMLTextOutputter.c,v 1.6 2003/09/26 16:32:01 arms22 Exp $
 */
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "XML+TextOutputter.h"

#include "xmloutputname.h"

static char *stylesheet_;
fpos_t position;

extern void TextOutputter_printHeader(OutputterRef self);
extern void XMLJUnitOutputter_printHeader(OutputterRef self);
static void XMLTextOutputter_printHeader(OutputterRef self)
    {
    TextOutputter_printHeader(self);
    XMLJUnitOutputter_printHeader(self);
    }

extern void XMLJUnitOutputter_printStartTest(OutputterRef self,
        TestRef test);
extern void TextOutputter_printStartTest(OutputterRef self,TestRef test);
static void XMLTextOutputter_printStartTest(OutputterRef self,
                                             TestRef test)
    {
    XMLJUnitOutputter_printStartTest(self, test);
    TextOutputter_printStartTest(self, test);
    }

extern void TextOutputter_printEndTest(OutputterRef self,TestRef test);
extern void XMLJUnitOutputter_printEndTest(OutputterRef self,
        TestRef test);
static void XMLTextOutputter_printEndTest(OutputterRef self,
                                           TestRef test)
    {
    TextOutputter_printEndTest(self, test);
    XMLJUnitOutputter_printEndTest(self, test);
    }

extern void XMLJUnitOutputter_printSuccessful(OutputterRef self,
                                              TestRef test,
                                              int runCount);
static void XMLTextOutputter_printSuccessful(OutputterRef self,
                                              TestRef test,
                                              int runCount)
    {
    /*TextOutputter_printSuccessful(self, test, runCount);*/
    XMLJUnitOutputter_printSuccessful(self, test, runCount);
    }

extern void TextOutputter_printFailure(OutputterRef self,TestRef test,char *msg,int line,char *file,int runCount);
extern void XMLJUnitOutputter_printFailure(OutputterRef self,
                                           TestRef test,
                                           char *msg,
                                           int line,
                                           char *file,
                                           int runCount);
static void XMLTextOutputter_printFailure(OutputterRef self,
                                           TestRef test,
                                           char *msg,
                                           int line,
                                           char *file,
                                           int runCount)
    {
    TextOutputter_printFailure(self, test, msg, line, file, runCount);
    XMLJUnitOutputter_printFailure(self, test, msg, line, file, runCount);
    }

extern void TextOutputter_printStatistics(OutputterRef self,TestResultRef result);
extern void XMLJUnitOutputter_printStatistics(OutputterRef self,
        TestResultRef result);
static void XMLTextOutputter_printStatistics(OutputterRef self,
                                              TestResultRef result)
    {
    TextOutputter_printStatistics(self, result);
    XMLJUnitOutputter_printStatistics(self, result);
    }

static const OutputterImplement XMLTextOutputterImplement =
{ (OutputterPrintHeaderFunction) XMLTextOutputter_printHeader,
        (OutputterPrintStartTestFunction) XMLTextOutputter_printStartTest,
        (OutputterPrintEndTestFunction) XMLTextOutputter_printEndTest,
        (OutputterPrintSuccessfulFunction) XMLTextOutputter_printSuccessful,
        (OutputterPrintFailureFunction) XMLTextOutputter_printFailure,
        (OutputterPrintStatisticsFunction) XMLTextOutputter_printStatistics, };

static const Outputter XMLTextOutputter =
{ (OutputterImplementRef) &XMLTextOutputterImplement, };

void XMLTextOutputter_setStyleSheet(char *style)
    {
    stylesheet_ = style;
    }

OutputterRef XMLTextOutputter_outputter()
    {
    return (OutputterRef) &XMLTextOutputter;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : EncTextOutputter.h
 *
 * Created Date: Dec 15, 2011
 *
 * Description : This file contain definitions for TODO
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#ifndef ENCTEXTOUTPUTTER_H_
#define ENCTEXTOUTPUTTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Outputter.h"

#ifdef __cplusplus
extern "C" {
#endif

OutputterRef EncTextOutputter_outputter(void);

#ifdef  __cplusplus
}
#endif

#endif /* ENCTEXTOUTPUTTER_H_ */

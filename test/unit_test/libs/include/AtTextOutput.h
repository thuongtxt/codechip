/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtTextOutput.h
 * 
 * Created Date: Jul 5, 2014
 *
 * Description : Customize test output
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTEXTOUTPUT_H_
#define _ATTEXTOUTPUT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "Outputter.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
OutputterRef AtTextOutput(void);

void AtTextOutputSilenceOnSuccess(eBool silence);

#endif /* _ATTEXTOUTPUT_H_ */


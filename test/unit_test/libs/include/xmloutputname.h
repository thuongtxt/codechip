/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : unittest lib
 *
 * File        : xmloutputname.h
 *
 * Created Date: Dec-12-2011
 *
 * Description :
 *
 * Notes       :
 *----------------------------------------------------------------------------*/


/*--------------------------- Description -------------------------------------
 *
 *
 *----------------------------------------------------------------------------*/


#ifndef XMLOUTPUTNAME_H_
#define XMLOUTPUTNAME_H_

/*--------------------------- Define -----------------------------------------*/
#define REPORT_PREFIX "unittest_result_"

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Entries ----------------------------------------*/
unsigned int AtXmlOutputNextId(void);

#endif /* XMLOUTPUTNAME_H_ */

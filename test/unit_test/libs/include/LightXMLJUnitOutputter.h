/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest library
 * 
 * File        : LightXMLJUnitOutputter.h
 * 
 * Created Date: Aug 9, 2019
 *
 * Description : Light XML JUnit outputter only records the failed testcases.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef __LIGHTXMLJUNITOUTPUTTER_H__
#define __LIGHTXMLJUNITOUTPUTTER_H__

/*--------------------------- Includes ---------------------------------------*/
#include "Outputter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void LightXMLJUnitOutputter_setStyleSheet(char *style);
void LightXMLJUnitOutputter_setName(char *name);

OutputterRef LightXMLJUnitOutputter_outputter();

#ifdef __cplusplus
}
#endif
#endif /* __LIGHTXMLJUNITOUTPUTTER_H__ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : unittest lib
 *
 * File        : xmloutputname.c
 *
 * Created Date: Dec-12-2011
 *
 * Description :
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "xmloutputname.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Entries -----------------------------------------*/
unsigned int AtXmlOutputNextId(void)
    {
    DIR *dir;
    struct dirent *ent;
    unsigned int retFileId = 0;
    dir = opendir(".");
    if (dir != NULL)
        {

        /* print all the files and directories within directory */
        while ((ent = readdir(dir)) != NULL)
            {
            char *ch = strstr(ent->d_name, REPORT_PREFIX);
            if (ch != NULL)
                {
                unsigned int fileId;
                sscanf(ch, REPORT_PREFIX "%u" ".xml", &fileId);

                if (fileId > retFileId)
                    {
                    retFileId = fileId;
                    }

                }
            }
        closedir(dir);
        }
    else
        {
        /* could not open directory */
        perror("");
        }

    return ++retFileId;
    }

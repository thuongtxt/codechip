/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtTextOutput.c
 *
 * Created Date: Jul 5, 2014
 *
 * Description : Customize test output
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include "atclib.h"
#include "AtTextOutput.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_silenceOnSuccess = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void printHeader(OutputterRef self)
    {
    }

static void printStartTest(OutputterRef self, TestRef test)
    {
    fprintf(stdout, "- %s\n", Test_name(test));
    }

static void printEndTest(OutputterRef self, TestRef test)
    {
    }

static void printSuccessful(OutputterRef self, TestRef test, int runCount)
    {
    if (m_silenceOnSuccess)
        return;

    AtPrintc(cSevNormal, "%u) ", runCount);
    AtPrintc(cSevInfo, "OK ");
    AtPrintc(cSevNormal, "%s\r\n", Test_name(test));
    }

static void printFailure(OutputterRef self,
                         TestRef test,
                         char *msg,
                         int line,
                         char *file,
                         int runCount)
    {
    AtPrintc(cSevNormal, "%u) ", runCount);
    AtPrintc(cSevCritical, "FAIL");
    AtPrintc(cSevNormal, " %s (%s %u) %s\r\n", Test_name(test), file, line, msg);
    }

static void printStatistics(OutputterRef self, TestResultRef result)
    {
    if (result->failureCount)
        AtPrintc(cSevCritical, "\nRun %u tests, failures %u\r\n", result->runCount, result->failureCount);
    else
        AtPrintc(cSevInfo, "\nOK (%u tests)\r\n", result->runCount);
    }

static const OutputterImplement m_implement =
    {
    (OutputterPrintHeaderFunction)     printHeader,
    (OutputterPrintStartTestFunction)  printStartTest,
    (OutputterPrintEndTestFunction)    printEndTest,
    (OutputterPrintSuccessfulFunction) printSuccessful,
    (OutputterPrintFailureFunction)    printFailure,
    (OutputterPrintStatisticsFunction) printStatistics,
    };

static const Outputter m_outputter =
    {
    (OutputterImplementRef) &m_implement,
    };

OutputterRef AtTextOutput(void)
    {
    return (OutputterRef) &m_outputter;
    }

void AtTextOutputSilenceOnSuccess(eBool silence)
    {
    m_silenceOnSuccess = silence;
    }

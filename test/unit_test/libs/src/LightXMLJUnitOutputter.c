/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest library
 *
 * File        : LightXMLJUnitOutputter.c
 *
 * Created Date: Aug 8, 2019
 *
 * Description : Light XML JUnit outputter only records the failed testcases.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "Outputter.h"

#include "xmloutputname.h"

static char buffer[512][256]; /* 512 rows x 256 columns */
static int row_;
static int failure_;
static char *stylesheet_;
static char *name_;
static FILE * pFile = NULL;
fpos_t position;

void LightXMLJUnitOutputter_printHeader(OutputterRef self)
    {
    static char outputFileName[50];

    sprintf(outputFileName,
            REPORT_PREFIX "%u" ".xml",
            AtXmlOutputNextId());

    pFile = fopen(outputFileName, "w+");

    fgetpos (pFile, &position);

    fprintf(pFile, "<?xml version=\"1.0\" standalone='yes' ?>\n");

    if (stylesheet_)
        fprintf(pFile,
                "<?xml-stylesheet type=\"text/xsl\" href=\"%s\" ?>\n",
                stylesheet_);

    if (name_)
        fprintf(pFile, "<testsuites name=\"%s\">\n", name_);
    else
        fprintf(pFile, "<testsuites>\n");
    }

void LightXMLJUnitOutputter_printStartTest(OutputterRef self,
                                             TestRef test)
    {
    row_ = 0;
    sprintf(buffer[row_++], "<testsuite name=\"%s\">\n", Test_name(test));
    failure_ = 0;
    }

void LightXMLJUnitOutputter_printEndTest(OutputterRef self,
                                           TestRef test)
    {
    sprintf(buffer[row_++], "</testsuite>\n");
    if (failure_)
        {
        int i;
        for (i = 0; i < row_; i++)
            fprintf(pFile, "%s", buffer[i]);
        }
    }

void LightXMLJUnitOutputter_printSuccessful(OutputterRef self,
                                              TestRef test,
                                              int runCount)
    {
    /* just do not record it. */
    }

void LightXMLJUnitOutputter_printFailure(OutputterRef self,
                                           TestRef test,
                                           char *msg,
                                           int line,
                                           char *file,
                                           int runCount)
    {
    sprintf(buffer[row_++], "    <testcase name=\"%s\">\n", Test_name(test));
    sprintf(buffer[row_++], "        <failure message=\"%s\">\n", msg);
    sprintf(buffer[row_++], "            File: %s\n",file);
    sprintf(buffer[row_++], "            Line: %d\n",line);
    sprintf(buffer[row_++], "        </failure>\n");
    sprintf(buffer[row_++], "    </testcase>\n");
    failure_ = 1;
    }

void LightXMLJUnitOutputter_printStatistics(OutputterRef self,
                                              TestResultRef result)
    {
    fprintf(pFile, "</testsuites>\n");
    fclose(pFile);
    }

static const OutputterImplement LightXMLJUnitOutputterImplement =
{ (OutputterPrintHeaderFunction) LightXMLJUnitOutputter_printHeader,
        (OutputterPrintStartTestFunction) LightXMLJUnitOutputter_printStartTest,
        (OutputterPrintEndTestFunction) LightXMLJUnitOutputter_printEndTest,
        (OutputterPrintSuccessfulFunction) LightXMLJUnitOutputter_printSuccessful,
        (OutputterPrintFailureFunction) LightXMLJUnitOutputter_printFailure,
        (OutputterPrintStatisticsFunction) LightXMLJUnitOutputter_printStatistics, };

static const Outputter LightXMLJUnitOutputter =
{ (OutputterImplementRef) &LightXMLJUnitOutputterImplement, };

void LightXMLJUnitOutputter_setStyleSheet(char *style)
    {
    stylesheet_ = style;
    }

void LightXMLJUnitOutputter_setName(char *name)
    {
    name_ = name;
    }

OutputterRef LightXMLJUnitOutputter_outputter()
    {
    return (OutputterRef) &LightXMLJUnitOutputter;
    }

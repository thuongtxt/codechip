/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtFrLinkTestRunner.c
 *
 * Created Date: Aug 3, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtIterator.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtHdlcChannel.h"
#include "AtFrLink.h"
#include "AtFrVirtualCircuit.h"
#include "../../../../driver/src/generic/man/AtDriverInternal.h"
#include "AtFrLinkTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxVirtualCircuits (8)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/
extern AtChannelTestRunner AtFrVirtualCircuitTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

/*--------------------------- Implementation ---------------------------------*/
static AtFrLink Link()
    {
    return (AtFrLink)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static AtModuleEncap EncapModule()
    {
    return (AtModuleEncap)AtChannelModuleGet((AtChannel)Link());
    }

static void setUp()
    {
    }

static void tearDown()
    {
    AtChannelInit((AtChannel)Link());
    }

static void testCreateFrVirtualCircuit()
    {
    uint8 i;
    AtFrVirtualCircuit dlciCircuit;

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL((dlciCircuit = AtFrLinkVirtualCircuitCreate(Link(), i)));
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)dlciCircuit));
        }
    }

static void testGetFrVirtualCircuit()
    {
    uint8 i;
    AtFrVirtualCircuit dlciCircuit;
    AtFrLink link = Link();

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL((AtFrLinkVirtualCircuitCreate(Link(), i)))
        TEST_ASSERT_NOT_NULL((dlciCircuit = AtFrLinkVirtualCircuitGet(link, i)))
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)dlciCircuit))
        }
    }

static void testCanDeleteACreatedVirtualCircuit()
    {
    uint32 i;
    AtFrLink link = Link();

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL(AtFrLinkVirtualCircuitCreate(link, i))
        TEST_ASSERT_EQUAL_INT(1, AtFrLinkNumberVirtualCircuitGet(link))

        TEST_ASSERT_EQUAL_INT(cAtOk, AtFrLinkVirtualCircuitDelete(link, i))
        TEST_ASSERT_NULL(AtFrLinkVirtualCircuitGet(link, i))
        TEST_ASSERT_EQUAL_INT(0, AtFrLinkNumberVirtualCircuitGet(link))
        }
    }

static void testCannotCreateFrVirtualCircuitMoreThanOneTimes()
    {
    uint32 i;
    AtFrLink link = Link();

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL(AtFrLinkVirtualCircuitCreate(link, i))
        TEST_ASSERT_EQUAL_INT(i+1, AtFrLinkNumberVirtualCircuitGet(link))
        TEST_ASSERT_NULL(AtFrLinkVirtualCircuitCreate(link, i))
        TEST_ASSERT_EQUAL_INT(i+1, AtFrLinkNumberVirtualCircuitGet(link))
        }
    }

static void testCannotCreateVirtualCircuitFromNULLFrLink()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtFrLinkVirtualCircuitCreate(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotGetAVirtualCircuitIfItHasNotBeenCreated()
    {
    uint32 i;
    for (i = 0; i < cMaxVirtualCircuits; i++)
        TEST_ASSERT_NULL(AtFrLinkVirtualCircuitGet(Link(), i));
    }

static void testFrVirtualCircuitGetFrOrMfr()
    {
    AtFrLink linkGet;
    AtFrLink link = Link();
    AtFrVirtualCircuit virtualCircuit = AtFrLinkVirtualCircuitCreate(link, 0);
    TEST_ASSERT_NOT_NULL(virtualCircuit)
    TEST_ASSERT_NULL(AtFrVirtualCircuitBundleGet(virtualCircuit))

    TEST_ASSERT_NOT_NULL((linkGet = AtFrVirtualCircuitLinkGet(virtualCircuit)))
    TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)linkGet), AtChannelIdGet((AtChannel)link))
    }

static void TestFrVirtualCircuit(AtUnittestRunner self)
    {
    uint16 i;
    AtFrLink link = Link();
    AtModuleEncap encap = EncapModule();

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        AtChannel frVc = (AtChannel)AtFrLinkVirtualCircuitCreate(Link(), i);
        AtUnittestRunner runner = (AtUnittestRunner)AtFrVirtualCircuitTestRunnerNew(frVc, (AtModuleTestRunner)encap);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        AtFrLinkVirtualCircuitDelete(link, i);
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_FrLink)
        {
        new_TestFixture("testCreateFrVirtualCircuit", testCreateFrVirtualCircuit),
        new_TestFixture("testGetFrVirtualCircuit", testGetFrVirtualCircuit),
        new_TestFixture("testCanDeleteACreatedVirtualCircuit", testCanDeleteACreatedVirtualCircuit),
        new_TestFixture("testCannotCreateFrVirtualCircuitMoreThanOneTimes", testCannotCreateFrVirtualCircuitMoreThanOneTimes),
        new_TestFixture("testCannotCreateVirtualCircuitFromNULLFrLink", testCannotCreateVirtualCircuitFromNULLFrLink),
        new_TestFixture("testCannotGetAVirtualCircuitIfItHasNotBeenCreated", testCannotGetAVirtualCircuitIfItHasNotBeenCreated),
        new_TestFixture("testFrVirtualCircuitGetFrOrMfr", testFrVirtualCircuitGetFrOrMfr),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_FrLink_Caller, "TestSuite_FrLink", setUp, tearDown, TestSuite_FrLink);

    return (TestRef)((void *)&TestSuite_FrLink_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    TestFrVirtualCircuit(self);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFrLinkTestRunner);
    }

AtChannelTestRunner AtFrLinkTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtFrLinkTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtFrLinkTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

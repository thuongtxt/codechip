/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtMfrBundleTestRunnerInternal.h
 *
 * Created Date: Aug 15, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMFRBUNDLETESTRUNNERINTERNAL_H_
#define _ATMFRBUNDLETESTRUNNERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtMfrBundleTestRunner
    {
    tAtChannelTestRunner super;
    }tAtMfrBundleTestRunner;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtMfrBundleTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

#ifdef __cplusplus
}
#endif

#endif /* _ATMFRBUNDLETESTRUNNERINTERNAL_H_ */

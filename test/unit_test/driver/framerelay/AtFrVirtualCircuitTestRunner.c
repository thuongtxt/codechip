/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtFrVirtualCircuitTestRunner.c
 *
 * Created Date: Aug 16, 2016 
 *
 * Description : TODO Description
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtEthFlow.h"
#include "AtFrVirtualCircuit.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tAtFrVirtualCircuitTestRunner
    {
    tAtChannelTestRunner super;
    }tAtFrVirtualCircuitTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save Super Implementation */
static const tAtUnittestRunnerMethods * m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule()
    {
    return (AtModuleEth)AtDeviceModuleGet(AtChannelTestRunnerDeviceGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner()), cAtModuleEth);
    }

static AtFrVirtualCircuit FrVirtualCircuit()
    {
    return (AtFrVirtualCircuit)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(AtModuleEthFlowCreate(EthModule(), 0))
    TEST_ASSERT_NOT_NULL(AtModuleEthFlowCreate(EthModule(), 1))
    AtChannelInit((AtChannel)FrVirtualCircuit());
    }

static void tearDown()
    {
    AtFrVirtualCircuitFlowBind(FrVirtualCircuit(), NULL);
    AtModuleEthFlowDelete(EthModule(), 0);
    AtModuleEthFlowDelete(EthModule(), 1);
    }

static void testBindFlow()
    {
    AtFrVirtualCircuit frVc = FrVirtualCircuit();
    AtEthFlow ethFlow = AtModuleEthFlowGet(EthModule(), 0);
    TEST_ASSERT_NOT_NULL(ethFlow)
    TEST_ASSERT_EQUAL_INT(cAtOk, AtFrVirtualCircuitFlowBind(frVc, ethFlow))
    }

static void testGetBoundFlow()
    {
    AtFrVirtualCircuit frVc = FrVirtualCircuit();
    AtEthFlow ethFlowGet;

    AtEthFlow ethFlow = AtModuleEthFlowGet(EthModule(), 0);
    TEST_ASSERT_NOT_NULL(ethFlow)

    ethFlowGet = AtFrVirtualCircuitBoundFlowGet(frVc);
    TEST_ASSERT_NULL(ethFlowGet)

    TEST_ASSERT_EQUAL_INT(cAtOk, AtFrVirtualCircuitFlowBind(frVc, ethFlow))

    ethFlowGet = AtFrVirtualCircuitBoundFlowGet(frVc);
    TEST_ASSERT_NOT_NULL(ethFlow)
    }

static void testUnbindFlow()
    {
    AtFrVirtualCircuit frVc = FrVirtualCircuit();
    AtEthFlow ethFlow = AtModuleEthFlowGet(EthModule(), 0);
    TEST_ASSERT_NOT_NULL(ethFlow)
    TEST_ASSERT_EQUAL_INT(cAtOk, AtFrVirtualCircuitFlowBind(frVc, ethFlow))
    TEST_ASSERT_NOT_NULL(AtFrVirtualCircuitBoundFlowGet(frVc))

    TEST_ASSERT_EQUAL_INT(cAtOk, AtFrVirtualCircuitFlowBind(frVc, NULL))
    TEST_ASSERT_NULL(AtFrVirtualCircuitBoundFlowGet(frVc))
    }

static void testCanNotBindMoreThanOneFlow()
    {
    AtFrVirtualCircuit frVc = FrVirtualCircuit();
    AtEthFlow ethFlow1 = AtModuleEthFlowGet(EthModule(), 0);
    AtEthFlow ethFlow2 = AtModuleEthFlowGet(EthModule(), 1);
    TEST_ASSERT_NOT_NULL(ethFlow1)
    TEST_ASSERT_NOT_NULL(ethFlow2)

    TEST_ASSERT_EQUAL_INT(cAtOk, AtFrVirtualCircuitFlowBind(frVc, ethFlow1))
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(cAtOk != AtFrVirtualCircuitFlowBind(frVc, ethFlow2))
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_FrVirtual)
        {
        new_TestFixture("testBindFlow", testBindFlow),
        new_TestFixture("testGetBoundFlow", testGetBoundFlow),
        new_TestFixture("testUnindFlow", testUnbindFlow),
        new_TestFixture("testGetBoundFlow", testCanNotBindMoreThanOneFlow),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_FrVirtualCircuit_Caller, "TestSuite_FrVirtualCircuit", setUp, tearDown, TestSuite_FrVirtual);

    return (TestRef)((void *)&TestSuite_FrVirtualCircuit_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFrVirtualCircuitTestRunner);
    }

static AtChannelTestRunner AtFrVirtualCircuitTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtFrVirtualCircuitTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtFrVirtualCircuitTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtMfrBundleTestRunner.c
 *
 * Created Date: Aug 3, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtHdlcChannel.h"
#include "AtMfrBundle.h"
#include "AtFrVirtualCircuit.h"
#include "AtMfrBundleTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxVirtualCircuits (8)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtMfrBundle Bundle()
    {
    return (AtMfrBundle)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    AtChannelInit((AtChannel)Bundle());
    }

static void testCreateMfrBundleVirtualCircuit()
	{
    uint8  i;
    AtFrVirtualCircuit dlciCircuit;

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        dlciCircuit = AtMfrBundleVirtualCircuitCreate(Bundle(), i);
        TEST_ASSERT_NOT_NULL(dlciCircuit);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)dlciCircuit));
        }
    }

static void testGetMfrBundleVirtualCircuit()
    {
    uint8 i;
    AtFrVirtualCircuit dlciCircuit;
    AtMfrBundle bundle = Bundle();

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL((AtMfrBundleVirtualCircuitCreate(bundle, i)));
        TEST_ASSERT_NOT_NULL((dlciCircuit = AtMfrBundleVirtualCircuitGet(bundle, i)));
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)dlciCircuit));
        }
    }

static void testCanDeleteACreatedVirtualCircuit()
    {
    uint32 i;

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL(AtMfrBundleVirtualCircuitCreate(Bundle(), i));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtMfrBundleVirtualCircuitDelete(Bundle(), i));
        TEST_ASSERT_NULL(AtMfrBundleVirtualCircuitGet(Bundle(), i));
        }
    }

static void testCannotCreateFrVirtualCircuitMoreThanOneTimes()
    {
    uint32 i;
    AtMfrBundle bundle = Bundle();

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NOT_NULL(AtMfrBundleVirtualCircuitCreate(bundle, i));
        TEST_ASSERT_NULL(AtMfrBundleVirtualCircuitCreate(bundle, i));
        }
    }

static void testCannotCreateVirtualCircuitFromNULLMfrBundle()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtMfrBundleVirtualCircuitCreate(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotGetAVirtualCircuitIfItHasNotBeenCreated()
    {
    uint32 i;

    for (i = 0; i < cMaxVirtualCircuits; i++)
        {
        TEST_ASSERT_NULL(AtMfrBundleVirtualCircuitGet(Bundle(), i));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_MfrBundle)
        {
        new_TestFixture("testCreateMfrBundleVirtualCircuit", testCreateMfrBundleVirtualCircuit),
		new_TestFixture("testGetMfrBundleVirtualCircuit", testGetMfrBundleVirtualCircuit),
        new_TestFixture("testCanDeleteACreatedVirtualCircuit", testCanDeleteACreatedVirtualCircuit),
        new_TestFixture("testCannotCreateFrVirtualCircuitMoreThanOneTimes", testCannotCreateFrVirtualCircuitMoreThanOneTimes),
        new_TestFixture("testCannotCreateVirtualCircuitFromNULLMfrBundle", testCannotCreateVirtualCircuitFromNULLMfrBundle),
        new_TestFixture("testCannotGetAVirtualCircuitIfItHasNotBeenCreated", testCannotGetAVirtualCircuitIfItHasNotBeenCreated),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_MfrBundle_Caller, "TestSuite_MfrBundle", setUp, NULL, TestSuite_MfrBundle);

    return (TestRef)((void *)&TestSuite_MfrBundle_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    /* TODO: [dungta]: Add runner for MFR Virtual Circuits */
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMfrBundleTestRunner);
    }

AtChannelTestRunner AtMfrBundleTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtMfrBundleTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtMfrBundleTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

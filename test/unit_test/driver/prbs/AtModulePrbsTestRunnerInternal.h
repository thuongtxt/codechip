/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtModulePrbsTestRunnerInternal.h
 * 
 * Created Date: Jan 7, 2016
 *
 * Description : PRBS Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPRBSTESTRUNNERINTERNAL_H_
#define _ATMODULEPRBSTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "AtModulePrbsTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePrbsTestRunnerMethods
    {
    AtUnittestRunner (*CreatePrbsEngineTestRunner)(AtModulePrbsTestRunner self, AtPrbsEngine engine);
    eBool (*CanTestDe3Engine)(AtModulePrbsTestRunner self);
    void (*TestVcDe1)(AtModulePrbsTestRunner self);
    void (*TestDe3)(AtModulePrbsTestRunner self);
    }tAtModulePrbsTestRunnerMethods;

typedef struct tAtModulePrbsTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModulePrbsTestRunnerMethods *methods;

    /* Private data */
    uint32 currentEngine;
    }tAtModulePrbsTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModulePrbsTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPRBSTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtModulePrbsTestRunner.c
 *
 * Created Date: Jan 7, 2016
 *
 * Description : PRBS Module test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtTests.h"
#include "AtPrbsEngineTestRunner.h"
#include "AtModulePrbsTestRunnerInternal.h"
#include "AtPdhDe1.h"
#include "../sdh/AtModuleSdhTests.h"
#include "../pdh/AtModulePdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModulePrbsTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePrbs PrbsModule(void)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    return (AtModulePrbs)(AtDeviceModuleGet(device, cAtModulePrbs));
    }

static void testMaxNumberOfPrbsEngineGreaterZero(void)
    {
    TEST_ASSERT(AtModulePrbsMaxNumEngines(PrbsModule()) > 0);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModulePrbs_Fixtures)
        {
        new_TestFixture("testMaxNumberOfPrbsEngineGreaterZero", testMaxNumberOfPrbsEngineGreaterZero)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModulePrbs_Caller, "TestSuite_ModulePrbs", NULL, NULL, TestSuite_ModulePrbs_Fixtures);

    return (TestRef)((void *)&TestSuite_ModulePrbs_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static uint32 NextEngine(AtModulePrbsTestRunner self)
    {
    uint32 nextEngine = self->currentEngine;

    /* Increase for next time */
    self->currentEngine = (self->currentEngine + 1) % AtModulePrbsMaxNumEngines(PrbsModule());
    return nextEngine;
    }

static void ResetEngine(AtModulePrbsTestRunner self)
    {
    self->currentEngine  = 0;
    }

static AtModulePdh PdhModule(AtModulePrbsTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    }

static void TestLiuDe1(AtModulePrbsTestRunner self)
    {
    AtModulePdh pdhModule = PdhModule(self);
    AtModulePrbs prbsModule = PrbsModule();
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(pdhModule);
    AtPrbsEngine engine;
    uint16 de1Id;

    for (de1Id = 0; de1Id < numDe1s; de1Id++)
        {
        AtUnittestRunner runner;
        uint32 engineId = NextEngine(self);
        AtPdhDe1 de1 = AtModulePdhDe1Get(pdhModule, de1Id);

        /* Clear current configure may exist before */
        AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm) == cAtOk);
        AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm) == cAtOk);

        if (engineId == AtModulePrbsMaxNumEngines(prbsModule))
            {
            ResetEngine(self);
            engineId = NextEngine(self);
            }

        /* Create and Enable generate prbs */
        engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, de1);
        AtAssert(engine);
        AtAssert(AtPrbsEngineEnable(engine, cAtTrue) == cAtOk);
        runner = mMethodsGet(self)->CreatePrbsEngineTestRunner(self, AtChannelPrbsEngineGet((AtChannel)de1));
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        /* Clean configure test */
        AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
        AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);
        }
    }

static AtUnittestRunner CreateNxDs0PrbsEngineRunner(AtModulePrbsTestRunner self, AtPdhNxDS0 nxDs0)
    {
    return (AtUnittestRunner)mMethodsGet(self)->CreatePrbsEngineTestRunner(self, AtChannelPrbsEngineGet((AtChannel)nxDs0));
    }

static void TestNxDs0(AtModulePrbsTestRunner self)
    {
    static const uint8 cNumTimeslotPerNxDs0 = 10;
    AtModulePdh pdhModule = PdhModule(self);
    AtModulePrbs prbsModule = PrbsModule();
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(pdhModule);
    uint32 numTestedDe1s = numDe1s;
    AtPrbsEngine engine;
    uint8 timeslot;
    uint16 de1_i, de1Id;

    if (AtTestQuickTestIsEnabled())
        numTestedDe1s = AtTestRandomNumChannels(numDe1s);

    for (de1_i = 0; de1_i < numTestedDe1s; de1_i++)
        {
        AtPdhDe1 de1;

        if (AtTestQuickTestIsEnabled())
            de1Id = AtTestRandom(numDe1s, de1Id);
        else
            de1Id = de1_i;

        de1 = AtModulePdhDe1Get(pdhModule, de1Id);

        /* Clear current configure to delete all of NxDS0s which may exist before */
        AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm) == cAtOk);
        AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm) == cAtOk);

        for (timeslot = 1; timeslot < 31; timeslot += cNumTimeslotPerNxDs0)
            {
            AtPdhNxDS0 nxDs0 = AtPdhDe1NxDs0Create(de1, AtTestMaskInNxDs0Make(timeslot, cNumTimeslotPerNxDs0));
            uint32 engineId = NextEngine(self);
            AtUnittestRunner runner;
            eAtRet ret;

            if (engineId == AtModulePrbsMaxNumEngines(prbsModule))
                {
                ResetEngine(self);
                engineId = NextEngine(self);
                }

            /* Create and Enable generate prbs */
            engine = AtModulePrbsNxDs0PrbsEngineCreate(prbsModule, engineId, nxDs0);
            AtAssert(engine);
            AtAssert(AtPrbsEngineEnable(engine, cAtTrue) == cAtOk);
            runner = CreateNxDs0PrbsEngineRunner(self, nxDs0);
            AtUnittestRunnerRun(runner);
            AtUnittestRunnerDelete(runner);

            /* Clean configure test */
            AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
            ret = AtModulePrbsEngineDelete(prbsModule, engineId);
            AtAssert(ret == cAtOk);
            }
        }
    }

static void TestVcDe1(AtModulePrbsTestRunner self)
    {
    /* Concrete product know that */
    }

static void TestDe1(AtModulePrbsTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    TestLiuDe1(self);

    if (sdhModule)
        mMethodsGet(self)->TestVcDe1(self);
    }

static void Run(AtUnittestRunner self)
    {
    AtModulePrbsTestRunner runner = (AtModulePrbsTestRunner)self;

    m_AtUnittestRunnerMethods->Run(self);

    TestNxDs0(runner);
    TestDe1(runner);

    if (mMethodsGet(runner)->CanTestDe3Engine(runner))
        mMethodsGet(runner)->TestDe3(runner);
    }

static AtUnittestRunner CreatePrbsEngineTestRunner(AtModulePrbsTestRunner self, AtPrbsEngine engine)
    {
    return (AtUnittestRunner)AtPrbsEngineTestRunnerNew(self, engine);
    }

static void TestDe3(AtModulePrbsTestRunner self)
    {
    /* Concrete product know that */
    }

static eBool CanTestDe3Engine(AtModulePrbsTestRunner self)
    {
    /* Concrete product know that */
    return cAtFalse;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static void MethodsInit(AtModulePrbsTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CreatePrbsEngineTestRunner);
        mMethodOverride(m_methods, TestVcDe1);
        mMethodOverride(m_methods, CanTestDe3Engine);
        mMethodOverride(m_methods, TestDe3);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePrbsTestRunner);
    }

AtModuleTestRunner AtModulePrbsTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtModulePrbsTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModulePrbsTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModulePrbsTestRunnerObjectInit(newRunner, module);
    }

void AtModulePrbsTestRunnerTestVcs(AtModulePrbsTestRunner self, char *vcIdString)
    {
    uint32 numberPaths, path_i, pathIndex = 0;
    AtSdhChannel *paths = (AtSdhChannel *)CliSdhPathFromArgumentGet(vcIdString, &numberPaths);
    AtUnittestRunner runner = (AtUnittestRunner)self;
    AtPrbsEngine engine;
    AtModulePrbs prbsModule = PrbsModule();
    uint32 numTestedPaths = numberPaths;

    if (AtTestQuickTestIsEnabled())
        numTestedPaths = AtTestRandomNumChannels(numTestedPaths);

    for (path_i = 0; path_i < numTestedPaths; path_i++)
        {
        uint32 engineId = NextEngine(self);

        if (AtTestQuickTestIsEnabled())
            pathIndex = AtTestRandom(numTestedPaths, pathIndex);
        else
            pathIndex = path_i;

        if (engineId == AtModulePrbsMaxNumEngines(prbsModule))
            {
            ResetEngine(self);
            engineId = NextEngine(self);
            }

        engine = AtModulePrbsSdhVcPrbsEngineCreate(prbsModule, engineId, paths[pathIndex]);
        AtAssert(engine);
        AtAssert(AtPrbsEngineEnable(engine, cAtTrue) == cAtOk);
        runner = mMethodsGet(self)->CreatePrbsEngineTestRunner(self, AtChannelPrbsEngineGet((AtChannel)paths[pathIndex]));

        /* Clean configure test */
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
        AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);
        }
    }

void AtModulePrbsTestRunnerTestDe1s(AtModulePrbsTestRunner self, char *de1IdString)
    {
    uint32 numChannels, channel_i, channelIndex = 0;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtUnittestRunner runner = (AtUnittestRunner)self;
    AtModulePrbs prbsModule = PrbsModule();
    AtPrbsEngine engine;
    uint32 numTestedChannels;


    numChannels = De1ListFromString(de1IdString, channelList, numChannels);
    if (numChannels == 0) /* FIXME: Can be make crash with AtTestRandom if numChannels = 0*/
    	return;

    numTestedChannels = numChannels;
    if (AtTestQuickTestIsEnabled())
        numTestedChannels = AtTestRandomNumChannels(numChannels);

    for (channel_i = 0; channel_i < numTestedChannels; channel_i++)
        {
        uint32 engineId = NextEngine(self);

        if (AtTestQuickTestIsEnabled())
            channelIndex = AtTestRandom(numChannels, channelIndex);
        else
            channelIndex = channel_i;

        if (engineId == AtModulePrbsMaxNumEngines(prbsModule))
            {
            ResetEngine(self);
            engineId = NextEngine(self);
            }

        engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, (AtPdhDe1)channelList[channelIndex]);
        AtAssert(engine);
        AtAssert(AtPrbsEngineEnable(engine, cAtTrue) == cAtOk);
        runner = mMethodsGet(self)->CreatePrbsEngineTestRunner(self, AtChannelPrbsEngineGet(channelList[channelIndex]));

        /* Clean configure test */
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
        AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);
        }
    }

void AtModulePrbsTestRunnerTestDe3s(AtModulePrbsTestRunner self, char *de3IdString)
    {
    uint32 numChannels, channel_i, channelIndex = 0;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtUnittestRunner runner = (AtUnittestRunner)self;
    AtModulePrbs prbsModule = PrbsModule();
    AtPrbsEngine engine;
    uint32 numTestedChannels;

    numChannels = CliDe3ListFromString(de3IdString, channelList, numChannels);
    AtAssert(numChannels);
    numTestedChannels = numChannels;

    if (AtTestQuickTestIsEnabled())
        numTestedChannels = AtTestRandomNumChannels(numChannels);

    for (channel_i = 0; channel_i < numTestedChannels; channel_i++)
        {
        uint32 engineId = NextEngine(self);

        if (AtTestQuickTestIsEnabled())
            channelIndex = AtTestRandom(numChannels, channelIndex);
        else
            channelIndex = channel_i;

        if (engineId == AtModulePrbsMaxNumEngines(prbsModule))
            {
            ResetEngine(self);
            engineId = NextEngine(self);
            }

        engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, engineId, (AtPdhDe3)channelList[channelIndex]);
        AtAssert(engine);
        AtAssert(AtPrbsEngineEnable(engine, cAtTrue) == cAtOk);
        runner = mMethodsGet(self)->CreatePrbsEngineTestRunner(self, AtChannelPrbsEngineGet(channelList[channelIndex]));

        /* Clean configure test */
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
        AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);
        }
    }

void AtModulePrbsTestCanNotCreateEnginePrbsOnVcs(AtModulePrbsTestRunner self, char *vcIdString)
    {
    uint32 numberPaths, path_i, pathIndex = 0;
    AtSdhChannel *paths = (AtSdhChannel *)CliSdhPathFromArgumentGet(vcIdString, &numberPaths);
    AtPrbsEngine engine;
    AtModulePrbs prbsModule = PrbsModule();
    uint32 numTestedPaths = numberPaths;

    if (AtTestQuickTestIsEnabled())
        numTestedPaths = AtTestRandomNumChannels(numTestedPaths);

    for (path_i = 0; path_i < numTestedPaths; path_i++)
        {
        uint32 engineId = NextEngine(self);

        if (AtTestQuickTestIsEnabled())
            pathIndex = AtTestRandom(numTestedPaths, pathIndex);
        else
            pathIndex = path_i;

        if (engineId == AtModulePrbsMaxNumEngines(prbsModule))
            {
            ResetEngine(self);
            engineId = NextEngine(self);
            }

        /* Should delete before check another testcase */
        engine = AtModulePrbsSdhVcPrbsEngineCreate(prbsModule, engineId, paths[pathIndex]);
        if (engine)
            AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);

        TEST_ASSERT(engine == NULL);
        }
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsEngineTestRunner.h
 * 
 * Created Date: Jan 19, 2016
 *
 * Description : PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSENGINETESTRUNNER_H_
#define _ATPRBSENGINETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtModulePrbsTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsEngineTestRunner *AtPrbsEngineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngineTestRunner AtPrbsEngineTestRunnerNew(AtModulePrbsTestRunner moduleRunner, AtPrbsEngine engine);

AtModulePrbsTestRunner AtPrbsEngineTestRunnerModuleRunner(AtPrbsEngineTestRunner self);
AtPrbsEngine AtPrbsEngineTestRunnerEngine(AtPrbsEngineTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSENGINETESTRUNNER_H_ */


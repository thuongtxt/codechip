/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsEngineTestRunnerInternal.h
 * 
 * Created Date: Jan 7, 2016
 *
 * Description : Prbs Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSENGINETESTRUNNERINTERNAL_H_
#define _ATPRBSENGINETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../runner/AtUnittestRunnerInternal.h"

#include "AtPrbsEngineTestRunner.h"
#include "AtModulePrbsTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPrbsEngineTestRunner
    {
    tAtObjectTestRunner super;

    /* Private data */
    AtModulePrbsTestRunner moduleRunner;
    }tAtPrbsEngineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngineTestRunner AtPrbsEngineTestRunnerObjectInit(AtPrbsEngineTestRunner self, AtModulePrbsTestRunner moduleRunner, AtPrbsEngine engine);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSENGINETESTRUNNERINTERNAL_H_ */


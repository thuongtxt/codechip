/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : AtPrbsEngineTestRunner.c
 *
 * Created Date: Jan 7, 2016
 *
 * Description : PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../driver/src/generic/prbs/AtPrbsEngineInternal.h"
#include "AtPrbsEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPrbsEngineTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine PrbsEngine(void)
    {
    return (AtPrbsEngine)AtPrbsEngineTestRunnerEngine((AtPrbsEngineTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testCanGetChannelWhichPrbsOperationOn(void)
    {
    TEST_ASSERT_NOT_NULL(AtPrbsEngineChannelGet(PrbsEngine()));
    }

static void testCanEnableOrDisableEngine(void)
    {
    TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue) == cAtOk);
    TEST_ASSERT(AtPrbsEngineIsEnabled(PrbsEngine()));

    TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtFalse) == cAtOk);
    TEST_ASSERT(AtPrbsEngineIsEnabled(PrbsEngine()) == cAtFalse);
    }

static void testEngineEnablingOfEngineNull(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPrbsEngineIsEnabled(NULL) == cAtFalse);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanChangeEngineModeByValidValues(void)
    {
    eAtPrbsMode mode;
    AtPrbsEngine engine = PrbsEngine();

    for (mode = cAtPrbsModeStart; mode < cAtPrbsModeNum; mode++)
        {
        if (AtPrbsEngineModeIsSupported(engine, mode))
            {
            TEST_ASSERT(AtPrbsEngineModeSet(engine, mode) == cAtOk);
            TEST_ASSERT(AtPrbsEngineModeGet(engine) == mode);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtPrbsEngineModeSet(engine, mode) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testCannotChangePrbsModeInvalid(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPrbsEngineModeSet(PrbsEngine(), cAtPrbsModeInvalid) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanChangePrbsInvert(void)
    {
    if (AtPrbsEngineInversionIsSupported(PrbsEngine()))
        {
        TEST_ASSERT(AtPrbsEngineInvert(PrbsEngine(), cAtTrue) == cAtOk);
        TEST_ASSERT(AtPrbsEngineIsInverted(PrbsEngine()));
        TEST_ASSERT(AtPrbsEngineInvert(PrbsEngine(), cAtFalse) == cAtOk);
        TEST_ASSERT(AtPrbsEngineIsInverted(PrbsEngine()) == cAtFalse);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPrbsEngineInvert(PrbsEngine(), cAtTrue) != cAtOk);
        TEST_ASSERT(AtPrbsEngineIsInverted(PrbsEngine()) == cAtFalse);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testEngineInvertingOfEngineNull(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPrbsEngineIsInverted(NULL) == cAtFalse);
    AtTestLoggerEnable(cAtTrue);
    }

static void TestCanChangeEngineSide(eAtPrbsSide side)
    {
    if (AtPrbsEngineSideIsSupported(PrbsEngine(), side))
        {
        TEST_ASSERT(AtPrbsEngineSideSet(PrbsEngine(), side) == cAtOk);
        TEST_ASSERT(AtPrbsEngineSideGet(PrbsEngine()) == side);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPrbsEngineSideSet(PrbsEngine(), side) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCanChangeEngineSideByValidValues(void)
    {
    TestCanChangeEngineSide(cAtPrbsSideTdm);
    TestCanChangeEngineSide(cAtPrbsSidePsn);
    }

static void testCannotChangePrbsSideInvalid(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPrbsEngineSideSet(PrbsEngine(), cAtPrbsSideUnknown) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static eBool Errored(AtPrbsEngine self)
    {
    if ((AtPrbsEngineAlarmHistoryGet(self) & cAtPrbsEngineAlarmTypeError) ||
        (AtPrbsEngineAlarmHistoryGet(self) & cAtPrbsEngineAlarmTypeLossSync))
        return cAtTrue;
    return cAtFalse;
    }

static void ErrorAssert(AtPrbsEngine engine, eBool error)
    {
    if (AtTestIsSimulationTesting())
        return;
    TEST_ASSERT_EQUAL_INT(error, Errored(engine));
    }

static void testEngineCanForceError(void)
    {
    if (AtPrbsEngineErrorForcingIsSupported(PrbsEngine()))
        {
        /* Initialize before test force alarm */
        TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue) == cAtOk);
        AtPrbsEngineAlarmHistoryClear(PrbsEngine());

        TEST_ASSERT(AtPrbsEngineErrorForce(PrbsEngine(), cAtTrue) == cAtOk);
        TEST_ASSERT(AtPrbsEngineErrorIsForced(PrbsEngine()));
        ErrorAssert(PrbsEngine(), cAtTrue);

        TEST_ASSERT(AtPrbsEngineErrorForce(PrbsEngine(), cAtFalse) == cAtOk);
        TEST_ASSERT(AtPrbsEngineErrorIsForced(PrbsEngine()) == cAtFalse);

        /* Clear current status */
        AtPrbsEngineAlarmHistoryClear(PrbsEngine());
        ErrorAssert(PrbsEngine(), cAtFalse);
        TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtFalse) == cAtOk);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue) == cAtOk);
        TEST_ASSERT(AtPrbsEngineErrorForce(PrbsEngine(), cAtTrue) != cAtOk);
        TEST_ASSERT(AtPrbsEngineErrorIsForced(PrbsEngine()) == cAtFalse);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testEngineForceErrorOfEngineNull(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPrbsEngineErrorIsForced(NULL) == cAtFalse);
    AtTestLoggerEnable(cAtTrue);
    }

static void testEngineCanforceSingleError(void)
    {
    if (AtPrbsEngineTxErrorInjectionIsSupported(PrbsEngine()))
        {
        /* Initialize before test force alarms */
        TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue) == cAtOk);
        AtPrbsEngineAlarmHistoryClear(PrbsEngine());

        TEST_ASSERT(AtPrbsEngineTxErrorInject(PrbsEngine(), 1) == cAtOk);
        ErrorAssert(PrbsEngine(), cAtTrue);

        /* After forcing, the state must normal */
        AtPrbsEngineAlarmHistoryClear(PrbsEngine());
        ErrorAssert(PrbsEngine(), cAtFalse);

        TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtFalse) == cAtOk);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue)  == cAtOk);
        TEST_ASSERT(AtPrbsEngineTxErrorInject(PrbsEngine(), 1) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCanChangePrbsErrorRateByValidValues(void)
    {
    uint8 errorRates[] = {cAtBerRate1E3, cAtBerRate1E4, cAtBerRate1E5, cAtBerRate1E6,
                          cAtBerRate1E7, cAtBerRate1E8, cAtBerRate1E9, cAtBerRate1E10};
    uint8 rate_i;

    for (rate_i = 0; rate_i < mCount(errorRates); rate_i++)
        {
        if (AtPrbsEngineErrorForcingRateIsSupported(PrbsEngine(), errorRates[rate_i]))
            {
            TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue) == cAtOk);
            TEST_ASSERT(AtPrbsEngineIsEnabled(PrbsEngine()));
            TEST_ASSERT(AtPrbsEngineTxErrorRateSet(PrbsEngine(), errorRates[rate_i]) == cAtOk);
            TEST_ASSERT(AtPrbsEngineTxErrorRateGet(PrbsEngine()) == errorRates[rate_i]);

            /* Make sure rx same value with tx */
            if (!AtTestIsSimulationTesting())
                {
                TEST_ASSERT(AtPrbsEngineRxErrorRateGet(PrbsEngine()) == errorRates[rate_i]);
                }

            /* Disable force error */
            TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtFalse) == cAtOk);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtPrbsEngineEnable(PrbsEngine(), cAtTrue) == cAtOk);
            TEST_ASSERT(AtPrbsEngineTxErrorRateSet(PrbsEngine(), errorRates[rate_i]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testPrbsEngineCannotForceErrorRateInvalid(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPrbsEngineTxErrorRateSet(PrbsEngine(), cAtBerRateUnknown) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanAccessPrbsCounters(void)
    {
    /* Clear before check */
    AtPrbsEngineCounterClear(PrbsEngine(), cAtPrbsEngineCounterTxFrame);
    AtPrbsEngineCounterClear(PrbsEngine(), cAtPrbsEngineCounterRxFrame);
    AtPrbsEngineCounterGet(PrbsEngine(), cAtPrbsEngineCounterTxFrame);
    AtPrbsEngineCounterGet(PrbsEngine(), cAtPrbsEngineCounterRxFrame);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPrbs_Fixtures)
        {
        new_TestFixture("testCanGetChannelWhichPrbsOperationOn", testCanGetChannelWhichPrbsOperationOn),
        new_TestFixture("testCanEnableOrDisableEngine", testCanEnableOrDisableEngine),
        new_TestFixture("testEngineEnablingOfEngineNull", testEngineEnablingOfEngineNull),
        new_TestFixture("testCanChangeEngineModeByValidValues", testCanChangeEngineModeByValidValues),
        new_TestFixture("testCanAccessPrbsCounters", testCanAccessPrbsCounters),
        new_TestFixture("testPrbsEngineCannotForceErrorRateInvalid", testPrbsEngineCannotForceErrorRateInvalid),
        new_TestFixture("testCanChangePrbsErrorRateByValidValues", testCanChangePrbsErrorRateByValidValues),
        new_TestFixture("testEngineCanforceSingleError", testEngineCanforceSingleError),
        new_TestFixture("testEngineForceErrorOfEngineNull", testEngineForceErrorOfEngineNull),
        new_TestFixture("testEngineCanForceError", testEngineCanForceError),
        new_TestFixture("testCannotChangePrbsSideInvalid", testCannotChangePrbsSideInvalid),
        new_TestFixture("testCanChangeEngineSideByValidValues", testCanChangeEngineSideByValidValues),
        new_TestFixture("testEngineInvertingOfEngineNull", testEngineInvertingOfEngineNull),
        new_TestFixture("testCanChangePrbsInvert", testCanChangePrbsInvert),
        new_TestFixture("testCannotChangePrbsModeInvalid", testCannotChangePrbsModeInvalid)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPrbs_Caller, "TestSuite_AtPrbs", NULL, NULL, TestSuite_AtPrbs_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPrbs_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "%s", AtObjectToString((AtObject)AtPrbsEngineTestRunnerEngine((AtPrbsEngineTestRunner)self)));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtPrbsEngineTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtPrbsEngineTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPrbsEngineTestRunner);
    }

AtPrbsEngineTestRunner AtPrbsEngineTestRunnerObjectInit(AtPrbsEngineTestRunner self, AtModulePrbsTestRunner moduleRunner, AtPrbsEngine engine)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)engine) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->moduleRunner = moduleRunner;

    return self;
    }

AtPrbsEngineTestRunner AtPrbsEngineTestRunnerNew(AtModulePrbsTestRunner moduleRunner, AtPrbsEngine engine)
    {
    AtPrbsEngineTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPrbsEngineTestRunnerObjectInit(newRunner, moduleRunner, engine);
    }

AtModulePrbsTestRunner AtPrbsEngineTestRunnerModuleRunner(AtPrbsEngineTestRunner self)
    {
    return self ? self->moduleRunner : NULL;
    }

AtPrbsEngine AtPrbsEngineTestRunnerEngine(AtPrbsEngineTestRunner self)
    {
    return (AtPrbsEngine)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

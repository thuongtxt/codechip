/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtModulePrbsTestRunner.h
 * 
 * Created Date: Jan 7, 2016
 *
 * Description : PRBS Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPRBSTESTRUNNER_H_
#define _ATMODULEPRBSTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePrbsTestRunner *AtModulePrbsTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtModulePrbsTestRunnerTestVcs(AtModulePrbsTestRunner self, char *vcIdString);
void AtModulePrbsTestRunnerTestDe1s(AtModulePrbsTestRunner self, char *de1IdString);
void AtModulePrbsTestRunnerTestDe3s(AtModulePrbsTestRunner self, char *de3IdString);
void AtModulePrbsTestCanNotCreateEnginePrbsOnVcs(AtModulePrbsTestRunner self, char *vcIdString);
#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPRBSTESTRUNNER_H_ */


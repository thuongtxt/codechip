/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtUnittestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Abstract unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtList.h"
#include "AtUnittestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtUnittestRunnerMethods m_methods;

static AtList m_runners = NULL;

static AtUnittestRunner m_currentRunner  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtUnittestRunner self)
    {
    AtOsalMemFree(self);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    return AtListCreate(0);
    }

static AtList Runners()
    {
    if (m_runners == NULL)
        m_runners = AtListCreate(0);
    return m_runners;
    }

static void PushRunner(AtUnittestRunner runner)
    {
    if (runner)
        AtListObjectAdd(Runners(), (AtObject)runner);
    }

static AtUnittestRunner PopRunner()
    {
    AtList runners;
    AtUnittestRunner runner;

    if (AtListLengthGet(m_runners) == 0)
        return NULL;

    runners = Runners();
    runner = (AtUnittestRunner)AtListObjectRemoveAtIndex(runners, AtListLengthGet(runners) - 1);

    /* Free up memory */
    if (AtListLengthGet(runners) == 0)
        {
        AtObjectDelete((AtObject)m_runners);
        m_runners = NULL;
        }

    return runner;
    }

static void Run(AtUnittestRunner self)
    {
    AtList testSuites = self->methods->AllTestSuitesCreate(self);
    uint32 i;

    m_currentRunner = self;

    for (i = 0; i < AtListLengthGet(testSuites); i++)
        {
        TestRef testSuite = (TestRef)AtListObjectGet(testSuites, i);
        AtUnittestRunnerRunTestSuite(self, testSuite);
        }

    AtObjectDelete((AtObject)testSuites);
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    return NULL;
    }

static void MethodsInit(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.Delete             = Delete;
        m_methods.Run                = Run;
        m_methods.AllTestSuitesCreate = AllTestSuitesCreate;
        m_methods.NameBuild          = NameBuild;
        }

    self->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtUnittestRunner);
    }

AtUnittestRunner AtUnittestRunnerObjectInit(AtUnittestRunner self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtUnittestRunnerRun(AtUnittestRunner self)
    {
    eBool logEnabled;

    if (self == NULL)
        return;

    AtPrintc(cSevInfo, "Run: %s\r\n", AtUnittestRunnerName(self));

    PushRunner(m_currentRunner);

    logEnabled = AtTestLoggerIsEnabled();
    self->methods->Run(self);
    AtTestLoggerEnable(logEnabled);

    m_currentRunner = PopRunner();
    }

void AtUnittestRunnerDelete(AtUnittestRunner self)
    {
    if (self)
        self->methods->Delete(self);
    }

AtUnittestRunner AtUnittestRunnerCurrentRunner()
    {
    return m_currentRunner;
    }

const char *AtUnittestRunnerName(AtUnittestRunner self)
    {
    if (self == NULL)
        return NULL;

    if (self->name)
        return self->name;

    AtOsalMemInit(self->nameBuffer, 0, sizeof(self->nameBuffer));
    self->name = mMethodsGet(self)->NameBuild(self, self->nameBuffer, sizeof(self->nameBuffer));
    return self->name;
    }

void AtUnittestRunnerRunTestSuite(AtUnittestRunner self, void *suite)
    {
    if ((self == NULL) || (suite == NULL))
        return;
    TextUIRunner_runTest(suite);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtUnittestRunner.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUNITTESTRUNNER_H_
#define _ATUNITTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtUnittestRunner * AtUnittestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtUnittestRunnerRun(AtUnittestRunner self);
void AtUnittestRunnerDelete(AtUnittestRunner self);
const char *AtUnittestRunnerName(AtUnittestRunner self);
AtUnittestRunner AtUnittestRunnerCurrentRunner();
void AtUnittestRunnerRunTestSuite(AtUnittestRunner self, void *suite);

#endif /* _ATUNITTESTRUNNER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtUnittestRunnerInternal.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUNITTESTRUNNERINTERNAL_H_
#define _ATUNITTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTests.h"
#include "AtUnittestRunner.h"
#include "AtOsal.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtUnittestRunnerMethods
    {
    void (*Delete)(AtUnittestRunner self);
    void (*Run)(AtUnittestRunner self);
    AtList (*AllTestSuitesCreate)(AtUnittestRunner self);
    char *(*NameBuild)(AtUnittestRunner self, char *buffer, uint32 bufferSize);
    }tAtUnittestRunnerMethods;

typedef struct tAtUnittestRunner
    {
    const tAtUnittestRunnerMethods *methods;

    /* Private data */
    char nameBuffer[256];
    char *name;
    }tAtUnittestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner AtUnittestRunnerObjectInit(AtUnittestRunner self);

#endif /* _ATUNITTESTRUNNERINTERNAL_H_ */


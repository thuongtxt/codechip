/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtHalSimTests.c
 *
 * Created Date: Sep 18, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "stdio.h"
#include "AtTests.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumShiftInDword        32
#define cHalSimTestMaxNumHoldRegs   6
#define cAtHalSimRegMemNumBlock       16
#define cAtHalSimOneMega              0x100000

/*--------------------------- Macros -----------------------------------------*/
#define mAtReadWriteErrStr(addr, value, exp)        ("Read "##addr "= "##value ", expected value = "##exp)

/* This macro is used to get the value of bit at desired location */
#define mAtBitGet(reg, shift, pBitVal) \
    { *(pBitVal) = (((reg)>>(shift)) & 0x1); }

/* This macro is used to insert the bit into desired location of register */
#define mAtBitIns(pReg, shift, bitVal) \
    {*(pReg) = ((*(pReg))&(~((0x1)<<(shift)))) | (((bitVal) & 0x1)<<(shift));}

/*--------------------------- Local typedefs ---------------------------------*/
/* The following enumeration defines constants indicating PRBS mode */
typedef enum eAtPrbsMd
    {
    cAtPrbs9        = 0,                /* Prbs-9 */
    cAtPrbs11,                          /* Prbs-11 */
    cAtPrbs15,                          /* Prbs-15 */
    cAtPrbs20a,                         /* Prbs-20 */
    cAtPrbs20b,                         /* Prbs-20(with zero suppression) */
    cAtPrbs23,                          /* Prbs-23 */
    cAtPrbs29,                          /* Prbs-29 */
    cAtPrbs31,                          /* Prbs-31 */
    cAtFixPat                           /* Fixed pattern */
    } eAtPrbsMd;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtHal hal = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*------------------------------------------------------------------------------
Prototype    : eBool AtPrbsGen(dword  inVal, eAtPrbsMd  prbsMd, dword *pOutVal);

Purpose      : This API is used to generate 32-bit PRBS sequences.

Inputs       : inVal                - is the present pattern of PRBS sequence.
               prbsMd               - is the mode of PRBS gereration.

Outputs      : pOutVal              - is the pointer that points the location
                                      storing the next pattern of PRBS sequence.
Return       : None

Valid State  : Ready state of the driver state

Side Effect  : None

------------------------------------------------------------------------------*/
eBool AtPrbsGen(uint32  inVal, eAtPrbsMd  prbsMd, uint32 *pOutVal)
    {
    uint32    d;
    uint16    wId;
    uint32    bita = 0;
    uint32    bitb = 0;

    /* Take the input pattern */
    d = inVal;
    switch(prbsMd)
        {
        /* Generate PRBS-9 sequence. The sequence may be generated in a nine
        -stage shift register whose 5th and 9th stage outputs are added in a
        modulo-two addition stage, and the result is fed back to the input
        of the first stage. */
        case cAtPrbs9:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 8, &bita);     /* Get the value of 9th bit: Q9(k) */
                mAtBitGet(d, 4, &bitb);     /* Get the value of 5th bit: Q5(k) */
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q9(k) XOR Q5(k)*/
                }
            }break;
        /* Generate PRBS-11 sequence. The sequence may be generated in an eleven
        -stage shift register whose 9th and 11th stage outputs are added in a
        modulo-two addition stage, and the result is fed back to the input of
        the first stage. */
        case cAtPrbs11:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 10, &bita);    /* Get the value of 11th bit Q11(k)*/
                mAtBitGet(d, 8, &bitb);     /* Get the value of 9th bit Q9(k) */
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q11(k) XOR Q9(k)*/
                }
            }break;
        /* Generate PRBS-15 sequence. The sequence may be generated in a fifteen
        -stage shift register whose 14th and 15th stage outputs are added in a
        modulo-two addition stage, and the result is fed back to the input of
        the first stage. */
        case cAtPrbs15:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 14, &bita);    /* Get the value of 15th bit Q15(k) */
                mAtBitGet(d, 13, &bitb);    /* Get the value of 14th bit Q14(k) */
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q15(k) XOR Q14(k)*/
                }
            }break;
        /* Generate PRBS-20 sequence. The sequence may be generated in a twenty-
        stage shift register whose 3rd and 20th stage outputs are added in a
        modulo-two addition stage, and the result is fed back to the input of
        the first stage. */
        case cAtPrbs20a:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 19, &bita);    /* Get the value of 20th bit Q20(k)*/
                mAtBitGet(d, 2, &bitb);     /* Get the value of 3rd bit Q3(k)*/
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q20(k) XOR Q3(k)*/
                }
            }break;
        /* Generate PRBS-20(with zero suppression) sequence. The sequence may be
        generated in a twenty-stage shift register whose 17th and 20th stage
        outputs are added in a modulo-two addition stage, and the result is fed
        back to the input of the first stage. An output bit is forced to be a ONE
        whenever the next 14 bits are all ZERO. */
        case cAtPrbs20b:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 19, &bita);    /* Get the value of 20th bit Q20(k)*/
                mAtBitGet(d, 16, &bitb);    /* Get the value of 17th bit Q17(k)*/
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q20(k) XOR Q17(k)*/
                }
            }break;
        /* Generate PRBS-23 sequence. The sequence may be generated in a twenty-
        three-stage shift register whose 18th and 23rd stage outputs are added
        in a modulo-two addition stage, and the result is fed back to the input
        of the first stage. */
        case cAtPrbs23:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 22, &bita);    /* Get the value of 23rd bit Q23(k)*/
                mAtBitGet(d, 17, &bitb);    /* Get the value of 18th bit Q18(k)*/
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q23(k) XOR Q18(k)*/
                }
            }break;
        /* Generate PRBS-29 sequence. The sequence may be generated in a twenty-
        nine-stage shift register whose 27th and 29th stage outputs are added in
        a modulo-two addition stage, and the result is fed back to the input of
        the first stage. */
        case cAtPrbs29:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 28, &bita);    /* Get the value of 29th bit Q29(k)*/
                mAtBitGet(d, 26, &bitb);    /* Get the value of 27th bit Q27(k)*/
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q29(k) XOR Q27(k)*/
                }
            }break;
        /* Generate PRBS-31 sequence. The sequence may be generated in a thirty-
        one-stage shift register whose 28th and 31st stage outputs are added in
        a modulo-two addition stage, and the result is fed back to the input of
        the first stage. */
        case cAtPrbs31:
            {
            for (wId = 0; wId < cNumShiftInDword; wId++)
                {
                mAtBitGet(d, 30, &bita);    /* Get the value of 31st bit Q31(k)*/
                mAtBitGet(d, 27, &bitb);    /* Get the value of 28th bit Q28(k)*/
                d = d<<1;                   /* Shift left for one bit
                                               Qn+1(k+1) = Qn(k)*/
                mAtBitIns(&d, 0, bita^bitb);/* Feed back to d[0]
                                               Q1(k+1) = Q31(k) XOR Q28(k)*/
                }
            }break;
        case cAtFixPat:
            break;
        default:
            return cAtFalse;
        }

    /* Return the next pattern of sequence */
    *pOutVal = (prbsMd == cAtFixPat) ? inVal : d;

    return cAtTrue;
    }

static void setUp()
    {
    AtOsalSharedSet(TestedOsal());
    TEST_ASSERT_NOT_NULL((hal = AtHalSimE1New(cAtHalSimDefaultMemorySizeInMbyte)));
    }

static void tearDown()
    {
    if (hal != NULL)
        {
        AtHalMethodsGet(hal)->Delete(hal);
        }
    }

/*
 * Test read/write to 32bits register: Write value (= address of register) to register
 * from startAddr to endAddr and read to check it
 *
 * @param testedHal
 * @param startAddr
 * @param endAddr
 */
static void ReadWriteTest(AtHal testedHal, uint32 startAddr, uint32 endAddr)
    {
    uint32 addr;
    uint32 value;

    /* Write to memory */
    for (addr = startAddr; addr <= endAddr ; addr++)
        {
        AtHalMethodsGet(testedHal)->Write(testedHal, addr, addr);
        }

    /* Read to check again */
    for (addr = startAddr; addr <= endAddr ; addr++)
        {
        value = AtHalMethodsGet(testedHal)->Read(testedHal, addr);
        if (value != addr)
            {
            char str[100];
            sprintf(str, "Read 0x%x = 0x%x, but expect 0x%x", addr, value, addr);
            TEST_FAIL(str);
            return;
            }
        }
    }

static void LongRegRead(AtHal testedHal, uint32 address, uint32 *value, uint32 *holdRegAddr, uint32 numHoldReg)
    {
    int i;
    /* Read register and compare to check */
    value[0] = AtHalMethodsGet(testedHal)->Read(testedHal, address);
    for (i = 0; i < numHoldReg; i++)
        {
        value[i + 1] = AtHalMethodsGet(testedHal)->Read(testedHal, holdRegAddr[i]);
        }
    }

static void LongRegWrite(AtHal testedHal, uint32 address, uint32 *value, uint32 *holdRegAddr, uint32 numHoldReg)
    {
    int i;

    /* Write to hold registers */
    for (i = 0; i < numHoldReg; i++)
        {
        AtHalMethodsGet(testedHal)->Write(testedHal, holdRegAddr[i], value[i + 1]);
        }

    /* Write to register */
    AtHalMethodsGet(testedHal)->Write(testedHal, address, value[0]);
    }

/*
 * Test Read/Write to long register: Write PRBS value to long register
 * from startAddr to endAddr, and read to check it
 *
 * @param testedHal
 * @param startAddr
 * @param endAddr
 * @param holdRegAddr
 * @param numHoldReg
 */
static void LongReadWriteTest(AtHal testedHal, uint32 startAddr, uint32 endAddr, uint32 *holdRegAddr, uint32 numHoldReg)
    {
    uint32 addr;
    uint32 value[cHalSimTestMaxNumHoldRegs + 1];
    uint32 prbsVal[cHalSimTestMaxNumHoldRegs + 1];
    int i;
    uint8 numHold;
    atbool testOk;

    numHold = (numHoldReg < cHalSimTestMaxNumHoldRegs)? numHoldReg : cHalSimTestMaxNumHoldRegs;

    /* Write to memory */
    prbsVal[0] = 0xAAAAAAAA;
    prbsVal[1] = ~prbsVal[0];
    prbsVal[2] = 0xCCCCCCCC;
    prbsVal[3] = ~prbsVal[2];
    prbsVal[4] = 0xFF00FF00;
    prbsVal[5] = ~prbsVal[4];
    prbsVal[6] = 0xF0F0F0F0;
    for (addr = startAddr; addr <= endAddr ; addr++)
        {
        LongRegWrite(testedHal, addr, prbsVal, holdRegAddr, numHold);

        /* Generate PRBS value for next address */
        for (i = 0; i < numHold + 1; i++)
            {
            AtPrbsGen(prbsVal[i], cAtPrbs31, &prbsVal[i]);
            }
        }

    /* Read to check again */
    prbsVal[0] = 0xAAAAAAAA;
    prbsVal[1] = ~prbsVal[0];
    prbsVal[2] = 0xCCCCCCCC;
    prbsVal[3] = ~prbsVal[2];
    prbsVal[4] = 0xFF00FF00;
    prbsVal[5] = ~prbsVal[4];
    prbsVal[6] = 0xF0F0F0F0;
    testOk = cAtTrue;
    for (addr = startAddr; addr <= endAddr ; addr++)
        {
        /* Read register and compare to check */
        LongRegRead(testedHal, addr, value, holdRegAddr, numHold);
        for (i = 0; i < numHold + 1; i++)
            {
            if (value[i] != prbsVal[i])
                {
                testOk = cAtFalse;
                break;
                }
            }

        if (testOk == cAtTrue)
            {
            /* Generate PRBS value for next address */
            for (i = 0; i < numHold + 1; i++)
                {
                AtPrbsGen(prbsVal[i], cAtPrbs31, &prbsVal[i]);
                }
            }
        else
            {
            char str[100];
            char valStr[100]="0x";

            sprintf(str, "Read 0x%x = ", addr);
            for (i = numHold; i >= 0; i--)
                {
                sprintf(valStr, "%s.%x", valStr, value[i]);
                }
            strcat(str, valStr);
            strcat(str, ", but expect ");

            sprintf(valStr, "0x");
            for (i = numHold; i >= 0; i--)
                {
                sprintf(valStr, "%s.%x", valStr, prbsVal[i]);
                }
            strcat(str, valStr);

            TEST_FAIL(str);
            return;
            }
        }
    }

/*
 * Test Read/Write both 32bit registers and long registers: assume register has even address is 32bits register
 * and register has odd address is long register.
 * Write value to register from startAddr to endAddr and read to check it
 *
 * @param testedHal
 * @param startAddr
 * @param endAddr
 * @param holdRegAddr
 * @param numHoldReg
 */
static void MixReadWriteTest(AtHal testedHal, uint32 startAddr, uint32 endAddr, uint32 *holdRegAddr, uint32 numHoldReg)
    {
    uint32 addr;
    uint32 value[cHalSimTestMaxNumHoldRegs + 1];
    uint32 prbsVal[cHalSimTestMaxNumHoldRegs + 1];
    int i;
    uint8 numHold;
    atbool testOk;

    numHold = (numHoldReg < cHalSimTestMaxNumHoldRegs)? numHoldReg : cHalSimTestMaxNumHoldRegs;

    /* Write to memory: even address is normal registers, odd address is long register */
    prbsVal[0] = 0xAAAAAAAA;
    prbsVal[1] = ~prbsVal[0];
    prbsVal[2] = 0xCCCCCCCC;
    prbsVal[3] = ~prbsVal[2];
    prbsVal[4] = 0xFF00FF00;
    prbsVal[5] = ~prbsVal[4];
    prbsVal[6] = 0xF0F0F0F0;
    for (addr = startAddr; addr <= endAddr ; addr++)
        {
        if ((addr / 2) == 0)
            {
            AtHalMethodsGet(testedHal)->Write(testedHal, addr, addr);
            }
        else
            {
            LongRegWrite(testedHal, addr, prbsVal, holdRegAddr, numHold);
            }

        /* Generate PRBS value for next address */
        for (i = 0; i < numHold + 1; i++)
            {
            AtPrbsGen(prbsVal[i], cAtPrbs31, &prbsVal[i]);
            }
        }

    /* Read to check again */
    prbsVal[0] = 0xAAAAAAAA;
    prbsVal[1] = ~prbsVal[0];
    prbsVal[2] = 0xCCCCCCCC;
    prbsVal[3] = ~prbsVal[2];
    prbsVal[4] = 0xFF00FF00;
    prbsVal[5] = ~prbsVal[4];
    prbsVal[6] = 0xF0F0F0F0;
    testOk = cAtTrue;
    for (addr = startAddr; addr <= endAddr ; addr++)
        {
        if ((addr / 2) == 0)
            {
            value[0] = AtHalMethodsGet(testedHal)->Read(testedHal, addr);
            if (value[0] != addr)
                {
                char str[100];
                sprintf(str, "Read 0x%x = 0x%x, but expect 0x%x", addr, value[0], addr);
                TEST_FAIL(str);
                return;
                }
            }
        else
            {
            /* Read register and compare to check */
            LongRegRead(testedHal, addr, value, holdRegAddr, numHold);
            for (i = 0; i < numHold + 1; i++)
                {
                if (value[i] != prbsVal[i])
                    {
                    testOk = cAtFalse;
                    break;
                    }
                }

            if (testOk == cAtTrue)
                {
                /* Generate PRBS value for next address */
                for (i = 0; i < numHold + 1; i++)
                    {
                    AtPrbsGen(prbsVal[i], cAtPrbs31, &prbsVal[i]);
                    }
                }
            else
                {
                char str[100];
                char valStr[100]="0x";

                sprintf(str, "Read 0x%x = ", addr);
                for (i = numHold; i >= 0; i--)
                    {
                    sprintf(valStr, "%s.%x", valStr, value[i]);
                    }
                strcat(str, valStr);
                strcat(str, ", but expect ");

                sprintf(valStr, "0x");
                for (i = numHold; i >= 0; i--)
                    {
                    sprintf(valStr, "%s.%x", valStr, prbsVal[i]);
                    }
                strcat(str, valStr);

                TEST_FAIL(str);
                return;
                }
            }
        }
    }

/*
 * Test Read/Write 32bits register
 */
static void testReadWrite()
    {
    uint16 i;

    for (i = 0; i < cAtHalSimRegMemNumBlock; i++)
        {
        ReadWriteTest(hal, (i * cAtHalSimOneMega), (i * cAtHalSimOneMega) + 0x1000);
        }
    }

/*
 * Test Read/Write long register
 */
static void testLongReadWrite()
    {
    uint32 holdRegisters[] = {0x840010, 0x840011, 0x840012, 0x840013};
    LongReadWriteTest(hal, 0x840020, 0x841000, holdRegisters, 4);
    }


/*
 * Test Read/Write both 32bits register and long register
 */
static void testReadWriteMix()
    {
    uint32 holdRegisters[] = {0x840010, 0x840011, 0x840012, 0x840013};
    MixReadWriteTest(hal, 0x840020, 0x841000, holdRegisters, 4);
    }

TestRef TestSuite_HalSim(void)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_HalSim_Fixtures)
        {
        new_TestFixture("testReadWrite", testReadWrite),
        new_TestFixture("testLongReadWrite", testLongReadWrite),
        new_TestFixture("testReadWriteMix", testReadWriteMix),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_HalSim_Caller, "TestSuite_HalSim", setUp, tearDown, TestSuite_HalSim_Fixtures);

    return (TestRef)&TestSuite_HalSim_Caller;
    }

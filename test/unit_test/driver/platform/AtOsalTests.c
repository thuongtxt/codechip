/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtOsalTests.c
 *
 * Created Date: Aug 1, 2012
 *
 * Author      : namnn
 *
 * Description : OSAL testing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtOsalLinuxDebug.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtOsal Osal()
    {
    return AtOsalLinuxDebug();
    }

static void testNoLeak()
    {
    AtOsalLeakTracker leakTracker = AtOsalLinuxDebugLeakTracker(Osal());

    if (AtOsalLeakTrackerIsLeak(leakTracker))
        AtOsalLeakTrackerReport(leakTracker);

    TEST_ASSERT(!AtOsalLeakTrackerIsLeak(leakTracker));
    }

static TestRef TestSuite_AtOsal()
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtOsal)
        {
        new_TestFixture("testNoLeak", testNoLeak)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPw_Caller, "TestSuite_AtOsal", NULL, NULL, TestSuite_AtOsal);

    return (TestRef)((void *)&TestSuite_AtPw_Caller);
    }

void AtOsalTests()
    {
    TextUIRunner_runTest(TestSuite_AtOsal());
    AtOsalLinuxDebugCleanUp(Osal());
    }

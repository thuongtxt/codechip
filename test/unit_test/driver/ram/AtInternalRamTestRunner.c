/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtInternalRamTestRunner.c
 *
 * Created Date: Aug 1, 2016
 *
 * Description : Internal RAM test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRam.h"
#include "AtRam.h"
#include "../../../../driver/src/generic/ram/AtInternalRamInternal.h"
#include "../runner/AtUnittestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "../man/AtObjectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtInternalRamTestRunner*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtInternalRamTestRunner
    {
    tAtUnittestRunner super;
    AtModuleRam moduleRam;
    }tAtInternalRamTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static const uint32 ramCounterType[] = {
                                      cAtRamCounterTypeEccCorrectableErrors,   /**< Number of correctable errors */
                                      cAtRamCounterTypeEccUncorrectableErrors, /**< Number of uncorrectable errors */
                                      cAtRamCounterTypeCrcErrors,              /**< Number of CRC errors */
                                      cAtRamCounterTypeRead,                   /**< Number of Read operations */
                                      cAtRamCounterTypeWrite,                  /**< Number of Write operations */
                                      cAtRamCounterTypeErrors,                 /**< Number of general errors */
                                      cAtRamCounterTypeDdrStartCounter,        /**< DDR start counter type (private used) */
                                      cAtRamCounterTypeZbtStartCounter         /**< ZBT start counter type (private used) */
                                      };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void setUp()
    {
    /* TODO: add code to tear down if necessary */
    }

static void tearDown()
    {
    /* TODO: add code to tear down if necessary */
    }

static AtModuleRam ModuleRam(void)
    {
    AtUnittestRunner runner = AtUnittestRunnerCurrentRunner();
    return mThis(runner)->moduleRam;
    }

static eBool ShouldTestInternalRam(AtInternalRam ram)
    {
    if (AtInternalRamIsReserved(ram))
        return cAtFalse;
    return cAtTrue;
    }

static void testParityRamCanEnableParityMonitoring()
    {
    uint32 ramId;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        AtInternalRam ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamParityMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamParityMonitorEnable(ram, cAtTrue) == cAtOk);
        TEST_ASSERT(AtInternalRamParityMonitorEnable(ram, cAtFalse) == cAtOk);
        TEST_ASSERT(AtInternalRamParityMonitorEnable(ram, cAtTrue) == cAtOk);
        }
    }

static void testParityRamCanNotEnableCrcMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamParityMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtInternalRamCrcMonitorEnable(ram, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testParityRamCanNotEnableEccMonitoring()
    {
    uint32 ramId;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        AtInternalRam ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamParityMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtInternalRamEccMonitorEnable(ram, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testParityRamCrcMonitoringMustBeDisabled()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamParityMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamCrcMonitorIsEnabled(ram) == cAtFalse);
        }
    }

static void testParityRamEccMonitoringMustBeDisabled()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamParityMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamEccMonitorIsEnabled(ram) == cAtFalse);
        }
    }

static void testCrcRamCanEnableCrcMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        eBool values[] = {cAtTrue, cAtFalse, cAtTrue};
        uint32 value_i;

        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamCrcMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        for (value_i = 0; value_i < mCount(values); value_i++)
            {
            if (AtInternalRamCrcMonitorCanEnable(ram, values[value_i]))
                {
                TEST_ASSERT(AtInternalRamCrcMonitorEnable(ram, values[value_i]) == cAtOk);
                TEST_ASSERT_EQUAL_INT(values[value_i], AtInternalRamCrcMonitorIsEnabled(ram));
                }
            else
                {
                AtTestLoggerEnable(cAtFalse);
                TEST_ASSERT(AtInternalRamCrcMonitorEnable(ram, values[value_i]) != cAtOk);
                AtTestLoggerEnable(cAtTrue);
                }
            }
        }
    }

static void testCrcRamParityMonitoringMustBeDisabled()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamCrcMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamParityMonitorIsEnabled(ram) == cAtFalse);
        }
    }

static void testCrcRamEccMonitoringMustBeDisabled()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamCrcMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamEccMonitorIsEnabled(ram) == cAtFalse);
        }
    }

static void testCrcRamCanNotEnableParityMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamCrcMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtInternalRamParityMonitorEnable(ram, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCrcRamCanNotEnableEccMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamCrcMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtInternalRamEccMonitorEnable(ram, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testEccRamCanEnableEccMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        eBool values[] = {cAtTrue, cAtFalse, cAtTrue};
        uint32 value_i;

        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamEccMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        for (value_i = 0; value_i < mCount(values); value_i++)
            {
            if (AtInternalRamEccMonitorCanEnable(ram, values[value_i]))
                {
                TEST_ASSERT(AtInternalRamEccMonitorEnable(ram, values[value_i]) == cAtOk);
                TEST_ASSERT_EQUAL_INT(values[value_i], AtInternalRamEccMonitorIsEnabled(ram));
                }
            else
                {
                AtTestLoggerEnable(cAtFalse);
                TEST_ASSERT(AtInternalRamEccMonitorEnable(ram, values[value_i]) != cAtOk);
                AtTestLoggerEnable(cAtTrue);
                }
            }
        }
    }

static void testEccRamParityMonitoringMustBeDisabled()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamEccMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamParityMonitorIsEnabled(ram) == cAtFalse);
        }
    }

static void testEccRamCrcMonitoringMustBeDisabled()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamEccMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        TEST_ASSERT(AtInternalRamCrcMonitorIsEnabled(ram) == cAtFalse);
        }
    }

static void testEccRamCanNotEnableParityMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamEccMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtInternalRamParityMonitorEnable(ram, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testEccRamCanNotEnableCrcMonitoring()
    {
    uint32 ramId;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!AtInternalRamEccMonitorIsSupported(ram) || !ShouldTestInternalRam(ram))
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtInternalRamCrcMonitorEnable(ram, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testRamCanForceAllForcableErrors()
    {
    uint32 ramId, errors;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (AtInternalRamIsReserved(ram) || !ShouldTestInternalRam(ram))
            continue;

        errors = AtInternalRamForcableErrorsGet(ram);
        TEST_ASSERT(AtInternalRamErrorForce(ram, errors) == cAtOk);
        TEST_ASSERT(AtInternalRamErrorUnForce(ram, errors) == cAtOk);
        }
    }

static void testRamCanGetCountersWhichAreSupported()
    {
    uint32 ramId, counterType;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        if (!ShouldTestInternalRam(ram))
            continue;

        for (counterType = 0; counterType < mCount(ramCounterType); counterType++)
            {
            if (!AtInternalRamCounterTypeIsSupported(ram, ramCounterType[counterType]))
                continue;

            TEST_ASSERT(AtInternalRamCounterGet(ram, ramCounterType[counterType]) == 0);
            }
        }
    }

static void testRamCanNotGetCountersWhichAreNotSupported()
    {
    uint32 ramId, counterType;
    AtInternalRam ram;
    AtModuleRam moduleRam = ModuleRam();

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(moduleRam); ramId++)
        {
        ram = AtModuleRamInternalRamGet(moduleRam, ramId);
        for (counterType = 0; counterType < mCount(ramCounterType); counterType++)
            {
            if (AtInternalRamCounterTypeIsSupported(ram, ramCounterType[counterType]))
                continue;

            TEST_ASSERT(AtInternalRamCounterGet(ram, ramCounterType[counterType]) == 0);
            }
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_InternalRam_Fixtures)
        {
        new_TestFixture("testParityRamCanEnableParityMonitoring", testParityRamCanEnableParityMonitoring),
        new_TestFixture("testParityRamCrcMonitoringMustBeDisabled", testParityRamCrcMonitoringMustBeDisabled),
        new_TestFixture("testParityRamEccMonitoringMustBeDisabled", testParityRamEccMonitoringMustBeDisabled),
        new_TestFixture("testParityRamCanNotEnableCrcMonitoring", testParityRamCanNotEnableCrcMonitoring),
        new_TestFixture("testParityRamCanNotEnableEccMonitoring", testParityRamCanNotEnableEccMonitoring),

        new_TestFixture("testCrcRamCanEnableCrcMonitoring", testCrcRamCanEnableCrcMonitoring),
        new_TestFixture("testCrcRamParityMonitoringMustBeDisabled", testCrcRamParityMonitoringMustBeDisabled),
        new_TestFixture("testCrcRamEccMonitoringMustBeDisabled", testCrcRamEccMonitoringMustBeDisabled),
        new_TestFixture("testCrcRamCanNotEnableParityMonitoring", testCrcRamCanNotEnableParityMonitoring),
        new_TestFixture("testCrcRamCanNotEnableEccMonitoring", testCrcRamCanNotEnableEccMonitoring),

        new_TestFixture("testEccRamCanEnableEccMonitoring", testEccRamCanEnableEccMonitoring),
        new_TestFixture("testEccRamParityMonitoringMustBeDisabled", testEccRamParityMonitoringMustBeDisabled),
        new_TestFixture("testEccRamCrcMonitoringMustBeDisabled", testEccRamCrcMonitoringMustBeDisabled),
        new_TestFixture("testEccRamCanNotEnableParityMonitoring", testEccRamCanNotEnableParityMonitoring),
        new_TestFixture("testEccRamCanNotEnableCrcMonitoring", testEccRamCanNotEnableCrcMonitoring),

        new_TestFixture("testRamCanForceAllForcableErrors", testRamCanForceAllForcableErrors),
        new_TestFixture("testRamCanGetCountersWhichAreSupported", testRamCanGetCountersWhichAreSupported),
        new_TestFixture("testRamCanNotGetCountersWhichAreNotSupported", testRamCanNotGetCountersWhichAreNotSupported),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_InternalRam_Caller, "TestSuite_InternalRam", setUp, tearDown, TestSuite_InternalRam_Fixtures);

    return (TestRef)((void *)&TestSuite_InternalRam_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "Internal RAM");
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(self), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtInternalRamTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtModuleRam ramModule)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit((AtUnittestRunner)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->moduleRam = ramModule;

    return self;
    }

AtUnittestRunner AtInternalRamTestRunnerNew(AtModuleRam ramModule)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, ramModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : AtModuleRamTestRunner.c
 *
 * Created Date: Aug 1, 2016
 *
 * Description : RAM unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRam.h"
#include "../man/module_runner/AtModuleTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtModuleRamTestRunner * AtModuleRamTestRunner;
typedef struct tAtModuleRamTestRunner
    {
    tAtModuleTestRunner super;
    }tAtModuleRamTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleRam Module()
    {
    return (AtModuleRam)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testMaxNumberOfInternalRamGreaterZero()
    {
    TEST_ASSERT(AtModuleRamNumInternalRams(Module()) > 0);
    }

static void testCanGetEnoughInternalRamObject()
    {
    uint32 ramId;

    for (ramId = 0; ramId < AtModuleRamNumInternalRams(Module()); ramId++)
        TEST_ASSERT_NOT_NULL(AtModuleRamInternalRamGet(Module(), ramId));
    }

static void testMaxNumInternalRamNullModuleIsZero()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModuleRamNumInternalRams(NULL) == 0);
    AtTestLoggerEnable(cAtTrue);
    }

static void setUp()
    {
    /* TODO: add code to tear down if necessary */
    }

static void tearDown()
    {
    /* TODO: add code to tear down if necessary */
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleRam_Fixtures)
        {
        new_TestFixture("testMaxNumberOfInternalRamGreaterZero", testMaxNumberOfInternalRamGreaterZero),
        new_TestFixture("testCanGetEnoughInternalRamObject", testCanGetEnoughInternalRamObject),
        new_TestFixture("testMaxNumInternalRamNullModuleIsZero", testMaxNumInternalRamNullModuleIsZero),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleRam_Caller, "TestSuite_ModulePw", setUp, tearDown, TestSuite_ModuleRam_Fixtures);

    return (TestRef)((void *)&TestSuite_ModuleRam_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));

    return suites;
    }

static void InternalRamTest(AtModuleRamTestRunner self)
    {
    extern AtUnittestRunner AtInternalRamTestRunnerNew(AtModuleRam moduleRam);
    AtUnittestRunner runner = AtInternalRamTestRunnerNew((AtModuleRam)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self));
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static void Run(AtUnittestRunner self)
    {
    AtModuleRamTestRunner runner;
    m_AtUnittestRunnerMethods->Run(self);

    runner = (AtModuleRamTestRunner)self;
    InternalRamTest(runner);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleRamTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModuleRamTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

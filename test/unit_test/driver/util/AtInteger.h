/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtInteger.h
 * 
 * Created Date: Oct 13, 2012
 *
 * Description : UTIL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTEGER_H_
#define _ATINTEGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtInteger * AtInteger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInteger AtIntegerNew(uint32 value);
void AtIntegerValueSet(AtInteger self, uint32 value);
uint32 AtIntegerValueGet(AtInteger self);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTEGER_H_ */


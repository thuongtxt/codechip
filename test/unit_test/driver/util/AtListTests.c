/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : AtListTests.c
 *
 * Created Date: Oct 13, 2012
 *
 * Description : List test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtList.h"
#include "AtInteger.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtList m_list = NULL;
static AtInteger m_integers[101];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void testCreateNoneLimittedList()
    {
    AtList newList = AtListCreate(0);
    TEST_ASSERT_NOT_NULL(newList);
    AtObjectDelete((AtObject)newList);
    }

static void testCreateLimittedList()
    {
    AtList newList = AtListCreate(100);
    TEST_ASSERT_NOT_NULL(newList);
    AtObjectDelete((AtObject)newList);
    }

static void testAddObject()
    {
    uint32 length = AtListLengthGet(m_list);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtListObjectAdd(m_list, (AtObject)m_integers[0]));
    TEST_ASSERT_EQUAL_INT(length + 1, AtListLengthGet(m_list));
    }

static void testAddObjectToNullListMustNotCrash()
    {
    AtList aList = NULL;
    TEST_ASSERT_EQUAL_INT(cAtError, AtListObjectAdd(aList, (AtObject)m_integers[0]));
    }

static eAtRet allObjectsAdd()
    {
    uint8 i;
    eAtRet ret;

    for(i = 0; i < 100; i++)
        {
        ret = AtListObjectAdd(m_list, (AtObject)m_integers[i]);
        if (cAtOk != ret)
            return ret;
        }

    return cAtOk;
    }

static void testAddObjectToLimittedListWithOutOfRangeIndex()
    {
    if (AtListCapacityGet(m_list) > 0)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());

        /* One more time */
        TEST_ASSERT_EQUAL_INT(cAtError, AtListObjectAdd(m_list, (AtObject)m_integers[100]));
        }
    TEST_ASSERT(cAtTrue);
    }

static void testRemoveFirstObject()
    {
    AtInteger removeObject;

    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());

    removeObject = (AtInteger)AtListObjectRemoveAtIndex(m_list, 0);
    TEST_ASSERT_NOT_NULL(removeObject);
    TEST_ASSERT_EQUAL_INT(0, AtIntegerValueGet(removeObject));
    }

static void testRemoveLastObject()
    {
    AtInteger removeObject;

    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());

    removeObject = (AtInteger)AtListObjectRemoveAtIndex(m_list, AtListLengthGet(m_list) - 1);
    TEST_ASSERT_NOT_NULL(removeObject);
    TEST_ASSERT_EQUAL_INT(99, AtIntegerValueGet(removeObject));
    }

static void testRemoveMiddleObject()
    {
    AtInteger removeObject;

    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());

    removeObject = (AtInteger)AtListObjectRemoveAtIndex(m_list, AtListLengthGet(m_list)/2);
    TEST_ASSERT_NOT_NULL(removeObject);
    TEST_ASSERT_EQUAL_INT(50, AtIntegerValueGet(removeObject));
    }

static void testRemoveObjectAtOutOfRangeIndex()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());
    TEST_ASSERT_NULL(AtListObjectRemoveAtIndex(m_list, AtListLengthGet(m_list)));
    }

static void testRemoveObjectOnNullListMustNotCrash()
    {
    AtList nullList = NULL;
    TEST_ASSERT_EQUAL_INT(cAtError, AtListObjectRemove(nullList, (AtObject)m_integers[0]));
    }

static void testRemoveObjectByReference()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());
    TEST_ASSERT_EQUAL_INT(cAtOk, AtListObjectRemove(m_list, (AtObject)m_integers[40]));
    }

static void testRemoveObjectWithNullReferenceMustNotCrash()
    {
    /* Return error because there is no NULL object in list */
    TEST_ASSERT_EQUAL_INT(cAtError, AtListObjectRemove(m_list, NULL));
    }

static void testListCanBeIterate()
    {
    AtInteger integer;
    uint32 i = 0;
    AtIterator iterator;

    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());
    iterator = (AtIterator)AtListIteratorCreate(m_list);

    TEST_ASSERT_NOT_NULL(iterator);

    /* check all integers in the list */
    while((integer = (AtInteger)AtIteratorNext(iterator)) != NULL)
        {
        TEST_ASSERT_EQUAL_INT(i, AtIntegerValueGet(integer));
        i++;
        }
    AtObjectDelete((AtObject)iterator);
    }

static void testListIteratorGetCount()
    {
    AtIterator iterator;

    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());
    iterator = (AtIterator)AtListIteratorCreate(m_list);

    TEST_ASSERT_EQUAL_INT(100, AtIteratorCount(iterator));
    AtObjectDelete((AtObject)iterator);
    }

static void testListContainsObject()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());
    TEST_ASSERT(AtListContainsObject(m_list, (AtObject)m_integers[10]) == cAtTrue);
    }

static void testListIteratorRemoveCurrentObject()
    {
    AtIterator iterator;
    AtInteger integer;

    TEST_ASSERT_EQUAL_INT(cAtOk, allObjectsAdd());
    iterator = (AtIterator)AtListIteratorCreate(m_list);

    /* check all integers in the list */
    while((integer = (AtInteger)AtIteratorNext(iterator)) != NULL)
        {
        if (50 == AtIntegerValueGet(integer))
            AtIteratorRemove(iterator);
        }

    TEST_ASSERT_EQUAL_INT(99, AtListLengthGet(m_list));
    AtObjectDelete((AtObject)iterator);
    }

static void SetUp()
    {
    uint32 length;
    TEST_ASSERT_NOT_NULL(m_list);
    while((length = AtListLengthGet(m_list)) > 0)
        {
        AtListObjectRemoveAtIndex(m_list, length - 1);
        }
    }

static TestRef _TestSuite_util_ListFactory()
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Util_ListFactory_Fixtures)
        {
        new_TestFixture("testCreateNoneLimittedList", testCreateNoneLimittedList),
        new_TestFixture("testCreateLimittedList", testCreateLimittedList),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Util_ListFactory_Caller, "TestSuite_Util_ListFactory", NULL, NULL, TestSuite_Util_ListFactory_Fixtures);

    return (TestRef)((void *)&TestSuite_Util_ListFactory_Caller);
    }

/* Test suite: Test util */
static TestRef _TestSuite_util()
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Util_Fixtures)
        {
        new_TestFixture("testAddObject", testAddObject),
        new_TestFixture("testAddObjectToNullListMustNotCrash", testAddObjectToNullListMustNotCrash),
        new_TestFixture("testAddObjectToLimittedListWithOutOfRangeIndex", testAddObjectToLimittedListWithOutOfRangeIndex),
        new_TestFixture("testRemoveFirstObject", testRemoveFirstObject),
        new_TestFixture("testRemoveLastObject", testRemoveLastObject),
        new_TestFixture("testRemoveMiddleObject", testRemoveMiddleObject),
        new_TestFixture("testRemoveObjectAtOutOfRangeIndex", testRemoveObjectAtOutOfRangeIndex),
        new_TestFixture("testRemoveObjectOnNullListMustNotCrash", testRemoveObjectOnNullListMustNotCrash),
        new_TestFixture("testRemoveObjectByReference", testRemoveObjectByReference),
        new_TestFixture("testRemoveObjectWithNullReferenceMustNotCrash", testRemoveObjectWithNullReferenceMustNotCrash),
        new_TestFixture("testListCanBeIterate", testListCanBeIterate),
        new_TestFixture("testListIteratorGetCount", testListIteratorGetCount),
        new_TestFixture("testListIteratorRemoveCurrentObject", testListIteratorRemoveCurrentObject),
        new_TestFixture("testListContainsObject", testListContainsObject)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Util_Caller, "TestSuite_Util", SetUp, NULL, TestSuite_Util_Fixtures);

    return (TestRef)((void *)&TestSuite_Util_Caller);
    }

int TestSuite_UtilRun()
    {
    uint8 i;

    /* Test factory methods */
    TextUIRunner_runTest(_TestSuite_util_ListFactory());

    /* Init all objects */
    for (i = 0; i < 101; i++)
        m_integers[i] = AtIntegerNew(i);

    /* Test none-limitted list */
    m_list = AtListCreate(0);
    TextUIRunner_runTest(_TestSuite_util());
    AtObjectDelete((AtObject)m_list);

    /* Test limitted list */
    m_list = AtListCreate(100);
    TextUIRunner_runTest(_TestSuite_util());
    AtObjectDelete((AtObject)m_list);

    /* Delete all objects */
    for (i = 0; i < 101; i++)
        AtObjectDelete((AtObject)m_integers[i]);

    return 0;
    }

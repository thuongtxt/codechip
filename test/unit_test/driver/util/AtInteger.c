/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtInteger.c
 *
 * Created Date: Oct 13, 2012
 *
 * Description : UTIL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtInteger.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtInteger
    {
    tAtObject super;

    /* Property */
    uint32 value;
    }tAtInteger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtInteger AtIntegerNew(uint32 value)
    {
    AtInteger newInteger = AtOsalMemAlloc(sizeof(tAtInteger));
    if (newInteger == NULL)
        return NULL;

    if (AtObjectInit((AtObject)newInteger) == NULL)
        return NULL;

    newInteger->value = value;

    return newInteger;
    }

void AtIntegerValueSet(AtInteger self, uint32 value)
    {
    self->value = value;
    }

uint32 AtIntegerValueGet(AtInteger self)
    {
    return self->value;
    }

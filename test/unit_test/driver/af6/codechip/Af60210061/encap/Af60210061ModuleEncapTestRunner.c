/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Af60210061ModuleEncapTestRunner.c
 *
 * Created Date: Apr 13, 2017
 *
 * Description : Af60210061 ENCAP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210012/encap/Af60210012ModuleEncapTestRunner.h"
#include "../../../../sdh/AtModuleSdhTests.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061ModuleEncapTestRunner
    {
    tAf60210012ModuleEncapTestRunner super;
    }tAf60210061ModuleEncapTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapTestRunnerMethods m_AtModuleEncapTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh ModuleSdh(AtDevice device)
    {
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static AtChannel NxDs0Create(AtPdhDe1 de1, uint32 timeslotBitmap)
    {
    AtChannel nxDs0 = (AtChannel)AtPdhDe1NxDs0Get(de1, timeslotBitmap);
    if (nxDs0 == NULL)
        nxDs0 = (AtChannel)AtPdhDe1NxDs0Create(de1, timeslotBitmap);

    return nxDs0;
    }

static AtChannel *PhysicalChannelsToTest(AtModuleEncapTestRunner self, uint16 *numChannels)
    {
    static AtChannel channels[18];
    AtSdhLine line_1;
    AtSdhChannel aug4s, aug1, vc4, tug3, tug2, vc12, vc11, vc3;
    AtPdhDe1 de1;
    uint8 lineId;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    /* Setup mapping for AUG-4#0 to map AUG-4#0 <--> VC4-4C */
    /* Create aug4 of line 1*/
    lineId = 0;
    line_1 = AtModuleSdhLineGet(ModuleSdh(device), lineId);
    AtSdhLineRateSet(line_1, cAtSdhLineRateStm4);
    AtSdhTestCreateAllAug4s(line_1, &aug4s);

    /* Use 1 c4-4c */
    AtSdhChannelMapTypeSet(aug4s, cAtSdhAugMapTypeAug4MapVc4_4c);
    channels[0] = (AtChannel)AtSdhLineVc4_4cGet(line_1, 0);

    /* Setup mapping for AUG-4#1 to map AUG-4#1 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    /* Create aug4 of line 2*/
    lineId = 1;
    line_1 = AtModuleSdhLineGet(ModuleSdh(device), lineId);
    AtSdhLineRateSet(line_1, cAtSdhLineRateStm4);
    AtSdhTestCreateAllAug4s(line_1, &aug4s);

    /* Use 1 c4 */
    AtSdhChannelMapTypeSet(aug4s, cAtSdhAugMapTypeAug4Map4xAug1s);
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 0);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    channels[1] = (AtChannel)AtSdhLineVc4Get(line_1, 0);

    /* Use 1 c3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 1);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line_1, 1);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line_1, 1, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3);
    channels[2] = (AtChannel)AtSdhLineVc3Get(line_1, 1, 0);

    /* Use 1 c12 */
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line_1, 1, 1);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 1, 1, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    channels[3] = (AtChannel)AtSdhLineVc1xGet(line_1, 1, 1, 0, 0);

    /* Use in e1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 1, 1, 0, 1);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);
    channels[4] = (AtChannel)AtSdhChannelMapChannelGet(vc12);

    /* Use 1 NxDS0 in e1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm);
    channels[5] = NxDs0Create(de1, cBit2 | cBit17 | cBit30);

    /* Setup mapping for AUG-4#2 to map AUG-4#2 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-11 */
    /* Create aug4 of line 3*/
    lineId = 2;
    line_1 = AtModuleSdhLineGet(ModuleSdh(device), lineId);
    AtSdhLineRateSet(line_1, cAtSdhLineRateStm4);
    AtSdhTestCreateAllAug4s(line_1, &aug4s);
    AtSdhChannelMapTypeSet(aug4s, cAtSdhAugMapTypeAug4Map4xAug1s);
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 0);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line_1, 0);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line_1, 0, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 0, 0, 0);

    /* Use 1 vc11 */
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    channels[6] = (AtChannel)AtSdhLineVc1xGet(line_1, 0, 0, 0, 0);

    /* Use 1 ds1 */
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 0, 0, 0, 1);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapDe1);
    channels[7] = (AtChannel)AtSdhChannelMapChannelGet(vc11);

    /* Use 1 NxDS0 in ds1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmSf);
    channels[8] = NxDs0Create(de1, cBit3 | cBit18);

    /* Setup mapping for AUG-4#3 to map AUG-4#3 <--> VC-3 <--> TUG-2 <--> VC-1X */
    /* Create aug4 of line 4 */
    lineId = 3;
    line_1 = AtModuleSdhLineGet(ModuleSdh(device), lineId);
    AtSdhLineRateSet(line_1, cAtSdhLineRateStm4);
    AtSdhTestCreateAllAug4s(line_1, &aug4s);
    AtSdhChannelMapTypeSet(aug4s, cAtSdhAugMapTypeAug4Map4xAug1s);

    /* Use 1 c3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 0);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    channels[9] = (AtChannel)AtSdhLineVc3Get(line_1, 0, 0);

   /* Use 1 c11 */
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line_1, 0, 1);
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 0, 1, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    channels[10] = (AtChannel)AtSdhLineVc1xGet(line_1, 0, 1, 0, 0);

    /* Use 1 ds1 */
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 0, 1, 0, 1);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapDe1);
    channels[11] = (AtChannel)AtSdhChannelMapChannelGet(vc11);

    /* Use 1 NxDS0 in ds1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmSf);
    channels[12] = NxDs0Create(de1, cBit4 | cBit19);

    /* Use 1 c12 */
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 0, 1, 1);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    channels[13] = (AtChannel)AtSdhLineVc1xGet(line_1, 0, 1, 1, 0);

    /* Use in e1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 0, 1, 1, 1);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);
    channels[14] = (AtChannel)AtSdhChannelMapChannelGet(vc12);

    /* Use 1 NxDS0 in e1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1MFCrc);
    channels[15] = NxDs0Create(de1, cBit1 | cBit16 | cBit29);

    *numChannels = 16;
    return channels;
    }

static void OverrideAtModuleEncapTestRunner(AtModuleTestRunner self)
    {
    AtModuleEncapTestRunner runner = (AtModuleEncapTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEncapTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEncapTestRunnerOverride));

        mMethodOverride(m_AtModuleEncapTestRunnerOverride, PhysicalChannelsToTest);
        }

    mMethodsSet(runner, &m_AtModuleEncapTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEncapTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061ModuleEncapTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210012ModuleEncapTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210061ModuleEncapTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return ObjectInit(newRunner, module);
    }

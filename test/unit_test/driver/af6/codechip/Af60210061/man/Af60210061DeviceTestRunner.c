/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210061DeviceTestRunner.c
 *
 * Created Date: Sep 30, 2016
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210012/man/Af60210012DeviceTestRunnerInternal.h"
#include "Af60210061ModuleTestRunnerFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061DeviceTestRunner
    {
    tAf60210012DeviceTestRunner super;
    }tAf60210061DeviceTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60210061ModuleTestRunnerFactorySharedFactory();
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061DeviceTestRunner);
    }

static AtDeviceTestRunner ObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210012DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60210061DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, device);
    }

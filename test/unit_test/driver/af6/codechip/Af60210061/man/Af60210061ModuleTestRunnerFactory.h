/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af60210061ModuleTestRunnerFactory.h
 * 
 * Created Date: Oct 10, 2016
 *
 * Description : Default device unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210061MODULETESTRUNNERFACTORY_H_
#define _AF60210061MODULETESTRUNNERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory Af60210061ModuleTestRunnerFactorySharedFactory();

#ifdef __cplusplus
}
#endif
#endif /* _AF60210061MODULETESTRUNNERFACTORY_H_ */


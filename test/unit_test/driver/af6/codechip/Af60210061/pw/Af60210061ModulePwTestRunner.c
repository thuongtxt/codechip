/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210061ModulePwTestRunner.c
 *
 * Created Date: Sep 30, 2016
 *
 * Description : PW unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210012/pw/Af60210012ModulePwTestRunnerInternal.h"
#include "Af60210061ModulePwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061ModulePwTestRunner
    {
    tAf60210012ModulePwTestRunner super;
    }tAf60210061ModulePwTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/* Save super implementation */
static const tAtModulePwTestRunnerMethods *m_AtModulePwTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtUnittestRunner Af60210061PwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 2048;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 1024;
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    return 1536;
    }

static eBool PwCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    /* This time ignore EC1 lines 4 to 7 */
    if (AtChannelIdGet((AtChannel)line) >= 4)
        return cAtFalse;

    return cAtTrue;
    }

static eBool CepCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    return PwCanTestOnLine(self, line);
    }

static AtList MakePwsForStm4Test(AtModulePwTestRunner self, AtList pws, uint32 lineId)
    {
    uint32 cliLineId = lineId + 1;
    uint32 startPwId, stopPwId;
    char circuitString[64];
    char pwString[64];

    /* Have VC-4s */
    AtSprintf(pwString, "1");
    AtSprintf(circuitString, "vc4.%d.1", cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.1 vc4", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c4", circuitString));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* And VC-3s */
    AtSprintf(pwString, "2-4");
    AtSprintf(circuitString, "vc3.%d.2.1-%d.2.3", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.3 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c3", circuitString));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* VC-12 */
    startPwId = 5;
    stopPwId = (3 * 14) + startPwId - 1;
    AtSprintf(circuitString, "vc1x.%d.3.1.1.1-%d.3.2.7.3", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.3 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.3.1-%d.3.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.3.1.1-%d.3.2.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.3.1.1.1-%d.3.2.7.3 c1x", cliLineId, cliLineId));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* VC-11 */
    startPwId = stopPwId + 1;
    stopPwId = (4 * 7) + startPwId - 1;
    AtSprintf(circuitString, "vc1x.%d.3.3.1.1-%d.3.3.7.4", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.3.3.1-%d.3.3.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.3.3.1.1-%d.3.3.7.4 c1x", cliLineId, cliLineId));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* DS1 SAToP */
    startPwId = stopPwId + 1;
    stopPwId = (4 * 14) + startPwId - 1;
    AtSprintf(circuitString, "de1.%d.4.1.1.1-%d.4.2.7.4", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.4 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.4.1-%d.4.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.4.1.1-%d.4.2.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.4.1.1.1-%d.4.2.7.4 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.4.1.1.1-%d.4.3.7.4 ds1_unframed", cliLineId, cliLineId));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    /* E1 SAToP */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 7) + startPwId - 1;
    AtSprintf(circuitString, "de1.%d.4.3.1.1-%d.4.3.7.3", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.4.3.1-%d.4.3.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.4.3.1.1-%d.4.3.7.3 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.4.3.1.1-%d.4.3.7.3 e1_unframed", cliLineId, cliLineId));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    return pws;
    }

static AtList MakePwsForStm1Test(AtModulePwTestRunner self, AtList pws, uint32 lineId)
    {
    uint32 cliLineId = lineId + 1;
    uint32 startPwId, stopPwId;
    char circuitString[64];
    char pwString[64];

    /* And VC-3s */
    AtSprintf(pwString, "1");
    AtSprintf(circuitString, "vc3.%d.1.1", cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.1 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c3", circuitString));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* VC-12 vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<tuId> */
    startPwId = 2;
    stopPwId = (3 * 4) + startPwId - 1;
    AtSprintf(circuitString, "vc1x.%d.1.2.1.1-%d.1.2.3.3", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.1 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.1.2 7xtug2s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.1.2.1-%d.1.2.3 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.1.2.1.1-%d.1.2.3.3 c1x", cliLineId, cliLineId));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* VC-11 */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 4) + startPwId - 1;
    AtSprintf(circuitString, "vc1x.%d.1.2.4.1-%d.1.2.7.4", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.1.2.4-%d.1.2.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.1.2.4.1-%d.1.2.7.4 c1x", cliLineId, cliLineId));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* DS1 SAToP */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 16) + startPwId - 1;
    AtSprintf(circuitString, "de1.%d.1.3.1.1-%d.1.3.3.4", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.1 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.1.3 7xtug2s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.1.3.1-%d.1.3.3 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.1.3.1.1-%d.1.3.3.4 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.1.3.1.1-%d.1.3.3.4 ds1_unframed", cliLineId, cliLineId));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    /* E1 SAToP */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 9) + startPwId - 1;
    AtSprintf(circuitString, "de1.%d.1.3.4.1-%d.1.3.7.3", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.1.3.4-%d.1.3.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.1.3.4.1-%d.1.3.7.3 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.1.3.4.1-%d.1.3.7.3 e1_unframed", cliLineId, cliLineId));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    return pws;
    }

static AtList MakePwsForGroupTest(AtModulePwTestRunner self, AtList pws)
    {
    AtSdhLine line;
    eAtSdhLineRate rate;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    uint32 lineId = AtTestRandom(AtModuleSdhMaxLinesGet(sdhModule) / 2, 0);

    line = AtModuleSdhLineGet(sdhModule, lineId);
    rate = AtSdhLineRateGet(line);
    if (rate == cAtSdhLineRateStm16)
        return m_AtModulePwTestRunnerMethods->MakePwsForGroupTest(self, pws);

    if (rate == cAtSdhLineRateStm4)
        return MakePwsForStm4Test(self, pws, lineId);

    if (rate == cAtSdhLineRateStm1)
        return MakePwsForStm1Test(self, pws, lineId);

    return NULL;
    }

static eBool SAToPCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    return PwCanTestOnLine(self, line);
    }

static AtUnittestRunner ConstrainerTestRunnerCreate(AtModulePwTestRunner self)
    {
    AtPwConstrainer constrainer = AtPwConstrainer3GMsCardNew();
    if (constrainer)
        return Af60210061PwConstrainerTestRunnerNew(self, constrainer);

    return NULL;
    }

static eBool ShouldTestOnPort(AtModulePwTestRunner self, uint8 portId)
    {
    if (portId != 0)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ShouldTestLine(AtModulePwTestRunner self, uint8 lineId)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return mOutOfRange(lineId,  0, ((AtModuleSdhMaxLinesGet(sdhModule) / 2) - 1)) ? cAtFalse : cAtTrue;
    }

static AtEthPort EthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, 0);
    }

static AtEthPort EthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, 0);
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtModulePwTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumPws);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CepCanTestOnLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MakePwsForGroupTest);
        mMethodOverride(m_AtModulePwTestRunnerOverride, SAToPCanTestOnLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ConstrainerTestRunnerCreate);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestOnPort);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForPdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForSdhChannel);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061ModulePwTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210012ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210061ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

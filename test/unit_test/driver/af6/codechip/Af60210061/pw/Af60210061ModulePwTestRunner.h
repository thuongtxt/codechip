/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60210061ModulePwTestRunner.h
 * 
 * Created Date: Oct 10, 2016
 *
 * Description : Module PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210061MODULEPWTESTRUNNER_H_
#define _AF60210061MODULEPWTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210061ModulePwTestRunnerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210061MODULEPWTESTRUNNER_H_ */


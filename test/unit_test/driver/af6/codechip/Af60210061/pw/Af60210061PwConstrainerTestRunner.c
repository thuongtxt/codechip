/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210061PwConstrainerTestRunner.c
 *
 * Created Date: Oct 13, 2016
 *
 * Description : PW Constrainer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210012/pw/Af60210012PwConstrainerTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061PwConstrainerTestRunner
    {
    tAf60210012PwConstrainerTestRunner super;
    }tAf60210061PwConstrainerTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwConstrainerTestRunnerMethods m_AtPwConstrainerTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumPwToTest(AtPwConstrainerTestRunner self)
    {
    AtUnused(self);
    return 1356;
    }

static void OverrideAtPwConstrainerTestRunner(AtUnittestRunner self)
    {
    AtPwConstrainerTestRunner runner = (AtPwConstrainerTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtPwConstrainerTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtPwConstrainerTestRunnerOverride));
        mMethodOverride(m_AtPwConstrainerTestRunnerOverride, NumPwToTest);
        }

    mMethodsSet(runner, &m_AtPwConstrainerTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtPwConstrainerTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061PwConstrainerTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210012PwConstrainerTestRunnerObjectInit(self, modulePwTestRunner, constrainer) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner Af60210061PwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return ObjectInit(newRunner, modulePwTestRunner, constrainer);
    }

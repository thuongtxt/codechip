/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210061ModuleSdhTestRunner.c
 *
 * Created Date: Sep 30, 2016
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210012/sdh/Af60210012ModuleSdhTestRunnerInternal.h"

#include "Af60210061ModuleSdhTestRunner.h"
#include "Af60210061SdhLineTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061ModuleSdhTestRunner
    {
    tAf60210012ModuleSdhTestRunner super;
    }tAf60210061ModuleSdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhTestRunnerMethods m_AtModuleSdhTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumTfi5s(AtModuleSdhTestRunner self)
    {
    return 0;
    }

static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    return 8;
    }

static AtChannelTestRunner CreateLineTestRunner(AtModuleSdhTestRunner self, AtChannel line)
    {
    return At60210061SdhLineTestRunnerNew(line, (AtModuleTestRunner)self);
    }

static eBool ShouldTestLine(AtModuleSdhTestRunner self, uint8 lineId)
    {
    /* Ignore EC1 lines */
    return mOutOfRange(lineId, 0, 3) ? cAtFalse : cAtTrue;
    }

static eBool SupportTtiMode1Byte(AtModuleSdhTestRunner self)
    {
    return cAtTrue;
    }

static uint8 DefaultLineRate(AtModuleSdhTestRunner self, uint8 lineId)
    {
    return cAtSdhLineRateStm1;
    }

static void OverrideAtModuleSdhTestRunner(AtModuleTestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner) self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleSdhTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleSdhTestRunnerOverride));

        mMethodOverride(m_AtModuleSdhTestRunnerOverride, NumTfi5s);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TotalLines);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, ShouldTestLine);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, SupportTtiMode1Byte);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, CreateLineTestRunner);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, DefaultLineRate);
        }

    mMethodsSet(runner, &m_AtModuleSdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleSdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061ModuleSdhTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210012ModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210061ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return ObjectInit(newRunner, module);
    }

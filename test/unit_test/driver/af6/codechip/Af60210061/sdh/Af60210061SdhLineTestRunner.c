/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60210061SdhLineTestRunner.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : SDH Line unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhChannelTestRunnerInternal.h"
#include "Af60210061SdhLineTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061SdhLineTestRunner
    {
    tAtSdhLineTestRunner super;
    }tAf60210061SdhLineTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineTestRunnerMethods m_AtSdhLineTestRunnerOverride;
static tAtUnittestRunnerMethods    m_AtUnittestRunnerOverride;

/* Override */
static const tAtSdhLineTestRunnerMethods *m_AtSdhLineTestRunnerMethods = NULL;
static const tAtUnittestRunnerMethods    *m_AtUnittestRunnerMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhLine TestedLine()
    {
    return (AtSdhLine)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void TestLineRateIsSupported(AtSdhLineTestRunner self)
    {
    TEST_ASSERT(AtSdhLineRateIsSupported(TestedLine(), cAtSdhLineRateStm16));
    TEST_ASSERT(AtSdhLineRateIsSupported(TestedLine(), cAtSdhLineRateStm4));
    TEST_ASSERT(AtSdhLineRateIsSupported(TestedLine(), cAtSdhLineRateStm1));
    }

static void testDonotSupportStm0()
    {
    TEST_ASSERT(AtSdhLineRateIsSupported(TestedLine(), cAtSdhLineRateStm0) == cAtFalse);
    }

static void testCannotChangeNotSupportedRate()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSdhLineRateSet(TestedLine(), cAtSdhLineRateStm0) == cAtErrorModeNotSupport);
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_At60210011SdhLine_Fixtures)
        {
        new_TestFixture("testDonotSupportStm0", testDonotSupportStm0),
        new_TestFixture("testCannotChangeNotSupportedRate", testCannotChangeNotSupportedRate),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_At60210011SdhLine_Caller, "TestSuite_At60210061SdhLine", NULL, NULL, TestSuite_At60210011SdhLine_Fixtures);

    return (TestRef)((void *)&TestSuite_At60210011SdhLine_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static eBool ShouldTestLineRateOnLine(AtSdhLineTestRunner self, AtSdhLine line)
    {
    if (AtChannelIdGet((AtChannel)line) < 4)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void OverrideAtSdhLineTestRunner(AtChannelTestRunner self)
    {
    AtSdhLineTestRunner runner = (AtSdhLineTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtSdhLineTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtSdhLineTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtSdhLineTestRunnerMethods));

        mMethodOverride(m_AtSdhLineTestRunnerOverride, TestLineRateIsSupported);
        mMethodOverride(m_AtSdhLineTestRunnerOverride, ShouldTestLineRateOnLine);
        }

    mMethodsSet(runner, &m_AtSdhLineTestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtSdhLineTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061SdhLineTestRunner);
    }

static AtChannelTestRunner ObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner At60210061SdhLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return ObjectInit(newRunner, channel, moduleRunner);
    }

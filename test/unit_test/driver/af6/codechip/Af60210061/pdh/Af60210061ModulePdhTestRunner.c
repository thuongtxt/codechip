/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60210061ModulePdhTestRunner.c
 *
 * Created Date: Oct 18, 2016
 *
 * Description : PDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210011/pdh/Af60210011ModulePdhTestRunner.h"
#include "Af60210061ModulePdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210061ModulePdhTestRunner
    {
    tAf60210011ModulePdhTestRunner super;
    }tAf60210061ModulePdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;

/* Save super implementation */
static const tAtModulePdhTestRunnerMethods *m_AtModulePdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldTestMaximumNumberOfDe1Get(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static uint32 NumDe3SerialLines(AtModulePdhTestRunner self)
    {
    return 4;
    }

static eBool ShouldTestSdhLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    /* Ignore EC1 lines in this phase */
    return mOutOfRange(lineId, 0, 3) ? cAtFalse : cAtTrue;
    }

static eBool ShouldTestDe1InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    return ShouldTestSdhLine(self, lineId);
    }

static eBool ShouldTestDe3InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    return ShouldTestSdhLine(self, lineId);
    }

static void OverrideModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner) self;
    if (!m_methodsInit)
        {
        m_AtModulePdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestMaximumNumberOfDe1Get);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, NumDe3SerialLines);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestDe1InLine);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestDe3InLine);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210061ModulePdhTestRunner);
    }

static AtModuleTestRunner Af60210061ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210011ModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210061ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return Af60210061ModulePdhTestRunnerObjectInit(newRunner, module);
    }

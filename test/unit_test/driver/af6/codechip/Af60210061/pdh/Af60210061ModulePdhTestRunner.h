/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Af60210061ModulePdhTestRunner.h
 * 
 * Created Date: Oct 18, 2016
 *
 * Description : PDH test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210061MODULEPDHTESTRUNNER_H_
#define _AF60210061MODULEPDHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210061ModulePdhTestRunnerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210061MODULEPDHTESTRUNNER_H_ */


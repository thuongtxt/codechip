/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60291022ModuleSdhTestRunner.c
 *
 * Created Date: Sep 12, 2018
 *
 * Description : Module SDH test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Af60290021/sdh/Af6029ModuleSdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModule(self) (AtModuleSdh)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60291022ModuleSdhTestRunner
    {
    tAf6029ModuleSdhTestRunner super;
    }tAf60291022ModuleSdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods          m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods   *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void setAllPlateLinesToStm4(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm4) == cAtOk);
        }
    }

static void setAllPlateLinesToStm1(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm1) == cAtOk);
        }
    }

static void testConstrainLine0Stm64CauseLinesUnused(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhLineGet(module, 0), cAtSdhLineRateStm64) == cAtOk);

    for (line_i = 1; line_i < 16; line_i ++)
        {
        TEST_ASSERT_NULL(AtModuleSdhLineGet(module, line_i));
        TEST_ASSERT_NULL(AtModuleSdhFaceplateLineGet(module, line_i));
        }
    }

static void testConstrainLineStm16CauseNextThreeLinesUnused(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 4)
        {
        uint32 i;
        TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhLineGet(module, line_i), cAtSdhLineRateStm16) == cAtOk);

        for (i = 1; i < 4; i++)
            {
            uint32 nextLine = line_i + i;
            TEST_ASSERT_NULL(AtModuleSdhLineGet(module, nextLine));
            TEST_ASSERT_NULL(AtModuleSdhFaceplateLineGet(module, nextLine));
            }
        }
    }

static void testLine0InEachFourFacePlateLinesSupportStm16(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 4)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtModuleSdhLineRateIsSupported(module, line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        }
    }

static void testLine1to3InEachFourFacePlateLinesNotSupportStm16(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 4)
        {
        uint32 i;
        for (i = 1; i < 4; i++)
            {
            AtSdhLine line = AtModuleSdhLineGet(module, line_i + i);
            if (line == NULL)
                continue;

            TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtFalse);
            }
        }
    }

static void testFacePlateLine0SupportStm64(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines, so all faceplate lines are accessible */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if (line_i == 0)
            {
            TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm64) == cAtTrue);
            }
        else
            {
        	AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm64) == cAtFalse);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) != cAtOk);
        	AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testChangeStm64ToStm16CauseFacePlateLine_4_8_12_DefaultStm16(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if (line_i == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
            }
        else if ((line_i % 4) == 0)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm16);
            }
        }
    }

static void testChangeStm64ToStm4CauseFacePlateLine1to3DefaultStm4(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm1 for all faceplate lines to check rate change */
    setAllPlateLinesToStm1();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if (line_i == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm4) == cAtOk);
            }
        else if (line_i < 4)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm4);
            }
        else if ((line_i % 4) == 0)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm16);
            }
        }
    }

static void testChangeStm64ToStm1CauseFacePlateLine1to3DefaultStm1(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if (line_i == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm1) == cAtOk);
            }
        else if (line_i < 4)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm1);
            }
        else if ((line_i % 4) == 0)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm16);
            }
        }
    }

static void testChangeStm64ToStm1CauseFacePlateLine1to3DefaultStm0(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if (line_i == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm0) == cAtOk);
            }
        else if (line_i < 4)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm0);
            }
        else if ((line_i % 4) == 0)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm16);
            }
        }
    }

static void testChangeFacePlateLineStm16ToStm4CauseNextThreeLinesDefaultStm4(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm1 for all faceplate lines to check rate change */
    setAllPlateLinesToStm1();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 4)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        uint8 i;

        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm4) == cAtOk);

        for (i = 1; i < 4; i++)
            {
            line = AtModuleSdhFaceplateLineGet(module, line_i + i);
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm4);
            }
        }
    }

static void testChangeFacePlateLineStm16ToStm1CauseNextThreeLinesDefaultStm1(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 4)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        uint8 i;

        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm1) == cAtOk);

        for (i = 1; i < 4; i++)
            {
            line = AtModuleSdhFaceplateLineGet(module, line_i + i);
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm1);
            }
        }
    }

static void testChangeFacePlateLineStm16ToStm0CauseNextThreeLinesDefaultStm0(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 4)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        uint8 i;

        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm0) == cAtOk);

        for (i = 1; i < 4; i++)
            {
            line = AtModuleSdhFaceplateLineGet(module, line_i + i);
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm0);
            }
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60291022ModuleSdhLineRate_Fixtures)
        {
        new_TestFixture("testChangeStm64ToStm16CauseEvenFacePlateLinesDefaultStm16", testChangeStm64ToStm16CauseFacePlateLine_4_8_12_DefaultStm16),
        new_TestFixture("testChangeStm64ToStm4CauseFacePlateLine1to3DefaultStm4", testChangeStm64ToStm4CauseFacePlateLine1to3DefaultStm4),
        new_TestFixture("testChangeStm64ToStm1CauseFacePlateLine1to3DefaultStm1", testChangeStm64ToStm1CauseFacePlateLine1to3DefaultStm1),
        new_TestFixture("testChangeStm64ToStm1CauseFacePlateLine1to3DefaultStm0", testChangeStm64ToStm1CauseFacePlateLine1to3DefaultStm0),
        new_TestFixture("testChangeFacePlateLineStm16ToStm4CauseNextThreeLinesDefaultStm4", testChangeFacePlateLineStm16ToStm4CauseNextThreeLinesDefaultStm4),
        new_TestFixture("testChangeFacePlateLineStm16ToStm1CauseNextThreeLinesDefaultStm1", testChangeFacePlateLineStm16ToStm1CauseNextThreeLinesDefaultStm1),
        new_TestFixture("testChangeFacePlateLineStm16ToStm0CauseNextThreeLinesDefaultStm0", testChangeFacePlateLineStm16ToStm0CauseNextThreeLinesDefaultStm0),
        new_TestFixture("testLine0InEachFourFacePlateLinesSupportStm16", testLine0InEachFourFacePlateLinesSupportStm16),
        new_TestFixture("testLine1to3InEachFourFacePlateLinesNotSupportStm16", testLine1to3InEachFourFacePlateLinesNotSupportStm16),
        new_TestFixture("testFacePlateLine0SupportStm64", testFacePlateLine0SupportStm64),
        new_TestFixture("testConstrainLine0Stm64CauseLinesUnused", testConstrainLine0Stm64CauseLinesUnused),
        new_TestFixture("testConstrainLineStm16CauseNextThreeLinesUnused", testConstrainLineStm16CauseNextThreeLinesUnused),
        };

    EMB_UNIT_TESTCALLER(TestSuite_Af60291022ModuleSdhLineRate_Caller, "Af60291022ModuleSdh", NULL, NULL, TestSuite_Af60291022ModuleSdhLineRate_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60291022ModuleSdhLineRate_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60291022ModuleSdhTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af6029ModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60291022ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;
    return ObjectInit(newRunner, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af60291022DeviceTestRunnerInternal.h
 * 
 * Created Date: Sep 20, 2019
 *
 * Description : Device unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60291022DEVICETESTRUNNERINTERNAL_H_
#define _AF60291022DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60290021/man/Af60290021DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60291022DeviceTestRunner
    {
    tAf60290021DeviceTestRunner super;
    }tAf60291022DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60291022DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60291022DEVICETESTRUNNERINTERNAL_H_ */


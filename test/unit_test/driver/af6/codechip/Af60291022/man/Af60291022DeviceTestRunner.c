/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60291022DeviceTestRunner.c
 *
 * Created Date: Sep 11, 2018
 *
 * Description : Device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../physical/AtPhysicalTests.h"
#include "Af60291022DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods   m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60291022ModuleTestRunnerFactorySharedFactory();
extern AtSerdesManagerTestRunner Af60291022SerdesManagerTestRunnerNew(AtSerdesManager manager);

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60291022ModuleTestRunnerFactorySharedFactory();
    }

static AtUnittestRunner SerdesManagerTestRunnerCreate(AtDeviceTestRunner self, AtSerdesManager manager)
    {
    return (AtUnittestRunner)Af60291022SerdesManagerTestRunnerNew(manager);
    }

static eBool ModuleNotReady(AtDeviceTestRunner self, eAtModule moduleId)
    {
    return cAtFalse;
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleNotReady);
        mMethodOverride(m_AtDeviceTestRunnerOverride, SerdesManagerTestRunnerCreate);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60291022DeviceTestRunner);
    }

AtDeviceTestRunner Af60291022DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60291022DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60291022DeviceTestRunnerObjectInit(newRunner, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60030080ModulePdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pdh/AtModulePdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60030080ModulePdhTestRunner
    {
    tAtModulePdhTestRunner super;
    }tAf60030080ModulePdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanUseTimeslot16InNxDs0(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static eBool CanUseTimeslot0InNxDs0(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideAtModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, CanUseTimeslot16InNxDs0);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, CanUseTimeslot0InNxDs0);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60030080ModulePdhTestRunner);
    }

AtModuleTestRunner Af60030080ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60030080ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60030080ModulePdhTestRunnerObjectInit(newRunner, module);
    }

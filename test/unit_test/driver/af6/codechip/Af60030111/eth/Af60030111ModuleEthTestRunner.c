/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Af60030111ModuleEthTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : ETH module test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../eth/AtModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60030111ModuleEthTestRunner
    {
    tAtModuleEthTestRunner super;
    }tAf60030111ModuleEthTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthTestRunnerMethods m_AtModuleEthTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldTestEthPort(AtModuleEthTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, ShouldTestEthPort);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60030111ModuleEthTestRunner);
    }

AtModuleTestRunner Af60030111ModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60030111ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60030111ModuleEthTestRunnerObjectInit(newRunner, module);
    }

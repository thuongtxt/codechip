/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60031031ModulePdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pdh/AtModulePdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60031031ModulePdhTestRunner
    {
    tAtModulePdhTestRunner super;
    }tAf60031031ModulePdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasSpecialSystemTiming(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideAtModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, HasSpecialSystemTiming);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60031031ModulePdhTestRunner);
    }

AtModuleTestRunner Af60031031ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60031031ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60031031ModulePdhTestRunnerObjectInit(newRunner, module);
    }

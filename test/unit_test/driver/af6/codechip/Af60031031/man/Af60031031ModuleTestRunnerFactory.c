/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60031031ModuleTestRunnerFactory.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../man/module_runner/AtModuleTestRunnerFactoryInternal.h"
#include "../../../../man/module_runner/AtModuleTestRunner.h"
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60031031ModuleTestRunnerFactory
    {
    tAtModuleTestRunnerFactory super;
    }tAf60031031ModuleTestRunnerFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleTestRunnerFactoryMethods m_AtModuleTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtModuleTestRunnerFactoryMethods *m_AtModuleTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunner Af60031031ModulePdhTestRunnerNew(AtModule module);
extern AtModuleTestRunner Af60031031ModulePwTestRunnerNew(AtModule module);

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60031031ModulePdhTestRunnerNew(module);
    }

static AtModuleTestRunner PwTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60031031ModulePwTestRunnerNew(module);
    }

static void OverrideAtModuleTestRunnerFactory(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtModuleTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtModuleTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtModuleTestRunnerFactoryOverride));

        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PwTestRunnerCreate);
        }

    self->methods = &m_AtModuleTestRunnerFactoryOverride;
    }

static void Override(AtModuleTestRunnerFactory self)
    {
    OverrideAtModuleTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60031031ModuleTestRunnerFactory);
    }

AtModuleTestRunnerFactory Af60031031ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory Af60031031ModuleTestRunnerFactorySharedFactory()
    {
    static tAf60031031ModuleTestRunnerFactory sharedFactory;
    return Af60031031ModuleTestRunnerFactoryObjectInit((AtModuleTestRunnerFactory)&sharedFactory);
    }

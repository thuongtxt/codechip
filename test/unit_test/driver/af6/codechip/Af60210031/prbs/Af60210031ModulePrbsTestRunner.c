/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Af60210031ModulePrbsTestRunner.c
 *
 * Created Date: Jan 26, 2016
 *
 * Description : PRBS Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../prbs/AtPrbsEngineTestRunner.h"
#include "../../../../prbs/AtModulePrbsTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210031ModulePrbsTestRunner
    {
    tAtModulePrbsTestRunner super;
    }tAf60210031ModulePrbsTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsTestRunnerMethods m_AtModulePrbsTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TestDe3(AtModulePrbsTestRunner self)
    {
    char idString[64];

    mCliSuccessAssert(AtCliExecute("pdh serline mode 1-48 ds3"));

    /* DS3 */
    AtSprintf(idString, "1-20");
    mCliSuccessAssert(AtCliExecuteFormat("pdh de3 framing %s ds3_unframed", idString));
    AtModulePrbsTestRunnerTestDe3s(self, idString);

    /* DS1 */
    mCliSuccessAssert(AtCliExecute("pdh de3 framing 25-48 ds3_cbit_28ds1"));
    mCliSuccessAssert(AtCliExecute("pdh de1 framing 25.1.1-48.7.4 ds1_unframed"));
    AtSprintf(idString, "25.1.1-48.7.4");
    AtModulePrbsTestRunnerTestDe1s(self, idString);
    }

static eBool CanTestDe3Engine(AtModulePrbsTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePrbsTestRunner runner = (AtModulePrbsTestRunner)self;
    
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePrbsTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePrbsTestRunnerOverride));

        mMethodOverride(m_AtModulePrbsTestRunnerOverride, CanTestDe3Engine);
        mMethodOverride(m_AtModulePrbsTestRunnerOverride, TestDe3);
        }

    mMethodsSet(runner, &m_AtModulePrbsTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210031ModulePrbsTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePrbsTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210031ModulePrbsTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

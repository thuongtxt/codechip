/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Af60210031ModuleTestRunnerFactory.h
 * 
 * Created Date: Jun 16, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_MAN_AF60210031MODULETESTRUNNERFACTORY_H_
#define _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_MAN_AF60210031MODULETESTRUNNERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../man/module_runner/AtModuleTestRunnerFactoryInternal.h"
#include "../../../../man/module_runner/AtModuleTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210031ModuleTestRunnerFactory *Af60210031ModuleTestRunnerFactory;

typedef struct tAf60210031ModuleTestRunnerFactory
    {
    tAtModuleTestRunnerFactory super;
    }tAf60210031ModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory Af60210031ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_MAN_AF60210031MODULETESTRUNNERFACTORY_H_ */


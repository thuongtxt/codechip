/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210031ModuleTestRunnerFactory.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210031ModuleTestRunnerFactory.h"
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleTestRunnerFactoryMethods m_AtModuleTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtModuleTestRunnerFactoryMethods *m_AtModuleTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
AtModuleTestRunner Af60210031ModulePdhTestRunnerNew(AtModule module);
AtModuleTestRunner Af60210031ModulePwTestRunnerNew(AtModule module);
AtModuleTestRunner Af60150011ModuleEthTestRunnerNew(AtModule module);
AtModuleTestRunner Af60210031ModuleSdhTestRunnerNew(AtModule module);
AtModuleTestRunner Af60210031ModulePrbsTestRunnerNew(AtModule module);

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60210031ModulePdhTestRunnerNew(module);
    }

static AtModuleTestRunner EthTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60150011ModuleEthTestRunnerNew(module);
    }

static AtModuleTestRunner PwTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60210031ModulePwTestRunnerNew(module);
    }

static AtModuleTestRunner SdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60210031ModuleSdhTestRunnerNew(module);
    }

static AtModuleTestRunner PrbsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return Af60210031ModulePrbsTestRunnerNew(module);
    }

static void OverrideAtModuleTestRunnerFactory(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtModuleTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtModuleTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtModuleTestRunnerFactoryOverride));

        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, EthTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PwTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, SdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PrbsTestRunnerCreate);
        }

    self->methods = &m_AtModuleTestRunnerFactoryOverride;
    }

static void Override(AtModuleTestRunnerFactory self)
    {
    OverrideAtModuleTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210031ModuleTestRunnerFactory);
    }

AtModuleTestRunnerFactory Af60210031ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory Af60210031ModuleTestRunnerFactorySharedFactory()
    {
    static tAf60210031ModuleTestRunnerFactory sharedFactory;
    return Af60210031ModuleTestRunnerFactoryObjectInit((AtModuleTestRunnerFactory)&sharedFactory);
    }

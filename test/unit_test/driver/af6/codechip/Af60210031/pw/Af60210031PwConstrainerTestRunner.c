/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Af60210031PwConstrainerTestRunner.c
 *
 * Created Date: Feb 24, 2016
 *
 * Description : Constrainer test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210031PwConstrainerTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwConstrainerTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwConstrainerTestRunner Runner()
    {
    return (AtPwConstrainerTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtDevice Device(AtPwConstrainerTestRunner self)
    {
    return AtPwConstrainerTestRunnerDeviceGet(self);
    }

static AtModulePw ModulePw(AtPwConstrainerTestRunner self)
    {
    return (AtModulePw)AtDeviceModuleGet(Device(self), cAtModulePw);
    }

static AtModuleEth ModuleEth(AtPwConstrainerTestRunner self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static AtModulePdh ModulePdh(AtPwConstrainerTestRunner self)
    {
    return (AtModulePdh)AtDeviceModuleGet(Device(self), cAtModulePdh);
    }

static AtModulePwTestRunner ModulePwTestRunner(AtPwConstrainerTestRunner self)
    {
    return self->modulePwTestRunner;
    }

static AtPwConstrainer Constrainer(AtPwConstrainerTestRunner self)
    {
    return self->constrainer;
    }

static void QSGMIISAToPSetupE3(AtPwConstrainerTestRunner self, AtPdhDe3 de3)
    {
    AtPw pw = (AtPw)AtModulePwSAToPCreate(ModulePw(self), AtPwConstrainerTestRunnerNextPw(self));
    AtModulePwTestRunner moduleRunner = ModulePwTestRunner(self);

    AtAssert(pw);

    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhE3Unfrm) == cAtOk);
    AtPwConstrainerPwTypeSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), cAtPwConstrainerPwTypeSAToP);
    AtPwConstrainerTestRunnerPwNormalSetup(self, pw, (AtChannel)de3, AtModulePwTestEthPortForPdhChannel(moduleRunner, (AtPdhChannel)de3), cAtCircuitRateE3);
    AtPwConstrainerPwQueueSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), AtPwEthPortQueueGet(pw));
    }

static uint32 QSGMIISetup(AtPwConstrainerTestRunner self)
    {
    AtModulePdh pdhModule = ModulePdh(self);
    uint32 channel_i, numPw = 0;
    uint32 numChannels;

    AtPwConstrainerTestRunnerInit(self);
    if (AtEthPortInterfaceSet(AtModuleEthPortGet(ModuleEth(self), 0), cAtEthPortInterfaceQSgmii) != cAtOk)
        return 0;

    AtPwConstrainerPwEthPortTypeSet(Constrainer(self), 0, cAtPwConstrainerEthPortTypeQsgmii);
    numChannels = AtModulePdhNumberOfDe3sGet(pdhModule);
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        QSGMIISAToPSetupE3(self, AtModulePdhDe3Get(pdhModule, channel_i));
        numPw += 1;
        }

    return numPw;
    }

static void MultiplePwTestTearDown()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw(Runner());
    uint32 maxNumPw = AtModulePwMaxPwsGet(modulePw);
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < maxNumPw; pwId++)
        {
        AtPw pw = AtModulePwGetPw(modulePw, pwId);
        if (pw == NULL)
            continue;

        AtPwCircuitUnbind(pw);
        AtPwEthPortSet(pw, NULL);
        AtModulePwDeletePw(modulePw, pwId);
        AtPwConstrainerPwReset(constrainer, pwId);
        }
    }

static void QSGMIITest(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = QSGMIISetup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "QSGMII");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    QSGMIITest(mThis(self));
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)m_AtUnittestRunnerMethods, sizeof(m_AtUnittestRunnerOverride));
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210031PwConstrainerTestRunner);
    }

AtUnittestRunner Af60210031PwConstrainerTestRunnerObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwConstrainerTestRunnerObjectInit(self, modulePwTestRunner, constrainer) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner Af60210031PwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return Af60210031PwConstrainerTestRunnerObjectInit(newRunner, modulePwTestRunner, constrainer);
    }

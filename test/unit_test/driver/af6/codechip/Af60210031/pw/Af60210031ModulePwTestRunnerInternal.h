/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Af60210031ModulePwTestRunnerInternal.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_PW_AF60210031MODULEPWTESTRUNNERINTERNAL_H_
#define _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_PW_AF60210031MODULEPWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../pw/AtModulePwTestRunnerInternal.h"
#include "AtModulePdh.h"
#include "AtPdhSerialLine.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210031ModulePwTestRunner *Af60210031ModulePwTestRunner;
typedef struct tAf60210031ModulePwTestRunner
    {
    tAtModulePwTestRunner super;
    }tAf60210031ModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210031ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_PW_AF60210031MODULEPWTESTRUNNERINTERNAL_H_ */


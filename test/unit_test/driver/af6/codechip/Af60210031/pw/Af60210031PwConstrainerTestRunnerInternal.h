/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60210031PwConstrainerTestRunnerInternal.h
 * 
 * Created Date: Sep 12, 2016
 *
 * Description : PW constrainst
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210031PWCONSTRAINERTESTRUNNERINTERNAL_H_
#define _AF60210031PWCONSTRAINERTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../pw/AtPwConstrainerTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210031PwConstrainerTestRunner *Af60210031PwConstrainerTestRunner;

typedef struct tAf60210031PwConstrainerTestRunner
    {
    tAtPwConstrainerTestRunner super;
    } tAf60210031PwConstrainerTestRunner;
    
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner Af60210031PwConstrainerTestRunnerObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210031PWCONSTRAINERTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210031ModulePwTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PW module unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210031ModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CEPCanTestVc11(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 1024;
    }

static eBool CepCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    return line ? cAtTrue : cAtFalse;
    }

static eBool ShouldTestAu3Paths(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static void WillTestStmLine(AtModulePwTestRunner self, uint8 lineId)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(pdhModule, lineId);
    AtSdhChannel vc3;
    AtAssert(AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeEc1) == cAtOk);
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(AtModuleSdhLineGet(sdhModule, lineId), 0, 0);
    AtAssert(AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapC3) == cAtOk);
    }

static AtEthPort EthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, 0);
    }

static AtEthPort EthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, 0);
    }

static uint32 QueueForPw(AtModulePwTestRunner self, AtPw pw)
    {
    return AtChannelIdGet((AtChannel)pw) % 5;
    }

static eBool CanChangeLopsClearThreshold(AtModulePwTestRunner self)
    {
    return cAtFalse;
    }

static AtList MakePwsForGroupTest(AtModulePwTestRunner self, AtList pws)
    {
    uint32 startPwId, stopPwId;
    char circuitString[64];
    char pwString[64];

    /* Just make sure that we are in determined situation */
    mCliSuccessAssert(AtCliExecute("pdh serline mode 1-48 ds3"));

    /* DS3 */
    AtSprintf(pwString, "1-20");
    AtSprintf(circuitString, "de3.1-20");
    mCliSuccessAssert(AtCliExecute("pdh de3 framing 1-24 ds3_unframed"));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    /* DS1 */
    mCliSuccessAssert(AtCliExecute("pdh de3 framing 25-48 ds3_cbit_28ds1"));
    mCliSuccessAssert(AtCliExecute("pdh de1 framing 25.1.1-48.7.4 ds1_unframed"));
    startPwId = 21;
    stopPwId  = startPwId + (24 * 28) - 1;
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    AtSprintf(circuitString, "de1.25.1.1-48.7.4");
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    return pws;
    }

static eBool SAToPCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    return cAtTrue;
    }

static AtUnittestRunner ConstrainerTestRunnerCreate(AtModulePwTestRunner self)
    {
    AtPwConstrainer constrainer = AtPwConstrainerDs3CardNew();
    if (constrainer)
        return Af60210031PwConstrainerTestRunnerNew(self, constrainer);

    return NULL;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, CEPCanTestVc11);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CepCanTestOnLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestAu3Paths);
        mMethodOverride(m_AtModulePwTestRunnerOverride, WillTestStmLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForSdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForPdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, QueueForPw);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CanChangeLopsClearThreshold);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MakePwsForGroupTest);
        mMethodOverride(m_AtModulePwTestRunnerOverride, SAToPCanTestOnLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ConstrainerTestRunnerCreate);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210031ModulePwTestRunner);
    }

AtModuleTestRunner Af60210031ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210031ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210031ModulePwTestRunnerObjectInit(newRunner, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Af60210031ModuleSdhTestRunner.h
 * 
 * Created Date: Jun 18, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_SDH_AF60210031MODULESDHTESTRUNNER_H_
#define _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_SDH_AF60210031MODULESDHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../sdh/AtModuleSdhTestRunnerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210031ModuleSdhTestRunner
    {
    tAtModuleSdhTestRunner super;
    }tAf60210031ModuleSdhTestRunner;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210031ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);
#ifdef __cplusplus
}
#endif
#endif /* _TEST_UNIT_TEST_DRIVER_AF6_CODECHIP_AF60210031_SDH_AF60210031MODULESDHTESTRUNNER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60210031ModuleSdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhSerialLine.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"
#include "Af60210031ModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhTestRunnerMethods m_AtModuleSdhTestRunnerOverride;

/* Save super implementation */
static const tAtModuleSdhTestRunnerMethods *m_AtModuleSdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    return 48;
    }

static void SetUp(AtModuleSdhTestRunner self)
    {
    uint8 serialLineId;
    AtSdhChannel vc3;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    for (serialLineId = 0; serialLineId < AtModulePdhNumberOfDe3SerialLinesGet(pdhModule); serialLineId++)
        {
        AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(pdhModule, serialLineId);
        AtAssert(AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeEc1) == cAtOk);

        vc3 = (AtSdhChannel)AtSdhLineVc3Get(AtModuleSdhLineGet(sdhModule, serialLineId), 0, 0);
        AtAssert(AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3) == cAtOk);
        }
    }

static eBool SupportTtiMode1Byte(AtModuleSdhTestRunner self)
    {
    return cAtFalse;
    }

static uint8 DefaultLineRate(AtModuleSdhTestRunner self, uint8 lineId)
    {
    return cAtSdhLineRateStm0;
    }

static eBool PathTimAutoRdiEnabledAsDefault(AtModuleSdhTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtModuleSdhTestRunner(AtModuleTestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner) self;

    if (!m_methodsInit)
        {
        m_AtModuleSdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModuleSdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModuleSdhTestRunnerOverride));

        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TotalLines);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, SetUp);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, SupportTtiMode1Byte);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, DefaultLineRate);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, PathTimAutoRdiEnabledAsDefault);
        }

    mMethodsSet(runner, &m_AtModuleSdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleSdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210031ModuleSdhTestRunner);
    }

AtModuleTestRunner Af60210031ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210031ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210031ModuleSdhTestRunnerObjectInit(newRunner, module);
    }

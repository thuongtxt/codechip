/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290081ModuleEthTestRunner.c
 *
 * Created Date: Sep 23, 2019
 *
 * Description : 60290081 Module ETH test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60290021/eth/Af60290021ModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290081ModuleEthTestRunner
    {
    tAf60290021ModuleEthTestRunner super;
    }tAf60290081ModuleEthTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthTestRunnerMethods            m_AtModuleEthTestRunnerOverride;

/* Methods */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannelTestRunner CreateEthFlowTestRunner(AtModuleEthTestRunner self, AtEthFlow flow)
    {
    extern AtChannelTestRunner Af60290081EthFlowTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
    return Af60290081EthFlowTestRunnerNew((AtChannel)flow, (AtModuleTestRunner)self);
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, CreateEthFlowTestRunner);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290081ModuleEthTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021ModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290081ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

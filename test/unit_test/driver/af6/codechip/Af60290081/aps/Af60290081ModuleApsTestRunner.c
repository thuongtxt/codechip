/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290081ModuleApsTestRunner.c
 *
 * Created Date: Sep 23, 2019
 *
 * Description : 60290081 Module APS test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60290021/aps/Af60290021ModuleApsTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290081ModuleApsTestRunner
    {
    tAf60290021ModuleApsTestRunner super;
    }tAf60290081ModuleApsTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleApsTestRunnerMethods   m_AtModuleApsTestRunnerOverride;

/* Save super implementation */
static const tAtModuleApsTestRunnerMethods *m_AtModuleApsTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumUpsrEngines(AtModuleApsTestRunner self)
    {
    return 192;
    }

static uint32 MaxNumApsSelectors(AtModuleApsTestRunner self)
    {
    return 576;
    }

static void OverrideAtModuleApsTestRunner(AtModuleTestRunner self)
    {
    AtModuleApsTestRunner runner = (AtModuleApsTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtModuleApsTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtModuleApsTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleApsTestRunnerOverride));

        mMethodOverride(m_AtModuleApsTestRunnerOverride, MaxNumUpsrEngines);
        mMethodOverride(m_AtModuleApsTestRunnerOverride, MaxNumApsSelectors);
        }

    mMethodsSet(runner, &m_AtModuleApsTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleApsTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290081ModuleApsTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021ModuleApsTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290081ModuleApsTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290081ModulePwTestRunner.c
 *
 * Created Date: Sep 23, 2019
 *
 * Description : 60290081 Module PW unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60290021/pw/Af60290021ModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290081ModulePwTestRunner
    {
    tAf60290021ModulePwTestRunner super;
    }tAf60290081ModulePwTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePw Module()
    {
    return (AtModulePw)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    return 5376;
    }

static eAtRet EthFlowSetup(AtModulePwTestRunner self, AtPw pw)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)Module()), cAtModuleEth);
    AtEthFlow flow = AtModuleEthFlowCreate(moduleEth, AtChannelIdGet((AtChannel)pw));
    return AtPwEthFlowSet(pw, flow);
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumPws);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthFlowSetup);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290081ModulePwTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290081ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

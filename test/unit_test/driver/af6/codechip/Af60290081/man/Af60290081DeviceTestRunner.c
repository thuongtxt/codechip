/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290081DeviceTestRunner.c
 *
 * Created Date: Sep 20, 2019
 *
 * Description : Device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../physical/AtPhysicalTests.h"
#include "../../Af60291022/man/Af60291022DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290081DeviceTestRunner
    {
    tAf60291022DeviceTestRunner super;
    }tAf60290081DeviceTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods   m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    extern AtModuleTestRunnerFactory Af60290081ModuleTestRunnerFactorySharedFactory();
    return Af60290081ModuleTestRunnerFactorySharedFactory();
    }

static eBool ModuleNotReady(AtDeviceTestRunner self, eAtModule moduleId)
    {
    if ((moduleId == cAtModuleEncap)   ||
        (moduleId == cAtModuleConcate))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ModuleMustBeSupported(AtDeviceTestRunner self, eAtModule module)
    {
    switch (module)
        {
        case cAtModuleConcate    : return cAtTrue;
        case cAtModuleEncap      : return cAtTrue;

        /* Cannot determine at this time, need to ask super */
        default:
            return m_AtDeviceTestRunnerMethods->ModuleMustBeSupported(self, module);
        }
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleNotReady);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleMustBeSupported);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290081DeviceTestRunner);
    }

static AtDeviceTestRunner ObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60291022DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60290081DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Af60291011DeviceTestRunnerInternal.h
 * 
 * Created Date: Aug 27, 2018
 *
 * Description : 60291011 Device Test runner internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60291011DEVICETESTRUNNERINTERNAL_H_
#define _AF60291011DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60290011/man/Af60290011DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60291011DeviceTestRunner
    {
    tAf60290011DeviceTestRunner super;
    }tAf60291011DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60291011DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60291011DEVICETESTRUNNERINTERNAL_H_ */


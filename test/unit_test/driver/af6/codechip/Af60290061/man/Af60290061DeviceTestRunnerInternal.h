/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Af60290061DeviceTestRunnerInternal.h
 * 
 * Created Date: Aug 27, 2018
 *
 * Description : 60290061 Device Test runner internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290061DEVICETESTRUNNERINTERNAL_H_
#define _AF60290061DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60291011/man/Af60291011DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290061DeviceTestRunner
    {
    tAf60291011DeviceTestRunner super;
    }tAf60290061DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _AF60290061DEVICETESTRUNNERINTERNAL_H_ */


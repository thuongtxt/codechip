/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210011ModulePwTestRunner.c
 *
 * Created Date: May 21, 2015
 *
 * Description : PW unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210011ModulePwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 8 * 1024;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 4 * 1024;
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    return 8 * 1024;
    }

static eBool CEPCanTestVc11(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static eBool ShouldTestCESoP(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static AtModuleSdh SdhModule(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static AtList MakePwsForGroupTest(AtModulePwTestRunner self, AtList pws)
    {
    AtModuleSdh sdhModule = SdhModule(self);
    uint32 lineId = AtTestRandom(AtModuleSdhMaxLinesGet(sdhModule), 0);
    uint32 cliLineId = lineId + 1;
    uint32 startPwId, stopPwId;
    char circuitString[64];
    char pwString[64];

    /* Have VC-4s */
    AtSprintf(pwString, "1-2");
    AtSprintf(circuitString, "vc4.%d.1-%d.2", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.1-%d.2 vc4", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c4", circuitString));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* And VC-3s */
    AtSprintf(pwString, "3-8");
    AtSprintf(circuitString, "vc3.%d.3.1-%d.4.3", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.3-%d.4 3xvc3s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c3", circuitString));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* VC-12 */
    startPwId = 9;
    stopPwId = (3 * 21) + startPwId - 1;
    AtSprintf(circuitString, "vc1x.%d.5.1.1.1-%d.5.3.7.3", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.5 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.5.1-%d.5.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.5.1.1-%d.5.3.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.5.1.1.1-%d.5.3.7.3 c1x", cliLineId, cliLineId));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* VC-11 */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 28) + startPwId - 1;
    AtSprintf(circuitString, "vc1x.%d.6.1.1.1-%d.6.3.7.4", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.6 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.6.1-%d.6.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.6.1.1-%d.6.3.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.6.1.1.1-%d.6.3.7.4 c1x", cliLineId, cliLineId));
    AtModulePwTestSetupCepBasicWithCircuits(pws, pwString, circuitString);

    /* DS1 SAToP */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 28) + startPwId - 1;
    AtSprintf(circuitString, "de1.%d.7.1.1.1-%d.7.3.7.4", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.7 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.7.1-%d.7.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.7.1.1-%d.7.3.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.7.1.1.1-%d.7.3.7.4 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.7.1.1.1-%d.7.3.7.4 ds1_unframed", cliLineId, cliLineId));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    /* E1 SAToP */
    startPwId = stopPwId + 1;
    stopPwId = (3 * 21) + startPwId - 1;
    AtSprintf(circuitString, "de1.%d.8.1.1.1-%d.8.3.7.3", cliLineId, cliLineId);
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.8 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.8.1-%d.8.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.8.1.1-%d.8.3.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.8.1.1.1-%d.8.3.7.3 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.8.1.1.1-%d.8.3.7.3 e1_unframed", cliLineId, cliLineId));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    return pws;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumPws);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestCESoP);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CEPCanTestVc11);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MakePwsForGroupTest);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011ModulePwTestRunner);
    }

AtModuleTestRunner Af60210011ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60150011ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210011ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210011ModulePwTestRunnerObjectInit(newRunner, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60210011ModulePwTestRunner.h
 * 
 * Created Date: Nov 5, 2015
 *
 * Description : PW module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011MODULEPWTESTRUNNER_H_
#define _AF60210011MODULEPWTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60150011/pw/Af60150011ModulePwTestRunner.h"
#include "../sdh/Af60210011ModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011ModulePwTestRunner
    {
    tAf60150011ModulePwTestRunner super;
    }tAf60210011ModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210011ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011MODULEPWTESTRUNNER_H_ */


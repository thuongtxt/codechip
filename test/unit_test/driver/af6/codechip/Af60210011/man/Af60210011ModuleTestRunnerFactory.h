/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Af60210011ModuleTestRunnerFactory.h
 * 
 * Created Date: Nov 5, 2015
 *
 * Description : Module runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011MODULETESTRUNNERFACTORY_H_
#define _AF60210011MODULETESTRUNNERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../man/module_runner/AtModuleTestRunnerFactoryInternal.h"
#include "../../../../man/module_runner/AtModuleTestRunner.h"
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011ModuleTestRunnerFactory
    {
    tAtModuleTestRunnerFactory super;
    }tAf60210011ModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory Af60210011ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011MODULETESTRUNNERFACTORY_H_ */


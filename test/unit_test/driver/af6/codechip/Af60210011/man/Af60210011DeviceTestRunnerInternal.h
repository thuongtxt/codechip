/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Af60210011DeviceTestRunnerInternal.h
 * 
 * Created Date: Oct 31, 2015
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011DEVICETESTRUNNERINTERNAL_H_
#define _AF60210011DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60150011/man/Af60150011DeviceTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011DeviceTestRunner
    {
    tAtDeviceTestDefaultRunner super;
    }tAf60210011DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60210011DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011DEVICETESTRUNNERINTERNAL_H_ */


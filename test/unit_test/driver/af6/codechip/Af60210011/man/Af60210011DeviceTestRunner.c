/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210011DeviceTestRunner.c
 *
 * Created Date: May 04, 2015
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210011DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60210011ModuleTestRunnerFactorySharedFactory();

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60210011ModuleTestRunnerFactorySharedFactory();
    }

static eBool ModuleMustBeSupported(AtDeviceTestRunner self, eAtModule module)
    {
    switch (module)
        {
        case cAtModuleSdh        : return cAtTrue;
        case cAtModulePdh        : return cAtTrue;
        case cAtModuleEth        : return cAtTrue;
        case cAtModulePw         : return cAtTrue;
        case cAtModuleRam        : return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;
        case cAtModuleXc         : return cAtTrue;
        case cAtModulePrbs       : return cAtTrue;
        case cAtModuleBer        : return cAtTrue;
        case cAtModuleClock      : return cAtTrue;

        case cAtModulePpp        : return cAtFalse;
        case cAtModuleFr         : return cAtFalse;
        case cAtModuleEncap      : return cAtFalse;
        case cAtModuleIma        : return cAtFalse;
        case cAtModuleAtm        : return cAtFalse;
        case cAtModuleAps        : return cAtFalse;

        /* Cannot determine at this time, need to ask super */
        default:
            return m_AtDeviceTestRunnerMethods->ModuleMustBeSupported(self, module);
        }
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleMustBeSupported);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011DeviceTestRunner);
    }

AtDeviceTestRunner Af60210011DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60150011DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60210011DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210011DeviceTestRunnerObjectInit(newRunner, device);
    }

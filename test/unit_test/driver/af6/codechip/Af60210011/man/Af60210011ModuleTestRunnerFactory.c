/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210011ModuleTestRunnerFactory.c
 *
 * Created Date: May 04, 2015
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210011ModuleTestRunnerFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleTestRunnerFactoryMethods m_AtModuleTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtModuleTestRunnerFactoryMethods *m_AtModuleTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner SdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60210011ModuleSdhTestRunnerNew(AtModule module);
    return Af60210011ModuleSdhTestRunnerNew(module);
    }

static AtModuleTestRunner PwTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60210011ModulePwTestRunnerNew(AtModule module);
    return Af60210011ModulePwTestRunnerNew(module);
    }

static AtModuleTestRunner EthTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60150011ModuleEthTestRunnerNew(AtModule module);
    return Af60150011ModuleEthTestRunnerNew(module);
    }

static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60210011ModulePdhTestRunnerNew(AtModule module);
    return Af60210011ModulePdhTestRunnerNew(module);
    }

static void OverrideAtModuleTestRunnerFactory(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtModuleTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtModuleTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtModuleTestRunnerFactoryOverride));

        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, SdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PwTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, EthTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PdhTestRunnerCreate);
        }

    self->methods = &m_AtModuleTestRunnerFactoryOverride;
    }

static void Override(AtModuleTestRunnerFactory self)
    {
    OverrideAtModuleTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011ModuleTestRunnerFactory);
    }

AtModuleTestRunnerFactory Af60210011ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory Af60210011ModuleTestRunnerFactorySharedFactory()
    {
    static tAf60210011ModuleTestRunnerFactory sharedFactory;
    return Af60210011ModuleTestRunnerFactoryObjectInit((AtModuleTestRunnerFactory)&sharedFactory);
    }

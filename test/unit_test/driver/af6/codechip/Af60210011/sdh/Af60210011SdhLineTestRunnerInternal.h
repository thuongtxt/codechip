/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Af60210011SdhLineTestRunnerInternal.h
 * 
 * Created Date: Oct 10, 2016
 *
 * Description : SDH Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011SDHLINETESTRUNNERINTERNAL_H_
#define _AF60210011SDHLINETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011SdhLineTestRunner
    {
    tAtSdhLineTestRunner super;
    }tAf60210011SdhLineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner At60210011SdhLineTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011SDHLINETESTRUNNERINTERNAL_H_ */


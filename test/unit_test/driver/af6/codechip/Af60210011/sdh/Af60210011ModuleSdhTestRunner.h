/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Af60210011ModuleSdhTestRunner.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : SDH module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011MODULESDHTESTRUNNER_H_
#define _AF60210011MODULESDHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../sdh/AtModuleSdhTestRunnerInternal.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011ModuleSdhTestRunner * Af60210011ModuleSdhTestRunner;

typedef struct tAf60210011ModuleSdhTestRunnerMethods
    {
    void (*TestRetrieveTfi5Line)(Af60210011ModuleSdhTestRunner self);
    }tAf60210011ModuleSdhTestRunnerMethods;

typedef struct tAf60210011ModuleSdhTestRunner
    {
    tAtModuleSdhTestRunner super;
    const tAf60210011ModuleSdhTestRunnerMethods *methods;
    }tAf60210011ModuleSdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210011ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

eBool Af60210011ModuleSdhTestRunnerIsTfi5Line(AtSdhLine line);
eBool Af60210011ModuleSdhTestRunnerIsHoLine(AtSdhLine line);
eBool Af60210011ModuleSdhTestRunnerIsLoLine(AtSdhLine line);

uint8 Af60210011ModuleSdhTestRunnerAnyLoLine(void);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011MODULESDHTESTRUNNER_H_ */


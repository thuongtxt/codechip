/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60210011ModuleSdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../man/channel_runner/AtChannelTestRunner.h"
#include "Af60210011ModuleSdhTestRunner.h"
#include "AtCisco.h"
#include "AtSdhVc.h"
#include "AtSdhAug.h"

/*--------------------------- Define -----------------------------------------*/
/* Just to enforce the internal implementation of the driver */
#define cTfi5LineStartId 0
#define cTfi5LineEndId   7
#define cNumTfi5Lines    (cTfi5LineEndId - cTfi5LineStartId + 1)
#define cHoLineStartId   8
#define cHoLineEndId     15
#define cNumHoLines      (cHoLineEndId - cHoLineStartId + 1)
#define cLoLineStartId   16
#define cLoLineEndId     21
#define cNumLoLines      (cLoLineEndId - cLoLineStartId + 1)
#define cTotalLines      cNumTfi5Lines
#define cMaxNumLoLineVc3 cNumLoLines * 48

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAf60210011ModuleSdhTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods      m_AtUnittestRunnerOverride;
static tAtModuleSdhTestRunnerMethods m_AtModuleSdhTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods      *m_AtUnittestRunnerMethods      = NULL;
static const tAtModuleSdhTestRunnerMethods *m_AtModuleSdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtChannelTestRunner Af60210011SdhLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

/*--------------------------- Implementation ---------------------------------*/
static AtChannelTestRunner CreateLineTestRunner(AtModuleSdhTestRunner self, AtChannel line)
    {
    return Af60210011SdhLineTestRunnerNew(line, (AtModuleTestRunner)self);
    }

static eBool LineHasLoMapping(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    return cAtTrue;
    }

static eBool CanTestVc11(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    return mMethodsGet(self)->LineHasLoMapping(self, line);
    }

static void TestRetrieveTfi5Line(Af60210011ModuleSdhTestRunner self)
    {
    uint8 line_i = 0;
    AtModuleSdh sdhModule = (AtModuleSdh)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);

    for (line_i = 0; line_i < AtModuleSdhNumTfi5Lines(sdhModule); line_i++)
        {
        AtSdhLine line = AtModuleSdhTfi5LineGet(sdhModule, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT_EQUAL_INT(line_i, AtChannelIdGet((AtChannel)line));
        }
    }

static void testCanRetrieveTfi5Line(void)
    {
    Af60210011ModuleSdhTestRunner runner = (Af60210011ModuleSdhTestRunner)AtUnittestRunnerCurrentRunner();
    mMethodsGet(runner)->TestRetrieveTfi5Line(runner);
    }

static eAtSdhLineRate MaxLineRate(AtModuleSdhTestRunner self)
    {
    return cAtSdhLineRateStm16;
    }

static eBool ShouldTestLine(AtModuleSdhTestRunner self, uint8 lineId)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    return line ? cAtTrue : cAtFalse;
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60210011ModuleSdhTestRunner_Fixtures)
        {
        new_TestFixture("testCanRetrieveTfi5Line", testCanRetrieveTfi5Line)
        };

    EMB_UNIT_TESTCALLER(TestSuite_Af60210011ModuleSdhTestRunner_Caller, "Af60210011ModuleSdh", NULL, NULL, TestSuite_Af60210011ModuleSdhTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60210011ModuleSdhTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static uint32 NumTfi5s(AtModuleSdhTestRunner self)
    {
    return 8;
    }

static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    return 8;
    }

static eBool SupportTtiMode1Byte(AtModuleSdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool ShouldTestUpsr(AtModuleSdhTestRunner self)
    {
    return (AtTestIsSimulationTesting()) ? cAtFalse : cAtTrue;
    }

static AtUnittestRunner CreatePathUpsrTestRunner(AtModuleSdhTestRunner self, AtChannel path)
    {
    extern AtUnittestRunner Af60210011UpsrTestRunnerNew(AtChannel channel);
    return Af60210011UpsrTestRunnerNew(path);
    }

static uint8 DefaultLineRate(AtModuleSdhTestRunner self, uint8 lineId)
    {
    return cAtSdhLineRateStm16;
    }

static void MethodsInit(AtModuleTestRunner self)
    {
    Af60210011ModuleSdhTestRunner runner = (Af60210011ModuleSdhTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TestRetrieveTfi5Line);
        }

    mMethodsSet(runner, &m_methods);
    }

static void OverrideAtModuleSdhTestRunner(AtModuleTestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner) self;

    if (!m_methodsInit)
        {
        m_AtModuleSdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModuleSdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModuleSdhTestRunnerOverride));

        mMethodOverride(m_AtModuleSdhTestRunnerOverride, CreateLineTestRunner);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, CanTestVc11);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, LineHasLoMapping);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, ShouldTestLine);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, NumTfi5s);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TotalLines);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, SupportTtiMode1Byte);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, ShouldTestUpsr);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, CreatePathUpsrTestRunner);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, DefaultLineRate);
        }

    mMethodsSet(runner, &m_AtModuleSdhTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleSdhTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011ModuleSdhTestRunner);
    }

AtModuleTestRunner Af60210011ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210011ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return Af60210011ModuleSdhTestRunnerObjectInit(newRunner, module);
    }

eBool Af60210011ModuleSdhTestRunnerIsTfi5Line(AtSdhLine line)
    {
    uint8 lineId = AtChannelIdGet((AtChannel)line);
    return mOutOfRange(lineId, cTfi5LineStartId, cTfi5LineEndId) ? cAtFalse : cAtTrue;
    }

eBool Af60210011ModuleSdhTestRunnerIsHoLine(AtSdhLine line)
    {
    uint8 lineId = AtChannelIdGet((AtChannel)line);
    return mOutOfRange(lineId, cHoLineStartId, cHoLineEndId) ? cAtFalse : cAtTrue;
    }

eBool Af60210011ModuleSdhTestRunnerIsLoLine(AtSdhLine line)
    {
    uint8 lineId = AtChannelIdGet((AtChannel)line);
    return mOutOfRange(lineId, cLoLineStartId, cLoLineEndId) ? cAtFalse : cAtTrue;
    }

uint8 Af60210011ModuleSdhTestRunnerAnyLoLine(void)
    {
    static uint8 currentLineId = cLoLineStartId;
    uint8 lineId = currentLineId;
    currentLineId = (currentLineId + 1);
    if (currentLineId > cLoLineEndId)
        currentLineId = cLoLineStartId;
    return lineId;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Af60210011ModulePdhTestRunner.h
 * 
 * Created Date: Nov 5, 2015
 *
 * Description : Test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011MODULEPDHTESTRUNNER_H_
#define _AF60210011MODULEPDHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../pdh/AtModulePdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011ModulePdhTestRunner
    {
    tAtModulePdhTestRunner super;
    }tAf60210011ModulePdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210011ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011MODULEPDHTESTRUNNER_H_ */


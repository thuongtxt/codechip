/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60210011ModulePdhTestRunner.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : PDH unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210011ModulePdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;

/* Save super implementation */
static const tAtModulePdhTestRunnerMethods *m_AtModulePdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldTestMaximumNumberOfDe1Get(AtModulePdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool CanTestDe1Prm(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner) self;
    if (!m_methodsInit)
        {
        m_AtModulePdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestMaximumNumberOfDe1Get);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, CanTestDe1Prm);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011ModulePdhTestRunner);
    }

AtModuleTestRunner Af60210011ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210011ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return Af60210011ModulePdhTestRunnerObjectInit(newRunner, module);
    }

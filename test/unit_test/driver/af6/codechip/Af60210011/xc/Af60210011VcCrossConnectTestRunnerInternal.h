/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO
 * 
 * File        : Af60210011VcCrossConnectTestRunnerInternal.h
 * 
 * Created Date: Jul 15, 2016
 *
 * Description : TODO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011VCCROSSCONNECTTESTRUNNERINTERNAL_H_
#define _AF60210011VCCROSSCONNECTTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../xc/AtCrossConnectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011VcCrossConnectTestRunner
    {
    tAtCrossConnectTestRunner super;
    }tAf60210011VcCrossConnectTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011VCCROSSCONNECTTESTRUNNERINTERNAL_H_ */


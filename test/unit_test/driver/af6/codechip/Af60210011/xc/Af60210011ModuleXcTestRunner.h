/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Af60210011ModuleXcTestRunner.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : XC module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011MODULEXCTESTRUNNER_H_
#define _AF60210011MODULEXCTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011ModuleXcTestRunner * Af60210011ModuleXcTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXcTestRunner Af60210011ModuleXcTestRunnerNew(AtModule module);
uint8 Af60210011ModuleXcAnyLoLine(Af60210011ModuleXcTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011MODULEXCTESTRUNNER_H_ */


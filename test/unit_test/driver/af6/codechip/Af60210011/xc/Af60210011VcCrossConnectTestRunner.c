/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Af60210011VcCrossConnectTestRunner.c
 *
 * Created Date: May 23, 2015
 *
 * Description : Cross-connect test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../sdh/AtModuleSdhTests.h"
#include "../sdh/Af60210011ModuleSdhTestRunner.h"
#include "Af60210011VcCrossConnectTestRunnerInternal.h"
#include "Af60210011ModuleXcTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumStsPerLine 48

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static AtSdhChannel line1_channels[cMaxNumStsPerLine];
static AtSdhChannel line2_channels[cMaxNumStsPerLine];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtCrossConnectTestRunner Runner()
    {
    return (AtCrossConnectTestRunner)AtUnittestRunnerCurrentRunner();
    }

static Af60210011ModuleXcTestRunner XcTestRunner()
    {
    return (Af60210011ModuleXcTestRunner) AtCrossConnectTestRunnerModuleRunner(Runner());
    }

static AtCrossConnect Xc()
    {
    return AtCrossConnectTestRunnerCrossConnect(Runner());
    }

static AtModuleXc XcModule()
    {
    return (AtModuleXc)AtCrossConnectModuleGet(Xc());
    }

static AtModuleSdh SdhModule()
    {
    AtDevice device = AtModuleDeviceGet((AtModule)XcModule());
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eBool ChannelHasConnections(AtChannel channel)
    {
    if (AtCrossConnectNumDestChannelsGet(Xc(), channel) > 0)
        return cAtTrue;
    if (AtCrossConnectSourceChannelGet(Xc(), channel))
        return cAtTrue;
    return cAtFalse;
    }

static eBool ChannelHasNoConnection(AtChannel channel)
    {
    return ChannelHasConnections(channel) ? cAtFalse : cAtTrue;
    }

static void TestDisconnectTwoVcs(AtChannel source, AtChannel dest)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtCrossConnectChannelDisconnect(Xc(), source, dest));

    TEST_ASSERT_EQUAL_INT(0, AtCrossConnectNumDestChannelsGet(Xc(), source));
    TEST_ASSERT_NULL(AtCrossConnectSourceChannelGet(Xc(), dest));
    }

static void TestConnectTwoVcs(AtChannel source, AtChannel dest)
    {
    /* Try connecting for more than one times */
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtCrossConnectChannelConnect(Xc(), source, dest));
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtCrossConnectChannelConnect(Xc(), source, dest));

    TEST_ASSERT_EQUAL_INT(1, AtCrossConnectNumDestChannelsGet(Xc(), source));
    TEST_ASSERT(AtCrossConnectDestChannelGetByIndex(Xc(), source, 0) == dest);

    if (source != dest)
        {
        TEST_ASSERT_EQUAL_INT(0, AtCrossConnectNumDestChannelsGet(Xc(), dest));
        TEST_ASSERT(AtCrossConnectSourceChannelGet(Xc(), dest) == source);
        }
    }

static void TestConnectDisconnectTwoVcs(AtChannel channel1, AtChannel channel2)
    {
    /* Make sure that they are in fresh state */
    TEST_ASSERT(ChannelHasNoConnection(channel1));
    TEST_ASSERT(ChannelHasNoConnection(channel2));

    TestConnectTwoVcs(channel1, channel2);
    TestDisconnectTwoVcs(channel1, channel2);

    TestConnectTwoVcs(channel2, channel1);
    TestDisconnectTwoVcs(channel2, channel1);
    }

static void TestConnectDisconnectHoVcs(AtSdhLine line1, AtSdhLine line2, uint16 (*VcCreateFunc)(AtSdhLine line, AtSdhChannel *channels))
    {
    uint16 line1Vc_i, line2Vc_i;
    uint16 line1_numVcs, line2_numVcs;

    /* Make sure they are in fresh state */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInit((AtChannel)line1));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInit((AtChannel)line2));

    /* Mapping them */
    line1_numVcs = VcCreateFunc(line1, line1_channels);
    line2_numVcs = VcCreateFunc(line2, line2_channels);
    TEST_ASSERT_EQUAL_INT(line1_numVcs, line2_numVcs);
    TEST_ASSERT(line1_numVcs > 0);

    /* Cross them */
    for (line1Vc_i = 0; line1Vc_i < line1_numVcs; line1Vc_i++)
        {
        AtChannel line1Vc = (AtChannel)line1_channels[line1Vc_i];

        /* Can connect/disconnect itself */
        TestConnectDisconnectTwoVcs(line1Vc, line1Vc);

        for (line2Vc_i = 0; line2Vc_i < line1_numVcs; line2Vc_i++)
            {
            AtChannel line2Vc = (AtChannel)line2_channels[line2Vc_i];

            TestConnectDisconnectTwoVcs(line2Vc, line2Vc);
            TestConnectDisconnectTwoVcs(line1Vc, line2Vc);
            }
        }
    }

static uint8 MaxNumLines(void)
    {
    return AtModuleSdhNumTfi5Lines(SdhModule());
    }

static void testCanConnectDisconnectHoVc()
    {
    uint8 numLines = MaxNumLines();
    uint8 line_i, line_j;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line1 = AtModuleSdhLineGet(SdhModule(), line_i);
        if (line1 == NULL)
        	continue;

        for (line_j = 0; line_j < numLines; line_j++)
            {
            AtSdhLine line2 = AtModuleSdhLineGet(SdhModule(), line_j);
            if (line2 == NULL)
            	continue;

            TestConnectDisconnectHoVcs(line1, line2, AtSdhTestCreateAllVc3sInAu3s);
            TestConnectDisconnectHoVcs(line1, line2, AtSdhTestCreateAllVc4s);
            }
        }
    }

static uint8 AnyLine()
    {
    static uint8 currentLineId = 0;
    uint8 line_i;

    for (line_i = 0; line_i < MaxNumLines(); line_i++)
        {
        currentLineId = (currentLineId + 1) % MaxNumLines();

        if (AtModuleSdhLineGet(SdhModule(), currentLineId))
            break;
        }

    return currentLineId;
    }

static uint8 AnyLoLine()
    {
    return Af60210011ModuleXcAnyLoLine(XcTestRunner());
    }

static void testCanConnectToMoreThanOneSources()
    {
    AtSdhLine line1 = AtModuleSdhLineGet(SdhModule(), AnyLine());
    AtSdhLine line2 = AtModuleSdhLineGet(SdhModule(), AnyLine());
    uint16 line1NumChannels = AtSdhTestCreateAllVc4s(line1, line1_channels);
    uint16 line2NumChannels = AtSdhTestCreateAllVc4s(line2, line2_channels);
    uint16 channel_i;
    AtChannel source;

    /* Just pick one channel from line 1 and cross it to all of channels of line 2 */
    source = (AtChannel)line1_channels[line1NumChannels / 2];
    for (channel_i = 0; channel_i < line2NumChannels; channel_i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtCrossConnectChannelConnect(Xc(), source, (AtChannel)line2_channels[channel_i]));
        }

    /* Make sure that they are all connected */
    TEST_ASSERT_EQUAL_INT(line2NumChannels, AtCrossConnectNumDestChannelsGet(Xc(), source));
    for (channel_i = 0; channel_i < line2NumChannels; channel_i++)
        {
        TEST_ASSERT(AtCrossConnectSourceChannelGet(Xc(), (AtChannel)line2_channels[channel_i]) == source);
        }
    }

static void testCannotConnectToAChannelWhichIsBeingConnectedByOtherChannel()
    {
    AtSdhLine line1 = AtModuleSdhLineGet(SdhModule(), AnyLine());
    AtSdhLine line2 = AtModuleSdhLineGet(SdhModule(), AnyLine());
    uint16 line1NumChannels = AtSdhTestCreateAllVc4s(line1, line1_channels);
    uint16 line2NumChannels = AtSdhTestCreateAllVc4s(line2, line2_channels);

    TEST_ASSERT(line1NumChannels > 0);
    TEST_ASSERT(line2NumChannels > 0);

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)line1_channels[0], (AtChannel)line2_channels[1]));
    TEST_ASSERT((eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)line1_channels[1], (AtChannel)line2_channels[1]) != cAtOk);
    }

static void testCannotConnectVc4AndVc3()
    {
    AtSdhLine line1 = AtModuleSdhLineGet(SdhModule(), AnyLine());
    AtSdhLine line2 = AtModuleSdhLineGet(SdhModule(), AnyLine());
    uint16 line1NumChannels = AtSdhTestCreateAllVc4s(line1, line1_channels);
    uint16 line2NumChannels = AtSdhTestCreateAllVc3sInAu3s(line2, line2_channels);
    uint16 numChannels = mMin(line1NumChannels, line2NumChannels);
    uint16 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        TEST_ASSERT((eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)line1_channels[channel_i], (AtChannel)line2_channels[channel_i]) != cAtOk);
        TEST_ASSERT((eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)line2_channels[channel_i], (AtChannel)line1_channels[channel_i]) != cAtOk);
        }
    }

static void testCannotConnectVC1x()
    {
    AtSdhLine loLine1 = AtModuleSdhLineGet(SdhModule(), AnyLoLine());
    AtSdhLine loLine2 = AtModuleSdhLineGet(SdhModule(), AnyLoLine());

    AtSdhTestCreateAllVc1xInTug3s(loLine1, cAtSdhChannelTypeVc12);
    AtSdhTestCreateAllVc1xInTug3s(loLine2, cAtSdhChannelTypeVc12);

    TEST_ASSERT((eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)AtSdhLineVc1xGet(loLine1, 0, 0, 0, 0), (AtChannel)AtSdhLineVc1xGet(loLine2, 0, 0, 0, 0)) != cAtOk);
    }

static void testCannotConnectLoVc3()
    {
    AtSdhLine loLine1 = AtModuleSdhLineGet(SdhModule(), AnyLoLine());
    AtSdhLine loLine2 = AtModuleSdhLineGet(SdhModule(), AnyLoLine());
    uint16 line1NumVcs = AtSdhTestCreateAllVc3sInTug3s(loLine1, line1_channels);
    uint16 line2NumVcs = AtSdhTestCreateAllVc3sInTug3s(loLine2, line2_channels);

    TEST_ASSERT((eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)line1_channels[0], (AtChannel)line2_channels[0]) != cAtOk);
    TEST_ASSERT((eAtRet)AtCrossConnectChannelConnect(Xc(), (AtChannel)line1_channels[line1NumVcs / 2], (AtChannel)line2_channels[line2NumVcs / 2]) != cAtOk);
    }

static void setup()
    {
    AtAssert(AtModuleInit((AtModule)XcModule())  == cAtOk);
    AtAssert(AtModuleInit((AtModule)SdhModule()) == cAtOk);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_At60210011Xc_Fixtures)
        {
        new_TestFixture("testCanConnectDisconnectHoVc", testCanConnectDisconnectHoVc),
        new_TestFixture("testCanConnectToMoreThanOneSources", testCanConnectToMoreThanOneSources),
        new_TestFixture("testCannotConnectToAChannelWhichIsBeingConnectedByOtherChannel", testCannotConnectToAChannelWhichIsBeingConnectedByOtherChannel),
        new_TestFixture("testCannotConnectVc4AndVc3", testCannotConnectVc4AndVc3),
        new_TestFixture("testCannotConnectVC1x", testCannotConnectVC1x),
        new_TestFixture("testCannotConnectLoVc3", testCannotConnectLoVc3),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_At60210011Xc_Caller, "TestSuite_At60210011Xc", setup, NULL, TestSuite_At60210011Xc_Fixtures);

    return (TestRef)((void *)&TestSuite_At60210011Xc_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtCrossConnectTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtCrossConnectTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011VcCrossConnectTestRunner);
    }

AtCrossConnectTestRunner Af60210011VcCrossConnectTestRunnerObjectInit(AtCrossConnectTestRunner self, AtModuleXcTestRunner moduleRunner, AtCrossConnect xc)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtCrossConnectTestRunnerObjectInit(self, moduleRunner, xc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtCrossConnectTestRunner Af60210011VcCrossConnectTestRunnerNew(AtModuleXcTestRunner moduleRunner, AtCrossConnect xc)
    {
    AtCrossConnectTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210011VcCrossConnectTestRunnerObjectInit(newRunner, moduleRunner, xc);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Af60210011ModuleXcTestRunnerInternal.h
 * 
 * Created Date: Jul 15, 2016
 *
 * Description : XC Module test for af60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210011MODULEXCTESTRUNNERINTERNAL_H_
#define _AF60210011MODULEXCTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../xc/AtModuleXcTestRunnerInternal.h"
#include "Af60210011ModuleXcTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210011ModuleXcTestRunnerMethods
    {
    uint8 (*AnyLoLine)(Af60210011ModuleXcTestRunner self);
    }tAf60210011ModuleXcTestRunnerMethods;


typedef struct tAf60210011ModuleXcTestRunner
    {
    tAtModuleXcTestRunner super;
    const tAf60210011ModuleXcTestRunnerMethods *methods;
    }tAf60210011ModuleXcTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXcTestRunner Af60210011ModuleXcTestRunnerObjectInit(AtModuleXcTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210011MODULEXCTESTRUNNERINTERNAL_H_ */


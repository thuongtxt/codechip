/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210011ModulePwTestRunner.c
 *
 * Created Date: May 04, 2015
 *
 * Description : PW module unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210011ModuleXcTestRunnerInternal.h"
#include "../sdh/Af60210011ModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAf60210011ModuleXcTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods     m_AtUnittestRunnerOverride;
static tAtModuleXcTestRunnerMethods m_AtModuleXcTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule()
    {
    AtModuleTestRunner runner = (AtModuleTestRunner)AtUnittestRunnerCurrentRunner();
    return (AtModuleXc)AtModuleTestRunnerModuleGet(runner);
    }

static void testOnlyVcXcIsSupported()
    {
    TEST_ASSERT_NOT_NULL(AtModuleXcVcCrossConnectGet(XcModule()));
    TEST_ASSERT_NULL(AtModuleXcDe1CrossConnectGet(XcModule()));
    TEST_ASSERT_NULL(AtModuleXcDe3CrossConnectGet(XcModule()));
    TEST_ASSERT_NULL(AtModuleXcDs0CrossConnectGet(XcModule()));
    }

static AtCrossConnectTestRunner CreateVcXcTestRunner(AtModuleXcTestRunner self, AtCrossConnect xc)
    {
    extern AtCrossConnectTestRunner Af60210011VcCrossConnectTestRunnerNew(AtModuleXcTestRunner moduleRunner, AtCrossConnect xc);
    return Af60210011VcCrossConnectTestRunnerNew(self, xc);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60210011ModuleXc_Fixtures)
        {
        new_TestFixture("testOnlyVcXcIsSupported", testOnlyVcXcIsSupported)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af60210011ModuleXc_Caller, "TestSuite_Af60210011ModuleXc", NULL, NULL, TestSuite_Af60210011ModuleXc_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60210011ModuleXc_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static uint8 AnyLoLine(Af60210011ModuleXcTestRunner self)
    {
    return Af60210011ModuleSdhTestRunnerAnyLoLine();
    }

static void OverrideAtModuleXcTestRunner(AtModuleXcTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleXcTestRunnerOverride, mMethodsGet(self), sizeof(m_AtModuleXcTestRunnerOverride));

        mMethodOverride(m_AtModuleXcTestRunnerOverride, CreateVcXcTestRunner);
        }

    mMethodsSet(self, &m_AtModuleXcTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtModuleXcTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleXcTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    OverrideAtModuleXcTestRunner(self);
    }

static void MethodsInit(AtModuleXcTestRunner self)
    {
    Af60210011ModuleXcTestRunner runner = (Af60210011ModuleXcTestRunner) self;

    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, AnyLoLine);
        }

    mMethodsSet(runner, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011ModuleXcTestRunner);
    }

AtModuleXcTestRunner Af60210011ModuleXcTestRunnerObjectInit(AtModuleXcTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleXcTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXcTestRunner Af60210011ModuleXcTestRunnerNew(AtModule module)
    {
    AtModuleXcTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210011ModuleXcTestRunnerObjectInit(newRunner, module);
    }

uint8 Af60210011ModuleXcAnyLoLine(Af60210011ModuleXcTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->AnyLoLine(self);
    return 0;
    }

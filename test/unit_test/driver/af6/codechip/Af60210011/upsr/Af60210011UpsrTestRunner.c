/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60210011UpsrTestRunner.c
 *
 * Created Date: Mar 1, 2016
 *
 * Description : UPSR tesr runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdint.h>
#include "AtCiscoUpsr.h"
#include "AtSdhPath.h"
#include "AtTests.h"
#include "../../../../runner/AtUnittestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAf60210011UpsrTestRunner *)self)
#define cNumBitsInDword         32
#define cSwingOffsetInMs        10
#define cDefaultTimeoutInMs     2000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210011UpsrTestRunner * Af60210011UpsrTestRunner;

typedef struct tAf60210011UpsrTestRunner
    {
    tAtUnittestRunner super;

    /* Private data */
    AtSdhPath testedChannel;
    uint32 upsrAlarms;
    AtCiscoUpsr upsrController;
    }tAf60210011UpsrTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Af60210011UpsrTestRunner CurrentRunner(void)
    {
    return (Af60210011UpsrTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtSdhPath TestedChannel()
    {
    return mThis(AtUnittestRunnerCurrentRunner())->testedChannel;
    }

static uint32_t DeviceVirtualAddress(AtDevice device)
    {
    return (uint32_t)(AtHalBaseAddressGet(AtDeviceIpCoreHalGet(device, 0)) + (0x2F0000 << 2));
    }

static uint32_t DeviceMetaBaseAddress(AtDevice device)
    {
    return (uint32_t)AtHalBaseAddressGet(AtDeviceIpCoreHalGet(device, 0));
    }

static eBool IsHoVc(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeVc4_16c ||
        channelType == cAtSdhChannelTypeVc4_4c ||
        channelType == cAtSdhChannelTypeVc4 ||
        channelType == cAtSdhChannelTypeVc4_nc)
        return cAtTrue;

    if (channelType == cAtSdhChannelTypeVc3)
        return (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)path)) == cAtSdhChannelTypeAu3) ? cAtTrue: cAtFalse;

    return cAtFalse;
    }

static eBool IsLoVc(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeVc12 ||
        channelType == cAtSdhChannelTypeVc11)
        return cAtTrue;

    if (channelType == cAtSdhChannelTypeVc3)
        return (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)path)) == cAtSdhChannelTypeTu3) ? cAtTrue: cAtFalse;

    return cAtFalse;
    }

static eBool IsAu(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeAu4_16c ||
        channelType == cAtSdhChannelTypeAu4_4c ||
        channelType == cAtSdhChannelTypeAu4 ||
        channelType == cAtSdhChannelTypeAu3 ||
        channelType == cAtSdhChannelTypeAu4_nc)
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsTu(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeTu3  ||
        channelType == cAtSdhChannelTypeTu12 ||
        channelType == cAtSdhChannelTypeTu11)
        return cAtTrue;
    return cAtFalse;
    }

static eBool PathIsValid(AtSdhPath path)
    {
    return IsHoVc(path) || IsLoVc(path) || IsAu(path) || IsTu(path);
    }

static uint32 AuIndexGet(AtSdhPath sdhAu)
    {
    uint8 lineId = AtSdhChannelLineGet((AtSdhChannel)sdhAu);
    uint8 stsId = AtSdhChannelSts1Get((AtSdhChannel)sdhAu);
    return (uint32)(lineId * 48 + stsId);
    }

static uint32 TuIndexGet(AtSdhPath sdhTu)
    {
    uint8 lineId = AtSdhChannelLineGet((AtSdhChannel)sdhTu);
    uint8 stsId = AtSdhChannelSts1Get((AtSdhChannel)sdhTu);
    uint32 vtIndex = (uint32)((lineId * 48 + stsId) * 28);
    vtIndex += AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)sdhTu)) * 4UL + AtChannelIdGet((AtChannel)sdhTu);
    return vtIndex;
    }

static uint32 HoVcIndexGet(AtSdhPath sdhVc)
    {
    AtSdhPath sdhAu = (AtSdhPath)AtSdhChannelParentChannelGet((AtSdhChannel)sdhVc);
    return AuIndexGet(sdhAu);
    }

static uint32 LoVcIndexGet(AtSdhPath sdhVc)
    {
    AtSdhPath sdhTu = (AtSdhPath)AtSdhChannelParentChannelGet((AtSdhChannel)sdhVc);
    return TuIndexGet(sdhTu);
    }

static uint32 ChannelRow(AtSdhPath path)
    {
    if (IsAu(path))
        return AuIndexGet(path) / 32;

    if (IsHoVc(path))
        return HoVcIndexGet(path) / 32;

    if (IsTu(path))
        return TuIndexGet(path) / 28;

    if (IsLoVc(path))
        return LoVcIndexGet(path) / 28;

    return cBit31_0;
    }

static uint32 ChannelBit(AtSdhPath path)
    {
    if (IsAu(path))
        return cBit0 << (AuIndexGet(path) % 32);

    if (IsHoVc(path))
        return cBit0 << (HoVcIndexGet(path) % 32);

    if (IsTu(path))
        return cBit0 << (TuIndexGet(path) % 28);

    if (IsLoVc(path))
        return cBit0 << (LoVcIndexGet(path) % 28);

    return 0;
    }

static AtCiscoUpsr UpsrController(void)
    {
    AtUnittestRunner self = AtUnittestRunnerCurrentRunner();
    return mThis(self)->upsrController;
    }

static eAtRet UpsrInterruptMaskSet(AtSdhPath path, uint32 defectMask, uint32 enableMask)
    {
    uint32 rowIndex = ChannelRow(path);
    uint32 bitMask = ChannelBit(path);
    uint32 regVal;
    uint32_t (*MaskRead)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32_t) = NULL;
    void (*MaskWrite)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32_t, uint32_t) = NULL;

    if (!PathIsValid(path))
        return cAtErrorInvlParm;

    if (IsAu(path) || IsHoVc(path))
        {
        MaskRead = AtCiscoUpsrStsMaskRead;
        MaskWrite = AtCiscoUpsrStsMaskWrite;
        }

    if (IsTu(path) || IsLoVc(path))
        {
        MaskRead = AtCiscoUpsrVtMaskRead;
        MaskWrite = AtCiscoUpsrVtMaskWrite;
        }

    if ((MaskRead == NULL) || (MaskWrite == NULL))
        return cAtErrorInvlParm;

    if (defectMask & cAtCiscoUpsrAlarmSf)
        {
        regVal = MaskRead(UpsrController(), cAtCiscoUpsrAlarmSf, rowIndex);
        regVal = (enableMask & cAtCiscoUpsrAlarmSf) ? (regVal | bitMask) : (regVal & ~bitMask);
        MaskWrite(UpsrController(), cAtCiscoUpsrAlarmSf, rowIndex, regVal);
        }

    if (defectMask & cAtCiscoUpsrAlarmSd)
        {
        regVal = MaskRead(UpsrController(), cAtCiscoUpsrAlarmSd, rowIndex);
        regVal = (enableMask & cAtCiscoUpsrAlarmSd) ? (regVal | bitMask) : (regVal & ~bitMask);
        MaskWrite(UpsrController(), cAtCiscoUpsrAlarmSd, rowIndex, regVal);
        }

    return cAtOk;
    }

static uint32 UpsrInterruptMaskGet(AtSdhPath path)
    {
    uint32 rowIndex = ChannelRow(path);
    uint32 bitMask = ChannelBit(path);
    uint32 regVal;
    uint32_t (*MaskRead)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32_t) = NULL;
    uint32 defectMask = 0;

    if (!PathIsValid(path))
        return 0;

    if (IsAu(path) || IsHoVc(path))
        MaskRead = AtCiscoUpsrStsMaskRead;

    if (IsTu(path) || IsLoVc(path))
        MaskRead = AtCiscoUpsrVtMaskRead;

    /* Impossible, but to make static analysis tool be happy */
    if (MaskRead == NULL)
        return 0x0;

    regVal = MaskRead(UpsrController(), cAtCiscoUpsrAlarmSf, rowIndex);
    if (regVal & bitMask)
        defectMask |= cAtCiscoUpsrAlarmSf;

    regVal = MaskRead(UpsrController(), cAtCiscoUpsrAlarmSd, rowIndex);
    if (regVal & bitMask)
        defectMask |= cAtCiscoUpsrAlarmSd;

    return defectMask;
    }

static uint32 UpsrDefectGet(AtSdhPath path)
    {
    uint32_t rowIndex = ChannelRow(path);
    uint32_t bitMask = ChannelBit(path);
    uint32_t regVal;
    uint32 defectStat = 0;
    uint32_t (*StatusRead)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32_t) = NULL;

    if (!PathIsValid(path))
        return 0;

    if (IsAu(path) || IsHoVc(path))
        StatusRead = AtCiscoUpsrStsStatusRead;

    if (IsTu(path) || IsLoVc(path))
        StatusRead = AtCiscoUpsrVtStatusRead;

    /* Impossible, but to make static analysis tool be happy */
    if (StatusRead == NULL)
        return 0x0;

    regVal = StatusRead(UpsrController(), cAtCiscoUpsrAlarmSf, rowIndex);
    if (regVal & bitMask)
        defectStat |= cAtCiscoUpsrAlarmSf;

    regVal = StatusRead(UpsrController(), cAtCiscoUpsrAlarmSd, rowIndex);
    if (regVal & bitMask)
        defectStat |= cAtCiscoUpsrAlarmSd;

    return defectStat;
    }

static void StsUpsrNotify(uint32_t baseAddress, void *data, uint32_t stsId, eAtCiscoUpsrAlarm changedAlarms, eAtCiscoUpsrAlarm currentStatus)
    {
    Af60210011UpsrTestRunner self = mThis(data) ;
    self->upsrAlarms = ((self->upsrAlarms & (~changedAlarms)) | currentStatus);
    }

static void VtUpsrNotify(uint32_t baseAddress, void *data, uint32_t stsId, uint32_t vtId, eAtCiscoUpsrAlarm changedAlarms, eAtCiscoUpsrAlarm currentStatus)
    {
    Af60210011UpsrTestRunner self = mThis(data) ;
    self->upsrAlarms = ((self->upsrAlarms & (~changedAlarms)) | currentStatus);
    }

static eBool UpsrNotificationEnable(eBool enable)
    {
    static tAtCiscoUpsrListener listener;

    listener.StsInterruptChanged = StsUpsrNotify;
    listener.VtInterruptChanged = VtUpsrNotify;

    if (enable)
        AtCiscoUpsrListenerAdd(UpsrController(), CurrentRunner(), &listener);
    else
        AtCiscoUpsrListenerRemove(UpsrController(), CurrentRunner(), &listener);

    return cAtTrue;
    }

static uint32 ForceableAlarms(eAtCiscoUpsrAlarm alarm)
    {
    if (alarm == cAtCiscoUpsrAlarmSd)
        return cAtSdhPathAlarmBerSd;

    return cAtSdhPathAlarmLop | cAtSdhPathAlarmAis | cAtSdhPathAlarmPlm;
    }

static void Force(eAtSdhPathAlarmType alarm, eBool force)
    {
    AtChannel channel = (AtChannel)TestedChannel();

    /* TODO: need to think how to test BER SD */
    if (alarm == cAtSdhPathAlarmBerSd)
        {
        return;
        }

    if (alarm == cAtSdhPathAlarmLop)
        {
        AtSdhPath path = TestedChannel();
        uint8 ssBit = AtSdhPathTxSsGet(path);
        if (force)
            {
            AtSdhPathExpectedSsSet(path, (ssBit == 0) ? (ssBit + 1) : (ssBit - 1));
            AtSdhPathSsCompareEnable(path, cAtTrue);
            }
        else
            AtSdhPathExpectedSsSet(path, ssBit);
        return;
        }

    if (alarm == cAtSdhPathAlarmPlm)
        {
        AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)TestedChannel(), 0);
        uint8 psl = AtSdhPathExpectedPslGet(vc);
        if (force)
            AtSdhPathTxPslSet(vc, psl + 3);
        else
            AtSdhPathTxPslSet(vc, psl);
        return;
        }

    if (force)
        AtChannelTxAlarmForce(channel, alarm);
    else
        AtChannelTxAlarmUnForce(channel, alarm);
    }

static void InterruptProcess(void)
    {
    AtOsalUSleep(cSwingOffsetInMs);
    AtCiscoUpsrInterruptProcess(UpsrController());
    }

static uint32 InterruptProcessWithResult(uint32 *alarm, uint32 mask, uint32 expectedValue)
    {
    uint32 elapseTime = 0;
    tAtOsalCurTime curTime, startTime;

    AtOsalCurTimeGet(&startTime);
    while (elapseTime < cDefaultTimeoutInMs)
        {
        InterruptProcess();
        if ((*alarm & mask) == expectedValue)
            break;

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return elapseTime;
    }

static void testInterruptCanWork(eAtCiscoUpsrAlarm upsrAlarm)
    {
    AtSdhPath path = (AtSdhPath)TestedChannel();
    uint32 holdOffTimer = AtSdhChannelHoldOffTimerGet((AtSdhChannel)path);
    uint32 forcableAlarm = ForceableAlarms(upsrAlarm);
    Af60210011UpsrTestRunner runner = CurrentRunner();
    tAtOsalCurTime curTime, startTime;
    uint32 bit_i;

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 elapseTime = 0;
        uint32 forceAlarm = cBit0 << bit_i;

        if ((forceAlarm & forcableAlarm) == 0)
            continue;

        Force(forceAlarm, cAtTrue);
        AtOsalCurTimeGet(&startTime);
        while (elapseTime < (holdOffTimer - cSwingOffsetInMs))
            {
            InterruptProcess();

            /* Alarm must not happen during the hold-off time */
            TEST_ASSERT((UpsrDefectGet(path) & upsrAlarm) == 0);
            TEST_ASSERT((runner->upsrAlarms & upsrAlarm) == 0);

            AtOsalCurTimeGet(&curTime);
            elapseTime = mTimeIntervalInMsGet(startTime, curTime);
            }

        /* Alarm must happen now */
        elapseTime = InterruptProcessWithResult(&runner->upsrAlarms, upsrAlarm, upsrAlarm);
        TEST_ASSERT((UpsrDefectGet(path) & upsrAlarm) == upsrAlarm);
        TEST_ASSERT((runner->upsrAlarms & upsrAlarm) == upsrAlarm);

        Force(forceAlarm, cAtFalse);
        elapseTime = InterruptProcessWithResult(&runner->upsrAlarms, upsrAlarm, 0);

        /* Alarm must be clear now */
        TEST_ASSERT((UpsrDefectGet(path) & upsrAlarm) == 0);
        TEST_ASSERT((runner->upsrAlarms & upsrAlarm) == 0);
        }
    }

static void TestCanSetUpsrMask(void)
    {
    AtSdhPath channel = TestedChannel();
    UpsrInterruptMaskSet(channel, cAtCiscoUpsrAlarmSd | cAtCiscoUpsrAlarmSf, 0);
    TEST_ASSERT(UpsrInterruptMaskGet(channel) == 0);

    UpsrInterruptMaskSet(channel, cAtCiscoUpsrAlarmSd, cAtCiscoUpsrAlarmSd);
    TEST_ASSERT(UpsrInterruptMaskGet(channel) == cAtCiscoUpsrAlarmSd);
    UpsrInterruptMaskSet(channel, cAtCiscoUpsrAlarmSd, 0);
    TEST_ASSERT(UpsrInterruptMaskGet(channel) == 0);

    UpsrInterruptMaskSet(channel, cAtCiscoUpsrAlarmSf, cAtCiscoUpsrAlarmSf);
    TEST_ASSERT(UpsrInterruptMaskGet(channel) == cAtCiscoUpsrAlarmSf);
    UpsrInterruptMaskSet(channel, cAtCiscoUpsrAlarmSf, 0);
    TEST_ASSERT(UpsrInterruptMaskGet(channel) == 0);
    }

static void TestSfInterruptCanWork(void)
    {
    testInterruptCanWork(cAtCiscoUpsrAlarmSf);
    }

/* TODO: need to update after have way to test SD */
static void TestSdInterruptCanWork(void)
    {
    return;
    testInterruptCanWork(cAtCiscoUpsrAlarmSd);
    }

static void TestDisablingMaskCanDisableInterrupt(eAtCiscoUpsrAlarm upsrAlarm)
    {
    AtSdhPath path = (AtSdhPath)TestedChannel();
    uint32 holdOffTimer = AtSdhChannelHoldOffTimerGet((AtSdhChannel)path);
    uint32 forcableAlarm = ForceableAlarms(upsrAlarm);
    Af60210011UpsrTestRunner runner = CurrentRunner();
    tAtOsalCurTime curTime, startTime;
    uint32 bit_i;

    UpsrInterruptMaskSet(TestedChannel(), upsrAlarm, 0);

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 elapseTime = 0;
        uint32 forceAlarm = cBit0 << bit_i;

        if ((forceAlarm & forcableAlarm) == 0)
            continue;

        Force(forceAlarm, cAtTrue);
        AtOsalCurTimeGet(&startTime);

        /* Timeout should be large enough,
         * so it should be multiple of hold-off timer */
        while (elapseTime < (4 * holdOffTimer))
            {
            InterruptProcess();

            /* Alarm must not happen due to mask disabled */
            TEST_ASSERT((UpsrDefectGet(path) & upsrAlarm) == 0);
            TEST_ASSERT((runner->upsrAlarms & upsrAlarm) == 0);

            AtOsalCurTimeGet(&curTime);
            elapseTime = mTimeIntervalInMsGet(startTime, curTime);
            }
        Force(forceAlarm, cAtFalse);
        }
    }

static void TestDisablingSfMaskCanDisableInterrupt(void)
    {
    TestDisablingMaskCanDisableInterrupt(cAtCiscoUpsrAlarmSf);
    }

static void setUp(void)
    {
    UpsrInterruptMaskSet(TestedChannel(), cAtCiscoUpsrAlarmSd | cAtCiscoUpsrAlarmSf, cAtCiscoUpsrAlarmSd | cAtCiscoUpsrAlarmSf);
    UpsrNotificationEnable(cAtTrue);
    InterruptProcess();
    if (UpsrDefectGet(TestedChannel()) != 0)
        AtAssert(0);
    }

static void tearDown(void)
    {
    Force(cAtSdhPathAlarmLop, cAtFalse);
    Force(cAtSdhPathAlarmAis, cAtFalse);
    Force(cAtSdhPathAlarmPlm, cAtFalse);
    InterruptProcess();
    UpsrInterruptMaskSet(TestedChannel(), cAtCiscoUpsrAlarmSd | cAtCiscoUpsrAlarmSf, 0);
    UpsrNotificationEnable(cAtFalse);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60210011Upsr_Fixtures)
        {
        new_TestFixture("TestCanSetUpsrMask"    , TestCanSetUpsrMask),
        new_TestFixture("TestSfInterruptCanWork", TestSfInterruptCanWork),
        new_TestFixture("TestSdInterruptCanWork", TestSdInterruptCanWork),
        new_TestFixture("TestDisablingSfMaskCanDisableInterrupt", TestDisablingSfMaskCanDisableInterrupt),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af60210011Upsr_Caller, "TestSuite_Af60210011Upsr", setUp, tearDown, TestSuite_Af60210011Upsr_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60210011Upsr_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize - 1, "upsr");
    return buffer;
    }

static void Delete(AtUnittestRunner self)
    {
    AtCiscoUpsrDelete(mThis(self)->upsrController);
    m_AtUnittestRunnerMethods->Delete(self);
    }

static void WriteProductCode(AtDevice device)
    {
    AtHalWrite(AtDeviceIpCoreHalGet(device, 0), 0, AtDeviceProductCodeGet(device));
    }

static AtCiscoUpsr UpsrCreate(AtChannel channel)
    {
    AtCiscoUpsr upsr;
    AtDevice device = AtChannelDeviceGet(channel);

    WriteProductCode(device);
    upsr = AtCiscoUpsrCreate(DeviceMetaBaseAddress(device), NULL, NULL);
    AtCiscoUpsrBaseAddressSet(upsr, DeviceVirtualAddress(device));

    return upsr;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)self->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        }

    mMethodsSet(self, &m_AtUnittestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210011UpsrTestRunner);
    }

AtUnittestRunner Af60210011UpsrTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->testedChannel = (AtSdhPath)channel;
    mThis(self)->upsrController = UpsrCreate(channel);

    return self;
    }

AtUnittestRunner Af60210011UpsrTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210011UpsrTestRunnerObjectInit(newRunner, channel);
    }

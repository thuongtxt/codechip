/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60290021ModuleSdhTestRunner.c
 *
 * Created Date: Jul 17, 2017
 *
 * Description : SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Af6029ModuleSdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModule(self) (AtModuleSdh)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290021ModuleSdhTestRunner
    {
    tAf6029ModuleSdhTestRunner super;
    }tAf60290021ModuleSdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods              m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void setAllPlateLinesToStm4(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm4) == cAtOk);
        }
    }

static void setAllPlateLinesToStm1(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm1) == cAtOk);
        }
    }

static void testConstrainLine0Stm64CauseLinesUnused(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhLineGet(module, 0), cAtSdhLineRateStm64) == cAtOk);

    for (line_i = 1; line_i <= 7; line_i ++)
        {
        TEST_ASSERT_NULL(AtModuleSdhLineGet(module, line_i));
        TEST_ASSERT_NULL(AtModuleSdhFaceplateLineGet(module, line_i));
        }
    }

static void testConstrainLine8Stm64CauseLinesUnused(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    if (Tha60290021ModuleSdhNumUseableFaceplateLines(module) <= 8)
        return;

    TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhLineGet(module, 8), cAtSdhLineRateStm64) == cAtOk);

    for (line_i = 9; line_i <= 15; line_i ++)
        {
        TEST_ASSERT_NULL(AtModuleSdhLineGet(module, line_i));
        TEST_ASSERT_NULL(AtModuleSdhFaceplateLineGet(module, line_i));
        }
    }

static void testConstrainLineStm16CauseOddIdLinesUnused(void)
    {
    uint32 line_i;
    uint32 oddLine;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 2)
        {
        oddLine = line_i + 1;
        TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhLineGet(module, line_i), cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT_NULL(AtModuleSdhLineGet(module, oddLine));
        TEST_ASSERT_NULL(AtModuleSdhFaceplateLineGet(module, oddLine));
        }
    }

static void testEvenFacePlateLineSupportStm16(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 2)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtModuleSdhLineRateIsSupported(module, line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        }
    }

static void testOddFacePlateLineNotSupportStm16(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());

    for (line_i = 1; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 2)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        if (line)
            {
            TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtFalse);
            }
        }
    }

static void testFacePlateLine0And8SupportStm64(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines, so all faceplate lines are accessible */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if ((line_i % 8) == 0)
            {
            TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm64) == cAtTrue);
            }
        else
            {
        	AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm64) == cAtFalse);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) != cAtOk);
        	AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testChangeStm64ToStm16CauseEvenFacePlateLinesDefaultStm16(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if ((line_i % 8) == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
            }
        else if ((line_i % 2) == 0)
            {
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm16);
            }
        }
    }

static void testChangeStm64ToStm4CauseAllFacePlateLinesDefaultStm4(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm1 for all faceplate lines to check rate change */
    setAllPlateLinesToStm1();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if ((line_i % 8) == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm4) == cAtOk);
            }
        else
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm4);
        }
    }

static void testChangeStm64ToStm1CauseAllFacePlateLinesDefaultStm1(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if ((line_i % 8) == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm1) == cAtOk);
            }
        else
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm1);
        }
    }

static void testChangeStm64ToStm1CauseAllFacePlateLinesDefaultStm0(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        if ((line_i % 8) == 0)
            {
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm64) == cAtOk);
            TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm0) == cAtOk);
            }
        else
            TEST_ASSERT(AtSdhLineRateGet(line) == cAtSdhLineRateStm0);
        }
    }

static void testChangeEvenFacePlateLineStm16ToStm4CauseNextLineDefaultStm4(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm1 for all faceplate lines to check rate change */
    setAllPlateLinesToStm1();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 2)
        {
        AtSdhLine lineEven = AtModuleSdhFaceplateLineGet(module, line_i);
        AtSdhLine lineOdd;

        TEST_ASSERT(AtSdhLineRateSet(lineEven, cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT(AtSdhLineRateSet(lineEven, cAtSdhLineRateStm4) == cAtOk);

        lineOdd = AtModuleSdhFaceplateLineGet(module, line_i + 1);
        TEST_ASSERT(AtSdhLineRateGet(lineOdd) == cAtSdhLineRateStm4);
        }
    }

static void testChangeEvenFacePlateLineStm16ToStm1CauseNextLineDefaultStm1(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 2)
        {
        AtSdhLine lineEven = AtModuleSdhFaceplateLineGet(module, line_i);
        AtSdhLine lineOdd;

        TEST_ASSERT(AtSdhLineRateSet(lineEven, cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT(AtSdhLineRateSet(lineEven, cAtSdhLineRateStm1) == cAtOk);
        lineOdd = AtModuleSdhFaceplateLineGet(module, line_i + 1);
        TEST_ASSERT(AtSdhLineRateGet(lineOdd) == cAtSdhLineRateStm1);
        }
    }

static void testChangeEvenFacePlateLineStm16ToStm1CauseNextLineDefaultStm0(void)
    {
    AtModuleSdh module = mModule(AtUnittestRunnerCurrentRunner());
    uint8 line_i;

    /* Set rate Stm4 for all faceplate lines to check rate change */
    setAllPlateLinesToStm4();

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i += 2)
        {
        AtSdhLine lineEven = AtModuleSdhFaceplateLineGet(module, line_i);
        AtSdhLine lineOdd;

        TEST_ASSERT(AtSdhLineRateSet(lineEven, cAtSdhLineRateStm16) == cAtOk);
        TEST_ASSERT(AtSdhLineRateSet(lineEven, cAtSdhLineRateStm0) == cAtOk);
        lineOdd = AtModuleSdhFaceplateLineGet(module, line_i + 1);
        TEST_ASSERT(AtSdhLineRateGet(lineOdd) == cAtSdhLineRateStm0);
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60290021ModuleSdhLineRate_Fixtures)
        {
        new_TestFixture("testAllPlateLinesCanSetStm1", setAllPlateLinesToStm1),
        new_TestFixture("testAllPlateLinesCanSetStm4", setAllPlateLinesToStm4),
        new_TestFixture("testChangeStm64ToStm16CauseEvenFacePlateLinesDefaultStm16", testChangeStm64ToStm16CauseEvenFacePlateLinesDefaultStm16),
        new_TestFixture("testChangeStm64ToStm4CauseAllFacePlateLinesDefaultStm4", testChangeStm64ToStm4CauseAllFacePlateLinesDefaultStm4),
        new_TestFixture("testChangeStm64ToStm1CauseAllFacePlateLinesDefaultStm1", testChangeStm64ToStm1CauseAllFacePlateLinesDefaultStm1),
        new_TestFixture("testChangeStm64ToStm1CauseAllFacePlateLinesDefaultStm0", testChangeStm64ToStm1CauseAllFacePlateLinesDefaultStm0),
        new_TestFixture("testChangeEvenFacePlateLineStm16ToStm4CauseNextLineDefaultStm4", testChangeEvenFacePlateLineStm16ToStm4CauseNextLineDefaultStm4),
        new_TestFixture("testChangeEvenFacePlateLineStm16ToStm1CauseNextLineDefaultStm1", testChangeEvenFacePlateLineStm16ToStm1CauseNextLineDefaultStm1),
        new_TestFixture("testChangeEvenFacePlateLineStm16ToStm1CauseNextLineDefaultStm0", testChangeEvenFacePlateLineStm16ToStm1CauseNextLineDefaultStm0),
        new_TestFixture("testOddFacePlateLineNotSupportStm16", testOddFacePlateLineNotSupportStm16),
        new_TestFixture("testEvenFacePlateLineSupportStm16", testEvenFacePlateLineSupportStm16),
        new_TestFixture("testFacePlateLine0And8SupportStm64", testFacePlateLine0And8SupportStm64),
        new_TestFixture("testConstrainLine0Stm64CauseLinesUnused", testConstrainLine0Stm64CauseLinesUnused),
        new_TestFixture("testConstrainLine8Stm64CauseLinesUnused", testConstrainLine8Stm64CauseLinesUnused),
        new_TestFixture("testConstrainLineStm16CauseOddIdLinesUnused", testConstrainLineStm16CauseOddIdLinesUnused),
        };

    EMB_UNIT_TESTCALLER(TestSuite_Af60290021ModuleSdhLineRate_Caller, "Af60290021ModuleSdh", NULL, NULL, TestSuite_Af60290021ModuleSdhLineRate_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60290021ModuleSdhLineRate_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModuleSdhTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af6029ModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290021ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return ObjectInit(newRunner, module);
    }

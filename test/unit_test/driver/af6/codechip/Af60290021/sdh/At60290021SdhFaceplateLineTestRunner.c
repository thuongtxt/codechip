/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : At60290021SdhFaceplateLineTestRunner.c
 *
 * Created Date: Jul 17, 2016
 *
 * Description : SDH channel test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAt60290021SdhFaceplateLineTestRunner
    {
    tAtSdhLineTestRunner super;
    }tAt60290021SdhFaceplateLineTestRunner;

typedef eAtRet (*EnableFunc)(AtChannel self, eBool enable);
typedef eBool (*IsEnabledFunc)(AtChannel self);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineTestRunnerMethods m_AtSdhLineTestRunnerOverride;
static tAtUnittestRunnerMethods    m_AtUnittestRunnerOverride;

/* Override */
static const tAtSdhLineTestRunnerMethods *m_AtSdhLineTestRunnerMethods = NULL;
static const tAtUnittestRunnerMethods    *m_AtUnittestRunnerMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhLine TestedLine()
    {
    return (AtSdhLine)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void TestLineRateIsSupported(AtSdhLineTestRunner self)
    {
    /* Let's handle this by a test suite as many cases need to be tested. See module. */
    AtSdhLine line = (AtSdhLine)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm0)  == cAtTrue)
    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm1)  == cAtTrue)
    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm4)  == cAtTrue)
    }

static eBool DoesFraming(AtSdhLineTestRunner self)
    {
    return cAtTrue;
    }

static void TestEnableAttribute(EnableFunc enableFunc, IsEnabledFunc isEnabledFunc)
    {
    AtChannel channel = (AtChannel)TestedLine();

    TEST_ASSERT_EQUAL_INT(cAtOk, enableFunc(channel, cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, isEnabledFunc(channel));
    TEST_ASSERT_EQUAL_INT(cAtOk, enableFunc(channel, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, isEnabledFunc(channel));
    }

static void testCanChangeScramble()
    {
    TestEnableAttribute((EnableFunc)AtSdhLineScrambleEnable, (IsEnabledFunc)AtSdhLineScrambleIsEnabled);
    }

static void testCanEnableDisable()
    {
    TestEnableAttribute(AtChannelEnable, AtChannelIsEnabled);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60290021SdhFaceplateLine_Fixtures)
        {
        new_TestFixture("testCanChangeScramble", testCanChangeScramble),
        new_TestFixture("testCanEnableDisable", testCanEnableDisable)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af60290021SdhFaceplateLine_Caller, "TestSuite_Af60290021SdhFaceplateLine", NULL, NULL, TestSuite_Af60290021SdhFaceplateLine_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60290021SdhFaceplateLine_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void OverrideAtSdhLineTestRunner(AtChannelTestRunner self)
    {
    AtSdhLineTestRunner runner = (AtSdhLineTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtSdhLineTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtSdhLineTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtSdhLineTestRunnerOverride));

        mMethodOverride(m_AtSdhLineTestRunnerOverride, DoesFraming);
        mMethodOverride(m_AtSdhLineTestRunnerOverride, TestLineRateIsSupported);
        }

    mMethodsSet(runner, &m_AtSdhLineTestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    OverrideAtSdhLineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAt60290021SdhFaceplateLineTestRunner);
    }

static AtChannelTestRunner ObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner At60290021SdhFaceplateLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return ObjectInit(newRunner, channel, moduleRunner);
    }

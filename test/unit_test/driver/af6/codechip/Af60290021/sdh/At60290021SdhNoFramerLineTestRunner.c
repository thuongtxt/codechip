/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : At60290021SdhNoFramerLineTestRunner.c
 *
 * Created Date: Jul 17, 2016
 *
 * Description : SDH line test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "AtSdhLine.h"
#include "AtSdhChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAt60290021SdhNoFramerLineTestRunner
    {
    tAtSdhLineTestRunner super;
    }tAt60290021SdhNoFramerLineTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineTestRunnerMethods m_AtSdhLineTestRunnerOverride;

/* Override */
static const tAtSdhLineTestRunnerMethods *m_AtSdhLineTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TestLineRateIsSupported(AtSdhLineTestRunner self)
    {
    AtSdhLine line = (AtSdhLine)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)line);
    eBool isMate = Tha60290021ModuleSdhLineIsMate(sdhModule, AtChannelIdGet((AtChannel)line));

    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm0)  == cAtFalse);
    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm1)  == cAtFalse);
    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm4)  == cAtFalse);
    TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtTrue);

    /* 10G does not support OC-192 */
    if (Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule) == 8)
        {
        TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm64) == cAtFalse);
        }
    else
        {
        TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm64) == (isMate ? cAtFalse : cAtTrue));
        }
    }

static eBool DoesFraming(AtSdhLineTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtSdhLineTestRunner(AtChannelTestRunner self)
    {
    AtSdhLineTestRunner runner = (AtSdhLineTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtSdhLineTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtSdhLineTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtSdhLineTestRunnerOverride));

        mMethodOverride(m_AtSdhLineTestRunnerOverride, DoesFraming);
        mMethodOverride(m_AtSdhLineTestRunnerOverride, TestLineRateIsSupported);
        }

    mMethodsSet(runner, &m_AtSdhLineTestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtSdhLineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAt60290021SdhNoFramerLineTestRunner);
    }

static AtChannelTestRunner ObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner At60290021SdhNoFramerLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return ObjectInit(newRunner, channel, moduleRunner);
    }

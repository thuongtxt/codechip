/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af6029ModuleSdhTestRunner.c
 *
 * Created Date: Sep 13, 2018
 *
 * Description : Module SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Af6029ModuleSdhTestRunnerInternal.h"
#include "Af60290021ModuleSdhTest.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModule(self) (AtModuleSdh)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAf6029ModuleSdhTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods              m_AtUnittestRunnerOverride;
static tAtModuleSdhTestRunnerMethods         m_AtModuleSdhTestRunnerOverride;
static tAf60210011ModuleSdhTestRunnerMethods m_Af60210011ModuleSdhTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Af6029ModuleSdhTestRunner CurrentRunner(void)
    {
    return (Af6029ModuleSdhTestRunner)AtUnittestRunnerCurrentRunner();
    }

static uint32 NumTfi5s(AtModuleSdhTestRunner self)
    {
    return 8;
    }

static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    Af6029ModuleSdhTestRunner runner = (Af6029ModuleSdhTestRunner)self;
    return mMethodsGet(runner)->NumFaceplateLines(runner) +
           mMethodsGet(runner)->NumMateLines(runner) +
           mMethodsGet(runner)->NumTerminatedLines(runner);
    }

static uint32 FacePlateLineStartId(void)
    {
    return 0;
    }

static uint32 MateLineStartId(void)
    {
    return 16;
    }

static uint32 TerminatedLineStartId(void)
    {
    return 24;
    }

static void testNumTerminatedLinesMustBeEnough(void)
    {
    Af6029ModuleSdhTestRunner runner = CurrentRunner();
    uint32 numTerminatedLines = mMethodsGet(runner)->NumTerminatedLines(runner);
    TEST_ASSERT_EQUAL_INT(numTerminatedLines, AtModuleSdhNumTerminatedLines(mModule(runner)));
    }

static void testNumFaceplateLinesMustBeEnough(void)
    {
    Af6029ModuleSdhTestRunner runner = CurrentRunner();
    uint32 numFaceplateLines = mMethodsGet(runner)->NumFaceplateLines(runner);
    TEST_ASSERT_EQUAL_INT(numFaceplateLines, AtModuleSdhNumFaceplateLines(mModule(runner)));
    }

static void testCannotGetLineByInvalidLineId(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    AtSdhLine line = AtModuleSdhFaceplateLineGet(module, AtModuleSdhMaxLinesGet(module));
    TEST_ASSERT_NULL(line);
    }

static void testCannotCreateLineByInvalidLineId(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 invalidlineId =  AtModuleSdhMaxLinesGet(module);
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeLine, invalidlineId));
    }

static void testCannotCreateAuByInvalidLineId(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 invalidlineId =  AtModuleSdhMaxLinesGet(module);
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeAu4_16c, invalidlineId));
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeAu4_4c, invalidlineId));
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeAu4, invalidlineId));
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeAu3, invalidlineId));
    }

static void testCannotCreateAuVcByInvalidLineId(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 invalidlineId =  AtModuleSdhMaxLinesGet(module);
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeVc4_16c, invalidlineId));
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeVc4_4c, invalidlineId));
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeVc4, invalidlineId));
    TEST_ASSERT_NULL(AtModuleSdhChannelCreate(module, invalidlineId, NULL, cAtSdhChannelTypeVc3, invalidlineId));
    }

static void testCannotGetFacePlateLineFromNullModule(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(CurrentRunner());
    uint32 numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(module);

    for (line_i = 0; line_i < numLines; line_i++)
        TEST_ASSERT_NULL(AtModuleSdhFaceplateLineGet(NULL, line_i));
    }

static void testCannotGetMateLineFromNullModule(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(CurrentRunner());
    uint32 numLines = Tha60290021ModuleSdhNumUseableMateLines(module);

    for (line_i = 0; line_i < numLines; line_i++)
        TEST_ASSERT_NULL(AtModuleSdhMateLineGet(NULL, line_i));
    }

static void testCannotGetTerminatedLineFromNullModule(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(CurrentRunner());
    uint32 numLines = AtModuleSdhNumTerminatedLines(module);

    for (line_i = 0; line_i < numLines; line_i++)
        TEST_ASSERT_NULL(AtModuleSdhTerminatedLineGet(NULL, line_i));
    }

static uint32 NumUsableFaceplateLines(AtModuleSdh sdhModule)
    {
    return Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule);
    }

static void testAllPlateLinesCanSetStm0(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < NumUsableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm0) == cAtOk);
        TEST_ASSERT_EQUAL_INT(cAtSdhLineRateStm0, AtSdhLineRateGet(line));
        }
    }

static void testAllPlateLinesCanSetStm1(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < NumUsableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm1) == cAtOk);
        }
    }

static void testAllPlateLinesCanSetStm4(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < NumUsableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm4) == cAtOk);
        }
    }

static void testMateLineSupportStm16(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(CurrentRunner());

    for (line_i = MateLineStartId(); line_i < (MateLineStartId() + Tha60290021ModuleSdhNumUseableMateLines(module)); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtModuleSdhLineRateIsSupported(module, line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        }
    }

static void testTerminatedLineSupportStm16(void)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(CurrentRunner());

    for (line_i = TerminatedLineStartId(); line_i < (TerminatedLineStartId() + Tha60290021ModuleSdhNumUseableMateLines(module)); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT(AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtModuleSdhLineRateIsSupported(module, line, cAtSdhLineRateStm16) == cAtTrue);
        TEST_ASSERT(AtSdhLineRateSet(line, cAtSdhLineRateStm16) == cAtOk);
        }
    }

static void FirstFacePlateLineSetUpXc(void)
    {
    mCliSuccessAssert(AtCliExecuteFormat("sdh line rate 1 stm16"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug16.1.1 4xaug4s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug4.1.1 4xaug1s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.1.1 3xvc3s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug16.25.1 4xaug4s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug4.25.1 4xaug1s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.25.1 3xvc3s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.25.1.1 de3"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.25.1.1 de3"));
    mCliSuccessAssert(AtCliExecuteFormat("xc vc connect vc3.25.1.1 vc3.1.1.1 two-way"));
    }

static void FirstFacePlateLineCleanUpXc(void)
    {
    mCliSuccessAssert(AtCliExecuteFormat("xc vc disconnect vc3.25.1.1 vc3.1.1.1 two-way"));
    }

static void testFacePlateLineCannotChangeRateWhenHaveXc(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    FirstFacePlateLineSetUpXc();
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhFaceplateLineGet(module, 0), cAtSdhLineRateStm4) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    FirstFacePlateLineCleanUpXc();
    }

static void testFacePlateLineCanChangeRateWhenHaveNoXc(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    FirstFacePlateLineSetUpXc();
    FirstFacePlateLineCleanUpXc();
    TEST_ASSERT(AtSdhLineRateSet(AtModuleSdhFaceplateLineGet(module, 0), cAtSdhLineRateStm4) == cAtOk);
    }

static void testFacePlateLineCannotChangeAugMappingWhenSubChannelsHaveXc(void)
    {
    FirstFacePlateLineSetUpXc();
    AtTestLoggerEnable(cAtFalse);
    mCliSuccessAssert(!AtCliExecuteFormat("sdh map aug1.1.1 vc4"));
    mCliSuccessAssert(!AtCliExecuteFormat("sdh map aug4.1.1 vc4_4c"));
    AtTestLoggerEnable(cAtTrue);
    FirstFacePlateLineCleanUpXc();
    }

static void testFacePlateLineCanChangeAugMappingWhenSubChannelsHaveNoXc(void)
    {
    FirstFacePlateLineSetUpXc();
    FirstFacePlateLineCleanUpXc();
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.1.1 vc4"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug4.1.1 vc4_4c"));
    }

static void testTerminatedLineCannotChangeAugMappingWhenSubChannelsHaveXc(void)
    {
    FirstFacePlateLineSetUpXc();
    AtTestLoggerEnable(cAtFalse);
    mCliSuccessAssert(!AtCliExecuteFormat("sdh map aug1.25.1 vc4"));
    mCliSuccessAssert(!AtCliExecuteFormat("sdh map aug4.25.1 vc4_4c"));
    AtTestLoggerEnable(cAtTrue);
    FirstFacePlateLineCleanUpXc();
    }

static void testTerminatedLineCanChangeAugMappingWhenSubChannelsHaveNoXc(void)
    {
    FirstFacePlateLineSetUpXc();
    FirstFacePlateLineCleanUpXc();
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.25.1 vc4"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug4.25.1 vc4_4c"));
    }

static void testSerdesWithSameMode_SERDESMustWorkCloselyWithCorrespondingPort()
    {
    AtModuleSdh module = mModule(CurrentRunner());
    AtDevice device = AtModuleDeviceGet((AtModule)module);
    uint8 numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(module);
    uint8 line_i;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);

    /* Have full lines be used */
    testAllPlateLinesCanSetStm4();

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        AtSerdesController faceplateSerdes = AtSerdesManagerSerdesControllerGet(serdesManager, line_i);

        TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerModeSet(faceplateSerdes, cAtSerdesModeStm4));
        TEST_ASSERT(AtSdhLineSerdesController(line) == faceplateSerdes);
        TEST_ASSERT(AtSerdesControllerPhysicalPortGet(faceplateSerdes) == (AtChannel)line);
        }
    }

static void testIfSerdesWorkInDifferentMode_PortJustHaveWeakReferenceToSerdes()
    {
    AtModuleSdh module = mModule(CurrentRunner());
    AtDevice device = AtModuleDeviceGet((AtModule)module);
    uint8 numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(module);
    uint8 line_i;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);

    /* Have full lines be used */
    testAllPlateLinesCanSetStm1();

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        AtSerdesController expectedSerdes = AtSerdesManagerSerdesControllerGet(serdesManager, line_i);

        /* Mismatch STM mode still have reference between SERDES and Port */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerModeSet(expectedSerdes, cAtSerdesModeStm4));
        expectedSerdes = AtSerdesManagerSerdesControllerGet(serdesManager, line_i);
        TEST_ASSERT(AtSdhLineSerdesController(line) == expectedSerdes);
        TEST_ASSERT(AtSerdesControllerPhysicalPortGet(expectedSerdes) == (AtChannel)line);

        /* ETH mode */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerModeSet(expectedSerdes, cAtSerdesModeEth1G));
        expectedSerdes = AtSerdesManagerSerdesControllerGet(serdesManager, line_i);
        TEST_ASSERT(AtSdhLineSerdesController(line) == expectedSerdes);
        TEST_ASSERT(AtSerdesControllerPhysicalPortGet(expectedSerdes) != (AtChannel)line);
        }
    }

static void testCanEnableDisableExtendedKByteOnFaceplateLines(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);

        TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaSdhLineExtendedKByteEnable(line, cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtCienaSdhLineExtendedKByteIsEnabled(line));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaSdhLineExtendedKByteEnable(line, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtCienaSdhLineExtendedKByteIsEnabled(line));
        }
    }

static void testExtendedKByteMustBeDisabledOnNullLine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtCienaSdhLineExtendedKByteIsEnabled(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotEnableExtendedKByteOnNoneFaceplateLine(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i < AtModuleSdhMaxLinesGet(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        if (line == NULL)
            continue;

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtCienaSdhLineExtendedKByteEnable(line, cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);

        TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaSdhLineExtendedKByteEnable(line, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtCienaSdhLineExtendedKByteIsEnabled(line));
        }
    }

static void testAllFacePlateLinesExtendedKBytePosition(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);

        TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaSdhLineExtendedKBytePositionSet(line, cAtCienaSdhLineExtendedKBytePositionD1Sts4));
        TEST_ASSERT_EQUAL_INT(cAtCienaSdhLineExtendedKBytePositionD1Sts4, AtCienaSdhLineExtendedKBytePositionGet(line));

        TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaSdhLineExtendedKBytePositionSet(line, cAtCienaSdhLineExtendedKBytePositionD1Sts10));
        TEST_ASSERT_EQUAL_INT(cAtCienaSdhLineExtendedKBytePositionD1Sts10, AtCienaSdhLineExtendedKBytePositionGet(line));
        }
    }

static void testCannotUseInvalidPositionForExtendedKBytes(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    AtTestLoggerEnable(cAtFalse);
    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        TEST_ASSERT(AtCienaSdhLineExtendedKBytePositionSet(line, cAtCienaSdhLineExtendedKBytePositionInvalid));
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotSpecifyExtendedKBytePositionOnNullLine(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtCienaSdhLineExtendedKBytePositionSet(NULL, cAtCienaSdhLineExtendedKBytePositionD1Sts4));
    AtTestLoggerEnable(cAtTrue);
    }

static void testExtendedKBytePositionOnNotAllFacePlateLinesMustBeInvalid(void)
    {
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 line_i;

    for (line_i = Tha60290021ModuleSdhNumUseableFaceplateLines(module); line_i < AtModuleSdhMaxLinesGet(module); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, line_i);
        if (line == NULL)
            continue;
        TEST_ASSERT_EQUAL_INT(cAtCienaSdhLineExtendedKBytePositionInvalid, AtCienaSdhLineExtendedKBytePositionGet(line));
        }
    }

static void TestLineCanNotCreateDcc(AtSdhLine line)
    {
    TEST_ASSERT(AtSdhLineDccChannelCreate(line, cAtSdhLineDccLayerSection) == NULL);
    TEST_ASSERT(AtSdhLineDccChannelCreate(line, cAtSdhLineDccLayerLine) == NULL);
    TEST_ASSERT(AtSdhLineDccChannelCreate(line, cAtSdhLineDccLayerSection | cAtSdhLineDccLayerLine) == NULL);
    }

static void testTerminatedLineCanNotCreateDcc(void)
    {
    uint8 lineOffset;
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 startMateLineId = Tha60290021ModuleSdhTerminatedLineStartId(module);
    for (lineOffset = 0; lineOffset < Tha60290021ModuleSdhNumUseableTerminatedLines(module); lineOffset++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, startMateLineId + lineOffset);
        if (line == NULL)
            continue;

        TestLineCanNotCreateDcc(line);
        }
    }

static void testMateLineCanNotCreateDcc(void)
    {
    uint8 lineOffset;
    AtModuleSdh module = mModule(CurrentRunner());
    uint8 startMateLineId = Tha60290021ModuleSdhMateLineStartId(module);
    for (lineOffset = 0; lineOffset < Tha60290021ModuleSdhNumUseableMateLines(module); lineOffset++)
        {
        AtSdhLine line = AtModuleSdhLineGet(module, startMateLineId + lineOffset);
        if (line == NULL)
            continue;

        TestLineCanNotCreateDcc(line);
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_Af6029ModuleSdhLineRate_Fixtures)
        {
        new_TestFixture("testNumTerminatedLinesMustBeEnough", testNumTerminatedLinesMustBeEnough),
        new_TestFixture("testNumFaceplateLinesMustBeEnough", testNumFaceplateLinesMustBeEnough),
        new_TestFixture("testAllPlateLinesCanSetStm0", testAllPlateLinesCanSetStm0),
        new_TestFixture("testCannotGetFacePlateLineFromNullModule", testCannotGetFacePlateLineFromNullModule),
        new_TestFixture("testCannotGetMateLineFromNullModule", testCannotGetMateLineFromNullModule),
        new_TestFixture("testCannotGetTerminatedLineFromNullModule", testCannotGetTerminatedLineFromNullModule),
        new_TestFixture("testAllPlateLinesCanSetStm1", testAllPlateLinesCanSetStm1),
        new_TestFixture("testAllPlateLinesCanSetStm4", testAllPlateLinesCanSetStm4),
        new_TestFixture("testCannotGetLineByInvalidLineId", testCannotGetLineByInvalidLineId),
        new_TestFixture("testCannotCreateLineByInvalidLineId", testCannotCreateLineByInvalidLineId),
        new_TestFixture("testCannotCreateAuByInvalidLineId", testCannotCreateAuByInvalidLineId),
        new_TestFixture("testCannotCreateAuVcByInvalidLineId", testCannotCreateAuVcByInvalidLineId),
        new_TestFixture("testMateLineSupportStm16", testMateLineSupportStm16),
        new_TestFixture("testTerminatedLineSupportStm16", testTerminatedLineSupportStm16),
        new_TestFixture("testFacePlateLineCannotChangeRateWhenHaveXc", testFacePlateLineCannotChangeRateWhenHaveXc),
        new_TestFixture("testFacePlateLineCanChangeRateWhenHaveNoXc", testFacePlateLineCanChangeRateWhenHaveNoXc),
        new_TestFixture("testFacePlateLineCannotChangeAugMappingWhenSubChannelsHaveXc", testFacePlateLineCannotChangeAugMappingWhenSubChannelsHaveXc),
        new_TestFixture("testFacePlateLineCanChangeAugMappingWhenSubChannelsHaveNoXc", testFacePlateLineCanChangeAugMappingWhenSubChannelsHaveNoXc),
        new_TestFixture("testTerminatedLineCannotChangeAugMappingWhenSubChannelsHaveXc", testTerminatedLineCannotChangeAugMappingWhenSubChannelsHaveXc),
        new_TestFixture("testTerminatedLineCanChangeAugMappingWhenSubChannelsHaveNoXc", testTerminatedLineCanChangeAugMappingWhenSubChannelsHaveNoXc),
        new_TestFixture("testSerdesWithSameMode_SERDESMustWorkCloselyWithCorrespondingPort", testSerdesWithSameMode_SERDESMustWorkCloselyWithCorrespondingPort),
        new_TestFixture("testIfSerdesWorkInDifferentMode_PortJustHaveWeakReferenceToSerdes", testIfSerdesWorkInDifferentMode_PortJustHaveWeakReferenceToSerdes),
        new_TestFixture("testCanEnableDisableExtendedKByteOnFaceplateLines", testCanEnableDisableExtendedKByteOnFaceplateLines),
        new_TestFixture("testExtendedKByteMustBeDisabledOnNullLine", testExtendedKByteMustBeDisabledOnNullLine),
        new_TestFixture("testCannotEnableExtendedKByteOnNoneFaceplateLine", testCannotEnableExtendedKByteOnNoneFaceplateLine),
        new_TestFixture("testAllFacePlateLinesExtendedKBytePosition", testAllFacePlateLinesExtendedKBytePosition),
        new_TestFixture("testCannotUseInvalidPositionForExtendedKBytes", testCannotUseInvalidPositionForExtendedKBytes),
        new_TestFixture("testCannotSpecifyExtendedKBytePositionOnNullLine", testCannotSpecifyExtendedKBytePositionOnNullLine),
        new_TestFixture("testExtendedKBytePositionOnNotAllFacePlateLinesMustBeInvalid", testExtendedKBytePositionOnNotAllFacePlateLinesMustBeInvalid),
        new_TestFixture("testTerminatedLineCanNotCreateDcc", testTerminatedLineCanNotCreateDcc),
        new_TestFixture("testMateLineCanNotCreateDcc", testMateLineCanNotCreateDcc),
        };

    EMB_UNIT_TESTCALLER(TestSuite_Af6029ModuleSdhLineRate_Caller, "Af6029ModuleSdh", NULL, NULL, TestSuite_Af6029ModuleSdhLineRate_Fixtures);

    return (TestRef)((void *)&TestSuite_Af6029ModuleSdhLineRate_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void TestGetFacePlateLine(AtModuleSdhTestRunner self)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(self);
    uint32 numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(module);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhFaceplateLineGet(module, line_i);

        /* Depend on line rate, some lines are not accessible */
        if (line == NULL)
            continue;

        TEST_ASSERT_EQUAL_INT(line_i + FacePlateLineStartId(), AtChannelIdGet((AtChannel)line));
        }
    }

static void TestGetMateLine(AtModuleSdhTestRunner self)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(self);
    uint32 numLines = Tha60290021ModuleSdhNumUseableMateLines(module);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhMateLineGet(module, line_i);
        TEST_ASSERT_NOT_NULL(line);
        TEST_ASSERT_EQUAL_INT(line_i + MateLineStartId(), AtChannelIdGet((AtChannel)line));
        }
    }

static void TestGetTerminatedLine(AtModuleSdhTestRunner self)
    {
    uint32 line_i;
    AtModuleSdh module = mModule(self);
    uint32 numLines = AtModuleSdhNumTerminatedLines(module);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhTerminatedLineGet(module, line_i);

        if (line_i < Tha60290021ModuleSdhNumUseableTerminatedLines(module))
            {
            TEST_ASSERT_NOT_NULL(line);
            TEST_ASSERT_EQUAL_INT(line_i + TerminatedLineStartId(), AtChannelIdGet((AtChannel)line));
            }
        else
            {
            TEST_ASSERT_NULL(line);
            }
        }
    }

static eAtSdhLineRate MaxLineRate(AtModuleSdhTestRunner self)
    {
    return cAtSdhLineRateStm64;
    }

static void TestGetLine(AtModuleSdhTestRunner self)
    {
    TestGetFacePlateLine(self);
    TestGetMateLine(self);
    TestGetTerminatedLine(self);
    }

static void TestRetrieveTfi5Line(Af60210011ModuleSdhTestRunner self)
    {
    /* This is already handled by TestGetLine() */
    }

static eBool IsFaceplateLine(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    if ((lineId >= FacePlateLineStartId()) &&
        (lineId <  FacePlateLineStartId() + Tha60290021ModuleSdhNumUseableFaceplateLines(mModule(self))))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanTestVc11(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    return Af6029ModuleSdhIsTerminatedLine(mModule(self), AtChannelIdGet((AtChannel)line));
    }

static eBool LineHasLoMapping(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    return Af6029ModuleSdhIsTerminatedLine(mModule(self), AtChannelIdGet((AtChannel)line));
    }

static AtSdhLine LineFromChannel(AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    uint8 lineId = AtSdhChannelLineGet(channel);
    return AtModuleSdhLineGet(sdhModule, lineId);
    }

static eBool MapTypeMustBeSupported(AtModuleSdhTestRunner self, AtSdhChannel channel, uint8 mapType)
    {
    AtSdhLine line = LineFromChannel(channel);

    if (Af6029ModuleSdhIsTerminatedLine(mModule(self), AtChannelIdGet((AtChannel)line)))
        return cAtTrue;

    if (AtSdhChannelIsAug(channel) ||
        AtSdhChannelIsAu(channel))
        return cAtTrue;

    if (AtSdhChannelIsHoVc(channel))
        {
        eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

        if (channelType == cAtSdhChannelTypeVc4)
            return (mapType == cAtSdhVcMapTypeVc4Map3xTug3s) ? cAtFalse : cAtTrue;

        if (channelType == cAtSdhChannelTypeVc3)
            return (mapType == cAtSdhVcMapTypeVc3MapC3) ? cAtTrue : cAtFalse;

        return cAtTrue;
        }

    return cAtFalse;
    }

static AtChannelTestRunner CreateLineTestRunner(AtModuleSdhTestRunner self, AtChannel line)
    {
    if (IsFaceplateLine(self, (AtSdhLine)line))
        return At60290021SdhFaceplateLineTestRunnerNew(line, (AtModuleTestRunner)self);
    return At60290021SdhNoFramerLineTestRunnerNew(line, (AtModuleTestRunner)self);
    }

static eBool ShouldTestLine(AtModuleSdhTestRunner self, uint8 lineId)
    {
    AtModuleSdh sdhModule = mModule(self);
    return AtModuleSdhLineGet(sdhModule, lineId) ? cAtTrue : cAtFalse;
    }

static uint8 *AllTestedLinesAllocate(AtModuleSdhTestRunner self, uint8 *numLines)
    {
    return Af6029ModuleSdhAllApplicableLinesAllocate(mModule(self), numLines);
    }

static uint32 NumMateLines(Af6029ModuleSdhTestRunner self)
    {
    return 8;
    }

static uint32 NumTerminatedLines(Af6029ModuleSdhTestRunner self)
    {
    return 8;
    }

static uint32 NumFaceplateLines(Af6029ModuleSdhTestRunner self)
    {
    return 16;
    }

static void MethodsInit(AtModuleTestRunner self)
    {
    Af6029ModuleSdhTestRunner runner = (Af6029ModuleSdhTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NumMateLines);
        mMethodOverride(m_methods, NumTerminatedLines);
        mMethodOverride(m_methods, NumFaceplateLines);
        }

    mMethodsSet(runner, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void OverrideAf60210011ModuleSdhTestRunner(AtModuleTestRunner self)
    {
    Af60210011ModuleSdhTestRunner runner = (Af60210011ModuleSdhTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_Af60210011ModuleSdhTestRunnerOverride, mMethodsGet(runner), sizeof(m_Af60210011ModuleSdhTestRunnerOverride));

        mMethodOverride(m_Af60210011ModuleSdhTestRunnerOverride, TestRetrieveTfi5Line);
        }

    mMethodsSet(runner, &m_Af60210011ModuleSdhTestRunnerOverride);
    }

static void OverrideAtModuleSdhTestRunner(AtModuleTestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner) self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleSdhTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleSdhTestRunnerOverride));

        mMethodOverride(m_AtModuleSdhTestRunnerOverride, NumTfi5s);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TotalLines);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TestGetLine);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, CanTestVc11);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, LineHasLoMapping);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, MapTypeMustBeSupported);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, ShouldTestLine);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, CreateLineTestRunner);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, AllTestedLinesAllocate);
        }

    mMethodsSet(runner, &m_AtModuleSdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleSdhTestRunner(self);
    OverrideAf60210011ModuleSdhTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf6029ModuleSdhTestRunner);
    }

AtModuleTestRunner Af6029ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210051ModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint8 *Af6029ModuleSdhAllApplicableLinesAllocate(AtModuleSdh sdhModule, uint8 *numLines)
    {
    uint8 *lineIds;
    uint32 memorySize;
    uint8 line_i = 0, currentLineIndex = 0;

    *numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule) +
                Tha60290021ModuleSdhNumUseableMateLines(sdhModule) +
                Tha60290021ModuleSdhNumUseableTerminatedLines(sdhModule);
    memorySize = sizeof(uint8) * (*numLines);
    lineIds = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(lineIds, 0, memorySize);

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule); line_i++)
        {
        lineIds[currentLineIndex] = FacePlateLineStartId() + line_i;
        currentLineIndex = currentLineIndex + 1;
        }

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableMateLines(sdhModule); line_i++)
        {
        lineIds[currentLineIndex] = MateLineStartId() + line_i;
        currentLineIndex = currentLineIndex + 1;
        }

    for (line_i = 0; line_i < Tha60290021ModuleSdhNumUseableTerminatedLines(sdhModule); line_i++)
        {
        lineIds[currentLineIndex] = TerminatedLineStartId() + line_i;
        currentLineIndex = currentLineIndex + 1;
        }

    return lineIds;
    }

eBool Af6029ModuleSdhIsTerminatedLine(AtModuleSdh sdhModule, uint8 lineId)
    {
    if (lineId >= TerminatedLineStartId())
        return AtModuleSdhLineCanBeUsed(sdhModule, lineId);

    return cAtFalse;
    }

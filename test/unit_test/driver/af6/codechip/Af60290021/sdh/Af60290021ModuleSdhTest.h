/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Af60290021ModuleSdhTestRunner.h
 * 
 * Created Date: Jul 15, 2016
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021MODULESDHTEST_H_
#define _AF60290021MODULESDHTEST_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../sdh/AtModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner At60290021SdhFaceplateLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner At60290021SdhNoFramerLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

uint8 *Af6029ModuleSdhAllApplicableLinesAllocate(AtModuleSdh sdhModule, uint8 *numLines);
eBool Af6029ModuleSdhIsTerminatedLine(AtModuleSdh sdhModule, uint8 lineId);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021MODULESDHTEST_H_ */


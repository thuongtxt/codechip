/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af6029ModuleSdhTestRunnerInternal.h
 * 
 * Created Date: Sep 12, 2018
 *
 * Description : Module SDH test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF6029MODULESDHTESTRUNNERINTERNAL_H_
#define _AF6029MODULESDHTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/sdh/Af60210051ModuleSdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf6029ModuleSdhTestRunner *Af6029ModuleSdhTestRunner;

typedef struct tAf6029ModuleSdhTestRunnerMethods
    {
    uint32 (*NumMateLines)(Af6029ModuleSdhTestRunner self);
    uint32 (*NumTerminatedLines)(Af6029ModuleSdhTestRunner self);
    uint32 (*NumFaceplateLines)(Af6029ModuleSdhTestRunner self);
    }tAf6029ModuleSdhTestRunnerMethods;

typedef struct tAf6029ModuleSdhTestRunner
    {
    tAf60210051ModuleSdhTestRunner super;
    const tAf6029ModuleSdhTestRunnerMethods *methods;
    }tAf6029ModuleSdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af6029ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF6029MODULESDHTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : Af60290021ModulePtpTestRunner.c
 *
 * Created Date: May 6, 2019
 *
 * Description : Modules PTP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../ptp/AtModulePtpTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290021ModulePtpTestRunner
    {
    tAtModulePtpTestRunner super;
    }tAf60290021ModulePtpTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePtpTestRunnerMethods m_AtModulePtpTestRunnerOverride;

/* Save super implementation */
static const tAtModulePtpTestRunnerMethods *m_AtModulePtpTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList MakePtpPortForGroupTest(AtModulePtpTestRunner self, AtList ptpPorts)
    {
    char portString[64];

    AtSprintf(portString, "2-17");
    AtModulePtpTestRunnerAddPtpPortsFromString(ptpPorts, portString);
    return ptpPorts;
    }

static void OverrideAtModulePtpTestRunner(AtModulePtpTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtModulePtpTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtModulePtpTestRunnerOverride, (void *)self->methods,
                     sizeof(m_AtModulePtpTestRunnerOverride));

        mMethodOverride(m_AtModulePtpTestRunnerOverride, MakePtpPortForGroupTest);
        }

    self->methods = &m_AtModulePtpTestRunnerOverride;
    }

static void Override(AtModulePtpTestRunner self)
    {
    OverrideAtModulePtpTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModulePtpTestRunner);
    }

AtModuleTestRunner Af60290021ModulePtpTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModulePtpTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((AtModulePtpTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290021ModulePtpTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60290021ModulePtpTestRunnerObjectInit(newRunner, module);
    }

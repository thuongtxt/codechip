/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : Af60290021ModuleApsTestRunner.c
 *
 * Created Date: Jul 21, 2016
 *
 * Description : APS Module of Af60290021 device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60290021ModuleApsTestRunner.h"
#include "Af60290021ModuleApsTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAf60290021ModuleApsTestRunner*)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleApsTestRunnerMethods   m_AtModuleApsTestRunnerOverride;

/* Save super implementation */
static const tAtModuleApsTestRunnerMethods *m_AtModuleApsTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumUpsrEngines(AtModuleApsTestRunner self)
    {
    return 384;
    }

static uint32 MaxNumApsGroups(AtModuleApsTestRunner self)
    {
    return 16;
    }

static uint32 MaxNumApsSelectors(AtModuleApsTestRunner self)
    {
    return 3*384;
    }

static AtSdhLine LineGet(AtModuleApsTestRunner self, uint8 lineId)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, lineId);
    }

static void PathSetup(AtModuleApsTestRunner self, AtSdhPath paths[6])
    {
    if (mThis(self)->didSetup)
        return;

    AtAssert(AtCliExecute("device init") == 0);
    AtAssert(AtCliExecute("sdh line rate 1,5,25 stm16") == 0);
    AtAssert(AtCliExecute("sdh map aug16.1.1,5.1,25.1 4xaug4s") == 0);
    AtAssert(AtCliExecute("sdh map aug4.1.1-1.4,5.1-5.4,25.1-25.4 4xaug1s") == 0);
    AtAssert(AtCliExecute("sdh map aug1.1.1-1.16,5.1-5.16,25.1-25.16 3xvc3s") == 0);

    paths[0] = (AtSdhPath)AtSdhLineVc3Get(LineGet(self, 0), 0, 0);
    paths[1] = (AtSdhPath)AtSdhLineVc3Get(LineGet(self, 4), 0, 0);
    paths[2] = (AtSdhPath)AtSdhLineVc3Get(LineGet(self, 0), 0, 1);
    paths[3] = (AtSdhPath)AtSdhLineVc3Get(LineGet(self, 4), 0, 1);
    paths[4] = (AtSdhPath)AtSdhLineVc3Get(LineGet(self, 24), 0, 0);
    paths[5] = (AtSdhPath)AtSdhLineVc3Get(LineGet(self, 24), 0, 1);

    mThis(self)->didSetup = cAtTrue;
    }

static void OverrideAtModuleApsTestRunner(AtModuleTestRunner self)
    {
    AtModuleApsTestRunner runner = (AtModuleApsTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtModuleApsTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtModuleApsTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleApsTestRunnerOverride));

        mMethodOverride(m_AtModuleApsTestRunnerOverride, PathSetup);
        mMethodOverride(m_AtModuleApsTestRunnerOverride, MaxNumUpsrEngines);
        mMethodOverride(m_AtModuleApsTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModuleApsTestRunnerOverride, MaxNumApsSelectors);
        }

    mMethodsSet(runner, &m_AtModuleApsTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleApsTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModuleApsTestRunner);
    }

AtModuleTestRunner Af60290021ModuleApsTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleApsTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290021ModuleApsTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60290021ModuleApsTestRunnerObjectInit(newRunner, module);
    }

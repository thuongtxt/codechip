/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af60290021ModuleApsTestRunner.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : 60290021 Module APS test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef AF60290021MODULEAPSTESTRUNNER_H_
#define AF60290021MODULEAPSTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../aps/AtModuleApsTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60290021ModuleApsTestRunnerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* AF60290021MODULEAPSTESTRUNNER_H_ */


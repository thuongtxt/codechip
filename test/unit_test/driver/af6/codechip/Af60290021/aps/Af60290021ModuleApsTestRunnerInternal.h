/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af60290021ModuleApsTestRunnerInternal.h
 * 
 * Created Date: Sep 23, 2019
 *
 * Description : 60290021 Module APS test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021MODULEAPSTESTRUNNERINTERNAL_H_
#define _AF60290021MODULEAPSTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../aps/AtModuleApsTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290021ModuleApsTestRunner
    {
    tAtModuleApsTestRunner super;
    eBool didSetup;
    }tAf60290021ModuleApsTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60290021ModuleApsTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021MODULEAPSTESTRUNNERINTERNAL_H_ */


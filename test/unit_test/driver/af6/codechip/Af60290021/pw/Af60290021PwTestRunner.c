/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : At60210029PwTestRunner.c
 *
 * Created Date: Aug 22, 2016
 *
 * Description : CEP test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pw/AtPwTestRunnerInternal.h"
#include "../../../../../../../driver/src/generic/pw/AtPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAt60290021PwTestRunner
    {
    tAtPwTestRunner super;
    }tAt60290021PwTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw TestedPw()
    {
    return (AtPw)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testCanForceTxActive()
    {
    AtPw pw = TestedPw();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwTxActiveForce(pw, cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwTxActiveIsForced(pw));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwTxActiveForce(pw, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwTxActiveIsForced(pw));
    }

static void testCanNotForceTxActiveOnNullPw()
    {
    eBool   isForced;
    AtPw pw = TestedPw();
    isForced = AtPwTxActiveIsForced(pw);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPwTxActiveForce(NULL, cAtTrue));
    TEST_ASSERT_EQUAL_INT(isForced, AtPwTxActiveIsForced(pw));
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPwTxActiveForce(NULL, cAtFalse));
    TEST_ASSERT_EQUAL_INT(isForced, AtPwTxActiveIsForced(pw));
    }

static void testForceTxActiveStatusMustDisableOnNullPw()
    {
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwTxActiveIsForced(NULL));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwCESoP_Fixtures)
        {
        new_TestFixture("testCanForceTxActive", testCanForceTxActive),
        new_TestFixture("testCanNotForceTxActiveOnNullPw", testCanNotForceTxActiveOnNullPw),
        new_TestFixture("testForceTxActiveStatusMustDisableOnNullPw", testForceTxActiveStatusMustDisableOnNullPw),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwCESoP_Caller, "TestSuite_At60290021Pw", NULL, NULL, TestSuite_AtPwCESoP_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwCESoP_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAt60290021PwTestRunner);
    }

static AtChannelTestRunner ObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner Af60290021PwTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, channel, moduleRunner);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60290021PwCustomerTestRunner.c
 *
 * Created Date: Sep 13, 2016
 *
 * Description : PW Customer Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/eth/Tha60290021ModuleEth.h"
#include "Af60290021PwTohPassThroughTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Af60290021PwCustomerTestRunner Runner()
    {
    return (Af60290021PwCustomerTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPw TestedPw(void)
    {
    return Af60290021PwTohPassThroughTestRunnerPwGet(Runner());
    }

static AtModuleEth ModuleEth(void)
    {
	AtDevice device = AtChannelDeviceGet((AtChannel) TestedPw());
	return (AtModuleEth) AtDeviceModuleGet(device, cAtModuleEth);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021PwCustomerTestRunner);
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    Af60290021PwCustomerTestRunner runner = (Af60290021PwCustomerTestRunner)self;
    AtPw pw = Af60290021PwTohPassThroughTestRunnerPwGet(runner);
    AtSprintf(buffer, "%s", AtObjectToString((AtObject)pw));
    return buffer;
    }

static void setup()
    {
    }

static void teardown()
    {
    }

static uint8* MacDefault(void)
    {
    static uint8 macDefault[6] = {0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D};
    return macDefault;
    }

static void testOnlyCanConfigureCVlan(void)
    {
    tAtEthVlanTag tag;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), MacDefault(), &tag, NULL));
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(cAtOk != AtPwEthHeaderSet(TestedPw(), MacDefault(), NULL, NULL));
    TEST_ASSERT(cAtOk != AtPwEthHeaderSet(TestedPw(), MacDefault(), NULL, &tag));
    AtTestLoggerEnable(cAtTrue);
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &tag));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &tag));
    }

static eBool VlansAreSame(const tAtEthVlanTag *tag1, const tAtEthVlanTag *tag2)
    {
    if ((tag1->priority == tag2->priority) &&
        (tag1->cfi      == tag2->cfi) &&
        (tag1->vlanId   == tag2->vlanId))
        return cAtTrue;

    return cAtFalse;
    }

static eBool MacIsSame(uint8 *mac1, uint8 *mac2)
    {
    return AtStrncmp((const char *)mac1, (const char *)mac2, 6) == 0 ? cAtTrue : cAtFalse;
    }

static void testChangeCVlanValue(void)
    {
    tAtEthVlanTag cVlan;
    tAtEthVlanTag cVlan_actual;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), MacDefault(), AtEthVlanTagConstruct(1, 1, 1, &cVlan), NULL));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &cVlan_actual));
    TEST_ASSERT(VlansAreSame(&cVlan, &cVlan_actual));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), MacDefault(), AtEthVlanTagConstruct(1, 1, 100, &cVlan), NULL));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &cVlan_actual));
    TEST_ASSERT(VlansAreSame(&cVlan, &cVlan_actual));

    }

static void ChangeDestMac(uint8* mac_cfg)
    {
    uint8 mac[6];
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthDestMacSet(TestedPw(), mac_cfg));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthDestMacGet(TestedPw(), mac));
    TEST_ASSERT(MacIsSame(mac, mac_cfg));
    }

static void testCanChangeDestMac(void)
    {
    uint8 mac_cfg[] = {0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD};
    ChangeDestMac(MacDefault());
    ChangeDestMac(mac_cfg);
    }

static void ChangeSourceMac(uint8* mac_cfg)
    {
    uint8 mac[6];
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthSrcMacSet(TestedPw(), mac_cfg));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthSrcMacGet(TestedPw(), mac));
    TEST_ASSERT(MacIsSame(mac, mac_cfg));
    }

static void testCanChangeSourceMac()
    {
    uint8 mac_cfg[] = {0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD};
    ChangeSourceMac(MacDefault());
    ChangeSourceMac(mac_cfg);
    }

static void tesConfigureNullCVlan(void)
    {
    tAtEthVlanTag sVlan;
    tAtEthVlanTag cVlan;
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(cAtOk != AtPwEthHeaderSet(TestedPw(), MacDefault(), NULL, NULL));
    AtTestLoggerEnable(cAtTrue);
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &cVlan));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &sVlan));
    }

/* Cannot set Ethernet header with NULL DMAC */
static void testCannotSetEthernetHeaderWithNullDMac()
    {
    tAtEthVlanTag cVlan;
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT((AtPwEthHeaderSet(TestedPw(), NULL, AtEthVlanTagConstruct(1, 1, 3, &cVlan), NULL) != cAtOk), cAtTrue);
    AtTestLoggerEnable(cAtTrue);
    }

static AtEthPort SgmiiEthPort(void)
	{
    return Tha60290021ModuleEthSgmiiDccKbytePortGet(ModuleEth());
	}

static void testPWBoundSGMIIPort()
    {
    AtEthPort port = AtPwEthPortGet(TestedPw());
    TEST_ASSERT(port != NULL);
    TEST_ASSERT(port == SgmiiEthPort());
    }

static void testCannotSetPsn()
    {
    AtPwMplsPsn mpls = AtPwMplsPsnNew();
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwPsnGet(TestedPw()) == NULL);
    TEST_ASSERT(AtPwPsnSet(TestedPw(), (AtPwPsn )mpls) != cAtOk);
    AtObjectDelete((AtObject)mpls);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotNullObject()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwEthSrcMacSet(NULL, NULL) != cAtOk);
    TEST_ASSERT(AtPwEthDestMacSet(NULL, NULL) != cAtOk);
    TEST_ASSERT(AtPwEthSrcMacSet(TestedPw(), NULL) != cAtOk);
    TEST_ASSERT(AtPwEthDestMacSet(TestedPw(), NULL) != cAtOk);
    TEST_ASSERT(AtPwEthSrcMacGet(NULL, NULL) != cAtOk);
    TEST_ASSERT(AtPwEthDestMacGet(NULL, NULL) != cAtOk);
    TEST_ASSERT(AtPwEthSrcMacGet(TestedPw(), NULL) != cAtOk);
    TEST_ASSERT(AtPwEthDestMacGet(TestedPw(), NULL) != cAtOk);
    TEST_ASSERT(AtPwEthVlanSet(NULL, NULL, NULL) != cAtOk);
    TEST_ASSERT(AtPwEthVlanSet(TestedPw(), NULL, NULL) != cAtOk);
    TEST_ASSERT(AtPwEthCVlanGet(NULL, NULL) == NULL);
    TEST_ASSERT(AtPwEthSVlanGet(NULL, NULL) == NULL);
    TEST_ASSERT(AtPwEthCVlanGet(TestedPw(), NULL) == NULL);
    TEST_ASSERT(AtPwEthSVlanGet(TestedPw(), NULL) == NULL);
    AtTestLoggerEnable(cAtTrue);
    }

/* Just confirm it not crash */
static void testGetPwType()
    {
    AtPwTypeGet(TestedPw());
    }

static AtEthPort EthPort40G(void)
	{
    return AtModuleEthPortGet(ModuleEth(), 0);
	}

static void testCannotSetEthPort()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwEthPortSet(TestedPw(), EthPort40G()) == cAtErrorNotApplicable);
    AtTestLoggerEnable(cAtTrue);

    /* only can set for SGMII port */
    TEST_ASSERT(AtPwEthPortSet(TestedPw(), SgmiiEthPort()) == cAtOk);
    }

static void testCannotUnbind()
    {
    TEST_ASSERT(AtPwCircuitUnbind(TestedPw()) == cAtErrorNotApplicable);
    }

static void testDontHaveCapacity()
    {
    TEST_ASSERT(AtPwRtpTimeStampModeIsSupported(TestedPw(), cAtPwRtpTimeStampModeDifferential) == cAtFalse);
    TEST_ASSERT(AtPwCwSequenceModeIsSupported(TestedPw(), cAtPwCwSequenceModeSkipZero) == cAtFalse);
    TEST_ASSERT(AtPwCwLengthModeIsSupported(TestedPw(), cAtPwCwLengthModeFullPacket) == cAtFalse);
    TEST_ASSERT(AtPwCwAutoRxLBitCanEnable(TestedPw(), cAtTrue) == cAtFalse);
    }

static void testFeaturesShouldNotApplicable()
    {
    tAtEthVlanTag vlan;
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwPayloadSizeSet(TestedPw(), 0x100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwReorderingEnable(TestedPw(), cAtFalse) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwReorderingEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwSuppressEnable(TestedPw(), cAtFalse) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwSuppressEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferSizeSet(TestedPw(), 500) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferDelaySet(TestedPw(), 500) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferSizeInPacketSet(TestedPw(), 3) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferDelayInPacketSet(TestedPw(), 3) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferAndPayloadSizeSet(TestedPw(), 1000, 500, 2048) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpEnable(TestedPw(), cAtFalse) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpTimeStampModeSet(TestedPw(), cAtPwRtpTimeStampModeDifferential) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpPayloadTypeSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpTxPayloadTypeSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpExpectedPayloadTypeSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpPayloadTypeCompare(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpPayloadTypeCompare(TestedPw(), cAtFalse) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpSsrcSet(TestedPw(), 22) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpTxSsrcSet(TestedPw(), 22) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpExpectedSsrcSet(TestedPw(), 22) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpSsrcCompare(TestedPw(), cAtFalse) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRtpSsrcCompare(TestedPw(), cAtTrue) == cAtErrorNotApplicable);

    TEST_ASSERT(AtPwCwEnable(TestedPw(), cAtFalse) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwAutoTxLBitEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwAutoRxLBitEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwAutoRBitEnable(TestedPw(), cAtTrue) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwSequenceModeSet(TestedPw(), 0) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwLengthModeSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCwPktReplaceModeSet(TestedPw(), cAtPwPktReplaceModeAis) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwIdleCodeSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwEthExpectedCVlanSet(TestedPw(), &vlan) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwEthExpectedSVlanSet(TestedPw(), &vlan) == cAtErrorNotApplicable);

    TEST_ASSERT(AtPwLopsSetThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwLopsClearThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwMissingPacketDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwExcessivePacketLossRateDefectThresholdSet(TestedPw(), 100, 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwStrayPacketDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwMalformedPacketDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwRemotePacketLossDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferOverrunDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwJitterBufferUnderrunDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwMisConnectionDefectThresholdSet(TestedPw(), 100) == cAtErrorNotApplicable);
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_At6029021PwCustomer_Fixtures)
        {
        new_TestFixture("testCanChangeSourceMac", testCanChangeSourceMac),
        new_TestFixture("testCanChangeDestMac", testCanChangeDestMac),
        new_TestFixture("testPWIsBoundSGMIIEthernetPort", testPWBoundSGMIIPort),
        new_TestFixture("testOnlyCanConfigureCVlan", testOnlyCanConfigureCVlan),
        new_TestFixture("testChangeCVlanValue", testChangeCVlanValue),
        new_TestFixture("tesConfigureNullCVlan", tesConfigureNullCVlan),
        new_TestFixture("testCannotSetPsn", testCannotSetPsn),
        new_TestFixture("testCannotSetEthernetHeaderWithNullDMac", testCannotSetEthernetHeaderWithNullDMac),
        new_TestFixture("testGetPwType", testGetPwType),
        new_TestFixture("testCannotSetEthPort", testCannotSetEthPort),
        new_TestFixture("testCannotUnbind", testCannotUnbind),
        new_TestFixture("testDontHaveCapacity", testDontHaveCapacity),
        new_TestFixture("testFeaturesShouldNotApplicable", testFeaturesShouldNotApplicable),
        new_TestFixture("testCannotNullObject", testCannotNullObject),

        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_At6029021PwCustomer_Caller, "TestSuite_At6029021PwCustomer", setup, teardown, TestSuite_At6029021PwCustomer_Fixtures);

    return (TestRef)((void *)&TestSuite_At6029021PwCustomer_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(Af60290021PwCustomerTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(Af60290021PwCustomerTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

Af60290021PwCustomerTestRunner Af60290021PwTohPassThroughTestRunnerObjectInit(Af60290021PwCustomerTestRunner self, AtPw pw)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPw Af60290021PwTohPassThroughTestRunnerPwGet(Af60290021PwCustomerTestRunner self)
    {
    return (AtPw)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60290021PwCustomerTestRunner.h
 * 
 * Created Date: Sep 13, 2016
 *
 * Description : PW customer speficic test
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021PWCUSTOMERTESTRUNNER_H_
#define _AF60290021PWCUSTOMERTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../man/AtObjectTestRunnerInternal.h"
#include "../../../../pw/AtModulePwTestRunner.h"
#include "AtModuleEth.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290021PwCustomerTestRunner * Af60290021PwCustomerTestRunner;

typedef struct tAf60290021PwCustomerTestRunnerethods
    {
    uint8 dummy;
    }tAf60290021PwCustomerTestRunnerethods;

typedef struct tAf60290021PwCustomerTestRunner
    {
    tAtObjectTestRunner super;
    const tAf60290021PwCustomerTestRunnerethods *methods;
    }tAf60290021PwCustomerTestRunner;


typedef struct tAf60290021PwCustomerDccTestRunner
    {
    tAf60290021PwCustomerTestRunner super;
    }tAf60290021PwCustomerDccTestRunner;

typedef struct tAf60290021PwCustomerKByteTestRunner
    {
    tAf60290021PwCustomerTestRunner super;
    }tAf60290021PwCustomerKByteTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Af60290021PwCustomerTestRunner Af60290021PwTohPassThroughTestRunnerObjectInit(Af60290021PwCustomerTestRunner self, AtPw pw);
AtPw Af60290021PwTohPassThroughTestRunnerPwGet(Af60290021PwCustomerTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021PWCUSTOMERTESTRUNNER_H_ */


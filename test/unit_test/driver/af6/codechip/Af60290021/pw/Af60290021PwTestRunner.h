/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO
 * 
 * File        : Af60290021PwTestRunner.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : TODO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef AF60290021PWTESTRUNNER_H_
#define AF60290021PWTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../pw/AtPwTestRunnerInternal.h"
#include "Af60290021PwTohPassThroughTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner Af60290021PwTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
Af60290021PwCustomerTestRunner Af60290021PwCustomerKByteTestRunnerNew(AtPw pw);
Af60290021PwCustomerTestRunner Af60290021PwDccTestRunnerNew(AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* AF60290021PWTESTRUNNER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60290021ModulePwTestRunner.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : PW unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "../../../../../../../driver/src/implement/default/pw/ThaModulePw.h"
#include "../../../../../../../driver/src/generic/encap/AtEncapChannelInternal.h"
#include "../../../../man/channel_runner/AtChannelTestRunner.h"
#include "../sdh/Af60290021ModuleSdhTest.h"
#include "../man/Af60290021DeviceTestRunner.h"
#include "Af60290021ModulePwTestRunnerInternal.h"
#include "Af60290021PwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;
static tAtUnittestRunnerMethods              m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner Runner()
    {
    return (AtModuleTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModulePw ModulePw()
    {
    return (AtModulePw)AtModuleTestRunnerModuleGet(Runner());
    }

static AtModuleSdh ModuleSdh()
    {
    AtDevice device = AtModuleTestRunnerDeviceGet(Runner());
    return (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eBool ShouldTestPwConstrain(AtModulePwTestRunner runner)
    {
    return cAtFalse;
    }

static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    AtDeviceTestRunner deviceRunner = AtModuleTestRunnerDeviceRunnerGet((AtModuleTestRunner)self);
    uint32 numPws = 5376;

    if (Af60290021DeviceTestRunnerFullCapacityOpened(deviceRunner))
        return numPws * 2;

    return numPws;
    }

static AtModuleSdh SdhModule(AtModulePwTestRunner self)
    {
    AtModule module = AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtDevice device = AtModuleDeviceGet(module);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eBool ShouldTestLine(AtModulePwTestRunner self, uint8 lineId)
    {
    return Af6029ModuleSdhIsTerminatedLine(SdhModule(self), lineId);
    }

static uint8 *AllTestedLinesAllocate(AtModulePwTestRunner self, uint8 *numLines)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return Af6029ModuleSdhAllApplicableLinesAllocate(sdhModule, numLines);
    }

static void testHoVcFacePlateLineCanNotBindPw(void)
    {
    uint32 pwId;
    AtPw pw;
    AtSdhVc vc3;
    AtModulePw pwModule = ModulePw();
    AtModuleSdh sdhModule = ModuleSdh();
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 0);

    mCliSuccessAssert(AtCliExecuteFormat("sdh line rate 1 stm16"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug16.1.1 4xaug4s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug4.1.1 4xaug1s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.1.1 3xvc3s"));
    mCliSuccessAssert(AtCliExecuteFormat("pw create cep 1 basic"));

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
        {
        pw = AtModulePwGetPw(pwModule, pwId);
        if (pw != NULL)
            continue;

        TEST_ASSERT(AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic) != NULL);
        break;
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(pwId < AtModulePwMaxPwsGet(pwModule));
    vc3 = AtSdhLineVc3Get(line, 0, 0);
    TEST_ASSERT(vc3 != NULL);
    pw = AtModulePwGetPw(pwModule, pwId);
    TEST_ASSERT(AtPwCircuitBind(pw, (AtChannel)vc3) != cAtOk);
    TEST_ASSERT(AtModulePwDeletePw(pwModule, pwId) == cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testHoVcMatePlateLineCanNotBindPw(void)
    {
    uint32 pwId;
    AtPw pw;
    AtSdhVc vc3;
    AtModulePw pwModule = ModulePw();
    AtModuleSdh sdhModule = ModuleSdh();
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 16);

    TEST_ASSERT(line != NULL);
    mCliSuccessAssert(AtCliExecuteFormat("sdh line rate 17 stm16"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug16.17.1 4xaug4s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug4.17.1 4xaug1s"));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.17.1 3xvc3s"));

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
        {
        pw = AtModulePwGetPw(pwModule, pwId);
        if (pw != NULL)
            continue;

        TEST_ASSERT(AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic) != NULL);
        break;
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(pwId < AtModulePwMaxPwsGet(pwModule));
    vc3 = AtSdhLineVc3Get(line, 0, 0);
    TEST_ASSERT(vc3 != NULL);
    pw = AtModulePwGetPw(pwModule, pwId);
    TEST_ASSERT(AtPwCircuitBind(pw, (AtChannel )vc3) != cAtOk);
    TEST_ASSERT(AtModulePwDeletePw(pwModule, pwId) == cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static AtModulePw PwModule(AtModulePwTestRunner self)
    {
    AtModule module = AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtDevice device = AtModuleDeviceGet(module);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static void TestKbytePw(AtModulePwTestRunner self)
    {
    AtPw pw = ThaModulePwKBytePwGet((ThaModulePw)PwModule(self));
    AtUnittestRunner runner;

    if (pw == NULL)
        return;

    runner = (AtUnittestRunner) Af60290021PwCustomerKByteTestRunnerNew(pw);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static eBool NeedToCreateDccPw(AtModulePwTestRunner self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedToDeleteDccPw(AtModulePwTestRunner self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void TestPwDccChannel(AtModulePwTestRunner self, AtHdlcChannel dccChannel)
    {
    AtPwHdlc pw;
    AtSdhLine line;
    AtUnittestRunner runner;

    if (dccChannel == NULL)
        return;

    if (NeedToCreateDccPw(self))
        pw = AtModulePwHdlcCreate(PwModule(self), dccChannel);
    else
        pw = (AtPwHdlc) AtChannelBoundPwGet((AtChannel) dccChannel);

    TEST_ASSERT_NOT_NULL(pw);

    runner = (AtUnittestRunner) Af60290021PwDccTestRunnerNew((AtPw) pw);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);

    line = (AtSdhLine) AtEncapChannelBoundPhyGet((AtEncapChannel) dccChannel);
    TEST_ASSERT_NOT_NULL(line);
    if (NeedToDeleteDccPw(self))
        {
        TEST_ASSERT(AtModulePwDeletePwObject(PwModule(self), (AtPw )pw) == cAtOk);
        }

    TEST_ASSERT(AtSdhLineDccChannelDelete(line, dccChannel) == cAtOk);
    }

static void TestDccPw(AtModulePwTestRunner self)
    {
    uint8 lineId;
    AtModuleSdh sdhModule = SdhModule(self);

    for (lineId = 0; lineId < AtModuleSdhMaxLinesGet(sdhModule); lineId++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        if (line == NULL)
            continue;

        TestPwDccChannel(self, AtSdhLineDccChannelCreate(line, cAtSdhLineDccLayerSection));
        TestPwDccChannel(self, AtSdhLineDccChannelCreate(line, cAtSdhLineDccLayerLine));
        TestPwDccChannel(self, AtSdhLineDccChannelCreate(line, cAtSdhLineDccLayerSection|cAtSdhLineDccLayerLine));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60290021ModulePwTestRunner_Fixtures)
        {
        new_TestFixture("testHoVcFacePlateLineCanNotBindPw", testHoVcFacePlateLineCanNotBindPw),
        new_TestFixture("testHoVcMatePlateLineCanNotBindPw", testHoVcMatePlateLineCanNotBindPw)
        };

    EMB_UNIT_TESTCALLER(TestSuite_Af60290021ModulePwTestRunner_Caller, "Af60290021ModulePwTestRunner", NULL, NULL, TestSuite_Af60290021ModulePwTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60290021ModulePwTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtEthPort EthPortForChannel(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, 0);
    }

static AtEthPort EthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel)
    {
    return EthPortForChannel((AtChannel)channel);
    }

static AtEthPort EthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    return EthPortForChannel((AtChannel)channel);
    }

static AtUnittestRunner CreateCepPwTestRunner(AtModulePwTestRunner self, AtPw pw)
    {
    return (AtUnittestRunner)Af60290021PwTestRunnerNew((AtChannel)pw, (AtModuleTestRunner)self);
    }

static void Run(AtUnittestRunner self)
    {
    TestDccPw((AtModulePwTestRunner) self);
    TestKbytePw((AtModulePwTestRunner) self);
    m_AtUnittestRunnerMethods->Run(self);
    }

static eAtRet EthLoopback(AtModulePwTestRunner self, AtEthPort ethPort)
    {
    AtSerdesController serdes = AtEthPortSerdesController(ethPort);
    return AtSerdesControllerLoopbackSet(serdes, cAtLoopbackModeLocal);
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumPws);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, AllTestedLinesAllocate);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestPwConstrain);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForSdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForPdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CreateCepPwTestRunner);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthLoopback);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModulePwTestRunner);
    }

AtModuleTestRunner Af60290021ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210051ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290021ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60290021ModulePwTestRunnerObjectInit(newRunner, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FIXME module name
 * 
 * File        : Af60290021ModulePwTestRunnerInternal.h
 * 
 * Created Date: Sep 19, 2018
 *
 * Description : FIXME Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021MODULEPWTESTRUNNERINTERNAL_H_
#define _AF60290021MODULEPWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/pw/Af60210051ModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290021ModulePwTestRunner
    {
    tAf60210051ModulePwTestRunner super;
    }tAf60290021ModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60290021ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021MODULEPWTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW Module
 *
 * File        : Af60290021PwCustomerKByteTestRunner.c
 *
 * Created Date: Sep 13, 2016
 *
 * Description : PW Kbyte Customer Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/pw/Tha60290021ModulePw.h"
#include "../../../../../../../driver/src/implement/default/pw/ThaModulePw.h"
#include "AtCiena.h"
#include "Af60290021PwTohPassThroughTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Af60290021PwCustomerTestRunner Runner()
    {
    return (Af60290021PwCustomerTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPw TestedPw(void)
    {
    return Af60290021PwTohPassThroughTestRunnerPwGet(Runner());
    }

static AtModulePw ModulePw(void)
    {
    return (AtModulePw) AtChannelModuleGet((AtChannel) TestedPw());
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021PwCustomerKByteTestRunner);
    }

static void testChangeKBytePwVersion()
    {
    AtModulePw pwModule = (AtModulePw)ModulePw();

    uint8 value = 0;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwVersionSet(pwModule, value));
    TEST_ASSERT(value == AtCienaKBytePwVersionGet(pwModule));

    value = 0xF;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwVersionSet(pwModule, value));
    TEST_ASSERT(value == AtCienaKBytePwVersionGet(pwModule));

    value = 0xFF;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwVersionSet(pwModule, value));
    TEST_ASSERT(value == AtCienaKBytePwVersionGet(pwModule));
    }

static void testChangeKBytePwType()
    {
    uint8 value = 0;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwTypeSet(ModulePw(), value));
    TEST_ASSERT(value == AtCienaKBytePwTypeGet(ModulePw()));

    value = 0xF;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwTypeSet(ModulePw(), value));
    TEST_ASSERT(value == AtCienaKBytePwTypeGet(ModulePw()));

    value = 0xFF;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwTypeSet(ModulePw(), value));
    TEST_ASSERT(value == AtCienaKBytePwTypeGet(ModulePw()));
    }

static void testChangeKBytePwEthType()
    {
    uint16 value = 0;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwEthTypeSet(ModulePw(), value));
    TEST_ASSERT(value == AtCienaKBytePwEthTypeGet(ModulePw()));

    value = 0x7FF;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwEthTypeSet(ModulePw(), value));
    TEST_ASSERT(value == AtCienaKBytePwEthTypeGet(ModulePw()));

    value = 0xFFFF;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtCienaKBytePwEthTypeSet(ModulePw(), value));
    TEST_ASSERT(value == AtCienaKBytePwEthTypeGet(ModulePw()));
    }

static void testCannotNullObject()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtCienaKBytePwEthTypeSet(NULL, 0x0) != cAtOk);
    TEST_ASSERT(AtCienaKBytePwTypeSet(NULL, 0x0) != cAtOk);
    TEST_ASSERT(AtCienaKBytePwVersionSet(NULL, 0x0) != cAtOk);
    AtCienaKBytePwVersionGet(NULL);
    AtCienaKBytePwEthTypeGet(NULL);
    AtCienaKBytePwTypeGet(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanNotBindWithAnyChannel()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCircuitUnbind((AtPw)TestedPw()) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwCircuitBind((AtPw)TestedPw(), (AtChannel)TestedPw()) == cAtErrorNotApplicable);
    TEST_ASSERT(AtPwBoundCircuitGet((AtPw)TestedPw()) == NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_At6029021KbytePw_Fixtures)
        {
        new_TestFixture("testChangeKBytePwVersion", testChangeKBytePwVersion),
        new_TestFixture("testChangeKBytePwType", testChangeKBytePwType),
        new_TestFixture("testChangeKBytePwEthType", testChangeKBytePwEthType),
        new_TestFixture("testCanNotBindWithAnyChannel", testCanNotBindWithAnyChannel),
        new_TestFixture("testCannotNullObject", testCannotNullObject),

        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_At6029021KbytePw_Caller, "TestSuite_At6029021KbytePw", NULL, NULL, TestSuite_At6029021KbytePw_Fixtures);

    return (TestRef)((void *)&TestSuite_At6029021KbytePw_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(Af60290021PwCustomerTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(Af60290021PwCustomerTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static Af60290021PwCustomerTestRunner ObjectInit(Af60290021PwCustomerTestRunner self, AtPw pw)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021PwTohPassThroughTestRunnerObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Af60290021PwCustomerTestRunner Af60290021PwCustomerKByteTestRunnerNew(AtPw pw)
    {
    Af60290021PwCustomerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, pw);
    }

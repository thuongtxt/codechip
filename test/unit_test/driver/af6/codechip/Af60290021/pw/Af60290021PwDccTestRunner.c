/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW Module
 *
 * File        : Af60290021PwCustomerDccTestRunner.c
 *
 * Created Date: Sep 13, 2016
 *
 * Description : PW DCC Customer Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/pw/Tha60290021ModulePw.h"
#include "Af60290021PwTohPassThroughTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Af60290021PwCustomerTestRunner Runner()
    {
    return (Af60290021PwCustomerTestRunner)AtUnittestRunnerCurrentRunner();
    }

static Tha60290021PwHdlc TestedPw(void)
    {
    return (Tha60290021PwHdlc)Af60290021PwTohPassThroughTestRunnerPwGet(Runner());
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021PwCustomerDccTestRunner);
    }

static void testChangeDccPwVersion()
    {
    uint8 value = 0;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwVersionSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwVersionGet(TestedPw()));

    value = 0xF;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwVersionSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwVersionGet(TestedPw()));

    value = 0xFF;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwVersionSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwVersionGet(TestedPw()));
    }

static void testChangeDccPwType()
    {
    uint8 value = 0;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwTypeSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwTypeGet(TestedPw()));

    value = 0xF;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwTypeSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwTypeGet(TestedPw()));

    value = 0xFF;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwTypeSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwTypeGet(TestedPw()));
    }

static void testChangeDccPwEthType()
    {
    uint16 value = cThaDccPwEthTypeDefault;
    TEST_ASSERT_EQUAL_INT(cAtOk, Tha6029PwDccPwEthTypeSet(TestedPw(), value));
    TEST_ASSERT(value == Tha6029PwDccPwEthTypeGet(TestedPw()));

    value = 0x7FF;
    TEST_ASSERT(cAtOk != Tha6029PwDccPwEthTypeSet(TestedPw(), value));
    TEST_ASSERT(cThaDccPwEthTypeDefault == Tha6029PwDccPwEthTypeGet(TestedPw()));
    }

static void testCannotNullObject()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(Tha6029PwDccPwEthTypeSet(NULL, 0x0) != cAtOk);
    TEST_ASSERT(Tha6029PwDccPwTypeSet(NULL, 0x0) != cAtOk);
    TEST_ASSERT(Tha6029PwDccPwVersionSet(NULL, 0x0) != cAtOk);
    Tha6029PwDccPwEthTypeGet(NULL);
    Tha6029PwDccPwTypeGet(NULL);
    Tha6029PwDccPwVersionGet(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_At6029021DccPw_Fixtures)
        {
        new_TestFixture("testChangeDccPwVersion", testChangeDccPwVersion),
        new_TestFixture("testChangeDccPwType", testChangeDccPwType),
        new_TestFixture("testChangeDccPwEthType", testChangeDccPwEthType),
        new_TestFixture("testCannotNullObject", testCannotNullObject),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_At6029021DccPw_Caller, "TestSuite_At6029021DccPw", NULL, NULL, TestSuite_At6029021DccPw_Fixtures);

    return (TestRef)((void *)&TestSuite_At6029021DccPw_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(Af60290021PwCustomerTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(Af60290021PwCustomerTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static Af60290021PwCustomerTestRunner ObjectInit(Af60290021PwCustomerTestRunner self, AtPw pw)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021PwTohPassThroughTestRunnerObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Af60290021PwCustomerTestRunner Af60290021PwDccTestRunnerNew(AtPw pw)
    {
    Af60290021PwCustomerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, pw);
    }

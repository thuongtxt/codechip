/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Af60290021VcCrossConnectTestRunner.h
 * 
 * Created Date: Jul 15, 2016
 *
 * Description : VC Cross connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021VCCROSSCONNECTTESTRUNNER_H_
#define _AF60290021VCCROSSCONNECTTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../xc/AtCrossConnectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnectTestRunner Af60290021VcCrossConnectTestRunnerNew(AtModuleXcTestRunner moduleRunner, AtCrossConnect xc);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021VCCROSSCONNECTTESTRUNNER_H_ */


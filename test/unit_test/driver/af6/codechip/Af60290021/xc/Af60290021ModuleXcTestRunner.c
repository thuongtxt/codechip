/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Af60290021ModuleXcTestRunner.c
 *
 * Created Date: Jul 15, 2016
 *
 * Description : XC Module unit-test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210011/xc/Af60210011ModuleXcTestRunnerInternal.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/sdh/Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290021ModuleXcTestRunner
    {
    tAf60210011ModuleXcTestRunner super;
    }tAf60290021ModuleXcTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAf60210011ModuleXcTestRunnerMethods m_Af60210011ModuleXcTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(Af60210011ModuleXcTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner) self);
    return (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint8 StartLineId(Af60210011ModuleXcTestRunner self)
    {
    return Tha60290021ModuleSdhTerminatedLineStartId(SdhModule(self));
    }

static uint8 MaxNumLines(Af60210011ModuleXcTestRunner self)
    {
    return Tha60290021ModuleSdhNumTerminatedLines(SdhModule(self));
    }

static uint8 AnyLoLine(Af60210011ModuleXcTestRunner self)
    {
    static uint8 currentLineId = 0;
    uint8 lineId = currentLineId;
    currentLineId = (currentLineId + 1) % MaxNumLines(self);

    return lineId + StartLineId(self);
    }

static void OverrideAtModuleXcTestRunner(AtModuleXcTestRunner self)
    {
    Af60210011ModuleXcTestRunner runner = (Af60210011ModuleXcTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_Af60210011ModuleXcTestRunnerOverride, mMethodsGet(runner), sizeof(m_Af60210011ModuleXcTestRunnerOverride));

        mMethodOverride(m_Af60210011ModuleXcTestRunnerOverride, AnyLoLine);
        }

    mMethodsSet(runner, &m_Af60210011ModuleXcTestRunnerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModuleXcTestRunner);
    }

static void Override(AtModuleXcTestRunner self)
    {
    OverrideAtModuleXcTestRunner(self);
    }

static AtModuleXcTestRunner ObjectInit(AtModuleXcTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210011ModuleXcTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXcTestRunner Af60290021ModuleXcTestRunnerNew(AtModule module)
    {
    AtModuleXcTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Af60290021ModuleXcTestRunner.h
 * 
 * Created Date: Jul 15, 2016
 *
 * Description : XC Module unitest header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021MODULEXCTESTRUNNER_H_
#define _AF60290021MODULEXCTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../xc/AtModuleXcTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXcTestRunner Af60290021ModuleXcTestRunnerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021MODULEXCTESTRUNNER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Af60290021VcCrossConnectTestRunner.c
 *
 * Created Date: Jul 15, 2016
 *
 * Description : VC Cross Connect test for af60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210011/xc/Af60210011VcCrossConnectTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290021VcCrossConnectTestRunner
    {
    tAf60210011VcCrossConnectTestRunner super;
    }tAf60290021VcCrossConnectTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021VcCrossConnectTestRunner);
    }

static AtCrossConnectTestRunner ObjectInit(AtCrossConnectTestRunner self, AtModuleXcTestRunner moduleRunner, AtCrossConnect xc)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtCrossConnectTestRunnerObjectInit(self, moduleRunner, xc) == NULL)
        return NULL;

    return self;
    }

AtCrossConnectTestRunner Af60290021VcCrossConnectTestRunnerNew(AtModuleXcTestRunner moduleRunner, AtCrossConnect xc)
    {
    AtCrossConnectTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, moduleRunner, xc);
    }

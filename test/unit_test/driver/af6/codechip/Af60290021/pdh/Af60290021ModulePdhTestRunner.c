/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60290021ModulePdhTestRunner.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : PDH unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pdh/AtModulePdhTestRunnerInternal.h"
#include "../sdh/Af60290021ModuleSdhTest.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModule(self) (AtModulePdh)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290021ModulePdhTestRunner
    {
    tAtModulePdhTestRunner super;
    }tAf60290021ModulePdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;

/* Save super implementation */
static const tAtModulePdhTestRunnerMethods *m_AtModulePdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtModulePdhTestRunner self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)mModule(self));
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eBool ShouldTestDe1InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    return Af6029ModuleSdhIsTerminatedLine(SdhModule(self), lineId);
    }

static eBool ShouldTestDe3InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    return Af6029ModuleSdhIsTerminatedLine(SdhModule(self), lineId);
    }

static eBool ShouldTestMaximumNumberOfDe1Get(AtModulePdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool CanTestDe1Prm(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static uint8 *AllTestedLinesAllocate(AtModulePdhTestRunner self, uint8 *numLines)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return Af6029ModuleSdhAllApplicableLinesAllocate(sdhModule, numLines);
    }

static void OverrideModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner) self;
    if (!m_methodsInit)
        {
        m_AtModulePdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestMaximumNumberOfDe1Get);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, CanTestDe1Prm);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, AllTestedLinesAllocate);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestDe1InLine);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestDe3InLine);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModulePdhTestRunner);
    }

AtModuleTestRunner Af60290021ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290021ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return Af60290021ModulePdhTestRunnerObjectInit(newRunner, module);
    }

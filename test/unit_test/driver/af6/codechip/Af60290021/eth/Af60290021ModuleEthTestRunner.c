/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Af60290021ModuleEthTestRunner.c
 *
 * Created Date: Jul 21, 2016
 *
 * Description : ETH module test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/eth/Tha60290021ModuleEth.h"
#include "../../../../../../../driver/src/generic/eth/AtModuleEthInternal.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290021/man/Tha60290021Device.h"
#include "Af60290021ModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModule(self) (AtModuleEth)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAf60290021ModuleEthTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods      m_AtUnittestRunnerOverride;
static tAtModuleEthTestRunnerMethods m_AtModuleEthTestRunnerOverride;

/* Methods */
static const tAtUnittestRunnerMethods      *m_AtUnittestRunnerMethods      = NULL;
static const tAtModuleEthTestRunnerMethods *m_AtModuleEthTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 MaxVlanId()
    {
    return 4095;
    }

static uint16 MaxVlanPrio()
    {
    return 7;
    }

static uint16 MaxVlanCFI()
    {
    return 1;
    }

static uint32 FlowControlHighThresholdDefault()
    {
    return 192;
    }

static uint32 FlowControlLowThresholdDefault()
    {
    return 64;
    }

static AtModuleEth TestedModule()
    {
    return (AtModuleEth)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static uint32 FacePlatePortEnd()
    {
    AtModuleEth module = TestedModule();
    return Tha60290021ModuleEthStopFaceplatePortId(module);;
    }

static uint32 Start40GBackplaneMacId()
    {
    AtModuleEth module = TestedModule();
    return Tha60290021ModuleEthStart40GBackplaneMacId(module);
    }

static uint32 Stop40GBackplaneMacId()
    {
    AtModuleEth module = TestedModule();
    return Tha60290021ModuleEthStop40GBackplaneMacId(module);
    }

static eBool FlowControlIsSupported(AtModuleEthTestRunner self)
    {
    return cAtFalse;
    }

static eBool IsFacePlatePort(uint8 portId)
    {
    AtModuleEth module = TestedModule();
    uint32 minId = Tha60290021ModuleEthStartFaceplatePortId(module);
    uint32 maxId = Tha60290021ModuleEthStopFaceplatePortId(module);
    return ((portId >= minId) && (portId <= maxId)) ? cAtTrue : cAtFalse;
    }

static eBool EthPortHasMacChecking(AtModuleEthTestRunner self, AtEthPort port)
    {
    return AtEthPortMacCheckingCanEnable(port, cAtTrue);
    }

static eBool EthPortHasMac(AtModuleEthTestRunner self, AtEthPort port)
    {
    if (Tha60290021ModuleEthIs40GInternalPort(port))
        return cAtTrue;

    if (Tha60290021EthPortIsSgmiiPort(port))
        return cAtTrue;

    if (Tha60290021ModuleEthIsFaceplatePort(port))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    return AtEthPortRxIpgIsConfigurable(port);
    }

static eBool TxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    return AtEthPortTxIpgIsConfigurable(port);
    }

static uint32 NumEthPorts(AtModuleEthTestRunner self)
    {
    return 23;
    }

static eBool PortHasSubPortVlans(uint8 portId)
    {
    if (IsFacePlatePort(portId))
        return cAtTrue;

    return cAtFalse;
    }

static void testOnlyFaceplatePortCanBeBypassed(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);

        TEST_ASSERT_NOT_NULL(port);

        if (IsFacePlatePort(port_i))
            {
            TEST_ASSERT(AtCienaEthPortBypassEnable(port, cAtTrue) == cAtOk);
            TEST_ASSERT(AtCienaEthPortBypassIsEnabled(port) == cAtTrue);
            TEST_ASSERT(AtCienaEthPortBypassEnable(port, cAtFalse) == cAtOk);
            TEST_ASSERT(AtCienaEthPortBypassIsEnabled(port) == cAtFalse);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtCienaEthPortBypassEnable(port, cAtTrue)  != cAtOk);
            TEST_ASSERT(AtCienaEthPortBypassEnable(port, cAtFalse) == cAtOk);
            TEST_ASSERT(AtCienaEthPortBypassIsEnabled(port) == cAtFalse);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static tAtVlan *AnyVlan()
    {
    static tAtVlan vlan;

    AtOsalMemInit(&vlan, 0, sizeof(vlan));
    vlan.tpid = AtCienaModuleEthSubPortVlanTxTpidGet(TestedModule(), 0);
    vlan.vlanId = 100;

    return &vlan;
    }

static void TestCannotConfigureNullSubPortVlan(eAtRet (*TxVlanSet)(AtEthPort self, const tAtVlan *vlan))
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        TEST_ASSERT_NOT_NULL(port);
        TEST_ASSERT(TxVlanSet(port, NULL) != cAtOk);
        }
    }

static void TestCanConfigureSubPortVlan(eAtRet (*TxVlanSet)(AtEthPort self, const tAtVlan *vlan),
                                        eAtRet (*TxVlanGet)(AtEthPort self, tAtVlan *vlan))
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;
    tAtVlan *vlan = AnyVlan();
    tAtVlan actualVlan;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        vlan->vlanId = port_i % 4096;

        TEST_ASSERT_NOT_NULL(port);

        if (PortHasSubPortVlans(port_i))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, TxVlanSet(port, vlan));
            TEST_ASSERT_EQUAL_INT(cAtOk, TxVlanGet(port, &actualVlan));
            TEST_ASSERT(AtOsalMemCmp(&actualVlan, vlan, sizeof(vlan)) == 0);
            }
        else
            {
            TEST_ASSERT(TxVlanSet(port, vlan) != cAtOk);
            TEST_ASSERT(TxVlanGet(port, &actualVlan) != cAtOk);
            }
        }
    }

static void TestCannotConfigureSameExpectedSubPortVlan(uint8 configuredPort, const tAtVlan *configuredVlan)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        TEST_ASSERT_NOT_NULL(port);

        if (port_i == configuredPort)
            {
            TEST_ASSERT(AtCienaEthPortSubportExpectedVlanSet(port, configuredVlan) == cAtOk);
            }
        else
            {
            TEST_ASSERT(AtCienaEthPortSubportExpectedVlanSet(port, configuredVlan) != cAtOk);
            }
        }
    }

static void TestCannotConfigureSubPortInvalidVlanId(eAtRet (*TxVlanSet)(AtEthPort self, const tAtVlan *vlan))
    {
    AtModuleEth module = TestedModule();
    tAtVlan *vlan = AnyVlan();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        vlan->vlanId = MaxVlanId() + 1;
        TEST_ASSERT_NOT_NULL(port);

        TEST_ASSERT(TxVlanSet(port, NULL) != cAtOk);
        }
    }

static void TestCannotConfigureSubPortInvalidVlanPri(eAtRet (*TxVlanSet)(AtEthPort self, const tAtVlan *vlan))
    {
    AtModuleEth module = TestedModule();
    tAtVlan *vlan = AnyVlan();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        vlan->priority = MaxVlanPrio() + 1;
        TEST_ASSERT_NOT_NULL(port);

        TEST_ASSERT(TxVlanSet(port, NULL) != cAtOk);
        }
    }

static void TestCannotConfigureSubPortInvalidVlanCfi(eAtRet (*TxVlanSet)(AtEthPort self, const tAtVlan *vlan))
    {
    AtModuleEth module = TestedModule();
    tAtVlan *vlan = AnyVlan();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        vlan->cfi = MaxVlanCFI() + 1;
        TEST_ASSERT_NOT_NULL(port);

        TEST_ASSERT(TxVlanSet(port, NULL) != cAtOk);
        }
    }

static void testCanConfigureTxSubPortVlan(void)
    {
    TestCanConfigureSubPortVlan(AtCienaEthPortSubportTxVlanSet, AtCienaEthPortSubportTxVlanGet);
    }

static void testCannotConfigureNullTxSubPortVlan(void)
    {
    TestCannotConfigureNullSubPortVlan(AtCienaEthPortSubportTxVlanSet);
    }

static void testCannotConfigureTxSubPortVlanOnNullPort(void)
    {
    TEST_ASSERT(AtCienaEthPortSubportTxVlanSet(NULL, AnyVlan()) != cAtOk);
    }

static void testCanConfigureExpectedSubPortVlan(void)
    {
    TestCanConfigureSubPortVlan(AtCienaEthPortSubportExpectedVlanSet, AtCienaEthPortSubportExpectedVlanGet);
    }

static void testCannotConfigureNullExpectedSubPortVlan(void)
    {
    TestCannotConfigureNullSubPortVlan(AtCienaEthPortSubportExpectedVlanSet);
    }

static void testCannotConfigureSubPortVlanOnNullPort(void)
    {
    TEST_ASSERT(AtCienaEthPortSubportExpectedVlanSet(NULL, AnyVlan()) != cAtOk);
    }

static void testCannotConfigureSameExpectedSubportVlanForMoreThanOnePorts()
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;
    tAtVlan expectedVlan;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);
        TEST_ASSERT_NOT_NULL(port);

        if (IsFacePlatePort(port_i))
            {
            TEST_ASSERT(AtCienaEthPortSubportExpectedVlanGet(port, &expectedVlan) == cAtOk);
            TestCannotConfigureSameExpectedSubPortVlan(port_i, &expectedVlan);
            }
        }
    }

static void testCannotConfigureTxSubPortInvalidVlanId(void)
    {
    TestCannotConfigureSubPortInvalidVlanId(AtCienaEthPortSubportTxVlanSet);
    }

static void testCannotConfigureTxSubPortInvalidVlanPri(void)
    {
    TestCannotConfigureSubPortInvalidVlanPri(AtCienaEthPortSubportTxVlanSet);
    }

static void testCannotConfigureTxSubPortInvalidVlanCfi(void)
    {
    TestCannotConfigureSubPortInvalidVlanCfi(AtCienaEthPortSubportTxVlanSet);
    }

static void testCannotConfigureExpectedSubPortInvalidVlanId(void)
    {
    TestCannotConfigureSubPortInvalidVlanId(AtCienaEthPortSubportExpectedVlanSet);
    }

static void testCannotConfigureExpectedSubPortInvalidVlanPri(void)
    {
    TestCannotConfigureSubPortInvalidVlanPri(AtCienaEthPortSubportExpectedVlanSet);
    }

static void testCannotConfigureExpectedSubPortInvalidVlanCfi(void)
    {
    TestCannotConfigureSubPortInvalidVlanCfi(AtCienaEthPortSubportExpectedVlanSet);
    }

static AtSerdesManager SerdesManager()
    {
    AtModuleEth module = TestedModule();
    AtDevice device = AtModuleDeviceGet((AtModule)module);
    return AtDeviceSerdesManagerGet(device);
    }

static void testNumberOfSERDESControllerReturnedByETHModuleMustEqualToThatOfSERDESManager()
    {
    AtModuleEth module = TestedModule();
    TEST_ASSERT_EQUAL_INT(AtSerdesManagerNumSerdesControllers(SerdesManager()), AtModuleEthNumSerdesControllers(module));
    }

static void testAllOfSERDESInstancesReturnedByETHModuleMustBeTheSameAsThoseReturnedBySERDESManager()
    {
    AtModuleEth module = TestedModule();
    AtSerdesManager serdesManager = SerdesManager();
    uint32 numSerdes = AtModuleEthNumSerdesControllers(module);
    uint32 serdes_i;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = AtModuleEthSerdesController(module, serdes_i);
        TEST_ASSERT(serdes == AtSerdesManagerSerdesControllerGet(serdesManager, serdes_i));
        }
    }

static AtEthPort Mac40GPort()
    {
    AtModuleEth ethModule = TestedModule();
    return AtModuleEthPortGet(ethModule, 0);
    }

static uint32 BackplaneSerdesId(Af60290021ModuleEthTestRunner self)
    {
    AtUnused(self);
    return 24;
    }

static void testEthernet40GSERDESMustBeAssociatedCorrectly()
    {
    Af60290021ModuleEthTestRunner runner = (Af60290021ModuleEthTestRunner)AtUnittestRunnerCurrentRunner();
    uint32 backplaneSerdesId = mMethodsGet(runner)->BackplaneSerdesId(runner);
    TEST_ASSERT(AtEthPortSerdesController(Mac40GPort()) == AtSerdesManagerSerdesControllerGet(SerdesManager(), backplaneSerdesId));
    }

static void testAllOfETHMACAtFaceplateSideShouldHaveWeakReferenceToCorrespondingFaceplateSERDES()
    {
    AtModuleEth ethModule = TestedModule();
    uint8 faceplateSerdesId = 0;
    uint8 startPortId = Tha60290021ModuleEthStartFaceplatePortId(ethModule);
    uint8 stopPorts = Tha60290021ModuleEthStopFaceplatePortId(ethModule);
    uint8 portId;
    AtSerdesManager serdesManager = SerdesManager();

    for (portId = startPortId; portId <= stopPorts; portId++)
        {
        AtEthPort port = AtModuleEthPortGet(TestedModule(), portId);
        AtSerdesController serdes = AtEthPortSerdesController(port);
        TEST_ASSERT(AtSerdesManagerSerdesControllerGet(serdesManager, faceplateSerdesId) == serdes);
        faceplateSerdesId = faceplateSerdesId + 1;
        }
    }

static void testIfFaceplateSerdesWorkInEthMode_TheyMustWorkCloselyWithCorrespondingPort()
    {
    uint8 faceplateSerdesId = 0;
    AtSerdesManager serdesManager = SerdesManager();
    AtModuleEth ethModule = TestedModule();
    AtDevice device = AtModuleDeviceGet((AtModule)ethModule);
    uint32 numFaceplateSerdes = Tha60290021DeviceNumFaceplateSerdes((Tha60290021Device)device);

    for (faceplateSerdesId = 0; faceplateSerdesId < numFaceplateSerdes; faceplateSerdesId++)
        {
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(serdesManager, faceplateSerdesId);
        uint8 ethPortId = faceplateSerdesId + 1;
        AtEthPort ethPort = AtModuleEthPortGet(TestedModule(), ethPortId);

        if (!AtSerdesControllerIsControllable(serdes))
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerModeSet(serdes, cAtSerdesModeEth1G));
        TEST_ASSERT((AtChannel)ethPort == AtSerdesControllerPhysicalPortGet(serdes));
        TEST_ASSERT(AtEthPortSerdesController(ethPort) == serdes);
        }
    }

static void testInternalMacPortOnlySupport40G()
    {
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtEthPortSpeedIsSupported(Mac40GPort(), cAtEthPortSpeed40G));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtEthPortSpeedIsSupported(Mac40GPort(), cAtEthPortSpeed10G));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtEthPortSpeedIsSupported(Mac40GPort(), cAtEthPortSpeed1000M));
    }

static void testSpeedOfInternalMACPortMustBe40G()
    {
    TEST_ASSERT_EQUAL_INT(cAtEthPortSpeed40G, AtEthPortSpeedGet(Mac40GPort()));
    }

static void testCannotChangeSpeedOfTheInternalMAC()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtEthPortSpeedSet(Mac40GPort(), cAtEthPortSpeed10G)   != cAtOk);
    TEST_ASSERT(AtEthPortSpeedSet(Mac40GPort(), cAtEthPortSpeed1000M) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void TestEthPortFlowControlThresholdApplicableAccess(AtEthPort self, uint32 value)
    {
    TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(self, value) == cAtOk);
    TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdGet(self) == value);
    TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(self, value) == cAtOk);
    TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdGet(self) == value);
    }

static void TestEthPortFlowControlThresholdNotApplicableAccess(AtEthPort self, uint32 value)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(self, value) != cAtOk);
    TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdGet(self) == 0);
    TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(self, value) != cAtOk);
    TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdGet(self) == 0);
    AtTestLoggerEnable(cAtTrue);
    }

static void testAllOfPortsMustHaveDefaultFlowControlThreshold(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;
    uint32 highThresDef = FlowControlHighThresholdDefault();
    uint32 lowThresDef = FlowControlLowThresholdDefault();

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);

        if (!AtModuleEthPortCanBeUsed(module, port_i))
            continue;

        TEST_ASSERT_NOT_NULL(port);

        if (IsFacePlatePort(port_i))
            {
            TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdGet(port) == highThresDef);
            TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdGet(port) == lowThresDef);
            }
        else
            {
            TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdGet(port) == 0);
            TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdGet(port) == 0);
            }
        }
    }

static void testOnlyFaceplatePortCanSetFlowControlThreshold(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);

        TEST_ASSERT_NOT_NULL(port);

        if (IsFacePlatePort(port_i))
            {
            TestEthPortFlowControlThresholdApplicableAccess(port, 65);
            TestEthPortFlowControlThresholdApplicableAccess(port, 255);
            }
        else
            {
            TestEthPortFlowControlThresholdNotApplicableAccess(port, 1);
            TestEthPortFlowControlThresholdNotApplicableAccess(port, 255);
            }
        }
    }

static void testFlowControlThresholdOnNullPortMustBeZero(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(NULL, 5) != cAtOk);
    TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdGet(NULL) == 0);
    TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(NULL, 5) != cAtOk);
    TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdGet(NULL) == 0);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotApplyOutOfRangeFlowControlThresholdValue(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < FacePlatePortEnd(); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i + 1);

        TEST_ASSERT_NOT_NULL(port);

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(port, 256) == cAtErrorOutOfRangParm);
        TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdGet(port) != 256);
        TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(port, 256) == cAtErrorOutOfRangParm);
        TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdGet(port) != 256);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCannotSetFlowControlLowThresholdValueBeLagerThanHighThresholdValue(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < FacePlatePortEnd(); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i + 1);

        TEST_ASSERT_NOT_NULL(port);

        TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(port, port_i + 100) == cAtOk);
        TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(port, port_i + 90) != cAtOk);

        TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(port, port_i + 110) == cAtOk);
        TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(port, port_i + 120) != cAtOk);
        }
    }

static void testCannotSetZeroToFlowControlThresholdValue(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i < FacePlatePortEnd(); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i + 1);

        TEST_ASSERT_NOT_NULL(port);

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtCienaEthPortFlowControlHighThresholdSet(port, 0) != cAtOk);
        TEST_ASSERT(AtCienaEthPortFlowControlLowThresholdSet(port, 0) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void TestEthPortPacketSizeApplicableAccess(AtEthPort self, uint32 value)
    {
    TEST_ASSERT(AtEthPortMaxPacketSizeSet(self, value) == cAtOk);
    TEST_ASSERT(AtEthPortMaxPacketSizeGet(self) == value);
    TEST_ASSERT(AtEthPortMinPacketSizeSet(self, (value - 1)) == cAtOk);
    TEST_ASSERT(AtEthPortMinPacketSizeGet(self) == (value - 1));
    }

static void testOnly40GBackplaneMacsCanSetMinPacketSizeAndMaxPacketSize(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = 0; port_i <  AtModuleEthMaxPortsGet(module); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);

        TEST_ASSERT_NOT_NULL(port);

        if (Tha60290021ModuleEthIsBackplaneMac(port))
            {
            TestEthPortPacketSizeApplicableAccess(port, 100 + port_i);
            TestEthPortPacketSizeApplicableAccess(port, 120 + port_i);
            }
        }
    }

static void testCannotSetMinPacketSizeBeLagerMinPacketSize(void)
    {
    AtModuleEth module = TestedModule();
    uint8 port_i;

    for (port_i = Start40GBackplaneMacId(); port_i <  Stop40GBackplaneMacId(); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(module, port_i);

        TEST_ASSERT_NOT_NULL(port);

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtEthPortMaxPacketSizeSet(port, 100 + port_i) != cAtOk);
        TEST_ASSERT(AtEthPortMinPacketSizeSet(port, 101 + port_i) == cAtOk);

        TEST_ASSERT(AtEthPortMinPacketSizeSet(port, 90 + port_i) == cAtOk);
        TEST_ASSERT(AtEthPortMaxPacketSizeSet(port, 80 + port_i) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testPakeSizeOnNullPortMustBeZero(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtEthPortMaxPacketSizeSet(NULL, 100) != cAtOk);
    TEST_ASSERT(AtEthPortMaxPacketSizeGet(NULL) == 0);
    TEST_ASSERT(AtEthPortMinPacketSizeSet(NULL, 100) != cAtOk);
    TEST_ASSERT(AtEthPortMinPacketSizeGet(NULL) == 0);
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60290021ModuleEthTestRunner_Fixtures)
        {
        new_TestFixture("testOnlyFaceplatePortCanBeBypassed", testOnlyFaceplatePortCanBeBypassed),
        new_TestFixture("testCanConfigureTxSubPortVlan", testCanConfigureTxSubPortVlan),
        new_TestFixture("testCanConfigureTxSubPortVlan", testCannotConfigureTxSubPortInvalidVlanId),
        new_TestFixture("testCanConfigureTxSubPortVlan", testCannotConfigureTxSubPortInvalidVlanPri),
        new_TestFixture("testCanConfigureTxSubPortVlan", testCannotConfigureTxSubPortInvalidVlanCfi),
        new_TestFixture("testCannotConfigureNullTxSubPortVlan", testCannotConfigureNullTxSubPortVlan),
        new_TestFixture("testCannotConfigureTxSubPortVlanOnNullPort", testCannotConfigureTxSubPortVlanOnNullPort),
        new_TestFixture("testCanConfigureExpectedSubPortVlan", testCanConfigureExpectedSubPortVlan),
        new_TestFixture("testCanConfigureExpectedSubPortVlan", testCannotConfigureExpectedSubPortInvalidVlanId),
        new_TestFixture("testCanConfigureExpectedSubPortVlan", testCannotConfigureExpectedSubPortInvalidVlanPri),
        new_TestFixture("testCanConfigureExpectedSubPortVlan", testCannotConfigureExpectedSubPortInvalidVlanCfi),
        new_TestFixture("testCannotConfigureNullExpectedSubPortVlan", testCannotConfigureNullExpectedSubPortVlan),
        new_TestFixture("testCannotConfigureSubPortVlanOnNullPort", testCannotConfigureSubPortVlanOnNullPort),
        new_TestFixture("testCannotConfigureSameExpectedSubportVlanForMoreThanOnePorts", testCannotConfigureSameExpectedSubportVlanForMoreThanOnePorts),
        new_TestFixture("testNumberOfSERDESControllerReturnedByETHModuleMustEqualToThatOfSERDESManager", testNumberOfSERDESControllerReturnedByETHModuleMustEqualToThatOfSERDESManager),
        new_TestFixture("testAllOfSERDESInstancesReturnedByETHModuleMustBeTheSameAsThoseReturnedBySERDESManager", testAllOfSERDESInstancesReturnedByETHModuleMustBeTheSameAsThoseReturnedBySERDESManager),
        new_TestFixture("testEthernet40GSERDESMustBeAssociatedCorrectly", testEthernet40GSERDESMustBeAssociatedCorrectly),
        new_TestFixture("testAllOfETHMACAtFaceplateSideShouldHaveWeakReferenceToCorrespondingFaceplateSERDES", testAllOfETHMACAtFaceplateSideShouldHaveWeakReferenceToCorrespondingFaceplateSERDES),
        new_TestFixture("testIfFaceplateSerdesWorkInEthMode_TheyMustWorkCloselyWithCorrespondingPort", testIfFaceplateSerdesWorkInEthMode_TheyMustWorkCloselyWithCorrespondingPort),
        new_TestFixture("testInternalMacPortOnlySupport40G", testInternalMacPortOnlySupport40G),
        new_TestFixture("testSpeedOfInternalMACPortMustBe40G", testSpeedOfInternalMACPortMustBe40G),
        new_TestFixture("testCannotChangeSpeedOfTheInternalMAC", testCannotChangeSpeedOfTheInternalMAC),
        new_TestFixture("testAllOfPortsMustHaveDefaultFlowControlThreshold", testAllOfPortsMustHaveDefaultFlowControlThreshold),
        new_TestFixture("testOnlyFaceplatePortCanSetFlowControlThreshold", testOnlyFaceplatePortCanSetFlowControlThreshold),
        new_TestFixture("testFlowControlThresholdOnNullPortMustBeZero", testFlowControlThresholdOnNullPortMustBeZero),
        new_TestFixture("testCannotApplyOutOfRangeFlowControlThresholdValue", testCannotApplyOutOfRangeFlowControlThresholdValue),
        new_TestFixture("testCannotSetFlowControlLowThresholdValueBeLagerThanHighThresholdValue", testCannotSetFlowControlLowThresholdValueBeLagerThanHighThresholdValue),
        new_TestFixture("testCannotSetZeroToFlowControlThresholdValue", testCannotSetZeroToFlowControlThresholdValue),
        new_TestFixture("testOnly40GBackplaneMacsCanSetMinPacketSizeAndMaxPacketSize", testOnly40GBackplaneMacsCanSetMinPacketSizeAndMaxPacketSize),
        new_TestFixture("testCannotSetMinPacketSizeBeLagerMinPacketSize", testCannotSetMinPacketSizeBeLagerMinPacketSize),
        new_TestFixture("testPakeSizeOnNullPortMustBeZero", testPakeSizeOnNullPortMustBeZero)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af60290021ModuleEthTestRunner_Caller, "TestSuite_Af60290021ModuleEthTestRunner", NULL, NULL, TestSuite_Af60290021ModuleEthTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60290021ModuleEthTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtModuleEthTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, FlowControlIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, EthPortHasMac);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, RxIpgIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, TxIpgIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, EthPortHasMacChecking);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, NumEthPorts);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void MethodsInit(AtModuleTestRunner self)
    {
    Af60290021ModuleEthTestRunner runner = (Af60290021ModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BackplaneSerdesId);
        }

    mMethodsSet(runner, &m_methods);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    OverrideAtModuleEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModuleEthTestRunner);
    }

AtModuleTestRunner Af60290021ModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290021ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60290021ModuleEthTestRunnerObjectInit(newRunner, module);
    }

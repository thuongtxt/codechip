/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Af60290021ModuleEthTestRunnerInternal.h
 * 
 * Created Date: Sep 17, 2018
 *
 * Description : 60290021 module ETH test runner internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021MODULEETHTESTRUNNERINTERNAL_H_
#define _AF60290021MODULEETHTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../eth/AtModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290021ModuleEthTestRunner *Af60290021ModuleEthTestRunner;

typedef struct tAf60290021ModuleEthTestRunnerMethods
    {
    uint32 (*BackplaneSerdesId)(Af60290021ModuleEthTestRunner self);
    }tAf60290021ModuleEthTestRunnerMethods;

typedef struct tAf60290021ModuleEthTestRunner
    {
    tAtModuleEthTestRunner super;
    const tAf60290021ModuleEthTestRunnerMethods *methods;
    }tAf60290021ModuleEthTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60290021ModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021MODULEETHTESTRUNNERINTERNAL_H_ */


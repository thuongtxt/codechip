/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Af6029SerdesManagerTestRunner.c
 *
 * Created Date: Jul 14, 2016
 *
 * Description : SERDES Manager test runner for Tha6029 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af6029SerdesManagerTestRunnerInternal.h"
#include "Af6029PhysicalTests.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSerdesManager(self) (AtSerdesManager)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)
#define mThis(self)          ((Af6029SerdesManagerTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAf6029SerdesManagerTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods          m_AtUnittestRunnerOverride;
static tAtSerdesManagerTestRunnerMethods m_AtSerdesManagerTestRunnerOverride;

/* Methods */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool Tha60290021SerdesManagerIsFaceplateSerdes(AtSerdesManager self, uint32 serdesId);
extern eBool Tha60290021SerdesManagerIsMateSerdes(AtSerdesManager self, uint32 serdesId);
extern eBool Tha60290021SerdesManagerSerdesIsBackplane(AtSerdesManager self, uint32 serdesId);
extern eBool Tha60290021SerdesManagerSerdesIsSgmii(AtSerdesManager self, uint32 serdesId);

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesManager Manager(void)
    {
    return mSerdesManager(AtUnittestRunnerCurrentRunner());
    }

static eBool IsStm64Or10GSerdes(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    if ((serdesId == 0) || (serdesId == 8))
        return cAtTrue;
    return cAtFalse;
    }

static eBool FaceplateSerdesModeMustBeSupported(Af6029SerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode)
    {
    AtSerdesController serdes = (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)serdesRunner);
    switch (mode)
        {
        case cAtSerdesModeStm0   : return cAtTrue;
        case cAtSerdesModeStm1   : return cAtTrue;
        case cAtSerdesModeStm4   : return cAtTrue;
        case cAtSerdesModeStm16  : return cAtTrue;
        case cAtSerdesModeStm64  : return IsStm64Or10GSerdes(serdes);
        case cAtSerdesModeEth100M: return cAtTrue;
        case cAtSerdesModeEth1G  : return cAtTrue;
        case cAtSerdesModeEth10G : return IsStm64Or10GSerdes(serdes);
        default: return cAtFalse;
        }
    }

static eBool MateSerdesModeMustBeSupported(eAtSerdesMode mode)
    {
    return (mode == cAtSerdesModeStm16) ? cAtTrue : cAtFalse;
    }

static eBool BackplanSerdesModeMustBeSupported(eAtSerdesMode mode)
    {
    return (mode == cAtSerdesModeEth40G) ? cAtTrue : cAtFalse;
    }

static eBool SgmiiSerdesModeMustBeSupported(eAtSerdesMode mode)
    {
    return (mode == cAtSerdesModeEth1G) ? cAtTrue : cAtFalse;
    }

static eBool IsSerdesWithHandler(AtSerdesController serdes, eBool (*SerdesDeterminer)(AtSerdesManager, uint32))
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    return SerdesDeterminer(AtSerdesControllerManagerGet(serdes), serdesId);
    }

static eBool IsFaceplateSerdes(AtSerdesController serdes)
    {
    return IsSerdesWithHandler(serdes, Tha60290021SerdesManagerIsFaceplateSerdes);
    }

static eBool IsMateSerdes(AtSerdesController serdes)
    {
    return IsSerdesWithHandler(serdes, Tha60290021SerdesManagerIsMateSerdes);
    }

static eBool IsBackplaneSerdes(AtSerdesController serdes)
    {
    return IsSerdesWithHandler(serdes, Tha60290021SerdesManagerSerdesIsBackplane);
    }

static eBool IsSgmiiSerdes(AtSerdesController serdes)
    {
    return IsSerdesWithHandler(serdes, Tha60290021SerdesManagerSerdesIsSgmii);
    }

static eBool SerdesModeMustBeSupported(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode)
    {
    AtSerdesController serdes = (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)serdesRunner);

    if (IsFaceplateSerdes(serdes))
        return mMethodsGet(mThis(self))->FaceplateSerdesModeMustBeSupported(mThis(self), serdesRunner, mode);
    if (IsMateSerdes(serdes))
        return MateSerdesModeMustBeSupported(mode);
    if (IsBackplaneSerdes(serdes))
        return BackplanSerdesModeMustBeSupported(mode);
    if (IsSgmiiSerdes(serdes))
        return SgmiiSerdesModeMustBeSupported(mode);

    /* How can this happen? */
    AtAssert(0);

    return cAtFalse;
    }

static eBool SerdesPowerCanBeControlled(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner)
    {
    AtSerdesController serdes = (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)serdesRunner);

    if (IsFaceplateSerdes(serdes))
        return cAtTrue;
    if (IsMateSerdes(serdes))
        return cAtTrue;
    if (IsBackplaneSerdes(serdes))
        return cAtFalse;
    if (IsSgmiiSerdes(serdes))
        return cAtTrue;
    /* How can this happen? */
    AtAssert(0);

    return cAtFalse;
    }

static uint8 NumSerdesControllers(AtSerdesManagerTestRunner self)
    {
	/*
	 *  16 FacePlateLine
	 *  8  Mateline
	 *  2  BackPlan
	 *  4  SGMII
	 * */
    return 30;
    }

static void testAllControllableSerdesMustHavePrbsEngine(void)
    {
    AtSerdesManager manager = Manager();
    uint32 numSerdes = AtSerdesManagerNumSerdesControllers(manager);
    uint32 serdes_i;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(manager, serdes_i);

        if (AtSerdesControllerIsControllable(serdes))
            {
            TEST_ASSERT_NOT_NULL(AtSerdesControllerPrbsEngine(serdes));
            }
        }
    }

static void testAllSerdesMustHaveEyescanController(void)
    {
    AtSerdesManager manager = Manager();
    uint32 numSerdes = AtSerdesManagerNumSerdesControllers(manager);
    uint32 serdes_i;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(manager, serdes_i);
        if (AtSerdesControllerIsControllable(serdes))
            {
            TEST_ASSERT_NOT_NULL(AtSerdesControllerEyeScanControllerGet(serdes));
            }
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Af6029SerdesManagerTestRunner_Fixtures)
        {
        new_TestFixture("testAllControllableSerdesMustHavePrbsEngine", testAllControllableSerdesMustHavePrbsEngine),
        new_TestFixture("testAllSerdesMustHaveEyescanController", testAllSerdesMustHaveEyescanController)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af6029SerdesManagerTestRunner_Caller, "TestSuite_Af6029SerdesManagerTestRunner", NULL, NULL, TestSuite_Af6029SerdesManagerTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_Af6029SerdesManagerTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtSerdesManagerTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void OverrideAtSerdesManagerTestRunner(AtSerdesManagerTestRunner self)
    {
    AtSerdesManagerTestRunner runner = (AtSerdesManagerTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSerdesManagerTestRunnerOverride, (void *)runner->methods, sizeof(m_AtSerdesManagerTestRunnerOverride));

        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, SerdesModeMustBeSupported);
        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, SerdesPowerCanBeControlled);
        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, NumSerdesControllers);
        }

    mMethodsSet(runner, &m_AtSerdesManagerTestRunnerOverride);
    }

static void Override(AtSerdesManagerTestRunner self)
    {
    OverrideAtSerdesManagerTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static void MethodsInit(AtSerdesManagerTestRunner self)
    {
    Af6029SerdesManagerTestRunner runner = (Af6029SerdesManagerTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, FaceplateSerdesModeMustBeSupported);
        }

    mMethodsSet(runner, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf6029SerdesManagerTestRunner);
    }

AtSerdesManagerTestRunner Af6029SerdesManagerTestRunnerObjectInit(AtSerdesManagerTestRunner self, AtSerdesManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesManagerTestRunnerObjectInit(self, manager) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManagerTestRunner Af6029SerdesManagerTestRunnerNew(AtSerdesManager manager)
    {
    AtSerdesManagerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af6029SerdesManagerTestRunnerObjectInit(newRunner, manager);
    }

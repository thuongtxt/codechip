/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Af6029SerdesManagerTestRunningInternal.h
 * 
 * Created Date: Jul 14, 2016
 *
 * Description : SERDES Manager test runner for Tha6029 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF6029SERDESMANAGERTESTRUNNERINTERNAL_H_
#define _AF6029SERDESMANAGERTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../physical/AtSerdesManagerTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf6029SerdesManagerTestRunner * Af6029SerdesManagerTestRunner;

typedef struct tAf6029SerdesManagerTestRunnerMethods
    {
    eBool (*FaceplateSerdesModeMustBeSupported)(Af6029SerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode);
    }tAf6029SerdesManagerTestRunnerMethods;

typedef struct tAf6029SerdesManagerTestRunner
    {
    tAtSerdesManagerTestRunner super;
    const tAf6029SerdesManagerTestRunnerMethods *methods;
    }tAf6029SerdesManagerTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManagerTestRunner Af6029SerdesManagerTestRunnerObjectInit(AtSerdesManagerTestRunner self, AtSerdesManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _AF6029SERDESMANAGERTESTRUNNERINTERNAL_H_ */


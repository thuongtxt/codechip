/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Af6029PhysicalTests.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : Common public declarations for physical testing
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF6029PHYSICALTESTS_H_
#define _AF6029PHYSICALTESTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../physical/AtPhysicalTests.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManagerTestRunner Af6029SerdesManagerTestRunnerNew(AtSerdesManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _AF6029PHYSICALTESTS_H_ */


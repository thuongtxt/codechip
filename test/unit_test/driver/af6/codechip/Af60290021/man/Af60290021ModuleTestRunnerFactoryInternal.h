/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af60290021ModuleTestRunnerFactoryInternal.h
 * 
 * Created Date: Sep 12, 2018
 *
 * Description : Device runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021MODULETESTRUNNERFACTORYINTERNAL_H_
#define _AF60290021MODULETESTRUNNERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/man/Af60210051ModuleTestRunnerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290021ModuleTestRunnerFactory
    {
    tAf60210051ModuleTestRunnerFactory super;
    }tAf60290021ModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory Af60290021ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021MODULETESTRUNNERFACTORYINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290021ModuleTestRunnerFactory.c
 *
 * Created Date: May 04, 2015
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../xc/Af60290021ModuleXcTestRunner.h"
#include "../aps/Af60290021ModuleApsTestRunner.h"
#include "Af60290021ModuleTestRunnerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleTestRunnerFactoryMethods m_AtModuleTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtModuleTestRunnerFactoryMethods *m_AtModuleTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner SdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290021ModuleSdhTestRunnerNew(AtModule module);
    return Af60290021ModuleSdhTestRunnerNew(module);
    }

static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290021ModulePdhTestRunnerNew(AtModule module);
    return Af60290021ModulePdhTestRunnerNew(module);
    }

static AtModuleTestRunner PwTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290021ModulePwTestRunnerNew(AtModule module);
    return Af60290021ModulePwTestRunnerNew(module);
    }

static AtModuleTestRunner XcTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return (AtModuleTestRunner)Af60290021ModuleXcTestRunnerNew(module);
    }

static AtModuleTestRunner ApsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return (AtModuleTestRunner)Af60290021ModuleApsTestRunnerNew(module);
    }

static AtModuleTestRunner EthTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290021ModuleEthTestRunnerNew(AtModule module);
    return Af60290021ModuleEthTestRunnerNew(module);
    }

static AtModuleTestRunner PrbsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290021ModulePrbsTestRunnerNew(AtModule module);
    return Af60290021ModulePrbsTestRunnerNew(module);
    }

static AtModuleTestRunner PtpTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290021ModulePtpTestRunnerNew(AtModule module);
    return Af60290021ModulePtpTestRunnerNew(module);
    }

static void OverrideAtModuleTestRunnerFactory(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtModuleTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtModuleTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtModuleTestRunnerFactoryOverride));

        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, SdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PwTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, XcTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, ApsTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, EthTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PrbsTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PtpTestRunnerCreate);
        }

    self->methods = &m_AtModuleTestRunnerFactoryOverride;
    }

static void Override(AtModuleTestRunnerFactory self)
    {
    OverrideAtModuleTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021ModuleTestRunnerFactory);
    }

AtModuleTestRunnerFactory Af60290021ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210051ModuleTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory Af60290021ModuleTestRunnerFactorySharedFactory()
    {
    static tAf60290021ModuleTestRunnerFactory sharedFactory;
    return Af60290021ModuleTestRunnerFactoryObjectInit((AtModuleTestRunnerFactory)&sharedFactory);
    }

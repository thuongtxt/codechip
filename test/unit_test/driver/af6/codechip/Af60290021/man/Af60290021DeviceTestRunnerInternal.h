/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Af60290021DeviceTestRunnerInternal.h
 * 
 * Created Date: Sep 11, 2018
 *
 * Description : Device test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021DEVICETESTRUNNERINTERNAL_H_
#define _AF60290021DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/man/Af60210051DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290021DeviceTestRunner
    {
    tAf60210051DeviceTestRunner super;

    eBool fullCapacityOpened;
    }tAf60290021DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60290021DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021DEVICETESTRUNNERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Af60290021DeviceTestRunner.h
 * 
 * Created Date: Apr 28, 2017
 *
 * Description : Device test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290021DEVICETESTRUNNER_H_
#define _AF60290021DEVICETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../man/AtTestRunnerClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Af60290021DeviceTestRunnerFullCapacityOpen(AtDeviceTestRunner self, eBool enabled);
eBool Af60290021DeviceTestRunnerFullCapacityOpened(AtDeviceTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290021DEVICETESTRUNNER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290021DeviceTestRunner.c
 *
 * Created Date: May 04, 2015
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../physical/Af6029PhysicalTests.h"
#include "Af60290021DeviceTestRunner.h"
#include "Af60290021DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAf60290021DeviceTestRunner *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods   m_AtDeviceTestRunnerOverride;
static tAtUnittestRunnerMethods     m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;
static const tAtUnittestRunnerMethods   *m_AtUnittestRunnerMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60290021ModuleTestRunnerFactorySharedFactory();

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60290021ModuleTestRunnerFactorySharedFactory();
    }

static eBool ModuleMustBeSupported(AtDeviceTestRunner self, eAtModule module)
    {
    switch (module)
        {
        case cAtModuleAps: return cAtTrue;

        /* Cannot determine at this time, need to ask super */
        default:
            return m_AtDeviceTestRunnerMethods->ModuleMustBeSupported(self, module);
        }
    }

static AtUnittestRunner SerdesManagerTestRunnerCreate(AtDeviceTestRunner self, AtSerdesManager manager)
    {
    return (AtUnittestRunner)Af6029SerdesManagerTestRunnerNew(manager);
    }

static void testSerdesManageMustExist()
    {
    TEST_ASSERT(AtDeviceSerdesManagerGet(AtDeviceTestRunnerDeviceGet((AtDeviceTestRunner)AtUnittestRunnerCurrentRunner())));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60290021DeviceTestRunner_Fixtures)
        {
        new_TestFixture("testSerdesManageMustExist", testSerdesManageMustExist)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af60290021DeviceTestRunner_Caller, "TestSuite_Af60290021DeviceTestRunner", NULL, NULL, TestSuite_Af60290021DeviceTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60290021DeviceTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static eBool ModuleNotReady(AtDeviceTestRunner self, eAtModule moduleId)
    {
    return cAtFalse;
    }

static eBool ShouldTestAsyncInit(AtDeviceTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleMustBeSupported);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleNotReady);
        mMethodOverride(m_AtDeviceTestRunnerOverride, SerdesManagerTestRunnerCreate);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ShouldTestAsyncInit);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void OverrideAtUnittestRunner(AtDeviceTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290021DeviceTestRunner);
    }

AtDeviceTestRunner Af60290021DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210051DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60290021DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60290021DeviceTestRunnerObjectInit(newRunner, device);
    }

void Af60290021DeviceTestRunnerFullCapacityOpen(AtDeviceTestRunner self, eBool enabled)
    {
    if (self)
        mThis(self)->fullCapacityOpened = enabled;
    }

eBool Af60290021DeviceTestRunnerFullCapacityOpened(AtDeviceTestRunner self)
    {
    return self ? mThis(self)->fullCapacityOpened : cAtFalse;
    }

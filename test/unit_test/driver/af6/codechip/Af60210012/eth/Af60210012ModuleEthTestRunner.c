/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth
 *
 * File        : Af60210012ModuleEthTestRunner.c
 *
 * Created Date: Mar 6, 2017
 *
 * Description : ETH module test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60150011/eth/Af60150011ModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210012ModuleEthTestRunner
    {
    tAf60150011ModuleEthTestRunner super;
    }tAf60210012ModuleEthTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthTestRunnerMethods m_AtModuleEthTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    if (portId == 0)
        return cAtTrue;

    return cAtFalse;
    }

static eBool TxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    return IpgIsSupported(self, port);
    }

static eBool RxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    return IpgIsSupported(self, port);
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, TxIpgIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, RxIpgIsSupported);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210012ModuleEthTestRunner);
    }

AtModuleTestRunner Af60210012ModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60150011ModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210012ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210012ModuleEthTestRunnerObjectInit(newRunner, module);
    }

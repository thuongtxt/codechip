/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Af60210012ModuleEncapTestRunner.h
 * 
 * Created Date: Apr 13, 2017
 *
 * Description : ENCAP unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210012MODULEENCAPTESTRUNNER_H_
#define _AF60210012MODULEENCAPTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../encap/AtModuleEncapTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210012ModuleEncapTestRunner
    {
    tAtStmModuleEncapTestRunner super;
    }tAf60210012ModuleEncapTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210012ModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210012MODULEENCAPTESTRUNNER_H_ */


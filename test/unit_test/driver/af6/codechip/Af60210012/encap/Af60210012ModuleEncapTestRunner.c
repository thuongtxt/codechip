/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Af60210012ModuleEncapTestRunner.c
 *
 * Created Date: Feb 22, 2017
 *
 * Description : ENCAP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "../../../../encap/AtEncapChannelTestRunner.h"
#include "../../../../sdh/AtModuleSdhTests.h"
#include "AtFrLink.h"
#include "AtHdlcChannel.h"
#include "AtMfrBundle.h"
#include "AtFrVirtualCircuit.h"
#include "AtHdlcBundle.h"
#include "Af60210012ModuleEncapTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapTestRunnerMethods m_AtModuleEncapTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/
extern AtChannelTestRunner Af60210012FrLinkTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
extern AtChannelTestRunner Af60210012MfrBundleTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh ModuleSdh(AtDevice device)
    {
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static AtChannel NxDs0Create(AtPdhDe1 de1, uint32 timeslotBitmap)
    {
    AtChannel nxDs0 = (AtChannel)AtPdhDe1NxDs0Get(de1, timeslotBitmap);
    if (nxDs0 == NULL)
        nxDs0 = (AtChannel)AtPdhDe1NxDs0Create(de1, timeslotBitmap);

    return nxDs0;
    }

static AtChannel *PhysicalChannelsToTest(AtModuleEncapTestRunner self, uint16 *numChannels)
    {
    static AtChannel channels[17];
    AtSdhLine line_1, line_2;
    AtSdhChannel aug4s[4], aug1, tug2, vc12, vc4, tug3, vc11, vc3, aug16;
    uint8 lineId;
    AtPdhDe1 de1;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    /* Create aug4 of line 1*/
    lineId = 0;
    line_1 = AtModuleSdhLineGet(ModuleSdh(device), lineId);
    AtSdhTestCreateAllAug4s(line_1, aug4s);

    /* Setup mapping for AUG-1#0 to map AUG-4 <--> VC4-4C */
    /* Use 1 c4-4c */
    AtSdhChannelMapTypeSet(aug4s[0], cAtSdhAugMapTypeAug4MapVc4_4c);
    channels[0] = (AtChannel)AtSdhLineVc4_4cGet(line_1, 0);

    /* Setup mapping for AUG-1#1 to map AUG-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    /* Use 1 c4 */
    AtSdhChannelMapTypeSet(aug4s[1], cAtSdhAugMapTypeAug4Map4xAug1s);
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 4);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    channels[1] = (AtChannel)AtSdhLineVc4Get(line_1, 4);

    /* Use 1 c3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 5);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line_1, 5);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line_1, 5, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3);
    channels[2] = (AtChannel)AtSdhLineVc3Get(line_1, 5, 0);

    /* Use 1 c12 */
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line_1, 5, 1);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 5, 1, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    channels[3] = (AtChannel)AtSdhLineVc1xGet(line_1, 5, 1, 0, 0);

    /* Use in e1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 5, 1, 0, 1);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);
    channels[4] = (AtChannel)AtSdhChannelMapChannelGet(vc12);

    /* Use 1 NxDS0 in e1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1MFCrc);
    channels[5] = NxDs0Create(de1, cBit2 | cBit17 | cBit30);

    /* Setup mapping for AUG-1#2 to map AUG-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-11 */
    AtSdhChannelMapTypeSet(aug4s[2], cAtSdhAugMapTypeAug4Map4xAug1s);
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 8);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line_1, 8);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line_1, 8, 0);
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 8, 0, 0);

    /* Use 1 vc11 */
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    channels[6] = (AtChannel)AtSdhLineVc1xGet(line_1, 8, 0, 0, 0);

    /* Use 1 ds1 */
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 8, 0, 0, 1);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapDe1);
    channels[7] = (AtChannel)AtSdhChannelMapChannelGet(vc11);

    /* Use 1 NxDS0 in ds1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmSf);
    channels[8] = NxDs0Create(de1, cBit3 | cBit18);

    /* Setup mapping for AUG-1#3 to map AU-4 <--> VC-3 <--> TUG-2 <--> VC-1X */
    AtSdhChannelMapTypeSet(aug4s[3], cAtSdhAugMapTypeAug4Map4xAug1s);

    /* Use 1 c3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_1, 12);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    channels[9] = (AtChannel)AtSdhLineVc3Get(line_1, 12, 0);

    /* Use 1 c11 */
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line_1, 12, 1);
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 12, 1, 0);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    channels[10] = (AtChannel)AtSdhLineVc1xGet(line_1, 12, 1, 0, 0);

    /* Use 1 ds1 */
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 12, 1, 0, 1);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapDe1);
    channels[11] = (AtChannel)AtSdhChannelMapChannelGet(vc11);

    /* Use 1 NxDS0 in ds1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmSf);
    channels[12] = NxDs0Create(de1, cBit4 | cBit18);

    /* Use 1 c12 */
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_1, 12, 1, 1);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    channels[13] = (AtChannel)AtSdhLineVc1xGet(line_1, 12, 1, 1, 0);

    /* Use in e1 */
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line_1, 12, 1, 1, 1);
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapDe1);
    channels[14] = (AtChannel)AtSdhChannelMapChannelGet(vc12);

    /* Use 1 NxDS0 in e1 */
    de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc12);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1MFCrc);
    channels[15] = NxDs0Create(de1, cBit2 | cBit17 | cBit30);

    /* Use 1 c4-16c of line 2*/
    lineId = 1;
    line_2 = AtModuleSdhLineGet(ModuleSdh(device), lineId);
    aug16 = (AtSdhChannel)AtSdhLineAug16Get(line_2, 0);
    AtSdhChannelMapTypeSet(aug16, cAtSdhAugMapTypeAug16MapVc4_16c);
    channels[16] = (AtChannel)AtSdhLineVc4_16cGet(line_2, 0);

    *numChannels = 17;
    return channels;
    }

static AtChannelTestRunner CreateFrLinkTestRunner(AtModuleEncapTestRunner self, AtChannel channel)
    {
    return Af60210012FrLinkTestRunnerNew(channel, (AtModuleTestRunner)self);
    }

static AtChannelTestRunner CreateMfrBundleTestRunner(AtModuleEncapTestRunner self, AtChannel channel)
    {
    return Af60210012MfrBundleTestRunnerNew(channel, (AtModuleTestRunner)self);
    }
    
static void OverrideAtModuleEncapTestRunner(AtModuleTestRunner self)
    {
    AtModuleEncapTestRunner runner = (AtModuleEncapTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEncapTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEncapTestRunnerOverride));

        mMethodOverride(m_AtModuleEncapTestRunnerOverride, PhysicalChannelsToTest);
		mMethodOverride(m_AtModuleEncapTestRunnerOverride, CreateFrLinkTestRunner);
        mMethodOverride(m_AtModuleEncapTestRunnerOverride, CreateMfrBundleTestRunner);
        }

    mMethodsSet(runner, &m_AtModuleEncapTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEncapTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210012ModuleEncapTestRunner);
    }

AtModuleTestRunner Af60210012ModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtStmModuleEncapTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210012ModuleEncapTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;
        
    return Af60210012ModuleEncapTestRunnerObjectInit(newRunner, module);
    }

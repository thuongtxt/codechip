/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60210012PwConstrainerTestRunnerInternal.h
 * 
 * Created Date: Oct 13, 2016
 *
 * Description : PW Constrainer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210012PWCONSTRAINERTESTRUNNERINTERNAL_H_
#define _AF60210012PWCONSTRAINERTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../pw/AtPwConstrainerTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210012PwConstrainerTestRunner
    {
    tAtPwConstrainerTestRunner super;
    }tAf60210012PwConstrainerTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner Af60210012PwConstrainerTestRunnerObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210012PWCONSTRAINERTESTRUNNERINTERNAL_H_ */


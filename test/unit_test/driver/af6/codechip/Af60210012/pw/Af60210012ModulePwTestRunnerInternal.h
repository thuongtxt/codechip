/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60210012ModulePwTestRunnerInternal.h
 * 
 * Created Date: Oct 3, 2016
 *
 * Description : PW unittest
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210012MODULEPWTESTRUNNERINTERNAL_H_
#define _AF60210012MODULEPWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/pw/Af60210051ModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210012ModulePwTestRunner
    {
    tAf60210051ModulePwTestRunner super;
    }tAf60210012ModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210012ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210012MODULEPWTESTRUNNERINTERNAL_H_ */


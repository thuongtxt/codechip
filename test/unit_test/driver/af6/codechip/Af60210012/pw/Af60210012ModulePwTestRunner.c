/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210012ModulePwTestRunner.c
 *
 * Created Date: May 21, 2015
 *
 * Description : PW unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210012ModulePwTestRunnerInternal.h"
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 4096;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 2048;
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    return 3072;
    }

static AtUnittestRunner ConstrainerTestRunnerCreate(AtModulePwTestRunner self)
    {
    AtPwConstrainer constrainer = AtPwConstrainer5GMsCardNew();
    if (constrainer)
        return Af60210012PwConstrainerTestRunnerNew(self, constrainer);

    return NULL;
    }

static eBool ShouldTestHdlcPws(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumPws);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ConstrainerTestRunnerCreate);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestHdlcPws);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210012ModulePwTestRunner);
    }

AtModuleTestRunner Af60210012ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210051ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210012ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210012ModulePwTestRunnerObjectInit(newRunner, module);
    }

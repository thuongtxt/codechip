/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Af60210012PwConstrainerTestRunner.c
 *
 * Created Date: May 16, 2016
 *
 * Description : PW constrainer test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210012PwConstrainerTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwConstrainerTestRunnerMethods m_AtPwConstrainerTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumPwToTest(AtPwConstrainerTestRunner self)
    {
    AtUnused(self);
    return 3072;
    }

static void OverrideAtPwConstrainerTestRunner(AtUnittestRunner self)
    {
    AtPwConstrainerTestRunner runner = (AtPwConstrainerTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtPwConstrainerTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtPwConstrainerTestRunnerOverride));
        mMethodOverride(m_AtPwConstrainerTestRunnerOverride, NumPwToTest);
        }

    mMethodsSet(runner, &m_AtPwConstrainerTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtPwConstrainerTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210012PwConstrainerTestRunner);
    }

AtUnittestRunner Af60210012PwConstrainerTestRunnerObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwConstrainerTestRunnerObjectInit(self, modulePwTestRunner, constrainer) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner Af60210012PwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return Af60210012PwConstrainerTestRunnerObjectInit(newRunner, modulePwTestRunner, constrainer);
    }

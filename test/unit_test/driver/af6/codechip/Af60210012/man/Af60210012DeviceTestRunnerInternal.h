/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Af60210012DeviceTestRunnerInternal.h
 * 
 * Created Date: Sep 30, 2016
 *
 * Description : Device of 60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210012DEVICETESTRUNNERINTERNAL_H_
#define _AF60210012DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/man/Af60210051DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210012DeviceTestRunner
    {
    tAf60210051DeviceTestRunner super;
    }tAf60210012DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60210012DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210012DEVICETESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210051DeviceTestRunner.c
 *
 * Created Date: May 04, 2015
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210012DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60210012ModuleTestRunnerFactorySharedFactory();

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60210012ModuleTestRunnerFactorySharedFactory();
    }

static eBool ModuleNotReady(AtDeviceTestRunner self, eAtModule moduleId)
    {
    if (moduleId == cAtModulePpp)
        return cAtTrue;

    return m_AtDeviceTestRunnerMethods->ModuleNotReady(self, moduleId);
    }

static eBool ShouldTestModule(AtDeviceTestRunner self, eAtModule moduleId)
    {
    return m_AtDeviceTestRunnerMethods->ShouldTestModule(self, moduleId);
    }

static eBool ModuleMustBeSupported(AtDeviceTestRunner self, eAtModule module)
    {
    switch (module)
        {
        case cAtModulePpp        : return cAtTrue;
        case cAtModuleFr         : return cAtFalse;
        case cAtModuleEncap      : return cAtTrue;
        case cAtModuleIma        : return cAtFalse;
        case cAtModuleAtm        : return cAtFalse;
        case cAtModuleAps        : return cAtFalse;

        /* Cannot determine at this time, need to ask super */
        default:
            return m_AtDeviceTestRunnerMethods->ModuleMustBeSupported(self, module);
        }
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, mMethodsGet(self), sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ShouldTestModule);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleMustBeSupported);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleNotReady);
        }

    mMethodsSet(self, &m_AtDeviceTestRunnerOverride);
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210051DeviceTestRunner);
    }

AtDeviceTestRunner Af60210012DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210051DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60210012DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210012DeviceTestRunnerObjectInit(newRunner, device);
    }

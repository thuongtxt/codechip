/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : Af60210012ModuleTestRunnerFactoryInternal.h
 * 
 * Created Date: Sep 30, 2016
 *
 * Description : Default device runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210012MODULETESTRUNNERFACTORYINTERNAL_H_
#define _AF60210012MODULETESTRUNNERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210051/man/Af60210051ModuleTestRunnerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210012ModuleTestRunnerFactory
    {
    tAf60210051ModuleTestRunnerFactory super;
    }tAf60210012ModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory Af60210012ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210012MODULETESTRUNNERFACTORYINTERNAL_H_ */


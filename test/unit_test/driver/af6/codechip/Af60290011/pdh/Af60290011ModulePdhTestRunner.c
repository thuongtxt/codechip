/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60210031ModulePdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pdh/AtModulePdhTestRunnerInternal.h"
#include "AtCiena.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290011ModulePdhTestRunner
    {
    tAtModulePdhTestRunner super;
    }tAf60290011ModulePdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;
static tAtUnittestRunnerMethods      m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods      *m_AtUnittestRunnerMethods      = NULL;
static const tAtModulePdhTestRunnerMethods *m_AtModulePdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldTestMaximumNumberOfDe1Get(AtModulePdhTestRunner self)
    {
    return cAtFalse;
    }

static uint32 NumDe3SerialLines(AtModulePdhTestRunner self)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    return AtModulePdhNumberOfDe3SerialLinesGet(pdhModule);
    }

static void WillTestSerialLine(AtModulePdhTestRunner self)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtAssert(AtCienaModulePdhInterfaceTypeSet(pdhModule, cAtCienaPdhInterfaceTypeDe3) == cAtOk);
    }

static eBool CanTestDe1Prm(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static void PdhInterfaceSwitch(AtUnittestRunner self)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    eAtCienaPdhInterfaceType type = AtCienaModulePdhInterfaceTypeGet(pdhModule);
    eAtRet ret;

    if (type != cAtCienaPdhInterfaceTypeDe1)
        {
        ret = AtCienaModulePdhInterfaceTypeSet(pdhModule, cAtCienaPdhInterfaceTypeDe1);
        AtAssert(ret == cAtOk);
        }
    else
        {
        ret = AtCienaModulePdhInterfaceTypeSet(pdhModule, cAtCienaPdhInterfaceTypeDe3);
        AtAssert(ret == cAtOk);
        }

    }

static eBool ShouldTestDe3InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);

    if (AtCienaModulePdhInterfaceTypeGet(pdhModule) == cAtCienaPdhInterfaceTypeDe1)
        return cAtFalse;

    return m_AtModulePdhTestRunnerMethods->ShouldTestDe3InLine(self, lineId);
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    PdhInterfaceSwitch(self);
    m_AtUnittestRunnerMethods->Run(self);
    PdhInterfaceSwitch(self);
    m_AtUnittestRunnerMethods->Run(self);
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)m_AtUnittestRunnerMethods, sizeof(m_AtUnittestRunnerOverride));
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void OverrideAtModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtModulePdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestMaximumNumberOfDe1Get);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, NumDe3SerialLines);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, WillTestSerialLine);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, CanTestDe1Prm);
        mMethodOverride(m_AtModulePdhTestRunnerOverride, ShouldTestDe3InLine);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner((AtUnittestRunner)self);
    OverrideAtModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290011ModulePdhTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290011ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

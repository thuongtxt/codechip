/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Af60290011ModuleEthTestRunner.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : ETH module test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../../../eth/AtModuleEthTestRunnerInternal.h"
#include "../../../../../../../driver/src/implement/codechip/Tha60290011/eth/Tha60290011ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModule(self) (AtModuleEth)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290011ModuleEthTestRunner
    {
    tAtModuleEthTestRunner super;
    }tAf60290011ModuleEthTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods      m_AtUnittestRunnerOverride;
static tAtModuleEthTestRunnerMethods m_AtModuleEthTestRunnerOverride;

/* Methods */
static const tAtUnittestRunnerMethods      *m_AtUnittestRunnerMethods      = NULL;
static const tAtModuleEthTestRunnerMethods *m_AtModuleEthTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth TestedModule()
    {
    return (AtModuleEth)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eBool FlowControlIsSupported(AtModuleEthTestRunner self)
    {
    return cAtFalse;
    }

static AtSerdesManager SerdesManager()
    {
    AtModuleEth module = TestedModule();
    AtDevice device = AtModuleDeviceGet((AtModule)module);
    return AtDeviceSerdesManagerGet(device);
    }

static uint32 NumEthPorts(AtModuleEthTestRunner self)
    {
    return 6;
    }

static AtEthPort BackplanePort()
    {
    AtModuleEth ethModule = TestedModule();
    return AtModuleEthPortGet(ethModule, 0);
    }

static AtEthPort Axi4Port()
    {
    AtModuleEth ethModule = TestedModule();
    return AtModuleEthPortGet(ethModule, 1);
    }

static uint32 SgmiiPortId(void)
    {
    return 2;
    }

static AtEthPort SgmiiPort()
    {
    AtModuleEth ethModule = TestedModule();
    return AtModuleEthPortGet(ethModule, SgmiiPortId());
    }

static void testEthernetPortMustBeAssociatedCorrectly()
    {
    TEST_ASSERT(AtEthPortSerdesController(BackplanePort()) == AtSerdesManagerSerdesControllerGet(SerdesManager(), 0));
    TEST_ASSERT(AtEthPortSerdesController(Axi4Port()) == AtSerdesManagerSerdesControllerGet(SerdesManager(), 2));
    TEST_ASSERT(AtEthPortSerdesController(SgmiiPort()) == AtSerdesManagerSerdesControllerGet(SerdesManager(), 3));
    }

static void testBackplanePortOnlySupport10GAnd1G()
    {
    TEST_ASSERT(AtEthPortInterfaceSet(BackplanePort(), cAtEthPortInterfaceXGMii) == cAtOk);
    TEST_ASSERT(AtEthPortSpeedIsSupported(BackplanePort(), cAtEthPortSpeed10G));
    TEST_ASSERT(AtEthPortInterfaceSet(BackplanePort(), cAtEthPortInterfaceSgmii) == cAtOk);
    TEST_ASSERT(AtEthPortSpeedIsSupported(BackplanePort(), cAtEthPortSpeed1000M));
    }

static void testBackplanePortOnlySupportXGMiiAndSgmii()
    {
    TEST_ASSERT(AtEthPortInterfaceSet(BackplanePort(), cAtEthPortInterfaceXGMii) == cAtOk);
    TEST_ASSERT(AtEthPortInterfaceSet(BackplanePort(), cAtEthPortInterfaceSgmii) == cAtOk);
    }

static void testSpeedOfbackplanePortMustReflectInterface()
    {
    TEST_ASSERT(AtEthPortInterfaceSet(BackplanePort(), cAtEthPortInterfaceXGMii) == cAtOk);
    TEST_ASSERT_EQUAL_INT(AtEthPortSpeedGet(BackplanePort()), cAtEthPortSpeed10G);
    TEST_ASSERT(AtEthPortInterfaceSet(BackplanePort(), cAtEthPortInterfaceSgmii) == cAtOk);
    TEST_ASSERT_EQUAL_INT(AtEthPortSpeedGet(BackplanePort()), cAtEthPortSpeed1000M);
    }

static void testAxi4PortOnlySupport2dot5G()
    {
    TEST_ASSERT(AtEthPortSpeedIsSupported(Axi4Port(), cAtEthPortSpeed2500M));
    }

static void testSpeedOfAxi4PortMustBe2dot5G()
    {
    TEST_ASSERT_EQUAL_INT(cAtEthPortSpeed2500M, AtEthPortSpeedGet(Axi4Port()));
    }

static void testSgmiiPortOnlySupport1G()
    {
    TEST_ASSERT(AtEthPortSpeedIsSupported(SgmiiPort(), cAtEthPortSpeed1000M));
    }

static void testSpeedOfSgmiiPortMustBe1G()
    {
    TEST_ASSERT_EQUAL_INT(cAtEthPortSpeed1000M, AtEthPortSpeedGet(SgmiiPort()));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Af60290011ModuleEthTestRunner_Fixtures)
        {
        new_TestFixture("testEthernetPortMustBeAssociatedCorrectly", testEthernetPortMustBeAssociatedCorrectly),
        new_TestFixture("testBackplanePortOnlySupport10GAnd1G", testBackplanePortOnlySupport10GAnd1G),
        new_TestFixture("testSpeedOfbackplanePortMustReflectInterface", testSpeedOfbackplanePortMustReflectInterface),
        new_TestFixture("testAxi4PortOnlySupport2dot5G", testAxi4PortOnlySupport2dot5G),
        new_TestFixture("testSpeedOfAxi4PortMustBe2dot5G", testSpeedOfAxi4PortMustBe2dot5G),
        new_TestFixture("testSgmiiPortOnlySupport1G", testSgmiiPortOnlySupport1G),
        new_TestFixture("testSpeedOfSgmiiPortMustBe1G", testSpeedOfSgmiiPortMustBe1G),
        new_TestFixture("testBackplanePortOnlySupportXGMiiAndSgmii", testBackplanePortOnlySupportXGMiiAndSgmii),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Af60290011ModuleEthTestRunner_Caller, "TestSuite_Af60290011ModuleEthTestRunner", NULL, NULL, TestSuite_Af60290011ModuleEthTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_Af60290011ModuleEthTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static eBool EthPortHasMac(AtModuleEthTestRunner self, AtEthPort port)
    {
    /* Not now */
    if (Tha60290011ModuleEthIsDccSgmiiPort(port))
        return cAtFalse;

    if (Tha60290011ModuleEthIsSerdesBackplanePort(port))
        return cAtFalse;

    return m_AtModuleEthTestRunnerMethods->EthPortHasMac(self, port);
    }

static eBool TxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    AtUnused(self);
    return AtEthPortTxIpgIsConfigurable(port);
    }

static eBool RxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    AtUnused(self);
    return AtEthPortRxIpgIsConfigurable(port);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtModuleEthTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, FlowControlIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, EthPortHasMac);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, TxIpgIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, RxIpgIsSupported);
        mMethodOverride(m_AtModuleEthTestRunnerOverride, NumEthPorts);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    OverrideAtModuleEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290011ModuleEthTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290011ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

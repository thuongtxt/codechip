/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Af60291011DeviceTestRunnerInternal.h
 * 
 * Created Date: Aug 27, 2018
 *
 * Description : Af60290011 Device Test Runner internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290011DEVICETESTRUNNERINTERNAL_H_
#define _AF60290011DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../man/device_runner/AtDeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60290011DeviceTestRunner
    {
    tAtDeviceTestDefaultRunner super;
    }tAf60290011DeviceTestRunner;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60290011DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);
#ifdef __cplusplus
}
#endif
#endif /* _AF60290011DEVICETESTRUNNERINTERNAL_H_ */


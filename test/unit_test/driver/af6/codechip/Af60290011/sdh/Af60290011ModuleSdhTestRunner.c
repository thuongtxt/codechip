/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60210031ModuleSdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhSerialLine.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"
#include "../../Af60210031/sdh/Af60210031ModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290011ModuleSdhTestRunner
    {
    tAf60210031ModuleSdhTestRunner super;
    }tAf60290011ModuleSdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhTestRunnerMethods m_AtModuleSdhTestRunnerOverride;

/* Save super implementation */
static const tAtModuleSdhTestRunnerMethods *m_AtModuleSdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    return 24;
    }

static void OverrideModuleSdhTestRunner(AtModuleTestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner) self;

    if (!m_methodsInit)
        {
        m_AtModuleSdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModuleSdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModuleSdhTestRunnerOverride));

        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TotalLines);
        }

    mMethodsSet(runner, &m_AtModuleSdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideModuleSdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210031ModuleSdhTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210031ModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290011ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

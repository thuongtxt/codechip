/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210031ModulePwTestRunner.c
 *
 * Created Date: Jul 23, 2016
 *
 * Description : PW module unittest
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60210031/pw/Af60210031ModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290011ModulePwTestRunner
    {
    tAf60210031ModulePwTestRunner super;
    }tAf60290011ModulePwTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtList MakePwsForGroupTest(AtModulePwTestRunner self, AtList pws)
    {
    uint32 startPwId, stopPwId;
    char circuitString[64];
    char pwString[64];

    /* Just make sure that we are in determined situation */
    mCliSuccessAssert(AtCliExecute("pdh serline mode 1-24 ds3"));

    /* DS3 */
    AtSprintf(pwString, "1-10");
    AtSprintf(circuitString, "de3.1-10");
    mCliSuccessAssert(AtCliExecute("pdh de3 framing 1-12 ds3_unframed"));
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    /* DS1 */
    mCliSuccessAssert(AtCliExecute("pdh de3 framing 13-24 ds3_cbit_28ds1"));
    mCliSuccessAssert(AtCliExecute("pdh de1 framing 13.1.1-24.7.4 ds1_unframed"));
    startPwId = 21;
    stopPwId  = startPwId + (12 * 28) - 1;
    AtSprintf(pwString, "%d-%d", startPwId, stopPwId);
    AtSprintf(circuitString, "de1.13.1.1-24.7.4");
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    return pws;
    }

static eBool ShouldTestPwConstrain(AtModulePwTestRunner runner)
    {
    return cAtFalse;
    }

static eAtRet EthLoopback(AtModulePwTestRunner self, AtEthPort ethPort)
    {
    AtSerdesController serdes = AtEthPortSerdesController(ethPort);
    return AtSerdesControllerLoopbackSet(serdes, cAtLoopbackModeLocal);
    }

static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MakePwsForGroupTest);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestPwConstrain);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthLoopback);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
       }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290011ModulePwTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210031ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290011ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

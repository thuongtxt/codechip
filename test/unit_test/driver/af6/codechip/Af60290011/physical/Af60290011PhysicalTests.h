/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Af60290011PhysicalTests.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : Physical testing
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60290011PHYSICALTESTS_H_
#define _AF60290011PHYSICALTESTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../physical/AtPhysicalTests.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManagerTestRunner Af60290011SerdesManagerTestRunnerNew(AtSerdesManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _AF60290011PHYSICALTESTS_H_ */


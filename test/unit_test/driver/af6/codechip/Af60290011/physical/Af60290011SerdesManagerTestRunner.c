/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Af60290011SerdesManagerTestRunner.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : SERDES Manager test runner for Tha60290011 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../physical/AtSerdesManagerTestRunnerInternal.h"
#include "Af60290011PhysicalTests.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSerdesManager(self) (AtSerdesManager)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290011SerdesManagerTestRunner
    {
    tAtSerdesManagerTestRunner super;
    }tAf60290011SerdesManagerTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesManagerTestRunnerMethods    m_AtSerdesManagerTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Axi4SerdesModeMustBeSupported(eAtSerdesMode mode)
    {
    return (mode == cAtSerdesModeEth2500M) ? cAtTrue : cAtFalse;
    }

static eBool SgmiiSerdesModeMustBeSupported(eAtSerdesMode mode)
    {
    return (mode == cAtSerdesModeEth1G) ? cAtTrue : cAtFalse;
    }

static eBool BackplanSerdesModeMustBeSupported(eAtSerdesMode mode)
    {
    return (mode == cAtSerdesModeEth10G) ? cAtTrue : cAtFalse;
    }

static eBool IsAxid4Serdes(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    return (serdesId == 2) ? cAtTrue : cAtFalse;
    }

static eBool IsSgmiiSerdes(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    return (serdesId >= 3) ? cAtTrue : cAtFalse;
    }

static eBool IsBackplaneSerdes(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    return ((serdesId == 0) || (serdesId == 1)) ? cAtTrue : cAtFalse;
    }

static eBool SerdesModeMustBeSupported(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode)
    {
    AtSerdesController serdes = (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)serdesRunner);

    if (IsAxid4Serdes(serdes))
        return Axi4SerdesModeMustBeSupported(mode);
    if (IsSgmiiSerdes(serdes))
        return SgmiiSerdesModeMustBeSupported(mode);
    if (IsBackplaneSerdes(serdes))
        return BackplanSerdesModeMustBeSupported(mode);

    return cAtFalse;
    }

static uint8 NumSerdesControllers(AtSerdesManagerTestRunner self)
    {
    return 6;
    }

static eBool SerdesPowerCanBeControlled(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner)
    {
    AtSerdesController serdes = (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)serdesRunner);
    if (IsBackplaneSerdes(serdes))
        return cAtFalse;
    return cAtTrue;
    }

static void OverrideAtSerdesManagerTestRunner(AtSerdesManagerTestRunner self)
    {
    AtSerdesManagerTestRunner runner = (AtSerdesManagerTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSerdesManagerTestRunnerOverride, (void *)runner->methods, sizeof(m_AtSerdesManagerTestRunnerOverride));

        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, SerdesModeMustBeSupported);
        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, NumSerdesControllers);
        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, SerdesPowerCanBeControlled);
        }

    mMethodsSet(runner, &m_AtSerdesManagerTestRunnerOverride);
    }

static void Override(AtSerdesManagerTestRunner self)
    {
    OverrideAtSerdesManagerTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290011SerdesManagerTestRunner);
    }

static AtSerdesManagerTestRunner ObjectInit(AtSerdesManagerTestRunner self, AtSerdesManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesManagerTestRunnerObjectInit(self, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManagerTestRunner Af60290011SerdesManagerTestRunnerNew(AtSerdesManager manager)
    {
    AtSerdesManagerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, manager);
    }

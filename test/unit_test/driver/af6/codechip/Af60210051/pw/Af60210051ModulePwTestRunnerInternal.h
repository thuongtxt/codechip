/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60210051ModulePwTestRunnerInternal.h
 * 
 * Created Date: May 15, 2016
 *
 * Description : PW module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210051MODULEPWTESTRUNNERINTERNAL_H_
#define _AF60210051MODULEPWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210011/pw/Af60210011ModulePwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210051ModulePwTestRunner
    {
    tAf60210011ModulePwTestRunner super;
    }tAf60210051ModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60210051ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210051MODULEPWTESTRUNNERINTERNAL_H_ */


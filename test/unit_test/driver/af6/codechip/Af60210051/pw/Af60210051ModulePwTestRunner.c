/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210051ModulePwTestRunner.c
 *
 * Created Date: May 21, 2015
 *
 * Description : PW unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210051ModulePwTestRunnerInternal.h"
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 2 * 1024;
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    return 4 * 1344;
    }

static AtEthPort EthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    uint8 lineId = AtSdhChannelLineGet(channel);

    return AtModuleEthPortGet(ethModule, (lineId < 2) ? 0 : 1);
    }

static AtEthPort EthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcGet(channel);
    uint8 lineId;

    if (sdhChannel == NULL) /* may be ds1 in ds3 */
        sdhChannel = (AtSdhChannel)AtPdhChannelVcGet(AtPdhChannelParentChannelGet(AtPdhChannelParentChannelGet(channel)));

    lineId = AtSdhChannelLineGet(sdhChannel);
    return AtModuleEthPortGet(ethModule, (lineId < 2) ? 0 : 1);
    }

static AtUnittestRunner ConstrainerTestRunnerCreate(AtModulePwTestRunner self)
    {
    AtPwConstrainer constrainer = AtPwConstrainerOcnCardNew();
    if (constrainer)
        return AtPwConstrainerTestRunnerNew(self, constrainer);

    return NULL;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumPws);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForSdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForPdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ConstrainerTestRunnerCreate);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210051ModulePwTestRunner);
    }

AtModuleTestRunner Af60210051ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210011ModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210051ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210051ModulePwTestRunnerObjectInit(newRunner, module);
    }

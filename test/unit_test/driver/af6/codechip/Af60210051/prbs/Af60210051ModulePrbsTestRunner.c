/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Af60210051ModulePrbsTestRunner.c
 *
 * Created Date: Jan 20, 2016
 *
 * Description : PRBS Module Test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../prbs/AtPrbsEngineTestRunner.h"
#include "../../../../prbs/AtModulePrbsTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210051ModulePrbsTestRunner
    {
    tAtModulePrbsTestRunner super;
    }tAf60210051ModulePrbsTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsTestRunnerMethods m_AtModulePrbsTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtModulePrbsTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static void TestVcDe1(AtModulePrbsTestRunner self)
    {
    AtModuleSdh sdhModule = SdhModule(self);
    uint32 lineId = AtTestRandom(AtModuleSdhMaxLinesGet(sdhModule), 0);
    uint32 cliLineId = lineId + 1;
    char idString[64];

    /* VC-4 */
    AtSprintf(idString, "vc4.%d.1-%d.2", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.1-%d.2 vc4", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c4", idString));
    AtModulePrbsTestRunnerTestVcs(self, idString);

    /* And VC-3s */
    AtSprintf(idString, "vc3.%d.3.1-%d.4.3", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.3-%d.4 3xvc3s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map %s c3", idString));
    AtModulePrbsTestRunnerTestVcs(self, idString);

    /* VC-12 */
    AtSprintf(idString, "vc1x.%d.5.1.1.1-%d.5.3.7.3", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.5 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.5.1-%d.5.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.5.1.1-%d.5.3.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.5.1.1.1-%d.5.3.7.3 c1x", cliLineId, cliLineId));
    AtModulePrbsTestRunnerTestVcs(self, idString);

    /* VC-11 */
    AtSprintf(idString, "vc1x.%d.6.1.1.1-%d.6.3.7.4", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.6 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.6.1-%d.6.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.6.1.1-%d.6.3.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.6.1.1.1-%d.6.3.7.4 c1x", cliLineId, cliLineId));
    AtModulePrbsTestRunnerTestVcs(self, idString);

    /* DS1 */
    AtSprintf(idString, "de1.%d.7.1.1.1-%d.7.3.7.4", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.7 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.7.1-%d.7.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.7.1.1-%d.7.3.7 tu11", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.7.1.1.1-%d.7.3.7.4 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.7.1.1.1-%d.7.3.7.4 ds1_unframed", cliLineId, cliLineId));
    AtModulePrbsTestRunnerTestDe1s(self, idString);

    /* E1 */
    AtSprintf(idString, "de1.%d.8.1.1.1-%d.8.3.7.3", cliLineId, cliLineId);
    mCliSuccessAssert(AtCliExecuteFormat("sdh map aug1.%d.8 3xvc3s", cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc3.%d.8.1-%d.8.3 7xtug2s", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map tug2.%d.8.1.1-%d.8.3.7 tu12", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("sdh map vc1x.%d.8.1.1.1-%d.8.3.7.3 de1", cliLineId, cliLineId));
    mCliSuccessAssert(AtCliExecuteFormat("pdh de1 framing %d.8.1.1.1-%d.8.3.7.3 e1_unframed", cliLineId, cliLineId));

    AtModulePrbsTestRunnerTestDe1s(self, idString);
    }

static eBool CanTestDe3Engine(AtModulePrbsTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePrbsTestRunner runner = (AtModulePrbsTestRunner)self;
    
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePrbsTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePrbsTestRunnerOverride));

        mMethodOverride(m_AtModulePrbsTestRunnerOverride, CanTestDe3Engine);
        mMethodOverride(m_AtModulePrbsTestRunnerOverride, TestVcDe1);
        }

    mMethodsSet(runner, &m_AtModulePrbsTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210051ModulePrbsTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePrbsTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210051ModulePrbsTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

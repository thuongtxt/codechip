/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60210011ModuleSdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210051ModuleSdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhTestRunnerMethods m_AtModuleSdhTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ERDIInterruptMaskSupported(AtModuleSdhTestRunner self)
    {
    return cAtTrue;
    }

static uint32 NumTfi5s(AtModuleSdhTestRunner self)
    {
    return 4;
    }

static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    return 4;
    }

static void OverrideModuleSdhTestRunner(AtModuleTestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner) self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleSdhTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleSdhTestRunnerOverride));

        mMethodOverride(m_AtModuleSdhTestRunnerOverride, NumTfi5s);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, TotalLines);
        mMethodOverride(m_AtModuleSdhTestRunnerOverride, ERDIInterruptMaskSupported);
        }

    mMethodsSet(runner, &m_AtModuleSdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideModuleSdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210051ModuleSdhTestRunner);
    }

AtModuleTestRunner Af60210051ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210011ModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210051ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return Af60210051ModuleSdhTestRunnerObjectInit(newRunner, module);
    }

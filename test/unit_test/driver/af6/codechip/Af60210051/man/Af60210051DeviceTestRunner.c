/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60210051DeviceTestRunner.c
 *
 * Created Date: May 04, 2015
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60210051DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60210051ModuleTestRunnerFactorySharedFactory();
extern uint32 Tha60210051DeviceStartVersionApplyNewReset(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60210051ModuleTestRunnerFactorySharedFactory();
    }

static eBool ShouldTestAsyncInit(AtDeviceTestRunner self)
    {
    return cAtTrue;
    }

static AtDevice Device()
    {
    return AtDeviceTestRunnerDeviceGet((AtDeviceTestRunner)AtUnittestRunnerCurrentRunner());
    }

static uint32 VersionTestAsyncInit(AtDeviceTestRunner self)
    {
    uint32 versionWithBuilt = Tha60210051DeviceStartVersionApplyNewReset(Device());
    uint32 major = versionWithBuilt >> 20;
    uint32 minor = versionWithBuilt >> 16;

    return (major | minor) & cBit7_0;
    }

static uint32 BuiltNumberTestAsyncInit(AtDeviceTestRunner self)
    {
    uint32 versionWithBuilt = Tha60210051DeviceStartVersionApplyNewReset(Device());
    return (versionWithBuilt & cBit15_0);
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ShouldTestAsyncInit);
        mMethodOverride(m_AtDeviceTestRunnerOverride, VersionTestAsyncInit);
        mMethodOverride(m_AtDeviceTestRunnerOverride, BuiltNumberTestAsyncInit);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210051DeviceTestRunner);
    }

AtDeviceTestRunner Af60210051DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60210011DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60210051DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60210051DeviceTestRunnerObjectInit(newRunner, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : Af60210051DeviceTestRunner.h
 * 
 * Created Date: May 14, 2016
 *
 * Description : Device test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210051DEVICETESTRUNNERINTERNAL_H_
#define _AF60210051DEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210011/man/Af60210011DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210051DeviceTestRunner
    {
    tAf60210011DeviceTestRunner super;
    }tAf60210051DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60210051DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210051DEVICETESTRUNNERINTERNAL_H_ */


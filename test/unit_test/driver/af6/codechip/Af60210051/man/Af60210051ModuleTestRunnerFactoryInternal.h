/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : Af60210051ModuleTestRunnerFactoryInternal.h
 * 
 * Created Date: May 14, 2016
 *
 * Description : Module runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60210051MODULETESTRUNNERFACTORYINTERNAL_H_
#define _AF60210051MODULETESTRUNNERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Af60210011/man/Af60210011ModuleTestRunnerFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60210051ModuleTestRunnerFactory
    {
    tAf60210011ModuleTestRunnerFactory super;
    }tAf60210051ModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory Af60210051ModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _AF60210051MODULETESTRUNNERFACTORYINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60000031DeviceTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../man/device_runner/AtDeviceTestRunnerInternal.h"
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60000031DeviceTestRunner
    {
    tAtDeviceTestDefaultRunner super;
    }tAf60000031DeviceTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60000031ModuleTestRunnerFactorySharedFactory();

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60000031ModuleTestRunnerFactorySharedFactory();
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60000031DeviceTestRunner);
    }

AtDeviceTestRunner Af60000031DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60000031DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60000031DeviceTestRunnerObjectInit(newRunner, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60291022ModuleTestRunnerFactory.c
 *
 * Created Date: Sep 12, 2018
 *
 * Description : Device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60290021/man/Af60290021ModuleTestRunnerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290051ModuleTestRunnerFactory
    {
    tAf60290021ModuleTestRunnerFactory super;
    }tAf60290051ModuleTestRunnerFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleTestRunnerFactoryMethods m_AtModuleTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtModuleTestRunnerFactoryMethods *m_AtModuleTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner SdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner PppTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner EncapTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner PwTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner XcTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner ApsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner PrbsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290051ModulePrbsTestRunnerNew(AtModule module);
    return Af60290051ModulePrbsTestRunnerNew(module);
    }

static AtModuleTestRunner RamTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner SurTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtModuleTestRunner EthTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    extern AtModuleTestRunner Af60290051ModuleEthTestRunnerNew(AtModule module);
    return Af60290051ModuleEthTestRunnerNew(module);
    }

static void OverrideAtModuleTestRunnerFactory(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtModuleTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtModuleTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtModuleTestRunnerFactoryOverride));

        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, SdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PppTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PdhTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, EncapTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PwTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, XcTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, PrbsTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, RamTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, SurTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, ApsTestRunnerCreate);
        mMethodOverride(m_AtModuleTestRunnerFactoryOverride, EthTestRunnerCreate);
        }

    self->methods = &m_AtModuleTestRunnerFactoryOverride;
    }

static void Override(AtModuleTestRunnerFactory self)
    {
    OverrideAtModuleTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290051ModuleTestRunnerFactory);
    }

static AtModuleTestRunnerFactory ObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021ModuleTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory Af60290051ModuleTestRunnerFactorySharedFactory()
    {
    static tAf60290051ModuleTestRunnerFactory sharedFactory;
    return ObjectInit((AtModuleTestRunnerFactory)&sharedFactory);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290051DeviceTestRunner.c
 *
 * Created Date: Sep 11, 2018
 *
 * Description : Device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../physical/AtPhysicalTests.h"
#include "../../Af60290021/man/Af60290021DeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290051DeviceTestRunner
    {
    tAf60290021DeviceTestRunner super;
    }tAf60290051DeviceTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods   m_AtDeviceTestRunnerOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerMethods *m_AtDeviceTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleTestRunnerFactory Af60290051ModuleTestRunnerFactorySharedFactory();

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return Af60290051ModuleTestRunnerFactorySharedFactory();
    }

static eBool ModuleMustBeSupported(AtDeviceTestRunner self, eAtModule module)
    {
    if (module == cAtModuleEth ||
        module == cAtModuleRam ||
        module == cAtModulePktAnalyzer ||
        module == cAtModulePrbs ||
        module == cAtModuleClock ||
        module == cAtModulePtp)
        return cAtTrue;
    return cAtFalse;
    }

static AtUnittestRunner SerdesManagerTestRunnerCreate(AtDeviceTestRunner self, AtSerdesManager manager)
    {
    AtSerdesManagerTestRunner Af60290051SerdesManagerTestRunnerNew(AtSerdesManager manager);
    return (AtUnittestRunner)Af60290051SerdesManagerTestRunnerNew(manager);
    }

static eBool ModuleNotReady(AtDeviceTestRunner self, eAtModule moduleId)
    {
    /* TODO: Module PTP does not have unittest implementation. */
    if (moduleId == cAtModulePtp)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleRunnerFactory);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleNotReady);
        mMethodOverride(m_AtDeviceTestRunnerOverride, SerdesManagerTestRunnerCreate);
        mMethodOverride(m_AtDeviceTestRunnerOverride, ModuleMustBeSupported);
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290051DeviceTestRunner);
    }

static AtDeviceTestRunner ObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021DeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner Af60290051DeviceTestRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, device);
    }

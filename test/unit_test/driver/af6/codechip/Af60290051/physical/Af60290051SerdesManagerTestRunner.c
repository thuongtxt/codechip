/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Af60290051SerdesManagerTestRunner.c
 *
 * Created Date: Sep 14, 2018
 *
 * Description : SERDES manager test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60290021/physical/Af6029SerdesManagerTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290051SerdesManagerTestRunner
    {
    tAf6029SerdesManagerTestRunner super;
    }tAf60290051SerdesManagerTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAf6029SerdesManagerTestRunnerMethods    m_Af6029SerdesManagerTestRunnerOverride;
static tAtSerdesManagerTestRunnerMethods m_AtSerdesManagerTestRunnerOverride;

/* Methods */
static const tAf6029SerdesManagerTestRunnerMethods *m_Af6029SerdesManagerTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Is10GSerdes(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    if (serdesId == 0 || serdesId == 8 || serdesId == 16 || serdesId == 24)
        return cAtTrue;
    return cAtFalse;
    }

static eBool FaceplateSerdesModeMustBeSupported(Af6029SerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode)
    {
    AtSerdesController serdes = (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)serdesRunner);
    switch (mode)
        {
        case cAtSerdesModeEth100M: return cAtTrue;
        case cAtSerdesModeEth1G  : return cAtTrue;
        case cAtSerdesModeEth10G : return Is10GSerdes(serdes);
        default:
            return cAtFalse;
        }
    }

static uint8 NumSerdesControllers(AtSerdesManagerTestRunner self)
    {
    /*
     *  32 FacePlateLine
     *  2  BackPlan
     * */
    return 34;
    }

static void OverrideAf6029SerdesManagerTestRunner(AtSerdesManagerTestRunner self)
    {
    Af6029SerdesManagerTestRunner runner = (Af6029SerdesManagerTestRunner)self;

    if (!m_methodsInit)
        {
        m_Af6029SerdesManagerTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_Af6029SerdesManagerTestRunnerOverride, mMethodsGet(runner), sizeof(m_Af6029SerdesManagerTestRunnerOverride));

        mMethodOverride(m_Af6029SerdesManagerTestRunnerOverride, FaceplateSerdesModeMustBeSupported);
        }

    mMethodsSet(runner, &m_Af6029SerdesManagerTestRunnerOverride);
    }

static void OverrideAtSerdesManagerTestRunner(AtSerdesManagerTestRunner self)
    {
    AtSerdesManagerTestRunner runner = (AtSerdesManagerTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSerdesManagerTestRunnerOverride, (void *)runner->methods, sizeof(m_AtSerdesManagerTestRunnerOverride));

        mMethodOverride(m_AtSerdesManagerTestRunnerOverride, NumSerdesControllers);
        }

    mMethodsSet(runner, &m_AtSerdesManagerTestRunnerOverride);
    }

static void Override(AtSerdesManagerTestRunner self)
    {
    OverrideAtSerdesManagerTestRunner(self);
    OverrideAf6029SerdesManagerTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290051SerdesManagerTestRunner);
    }

static AtSerdesManagerTestRunner ObjectInit(AtSerdesManagerTestRunner self, AtSerdesManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af6029SerdesManagerTestRunnerObjectInit(self, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManagerTestRunner Af60290051SerdesManagerTestRunnerNew(AtSerdesManager manager)
    {
    AtSerdesManagerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, manager);
    }

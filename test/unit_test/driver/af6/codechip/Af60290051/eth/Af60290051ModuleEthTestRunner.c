/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af60290051ModuleEthTestRunner.c
 *
 * Created Date: Sep 17, 2018
 *
 * Description : 60290051 Module ETH test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Af60290021/eth/Af60290021ModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60290051ModuleEthTestRunner
    {
    tAf60290021ModuleEthTestRunner super;
    }tAf60290051ModuleEthTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthTestRunnerMethods            m_AtModuleEthTestRunnerOverride;
static tAf60290021ModuleEthTestRunnerMethods    m_Af602900021ModuleEthTestRunnerOverride;

/* Methods */
static const tAf60290021ModuleEthTestRunnerMethods      *m_Af602900021ModuleEthTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumEthPorts(AtModuleEthTestRunner self)
    {
    AtUnused(self);
    return 35;
    }

static uint32 BackplaneSerdesId(Af60290021ModuleEthTestRunner self)
    {
    AtUnused(self);
    return 32;
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, NumEthPorts);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void OverrideAf60290021ModuleEthEthTestRunner(AtModuleTestRunner self)
    {
    Af60290021ModuleEthTestRunner runner = (Af60290021ModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        m_Af602900021ModuleEthTestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_Af602900021ModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_Af602900021ModuleEthTestRunnerOverride));

        mMethodOverride(m_Af602900021ModuleEthTestRunnerOverride, BackplaneSerdesId);
        }

    mMethodsSet(runner, &m_Af602900021ModuleEthTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEthTestRunner(self);
    OverrideAf60290021ModuleEthEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60290051ModuleEthTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (Af60290021ModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60290051ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

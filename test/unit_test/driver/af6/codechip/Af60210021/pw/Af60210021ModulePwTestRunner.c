/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60210021ModulePwTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PW module unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pw/AtModulePwTestRunnerInternal.h"
#include "AtModulePdh.h"
#include "AtPdhSerialLine.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210021ModulePwTestRunner
    {
    tAtModulePwTestRunner super;
    }tAf60210021ModulePwTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 64;
    }

static AtEthPort EthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, 0);
    }

static eBool CanChangeLopsClearThreshold(AtModulePwTestRunner self)
    {
    return cAtFalse;
    }

static AtList MakePwsForGroupTest(AtModulePwTestRunner self, AtList pws)
    {
    char circuitString[16];
    char pwString[16];

    mCliSuccessAssert(AtCliExecute("pdh de1 framing 1-48 ds1_unframed"));
    AtSprintf(pwString, "1-48");
    AtSprintf(circuitString, "de1.1-48");
    AtModulePwTestSetupSAToPWithCircuits(pws, pwString, circuitString);

    return pws;
    }

static AtUnittestRunner ConstrainerTestRunnerCreate(AtModulePwTestRunner self)
    {
    AtPwConstrainer constrainer = AtPwConstrainerDs1CardNew();
    if (constrainer)
        return AtPwConstrainerTestRunnerNew(self, constrainer);

    return NULL;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumApsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MaxNumHsGroups);
        mMethodOverride(m_AtModulePwTestRunnerOverride, EthPortForPdhChannel);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CanChangeLopsClearThreshold);
        mMethodOverride(m_AtModulePwTestRunnerOverride, MakePwsForGroupTest);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ConstrainerTestRunnerCreate);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210021ModulePwTestRunner);
    }

static AtModuleTestRunner ObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210021ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Af60210021ModulePdhTestRunner.c
 *
 * Created Date: Jan 28, 2016
 *
 * Description : PDH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../pdh/AtModulePdhTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60210021ModulePdhTestRunner
    {
    tAtModulePdhTestRunner super;
    }tAf60210021ModulePdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhTestRunnerMethods m_AtModulePdhTestRunnerOverride;

/* Save super implementation */
static const tAtModulePdhTestRunnerMethods *m_AtModulePdhTestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanTestDe1Prm(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideModulePdhTestRunner(AtModuleTestRunner self)
    {
    AtModulePdhTestRunner runner = (AtModulePdhTestRunner) self;
    if (!m_methodsInit)
        {
        m_AtModulePdhTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtModulePdhTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModulePdhTestRunnerOverride));

        mMethodOverride(m_AtModulePdhTestRunnerOverride, CanTestDe1Prm);
        }

    mMethodsSet(runner, &m_AtModulePdhTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideModulePdhTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60210021ModulePdhTestRunner);
    }

AtModuleTestRunner Af60210021ModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60210021ModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());

    if (newRunner == NULL)
        return NULL;
    return Af60210021ModulePdhTestRunnerObjectInit(newRunner, module);
    }

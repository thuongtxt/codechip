/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Af60150011ModulePwTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PW module unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60150011ModulePwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwTestRunnerMethods m_AtModulePwTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanChangeLopsClearThreshold(AtModulePwTestRunner self)
    {
    return cAtFalse;
    }

static eBool CepCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    return cAtTrue;
    }

static eBool ShouldTestAu3Paths(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static void OverrideAtModulePwTestRunner(AtModuleTestRunner self)
    {
    AtModulePwTestRunner runner = (AtModulePwTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModulePwTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModulePwTestRunnerOverride));

        mMethodOverride(m_AtModulePwTestRunnerOverride, CanChangeLopsClearThreshold);
        mMethodOverride(m_AtModulePwTestRunnerOverride, CepCanTestOnLine);
        mMethodOverride(m_AtModulePwTestRunnerOverride, ShouldTestAu3Paths);
        }

    mMethodsSet(runner, &m_AtModulePwTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModulePwTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60150011ModulePwTestRunner);
    }

AtModuleTestRunner Af60150011ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePwTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60150011ModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60150011ModulePwTestRunnerObjectInit(newRunner, module);
    }

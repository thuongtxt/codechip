/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Af60150011ModulePwTestRunner.h
 * 
 * Created Date: May 21, 2015
 *
 * Description : PW unittest
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60150011MODULEPWTESTRUNNER_H_
#define _AF60150011MODULEPWTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../pw/AtModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60150011ModulePwTestRunner
    {
    tAtModulePwTestRunner super;
    }tAf60150011ModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60150011ModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60150011MODULEPWTESTRUNNER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Af60150011ModuleSdhTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : SDH Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../sdh/AtModuleSdhTestRunnerInternal.h"
#include "../../../../../../../driver/src/implement/default/man/versionreader/ThaVersionReader.h"
#include "../../../../../../../driver/src/implement/default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf60150011ModuleSdhTestRunner
    {
    tAtModuleSdhTestRunner super;
    }tAf60150011ModuleSdhTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern ThaVersionReader ThaDeviceVersionReader(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
static void WriteVersion(uint32 versionNumber)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    uint32 longRegVal[4];
    AtDeviceLongReadOnCore(device, 0, longRegVal, 4, 0);
    longRegVal[1] = versionNumber;
    AtDeviceLongWriteOnCore(device, 0, longRegVal, 4, 0);
    }

static void testLaterVersionMustSupportRemoteLoopback(void)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtChannel line = (AtChannel)AtModuleSdhLineGet(sdhModule, 0);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 cStartVersionSupportLoopback = ThaVersionReaderPwVersionBuild(versionReader, 0x15, 0x01, 0x05, 0x63);
    eBool loopbackMustBeSupported = cAtFalse;

    if (AtTestIsSimulationTesting())
        {
        WriteVersion(cStartVersionSupportLoopback - 1);
        TEST_ASSERT(!AtChannelLoopbackIsSupported(line, cAtLoopbackModeRemote));

        WriteVersion(cStartVersionSupportLoopback);
        TEST_ASSERT(AtChannelLoopbackIsSupported(line, cAtLoopbackModeRemote));

        WriteVersion(cStartVersionSupportLoopback + 1);
        TEST_ASSERT(AtChannelLoopbackIsSupported(line, cAtLoopbackModeRemote));

        return;
        }

    loopbackMustBeSupported = AtDeviceVersionNumber(device) >= cStartVersionSupportLoopback;
    TEST_ASSERT(AtChannelLoopbackIsSupported(line, cAtLoopbackModeRemote) == loopbackMustBeSupported);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(Af60150011ModuleSdhTestRunnerFixtures)
        {
        new_TestFixture("testLaterVersionMustSupportRemoteLoopback", testLaterVersionMustSupportRemoteLoopback)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(Af60150011ModuleSdhTestRunnerCaller, "TestSuite_EncapChannel", NULL, NULL, Af60150011ModuleSdhTestRunnerFixtures);

    return (TestRef)((void *)&Af60150011ModuleSdhTestRunnerCaller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60150011ModuleSdhTestRunner);
    }

AtModuleTestRunner Af60150011ModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleSdhTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60150011ModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60150011ModuleSdhTestRunnerObjectInit(newRunner, module);
    }

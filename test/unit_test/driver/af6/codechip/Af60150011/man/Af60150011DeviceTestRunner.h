/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device
 * 
 * File        : Af60150011DeviceTestRunner.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : Device test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60150011DEVICETESTRUNNER_H_
#define _AF60150011DEVICETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../man/device_runner/AtDeviceTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60150011DeviceTestRunner
    {
    tAtDeviceTestDefaultRunner super;
    }tAf60150011DeviceTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner Af60150011DeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _AF60150011DEVICETESTRUNNER_H_ */


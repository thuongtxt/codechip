/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Af60150011ModuleEthTestRunnerInternal.h
 * 
 * Created Date: Mar 6, 2017
 *
 * Description : Module ethernet test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF60150011MODULEETHTESTRUNNERINTERNAL_H_
#define _AF60150011MODULEETHTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../eth/AtModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAf60150011ModuleEthTestRunner
    {
    tAtModuleEthTestRunner super;
    }tAf60150011ModuleEthTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner Af60150011ModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _AF60150011MODULEETHTESTRUNNERINTERNAL_H_ */


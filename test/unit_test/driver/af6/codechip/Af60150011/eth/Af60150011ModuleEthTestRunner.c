/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Af60150011ModuleEthTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : ETH module test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Af60150011ModuleEthTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthTestRunnerMethods m_AtModuleEthTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FlowControlIsSupported(AtModuleEthTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtModuleEthTestRunner(AtModuleTestRunner self)
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEthTestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtModuleEthTestRunnerOverride));

        mMethodOverride(m_AtModuleEthTestRunnerOverride, FlowControlIsSupported);
        }

    mMethodsSet(runner, &m_AtModuleEthTestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEthTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf60150011ModuleEthTestRunner);
    }

AtModuleTestRunner Af60150011ModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEthTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner Af60150011ModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return Af60150011ModuleEthTestRunnerObjectInit(newRunner, module);
    }

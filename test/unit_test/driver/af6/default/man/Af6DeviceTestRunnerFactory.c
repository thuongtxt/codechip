/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : Af6DeviceTestRunnerFactory.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../man/device_runner/AtDeviceTestRunnerFactoryInternal.h"
#include "../../../man/device_runner/AtDeviceTestRunner.h"
#include "../../codechip/Af60290021/man/Af60290021DeviceTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAf6DeviceTestRunnerFactory
    {
    tAtDeviceTestRunnerDefaultFactory super;
    }tAf6DeviceTestRunnerFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerFactoryMethods m_AtDeviceTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtDeviceTestRunnerFactoryMethods *m_AtDeviceTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtDeviceTestRunner Af60210011DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60210031DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60150011DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60030080DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60031031DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60030111DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60000031DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60210051DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60210012DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60210021DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60290021DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60290011DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60210061DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60291011DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60291022DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60290051DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60290061DeviceTestRunnerNew(AtDevice device);
extern AtDeviceTestRunner Af60290081DeviceTestRunnerNew(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
/* TODO: Too bad, the product code is modified at the outside. Try to detect
 * the real code in this context. This is bad, and product code management scheme
 * need to be changed */
static AtDeviceTestRunner DeviceTestRunnerCreate(AtDeviceTestRunnerFactory self, AtDevice device)
    {
    switch (AtDeviceProductCodeGet(device) & (~cBit31_28))
        {
        case 0x00290022:
            {
            AtDeviceTestRunner newRunner = Af60290021DeviceTestRunnerNew(device);
            Af60290021DeviceTestRunnerFullCapacityOpen(newRunner, cAtTrue);
            return newRunner;
            }

        case 0x00290061: return Af60290061DeviceTestRunnerNew(device);
        case 0x00291011: return Af60291011DeviceTestRunnerNew(device);

        case 0x00290021: return Af60290021DeviceTestRunnerNew(device);
        case 0x00210011: return Af60210011DeviceTestRunnerNew(device);
        case 0x00210051: return Af60210051DeviceTestRunnerNew(device);
        case 0x00210012: return Af60210012DeviceTestRunnerNew(device);
        case 0x00210031: return Af60210031DeviceTestRunnerNew(device);
        case 0x00150011: return Af60150011DeviceTestRunnerNew(device);
        case 0x00030080: return Af60030080DeviceTestRunnerNew(device);
        case 0x00030081: return Af60030080DeviceTestRunnerNew(device);
        case 0x00031031: return Af60031031DeviceTestRunnerNew(device);
        case 0x00030111: return Af60030111DeviceTestRunnerNew(device);
        case 0x00000031: return Af60000031DeviceTestRunnerNew(device);
        case 0x00210021: return Af60210021DeviceTestRunnerNew(device);
        case 0x00290011: return Af60290011DeviceTestRunnerNew(device);
        case 0x00210061: return Af60210061DeviceTestRunnerNew(device);
        case 0x00291022: return Af60291022DeviceTestRunnerNew(device);
        case 0x00290051: return Af60290051DeviceTestRunnerNew(device);
        case 0x00290081: return Af60290081DeviceTestRunnerNew(device);
        default:
            return m_AtDeviceTestRunnerFactoryMethods->DeviceTestRunnerCreate(self, device);
        }
    }

static void OverrideAtDeviceTestRunnerFactory(AtDeviceTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtDeviceTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtDeviceTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerFactoryOverride));

        m_AtDeviceTestRunnerFactoryOverride.DeviceTestRunnerCreate = DeviceTestRunnerCreate;
        }

    self->methods = &m_AtDeviceTestRunnerFactoryOverride;
    }

static void Override(AtDeviceTestRunnerFactory self)
    {
    OverrideAtDeviceTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAf6DeviceTestRunnerFactory);
    }

AtDeviceTestRunnerFactory Af6DeviceTestRunnerFactoryObjectInit(AtDeviceTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDeviceTestRunnerDefaultFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunnerFactory Af6DeviceTestRunnerFactorySharedFactory()
    {
    static tAf6DeviceTestRunnerFactory sharedFactory;
    return Af6DeviceTestRunnerFactoryObjectInit((AtDeviceTestRunnerFactory)&sharedFactory);
    }

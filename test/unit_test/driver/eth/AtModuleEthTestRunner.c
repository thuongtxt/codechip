/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModuleEthTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PPP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModuleEth.h"
#include "AtEthFlow.h"
#include "AtMpBundle.h"
#include "AtModuleEthTestRunnerInternal.h"
#include "../man/device_runner/AtDeviceTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleEthTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleEthTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth Module()
    {
    return (AtModuleEth)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    AtModuleInit((AtModule)Module());
    }

static void testAtLeastOnePortMustBeSupported()
    {
    TEST_ASSERT(AtModuleEthMaxPortsGet(Module()) > 0);
    }

static void testAccessPorts()
    {
    uint8 i;
    AtEthPort port;

    for (i = 0; i < AtModuleEthMaxPortsGet(Module()); i++)
        {
        port = AtModuleEthPortGet(Module(), i);
        TEST_ASSERT_NOT_NULL(port);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)port));
        }
    }

static void testCannotAccessOutOfRangePort()
    {
    TEST_ASSERT_NULL(AtModuleEthPortGet(Module(), AtModuleEthMaxPortsGet(Module())));
    }

static eBool DynamicFlowManagement(AtModuleEthTestRunner self)
    {
    return cAtTrue;
    }

static eBool _DynamicFlowManagement()
    {
    AtUnittestRunner runner = AtUnittestRunnerCurrentRunner();
    return mMethodsGet(mThis(runner))->DynamicFlowManagement(mThis(runner));
    }

static void testCreateNOPTrafficFlow()
    {
    uint16 maxFlows = AtModuleEthMaxFlowsGet(Module());
    uint16 flows[] = {0, maxFlows / 2, maxFlows - 1};
    uint8 i;
    AtEthFlow flow;

    if (maxFlows == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    for (i = 0; i < mCount(flows); i++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleEthNopFlowCreate(Module(), flows[i]));
        flow = AtModuleEthFlowGet(Module(), flows[i]);
        TEST_ASSERT_NOT_NULL(flow);
        TEST_ASSERT_EQUAL_INT(flows[i], AtChannelIdGet((AtChannel)flow));
        }

    for (i = 0; i < mCount(flows); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEthFlowDelete(Module(), flows[i]));
        }
    }

static void testCreateEOPTrafficFlow()
    {
    uint16 maxFlows = AtModuleEthMaxFlowsGet(Module());
    uint16 flows[] = {0, maxFlows / 2, maxFlows - 1};
    uint8 i;
    AtEthFlow flow;

    if (maxFlows == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    for (i = 0; i < mCount(flows); i++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleEthEopFlowCreate(Module(), flows[i]));
        flow = AtModuleEthFlowGet(Module(), flows[i]);
        TEST_ASSERT_NOT_NULL(flow);
        TEST_ASSERT_EQUAL_INT(flows[i], AtChannelIdGet((AtChannel)flow));
        }

    for (i = 0; i < mCount(flows); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEthFlowDelete(Module(), flows[i]));
        }
    }

static void testAllVlansAreInvalidAfterCreatingFlow()
    {
    tAtEthVlanDesc egVlan;
    AtEthFlow flow;

    if (AtModuleEthMaxFlowsGet(Module()) == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    flow = AtModuleEthNopFlowCreate(Module(), AtModuleEthMaxFlowsGet(Module()) / 2);

    /* Check EG VLAN */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthFlowEgressVlanGet(flow, &egVlan));
    TEST_ASSERT_EQUAL_INT(0, egVlan.ethPortId);
    TEST_ASSERT_EQUAL_INT(0, egVlan.numberOfVlans);

    /* Check IG VLAN */
    TEST_ASSERT_EQUAL_INT(0, AtEthFlowIngressAllVlansGet(flow, NULL));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEthFlowDelete(Module(), AtModuleEthMaxFlowsGet(Module()) / 2));
    }

static void testCannotCreateFlowWithOutOfRangeFlowId()
    {
    if (AtModuleEthMaxFlowsGet(Module()) == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    TEST_ASSERT_NULL(AtModuleEthEopFlowCreate(Module(), AtModuleEthMaxFlowsGet(Module())));
    }

static void testCannotCreateDuplicatedFlows()
    {
    uint16 flowToTest = AtModuleEthMaxFlowsGet(Module()) / 2;

    if (AtModuleEthMaxFlowsGet(Module()) == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    TEST_ASSERT_NOT_NULL(AtModuleEthNopFlowCreate(Module(), flowToTest));
    TEST_ASSERT_NULL(AtModuleEthNopFlowCreate(Module(), flowToTest));
    TEST_ASSERT_NULL(AtModuleEthEopFlowCreate(Module(), flowToTest));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEthFlowDelete(Module(), flowToTest));
    }

static void testNullIsReturnedForFlowThatHasNotBeenCreated()
    {
    if (AtModuleEthMaxFlowsGet(Module()) == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    TEST_ASSERT_NULL(AtModuleEthFlowGet(Module(), 0));
    TEST_ASSERT_NULL(AtModuleEthFlowGet(Module(), AtModuleEthMaxFlowsGet(Module()) / 2));
    TEST_ASSERT_NULL(AtModuleEthFlowGet(Module(), AtModuleEthMaxFlowsGet(Module()) - 1));
    }

static void testDeleteFlow()
    {
    uint16 flowToTest = AtModuleEthMaxFlowsGet(Module()) / 2;

    if (AtModuleEthMaxFlowsGet(Module()) == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    /* Create one */
    TEST_ASSERT_NOT_NULL(AtModuleEthNopFlowCreate(Module(), flowToTest));
    TEST_ASSERT_NULL(AtModuleEthNopFlowCreate(Module(), flowToTest));

    /* Delete */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEthFlowDelete(Module(), flowToTest));
    TEST_ASSERT_NULL(AtModuleEthFlowGet(Module(), flowToTest));

    /* It can be created again */
    TEST_ASSERT_NOT_NULL(AtModuleEthNopFlowCreate(Module(), flowToTest));
    }

static void testCanGetFreeFlow()
    {
    uint16 freeFlow, maxFlows, i, flowToTest = 0;

    if (AtModuleEthMaxFlowsGet(Module()) == 0)
        return;

    if (!_DynamicFlowManagement())
        return;

    /* At least one flow is free after initializing */
    TEST_ASSERT_EQUAL_INT(0, AtModuleEthFreeFlowIdGet(Module()));

    /* Create one flow */
    TEST_ASSERT_NOT_NULL(AtModuleEthEopFlowCreate(Module(), flowToTest));

    /* Get a free one */
    maxFlows = AtModuleEthMaxFlowsGet(Module());
    freeFlow = AtModuleEthFreeFlowIdGet(Module());
    if (maxFlows == 1)
        {
        TEST_ASSERT(freeFlow >= maxFlows); /* Invalid flow is returned */
        }
    else
        {
        TEST_ASSERT(freeFlow != flowToTest);
        TEST_ASSERT(freeFlow < maxFlows);
        }

    /* Create all flows */
    for (i = 0; i < maxFlows; i++)
        AtModuleEthNopFlowCreate(Module(), i);

    /* There is no free flow */
    TEST_ASSERT(AtModuleEthFreeFlowIdGet(Module()) >= maxFlows);

    /* Delete one to have free space */
    flowToTest = maxFlows / 2;
    AtModuleEthFlowDelete(Module(), flowToTest);
    TEST_ASSERT_EQUAL_INT(flowToTest, AtModuleEthFreeFlowIdGet(Module()));

    /* And can create it again */
    AtModuleEthNopFlowCreate(Module(), flowToTest);
    TEST_ASSERT(AtModuleEthFreeFlowIdGet(Module()) >= maxFlows);

    /* And delete all of flow */
    for (i = 0; i < maxFlows; i++)
        AtModuleEthFlowDelete(Module(), i);
    }

static AtSerdesManager SerdesManager()
    {
    AtDevice device = AtModuleDeviceGet((AtModule)Module());
    return AtDeviceSerdesManagerGet(device);
    }

static void testNumberOfSERDESControllerReturnedByETHModuleMustEqualToThatOfSERDESManager()
    {
    if (SerdesManager() == NULL)
        return;

    TEST_ASSERT_EQUAL_INT(AtSerdesManagerNumSerdesControllers(SerdesManager()), AtModuleEthNumSerdesControllers(Module()));
    }

static void testAllOfSERDESInstancesReturnedByETHModuleMustBeTheSameAsThoseReturnedBySERDESManager()
    {
    AtModuleEth module = Module();
    AtSerdesManager serdesManager = SerdesManager();
    uint32 numSerdes = AtModuleEthNumSerdesControllers(module);
    uint32 serdes_i;

    if (serdesManager == NULL)
        return;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = AtModuleEthSerdesController(module, serdes_i);
        TEST_ASSERT(serdes == AtSerdesManagerSerdesControllerGet(serdesManager, serdes_i));
        }
    }

static void testNumPortsMustBeEnough()
    {
    AtModuleEthTestRunner runner = (AtModuleEthTestRunner)AtUnittestRunnerCurrentRunner();
    uint32 numPorts = mMethodsGet(runner)->NumEthPorts(runner);
    TEST_ASSERT_EQUAL_INT(numPorts, AtModuleEthMaxPortsGet(Module()));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleEth)
        {
        new_TestFixture("testAtLeastOnePortMustBeSupported", testAtLeastOnePortMustBeSupported),
        new_TestFixture("testAccessPorts", testAccessPorts),
        new_TestFixture("testCannotAccessOutOfRangePort", testCannotAccessOutOfRangePort),
        new_TestFixture("testCreateNOPTrafficFlow", testCreateNOPTrafficFlow),
        new_TestFixture("testCreateEOPTrafficFlow", testCreateEOPTrafficFlow),
        new_TestFixture("testAllVlansAreInvalidAfterCreatingFlow", testAllVlansAreInvalidAfterCreatingFlow),
        new_TestFixture("testCannotCreateFlowWithOutOfRangeFlowId", testCannotCreateFlowWithOutOfRangeFlowId),
        new_TestFixture("testCannotCreateDuplicatedFlows", testCannotCreateDuplicatedFlows),
        new_TestFixture("testNullIsReturnedForFlowThatHasNotBeenCreated", testNullIsReturnedForFlowThatHasNotBeenCreated),
        new_TestFixture("testDeleteFlow", testDeleteFlow),
        new_TestFixture("testCanGetFreeFlow", testCanGetFreeFlow),
        new_TestFixture("testNumberOfSERDESControllerReturnedByETHModuleMustEqualToThatOfSERDESManager", testNumberOfSERDESControllerReturnedByETHModuleMustEqualToThatOfSERDESManager),
        new_TestFixture("testAllOfSERDESInstancesReturnedByETHModuleMustBeTheSameAsThoseReturnedBySERDESManager", testAllOfSERDESInstancesReturnedByETHModuleMustBeTheSameAsThoseReturnedBySERDESManager),
        new_TestFixture("testNumPortsMustBeEnough", testNumPortsMustBeEnough)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleEth_Caller, "TestSuite_ModuleEth", setUp, NULL, TestSuite_ModuleEth);

    return (TestRef)((void *)&TestSuite_ModuleEth_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtChannelTestRunner CreateEthPortTestRunner(AtModuleEthTestRunner self, AtEthPort port)
    {
    return AtEthPortTestRunnerNew((AtChannel)port, (AtModuleTestRunner)self);
    }

static AtChannelTestRunner CreateEthFlowTestRunner(AtModuleEthTestRunner self, AtEthFlow flow)
    {
    return AtEthFlowTestRunnerNew((AtChannel)flow, (AtModuleTestRunner)self);
    }

static eBool ShouldTestEthPort(AtModuleEthTestRunner self)
    {
    return cAtTrue;
    }

static void TestEthPort(AtModuleEthTestRunner self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint8 portId;

    if (!mMethodsGet(self)->ShouldTestEthPort(self))
        return;

    for (portId = 0; portId < AtModuleEthMaxPortsGet(ethModule); portId++)
        {
        AtEthPort port = AtModuleEthPortGet(ethModule, portId);
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(mThis(self))->CreateEthPortTestRunner(mThis(self), port);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        }
    }

static void TestEthFlowInBundle(AtModuleEthTestRunner self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtEthFlow flow;
    AtHdlcBundle hdlcBundle;
    AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self), cAtModulePpp);
    AtUnittestRunner runner;
    AtDeviceTestRunner deviceRunner = AtModuleTestRunnerDeviceRunnerGet((AtModuleTestRunner)self);

    if (AtDeviceTestRunnerModuleNotReady(deviceRunner, cAtModulePpp))
        {
        AtPrintc(cSevWarning, "WARNING: %s is ignored because of PPP module is not ready\r\n", __FUNCTION__);
        return;
        }

    if (AtModulePppMaxBundlesGet(pppModule) == 0)
        return;

    hdlcBundle = (AtHdlcBundle)AtModulePppMpBundleCreate(pppModule, 0);
    flow = AtModuleEthNopFlowCreate(ethModule, 0);
    AtAssert(AtMpBundleFlowAdd((AtMpBundle)hdlcBundle, flow, 0) == cAtOk);
    runner = (AtUnittestRunner)mMethodsGet(self)->CreateEthFlowTestRunner(self, flow);
    AtUnittestRunnerDelete(runner);

    AtMpBundleFlowRemove((AtMpBundle)hdlcBundle, flow);
    AtModuleEthFlowDelete(ethModule, 0x0);
    }

static void TestEthFlow(AtModuleEthTestRunner self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint16 flowId;
    AtUnittestRunner runner;

    if (AtModuleEthMaxFlowsGet(ethModule) == 0)
        return;

    for (flowId = 0; flowId < AtModuleEthMaxFlowsGet(ethModule); flowId++)
        {
        AtEthFlow flow;

        flow   = AtModuleEthNopFlowCreate(ethModule, flowId);
        runner = (AtUnittestRunner)mMethodsGet(self)->CreateEthFlowTestRunner(self, flow);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtModuleEthFlowDelete(ethModule, flowId);

        flow   = AtModuleEthEopFlowCreate(ethModule, flowId);
        runner = (AtUnittestRunner)mMethodsGet(self)->CreateEthFlowTestRunner(self, flow);
        AtUnittestRunnerDelete(runner);
        AtModuleEthFlowDelete(ethModule, flowId);
        }

    TestEthFlowInBundle(self);
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);

    mMethodsGet(mThis(self))->TestEthPort(mThis(self));
    mMethodsGet(mThis(self))->TestEthFlow(mThis(self));
    }

static eBool TxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    return cAtTrue;
    }

static eBool RxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    return cAtTrue;
    }

static eBool CanChangeFlowControlParameters(AtModuleEthTestRunner self)
    {
    return mMethodsGet(self)->FlowControlIsSupported(self);
    }

static eBool FlowControlIsSupported(AtModuleEthTestRunner self)
    {
    return cAtTrue;
    }

static eBool EthPortHasMac(AtModuleEthTestRunner self, AtEthPort port)
    {
    return cAtTrue;
    }

static eBool EthPortHasMacChecking(AtModuleEthTestRunner self, AtEthPort port)
    {
    return cAtTrue;
    }

static uint32 NumEthPorts(AtModuleEthTestRunner self)
    {
    return 0;
    }

static void MethodsInit(AtModuleEthTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TestEthPort);
        mMethodOverride(m_methods, TestEthFlow);
        mMethodOverride(m_methods, CreateEthFlowTestRunner);
        mMethodOverride(m_methods, CreateEthPortTestRunner);
        mMethodOverride(m_methods, DynamicFlowManagement);
        mMethodOverride(m_methods, TxIpgIsSupported);
        mMethodOverride(m_methods, RxIpgIsSupported);
        mMethodOverride(m_methods, CanChangeFlowControlParameters);
        mMethodOverride(m_methods, FlowControlIsSupported);
        mMethodOverride(m_methods, ShouldTestEthPort);
        mMethodOverride(m_methods, EthPortHasMac);
        mMethodOverride(m_methods, EthPortHasMacChecking);
        mMethodOverride(m_methods, NumEthPorts);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleEthTestRunner);
    }

AtModuleTestRunner AtModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtModuleEthTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModuleEthTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModuleEthTestRunnerObjectInit(newRunner, module);
    }

eBool AtModuleEthTestRunnerTxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->TxIpgIsSupported(self, port);
    return cAtFalse;
    }

eBool AtModuleEthTestRunnerRxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->RxIpgIsSupported(self, port);
    return cAtFalse;
    }

eBool AtModuleEthTestRunnerCanChangeFlowControlParameters(AtModuleEthTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->CanChangeFlowControlParameters(self);
    return cAtFalse;
    }

eBool AtModuleEthTestRunnerFlowControlIsSupported(AtModuleEthTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->FlowControlIsSupported(self);
    return cAtFalse;
    }

eBool AtModuleEthTestRunnerEthPortHasMac(AtModuleEthTestRunner self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->EthPortHasMac(self, port);
    return cAtFalse;
    }

eBool AtModuleEthTestRunnerEthPortHasMacChecking(AtModuleEthTestRunner self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->EthPortHasMacChecking(self, port);
    return cAtFalse;
    }

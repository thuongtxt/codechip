/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtEthPortTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : ETH Port test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "../../../../driver/src/generic/eth/AtEthPortInternal.h"
#include "AtEthPortTestRunnerInternal.h"
#include "AtModuleEthTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPortTestRunner CurrentRunner()
    {
    return (AtEthPortTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtEthPort Port()
    {
    return (AtEthPort)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eAtEthPortSpeed *ValidPortSpeeds(uint8 *numSpeeds)
    {
    static eAtEthPortSpeed speeds[] = {
                                      cAtEthPortSpeed10M,
                                      cAtEthPortSpeed100M,
                                      cAtEthPortSpeed1000M,
                                      cAtEthPortSpeed2500M,
                                      cAtEthPortSpeed10G,
                                      cAtEthPortSpeed40G
                                      };
    *numSpeeds = mCount(speeds);

    return speeds;
    }

static eBool PortSpeedIsValid(eAtEthPortSpeed speed)
    {
    eAtEthPortSpeed *validSpeeds;
    uint8 numSpeeds, i;

    validSpeeds = ValidPortSpeeds(&numSpeeds);
    for (i = 0; i < numSpeeds; i++)
        {
        if (speed == validSpeeds[i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static void testPortSpeedMustBeValidAsDefault()
    {
    TEST_ASSERT(PortSpeedIsValid(AtEthPortSpeedGet(Port())));
    }

static void testAtLeastOneSpeedMustBeSupported()
    {
    eAtEthPortSpeed *validSpeeds;
    uint8 numSpeeds, i;

    validSpeeds = ValidPortSpeeds(&numSpeeds);
    for (i = 0; i < numSpeeds; i++)
        {
        if (AtEthPortSpeedIsSupported(Port(), validSpeeds[i]))
            return;
        }

    TEST_FAIL("At least one speed must be supported");
    }

static void testChangeSpeed()
    {
    eAtEthPortSpeed *validSpeeds, speed;
    uint8 numSpeeds, i;

    validSpeeds = ValidPortSpeeds(&numSpeeds);
    for (i = 0; i < numSpeeds; i++)
        {
        speed = validSpeeds[i];
        if (AtEthPortSpeedIsSupported(Port(), speed))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortSpeedSet(Port(), speed));
            TEST_ASSERT_EQUAL_INT(speed, AtEthPortSpeedGet(Port()));
            }

        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtEthPortSpeedSet(Port(), speed) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testCannotSetInvalidSpeed()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtEthPortSpeedSet(Port(), 0xfff) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testDuplexModeMustBeValidAsDefault()
    {
    eAtEthPortDuplexMode mode = AtEthPortDuplexModeGet(Port());
    TEST_ASSERT((mode == cAtEthPortWorkingModeHalfDuplex) ||
                (mode == cAtEthPortWorkingModeFullDuplex) ||
                (mode == cAtEthPortWorkingModeAutoDetect));
    }

static eBool DuplexModeIsSupported()
    {
    return cAtFalse;
    }

static void testChangeDuplexMode()
    {
    if (!DuplexModeIsSupported())
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortDuplexModeSet(Port(), cAtEthPortWorkingModeHalfDuplex));
    TEST_ASSERT_EQUAL_INT(cAtEthPortWorkingModeHalfDuplex, AtEthPortDuplexModeGet(Port()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortDuplexModeSet(Port(), cAtEthPortWorkingModeFullDuplex));
    TEST_ASSERT_EQUAL_INT(cAtEthPortWorkingModeFullDuplex, AtEthPortDuplexModeGet(Port()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortDuplexModeSet(Port(), cAtEthPortWorkingModeAutoDetect));
    TEST_ASSERT_EQUAL_INT(cAtEthPortWorkingModeAutoDetect, AtEthPortDuplexModeGet(Port()));
    }

static void testCannotSetInvalidDuplexMode()
    {
    if (!DuplexModeIsSupported())
        return;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtEthPortDuplexModeSet(Port(), 0xffff) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static AtModuleEthTestRunner ModuleRunner(void)
    {
    return (AtModuleEthTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)CurrentRunner());
    }

static eBool TxIpgIsSupported(AtEthPort port)
    {
    return AtModuleEthTestRunnerTxIpgIsSupported(ModuleRunner(), port);
    }

static eBool RxIpgIsSupported(AtEthPort port)
    {
    return AtModuleEthTestRunnerRxIpgIsSupported(ModuleRunner(), port);
    }

static eBool HasMac()
    {
    return AtModuleEthTestRunnerEthPortHasMac(ModuleRunner(), Port());
    }

static void testChangeIpg()
    {
    /* TX */
    if (TxIpgIsSupported(Port()))
        {
        if (AtEthPortTxIpgIsInRange(Port(), 4))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortTxIpgSet(Port(), 4));
            TEST_ASSERT_EQUAL_INT(4, AtEthPortTxIpgGet(Port()));
            }
        if (AtEthPortTxIpgIsInRange(Port(), 5))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortTxIpgSet(Port(), 5));
            TEST_ASSERT_EQUAL_INT(5, AtEthPortTxIpgGet(Port()));
            }
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEthPortTxIpgSet(Port(), 4));
        AtTestLoggerEnable(cAtTrue);
        }

    /* RX */
    if (RxIpgIsSupported(Port()))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortRxIpgSet(Port(), 4));
        TEST_ASSERT_EQUAL_INT(4, AtEthPortRxIpgGet(Port()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortRxIpgSet(Port(), 5));
        TEST_ASSERT_EQUAL_INT(5, AtEthPortRxIpgGet(Port()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEthPortRxIpgSet(Port(), 4));
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCannotSetInvalidIpg()
    {
    AtTestLoggerEnable(cAtFalse);

    if (TxIpgIsSupported(Port()))
        {
        TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtEthPortTxIpgSet(Port(), 3));
        }

    if (RxIpgIsSupported(Port()))
        {
        TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtEthPortRxIpgSet(Port(), 3));
        }

    AtTestLoggerEnable(cAtTrue);
    }

static void testFlowControlEnabling()
    {
    if (!AtModuleEthTestRunnerFlowControlIsSupported(ModuleRunner()))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortFlowControlEnable(Port(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtEthPortFlowControlIsEnabled(Port()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortFlowControlEnable(Port(), cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtEthPortFlowControlIsEnabled(Port()));
    }

static eBool CanChangeFlowControlParameters(void)
    {
    return AtModuleEthTestRunnerCanChangeFlowControlParameters(ModuleRunner());
    }

static void testChangePauseFrameInterval()
    {
    if (CanChangeFlowControlParameters())
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortPauseFrameIntervalSet(Port(), 100));
        TEST_ASSERT_EQUAL_INT(100, AtEthPortPauseFrameIntervalGet(Port()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtEthPortPauseFrameIntervalSet(Port(), 100) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        AtEthPortPauseFrameIntervalGet(Port());
        }
    }

static void testChangePauseExpireTime()
    {
    if (CanChangeFlowControlParameters())
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortPauseFrameExpireTimeSet(Port(), 100));
        TEST_ASSERT_EQUAL_INT(100, AtEthPortPauseFrameExpireTimeGet(Port()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtEthPortPauseFrameExpireTimeSet(Port(), 100) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        AtEthPortPauseFrameExpireTimeGet(Port());
        }
    }

static void TestAutoNegEnableDisable(eBool enable)
    {
    AtEthPort port = Port();

    if (AtEthPortAutoNegIsSupported(port))
        {
        TEST_ASSERT(AtEthPortAutoNegEnable(Port(), enable) == cAtOk);
        TEST_ASSERT_EQUAL_INT(enable, AtEthPortAutoNegIsEnabled(Port()));
        }

    else
        {
        AtTestLoggerEnable(cAtFalse);

        if (enable)
            {
            TEST_ASSERT(AtEthPortAutoNegEnable(Port(), enable) != cAtOk);
            }
        else
            {
            TEST_ASSERT(AtEthPortAutoNegEnable(Port(), enable) == cAtOk);
            }

        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testAutoNegEnabling()
    {
    TestAutoNegEnableDisable(cAtTrue);
    TestAutoNegEnableDisable(cAtFalse);
    }

static void testChangeEthPortSourceMAC()
    {
    uint8 mac[] = {0x26, 0x95, 0x11, 0xB4, 0xD2, 0x27};
    uint8 actualMac[6];

    if (AtEthPortHasSourceMac(Port()))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortSourceMacAddressSet(Port(), mac));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortSourceMacAddressGet(Port(), actualMac));
        TEST_ASSERT(memcmp(actualMac, mac, 6) == 0);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtEthPortSourceMacAddressSet(Port(), mac) != cAtOk);
        TEST_ASSERT(AtEthPortSourceMacAddressGet(Port(), actualMac) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testChangeEthPortDestMAC()
    {
    uint8 mac[] = {0x26, 0x95, 0x11, 0xB4, 0xD2, 0x27};
    uint8 actualMac[6];

    if (AtEthPortHasDestMac(Port()))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortDestMacAddressSet(Port(), mac));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortDestMacAddressGet(Port(), actualMac));
        TEST_ASSERT(AtOsalMemCmp(actualMac, mac, 6) == 0);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtEthPortDestMacAddressSet(Port(), mac) != cAtOk);
        TEST_ASSERT(AtEthPortDestMacAddressGet(Port(), actualMac) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static eBool HasMacChecking(void)
    {
    if (!HasMac())
        return cAtFalse;

    return AtModuleEthTestRunnerEthPortHasMacChecking(ModuleRunner(), Port());
    }

static void testChangeMacChecking()
    {
    if (HasMacChecking())
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortMacCheckingEnable(Port(), cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtEthPortMacCheckingIsEnabled(Port()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortMacCheckingEnable(Port(), cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtEthPortMacCheckingIsEnabled(Port()));
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortMacCheckingEnable(Port(), cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtEthPortMacCheckingIsEnabled(Port()));

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtEthPortMacCheckingEnable(Port(), cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }

    AtTestLoggerEnable(cAtTrue);
    }

static void testLoopback()
    {
    AtChannel port = (AtChannel)Port();
    eAtLoopbackMode loopbackModes[] = {cAtLoopbackModeLocal, cAtLoopbackModeRemote, cAtLoopbackModeRelease};
    uint8 i;

    /* Release must be supported */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet(port, cAtLoopbackModeRelease));
    TEST_ASSERT_EQUAL_INT(cAtLoopbackModeRelease, AtChannelLoopbackGet(port));

    for (i = 0; i < mCount(loopbackModes); i++)
        {
        if (!AtChannelLoopbackIsSupported(port, loopbackModes[i]))
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet(port, loopbackModes[i]));
        TEST_ASSERT_EQUAL_INT(loopbackModes[i], AtChannelLoopbackGet(port));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_EthPort_Fixtures)
        {
        new_TestFixture("testChangeEthPortSourceMAC", testChangeEthPortSourceMAC),
        new_TestFixture("testChangeEthPortDestMAC", testChangeEthPortDestMAC),
        new_TestFixture("testPortSpeedMustBeValidAsDefault", testPortSpeedMustBeValidAsDefault),
        new_TestFixture("testAtLeastOneSpeedMustBeSupported", testAtLeastOneSpeedMustBeSupported),
        new_TestFixture("testChangeSpeed", testChangeSpeed),
        new_TestFixture("testCannotSetInvalidSpeed", testCannotSetInvalidSpeed),
        new_TestFixture("testDuplexModeMustBeValidAsDefault", testDuplexModeMustBeValidAsDefault),
        new_TestFixture("testChangeDuplexMode", testChangeDuplexMode),
        new_TestFixture("testCannotSetInvalidDuplexMode", testCannotSetInvalidDuplexMode),
        new_TestFixture("testChangeIpg", testChangeIpg),
        new_TestFixture("testCannotSetInvalidIpg", testCannotSetInvalidIpg),
        new_TestFixture("testFlowControlEnabling", testFlowControlEnabling),
        new_TestFixture("testChangePauseFrameInterval", testChangePauseFrameInterval),
        new_TestFixture("testChangePauseExpireTime", testChangePauseExpireTime),
        new_TestFixture("testAutoNegEnabling", testAutoNegEnabling),
        new_TestFixture("testChangeMacChecking", testChangeMacChecking),
        new_TestFixture("testLoopback", testLoopback)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_EthPort_Caller, "TestSuite_EthPort", NULL, NULL, TestSuite_EthPort_Fixtures);

    return (TestRef)((void *)&TestSuite_EthPort_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthPortTestRunner);
    }

AtChannelTestRunner AtEthPortTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtEthPortTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtEthPortTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

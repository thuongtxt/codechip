/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtEthFlowTestRunnerInternal.h
 * 
 * Created Date: Jun 20, 2014
 *
 * Description : ETH Flow test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHFLOWTESTRUNNERINTERNAL_H_
#define _ATETHFLOWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEthFlowTestRunner * AtEthFlowTestRunner;

typedef struct tAtEthFlowTestRunnerMethods
    {
    eBool (*CVlanIsSupported)(AtEthFlowTestRunner self);
    eBool (*SVlanIsSupported)(AtEthFlowTestRunner self);
    eBool (*VlanDescsAreSame)(AtEthFlowTestRunner self, const tAtEthVlanDesc *vlan1, const tAtEthVlanDesc *vlan2);
    eBool (*EgressDMACIsSupported)(AtEthFlowTestRunner self);
    eBool (*EgressVlanIsSupported)(AtEthFlowTestRunner self);
    tAtEthVlanDesc *(*GenerateIngressVlanDescriptor)(AtEthFlowTestRunner self, tAtEthVlanDesc *descriptor);
    tAtEthVlanDesc *(*GenerateEgressVlanDescriptor)(AtEthFlowTestRunner self, tAtEthVlanDesc *descriptor);
    uint32 (*MaxNumIngressVlans)(AtEthFlowTestRunner self);
    }tAtEthFlowTestRunnerMethods;

typedef struct tAtEthFlowTestRunner
    {
    tAtChannelTestRunner super;
    const tAtEthFlowTestRunnerMethods *methods;
    }tAtEthFlowTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
    AtChannelTestRunner AtEthFlowTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

#endif /* _ATETHFLOWTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtEthFlowTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : ETH Port test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModuleEth.h"
#include "AtEthFlow.h"
#include "AtModulePpp.h"
#include "AtMpBundle.h"
#include "AtEthFlowTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEthFlowTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEthFlowTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthFlowTestRunner CurrentRunner()
    {
    return (AtEthFlowTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtEthFlow Flow()
    {
    return (AtEthFlow)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void RemoveAllVlans()
    {
    tAtEthVlanDesc *allVlans;
    uint8 numVlans, i;

    numVlans = AtEthFlowIngressAllVlansGet(Flow(), NULL);
    if (numVlans == 0)
        return;

    /* Remove all VLANs */
    allVlans = AtOsalMemAlloc(sizeof(tAtEthVlanDesc) * numVlans);
    numVlans = AtEthFlowIngressAllVlansGet(Flow(), allVlans);
    for (i = 0; i < numVlans; i++)
        AtEthFlowIngressVlanRemove(Flow(), &(allVlans[i]));
    AtOsalMemFree(allVlans);
    }

static void setUp()
    {
    RemoveAllVlans();
    }

static void tearDown()
    {
    RemoveAllVlans();
    }

static eBool EgressDMACIsSupported(AtEthFlowTestRunner self)
    {
    return cAtTrue;
    }

static eBool EgressVlanIsSupported(AtEthFlowTestRunner self)
    {
    return cAtTrue;
    }

static void testChangeEgressDestinationMAC()
    {
    uint8 mac[] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66};
    uint8 actualMac[6];
    eAtRet ret = AtEthFlowEgressDestMacSet(Flow(), mac);

    if (mMethodsGet(CurrentRunner())->EgressDMACIsSupported(CurrentRunner()))
        {
        TEST_ASSERT_EQUAL_INT(ret, cAtOk);
        TEST_ASSERT_EQUAL_INT(AtEthFlowEgressDestMacGet(Flow(), actualMac), cAtOk);
        TEST_ASSERT(memcmp(actualMac, mac, 6) == 0);
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(ret, cAtErrorModeNotSupport);
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEthFlowEgressDestMacGet(Flow(), actualMac));
        }
    }

static eBool VlansAreSame(const tAtEthVlanTag *vlan1, const tAtEthVlanTag *vlan2)
    {
    return (vlan1->priority == vlan2->priority) &&
           (vlan1->cfi == vlan2->cfi) &&
           (vlan1->vlanId == vlan2->vlanId);
    }

static eBool VlanDescsAreSame(AtEthFlowTestRunner self, const tAtEthVlanDesc *vlan1, const tAtEthVlanDesc *vlan2)
    {
    uint8 i;

    if ((vlan1->ethPortId != vlan2->ethPortId) ||
        (vlan1->numberOfVlans != vlan2->numberOfVlans))
        return cAtFalse;

    for (i = 0; i < vlan1->numberOfVlans; i++)
        {
        if (!VlansAreSame(&(vlan1->vlans[i]), &(vlan2->vlans[i])))
            return cAtFalse;
        }

    return cAtTrue;
    }

static tAtEthVlanTag *GenerateVlan(tAtEthVlanTag *tag)
    {
    static uint8 cfi = 0, priority = 0, vlanId = 1;
    static const uint8 maxPriority = 8;
    static const uint16 maxVlanId = 4096;

    /* Get current data */
    tag->cfi = cfi;
    tag->priority = priority;
    tag->vlanId = vlanId;

    /* Prepare for next generating */
    cfi = cfi ? 0 : 1;
    priority = ((priority + 1) % maxPriority);
    vlanId = (vlanId + 1) % maxVlanId;

    return tag;
    }

static tAtEthVlanDesc *GenerateVlanDescriptor(tAtEthVlanDesc *descriptor)
    {
    static uint8 portId = 0;
    static uint8 numVlans = 1;
    uint8 maxNumVlans = 2;
    uint8 i;
    AtEthFlowTestRunner runner = mThis(AtUnittestRunnerCurrentRunner());

    AtOsalMemInit(descriptor, 0x0, sizeof(tAtEthVlanDesc));

    /* Get current data */
    descriptor->ethPortId = portId;

    /* No VLAN is supported */
    if (!mMethodsGet(runner)->CVlanIsSupported(runner))
        return descriptor;

    descriptor->numberOfVlans = numVlans;
    for (i = 0; i < numVlans; i++)
        GenerateVlan(&(descriptor->vlans[i]));

    /* Prepare for next generating */
    if (!mMethodsGet(runner)->SVlanIsSupported(runner))
        maxNumVlans = 1;
    portId   = (portId + 1) % AtModuleEthMaxPortsGet((AtModuleEth)AtChannelModuleGet((AtChannel)Flow()));
    numVlans = (numVlans + 1) % (maxNumVlans + 1);
    if (numVlans == 0)
        numVlans = 1;

    return descriptor;
    }

static void testChangeEgressVLANDescriptor()
    {
    tAtEthVlanDesc vlanDesc, actualVlanDesc;
    uint8 i;
    AtEthFlowTestRunner runner = CurrentRunner();
    AtOsalMemInit(&vlanDesc, 0x0, sizeof(tAtEthVlanDesc));

    if (mMethodsGet(runner)->EgressVlanIsSupported(runner))
        {
        for (i = 0; i < 3; i++)
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtEthFlowEgressVlanSet(Flow(), mMethodsGet(runner)->GenerateEgressVlanDescriptor(runner, &vlanDesc)));
            TEST_ASSERT_EQUAL_INT(cAtOk, AtEthFlowEgressVlanGet(Flow(), &actualVlanDesc));
            TEST_ASSERT(mMethodsGet(runner)->VlanDescsAreSame(runner, &vlanDesc, &actualVlanDesc));
            }
        }

    else
        {
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEthFlowEgressVlanSet(Flow(), mMethodsGet(runner)->GenerateEgressVlanDescriptor(runner, &vlanDesc)));
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEthFlowEgressVlanGet(Flow(), &actualVlanDesc));
        }
    }

static void testChangeEgressNotSupportedVlans()
    {
    tAtEthVlanDesc vlanDesc;
    tAtEthVlanTag cVlan, sVlan;

    AtOsalMemInit(&vlanDesc, 0, sizeof(vlanDesc));
    AtEthVlanTagConstruct(1, 0, 1, &cVlan);
    AtEthVlanTagConstruct(2, 1, 2, &sVlan);

    /* No VLANs are supported */
    if (!mMethodsGet(CurrentRunner())->CVlanIsSupported(CurrentRunner()))
        {
        TEST_ASSERT(AtEthFlowEgressVlanSet(Flow(), AtEthFlowVlanDescConstruct(0, NULL, &cVlan, &vlanDesc)) != cAtOk);
        return;
        }

    /* Only C-VLAN is supported */
    if (!mMethodsGet(CurrentRunner())->SVlanIsSupported(CurrentRunner()))
        {
        TEST_ASSERT(AtEthFlowEgressVlanSet(Flow(), AtEthFlowVlanDescConstruct(0, &sVlan, &cVlan, &vlanDesc)) != cAtOk);
        return;
        }
    }

static void testCannotChangeEgressVlanDescriptorToNull()
    {
    TEST_ASSERT(AtEthFlowEgressVlanSet(Flow(), NULL) != cAtOk);
    }

static eBool FlowContainsVlan(const tAtEthVlanDesc *vlan)
    {
    tAtEthVlanDesc *allDescs;
    uint16 numVlans, i;

    if (AtEthFlowIngressAllVlansGet(Flow(), NULL) == 0)
        return cAtFalse;

    allDescs = AtOsalMemAlloc(sizeof(tAtEthVlanDesc) * AtEthFlowIngressAllVlansGet(Flow(), NULL));
    numVlans = AtEthFlowIngressAllVlansGet(Flow(), allDescs);
    for (i = 0; i < numVlans; i++)
        {
        if (mMethodsGet(mThis(AtUnittestRunnerCurrentRunner()))->VlanDescsAreSame(mThis(AtUnittestRunnerCurrentRunner()), &(allDescs[i]), vlan))
            {
            AtOsalMemFree(allDescs);
            return cAtTrue;
            }
        }

    AtOsalMemFree(allDescs);

    return cAtFalse;
    }

static eBool SVlanIsSupported(AtEthFlowTestRunner self)
    {
    return cAtTrue;
    }

static eBool CVlanIsSupported(AtEthFlowTestRunner self)
    {
    return cAtTrue;
    }

static tAtEthVlanDesc *GenerateIngressVlanDescriptor(AtEthFlowTestRunner self, tAtEthVlanDesc *descriptor)
    {
    return GenerateVlanDescriptor(descriptor);
    }

static tAtEthVlanDesc *GenerateEgressVlanDescriptor(AtEthFlowTestRunner self, tAtEthVlanDesc *descriptor)
    {
    return GenerateVlanDescriptor(descriptor);
    }

static uint32 MaxNumIngressVlans(AtEthFlowTestRunner self)
    {
    return 2;
    }

static void testAddIngressVlan()
    {
    tAtEthVlanDesc vlanDesc;
    eAtRet ret = AtEthFlowIngressVlanAdd(Flow(), mMethodsGet(CurrentRunner())->GenerateIngressVlanDescriptor(CurrentRunner(), &vlanDesc));

    TEST_ASSERT(ret == cAtOk);
    TEST_ASSERT(FlowContainsVlan(&vlanDesc));
    }

static void testIngressCannotAddNotSupportedVlans()
    {
    tAtEthVlanDesc vlanDesc;
    tAtEthVlanTag cVlan, sVlan;

    AtOsalMemInit(&vlanDesc, 0, sizeof(vlanDesc));
    AtEthVlanTagConstruct(1, 0, 1, &cVlan);
    AtEthVlanTagConstruct(2, 1, 2, &sVlan);

    /* No VLANs are supported */
    if (!mMethodsGet(CurrentRunner())->CVlanIsSupported(CurrentRunner()))
        {
        TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), AtEthFlowVlanDescConstruct(0, NULL, &cVlan, &vlanDesc)) != cAtOk);
        return;
        }

    /* Only C-VLAN is supported */
    if (!mMethodsGet(CurrentRunner())->SVlanIsSupported(CurrentRunner()))
        {
        TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), AtEthFlowVlanDescConstruct(0, &sVlan, &cVlan, &vlanDesc)) != cAtOk);
        }
    }

static void testCannotAddTheSameVlanDescriptor()
    {
    tAtEthVlanDesc vlanDesc;

    TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), mMethodsGet(CurrentRunner())->GenerateIngressVlanDescriptor(CurrentRunner(), &vlanDesc)) == cAtOk);
    TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), &vlanDesc) != cAtOk);
    }

static void testCannotAddNullIngressVLAN()
    {
    TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), NULL) != cAtOk);
    }

static void testRemoveIngressVlan()
    {
    tAtEthVlanDesc vlanDesc[2];
    uint32 maxVlans = mMethodsGet(CurrentRunner())->MaxNumIngressVlans(CurrentRunner());
    uint8 numVlans;
    uint8 vlan_i;

    numVlans = AtEthFlowIngressAllVlansGet(Flow(), NULL);
    if (numVlans >= maxVlans)
        return;

    for (vlan_i = 0; vlan_i < maxVlans; vlan_i++)
        {
        TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), mMethodsGet(CurrentRunner())->GenerateIngressVlanDescriptor(CurrentRunner(), &vlanDesc[vlan_i])) == cAtOk);
        }

    TEST_ASSERT_EQUAL_INT(numVlans + vlan_i, AtEthFlowIngressAllVlansGet(Flow(), NULL));
    numVlans = numVlans + vlan_i;

    /* Remove one by one */
    for (; vlan_i > 0; vlan_i--)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtEthFlowIngressVlanRemove(Flow(), &vlanDesc[vlan_i - 1]));
        TEST_ASSERT(!FlowContainsVlan(&vlanDesc[vlan_i - 1]));
        }
    }

static void testCannotRemoveVlanDescriptorIfItHasNotBeenAdded()
    {
    tAtEthVlanDesc vlanDesc;

    TEST_ASSERT(AtEthFlowIngressVlanAdd(Flow(), mMethodsGet(CurrentRunner())->GenerateIngressVlanDescriptor(CurrentRunner(), &vlanDesc)) == cAtOk);
    TEST_ASSERT(AtEthFlowIngressVlanRemove(Flow(), mMethodsGet(CurrentRunner())->GenerateIngressVlanDescriptor(CurrentRunner(), &vlanDesc)) != cAtOk);
    }

static void testExhausted()
    {
    tAtEthVlanDesc vlanDesc;
    uint16 i;

    /* Try to create many VLANs to check if driver check exhausted case */
    for (i = 0; i < 65535; i++)
        {
        if (AtEthFlowIngressVlanAdd(Flow(), mMethodsGet(CurrentRunner())->GenerateIngressVlanDescriptor(CurrentRunner(), &vlanDesc)) != cAtOk)
            return;
        }

    /* It does not check as it should */
    TEST_FAIL("Driver should handle exhausted case");
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_EthFlow_Fixtures)
        {
        new_TestFixture("testChangeEgressDestinationMAC", testChangeEgressDestinationMAC),
        new_TestFixture("testChangeEgressVLANDescriptor", testChangeEgressVLANDescriptor),
        new_TestFixture("testChangeEgressNotSupportedVlans", testChangeEgressNotSupportedVlans),
        new_TestFixture("testCannotChangeEgressVlanDescriptorToNull", testCannotChangeEgressVlanDescriptorToNull),
        new_TestFixture("testAddIngressVlan", testAddIngressVlan),
        new_TestFixture("testIngressCannotAddNotSupportedVlans", testIngressCannotAddNotSupportedVlans),
        new_TestFixture("testCannotAddTheSameVlanDescriptor", testCannotAddTheSameVlanDescriptor),
        new_TestFixture("testCannotAddNullIngressVLAN", testCannotAddNullIngressVLAN),
        new_TestFixture("testRemoveIngressVlan", testRemoveIngressVlan),
        new_TestFixture("testCannotRemoveVlanDescriptorIfItHasNotBeenAdded", testCannotRemoveVlanDescriptorIfItHasNotBeenAdded),
        new_TestFixture("testExhausted", testExhausted)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_EthFlow_Caller, "TestSuite_EthFlow", setUp, tearDown, TestSuite_EthFlow_Fixtures);

    return (TestRef)((void *)&TestSuite_EthFlow_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void MethodsInit(AtEthFlowTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SVlanIsSupported);
        mMethodOverride(m_methods, CVlanIsSupported);
        mMethodOverride(m_methods, VlanDescsAreSame);
        mMethodOverride(m_methods, EgressDMACIsSupported);
        mMethodOverride(m_methods, EgressVlanIsSupported);
        mMethodOverride(m_methods, GenerateIngressVlanDescriptor);
        mMethodOverride(m_methods, GenerateEgressVlanDescriptor);
        mMethodOverride(m_methods, MaxNumIngressVlans);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthFlowTestRunner);
    }

AtChannelTestRunner AtEthFlowTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtEthFlowTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtEthFlowTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtEthFlowTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

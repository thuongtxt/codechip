/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtModuleEthTestRunnerInternal.h
 * 
 * Created Date: Jun 19, 2014
 *
 * Description : ETH module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEETHTESTRUNNERINTERNAL_H_
#define _ATMODULEETHTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtEthPort.h"
#include "AtEthFlow.h"
#include "AtModuleEthTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleEthTestRunnerMethods
    {
    void (*TestEthFlow)(AtModuleEthTestRunner self);
    void (*TestEthPort)(AtModuleEthTestRunner self);
    AtChannelTestRunner (*CreateEthPortTestRunner)(AtModuleEthTestRunner self, AtEthPort port);
    AtChannelTestRunner (*CreateEthFlowTestRunner)(AtModuleEthTestRunner self, AtEthFlow flow);
    eBool (*DynamicFlowManagement)(AtModuleEthTestRunner self);
    eBool (*TxIpgIsSupported)(AtModuleEthTestRunner self, AtEthPort port);
    eBool (*RxIpgIsSupported)(AtModuleEthTestRunner self, AtEthPort port);
    eBool (*CanChangeFlowControlParameters)(AtModuleEthTestRunner self);
    eBool (*FlowControlIsSupported)(AtModuleEthTestRunner self);
    eBool (*ShouldTestEthPort)(AtModuleEthTestRunner self);
    eBool (*EthPortHasMac)(AtModuleEthTestRunner self, AtEthPort port);
    eBool (*EthPortHasMacChecking)(AtModuleEthTestRunner self, AtEthPort port);
    uint32 (*NumEthPorts)(AtModuleEthTestRunner self);
    }tAtModuleEthTestRunnerMethods;

typedef struct tAtModuleEthTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModuleEthTestRunnerMethods *methods;
    }tAtModuleEthTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleEthTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#endif /* _ATMODULEETHTESTRUNNERINTERNAL_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtEthPortTestRunnerInternal.h
 * 
 * Created Date: Jul 5, 2014
 *
 * Description : ETH Port test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPORTTESTRUNNERINTERNAL_H_
#define _ATETHPORTTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEthPortTestRunner * AtEthPortTestRunner;

typedef struct tAtEthPortTestRunner
    {
    tAtChannelTestRunner super;
    }tAtEthPortTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtEthPortTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

#endif /* _ATETHPORTTESTRUNNERINTERNAL_H_ */


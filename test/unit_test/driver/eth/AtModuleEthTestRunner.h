/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtModuleEthTestRunner.h
 * 
 * Created Date: Mar 5, 2015
 *
 * Description : ETH test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEETHTESTRUNNER_H_
#define _ATMODULEETHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunner.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleEthTestRunner * AtModuleEthTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AtModuleEthTestRunnerTxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port);
eBool AtModuleEthTestRunnerRxIpgIsSupported(AtModuleEthTestRunner self, AtEthPort port);
eBool AtModuleEthTestRunnerCanChangeFlowControlParameters(AtModuleEthTestRunner self);
eBool AtModuleEthTestRunnerFlowControlIsSupported(AtModuleEthTestRunner self);
eBool AtModuleEthTestRunnerEthPortHasMac(AtModuleEthTestRunner self, AtEthPort port);
eBool AtModuleEthTestRunnerEthPortHasMacChecking(AtModuleEthTestRunner self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEETHTESTRUNNER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModulePppTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PPP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModulePpp.h"
#include "AtHdlcChannel.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "../man/module_runner/AtModuleTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define numLinksToTest 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtModulePppTestRunner * AtModulePppTestRunner;

typedef struct tAtModulePppTestRunner
    {
    tAtModuleTestRunner super;
    }tAtModulePppTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePpp Module()
    {
    return (AtModulePpp)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    AtModuleInit((AtModule)Module());
    }

static eBool CanTestBundle(AtModulePppTestRunner self)
    {
    return AtModulePppMaxBundlesGet((AtModulePpp)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self)) > 0;
    }

static void testCannotCreateOutOfRangeBundle()
    {
    TEST_ASSERT_NULL(AtModulePppMpBundleCreate(Module(), AtModulePppMaxBundlesGet(Module())));
    }

static void testCannotCreateBundleForNULLModule()
    {
    TEST_ASSERT_NULL(AtModulePppMpBundleCreate(NULL, 0));
    }

static void testCanCreateAValidBundle()
    {
    uint32 i;

    for (i = 0; i < AtModulePppMaxBundlesGet(Module()); i++)
        {
        AtMpBundle bundle = AtModulePppMpBundleCreate(Module(), i);
        TEST_ASSERT_NOT_NULL(bundle);
        TEST_ASSERT(AtModulePppMpBundleGet(Module(), i) == bundle);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)bundle));
        }
    }

static void testCannotCreateBundleMoreThanOneTimes()
    {
    uint32 i;

    for (i = 0; i < AtModulePppMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_NOT_NULL(AtModulePppMpBundleCreate(Module(), i));
        TEST_ASSERT_NULL(AtModulePppMpBundleCreate(Module(), i));
        }
    }

static void testCannotGetABundleIfItHasNotBeenCreated()
    {
    uint32 i;

    for (i = 0; i < AtModulePppMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_NULL(AtModulePppMpBundleGet(Module(), i));
        }
    }

static void testCannotGetOutOfRangeBundle()
    {
    TEST_ASSERT_NULL(AtModulePppMpBundleGet(Module(), AtModulePppMaxBundlesGet(Module())));
    }

static void testCannotGetABundleOfANULLModule()
    {
    TEST_ASSERT_NULL(AtModulePppMpBundleGet(NULL, 0));
    }

static void testNextFreeBundleMustBeValid()
    {
    uint32 freeBundle = AtModulePppFreeMpBundleGet(Module());
    TEST_ASSERT(freeBundle < AtModulePppMaxBundlesGet(Module()));
    TEST_ASSERT_NOT_NULL(AtModulePppMpBundleCreate(Module(), freeBundle));
    TEST_ASSERT(AtModulePppFreeMpBundleGet(Module()) != freeBundle);
    TEST_ASSERT(AtModulePppFreeMpBundleGet(Module()) < AtModulePppMaxBundlesGet(Module()));
    }

static void testAfterCreatingAllBundles_nextFreeBundleMustBeInvalid()
    {
    uint32 i;

    for (i = 0; i < AtModulePppMaxBundlesGet(Module()); i++)
        AtModulePppMpBundleCreate(Module(), i);

    TEST_ASSERT(AtModulePppFreeMpBundleGet(Module()) >= AtModulePppMaxBundlesGet(Module()));
    }

static void testNextFreeBundleOfANULLModuleMustBeZero()
    {
    TEST_ASSERT_EQUAL_INT(0, AtModulePppFreeMpBundleGet(NULL));
    }

static void testCanDeleteACreatedBundle()
    {
    uint32 i;

    for (i = 0; i < AtModulePppMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_NOT_NULL(AtModulePppMpBundleCreate(Module(), i));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePppMpBundleDelete(Module(), i));
        TEST_ASSERT_NULL(AtModulePppMpBundleGet(Module(), i));
        }
    }

static void testCanDeleteABundleThatHasNotBeenCreated()
    {
    uint32 i;

    for (i = 0; i < AtModulePppMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePppMpBundleDelete(Module(), i));
        }
    }

static void testCannotDeleteAnOutOfRangeBundle()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModulePppMpBundleDelete(Module(), AtModulePppMaxBundlesGet(Module())));
    }

static void testCannotDeleteABundleOfNULLModule()
    {
    TEST_ASSERT(AtModulePppMpBundleDelete(NULL, 0) != cAtOk);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModulePppBundle)
        {
        new_TestFixture("testCannotCreateOutOfRangeBundle", testCannotCreateOutOfRangeBundle),
        new_TestFixture("testCannotCreateBundleForNULLModule", testCannotCreateBundleForNULLModule),
        new_TestFixture("testCanCreateAValidBundle", testCanCreateAValidBundle),
        new_TestFixture("testCannotCreateBundleMoreThanOneTimes", testCannotCreateBundleMoreThanOneTimes),
        new_TestFixture("testCannotGetABundleIfItHasNotBeenCreated", testCannotGetABundleIfItHasNotBeenCreated),
        new_TestFixture("testCannotGetOutOfRangeBundle", testCannotGetOutOfRangeBundle),
        new_TestFixture("testCannotGetABundleOfANULLModule", testCannotGetABundleOfANULLModule),
        new_TestFixture("testNextFreeBundleMustBeValid", testNextFreeBundleMustBeValid),
        new_TestFixture("testAfterCreatingAllBundles_nextFreeBundleMustBeInvalid", testAfterCreatingAllBundles_nextFreeBundleMustBeInvalid),
        new_TestFixture("testNextFreeBundleOfANULLModuleMustBeZero", testNextFreeBundleOfANULLModuleMustBeZero),
        new_TestFixture("testCanDeleteACreatedBundle", testCanDeleteACreatedBundle),
        new_TestFixture("testCanDeleteABundleThatHasNotBeenCreated", testCanDeleteABundleThatHasNotBeenCreated),
        new_TestFixture("testCannotDeleteAnOutOfRangeBundle", testCannotDeleteAnOutOfRangeBundle),
        new_TestFixture("testCannotDeleteABundleOfNULLModule", testCannotDeleteABundleOfNULLModule)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModulePppBundle_Caller, "TestSuite_ModulePppBundle", setUp, NULL, TestSuite_ModulePppBundle);

    return (TestRef)((void *)&TestSuite_ModulePppBundle_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    if (CanTestBundle((AtModulePppTestRunner)self))
        AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));

    return suites;
    }

static AtChannelTestRunner CreateMpBundleTestRunner(AtModulePppTestRunner self, AtChannel bundle)
    {
    return AtMpBundleTestRunnerNew(bundle, (AtModuleTestRunner)self);
    }

static AtChannelTestRunner CreatePppLinkTestRunner(AtModulePppTestRunner self, AtChannel pppLink)
    {
    return AtPppLinkTestRunnerNew(pppLink, (AtModuleTestRunner)self);
    }

static AtHdlcLink *HdlcPppLinkToTest(AtModulePppTestRunner self, uint16 *numPppLink)
    {
    uint8  wIndex;
    static AtHdlcChannel hdlcChannel[numLinksToTest];
    static AtHdlcLink hdlcLinks[numLinksToTest];
    AtModuleEncap moduleEncap;

    /* create encap hdlcppp link */
    moduleEncap = (AtModuleEncap)AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self), cAtModuleEncap);
    for (wIndex = 0; wIndex < numLinksToTest; wIndex++)
        {
        hdlcChannel[wIndex] = (AtHdlcChannel)AtModuleEncapChannelGet(moduleEncap, wIndex);
        if (hdlcChannel[wIndex] == NULL)
            hdlcChannel[wIndex] = AtModuleEncapHdlcPppChannelCreate(moduleEncap, wIndex);
        }

    /* get Hdlc ppplink */
    for (wIndex = 0; wIndex < numLinksToTest; wIndex++)
        hdlcLinks[wIndex] = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)hdlcChannel[wIndex]);

    *numPppLink = numLinksToTest;
    return hdlcLinks;
    }

static void TestBundle(AtModulePppTestRunner self)
    {
    uint16 maxBundle;
    uint16 wIndex;
    uint16 numPppLink;
    AtHdlcLink *links;
    AtModulePpp pppModule = (AtModulePpp)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);

    if (!CanTestBundle(self))
        return;

    AtAssert(AtModuleInit((AtModule)pppModule) == cAtOk);

    links = HdlcPppLinkToTest(self, &numPppLink);
    maxBundle = AtModulePppMaxBundlesGet(pppModule);
    for (wIndex = 0; wIndex < maxBundle; wIndex++)
        {
        AtChannel hdlcBundle = (AtChannel)AtModulePppMpBundleCreate(pppModule, wIndex);
        AtUnittestRunner runner = (AtUnittestRunner)CreateMpBundleTestRunner(self, hdlcBundle);
        AtChannelTestRunnerOtherChannelsSet((AtChannelTestRunner)runner, (AtChannel *)links, numPppLink);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtModulePppMpBundleDelete(pppModule, wIndex);
        }
    }

static void TestPppLink(AtModulePppTestRunner self)
    {
    uint32 i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self), cAtModuleEncap);

    AtModuleInit((AtModule)encapModule);

    for (i = 0; i < AtModuleEncapMaxChannelsGet(encapModule); i++)
        {
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtModuleEncapHdlcPppChannelCreate(encapModule, i);
        AtChannel link = (AtChannel)AtHdlcChannelHdlcLinkGet(hdlcChannel);
        AtUnittestRunner runner = (AtUnittestRunner)CreatePppLinkTestRunner(self, link);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtModuleEncapChannelDelete(encapModule, i);
        }
    }

static void Run(AtUnittestRunner self)
    {
    AtModulePppTestRunner runner = (AtModulePppTestRunner)self;

    m_AtUnittestRunnerMethods->Run(self);

    TestPppLink(runner);
    TestBundle(runner);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePppTestRunner);
    }

AtModuleTestRunner AtModulePppTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModulePppTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModulePppTestRunnerObjectInit(newRunner, module);
    }

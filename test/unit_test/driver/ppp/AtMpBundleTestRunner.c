/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtMpBundleTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : PPP Bundle test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtMpBundle.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtMpBundleTestRunner
    {
    tAtChannelTestRunner super;
    }tAtMpBundleTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static AtEthFlow *m_flows = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtMpBundle MpBundle()
    {
    return (AtMpBundle)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void tearDown()
    {
    AtEthFlow  flow;
    AtIterator flowIterator;

    /* Remove all flows that are added to bundle */
    flowIterator = AtMpBundleFlowIteratorCreate(MpBundle());
    while ((flow = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
        AtIteratorRemove(flowIterator);

    AtObjectDelete((AtObject)flowIterator);
    }

static eBool SequenceModeIsValid(eAtMpSequenceMode sequence)
    {
    eAtMpSequenceMode pSequence[] = {cAtMpSequenceModeLong, cAtMpSequenceModeShort};
    uint8 numSeq, wIndex;

    numSeq = mCount(pSequence);

    for (wIndex = 0; wIndex < numSeq; wIndex++)
        {
        if (sequence == pSequence[wIndex])
            return cAtTrue;
        }

    return cAtFalse;
    }

static void testSequenceModeMustBeValidAsDefault()
    {
    TEST_ASSERT(SequenceModeIsValid(AtMpBundleRxSequenceModeGet(MpBundle())));
    TEST_ASSERT(SequenceModeIsValid(AtMpBundleTxSequenceModeGet(MpBundle())));
    }

static void testChangeSeqMode()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleTxSequenceModeSet(MpBundle(), cAtMpSequenceModeShort));
    TEST_ASSERT_EQUAL_INT(cAtMpSequenceModeShort, AtMpBundleTxSequenceModeGet(MpBundle()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleRxSequenceModeSet(MpBundle(), cAtMpSequenceModeShort));
    TEST_ASSERT_EQUAL_INT(cAtMpSequenceModeShort, AtMpBundleRxSequenceModeGet(MpBundle()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleTxSequenceModeSet(MpBundle(), cAtMpSequenceModeLong));
    TEST_ASSERT_EQUAL_INT(cAtMpSequenceModeLong, AtMpBundleTxSequenceModeGet(MpBundle()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleRxSequenceModeSet(MpBundle(), cAtMpSequenceModeLong));
    TEST_ASSERT_EQUAL_INT(cAtMpSequenceModeLong, AtMpBundleRxSequenceModeGet(MpBundle()));
    }

static void testCannotChangeInvalidSeqMode()
    {
    TEST_ASSERT(AtMpBundleTxSequenceModeSet(MpBundle(), 0xff) != cAtOk);
    TEST_ASSERT(AtMpBundleRxSequenceModeSet(MpBundle(), 0xff) != cAtOk);
    }

static eBool WorkingModeIsValid(eAtMpWorkingMode sequence)
    {
    eAtMpWorkingMode pWorkingMode[] = {cAtMpWorkingModeLfi, cAtMpWorkingModeMcml};
    uint8 numWorkMod, wIndex;

    numWorkMod = mCount(pWorkingMode);

    for (wIndex = 0; wIndex < numWorkMod; wIndex++)
        {
        if (sequence == pWorkingMode[wIndex])
            return cAtTrue;
        }

    return cAtFalse;
    }

static void testWorkingModeMustBeValidAsDefault()
    {
    TEST_ASSERT(WorkingModeIsValid(AtMpBundleWorkingModeGet(MpBundle())));
    }

static void testCannotChangeInvalidWorkingMode()
    {
    TEST_ASSERT(AtMpBundleWorkingModeSet(MpBundle(), 0xff) != cAtOk);
    }

static void testMrruMustBeGreaterThan64()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorInvlParm, AtMpBundleMrruSet(MpBundle(), 60));
    }

static AtEthFlow * FlowsForTesting(AtMpBundle self, uint8 *numFlows)
    {
    uint16      wIndex;
    AtModuleEth moduleEth;
    AtDevice device = AtChannelDeviceGet((AtChannel)MpBundle());

    /* Allocate memory to hold all flows that are used to test */
    *numFlows = 10;
    if (m_flows == NULL)
        m_flows = AtOsalMemAlloc(sizeof(AtEthFlow) * (*numFlows));

    /* Ask Ethernet module to create a number of flows */
    moduleEth = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    for (wIndex = 0; wIndex < (*numFlows); wIndex++)
        {
        /* Try to get flows first */
        m_flows[wIndex] = AtModuleEthFlowGet(moduleEth, wIndex);
        AtChannelInit((AtChannel)m_flows[wIndex]);
        if (m_flows[wIndex] == NULL)
            {
            m_flows[wIndex] = AtModuleEthNopFlowCreate(moduleEth, wIndex);
            AtChannelInit((AtChannel)m_flows[wIndex]);
            }

        /* Cannot find or create new one */
        if (m_flows[wIndex] == NULL)
            return NULL;
        }

    return m_flows;
    }

static void testAddFlowToClass0()
    {
    uint8     numberFlow;
    AtEthFlow  *flows;

    flows = FlowsForTesting(MpBundle(), &numberFlow);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleFlowAdd(MpBundle(), flows[0], 0));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleFlowRemove(MpBundle(), flows[0]));
    }

static void testAddFlowToClassToOtherClassFromClass0()
    {
    AtEthFlow *flows;
    uint8 numberFlow;

    flows = FlowsForTesting(MpBundle(), &numberFlow);

    /* add bigger class to flow */
    TEST_ASSERT(AtMpBundleFlowAdd(MpBundle(), flows[0], numberFlow + 7) !=  cAtOk);
    }

static void testRemoveFlowInBundle()
    {
    AtEthFlow  *flows;
    uint8      numberFlow;

    flows = FlowsForTesting(MpBundle(), &numberFlow);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleFlowAdd(MpBundle(), flows[0], 1));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtMpBundleFlowRemove(MpBundle(), flows[0]));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_MpBundle_Fixtures)
        {
        new_TestFixture("testSequenceModeMustBeValidAsDefault", testSequenceModeMustBeValidAsDefault),
        new_TestFixture("testChangeSeqMode", testChangeSeqMode),
        new_TestFixture("testCannotChangeInvalidSeqMode", testCannotChangeInvalidSeqMode),
        new_TestFixture("testWorkingModeMustBeValidAsDefault", testWorkingModeMustBeValidAsDefault),
        new_TestFixture("testCannotChangeInvalidWorkingMode", testCannotChangeInvalidWorkingMode),
        new_TestFixture("testMrruMustBeGreaterThan64", testMrruMustBeGreaterThan64),
        new_TestFixture("testAddFlowToClass0", testAddFlowToClass0),
        new_TestFixture("testAddFlowToClassmToOtherClass", testAddFlowToClassToOtherClassFromClass0),
        new_TestFixture("testRemoveFlowInBundle", testRemoveFlowInBundle),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_MpBundle_Caller, "TestSuite_MpBundle", NULL, tearDown, TestSuite_MpBundle_Fixtures);

    return (TestRef)((void *)&TestSuite_MpBundle_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);

    AtOsalMemFree(m_flows);
    m_flows = NULL;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMpBundleTestRunner);
    }

AtChannelTestRunner AtMpBundleTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtMpBundleTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtMpBundleTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

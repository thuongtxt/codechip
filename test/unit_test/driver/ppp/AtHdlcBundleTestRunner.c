/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtHdlcBundleTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : PPP Bundle test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtHdlcBundle.h"
#include "AtHdlcLink.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHdlcBundleTestRunner
    {
    tAtChannelTestRunner super;
    }tAtHdlcBundleTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHdlcBundle HdlcBundle()
    {
    return (AtHdlcBundle)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void RemoveAllLinksFromBundle(AtHdlcBundle bundle)
    {
    while (AtHdlcBundleNumberOfLinksGet(bundle) > 0)
        {
        AtHdlcLink link = AtHdlcBundleLinkGet(bundle, 0);
        AtHdlcBundleLinkRemove(bundle, link);
        }
    }

static void tearDown()
    {
    RemoveAllLinksFromBundle(HdlcBundle());
    }

static void testChangeMaxLinkDelay()
    {
    /* set maxlinkdelay */
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleMaxDelaySet(HdlcBundle(), 100));
    TEST_ASSERT_EQUAL_INT(100, AtHdlcBundleMaxDelayGet(HdlcBundle()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleMaxDelaySet(HdlcBundle(), 120));
    TEST_ASSERT_EQUAL_INT(120, AtHdlcBundleMaxDelayGet(HdlcBundle()));

    /* set minimum maxlink delay */
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, (eAtRet)AtHdlcBundleMaxDelaySet(HdlcBundle(), 5));

    /* set maximum maxlink delay */
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, (eAtRet)AtHdlcBundleMaxDelaySet(HdlcBundle(), 250));
    }

static eBool FragmentSizeIsValid(eAtFragmentSize fragmentSize)
    {
    eAtFragmentSize pFragmentSize[] = {cAtNoFragment, cAtFragmentSize128, cAtFragmentSize256, cAtFragmentSize512};
    uint8 numFragment, wIndex;

    numFragment = mCount(pFragmentSize);
    for (wIndex = 0; wIndex < numFragment; wIndex++)
        {
        if (fragmentSize == pFragmentSize[wIndex])
            return cAtTrue;
        }

    return cAtFalse;
    }

static void testFragmentSizeIsValidAsDefault()
    {
    TEST_ASSERT(FragmentSizeIsValid(AtHdlcBundleFragmentSizeGet(HdlcBundle())));
    }

static void testChangeFragmentSize()
    {
    /* set fragmentsize */
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleFragmentSizeSet(HdlcBundle(), cAtFragmentSize128));
    TEST_ASSERT_EQUAL_INT(cAtFragmentSize128, AtHdlcBundleFragmentSizeGet(HdlcBundle()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleFragmentSizeSet(HdlcBundle(), cAtFragmentSize256));
    TEST_ASSERT_EQUAL_INT(cAtFragmentSize256, AtHdlcBundleFragmentSizeGet(HdlcBundle()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleFragmentSizeSet(HdlcBundle(), cAtFragmentSize512));
    TEST_ASSERT_EQUAL_INT(cAtFragmentSize512, AtHdlcBundleFragmentSizeGet(HdlcBundle()));
    }

static void testCannotChangeFragmentSizeInValid()
    {
    TEST_ASSERT((eAtRet)AtHdlcBundleFragmentSizeSet(HdlcBundle(), 0xff) != cAtOk);
    }

static void testAddPppLinkInBundle()
    {
    uint16  i;
    uint32 numOtherChannels;
    AtHdlcLink *otherChannels = (AtHdlcLink *)AtChannelTestRunnerOtherChannelsGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner(), &numOtherChannels);

    for (i = 0; i < numOtherChannels; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleLinkAdd(HdlcBundle(), otherChannels[i]));
        TEST_ASSERT(AtHdlcLinkBundleGet(otherChannels[i]) == HdlcBundle());
        }

    /* Remove link from bundle */
    for (i = 0; i < numOtherChannels; i++)
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleLinkRemove(HdlcBundle(), otherChannels[i]));
    }

static void Nothing()
    {

    }

static void testRemoveLinkInBundle()
    {
    uint16  wIndex;
    uint32 numOtherChannels;
    AtHdlcLink *otherChannels = (AtHdlcLink *)AtChannelTestRunnerOtherChannelsGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner(), &numOtherChannels);

    /* Add link to bundle */
    for (wIndex = 0; wIndex < numOtherChannels - 1; wIndex++)
        {
        eAtRet ret = AtHdlcBundleLinkAdd(HdlcBundle(), otherChannels[wIndex]);
        if (ret != cAtOk)
            Nothing();
        TEST_ASSERT_EQUAL_INT(cAtOk, ret);
        }

    /* Remove link from bundle */
    for (wIndex = 0; wIndex < numOtherChannels - 1; wIndex++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcBundleLinkRemove(HdlcBundle(), otherChannels[wIndex]));
        TEST_ASSERT(AtHdlcLinkBundleGet(otherChannels[wIndex]) == NULL);
        }

    /* Remove not belong bundle  */
    TEST_ASSERT((eAtRet)AtHdlcBundleLinkRemove(HdlcBundle(), otherChannels[numOtherChannels - 1]) != cAtOk);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_HdlcBundle_Fixtures)
        {
        new_TestFixture("testChangeMaxLinkDelay", testChangeMaxLinkDelay),
        new_TestFixture("testFragmentSizeIsValidAsDefault", testFragmentSizeIsValidAsDefault),
        new_TestFixture("testChangeFragmentSize", testChangeFragmentSize),
        new_TestFixture("testCannotChangeFragmentSizeInValid", testCannotChangeFragmentSizeInValid),
        new_TestFixture("testRemoveLinkInBundle", testRemoveLinkInBundle),
        new_TestFixture("testAddPppLinkInBundle", testAddPppLinkInBundle),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_HdlcBundle_Caller, "TestSuite_HdlcBundle", NULL, tearDown, TestSuite_HdlcBundle_Fixtures);

    return (TestRef)((void *)&TestSuite_HdlcBundle_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHdlcBundleTestRunner);
    }

AtChannelTestRunner AtHdlcBundleTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtHdlcBundleTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtHdlcBundleTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

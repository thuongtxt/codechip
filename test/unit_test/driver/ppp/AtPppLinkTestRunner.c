/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtPppLinkTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : PPP Bundle test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModuleEncap.h"
#include "AtHdlcChannel.h"
#include "AtPppLink.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPppLinkTestRunner
    {
    tAtChannelTestRunner super;
    }tAtPppLinkTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPppLink Link()
    {
    return (AtPppLink)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testChangeLinkPhase()
    {
    eAtPppLinkPhase allPhases[] = {cAtPppLinkPhaseDead,
                                   cAtPppLinkPhaseEstablish,
                                   cAtPppLinkPhaseEnterNetwork,
                                   cAtPppLinkPhaseNetworkActive};
    eAtPppLinkPhase currentPhase, nextPhase;
    uint8 currentPhase_i, nextPhase_i;

    /* Make transition from one phase to all other phases */
    for (currentPhase_i = 0; currentPhase_i < mCount(allPhases); currentPhase_i++)
        {
        for (nextPhase_i = 0; nextPhase_i < mCount(allPhases); nextPhase_i++)
            {
            /* Set current phase */
            currentPhase = allPhases[currentPhase_i];
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPppLinkPhaseSet(Link(), currentPhase));
            TEST_ASSERT_EQUAL_INT(currentPhase, AtPppLinkPhaseGet(Link()));

            /* Move to next phase */
            nextPhase = allPhases[nextPhase_i];
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPppLinkPhaseSet(Link(), nextPhase));
            TEST_ASSERT_EQUAL_INT(nextPhase, AtPppLinkPhaseGet(Link()));
            }
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_PppLink)
        {
        new_TestFixture("testChangeLinkPhase", testChangeLinkPhase),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_PppLink_Caller, "TestSuite_PppLink", NULL, NULL, TestSuite_PppLink);

    return (TestRef)((void *)&TestSuite_PppLink_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPppLinkTestRunner);
    }

AtChannelTestRunner AtPppLinkTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPppLinkTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPppLinkTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

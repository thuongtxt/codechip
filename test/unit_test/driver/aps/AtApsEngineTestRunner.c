/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsEngineTestRunner.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Testcases for APS Engine Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtApsEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtApsEngineTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtApsEngineTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtApsEngine TestedEngine()
    {
    return (AtApsEngine)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testCanInitializeEngine()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineInit(TestedEngine()));
    }

static void testCanNotInitializeNullEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtApsEngineInit(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testIdOfNullEngineMustBeInvalid()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cInvalidUint32, AtApsEngineIdGet(NULL))
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanStartEngine()
    {
    if (!AtApsEngineCanStart(TestedEngine()))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStart(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtApsEngineStateStart, AtApsEngineStateGet(TestedEngine()));
    }

static void testCanReStartEngine()
    {
    if (!AtApsEngineCanStart(TestedEngine()))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStart(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStart(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtApsEngineStateStart, AtApsEngineStateGet(TestedEngine()));
    }

static void testCanNotStartNullEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsEngineStart(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanStopEngine()
    {
    if (AtApsEngineCanBeStopped((AtApsEngineTestRunner)AtUnittestRunnerCurrentRunner()) == cAtFalse)
        return;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStop(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtApsEngineStateStop, AtApsEngineStateGet(TestedEngine()));
    }

static void testCanStopingEngineHasBeenStopped()
    {
    if (AtApsEngineCanBeStopped((AtApsEngineTestRunner)AtUnittestRunnerCurrentRunner()) == cAtFalse)
        return;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStop(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStop(TestedEngine()));
    }

static void testCanNotStopNullEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsEngineStop(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanConfigureSwitchingType()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineSwitchTypeSet(TestedEngine(), cAtApsSwitchTypeRev));
    TEST_ASSERT_EQUAL_INT(cAtApsSwitchTypeRev, AtApsEngineSwitchTypeGet(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineSwitchTypeSet(TestedEngine(), cAtApsSwitchTypeNonRev));
    TEST_ASSERT_EQUAL_INT(cAtApsSwitchTypeNonRev, AtApsEngineSwitchTypeGet(TestedEngine()));
    }

static void testCanNotConfigureSwitchingTypeOfNullEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsEngineSwitchTypeSet(NULL, cAtApsSwitchTypeRev));
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsEngineSwitchTypeSet(NULL, cAtApsSwitchTypeNonRev));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanNotConfigureInvalidSwitchingTypeOfValidEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorInvlParm, AtApsEngineSwitchTypeSet(TestedEngine(), cInvalidUint32));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanConfigureSwitchingCondition()
    {
    AtApsEngineTestRunner self = (AtApsEngineTestRunner)AtUnittestRunnerCurrentRunner();
    AtApsEngine engine = TestedEngine();
    uint32 defects = mMethodsGet(self)->ApplicableDefects(self);

    AtApsEngineSwitchingConditionSet(engine, defects, cAtApsSwitchingConditionSd);
    TEST_ASSERT_EQUAL_INT(cAtApsSwitchingConditionSd, AtApsEngineSwitchingConditionGet(engine, defects));
    AtApsEngineSwitchingConditionSet(engine, defects, cAtApsSwitchingConditionSf);
    TEST_ASSERT_EQUAL_INT(cAtApsSwitchingConditionSf, AtApsEngineSwitchingConditionGet(engine, defects));
    }

static void testCanNotConfigureSwitchingConditionOfNullEngine()
    {
    AtApsEngineTestRunner self = (AtApsEngineTestRunner)AtUnittestRunnerCurrentRunner();
    uint32 defects = mMethodsGet(self)->ApplicableDefects(self);
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsEngineSwitchingConditionSet(NULL, defects, cAtApsSwitchingConditionSd));
    AtTestLoggerEnable(cAtFalse);
    }

static void testCanAccessRequestState()
    {
    AtApsEngineRequestStateGet(TestedEngine());
    }

static void testRequestStateOfNullEngineMustBeUnknown()
    {
    TEST_ASSERT_EQUAL_INT(cAtApsRequestStateUnknown, AtApsEngineRequestStateGet(NULL));
    }

static void testCanChangeWtr()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineWtrSet(TestedEngine(), 60));
    TEST_ASSERT_EQUAL_INT(60, AtApsEngineWtrGet(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineWtrSet(TestedEngine(), 120));
    TEST_ASSERT_EQUAL_INT(120, AtApsEngineWtrGet(TestedEngine()));
    }

static void testCanNotChangeWtrOfNullEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtApsEngineWtrSet(NULL, 5));
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ApsEngine_Fixtures)
        {
        new_TestFixture("testCanInitializeEngine", testCanInitializeEngine),
        new_TestFixture("testCanNotInitializeNullEngine", testCanNotInitializeNullEngine),
        new_TestFixture("testIdOfNullEngineMustBeInvalid", testIdOfNullEngineMustBeInvalid),
        new_TestFixture("testCanStartEngine", testCanStartEngine),
        new_TestFixture("testCanReStartEngine", testCanReStartEngine),
        new_TestFixture("testCanNotStartNullEngine", testCanNotStartNullEngine),
        new_TestFixture("testCanStopEngine", testCanStopEngine),
        new_TestFixture("testCanStopingEngineHasBeenStopped", testCanStopingEngineHasBeenStopped),
        new_TestFixture("testCanNotStopNullEngine", testCanNotStopNullEngine),
        new_TestFixture("testCanConfigureSwitchingType", testCanConfigureSwitchingType),
        new_TestFixture("testCanNotConfigureSwitchingTypeOfNullEngine", testCanNotConfigureSwitchingTypeOfNullEngine),
        new_TestFixture("testCanNotConfigureInvalidSwitchingTypeOfValidEngine", testCanNotConfigureInvalidSwitchingTypeOfValidEngine),
        new_TestFixture("testCanConfigureSwitchingCondition", testCanConfigureSwitchingCondition),
        new_TestFixture("testCanNotConfigureSwitchingConditionOfNullEngine", testCanNotConfigureSwitchingConditionOfNullEngine),
        new_TestFixture("testCanAccessRequestState", testCanAccessRequestState),
        new_TestFixture("testRequestStateOfNullEngineMustBeUnknown", testRequestStateOfNullEngineMustBeUnknown),
        new_TestFixture("testCanChangeWtr", testCanChangeWtr),
        new_TestFixture("testCanNotChangeWtrOfNullEngine", testCanNotChangeWtrOfNullEngine)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ApsEngine_Caller, "TestSuite_ApsEngine", NULL, NULL, TestSuite_ApsEngine_Fixtures);

    return (TestRef)((void *)&TestSuite_ApsEngine_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static uint32 ApplicableDefects(AtApsEngineTestRunner self)
    {
    /* To enforce sub class override this */
    AtAssert(0);
    return 0;
    }

static atbool CanEngineBeStopped(AtApsEngineTestRunner self)
    {
    /* To enforce sub class override this */
    AtAssert(0);
    return cAtTrue;
    }

static void OverrideAtUnittestRunner(AtApsEngineTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void MethodsInit(AtApsEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ApplicableDefects);
        mMethodOverride(m_methods, CanEngineBeStopped);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtApsEngineTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsEngineTestRunner);
    }

AtApsEngineTestRunner AtApsEngineTestRunnerObjectInit(AtApsEngineTestRunner self, AtApsEngine engine, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit((AtChannelTestRunner)self, (AtChannel)engine, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

atbool AtApsEngineCanBeStopped(AtApsEngineTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->CanEngineBeStopped(self);

    return cAtFalse;
    }

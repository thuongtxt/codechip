/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsGroupTestRunner.h
 * 
 * Created Date: Aug 9, 2016
 *
 * Description : APS group test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSGROUPTESTRUNNER_H_
#define ATAPSGROUPTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsGroup.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsGroupTestRunner * AtApsGroupTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsGroupTestRunner AtApsGroupTestRunnerNew(AtApsGroup group, AtApsSelector *selectors);

#ifdef __cplusplus
}
#endif
#endif /* ATAPSGROUPTESTRUNNER_H_ */


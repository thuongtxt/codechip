/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtModuleApsTestRunnerInternal.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : APS Module Internal Test Runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATMODULEAPSTESTRUNNERINTERNAL_H_
#define ATMODULEAPSTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtModuleApsTestRunner.h"
#include "AtApsEngineTestRunner.h"
#include "AtApsLinearEngine.h"
#include "AtApsUpsrEngine.h"
#include "AtApsGroupTestRunner.h"
#include "AtApsSelectorTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleApsTestRunnerMethods
    {
    void (*TestUpsrEngines)(AtModuleApsTestRunner self);
    void (*TestApsGroups)(AtModuleApsTestRunner self);
    void (*TestApsSelectors)(AtModuleApsTestRunner self);
    void (*PathSetup)(AtModuleApsTestRunner self, AtSdhPath paths[6]);
    void (*SelectorSetup)(AtModuleApsTestRunner self, AtApsSelector selectors[2]);
    AtApsEngineTestRunner (*CreateUpsrEngineTestRunner)(AtModuleApsTestRunner self, AtApsEngine port);
    AtApsGroupTestRunner (*CreateApsGroupTestRunner)(AtModuleApsTestRunner self, AtApsGroup group);
    AtApsSelectorTestRunner (*CreateApsSelectorTestRunner)(AtModuleApsTestRunner self, AtApsSelector selector);
    uint32 (*MaxNumUpsrEngines)(AtModuleApsTestRunner self);
    uint32 (*MaxNumLinearEngines)(AtModuleApsTestRunner self);
    uint32 (*MaxNumApsGroups)(AtModuleApsTestRunner self);
    uint32 (*MaxNumApsSelectors)(AtModuleApsTestRunner self);
    }tAtModuleApsTestRunnerMethods;

typedef struct tAtModuleApsTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModuleApsTestRunnerMethods *methods;
    }tAtModuleApsTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleApsTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* ATMODULEAPSTESTRUNNERINTERNAL_H_ */


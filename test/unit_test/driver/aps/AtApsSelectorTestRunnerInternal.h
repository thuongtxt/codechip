/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsSelectorTestRunnerInternal.h
 * 
 * Created Date: Aug 9, 2016
 *
 * Description : TODO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSSELECTORTESTRUNNERINTERNAL_H_
#define ATAPSSELECTORTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsSelector.h"
#include "../../driver/man/AtObjectTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsSelectorTestRunner
    {
    tAtObjectTestRunner super;
    }tAtApsSelectorTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* ATAPSSELECTORTESTRUNNERINTERNAL_H_ */


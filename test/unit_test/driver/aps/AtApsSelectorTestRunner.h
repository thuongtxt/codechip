/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsSelectorTestRunner.h
 * 
 * Created Date: Aug 9, 2016
 *
 * Description : TODO
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSSELECTORTESTRUNNER_H_
#define ATAPSSELECTORTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsSelector.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsSelectorTestRunner * AtApsSelectorTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsSelectorTestRunner AtApsSelectorTestRunnerNew(AtApsSelector group);

#ifdef __cplusplus
}
#endif
#endif /* ATAPSSELECTORTESTRUNNER_H_ */


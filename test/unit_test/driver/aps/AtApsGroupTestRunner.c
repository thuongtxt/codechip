/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsGroupTestRunner.c
 *
 * Created Date: Aug 9, 2016
 *
 * Description : APS group test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtApsGroupTestRunner.h"
#include "AtApsGroupTestRunnerInternal.h"
#include "AtApsSelector.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtApsGroupTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtApsGroup ApsGroupGet(AtApsGroupTestRunner self)
    {
    return (AtApsGroup)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

static AtApsGroup TestedGroup()
    {
    return (AtApsGroup)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static AtApsSelector ApsSelector1()
    {
    AtApsGroupTestRunner groupTestRunner = (AtApsGroupTestRunner)AtUnittestRunnerCurrentRunner();
    return groupTestRunner->selectors[0];
    }

static AtApsSelector ApsSelector2()
    {
    AtApsGroupTestRunner groupTestRunner = (AtApsGroupTestRunner)AtUnittestRunnerCurrentRunner();
    return groupTestRunner->selectors[1];
    }

static void testCanInitializeApsGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupInit(TestedGroup()));
    }

static void testCanNotInitializeNullGroup()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtApsGroupInit(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testIdOfNullGroupMustBeInvalid()
    {
    TEST_ASSERT_EQUAL_INT(cInvalidUint32, AtApsGroupIdGet(NULL));
    }

static void testCanNotSetStateOfNullGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsGroupStateSet(NULL, cAtApsGroupStateWorking));
    }

static void testCanSetStateOfValidGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupStateSet(TestedGroup(), cAtApsGroupStateWorking));
    TEST_ASSERT_EQUAL_INT(cAtApsGroupStateWorking, AtApsGroupStateGet(TestedGroup()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupStateSet(TestedGroup(), cAtApsGroupStateProtection));
        TEST_ASSERT_EQUAL_INT(cAtApsGroupStateProtection, AtApsGroupStateGet(TestedGroup()));
    }

static void tesStateOfNullGroupIsUnknown()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtApsGroupStateUnknown, AtApsGroupStateGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanAddSelectorToValidGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupSelectorAdd(TestedGroup(), ApsSelector1()));
    TEST_ASSERT_EQUAL_INT(1, AtApsGroupNumSelectors(TestedGroup()));
    }

static void testCanNotAddSelectorToNullGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsGroupSelectorAdd(NULL, ApsSelector1()));
    TEST_ASSERT_EQUAL_INT(1, AtApsGroupNumSelectors(TestedGroup()));
    }

static void testCanAddSelectorToValidGroupWhileTheSelectorIsInTheGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupSelectorAdd(TestedGroup(), ApsSelector1()));
    }

static void testCanNotAddSelectorToValidGroupWhileTheSelectorIsInAnotherGroup()
    {
    /*TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupSelectorAdd(TestedGroup(), ApsSelector1()));*/
    }

static void testCanAddMoreThanOneSelectorToValidGroupIfSupport()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupSelectorAdd(TestedGroup(), ApsSelector2()));
    TEST_ASSERT_EQUAL_INT(2, AtApsGroupNumSelectors(TestedGroup()));
    }

static void testCanGetSelectorFromValidGroupByIndex()
    {
    TEST_ASSERT_NOT_NULL(AtApsGroupSelectorAtIndex(TestedGroup(), 0));
    TEST_ASSERT_NOT_NULL(AtApsGroupSelectorAtIndex(TestedGroup(), 1));
    }

static void testSelectorFromNullGroupMustBeNull()
    {
    TEST_ASSERT_NULL(AtApsGroupSelectorAtIndex(NULL, 0));
    }

static void testCanNotRemoveSelectorFromNullGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsGroupSelectorRemove(NULL, ApsSelector1()));
    }

static void testCanRemoveSelectorFromValidGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupSelectorRemove(TestedGroup(), ApsSelector1()));
    TEST_ASSERT_EQUAL_INT(1, AtApsGroupNumSelectors(TestedGroup()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsGroupSelectorRemove(TestedGroup(), ApsSelector2()));
    TEST_ASSERT_EQUAL_INT(0, AtApsGroupNumSelectors(TestedGroup()));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtApsGroup_Fixtures)
        {
        new_TestFixture("testCanInitializeApsGroup", testCanInitializeApsGroup),
        new_TestFixture("testCanNotInitializeNullGroup", testCanNotInitializeNullGroup),
        new_TestFixture("testIdOfNullGroupMustBeInvalid", testIdOfNullGroupMustBeInvalid),
        new_TestFixture("testCanNotSetStateOfNullGroup", testCanNotSetStateOfNullGroup),
        new_TestFixture("testCanSetStateOfValidGroup", testCanSetStateOfValidGroup),
        new_TestFixture("tesStateOfNullGroupIsUnknown", tesStateOfNullGroupIsUnknown),
        new_TestFixture("testCanAddSelectorToValidGroup", testCanAddSelectorToValidGroup),
        new_TestFixture("testCanNotAddSelectorToNullGroup", testCanNotAddSelectorToNullGroup),
        new_TestFixture("testCanAddSelectorToValidGroupWhileTheSelectorIsInTheGroup", testCanAddSelectorToValidGroupWhileTheSelectorIsInTheGroup),
        new_TestFixture("testCanNotAddSelectorToValidGroupWhileTheSelectorIsInAnotherGroup", testCanNotAddSelectorToValidGroupWhileTheSelectorIsInAnotherGroup),
        new_TestFixture("testCanAddMoreThanOneSelectorToValidGroupIfSupport", testCanAddMoreThanOneSelectorToValidGroupIfSupport),
        new_TestFixture("testCanGetSelectorFromValidGroupByIndex", testCanGetSelectorFromValidGroupByIndex),
        new_TestFixture("testSelectorFromNullGroupMustBeNull", testSelectorFromNullGroupMustBeNull),
        new_TestFixture("testCanNotRemoveSelectorFromNullGroup", testCanNotRemoveSelectorFromNullGroup),
        new_TestFixture("testCanRemoveSelectorFromValidGroup", testCanRemoveSelectorFromValidGroup),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtApsGroup_Caller, "TestSuite_AtApsGroup", NULL, NULL, TestSuite_AtApsGroup_Fixtures);

    return (TestRef)((void *)&TestSuite_AtApsGroup_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtApsGroup group = ApsGroupGet((AtApsGroupTestRunner)self);
    AtSnprintf(buffer, bufferSize, "%s", AtObjectToString((AtObject)group));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtApsGroupTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtApsGroupTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsGroupTestRunner);
    }

static AtApsGroupTestRunner ObjectInit(AtApsGroupTestRunner self, AtApsGroup group, AtApsSelector *selectors)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)group) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->selectors = selectors;

    return self;
    }

AtApsGroupTestRunner AtApsGroupTestRunnerNew(AtApsGroup group, AtApsSelector *selectors)
    {
    AtApsGroupTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, group, selectors);
    }

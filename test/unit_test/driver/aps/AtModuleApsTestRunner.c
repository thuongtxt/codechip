/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtModuleApsTestRunner.c
 *
 * Created Date: Jul 21, 2016
 *
 * Description : APS Module Test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleApsTestRunner.h"
#include "AtModuleApsTestRunnerInternal.h"
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleApsTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleApsTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static AtSdhPath     m_paths[]      = {NULL, NULL, NULL, NULL, NULL, NULL};
static AtApsSelector m_selectors[]  = {NULL, NULL};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleApsTestRunner CurrentRunner()
    {
    return (AtModuleApsTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModuleAps Module()
    {
    return (AtModuleAps)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static AtSdhPath WorkingPath1()
    {
    return m_paths[0];
    }

static AtSdhPath ProtectionPath1()
    {
    return m_paths[1];
    }

static AtSdhPath OutgoingPath1()
    {
    return m_paths[4];
    }

static AtSdhPath WorkingPath2()
    {
    return m_paths[2];
    }

static AtSdhPath ProtectionPath2()
    {
    return m_paths[3];
    }

static AtSdhPath OutgoingPath2()
    {
    return m_paths[5];
    }

static void testNumberOfUpsrEnginesMustBeEnough()
    {
    AtModuleApsTestRunner runner = CurrentRunner();
    uint32 maxNumUpsrEngines = mMethodsGet(runner)->MaxNumUpsrEngines(runner);
    TEST_ASSERT_EQUAL_INT(maxNumUpsrEngines, AtModuleApsMaxNumUpsrEngines(Module()));
    }

static void testNumberOfSupportedEngineOfNullModuleMustBeZero()
    {
    TEST_ASSERT_EQUAL_INT(0, AtModuleApsMaxNumUpsrEngines(NULL));
    }

static void testCanNotCreateEngineWithOutOfRangeId()
    {
    uint32 numEngine = AtModuleApsMaxNumUpsrEngines(Module());
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(Module(), numEngine, WorkingPath1(), ProtectionPath1()));
    }

static void testCanNotCreateEngineWithNullWorkingOrProtectionPath()
    {
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, NULL, ProtectionPath1()));
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), NULL));
    }

static void testCanNotCreateEngineWithNullModule()
    {
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(NULL, 0, WorkingPath1(), ProtectionPath1()));
    }

static void testCanCreateEngineWithValidAllValidInput()
    {
    TEST_ASSERT_NOT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), ProtectionPath1()));
    }

static void testCanNotReCreateEngine()
    {
    TEST_ASSERT_NOT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), ProtectionPath1()));
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), ProtectionPath1()));
    }

static void testCanNotCreateEngineWhilePathsBelongToOthers()
    {
    TEST_ASSERT_NOT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), ProtectionPath1()));
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(Module(), 1, WorkingPath1(), ProtectionPath2()));
    TEST_ASSERT_NULL(AtModuleApsUpsrEngineCreate(Module(), 2, WorkingPath2(), ProtectionPath1()));
    }

static void testCanCreateMoreThanOneEngineWithValidAllValidInputIfSupport()
    {
    TEST_ASSERT_NOT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), ProtectionPath1()));
    TEST_ASSERT_NOT_NULL(AtModuleApsUpsrEngineCreate(Module(), 1, WorkingPath2(), ProtectionPath2()));
    }

static void testCanGetIteratorUpsrEngineOfValidModule()
    {
    uint32      numEngine;
    uint32      count = 0;
    AtObject    engine;
    AtIterator oIter = AtModuleApsUpsrEngineIteratorCreate(Module());
    TEST_ASSERT_NOT_NULL(oIter);
    numEngine = AtIteratorCount(oIter);
    while ((engine = AtIteratorNext(oIter)))
        {
        count++;
        }
    AtObjectDelete((AtObject)oIter);
    TEST_ASSERT_EQUAL_INT(numEngine, count);
    }

static void testCanDeleteCreatedEngine()
    {
    TEST_ASSERT_NOT_NULL(AtModuleApsUpsrEngineCreate(Module(), 0, WorkingPath1(), ProtectionPath1()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsUpsrEngineDelete(Module(), 0));
    }

static void testCanNotDeleteEngineOfNullModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleApsUpsrEngineDelete(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDeleteEngineNotBeenCreatedMustFine()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsUpsrEngineDelete(Module(), 0));
    }

static void testCanNotDeleteEngineWhichOutOfRangeId()
    {
    uint32 numEngine;

    AtTestLoggerEnable(cAtFalse);
    numEngine = AtModuleApsMaxNumUpsrEngines(Module());
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModuleApsUpsrEngineDelete(Module(), numEngine));
    AtTestLoggerEnable(cAtTrue);
    }

static void testModuleCanPeriodicallyProcessed()
    {
    AtModuleApsPeriodicProcess(Module(), 10);
    }

static void testNullModuleCanNotPeriodicallyProcessed()
    {
    TEST_ASSERT_EQUAL_INT(0, AtModuleApsPeriodicProcess(NULL, 10));
    }

static void testCanGetApsCapacityDescriptionOfValidModule()
    {
    TEST_ASSERT_NOT_NULL(AtModuleCapacityDescription((AtModule)Module()));
    }

static void testCanNotGetApsCapacityDescriptionOfNullModule()
    {
    TEST_ASSERT_NULL(AtModuleCapacityDescription(NULL));
    }

static void testNumberOfGroupsMustBeEnough()
    {
    AtModuleApsTestRunner runner = CurrentRunner();
    uint32 maxNumApsGroups = mMethodsGet(runner)->MaxNumApsGroups(runner);
    TEST_ASSERT_EQUAL_INT(maxNumApsGroups, AtModuleApsMaxNumGroups(Module()));
    }

static void testNumberOfSupportedGroupOfNullModuleMustBeZero()
    {
    TEST_ASSERT_EQUAL_INT(0, AtModuleApsMaxNumGroups(NULL));
    }

static void testCanNotCreateGroupWithOutOfRangeId()
    {
    uint32 maxId = AtModuleApsMaxNumGroups(Module());
    TEST_ASSERT_NULL(AtModuleApsGroupCreate(Module(), maxId));
    }

static void testCanNotCreateGroupWithNullModule()
    {
    TEST_ASSERT_NULL(AtModuleApsGroupCreate(NULL, 0));
    }

static void testCanCreateGroupWithValidGroupId()
    {
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    if (numGroup > 0)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupCreate(Module(), 0));
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupGet(Module(), 0));
        }
    }

static void testCanNotReCreateGroup()
    {
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    if (numGroup > 0)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupCreate(Module(), 0));
        TEST_ASSERT_NULL(AtModuleApsGroupCreate(Module(), 0));
        }
    }

static void testGetGroupMustBeNullWithOutOfRangeId()
    {
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    TEST_ASSERT_NULL(AtModuleApsGroupGet(Module(), numGroup));
    }

static void testCanDeleteCreatedGroup()
    {
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    if (numGroup > 0)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupCreate(Module(), 0));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsGroupDelete(Module(), 0));
        TEST_ASSERT_NULL(AtModuleApsGroupGet(Module(), 0));
        }
    }

static void testCanNotDeleteGroupOfNullModule()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleApsGroupDelete(NULL, 0));
    }

static void testCanNotDeleteGroupWithOutOfRangeId()
    {
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModuleApsGroupDelete(Module(), numGroup));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDeleteGroupNotBeenCreatedMustFine()
    {
    uint32 groupId;
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    for (groupId = 0; groupId < numGroup; groupId ++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsGroupDelete(Module(), groupId));
        }
    }

static void testCanNotDeleteGroupWhichOutOfRangeId()
    {
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModuleApsGroupDelete(Module(), numGroup));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanCreateMaxGroupOfValidModule()
    {
    uint32 groupId;
    uint32 numGroup = AtModuleApsMaxNumGroups(Module());
    for (groupId = 0; groupId < numGroup; groupId ++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupCreate(Module(), groupId));
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupGet(Module(), groupId));
        }
    }

static void testGroupIDMustMatchWithIDUsedToGetItsInstance()
    {
    uint32      groupId;
    AtApsGroup  group;
    uint32      numGroup = AtModuleApsMaxNumGroups(Module());
    for (groupId = 0; groupId < numGroup; groupId ++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsGroupCreate(Module(), groupId));
        group = AtModuleApsGroupGet(Module(), groupId);
        TEST_ASSERT_NOT_NULL(group);
        TEST_ASSERT_EQUAL_INT(groupId, AtApsGroupIdGet(group));
        }
    }

static void testNumberOfSelectorsMustBeEnough()
    {
    AtModuleApsTestRunner runner = CurrentRunner();
    uint32 maxNumApsSelectors = mMethodsGet(runner)->MaxNumApsSelectors(runner);
    TEST_ASSERT_EQUAL_INT(maxNumApsSelectors, AtModuleApsMaxNumSelectors(Module()));
    }

static void testNumberOfSupportedSelectorOfNullModuleMustBeZero()
    {
    TEST_ASSERT_EQUAL_INT(0, AtModuleApsMaxNumSelectors(NULL));
    }

static void testCanNotCreateSelectorWithOutOfRangeId()
    {
    uint32 maxId = AtModuleApsMaxNumSelectors(Module());
    TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), maxId, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
    }

static void testCanNotCreateSelectorWithNullModule()
    {
    TEST_ASSERT_NULL(AtModuleApsSelectorCreate(NULL, 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
    }

static void testCanNotCreateSelectorWithNullWorkingOrProtectionOrOutgoingPath()
    {
    TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 0, NULL, (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
    TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), NULL, (AtChannel)OutgoingPath1()));
    TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), NULL));
    }

static void testCanCreateSelectorWithAllValidInput()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    if (numSelector > 0)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorGet(Module(), 0));
        }
    }

static void testCanNotReCreateSelector()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    if (numSelector > 0)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorGet(Module(), 0));
        TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
        }
    }

static void testCanNotCreateSelectorWhilePathsBelongToOthers()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    if (numSelector > 1)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
        TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 1, NULL, (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
        TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 1, (AtChannel)WorkingPath1(), NULL, (AtChannel)OutgoingPath1()));
        TEST_ASSERT_NULL(AtModuleApsSelectorCreate(Module(), 1, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), NULL));
        }
    }

static void testCanCreateMoreThanOneSelectorWithValidAllValidInputIfSupport()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    if (numSelector > 1)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorCreate(Module(), 1, (AtChannel)WorkingPath2(), (AtChannel)ProtectionPath2(), (AtChannel)OutgoingPath2()));
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorGet(Module(), 1));
        }
    }

static void testSelectorIDMustMatchWithIDUsedToGetItsInstance()
    {
    AtApsSelector selector;
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    if (numSelector > 0)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1()));
        selector = AtModuleApsSelectorGet(Module(), 0);
        TEST_ASSERT_NOT_NULL(selector);
        TEST_ASSERT_EQUAL_INT(0, AtApsSelectorIdGet(selector));
        }
    if (numSelector > 1)
        {
        TEST_ASSERT_NOT_NULL(AtModuleApsSelectorCreate(Module(), 1, (AtChannel)WorkingPath2(), (AtChannel)ProtectionPath2(), (AtChannel)OutgoingPath2()));
        selector = AtModuleApsSelectorGet(Module(), 1);
        TEST_ASSERT_NOT_NULL(selector);
        TEST_ASSERT_EQUAL_INT(1, AtApsSelectorIdGet(selector));
        }
    }

static void testCanDeleteCreatedSelector()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    if (numSelector > 0)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsSelectorDelete(Module(), 0));
        TEST_ASSERT_NULL(AtModuleApsSelectorGet(Module(), 0));
        }
    if (numSelector > 1)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsSelectorDelete(Module(), 1));
        TEST_ASSERT_NULL(AtModuleApsSelectorGet(Module(), 1));
        }
    }

static void testCanNotDeleteSelectorOfNullModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleApsSelectorDelete(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanNotDeleteSelectorWithOutOfRangeId()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModuleApsSelectorDelete(Module(), numSelector));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDeleteSelectorNotBeenCreatedMustFine()
    {
    uint32 selectorId;
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    for (selectorId = 0; selectorId < numSelector; selectorId ++)
       {
       TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleApsSelectorDelete(Module(), selectorId));
       }
    }

static void testCanNotDeleteSelectorWhichOutOfRangeId()
    {
    uint32 numSelector = AtModuleApsMaxNumSelectors(Module());
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModuleApsSelectorDelete(Module(), numSelector));
    AtTestLoggerEnable(cAtTrue);
    }

static void testNumberOfLinearEnginesMustBeEnough()
    {
    AtModuleApsTestRunner runner = CurrentRunner();
    uint32 maxNumEngines = mMethodsGet(runner)->MaxNumLinearEngines(runner);
    TEST_ASSERT_EQUAL_INT(maxNumEngines, AtModuleApsMaxNumLinearEngines(Module()));
    }

static void AllUpsrEnginesDelete()
    {
    uint32 numEngines = AtModuleApsMaxNumUpsrEngines(Module());
    uint32 engine_i;

    for (engine_i = 0; engine_i < numEngines; engine_i++)
        AtAssert(AtModuleApsUpsrEngineDelete(Module(), engine_i) == cAtOk);
    }

static void AllApsGroupsDelete()
    {
    uint32 numGroups = AtModuleApsMaxNumGroups(Module());
    uint32 group_i;

    for (group_i = 0; group_i < numGroups; group_i++)
        {
        AtApsGroup group = AtModuleApsGroupGet(Module(), group_i);
        if (group)
            AtAssert(AtModuleApsGroupDelete(Module(), group_i) == cAtOk);
        }
    }

static void AllApsSelectorsDelete()
    {
    uint32 numSelectors = AtModuleApsMaxNumSelectors(Module());
    uint32 selector_i;

    for (selector_i = 0; selector_i < numSelectors; selector_i++)
        {
        AtApsSelector selector = AtModuleApsSelectorGet(Module(), selector_i);
        if (selector)
            AtAssert(AtModuleApsSelectorDelete(Module(), selector_i) == cAtOk);
        }
    }

static void Setup()
    {
    AtModuleApsTestRunner self = (AtModuleApsTestRunner)AtUnittestRunnerCurrentRunner();
    AllUpsrEnginesDelete();
    AllApsGroupsDelete();
    AllApsSelectorsDelete();
    mMethodsGet(self)->PathSetup(self, m_paths);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleAps)
        {
        new_TestFixture("testNumberOfUpsrEnginesMustBeEnough", testNumberOfUpsrEnginesMustBeEnough),
        new_TestFixture("testNumberOfSupportedEngineOfNullModuleMustBeZero", testNumberOfSupportedEngineOfNullModuleMustBeZero),
        new_TestFixture("testCanNotCreateEngineWithOutOfRangeId", testCanNotCreateEngineWithOutOfRangeId),
        new_TestFixture("testCanNotCreateEngineWithNullWorkingOrProtectionPath", testCanNotCreateEngineWithNullWorkingOrProtectionPath),
        new_TestFixture("testCanNotCreateEngineWithNullModule", testCanNotCreateEngineWithNullModule),
        new_TestFixture("testCanCreateEngineWithValidAllValidInput", testCanCreateEngineWithValidAllValidInput),
        new_TestFixture("testCanNotReCreateEngine", testCanNotReCreateEngine),
        new_TestFixture("testCanNotCreateEngineWhilePathsBelongToOthers", testCanNotCreateEngineWhilePathsBelongToOthers),
        new_TestFixture("testCanCreateMoreThanOneEngineWithValidAllValidInputIfSupport", testCanCreateMoreThanOneEngineWithValidAllValidInputIfSupport),
        new_TestFixture("testCanDeleteCreatedEngine", testCanDeleteCreatedEngine),
        new_TestFixture("testCanNotDeleteEngineOfNullModule", testCanNotDeleteEngineOfNullModule),
        new_TestFixture("testCanDeleteEngineNotBeenCreatedMustFine", testCanDeleteEngineNotBeenCreatedMustFine),
        new_TestFixture("testCanNotDeleteEngineWhichOutOfRangeId", testCanNotDeleteEngineWhichOutOfRangeId),
        new_TestFixture("testModuleCanPeriodicallyProcessed", testModuleCanPeriodicallyProcessed),
        new_TestFixture("testNullModuleCanNotPeriodicallyProcessed", testNullModuleCanNotPeriodicallyProcessed),
        new_TestFixture("testCanGetApsCapacityDescriptionOfValidModule", testCanGetApsCapacityDescriptionOfValidModule),
        new_TestFixture("testCanNotGetApsCapacityDescriptionOfNullModule", testCanNotGetApsCapacityDescriptionOfNullModule),
        new_TestFixture("testCanGetIteratorUpsrEngineOfValidModule", testCanGetIteratorUpsrEngineOfValidModule),

        new_TestFixture("testNumberOfGroupsMustBeEnough", testNumberOfGroupsMustBeEnough),
        new_TestFixture("testNumberOfSupportedGroupOfNullModuleMustBeZero", testNumberOfSupportedGroupOfNullModuleMustBeZero),
        new_TestFixture("testCanNotCreateGroupWithOutOfRangeId", testCanNotCreateGroupWithOutOfRangeId),
        new_TestFixture("testCanNotCreateGroupWithNullModule", testCanNotCreateGroupWithNullModule),
        new_TestFixture("testCanCreateGroupWithValidGroupId", testCanCreateGroupWithValidGroupId),
        new_TestFixture("testCanNotReCreateGroup", testCanNotReCreateGroup),
        new_TestFixture("testGetGroupMustBeNullWithOutOfRangeId", testGetGroupMustBeNullWithOutOfRangeId),
        new_TestFixture("testCanDeleteCreatedGroup", testCanDeleteCreatedGroup),
        new_TestFixture("testCanNotDeleteGroupOfNullModule", testCanNotDeleteGroupOfNullModule),
        new_TestFixture("testCanNotDeleteGroupWithOutOfRangeId", testCanNotDeleteGroupWithOutOfRangeId),
        new_TestFixture("testCanDeleteGroupNotBeenCreatedMustFine", testCanDeleteGroupNotBeenCreatedMustFine),
        new_TestFixture("testCanNotDeleteGroupWhichOutOfRangeId", testCanNotDeleteGroupWhichOutOfRangeId),
        new_TestFixture("testCanCreateMaxGroupOfValidModule", testCanCreateMaxGroupOfValidModule),
        new_TestFixture("testGroupIDMustMatchWithIDUsedToGetItsInstance", testGroupIDMustMatchWithIDUsedToGetItsInstance),

        new_TestFixture("testNumberOfSelectorsMustBeEnough", testNumberOfSelectorsMustBeEnough),
        new_TestFixture("testNumberOfSupportedSelectorOfNullModuleMustBeZero", testNumberOfSupportedSelectorOfNullModuleMustBeZero),
        new_TestFixture("testCanNotCreateSelectorWithOutOfRangeId", testCanNotCreateSelectorWithOutOfRangeId),
        new_TestFixture("testCanNotCreateSelectorWithNullModule", testCanNotCreateSelectorWithNullModule),
        new_TestFixture("testCanNotCreateSelectorWithNullWorkingOrProtectionOrOutgoingPath", testCanNotCreateSelectorWithNullWorkingOrProtectionOrOutgoingPath),
        new_TestFixture("testCanCreateSelectorWithAllValidInput", testCanCreateSelectorWithAllValidInput),
        new_TestFixture("testCanNotReCreateSelector", testCanNotReCreateSelector),
        new_TestFixture("testCanNotCreateSelectorWhilePathsBelongToOthers", testCanNotCreateSelectorWhilePathsBelongToOthers),
        new_TestFixture("testCanCreateMoreThanOneSelectorWithValidAllValidInputIfSupport", testCanCreateMoreThanOneSelectorWithValidAllValidInputIfSupport),
        new_TestFixture("testSelectorIDMustMatchWithIDUsedToGetItsInstance", testSelectorIDMustMatchWithIDUsedToGetItsInstance),
        new_TestFixture("testCanDeleteCreatedSelector", testCanDeleteCreatedSelector),
        new_TestFixture("testCanNotDeleteSelectorOfNullModule", testCanNotDeleteSelectorOfNullModule),
        new_TestFixture("testCanNotDeleteSelectorWithOutOfRangeId", testCanNotDeleteSelectorWithOutOfRangeId),
        new_TestFixture("testCanDeleteSelectorNotBeenCreatedMustFine", testCanDeleteSelectorNotBeenCreatedMustFine),
        new_TestFixture("testCanNotDeleteSelectorWhichOutOfRangeId", testCanNotDeleteSelectorWhichOutOfRangeId),

        new_TestFixture("testNumberOfLinearEnginesMustBeEnough", testNumberOfLinearEnginesMustBeEnough),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleAps_Caller, "TestSuite_ModuleAps", Setup, NULL, TestSuite_ModuleAps);

    return (TestRef)((void *)&TestSuite_ModuleAps_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void PathSetup(AtModuleApsTestRunner self, AtSdhPath paths[6])
    {
    /* Enforce sub class override this */
    AtAssert(0);
    }

static void SelectorSetup(AtModuleApsTestRunner self, AtApsSelector selectors[2])
    {
    selectors[0] = AtModuleApsSelectorCreate(Module(), 0, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1());
    selectors[1] = AtModuleApsSelectorCreate(Module(), 1, (AtChannel)WorkingPath2(), (AtChannel)ProtectionPath2(), (AtChannel)OutgoingPath2());
    AtAssert(selectors[0]);
    AtAssert(selectors[1]);
    }

static void TestUpsrEngines(AtModuleApsTestRunner self)
    {
    AtModuleAps apsModule = (AtModuleAps)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint8 engine_i = 0;
    uint8 numEngines = AtModuleApsMaxNumUpsrEngines(apsModule);

    for (engine_i = 0; engine_i < numEngines; engine_i++)
        {
        AtApsEngine engine = AtModuleApsUpsrEngineCreate(Module(), engine_i, WorkingPath1(), ProtectionPath1());
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(mThis(self))->CreateUpsrEngineTestRunner(mThis(self), engine);
        AtAssert(engine);
        AtUnittestRunnerRun(runner);
        AtAssert(AtModuleApsUpsrEngineDelete(Module(), engine_i) == cAtOk);
        AtUnittestRunnerDelete(runner);
        }
    }

static void TestApsGroups(AtModuleApsTestRunner self)
    {
    AtModuleAps apsModule = (AtModuleAps)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint32 group_i = 0;
    uint32 numGroup = AtModuleApsMaxNumGroups(apsModule);

    for (group_i = 0; group_i < numGroup; group_i++)
        {
        AtApsGroup group = AtModuleApsGroupCreate(Module(), group_i);
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(mThis(self))->CreateApsGroupTestRunner(mThis(self), group);
        AtAssert(group);
        AtUnittestRunnerRun(runner);
        AtAssert(AtModuleApsGroupDelete(Module(), group_i) == cAtOk);
        AtUnittestRunnerDelete(runner);
        }
    }

static void TestApsSelectors(AtModuleApsTestRunner self)
    {
    AtModuleAps apsModule = (AtModuleAps)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint32 selector_i = 0;
    uint32 numSelector = AtModuleApsMaxNumSelectors(apsModule);

    for (selector_i = 0; selector_i < numSelector; selector_i++)
        {
        AtApsSelector selector = AtModuleApsSelectorCreate(Module(), selector_i, (AtChannel)WorkingPath1(), (AtChannel)ProtectionPath1(), (AtChannel)OutgoingPath1());
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(mThis(self))->CreateApsSelectorTestRunner(mThis(self), selector);
        AtAssert(selector);
        AtUnittestRunnerRun(runner);
        AtAssert(AtModuleApsSelectorDelete(Module(), selector_i) == cAtOk);
        AtUnittestRunnerDelete(runner);
        }
    }

static AtApsEngineTestRunner CreateUpsrEngineTestRunner(AtModuleApsTestRunner self, AtApsEngine engine)
    {
    return AtApsUpsrEngineTestRunnerNew((AtApsEngine)engine, (AtModuleTestRunner)self);
    }

static AtApsGroupTestRunner CreateApsGroupTestRunner(AtModuleApsTestRunner self, AtApsGroup group)
    {
    AtUnused(self);
    return AtApsGroupTestRunnerNew(group, m_selectors);
    }

static AtApsSelectorTestRunner CreateApsSelectorTestRunner(AtModuleApsTestRunner self, AtApsSelector selector)
    {
    AtUnused(self);
    return AtApsSelectorTestRunnerNew(selector);
    }

static uint32 MaxNumUpsrEngines(AtModuleApsTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumLinearEngines(AtModuleApsTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumApsGroups(AtModuleApsTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumApsSelectors(AtModuleApsTestRunner self)
    {
    return 0;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);

    mMethodsGet(mThis(self))->PathSetup(mThis(self), m_paths);
    mMethodsGet(mThis(self))->TestUpsrEngines(mThis(self));
    mMethodsGet(mThis(self))->TestApsSelectors(mThis(self));
    mMethodsGet(mThis(self))->SelectorSetup(mThis(self), m_selectors);
    mMethodsGet(mThis(self))->TestApsGroups(mThis(self));
    }

static void MethodsInit(AtModuleApsTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PathSetup);
        mMethodOverride(m_methods, SelectorSetup);
        mMethodOverride(m_methods, TestUpsrEngines);
        mMethodOverride(m_methods, TestApsGroups);
        mMethodOverride(m_methods, TestApsSelectors);
        mMethodOverride(m_methods, CreateUpsrEngineTestRunner);
        mMethodOverride(m_methods, CreateApsGroupTestRunner);
        mMethodOverride(m_methods, CreateApsSelectorTestRunner);
        mMethodOverride(m_methods, MaxNumUpsrEngines);
        mMethodOverride(m_methods, MaxNumLinearEngines);
        mMethodOverride(m_methods, MaxNumApsGroups);
        mMethodOverride(m_methods, MaxNumApsSelectors);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleApsTestRunner);
    }

AtModuleTestRunner AtModuleApsTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtModuleApsTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModuleApsTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModuleApsTestRunnerObjectInit(newRunner, module);
    }

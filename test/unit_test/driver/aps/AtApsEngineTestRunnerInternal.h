/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsEngineTestRunnerInternal.h
 * 
 * Created Date: Jul 22, 2016
 *
 * Description : APS Engine Unit test
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSENGINETESTRUNNERINTERNAL_H_
#define ATAPSENGINETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtApsEngineTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsEngineTestRunnerMethods
    {
    uint32 (*ApplicableDefects)(AtApsEngineTestRunner self);
    atbool (*CanEngineBeStopped)(AtApsEngineTestRunner self);
    }tAtApsEngineTestRunnerMethods;

typedef struct tAtApsEngineTestRunner
    {
    tAtChannelTestRunner super;
    const tAtApsEngineTestRunnerMethods *methods;
    }tAtApsEngineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngineTestRunner AtApsEngineTestRunnerObjectInit(AtApsEngineTestRunner self, AtApsEngine engine, AtModuleTestRunner moduleRunner);
atbool AtApsEngineCanBeStopped(AtApsEngineTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* ATAPSENGINETESTRUNNERINTERNAL_H_ */


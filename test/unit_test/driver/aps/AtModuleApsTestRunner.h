/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtModuleApsTestRunner.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : APS Module Test Runner Header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATMODULEAPSTESTRUNNER_H_
#define ATMODULEAPSTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleApsTestRunner * AtModuleApsTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleApsTestRunnerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* ATMODULEAPSTESTRUNNER_H_ */


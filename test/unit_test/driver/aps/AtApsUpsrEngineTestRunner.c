/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsUpsrEngineTestRunner.c
 *
 * Created Date: Jul 21, 2016
 *
 * Description : Testcases for UPSR Engine Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "AtTests.h"
#include "AtApsUpsrEngine.h"
#include "AtApsEngineTestRunnerInternal.h"
#include "AtApsEngineTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtApsUpsrEngineTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtApsUpsrEngineTestRunner
    {
    tAtApsEngineTestRunner super;
    }tAtApsUpsrEngineTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods      m_AtUnittestRunnerOverride;
static tAtApsEngineTestRunnerMethods m_AtApsEngineTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static const eAtApsUpsrExtCmd cUpsrExtCmdVal[] = {
                                    cAtApsUpsrExtCmdUnknown,
                                    cAtApsUpsrExtCmdClear,
                                    cAtApsUpsrExtCmdFs2Wrk,
                                    cAtApsUpsrExtCmdFs2Prt,
                                    cAtApsUpsrExtCmdMs2Wrk,
                                    cAtApsUpsrExtCmdMs2Prt,
                                    cAtApsUpsrExtCmdLp
                                    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtApsEngine TestedEngine()
    {
    return (AtApsEngine)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testWorkingAndProtectionPathMustValid()
    {
    TEST_ASSERT_NOT_NULL(AtApsUpsrEngineWorkingPathGet((AtApsUpsrEngine)TestedEngine()));
    TEST_ASSERT_NOT_NULL(AtApsUpsrEngineProtectionPathGet((AtApsUpsrEngine)TestedEngine()));
    }

static void testWorkingAndProtectionPathOfNullEngineMustNull()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtApsUpsrEngineWorkingPathGet(NULL));
    TEST_ASSERT_NULL(AtApsUpsrEngineProtectionPathGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanGetActivePathFromValidEngine()
    {
    TEST_ASSERT_NOT_NULL(AtApsUpsrEngineActivePathGet((AtApsUpsrEngine)TestedEngine()));
    }

static void testNullEngineMustHaveNullActivePath()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtApsUpsrEngineActivePathGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanNotIssueAnyExternalCommandOnStoppedEngine()
    {
    uint8 numExtCmd = mCount(cUpsrExtCmdVal);
    uint8 cmd_i;

    if (AtApsEngineCanBeStopped((AtApsEngineTestRunner)AtUnittestRunnerCurrentRunner()) == cAtFalse)
        return;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStop(TestedEngine()));
    for (cmd_i = 0; cmd_i < numExtCmd; cmd_i ++)
        {
        TEST_ASSERT_EQUAL_INT(cAtErrorNotReady, AtApsUpsrEngineExtCmdSet((AtApsUpsrEngine)TestedEngine(), cUpsrExtCmdVal[cmd_i]));
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanNotIssueAnyExternalCommandOnNullEngine()
    {
    uint8 numExtCmd = mCount(cUpsrExtCmdVal);
    uint8 extCmdIdx;
    AtTestLoggerEnable(cAtFalse);
    for (extCmdIdx = 0; extCmdIdx < numExtCmd; extCmdIdx ++)
        {
        TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsUpsrEngineExtCmdSet(NULL, cUpsrExtCmdVal[extCmdIdx]));
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanIssueSupportedExternalCommandOnValidEngine()
    {
    uint8 numExtCmd = mCount(cUpsrExtCmdVal);
    uint8 extCmdIdx;

    if (!AtApsEngineCanStart(TestedEngine()))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsEngineStart(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtApsEngineStateStart, AtApsEngineStateGet(TestedEngine()));
    for (extCmdIdx = 1; extCmdIdx < numExtCmd; extCmdIdx ++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtApsUpsrEngineExtCmdSet((AtApsUpsrEngine)TestedEngine(), cUpsrExtCmdVal[extCmdIdx]));
        TEST_ASSERT_EQUAL_INT(cUpsrExtCmdVal[extCmdIdx], AtApsUpsrEngineExtCmdGet((AtApsUpsrEngine)TestedEngine()));
        }
    }

static void testCanNotGetExternalCommandOnNullEngine()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtApsUpsrExtCmdUnknown, AtApsUpsrEngineExtCmdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ApsUpsrEngine_Fixtures)
        {
        new_TestFixture("testWorkingAndProtectionPathMustValid", testWorkingAndProtectionPathMustValid),
        new_TestFixture("testWorkingAndProtectionPathOfNullEngineMustNull", testWorkingAndProtectionPathOfNullEngineMustNull),
        new_TestFixture("testCanGetActivePathFromValidEngine", testCanGetActivePathFromValidEngine),
        new_TestFixture("testNullEngineMustHaveNullActivePath", testNullEngineMustHaveNullActivePath),
        new_TestFixture("testCanNotIssueAnyExternalCommandOnStoppedEngine", testCanNotIssueAnyExternalCommandOnStoppedEngine),
        new_TestFixture("testCanNotIssueAnyExternalCommandOnNullEngine", testCanNotIssueAnyExternalCommandOnNullEngine),
        new_TestFixture("testCanIssueSupportedExternalCommandOnValidEngine", testCanIssueSupportedExternalCommandOnValidEngine),
        new_TestFixture("testCanNotGetExternalCommandOnNullEngine", testCanNotGetExternalCommandOnNullEngine)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ApsUpsrEngine_Caller, "TestSuite_ApsUpsrEngine", NULL, NULL, TestSuite_ApsUpsrEngine_Fixtures);

    return (TestRef)((void *)&TestSuite_ApsUpsrEngine_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static uint32 ApplicableDefects(AtApsEngineTestRunner self)
    {
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop | cAtSdhPathAlarmTim | cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm | cAtSdhPathAlarmBerSd | cAtSdhPathAlarmBerSf;
    }

static atbool CanEngineBeStopped(AtApsEngineTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtApsEngineTestRunner(AtApsEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtApsEngineTestRunnerOverride, mMethodsGet(self), sizeof(m_AtApsEngineTestRunnerOverride));

        mMethodOverride(m_AtApsEngineTestRunnerOverride, ApplicableDefects);
        mMethodOverride(m_AtApsEngineTestRunnerOverride, CanEngineBeStopped);
        }

    mMethodsSet(self, &m_AtApsEngineTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtApsEngineTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtApsEngineTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    OverrideAtApsEngineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsUpsrEngineTestRunner);
    }

static AtApsEngineTestRunner ObjectInit(AtApsEngineTestRunner self, AtApsEngine engine, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsEngineTestRunnerObjectInit(self, engine, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtApsEngineTestRunner AtApsUpsrEngineTestRunnerNew(AtApsEngine engine, AtModuleTestRunner moduleRunner)
    {
    AtApsEngineTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, engine, moduleRunner);
    }

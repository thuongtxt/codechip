/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsEngineTestRunner.h
 * 
 * Created Date: Jul 22, 2016
 *
 * Description : APS Engine unitest header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSENGINETESTRUNNER_H_
#define ATAPSENGINETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "../man/module_runner/AtModuleTestRunner.h"
#include "AtApsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsEngineTestRunner * AtApsEngineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concretes */
AtApsEngineTestRunner AtApsUpsrEngineTestRunnerNew(AtApsEngine engine, AtModuleTestRunner moduleRunner);

#ifdef __cplusplus
}
#endif
#endif /* ATAPSENGINETESTRUNNER_H_ */


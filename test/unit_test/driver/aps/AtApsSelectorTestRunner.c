/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsSelectorTestRunner.c
 *
 * Created Date: Aug 9, 2016
 *
 * Description : APS selector unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtApsSelectorTestRunner.h"
#include "AtApsSelectorTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtApsSelector ApsSelectorGet(AtApsSelectorTestRunner self)
    {
    return (AtApsSelector)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

static AtApsSelector TestedSelector()
    {
    return (AtApsSelector)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testCanInitializeApsGroup()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsSelectorInit(TestedSelector()));
    }

static void testCanNotInitializeNullSelector()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtApsSelectorInit(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testIdOfNullSelectorMustBeInvalid()
    {
    TEST_ASSERT_EQUAL_INT(cInvalidUint32, AtApsSelectorIdGet(NULL));
    }

static void testCanEnableSelector()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsSelectorEnable(TestedSelector(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtApsSelectorIsEnabled(TestedSelector()));
    }

static void testCanNotEnableNullSelector()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtApsSelectorEnable(NULL, cAtTrue));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDisableSelector()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsSelectorEnable(TestedSelector(), cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtApsSelectorIsEnabled(TestedSelector()));
    }

static void testCanGetWorkingChannelOfValidSelector()
    {
    TEST_ASSERT_NOT_NULL(AtApsSelectorWorkingChannelGet(TestedSelector()));
    }

static void testWorkingChannelOfNullSelectorMustBeNull()
    {
    TEST_ASSERT_NULL(AtApsSelectorWorkingChannelGet(NULL));
    }

static void testCanGetProtectionChannelOfValidSelector()
    {
    TEST_ASSERT_NOT_NULL(AtApsSelectorProtectionChannelGet(TestedSelector()));
    }

static void testProtectionChannelOfNullSelectorMustBeNull()
    {
    TEST_ASSERT_NULL(AtApsSelectorProtectionChannelGet(NULL));
    }

static void testCanGetOutgoingChannelOfValidSelector()
    {
    TEST_ASSERT_NOT_NULL(AtApsSelectorOutgoingChannelGet(TestedSelector()));
    }

static void testOutgoingChannelOfNullSelectorMustBeNull()
    {
    TEST_ASSERT_NULL(AtApsSelectorOutgoingChannelGet(NULL));
    }

static void testCanSelectWorkingChannelOfValidSelector()
    {
    AtChannel working = AtApsSelectorWorkingChannelGet(TestedSelector());
    TEST_ASSERT_NOT_NULL(working);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsSelectorChannelSelect(TestedSelector(), working));
    TEST_ASSERT(working == AtApsSelectorSelectedChannel(TestedSelector()))
    }

static void testCanNotSelectWorkingChannelOfNullSelector()
    {
    AtChannel working = AtApsSelectorWorkingChannelGet(TestedSelector());
    TEST_ASSERT_NOT_NULL(working);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsSelectorChannelSelect(NULL, working));
    }

static void testCanSelectProtectionChannelOfValidSelector()
    {
    AtChannel protection = AtApsSelectorProtectionChannelGet(TestedSelector());
    TEST_ASSERT_NOT_NULL(protection);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtApsSelectorChannelSelect(TestedSelector(), protection));
    TEST_ASSERT(protection == AtApsSelectorSelectedChannel(TestedSelector()))
    }

static void testCanNotSelectProtectionChannelOfNullSelector()
    {
    AtChannel protection = AtApsSelectorProtectionChannelGet(TestedSelector());
    TEST_ASSERT_NOT_NULL(protection);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtApsSelectorChannelSelect(NULL, protection));
    }

static void testCanNotSelectNullChannelOfValidSelector()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtApsSelectorChannelSelect(TestedSelector(), NULL));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtApsSelector_Fixtures)
        {
        new_TestFixture("testCanInitializeApsGroup", testCanInitializeApsGroup),
        new_TestFixture("testCanNotInitializeNullSelector", testCanNotInitializeNullSelector),
        new_TestFixture("testIdOfNullSelectorMustBeInvalid", testIdOfNullSelectorMustBeInvalid),
        new_TestFixture("testCanEnableSelector", testCanEnableSelector),
        new_TestFixture("testCanNotEnableNullSelector", testCanNotEnableNullSelector),
        new_TestFixture("testCanDisableSelector", testCanDisableSelector),
        new_TestFixture("testCanGetWorkingChannelOfValidSelector", testCanGetWorkingChannelOfValidSelector),
        new_TestFixture("testWorkingChannelOfNullSelectorMustBeNull", testWorkingChannelOfNullSelectorMustBeNull),
        new_TestFixture("testCanGetProtectionChannelOfValidSelector", testCanGetProtectionChannelOfValidSelector),
        new_TestFixture("testProtectionChannelOfNullSelectorMustBeNull", testProtectionChannelOfNullSelectorMustBeNull),
        new_TestFixture("testCanGetOutgoingChannelOfValidSelector", testCanGetOutgoingChannelOfValidSelector),
        new_TestFixture("testOutgoingChannelOfNullSelectorMustBeNull", testOutgoingChannelOfNullSelectorMustBeNull),
        new_TestFixture("testCanSelectWorkingChannelOfValidSelector", testCanSelectWorkingChannelOfValidSelector),
        new_TestFixture("testCanNotSelectWorkingChannelOfNullSelector", testCanNotSelectWorkingChannelOfNullSelector),
        new_TestFixture("testCanSelectProtectionChannelOfValidSelector", testCanSelectProtectionChannelOfValidSelector),
        new_TestFixture("testCanNotSelectProtectionChannelOfNullSelector", testCanNotSelectProtectionChannelOfNullSelector),
        new_TestFixture("testCanNotSelectNullChannelOfValidSelector", testCanNotSelectNullChannelOfValidSelector),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtApsSelector_Caller, "TestSuite_AtApsSelector", NULL, NULL, TestSuite_AtApsSelector_Fixtures);

    return (TestRef)((void *)&TestSuite_AtApsSelector_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtApsSelector selector = ApsSelectorGet((AtApsSelectorTestRunner)self);
    AtSnprintf(buffer, bufferSize, "%s", AtObjectToString((AtObject)selector));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtApsSelectorTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtApsSelectorTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsSelectorTestRunner);
    }

static AtApsSelectorTestRunner ObjectInit(AtApsSelectorTestRunner self, AtApsSelector group)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)group) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtApsSelectorTestRunner AtApsSelectorTestRunnerNew(AtApsSelector group)
    {
    AtApsSelectorTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, group);
    }

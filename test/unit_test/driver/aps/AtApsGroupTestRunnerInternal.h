/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsGroupTestRunnerInternal.h
 * 
 * Created Date: Aug 9, 2016
 *
 * Description : APS group test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSGROUPTESTRUNNERINTERNAL_H_
#define ATAPSGROUPTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsGroup.h"
#include "../../driver/man/AtObjectTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsGroupTestRunner
    {
    tAtObjectTestRunner super;

    /* Private data */
    AtApsSelector       *selectors;
    }tAtApsGroupTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* ATAPSGROUPTESTRUNNERINTERNAL_H_ */


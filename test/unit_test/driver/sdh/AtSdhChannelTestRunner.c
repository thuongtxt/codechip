/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhChannelTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : SDH channel test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtTests.h"
#include "AtChannel.h"
#include "AtSdhChannel.h"
#include "AtSdhChannelTestRunnerInternal.h"
#include "AtModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool TTIsAreSame(const tAtSdhTti *tti1, const tAtSdhTti *tti2);
extern AtSdhLine AtSdhChannelLineObjectGet(AtSdhChannel self);

/*--------------------------- Implementation ---------------------------------*/
static AtChannelTestRunner CurrentRunner()
    {
    return (AtChannelTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModuleSdhTestRunner ModuleSdhTestRunner()
    {
    return (AtModuleSdhTestRunner)AtChannelTestRunnerModuleRunnerGet(CurrentRunner());
    }

static AtSdhChannel TestedChannel()
    {
    return (AtSdhChannel)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eBool NoAlarm(AtChannel channel)
    {
    static const uint32 timeoutInMs = 1000;
    uint32 elapseTimeInMs = 0;

    if (AtTestIsSimulationTesting())
        return cAtTrue;

    while (elapseTimeInMs < timeoutInMs)
        {
        if (AtChannelAlarmGet(channel) == 0)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(TestedChannel());
    }

static void ChangeTxTti(AtSdhChannel self, eAtSdhTtiMode mode, const uint8 *message)
    {
    tAtSdhTti txTtiSet, txTtiGet;
    eAtRet ret;

    AtOsalMemInit(&txTtiSet, 0, sizeof(tAtSdhTti));
    AtOsalMemInit(&txTtiGet, 0, sizeof(tAtSdhTti));

    ret = AtSdhChannelTxTtiSet(self, AtSdhTtiMake(mode, message, AtStrlen((char*)message), &txTtiSet));
    if (ret == cAtErrorModeNotSupport)
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, ret);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelTxTtiGet(self, &txTtiGet));
    TEST_ASSERT(TTIsAreSame(&txTtiSet, &txTtiGet));
    }

static void testChangeTxTti()
    {
    AtSdhChannel sdhChannel = TestedChannel();
    const char* message[] = {"A", "ArriveTechnolog", "ArriveTechnologiesVietNam"};
    AtModuleSdhTestRunner moduleTestRunner = ModuleSdhTestRunner();
    if (AtModuleSdhTestRunnerSupportTtiMode1Byte(moduleTestRunner))
        ChangeTxTti(sdhChannel, cAtSdhTtiMode1Byte,  (const uint8*)message[0]);
    ChangeTxTti(sdhChannel, cAtSdhTtiMode16Byte   ,  (const uint8*)message[1]);
    ChangeTxTti(sdhChannel, cAtSdhTtiMode64Byte   ,  (const uint8*)message[2]);
    }

static void ChangeExpectedTti(AtSdhChannel self, eAtSdhTtiMode mode, const uint8 *message)
    {
    eAtRet ret;
    tAtSdhTti txTtiSet, txTtiGet;

    AtOsalMemInit(&txTtiSet, 0, sizeof(tAtSdhTti));
    AtOsalMemInit(&txTtiGet, 0, sizeof(tAtSdhTti));

    ret = AtSdhChannelExpectedTtiSet(self, AtSdhTtiMake(mode, message, AtStrlen((char*)message), &txTtiSet));
    if (ret == cAtErrorModeNotSupport)
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, ret);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelExpectedTtiGet(self, &txTtiGet));
    TEST_ASSERT(TTIsAreSame(&txTtiSet, &txTtiGet));
    }

static void testChangeExpectedTti()
    {
    AtSdhChannel sdhChannel = TestedChannel();
    const char* message[] = {"A", "ArriveTechnolog", "ArriveTechnologiesVietNam"};
    AtModuleSdhTestRunner moduleTestRunner = ModuleSdhTestRunner();
    if (AtModuleSdhTestRunnerSupportTtiMode1Byte(moduleTestRunner))
        ChangeExpectedTti(sdhChannel, cAtSdhTtiMode1Byte,  (const uint8*)message[0]);
    ChangeExpectedTti(sdhChannel, cAtSdhTtiMode16Byte   ,  (const uint8*)message[1]);
    ChangeExpectedTti(sdhChannel, cAtSdhTtiMode64Byte   ,  (const uint8*)message[2]);
    }

eBool AtSdhChannelRxTtiMatch(AtSdhChannel channel, const tAtSdhTti *expectedTti)
    {
    static const uint32 timeoutInMs    = 10000;
    uint32 elapseTimeInMs = 0;
    tAtSdhTti rxTti;
    AtOsalMemInit(&rxTti, 0, sizeof(tAtSdhTti));

    while (elapseTimeInMs < timeoutInMs)
        {
        AtSdhChannelRxTtiGet(TestedChannel(), &rxTti);
        if (TTIsAreSame(&rxTti, expectedTti))
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static void TtiMessageSendAndCheckReceivedMessage(eAtSdhTtiMode mode, const uint8 *message)
    {
    tAtSdhTti txTti;
    eAtRet  ret;

    AtOsalMemInit(&txTti, 0, sizeof(tAtSdhTti));
    ret = AtSdhChannelTxTtiSet(TestedChannel(), AtSdhTtiMake(mode, message, AtStrlen((char*)message), &txTti));
    if (ret == cAtErrorModeNotSupport)
        return;

    /* Send messages and receive them at Rx */
    TEST_ASSERT_EQUAL_INT(cAtOk, ret);

    /* Need to configure expect mode */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelExpectedTtiSet(TestedChannel(), &txTti));
    TEST_ASSERT(AtSdhChannelRxTtiMatch(TestedChannel(), &txTti));
    }

static void testTtiMessageSendAndReceive()
    {
    const char* message[] = {"A", "ArriveTechnolog", "ArriveTechnologiesVietNam"};
    AtModuleSdhTestRunner moduleTestRunner = ModuleSdhTestRunner();

    if (AtTestIsSimulationTesting())
        return;

    /* Send messages and receive them at Rx */
    if (AtModuleSdhTestRunnerSupportTtiMode1Byte(moduleTestRunner))
        TtiMessageSendAndCheckReceivedMessage(cAtSdhTtiMode1Byte,  (const uint8*)message[0]);
    TtiMessageSendAndCheckReceivedMessage(cAtSdhTtiMode16Byte   ,  (const uint8*)message[1]);
    TtiMessageSendAndCheckReceivedMessage(cAtSdhTtiMode64Byte   ,  (const uint8*)message[2]);
    }

static void testChangeExpectedTtiLine()
    {
    const uint8 * ttiMessage  = (const uint8*)"a";
    const uint8 * ttiMessage1 = (const uint8*)"b";
    tAtSdhTti txTti;
    AtSdhChannel sdhChannel = TestedChannel();
    eAtSdhTtiMode ttiMode = cAtSdhTtiMode1Byte;
    AtModuleSdhTestRunner moduleTestRunner = ModuleSdhTestRunner();
    AtOsalMemInit(&txTti, 0, sizeof(tAtSdhTti));

    /* Return if simulation test */
    if (AtTestIsSimulationTesting())
        return;

    if (AtModuleSdhTestRunnerSupportTtiMode1Byte(moduleTestRunner) == cAtFalse)
        ttiMode = cAtSdhTtiMode16Byte;

    /* Ensure message "a" is sent OK */
    TtiMessageSendAndCheckReceivedMessage(ttiMode, ttiMessage);

    /* Change expected messenger is "b" */
    AtSdhTtiMake(ttiMode, ttiMessage1, AtStrlen((char*)ttiMessage1), &txTti);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelExpectedTtiSet(sdhChannel, &txTti));

    /* Compare sender message and receiver message are different */
    TEST_ASSERT(AtSdhChannelRxTtiMatch(TestedChannel(), &txTti) == cAtFalse);
    }

static void helperTestTimWhenTtiMismatch(eAtSdhTtiMode mode, const uint8 *message1, const uint8 *message2)
    {
    tAtSdhTti txTti;
    AtSdhChannel sdhChannel = TestedChannel();
    AtOsalMemInit(&txTti, 0, sizeof(tAtSdhTti));

    /* Return if simulation test */
    if (AtTestIsSimulationTesting())
        return;

    /* Ensure message "a" is sent OK */
    TtiMessageSendAndCheckReceivedMessage(mode, message1);

    /* Check TIM is clear */
    TEST_ASSERT(NoAlarm((AtChannel)TestedChannel()));

    /* Change expected messenger is "b" */
    AtSdhTtiMake(mode, message2, AtStrlen((char*)message2), &txTti);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelExpectedTtiSet(sdhChannel, &txTti));

    /* Compare sender message and receiver message */
    TEST_ASSERT(AtSdhChannelRxTtiMatch(sdhChannel, &txTti) == cAtFalse);

    /* Alarm raised */
    TEST_ASSERT(NoAlarm((AtChannel)TestedChannel()) == cAtFalse);

    /* Revert to old message "a" to make TIM clear */
    AtSdhTtiMake(mode, message1, AtStrlen((char*)message1), &txTti);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelExpectedTtiSet(sdhChannel, &txTti));

    /* Compare sender message and receiver message */
    TEST_ASSERT(AtSdhChannelRxTtiMatch(sdhChannel, &txTti));

    TEST_ASSERT(NoAlarm((AtChannel)TestedChannel()));
    }

static void testTimWhenTtiMismatch()
    {
    AtModuleSdhTestRunner moduleTestRunner = ModuleSdhTestRunner();
    if (AtModuleSdhTestRunnerSupportTtiMode1Byte(moduleTestRunner))
        helperTestTimWhenTtiMismatch(cAtSdhTtiMode1Byte,  (const uint8*)"a"                , (const uint8*)"b");
    helperTestTimWhenTtiMismatch(cAtSdhTtiMode16Byte, (const uint8*)"abcdefghijklm"        , (const uint8*)"1234567891011");
    helperTestTimWhenTtiMismatch(cAtSdhTtiMode64Byte, (const uint8*)"102314654653132132213", (const uint8*)"zxcvbnmasdfghjklqwe");
    }

static eBool BerMonitoringIsSupported()
    {
    return cAtTrue;
    }

static void testBerMonitorEnable()
    {
    AtBerController berController = AtSdhChannelBerControllerGet(TestedChannel());

    if (!BerMonitoringIsSupported())
        return;

    /* BER may be not applicable for this channel */
    if (berController == NULL)
        return;

    /* Enable BER, check then Disable it and check again */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtBerControllerEnable(berController, cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtBerControllerIsEnabled(berController));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtBerControllerEnable(berController, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtBerControllerIsEnabled(berController));
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtBerControllerMonitoredChannel(self);
    return AtSdhChannelNumSts(channel) / 3;
    }

static eBool IsRateOverSts12c(AtBerController self)
    {
    return (HwChannelTypeValue(self) > 4) ? cAtTrue : cAtFalse;
    }

static eAtBerRate *SupportedBerRatesOverSts12c(void)
    {
    static eAtBerRate berRates[] =   {
                                     cAtBerRate1E4, /**< Error rate is 10E-4 */
                                     cAtBerRate1E5, /**< Error rate is 10E-5 */
                                     cAtBerRate1E6, /**< Error rate is 10E-6 */
                                     cAtBerRate1E7, /**< Error rate is 10E-7 */
                                     cAtBerRate1E8, /**< Error rate is 10E-8 */
                                     cAtBerRate1E9, /**< Error rate is 10E-9 */
                                     cAtBerRate1E10 /**< Error rate is 10E-10 */
                                    };
    return berRates;
    }

static eAtBerRate *SupportedBerRatesUnderSts12c(void)
    {
    static eAtBerRate berRates[] =   {
                                     cAtBerRate1E3, /**< Error rate is 10E-3 */
                                     cAtBerRate1E4, /**< Error rate is 10E-4 */
                                     cAtBerRate1E5, /**< Error rate is 10E-5 */
                                     cAtBerRate1E6, /**< Error rate is 10E-6 */
                                     cAtBerRate1E7, /**< Error rate is 10E-7 */
                                     cAtBerRate1E8, /**< Error rate is 10E-8 */
                                     cAtBerRate1E9, /**< Error rate is 10E-9 */
                                     cAtBerRate1E10 /**< Error rate is 10E-10 */
                                    };
    return berRates;
    }

static eAtBerRate *SupportedBerRates(uint8 *numOfTypes, AtBerController berController)
    {
    eAtBerRate *berRates = IsRateOverSts12c(berController) ? SupportedBerRatesOverSts12c() : SupportedBerRatesUnderSts12c();

    *numOfTypes = mCount(berRates);
    return berRates;
    }

static void testChangeBerThreshold()
    {
    uint8 numBerRate, i;
    AtBerController berController = AtSdhChannelBerControllerGet(TestedChannel());
    eAtBerRate *berRate = SupportedBerRates(&numBerRate, berController);

    if (!BerMonitoringIsSupported())
        return;

    /* BER may be not applicable for this channel */
    if (berController == NULL)
        return;

    for (i = 0; i < numBerRate; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtBerControllerSdThresholdSet(berController, berRate[i]));
        TEST_ASSERT_EQUAL_INT(berRate[i], AtBerControllerSdThresholdGet(berController));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtBerControllerSfThresholdSet(berController, berRate[i]));
        TEST_ASSERT_EQUAL_INT(berRate[i], AtBerControllerSfThresholdGet(berController));
        }
    }

static void testEnableCompareTti()
    {
    AtSdhChannel sdhChannel = TestedChannel();

    /* TODO: Should use internal APIs to enforce TTI comparing function rather
     * than disabling logger */

    AtTestLoggerEnable(cAtFalse);

    if (AtSdhChannelTtiCompareEnable(sdhChannel, cAtTrue) != cAtErrorModeNotSupport)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelTtiCompareEnable(sdhChannel, cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtSdhChannelTtiCompareIsEnabled(sdhChannel));
        }

    if (AtSdhChannelTtiCompareEnable(sdhChannel, cAtFalse) != cAtErrorModeNotSupport)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelTtiCompareEnable(sdhChannel, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtSdhChannelTtiCompareIsEnabled(sdhChannel));
        }

    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotEnableCompareTti_WithNULLChannel()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSdhChannelTtiCompareEnable(NULL, cAtFalse) != cAtOk);
    TEST_ASSERT(AtSdhChannelTtiCompareEnable(NULL, cAtTrue) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testDefaultChannelMode()
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(TestedChannel());
    TEST_ASSERT_EQUAL_INT(AtSdhChannelModeGet((AtSdhChannel)line), AtSdhChannelModeGet(TestedChannel()));
    }

static void helperTestChannelIsSdhOrSonet(eAtSdhChannelMode mode)
    {
    if (!AtSdhChannelCanChangeMode(TestedChannel(), mode))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelModeSet(TestedChannel(), mode));
    TEST_ASSERT_EQUAL_INT(mode, AtSdhChannelModeGet(TestedChannel()));
    }

static void testChangeChannelMode()
    {
    helperTestChannelIsSdhOrSonet(cAtSdhChannelModeSdh);
    helperTestChannelIsSdhOrSonet(cAtSdhChannelModeSonet);
    helperTestChannelIsSdhOrSonet(cAtSdhChannelModeSdh);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_SdhChannel_Fixtures)
        {
        new_TestFixture("testChangeTxTti", testChangeTxTti),
        new_TestFixture("testChangeExpectedTti", testChangeExpectedTti),
        new_TestFixture("testTtiMessageSendAndReceive", testTtiMessageSendAndReceive),
        new_TestFixture("testChangeExpectedTtiLine", testChangeExpectedTtiLine),
        new_TestFixture("testTimWhenTtiMismatch", testTimWhenTtiMismatch),
        new_TestFixture("testBerMonitorEnable", testBerMonitorEnable),
        new_TestFixture("testChangeBerThreshold", testChangeBerThreshold),
        new_TestFixture("testEnableCompareTti", testEnableCompareTti),
        new_TestFixture("testCannotEnableCompareTti_WithNULLChannel", testCannotEnableCompareTti_WithNULLChannel),
        new_TestFixture("testDefaultChannelMode", testDefaultChannelMode),
        new_TestFixture("testChangeChannelMode", testChangeChannelMode),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_SdhChannel_Caller, "TestSuite_SdhChannel", setUp, NULL, TestSuite_SdhChannel_Fixtures);

    return (TestRef)((void *)&TestSuite_SdhChannel_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhChannelTestRunner);
    }

AtChannelTestRunner AtSdhChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtSdhChannelTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSdhChannelTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtModuleSdhTests.h
 * 
 * Created Date: Nov 30, 2012
 *
 * Description : SDH Test
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESDHTESTS_H_
#define _ATMODULESDHTESTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTests.h"

#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtSdhAug.h"
#include "AtSdhAu.h"
#include "AtSdhTu.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void TestSuite_SdhChannelRun(AtSdhChannel channel);

AtSdhChannel *AtSdhTestSharedChannelList(uint32 *numChannels);
uint16 AtSdhTestCreateAllAug64s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAug16s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAug4s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAug1s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAu4_64cs(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAu4_16cs(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAu4_4cs(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAu4s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllVc4_64cs(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllVc4_16cs(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllVc4_4cs(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllVc4s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllTug3(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllAu3s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllVc3sInAu3s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllVc3sInTug3s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllTug2sInTug3s(AtSdhLine line, AtSdhChannel *channels);
uint16 AtSdhTestCreateAllTug2sInVc3(AtSdhLine line, AtSdhChannel *channels);
void AtSdhTestCreateAllVc1xInTug3s(AtSdhLine line, eAtSdhChannelType vc1xType);
void AtSdhTestCreateAllVc1xInVc3s(AtSdhLine line, eAtSdhChannelType vc1xType);
uint16 AtSdhTestAllVc1xInTug2(AtSdhLine line, eAtSdhChannelType vc1xType, AtSdhChannel *vc1x);
eBool AtSdhChannelRxTtiMatch(AtSdhChannel channel, const tAtSdhTti *expectedTti);
uint8 *AtModuleSdhAllApplicableLinesAllocate(AtModuleSdh sdhModule, uint8 *numLines);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESDHTESTS_H_ */


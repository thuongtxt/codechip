/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtModuleSdhTestRunnerInternal.h
 * 
 * Created Date: Jun 23, 2014
 *
 * Description : SDH Module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESDHTESTRUNNERINTERNAL_H_
#define _ATMODULESDHTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModuleSdhTestRunner.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSdhTestRunnerMethods
    {
    AtChannelTestRunner (*CreateLineTestRunner)(AtModuleSdhTestRunner self, AtChannel line);
    AtChannelTestRunner (*CreateMapTestRunner)(AtModuleSdhTestRunner self, AtChannel line);
    AtChannelTestRunner (*CreatePathTestRunner)(AtModuleSdhTestRunner self, AtChannel path);
    AtUnittestRunner (*CreatePathInterruptTestRunner)(AtModuleSdhTestRunner self, AtChannel path);
    AtUnittestRunner (*CreatePathUpsrTestRunner)(AtModuleSdhTestRunner self, AtChannel path);

    eBool (*CanTestVc11)(AtModuleSdhTestRunner self, AtSdhLine line);
    eBool (*LineHasLoMapping)(AtModuleSdhTestRunner self, AtSdhLine line);
    eBool (*MapTypeMustBeSupported)(AtModuleSdhTestRunner self, AtSdhChannel channel, uint8 mapType);
    eBool (*ShouldTestPoh)(AtModuleSdhTestRunner self);
    void (*TestGetLine)(AtModuleSdhTestRunner self);

    eBool (*ShouldTestLine)(AtModuleSdhTestRunner self, uint8 lineId);
    eBool (*ERDIInterruptMaskSupported)(AtModuleSdhTestRunner self);

    /* Capacity constrains */
    uint32 (*NumTfi5s)(AtModuleSdhTestRunner self);
    uint32 (*TotalLines)(AtModuleSdhTestRunner self);
    eAtSdhLineRate (*MaxLineRate)(AtModuleSdhTestRunner self);

    void (*SetUp)(AtModuleSdhTestRunner self);
    eBool (*SupportTtiMode1Byte)(AtModuleSdhTestRunner self);
    eBool (*ShouldTestUpsr)(AtModuleSdhTestRunner self);
    uint8 *(*AllTestedLinesAllocate)(AtModuleSdhTestRunner self, uint8 *numLines);

    uint8 (*DefaultLineRate)(AtModuleSdhTestRunner self, uint8 lineId);
    eBool (*PathTimAutoRdiEnabledAsDefault)(AtModuleSdhTestRunner self);
    }tAtModuleSdhTestRunnerMethods;

typedef struct tAtModuleSdhTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModuleSdhTestRunnerMethods *methods;

    /* Lines can be tested */
    uint8 *allLines;
    uint8 numLines;
    }tAtModuleSdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#endif /* _ATMODULESDHTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModuleSdhTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PPP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtSdhVc.h"
#include "AtModuleSdhTests.h"
#include "AtModuleSdhTestRunnerInternal.h"
#include "../interrupt/AtChannelInterruptTestRunner.h"
#include "../../../../driver/src/generic/sdh/AtSdhPathInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleSdhTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleSdhTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannelTestRunner CreateLineTestRunner(AtModuleSdhTestRunner self, AtChannel line)
    {
    return AtSdhLineTestRunnerNew(line, (AtModuleTestRunner)self);
    }

static AtChannelTestRunner CreateMapTestRunner(AtModuleSdhTestRunner self, AtChannel line)
    {
    return AtSdhMapTestRunnerNew(line, (AtModuleTestRunner)self);
    }

static AtChannelTestRunner CreatePathTestRunner(AtModuleSdhTestRunner self, AtChannel path)
    {
    return AtSdhPathTestRunnerNew(path, (AtModuleTestRunner)self);
    }

static eBool IsAuTu(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    if ((channelType == cAtSdhChannelTypeAu3)    ||
        (channelType == cAtSdhChannelTypeAu4)    ||
        (channelType == cAtSdhChannelTypeAu4_16c)||
        (channelType == cAtSdhChannelTypeAu4_4c) ||
        (channelType == cAtSdhChannelTypeAu4_nc) ||
        (channelType == cAtSdhChannelTypeTu3)    ||
        (channelType == cAtSdhChannelTypeTu11)   ||
        (channelType == cAtSdhChannelTypeTu12))
        return cAtTrue;

    return cAtFalse;
    }

static AtUnittestRunner CreatePathInterruptTestRunner(AtModuleSdhTestRunner self, AtChannel path)
    {
    if (IsAuTu((AtSdhChannel)path))
        return AtSdhPointerProcessorInterruptTestRunnerNew(path);
    return AtSdhVcInterruptTestRunnerNew(path);
    }

static eBool ShouldTestInterrupt(AtModuleSdhTestRunner self)
    {
    if (AtTestIsSimulationTesting())
        return cAtFalse;

    return cAtTrue;
    }

static void SetUpDataPathForInterruptTestRunner(AtSdhChannel channel)
    {
    AtUnused(channel);
    }

static eBool ShouldTestUpsr(AtModuleSdhTestRunner self)
    {
    return cAtFalse;
    }

static AtUnittestRunner CreatePathUpsrTestRunner(AtModuleSdhTestRunner self, AtChannel path)
    {
    return NULL;
    }

static void helper_testAllSubPathsOfChannel(AtModuleSdhTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType)
    {
    uint8 i;
    AtSdhChannel subChannel;
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    eBool testInterrupt = ShouldTestInterrupt(self);
    eBool testUpsr = mMethodsGet(self)->ShouldTestUpsr(self);
    eBool foundTestedChannel = cAtFalse;

    for (i = 0; i < numSubChannels; i++)
        {
        /* If this is the path that needs to be tested, test it */
        subChannel = AtSdhChannelSubChannelGet(channel, i);
        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            AtUnittestRunner interruptRunner;
            AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(self)->CreatePathTestRunner(self, (AtChannel)subChannel);
            AtUnittestRunnerRun(runner);
            AtUnittestRunnerDelete(runner);
            foundTestedChannel = cAtTrue;

            if (testInterrupt == cAtFalse)
                continue;

            /* Test interrupt for VC */
            interruptRunner = mMethodsGet(self)->CreatePathInterruptTestRunner(self, (AtChannel)subChannel);
            SetUpDataPathForInterruptTestRunner(subChannel);
            AtUnittestRunnerRun(interruptRunner);
            AtUnittestRunnerDelete(interruptRunner);

            /* Test interrupt for AU/TU */
            interruptRunner = mMethodsGet(self)->CreatePathInterruptTestRunner(self, (AtChannel)channel);
            SetUpDataPathForInterruptTestRunner(channel);
            AtUnittestRunnerRun(interruptRunner);
            AtUnittestRunnerDelete(interruptRunner);
            }

        /* Otherwise, test all of its sub channels */
        else
            helper_testAllSubPathsOfChannel(self, subChannel, pathType);
        }

    /* Only support test UPSR on AU/TU layer */
    if ((testUpsr == cAtTrue) && foundTestedChannel)
        {
        AtUnittestRunner upsrRunner = (AtUnittestRunner)mMethodsGet(self)->CreatePathUpsrTestRunner(self, (AtChannel)channel);
        AtUnittestRunnerRun(upsrRunner);
        AtUnittestRunnerDelete(upsrRunner);
        }
    }

static void CreateAllVc1xInTug2(AtSdhLine line, eAtSdhChannelType vc1xType, AtSdhChannel *tug2List, uint16 numTug2s)
    {
    uint16 i;
    uint8 numSubChannels, subChannel_i;
    AtSdhChannel tu, vc;
    eAtSdhTugMapType tug2MapType;

    tug2MapType = (vc1xType == cAtSdhChannelTypeVc11) ? cAtSdhTugMapTypeTug2Map4xTu11s : cAtSdhTugMapTypeTug2Map3xTu12s;

    /* Map these TUG-2s */
    for (i = 0; i < numTug2s; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(tug2List[i], tug2MapType));
        numSubChannels = AtSdhChannelNumberOfSubChannelsGet(tug2List[i]);
        TEST_ASSERT(numSubChannels > 0);

        /* Map Ds1/E1 to VC-12s*/
        for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
            {
            tu = AtSdhChannelSubChannelGet(tug2List[i], subChannel_i);
            vc = AtSdhChannelSubChannelGet(tu, 0);
            AtAssert(vc);
            if (AtSdhChannelMapTypeIsSupported(vc, cAtSdhVcMapTypeVc1xMapDe1))
                {
                AtAssert(AtSdhChannelMapTypeSet(vc, cAtSdhVcMapTypeVc1xMapDe1) == cAtOk);
                AtAssert(AtSdhChannelMapChannelGet(vc));
                }
            }
        }
    }

static void TestAu4_64c(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllAu4_64cs(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeAu4_64c);
    }

static void TestVc4_64c(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllVc4_64cs(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeVc4_64c);
    }

static void TestAu4_16c(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllAu4_16cs(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeAu4_16c);
    }

static void TestVc4_16c(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllVc4_16cs(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeVc4_16c);
    }

static void TestAu4_4c(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllAu4_4cs(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeAu4_4c);
    }

static void TestVc4_4c(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllVc4_4cs(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeVc4_4c);
    }

static void TestAu4(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllAu4s(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeAu4);
    }

static void TestVc4(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllTug2sInTug3s(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeVc4);
    }

static void TestAu3(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllAu3s(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeAu3);
    }

static void TestVc3(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhTestCreateAllTug2sInVc3(line, AtSdhTestSharedChannelList(NULL));
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeVc3);
    }

static void TestVc3InTu3(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    if (!mMethodsGet(self)->LineHasLoMapping(self, line))
        return;

    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return;

    AtAssert(AtSdhTestCreateAllVc3sInTug3s(line, NULL) > 0);
    helper_testAllSubPathsOfChannel(self, (AtSdhChannel)line, cAtSdhChannelTypeVc3);
    }

static void TestVc12(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhChannel lineChannel = (AtSdhChannel)line;

    if (!mMethodsGet(self)->LineHasLoMapping(self, line))
        return;

    if (AtSdhLineRateGet(line) > cAtSdhLineRateStm0)
        {
    AtSdhTestCreateAllVc1xInTug3s(line, cAtSdhChannelTypeVc12);
    helper_testAllSubPathsOfChannel(self, lineChannel, cAtSdhChannelTypeVc12);
        }

    AtSdhTestCreateAllVc1xInVc3s(line, cAtSdhChannelTypeVc12);
    helper_testAllSubPathsOfChannel(self, lineChannel, cAtSdhChannelTypeVc12);
    }

static eBool CanTestVc11(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    /* Most of products so far have not been fully tested on this feature.
     * Temporary disable this testing as default and let concretes determine */
    return cAtFalse;
    }

static void TestVc11(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    AtSdhChannel lineChannel = (AtSdhChannel)line;

    if (!mMethodsGet(self)->LineHasLoMapping(self, line))
        return;

    if (AtSdhLineRateGet(line) > cAtSdhLineRateStm0)
        {
        AtSdhTestCreateAllVc1xInTug3s(line, cAtSdhChannelTypeVc11);
        helper_testAllSubPathsOfChannel(self, lineChannel, cAtSdhChannelTypeVc11);
        }

    AtSdhTestCreateAllVc1xInVc3s(line, cAtSdhChannelTypeVc11);
    helper_testAllSubPathsOfChannel(self, lineChannel, cAtSdhChannelTypeVc11);
    }

static void TestSuiteSdhPathRunForLine(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    TestAu4_64c(self, line);
    TestVc4_64c(self, line);
    TestAu4_16c(self, line);
    TestVc4_16c(self, line);
    TestAu4_4c(self, line);
    TestVc4_4c(self, line);
    TestAu4(self, line);
    TestVc4(self, line);
    TestAu3(self, line);
    TestVc3(self, line);
    TestVc3InTu3(self, line);
    TestVc12(self, line);

    if (mMethodsGet(self)->CanTestVc11(self, line))
        TestVc11(self, line);
    }

static eAtSdhLineRate *AllRates(uint8 *numRates)
    {
    static eAtSdhLineRate rates[] = {cAtSdhLineRateStm0, cAtSdhLineRateStm1, cAtSdhLineRateStm4, cAtSdhLineRateStm16};
    if (numRates)
        *numRates = mCount(rates);
    return rates;
    }

static uint8 *AllTestedLinesAllocate(AtModuleSdhTestRunner self, uint8 *numLines)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    return AtModuleSdhAllApplicableLinesAllocate(sdhModule, numLines);
    }

static uint8 *AllLines(AtModuleSdhTestRunner self, uint8 *numLines)
    {
    if (self->allLines == NULL)
        self->allLines = mMethodsGet(self)->AllTestedLinesAllocate(self, &(self->numLines));

    if (numLines)
        *numLines = self->numLines;

    return self->allLines;
    }

static void TestPath(AtModuleSdhTestRunner self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    uint8 numRates;
    eAtSdhLineRate *rates = AllRates(&numRates);
    uint8 rate_i;
    uint8 numLines;
    uint8 *testedLines = AllLines(self, &numLines);

    /* Test all lines with all supported rate */
    for (rate_i = 0; rate_i < numRates; rate_i++)
        {
        uint8 line_i;

        for (line_i = 0; line_i < numLines; line_i++)
            {
            eAtRet ret = cAtOk;
            uint8 lineId = testedLines[line_i];
            AtSdhLine line;
            eAtSdhLineRate defaultRate = mMethodsGet(self)->DefaultLineRate(self, lineId);

            if (!AtModuleSdhTestRunnerShouldTestLine(self, lineId))
                continue;

            line = AtModuleSdhLineGet(sdhModule, lineId);

            if (!AtSdhLineRateIsSupported(line, rates[rate_i]))
                continue;

            AtAssert(AtSdhLineRateSet(line, rates[rate_i]) == cAtOk);
            if (AtChannelLoopbackIsSupported((AtChannel)line, cAtLoopbackModeLocal))
                ret = AtChannelLoopbackSet((AtChannel)line, cAtLoopbackModeLocal);
            if (ret != cAtErrorModeNotSupport)
                AtAssert(ret == cAtOk);

            TestSuiteSdhPathRunForLine(self, line);

            /* Reset line rate */
            AtSdhLineRateSet(line, defaultRate);
            }
        }
    }

static void TestLine(AtModuleSdhTestRunner self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint8 line_i, numLines;
    uint8 *testedLines = AllLines(self, &numLines);
    uint8 numRates;
    eAtSdhLineRate *rates = AllRates(&numRates);
    uint8 rate_i;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        uint8 lineId = testedLines[line_i];
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        eAtSdhLineRate defaultRate = mMethodsGet(self)->DefaultLineRate(self, lineId);

        if (!AtModuleSdhTestRunnerShouldTestLine(self, lineId))
            continue;

        for (rate_i = 0; rate_i < numRates; rate_i++)
            {
            AtUnittestRunner runner;

            if (!AtSdhLineRateIsSupported(line, rates[rate_i]))
                continue;

            AtSdhLineRateSet(line, rates[rate_i]);

            /* Test super class of SDH line */
            runner = (AtUnittestRunner)mMethodsGet(self)->CreateLineTestRunner(self, (AtChannel)line);
            AtUnittestRunnerRun(runner);
            AtUnittestRunnerDelete(runner);

            /* Reset line rate */
            AtSdhLineRateSet(line, defaultRate);
            }
        }
    }

static void TestMapping(AtModuleSdhTestRunner self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    uint8 line_i;
    uint8 numRates;
    eAtSdhLineRate *rates = AllRates(&numRates);
    uint8 numLines;
    uint8 *testedLines = AllLines(self, &numLines);

    /* Run mapping for all lines */
    for (line_i = 0; line_i < numLines; line_i++)
        {
        uint8 rate_i;
        uint8 lineId;
        AtSdhLine line;
        eAtSdhLineRate defaultRate;

        lineId = testedLines[line_i];
        if (!AtModuleSdhTestRunnerShouldTestLine(self, lineId))
            continue;

        defaultRate = mMethodsGet(self)->DefaultLineRate(self, lineId);
        line = AtModuleSdhLineGet(sdhModule, lineId);

        for (rate_i = 0; rate_i < numRates; rate_i++)
            {
            AtUnittestRunner runner;

            if (!AtSdhLineRateIsSupported(line, rates[rate_i]))
                continue;

            AtSdhLineRateSet(line, rates[rate_i]);
            runner = (AtUnittestRunner)mMethodsGet(self)->CreateMapTestRunner(self, (AtChannel)line);
            AtUnittestRunnerRun(runner);
            AtUnittestRunnerDelete(runner);

            /* Reset line rate */
            AtSdhLineRateSet(line, defaultRate);
            }
        }
    }

static AtModuleSdh Module()
    {
    return (AtModuleSdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(Module());
    AtModuleInit((AtModule)Module());
    }

static void tearDown()
    {
    }

static void testCanGetMaxNumberOfLines()
    {
    TEST_ASSERT(AtModuleSdhMaxLinesGet(Module()) > 0);
    }

static void testGetLine()
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner)AtUnittestRunnerCurrentRunner();
    mMethodsGet(runner)->TestGetLine(runner);
    }

static void testNumberOfTfi5LinesMustBeEnough(void)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner)AtUnittestRunnerCurrentRunner();
    uint32 numTfi5s = mMethodsGet(runner)->NumTfi5s(runner);
    TEST_ASSERT_EQUAL_INT(numTfi5s, AtModuleSdhNumTfi5Lines(Module()));
    }

static void testTotalLinesMustBeEnough(void)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner)AtUnittestRunnerCurrentRunner();
    uint32 numLines = mMethodsGet(runner)->TotalLines(runner);
    TEST_ASSERT_EQUAL_INT(numLines, AtModuleSdhMaxLinesGet(Module()));
    }

static void testMaxLineRate(void)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner)AtUnittestRunnerCurrentRunner();
    eAtSdhLineRate maxRate = mMethodsGet(runner)->MaxLineRate(runner);
    if (maxRate == cAtSdhLineRateInvalid)
        {
        AtPrintc(cSevWarning, "WARNING: Max line rate should be defined\r\n");
        return;
        }

    TEST_ASSERT_EQUAL_INT(maxRate, AtModuleSdhMaxLineRate(Module()));
    }

static TestRef _TestSuite()
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleSdh_Fixtures)
        {
        new_TestFixture("testCanGetMaxNumberOfLines", testCanGetMaxNumberOfLines),
        new_TestFixture("testGetLine", testGetLine),
        new_TestFixture("testNumberOfTfi5LinesMustBeEnough", testNumberOfTfi5LinesMustBeEnough),
        new_TestFixture("testTotalLinesMustBeEnough", testTotalLinesMustBeEnough),
        new_TestFixture("testMaxLineRate", testMaxLineRate)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleSdh_Caller, "TestSuite_ModuleSdh", setUp, tearDown, TestSuite_ModuleSdh_Fixtures);

    return (TestRef)((void *)&TestSuite_ModuleSdh_Caller);
    }

static void TestModule(AtModuleSdhTestRunner self)
    {
    TextUIRunner_runTest(_TestSuite());
    }

static void SetUp(AtModuleSdhTestRunner self)
    {
    }

static void Run(AtUnittestRunner self)
    {
    AtModuleSdhTestRunner runner = (AtModuleSdhTestRunner)self;

    mMethodsGet(runner)->SetUp(runner);
    m_AtUnittestRunnerMethods->Run(self);

    TestModule(runner);
    TestLine(runner);
    TestMapping(runner);
    TestPath(runner);
    }

static eBool LineHasLoMapping(AtModuleSdhTestRunner self, AtSdhLine line)
    {
    /* For some lines, LO mapping may be not applicable. But most of them have
     * this mapping */
    return cAtTrue;
    }

static eBool PathHasPohProcessor(AtModuleSdhTestRunner self, AtSdhPath path)
    {
    return AtSdhPathHasPohProcessor(path);
    }

static eBool MapTypeMustBeSupported(AtModuleSdhTestRunner self, AtSdhChannel channel, uint8 mapType)
    {
    /* Let's assume all of valid mapping type must be support and let concrete
     * product override this */
    return cAtTrue;
    }

static eBool ShouldTestPoh(AtModuleSdhTestRunner self)
    {
    /* During development, this module may be not available, this method is used
     * to temporary bypass it */
    return cAtTrue;
    }

static eBool ShouldTestLine(AtModuleSdhTestRunner self, uint8 lineId)
    {
    return cAtTrue;
    }

static uint32 NumTfi5s(AtModuleSdhTestRunner self)
    {
    return 0;
    }

static uint32 TotalLines(AtModuleSdhTestRunner self)
    {
    return 0;
    }

static eBool ERDIInterruptMaskSupported(AtModuleSdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool SupportTtiMode1Byte(AtModuleSdhTestRunner self)
    {
    return cAtTrue;
    }

static void TestGetLine(AtModuleSdhTestRunner self)
    {
    uint8 maxLines, i;

    maxLines = AtModuleSdhMaxLinesGet(Module());
    for (i = 0; i < maxLines; i++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleSdhLineGet(Module(), i));
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)AtModuleSdhLineGet(Module(), i)));
        }
    }

static eAtSdhLineRate MaxLineRate(AtModuleSdhTestRunner self)
    {
    return cAtSdhLineRateInvalid;
    }

static void Delete(AtUnittestRunner self)
    {
    AtOsalMemFree(mThis(self)->allLines);
    m_AtUnittestRunnerMethods->Delete(self);
    }

static uint8 DefaultLineRate(AtModuleSdhTestRunner self, uint8 lineId)
    {
    return cAtSdhLineRateStm1;
    }

static eBool PathTimAutoRdiEnabledAsDefault(AtModuleSdhTestRunner self)
    {
    return cAtFalse;
    }

static void MethodsInit(AtModuleSdhTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CreateLineTestRunner);
        mMethodOverride(m_methods, CreateMapTestRunner);
        mMethodOverride(m_methods, CreatePathTestRunner);
        mMethodOverride(m_methods, CanTestVc11);
        mMethodOverride(m_methods, LineHasLoMapping);
        mMethodOverride(m_methods, MapTypeMustBeSupported);
        mMethodOverride(m_methods, ShouldTestPoh);
        mMethodOverride(m_methods, ShouldTestLine);
        mMethodOverride(m_methods, NumTfi5s);
        mMethodOverride(m_methods, TotalLines);
        mMethodOverride(m_methods, ERDIInterruptMaskSupported);
        mMethodOverride(m_methods, CreatePathInterruptTestRunner);
        mMethodOverride(m_methods, SetUp);
        mMethodOverride(m_methods, SupportTtiMode1Byte);
        mMethodOverride(m_methods, ShouldTestUpsr);
        mMethodOverride(m_methods, CreatePathUpsrTestRunner);
        mMethodOverride(m_methods, TestGetLine);
        mMethodOverride(m_methods, MaxLineRate);
        mMethodOverride(m_methods, AllTestedLinesAllocate);
        mMethodOverride(m_methods, DefaultLineRate);
        mMethodOverride(m_methods, PathTimAutoRdiEnabledAsDefault);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleSdhTestRunner);
    }

AtModuleTestRunner AtModuleSdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((AtModuleSdhTestRunner)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModuleSdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModuleSdhTestRunnerObjectInit(newRunner, module);
    }

void AtSdhTestCreateAllVc1xInTug3s(AtSdhLine line, eAtSdhChannelType vc1xType)
    {
    uint16 numTug2s;
    numTug2s = AtSdhTestCreateAllTug2sInTug3s(line, AtSdhTestSharedChannelList(NULL));
    AtAssert(numTug2s > 0);
    CreateAllVc1xInTug2(line, vc1xType, AtSdhTestSharedChannelList(NULL), numTug2s);
    }

void AtSdhTestCreateAllVc1xInVc3s(AtSdhLine line, eAtSdhChannelType vc1xType)
    {
    uint16 numTug2s;

    numTug2s = AtSdhTestCreateAllTug2sInVc3(line, AtSdhTestSharedChannelList(NULL));
    CreateAllVc1xInTug2(line, vc1xType, AtSdhTestSharedChannelList(NULL), numTug2s);
    }

eBool AtModuleSdhTestRunnerPathHasPohProcessor(AtModuleSdhTestRunner self, AtSdhPath path)
    {
    if (self)
        return PathHasPohProcessor(self, path);
    return cAtFalse;
    }

eBool AtModuleSdhTestRunnerMapTypeMustBeSupported(AtModuleSdhTestRunner self, AtSdhChannel channel, uint8 mapType)
    {
    if (self)
        return mMethodsGet(self)->MapTypeMustBeSupported(self, channel, mapType);
    return cAtFalse;
    }

eBool AtModuleSdhTestRunnerShouldTestPoh(AtModuleSdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->ShouldTestPoh(self);
    return cAtFalse;
    }

eBool AtModuleSdhTestRunnerShouldTestLine(AtModuleSdhTestRunner self, uint8 lineId)
    {
    if (self)
        return mMethodsGet(self)->ShouldTestLine(self, lineId);
    return cAtFalse;
    }

eBool AtModuleSdhTestRunnerERDIInterruptMaskSupported(AtModuleSdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->ERDIInterruptMaskSupported(self);
    return cAtFalse;
    }

eBool AtModuleSdhTestRunnerSupportTtiMode1Byte(AtModuleSdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->SupportTtiMode1Byte(self);
    return cAtFalse;
    }

uint8 *AtModuleSdhAllApplicableLinesAllocate(AtModuleSdh sdhModule, uint8 *numLines)
    {
    uint8 *lineIds, line_i;
    uint32 memorySize;

    *numLines = AtModuleSdhMaxLinesGet(sdhModule);
    memorySize = sizeof(uint8) * (*numLines);
    lineIds = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(lineIds, 0, memorySize);

    for (line_i = 0; line_i < *numLines; line_i++)
        lineIds[line_i] = line_i;

    return lineIds;
    }

eBool AtModuleSdhTestRunnerPathTimAutoRdiEnabledAsDefault(AtModuleSdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->PathTimAutoRdiEnabledAsDefault(self);
    return cAtFalse;
    }

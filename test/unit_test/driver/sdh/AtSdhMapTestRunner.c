/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhMapRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : SDH channel test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtModuleSdhTests.h"
#include "AtSdhChannelTestRunnerInternal.h"
#include "AtModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhMapTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtModuleSdhRet AtSdhChannelCanChangeMapping(AtSdhChannel self, uint8 mapType);

/*--------------------------- Implementation ---------------------------------*/
static AtSdhLine TestedLine()
    {
    return (AtSdhLine)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static AtModuleSdhTestRunner ModuleRunner(void)
    {
    return (AtModuleSdhTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eBool MapTypeMustBeSupported(AtSdhChannel channel, uint8 mapType)
    {
    return AtModuleSdhTestRunnerMapTypeMustBeSupported(ModuleRunner(), channel, mapType);
    }

static void testAugOfLineMustBeValid()
    {
    eAtSdhLineRate rate;
    eAtSdhChannelType augType;
    AtSdhChannel aug;

    /* Get AUG */
    TEST_ASSERT(AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)TestedLine()) == 1);
    aug = AtSdhChannelSubChannelGet((AtSdhChannel)TestedLine(), 0);
    TEST_ASSERT_NOT_NULL(aug);
    TEST_ASSERT_NOT_NULL(AtChannelModuleGet((AtChannel)aug));

    /* Its type must be valid */
    rate = AtSdhLineRateGet(TestedLine());
    augType = AtSdhChannelTypeGet(aug);
    if (rate == cAtSdhLineRateStm16)
        {
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAug16, augType);
        }
    else if (rate == cAtSdhLineRateStm4)
        {
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAug4, augType);
        }
    else if (rate == cAtSdhLineRateStm1)
        {
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAug1, augType);
        }
    else if (rate == cAtSdhLineRateStm0)
        return;
    /* Invalid line rate */
    else
        TEST_FAIL("Line rate is invalid");

    /* Check ID conversion */
    TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)aug));
    TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(aug));
    TEST_ASSERT_EQUAL_INT(0, AtSdhChannelSts1Get(aug));
    }

static void testAug16MapVc4_16c()
    {
    uint8 mapType = cAtSdhAugMapTypeAug16MapVc4_16c;
    AtSdhChannel aug = (AtSdhChannel)AtSdhLineAug16Get(TestedLine(), 0);
    AtSdhChannel au, vc;

    /* Ignore if AUG-16 does not exist */
    if (aug == NULL)
        return;

    if (!MapTypeMustBeSupported((AtSdhChannel)aug, mapType))
        {
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtSdhChannelMapTypeIsSupported((AtSdhChannel)aug, mapType));
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSdhChannelMapTypeSet((AtSdhChannel)aug, mapType));
        return;
        }

    /* Map it */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet((AtSdhChannel)aug, mapType));
    TEST_ASSERT_EQUAL_INT(mapType, AtSdhChannelMapTypeGet((AtSdhChannel)aug));

    /* Sub-channel of AUG must be AU-4-16c */
    TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(aug));
    TEST_ASSERT_NOT_NULL((au = AtSdhChannelSubChannelGet(aug, 0)));
    TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAu4_16c, AtSdhChannelTypeGet(au));
    TEST_ASSERT(AtSdhChannelParentChannelGet(au) == aug);
    TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)au));

    /* Check ID conversion */
    TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(au));
    TEST_ASSERT_EQUAL_INT(0, AtSdhChannelSts1Get(au));

    /* And sub-channel of AU must be VC-4-16c */
    TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(au));
    TEST_ASSERT_NOT_NULL((vc = AtSdhChannelSubChannelGet(au, 0)));
    TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeVc4_16c, AtSdhChannelTypeGet(vc));
    TEST_ASSERT(AtSdhChannelParentChannelGet(vc) == au);
    TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc));

    /* Check ID conversion */
    TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(vc));
    TEST_ASSERT_EQUAL_INT(0, AtSdhChannelSts1Get(vc));
    }

static void testAug16Map4xAug4s()
    {
    uint8 mapType = cAtSdhAugMapTypeAug16Map4xAug4s;
    AtSdhChannel aug16 = (AtSdhChannel)AtSdhLineAug16Get(TestedLine(), 0);
    AtSdhChannel aug4;
    static const uint8 numAug4sInAug16 = 4;
    uint8 i, expectedSts;

    if (AtSdhLineRateGet(TestedLine()) < cAtSdhLineRateStm16)
        return;

    TEST_ASSERT_NOT_NULL(aug16);

    /* Also ignore if this AUG-16 does not support VC mapping */
    if (!MapTypeMustBeSupported((AtSdhChannel)aug16, mapType))
        return;

    /* Map it */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet((AtSdhChannel)aug16, mapType));
    TEST_ASSERT_EQUAL_INT(mapType, AtSdhChannelMapTypeGet((AtSdhChannel)aug16));

    /* Sub-channels of AUG-16 must be AUG-4s */
    TEST_ASSERT_EQUAL_INT(numAug4sInAug16, AtSdhChannelNumberOfSubChannelsGet(aug16));
    for (i = 0; i < numAug4sInAug16; i++)
        {
        aug4 = AtSdhChannelSubChannelGet(aug16, i);
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAug4, AtSdhChannelTypeGet(aug4));
        TEST_ASSERT(AtSdhChannelParentChannelGet(aug4) == aug16);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)aug4));

        /* Check ID conversion */
        expectedSts = i * 12;
        TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(aug4));
        TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(aug4));
        }
    }

static void testAug4MapVc4_4c()
    {
    uint8 numChannel, i;
    AtSdhChannel vc;
    uint8 expectedSts;

    /* Create all AUG-4 first */
    numChannel = AtSdhTestCreateAllAug4s(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numChannel == 0)
        return;

    /* Map these AUG-4s */
    for (i = 0; i < numChannel; i++)
        {
        AtSdhChannel channel = AtSdhTestSharedChannelList(NULL)[i];

        if (!MapTypeMustBeSupported(channel, cAtSdhAugMapTypeAug4MapVc4_4c))
            {
            TEST_ASSERT_EQUAL_INT(cAtFalse, AtSdhChannelMapTypeIsSupported(channel, cAtSdhAugMapTypeAug4MapVc4_4c));
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSdhChannelMapTypeSet(channel, cAtSdhAugMapTypeAug4MapVc4_4c));
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(channel, cAtSdhAugMapTypeAug4MapVc4_4c));
        TEST_ASSERT_EQUAL_INT(cAtSdhAugMapTypeAug4MapVc4_4c, AtSdhChannelMapTypeGet(channel));

        /* Check sub channel */
        TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(channel));
        vc = AtSdhChannelSubChannelGet(channel, 0);
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAu4_4c, AtSdhChannelTypeGet(vc));
        TEST_ASSERT(AtSdhChannelParentChannelGet(vc) == channel);
        TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc));

        /* Check ID conversion */
        expectedSts = AtChannelIdGet((AtChannel)channel) * 12;
        TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(channel));
        TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(channel));
        }
    }

static void testAug4Map4xAug1s()
    {
    uint8 numChannel, i, j;
    AtSdhChannel aug1;
    uint8 expectedSts;

    /* Create all AUG-4 first */
    numChannel = AtSdhTestCreateAllAug4s(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numChannel == 0)
        return;

    /* Map these AUG-4s */
    for (i = 0; i < numChannel; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(AtSdhTestSharedChannelList(NULL)[i], cAtSdhAugMapTypeAug4Map4xAug1s));
        TEST_ASSERT_EQUAL_INT(cAtSdhAugMapTypeAug4Map4xAug1s, AtSdhChannelMapTypeGet(AtSdhTestSharedChannelList(NULL)[i]));

        /* Check sub channel */
        TEST_ASSERT_EQUAL_INT(4, AtSdhChannelNumberOfSubChannelsGet(AtSdhTestSharedChannelList(NULL)[i]));
        for (j = 0; j < 4; j++)
            {
            aug1 = AtSdhChannelSubChannelGet(AtSdhTestSharedChannelList(NULL)[i], j);
            TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAug1, AtSdhChannelTypeGet(aug1));
            TEST_ASSERT(AtSdhChannelParentChannelGet(aug1) == AtSdhTestSharedChannelList(NULL)[i]);
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)aug1));

            /* Check ID conversion */
            expectedSts = AtSdhChannelSts1Get(AtSdhTestSharedChannelList(NULL)[i]) + (j * 3);
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(aug1));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(aug1));
            }
        }
    }

static void testAug1MapVc4()
    {
    uint8 numChannel, i;
    AtSdhChannel vc;
    uint8 expectedSts;

    /* Create all AUG-1 first */
    numChannel = AtSdhTestCreateAllAug1s(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numChannel == 0)
        return;

    /* Map these AUG-1s */
    for (i = 0; i < numChannel; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(AtSdhTestSharedChannelList(NULL)[i], cAtSdhAugMapTypeAug1MapVc4));
        TEST_ASSERT_EQUAL_INT(cAtSdhAugMapTypeAug1MapVc4, AtSdhChannelMapTypeGet(AtSdhTestSharedChannelList(NULL)[i]));

        /* Check sub channel */
        TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(AtSdhTestSharedChannelList(NULL)[i]));
        vc = AtSdhChannelSubChannelGet(AtSdhTestSharedChannelList(NULL)[i], 0);
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAu4, AtSdhChannelTypeGet(vc));
        TEST_ASSERT(AtSdhChannelParentChannelGet(vc) == AtSdhTestSharedChannelList(NULL)[i]);
        TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc));

        /* Check ID conversion */
        expectedSts = AtSdhChannelSts1Get(AtSdhTestSharedChannelList(NULL)[i]);
        TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(vc));
        TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(vc));
        }
    }

static void testAug1Map3xVc3s()
    {
    uint8 numChannel, i, j;
    AtSdhChannel au3, vc;
    uint8 expectedSts;

    /* Create all AUG-1 first */
    numChannel = AtSdhTestCreateAllAug1s(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numChannel == 0)
        return;

    /* Map these AUG-1s */
    for (i = 0; i < numChannel; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(AtSdhTestSharedChannelList(NULL)[i], cAtSdhAugMapTypeAug1Map3xVc3s));
        TEST_ASSERT_EQUAL_INT(cAtSdhAugMapTypeAug1Map3xVc3s, AtSdhChannelMapTypeGet(AtSdhTestSharedChannelList(NULL)[i]));

        /* Check sub channel */
        TEST_ASSERT_EQUAL_INT(3, AtSdhChannelNumberOfSubChannelsGet(AtSdhTestSharedChannelList(NULL)[i]));
        for (j = 0; j < 3; j++)
            {
            au3 = AtSdhChannelSubChannelGet(AtSdhTestSharedChannelList(NULL)[i], j);
            TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeAu3, AtSdhChannelTypeGet(au3));
            TEST_ASSERT(AtSdhChannelParentChannelGet(au3) == AtSdhTestSharedChannelList(NULL)[i]);
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)au3));

            /* Check ID conversion */
            expectedSts = AtSdhChannelSts1Get(AtSdhTestSharedChannelList(NULL)[i]) + j;
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(au3));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(au3));

            /* Sub channel of AU-3 must be VC-3 */
            TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(au3));
            vc = AtSdhChannelSubChannelGet(au3, 0);
            TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeVc3, AtSdhChannelTypeGet(vc));
            TEST_ASSERT(AtSdhChannelParentChannelGet(vc) == au3);
            TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc));

            /* Check ID conversion of VC */
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(vc));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(vc));
            }
        }
    }

static void testVc4Map3xTug3s()
    {
    uint8 numChannel, i, j;
    AtSdhChannel tug3;
    uint8 expectedSts;

    /* Create all VC-4 first */
    numChannel = AtSdhTestCreateAllVc4s(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numChannel == 0)
        return;

    /* Map these VC-4s */
    for (i = 0; i < numChannel; i++)
        {
        AtSdhChannel channel = AtSdhTestSharedChannelList(NULL)[i];

        if (!MapTypeMustBeSupported(channel, cAtSdhVcMapTypeVc4Map3xTug3s))
            {
            TEST_ASSERT_EQUAL_INT(cAtFalse, AtSdhChannelMapTypeIsSupported(channel, cAtSdhVcMapTypeVc4Map3xTug3s));
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSdhChannelMapTypeSet(channel, cAtSdhVcMapTypeVc4Map3xTug3s));
            continue;
            }

        if (AtSdhChannelCanChangeMapping(channel, cAtSdhVcMapTypeVc4Map3xTug3s) != cAtOk)
            {
            TEST_ASSERT(AtSdhChannelMapTypeSet(channel, cAtSdhVcMapTypeVc4Map3xTug3s) != cAtOk);
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(channel, cAtSdhVcMapTypeVc4Map3xTug3s));
        TEST_ASSERT_EQUAL_INT(cAtSdhVcMapTypeVc4Map3xTug3s, AtSdhChannelMapTypeGet(channel));

        /* Check sub channels */
        TEST_ASSERT_EQUAL_INT(3, AtSdhChannelNumberOfSubChannelsGet(channel));
        for (j = 0; j < 3; j++)
            {
            tug3 = AtSdhChannelSubChannelGet(channel, j);
            TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeTug3, AtSdhChannelTypeGet(tug3));
            TEST_ASSERT(AtSdhChannelParentChannelGet(tug3) == channel);
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)tug3));

            /* Check ID conversion */
            expectedSts = AtSdhChannelSts1Get(channel) + j;
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(tug3));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(tug3));
            }
        }
    }

static void testTug3MapVc3()
    {
    uint8 numTug3s, i;
    AtSdhChannel vc3, tu3;
    uint8 expectedSts;

    /* Create all TUG-3s */
    numTug3s = AtSdhTestCreateAllTug3(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numTug3s == 0)
        return;

    /* Map them */
    for (i = 0; i < numTug3s; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(AtSdhTestSharedChannelList(NULL)[i], cAtSdhTugMapTypeTug3MapVc3));
        TEST_ASSERT_EQUAL_INT(cAtSdhTugMapTypeTug3MapVc3, AtSdhChannelMapTypeGet(AtSdhTestSharedChannelList(NULL)[i]));

        /* Check sub channels */
        TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(AtSdhTestSharedChannelList(NULL)[i]));
        tu3 = AtSdhChannelSubChannelGet(AtSdhTestSharedChannelList(NULL)[i], 0);
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeTu3, AtSdhChannelTypeGet(tu3));
        TEST_ASSERT(AtSdhChannelParentChannelGet(tu3) == AtSdhTestSharedChannelList(NULL)[i]);
        TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)tu3));

        vc3 = AtSdhChannelSubChannelGet(tu3, 0);
        TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeVc3, AtSdhChannelTypeGet(vc3));
        TEST_ASSERT(AtSdhChannelParentChannelGet(vc3) == tu3);
        TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc3));

        /* Check ID conversion */
        expectedSts = AtSdhChannelSts1Get(AtSdhTestSharedChannelList(NULL)[i]);
        TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(vc3));
        TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(vc3));

        /* Cannot map VC-3 to TUG-2 */
        TEST_ASSERT(AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s) != cAtOk);
        }
    }

static void testTug3MapTug2()
    {
    uint8 numTug3s, i, j;
    AtSdhChannel tug2;
    uint8 expectedSts;

    /* Create all TUG-3s */
    numTug3s = AtSdhTestCreateAllTug3(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numTug3s == 0)
        return;

    /* Map them */
    for (i = 0; i < numTug3s; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(AtSdhTestSharedChannelList(NULL)[i], cAtSdhTugMapTypeTug3Map7xTug2s));
        TEST_ASSERT_EQUAL_INT(cAtSdhTugMapTypeTug3Map7xTug2s, AtSdhChannelMapTypeGet(AtSdhTestSharedChannelList(NULL)[i]));

        /* Check sub channels */
        TEST_ASSERT_EQUAL_INT(7, AtSdhChannelNumberOfSubChannelsGet(AtSdhTestSharedChannelList(NULL)[i]));
        for (j = 0; j < 7; j++)
            {
            tug2 = AtSdhChannelSubChannelGet(AtSdhTestSharedChannelList(NULL)[i], j);
            TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeTug2, AtSdhChannelTypeGet(tug2));
            TEST_ASSERT(AtSdhChannelParentChannelGet(tug2) == AtSdhTestSharedChannelList(NULL)[i]);
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)tug2));

            /* Check ID conversion */
            expectedSts = AtSdhChannelSts1Get(AtSdhTestSharedChannelList(NULL)[i]);
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(tug2));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(tug2));
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)tug2));
            }
        }
    }

static void testVc3InAu3MapTug2()
    {
    uint8 numVc3s, i, j;
    AtSdhChannel tug2;
    uint8 expectedSts;

    /* Create VC-3s mapped to AU-3 first */
    numVc3s = AtSdhTestCreateAllVc3sInAu3s(TestedLine(), AtSdhTestSharedChannelList(NULL));
    if (numVc3s == 0)
        return;

    /* Map them */
    for (i = 0; i < numVc3s; i++)
        {
        AtSdhChannel channel = AtSdhTestSharedChannelList(NULL)[i];

        if (!MapTypeMustBeSupported(channel, cAtSdhVcMapTypeVc3Map7xTug2s))
            {
            TEST_ASSERT(!AtSdhChannelMapTypeIsSupported(channel, cAtSdhVcMapTypeVc3Map7xTug2s));
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSdhChannelMapTypeSet(channel, cAtSdhVcMapTypeVc3Map7xTug2s));
            continue;
            }

        if (AtSdhChannelCanChangeMapping(channel, cAtSdhVcMapTypeVc3Map7xTug2s) != cAtOk)
            {
            TEST_ASSERT(AtSdhChannelMapTypeSet(channel, cAtSdhVcMapTypeVc3Map7xTug2s) != cAtOk);
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(channel, cAtSdhVcMapTypeVc3Map7xTug2s));
        TEST_ASSERT_EQUAL_INT(cAtSdhVcMapTypeVc3Map7xTug2s, AtSdhChannelMapTypeGet(channel));

        /* Check sub channels */
        TEST_ASSERT_EQUAL_INT(7, AtSdhChannelNumberOfSubChannelsGet(channel));
        for (j = 0; j < 7; j++)
            {
            tug2 = AtSdhChannelSubChannelGet(channel, j);
            TEST_ASSERT_EQUAL_INT(cAtSdhChannelTypeTug2, AtSdhChannelTypeGet(tug2));
            TEST_ASSERT(AtSdhChannelParentChannelGet(tug2) == channel);
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)tug2));

            /* Check ID conversion */
            expectedSts = AtSdhChannelSts1Get(channel);
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(tug2));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(tug2));
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)tug2));
            }
        }
    }

static uint8 NumberOfTuInTug2(uint8 tug2MapType)
    {
    if (tug2MapType == cAtSdhTugMapTypeTug2Map3xTu12s)
        return 3;
    if (tug2MapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        return 4;

    return 0;
    }

static uint8 TuTypeFromTug2MapType(uint8 tug2MapType)
    {
    if (tug2MapType == cAtSdhTugMapTypeTug2Map3xTu12s)
        return cAtSdhChannelTypeTu12;
    if (tug2MapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        return cAtSdhChannelTypeTu11;

    return cAtSdhChannelTypeUnknown;
    }

static uint8 VcTypeFromTuType(uint8 tuType)
    {
    if (tuType == cAtSdhChannelTypeTu12)
        return cAtSdhChannelTypeVc12;
    if (tuType == cAtSdhChannelTypeTu11)
        return cAtSdhChannelTypeVc11;
    if (tuType == cAtSdhChannelTypeTu3)
        return cAtSdhChannelTypeVc3;

    return cAtSdhChannelTypeUnknown;
    }

static void helper_testTug2MapTu1x(AtSdhChannel *tug2s, uint16 numChannels, uint8 tug2MapType)
    {
    uint16 i, j;
    AtSdhChannel tu, vc;
    uint8 numTus = NumberOfTuInTug2(tug2MapType);
    uint8 expectedSts;

    /* Map them */
    for (i = 0; i < numChannels; i++)
        {
        if (!AtSdhChannelMapTypeIsSupported(tug2s[i], tug2MapType))
            continue;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(tug2s[i], tug2MapType));
        TEST_ASSERT_EQUAL_INT(tug2MapType, AtSdhChannelMapTypeGet(tug2s[i]));

        /* Check sub channels */
        TEST_ASSERT_EQUAL_INT(numTus, AtSdhChannelNumberOfSubChannelsGet(tug2s[i]));
        for (j = 0; j < numTus; j++)
            {
            tu = AtSdhChannelSubChannelGet(tug2s[i], j);
            TEST_ASSERT_EQUAL_INT(TuTypeFromTug2MapType(tug2MapType), AtSdhChannelTypeGet(tu));
            TEST_ASSERT(AtSdhChannelParentChannelGet(tu) == tug2s[i]);
            TEST_ASSERT_EQUAL_INT(j, AtChannelIdGet((AtChannel)tu));

            /* Check ID conversion of TU */
            expectedSts = AtSdhChannelSts1Get(tug2s[i]);
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(tu));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(tu));
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)tug2s[i]), AtSdhChannelTug2Get(tu));
            TEST_ASSERT_EQUAL_INT(j, AtSdhChannelTu1xGet(tu));

            /* Check sub channel of this TU */
            TEST_ASSERT_EQUAL_INT(1, AtSdhChannelNumberOfSubChannelsGet(tu));
            vc = AtSdhChannelSubChannelGet(tu, 0);
            TEST_ASSERT_EQUAL_INT(VcTypeFromTuType(TuTypeFromTug2MapType(tug2MapType)), AtSdhChannelTypeGet(vc));
            TEST_ASSERT(AtSdhChannelParentChannelGet(vc) == tu);
            TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc));

            /* Check ID conversion of VC */
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)TestedLine()), AtSdhChannelLineGet(vc));
            TEST_ASSERT_EQUAL_INT(expectedSts, AtSdhChannelSts1Get(vc));
            TEST_ASSERT_EQUAL_INT(AtChannelIdGet((AtChannel)tug2s[i]), AtSdhChannelTug2Get(vc));
            TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)vc));
            TEST_ASSERT_EQUAL_INT(j, AtSdhChannelTu1xGet(vc));
            }
        }
    }

static void testTug2InTug3MapTu1x()
    {
    uint16 numChannels;

    /* Create TUG-2s */
    if ((numChannels = AtSdhTestCreateAllTug2sInTug3s(TestedLine(), AtSdhTestSharedChannelList(NULL))) == 0)
        return;

    helper_testTug2MapTu1x(AtSdhTestSharedChannelList(NULL), numChannels, cAtSdhTugMapTypeTug2Map3xTu12s);
    helper_testTug2MapTu1x(AtSdhTestSharedChannelList(NULL), numChannels, cAtSdhTugMapTypeTug2Map4xTu11s);
    }

static void testTug2InVc3MapTu1x()
    {
    uint16 numChannels;

    /* Create TUG-2s */
    if ((numChannels = AtSdhTestCreateAllTug2sInVc3(TestedLine(), AtSdhTestSharedChannelList(NULL))) == 0)
        return;

    helper_testTug2MapTu1x(AtSdhTestSharedChannelList(NULL), numChannels, cAtSdhTugMapTypeTug2Map3xTu12s);
    helper_testTug2MapTu1x(AtSdhTestSharedChannelList(NULL), numChannels, cAtSdhTugMapTypeTug2Map4xTu11s);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_SdhMapping_Fixtures)
        {
        new_TestFixture("testAugOfLineMustBeValid", testAugOfLineMustBeValid),
        new_TestFixture("testAug16MapVc4_16c", testAug16MapVc4_16c),
        new_TestFixture("testAug16Map4xAug4s", testAug16Map4xAug4s),
        new_TestFixture("testAug4MapVc4_4c", testAug4MapVc4_4c),
        new_TestFixture("testAug4Map4xAug1s", testAug4Map4xAug1s),
        new_TestFixture("testAug1MapVc4", testAug1MapVc4),
        new_TestFixture("testAug1Map3xVc3s", testAug1Map3xVc3s),
        new_TestFixture("testVc4Map3xTug3s", testVc4Map3xTug3s),
        new_TestFixture("testTug3MapVc3", testTug3MapVc3),
        new_TestFixture("testTug3MapTug2", testTug3MapTug2),
        new_TestFixture("testVc3InAu3MapTug2", testVc3InAu3MapTug2),
        new_TestFixture("testTug2InTug3MapTu1x", testTug2InTug3MapTu1x),
        new_TestFixture("testTug2InVc3MapTu1x", testTug2InVc3MapTu1x),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_SdhMapping_Caller, "TestSuite_SdhMapping", NULL, NULL, TestSuite_SdhMapping_Fixtures);

    return (TestRef)((void *)&TestSuite_SdhMapping_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhMapRunner);
    }

AtChannelTestRunner AtSdhMapTestRunnerObjectInit(AtChannelTestRunner self, AtChannel line, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhChannelTestRunnerObjectInit(self, line, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtSdhMapTestRunnerNew(AtChannel line, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSdhMapTestRunnerObjectInit(newRunner, line, moduleRunner);
    }

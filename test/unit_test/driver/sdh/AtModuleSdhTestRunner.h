/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtModuleSdhTestRunner.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : SDH test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESDHTESTRUNNER_H_
#define _ATMODULESDHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSdhTestRunner * AtModuleSdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AtModuleSdhTestRunnerPathHasPohProcessor(AtModuleSdhTestRunner self, AtSdhPath path);
eBool AtModuleSdhTestRunnerMapTypeMustBeSupported(AtModuleSdhTestRunner self, AtSdhChannel channel, uint8 mapType);
eBool AtModuleSdhTestRunnerShouldTestPoh(AtModuleSdhTestRunner self);
eBool AtModuleSdhTestRunnerShouldTestLine(AtModuleSdhTestRunner self, uint8 lineId);
eBool AtModuleSdhTestRunnerERDIInterruptMaskSupported(AtModuleSdhTestRunner self);
eBool AtModuleSdhTestRunnerSupportTtiMode1Byte(AtModuleSdhTestRunner self);
eBool AtModuleSdhTestRunnerPathTimAutoRdiEnabledAsDefault(AtModuleSdhTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESDHTESTRUNNER_H_ */


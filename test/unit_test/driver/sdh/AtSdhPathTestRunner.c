/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhPathTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : SDH channel test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <assert.h>
#include "atclib.h"
#include "AtTests.h"
#include "AtSdhLine.h"
#include "AtModuleSdhTests.h"
#include "AtSdhChannelTestRunnerInternal.h"
#include "AtModuleSdhTestRunner.h"
#include "../../../../driver/src/generic/sdh/AtSdhPathInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;
static tAtChannelTestRunnerMethods m_AtChannelTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhPath TestedPath()
    {
    return (AtSdhPath)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eBool HasPohProcessor()
    {
    AtChannelTestRunner channelRunner = (AtChannelTestRunner)AtUnittestRunnerCurrentRunner();
    AtModuleSdhTestRunner moduleRunner = (AtModuleSdhTestRunner)AtChannelTestRunnerModuleRunnerGet(channelRunner);
    eBool hasPohProcessor = AtModuleSdhTestRunnerPathHasPohProcessor(moduleRunner, TestedPath());
    eBool shouldTestPoh = AtModuleSdhTestRunnerShouldTestPoh(moduleRunner);

    if (hasPohProcessor && !shouldTestPoh)
        {
        AtPrintc(cSevWarning,
                 "WARNING: (%s, %d) need to override AtModuleSdhTestRunnerShouldTestPoh to test POH functionalities\r\n",
                 __FILE__, __LINE__);
        }

    return (shouldTestPoh && hasPohProcessor) ? cAtTrue : cAtFalse;
    }

static eBool CanExpectAlarm()
    {
    if (AtTestIsSimulationTesting())
        return cAtFalse;
    if (!HasPohProcessor())
        return cAtFalse;
    return cAtTrue;
    }

static eBool AlarmExpect(AtSdhPath path, uint32 alarmType, uint32 expectedStatus)
    {
    static const uint32 timeoutInMs = 1000;
    uint32 elapseTimeInMs = 0;

    if (!CanExpectAlarm())
        return cAtTrue;

    while (elapseTimeInMs < timeoutInMs)
        {
        if ((AtChannelAlarmGet((AtChannel)path) & alarmType) == expectedStatus)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static AtSdhPath TestedVc()
    {
    AtSdhPath path = TestedPath();
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if ((channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4)     ||
        (channelType == cAtSdhChannelTypeVc3)     ||
        (channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        return path;

    return (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)path, 0);
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(TestedPath());
    AtChannelInit((AtChannel)TestedPath());
    }

static AtModuleSdhTestRunner ModuleSdhTestRunner()
    {
    return (AtModuleSdhTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testChangeTtiPath()
    {
    const char *ttiMessage  = "Hello";
    const char *ttiMessage1 = "H";
    tAtSdhTti txTti;
    eAtRet expectRet = HasPohProcessor() ? cAtOk : cAtErrorModeNotSupport;
    AtModuleSdhTestRunner moduleTestRunner = ModuleSdhTestRunner();

    AtOsalMemInit(&txTti, 0, sizeof(tAtSdhTti));

    /* Change TX TTI */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, (const uint8*)ttiMessage, AtStrlen(ttiMessage), &txTti);
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelTxTtiSet((AtSdhChannel)TestedVc(), &txTti));
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelExpectedTtiSet((AtSdhChannel)TestedVc(), &txTti));
    if (CanExpectAlarm())
        {
        TEST_ASSERT(AtSdhChannelRxTtiMatch((AtSdhChannel)TestedVc(), &txTti));
        }

    if (AtModuleSdhTestRunnerSupportTtiMode1Byte(moduleTestRunner) == cAtFalse)
        return;

    /* Change another message */
    AtOsalMemInit(&txTti, 0, sizeof(tAtSdhTti));
    AtSdhTtiMake(cAtSdhTtiMode1Byte, (const uint8*)ttiMessage, AtStrlen(ttiMessage1), &txTti);
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelTxTtiSet((AtSdhChannel)TestedVc(), &txTti));
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelExpectedTtiSet((AtSdhChannel)TestedVc(), &txTti));
    if (CanExpectAlarm())
        {
        TEST_ASSERT(AtSdhChannelRxTtiMatch((AtSdhChannel)TestedVc(), &txTti));
        }
    }

static void testChangeExpectedTtiPath()
    {
    const char * ttiMessage  = "a";
    const char * ttiMessage1 = "b";
    tAtSdhTti tti;
    eAtRet expectRet = HasPohProcessor() ? cAtOk : cAtErrorModeNotSupport;

    AtOsalMemInit(&tti, 0, sizeof(tAtSdhTti));

    /* set tti messenger = a */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, (const uint8*)ttiMessage, AtStrlen(ttiMessage), &tti);
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelTxTtiSet((AtSdhChannel)TestedVc(), &tti));

    /* set expected messenger = b */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, (const uint8*)ttiMessage1, AtStrlen(ttiMessage1), &tti);
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelExpectedTtiSet((AtSdhChannel)TestedVc(), &tti));

    /* TIM must raise */
    TEST_ASSERT(AlarmExpect(TestedVc(), cAtSdhPathAlarmTim, cAtSdhPathAlarmTim));

    /* Make TIM clear */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, (const uint8*)ttiMessage, AtStrlen(ttiMessage), &tti);
    TEST_ASSERT_EQUAL_INT(expectRet, AtSdhChannelExpectedTtiSet((AtSdhChannel)TestedVc(), &tti));
    TEST_ASSERT(AlarmExpect(TestedVc(), cAtSdhPathAlarmTim, 0));
    }

static void helper_testChangeInterruptMask(uint32 mask)
    {
    if (mask == 0)  /* Nothing to be tested */
        return;

    /* Enable */
    if (AtSdhPathHasPohProcessor(TestedPath()))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInterruptMaskSet((AtChannel)TestedPath(), mask, mask));
        TEST_ASSERT_EQUAL_INT(mask, AtChannelInterruptMaskGet((AtChannel)TestedPath()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtChannelInterruptMaskSet((AtChannel)TestedPath(), mask, mask) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }

    /* Disable */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInterruptMaskSet((AtChannel)TestedPath(), mask, 0));
    TEST_ASSERT_EQUAL_INT(0, AtChannelInterruptMaskGet((AtChannel)TestedPath()));
    }

static eAtSdhPathAlarmType *AllAlarmsForPath(AtSdhPath path, uint8 *numAlarms)
    {
    static eAtSdhPathAlarmType auTuAlarms[]=
                                     {
                                      cAtSdhPathAlarmAis, cAtSdhPathAlarmLop
                                     };

    static eAtSdhPathAlarmType vcAlarms[]=
                                         {
                                          cAtSdhPathAlarmTim, cAtSdhPathAlarmUneq,
                                          cAtSdhPathAlarmPlm, cAtSdhPathAlarmRdi /*,
                                          cAtSdhPathAlarmBerSd, cAtSdhPathAlarmBerSf, cAtSdhPathAlarmLom,
                                          cAtSdhPathAlarmErdiS, cAtSdhPathAlarmErdiP,
                                          cAtSdhPathAlarmErdiC : TODO: Find a way to test this */
                                         };

    *numAlarms = 0;
    if ((AtSdhChannelIsAu((AtSdhChannel)path) || AtSdhChannelIsTu((AtSdhChannel)path)))
        {
        *numAlarms = mCount(auTuAlarms);
        return auTuAlarms;
        }

    if (AtSdhChannelIsVc((AtSdhChannel)path))
        {
        *numAlarms = mCount(vcAlarms);
        return vcAlarms;
        }

    return NULL;
    }

static void testConfigureInteruptMaskPath()
    {
    uint8 numAlarms;
    AtSdhPath path = TestedPath();
    eAtSdhPathAlarmType *alarms = AllAlarmsForPath(path, &numAlarms);
    eAtSdhPathAlarmType allMasks = 0;
    uint8  i;

    if (AtSdhChannelIsVc((AtSdhChannel)path) && AtSdhPathHasPohProcessor(path))
        AtSdhPathERdiEnable(TestedPath(), cAtFalse);

    /* Clear all interrupt masks first */
    for (i = 0; i < numAlarms; i++)
        {
        if (AtChannelInterruptMaskIsSupported((AtChannel)TestedPath(), alarms[i]))
            allMasks |= alarms[i];
        }
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInterruptMaskSet((AtChannel)TestedPath(), allMasks, 0));

    /* Test each mask */
    for (i = 0; i < numAlarms; i++)
        {
        if (AtChannelInterruptMaskIsSupported((AtChannel)TestedPath(), alarms[i]))
            helper_testChangeInterruptMask(alarms[i]);
        }

    /* Test all masks */
    helper_testChangeInterruptMask(allMasks);
    }

static void testGetAlarmHistory()
    {
    eAtSdhPathAlarmType alarms;

    /* Get alarm history */
    alarms = AtChannelAlarmInterruptGet((AtChannel)TestedPath());
    TEST_ASSERT_EQUAL_INT(alarms, AtChannelAlarmInterruptGet((AtChannel)TestedPath()));

    /* Clear */
    AtChannelAlarmInterruptClear((AtChannel)TestedPath());
    if (CanExpectAlarm())
        {
        TEST_ASSERT_EQUAL_INT(0, AtChannelAlarmInterruptGet((AtChannel)TestedPath()));
        }
    }

static void helper_counterTest(eAtSdhPathCounterType counterType)
    {
    /* Just to make sure that the concrete class implement these APIs */
    AtChannelCounterGet((AtChannel)TestedPath(), counterType);
    AtChannelCounterClear((AtChannel)TestedPath(), counterType);
    }

static void testGetBIPErrorCounterandClear()
    {
    helper_counterTest(cAtSdhPathCounterTypeBip);
    }

static void testGetREIErrorCounterandClear()
    {
    helper_counterTest(cAtSdhPathCounterTypeRei);
    }

static void testERDIInterruptMask()
    {
    AtChannelTestRunner runner = (AtChannelTestRunner)AtUnittestRunnerCurrentRunner();
    AtModuleSdhTestRunner moduleRunner = (AtModuleSdhTestRunner)AtChannelTestRunnerModuleRunnerGet(runner);
    if (!AtModuleSdhTestRunnerERDIInterruptMaskSupported(moduleRunner))
        return;

    if (!AtSdhChannelIsVc((AtSdhChannel)TestedPath()))
        return;

    if (AtSdhPathHasPohProcessor(TestedPath()))
        {
        TEST_ASSERT(AtSdhPathERdiEnable(TestedPath(), cAtTrue) == cAtOk);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtSdhPathERdiEnable(TestedPath(), cAtTrue)  != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }

    helper_testChangeInterruptMask(cAtSdhPathAlarmErdiC);
    helper_testChangeInterruptMask(cAtSdhPathAlarmErdiP);
    helper_testChangeInterruptMask(cAtSdhPathAlarmErdiS);
    }

static eBool rxPslMatch(AtSdhPath channel, uint8 expectedPsl)
    {
    static const uint32 timeoutInMs    = 1000;
    uint32 elapseTimeInMs = 0;

    if (!CanExpectAlarm())
        return cAtTrue;

    while (elapseTimeInMs < timeoutInMs)
        {
        if (AtSdhPathRxPslGet(channel) == expectedPsl)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static void testChangeTxPsl()
    {
    uint8 pslValue[] = {0x2, 0x3, 0x6, 0x5};
    uint8 numPsl, wIndex;

    numPsl = mCount(pslValue);
    for (wIndex = 0; wIndex < numPsl; wIndex++)
        {
        if (HasPohProcessor())
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathTxPslSet(TestedVc(), pslValue[wIndex]));
            TEST_ASSERT(rxPslMatch(TestedVc(), pslValue[wIndex]));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSdhPathTxPslSet(TestedVc(), pslValue[wIndex]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testChangeExpectedPslPath()
    {
    uint8 pslValue[] = {0x2, 0x3, 0x6, 0x5};
    uint8 numPsl, wIndex;

    numPsl = mCount(pslValue);
    for (wIndex = 0; wIndex < numPsl; wIndex++)
        {
        if (HasPohProcessor())
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathExpectedPslSet(TestedVc(), pslValue[wIndex]));
            TEST_ASSERT_EQUAL_INT(pslValue[wIndex], AtSdhPathExpectedPslGet(TestedVc()));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSdhPathExpectedPslSet(TestedVc(), pslValue[wIndex]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testPlmWhenPslMismatch()
    {
    if (!CanExpectAlarm())
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathTxPslSet(TestedVc(), 0x2));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathExpectedPslSet(TestedVc(), 0x5));

    /* Check alarm psl: raised */
    TEST_ASSERT(AlarmExpect(TestedVc(), cAtSdhPathAlarmPlm, cAtSdhPathAlarmPlm));

    /* revert psl expected = 0x2 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathExpectedPslSet(TestedVc(), 0x2));

    /* Check alarm psl: clear */
    TEST_ASSERT(AlarmExpect(TestedVc(), cAtSdhPathAlarmPlm, 0));
    }

static void testLopWhenSsMisMatch()
    {
    /* TODO: only apply this test case for AU/TU where pointer exists
     * - Send different SS bit, make sure that LOP happen
     * - Revert previois SS bit, make sure that no LOP
     * - Change expected SS bit, make sure that LOP happen
     * - Revert previous SS bit, make sure that no LOP
     */
    }

static eBool HasPointerProcessor()
    {
    return AtSdhPathHasPointerProcessor(TestedPath());
    }

static eBool CanEnableSsInsertion()
    {
    AtSdhChannel path = (AtSdhChannel)TestedPath();
    uint8 channelType = AtSdhChannelTypeGet(path);

    if (!AtSdhPathHasPointerProcessor(TestedPath()))
        return cAtFalse;

    if ((channelType == cAtSdhChannelTypeTu12) ||
        (channelType == cAtSdhChannelTypeTu11) ||
        (channelType == cAtSdhChannelTypeVc12) ||
        (channelType == cAtSdhChannelTypeVc11))
        return cAtFalse;

    if ((channelType == cAtSdhChannelTypeTu3) ||
        ((channelType == cAtSdhChannelTypeVc3) &&
         (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(path)) == cAtSdhChannelTypeTu3)))
        return cAtFalse;

    return cAtTrue;
    }

static void testChangeTxSs()
    {
    uint8 ssValue[] = {0x0, 0x1, 0x2, 0x3};
    uint8 numSs, wIndex;

    numSs = mCount(ssValue);
    for (wIndex = 0; wIndex < numSs; wIndex++)
        {
        if (HasPointerProcessor())
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathTxSsSet(TestedPath(), ssValue[wIndex]));
            TEST_ASSERT_EQUAL_INT(AtSdhPathTxSsGet(TestedPath()), ssValue[wIndex]);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSdhPathTxSsSet(TestedPath(), ssValue[wIndex]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }

    if (CanEnableSsInsertion())
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathSsInsertionEnable(TestedPath(), cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtSdhPathSsInsertionIsEnabled(TestedPath()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtSdhPathSsInsertionEnable(TestedPath(), cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testChangeExpectedSs()
    {
    uint8 ssValue[] = {0x0, 0x1, 0x2, 0x3};
    uint8 numSs, wIndex;

    numSs = mCount(ssValue);
    for (wIndex = 0; wIndex < numSs; wIndex++)
        {
        if (HasPointerProcessor())
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathExpectedSsSet(TestedPath(), ssValue[wIndex]));
            TEST_ASSERT_EQUAL_INT(AtSdhPathExpectedSsGet(TestedPath()), ssValue[wIndex]);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSdhPathExpectedSsSet(TestedPath(), ssValue[wIndex]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }

    if (HasPointerProcessor())
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathSsCompareEnable(TestedPath(), cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtSdhPathSsCompareIsEnabled(TestedPath()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtSdhPathSsCompareEnable(TestedPath(), cAtTrue) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testEnableERDI()
    {
    if (!AtSdhChannelIsVc((AtSdhChannel)TestedPath()))
        return;

    if (HasPohProcessor())
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathERdiEnable(TestedVc(), cAtTrue));
        TEST_ASSERT_EQUAL_INT(AtSdhPathERdiIsEnabled(TestedVc()), cAtTrue);
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathERdiEnable(TestedVc(), cAtFalse));
        TEST_ASSERT_EQUAL_INT(AtSdhPathERdiIsEnabled(TestedVc()), cAtFalse);
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtSdhPathERdiEnable(TestedVc(), cAtTrue) != cAtOk);
        TEST_ASSERT(AtSdhPathERdiEnable(TestedVc(), cAtFalse) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static uint32 *AllAlarmsAutoRdi(uint8 *numAlarms)
    {
    static uint32 alarmAutoRdi[] = {cAtSdhPathAlarmAis, cAtSdhPathAlarmLop, cAtSdhPathAlarmUneq, cAtSdhPathAlarmTim, cAtSdhPathAlarmPlm};
    if (numAlarms)
        *numAlarms = mCount(alarmAutoRdi);
    return alarmAutoRdi;
    }

static void helper_testDefaultAlarmAutoRdi(AtSdhChannel path, uint32 defaultAlarms)
    {
    uint8 numAlarms;
    uint32 *alarmAutoRdi = AllAlarmsAutoRdi(&numAlarms);
    uint8 alarm_i;

    for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
        {
        if (alarmAutoRdi[alarm_i] & defaultAlarms)
            {
            TEST_ASSERT_EQUAL_INT(cAtTrue, AtSdhChannelAlarmAutoRdiIsEnabled(path, alarmAutoRdi[alarm_i]));
            }
        else
            {
            TEST_ASSERT_EQUAL_INT(cAtFalse, AtSdhChannelAlarmAutoRdiIsEnabled(path, alarmAutoRdi[alarm_i]));
            }
        }
    }

static eBool TimAutoRdiIsEnabledAsDefault()
    {
    return AtModuleSdhTestRunnerPathTimAutoRdiEnabledAsDefault(ModuleSdhTestRunner());
    }

static void testDefaultAlarmAutoEnhancedRdi()
    {
    AtSdhChannel path = (AtSdhChannel)TestedPath();
    uint32 defaultAlarmAutoRdi = cAtSdhPathAlarmAis|cAtSdhPathAlarmLop|cAtSdhPathAlarmUneq|cAtSdhPathAlarmPlm;

    if (!AtSdhChannelIsVc(path) || !HasPohProcessor())
        return;

    if (TimAutoRdiIsEnabledAsDefault())
        defaultAlarmAutoRdi |= cAtSdhPathAlarmTim;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathERdiEnable(TestedPath(), cAtTrue));
    helper_testDefaultAlarmAutoRdi(path, defaultAlarmAutoRdi);
    }

static void testDefaultAlarmAutoOnebitRdi()
    {
    AtSdhChannel path = (AtSdhChannel)TestedPath();
    uint32 defaultSdhAlarmAutoRdi = cAtSdhPathAlarmAis|cAtSdhPathAlarmLop|cAtSdhPathAlarmUneq;
    uint32 defaultSonetAlarmAutoRdi = cAtSdhPathAlarmAis|cAtSdhPathAlarmLop;

    if (!AtSdhChannelIsVc(path) || !HasPohProcessor())
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhPathERdiEnable(TestedPath(), cAtFalse));

    if (AtSdhChannelCanChangeMode(path, cAtSdhChannelModeSdh))
        {
        if (TimAutoRdiIsEnabledAsDefault())
            defaultSdhAlarmAutoRdi |= cAtSdhPathAlarmTim;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelModeSet(path, cAtSdhChannelModeSdh));
        helper_testDefaultAlarmAutoRdi(path, defaultSdhAlarmAutoRdi);
        }

    if (AtSdhChannelCanChangeMode(path, cAtSdhChannelModeSonet))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelModeSet(path, cAtSdhChannelModeSonet));
        helper_testDefaultAlarmAutoRdi(path, defaultSonetAlarmAutoRdi);
        }
    }

static void testChangeAutoRdi()
    {
    AtSdhChannel path = (AtSdhChannel)TestedPath();

    if (!AtSdhChannelIsVc(path) || !HasPohProcessor())
        return;

    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiEnable(path, cAtTrue), cAtOk);
    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiIsEnabled(path), cAtTrue);
    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiEnable(path, cAtFalse), cAtOk);
    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiIsEnabled(path), cAtFalse);
    }

static void testRemoteLoopbackWithAlarmAffect()
    {
    static const uint32 cSdhVcAlarmsAffect = cAtSdhPathAlarmUneq|cAtSdhPathAlarmPlm|cAtSdhPathAlarmTim;
    AtSdhPath vc = TestedVc();
    uint32 alarmsAffect;

    if (!AtChannelLoopbackIsSupported((AtChannel)vc, cAtLoopbackModeRemote) || !AtSdhPathHasPohProcessor(vc))
        return;

    alarmsAffect = AtSdhChannelAlarmAffectingMaskGet((AtSdhChannel)vc);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet((AtChannel)vc, cAtLoopbackModeRemote));
    TEST_ASSERT((AtSdhChannelAlarmAffectingMaskGet((AtSdhChannel)vc) & cSdhVcAlarmsAffect) == 0);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet((AtChannel)vc, cAtLoopbackModeRelease));
    TEST_ASSERT(AtSdhChannelAlarmAffectingMaskGet((AtSdhChannel)vc) == alarmsAffect);
    }

static void testRemoteLoopbackWithPohInsertion()
    {
    AtSdhPath vc = TestedVc();

    if (!AtChannelLoopbackIsSupported((AtChannel)vc, cAtLoopbackModeRemote) || !AtSdhPathHasPohProcessor(vc))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet((AtChannel)vc, cAtLoopbackModeRemote));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtSdhPathPohInsertionIsEnabled(vc));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet((AtChannel)vc, cAtLoopbackModeRelease));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtSdhPathPohInsertionIsEnabled(vc));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_SdhPath_Fixtures)
        {
        new_TestFixture("testChangeTtiPath", testChangeTtiPath),
        new_TestFixture("testChangeExpectedTtiPath", testChangeExpectedTtiPath),
        new_TestFixture("testConfigureInteruptMaskPath", testConfigureInteruptMaskPath),
        new_TestFixture("testERDIInterruptMask", testERDIInterruptMask),
        new_TestFixture("testGetAlarmHistory", testGetAlarmHistory),
        new_TestFixture("testGetBIPErrorCounterandClear", testGetBIPErrorCounterandClear),
        new_TestFixture("testGetREIErrorCounterandClear", testGetREIErrorCounterandClear),
        new_TestFixture("testChangeTxPsl", testChangeTxPsl),
        new_TestFixture("testChangeExpectedPslPath", testChangeExpectedPslPath),
        new_TestFixture("testPlmWhenPslMismatch", testPlmWhenPslMismatch),
        new_TestFixture("testLopWhenSsMisMatch", testLopWhenSsMisMatch),
        new_TestFixture("testChangeTxSs", testChangeTxSs),
        new_TestFixture("testChangeExpectedSs", testChangeExpectedSs),
        new_TestFixture("testEnableERDI", testEnableERDI),
        new_TestFixture("testDefaultAlarmAutoEnhancedRdi", testDefaultAlarmAutoEnhancedRdi),
        new_TestFixture("testDefaultAlarmAutoOnebitRdi", testDefaultAlarmAutoOnebitRdi),
        new_TestFixture("testChangeAutoRdi", testChangeAutoRdi),
        new_TestFixture("testRemoteLoopbackWithAlarmAffect", testRemoteLoopbackWithAlarmAffect),
        new_TestFixture("testRemoteLoopbackWithPohInsertion", testRemoteLoopbackWithPohInsertion)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_SdhPath_Caller, "TestSuite_SdhPath", setUp, NULL, TestSuite_SdhPath_Fixtures);

    return (TestRef)((void *)&TestSuite_SdhPath_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtSurEngineTestRunner CreateSurEngineTestRunner(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule)
    {
    if (AtSdhChannelIsVc((AtSdhChannel)TestedPath()))
        return AtModuleSurTestRunnerCreateSdhPathSurEngineTestRunner(factoryModule, AtChannelSurEngineGet((AtChannel)TestedPath()));
    return NULL;
    }

static void OverrideAtChannelTestRunner(AtChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelTestRunnerOverride, (void *)self->methods, sizeof(m_AtChannelTestRunnerOverride));

        mMethodOverride(m_AtChannelTestRunnerOverride, CreateSurEngineTestRunner);
        }

    mMethodsSet(self, &m_AtChannelTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtChannelTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhPathTestRunner);
    }

AtChannelTestRunner AtSdhPathTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtSdhPathTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSdhPathTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

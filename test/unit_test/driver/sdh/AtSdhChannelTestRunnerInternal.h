/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhChannelTestRunnerInternal.h
 * 
 * Created Date: Jun 23, 2014
 *
 * Description : SDH channel test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHCHANNELTESTRUNNERINTERNAL_H_
#define _ATSDHCHANNELTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhLineTestRunner * AtSdhLineTestRunner;

typedef struct tAtSdhChannelTestRunner
    {
    tAtChannelTestRunner super;
    }tAtSdhChannelTestRunner;

typedef struct tAtSdhLineTestRunnerMethods
    {
    eBool (*DoesFraming)(AtSdhLineTestRunner self);
    void (*TestLineRateIsSupported)(AtSdhLineTestRunner self);
    eBool (*ShouldTestLineRateOnLine)(AtSdhLineTestRunner self, AtSdhLine line);
    }tAtSdhLineTestRunnerMethods;

typedef struct tAtSdhLineTestRunner
    {
    tAtSdhChannelTestRunner super;
    const tAtSdhLineTestRunnerMethods *methods;
    }tAtSdhLineTestRunner;

typedef struct tAtSdhPathTestRunner
    {
    tAtSdhChannelTestRunner super;
    }tAtSdhPathTestRunner;

typedef struct tAtSdhMapRunner
    {
    tAtSdhChannelTestRunner super;
    }tAtSdhMapRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtSdhChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtSdhLineTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

#endif /* _ATSDHCHANNELTESTRUNNERINTERNAL_H_ */


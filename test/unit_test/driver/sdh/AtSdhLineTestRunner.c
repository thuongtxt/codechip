/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhLineTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : SDH channel test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtTests.h"
#include "AtSdhLine.h"
#include "AtSdhChannelTestRunnerInternal.h"
#include "../sur/AtModuleSurTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhLineTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSdhLineTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;
static tAtChannelTestRunnerMethods m_AtChannelTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhLineTestRunner CurrentRunner()
    {
    return (AtSdhLineTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtSdhLine TestedLine()
    {
    return (AtSdhLine)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eBool NoAlarm(AtSdhLine line)
    {
    static const uint32 timeoutInMs = 10000;
    uint32 elapseTimeInMs = 0;

    if (AtTestIsSimulationTesting())
        return cAtTrue;

    while (elapseTimeInMs < timeoutInMs)
        {
        if (AtChannelAlarmGet((AtChannel)line) == 0)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static eBool NoInterrupt(AtSdhLine line)
    {
    static const uint32 timeoutInMs = 10000;
    uint32 elapseTimeInMs = 0;

    if (AtTestIsSimulationTesting())
        return cAtTrue;

    while (elapseTimeInMs < timeoutInMs)
        {
        if (AtChannelAlarmInterruptClear((AtChannel)line) == 0)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static eBool NoError(AtSdhLine line)
    {
    static const uint32 timeoutInMs = 10000;
    uint32 elapseTimeInMs = 0;

    if (AtTestIsSimulationTesting())
        return cAtTrue;

    while (elapseTimeInMs < timeoutInMs)
        {
        if ((AtChannelCounterClear((AtChannel)TestedLine(), cAtSdhLineCounterTypeB1) == 0) &&
            (AtChannelCounterClear((AtChannel)TestedLine(), cAtSdhLineCounterTypeB2) == 0) &&
            (AtChannelCounterClear((AtChannel)TestedLine(), cAtSdhLineCounterTypeRei) == 0))
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static eBool alarmExpect(AtSdhLine line,
                         uint32 alarmType,
                         uint32 expectedStatus)
    {
    static const uint32 timeoutInMs = 1000;
    uint32 elapseTimeInMs = 0;

    while (elapseTimeInMs < timeoutInMs)
        {
        if ((AtChannelAlarmGet((AtChannel)line) & alarmType) == expectedStatus)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(TestedLine());
    if (mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        AtSdhChannelAlarmAffectingEnable((AtSdhChannel)TestedLine(), cAtSdhLineAlarmTim, cAtFalse);

    if (AtChannelLoopbackIsSupported((AtChannel)TestedLine(), cAtLoopbackModeLocal))
        AtChannelLoopbackSet((AtChannel)TestedLine(), cAtLoopbackModeLocal);
    }

static void testLineMustBeNormalAfterStartup()
    {
    if (AtTestIsSimulationTesting())
        return;

    TEST_ASSERT(NoAlarm(TestedLine()));
    TEST_ASSERT(NoError(TestedLine()));
    }

static void testLineMustBelongToSdhModule()
    {
    /* Get SDH module from device handle */
    AtDevice device = AtChannelDeviceGet((AtChannel)TestedLine());
    AtModuleSdh sdhModuleFromDevice = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Get SDH module of this line and it must be the same to SDH module from device handle */
    TEST_ASSERT(sdhModuleFromDevice == (AtModuleSdh)AtChannelModuleGet((AtChannel)TestedLine()));
    }

eBool TTIsAreSame(const tAtSdhTti *tti1, const tAtSdhTti *tti2)
    {
    if (AtStrcmp((char*)tti1->message, (char*)tti2->message) != 0)
        return cAtFalse;

    if (tti1->mode != tti2->mode)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ohByteMatch(AtSdhLine line,
                         uint8 expectedOh,
                         uint8 (*RxOhGet)(AtSdhLine self))
    {
    static const uint32 timeoutInMs    = 1000;
    uint32 elapseTimeInMs = 0;

    while (elapseTimeInMs < timeoutInMs)
        {
        if (RxOhGet(line) == expectedOh)
            return cAtTrue;

        /* Retry */
        AtOsalUSleep(50000);
        elapseTimeInMs = elapseTimeInMs + 50;
        }

    return cAtFalse;
    }

static void TestOverheadNotSupportIfDoesNotHaveFrammingFeature(eAtModuleSdhRet (*SetFunc)(AtSdhLine, uint8), uint8 (*GetFunc)(AtSdhLine))
    {
    uint8 values[] = {0x0, 0x2, 0x12};
    uint8 i;

    for (i = 0; i < mCount(values); i++)
        {
        TEST_ASSERT(SetFunc(TestedLine(), values[i]) != cAtOk);
        TEST_ASSERT_EQUAL_INT(GetFunc(TestedLine()), 0);
        }
    }

static void testS1Byte()
    {
    uint8 values[] = {0x0, 0x2, 0x12};
    uint8 i;

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        {
        AtTestLoggerEnable(cAtFalse);
        TestOverheadNotSupportIfDoesNotHaveFrammingFeature(AtSdhLineTxS1Set, AtSdhLineTxS1Get);
        AtTestLoggerEnable(cAtTrue);
        return;
        }

    for (i = 0; i < mCount(values); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhLineTxS1Set(TestedLine(), values[i]));
        TEST_ASSERT_EQUAL_INT(AtSdhLineTxS1Get(TestedLine()), values[i]);

        if (AtTestIsSimulationTesting())
            continue;

        /* Test S1 when receiving it in Rx */
        TEST_ASSERT(ohByteMatch(TestedLine(), values[i], AtSdhLineRxS1Get));
        }
    }

static void testK1Byte()
    {
    uint8 values[] = {0x0, 0x2, 0x12};
    uint8 i;

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        {
        AtTestLoggerEnable(cAtFalse);
        TestOverheadNotSupportIfDoesNotHaveFrammingFeature(AtSdhLineTxK1Set, AtSdhLineTxK1Get);
        AtTestLoggerEnable(cAtTrue);
        return;
        }

    for (i = 0; i < mCount(values); i++)
        {
        /* Test set/get K1 byte */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhLineTxK1Set(TestedLine(), values[i]));
        TEST_ASSERT_EQUAL_INT(AtSdhLineTxK1Get(TestedLine()), values[i]);

        if (AtTestIsSimulationTesting())
            continue;

        /* Test K1 when receiving it in Rx */
        TEST_ASSERT(ohByteMatch(TestedLine(), values[i], AtSdhLineRxK1Get));
        }
    }

static void testK2Byte()
    {
    uint8 values[] = {0x0, 0x2, 0x12};
    uint8 i;

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        {
        AtTestLoggerEnable(cAtFalse);
        TestOverheadNotSupportIfDoesNotHaveFrammingFeature(AtSdhLineTxK2Set, AtSdhLineTxK2Get);
        AtTestLoggerEnable(cAtTrue);
        return;
        }

    for (i = 0; i < mCount(values); i++)
        {
        /* Test set/get K2 byte */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhLineTxK2Set(TestedLine(), values[i]));
        TEST_ASSERT_EQUAL_INT(AtSdhLineTxK2Get(TestedLine()), values[i]);

        if (AtTestIsSimulationTesting())
            continue;

        /* Test K2 when receiving it in Rx */
        TEST_ASSERT(ohByteMatch(TestedLine(), values[i], AtSdhLineRxK2Get));
        }
    }

static eAtSdhLineCounterType *SupportedCounterTypes(uint8 *numOfTypes)
    {
    static eAtSdhLineCounterType counterTypes[] = {
                                                  cAtSdhLineCounterTypeB1,  /**< B1 counter */
                                                  cAtSdhLineCounterTypeB2,  /**< B2 counter */
                                                  cAtSdhLineCounterTypeRei, /**< REI-L counter */
                                                    };

    *numOfTypes = sizeof(counterTypes) / sizeof(eAtSdhLineCounterType);
    return counterTypes;
    }

static eBool VerifyDataPathAtLineIsOk(AtSdhLine self)
    {
    eAtRet  ret = cAtOk;
    uint8 i, numTypes;
    eAtSdhLineCounterType *counterType;
    AtChannel channel = (AtChannel)self;
    tAtSdhLineCounters allCounters;

    /* Loop in system */
    if (AtChannelLoopbackIsSupported(channel, cAtLoopbackModeLocal))
        ret = AtChannelLoopbackSet(channel, (eAtLoopbackMode)cAtLoopbackModeLocal);

    /* Unforce all alarm */
    ret |= AtChannelTxAlarmUnForce(channel, (eAtSdhLineAlarmType)cAtSdhLineAlarmAis);
    ret |= AtChannelRxAlarmUnForce(channel, (eAtSdhLineAlarmType)cAtSdhLineAlarmAis);
    ret |= AtChannelAllCountersClear(channel, &allCounters);
    if (ret != cAtOk)
        return cAtFalse;

    /* Check data is OK */
    if (AtChannelAlarmGet(channel))
        return cAtFalse;

    /* Check counter types at Line */
    counterType = SupportedCounterTypes(&numTypes);
    for (i = 0; i < numTypes; i++)
        if (AtChannelCounterGet(channel, counterType[i]))
            return cAtFalse;

    return cAtFalse;
    }

static void TestForceTxAlarmLineIsNotSupported()
    {
    AtChannel channel = (AtChannel)TestedLine();
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtChannelTxAlarmForce(channel, cAtSdhLineAlarmAis) != cAtOk);
    TEST_ASSERT(AtChannelTxAlarmUnForce(channel, cAtSdhLineAlarmAis) == cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

/* At Line, just support to force AIS */
static void testForceTxAlarmLine()
    {
    AtChannel channel = (AtChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        {
        TestForceTxAlarmLineIsNotSupported();
        return;
        }

    /* Ensure data stream is OK */
    TEST_ASSERT_MESSAGE(!VerifyDataPathAtLineIsOk(TestedLine()), "VerifyDataPathAtLineIsOk in testForceTxAlarmLine testcase fail.\n");

    /* Force alarm */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTxAlarmForce(channel, cAtSdhLineAlarmAis));

    /* Check AIS at Rx */
    if (!AtTestIsSimulationTesting())
        {
        TEST_ASSERT(alarmExpect(TestedLine(), cAtSdhLineAlarmAis, cAtSdhLineAlarmAis));
        }

    /* Return previous state */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTxAlarmUnForce(channel, cAtSdhLineAlarmAis));
    }

static void CannotForceAisDownStream()
    {
    AtChannel channel = (AtChannel)TestedLine();
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtChannelRxAlarmForce(channel,   cAtSdhLineAlarmAis) != cAtOk);
    TEST_ASSERT(AtChannelRxAlarmUnForce(channel, cAtSdhLineAlarmAis) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testConfigureForceAlarmType()
    {
    AtChannel channel = (AtChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        {
        CannotForceAisDownStream();
        return;
        }

    if (AtChannelTxForcableAlarmsGet(channel) & cAtSdhLineAlarmAis)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTxAlarmForce(channel, cAtSdhLineAlarmAis));
        TEST_ASSERT_EQUAL_INT(cAtSdhLineAlarmAis, (cAtSdhLineAlarmAis & AtChannelTxForcedAlarmGet(channel)));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTxAlarmUnForce(channel, cAtSdhLineAlarmAis));
        TEST_ASSERT_EQUAL_INT(0x0, (cAtSdhLineAlarmAis & AtChannelTxForcedAlarmGet(channel)));
        }

    if (AtChannelRxForcableAlarmsGet(channel) & cAtSdhLineAlarmAis)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelRxAlarmForce(channel, cAtSdhLineAlarmAis));
        TEST_ASSERT_EQUAL_INT(cAtSdhLineAlarmAis, (cAtSdhLineAlarmAis & AtChannelRxForcedAlarmGet(channel)));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelRxAlarmUnForce(channel, cAtSdhLineAlarmAis));
        TEST_ASSERT_EQUAL_INT(0x0, (cAtSdhLineAlarmAis & AtChannelRxForcedAlarmGet(channel)));
        }
    }

static void testReadErrorCounters()
    {
    tAtSdhLineCounters  lineCounter;
    AtChannel channel = (AtChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        return;

    /* Performance counters */
    AtChannelCounterGet(channel, cAtSdhLineCounterTypeB1);
    AtChannelCounterClear(channel, cAtSdhLineCounterTypeB1);
    AtChannelCounterGet(channel, cAtSdhLineCounterTypeB2);
    AtChannelCounterClear(channel, cAtSdhLineCounterTypeB2);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelAllCountersGet(channel, &lineCounter));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelAllCountersClear(channel, &lineCounter));
    }

static void testConfigureInteruptMask()
    {
    uint8  i;
    AtChannel channel = (AtChannel)TestedLine();
    const uint32 enableMask[] = {cAtSdhLineAlarmLos , cAtSdhLineAlarmLof, cAtSdhLineAlarmTim | cAtSdhLineAlarmRdi,
                                 cAtSdhLineAlarmLos | cAtSdhLineAlarmLof | cAtSdhLineAlarmTim | cAtSdhLineAlarmRdi | cAtSdhLineAlarmAis};

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        return;

    /* Set 4 certain enable masks to test */
    for (i = 0; i < mCount(enableMask); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInterruptMaskSet(channel, enableMask[i], enableMask[i]));
        TEST_ASSERT_EQUAL_INT(enableMask[i], AtChannelInterruptMaskGet(channel));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInterruptMaskSet(channel, enableMask[i], 0x0));
        TEST_ASSERT_EQUAL_INT(0, AtChannelInterruptMaskGet(channel));
        }
    }

static eAtSdhLineRate *SupportedLineRates(uint8 *numberOfTypes)
    {
    static eAtSdhLineRate lineRates[] = {cAtSdhLineRateStm0, cAtSdhLineRateStm1, cAtSdhLineRateStm4, cAtSdhLineRateStm16, cAtSdhLineRateStm64};
    *numberOfTypes = sizeof(lineRates) / sizeof(eAtSdhLineRate);

    return lineRates;
    }

static void TestLineRateIsSupported(AtSdhLineTestRunner self)
    {
    uint8 i, numTypes;
    eAtSdhLineRate *lineRates = SupportedLineRates(&numTypes);
    eBool isSupported = cAtFalse;

    for (i = 0; i < numTypes; i++)
        {
        isSupported |= AtSdhLineRateIsSupported(TestedLine(), lineRates[i]);
        if (isSupported)
            break;
        }

    TEST_ASSERT_MESSAGE(isSupported, "AtSdhLineRateIsSupported fail");
    }

static void testLineRateIsSupported()
    {
    mMethodsGet(CurrentRunner())->TestLineRateIsSupported(CurrentRunner());
    }

static void helperChangeLineRate(eAtSdhLineRate  lineRate)
    {
    AtSdhChannel aug;

    if (!AtSdhLineRateIsSupported(TestedLine(), lineRate))
        return;

    /* Set line rates and get it from SDH line. They must be the same */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhLineRateSet(TestedLine(), lineRate));
    TEST_ASSERT_EQUAL_INT(lineRate, AtSdhLineRateGet(TestedLine()));

    /* Its sub channels must be valid */
    TEST_ASSERT(AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)TestedLine()) == 1);
    aug = AtSdhChannelSubChannelGet((AtSdhChannel)TestedLine(), 0);
    TEST_ASSERT_NOT_NULL(aug);
    TEST_ASSERT_EQUAL_INT(0, AtChannelIdGet((AtChannel)aug));

    /* Check no alarm and no error counter in real platform */
    if (AtTestIsSimulationTesting())
        return;

    /* After change line rate, loop-back state will be released
     * Need to loop back again */
    if (AtChannelLoopbackIsSupported((AtChannel)TestedLine(), cAtLoopbackModeLocal))
        AtChannelLoopbackSet((AtChannel)TestedLine(), cAtLoopbackModeLocal);

    NoError(TestedLine());
    TEST_ASSERT(NoAlarm(TestedLine()) && NoError(TestedLine()));
    }

static eBool ShouldTestLineRateOnLine(AtSdhLineTestRunner self, AtSdhLine line)
    {
    /* Just ID line < 2, can support 2 modes STM-1 and STM-4. So we just change line rate on these 2 lines */
    if (AtChannelIdGet((AtChannel)line) < 2)
        return cAtTrue;
    return cAtFalse;
    }

/* Change line rate */
static void testChangeLineRate()
    {
    AtSdhLineTestRunner lineTestRunner = (AtSdhLineTestRunner)AtUnittestRunnerCurrentRunner();
    if (mMethodsGet(lineTestRunner)->ShouldTestLineRateOnLine(lineTestRunner, TestedLine()))
        {
        eAtSdhLineRate rate = AtSdhLineRateGet(TestedLine());
        helperChangeLineRate(cAtSdhLineRateStm0);
        helperChangeLineRate(cAtSdhLineRateStm1);
        helperChangeLineRate(cAtSdhLineRateStm4);
        helperChangeLineRate(cAtSdhLineRateStm16);

        /* Change to STM-1 again */
        helperChangeLineRate(rate);
        }
    }

static void testGetAlarmHistoryAndClear()
    {
    AtChannel channel = (AtChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        return;

    /* In simulation mode, just make sure that it does not crash */
    if (AtTestIsSimulationTesting())
        {
        AtChannelAlarmInterruptGet(channel);
        AtChannelAlarmInterruptClear(channel);
        return;
        }

    /* Real platform */
    AtChannelTxAlarmForce(channel, cAtSdhLineAlarmAis);
    TEST_ASSERT_EQUAL_INT(AtChannelAlarmInterruptGet(channel), (AtChannelAlarmInterruptClear(channel)));
    TEST_ASSERT(NoInterrupt(TestedLine()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTxAlarmUnForce(channel, cAtSdhLineAlarmAis));
    TEST_ASSERT_EQUAL_INT(0x0, (cAtSdhLineAlarmAis & AtChannelTxForcedAlarmGet(channel)));
    }

static void testGetBIPErrorCounterAndClear()
    {
    uint8   numCount, i;
    eAtSdhLineCounterType countType[] = {cAtSdhLineCounterTypeB1, cAtSdhLineCounterTypeB2};
    AtChannel channel = (AtChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        return;

    numCount = mCount(countType);

    for (i = 0; i < numCount; i++)
        {
        /* Counter in AtChannelCounterGet have to be the same as counter in AtChannelCounterClear,
         * and then return value of AtChannelCounterClear have to be zero */
        TEST_ASSERT_EQUAL_INT(AtChannelCounterGet(channel, countType[i]), AtChannelCounterClear(channel, countType[i]));
        TEST_ASSERT_EQUAL_INT(0x0, AtChannelCounterClear(channel, countType[i]));
        }
    }

static void testGetREIErrorCounterAndClear()
    {
    uint16  counterType;
    AtChannel channel = (AtChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        return;

    counterType =  (uint16)cAtSdhLineCounterTypeRei;

    /* Counter in AtChannelCounterGet have to be the same as counter in AtChannelCounterClear,
    * and then return value of AtChannelCounterClear have to be zero */
    TEST_ASSERT_EQUAL_INT(AtChannelCounterGet(channel, counterType), AtChannelCounterClear(channel, counterType));
    TEST_ASSERT_EQUAL_INT(0x0, AtChannelCounterClear(channel, counterType));
    }

static void testChangeAutoRdi()
    {
    AtSdhChannel channel = (AtSdhChannel)TestedLine();

    if (!mMethodsGet(CurrentRunner())->DoesFraming(CurrentRunner()))
        return;

    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiEnable(channel, cAtTrue), cAtOk);
    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiIsEnabled(channel), cAtTrue);
    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiEnable(channel, cAtFalse), cAtOk);
    TEST_ASSERT_EQUAL_INT(AtSdhChannelAutoRdiIsEnabled(channel), cAtFalse);
    }

static eBool DoesFraming(AtSdhLineTestRunner self)
    {
    return cAtTrue;
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_SdhLine_Fixtures)
        {
        new_TestFixture("testLineMustBeNormalAfterStartup", testLineMustBeNormalAfterStartup),
        new_TestFixture("testLineMustBelongToSdhModule", testLineMustBelongToSdhModule),
        new_TestFixture("testGetBIPErrorCounterAndClear", testGetBIPErrorCounterAndClear),
        new_TestFixture("testGetREIErrorCounterAndClear", testGetREIErrorCounterAndClear),
        new_TestFixture("testLineRateIsSupported", testLineRateIsSupported),
        new_TestFixture("testS1Byte", testS1Byte),
        new_TestFixture("testK1Byte", testK1Byte),
        new_TestFixture("testK2Byte", testK2Byte),
        new_TestFixture("testReadErrorCounters", testReadErrorCounters),
        new_TestFixture("testForceTxAlarmLine", testForceTxAlarmLine),
        new_TestFixture("testConfigureForceAlarmType", testConfigureForceAlarmType),
        new_TestFixture("testConfigureInteruptMask", testConfigureInteruptMask),
        new_TestFixture("testGetAlarmHistoryAndClear", testGetAlarmHistoryAndClear),
        new_TestFixture("testChangeLineRate", testChangeLineRate),
        new_TestFixture("testChangeAutoRdi", testChangeAutoRdi),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_SdhLine_Caller, "TestSuite_SdhLine", setUp, NULL, TestSuite_SdhLine_Fixtures);

    return (TestRef)((void *)&TestSuite_SdhLine_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtSurEngineTestRunner CreateSurEngineTestRunner(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule)
    {
    if (!mMethodsGet(mThis(self))->DoesFraming(mThis(self)))
        return NULL;

    return AtModuleSurTestRunnerCreateSdhLineSurEngineTestRunner(factoryModule, AtChannelSurEngineGet((AtChannel)TestedLine()));
    }

static void OverrideAtChannelTestRunner(AtChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelTestRunnerOverride, (void *)self->methods, sizeof(m_AtChannelTestRunnerOverride));

        mMethodOverride(m_AtChannelTestRunnerOverride, CreateSurEngineTestRunner);
        }

    mMethodsSet(self, &m_AtChannelTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtChannelTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static void MethodsInit(AtSdhLineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DoesFraming);
        mMethodOverride(m_methods, TestLineRateIsSupported);
        mMethodOverride(m_methods, ShouldTestLineRateOnLine);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhLineTestRunner);
    }

AtChannelTestRunner AtSdhLineTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((AtSdhLineTestRunner)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtSdhLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSdhLineTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

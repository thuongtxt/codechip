/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtModuleSdhTests.c
 *
 * Created Date: Jun 23, 2014
 *
 * Description : SDH Unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtSdhChannel.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtSdhLine.h"
#include "AtModuleSdhTestRunner.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModuleSdhTests.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtSdhChannel m_channels[48 * 28];

/*--------------------------- Forward declarations ---------------------------*/
extern eAtModuleSdhRet AtSdhChannelCanChangeMapping(AtSdhChannel self, uint8 mapType);

/*--------------------------- Implementation ---------------------------------*/
AtSdhChannel *AtSdhTestSharedChannelList(uint32 *numChannels)
    {
    if (numChannels)
        *numChannels = mCount(m_channels);
    return m_channels;
    }

uint16 AtSdhTestCreateAllAug64s(AtSdhLine line, AtSdhChannel *channels)
    {
    if (AtSdhLineRateGet(line) < cAtSdhLineRateStm64)
        return 0;

    channels[0] = (AtSdhChannel)AtSdhLineAug64Get(line, 0);
    AtAssert(channels[0]);

    return 1;
    }

uint16 AtSdhTestCreateAllAug16s(AtSdhLine line, AtSdhChannel *channels)
    {
    static const uint8 numAug16sInAug64 = 4;
    eAtRet ret = cAtOk;
    uint16 i;
    AtSdhChannel aug64;

    /* STM-16 only has one AUG-16 */
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate == cAtSdhLineRateStm16)
        {
        channels[0] = AtSdhChannelSubChannelGet((AtSdhChannel)line, 0);
        AtAssert(channels[0]);
        return 1;
        }

    /* Try to get all AUG-64s */
    if (AtSdhTestCreateAllAug64s(line, &aug64) == 0)
        return 0;

    /* We only support STM-64, so there is only one AUG-64 in returned list.
     * Map it to AUG-16 */
    ret = AtSdhChannelMapTypeSet(aug64, cAtSdhAugMapTypeAug64Map4xAug16s);
    if (ret != cAtErrorModeNotSupport)
        AtAssert(ret == cAtOk);
    if (ret != cAtOk)
        return 0;

    /* Return all AUG-4s */
    for (i = 0; i < numAug16sInAug64; i++)
        channels[i] = AtSdhChannelSubChannelGet(aug64, i);

    return numAug16sInAug64;
    }

uint16 AtSdhTestCreateAllAug4s(AtSdhLine line, AtSdhChannel *channels)
    {
    static const uint8 numAug4sInAug16 = 4;
    eAtRet ret = cAtOk;
    uint16 i;
    AtSdhChannel aug16;

    /* STM-4 only has one AUG-4 */
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate == cAtSdhLineRateStm4)
        {
        channels[0] = AtSdhChannelSubChannelGet((AtSdhChannel)line, 0);
        AtAssert(channels[0]);
        return 1;
        }

    /* Try to get all AUG-16s */
    if (AtSdhTestCreateAllAug16s(line, &aug16) == 0)
        return 0;

    /* We only support STM-16, so there is only one AUG-16 in returned list.
     * Map it to AUG-4 */
    ret = AtSdhChannelMapTypeSet(aug16, cAtSdhAugMapTypeAug16Map4xAug4s);
    if (ret != cAtErrorModeNotSupport)
        AtAssert(ret == cAtOk);
    if (ret != cAtOk)
        return 0;

    /* Return all AUG-4s */
    for (i = 0; i < numAug4sInAug16; i++)
        channels[i] = AtSdhChannelSubChannelGet(aug16, i);

    return numAug4sInAug16;
    }

uint16 AtSdhTestCreateAllAug1s(AtSdhLine line, AtSdhChannel *channels)
    {
    uint8 i, j;
    AtSdhChannel aug4s[4];
    uint8 numAug4s, numChannels = 0;

    /* STM-1 only has one AUG-1 */
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate == cAtSdhLineRateStm1)
        {
        channels[0] = AtSdhChannelSubChannelGet((AtSdhChannel)line, 0);
        AtAssert(channels[0]);
        return 1;
        }

    /* Try to get all AUG-4s */
    numAug4s = AtSdhTestCreateAllAug4s(line, aug4s);
    if (numAug4s == 0)
        return 0;

    /* Map all AUG-4s to AUG-1s */
    for (i = 0; i < numAug4s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(aug4s[i], cAtSdhAugMapTypeAug4Map4xAug1s))
            AtAssert(AtSdhChannelMapTypeSet(aug4s[i], cAtSdhAugMapTypeAug4Map4xAug1s) == cAtOk);
        else
            continue;

        /* Get sub channels */
        for (j = 0; j < 4; j++)
            {
            channels[numChannels] = AtSdhChannelSubChannelGet(aug4s[i], j);
            AtAssert(channels[numChannels]);
            numChannels = numChannels + 1;
            }
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllAu4_64cs(AtSdhLine line, AtSdhChannel *channels)
    {
    uint16 numChannels = AtSdhTestCreateAllVc4_64cs(line, channels);
    uint16 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        channels[channel_i] = AtSdhChannelParentChannelGet(channels[channel_i]);
    return numChannels;
    }

uint16 AtSdhTestCreateAllAu4_16cs(AtSdhLine line, AtSdhChannel *channels)
    {
    uint16 numChannels = AtSdhTestCreateAllVc4_16cs(line, channels);
    uint16 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        channels[channel_i] = AtSdhChannelParentChannelGet(channels[channel_i]);
    return numChannels;
    }

uint16 AtSdhTestCreateAllAu4_4cs(AtSdhLine line, AtSdhChannel *channels)
    {
    uint16 numChannels = AtSdhTestCreateAllVc4_4cs(line, channels);
    uint16 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        channels[channel_i] = AtSdhChannelParentChannelGet(channels[channel_i]);
    return numChannels;
    }

uint16 AtSdhTestCreateAllAu4s(AtSdhLine line, AtSdhChannel *channels)
    {
    uint16 numChannels = AtSdhTestCreateAllVc4s(line, channels);
    uint16 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        channels[channel_i] = AtSdhChannelParentChannelGet(channels[channel_i]);
    return numChannels;
    }

uint16 AtSdhTestCreateAllVc4_64cs(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel aug64s[16], au;
    uint32 numChannels = 0, numAug64s, i;

    /* Create AUG-64s */
    if ((numAug64s = AtSdhTestCreateAllAug64s(line, aug64s)) == 0)
        return 0;

    /* Map these AUG-64s to VC-4-64c */
    for (i = 0; i < numAug64s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(aug64s[i], cAtSdhAugMapTypeAug64MapVc4_64c))
            AtAssert(AtSdhChannelMapTypeSet(aug64s[i], cAtSdhAugMapTypeAug64MapVc4_64c) == cAtOk);
        else
            continue;

        au = AtSdhChannelSubChannelGet(aug64s[i], 0);
        AtAssert(au);
        channels[numChannels] = AtSdhChannelSubChannelGet(au, 0);
        AtAssert(channels[numChannels]);
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllVc4_16cs(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel aug16s[16], au;
    uint32 numChannels = 0, numAug16s, i;

    /* Create AUG-16s */
    if ((numAug16s = AtSdhTestCreateAllAug16s(line, aug16s)) == 0)
        return 0;

    /* Map these AUG-16s to VC-4-16c */
    for (i = 0; i < numAug16s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(aug16s[i], cAtSdhAugMapTypeAug16MapVc4_16c))
            AtAssert(AtSdhChannelMapTypeSet(aug16s[i], cAtSdhAugMapTypeAug16MapVc4_16c) == cAtOk);
        else
            continue;

        au = AtSdhChannelSubChannelGet(aug16s[i], 0);
        AtAssert(au);
        channels[numChannels] = AtSdhChannelSubChannelGet(au, 0);
        AtAssert(channels[numChannels]);
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllVc4_4cs(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel aug4s[16], au;
    uint32 numChannels = 0, numAug4s, i;

    /* Create AUG-4s */
    if ((numAug4s = AtSdhTestCreateAllAug4s(line, aug4s)) == 0)
        return 0;

    /* Map these AUG-4s to VC-4-4c */
    for (i = 0; i < numAug4s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(aug4s[i], cAtSdhAugMapTypeAug4MapVc4_4c))
            AtAssert(AtSdhChannelMapTypeSet(aug4s[i], cAtSdhAugMapTypeAug4MapVc4_4c) == cAtOk);
        else
            continue;

        au = AtSdhChannelSubChannelGet(aug4s[i], 0);
        AtAssert(au);
        channels[numChannels] = AtSdhChannelSubChannelGet(au, 0);
        AtAssert(channels[numChannels]);
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllVc4s(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel aug1s[16], au;
    uint32 numChannels = 0, numAug1s, i;

    /* Create AUG-1s */
    if ((numAug1s = AtSdhTestCreateAllAug1s(line, aug1s)) == 0)
        return 0;

    /* Map these AUG-1s to VC-4 */
    for (i = 0; i < numAug1s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(aug1s[i], cAtSdhAugMapTypeAug1MapVc4))
            AtAssert(AtSdhChannelMapTypeSet(aug1s[i], cAtSdhAugMapTypeAug1MapVc4) == cAtOk);
        else
            continue;

        au = AtSdhChannelSubChannelGet(aug1s[i], 0);
        AtAssert(au);
        channels[numChannels] = AtSdhChannelSubChannelGet(au, 0);
        AtAssert(channels[numChannels]);
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllTug3(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel vc4s[16];
    uint8 numVc4, i, numChannels = 0, j;

    /* Create all VC-4 first */
    if ((numVc4 = AtSdhTestCreateAllVc4s(line, vc4s)) == 0)
        return 0;

    /* Map these VC-4s to TUG-3s */
    for (i = 0; i < numVc4; i++)
        {
        if ((AtSdhChannelMapTypeIsSupported(vc4s[i], cAtSdhVcMapTypeVc4Map3xTug3s)) &&
            (AtSdhChannelCanChangeMapping(vc4s[i], cAtSdhVcMapTypeVc4Map3xTug3s) == cAtOk))
            AtAssert(AtSdhChannelMapTypeSet(vc4s[i], cAtSdhVcMapTypeVc4Map3xTug3s) == cAtOk);
        else
            continue;

        /* Get sub channels */
        if (channels == NULL)
            continue;

        for (j = 0; j < 3; j++)
            {
            channels[numChannels] = AtSdhChannelSubChannelGet(vc4s[i], j);
            AtAssert(channels[numChannels]);
            numChannels = numChannels + 1;
            }
        }

    return numChannels;
    }

static void Nothing(void){}

uint16 AtSdhTestCreateAllAu3s(AtSdhLine line, AtSdhChannel *channels)
    {
    uint16 numChannels = AtSdhTestCreateAllVc3sInAu3s(line, channels);
    uint16 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        channels[channel_i] = AtSdhChannelParentChannelGet(channels[channel_i]);
    return numChannels;
    }

uint16 AtSdhTestCreateAllVc3sInAu3s(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel aug1s[16], au3;
    uint8 numAug1s, i, j, numChannels = 0;

    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        {
        AtSdhChannel au3 = AtSdhChannelSubChannelGet((AtSdhChannel)line, 0);
        if (channels)
            channels[0] = AtSdhChannelSubChannelGet(au3, 0);
        return 1;
        }

    /* Create all AUG-1s first */
    if ((numAug1s = AtSdhTestCreateAllAug1s(line, aug1s)) == 0)
        return 0;

    /* Map these AUG-1s to VC-3 */
    for (i = 0; i < numAug1s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(aug1s[i], cAtSdhAugMapTypeAug1Map3xVc3s))
            {
            eAtRet ret = AtSdhChannelMapTypeSet(aug1s[i], cAtSdhAugMapTypeAug1Map3xVc3s);
            if (ret != cAtOk)
                {
                Nothing();
                }

            AtAssert(ret == cAtOk);
            }
        else
            continue;

        /* Get sub channels */
        if (channels == NULL)
            continue;
        for (j = 0; j < 3; j++)
            {
            au3 = AtSdhChannelSubChannelGet(aug1s[i], j);
            AtAssert(au3);
            channels[numChannels] = AtSdhChannelSubChannelGet(au3, 0);
            if (AtSdhChannelMapTypeIsSupported(channels[numChannels], cAtSdhVcMapTypeVc3MapC3))
                if (AtSdhChannelMapTypeSet(channels[numChannels], cAtSdhVcMapTypeVc3MapC3) != cAtOk)
                    AtPrintc(cSevInfo, "catch you\r\n");
                AtAssert(AtSdhChannelMapTypeSet(channels[numChannels], cAtSdhVcMapTypeVc3MapC3) == cAtOk);
            AtAssert(channels[numChannels]);
            numChannels = numChannels + 1;
            }
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllVc3sInTug3s(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel aug1s[16];
    uint8 numAug1s, i, j, numChannels = 0;

    /* Create all AUG-1s first */
    if ((numAug1s = AtSdhTestCreateAllAug1s(line, aug1s)) == 0)
        return 0;

    /* Map these AUG-1s to VC-4 */
    for (i = 0; i < numAug1s; i++)
        {
        AtSdhChannel au4, vc4;
        if (AtSdhChannelMapTypeIsSupported(aug1s[i], cAtSdhAugMapTypeAug1MapVc4))
            AtAssert(AtSdhChannelMapTypeSet(aug1s[i], cAtSdhAugMapTypeAug1MapVc4) == cAtOk);
        else
            continue;

        /* Map this VC-4 to 3xTUG-3s */
        au4 = AtSdhChannelSubChannelGet(aug1s[i], 0);
        vc4 = AtSdhChannelSubChannelGet(au4, 0);
        AtAssert(au4);
        AtAssert(vc4);
        if ((AtSdhChannelMapTypeIsSupported(vc4, cAtSdhVcMapTypeVc4Map3xTug3s))&&
            (AtSdhChannelCanChangeMapping(vc4, cAtSdhVcMapTypeVc4Map3xTug3s) == cAtOk))
            AtAssert(AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s) == cAtOk);
        else
            continue;

        /* Map all TUG-3s to VC-3s */
        for (j = 0; j < 3; j++)
            {
            AtSdhChannel tu3;
            AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc4, j);
            AtAssert(tug3);
            if (AtSdhChannelMapTypeIsSupported(tug3, cAtSdhTugMapTypeTug3MapVc3))
                AtAssert(AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3) == cAtOk);
            else
                continue;

            tu3 = AtSdhChannelSubChannelGet(tug3, 0);
            AtAssert(tu3);
            AtAssert(AtSdhChannelSubChannelGet(tu3, 0));
            if (channels)
                {
                channels[numChannels] = AtSdhChannelSubChannelGet(tu3, 0);
                AtAssert(channels[numChannels]);
                AtAssert(AtSdhChannelMapTypeSet(channels[numChannels], cAtSdhVcMapTypeVc3MapC3) == cAtOk);
                }

            numChannels = numChannels + 1;
            }
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllTug2sInTug3s(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel tug3s[48];
    uint8 numTug3s, i, j;
    uint16 numChannels = 0;

    /* Create all TUG-3s */
    if ((numTug3s = AtSdhTestCreateAllTug3(line, tug3s)) == 0)
        return 0;

    /* Map them to TUG-2s */
    for (i = 0; i < numTug3s; i++)
        {
        if (AtSdhChannelMapTypeIsSupported(tug3s[i], cAtSdhTugMapTypeTug3Map7xTug2s))
            AtAssert(AtSdhChannelMapTypeSet(tug3s[i], cAtSdhTugMapTypeTug3Map7xTug2s) == cAtOk);
        else
            continue;

        /* Get sub channels */
        for (j = 0; j < 7; j++)
            {
            channels[numChannels] = AtSdhChannelSubChannelGet(tug3s[i], j);
            AtAssert(channels[numChannels]);
            numChannels = numChannels + 1;
            }
        }

    return numChannels;
    }

uint16 AtSdhTestCreateAllTug2sInVc3(AtSdhLine line, AtSdhChannel *channels)
    {
    AtSdhChannel vc3s[48];
    uint8 numAu3s, i, j;
    uint16 numChannels = 0;

    /* Create all TUG-3s */
    if ((numAu3s = AtSdhTestCreateAllVc3sInAu3s(line, vc3s)) == 0)
        return 0;

    /* Map them to TUG-2s */
    for (i = 0; i < numAu3s; i++)
        {
        if ((AtSdhChannelMapTypeIsSupported(vc3s[i], cAtSdhVcMapTypeVc3Map7xTug2s)) &&
            (AtSdhChannelCanChangeMapping(vc3s[i], cAtSdhVcMapTypeVc3Map7xTug2s) == cAtOk))
            AtAssert(AtSdhChannelMapTypeSet(vc3s[i], cAtSdhVcMapTypeVc3Map7xTug2s) == cAtOk);
        else
            continue;

        /* Get sub channels */
        for (j = 0; j < 7; j++)
            {
            channels[numChannels] = AtSdhChannelSubChannelGet(vc3s[i], j);
            AtAssert(channels[numChannels]);
            numChannels = numChannels + 1;
            }
        }

    return numChannels;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtEncapChannelTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : ETH Port test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtEncapChannelTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEncapChannelTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEncapChannelTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEncapChannel EncapChannel()
    {
    return (AtEncapChannel)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testBindPhysicalLayer()
    {
    AtUnittestRunner currentRunner = AtUnittestRunnerCurrentRunner();
    mMethodsGet(mThis(currentRunner))->TestBindPhysicalLayer(mThis(currentRunner));
    }

static void testChannelTypeMustBeValid()
    {
    eAtEncapType type = AtEncapChannelEncapTypeGet(EncapChannel());
    TEST_ASSERT((type == cAtEncapHdlc) || (type == cAtEncapAtm) || (type == cAtEncapGfp));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_EncapChannel_Fixtures)
        {
        new_TestFixture("testBindPhysicalLayer", testBindPhysicalLayer),
        new_TestFixture("testChannelTypeMustBeValid", testChannelTypeMustBeValid)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_EncapChannel_Caller, "TestSuite_EncapChannel", NULL, NULL, TestSuite_EncapChannel_Fixtures);

    return (TestRef)((void *)&TestSuite_EncapChannel_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void TestBindPhysicalLayer(AtEncapChannelTestRunner self)
    {
    uint16 i;

    if (self->numPhysicalChannels == 0)
        return;

    for (i = 0; i < self->numPhysicalChannels; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtEncapChannelPhyBind(EncapChannel(), self->physicalChannels[i]));
        TEST_ASSERT(AtEncapChannelBoundPhyGet(EncapChannel()) == self->physicalChannels[i]);

        /* Unbind */
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtEncapChannelPhyBind(EncapChannel(), NULL));
        TEST_ASSERT_NULL(AtEncapChannelBoundPhyGet(EncapChannel()));

        /* Init to have fresh status */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInit((AtChannel)EncapChannel()));
        }
    }

static void MethodsInit(AtEncapChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TestBindPhysicalLayer);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEncapChannelTestRunner);
    }

AtChannelTestRunner AtEncapChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtEncapChannelTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

void AtEncapChannelTestRunnerPhysicalChannelSet(AtChannelTestRunner self, AtChannel *physicals, uint32 numPhysicals)
    {
    mThis(self)->physicalChannels = physicals;
    mThis(self)->numPhysicalChannels = numPhysicals;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtModuleEncapTestRunnerInternal.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : ENCAP unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEENCAPTESTRUNNERINTERNAL_H_
#define _ATMODULEENCAPTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtEncapChannel.h"
#include "AtPdhDe1.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "AtModuleEncapTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleEncapTestRunnerMethods
    {
    AtChannel *(*PhysicalChannelsToTest)(AtModuleEncapTestRunner self, uint16 *numChannels);
    eBool (*DynamicChannelAllocation)(AtModuleEncapTestRunner self);

    void (*TestHdlc)(AtModuleEncapTestRunner self);
    void (*TestMfrBundle)(AtModuleEncapTestRunner self);
    void (*TestFrLink)(AtModuleEncapTestRunner self);
    AtChannelTestRunner (*CreateHdlcChannelTestRunner)(AtModuleEncapTestRunner self, AtChannel channel);
    AtChannelTestRunner (*CreateFrLinkTestRunner)(AtModuleEncapTestRunner self, AtChannel channel);
    AtChannelTestRunner (*CreateMfrBundleTestRunner)(AtModuleEncapTestRunner self, AtChannel channel);
    }tAtModuleEncapTestRunnerMethods;

typedef struct tAtModuleEncapTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModuleEncapTestRunnerMethods *methods;
    }tAtModuleEncapTestRunner;

typedef struct tAtPdhModuleEncapTestRunner
    {
    tAtModuleEncapTestRunner super;
    }tAtPdhModuleEncapTestRunner;

typedef struct tAtStmModuleEncapTestRunner
    {
    tAtModuleEncapTestRunner super;
    }tAtStmModuleEncapTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);
AtModuleTestRunner AtPdhModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);
AtModuleTestRunner AtStmModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

TestRef TestSuite_HdlcChannel(AtHdlcChannel channel);
TestRef TestSuite_EncapChannel(AtEncapChannel channel, AtChannel *physicalChannels, uint16 numPhysicalChannels);

#endif /* _ATMODULEENCAPTESTRUNNERINTERNAL_H_ */


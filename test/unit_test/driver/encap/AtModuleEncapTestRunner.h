/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : AtModuleEncapTestRunner.h
 * 
 * Created Date: Feb 20, 2017
 *
 * Description : Module encap runner interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEENCAPTESTRUNNER_H_
#define _ATMODULEENCAPTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleEncapTestRunner * AtModuleEncapTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel AtModuleEncapTestRunnerHdlcChannelCreateWithPhysical(AtModuleEncapTestRunner self, uint32 channelId);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEENCAPTESTRUNNER_H_ */


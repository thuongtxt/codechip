/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtEncapChannelTestRunnerInternal.h
 * 
 * Created Date: Jun 20, 2014
 *
 * Description : ETH Flow test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPCHANNELTESTRUNNERINTERNAL_H_
#define _ATENCAPCHANNELTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEncapChannelTestRunner * AtEncapChannelTestRunner;

typedef struct tAtEncapChannelTestRunnerMethods
    {
    void (*TestBindPhysicalLayer)(AtEncapChannelTestRunner self);
    }tAtEncapChannelTestRunnerMethods;

typedef struct tAtEncapChannelTestRunner
    {
    tAtChannelTestRunner super;
    const tAtEncapChannelTestRunnerMethods *methods;

    /* Private data */
    AtChannel *physicalChannels;
    uint32 numPhysicalChannels;
    }tAtEncapChannelTestRunner;

typedef struct tAtHdlcChannelTestRunner
    {
    tAtEncapChannelTestRunner super;
    }tAtHdlcChannelTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtHdlcChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtEncapChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);
void AtEncapChannelTestRunnerPhysicalChannelSet(AtChannelTestRunner self, AtChannel *physicals, uint32 numPhysicals);
AtChannelTestRunner Af60210012HdlcChannelTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

#endif /* _ATENCAPCHANNELTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtPdhModuleEncapTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PPP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModuleEncapTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapTestRunnerMethods m_AtModuleEncapTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel *PhysicalChannelsToTest(AtModuleEncapTestRunner self, uint16 *numChannels)
    {
    static AtChannel channels[4];
    AtPdhDe1 de1;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    /* For current software, just use PDH physical channels */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint8 numDe1s = AtModulePdhNumberOfDe1sGet(pdhModule);
    if (numDe1s == 0)
        return NULL;

    /* Use 1 DS1, 1 E1 */
    channels[0] = (AtChannel)AtModulePdhDe1Get(pdhModule, 0);
    AtPdhChannelFrameTypeSet((AtPdhChannel)channels[0], cAtPdhDs1J1UnFrm);
    channels[1] = (AtChannel)AtModulePdhDe1Get(pdhModule, 1);
    AtPdhChannelFrameTypeSet((AtPdhChannel)channels[1], cAtPdhE1UnFrm);

    /* Use 1 NxDS0 in DS1 */
    de1 = AtModulePdhDe1Get(pdhModule, 2);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmSf);
    channels[2] = (AtChannel)AtPdhDe1NxDs0Create(de1, cBit1 | cBit16 | cBit31);

    /* Use 1 NxDS0 in E1 */
    de1 = AtModulePdhDe1Get(pdhModule, 3);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1MFCrc);
    channels[3] = (AtChannel)AtPdhDe1NxDs0Create(de1, cBit2 | cBit17 | cBit30);

    *numChannels = 4;
    return channels;
    }

static void OverrideAtModuleEncapTestRunner(AtModuleTestRunner self)
    {
    AtModuleEncapTestRunner runner = (AtModuleEncapTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtModuleEncapTestRunnerOverride, (void *)runner->methods, sizeof(m_AtModuleEncapTestRunnerOverride));

        m_AtModuleEncapTestRunnerOverride.PhysicalChannelsToTest = PhysicalChannelsToTest;
        }

    runner->methods = &m_AtModuleEncapTestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtModuleEncapTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhModuleEncapTestRunner);
    }

AtModuleTestRunner AtPdhModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEncapTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtPdhModuleEncapTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPdhModuleEncapTestRunnerObjectInit(newRunner, module);
    }

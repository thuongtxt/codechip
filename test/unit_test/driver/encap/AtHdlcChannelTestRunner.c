/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtHdlcChannelTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : ETH Port test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModuleEncap.h"
#include "AtEncapChannelTestRunner.h"
#include "../../../../driver/src/generic/encap/AtHdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*EnableFunction)(AtHdlcChannel self, eBool enable);
typedef eBool (*IsEnabledFunction)(AtHdlcChannel self);
typedef eAtRet (*AddressControlSetFunction)(AtHdlcChannel self, uint8 *values);
typedef eAtRet (*AddressControlGetFunction)(AtHdlcChannel self, uint8 *values);
typedef eAtRet (*PacketDropConditionSetFunction)(AtHdlcChannel self, uint32 dropMask, uint32 enableMask);
typedef uint32 (*PacketDropConditionGetFunction)(AtHdlcChannel self);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods    m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods    *m_AtUnittestRunnerMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHdlcChannel Channel()
    {
    return (AtHdlcChannel)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testAddressSizeAndControlSizeMustBeGreaterThanZero()
    {
    TEST_ASSERT(AtHdlcChannelAddressSizeGet(Channel()) > 0);
    TEST_ASSERT(AtHdlcChannelControlSizeGet(Channel()) > 0);
    }

static void testChangeAddressSize()
    {
    /* TODO: Implement testing if hardware support changing address size */
    }

static void testChangeControlSize()
    {
    /* TODO: Implement testing if hardware support changing address size */
    }

static void testChangeFlag()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelFlagSet(Channel(), 0x7E));
    TEST_ASSERT_EQUAL_INT(0x7E, AtHdlcChannelFlagGet(Channel()));
    }

static void testCannotChangeInvalidFlagValue()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT((eAtRet)AtHdlcChannelFlagSet(Channel(), 0xF6) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeNumFlags()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelNumFlagsSet(Channel(), 1));
    TEST_ASSERT_EQUAL_INT(1, AtHdlcChannelNumFlagsGet(Channel()));
    }

static void testChangeFcsMode()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelFcsModeSet(Channel(), cAtHdlcFcsModeNoFcs));
    TEST_ASSERT_EQUAL_INT(cAtHdlcFcsModeNoFcs, AtHdlcChannelFcsModeGet(Channel()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelFcsModeSet(Channel(), cAtHdlcFcsModeFcs16));
    TEST_ASSERT_EQUAL_INT(cAtHdlcFcsModeFcs16, AtHdlcChannelFcsModeGet(Channel()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelFcsModeSet(Channel(), cAtHdlcFcsModeFcs32));
    TEST_ASSERT_EQUAL_INT(cAtHdlcFcsModeFcs32, AtHdlcChannelFcsModeGet(Channel()));
    }

static void testCannotChangeInvalidFcsMode()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT((eAtRet)AtHdlcChannelFcsModeSet(Channel(), 0xffff) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeFcsCalculationMode()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelFcsCalculationModeSet(Channel(), cAtHdlcFcsCalculationModeLsb));
    TEST_ASSERT_EQUAL_INT(cAtHdlcFcsCalculationModeLsb, AtHdlcChannelFcsCalculationModeGet(Channel()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelFcsCalculationModeSet(Channel(), cAtHdlcFcsCalculationModeMsb));
    TEST_ASSERT_EQUAL_INT(cAtHdlcFcsCalculationModeMsb, AtHdlcChannelFcsCalculationModeGet(Channel()));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT((eAtRet)AtHdlcChannelFcsCalculationModeSet(Channel(), cAtHdlcFcsCalculationModeUnknown) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeStuffMode()
    {
    eAtHdlcStuffMode currentMode = AtHdlcChannelStuffModeGet(Channel());

    if (AtHdlcChannelStuffModeIsSupported(Channel(), cAtHdlcStuffBit))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelStuffModeSet(Channel(), cAtHdlcStuffBit));
        TEST_ASSERT_EQUAL_INT(cAtHdlcStuffBit, AtHdlcChannelStuffModeGet(Channel()));
        }

    if (AtHdlcChannelStuffModeIsSupported(Channel(), cAtHdlcStuffByte))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelStuffModeSet(Channel(), cAtHdlcStuffByte));
        TEST_ASSERT_EQUAL_INT(cAtHdlcStuffByte, AtHdlcChannelStuffModeGet(Channel()));
        }

    AtHdlcChannelStuffModeSet(Channel(), currentMode);
    }

static void testCannotChangeInvalidStuffMode()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT((eAtRet)AtHdlcChannelStuffModeSet(Channel(), 0xfff) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static eBool MtuCanBeChanged()
    {
    return cAtFalse;
    }

static void testChangeMtu()
    {
    if (!MtuCanBeChanged())
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelMtuSet(Channel(), 1024));
    TEST_ASSERT_EQUAL_INT(1024, AtHdlcChannelMtuGet(Channel()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelMtuSet(Channel(), 500));
    TEST_ASSERT_EQUAL_INT(500, AtHdlcChannelMtuGet(Channel()));

    /* MTU is too small */
    TEST_ASSERT((eAtRet)AtHdlcChannelMtuSet(Channel(), 63) != cAtOk);

    /* MTU is too large */
    TEST_ASSERT((eAtRet)AtHdlcChannelMtuSet(Channel(), 9601) != cAtOk);
    }

static void testFramingTypeMustBeValid()
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet(Channel());
    TEST_ASSERT((frameType == cAtHdlcFrmCiscoHdlc) ||
                (frameType == cAtHdlcFrmPpp) ||
                (frameType == cAtHdlcFrmFr));
    }

static void testScrambleEnabling()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelScrambleEnable(Channel(), cAtTrue));
    TEST_ASSERT(AtHdlcChannelScrambleIsEnabled(Channel()));

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelScrambleEnable(Channel(), cAtFalse));
    TEST_ASSERT(!AtHdlcChannelScrambleIsEnabled(Channel()));
    }

static void testInternalLinkMustBeValid()
    {
    TEST_ASSERT_NOT_NULL(AtHdlcChannelHdlcLinkGet(Channel()));
    }

static void helperTestEnabledAttribute(EnableFunction Enable, IsEnabledFunction IsEnabled)
    {
    AtHdlcChannel channel = Channel();
    eBool currentStatus = IsEnabled(channel);
    eAtRet ret;

    AtTestLoggerEnable(cAtFalse);
    ret = Enable(channel, cAtTrue);
    if (ret == cAtErrorModeNotSupport)
        {
        /* Not support enable, status much be disabled */
        TEST_ASSERT_EQUAL_INT(cAtFalse, IsEnabled(channel));
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, ret);
        TEST_ASSERT_EQUAL_INT(cAtTrue, IsEnabled(channel));
        }

    ret = Enable(channel, cAtFalse);
    if (ret == cAtErrorModeNotSupport)
        {
        /* Not support disable, status much be enable */
        TEST_ASSERT_EQUAL_INT(cAtTrue, IsEnabled(channel));
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, ret);
        TEST_ASSERT_EQUAL_INT(cAtFalse, IsEnabled(channel));
        }

    AtTestLoggerEnable(cAtTrue);

    TEST_ASSERT_EQUAL_INT(cAtOk, Enable(channel, currentStatus));
    TEST_ASSERT_EQUAL_INT(currentStatus, IsEnabled(channel));
    }

static void testChangeAddressControlBypass()
    {
    return;
    helperTestEnabledAttribute((EnableFunction)AtHdlcChannelAddressControlBypass, (IsEnabledFunction)AtHdlcChannelAddressControlIsBypassed);
    }

static void testChangeAddressControlCompress()
    {
    return;
    helperTestEnabledAttribute((EnableFunction)AtHdlcChannelAddressControlCompress, (IsEnabledFunction)AtHdlcChannelAddressControlIsCompressed);
    }

static void helperTestAddressControl(EnableFunction Enable,
                                     IsEnabledFunction IsEnabled,
                                     AddressControlSetFunction AddressControlSet,
                                     AddressControlGetFunction AddressControlGet)
    {
    uint8 actualValue;
    uint8 values[] = {0x12, 0x7E, 0xFE};
    uint8 i;

    for (i = 0; i < mCount(values); i++)
        {
        /* Enable */
        TEST_ASSERT_EQUAL_INT(cAtOk, Enable(Channel(), cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, IsEnabled(Channel()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AddressControlSet(Channel(), &(values[i])));
        TEST_ASSERT_EQUAL_INT(cAtOk, AddressControlGet(Channel(), &actualValue));
        TEST_ASSERT_EQUAL_INT(values[i], actualValue);

        /* Enable but NULL address */
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AddressControlSet(Channel(), NULL));
        TEST_ASSERT_EQUAL_INT(cAtOk, AddressControlGet(Channel(), &actualValue));
        TEST_ASSERT_EQUAL_INT(values[i], actualValue);
        AtTestLoggerEnable(cAtTrue);

        /* Disable */
        TEST_ASSERT_EQUAL_INT(cAtOk, Enable(Channel(), cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, IsEnabled(Channel()));
        }
    }

static void testAddressCompare()
    {
    helperTestAddressControl(AtHdlcChannelAddressMonitorEnable,
                             AtHdlcChannelAddressMonitorIsEnabled,
                             AtHdlcChannelExpectedAddressSet,
                             AtHdlcChannelExpectedAddressGet);
    }

static void testPacketDropCondition()
    {
    AtEncapChannel channel = (AtEncapChannel)Channel();
    static uint32 conditions[] = {cAtHdlcChannelDropConditionPktFcsError,
                                  cAtHdlcChannelDropConditionPktAddrCtrlError};
    eBool currentStatus = AtEncapChannelDropPacketConditionMaskGet(channel);
    uint8 i;

    for (i = 0; i < mCount(conditions); i++)
        {
        if (AtEncapChannelCanChangePacketDroppingCondition(channel, conditions[i]))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtEncapChannelDropPacketConditionMaskSet(channel, conditions[i], conditions[i]));
            TEST_ASSERT(conditions[i] & AtEncapChannelDropPacketConditionMaskGet(channel));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            if (conditions[i] & AtEncapChannelDropPacketConditionMaskGet(channel))
                {
                TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEncapChannelDropPacketConditionMaskSet(channel, conditions[i], 0));
                }
            else
                {
                TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtEncapChannelDropPacketConditionMaskSet(channel, conditions[i], conditions[i]));
                }
            AtTestLoggerEnable(cAtTrue);
            }
        }

    TEST_ASSERT_EQUAL_INT(cAtOk, AtEncapChannelDropPacketConditionMaskSet(channel, currentStatus, currentStatus));
    }

static void testControlCompare()
    {
    helperTestAddressControl(AtHdlcChannelControlMonitorEnable,
                             AtHdlcChannelControlMonitorIsEnabled,
                             AtHdlcChannelExpectedControlSet,
                             AtHdlcChannelExpectedControlGet);
    }

static void testControlInsert()
    {
    helperTestAddressControl(AtHdlcChannelAddressInsertionEnable,
                             AtHdlcChannelAddressInsertionIsEnabled,
                             AtHdlcChannelTxAddressSet,
                             AtHdlcChannelTxAddressGet);
    }

static void testAddressInsert()
    {
    helperTestAddressControl(AtHdlcChannelControlInsertionEnable,
                             AtHdlcChannelControlInsertionIsEnabled,
                             AtHdlcChannelTxControlSet,
                             AtHdlcChannelTxControlGet);
    }

static void testChangeIdlePattern()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelIdlePatternSet(Channel(), 0x7E));
    TEST_ASSERT_EQUAL_INT(0x7E, AtHdlcChannelIdlePatternGet(Channel()));

    AtTestLoggerEnable(cAtFalse);
    if (AtHdlcChannelIdlePatternSet(Channel(), 0xFF) != cAtOk)
        {
        TEST_ASSERT_EQUAL_INT(0x7E, AtHdlcChannelIdlePatternGet(Channel()));
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtHdlcChannelIdlePatternSet(Channel(), 0xFF));
        TEST_ASSERT_EQUAL_INT(0xFF, AtHdlcChannelIdlePatternGet(Channel()));
        }
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_HdlcChannel_Fixtures)
        {
        new_TestFixture("testAddressSizeAndControlSizeMustBeGreaterThanZero", testAddressSizeAndControlSizeMustBeGreaterThanZero),
        new_TestFixture("testChangeAddressSize", testChangeAddressSize),
        new_TestFixture("testChangeControlSize", testChangeControlSize),
        new_TestFixture("testChangeFlag", testChangeFlag),
        new_TestFixture("testCannotChangeInvalidFlagValue", testCannotChangeInvalidFlagValue),
        new_TestFixture("testChangeNumFlags", testChangeNumFlags),
        new_TestFixture("testChangeFcsMode", testChangeFcsMode),
        new_TestFixture("testCannotChangeInvalidFcsMode", testCannotChangeInvalidFcsMode),
        new_TestFixture("testChangeFcsCalculationMode", testChangeFcsCalculationMode),
        new_TestFixture("testChangeStuffMode", testChangeStuffMode),
        new_TestFixture("testCannotChangeInvalidStuffMode", testCannotChangeInvalidStuffMode),
        new_TestFixture("testChangeMtu", testChangeMtu),
        new_TestFixture("testFramingTypeMustBeValid", testFramingTypeMustBeValid),
        new_TestFixture("testScrambleEnabling", testScrambleEnabling),
        new_TestFixture("testInternalLinkMustBeValid", testInternalLinkMustBeValid),
        new_TestFixture("testChangeAddressControlBypass", testChangeAddressControlBypass),
        new_TestFixture("testChangeAddressControlCompress", testChangeAddressControlCompress),
        new_TestFixture("testAddressCompare", testAddressCompare),
        new_TestFixture("testControlCompare", testControlCompare),
        new_TestFixture("testControlInsert", testControlInsert),
        new_TestFixture("testAddressInsert", testAddressInsert),
        new_TestFixture("testPacketDropCondition", testPacketDropCondition),
        new_TestFixture("testChangeIdlePattern", testChangeIdlePattern)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_HdlcChannel_Caller, "TestSuite_HdlcChannel", NULL, NULL, TestSuite_HdlcChannel_Fixtures);

    return (TestRef)((void *)&TestSuite_HdlcChannel_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHdlcChannelTestRunner);
    }

AtChannelTestRunner AtHdlcChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtEncapChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtHdlcChannelTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtHdlcChannelTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

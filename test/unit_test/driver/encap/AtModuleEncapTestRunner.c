/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModuleEncapTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PPP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtHdlcChannel.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModuleEncapTestRunnerInternal.h"
#include "AtEncapChannelTestRunner.h"
#include "AtFrLink.h"
#include "AtMfrBundle.h"
#include "AtFrVirtualCircuit.h"
#include "AtHdlcBundle.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleEncapTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtHdlcChannel (*HdlcChannelCreate)(AtModuleEncap self, uint16 channelId);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleEncapTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncapTestRunner CurrentRunner()
    {
    return (AtModuleEncapTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModuleEncap Module()
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner()), cAtModuleEncap);
    }

static void tearDown()
    {
    AtIterator channelIterator = AtModuleEncapChannelIteratorCreate(Module());
    AtIterator bundleIterator = AtModuleEncapBundleIteratorCreate(Module());
    AtHdlcBundle bundle;
    AtEncapChannel channel;

    /* Remove all channels */
    while ((channel = (AtEncapChannel)AtIteratorNext(channelIterator)) != NULL)
        AtModuleEncapChannelDelete(Module(), AtChannelIdGet((AtChannel)channel));
    AtObjectDelete((AtObject)channelIterator);

    /* Remove all bundles */
    while ((bundle = (AtHdlcBundle)AtIteratorNext(bundleIterator)) != NULL)
        AtModuleEncapBundleDelete(Module(), AtChannelIdGet((AtChannel)bundle));
    AtObjectDelete((AtObject)bundleIterator);
    }

static void testAtLeastOneChannelMustBeSupported()
    {
    TEST_ASSERT(AtModuleEncapMaxChannelsGet(Module()) > 0);
    }

static void testCreateHdlcPppChannel()
    {
    uint32 maxChannels = AtModuleEncapMaxChannelsGet(Module());
    uint32 channelIds[] = {0, maxChannels / 2, maxChannels - 1};
    uint8 numChannel = mCount(channelIds), i;
    AtHdlcChannel channel;

    /* Only test this when PPP module is supported */
    if (AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner()), cAtModulePpp) == NULL)
        return;

    for (i = 0; i < numChannel; i++)
        {
        channel = AtModuleEncapHdlcPppChannelCreate(Module(), i);
        TEST_ASSERT_NOT_NULL(channel);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)channel));
        TEST_ASSERT_NULL(AtEncapChannelBoundPhyGet((AtEncapChannel)channel));
        }
    }

static void testCreateHdlcFrChannel()
    {
    uint32 maxChannels = AtModuleEncapMaxChannelsGet(Module());
    uint32 channelIds[] = {0, maxChannels / 2, maxChannels - 1};
    uint8 numChannel = mCount(channelIds), i;
    AtHdlcChannel channel;

    for (i = 0; i < numChannel; i++)
        {
        channel = AtModuleEncapFrChannelCreate(Module(), i);
        TEST_ASSERT_NOT_NULL(channel);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)channel));
        TEST_ASSERT_NULL(AtEncapChannelBoundPhyGet((AtEncapChannel)channel));
        }
    }

static void testCreateHdlcCiscoChannel()
    {
    /* TODO: TBD */
    }

static eBool DynamicChannelAllocation(AtModuleEncapTestRunner self)
    {
    return cAtTrue;
    }

static void testCannotCreateAnOutOfRangeChannel()
    {
    if (!mMethodsGet(mThis(CurrentRunner()))->DynamicChannelAllocation(mThis(CurrentRunner())))
        return;

    TEST_ASSERT_NULL(AtModuleEncapHdlcPppChannelCreate(Module(), AtModuleEncapMaxChannelsGet(Module())));
    }

static void testCannotGetChannelThatHasNotBeenCreated()
    {
    uint8 maxChannels = AtModuleEncapMaxChannelsGet(Module());

    if (!mMethodsGet(mThis(CurrentRunner()))->DynamicChannelAllocation(mThis(CurrentRunner())))
        return;

    TEST_ASSERT_NULL(AtModuleEncapChannelGet(Module(), 0));
    TEST_ASSERT_NULL(AtModuleEncapChannelGet(Module(), maxChannels / 2));
    TEST_ASSERT_NULL(AtModuleEncapChannelGet(Module(), maxChannels - 1));
    }

static void testCanGetFreeChannel()
    {
    uint16 maxChannels;
    uint16 freeChannel;

    if (!mMethodsGet(mThis(CurrentRunner()))->DynamicChannelAllocation(mThis(CurrentRunner())))
        return;

    maxChannels = AtModuleEncapMaxChannelsGet(Module());
    AtModuleEncapHdlcPppChannelCreate(Module(), 0);
    freeChannel = AtModuleEncapFreeChannelGet(Module());

    TEST_ASSERT(freeChannel != 0);
    TEST_ASSERT(freeChannel < maxChannels);
    }

static eBool CanTestBundle(AtModuleEncapTestRunner self)
    {
    return AtModuleEncapMaxBundlesGet((AtModuleEncap)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self)) > 0;
    }

static void testCannotCreateOutOfRangeBundle()
    {
    TEST_ASSERT_NULL(AtModuleEncapMfrBundleCreate(Module(), AtModuleEncapMaxBundlesGet(Module())));

    }

static void testCannotCreateBundleForNULLModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtModuleEncapMfrBundleCreate(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanCreateAValidBundle()
    {
    uint32 i;

    for (i = 0; i < AtModuleEncapMaxBundlesGet(Module()); i++)
        {
        AtHdlcBundle bundle = (AtHdlcBundle)AtModuleEncapMfrBundleCreate(Module(), i);
        TEST_ASSERT_NOT_NULL(bundle);
        TEST_ASSERT(AtModuleEncapBundleGet(Module(), i) == bundle);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)bundle));
        }
    }

static void testCannotCreateBundleMoreThanOneTimes()
    {
    uint32 i;

    for (i = 0; i < AtModuleEncapMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleEncapMfrBundleCreate(Module(), i));
        TEST_ASSERT_NULL(AtModuleEncapMfrBundleCreate(Module(), i));
        }
    }

static void testCannotGetABundleIfItHasNotBeenCreated()
    {
    uint32 i;

    for (i = 0; i < AtModuleEncapMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_NULL(AtModuleEncapBundleGet(Module(), i));
        }
    }

static void testCannotGetOutOfRangeBundle()
    {
    TEST_ASSERT_NULL(AtModuleEncapBundleGet(Module(), AtModuleEncapMaxBundlesGet(Module())));
    }

static void testCannotGetABundleOfANULLModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtModuleEncapBundleGet(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDeleteACreatedBundle()
    {
    uint32 i;

    for (i = 0; i < AtModuleEncapMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleEncapMfrBundleCreate(Module(), i));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEncapBundleDelete(Module(), i));
        TEST_ASSERT_NULL(AtModuleEncapBundleGet(Module(), i));
        }
    }

static void testCanDeleteABundleThatHasNotBeenCreated()
    {
    uint32 i;

    for (i = 0; i < AtModuleEncapMaxBundlesGet(Module()); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleEncapBundleDelete(Module(), i));
        }
    }

static void testCannotDeleteAnOutOfRangeBundle()
    {
    TEST_ASSERT_EQUAL_INT(cAtErrorOutOfRangParm, AtModuleEncapBundleDelete(Module(), AtModuleEncapMaxBundlesGet(Module())));
    }

static void testCannotDeleteABundleOfNULLModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModuleEncapBundleDelete(NULL, 0) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testDeleteChannel()
    {
    uint8 channelToTest = AtModuleEncapMaxChannelsGet(Module()) / 2;

    if (!mMethodsGet(mThis(CurrentRunner()))->DynamicChannelAllocation(mThis(CurrentRunner())))
            return;

    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtModuleEncapChannelDelete(Module(), channelToTest));
    TEST_ASSERT_NULL(AtModuleEncapChannelGet(Module(), channelToTest));
    }

static void testExhausted()
    {
    uint16 maxChannels = AtModuleEncapMaxChannelsGet(Module());
    uint16 i, channelToTest;

    if (!mMethodsGet(mThis(CurrentRunner()))->DynamicChannelAllocation(mThis(CurrentRunner())))
        return;

    /* Create all channels */
    for (i = 0; i < maxChannels; i++)
        {
        TEST_ASSERT_NOT_NULL(AtModuleEncapHdlcPppChannelCreate(Module(), i));
        }

    /* And there is no free channel */
    TEST_ASSERT(AtModuleEncapFreeChannelGet(Module()) >= maxChannels);

    /* Delete one */
    channelToTest = maxChannels / 3;
    TEST_ASSERT_EQUAL_INT(cAtOk, (eAtRet)AtModuleEncapChannelDelete(Module(), channelToTest));

    /* Have it as free channel */
    TEST_ASSERT_EQUAL_INT(channelToTest, AtModuleEncapFreeChannelGet(Module()));
    }

static TestRef UnitTestSuiteForBundle(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleEncap)
        {
        new_TestFixture("testCannotCreateOutOfRangeBundle", testCannotCreateOutOfRangeBundle),
        new_TestFixture("testCannotCreateBundleForNULLModule", testCannotCreateBundleForNULLModule),
        new_TestFixture("testCanCreateAValidBundle", testCanCreateAValidBundle),
        new_TestFixture("testCannotCreateBundleMoreThanOneTimes", testCannotCreateBundleMoreThanOneTimes),
        new_TestFixture("testCannotGetABundleIfItHasNotBeenCreated", testCannotGetABundleIfItHasNotBeenCreated),
        new_TestFixture("testCannotGetOutOfRangeBundle", testCannotGetOutOfRangeBundle),
        new_TestFixture("testCannotGetABundleOfANULLModule", testCannotGetABundleOfANULLModule),
        new_TestFixture("testCanDeleteACreatedBundle", testCanDeleteACreatedBundle),
        new_TestFixture("testCanDeleteABundleThatHasNotBeenCreated", testCanDeleteABundleThatHasNotBeenCreated),
        new_TestFixture("testCannotDeleteAnOutOfRangeBundle", testCannotDeleteAnOutOfRangeBundle),
        new_TestFixture("testCannotDeleteABundleOfNULLModule", testCannotDeleteABundleOfNULLModule),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleEncap_Caller, "TestSuite_EncapBundle", NULL, tearDown, TestSuite_ModuleEncap);

    return (TestRef)((void *)&TestSuite_ModuleEncap_Caller);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleEncap)
        {
        new_TestFixture("testAtLeastOneChannelMustBeSupported", testAtLeastOneChannelMustBeSupported),
        new_TestFixture("testCreateHdlcPppChannel", testCreateHdlcPppChannel),
        new_TestFixture("testCreateHdlcFrChannel", testCreateHdlcFrChannel),
        new_TestFixture("testCreateHdlcFrChannel", testCreateHdlcFrChannel),
        new_TestFixture("testCreateHdlcCiscoChannel", testCreateHdlcCiscoChannel),
        new_TestFixture("testCannotCreateAnOutOfRangeChannel", testCannotCreateAnOutOfRangeChannel),
        new_TestFixture("testCannotGetChannelThatHasNotBeenCreated", testCannotGetChannelThatHasNotBeenCreated),
        new_TestFixture("testCanGetFreeChannel", testCanGetFreeChannel),
        new_TestFixture("testDeleteChannel", testDeleteChannel),
        new_TestFixture("testExhausted", testExhausted),

        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleEncap_Caller, "TestSuite_ModuleEncap", NULL, tearDown, TestSuite_ModuleEncap);

    return (TestRef)((void *)&TestSuite_ModuleEncap_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));

    if (CanTestBundle((AtModuleEncapTestRunner)self))
        AtListObjectAdd(suites, (AtObject)UnitTestSuiteForBundle(self));

    return suites;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    mMethodsGet(mThis(self))->TestHdlc(mThis(self));
  /*  mMethodsGet(mThis(self))->TestFrLink(mThis(self));
    mMethodsGet(mThis(self))->TestMfrBundle(mThis(self)); */
    }

static AtChannel *PhysicalChannelsToTest(AtModuleEncapTestRunner self, uint16 *numChannels)
    {
    *numChannels = 0;
    return NULL;
    }

static AtChannelTestRunner CreateHdlcChannelTestRunner(AtModuleEncapTestRunner self, AtChannel hdlcChannel)
    {
    return AtHdlcChannelTestRunnerNew(hdlcChannel, (AtModuleTestRunner)self);
    }

static AtModuleEncap EncapModule(AtModuleEncapTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static AtChannel* PhysicalChannels(AtModuleEncapTestRunner self, uint16* numChannels)
    {
    return mMethodsGet(self)->PhysicalChannelsToTest(self, numChannels);
    }

static void TestHdlc(AtModuleEncapTestRunner self)
    {
    AtModuleEncap encapModule = EncapModule(self);
    uint16 maxChannels = AtModuleEncapMaxChannelsGet(encapModule);
    uint16 i;

    for (i = 0; i < maxChannels; i++)
        {
        AtChannel hdlcChannel = (AtChannel)AtModuleEncapHdlcPppChannelCreate(encapModule, i);
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(self)->CreateHdlcChannelTestRunner(self, hdlcChannel);
        uint16 numPhysicalChannels;
        AtChannel *physicalChannels = PhysicalChannels(self, &numPhysicalChannels);
        AtEncapChannelTestRunnerPhysicalChannelSet((AtChannelTestRunner)runner, physicalChannels, numPhysicalChannels);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        AtModuleEncapChannelDelete(encapModule, i);
        }
    }

static HdlcChannelCreate HdlcCreate(void)
    {
    static HdlcChannelCreate m_func = NULL;

    if (m_func == AtModuleEncapHdlcPppChannelCreate)
        m_func = AtModuleEncapCiscoHdlcChannelCreate;
    else
        m_func = AtModuleEncapHdlcPppChannelCreate;

    return m_func;
    }

static AtChannel NextPhysicalChannel(AtModuleEncapTestRunner self)
    {
    static uint16 channel_i = 0;
    AtChannel* channels;
    AtChannel channel;
    uint16 numChannels;

    channels = PhysicalChannels(self, &numChannels);
    if (channel_i >= numChannels)
        channel_i = 0;

    channel = channels[channel_i];
    channel_i++;

    return channel;
    }

static AtChannelTestRunner CreateFrLinkTestRunner(AtModuleEncapTestRunner self, AtChannel channel)
    {
    return AtFrLinkTestRunnerNew(channel, (AtModuleTestRunner) self);
    }

static void TestFrLink(AtModuleEncapTestRunner self)
    {
    uint32 i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self), cAtModuleEncap);

    AtModuleInit((AtModule)encapModule);

    for (i = 0; i < AtModuleEncapMaxChannelsGet(encapModule); i++)
        {
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtModuleEncapFrChannelCreate(encapModule, i);
        AtChannel link = (AtChannel)AtHdlcChannelHdlcLinkGet(hdlcChannel);
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(self)->CreateFrLinkTestRunner(self, link);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtModuleEncapChannelDelete(encapModule, i);
        }
    }

static AtChannelTestRunner CreateMfrBundleTestRunner(AtModuleEncapTestRunner self, AtChannel channel)
    {
    return AtMfrBundleTestRunnerNew(channel, (AtModuleTestRunner) self);
    }

static void TestMfrBundle(AtModuleEncapTestRunner self)
    {
    uint32 i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self), cAtModuleEncap);

    AtModuleInit((AtModule)encapModule);

    for (i = 0; i < AtModuleEncapMaxBundlesGet(encapModule); i++)
        {
        AtChannel hdlcBundle = (AtChannel)AtModuleEncapMfrBundleCreate(encapModule, i);
        AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(self)->CreateMfrBundleTestRunner(self, hdlcBundle);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        AtModuleEncapBundleDelete(encapModule, i);
        }
    }

static void MethodsInit(AtModuleEncapTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TestHdlc);
        mMethodOverride(m_methods, TestFrLink);
        mMethodOverride(m_methods, TestMfrBundle);
        mMethodOverride(m_methods, PhysicalChannelsToTest);
        mMethodOverride(m_methods, DynamicChannelAllocation);
        mMethodOverride(m_methods, CreateHdlcChannelTestRunner);
        mMethodOverride(m_methods, CreateFrLinkTestRunner);
        mMethodOverride(m_methods, CreateMfrBundleTestRunner);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleEncapTestRunner);
    }

AtModuleTestRunner AtModuleEncapTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtModuleEncapTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel AtModuleEncapTestRunnerHdlcChannelCreateWithPhysical(AtModuleEncapTestRunner self, uint32 channelId)
    {
    AtModuleEncap encapModule = EncapModule(self);
    AtHdlcChannel hdlcChannel = HdlcCreate()(encapModule, channelId);
    if (AtEncapChannelPhyBind((AtEncapChannel)hdlcChannel, NextPhysicalChannel(self)) == cAtOk)
        return hdlcChannel;

    AtModuleEncapChannelDelete(encapModule, channelId);
    return NULL;
    }

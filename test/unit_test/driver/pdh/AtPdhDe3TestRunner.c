/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtPdhDe3TestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : DS3/E3 test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtPdhDe3.h"
#include "AtPdhChannelTestRunnerInternal.h"
#include "AtModulePdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;
static tAtChannelTestRunnerMethods m_AtChannelTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe3 De3()
    {
    return (AtPdhDe3)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    AtChannelInit((AtChannel)De3());
    }

static void testCanChangeFramingType()
    {
    uint32 i;
    static eAtPdhDe3FrameType frameTypes[] = {cAtPdhDs3Unfrm,
                                              cAtPdhDs3FrmCbitUnChn,
                                              cAtPdhDs3FrmCbitChnl28Ds1s,
                                              cAtPdhDs3FrmCbitChnl21E1s,
                                              cAtPdhDs3FrmM23,
                                              cAtPdhE3Unfrm,
                                              cAtPdhE3Frmg832,
                                              cAtPdhE3FrmG751};
    for (i = 0; i < mCount(frameTypes); i++)
        {
        AtPdhChannel pdhChannel = (AtPdhChannel)De3();

        if (AtPdhChannelFrameTypeIsSupported(pdhChannel, frameTypes[i]))
            {
            TEST_ASSERT(AtPdhChannelFrameTypeSet(pdhChannel, frameTypes[i]) == cAtOk);
            TEST_ASSERT_EQUAL_INT(frameTypes[i], AtPdhChannelFrameTypeGet(pdhChannel));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtPdhChannelFrameTypeSet(pdhChannel, frameTypes[i]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testCannotChangeInvalidFramingType()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPdhChannelFrameTypeSet((AtPdhChannel)De3(), 0xFF) != cAtOk);
    TEST_ASSERT(AtPdhChannelFrameTypeSet((AtPdhChannel)De3(), cAtPdhDe3FrameUnknown) != cAtOk);
    TEST_ASSERT(AtPdhChannelFrameTypeSet(NULL, cAtPdhE3Unfrm));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeToSystemTimingMode()
    {
    AtChannel channel = (AtChannel)De3();
    AtModulePdhTestRunner moduleRunner = (AtModulePdhTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet(channel, cAtTimingModeSys, NULL));

    /* Some products have special system timing mode, so do not need to expect
     * from this common place. Let concrete determine how to expect */
    if (AtModulePdhTestRunnerHasSpecialSystemTiming(moduleRunner))
        return;

    TEST_ASSERT_EQUAL_INT(cAtTimingModeSys, AtChannelTimingModeGet(channel));
    TEST_ASSERT(AtChannelTimingSourceGet(channel) == NULL);
    }

static void testChangeToLoopTimingMode()
    {
    AtChannel channel = (AtChannel)De3();
    AtChannel invalidChannel = (AtChannel)0xcafecafe;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet(channel, cAtTimingModeLoop, NULL));
    TEST_ASSERT_EQUAL_INT(cAtTimingModeLoop, AtChannelTimingModeGet(channel));
    TEST_ASSERT(channel == AtChannelTimingSourceGet(channel));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet(channel, cAtTimingModeLoop, channel));
    TEST_ASSERT_EQUAL_INT(cAtTimingModeLoop, AtChannelTimingModeGet(channel));
    TEST_ASSERT(channel == AtChannelTimingSourceGet(channel));

    /* Not crash when input other channel, even when it is invalid */
    TEST_ASSERT(AtChannelTimingSet(channel, cAtTimingModeLoop, invalidChannel) == cAtOk);
    }

static void testCannotChangeTimingModeOfNullChannel()
    {
    TEST_ASSERT(AtChannelTimingSet(NULL, cAtTimingModeSys, NULL));
    }

static void testChangeSlaveTimingMode()
    {
    AtChannel channel = (AtChannel)De3();
    AtChannel *otherChannels;
    uint32 numOtherChannels;

    /* Loop timing is a special case of slave timing mode */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet(channel, cAtTimingModeSlave, channel));
    TEST_ASSERT_EQUAL_INT(cAtTimingModeLoop, AtChannelTimingModeGet(channel));
    TEST_ASSERT(channel == AtChannelTimingSourceGet(channel));

    /* Can only test slave timing mode if master timing source is setup */
    otherChannels = AtChannelTestRunnerOtherChannelsGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner(), &numOtherChannels);
    if (numOtherChannels == 0)
        return;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtChannelTimingSet(channel, cAtTimingModeSlave, (AtChannel)otherChannels[0]) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    TEST_ASSERT_EQUAL_INT(cAtTimingModeSlave, AtChannelTimingModeGet(channel));
    TEST_ASSERT((AtChannel)otherChannels[0] == AtChannelTimingSourceGet(channel));
    }

static void testCannotChangeInvalidTimingMode()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtChannelTimingSet((AtChannel)De3(), cAtTimingModeUnknown, NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanGetAlarm()
    {
    /* Just can test if software does not crash. */
    AtChannelAlarmGet((AtChannel)De3());
    }

static void testCanGetInterrupt()
    {
    /* Just can test if software does not crash. */
    AtChannelAlarmInterruptGet((AtChannel)De3());
    AtChannelAlarmInterruptClear((AtChannel)De3());
    }

static void testCanGetCounters()
    {
    static const eAtPdhDe3CounterType counterTypes[] = {cAtPdhDe3CounterBip8,
                                                        cAtPdhDe3CounterRei,
                                                        cAtPdhDe3CounterFBit,
                                                        cAtPdhDe3CounterPBit,
                                                        cAtPdhDe3CounterCPBit};
    uint8 i;

    /* Just can test if software does not crash. */
    for (i = 0; i < mCount(counterTypes); i++)
        {
        AtChannelCounterGet((AtChannel)De3(), counterTypes[i]);
        AtChannelCounterClear((AtChannel)De3(), counterTypes[i]);
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_PdhDe3_Fixtures)
        {
        new_TestFixture("testCanChangeFramingType", testCanChangeFramingType),
        new_TestFixture("testCannotChangeInvalidFramingType", testCannotChangeInvalidFramingType),
        new_TestFixture("testChangeToSystemTimingMode", testChangeToSystemTimingMode),
        new_TestFixture("testChangeToLoopTimingMode", testChangeToLoopTimingMode),
        new_TestFixture("testCannotChangeTimingModeOfNullChannel", testCannotChangeTimingModeOfNullChannel),
        new_TestFixture("testChangeSlaveTimingMode", testChangeSlaveTimingMode),
        new_TestFixture("testCannotChangeInvalidTimingMode", testCannotChangeInvalidTimingMode),
        new_TestFixture("testCanGetAlarm", testCanGetAlarm),
        new_TestFixture("testCanGetInterrupt", testCanGetInterrupt),
        new_TestFixture("testCanGetCounters", testCanGetCounters)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_PdhDe3_Caller, "TestSuite_PdhDe3", setUp, NULL, TestSuite_PdhDe3_Fixtures);

    return (TestRef)((void *)&TestSuite_PdhDe3_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtSurEngineTestRunner CreateSurEngineTestRunner(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule)
    {
    return AtModuleSurTestRunnerCreatePdhDe3SurEngineTestRunner(factoryModule, AtChannelSurEngineGet((AtChannel)De3()));
    }

static void OverrideAtChannelTestRunner(AtChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelTestRunnerOverride, (void *)self->methods, sizeof(m_AtChannelTestRunnerOverride));

        mMethodOverride(m_AtChannelTestRunnerOverride, CreateSurEngineTestRunner);
        }

    mMethodsSet(self, &m_AtChannelTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtChannelTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe3TestRunner);
    }

AtChannelTestRunner AtPdhDe3TestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPdhDe3TestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPdhDe3TestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

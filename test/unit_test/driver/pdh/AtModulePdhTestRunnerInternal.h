/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtModulePdhTestRunnerInternal.h
 * 
 * Created Date: Jun 19, 2014
 *
 * Description : PDH Module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPDHTESTRUNNERINTERNAL_H_
#define _ATMODULEPDHTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModulePdhTestRunner.h"
#include "AtPdhDe1PrmTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePdhTestRunnerMethods
    {
    void (*TestModule)(AtModulePdhTestRunner self);
    AtChannelTestRunner (*CreateDe1TestRunner)(AtModulePdhTestRunner self, AtChannel de1);
    AtChannelTestRunner (*CreateDe3TestRunner)(AtModulePdhTestRunner self, AtChannel de3);
    AtPdhDe1PrmTestRunner (*CreateDe1PrmTestRunner)(AtModulePdhTestRunner self, AtPdhPrmController controller);
    AtUnittestRunner (*CreateDe1InterruptTestRunner)(AtModulePdhTestRunner self, AtChannel de1);
    AtUnittestRunner (*CreateDe3InterruptTestRunner)(AtModulePdhTestRunner self, AtChannel de3);
    eBool (*ShouldTestDe3InStm)(AtModulePdhTestRunner self, AtSdhLine line);
    eBool (*CanUseTimeslot16InNxDs0)(AtModulePdhTestRunner self);
    eBool (*CanUseTimeslot0InNxDs0)(AtModulePdhTestRunner self);
    eBool (*HasSpecialSystemTiming)(AtModulePdhTestRunner self);
    eBool (*ShouldTestDe1InLine)(AtModulePdhTestRunner self, uint8 lineId);
    eBool (*ShouldTestDe3InLine)(AtModulePdhTestRunner self, uint8 lineId);
    eBool (*ShouldTestMaximumNumberOfDe1Get)(AtModulePdhTestRunner self);
    void (*WillTestSerialLine)(AtModulePdhTestRunner self);
    uint32 (*NumDe3SerialLines)(AtModulePdhTestRunner self);
    eBool (*CanTestDe1Prm)(AtModulePdhTestRunner self);
    uint8 *(*AllTestedLinesAllocate)(AtModulePdhTestRunner self, uint8 *numLines);
    }tAtModulePdhTestRunnerMethods;

typedef struct tAtModulePdhTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModulePdhTestRunnerMethods *methods;

    uint8 *allLines;
    uint8 numLines;
    }tAtModulePdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);
AtModuleTestRunner AtPdhModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);
AtModuleTestRunner AtStmModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#endif /* _ATMODULEPDHTESTRUNNERINTERNAL_H_ */


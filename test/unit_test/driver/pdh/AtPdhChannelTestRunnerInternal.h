/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtPdhChannelTestRunnerInternal.h
 * 
 * Created Date: Jun 20, 2014
 *
 * Description : ETH Flow test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHCHANNELTESTRUNNERINTERNAL_H_
#define _ATPDHCHANNELTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhChannelTestRunner * AtPdhChannelTestRunner;

typedef struct tAtPdhChannelTestRunnerMethods
    {
    uint8 dummy;
    }tAtPdhChannelTestRunnerMethods;

typedef struct tAtPdhChannelTestRunner
    {
    tAtChannelTestRunner super;
    const tAtPdhChannelTestRunnerMethods *methods;
    }tAtPdhChannelTestRunner;

typedef struct tAtPdhDe1TestRunner
    {
    tAtPdhChannelTestRunner super;
    }tAtPdhDe1TestRunner;

typedef struct tAtPdhDe3TestRunner
    {
    tAtPdhChannelTestRunner super;
    }tAtPdhDe3TestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtPdhChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPdhDe1TestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPdhDe3TestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

#endif /* _ATPDHCHANNELTESTRUNNERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe1PrmTestRunnerInternal.h
 * 
 * Created Date: Jan 28, 2016
 *
 * Description : PRM test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE1PRMTESTRUNNERINTERNAL_H_
#define _ATPDHDE1PRMTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../runner/AtUnittestRunnerInternal.h"
#include "../../../../driver/src/generic/pdh/AtPdhPrmControllerInternal.h"
#include "AtPdhDe1Prm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhDe1PrmTestRunner *AtPdhDe1PrmTestRunner;

typedef struct tAtPdhDe1PrmTestRunner
    {
    tAtUnittestRunner super;

    /* Private data */
    AtModulePdhTestRunner module;
    AtPdhPrmController controller;
    }tAtPdhDe1PrmTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1PrmTestRunner AtPdhDe1PrmTestRunnerObjectInit(AtPdhDe1PrmTestRunner self,
                                                      AtModulePdhTestRunner moduleRunner,
                                                      AtPdhPrmController controller);
AtPdhDe1PrmTestRunner AtPdhDe1PrmTestRunnerNew(AtModulePdhTestRunner moduleRunner, AtPdhPrmController controller);

AtModulePdhTestRunner AtPdhDe1PrmTestRunnerModuleGet(AtPdhDe1PrmTestRunner self);
AtPdhPrmController AtPdhDe1PrmTestRunnerControllerGet(AtPdhDe1PrmTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDE1PRMTESTRUNNERINTERNAL_H_ */


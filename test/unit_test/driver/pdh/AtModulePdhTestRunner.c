/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModulePdhTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PPP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModulePdh.h"
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"
#include "AtPdhSerialLine.h"
#include "AtSdhLine.h"
#include "AtModulePdhTestRunnerInternal.h"
#include "../sdh/AtModuleSdhTests.h"
#include "../../../../driver/src/generic/sdh/AtSdhChannelInternal.h"
#include "../../../../driver/src/generic/pdh/AtPdhPrmController.h"
#include "../../../../driver/src/generic/pdh/AtPdhDe1Internal.h"
#include "../interrupt/AtChannelInterruptTestRunner.h"
#include "../sdh/AtModuleSdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModulePdhTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModulePdhTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool PdhDe1FrameTypeIsValid(AtPdhDe1 de1);
static void TestAllDe1sInSdhChannel(AtSdhChannel channel);
static void TestAllDe1sInPdhChannel(AtPdhChannel channel);

/*--------------------------- Implementation ---------------------------------*/
static AtModulePdhTestRunner CurrentRunner()
    {
    return (AtModulePdhTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModulePdh PdhModule()
    {
    return (AtModulePdh)(AtDeviceModuleGet(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner()), cAtModulePdh));
    }

static AtModuleSdh SdhModule(AtDevice device)
    {
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eBool ShouldTestInterrupt(AtModulePdhTestRunner self)
    {
    if (AtTestIsSimulationTesting())
        return cAtFalse;

    return cAtTrue;
    }

static AtUnittestRunner CreateDe1InterruptTestRunner(AtModulePdhTestRunner self, AtChannel de1)
    {
    AtSdhVc vc = AtPdhChannelVcGet((AtPdhChannel)de1);

    if (vc != NULL)
        return AtPdhVcDe1InterruptTestRunnerNew(de1);

    if (AtPdhChannelParentChannelGet((AtPdhChannel)de1))
        return AtPdhDe3De1InterruptTestRunnerNew(de1);

    return AtPdhLiuDe1InterruptTestRunnerNew(de1);
    }

static AtDevice Device(void)
    {
    return AtModuleTestRunnerDeviceGet((AtModuleTestRunner)CurrentRunner());
    }

static void TestE1(AtModulePdhTestRunner self, AtPdhDe1 de1)
    {
    AtModulePrbs prbsModule;
    AtPrbsEngine engine;
    AtUnittestRunner interruptRunner;
    AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(self)->CreateDe1TestRunner(self, (AtChannel)de1);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);

    if (ShouldTestInterrupt(self) == cAtFalse)
        return;

    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm);
    interruptRunner = mMethodsGet(self)->CreateDe1InterruptTestRunner(self, (AtChannel)de1);
    prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
    engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, 0, de1);
    AtPrbsEngineEnable(engine, cAtTrue);
    AtUnittestRunnerRun(interruptRunner);
    AtUnittestRunnerDelete(interruptRunner);
    AtModulePrbsEngineDelete(prbsModule, 0);
    }

static void TestDs1(AtModulePdhTestRunner self, AtPdhDe1 de1)
    {
    AtModulePrbs prbsModule;
    AtPrbsEngine engine;
    AtUnittestRunner interruptRunner;
    AtUnittestRunner runner = (AtUnittestRunner)mMethodsGet(self)->CreateDe1TestRunner(self, (AtChannel)de1);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);

    if (ShouldTestInterrupt(self) == cAtFalse)
        return;

    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmSf);
    interruptRunner = mMethodsGet(self)->CreateDe1InterruptTestRunner(self, (AtChannel)de1);
    prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
    engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, 0, de1);
    AtPrbsEngineEnable(engine, cAtTrue);
    AtUnittestRunnerRun(interruptRunner);
    AtUnittestRunnerDelete(interruptRunner);
    AtModulePrbsEngineDelete(prbsModule, 0);
    }

static void TestDe1(AtModulePdhTestRunner self, AtPdhDe1 de1)
    {
    TestE1(self, de1);
    TestDs1(self, de1);
    }

static void TestDs1Prm(AtModulePdhTestRunner self, AtPdhDe1 de1)
    {
    AtPdhPrmController controller;
    AtUnittestRunner runner;
    uint16 curFramedType;

    if (!mMethodsGet(self)->CanTestDe1Prm(self))
        return;

    /* Setup condition can test PRM */
    curFramedType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);
    if (!AtPdhChannelFrameTypeIsSupported((AtPdhChannel)de1, cAtPdhDs1FrmEsf))
        return;

    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhDs1FrmEsf) == cAtOk);
    controller = AtPdhDe1PrmControllerGet(de1);
    runner = (AtUnittestRunner)mMethodsGet(self)->CreateDe1PrmTestRunner(self, controller);
    AtUnittestRunnerRun(runner);

    /* Revert current status */
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, curFramedType);
    AtUnittestRunnerDelete(runner);
    }

static void TestLiuDe1(AtModulePdhTestRunner self)
    {
    uint32 i;
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);

    for (i = 0; i < AtModulePdhNumberOfDe1sGet(pdhModule); i++)
        {
        AtPdhDe1 de1 = AtModulePdhDe1Get(pdhModule, i);
        TestDe1(self, de1);
        TestDs1Prm(self, de1);
        }
    }

static void TestDe1sInSdhSubChannelWithIndex(AtSdhChannel channel, uint8 subChannelIndex)
    {
    /* This sub channel is not VC-1x, test all of its sub channels */
    AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, subChannelIndex);

    /* Map this VC-1x to DS1/E1 and test this */
    if (AtSdhChannelTypeGet(subChannel) == cAtSdhChannelTypeVc12)
        {
        AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapDe1);
        TestE1(CurrentRunner(), (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
        return;
        }

    if (AtSdhChannelTypeGet(subChannel) == cAtSdhChannelTypeVc11)
        {
        AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapDe1);
        TestDs1(CurrentRunner(), (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
        TestDs1Prm(CurrentRunner(), (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
        return;
        }

    TestAllDe1sInSdhChannel(subChannel);
    }

static void TestAllDe1sInSdhChannel(AtSdhChannel channel)
    {
    uint32 subChannel_i;
    uint32 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint32 numTestedChannels = numSubChannels;
    uint32 testedIndex;

    if (numTestedChannels == 0)
        return;

    if (AtTestQuickTestIsEnabled())
        {
        uint32 value = AtTestRandomNumChannels(numTestedChannels);
        numTestedChannels = mMax(1, value);
        }

    for (subChannel_i = 0; subChannel_i < numTestedChannels; subChannel_i++)
        {
        testedIndex = subChannel_i;

        if (AtTestQuickTestIsEnabled())
            testedIndex = AtTestRandom(numSubChannels, testedIndex);

        TestDe1sInSdhSubChannelWithIndex(channel, testedIndex);
        }
    }

static void TestAllDe1sInVc12Line(AtSdhLine line)
    {
    if (AtSdhLineRateGet(line) > cAtSdhLineRateStm0)
        {
        AtSdhTestCreateAllVc1xInTug3s(line, cAtSdhChannelTypeVc12);
        TestAllDe1sInSdhChannel((AtSdhChannel)line);
        }

    AtSdhTestCreateAllVc1xInVc3s(line, cAtSdhChannelTypeVc12);
    TestAllDe1sInSdhChannel((AtSdhChannel)line);
    }

static void TestAllDe1sInVc11Line(AtSdhLine line)
    {
    if (AtSdhLineRateGet(line) > cAtSdhLineRateStm0)
        {
        AtSdhTestCreateAllVc1xInTug3s(line, cAtSdhChannelTypeVc11);
        TestAllDe1sInSdhChannel((AtSdhChannel)line);
        }

    AtSdhTestCreateAllVc1xInVc3s(line, cAtSdhChannelTypeVc11);
    TestAllDe1sInSdhChannel((AtSdhChannel)line);
    }

static void TestAllDe1sInLine(AtModulePdhTestRunner self, AtSdhLine line)
    {
    TestAllDe1sInVc12Line(line);
    TestAllDe1sInVc11Line(line);
    }

static eAtRet LineRateDefault(AtSdhLine line)
    {
    eAtSdhLineRate rates[] = {cAtSdhLineRateStm16, cAtSdhLineRateStm4, cAtSdhLineRateStm1, cAtSdhLineRateStm0};
    uint32 rate_i;

    for (rate_i = 0; rate_i < mCount(rates); rate_i++)
        {
        if (AtSdhLineRateIsSupported(line, rates[rate_i]))
            return AtSdhLineRateSet(line, rates[rate_i]);
        }

    return cAtErrorModeNotSupport;
    }

static uint8 *AllTestedLinesAllocate(AtModulePdhTestRunner self, uint8 *numLines)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return AtModuleSdhAllApplicableLinesAllocate(sdhModule, numLines);
    }

static uint8 *AllLines(AtModulePdhTestRunner self, uint8 *numLines)
    {
    if (self->allLines == NULL)
        self->allLines = mMethodsGet(self)->AllTestedLinesAllocate(self, &(self->numLines));

    if (numLines)
        *numLines = self->numLines;

    return self->allLines;
    }

static void TestStmDe1(AtModulePdhTestRunner self)
    {
    uint8 numLines;
    uint8 *testedLines;
    uint8 line_i;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    if (AtDeviceModuleGet(device, cAtModuleSdh) == NULL)
        return;

    testedLines = AllLines(self, &numLines);
    AtAssert(numLines > 0);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line;
        uint8 lineId;
        AtPdhSerialLine serLine;

        lineId = testedLines[line_i];
        if (!mMethodsGet(self)->ShouldTestDe1InLine(self, lineId))
            continue;

        serLine = AtModulePdhDe3SerialLineGet(PdhModule(), lineId);
        if (serLine)
            AtPdhSerialLineModeSet(serLine, cAtPdhDe3SerialLineModeEc1);

        line = AtModuleSdhLineGet(SdhModule(device), lineId);
        AtAssert(LineRateDefault(line) == cAtOk);
        TestAllDe1sInLine(self, line);
        }
    }

static void TestDe1sInPdhSubChannelWithIndex(AtPdhChannel channel, uint8 subChannelIndex)
    {
    AtPdhChannel subChannel = AtPdhChannelSubChannelGet(channel, subChannelIndex);
    if (AtPdhChannelTypeGet(subChannel) != cAtPdhChannelTypeE1)
        {
        TestAllDe1sInPdhChannel(subChannel);
        return;
        }

    AtPdhChannelFrameTypeSet(subChannel, cAtPdhE1Frm);
    TestDe1(CurrentRunner(), (AtPdhDe1)subChannel);
    }

static void TestAllDe1sInPdhChannel(AtPdhChannel channel)
    {
    uint32 subChannel_i;
    uint32 numSubChannels = AtPdhChannelNumberOfSubChannelsGet(channel);
    uint32 numTestedChannels = numSubChannels;
    uint32 testedIndex;

    if (numTestedChannels == 0)
        return;

    if (AtTestQuickTestIsEnabled())
        {
        uint32 value = AtTestRandomNumChannels(numTestedChannels);
        numTestedChannels = mMax(1, value);
        }

    for (subChannel_i = 0; subChannel_i < numTestedChannels; subChannel_i++)
        {
        testedIndex = subChannel_i;

        if (AtTestQuickTestIsEnabled())
            testedIndex = AtTestRandom(numSubChannels, testedIndex);

        TestDe1sInPdhSubChannelWithIndex(channel, testedIndex);
        }
    }

static void TestAllDe1sInDe3(AtPdhDe3 de3)
    {
    AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhDs3FrmCbitChnl21E1s);
    TestAllDe1sInPdhChannel((AtPdhChannel)de3);
    }

static void TestDe3De1(AtModulePdhTestRunner self)
    {
    uint8 de3Id;
    uint32 numDe3s;
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);

    if (AtModulePdhDe3Get(pdhModule, 0) == NULL)
        return;

    numDe3s = AtModulePdhNumberOfDe3sGet(pdhModule);

    if (AtTestQuickTestIsEnabled())
        numDe3s = AtTestRandomNumChannels(numDe3s);

    for (de3Id = 0; de3Id < numDe3s; de3Id++)
        {
        AtPdhDe3 de3;
        uint32 testedDe3Id = de3Id;
        AtPdhSerialLine serLine;

        if (AtTestQuickTestIsEnabled())
            testedDe3Id = AtTestRandom(numDe3s, testedDe3Id);

        serLine = AtModulePdhDe3SerialLineGet(PdhModule(), testedDe3Id);
        if (serLine)
            AtPdhSerialLineModeSet(serLine, cAtPdhDe3SerialLineModeDs3);

        de3 = AtModulePdhDe3Get(pdhModule, testedDe3Id);
        TestAllDe1sInDe3(de3);
        }
    }

static void TestAllDe1s(AtModulePdhTestRunner self)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    if (AtModulePdhNumberOfDe1sGet(pdhModule) != 0)
        TestLiuDe1(self);

    if (AtModuleSdhMaxLinesGet(SdhModule(device)))
        TestStmDe1(self);

    if (AtModulePdhNumberOfDe3sGet(pdhModule) != 0)
        TestDe3De1(self);
    }

static AtUnittestRunner CreateDe3InterruptTestRunner(AtModulePdhTestRunner self, AtChannel de3)
    {
    AtUnused(self);
    if(AtPdhChannelVcGet((AtPdhChannel)de3))
        return AtPdhVcDe3InterruptTestRunnerNew(de3);
    return AtPdhLiuDe3InterruptTestRunnerNew(de3);
    }

static void TestLiuDe3(AtModulePdhTestRunner self)
    {
    uint32 i;
    AtModulePrbs prbsModule;
    AtPrbsEngine engine;
    AtModulePdh pdhModule = (AtModulePdh)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);

    for (i = 0; i < AtModulePdhNumberOfDe3sGet(pdhModule); i++)
        {
        AtUnittestRunner interruptRunner, de3TestRunner;
        AtPdhSerialLine serLine = AtModulePdhDe3SerialLineGet(pdhModule, i);
        AtChannel de3;
        uint32 serialLineMode = cAtPdhDe3SerialLineModeEc1;

        if (serLine && ((serialLineMode = AtPdhSerialLineModeGet(serLine)) == cAtPdhDe3SerialLineModeEc1))
            AtPdhSerialLineModeSet(serLine, cAtPdhDe3SerialLineModeDs3);

        de3 = (AtChannel)AtModulePdhDe3Get(pdhModule, i);
        de3TestRunner = (AtUnittestRunner)mMethodsGet(self)->CreateDe3TestRunner(self, de3);

        AtUnittestRunnerRun(de3TestRunner);
        AtUnittestRunnerDelete(de3TestRunner);

        if (ShouldTestInterrupt(self) == cAtFalse)
            continue;

        AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhDs3FrmCbitUnChn);
        interruptRunner = mMethodsGet(self)->CreateDe3InterruptTestRunner(self, de3);
        prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
        engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, 0, (AtPdhDe3)de3);
        AtPrbsEngineEnable(engine, cAtTrue);
        AtUnittestRunnerRun(interruptRunner);
        AtUnittestRunnerDelete(interruptRunner);
        AtModulePrbsEngineDelete(prbsModule, 0);

        if ((serLine) && (serialLineMode == cAtPdhDe3SerialLineModeEc1))
            AtPdhSerialLineModeSet(serLine, cAtPdhDe3SerialLineModeEc1);
        }
    }

static void TestAllDe3sInSdhChannel(AtSdhChannel channel)
    {
    uint8 i;
    AtModulePrbs prbsModule;
    AtPrbsEngine engine;

    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(channel); i++)
        {
        /* This sub channel is not VC-1x, test all of its sub channels */
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, i);
        AtUnittestRunner runner;
        AtUnittestRunner interruptRunner;
        AtChannel de3;

        /* If this channel is TUG-3, map it VC3 */
        if (AtSdhChannelTypeGet(subChannel) == cAtSdhChannelTypeTug3)
            {
            AtSdhChannel tu3;

            AtSdhChannelMapTypeSet(subChannel, cAtSdhTugMapTypeTug3MapVc3);
            tu3 = AtSdhChannelSubChannelGet(subChannel, 0);
            subChannel = AtSdhChannelSubChannelGet(tu3, 0);
            }

        if (AtSdhChannelTypeGet(subChannel) == cAtSdhChannelTypeAu3)
            subChannel = AtSdhChannelSubChannelGet(subChannel, 0);

        /* This channel is not VC-3, test all of its sub channels */
        if (AtSdhChannelTypeGet(subChannel) != cAtSdhChannelTypeVc3)
            {
            TestAllDe3sInSdhChannel(subChannel);
            continue;
            }

        /* Map this VC-3 to DS3/E3 and test this */
        if (AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc3MapDe3) == cAtErrorModeNotSupport)
            continue;

        de3 = (AtChannel)AtSdhChannelMapChannelGet(subChannel);
        runner = (AtUnittestRunner)mMethodsGet(CurrentRunner())->CreateDe3TestRunner(CurrentRunner(), de3);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        if (ShouldTestInterrupt(CurrentRunner()) == cAtFalse)
            continue;

        AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhDs3FrmCbitUnChn);
        interruptRunner = mMethodsGet(CurrentRunner())->CreateDe3InterruptTestRunner(CurrentRunner(), (AtChannel)de3);
        prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
        engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, 0, (AtPdhDe3)de3);
        AtPrbsEngineEnable(engine, cAtTrue);
        AtUnittestRunnerRun(interruptRunner);
        AtUnittestRunnerDelete(interruptRunner);
        AtModulePrbsEngineDelete(prbsModule, 0);
        }
    }

static eBool ShouldTestDe3InStm(AtModulePdhTestRunner self, AtSdhLine line)
    {
    return cAtTrue;
    }

static void TestAllDe3sInLine(AtSdhLine line)
    {
    AtModulePdhTestRunner runner = CurrentRunner();

    if (line == NULL)
        return;

    if (!mMethodsGet(runner)->ShouldTestDe3InStm(runner, line))
        {
        AtPrintc(cSevWarning, "WARNING: Ignore testing DS3/E3 in STM Line\r\n");
        return;
        }

    AtSdhTestCreateAllVc3sInAu3s(line, NULL);
    TestAllDe3sInSdhChannel((AtSdhChannel)line);
    AtSdhTestCreateAllTug3(line, NULL);
    TestAllDe3sInSdhChannel((AtSdhChannel)line);
    }

static void TestStmDe3(AtModulePdhTestRunner self)
    {
    uint8 numLines, *testedLines;
    uint8 line_i;
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    if (SdhModule(device) == NULL)
        return;

    testedLines = AllLines(self, &numLines);
    AtAssert(numLines);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line;
        uint8 lineId = testedLines[line_i];

        if (!mMethodsGet(self)->ShouldTestDe3InLine(self, lineId))
            continue;

        line = AtModuleSdhLineGet(SdhModule(device), lineId);
        AtAssert(LineRateDefault(line) == cAtOk);
        TestAllDe3sInLine(AtModuleSdhLineGet(SdhModule(device), lineId));
        }
    }

static void TestDe3(AtModulePdhTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);

    if (AtModulePdhNumberOfDe3sGet(PdhModule()) != 0)
        TestLiuDe3(self);

    if (AtModuleSdhMaxLinesGet(SdhModule(device)) != 0)
        TestStmDe3(self);
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);

    mMethodsGet(mThis(self))->TestModule(mThis(self));
    TestAllDe1s(mThis(self));
    TestDe3(mThis(self));
    }

static AtChannelTestRunner CreateDe1TestRunner(AtModulePdhTestRunner self, AtChannel de1)
    {
    return AtPdhDe1TestRunnerNew(de1, (AtModuleTestRunner)self);
    }

static AtChannelTestRunner CreateDe3TestRunner(AtModulePdhTestRunner self, AtChannel de3)
    {
    return AtPdhDe3TestRunnerNew(de3, (AtModuleTestRunner)self);
    }

static eBool CanUseTimeslot16InNxDs0(AtModulePdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool CanUseTimeslot0InNxDs0(AtModulePdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool HasSpecialSystemTiming(AtModulePdhTestRunner self)
    {
    return cAtFalse;
    }

static eBool ShouldTestDe1InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    return cAtTrue;
    }

static eBool ShouldTestDe3InLine(AtModulePdhTestRunner self, uint8 lineId)
    {
    return cAtTrue;
    }

static eBool ShouldTestMaximumNumberOfDe1Get(AtModulePdhTestRunner self)
    {
    return cAtTrue;
    }

static void testMaximumNumberOfDe1CanBeRetrieved()
    {
    AtModulePdhTestRunner pdhTestRunner = CurrentRunner();

    if (!mMethodsGet(pdhTestRunner)->ShouldTestMaximumNumberOfDe1Get(pdhTestRunner))
        return;
    TEST_ASSERT(AtModulePdhNumberOfDe1sGet(PdhModule()));
    }

static void testCannotGetOutOfRangeDe1Id()
    {
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(PdhModule());
    TEST_ASSERT_NULL(AtModulePdhDe1Get(PdhModule(), numDe1s));
    }

static void de1DefaultConfigurationMustBeValid(AtPdhDe1 de1)
    {
    TEST_ASSERT(PdhDe1FrameTypeIsValid(de1));
    TEST_ASSERT(!AtPdhDe1SignalingIsEnabled(de1));
    TEST_ASSERT(AtChannelTimingModeGet((AtChannel)de1) == cAtTimingModeSys);
    TEST_ASSERT(AtChannelIsEnabled((AtChannel)de1));
    }

static void testAllDe1ObjectsCanBeRetrieved()
    {
    uint16 i;
    AtPdhDe1 de1;

    for (i = 0; i < AtModulePdhNumberOfDe1sGet(PdhModule()); i++)
        {
        de1 = AtModulePdhDe1Get(PdhModule(), i);
        TEST_ASSERT_NOT_NULL(de1);
        TEST_ASSERT_EQUAL_INT(i, AtChannelIdGet((AtChannel)de1));
        de1DefaultConfigurationMustBeValid(de1);
        }
    }

static void testNumDe3SerialLinesMustBeEnough()
    {
    AtModulePdhTestRunner self = CurrentRunner();
    uint32 numSerialLines = mMethodsGet(self)->NumDe3SerialLines(self);
    TEST_ASSERT_EQUAL_INT(numSerialLines, AtModulePdhNumberOfDe3SerialLinesGet(PdhModule()));
    }

static void testCanAccessSerialLine()
    {
    AtModulePdhTestRunner self = CurrentRunner();
    uint32 numSerialLines = mMethodsGet(self)->NumDe3SerialLines(self);
    uint32 serialLine_i;
    AtModulePdh pdhModule = PdhModule();

    mMethodsGet(self)->WillTestSerialLine(self);

    for (serialLine_i = 0; serialLine_i < numSerialLines; serialLine_i++)
        {
        AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(pdhModule, serialLine_i);
        TEST_ASSERT(serialLine);

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeEc1));
        TEST_ASSERT_EQUAL_INT(cAtPdhDe3SerialLineModeEc1, AtPdhSerialLineModeGet(serialLine));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeDs3));
        TEST_ASSERT_EQUAL_INT(cAtPdhDe3SerialLineModeDs3, AtPdhSerialLineModeGet(serialLine));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeE3));
        TEST_ASSERT_EQUAL_INT(cAtPdhDe3SerialLineModeE3, AtPdhSerialLineModeGet(serialLine));
        }
    }

static TestRef _TestSuite()
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModulePdh_Fixtures)
        {
        new_TestFixture("testMaximumNumberOfDe1CanBeRetrieved", testMaximumNumberOfDe1CanBeRetrieved),
        new_TestFixture("testCannotGetOutOfRangeDe1Id", testCannotGetOutOfRangeDe1Id),
        new_TestFixture("testAllDe1ObjectsCanBeRetrieved", testAllDe1ObjectsCanBeRetrieved),
        new_TestFixture("testAllDe1ObjectsCanBeRetrieved", testNumDe3SerialLinesMustBeEnough),
        new_TestFixture("testCanAccessSerialLine", testCanAccessSerialLine)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModulePdh_Caller, "TestSuite_ModulePdh", NULL, NULL, TestSuite_ModulePdh_Fixtures);

    return (TestRef)((void *)&TestSuite_ModulePdh_Caller);
    }

static void TestModule(AtModulePdhTestRunner self)
    {
    TextUIRunner_runTest(_TestSuite());
    AtAssert(AtModuleInit(AtModuleTestRunnerModuleGet((AtModuleTestRunner)self)) == cAtOk);
    }

static uint32 NumDe3SerialLines(AtModulePdhTestRunner self)
    {
    return 0;
    }

static void WillTestSerialLine(AtModulePdhTestRunner self)
    {
    }

static AtPdhDe1PrmTestRunner CreateDe1PrmTestRunner(AtModulePdhTestRunner self, AtPdhPrmController controller)
    {
    return AtPdhDe1PrmTestRunnerNew(self, controller);
    }

static eBool CanTestDe1Prm(AtModulePdhTestRunner self)
    {
    /* Concreate product know that */
    return cAtFalse;
    }

static void Delete(AtUnittestRunner self)
    {
    AtOsalMemFree(mThis(self)->allLines);
    m_AtUnittestRunnerMethods->Delete(self);
    }

static void MethodsInit(AtModulePdhTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TestModule);
        mMethodOverride(m_methods, CreateDe1TestRunner);
        mMethodOverride(m_methods, CreateDe3TestRunner);
        mMethodOverride(m_methods, CanUseTimeslot0InNxDs0);
        mMethodOverride(m_methods, CanUseTimeslot16InNxDs0);
        mMethodOverride(m_methods, HasSpecialSystemTiming);
        mMethodOverride(m_methods, ShouldTestDe1InLine);
        mMethodOverride(m_methods, ShouldTestDe3InLine);
        mMethodOverride(m_methods, ShouldTestDe3InStm);
        mMethodOverride(m_methods, ShouldTestMaximumNumberOfDe1Get);
        mMethodOverride(m_methods, NumDe3SerialLines);
        mMethodOverride(m_methods, WillTestSerialLine);
        mMethodOverride(m_methods, CreateDe1PrmTestRunner);
        mMethodOverride(m_methods, CanTestDe1Prm);
        mMethodOverride(m_methods, CreateDe1InterruptTestRunner);
        mMethodOverride(m_methods, CreateDe3InterruptTestRunner);
        mMethodOverride(m_methods, AllTestedLinesAllocate);
        }

    self->methods = &m_methods;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePdhTestRunner);
    }

AtModuleTestRunner AtModulePdhTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtModulePdhTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModulePdhTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModulePdhTestRunnerObjectInit(newRunner, module);
    }

eBool AtModulePdhTestRunnerCanUseTimeslot16InNxDs0(AtModulePdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->CanUseTimeslot16InNxDs0(self);
    return cAtFalse;
    }

eBool AtModulePdhTestRunnerCanUseTimeslot0InNxDs0(AtModulePdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->CanUseTimeslot0InNxDs0(self);
    return cAtFalse;
    }

eBool AtModulePdhTestRunnerHasSpecialSystemTiming(AtModulePdhTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->HasSpecialSystemTiming(self);
    return cAtFalse;
    }
    
uint32 AtTestMaskInNxDs0Make(uint8 startTimeslot, uint8 numTimeslots)
    {
    uint32 mask = 0;
    uint8 i, numRemainedSlots;

    numRemainedSlots = 32 - startTimeslot;
    if (numTimeslots > numRemainedSlots)
        numTimeslots = numRemainedSlots;

    if (numTimeslots == 0)
        return 0;

    for (i = 0; i < numTimeslots; i++)
        mask |= (cBit0 << (startTimeslot + i));

    return mask;
    }


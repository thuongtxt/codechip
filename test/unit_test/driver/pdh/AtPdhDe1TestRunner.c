/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtPdhDe1TestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : ETH Port test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModulePdh.h"
#include "AtPdhDe1.h"
#include "AtPdhChannel.h"
#include "AtPdhChannelTestRunnerInternal.h"
#include "AtModulePdhTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;
static tAtChannelTestRunnerMethods m_AtChannelTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtPdhDe1FrameType *SupportedFrameTypes(uint8 *numberOfTypes)
    {
    static eAtPdhDe1FrameType frameTypes[] = {cAtPdhDs1J1UnFrm,
                                              cAtPdhDs1FrmSf,
                                              cAtPdhDs1FrmEsf,
                                              cAtPdhDs1FrmDDS,
                                              cAtPdhDs1FrmSLC,
                                              cAtPdhJ1FrmSf,
                                              cAtPdhJ1FrmEsf,
                                              cAtPdhE1UnFrm,
                                              cAtPdhE1Frm,
                                              cAtPdhE1MFCrc};
    *numberOfTypes = sizeof(frameTypes) / sizeof(eAtPdhDe1FrameType);

    return frameTypes;
    }

eBool PdhDe1FrameTypeIsValid(AtPdhDe1 de1)
    {
    uint8 numFrameTypes, i;
    eAtPdhDe1FrameType *frameTypes = SupportedFrameTypes(&numFrameTypes);
    for (i = 0; i < numFrameTypes; i++)
        {
        if (frameTypes[i] == AtPdhChannelFrameTypeGet((AtPdhChannel)de1))
            return cAtTrue;
        }

    return cAtFalse;
    }

static AtPdhDe1 De1()
    {
    return (AtPdhDe1)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eAtPdhDe1FrameType DefaultFrameType()
    {
    return AtPdhChannelFrameTypeIsSupported((AtPdhChannel)De1(), cAtPdhE1Frm) ? cAtPdhE1Frm : cAtPdhDs1FrmEsf;
    }

static eAtPdhDe1FrameType DefaultUnFrameType()
    {
    return AtPdhChannelFrameTypeIsSupported((AtPdhChannel)De1(), cAtPdhE1UnFrm) ? cAtPdhE1UnFrm : cAtPdhDs1J1UnFrm;
    }

static void setUp()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultUnFrameType()));
    AtChannelInit((AtChannel)De1());
    }

static void testChangeFramingType()
    {
    uint8 i, numTypes;
    eAtPdhDe1FrameType *frameTypes = SupportedFrameTypes(&numTypes);

    for (i = 0; i < numTypes; i++)
        {
        AtPdhChannel de1 = (AtPdhChannel)De1();

        if (AtPdhChannelFrameTypeIsSupported(de1, frameTypes[i]))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), frameTypes[i]));
            TEST_ASSERT_EQUAL_INT(frameTypes[i], AtPdhChannelFrameTypeGet((AtPdhChannel)De1()));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), frameTypes[i]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testCannotChangeInvalidFramingType()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), 0xfff) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testConfigureSystemClockMode()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet((AtChannel)De1(), cAtTimingModeSys, NULL));
    TEST_ASSERT_EQUAL_INT(cAtTimingModeSys, AtChannelTimingModeGet((AtChannel)De1()));
    }

static void testConfigureLooptimeClockMode()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet((AtChannel)De1(), cAtTimingModeLoop, NULL));
    TEST_ASSERT_EQUAL_INT(cAtTimingModeLoop, AtChannelTimingModeGet((AtChannel)De1()));
    }

static void testConfigureExternalRefSource()
    {
    eAtRet ret = AtChannelTimingSet((AtChannel)De1(), cAtTimingModeExt1Ref, NULL);
    if (ret == cAtErrorModeNotSupport)
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, ret);
    TEST_ASSERT_EQUAL_INT(cAtTimingModeExt1Ref, AtChannelTimingModeGet((AtChannel)De1()));
    }

static void testConfigureSlaveTimingMode()
    {
    uint8 i;

    /* Get another DS1/E1 to refer clock source */
    AtModulePdh pdhModule = (AtModulePdh)AtChannelModuleGet((AtChannel)De1());
    AtPdhDe1 anotherDe1;

    if (AtModulePdhNumberOfDe1sGet(pdhModule) == 0)
        return;

    for (i = 0; i < AtModulePdhNumberOfDe1sGet(pdhModule); i++)
        {
        anotherDe1 = AtModulePdhDe1Get(pdhModule, i);
        if ((anotherDe1 != De1()) && (AtChannelTimingModeGet((AtChannel)anotherDe1) != cAtTimingModeSlave))
            break;
        }
    TEST_ASSERT(i < AtModulePdhNumberOfDe1sGet(pdhModule));

    /* Use this channel as clock source */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet((AtChannel)De1(), cAtTimingModeSlave, (AtChannel)De1()));
    TEST_ASSERT_EQUAL_INT(cAtTimingModeLoop, AtChannelTimingModeGet((AtChannel)De1()));
    TEST_ASSERT(AtChannelTimingSourceGet((AtChannel)De1()) == (AtChannel)De1());
    }

static void helper_Create3NxDs0()
    {
    /* Configure framing mode to have NxDS0 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));
    TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), cBit1 | cBit2));
    TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), cBit3 | cBit4));
    if (AtPdhDe1IsE1(De1()))
        {
        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), cBit5 | cBit31_6));
        }
    else
        {
        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), cBit5 | cBit23_6));
        }
    }

static void helper_DeleteAllNxDs0()
    {
    static AtPdhNxDS0  iterateNxds0, iterateNxds0List[32];
    AtIterator nxds0Iterator;
    uint32 i=0, nxds0Count = 0;

    /* Call iterator */
    nxds0Iterator = AtPdhDe1nxDs0IteratorCreate(De1());

    /* Compare nxds0 in stored array with nxds0 in iterator */
    while ((iterateNxds0 = (AtPdhNxDS0)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        iterateNxds0List[nxds0Count] = iterateNxds0;
        nxds0Count++;
        }

    AtObjectDelete((AtObject)nxds0Iterator);

    for (i = 0; i < nxds0Count; i++)
        AtPdhDe1NxDs0Delete(De1(), iterateNxds0List[i]);
    }


static void testCanCreateAnyNxDS0()
    {
    helper_Create3NxDs0();
    }

static void testCanAccessAnyNxDS0()
    {
    helper_DeleteAllNxDs0();
    helper_Create3NxDs0();
    TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Get(De1(), cBit1 | cBit2));
    TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Get(De1(), cBit3 | cBit4));
    if (AtPdhDe1IsE1(De1()))
        {
        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Get(De1(), cBit5 | cBit31_6));
        }
    else
        {
        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Get(De1(), cBit5 | cBit23_6));
        }

    /* Change to unframe mode to free all DS0 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultUnFrameType()));
    }

static void E1CreateThenDeleteNxDs0WithOneDs0(uint8 numNxds0)
    {
    uint8 numNxds0_i;
    static uint8 ds0NumMax = 32;
    uint8 count = 0;

    /* Configure framing mode to have NxDS0 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), cAtPdhE1Frm));

    for (numNxds0_i = 0; numNxds0_i < ds0NumMax; numNxds0_i++)
        {
        /* Ignore timslot #0 and #16 (in case signaling enable)*/
        if ((numNxds0_i == 0) || (numNxds0_i == 16 && AtPdhDe1SignalingIsEnabled(De1())))
            continue;

        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), (cBit0 << numNxds0_i)));
        count++;
        if (count == numNxds0)
            break;
        }

    count = 0;
    for (numNxds0_i = 0; numNxds0_i < ds0NumMax; numNxds0_i++)
        {
        /* Ignore timslot #0 and #16 (in case signaling enable)*/
        if ((numNxds0_i == 0) || (numNxds0_i == 16 && AtPdhDe1SignalingIsEnabled(De1())))
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1NxDs0Delete(De1(), AtPdhDe1NxDs0Get(De1(), (cBit0 << numNxds0_i))));
        TEST_ASSERT_NULL(AtPdhDe1NxDs0Get(De1(), (cBit0 << numNxds0_i)));
        count++;
        if (count == numNxds0)
            break;
        }
    }

static void Ds1CreateThenDeleteNxDs0WithOneDs0(uint8 numNxds0)
    {
    uint8 numNxds0_i;
    static uint8 ds0NumMax = 24;
    uint8 count = 0;

    /* Configure framing mode to have NxDS0 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), cAtPdhDs1FrmEsf));

    for (numNxds0_i = 0; numNxds0_i < ds0NumMax; numNxds0_i++)
        {
        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), (cBit0 << numNxds0_i)));
        count++;
        if (count == numNxds0)
            break;
        }

    count = 0;
    for (numNxds0_i = 0; numNxds0_i < ds0NumMax; numNxds0_i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1NxDs0Delete(De1(), AtPdhDe1NxDs0Get(De1(), (cBit0 << numNxds0_i))));
        TEST_ASSERT_NULL(AtPdhDe1NxDs0Get(De1(), (cBit0 << numNxds0_i)));
        count++;
        if (count == numNxds0)
            break;
        }
    }

static void CreateThenDeleteNxDs0WithOneDs0(uint8 numNxds0)
    {

    if (AtPdhChannelFrameTypeIsSupported((AtPdhChannel)De1(), cAtPdhE1Frm) == cAtFalse)
        Ds1CreateThenDeleteNxDs0WithOneDs0(numNxds0);
    else
        E1CreateThenDeleteNxDs0WithOneDs0(numNxds0);
    }

static void testCannotCreateNxDS0WithFirstSlotForE1Frame()
    {

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));

    if (!AtPdhDe1IsE1(De1()))
        {
        TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), cBit15_0));
        }
    else
        {
        TEST_ASSERT_NULL(AtPdhDe1NxDs0Create(De1(), cBit0));
        TEST_ASSERT_NULL(AtPdhDe1NxDs0Create(De1(), cBit15_0));
        }
    }

static void testCanDeleteAnyNxDS0()
    {
    /* Create 1 nxds0 and delete it, after deleting, this nxds0 must be NULL */
    CreateThenDeleteNxDs0WithOneDs0(1);

    /* Check with 31 nxds0 case */
    if (AtPdhDe1IsE1(De1()))
        CreateThenDeleteNxDs0WithOneDs0(31);
    else
        CreateThenDeleteNxDs0WithOneDs0(24);


    /* Check with a certain case */
    helper_Create3NxDs0();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1NxDs0Delete(De1(), AtPdhDe1NxDs0Get(De1(), cBit1 | cBit2)));
    TEST_ASSERT_NULL(AtPdhDe1NxDs0Get(De1(), cBit1 | cBit2));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1NxDs0Delete(De1(), AtPdhDe1NxDs0Get(De1(), cBit3 | cBit4)));
    TEST_ASSERT_NULL(AtPdhDe1NxDs0Get(De1(), cBit3 | cBit4));
    if (AtPdhDe1IsE1(De1()))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1NxDs0Delete(De1(), AtPdhDe1NxDs0Get(De1(), cBit5 | cBit31_6)));
        TEST_ASSERT_NULL(AtPdhDe1NxDs0Get(De1(), cBit5 | cBit31_6));
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1NxDs0Delete(De1(), AtPdhDe1NxDs0Get(De1(), cBit5 | cBit23_6)));
        TEST_ASSERT_NULL(AtPdhDe1NxDs0Get(De1(), cBit5 | cBit23_6));
        }
    }

static void testNxDS0Iterator()
    {
    static AtPdhNxDS0  createdNxds0[32], iterateNxds0;
    AtIterator nxds0Iterator;
    uint32 i = 0;

    /* Create a 3 nxds0 groups */
    helper_Create3NxDs0();

    AtOsalMemInit(createdNxds0, 0, sizeof(createdNxds0));
    createdNxds0[0] = AtPdhDe1NxDs0Get(De1(), cBit1 | cBit2);
    createdNxds0[1] = AtPdhDe1NxDs0Get(De1(), cBit3 | cBit4);
    if (AtPdhDe1IsE1(De1()))
        {
        createdNxds0[2] = AtPdhDe1NxDs0Get(De1(), cBit5 | cBit31_6);
        }
    else
        {
        createdNxds0[2] = AtPdhDe1NxDs0Get(De1(), cBit5 | cBit23_6);
        }

    /* Call iterator */
    nxds0Iterator = AtPdhDe1nxDs0IteratorCreate(De1());
    TEST_ASSERT_NOT_NULL(nxds0Iterator);

    /* Count number of nxds0 in Iterator must be 3 nxds0 group */
    TEST_ASSERT_EQUAL_INT(3, AtIteratorCount(nxds0Iterator));

    /* Compare nxds0 in stored array with nxds0 in iterator */
    while ((iterateNxds0 = (AtPdhNxDS0)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        TEST_ASSERT(createdNxds0[i] == iterateNxds0);
        i = i + 1;
        }

    AtObjectDelete((AtObject)nxds0Iterator);
    }

static void testCannotCreateNxDs0WithNoBitMap()
    {
    /* Configure framing mode to have NxDS0 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));

    /* Return NULL if bitmap is zero */
    TEST_ASSERT_NULL(AtPdhDe1NxDs0Create(De1(), 0));
    }

static void testCannotEnabledSignalingIfNotConfigureDs0Mask()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));
    TEST_ASSERT(AtPdhDe1SignalingDs0MaskSet(De1(), 0) == cAtOk);
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPdhDe1SignalingEnable(De1(), cAtTrue) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    AtPdhDe1SignalingDs0MaskSet(De1(), cBit15_1);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingEnable(De1(), cAtTrue));
    }

static void testChangeSignalingDs0Mask()
    {
    /* Configure framing mode to have NxDS0 and disable signaling for this E1 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingDs0MaskSet(De1(), cBit15_1));
    TEST_ASSERT_EQUAL_INT(cBit15_1, AtPdhDe1SignalingDs0MaskGet(De1()));
    if (AtPdhDe1IsE1(De1()))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingDs0MaskSet(De1(), cBit31_17));
        TEST_ASSERT_EQUAL_INT(cBit31_17, AtPdhDe1SignalingDs0MaskGet(De1()));
        }
    else
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingDs0MaskSet(De1(), cBit23_16));
        TEST_ASSERT_EQUAL_INT(cBit23_16, AtPdhDe1SignalingDs0MaskGet(De1()));
        }

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingEnable(De1(), cAtTrue));
    TEST_ASSERT(AtPdhDe1SignalingIsEnabled(De1()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingEnable(De1(), cAtFalse));
    TEST_ASSERT(AtPdhDe1SignalingIsEnabled(De1()) == cAtFalse);
    }

static void testCannotCreateTwoNxDS0ThatHaveSomeSameBitMasks()
    {
    /* Configure framing mode to have NxDS0 */
    helper_DeleteAllNxDs0();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));
    TEST_ASSERT_NOT_NULL(AtPdhDe1NxDs0Create(De1(), cBit1 | cBit2));
    TEST_ASSERT_NULL(AtPdhDe1NxDs0Create(De1(), cBit1 | cBit3));

    /* Change to unframe mode to free all DS0 */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultUnFrameType()));
    }

static void testCanAccessAlarmsAndAlarmHistory()
    {
    AtChannelAlarmGet((AtChannel)De1());
    AtChannelAlarmInterruptGet((AtChannel)De1());
    AtChannelAlarmInterruptClear((AtChannel)De1());
    }

static uint8 *AllCounterTypes(uint8 *numCounterTypes)
    {
    static uint8 counterTypes[] = {cAtPdhDe1CounterBpvExz,
                                   cAtPdhDe1CounterCrc,
                                   cAtPdhDe1CounterFe,
                                   cAtPdhDe1CounterRei};

    *numCounterTypes = sizeof(counterTypes) / sizeof(uint8);

    return counterTypes;
    }

static void testCanAccessCounters()
    {
    uint8 i, numCounterTypes, *counterTypes;

    counterTypes = AllCounterTypes(&numCounterTypes);
    for (i = 0; i < numCounterTypes; i++)
        {
        AtChannelCounterGet((AtChannel)De1(), counterTypes[i]);
        AtChannelCounterClear((AtChannel)De1(), counterTypes[i]);
        }
    }

/* This function is to test Enable/Disable signaling */
static void testEnableDisableSignalling()
    {
    AtPdhDe1 de1 = De1();
    uint32 sigBitmap;

    /* Set frame mode to test signaling */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), DefaultFrameType()));

    if (AtPdhDe1IsE1(De1()))
        sigBitmap = 0xFFFFFFFF & ~(cBit0 | cBit16); /* Signaling timeslots 1-15; 17-32 */
    else
        sigBitmap = 0xFFFFFF;

    /* Set signaling bitmap and enable */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingDs0MaskSet(de1, sigBitmap));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingEnable(de1, cAtTrue));

    /* Check setting */
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPdhDe1SignalingIsEnabled(de1));
    TEST_ASSERT_EQUAL_INT(sigBitmap, AtPdhDe1SignalingDs0MaskGet(de1));

    /* Disable signaling */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhDe1SignalingEnable(de1, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPdhDe1SignalingIsEnabled(de1));
    }

static void testCannotEnableSignalingIfFrameModeIsUnframe()
    {
    AtPdhDe1 de1 = De1();

    AtTestLoggerEnable(cAtFalse);
    if (AtPdhChannelFrameTypeIsSupported((AtPdhChannel)De1(), cAtPdhDs1J1UnFrm))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), cAtPdhDs1J1UnFrm));
        TEST_ASSERT(AtPdhDe1SignalingEnable(de1, cAtTrue) != cAtOk);
        }

    if (AtPdhChannelFrameTypeIsSupported((AtPdhChannel)De1(), cAtPdhE1UnFrm))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)De1(), cAtPdhE1UnFrm));
        TEST_ASSERT(AtPdhDe1SignalingEnable(de1, cAtTrue) != cAtOk);
        }

    AtTestLoggerEnable(cAtTrue);
    }

static void testLoopback()
    {
    AtChannel de1 = (AtChannel)De1();
    uint8 i;
    static eAtPdhLoopbackMode allLoopbackModes[] = {cAtPdhLoopbackModeLocalPayload,
                                                    cAtPdhLoopbackModeRemotePayload,
                                                    cAtPdhLoopbackModeLocalLine,
                                                    cAtPdhLoopbackModeRemoteLine,
                                                    cAtPdhLoopbackModeRelease};

    /* Release mode must be supported as default */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet(de1, cAtPdhLoopbackModeRelease));
    TEST_ASSERT_EQUAL_INT(cAtPdhLoopbackModeRelease, AtChannelLoopbackGet(de1));

    /* Try other modes */
    for (i = 0; i < mCount(allLoopbackModes); i++)
        {
        if (!AtChannelLoopbackIsSupported(de1, allLoopbackModes[i]))
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet(de1, allLoopbackModes[i]));
        TEST_ASSERT_EQUAL_INT(allLoopbackModes[i], AtChannelLoopbackGet(de1));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_PdhDe1_Fixtures)
        {
        new_TestFixture("testChangeFramingType", testChangeFramingType),
        new_TestFixture("testCanCreateAnyNxDS0", testCanCreateAnyNxDS0),
        new_TestFixture("testCanAccessAnyNxDS0", testCanAccessAnyNxDS0),
        new_TestFixture("testCanDeleteAnyNxDS0", testCanDeleteAnyNxDS0),
        new_TestFixture("testNxDS0Iterator", testNxDS0Iterator),
        new_TestFixture("testConfigureSystemClockMode", testConfigureSystemClockMode),
        new_TestFixture("testConfigureLooptimeClockMode", testConfigureLooptimeClockMode),
        new_TestFixture("testConfigureExternalRefSource", testConfigureExternalRefSource),
        new_TestFixture("testCannotCreateTwoNxDS0ThatHaveSomeSameBitMasks", testCannotCreateTwoNxDS0ThatHaveSomeSameBitMasks),
        new_TestFixture("testCanAccessAlarmsAndAlarmHistory", testCanAccessAlarmsAndAlarmHistory),
        new_TestFixture("testCanAccessCounters", testCanAccessCounters),
        new_TestFixture("testCannotChangeInvalidFramingType", testCannotChangeInvalidFramingType),
        new_TestFixture("testEnableDisableSignalling", testEnableDisableSignalling),
        new_TestFixture("testChangeSignalingDs0Mask", testChangeSignalingDs0Mask),
        new_TestFixture("testCannotSetSignalingDs0MaskIfSignalingIsNotEnabled", testCannotEnabledSignalingIfNotConfigureDs0Mask),
        new_TestFixture("testCannotCreateNxDs0WithNoBitMap", testCannotCreateNxDs0WithNoBitMap),
        new_TestFixture("testCannotCreateNxDS0WithFirstSlotForE1Frame", testCannotCreateNxDS0WithFirstSlotForE1Frame),
        new_TestFixture("testCannotEnableSignalingIfFrameModeIsUnframe", testCannotEnableSignalingIfFrameModeIsUnframe),
        new_TestFixture("testConfigureSlaveTimingMode", testConfigureSlaveTimingMode),
        new_TestFixture("testLoopback", testLoopback)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_PdhDe1_Caller, "TestSuite_PdhDe1", setUp, NULL, TestSuite_PdhDe1_Fixtures);

    return (TestRef)((void *)&TestSuite_PdhDe1_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtSurEngineTestRunner CreateSurEngineTestRunner(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule)
    {
    return AtModuleSurTestRunnerCreatePdhDe1SurEngineTestRunner(factoryModule, AtChannelSurEngineGet((AtChannel)De1()));
    }

static void OverrideAtChannelTestRunner(AtChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelTestRunnerOverride, (void *)self->methods, sizeof(m_AtChannelTestRunnerOverride));

        mMethodOverride(m_AtChannelTestRunnerOverride, CreateSurEngineTestRunner);
        }

    mMethodsSet(self, &m_AtChannelTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtChannelTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe1TestRunner);
    }

AtChannelTestRunner AtPdhDe1TestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPdhDe1TestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPdhDe1TestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

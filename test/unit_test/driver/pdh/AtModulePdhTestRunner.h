/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtModulePdhTestRunner.h
 * 
 * Created Date: Apr 8, 2015
 *
 * Description : PDH module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPDHTESTRUNNER_H_
#define _ATMODULEPDHTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePdhTestRunner * AtModulePdhTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AtModulePdhTestRunnerCanUseTimeslot16InNxDs0(AtModulePdhTestRunner self);
eBool AtModulePdhTestRunnerCanUseTimeslot0InNxDs0(AtModulePdhTestRunner self);
eBool AtModulePdhTestRunnerHasSpecialSystemTiming(AtModulePdhTestRunner self);

/* Util */
uint32 AtTestMaskInNxDs0Make(uint8 startTimeslot, uint8 numTimeslots);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPDHTESTRUNNER_H_ */


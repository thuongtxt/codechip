/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhDe1PrmTestRunner.c
 *
 * Created Date: Jan 28, 2016
 *
 * Description : PRM Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdhTestRunnerInternal.h"
#include "AtPdhDe1PrmTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe1PrmTestRunner Runner(void)
    {
    return (AtPdhDe1PrmTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPdhPrmController Controller(void)
    {
    return (AtPdhPrmController)AtPdhDe1PrmTestRunnerControllerGet(Runner());
    }

static void testCanChangeStandardMode(void)
    {
    TEST_ASSERT(AtPdhPrmControllerStandardSet(Controller(), cAtPdhDe1PrmStandardAtt) == cAtOk);
    TEST_ASSERT(AtPdhPrmControllerStandardGet(Controller()) == cAtPdhDe1PrmStandardAtt);
    TEST_ASSERT(AtPdhPrmControllerStandardSet(Controller(), cAtPdhDe1PrmStandardAnsi) == cAtOk);
    TEST_ASSERT(AtPdhPrmControllerStandardGet(Controller()) == cAtPdhDe1PrmStandardAnsi);

    /* Invalid cases */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPdhPrmControllerStandardSet(NULL, cAtPdhDe1PrmStandardAtt) != cAtOk);
    TEST_ASSERT(AtPdhPrmControllerStandardGet(NULL) == cAtPdhDe1PrmStandardUnknown);
    TEST_ASSERT(AtPdhPrmControllerStandardSet(Controller(), cAtPdhDe1PrmStandardUnknown) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanEnableOrDisable(void)
    {
    TEST_ASSERT(AtPdhPrmControllerEnable(Controller(), cAtTrue) == cAtOk);
    TEST_ASSERT(AtPdhPrmControllerIsEnabled(Controller()));
    TEST_ASSERT(AtPdhPrmControllerEnable(Controller(), cAtFalse) == cAtOk);
    TEST_ASSERT(AtPdhPrmControllerIsEnabled(Controller()) == cAtFalse);

    /* Invalid case */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPdhPrmControllerEnable(NULL, cAtTrue) != cAtOk);
    TEST_ASSERT(AtPdhPrmControllerEnable(NULL, cAtFalse) != cAtOk);
    TEST_ASSERT(AtPdhPrmControllerIsEnabled(NULL) == cAtFalse);
    AtTestLoggerEnable(cAtTrue);
    }

static void TestChangeOverheadBit(eAtModulePdhRet (*CRBitSet)(AtPdhPrmController self, uint8 value),
                                     uint8 (*CRBitGet)(AtPdhPrmController self))
    {
    TEST_ASSERT(CRBitSet(Controller(), 0) == cAtOk);
    TEST_ASSERT(CRBitGet(Controller()) == 0);
    TEST_ASSERT(CRBitSet(Controller(), 1) == cAtOk);
    TEST_ASSERT(CRBitGet(Controller()) == 1);

    /* Invalid cases */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(CRBitSet(NULL, 0) != cAtOk);
    TEST_ASSERT(CRBitGet(NULL) == 0);
    TEST_ASSERT(CRBitSet(Controller(), 0xF) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanChangeTxCrBit(void)
    {
    TestChangeOverheadBit(AtPdhPrmControllerTxCRBitSet, AtPdhPrmControllerTxCRBitGet);
    }

static void testCanChangeExpectedCrBit(void)
    {
    TestChangeOverheadBit(AtPdhPrmControllerExpectedCRBitSet, AtPdhPrmControllerExpectedCRBitGet);
    }

static void testCanChangeLBBit(void)
    {
    TestChangeOverheadBit(AtPdhPrmControllerTxLBBitSet, AtPdhPrmControllerTxLBBitGet);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPdhPrmController_Fixtures)
        {
        new_TestFixture("testCanChangeStandardMode", testCanChangeStandardMode),
        new_TestFixture("testCanEnableOrDisable", testCanEnableOrDisable),
        new_TestFixture("testCanChangeTxCrBit", testCanChangeTxCrBit),
        new_TestFixture("testCanChangeExpectedCrBit", testCanChangeExpectedCrBit),
        new_TestFixture("testCanChangeLBBit", testCanChangeLBBit)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPdhPrmController_Caller, "TestSuite_AtPdhPrmController", NULL, NULL, TestSuite_AtPdhPrmController_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPdhPrmController_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    return (char *)AtObjectToString((AtObject)AtPdhDe1PrmTestRunnerControllerGet((AtPdhDe1PrmTestRunner)self));
    }

static void OverrideAtUnittestRunner(AtPdhDe1PrmTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtPdhDe1PrmTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe1PrmTestRunner);
    }

AtPdhDe1PrmTestRunner AtPdhDe1PrmTestRunnerObjectInit(AtPdhDe1PrmTestRunner self,
                                                      AtModulePdhTestRunner moduleRunner,
                                                      AtPdhPrmController controller)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit((AtUnittestRunner)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->module = moduleRunner;
    self->controller = controller;

    return self;
    }

AtPdhDe1PrmTestRunner AtPdhDe1PrmTestRunnerNew(AtModulePdhTestRunner moduleRunner, AtPdhPrmController controller)
    {
    AtPdhDe1PrmTestRunner runner = AtOsalMemAlloc(ObjectSize());
    return AtPdhDe1PrmTestRunnerObjectInit(runner, moduleRunner, controller);
    }

AtModulePdhTestRunner AtPdhDe1PrmTestRunnerModuleGet(AtPdhDe1PrmTestRunner self)
    {
    return (self) ? self->module : NULL;
    }

AtPdhPrmController AtPdhDe1PrmTestRunnerControllerGet(AtPdhDe1PrmTestRunner self)
    {
    return (self) ? self->controller : NULL;
    }

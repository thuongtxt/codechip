/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtModuleTestRunner.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AtModuleTestRUNNER_H_
#define _AtModuleTestRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "../AtTestRunnerClasses.h"
#include "../runner/AtUnittestRunner.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete runners */
AtModuleTestRunner AtModuleTestRunnerNew(AtModule module); /* The most common one */
AtModuleTestRunner AtModulePppTestRunnerNew(AtModule module);
AtModuleTestRunner AtModuleSdhTestRunnerNew(AtModule module);
AtModuleTestRunner AtModulePwTestRunnerNew(AtModule module);
AtModuleTestRunner AtModuleEthTestRunnerNew(AtModule module);
AtModuleTestRunner AtModuleApsTestRunnerNew(AtModule module);
AtModuleTestRunner AtModulePrbsTestRunnerNew(AtModule module);
AtModuleTestRunner AtModuleRamTestRunnerNew(AtModule module);
AtModuleTestRunner AtModuleSurTestRunnerNew(AtModule module);
AtModuleTestRunner AtModulePtpTestRunnerNew(AtModule module);

/* PDH test runners */
AtModuleTestRunner AtModulePdhTestRunnerNew(AtModule module);

/* ENCAP test runners */
AtModuleTestRunner AtStmModuleEncapTestRunnerNew(AtModule module);
AtModuleTestRunner AtPdhModuleEncapTestRunnerNew(AtModule module);

/* APIs */
AtModule AtModuleTestRunnerModuleGet(AtModuleTestRunner self);
AtDevice AtModuleTestRunnerDeviceGet(AtModuleTestRunner self);

void AtModuleTestRunnerDeviceRunnerSet(AtModuleTestRunner self, AtDeviceTestRunner deviceRunner);
AtDeviceTestRunner AtModuleTestRunnerDeviceRunnerGet(AtModuleTestRunner self);

#endif /* _AtModuleTestRUNNER_H_ */


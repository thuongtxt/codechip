/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtModuleTestRunnerFactoryInternal.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AtModuleTestRUNNERFACTORYINTERNAL_H_
#define _AtModuleTestRUNNERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* For common object macros */
#include "AtModuleTestRunnerFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleTestRunnerFactoryMethods
    {
    AtModuleTestRunner (*ModuleTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);

    AtModuleTestRunner (*PppTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*SdhTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*PdhTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*EncapTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*PwTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*EthTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*XcTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*ApsTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*PrbsTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*RamTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*SurTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    AtModuleTestRunner (*PtpTestRunnerCreate)(AtModuleTestRunnerFactory self, AtModule module);
    }tAtModuleTestRunnerFactoryMethods;

typedef struct tAtModuleTestRunnerFactory
    {
    const tAtModuleTestRunnerFactoryMethods *methods;
    }tAtModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunnerFactory AtModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self);

#endif /* _AtModuleTestRUNNERFACTORYINTERNAL_H_ */


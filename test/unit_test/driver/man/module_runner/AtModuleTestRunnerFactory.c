/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtModuleTestRunnerFactory.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleTestRunnerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleTestRunnerFactoryMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner PppTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModulePppTestRunnerNew(module);
    }

static AtModuleTestRunner SdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModuleSdhTestRunnerNew(module);
    }

static AtModuleTestRunner PwTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModulePwTestRunnerNew(module);
    }

static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModulePdhTestRunnerNew(module);
    }

static AtModuleTestRunner EncapTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    AtDevice device = AtModuleDeviceGet(module);
    if (AtDeviceModuleGet(device, cAtModuleSdh))
        return AtStmModuleEncapTestRunnerNew(module);
    return AtPdhModuleEncapTestRunnerNew(module);
    }

static AtModuleTestRunner EthTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModuleEthTestRunnerNew(module);
    }

static AtModuleTestRunner ApsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModuleApsTestRunnerNew(module);
    }

static AtModuleTestRunner RamTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModuleRamTestRunnerNew(module);
    }

static AtModuleTestRunner ModuleTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    if (module == NULL)
        return NULL;

    switch (AtModuleTypeGet(module))
        {
        case cAtModulePpp:   return mMethodsGet(self)->PppTestRunnerCreate(self, module);
        case cAtModuleSdh:   return mMethodsGet(self)->SdhTestRunnerCreate(self, module);
        case cAtModulePdh:   return mMethodsGet(self)->PdhTestRunnerCreate(self, module);
        case cAtModuleEncap: return mMethodsGet(self)->EncapTestRunnerCreate(self, module);
        case cAtModulePw:    return mMethodsGet(self)->PwTestRunnerCreate(self, module);
        case cAtModuleEth:   return mMethodsGet(self)->EthTestRunnerCreate(self, module);
        case cAtModuleXc:    return mMethodsGet(self)->XcTestRunnerCreate(self, module);
        case cAtModuleAps:   return mMethodsGet(self)->ApsTestRunnerCreate(self, module);
      	case cAtModulePrbs:  return mMethodsGet(self)->PrbsTestRunnerCreate(self, module);
        case cAtModuleRam:   return mMethodsGet(self)->RamTestRunnerCreate(self, module);
        case cAtModuleSur:   return mMethodsGet(self)->SurTestRunnerCreate(self, module);
        case cAtModulePtp:   return mMethodsGet(self)->PtpTestRunnerCreate(self, module);
        default:
            return NULL;
        }
    }

static AtModuleTestRunner XcTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return NULL;
    }

static AtModuleTestRunner PrbsTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModulePrbsTestRunnerNew(module);
    }

static AtModuleTestRunner SurTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModuleSurTestRunnerNew(module);
    }

static AtModuleTestRunner PtpTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModulePtpTestRunnerNew(module);
    }

static void MethodsInit(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ModuleTestRunnerCreate);
        mMethodOverride(m_methods, PppTestRunnerCreate);
        mMethodOverride(m_methods, SdhTestRunnerCreate);
        mMethodOverride(m_methods, PdhTestRunnerCreate);
        mMethodOverride(m_methods, EncapTestRunnerCreate);
        mMethodOverride(m_methods, PwTestRunnerCreate);
        mMethodOverride(m_methods, EthTestRunnerCreate);
        mMethodOverride(m_methods, XcTestRunnerCreate);
        mMethodOverride(m_methods, ApsTestRunnerCreate);
		mMethodOverride(m_methods, PrbsTestRunnerCreate);
        mMethodOverride(m_methods, RamTestRunnerCreate);
        mMethodOverride(m_methods, SurTestRunnerCreate);
        mMethodOverride(m_methods, PtpTestRunnerCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleTestRunnerFactory);
    }

AtModuleTestRunnerFactory AtModuleTestRunnerFactoryObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory AtModuleTestRunnerFactoryNew()
    {
    AtModuleTestRunnerFactory newFactory = AtOsalMemAlloc(ObjectSize());
    return AtModuleTestRunnerFactoryObjectInit(newFactory);
    }

AtModuleTestRunner AtModuleTestRunnerFactoryModuleTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    if (self)
        return self->methods->ModuleTestRunnerCreate(self, module);
    return NULL;
    }

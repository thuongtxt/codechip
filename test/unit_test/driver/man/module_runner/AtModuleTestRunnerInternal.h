/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtModuleTestRunnerInternal.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULETESTRUNNERINTERNAL_H_
#define _ATMODULETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../AtObjectTestRunnerInternal.h"
#include "AtModuleTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleTestRunner
    {
    tAtObjectTestRunner super;
 
    /* Private data */
    AtDeviceTestRunner deviceRunner;
    }tAtModuleTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#endif /* _ATMODULETESTRUNNERINTERNAL_H_ */


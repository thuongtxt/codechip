/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtModuleTestRunnerFactory.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AtModuleTestRUNNERFACTORY_H_
#define _AtModuleTestRUNNERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleTestRunner.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleTestRunnerFactory * AtModuleTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete factories */
AtModuleTestRunnerFactory AtModuleTestRunnerFactoryNew();
AtModuleTestRunnerFactory AtModuleTestRunnerFactoryPdhProductSharedFactory();
AtModuleTestRunnerFactory AtModuleTestRunnerFactoryStmProductSharedFactory();

/* APIs */
AtModuleTestRunner AtModuleTestRunnerFactoryModuleTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module);

#endif /* _AtModuleTestRUNNERFACTORY_H_ */


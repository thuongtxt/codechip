/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtModuleTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModuleTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtModuleTestRunner)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule Module()
    {
    return AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testInitializeModule()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInit(Module()));
    }

static void testModuleMustBeValid()
    {
    TEST_ASSERT_NOT_NULL(Module());
    }

static void Run(AtUnittestRunner self)
    {
    AtModuleTestRunner runner = (AtModuleTestRunner)self;
    AtModuleInit(AtModuleTestRunnerModuleGet(runner));
    m_AtUnittestRunnerMethods->Run(self);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModulePdh_Fixtures)
        {
        new_TestFixture("testInitializeModule", testInitializeModule),
        new_TestFixture("testModuleMustBeValid", testModuleMustBeValid)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModulePdh_Caller, "TestSuite_Module", NULL, NULL, TestSuite_ModulePdh_Fixtures);

    return (TestRef)((void *)&TestSuite_ModulePdh_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtModule module = AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtSnprintf(buffer, bufferSize, "Module %s", AtModuleTypeString(module));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleTestRunner);
    }

AtModuleTestRunner AtModuleTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModuleTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModuleTestRunnerObjectInit(newRunner, module);
    }

AtModule AtModuleTestRunnerModuleGet(AtModuleTestRunner self)
    {
    return (AtModule)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

AtDevice AtModuleTestRunnerDeviceGet(AtModuleTestRunner self)
    {
    return AtModuleDeviceGet(AtModuleTestRunnerModuleGet(self));
    }

void AtModuleTestRunnerDeviceRunnerSet(AtModuleTestRunner self, AtDeviceTestRunner deviceRunner)
    {
    if (self)
        self->deviceRunner = deviceRunner;
    }

AtDeviceTestRunner AtModuleTestRunnerDeviceRunnerGet(AtModuleTestRunner self)
    {
    return self ? self->deviceRunner : NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtModuleTestRunnerFactoryPdhProduct.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleTestRunnerFactoryInternal.h"
#include "AtModuleTestRunner.h"
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtModuleTestRunnerFactoryPdhProduct
    {
    tAtModuleTestRunnerFactory super;
    }tAtModuleTestRunnerFactoryPdhProduct;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleTestRunnerFactoryMethods m_AtModuleTestRunnerFactoryOverride;

/* Save super implementation */
static const tAtModuleTestRunnerFactoryMethods *m_AtModuleTestRunnerFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunner PdhTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtModulePdhTestRunnerNew(module);
    }

static AtModuleTestRunner EncapTestRunnerCreate(AtModuleTestRunnerFactory self, AtModule module)
    {
    return AtPdhModuleEncapTestRunnerNew(module);
    }

static void OverrideAtModuleTestRunnerFactory(AtModuleTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        m_AtModuleTestRunnerFactoryMethods = self->methods;
        AtOsalMemCpy(&m_AtModuleTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtModuleTestRunnerFactoryOverride));

        m_AtModuleTestRunnerFactoryOverride.PdhTestRunnerCreate   = PdhTestRunnerCreate;
        m_AtModuleTestRunnerFactoryOverride.EncapTestRunnerCreate = EncapTestRunnerCreate;
        }

    self->methods = &m_AtModuleTestRunnerFactoryOverride;
    }

static void Override(AtModuleTestRunnerFactory self)
    {
    OverrideAtModuleTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleTestRunnerFactoryPdhProduct);
    }

AtModuleTestRunnerFactory AtModuleTestRunnerFactoryPdhProductObjectInit(AtModuleTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunnerFactory AtModuleTestRunnerFactoryPdhProductSharedFactory()
    {
    static tAtModuleTestRunnerFactoryPdhProduct sharedFactory;
    return AtModuleTestRunnerFactoryPdhProductObjectInit((AtModuleTestRunnerFactory)&sharedFactory);
    }

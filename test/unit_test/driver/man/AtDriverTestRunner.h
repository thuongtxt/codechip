/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtDriverTestRunner.h
 * 
 * Created Date: Jul 1, 2014
 *
 * Description : Driver test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDRIVERTESTRUNNER_H_
#define _ATDRIVERTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObjectTestRunner.h" /* Super class */
#include "AtDriver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDriverTestRunner * AtDriverTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDriverTestRunner AtDriverTestRunnerNew();

#endif /* _ATDRIVERTESTRUNNER_H_ */


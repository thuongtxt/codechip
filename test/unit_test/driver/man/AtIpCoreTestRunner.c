/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtIpCoreTestRunner.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : Interrupt testing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtIpCore.h"
#include "AtObjectTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtIpCoreTestRunner *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtIpCoreTestRunner
    {
    tAtObjectTestRunner super;
    }tAtIpCoreTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device()
    {
    return (AtDevice)AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setup()
    {
    TEST_ASSERT_NOT_NULL(Device());
    }

static void tearDown()
    {
    }

static void testGetIpCoreId()
    {
    uint8 i;

    for (i = 0; i < AtDeviceNumIpCoresGet(Device()); i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(Device(), i);
        TEST_ASSERT_EQUAL_INT(i, AtIpCoreIdGet(core));
        }
    }

static void testAccessHal()
    {
    AtHal hal;

    /* Cannot change HAL for Core if it is already setup */
    AtIpCore core = AtDeviceIpCoreGet(Device(), 0);
    if (AtIpCoreHalGet(core) != NULL)
        return;

    hal = AtHalNew(0x0);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtIpCoreHalSet(core, hal));
    TEST_ASSERT(AtIpCoreHalGet(core) == hal);
    }

static void testCannotChangeInstalledHal()
    {
    AtHal hal1, hal2;

    /* If HAL is already setup */
    AtIpCore core = AtDeviceIpCoreGet(Device(), 0);
    if (AtIpCoreHalGet(core) != NULL)
        {
        hal1 = AtHalNew(0x0);
        TEST_ASSERT(AtIpCoreHalSet(core, hal1) != cAtOk);
        AtHalDelete(hal1);

        return;
        }

    /* If HAL has not been setup */
    /* Setup HAL */
    hal1 = AtHalNew(0x0);
    hal2 = AtHalNew(0x1);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtIpCoreHalSet(core, hal1));

    /* Try to change it, off course fail */
    TEST_ASSERT(AtIpCoreHalSet(core, hal2) != cAtOk);
    TEST_ASSERT(AtIpCoreHalSet(core, NULL) != cAtOk);

    /* Release memory when done */
    AtHalDelete(hal2);
    }

static void testGetDevice()
    {
    uint8 i;

    for (i = 0; i < AtDeviceNumIpCoresGet(Device()); i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(Device(), i);
        TEST_ASSERT_NOT_NULL(AtIpCoreDeviceGet(core));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ATIpCore_Fixtures)
        {
        new_TestFixture("testGetIpCoreId", testGetIpCoreId),
        new_TestFixture("testGetDevice", testGetDevice),
        new_TestFixture("testAccessHal", testAccessHal),
        new_TestFixture("testCannotChangeInstalledHal", testCannotChangeInstalledHal),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ATIpCore_Caller, "TestSuite_ATIpCore", setup, tearDown, TestSuite_ATIpCore_Fixtures);

    return (TestRef)((void *)&TestSuite_ATIpCore_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "AtIpCore");
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(self), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtIpCoreTestRunner);
    }

AtUnittestRunner AtIpCoreTestRunnerObjectInit(AtUnittestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtIpCoreTestRunnerNew(AtDevice device)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtIpCoreTestRunnerObjectInit(newRunner, device);
    }

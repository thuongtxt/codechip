/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtChannelTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : Channel test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "../device_runner/AtDeviceTestRunner.h"
#include "AtChannelTestRunnerInternal.h"
#include "AtPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtChannelTestRunner)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel Channel()
    {
    return AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testCanGetDevice(void)
    {
    AtDevice device = AtChannelDeviceGet(Channel());
    if (device == NULL)
        return;
    TEST_ASSERT_NOT_NULL(AtChannelDeviceGet(Channel()));
    }

static void testCanGetModule(void)
    {
    TEST_ASSERT_NOT_NULL(AtChannelModuleGet(Channel()));
    }

static void testCanGetChannelID(void)
    {
    /* Just get, do not know how to expect from this. This is just for crash testing */
    AtChannelIdGet(Channel());
    }

static void testCanInitialize(void)
    {
    /* TODO: this test case will make traffic test on board fail, need to find a betetr way */
    return;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelInit(Channel()));
    }

static void TestEnableDisable(AtChannelTestRunner self, eBool enable)
    {
    eBool wasEnabled = AtChannelIsEnabled(Channel());
    eAtRet ret;

    if (!AtChannelCanEnable(Channel(), enable))
        return;

    ret = AtChannelEnable(Channel(), enable);
    if (ret == cAtErrorModeNotSupport)
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, ret);
    TEST_ASSERT_EQUAL_INT(enable, AtChannelIsEnabled(Channel()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelEnable(Channel(), wasEnabled));
    }

static void testCanEnable_disable(void)
    {
    AtChannelTestRunner runner = (AtChannelTestRunner)AtUnittestRunnerCurrentRunner();
    mMethodsGet(runner)->TestEnableDisable(runner, cAtTrue);
    mMethodsGet(runner)->TestEnableDisable(runner, cAtFalse);
    }

static void testCanGetTimingMode(void)
    {
    AtChannelTimingModeGet(Channel());
    }

static void testCanGetTimingSource(void)
    {
    AtChannelTimingSourceGet(Channel());
    }

static void testCanGetClockState(void)
    {
    AtChannelClockStateGet(Channel());
    }

static void testCanGetAlarm(void)
    {
    AtChannelAlarmGet(Channel());
    }

static void testCanClearAlarm(void)
    {
    AtChannelAlarmInterruptGet(Channel());
    AtChannelAlarmInterruptClear(Channel());
    }

static void testForceRxAlarm(void)
    {
    AtChannel channel = Channel();
    uint32 forceableAlarms = AtChannelRxForcableAlarmsGet(channel);
    uint32 alarm = cBit31;

    if (forceableAlarms == 0)
        return;

    while (alarm > 0)
        {
        if (alarm & forceableAlarms)
            {
            TEST_ASSERT(AtChannelRxAlarmForce(channel, alarm) == cAtOk);
            TEST_ASSERT(AtChannelRxForcedAlarmGet(channel) == alarm);
            TEST_ASSERT(AtChannelRxAlarmUnForce(channel, alarm) == cAtOk);
            TEST_ASSERT(AtChannelRxForcedAlarmGet(channel) == 0);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtChannelRxAlarmForce(channel, alarm) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }

        alarm = (alarm >> 1);
        }
    }

static void testForceTxAlarm(void)
    {
    AtChannel channel = Channel();
    uint32 forceableAlarms = AtChannelTxForcableAlarmsGet(channel);
    uint32 alarm = cBit31;

    if (forceableAlarms == 0)
        return;

    while (alarm > 0)
        {
        if (alarm & forceableAlarms)
            {
            TEST_ASSERT(AtChannelTxAlarmForce(channel, alarm) == cAtOk);
            TEST_ASSERT(AtChannelTxForcedAlarmGet(channel) == alarm);
            TEST_ASSERT(AtChannelTxAlarmUnForce(channel, alarm) == cAtOk);
            TEST_ASSERT(AtChannelTxForcedAlarmGet(channel) == 0);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtChannelTxAlarmForce(channel, alarm) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }

        alarm = (alarm >> 1);
        }
    }

static void _testTryLoopback(eAtLoopbackMode mode)
    {
    if (AtChannelLoopbackIsSupported(Channel(), mode))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelLoopbackSet(Channel(), mode));
        TEST_ASSERT_EQUAL_INT(mode, AtChannelLoopbackGet(Channel()));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtChannelLoopbackSet(Channel(), mode) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testTryLoopback(void)
    {
    _testTryLoopback(cAtLoopbackModeRelease);
    _testTryLoopback(cAtLoopbackModeLocal);
    _testTryLoopback(cAtLoopbackModeRemote);
    _testTryLoopback(cAtLoopbackModeRelease);
    }

static void testCanGetCounters(void)
    {
    AtChannelCounterGet(Channel(), 1);
    AtChannelCounterClear(Channel(), 1);
    AtChannelAllCountersGet(Channel(), NULL);
    AtChannelAllCountersClear(Channel(), NULL);
    }

static void testNoCrashWhenRetrievingAllConfigration(void)
    {
    AtChannelAllConfigGet(Channel(), NULL);
    }

static void testCanAddRemoveEventListener(void)
    {
    tAtChannelEventListener listener;
    AtOsalMemInit(&listener, 0, sizeof(listener));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelEventListenerAdd(Channel(), &listener));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelEventListenerRemove(Channel(), &listener));
    }

static void testCanGetBoundEncapsulationChannel(void)
    {
    AtChannelBoundEncapChannelGet(Channel());
    }

static void testCanGetBoundPW(void)
    {
    AtChannelBoundPwGet(Channel());
    }

static void testIDStringMustNotBeNull(void)
    {
    TEST_ASSERT_NOT_NULL(AtChannelTypeString(Channel()));
    }

static void testTypeStringMustNotBeNull(void)
    {
    TEST_ASSERT_NOT_NULL(AtChannelIdString(Channel()));
    }

static void testCanAccessPRBSEngine(void)
    {
    AtChannelPrbsEngineGet(Channel());
    }

static void testCanClearStatus(void)
    {
    AtChannelStatusClear(Channel());
    }

static AtModule SurModule(AtChannelTestRunner self)
    {
    AtDevice device = AtChannelTestRunnerDeviceGet(self);
    return AtDeviceModuleGet(device, cAtModuleSur);
    }

static AtModuleSurTestRunner ModuleSurTestRunnerCreate(AtChannelTestRunner self)
    {
    AtDeviceTestRunner deviceRunner = AtModuleTestRunnerDeviceRunnerGet(AtChannelTestRunnerModuleRunnerGet(self));
    AtModuleTestRunnerFactory factory = AtDeviceTestRunnerModuleRunnerFactory(deviceRunner);
    AtModuleTestRunner surRunner = AtModuleTestRunnerFactoryModuleTestRunnerCreate(factory, SurModule(self));
    AtModuleTestRunnerDeviceRunnerSet(surRunner, deviceRunner);
    return (AtModuleSurTestRunner)surRunner;
    }

static void testSurEngine(void)
    {
    AtChannelTestRunner runner = (AtChannelTestRunner)AtUnittestRunnerCurrentRunner();
    AtModuleSurTestRunner moduleRunner;
    AtUnittestRunner engineRunner;

    moduleRunner = ModuleSurTestRunnerCreate(runner);
    if (moduleRunner == NULL)
        return;

    engineRunner = (AtUnittestRunner)mMethodsGet(runner)->CreateSurEngineTestRunner(runner, moduleRunner);
    if (engineRunner)
        {
        AtUnittestRunnerRun(engineRunner);
        AtUnittestRunnerDelete(engineRunner);
        }

    AtUnittestRunnerDelete((AtUnittestRunner)moduleRunner);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtChannel_Fixtures)
        {
        new_TestFixture("testCanGetDevice", testCanGetDevice),
        new_TestFixture("testCanGetModule", testCanGetModule),
        new_TestFixture("testCanGetChannelID", testCanGetChannelID),
        new_TestFixture("testCanInitialize", testCanInitialize),
        new_TestFixture("testCanEnable_disable", testCanEnable_disable),
        new_TestFixture("testCanGetTimingMode", testCanGetTimingMode),
        new_TestFixture("testCanGetTimingSource", testCanGetTimingSource),
        new_TestFixture("testCanGetClockState", testCanGetClockState),
        new_TestFixture("testCanGetAlarm", testCanGetAlarm),
        new_TestFixture("testCanClearAlarm", testCanClearAlarm),
        new_TestFixture("testForceRxAlarm", testForceRxAlarm),
        new_TestFixture("testForceTxAlarm", testForceTxAlarm),
        new_TestFixture("testTryLoopback", testTryLoopback),
        new_TestFixture("testCanGetCounters", testCanGetCounters),
        new_TestFixture("testNoCrashWhenRetrievingAllConfigration", testNoCrashWhenRetrievingAllConfigration),
        new_TestFixture("testCanAddRemoveEventListener", testCanAddRemoveEventListener),
        new_TestFixture("testCanGetBoundEncapsulationChannel", testCanGetBoundEncapsulationChannel),
        new_TestFixture("testCanGetBoundPW", testCanGetBoundPW),
        new_TestFixture("testIDStringMustNotBeNull", testIDStringMustNotBeNull),
        new_TestFixture("testTypeStringMustNotBeNull", testTypeStringMustNotBeNull),
        new_TestFixture("testCanAccessPRBSEngine", testCanAccessPRBSEngine),
        new_TestFixture("testCanClearStatus", testCanClearStatus),
        new_TestFixture("testSurEngine", testSurEngine)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtChannel_Caller, "TestSuite_AtChannel", NULL, NULL, TestSuite_AtChannel_Fixtures);

    return (TestRef)((void *)&TestSuite_AtChannel_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtChannel channel = AtChannelTestRunnerChannelGet((AtChannelTestRunner)self);
    AtSnprintf(buffer, bufferSize, "%s.%s", AtChannelTypeString(channel), AtChannelIdString(channel));
    return buffer;
    }

static AtSurEngineTestRunner CreateSurEngineTestRunner(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule)
    {
    /* Not all channel have SUR engine. */
    return NULL;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void MethodsInit(AtChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TestEnableDisable);
        mMethodOverride(m_methods, CreateSurEngineTestRunner);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtChannelTestRunner);
    }

AtChannelTestRunner AtChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->moduleRunner = moduleRunner;

    return self;
    }

AtChannel AtChannelTestRunnerChannelGet(AtChannelTestRunner self)
    {
    return (AtChannel)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

AtModule AtChannelTestRunnerModuleGet(AtChannelTestRunner self)
    {
    return AtChannelModuleGet(AtChannelTestRunnerChannelGet(self));
    }

AtDevice AtChannelTestRunnerDeviceGet(AtChannelTestRunner self)
    {
    return AtChannelDeviceGet(AtChannelTestRunnerChannelGet(self));
    }

void AtChannelTestRunnerOtherChannelsSet(AtChannelTestRunner self, AtChannel *otherChannels, uint32 numberOfOtherChannels)
    {
    if (self == NULL)
        return;

    self->otherChannels = otherChannels;
    self->numberOfOtherChannels = numberOfOtherChannels;
    }

AtChannel *AtChannelTestRunnerOtherChannelsGet(AtChannelTestRunner self, uint32 *numChannels)
    {
    if (numChannels)
        *numChannels = self->numberOfOtherChannels;
    return self->otherChannels;
    }

AtModuleTestRunner AtChannelTestRunnerModuleRunnerGet(AtChannelTestRunner self)
    {
    return self ? self->moduleRunner : NULL;
    }

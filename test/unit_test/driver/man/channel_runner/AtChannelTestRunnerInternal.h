/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtChannelTestRunnerInternal.h
 * 
 * Created Date: Jun 19, 2014
 *
 * Description : Channel test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELTESTRUNNERINTERNAL_H_
#define _ATCHANNELTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../sur/engine_runner/AtSurEngineTestRunner.h"
#include "../../sur/AtModuleSurTestRunner.h"
#include "../AtObjectTestRunnerInternal.h"
#include "AtChannel.h"
#include "AtChannelTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelTestRunnerMethods
    {
    void (*TestEnableDisable)(AtChannelTestRunner self, eBool enable);
    AtSurEngineTestRunner (*CreateSurEngineTestRunner)(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule);
    }tAtChannelTestRunnerMethods;

typedef struct tAtChannelTestRunner
    {
    tAtObjectTestRunner super;
    const tAtChannelTestRunnerMethods *methods;

    /* Private data */
    AtChannel *otherChannels;
    uint32 numberOfOtherChannels;
    AtModuleTestRunner moduleRunner;
    }tAtChannelTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtChannelTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

void AtChannelTestRunnerOtherChannelsSet(AtChannelTestRunner self, AtChannel *otherChannels, uint32 numberOfOtherChannels);
AtChannel *AtChannelTestRunnerOtherChannelsGet(AtChannelTestRunner self, uint32 *numChannels);

#endif /* _ATCHANNELTESTRUNNERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtChannelTestRunner.h
 * 
 * Created Date: Jun 19, 2014
 *
 * Description : Channel test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELTESTRUNNER_H_
#define _ATCHANNELTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../runner/AtUnittestRunner.h" /* Super class */
#include "AtChannel.h"
#include "../module_runner/AtModuleTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelTestRunner * AtChannelTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete runners */
AtChannelTestRunner AtEthPortTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtEthFlowTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPdhDe1TestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPdhDe3TestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtMfrBundleTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtFrLinkTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtMpBundleTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPppLinkTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtHdlcChannelTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtSdhLineTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtSdhPathTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtSdhMapTestRunnerNew(AtChannel line, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPwTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPwCESoPTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);
AtChannelTestRunner AtPwHdlcTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

/* APIs */
AtChannel AtChannelTestRunnerChannelGet(AtChannelTestRunner self);
AtModule AtChannelTestRunnerModuleGet(AtChannelTestRunner self);
AtDevice AtChannelTestRunnerDeviceGet(AtChannelTestRunner self);
AtModuleTestRunner AtChannelTestRunnerModuleRunnerGet(AtChannelTestRunner self);

void AtChannelTestRunnerOtherChannelsSet(AtChannelTestRunner self, AtChannel *otherChannels, uint32 numberOfOtherChannels);
AtChannel *AtChannelTestRunnerOtherChannelsGet(AtChannelTestRunner self, uint32 *numChannels);

#endif /* _ATCHANNELTESTRUNNER_H_ */


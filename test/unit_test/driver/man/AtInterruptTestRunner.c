/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtInterruptTestRunner.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : Interrupt testing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"

#include "../runner/AtUnittestRunnerInternal.h"
#include "../../../driver/src/generic/man/AtModuleInternal.h"
#include "../../../driver/src/implement/default/sdh/ThaModuleSdhReg.h"
#include "../../../driver/src/implement/default/man/ThaDeviceReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtInterruptTestRunner *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtInterruptTestRunner
    {
    tAtUnittestRunner super;

    /* Private data */
    AtDevice device;
    uint8 interruptAck;
    tAtChannelEventListener eventListener;
    }tAtInterruptTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device()
    {
    return mThis(AtUnittestRunnerCurrentRunner())->device;
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(Device());
    mThis(AtUnittestRunnerCurrentRunner())->interruptAck = 0;
    }

static void tearDown()
    {
    }

static void AlarmSdhChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    AtModule module = AtChannelModuleGet(channel);
    TEST_ASSERT_EQUAL_INT(cAtModuleSdh, AtModuleTypeGet(module));
    TEST_ASSERT_EQUAL_INT(1, AtChannelIdGet(channel));
    TEST_ASSERT_EQUAL_INT((changedAlarms & currentStatus), (AtChannelAlarmInterruptGet(channel) & changedAlarms));
    TEST_ASSERT_EQUAL_INT((currentStatus & changedAlarms), (AtChannelAlarmGet(channel) & changedAlarms));

    /* Interrupt acknowledge */
    mThis(AtUnittestRunnerCurrentRunner())->interruptAck = 1;
    }

/*
 * SDH line interrupt scenario
 */
static void SdhLineInterruptTrigger()
    {
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(Device(), 0));
    AtModule sdhModule = AtDeviceModuleGet(Device(), cAtModuleSdh);

    TEST_ASSERT_NOT_NULL(sdhModule);

    mModuleHwWrite(sdhModule, cThaRegOcnRxLineperLineIntrOrStat, cThaOcnRxLLineIntrLineIdMask(1));

    /* Raise interrupt at SDH line */
    if (hal)
        {
        mMethodsGet(hal)->Write(hal, cThaRegChipIntrSta, cThaRegChipIntrStaRxLineOcnIntAlarm);
        }
    }

static void testAddSdhLineEventListener()
    {
    AtSdhLine sdhLine;
    AtDevice testedDevice = Device();
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(testedDevice, cAtModuleSdh);

    TEST_ASSERT_NOT_NULL(sdhModule);

    /* A new event listener to SDH line 1 */
    mThis(AtUnittestRunnerCurrentRunner())->eventListener.AlarmChangeState = AlarmSdhChangeState;
    mThis(AtUnittestRunnerCurrentRunner())->eventListener.OamReceived = NULL;
    sdhLine = AtModuleSdhLineGet(sdhModule, 1);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelEventListenerAdd((AtChannel)sdhLine, &mThis(AtUnittestRunnerCurrentRunner())->eventListener));
    }

static void testRemoveSdhLineEventListener()
    {
    AtSdhLine sdhLine;
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    AtIpCore core = AtDeviceIpCoreGet(testedDevice, 0);

    TEST_ASSERT_NOT_NULL(sdhModule);

    sdhLine = AtModuleSdhLineGet((AtModuleSdh)sdhModule, 1);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelEventListenerRemove((AtChannel)sdhLine, &mThis(AtUnittestRunnerCurrentRunner())->eventListener));

    /* Trigger a line interrupt */
    SdhLineInterruptTrigger();

    /* Enable Module sdh interrupt */
    AtModuleInterruptEnable(sdhModule, cAtTrue);

    /* Enable IP core interrupt */
    AtIpCoreInterruptEnable(core, cAtTrue);

    /* Interrupt process */
    AtDeviceInterruptProcess(testedDevice);

    /* Interrupt cannot be acknowledged */
    TEST_ASSERT_EQUAL_INT(0, mThis(AtUnittestRunnerCurrentRunner())->interruptAck);
    }

static void testSdhLineIntrProcessWhenCoreIntrIsDisabled()
    {
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    AtIpCore core = AtDeviceIpCoreGet(testedDevice, 0);

    TEST_ASSERT_NOT_NULL(sdhModule);

    /* Enable Module sdh interrupt */
    AtModuleInterruptEnable(sdhModule, cAtTrue);

    /* Disable IP core interrupt */
    AtIpCoreInterruptEnable(core, cAtFalse);

    /* Trigger a line interrupt */
    SdhLineInterruptTrigger();

    /* Interrupt process */
    AtDeviceInterruptProcess(testedDevice);

    /* Interrupt should NOT be acknowledged */
    if (!AtTestIsSimulationTesting())
        {
        TEST_ASSERT_EQUAL_INT(0, mThis(AtUnittestRunnerCurrentRunner())->interruptAck);
        }
    }

static void testSdhLineIntrProcessWhenModuleIntrIsDisabled()
    {
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    AtIpCore core = AtDeviceIpCoreGet(testedDevice, 0);

    TEST_ASSERT_NOT_NULL(sdhModule);

    /* Disable Module sdh interrupt */
    AtModuleInterruptEnable(sdhModule, cAtFalse);

    /* Enable IP core interrupt */
    AtIpCoreInterruptEnable(core, cAtTrue);

    /* Trigger a line interrupt */
    SdhLineInterruptTrigger();

    /* Interrupt process */
    AtDeviceInterruptProcess(testedDevice);

    /* Interrupt should NOT be acknowledged */
    TEST_ASSERT_EQUAL_INT(0, mThis(AtUnittestRunnerCurrentRunner())->interruptAck);
    }

static void testSdhLineIntrProcessWhenIntrIsEnabled()
    {
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    AtIpCore core = AtDeviceIpCoreGet(testedDevice, 0);

    TEST_ASSERT_NOT_NULL(sdhModule);

    /* Enable Module sdh interrupt */
    AtModuleInterruptEnable(sdhModule, cAtTrue);

    /* Enable IP core interrupt */
    AtIpCoreInterruptEnable(core, cAtTrue);

    /* Trigger a line interrupt */
    SdhLineInterruptTrigger();

    /* Interrupt process */
    AtDeviceInterruptProcess(testedDevice);

    /* Interrupt should be acknowledged */
    /* FIXME: open the flowing line and make it pass */
    /*TEST_ASSERT_EQUAL_INT(1, mThis(AtUnittestRunnerCurrentRunner())->interruptAck);*/
    }

static void testIpCoreInterruptEnable()
    {
    AtDevice testedDevice = Device();
    uint8 numCore = AtDeviceNumIpCoresGet(testedDevice);
    AtIpCore core;
    uint8 i;

    for (i = 0; i < numCore; i++)
        {
        core = AtDeviceIpCoreGet(testedDevice, i);
        TEST_ASSERT_EQUAL_INT(cAtOk, AtIpCoreInterruptEnable(core, cAtTrue));
        }
    }

static void testIpCoreInterruptDisable()
    {
    AtDevice testedDevice = Device();
    uint8 numCore = AtDeviceNumIpCoresGet(testedDevice);
    AtIpCore core;
    uint8 i;

    for (i = 0; i < numCore; i++)
        {
        core = AtDeviceIpCoreGet(testedDevice, i);
        TEST_ASSERT_EQUAL_INT(cAtOk, AtIpCoreInterruptEnable(core, cAtFalse));
        }
    }

static void testIpCoreInterruptEnableGet()
    {
    AtDevice testedDevice = Device();
    uint8 numCore = AtDeviceNumIpCoresGet(testedDevice);
    AtIpCore core;
    uint8 i;

    for (i = 0; i < numCore; i++)
        {
        core = AtDeviceIpCoreGet(testedDevice, i);
        TEST_ASSERT_EQUAL_INT(cAtOk, AtIpCoreInterruptEnable(core, cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtIpCoreInterruptIsEnabled(core));

        TEST_ASSERT_EQUAL_INT(cAtOk, AtIpCoreInterruptEnable(core, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtIpCoreInterruptIsEnabled(core));
        }
    }

static void testModuleSdhInterruptEnable()
    {
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    if (sdhModule)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(sdhModule, cAtTrue));
        }
    }

static void testModuleSdhInterruptDisable()
    {
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    if (sdhModule)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(sdhModule, cAtFalse));
        }
    }

static void testModuleSdhInterruptEnableGet()
    {
    AtDevice testedDevice = Device();
    AtModule sdhModule = AtDeviceModuleGet(testedDevice, cAtModuleSdh);
    if (sdhModule)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(sdhModule, cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtModuleInterruptIsEnabled(sdhModule));

        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(sdhModule, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtModuleInterruptIsEnabled(sdhModule));
        }
    }

static void testModulePdhInterruptEnable()
    {
    AtDevice testedDevice = Device();
    AtModule pdhModule = AtDeviceModuleGet(testedDevice, cAtModulePdh);
    if (pdhModule)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(pdhModule, cAtTrue));
        }
    }

static void testModulePdhInterruptDisable()
    {
    AtDevice testedDevice = Device();
    AtModule pdhModule = AtDeviceModuleGet(testedDevice, cAtModulePdh);
    if (pdhModule)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(pdhModule, cAtFalse));
        }
    }

static void testModulePdhInterruptEnableGet()
    {
    AtDevice testedDevice = Device();
    AtModule pdhModule = AtDeviceModuleGet(testedDevice, cAtModulePdh);
    if (pdhModule)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(pdhModule, cAtTrue));
        TEST_ASSERT_EQUAL_INT(cAtTrue, AtModuleInterruptIsEnabled(pdhModule));

        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleInterruptEnable(pdhModule, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtModuleInterruptIsEnabled(pdhModule));
        }
    }

/* Test suite: Interrupt enable/disable */
static TestRef _TestSuite_InterruptEnable(void)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_InterruptEnable_Fixtures)
        {
        new_TestFixture("testIpCoreInterruptEnable", testIpCoreInterruptEnable),
        new_TestFixture("testIpCoreInterruptDisable", testIpCoreInterruptDisable),
        new_TestFixture("testIpCoreInterruptEnableGet", testIpCoreInterruptEnableGet),
        new_TestFixture("testModuleSdhInterruptEnable", testModuleSdhInterruptEnable),
        new_TestFixture("testModuleSdhInterruptDisable", testModuleSdhInterruptDisable),
        new_TestFixture("testModuleSdhInterruptEnableGet", testModuleSdhInterruptEnableGet),
        new_TestFixture("testModulePdhInterruptEnable", testModulePdhInterruptEnable),
        new_TestFixture("testModulePdhInterruptDisable", testModulePdhInterruptDisable),
        new_TestFixture("testModulePdhInterruptEnableGet", testModulePdhInterruptEnableGet),
        new_TestFixture("", NULL),
        };


    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_InterruptEnable_Caller, "TestSuite_InterruptEnable", NULL, NULL, TestSuite_InterruptEnable_Fixtures);

    return (TestRef)((void *)&TestSuite_InterruptEnable_Caller);
    }

/* Test suite: SDH line Interrupt process */
static TestRef _TestSuite_SdhLineInterruptProcess(void)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_SdhLineInterrupt_Fixtures)
        {
        new_TestFixture("testAddSdhLineEventListener", testAddSdhLineEventListener),
        new_TestFixture("testSdhLineIntrProcessWhenIntrIsEnabled", testSdhLineIntrProcessWhenIntrIsEnabled),
        new_TestFixture("testSdhLineIntrProcessWhenCoreIntrIsDisabled", testSdhLineIntrProcessWhenCoreIntrIsDisabled),
        new_TestFixture("testSdhLineIntrProcessWhenModuleIntrIsDisabled", testSdhLineIntrProcessWhenModuleIntrIsDisabled),
        new_TestFixture("testRemoveSdhLineEventListener", testRemoveSdhLineEventListener),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_SdhLineInterrupt_Caller, "TestSuite_SdhLineInterruptProcess", setUp, tearDown, TestSuite_SdhLineInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_SdhLineInterrupt_Caller);
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);

    TextUIRunner_runTest(_TestSuite_InterruptEnable());
    if (AtDeviceModuleGet(mThis(self)->device, cAtModuleSdh) != NULL)
        TextUIRunner_runTest(_TestSuite_SdhLineInterruptProcess());
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "Interrupt");
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(self), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtInterruptTestRunner);
    }

AtUnittestRunner AtInterruptTestRunnerObjectInit(AtUnittestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit((AtUnittestRunner)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->device = device;

    return self;
    }

AtUnittestRunner AtInterruptTestRunnerNew(AtDevice device)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtInterruptTestRunnerObjectInit(newRunner, device);
    }

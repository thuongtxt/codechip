/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtDeviceTestRunnerFactoryInternal.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICETESTRUNNERFACTORYINTERNAL_H_
#define _ATDEVICETESTRUNNERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDeviceTestRunnerFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDeviceTestRunnerFactoryMethods
    {
    AtDeviceTestRunner (*DeviceTestRunnerCreate)(AtDeviceTestRunnerFactory self, AtDevice device);
    }tAtDeviceTestRunnerFactoryMethods;

typedef struct tAtDeviceTestRunnerFactory
    {
    const tAtDeviceTestRunnerFactoryMethods *methods;
    }tAtDeviceTestRunnerFactory;

typedef struct tAtDeviceTestRunnerDefaultFactory
    {
    tAtDeviceTestRunnerFactory super;
    }tAtDeviceTestRunnerDefaultFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunnerFactory AtDeviceTestRunnerFactoryObjectInit(AtDeviceTestRunnerFactory self);
AtDeviceTestRunnerFactory AtDeviceTestRunnerDefaultFactoryObjectInit(AtDeviceTestRunnerFactory self);

#endif /* _ATDEVICETESTRUNNERFACTORYINTERNAL_H_ */


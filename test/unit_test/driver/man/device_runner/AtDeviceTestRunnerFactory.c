/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtDeviceTestRunnerFactory.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDeviceTestRunnerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtDeviceTestRunnerFactoryMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDeviceTestRunner DeviceTestRunnerCreate(AtDeviceTestRunnerFactory self, AtDevice device)
    {
    /* Sub class must know */
    return NULL;
    }

static void MethodsInit(AtDeviceTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        m_methods.DeviceTestRunnerCreate = DeviceTestRunnerCreate;
        }

    self->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDeviceTestRunnerFactory);
    }

AtDeviceTestRunnerFactory AtDeviceTestRunnerFactoryObjectInit(AtDeviceTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner AtDeviceTestRunnerFactoryDeviceTestRunnerCreate(AtDeviceTestRunnerFactory self, AtDevice device)
    {
    if (self)
        return self->methods->DeviceTestRunnerCreate(self, device);
    return NULL;
    }

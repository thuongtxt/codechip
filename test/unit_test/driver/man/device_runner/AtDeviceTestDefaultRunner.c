/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtDeviceTestDefaultRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDeviceTestRunnerInternal.h"
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerMethods m_AtDeviceTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(AtDeviceTestRunnerDeviceGet(self), cAtModuleSdh);
    if (sdhModule)
        return AtModuleTestRunnerFactoryStmProductSharedFactory();
    else
        return AtModuleTestRunnerFactoryPdhProductSharedFactory();
    }

static AtUnittestRunner InterruptTestRunnerCreate(AtDeviceTestRunner self)
    {
    extern AtUnittestRunner AtInterruptTestRunnerNew(AtDevice device);
    return AtInterruptTestRunnerNew(AtDeviceTestRunnerDeviceGet(self));
    }

static void TestInterruptProcessing(AtDeviceTestRunner self)
    {
    AtUnittestRunner runner;

    if (!mMethodsGet(self)->ShouldTestInterruptProcessing(self))
        return;

    runner = InterruptTestRunnerCreate(self);
    if (runner)
        AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static void OverrideAtDeviceTestRunner(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtDeviceTestRunnerOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerOverride));

        m_AtDeviceTestRunnerOverride.ModuleRunnerFactory     = ModuleRunnerFactory;
        m_AtDeviceTestRunnerOverride.TestInterruptProcessing = TestInterruptProcessing;
        }

    self->methods = &m_AtDeviceTestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtDeviceTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDeviceTestDefaultRunner);
    }

AtDeviceTestRunner AtDeviceTestDefaultRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDeviceTestRunnerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunner AtDeviceTestDefaultRunnerNew(AtDevice device)
    {
    AtDeviceTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtDeviceTestDefaultRunnerObjectInit(newRunner, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtDeviceTestRunnerDefaultFactory.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Default device runner factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDeviceTestRunnerFactoryInternal.h"
#include "AtDeviceTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceTestRunnerFactoryMethods m_AtDeviceTestRunnerFactoryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDeviceTestRunner DeviceTestRunnerCreate(AtDeviceTestRunnerFactory self, AtDevice device)
    {
    return AtDeviceTestDefaultRunnerNew(device);
    }

static void OverrideAtDeviceTestRunnerFactory(AtDeviceTestRunnerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtDeviceTestRunnerFactoryOverride, (void *)self->methods, sizeof(m_AtDeviceTestRunnerFactoryOverride));

        m_AtDeviceTestRunnerFactoryOverride.DeviceTestRunnerCreate = DeviceTestRunnerCreate;
        }

    self->methods = &m_AtDeviceTestRunnerFactoryOverride;
    }

static void Override(AtDeviceTestRunnerFactory self)
    {
    OverrideAtDeviceTestRunnerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDeviceTestRunnerDefaultFactory);
    }

AtDeviceTestRunnerFactory AtDeviceTestRunnerDefaultFactoryObjectInit(AtDeviceTestRunnerFactory self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDeviceTestRunnerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDeviceTestRunnerFactory AtDeviceTestRunnerDefaultFactorySharedFactory()
    {
    static tAtDeviceTestRunnerDefaultFactory sharedFactory;
    return AtDeviceTestRunnerDefaultFactoryObjectInit((AtDeviceTestRunnerFactory)&sharedFactory);
    }

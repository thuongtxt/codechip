/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtDeviceTestRunner.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device unittest runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICETESTRUNNER_H_
#define _ATDEVICETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "../AtTestRunnerClasses.h"
#include "../runner/AtUnittestRunner.h" /* Super class */
#include "../module_runner/AtModuleTestRunnerFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete runners */
AtDeviceTestRunner AtDeviceTestDefaultRunnerNew(AtDevice device);

/* APIs */
AtDevice AtDeviceTestRunnerDeviceGet(AtDeviceTestRunner self);
AtModuleTestRunnerFactory AtDeviceTestRunnerModuleRunnerFactory(AtDeviceTestRunner self);

eBool AtDeviceTestRunnerModuleNotReady(AtDeviceTestRunner self, eBool moduleId);

#endif /* _ATDEVICETESTRUNNER_H_ */


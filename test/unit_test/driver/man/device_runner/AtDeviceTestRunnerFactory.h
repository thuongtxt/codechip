/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtDeviceTestRunnerFactory.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICETESTRUNNERFACTORY_H_
#define _ATDEVICETESTRUNNERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDeviceTestRunner.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDeviceTestRunnerFactory * AtDeviceTestRunnerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete factories */
AtDeviceTestRunnerFactory AtDeviceTestRunnerDefaultFactorySharedFactory();

/* APIs */
AtDeviceTestRunner AtDeviceTestRunnerFactoryDeviceTestRunnerCreate(AtDeviceTestRunnerFactory self, AtDevice device);

#endif /* _ATDEVICETESTRUNNERFACTORY_H_ */


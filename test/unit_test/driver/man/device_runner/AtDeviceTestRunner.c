/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtDeviceTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtDeviceTestRunnerInternal.h"
#include "AtPhysicalTests.h"
#include "../module_runner/AtModuleTestRunner.h"
#include "../../../../driver/src/generic/man/AtDeviceInternal.h"
#include "../../../../driver/src/generic/man/AtIpCoreInternal.h"
#include "../../../../driver/src/generic/man/AtDeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtDeviceTestRunner)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtDeviceTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device()
    {
    return AtDeviceTestRunnerDeviceGet((AtDeviceTestRunner)AtUnittestRunnerCurrentRunner());
    }

static AtDeviceTestRunner CurrentRunner()
    {
    return (AtDeviceTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModuleTestRunnerFactory ModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return AtModuleTestRunnerFactoryNew();
    }

static eBool ModuleMustBeSupported(AtDeviceTestRunner self, eAtModule module)
    {
    uint8 numberOfSupportedModules, i;
    const eAtModule *supportedModuleTypes = AtDeviceAllSupportedModulesGet(Device(), &numberOfSupportedModules);

    for (i = 0; i < numberOfSupportedModules; i++)
        {
        if (supportedModuleTypes[i] == module)
            return cAtTrue;
        }

    return cAtFalse;
    }

static void testMustSupportSomeModules()
    {
    eAtModule moduleId;
    AtDeviceTestRunner currentRunner = CurrentRunner();

    for (moduleId = cAtModuleStart; moduleId < cAtModuleNumModules; moduleId++)
        {
        if (mMethodsGet(currentRunner)->ModuleMustBeSupported(currentRunner, moduleId))
            {
            if (AtDeviceModuleGet(Device(), moduleId) == NULL)
                {
                TEST_ASSERT_NOT_NULL(AtDeviceModuleGet(Device(), moduleId));
                }
            }
        else
            {
            TEST_ASSERT_NULL(AtDeviceModuleGet(Device(), moduleId));
            }
        }
    }

static eBool ShouldTestModule(AtDeviceTestRunner self, eAtModule moduleId)
    {
    if (AtDeviceTestRunnerModuleNotReady(self, moduleId))
        return cAtFalse;

    return cAtTrue;
    }

static void ModuleRun(AtDeviceTestRunner self, eAtModule moduleId)
    {
    AtDevice device;
    AtUnittestRunner runner;

    if (!mMethodsGet(self)->ShouldTestModule(self, moduleId))
        return;

    device = AtDeviceTestRunnerDeviceGet((AtDeviceTestRunner)self);
    runner = (AtUnittestRunner)AtModuleTestRunnerFactoryModuleTestRunnerCreate(AtDeviceTestRunnerModuleRunnerFactory(self), AtDeviceModuleGet(device, moduleId));
    AtModuleTestRunnerDeviceRunnerSet((AtModuleTestRunner)runner, self);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static void TestInterruptProcessing(AtDeviceTestRunner self)
    {
    }

static void DidReadRegister(AtHal hal, void *listener, uint32 address, uint32 value)
    {
    tAtDeviceTestRunnerHwAccessDb * asyncDb = (tAtDeviceTestRunnerHwAccessDb*)listener;

    if (asyncDb)
        {
        asyncDb->readTime ++;
        asyncDb->totalRegs += address;
        asyncDb->magicNumber += ((asyncDb->writeTime + asyncDb->readTime) * address);
        }
    }

static void DidWriteRegister(AtHal hal, void *listener, uint32 address, uint32 value)
    {
    tAtDeviceTestRunnerHwAccessDb * asyncDb = (tAtDeviceTestRunnerHwAccessDb*)listener;

    if (asyncDb)
        {
        asyncDb->writeTime ++;
        asyncDb->totalRegs += address;
        asyncDb->totalValues += value;
        asyncDb->magicNumber += ((asyncDb->writeTime + asyncDb->readTime) * address);
        }
    }

static tAtHalListener *HalListener(void)
    {
    static tAtHalListener listener;
    static tAtHalListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidReadRegister  = DidReadRegister;
    listener.DidWriteRegister = DidWriteRegister;
    pListener = &listener;

    return pListener;
    }

static void setup()
    {
    TEST_ASSERT_NOT_NULL(Device());
    }

static void HalListenerAdd()
    {
    /* Setup HAL profile */
    AtHal hal = AtDeviceIpCoreHalGet(Device(), 0);
    TEST_ASSERT_NOT_NULL(hal);
    AtHalListenerAdd(hal, &(mThis(CurrentRunner())->hwAccessDb), HalListener());
    }

static void HalListenerRemove()
    {
    AtHal hal = NULL;
    hal = AtDeviceIpCoreHalGet(Device(), 0);
    /* Un-install HAL profile */
    AtHalListenerRemove(hal, &(mThis(CurrentRunner())->hwAccessDb), HalListener());
    }

static void tearDown()
    {
    }

static void testGetProductCode()
    {
    uint32 productCode = AtTestProductCodeGet();
    if (AtTestIsSimulationTesting())
        productCode = AppSimulationProductCode(productCode);
    TEST_ASSERT_EQUAL_INT(AtDeviceProductCodeGet(Device()), productCode);
    }

static void testGetIpCores()
    {
    uint8 numCores, i;

    /* Device must have at least one core */
    numCores = AtDeviceNumIpCoresGet(Device());
    TEST_ASSERT(numCores > 0);

    /* And these cores can be accessed */
    for (i = 0; i < numCores; i++)
        {
        TEST_ASSERT_NOT_NULL(AtDeviceIpCoreGet(Device(), i));
        }
    }

static void testIpCoresIterator()
    {
    AtIpCore core;
    AtIterator iterator = AtDeviceCoreIteratorCreate(Device());
    TEST_ASSERT_NOT_NULL(iterator);
    TEST_ASSERT_EQUAL_INT(AtDeviceNumIpCoresGet(Device()), AtIteratorCount(iterator));
    while((core = (AtIpCore)AtIteratorNext(iterator)) != NULL)
        {
        TEST_ASSERT(AtIpCoreDeviceGet(core) == Device());
        }
    AtObjectDelete((AtObject)iterator);
    }

static void testModuleIterator()
    {
    AtModule iteratorModule;
    uint8 numModules, i, has;
    const eAtModule *modules;
    eAtModule iteratorModuleType;
    AtIterator iterator = AtDeviceModuleIteratorCreate(Device());

    TEST_ASSERT_NOT_NULL(iterator);
    modules = AtDeviceAllSupportedModulesGet(Device(), &numModules);
    TEST_ASSERT_EQUAL_INT(numModules, AtIteratorCount(iterator));
    while((iteratorModule = (AtModule)AtIteratorNext(iterator)) != NULL)
        {
        has = 0;
        iteratorModuleType = AtModuleTypeGet(iteratorModule);
        for (i = 0; i < numModules; i++)
            if (iteratorModuleType == modules[i])
                has = 1;
        TEST_ASSERT_EQUAL_INT(1, has);
        }

    AtObjectDelete((AtObject)iterator);
    }

static void testSerdesManageOfNullDevice()
    {
    TEST_ASSERT(AtDeviceSerdesManagerGet(NULL) == NULL);
    }

/* Can get supported modules */
static void testGetSupportedModules()
    {
    uint8 numberOfSupportedModules, i;
    const eAtModule *supportedModuleTypes;
    eAtModule moduleType;
    AtModule module;

    /* Get all of modules that can be supported by this device */
    supportedModuleTypes = AtDeviceAllSupportedModulesGet(Device(), &numberOfSupportedModules);
    TEST_ASSERT_NOT_NULL(supportedModuleTypes);
    TEST_ASSERT(numberOfSupportedModules > 0);

    /* Get all of modules */
    for (i = 0; i < numberOfSupportedModules; i++)
        {
        moduleType = supportedModuleTypes[i];
        module = AtDeviceModuleGet(Device(), moduleType);
        TEST_ASSERT_NOT_NULL(module);
        TEST_ASSERT_EQUAL_INT(AtModuleTypeGet(module), moduleType);
        TEST_ASSERT(AtModuleDeviceGet(module) == Device());
        }
    }

static void testInitializeAllModules()
    {
    uint8 numberOfSupportedModules, i;
    const eAtModule *supportedModuleTypes;
    eAtModule moduleType;
    AtModule module;

    /* Get all of modules */
    supportedModuleTypes = AtDeviceAllSupportedModulesGet(Device(), &numberOfSupportedModules);
    for (i = 0; i < numberOfSupportedModules; i++)
        {
        moduleType = supportedModuleTypes[i];
        module = AtDeviceModuleGet(Device(), moduleType);
        AtModuleInit(module);
        }

    /* At the end of this test, it is good to re-init the whole device just because
     * there are some sub components of device are still refering objects of
     * these modules */
    AtDeviceInit(Device());
    }

static void HwAccessDbReset()
    {
    AtOsalMemInit(&(mThis(CurrentRunner())->hwAccessDb), 0, sizeof(tAtDeviceTestRunnerHwAccessDb));
    }

static void HwAccessDbSave(tAtDeviceTestRunnerHwAccessDb* cache)
    {
    AtOsalMemCpy(cache, &(mThis(CurrentRunner())->hwAccessDb), sizeof(tAtDeviceTestRunnerHwAccessDb));
    }

static void testDeviceAsyncInitWhenDeviceIsActivated()
    {
    tAtDeviceTestRunnerHwAccessDb devInitCache;
    tAtDeviceTestRunnerHwAccessDb devAsyncInitCache;
    AtDeviceTestRunner runner = CurrentRunner();
    AtDevice device = Device();
    AtIpCore core = AtDeviceIpCoreGet(device, 0);

    if (!mMethodsGet(runner)->ShouldTestAsyncInit(runner))
        return;

    AtOsalMemInit(&devInitCache, 0, sizeof(tAtDeviceTestRunnerHwAccessDb));
    AtOsalMemInit(&devAsyncInitCache, 0, sizeof(tAtDeviceTestRunnerHwAccessDb));

    AtIpCoreActivate(core, cAtTrue);

    HalListenerAdd();

    HwAccessDbReset();
    AtDeviceInit(Device());
    HwAccessDbSave(&devInitCache);

    HwAccessDbReset();
    while (1)
        {
        /* Only retry when having again error code */
        eAtRet ret = AtDeviceAsyncInit(Device());
        if (ret != cAtErrorAgain && ret != cAtRetNeedDelay)
            break;
        }

    HwAccessDbSave(&devAsyncInitCache);
    HalListenerRemove();
    TEST_ASSERT_EQUAL_INT(0, AtOsalMemCmp(&devInitCache, &devAsyncInitCache, sizeof(tAtDeviceTestRunnerHwAccessDb)));
    }

static void testDeviceAsyncInitWhenDeviceIsNotActivated()
    {
    tAtDeviceTestRunnerHwAccessDb devInitCache;
    tAtDeviceTestRunnerHwAccessDb devAsyncInitCache;
    AtDeviceTestRunner runner = CurrentRunner();
    AtDevice device = Device();
    AtIpCore core = AtDeviceIpCoreGet(device, 0);

    if (!mMethodsGet(runner)->ShouldTestAsyncInit(runner) ||
        !AtDeviceShouldResetBeforeInitializing(Device()))
        return;

    AtOsalMemInit(&devInitCache, 0, sizeof(tAtDeviceTestRunnerHwAccessDb));
    AtOsalMemInit(&devAsyncInitCache, 0, sizeof(tAtDeviceTestRunnerHwAccessDb));

    AtIpCoreActivate(core, cAtFalse);

    HalListenerAdd();
    HwAccessDbReset();
    AtDeviceInit(Device());
    HwAccessDbSave(&devInitCache);

    HalListenerRemove();
    /* Device init will active device, deactivate */
    AtIpCoreActivate(core, cAtFalse);

    HalListenerAdd();
    HwAccessDbReset();
    while (1)
        {
        /* Only retry when having again error code */
        eAtRet ret = AtDeviceAsyncInit(Device());
        if (ret != cAtErrorAgain && ret != cAtRetNeedDelay)
            break;
        }

    HwAccessDbSave(&devAsyncInitCache);
    HalListenerRemove();
    TEST_ASSERT_EQUAL_INT(0, AtOsalMemCmp(&devInitCache, &devAsyncInitCache, sizeof(tAtDeviceTestRunnerHwAccessDb)));
    }

static void testDeviceAsyncReset()
    {
    tAtDeviceTestRunnerHwAccessDb devInitCache;
    tAtDeviceTestRunnerHwAccessDb devAsyncInitCache;
    AtDeviceTestRunner runner = CurrentRunner();
    AtDevice device = Device();

    if (!mMethodsGet(runner)->ShouldTestAsyncInit(runner) ||
        !AtDeviceShouldResetBeforeInitializing(device))
        return;

    AtOsalMemInit(&devInitCache, 0, sizeof(tAtDeviceTestRunnerHwAccessDb));
    AtOsalMemInit(&devAsyncInitCache, 0, sizeof(tAtDeviceTestRunnerHwAccessDb));

    HalListenerAdd();

    HwAccessDbReset();
    AtDeviceReset(device);
    HwAccessDbSave(&devInitCache);

    HwAccessDbReset();
    while (1)
        {
        /* Only retry when having again error code */
        eAtRet ret = AtDeviceAsyncReset(device);
        if (ret != cAtErrorAgain && ret != cAtRetNeedDelay)
            break;
        }

    HwAccessDbSave(&devAsyncInitCache);
    HalListenerRemove();

    TEST_ASSERT_EQUAL_INT(0, AtOsalMemCmp(&devInitCache, &devAsyncInitCache, sizeof(tAtDeviceTestRunnerHwAccessDb)));
    }
    
static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtDevice_Fixtures)
        {
        new_TestFixture("testGetProductCode", testGetProductCode),
        new_TestFixture("testGetSupportedModules", testGetSupportedModules),
        new_TestFixture("testMustSupportSomeModules", testMustSupportSomeModules),
        new_TestFixture("testInitializeAllModules", testInitializeAllModules),
        new_TestFixture("testGetIpCores", testGetIpCores),
        new_TestFixture("testIpCoreIterator", testIpCoresIterator),
        new_TestFixture("testModuleIterator", testModuleIterator),
        new_TestFixture("testSerdesManageOfNullDevice", testSerdesManageOfNullDevice),
        new_TestFixture("testDeviceAsyncReset", testDeviceAsyncReset),
        new_TestFixture("testDeviceAsyncInitWhenDeviceIsActivated", testDeviceAsyncInitWhenDeviceIsActivated),
        new_TestFixture("testDeviceAsyncInitWhenDeviceIsNotActivated", testDeviceAsyncInitWhenDeviceIsNotActivated),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtDevice_Caller, "TestSuite_AtDevice", setup, tearDown, TestSuite_AtDevice_Fixtures);

    return (TestRef)((void *)&TestSuite_AtDevice_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void TestInterrupt(AtDeviceTestRunner self)
    {
    AtDeviceInit(AtDeviceTestRunnerDeviceGet(self));
    mMethodsGet(mThis(self))->TestInterruptProcessing(mThis(self));
    }

static void TestAllModules(AtDeviceTestRunner self)
    {
    AtDevice device = AtDeviceTestRunnerDeviceGet((AtDeviceTestRunner)self);
    uint8 numModules, module_i;
    const eAtModule *supportedModules = AtDeviceAllSupportedModulesGet(device, &numModules);

    for (module_i = 0; module_i < numModules; module_i++)
        ModuleRun(mThis(self), supportedModules[module_i]);
    }

static AtUnittestRunner IpCoreTestRunnerCreate(AtDeviceTestRunner self)
    {
    extern AtUnittestRunner AtIpCoreTestRunnerNew(AtDevice device);
    return AtIpCoreTestRunnerNew(AtDeviceTestRunnerDeviceGet(self));
    }

static void TestIpCores(AtDeviceTestRunner self)
    {
    AtUnittestRunner runner = IpCoreTestRunnerCreate(mThis(self));
    if (runner)
        AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtDevice device = AtDeviceTestRunnerDeviceGet((AtDeviceTestRunner)self);
    AtSnprintf(buffer, bufferSize, "Product 0x%08x, Device %s", AtDeviceProductCodeGet(device), AtObjectToString((AtObject)device));
    return buffer;
    }

static void TestSerdesManager(AtDeviceTestRunner self)
    {
    AtDevice device = AtDeviceTestRunnerDeviceGet(self);
    AtSerdesManager manager = AtDeviceSerdesManagerGet(device);
    AtUnittestRunner runner;

    if (manager == NULL)
        return;

    runner = mMethodsGet(self)->SerdesManagerTestRunnerCreate(self, manager);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    TestSerdesManager(mThis(self));
    TestIpCores(mThis(self));
    TestInterrupt(mThis(self));
    TestAllModules(mThis(self));
    }

static eBool ShouldTestInterruptProcessing(AtDeviceTestRunner self)
    {
    return cAtFalse;
    }

static eBool ModuleNotReady(AtDeviceTestRunner self, eAtModule moduleId)
    {
    return cAtFalse;
    }

static AtUnittestRunner SerdesManagerTestRunnerCreate(AtDeviceTestRunner self, AtSerdesManager manager)
    {
    return (AtUnittestRunner)AtSerdesManagerTestRunnerNew(manager);
    }

static eBool ShouldTestAsyncInit(AtDeviceTestRunner self)
    {
    return cAtFalse;
    }

static uint32 VersionTestAsyncInit(AtDeviceTestRunner self)
    {
    return 0;
    }

static uint32 BuiltNumberTestAsyncInit(AtDeviceTestRunner self)
    {
    return 0;
    }

static void MethodsInit(AtDeviceTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ModuleRunnerFactory);
        mMethodOverride(m_methods, TestInterruptProcessing);
        mMethodOverride(m_methods, ModuleMustBeSupported);
        mMethodOverride(m_methods, ShouldTestInterruptProcessing);
        mMethodOverride(m_methods, ShouldTestModule);
        mMethodOverride(m_methods, ModuleNotReady);
        mMethodOverride(m_methods, SerdesManagerTestRunnerCreate);
        mMethodOverride(m_methods, ShouldTestAsyncInit);
        mMethodOverride(m_methods, VersionTestAsyncInit);
        mMethodOverride(m_methods, BuiltNumberTestAsyncInit);
        }

    self->methods = &m_methods;
    }

static void OverrideAtUnittestRunner(AtDeviceTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtDeviceTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDeviceTestRunner);
    }

AtDeviceTestRunner AtDeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice AtDeviceTestRunnerDeviceGet(AtDeviceTestRunner self)
    {
    return (AtDevice)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

AtModuleTestRunnerFactory AtDeviceTestRunnerModuleRunnerFactory(AtDeviceTestRunner self)
    {
    return mMethodsGet(mThis(self))->ModuleRunnerFactory(mThis(self));
    }

eBool AtDeviceTestRunnerModuleNotReady(AtDeviceTestRunner self, eBool moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleNotReady(self, moduleId);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtDeviceTestRunnerInternal.h
 * 
 * Created Date: Jun 18, 2014
 *
 * Description : Device test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICETESTRUNNERINTERNAL_H_
#define _ATDEVICETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../AtObjectTestRunnerInternal.h"
#include "../module_runner/AtModuleTestRunnerFactory.h"
#include "AtDeviceTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDeviceTestRunnerHwAccessDb
    {
    uint32 readTime;
    uint32 writeTime;
    uint64 totalRegs;
    uint64 totalValues;
    uint64 magicNumber;
    }tAtDeviceTestRunnerHwAccessDb;

typedef struct tAtDeviceTestRunnerMethods
    {
    AtModuleTestRunnerFactory (*ModuleRunnerFactory)(AtDeviceTestRunner self);
    void (*TestInterruptProcessing)(AtDeviceTestRunner self);
    eBool (*ShouldTestInterruptProcessing)(AtDeviceTestRunner self);
    eBool (*ModuleMustBeSupported)(AtDeviceTestRunner self, eAtModule module);
    eBool (*ShouldTestModule)(AtDeviceTestRunner self, eAtModule moduleId);
    eBool (*ModuleNotReady)(AtDeviceTestRunner self, eAtModule moduleId);
    AtUnittestRunner (*SerdesManagerTestRunnerCreate)(AtDeviceTestRunner self, AtSerdesManager manager);
    eBool (*ShouldTestAsyncInit)(AtDeviceTestRunner self);
    uint32 (*VersionTestAsyncInit)(AtDeviceTestRunner self);
    uint32 (*BuiltNumberTestAsyncInit)(AtDeviceTestRunner self);
    }tAtDeviceTestRunnerMethods;

typedef struct tAtDeviceTestRunner
    {
    tAtObjectTestRunner super;
    const tAtDeviceTestRunnerMethods *methods;

    /* Private data */
    tAtDeviceTestRunnerHwAccessDb hwAccessDb;
    }tAtDeviceTestRunner;

typedef struct tAtDeviceTestDefaultRunner
    {
    tAtDeviceTestRunner super;
    }tAtDeviceTestDefaultRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDeviceTestRunner AtDeviceTestRunnerObjectInit(AtDeviceTestRunner self, AtDevice device);

#endif /* _ATDEVICETESTRUNNERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtObjectTestRunnerInternal.h
 * 
 * Created Date: Feb 20, 2016
 *
 * Description : Test runner for AtObject
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOBJECTTESTRUNNERINTERNAL_H_
#define _ATOBJECTTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../runner/AtUnittestRunnerInternal.h"
#include "AtObjectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtObjectTestRunnerMethods
    {
    eBool (*ShouldTestFullSerializing)(AtObjectTestRunner self);
    }tAtObjectTestRunnerMethods;

typedef struct tAtObjectTestRunner
    {
    tAtUnittestRunner super;
    const tAtObjectTestRunnerMethods *methods;

    /* Private data */
    AtObject object;
    }tAtObjectTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtObjectTestRunner AtObjectTestRunnerObjectInit(AtObjectTestRunner self, AtObject object);

#ifdef __cplusplus
}
#endif
#endif /* _ATOBJECTTESTRUNNERINTERNAL_H_ */


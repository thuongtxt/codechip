/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtObjectTestRunner.c
 *
 * Created Date: Feb 20, 2016
 *
 * Description : Test runner for AtObject
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtObjectTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtObjectTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtObject TestedObject(void)
    {
    return AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testNoProblemWhenDeletingNullObject(void)
    {
    AtObjectDelete(NULL);
    }

static void testClone(void)
    {
    /*AtObject newObject = AtObjectClone(TestedObject());
    AtObjectDelete(newObject);*/
    }

static void testMustHaveObjectDescription(void)
    {
    TEST_ASSERT_NOT_NULL(AtObjectToString(TestedObject()));
    TEST_ASSERT_NULL(AtObjectToString(NULL));
    }

static void testSerializing(void)
    {
    AtCoder coder = AtDefaultCoderNew();
    AtObjectTestRunner self = (AtObjectTestRunner)AtUnittestRunnerCurrentRunner();

    /* Make serializing be silent */
    AtDefaultCoderOutputFileSet((AtDefaultCoder)coder, NULL);

    /* Just can check if operations are working with no issues. Cannot expect anything. */
    if (mMethodsGet(self)->ShouldTestFullSerializing(self))
        {
        AtObjectSerialize(TestedObject(), coder);
        AtObjectDeserialize(TestedObject(), coder);
        }

    /* Test invalid operation */
    AtObjectSerialize(NULL, coder);
    AtObjectDeserialize(NULL, coder);
    AtObjectSerialize(TestedObject(), NULL);
    AtObjectDeserialize(TestedObject(), NULL);

    AtCoderDelete(coder);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtObject_Fixtures)
        {
        new_TestFixture("testNoProblemWhenDeletingNullObject", testNoProblemWhenDeletingNullObject),
        new_TestFixture("testClone", testClone),
        new_TestFixture("testMustHaveObjectDescription", testMustHaveObjectDescription),
        new_TestFixture("testSerializing", testSerializing)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtObject_Caller, "TestSuite_AtObject", NULL, NULL, TestSuite_AtObject_Fixtures);

    return (TestRef)((void *)&TestSuite_AtObject_Caller);
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtObject object = AtObjectTestRunnerObjectGet(mThis(self));
    return (char *)AtObjectToString(object);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static eBool ShouldTestFullSerializing(AtObjectTestRunner self)
    {
    return cAtTrue;
    }

static void MethodsInit(AtObjectTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ShouldTestFullSerializing);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtObjectTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtObjectTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtObjectTestRunner);
    }

AtObjectTestRunner AtObjectTestRunnerObjectInit(AtObjectTestRunner self, AtObject object)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit((AtUnittestRunner)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    self->object = object;

    return self;
    }

AtObject AtObjectTestRunnerObjectGet(AtObjectTestRunner self)
    {
    return self ? self->object : NULL;
    }

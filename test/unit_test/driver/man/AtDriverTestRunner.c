/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtDriverTestRunner.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : Driver test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriverTestRunner.h"
#include "AtObjectTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDriverTestRunner
    {
    tAtObjectTestRunner super;
    }tAtDriverTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void testCreateDriver()
    {
    uint8 numAddedDevices;
    TEST_ASSERT_NOT_NULL(TestedDriver());

    /* Check its number of devices */
    AtDriverAddedDevicesGet(TestedDriver(), &numAddedDevices);
    TEST_ASSERT(AtDriverMaxNumberOfDevicesGet(TestedDriver()) > 0);
    TEST_ASSERT_EQUAL_INT(numAddedDevices, 0);
    }

static void testCannotCreateDriverWithNumberOfDevicesIsZero()
    {
    TEST_ASSERT_NULL(AtDriverCreate(0, TestedOsal()));
    }

static void testCannotCreateDriverWithNullOsal()
    {
    TEST_ASSERT_NULL(AtDriverCreate(1, NULL));
    }

/* Driver can be deleted */
static void testDeleteDriver()
    {
    AtDriverCreate(1, TestedOsal());
    AtDriverDelete(AtDriverSharedDriverGet());
    }

static uint32 CorrectProductCode(uint32 productCode)
    {
    if (AtTestIsSimulationTesting())
        productCode = AppSimulationProductCode(productCode);
    return productCode;
    }

/* Add a device with product code */
static void testAddDeviceWithSpecifiedProductCode()
    {
    uint8 product_i, numDevices, numAddedDevices;
    uint8 numProducts = 0;
    uint8 numTestedProducts;
    const uint32 *productsToBeTested = AtTestProductsToBeTested(&numProducts);
    AtDevice device;

    AtAssert(numProducts > 0);

    numTestedProducts = numProducts;
    if (AtTestQuickTestIsEnabled())
        numTestedProducts = AtTestRandomNumChannels(numProducts);

    /* Create all of supported devices */
    numDevices = 0;
    for (product_i = 0; product_i < numTestedProducts; product_i++)
        {
        uint32 productCode;
        uint32 productIndex = product_i;

        if (AtTestQuickTestIsEnabled())
            productIndex = AtTestRandom(numProducts, productIndex);

        productCode = CorrectProductCode(productsToBeTested[productIndex]);
        device = AtDriverDeviceCreate(TestedDriver(), productCode);
        TEST_ASSERT_NOT_NULL(device);
        TEST_ASSERT_EQUAL_INT(AtDeviceProductCodeGet(device), productCode);
        TEST_ASSERT_NOT_NULL(AtDriverAddedDevicesGet(TestedDriver(), &numAddedDevices));
        TEST_ASSERT_EQUAL_INT(numAddedDevices, numDevices + 1);

        numDevices = numDevices + 1;

        /* Check if this device belong to this driver */
        TEST_ASSERT(AtDeviceDriverGet(device) == TestedDriver());
        }
    }

static void testCannotAddDeviceWithInvalidCode()
    {
    TEST_ASSERT_NULL(AtDriverDeviceCreate(TestedDriver(), 0xffff));
    }

static void testDeviceIterator()
    {
    uint8 i, numDevicesToAdd = 0;
    AtDevice device;
    AtIterator iterator;
    const uint32 *supportedProducts = AtTestSupportedProducts(&numDevicesToAdd);

    /* Create all of supported devices */
    for (i = 0; i < numDevicesToAdd; i++)
        device = AtDriverDeviceCreate(TestedDriver(), CorrectProductCode(supportedProducts[i]));

    /* Create Iterator */
    iterator = AtDriverDeviceIteratorCreate(TestedDriver());
    TEST_ASSERT_NOT_NULL(iterator);
    TEST_ASSERT_EQUAL_INT(numDevicesToAdd, AtIteratorCount(iterator));

    i = 0;
    while((device = (AtDevice)AtIteratorNext(iterator)) != NULL)
        {
        TEST_ASSERT_NOT_NULL(device);
        TEST_ASSERT_EQUAL_INT(AtDeviceProductCodeGet(device), CorrectProductCode(supportedProducts[i++]));

        /* Check if this device belong to this driver */
        TEST_ASSERT(AtDeviceDriverGet(device) == TestedDriver());
        }

    AtObjectDelete((AtObject)iterator);
    }

/* Delete a device */
static void testDeleteDevice()
    {
    uint8 i, exptRemainDevices, remainDevices;
    AtDriver driver = TestedDriver();
    uint8 numProducts;
    const uint32 *supportedProducts;

    /* Create all of supported devices */
    supportedProducts = AtTestSupportedProducts(&numProducts);
    for (i = 0; i < numProducts; i++)
        AtDriverDeviceCreate(driver, CorrectProductCode(supportedProducts[i]));

    /* Delete them */
    AtDriverAddedDevicesGet(driver, &exptRemainDevices);
    while (exptRemainDevices > 0)
        {
        AtDriverDeviceDelete(driver, AtDriverAddedDevicesGet(driver, NULL)[0]);
        TEST_ASSERT_NOT_NULL(AtDriverAddedDevicesGet(driver, &remainDevices));
        TEST_ASSERT_EQUAL_INT(remainDevices, exptRemainDevices - 1);
        AtDriverAddedDevicesGet(driver, &exptRemainDevices);
        }
    }

static void testGetOsal()
    {
    TEST_ASSERT_NOT_NULL(AtDriverOsalGet(TestedDriver()));
    }

static void testCannotCreateMoreThanOneDrivers()
    {
    TEST_ASSERT_NOT_NULL(TestedDriver());
    TEST_ASSERT_NULL(AtDriverCreate(1, TestedOsal()));
    }

static void TestSuite_DriverManagement_TearDown()
    {
    Cleanup();
    }

/* Test suite: Driver management */
TestRef TestSuite_DriverManagement(void)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_DriverManagement_Fixtures)
        {
        new_TestFixture("testCreateDriver", testCreateDriver),
        new_TestFixture("testCannotCreateMoreThanOneDrivers", testCannotCreateMoreThanOneDrivers),
        new_TestFixture("testCannotCreateDriverWithNumberOfDevicesIsZero", testCannotCreateDriverWithNumberOfDevicesIsZero),
        new_TestFixture("testCannotCreateDriverWithNullOsal", testCannotCreateDriverWithNullOsal),
        new_TestFixture("testDeleteDriver", testDeleteDriver),
        new_TestFixture("testGetOsal", testGetOsal),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_DriverManagement_Caller, "TestSuite_DriverManagement", NULL, TestSuite_DriverManagement_TearDown, TestSuite_DriverManagement_Fixtures);

    return (TestRef)((void *)&TestSuite_DriverManagement_Caller);
    }

static void TestSuite_DeviceManagement_Setup()
    {
    TEST_ASSERT_NOT_NULL(TestedDriver());
    }

static void TestSuite_DeviceManagement_TearDown()
    {
    Cleanup();
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_DeviceManagement_Fixtures)
        {
        new_TestFixture("testAddDeviceWithSpecifiedProductCode", testAddDeviceWithSpecifiedProductCode),
        new_TestFixture("testDeleteDevice", testDeleteDevice),
        new_TestFixture("testCannotAddDeviceWithInvalidCode", testCannotAddDeviceWithInvalidCode),
        new_TestFixture("testDeviceIterator", testDeviceIterator)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_DeviceManagement_Caller, "TestSuite_DeviceManagement", TestSuite_DeviceManagement_Setup, TestSuite_DeviceManagement_TearDown, TestSuite_DeviceManagement_Fixtures);

    return (TestRef)((void *)&TestSuite_DeviceManagement_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "Driver");
    return buffer;
    }

static void OverrideAtUnittestRunner(AtDriverTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(runner);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtDriverTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDriverTestRunner);
    }

AtDriverTestRunner AtDriverTestRunnerObjectInit(AtDriverTestRunner self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)AtDriverSharedDriverGet()) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDriverTestRunner AtDriverTestRunnerNew()
    {
    AtDriverTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtDriverTestRunnerObjectInit(newRunner);
    }

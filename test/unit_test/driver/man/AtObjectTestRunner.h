/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtObjectTestRunner.h
 * 
 * Created Date: Feb 20, 2016
 *
 * Description : Test runner for AtObject
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOBJECTTESTRUNNER_H_
#define _ATOBJECTTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../runner/AtUnittestRunner.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtObjectTestRunner * AtObjectTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtObject AtObjectTestRunnerObjectGet(AtObjectTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATOBJECTTESTRUNNER_H_ */


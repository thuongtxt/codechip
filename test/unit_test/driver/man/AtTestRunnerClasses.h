/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : AtTestRunnerClasses.h
 * 
 * Created Date: May 15, 2016
 *
 * Description : Common place to define runner classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTESTRUNNERCLASSES_H_
#define _ATTESTRUNNERCLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDeviceTestRunner * AtDeviceTestRunner;
typedef struct tAtModuleTestRunner * AtModuleTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATTESTRUNNERCLASSES_H_ */


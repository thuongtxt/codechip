/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtPwConstrainerTestRunner.h
 * 
 * Created Date: Feb 19, 2016
 *
 * Description : PW Constrainer test runner definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCONSTRAINERTESTRUNNER_H_
#define _ATPWCONSTRAINERTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/device_runner/AtDeviceTestRunner.h"
#include "../runner/AtUnittestRunnerInternal.h"
#include "AtModulePwTestRunner.h"
#include "AtPwConstrainer.h"
#include "AtModuleEth.h"
#include "AtPwPsn.h"
#include "AtPw.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwConstrainerTestRunner * AtPwConstrainerTestRunner;

typedef struct tAtPwConstrainerTestRunnerMethods
    {
    uint32 (*NumPwToTest)(AtPwConstrainerTestRunner self);
    }tAtPwConstrainerTestRunnerMethods;

typedef struct tAtPwConstrainerTestRunner
    {
    tAtUnittestRunner super;
    const tAtPwConstrainerTestRunnerMethods * methods;

    AtPwConstrainer constrainer;
    AtModulePwTestRunner modulePwTestRunner;
    uint32 currentPw;

    }tAtPwConstrainerTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner AtPwConstrainerTestRunnerObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);

AtUnittestRunner AtPwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);
AtUnittestRunner AtPwConstrainerMultiplePwsTestRunnerNew(AtPwConstrainerTestRunner runner, uint32 numPw, const char* runCase);
AtUnittestRunner Af60210031PwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);
AtUnittestRunner Af60210012PwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer);

AtDevice AtPwConstrainerTestRunnerDeviceGet(AtPwConstrainerTestRunner self);
AtPwConstrainer AtPwConstrainerTestRunnerConstrainerGet(AtPwConstrainerTestRunner self);
uint32 AtPwConstrainerTestRunnerNextPw(AtPwConstrainerTestRunner self);
void AtPwConstrainerTestRunnerPwNormalSetup(AtPwConstrainerTestRunner self, AtPw pw, AtChannel circuit, AtEthPort ethPort, uint32_t circuitRate);
void AtPwConstrainerTestRunnerInit(AtPwConstrainerTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCONSTRAINERTESTRUNNER_H_ */


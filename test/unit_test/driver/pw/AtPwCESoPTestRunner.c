/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwCESoPTestRunner.c
 *
 * Created Date: Jul 10, 2014
 *
 * Description : CEP test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwTestRunnerInternal.h"
#include "AtPw.h"
#include "AtPdhNxDs0.h"
#include "AtPwCESoP.h"

/*--------------------------- Define -----------------------------------------*/
#define cStatusCheckingTimeoutMs 1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwCESoPTestRunner
    {
    tAtPwTestRunner super;
    }tAtPwCESoPTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw TestedPw()
    {
    return (AtPw)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(TestedPw());
    }

static void testCannotChangePayloadSizeUnderMinimumPayloadSize()
    {
    AtChannel circuit = AtPwBoundCircuitGet(TestedPw());
    uint32 jitterBuffer = AtPwJitterBufferSizeGet(TestedPw());
    uint16 minPayloadSize = AtPwMinPayloadSize(TestedPw(), circuit, jitterBuffer);
    uint32 currentPayloadSize = AtPwPayloadSizeGet(TestedPw());

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwPayloadSizeSet(TestedPw(), minPayloadSize - 1) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPayloadSizeSet(TestedPw(), minPayloadSize));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPayloadSizeSet(TestedPw(), currentPayloadSize));
    }

static void testChangeCircuitTimingMode()
    {
    eAtTimingMode timingModes[] = {cAtTimingModeAcr, cAtTimingModeDcr};
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPwBoundCircuitGet(TestedPw());
    AtChannel de1 = (AtChannel)AtPdhNxDS0De1Get(nxDs0);
    AtChannel timingSource = (AtChannel)TestedPw();
    uint8 i;

    for (i = 0; i < mCount(timingModes); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtChannelTimingSet(de1, timingModes[i], timingSource));
        TEST_ASSERT_EQUAL_INT(timingModes[i], AtChannelTimingModeGet(de1));
        TEST_ASSERT(AtChannelTimingSourceGet(de1) == timingSource);
        }
    }

static uint16 PayloadSizes(AtPw self, uint16 **pldSize)
    {
    static uint16 normpldSizes[] = {6, 8, 10};
    static uint16 caspldSizes[] = {8};
    uint16 numElement = 0;
    if (AtPwCESoPModeGet((AtPwCESoP)self) == cAtPwCESoPModeWithCas)
        {
        *pldSize = caspldSizes;
        numElement = mCount(caspldSizes);
        }
    else
        {
        *pldSize = normpldSizes;
        numElement = mCount(normpldSizes);
        }

    return numElement;
    }

static uint16 PayloadSizeToUnit(AtPw self, uint16 payloadSizeInFrames)
    {
    uint16 bytes = payloadSizeInFrames;
    if (AtModulePwCesopPayloadSizeInByteIsEnabled((AtModulePw)AtChannelModuleGet((AtChannel)self)))
        {
        AtChannel boundCircuit = AtPwBoundCircuitGet(TestedPw());
        uint8 numTimeslot = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)boundCircuit);
        bytes = payloadSizeInFrames * numTimeslot;
        }

    return bytes;
    }

static void testCanChangePayloadSize()
    {
    uint16 *pldSizes = NULL;
    uint16 numOfSizeToTest = PayloadSizes(TestedPw(), &pldSizes);
    uint32 i, minjitterBuffer, jitterBuffer;
    uint32 minJitterDelay, jitterDelay;

    for (i = 0; i < numOfSizeToTest; i++)
        {
        uint16 pldSize_i = PayloadSizeToUnit(TestedPw(), pldSizes[i]);
        minjitterBuffer = AtPwTestMinJitterBufferSizeInUs(pldSize_i);
        minJitterDelay = AtPwTestMinJitterDelayInUs(pldSize_i);
        jitterBuffer = AtPwJitterBufferSizeGet(TestedPw());
        jitterDelay = AtPwJitterBufferDelayGet(TestedPw());

        if (minJitterDelay > jitterDelay)
            {
            jitterDelay = minJitterDelay;
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), jitterDelay));
            }

        if (minjitterBuffer > jitterBuffer)
            {
            jitterBuffer = minjitterBuffer;
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), jitterBuffer));
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPayloadSizeSet(TestedPw(), pldSize_i));
        TEST_ASSERT_EQUAL_INT(pldSize_i, AtPwPayloadSizeGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

static void testCanChangePayloadSizeWhenNoCircuit()
    {
    uint16 *pldSizes = NULL;
    uint16 numOfSizeToTest = PayloadSizes(TestedPw(), &pldSizes);

    uint16 i, minjitterBuffer, jitterBuffer;
    AtChannel boundCircuit = AtPwBoundCircuitGet(TestedPw());

    for (i = 0; i < numOfSizeToTest; i++)
        {
        uint16 pldSize_i = PayloadSizeToUnit(TestedPw(), pldSizes[i]);
        minjitterBuffer = AtPwTestMinJitterBufferSizeInUs(pldSize_i);
        jitterBuffer = AtPwJitterBufferSizeGet(TestedPw());

        if (minjitterBuffer > jitterBuffer)
            {
            jitterBuffer = minjitterBuffer;
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), jitterBuffer));
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), jitterBuffer / 2));
            }

        /* Unbind and apply new configuration */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPayloadSizeSet(TestedPw(), pldSize_i));
        TEST_ASSERT_EQUAL_INT(pldSize_i, AtPwPayloadSizeGet(TestedPw()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitUnbind(TestedPw()));

        /* After binding, this configuration must remain unchanged */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitBind(TestedPw(), boundCircuit));
        TEST_ASSERT_EQUAL_INT(pldSize_i, AtPwPayloadSizeGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwCESoP_Fixtures)
        {
        new_TestFixture("testCannotChangePayloadSizeUnderMinimumPayloadSize", testCannotChangePayloadSizeUnderMinimumPayloadSize),
        new_TestFixture("testChangeCircuitTimingMode", testChangeCircuitTimingMode),
        new_TestFixture("testCanChangePayloadSize", testCanChangePayloadSize),
        new_TestFixture("testCanChangePayloadSizeWhenNoCircuit", testCanChangePayloadSizeWhenNoCircuit)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwCESoP_Caller, "TestSuite_AtPwCESoP", setUp, NULL, TestSuite_AtPwCESoP_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwCESoP_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwCESoPTestRunner);
    }

AtChannelTestRunner AtPwCESoPTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPwCESoPTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPwCESoPTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Applycation
 *
 * File        : AtPwConstrainerMaxMinTestRunner.c
 *
 * Created Date: Feb 18, 2016
 *
 * Description : PW Constrainer MAX/MIN test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwConstrainerTestRunner.h"
#include "AtPdhNxDs0.h"
#include "AtModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwConstrainerMultiplePwsTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwConstrainerMultiplePwsTestRunner * AtPwConstrainerMultiplePwsTestRunner;
typedef struct tAtPwConstrainerMultiplePwsTestRunner
    {
    tAtUnittestRunner super;
    AtPwConstrainerTestRunner constrainerRunner;
    uint32 numPw;
    const char* runCase;
    }tAtPwConstrainerMultiplePwsTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device()
    {
    AtPwConstrainerMultiplePwsTestRunner currentRunner = (AtPwConstrainerMultiplePwsTestRunner)AtUnittestRunnerCurrentRunner();
    return AtPwConstrainerTestRunnerDeviceGet((AtPwConstrainerTestRunner)(currentRunner->constrainerRunner));
    }

static AtModulePw ModulePw()
    {
    return (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    }

static AtModuleEth ModuleEth()
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
    }

static AtPwConstrainer Constrainer()
    {
    AtPwConstrainerMultiplePwsTestRunner currentRunner = (AtPwConstrainerMultiplePwsTestRunner)AtUnittestRunnerCurrentRunner();
    return AtPwConstrainerTestRunnerConstrainerGet((AtPwConstrainerTestRunner)(currentRunner->constrainerRunner));
    }

static AtModulePwTestRunner ModulePwTestRunner()
    {
    AtPwConstrainerMultiplePwsTestRunner currentRunner = (AtPwConstrainerMultiplePwsTestRunner)AtUnittestRunnerCurrentRunner();
    AtPwConstrainerTestRunner testRunner = currentRunner->constrainerRunner;
    return testRunner->modulePwTestRunner;
    }

static void Setup()
    {
    }

static void TearDown()
    {
    }

static uint32 NumPw()
    {
    AtPwConstrainerMultiplePwsTestRunner currentRunner = (AtPwConstrainerMultiplePwsTestRunner)AtUnittestRunnerCurrentRunner();
    return currentRunner->numPw;
    }

static eAtPwConstrainerPwType PwTypeToConstrainerType(eAtPwType pwType)
    {
    if (pwType == cAtPwTypeSAToP) return cAtPwConstrainerPwTypeSAToP;
    if (pwType == cAtPwTypeCEP)   return cAtPwConstrainerPwTypeCEP;
    if (pwType == cAtPwTypeCESoP) return cAtPwConstrainerPwTypeCESoP;

    return cAtPwConstrainerPwTypeInvalid;
    }

static void testMaxMinPayloadAfterSetup()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        uint32 pwBufferSize = AtPwJitterBufferSizeGet(pw);
        uint32_t constrainerBufferSize = AtPwConstrainerJitterBufferSizeGet(constrainer, pwId);
        AtChannel circuit = AtPwBoundCircuitGet(pw);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, constrainerBufferSize), AtPwMaxPayloadSize(pw, circuit, pwBufferSize));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, constrainerBufferSize), AtPwMinPayloadSize(pw, circuit, pwBufferSize));
        }
    }

static void testMaxMinJitterBufferAfterSetup()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        uint32 pwPldSize = AtPwPayloadSizeGet(pw);
        uint32_t constrainerPldSize = AtPwConstrainerPayloadSizeGet(constrainer, pwId);
        AtChannel circuit = AtPwBoundCircuitGet(pw);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, constrainerPldSize), AtPwMaxJitterBufferSize(pw, circuit, pwPldSize));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, constrainerPldSize), AtPwMinJitterBufferSize(pw, circuit, pwPldSize));
        }
    }

static uint32 BufferSizeToChange(AtPw pw)
    {
    uint32 pwPldSize = AtPwPayloadSizeGet(pw);
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint32 sizeToChange = AtPwMaxJitterBufferSize(pw, circuit, pwPldSize);
    if (sizeToChange != AtPwJitterBufferSizeGet(pw))
        return sizeToChange;

    return AtPwMinJitterBufferSize(pw, circuit, pwPldSize);
    }

static void testMaxMinPayloadAfterChangeBufferSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtChannel circuit = AtPwBoundCircuitGet(pw);
        uint32 changeSize = BufferSizeToChange(pw);
        uint32 currentBufferSize = AtPwJitterBufferSizeGet(pw);

        TEST_ASSERT_NOT_NULL(pw);

        /* Change buffer size to maximum number */
        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferSizeSet(pw, changeSize), cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, pwId, changeSize);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, changeSize), AtPwMaxPayloadSize(pw, circuit, changeSize));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, changeSize), AtPwMinPayloadSize(pw, circuit, changeSize));

        /* Restore previous buffer size */
        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferSizeSet(pw, currentBufferSize), cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, pwId, currentBufferSize);
        }
    }

static uint32 PayloadSizeToChange(AtPw pw)
    {
    uint32 pwBufferSize = AtPwJitterBufferSizeGet(pw);
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint32 sizeToChange = AtPwMaxPayloadSize(pw, circuit, pwBufferSize);
    if (sizeToChange != AtPwPayloadSizeGet(pw))
        return sizeToChange;

    return AtPwMinPayloadSize(pw, circuit, pwBufferSize);
    }

static void testMaxMinJitterBufferAfterChangePayloadSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtChannel circuit = AtPwBoundCircuitGet(pw);
        uint16 sizeToChange = PayloadSizeToChange(pw);
        uint16 currentPld = AtPwPayloadSizeGet(pw);

        TEST_ASSERT_NOT_NULL(pw);

        /* Change to min payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(pw, sizeToChange), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pwId, sizeToChange);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, sizeToChange), AtPwMaxJitterBufferSize(pw, circuit, sizeToChange));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, sizeToChange), AtPwMinJitterBufferSize(pw, circuit, sizeToChange));

        /* Restore previous payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(pw, currentPld), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pwId, currentPld);
        }
    }

static void testQueueBandwidthAfterChangePayloadSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();
    AtModuleEth moduleEth = ModuleEth();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        uint16 pldToChange = PayloadSizeToChange(pw);
        uint16 currentPld = AtPwPayloadSizeGet(pw);
        uint32 ethPortId = AtPwConstrainerPwEthPortGet(constrainer, pwId);
        uint32 queueId = AtPwConstrainerPwQueueGet(constrainer, pwId);
        uint32_t currentQueueBw = AtPwConstrainerEthPortQueueBandwidthInBpsGet(constrainer, ethPortId, queueId);

        if (currentQueueBw == 0)
            continue;

        TEST_ASSERT_NOT_NULL(pw);

        /* Change to min payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(pw, pldToChange), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pwId, pldToChange);

        TEST_ASSERT(AtPwConstrainerEthPortQueueBandwidthInBpsGet(constrainer, ethPortId, queueId) != currentQueueBw);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortQueueBandwidthInBpsGet(constrainer, ethPortId, queueId), AtEthPortQueueCurrentBandwidthInBpsGet(AtModuleEthPortGet(moduleEth, ethPortId), queueId));

        /* Restore previous payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(pw, currentPld), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pwId, currentPld);
        }
    }

static void testEthPortBandwidthAfterChangePayloadSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();
    AtModuleEth moduleEth = ModuleEth();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        uint16 pldToChange = PayloadSizeToChange(pw);
        uint16 currentPld = AtPwPayloadSizeGet(pw);
        uint32 ethPortId = AtPwConstrainerPwEthPortGet(constrainer, pwId);
        AtEthPort ethPort = AtModuleEthPortGet(moduleEth, ethPortId);
        uint32_t currentPortBw = AtPwConstrainerEthPortBandwidthInKbpsGet(constrainer, ethPortId);

        TEST_ASSERT_NOT_NULL(pw);

        /* Change to max payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(pw, pldToChange), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pwId, pldToChange);

        TEST_ASSERT(AtPwConstrainerEthPortBandwidthInKbpsGet(constrainer, ethPortId) != currentPortBw);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortBandwidthInKbpsGet(constrainer, ethPortId), AtEthPortProvisionedBandwidthInKbpsGet(ethPort));

        /* Restore previous payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(pw, currentPld), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pwId, currentPld);
        }
    }

static void testMaxMinPayloadSizeAfterAnotherPwChangePayloadSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        uint32 pw = ((pwId + 1) == NumPw()) ? 0 : (pwId + 1);
        AtPw testingPw = AtModulePwGetPw(ModulePw(), pwId);
        AtPw changingPw = AtModulePwGetPw(ModulePw(), pw);
        uint32 pldToChange = PayloadSizeToChange(changingPw);
        uint16 currentPld = AtPwPayloadSizeGet(changingPw);

        TEST_ASSERT_NOT_NULL(testingPw);
        TEST_ASSERT_NOT_NULL(changingPw);
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(changingPw, pldToChange), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pw, pldToChange);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(testingPw, AtPwBoundCircuitGet(testingPw), AtPwJitterBufferSizeGet(testingPw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(testingPw, AtPwBoundCircuitGet(testingPw), AtPwJitterBufferSizeGet(testingPw)));

        /* Restore previous payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(changingPw, currentPld), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, pw, currentPld);
        }
    }

static void testMaxMinPayloadSizeAfterAnotherPwChangeBufferSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        uint32 changingPwId = ((pwId + 1) == NumPw()) ? 0 : (pwId + 1);
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtPw changingPw = AtModulePwGetPw(ModulePw(), changingPwId);
        uint32 sizeToChange = BufferSizeToChange(changingPw);
        uint32 currentBuffer = AtPwJitterBufferSizeGet(changingPw);
        uint32 currentDelay  = AtPwJitterBufferDelayGet(changingPw);
        uint32 payloadSize   = AtPwPayloadSizeGet(changingPw);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_NOT_NULL(changingPw);
        if (sizeToChange < currentDelay)
            {
            TEST_ASSERT_EQUAL_INT(AtPwJitterBufferDelaySet(changingPw, sizeToChange / 2), cAtOk);
            }

        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferSizeSet(changingPw, sizeToChange), cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, changingPwId, sizeToChange);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        /* Restore previous buffer size */
        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferAndPayloadSizeSet(changingPw, currentBuffer, currentDelay, payloadSize), cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, changingPwId, currentBuffer);
        }
    }

static void testMaxMinJitterBufferAfterAnotherChangeBufferSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        uint32 changingPwId = ((pwId + 1) == NumPw()) ? 0 : (pwId + 1);
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtPw changingPw = AtModulePwGetPw(ModulePw(), changingPwId);
        uint32 sizeToChange = BufferSizeToChange(changingPw);
        uint32 currentBuffer = AtPwJitterBufferSizeGet(changingPw);
        uint32 currentDelay  = AtPwJitterBufferDelayGet(changingPw);
        uint32 payloadSize   = AtPwPayloadSizeGet(changingPw);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_NOT_NULL(changingPw);
        if (sizeToChange < currentDelay)
            {
            TEST_ASSERT_EQUAL_INT(AtPwJitterBufferDelaySet(changingPw, sizeToChange / 2), cAtOk);
            }

        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferSizeSet(changingPw, sizeToChange), cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, changingPwId, sizeToChange);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMaxJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMinJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        /* Restore previous buffer size */
        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferAndPayloadSizeSet(changingPw, currentBuffer, currentDelay, payloadSize), cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, changingPwId, currentBuffer);
        }
    }

static void testMaxMinJitterBufferAfterAnotherChangePayloadSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        uint32 changingPwId = ((pwId + 1) == NumPw()) ? 0 : (pwId + 1);
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtPw changingPw = AtModulePwGetPw(ModulePw(), changingPwId);
        uint32 sizeToChange = PayloadSizeToChange(changingPw);
        uint16 currentPld = AtPwPayloadSizeGet(changingPw);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_NOT_NULL(changingPw);
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(changingPw, sizeToChange), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, changingPwId, sizeToChange);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMaxJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMinJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        /* Restore previous payload size */
        TEST_ASSERT_EQUAL_INT(AtPwPayloadSizeSet(changingPw, currentPld), cAtOk);
        AtPwConstrainerPayloadSizeSet(constrainer, changingPwId, currentPld);
        }
    }

static void testMaxMinPayloadSizeAfterChangeEthHeader()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        tAtEthVlanTag cVlan;
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        uint32_t currentHeadLength = AtPwConstrainerPwEthHeaderLengthGet(constrainer, pwId);

        TEST_ASSERT_NOT_NULL(pw);

        AtEthVlanTagConstruct(1,1,1, &cVlan);
        TEST_ASSERT_EQUAL_INT(AtPwEthVlanSet(pw, &cVlan, NULL), cAtOk);
        AtPwConstrainerPwEthHeaderLengthSet(constrainer, pwId, currentHeadLength + 4);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        /* Restore */
        TEST_ASSERT_EQUAL_INT(AtPwEthVlanSet(pw, NULL, NULL), cAtOk);
        AtPwConstrainerPwEthHeaderLengthSet(constrainer, pwId, currentHeadLength);
        }
    }

static void testMaxMinPayloadSizeAfterChangePsn()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtPwPsn currentPsn = (AtPwPsn)AtObjectClone((AtObject)AtPwPsnGet(pw));
        AtPwPsn udpIpV4Psn = (AtPwPsn)AtPwUdpPsnNew();
        AtPwPsnLowerPsnSet(udpIpV4Psn, (AtPwPsn)AtPwIpV4PsnNew());

        TEST_ASSERT_NOT_NULL(pw);
        AtPwUdpPsnExpectedPortSet((AtPwUdpPsn)udpIpV4Psn, cBit15_0);
        TEST_ASSERT_EQUAL_INT(AtPwPsnSet(pw, udpIpV4Psn), cAtOk);
        AtPwConstrainerPwPsnLengthSet(constrainer, pwId, AtPwPsnLengthInBytes(udpIpV4Psn));
        AtObjectDelete((AtObject)udpIpV4Psn);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwPsnSet(pw, currentPsn), cAtOk);
        AtPwConstrainerPwPsnLengthSet(constrainer, pwId, AtPwPsnLengthInBytes(currentPsn));
        AtObjectDelete((AtObject)currentPsn);
        }
    }

static void testMaxMinPayloadSizeAfterChangeRtp()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        uint8_t rtpIsEnabled = AtPwConstrainerPwRtpIsEnabled(constrainer, pwId);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_EQUAL_INT(AtPwRtpEnable(pw, (rtpIsEnabled) ? cAtFalse : cAtTrue), cAtOk);
        AtPwConstrainerPwRtpEnable(constrainer, pwId, (rtpIsEnabled) ? 0 : 1);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        /* Restore */
        TEST_ASSERT_EQUAL_INT(AtPwRtpEnable(pw, (rtpIsEnabled) ? cAtTrue : cAtFalse), cAtOk);
        AtPwConstrainerPwRtpEnable(constrainer, pwId, (rtpIsEnabled) ? 1 : 0);
        }
    }

static uint32 PwEthHeaderLengthGet(AtPw pw)
    {
    uint32 ethHeaderLength = 14;
    tAtEthVlanTag vlan;
    if (AtPwEthCVlanGet(pw, &vlan))
        ethHeaderLength += 4;
    if (AtPwEthSVlanGet(pw, &vlan))
        ethHeaderLength += 4;

    return ethHeaderLength;
    }

static uint32 PwPortQueueGet(AtPw pw)
    {
    uint8 queue = AtPwEthPortQueueGet(pw);
    if (queue == cAtInvalidPwEthPortQueueId)
        return cAtInvalidUint32;

    return queue;
    }

static void testConstrainerCachedInfoAfterSetup()
    {
    extern uint32 AtChannelDataRateInBytesPer125Us(AtChannel self);
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);

        TEST_ASSERT_NOT_NULL(pw);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwTypeGet(constrainer, pwId), PwTypeToConstrainerType(AtPwTypeGet(pw)));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerCircuitRateGet(constrainer, pwId), AtChannelDataRateInBytesPer125Us(AtPwBoundCircuitGet(pw)));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPayloadSizeGet(constrainer, pwId), AtPwPayloadSizeGet(pw));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerJitterBufferSizeGet(constrainer, pwId), AtPwJitterBufferSizeGet(pw));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthHeaderLengthGet(constrainer, pwId), PwEthHeaderLengthGet(pw));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwPsnLengthGet(constrainer, pwId), AtPwPsnLengthInBytes(AtPwPsnGet(pw)));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthPortGet(constrainer, pwId), AtChannelIdGet((AtChannel)AtPwEthPortGet(pw)));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwRtpIsEnabled(constrainer, pwId), mBoolToBin(AtPwRtpIsEnabled(pw)));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwLopsThresholdGet(constrainer, pwId), AtPwLopsSetThresholdGet(pw));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwQueueGet(constrainer, pwId), PwPortQueueGet(pw));
        }
    }

static void testEthPortBandwidthMustBeSame()
    {
    AtModuleEth moduleEth = ModuleEth();
    uint8 numPort = AtModuleEthMaxPortsGet(moduleEth);
    uint8 port_i;
    AtPwConstrainer constrainer = Constrainer();
    AtModulePwTestRunner moduleTesterRunner = ModulePwTestRunner();

    for (port_i = 0; port_i < numPort; port_i++)
        {
        AtEthPort port;

        if (!mMethodsGet(moduleTesterRunner)->ShouldTestOnPort(moduleTesterRunner, port_i))
            continue;

        port = AtModuleEthPortGet(moduleEth, port_i);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortBandwidthInKbpsGet(constrainer, port_i), AtEthPortProvisionedBandwidthInKbpsGet(port));
        }
    }

static void testEthQueueBandwidthMustBeSame()
    {
    AtModuleEth moduleEth = ModuleEth();
    uint8 numPort = AtModuleEthMaxPortsGet(moduleEth);
    uint8 port_i;
    uint32 queue_i;
    AtPwConstrainer constrainer = Constrainer();
    AtModulePwTestRunner moduleTesterRunner = ModulePwTestRunner();

    for (port_i = 0; port_i < numPort; port_i++)
        {
        AtEthPort port;

        if (!mMethodsGet(moduleTesterRunner)->ShouldTestOnPort(moduleTesterRunner, port_i))
            continue;

        port = AtModuleEthPortGet(moduleEth, port_i);
        for (queue_i = 0; queue_i < AtEthPortMaxQueuesGet(port); queue_i++)
            TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortQueueBandwidthInBpsGet(constrainer, port_i, queue_i), AtEthPortQueueCurrentBandwidthInBpsGet(port, queue_i));
        }
    }

static void testMaxMinPayloadAfterPayloadAndBufferChange()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer();

    for (pwId = 0; pwId < NumPw(); pwId++)
        {
        AtPw pw = AtModulePwGetPw(ModulePw(), pwId);
        AtChannel circuit = AtPwBoundCircuitGet(pw);
        AtEthPort ethPort = AtPwEthPortGet(pw);

        uint16 currentPld = AtPwPayloadSizeGet(pw);
        uint32 currentBufferSize = AtPwJitterBufferSizeGet(pw);
        uint32 currentBufferDelay = AtPwJitterBufferDelayGet(pw);

        uint16 pldSizeToChange = AtPwMaxPayloadSize(pw, circuit, currentBufferSize);
        uint32 bufferSizeToChange = AtPwMaxJitterBufferSize(pw, circuit, currentPld);

        TEST_ASSERT_NOT_NULL(pw);

        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferAndPayloadSizeSet(pw, bufferSizeToChange, bufferSizeToChange/2, pldSizeToChange), cAtOk);
        AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, bufferSizeToChange, pldSizeToChange);

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, pldSizeToChange), AtPwMaxJitterBufferSize(pw, circuit, pldSizeToChange));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, pldSizeToChange), AtPwMinJitterBufferSize(pw, circuit, pldSizeToChange));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, bufferSizeToChange), AtPwMaxPayloadSize(pw, circuit, bufferSizeToChange));
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, bufferSizeToChange), AtPwMinPayloadSize(pw, circuit, bufferSizeToChange));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortBandwidthInKbpsGet(constrainer, AtChannelIdGet((AtChannel)ethPort)), AtEthPortProvisionedBandwidthInKbpsGet(ethPort));

        /* Restore previous payload size */
        TEST_ASSERT_EQUAL_INT(AtPwJitterBufferAndPayloadSizeSet(pw, currentBufferSize, currentBufferDelay, currentPld), cAtOk);
        AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, currentBufferSize, currentPld);
        }
    }

static TestRef SinglePwUnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwConstrainer_SinglePw_Fixtures)
        {
        new_TestFixture("testConstrainerCachedInfoAfterSetup", testConstrainerCachedInfoAfterSetup),
        new_TestFixture("testMaxMinPayloadAfterSetup", testMaxMinPayloadAfterSetup),
        new_TestFixture("testMaxMinJitterBufferAfterSetup", testMaxMinJitterBufferAfterSetup),
        new_TestFixture("testMaxMinPayloadAfterChangeBufferSize", testMaxMinPayloadAfterChangeBufferSize),
        new_TestFixture("testMaxMinPayloadSizeAfterChangeEthHeader", testMaxMinPayloadSizeAfterChangeEthHeader),
        new_TestFixture("testMaxMinPayloadSizeAfterChangePsn", testMaxMinPayloadSizeAfterChangePsn),
        new_TestFixture("testMaxMinPayloadSizeAfterChangeRtp", testMaxMinPayloadSizeAfterChangeRtp),
        new_TestFixture("testMaxMinJitterBufferAfterChangePayloadSize", testMaxMinJitterBufferAfterChangePayloadSize),
        new_TestFixture("testEthPortBandwidthAfterChangePayloadSize", testEthPortBandwidthAfterChangePayloadSize),
        new_TestFixture("testQueueBandwidthAfterChangePayloadSize", testQueueBandwidthAfterChangePayloadSize),
        new_TestFixture("testMaxMinPayloadAfterPayloadAndBufferChange", testMaxMinPayloadAfterPayloadAndBufferChange),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwConstrainer_SinglePw_Caller, "TestSuite_AtPwConstrainer_SinglePw", Setup, TearDown, TestSuite_AtPwConstrainer_SinglePw_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwConstrainer_SinglePw_Caller);
    }

static TestRef MultiplePwsUnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwConstrainer_MultiplePws_Fixtures)
        {
        new_TestFixture("testEthPortBandwidthMustBeSame", testEthPortBandwidthMustBeSame),
        new_TestFixture("testEthQueueBandwidthMustBeSame", testEthQueueBandwidthMustBeSame),
        new_TestFixture("testMaxMinPayloadSizeAfterAnotherPwChangePayloadSize", testMaxMinPayloadSizeAfterAnotherPwChangePayloadSize),
        new_TestFixture("testMaxMinPayloadSizeAfterAnotherPwChangeBufferSize", testMaxMinPayloadSizeAfterAnotherPwChangeBufferSize),
        new_TestFixture("testMaxMinJitterBufferAfterAnotherChangeBufferSize", testMaxMinJitterBufferAfterAnotherChangeBufferSize),
        new_TestFixture("testMaxMinJitterBufferAfterAnotherChangePayloadSize", testMaxMinJitterBufferAfterAnotherChangePayloadSize),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwConstrainer_MultiplePws_Caller, "TestSuite_AtPwConstrainer_MultiplePws", Setup, TearDown, TestSuite_AtPwConstrainer_MultiplePws_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwConstrainer_MultiplePws_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)SinglePwUnitTestSuite(self));
    AtListObjectAdd(suites, (AtObject)MultiplePwsUnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "PW Constrainer %s, %u pws", mThis(self)->runCase, mThis(self)->numPw);
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)m_AtUnittestRunnerMethods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwConstrainerMultiplePwsTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtPwConstrainerTestRunner runner, uint32 numPw, const char* runCase)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit((AtUnittestRunner)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->constrainerRunner = runner;
    mThis(self)->numPw = numPw;
    mThis(self)->runCase = runCase;

    return self;
    }

AtUnittestRunner AtPwConstrainerMultiplePwsTestRunnerNew(AtPwConstrainerTestRunner runner, uint32 numPw, const char* runCase)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return ObjectInit(newRunner, runner, numPw, runCase);
    }

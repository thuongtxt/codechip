/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtModulePwTestRunner.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : PW unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtPdhNxDs0.h"
#include "AtEthPort.h"
#include "AtPwPsn.h"
#include "AtPw.h"
#include "AtSdhLine.h"
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"
#include "../sdh/AtModuleSdhTests.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModulePwTestRunnerInternal.h"
#include "AtPwGroupTestRunner.h"
#include "AtPwConstrainerTestRunner.h"
#include "../interrupt/AtChannelInterruptTestRunner.h"
#include "../../../../driver/src/generic/sdh/AtSdhChannelInternal.h"
#include "../encap/AtModuleEncapTestRunner.h"
#include "AtPwCESoP.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModulePwTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModulePwTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort EthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    uint8 ethPortId = 0;

    if (AtDeviceModuleIsSupported(device, cAtModuleSdh))
        {
        AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcGet((AtPdhChannel)channel);
        ethPortId = (AtSdhChannelLineGet(vc) < 4) ? 0 : 1;
        }

    /* Assume that PDH device always support 1 ethernet port */
    return AtModuleEthPortGet(ethModule, ethPortId);
    }

static void TimingModeSet(AtModulePwTestRunner self, uint8 timingMd)
    {
    self->currentTimingMode = timingMd;
    }

static uint8 TimingModeGet(AtModulePwTestRunner self)
    {
    return self->currentTimingMode;
    }

static AtModulePw Module()
    {
    return (AtModulePw)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static uint8 *DefaultMac()
    {
    static uint8 mac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    return mac;
    }

static uint8 *DefaultSMAC()
    {
    return DefaultMac();
    }

static uint8 *DefaultDMAC()
    {
    return DefaultMac();
    }

static eAtRet EthLoopback(AtModulePwTestRunner self, AtEthPort ethPort)
    {
    return AtChannelLoopbackSet((AtChannel)ethPort, cAtLoopbackModeLocal);
    }

static eAtRet EthFlowSetup(AtModulePwTestRunner self, AtPw pw)
    {
    /* We do not need Ethernet flow setup in default. */
    return cAtOk;
    }

static eAtRet EthernetSetup(AtModulePwTestRunner self, AtPw pw, AtEthPort ethPort)
    {
    eAtRet ret = cAtOk;

    ret |= AtEthPortSourceMacAddressSet(ethPort, DefaultSMAC());
    ret |= mMethodsGet(self)->EthFlowSetup(self, pw);
    ret |= AtPwEthPortSet(pw, ethPort);
    ret |= AtPwEthHeaderSet(pw, DefaultDMAC(), NULL, NULL);
    ret |= mMethodsGet(self)->EthLoopback(self, ethPort);

    return ret;
    }

static eAtRet PsnSetup(AtPw pw)
    {
    AtPwMplsPsn mpls = AtPwMplsPsnNew();
    tAtPwMplsLabel label;
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    eAtRet ret;

    AtPwMplsPsnInnerLabelSet(mpls, AtPwMplsLabelMake(pwId + 1, pwId % 8, pwId % 256, &label));
    AtPwMplsPsnExpectedLabelSet(mpls, pwId + 1);
    ret = AtPwPsnSet(pw, (AtPwPsn)mpls);
    AtObjectDelete((AtObject)mpls);

    return ret;
    }

static eBool ShouldTestInterrupt(AtModulePwTestRunner self)
    {
    if (AtTestIsSimulationTesting())
        return cAtFalse;

    return cAtTrue;
    }

static AtDevice Device(void)
    {
    return AtModuleTestRunnerDeviceGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void PwNormalSetup(AtModulePwTestRunner self, AtPw pw, AtChannel circuit, AtEthPort ethPort)
    {
    eAtRet ret = cAtOk;
    uint8 timingMd = TimingModeGet(self);
    eAtPwType pwType = AtPwTypeGet(pw);

    ret = EthernetSetup(self, pw, ethPort);
    AtAssert(ret == cAtOk);

    ret = PsnSetup(pw);
    AtAssert(ret == cAtOk);

    ret = AtPwCircuitBind(pw, circuit);
    AtAssert(ret == cAtOk);

    if (pwType == cAtPwTypeCESoP)
        circuit = (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);

    if (timingMd == cAtTimingModeSys)
        ret |= AtChannelTimingSet(circuit, TimingModeGet(self), circuit);
    else if ((timingMd == cAtTimingModeAcr) || (timingMd == cAtTimingModeDcr))
        ret |= AtChannelTimingSet(circuit, TimingModeGet(self), (AtChannel)pw);

    AtAssert(ret == cAtOk);

    ret |= AtChannelEnable((AtChannel)pw, cAtTrue);

    /* Make sure that all seem fine */
    if ((ret != cAtErrorNotApplicable) && (ret != cAtErrorModeNotSupport))
        AtAssert(ret == cAtOk);
    AtAssert(AtPwEthPortGet(pw));
    AtAssert(AtPwBoundCircuitGet(pw));
    AtAssert(AtPwPsnGet(pw));
    }

static void PwNormalTeardown(AtPw pw)
    {
    eAtRet ret = AtPwCircuitUnbind(pw);
    AtAssert(ret == cAtOk);
    }

static AtModuleTestRunner Runner(void)
    {
    return (AtModuleTestRunner)AtUnittestRunnerCurrentRunner();
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(Module());
    AtModuleInit((AtModule)Module());
    }

static void tearDown()
    {
    }

/* Maximum number of PWs must be greater than 0 */
static void testMaxNumberOfPwsGreaterZero()
    {
    TEST_ASSERT(AtModulePwMaxPwsGet(Module()) > 0);
    }

/* Maximum number of PWs must be 0 for a NULL PW module */
static void testMaxNumberOfPwsNullModuleIsZero()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwMaxPwsGet(NULL) == 0);
    AtTestLoggerEnable(cAtTrue);
    }

/* Cannot delete a PW which has not been created */
static void testNoProblemWhenDeleteNoneExistPw()
    {
    uint16 maxPws, i;
    maxPws = AtModulePwMaxPwsGet(Module());
    for (i = 0; i < maxPws; i++)
        {
        if (AtModulePwGetPw(Module(), i) == NULL)
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePwDeletePw(Module(), i));
            }
        }
    }

static void testCannotGetOutOfRangePw()
    {
    uint16 maxPws;
    maxPws = AtModulePwMaxPwsGet(Module());
    TEST_ASSERT_NULL(AtModulePwGetPw(Module(), maxPws));
    }

static void testNumberOfApsGroupMustBeEnough(void)
    {
    AtModulePwTestRunner self = (AtModulePwTestRunner)Runner();
    uint32 numGroups = mMethodsGet(self)->MaxNumApsGroups(self);
    TEST_ASSERT_EQUAL_INT(numGroups, AtModulePwMaxApsGroupsGet(Module()));
    }

static void testNumberOfPwsMustBeEnough(void)
    {
    AtModulePwTestRunner self = (AtModulePwTestRunner)Runner();
    uint32 numPws = mMethodsGet(self)->MaxNumPws(self);
    TEST_ASSERT_EQUAL_INT(numPws, AtModulePwMaxPwsGet(Module()));
    }

static void testNumberOfHsGroupMustBeEnough(void)
    {
    AtModulePwTestRunner self = (AtModulePwTestRunner)Runner();
    uint32 numGroups = mMethodsGet(self)->MaxNumHsGroups(self);
    TEST_ASSERT_EQUAL_INT(numGroups, AtModulePwMaxHsGroupsGet(Module()));
    }

static void TestCreateHsGroup(uint32 groupId)
    {
    AtModulePw module = Module();
    AtPwGroup group = AtModulePwHsGroupCreate(module, groupId);
    TEST_ASSERT(group);
    TEST_ASSERT(AtModulePwHsGroupGet(module, groupId) == group);
    }

static void TestCreateApsGroup(uint32 groupId)
    {
    AtModulePw module = Module();
    AtPwGroup group = AtModulePwApsGroupCreate(module, groupId);
    TEST_ASSERT(group);
    TEST_ASSERT(AtModulePwApsGroupGet(module, groupId) == group);
    }

static void TestDeleteHsGroup(uint32 groupId)
    {
    AtModulePw module = Module();
    AtPwGroup group = AtModulePwHsGroupCreate(module, groupId);
    TEST_ASSERT(group);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePwHsGroupDelete(module, groupId));
    TEST_ASSERT(AtModulePwHsGroupGet(module, groupId) == NULL);
    }

static void TestDeleteApsGroup(uint32 groupId)
    {
    AtModulePw module = Module();
    AtPwGroup group = AtModulePwApsGroupCreate(module, groupId);
    TEST_ASSERT(group);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePwApsGroupDelete(module, groupId));
    TEST_ASSERT(AtModulePwApsGroupGet(module, groupId) == NULL);
    }

static void testCanCreateApsGroup()
    {
    uint32 numGroups = AtModulePwMaxApsGroupsGet(Module());
    TestCreateApsGroup(0);
    TestCreateApsGroup(numGroups / 2);
    TestCreateApsGroup(numGroups - 1);
    }

static void testCannotCreateInvalidApsGroup()
    {
    AtModulePw module = Module();
    TEST_ASSERT(AtModulePwApsGroupCreate(module, AtModulePwMaxApsGroupsGet(module)) == NULL);
    }

static void testCannotCreateApsGroupWithNullModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwApsGroupCreate(NULL, 0) == NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDeleteApsGroup()
    {
    uint32 numGroups = AtModulePwMaxApsGroupsGet(Module());
    TestDeleteApsGroup(0);
    TestDeleteApsGroup(numGroups / 2);
    TestDeleteApsGroup(numGroups - 1);
    }

static void testCannotDeleteInvalidApsGroup()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwApsGroupDelete(Module(), AtModulePwMaxApsGroupsGet(Module())) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotDeleteApsGroupOfNullModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwApsGroupDelete(NULL, 0) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testDeleteApsGroupWhichHasNotBeenCreated()
    {
    TEST_ASSERT_NULL(AtModulePwApsGroupGet(Module(), 0)); /* To make sure that group has not been created */
    TEST_ASSERT(AtModulePwApsGroupDelete(Module(), 0) == cAtOk);
    TEST_ASSERT_NULL(AtModulePwApsGroupGet(Module(), 1));
    TEST_ASSERT(AtModulePwApsGroupDelete(Module(), 1) == cAtOk);
    }

static void testCanCreateHsGroup()
    {
    uint32 numGroups = AtModulePwMaxHsGroupsGet(Module());
    TestCreateHsGroup(0);
    TestCreateHsGroup(numGroups / 2);
    TestCreateHsGroup(numGroups - 1);
    }

static void testCannotCreateInvalidHsGroup()
    {
    AtModulePw module = Module();
    TEST_ASSERT(AtModulePwHsGroupCreate(module, AtModulePwMaxHsGroupsGet(module)) == NULL);
    }

static void testCannotCreateHsGroupWithNullModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwHsGroupCreate(NULL, 0)  == NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanDeleteHsGroup()
    {
    uint32 numGroups = AtModulePwMaxHsGroupsGet(Module());
    TestDeleteHsGroup(0);
    TestDeleteHsGroup(numGroups / 2);
    TestDeleteHsGroup(numGroups - 1);
    }

static void testCannotDeleteInvalidHsGroup()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwHsGroupDelete(Module(), AtModulePwMaxHsGroupsGet(Module())) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotDeleteHsGroupOfNullModule()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtModulePwHsGroupDelete(NULL, 0)  != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testDeleteHsGroupWhichHasNotBeenCreated()
    {
    TEST_ASSERT_NULL(AtModulePwHsGroupGet(Module(), 0)); /* To make sure that group has not been created */
    TEST_ASSERT(AtModulePwHsGroupDelete(Module(), 0) == cAtOk);
    TEST_ASSERT_NULL(AtModulePwHsGroupGet(Module(), 1));
    TEST_ASSERT(AtModulePwHsGroupDelete(Module(), 1) == cAtOk);
    }

static AtModuleSdh SdhModule(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static AtModulePw PwModule(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eBool CepCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if ((rate == cAtSdhLineRateStm1) ||
        (rate == cAtSdhLineRateStm16))
        return cAtTrue;

    if (rate == cAtSdhLineRateStm4)
        return (AtChannelIdGet((AtChannel)line) == 0) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 NextPw(AtModulePwTestRunner self)
    {
    uint32 nextPw = self->currentPw;

    /* Increase for next time */
    self->currentPw = (self->currentPw + 1) % AtModulePwMaxPwsGet(Module());
    return nextPw;
    }

static void ResetPwId(AtModulePwTestRunner self)
    {
    self->currentPw = 0;
    }

static uint32 TimeslotMake(uint8 startTimeslot, uint8 numTimeslots)
    {
    uint32 mask = 0;
    uint8 i, numRemainedSlots;

    numRemainedSlots = 32 - startTimeslot;
    if (numTimeslots > numRemainedSlots)
        numTimeslots = numRemainedSlots;

    if (numTimeslots == 0)
        return 0;

    for (i = 0; i < numTimeslots; i++)
        mask |= (cBit0 << (startTimeslot + i));

    return mask;
    }

static void DeletePw(AtModulePwTestRunner self, uint32 pwId)
    {
    AtPwCircuitUnbind(AtModulePwGetPw(PwModule(self), pwId));
    AtModulePwDeletePw(PwModule(self), pwId);
    }

static AtUnittestRunner CreatePwTestRunner(AtModulePwTestRunner self, AtPw pw)
    {
    return (AtUnittestRunner)AtPwTestRunnerNew((AtChannel)pw, (AtModuleTestRunner)self);
    }

static AtUnittestRunner CreateCepPwTestRunner(AtModulePwTestRunner self, AtPw pw)
    {
    return (AtUnittestRunner)AtPwTestRunnerNew((AtChannel)pw, (AtModuleTestRunner)self);
    }

static AtUnittestRunner CreatePwInterruptTestRunner(AtModulePwTestRunner self, AtPw pw)
    {
    return AtPwInterruptTestRunnerNew((AtChannel)pw);
    }

static AtUnittestRunner CreateCESoPTestRunner(AtModulePwTestRunner self, AtPw pw)
    {
    return (AtUnittestRunner)AtPwCESoPTestRunnerNew((AtChannel)pw, (AtModuleTestRunner)self);
    }

static void E1CESoPTest(AtModulePwTestRunner self, AtPdhDe1 de1)
    {
    static const uint8 cNumTimeslotPerNxDs0 = 10;
    uint8 timeslot;

    /* Toggle framing to delete all of NxDS0s which may exist before */
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm) == cAtOk);
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm) == cAtOk);
    AtAssert(AtChannelLoopbackSet((AtChannel)de1, cAtPdhLoopbackModeLocalLine) == cAtOk);

    for (timeslot = 1; timeslot < 31; timeslot += cNumTimeslotPerNxDs0)
        {
        AtPdhNxDS0 nxDs0 = AtPdhDe1NxDs0Create(de1, TimeslotMake(timeslot, cNumTimeslotPerNxDs0));
        uint32 pwId = NextPw(self);
        AtPw pw;
        AtUnittestRunner runner;

        if (pwId == AtModulePwMaxPwsGet(PwModule(self)))
            {
            ResetPwId(self);
            pwId = NextPw(self);
            }

        pw = (AtPw)AtModulePwCESoPCreate(PwModule(self), pwId, cAtPwCESoPModeBasic);
        AtAssert(pw);
        AtAssert(nxDs0 != NULL);
        PwNormalSetup(self, pw, (AtChannel)nxDs0, mMethodsGet(self)->EthPortForPdhChannel(self, (AtPdhChannel)de1));
        if (AtModulePwCesopPayloadSizeInByteIsEnabled(PwModule(self)))
            {
            AtAssert(AtPwPayloadSizeSet(pw, 8*AtPdhNxDs0NumTimeslotsGet(nxDs0)) == cAtOk);
            }
        else
            {
            AtAssert(AtPwPayloadSizeSet(pw, 8) == cAtOk);
            }

        runner = CreateCESoPTestRunner(self, pw);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        /* Delete PW after testing to test all PW successfully */
        PwNormalTeardown(pw);
        DeletePw(self, pwId);
        AtAssert(AtPdhDe1NxDs0Delete(de1, nxDs0) == cAtOk);
        }
    }

static uint8 NextTimeslot(uint8 timeslot)
    {
    static const uint8 cTimeslot16ForSignaling = 16;

    timeslot++;
    if (timeslot == cTimeslot16ForSignaling)
        timeslot++;

    return timeslot;
    }

static uint32 TimeslotMakeForCESoPwCAS(uint8 startTimeslot, uint8 numTimeslots, uint8 *nextTimeslot)
    {
    uint32 mask = 0;
    uint8 i, numRemainedSlots;

    numRemainedSlots = 32 - startTimeslot;
    if (numTimeslots > numRemainedSlots)
        numTimeslots = numRemainedSlots;

    if (numTimeslots == 0)
        return 0;

    for (i = 0; i < numTimeslots; i++)
        {
        mask |= (cBit0 << startTimeslot);
        startTimeslot = NextTimeslot(startTimeslot);
        }

    *nextTimeslot = startTimeslot;

    return mask;
    }

static void E1CESoPwithCASTest(AtModulePwTestRunner self, AtPdhDe1 de1)
    {
    static const uint8 cNumTimeslotPerNxDs0 = 10;
    uint8 timeslot, abcd1 = 0x5, abcd2 = 0x7;
    uint32 signalingDs0Mask = (cBit15_1 | cBit31_17);
    if (!AtPdhDe1SignalingIsSupported(de1))
        return;

    /* Toggle framing to delete all of NxDS0s which may exist before */
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm) == cAtOk);
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm) == cAtOk);
    AtAssert(AtChannelLoopbackSet((AtChannel)de1, cAtPdhLoopbackModeLocalLine) == cAtOk);
    AtAssert(AtPdhDe1SignalingDs0MaskSet(de1, signalingDs0Mask) == cAtOk);
    AtAssert(AtPdhDe1SignalingEnable(de1, cAtTrue) == cAtOk);

    for (timeslot = 1; timeslot < 31; timeslot = NextTimeslot(timeslot))
        {
        AtAssert(AtPdhDe1SignalingPatternSet(de1, timeslot, abcd1) == cAtOk);
        AtAssert(AtPdhDe1SignalingPatternGet(de1, timeslot) == abcd1);
        }

    for (timeslot = 1; timeslot < 31; )
        {
        AtPdhNxDS0 nxDs0 = AtPdhDe1NxDs0Create(de1, TimeslotMakeForCESoPwCAS(timeslot, cNumTimeslotPerNxDs0, &timeslot));
        uint32 pwId = NextPw(self);
        AtPw pw;
        AtUnittestRunner runner;
        AtPwCESoP cesop = NULL;

        if (pwId == AtModulePwMaxPwsGet(PwModule(self)))
            {
            ResetPwId(self);
            pwId = NextPw(self);
            }

        pw = (AtPw)AtModulePwCESoPCreate(PwModule(self), pwId, cAtPwCESoPModeWithCas);
        AtAssert(pw);
        AtAssert(nxDs0 != NULL);
        PwNormalSetup(self, pw, (AtChannel)nxDs0, mMethodsGet(self)->EthPortForPdhChannel(self, (AtPdhChannel)de1));
        if (AtModulePwCesopPayloadSizeInByteIsEnabled(PwModule(self)))
            {
            AtAssert(AtPwPayloadSizeSet(pw, 8*AtPdhNxDs0NumTimeslotsGet(nxDs0)) == cAtOk);
            }
        else
            {
            AtAssert(AtPwPayloadSizeSet(pw, 8) == cAtOk);
            }

        cesop = (AtPwCESoP)pw;
        AtAssert(AtPwCESoPCasIdleCode1Set(cesop, abcd1) == cAtOk);
        AtAssert(AtPwCESoPCasIdleCode2Set(cesop, abcd2) == cAtOk);
        AtAssert(AtPwCESoPCasIdleCode1Get(cesop) == abcd1);
        AtAssert(AtPwCESoPCasIdleCode2Get(cesop) == abcd2);
        AtAssert(AtPwCESoPCasAutoIdleEnable(cesop, cAtFalse) == cAtOk);
        AtAssert(AtPwCESoPCasAutoIdleIsEnabled(cesop) == cAtFalse);
        AtAssert(AtPwCESoPCasAutoIdleEnable(cesop, cAtTrue) == cAtOk);
        AtAssert(AtPwCESoPCasAutoIdleIsEnabled(cesop) == cAtTrue);

        runner = CreateCESoPTestRunner(self, pw);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        /* Delete PW after testing to test all PW successfully */
        PwNormalTeardown(pw);
        DeletePw(self, pwId);
        AtAssert(AtPdhDe1NxDs0Delete(de1, nxDs0) == cAtOk);
        }

    AtAssert(AtPdhDe1SignalingEnable(de1, cAtFalse) == cAtOk);
    AtAssert(AtPdhDe1SignalingIsEnabled(de1) == cAtFalse);
    AtAssert(AtPdhDe1SignalingDs0MaskSet(de1, 0) == cAtOk);
    AtAssert(AtPdhDe1SignalingDs0MaskGet(de1) == 0);
    }

static void Initialize(AtModulePwTestRunner self)
    {
    AtAssert(AtDeviceInit(AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self)) == cAtOk);
    ResetPwId(self);
    }

static void PdhCESoPTest(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint16 de1Id;
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(pdhModule);

    if (numDe1s == 0)
        return;

    Initialize(self);
    for (de1Id = 0; de1Id < numDe1s; de1Id++)
        {
        E1CESoPTest(self, AtModulePdhDe1Get(pdhModule, de1Id));
        E1CESoPwithCASTest(self, AtModulePdhDe1Get(pdhModule, de1Id));
        }
    }

static void CESoPTestInAllSubPathsOfChannel(AtModulePwTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 numTestedChannels = numSubChannels;
    uint8 i;

    if (AtTestQuickTestIsEnabled())
        numTestedChannels = AtTestRandomNumChannels(numSubChannels);

    for (i = 0; i < numTestedChannels; i++)
        {
        uint8 channelIndex = i;
        AtSdhChannel subChannel;

        if (AtTestQuickTestIsEnabled())
            channelIndex = AtTestRandom(numSubChannels, channelIndex);

        subChannel = AtSdhChannelSubChannelGet(channel, channelIndex);

        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapDe1);
            E1CESoPTest(self, (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
            E1CESoPwithCASTest(self, (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
            }

        /* Otherwise, test all of its sub channels */
        else
            CESoPTestInAllSubPathsOfChannel(self, subChannel, pathType);
        }
    }

static void CESoPSdhRun(AtModulePwTestRunner self,
                        uint8 lineId,
                        eAtSdhChannelType channelType,
                        void (*CreateFunc)(AtSdhLine line, eAtSdhChannelType vc1xType))
    {
    AtSdhLine line;

    /* Initialize device have clean status */
    Initialize(self);
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    if (!mMethodsGet(self)->CepCanTestOnLine(self, line))
        return;

    mMethodsGet(self)->WillTestStmLine(self, lineId);
    CreateFunc(line, channelType);
    CESoPTestInAllSubPathsOfChannel(self, (AtSdhChannel)line, channelType);
    }

static eBool ShouldTestLine(AtModulePwTestRunner self, uint8 lineId)
    {
    return cAtTrue;
    }

static void StmAu3CESoPTest(AtModulePwTestRunner self, uint8 lineId)
    {
    Initialize(self);
    CESoPSdhRun(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInVc3s);
    }

static void StmTug3CESoPTest(AtModulePwTestRunner self, uint8 lineId)
    {
    AtSdhLine line;

    /* STM-0 does not have any VC-4s */
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return;

    Initialize(self);
    CESoPSdhRun(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInTug3s);
    }

static uint8 *AllTestedLinesAllocate(AtModulePwTestRunner self, uint8 *numLines)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return AtModuleSdhAllApplicableLinesAllocate(sdhModule, numLines);
    }

static uint8 *AllLines(AtModulePwTestRunner self, uint8 *numLines)
    {
    if (self->allLines == NULL)
        self->allLines = mMethodsGet(self)->AllTestedLinesAllocate(self, &(self->numLines));

    if (numLines)
        *numLines = self->numLines;

    return self->allLines;
    }

static void StmCESoPTest(AtModulePwTestRunner self)
    {
    uint8 numLines, *testedLines;
    uint8 line_i;

    if (SdhModule(self) == NULL)
        return;

    testedLines = AllLines(self, &numLines);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        uint8 lineId = testedLines[line_i];

        if (!mMethodsGet(self)->ShouldTestLine(self, lineId))
            continue;

        StmTug3CESoPTest(self, lineId);
        StmAu3CESoPTest(self, lineId);
        }
    }

static eBool CEPCanBeTested(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    if (AtDeviceModuleGet(device, cAtModulePw) &&
        AtDeviceModuleGet(device, cAtModuleSdh))
        return cAtTrue;

    return cAtFalse;
    }

static AtEthPort EthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    uint8 ethPortId = (AtSdhChannelLineGet(channel) < 4) ? 0 : 1;
    return AtModuleEthPortGet(ethModule, ethPortId);
    }

static void CEPHoVcRun(AtModulePwTestRunner self, uint8 lineId, uint16 (*VcCreateFunc)(AtSdhLine line, AtSdhChannel *channels))
    {
    uint16 numChannels, channel_i;
    AtSdhLine line;
    uint32 numTestedChannels;

    /* Initialize device have clean status */
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    if (!mMethodsGet(self)->CepCanTestOnLine(self, line))
        return;

    mMethodsGet(self)->WillTestStmLine(self, lineId);

    numChannels = VcCreateFunc(line, AtSdhTestSharedChannelList(NULL));
    numTestedChannels = numChannels;

    if (AtTestQuickTestIsEnabled())
        numTestedChannels = AtTestRandomNumChannels(numChannels);

    AtAssert(numTestedChannels);

    /* And test all of them */
    for (channel_i = 0; channel_i < numTestedChannels; channel_i++)
        {
        AtSdhChannel channel;
        AtUnittestRunner runner;
        AtUnittestRunner interruptRunner;
        uint32 testedIndex = channel_i;
        AtPw pw;

        if (AtTestQuickTestIsEnabled())
            testedIndex = AtTestRandom(numChannels, testedIndex);

        /* Create PW */
        pw = (AtPw)AtModulePwCepCreate(PwModule(self), NextPw(self), cAtPwCepModeBasic);
        AtAssert(pw);
        channel = AtSdhTestSharedChannelList(NULL)[testedIndex];
        PwNormalSetup(self, pw, (AtChannel)channel, mMethodsGet(self)->EthPortForSdhChannel(self, channel));

        runner = mMethodsGet(self)->CreateCepPwTestRunner(self, pw);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);

        if (ShouldTestInterrupt(self) == cAtTrue)
            {
            AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
            AtPrbsEngine engine = AtModulePrbsSdhVcPrbsEngineCreate(prbsModule, 0, channel);
            AtPrbsEngineEnable(engine, cAtTrue);

            interruptRunner = (AtUnittestRunner)CreatePwInterruptTestRunner(self, pw);
            AtUnittestRunnerRun(interruptRunner);
            AtUnittestRunnerDelete(interruptRunner);
            AtModulePrbsEngineDelete((AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs), 0);
            }

        PwNormalTeardown(pw);
        }
    }

static void CEPTestAllSubPathsOfChannel(AtModulePwTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 numTestedSubChannels = numSubChannels;
    AtSdhLine line;
    uint8 channel_i;

    line = AtSdhChannelLineObjectGet(channel);
    if (!mMethodsGet(self)->CepCanTestOnLine(self, line))
        return;

    if (AtTestQuickTestIsEnabled())
        numTestedSubChannels = AtTestRandomNumChannels(numSubChannels);

    AtAssert(numTestedSubChannels);

    for (channel_i = 0; channel_i < numTestedSubChannels; channel_i++)
        {
        AtSdhChannel subChannel;
        uint8 testIndex = channel_i;

        if (AtTestQuickTestIsEnabled())
            testIndex = AtTestRandom(numSubChannels, testIndex);

        subChannel = AtSdhChannelSubChannelGet(channel, testIndex);

        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            AtUnittestRunner runner;

            /* Create PW */
            AtPw pw = (AtPw)AtModulePwCepCreate(PwModule(self), NextPw(self), cAtPwCepModeBasic);
            AtAssert(pw);

            /* Make sure that this VC1x does not map DS1/E1 */
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapC1x));

            PwNormalSetup(self, pw, (AtChannel)subChannel, mMethodsGet(self)->EthPortForSdhChannel(self, subChannel));

            runner = mMethodsGet(self)->CreateCepPwTestRunner(self, pw);
            AtUnittestRunnerRun(runner);
            AtUnittestRunnerDelete(runner);
            PwNormalTeardown(pw);
            }

        /* Otherwise, test all of its sub channels */
        else
            CEPTestAllSubPathsOfChannel(self, subChannel, pathType);
        }
    }

static void CEPLoVcRun(AtModulePwTestRunner self,
                       uint8 lineId,
                       eAtSdhChannelType channelType,
                       void (*CreateFunc)(AtSdhLine line, eAtSdhChannelType vc1xType))
    {
    AtSdhLine line;

    /* Initialize device have clean status */
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    mMethodsGet(self)->WillTestStmLine(self, lineId);

    CreateFunc(line, channelType);

    CEPTestAllSubPathsOfChannel(self, (AtSdhChannel)line, channelType);
    }

static eBool CEPCanTestVc11(AtModulePwTestRunner self)
    {
    return cAtFalse;
    }

static eBool SAToPCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if ((rate == cAtSdhLineRateStm1) ||
        (rate == cAtSdhLineRateStm16))
        return cAtTrue;

    if (rate == cAtSdhLineRateStm4)
        return (AtChannelIdGet((AtChannel)line) == 0) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static void SAToPTestOnPdhChannel(AtModulePwTestRunner self, AtPdhChannel chanel)
    {
    AtUnittestRunner runner;
    AtPw pw = (AtPw)AtModulePwSAToPCreate(PwModule(self), NextPw(self));
    AtAssert(pw);

    PwNormalSetup(self, pw, (AtChannel)chanel, mMethodsGet(self)->EthPortForPdhChannel(self, chanel));

    runner = CreatePwTestRunner(self, pw);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);

    if (ShouldTestInterrupt(self) == cAtTrue)
        {
        AtUnittestRunner interruptRunner;
        AtPrbsEngine engine = NULL;
        AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
        if ((AtPdhChannelTypeGet(chanel) == cAtPdhChannelTypeE1) || (AtPdhChannelTypeGet(chanel) == cAtPdhChannelTypeDs1))
            engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, 0, (AtPdhDe1)chanel);

        if (engine != NULL)
            {
            AtPrbsEngineEnable(engine, cAtTrue);
            interruptRunner = (AtUnittestRunner)CreatePwInterruptTestRunner(self, pw);
            AtUnittestRunnerRun(interruptRunner);
            AtUnittestRunnerDelete(interruptRunner);
            AtModulePrbsEngineDelete((AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs), 0);
            }
        }

    PwNormalTeardown(pw);
    }

static void SAToPTestE1(AtModulePwTestRunner self, AtPdhDe1 de1)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm))
    SAToPTestOnPdhChannel(self, (AtPdhChannel)de1);
    }

static void SAToPTestDe3(AtModulePwTestRunner self, AtPdhDe3 de3)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhDs3Unfrm));
    SAToPTestOnPdhChannel(self, (AtPdhChannel)de3);
    }

static void StmSAToPDe1TestAllSubPathsOfChannel(AtModulePwTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 numTestedChannels = numSubChannels;
    uint8 channel_i;

    if (AtTestQuickTestIsEnabled())
        numTestedChannels = AtTestRandomNumChannels(numSubChannels);

    for (channel_i = 0; channel_i < numTestedChannels; channel_i++)
        {
        uint8 channelIndex = channel_i;
        AtSdhChannel subChannel;

        if (AtTestQuickTestIsEnabled())
            channelIndex = AtTestRandom(numSubChannels, channelIndex);

        subChannel = AtSdhChannelSubChannelGet(channel, channelIndex);
        AtAssert(subChannel);
        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapDe1);
            SAToPTestE1(self, (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
            }

        /* Otherwise, test all of its sub channels */
        else
            StmSAToPDe1TestAllSubPathsOfChannel(self, subChannel, pathType);
        }
    }

static void StmDe1SAToPSdhRun(AtModulePwTestRunner self,
                           uint8 lineId,
                           eAtSdhChannelType channelType,
                           void (*CreateFunc)(AtSdhLine line, eAtSdhChannelType vc1xType))
    {
    AtSdhLine line;

    /* Initialize device have clean status */
    Initialize(self);
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    if (!mMethodsGet(self)->SAToPCanTestOnLine(self, line))
        {
        AtPrintc(cSevWarning, "WARNING: Ignore SAToP testing on line %d\r\n", lineId + 1);
        return;
        }

    mMethodsGet(self)->WillTestStmLine(self, lineId);
    CreateFunc(line, channelType);
    StmSAToPDe1TestAllSubPathsOfChannel(self, (AtSdhChannel)line, channelType);
    }

static eBool ShouldTestAu3Paths(AtModulePwTestRunner self)
    {
    /* Let concrete determine. A lot of products are now working on SDH network
     * and AU-3 is not much used. */
    return cAtFalse;
    }

static void StmDe1SAToPTug3Test(AtModulePwTestRunner self, uint8 lineId)
    {
    AtSdhLine line;

    /* STM-0 does not have any VC-4s */
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return;

    Initialize(self);
    StmDe1SAToPSdhRun(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInTug3s);
    }

static void StmDe1SAToPAu3Test(AtModulePwTestRunner self, uint8 lineId)
    {
    if (mMethodsGet(self)->ShouldTestAu3Paths(self))
        {
        Initialize(self);
        StmDe1SAToPSdhRun(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInVc3s);
        }
    }

static void StmDe1SAToPTest(AtModulePwTestRunner self)
    {
    uint8 numLines, *testedLines;
    uint8 line_i;

    if (SdhModule(self) == NULL)
        return;

    testedLines = AllLines(self, &numLines);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        uint8 lineId = testedLines[line_i];

        if (!mMethodsGet(self)->ShouldTestLine(self, lineId))
            continue;

        StmDe1SAToPTug3Test(self, lineId);
        StmDe1SAToPAu3Test(self, lineId);
        }
    }

static void PdhSAToPTest(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint32 channel_i;
    uint32 numChannels = AtModulePdhNumberOfDe1sGet(pdhModule);

    if (numChannels)
        {
        Initialize(self);
        for (channel_i = 0; channel_i < numChannels; channel_i++)
            SAToPTestE1(self, AtModulePdhDe1Get(pdhModule, channel_i));
        }

    numChannels = AtModulePdhNumberOfDe3sGet(pdhModule);
    if (numChannels)
        {
        Initialize(self);
        for (channel_i = 0; channel_i < numChannels; channel_i++)
            SAToPTestDe3(self, AtModulePdhDe3Get(pdhModule, channel_i));
        }
    }

static void PsnTest(AtModulePwTestRunner self)
    {
    extern AtUnittestRunner AtPwPsnTestRunnerNew(AtModulePw pwModule);
    AtUnittestRunner runner = AtPwPsnTestRunnerNew((AtModulePw)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self));
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static void CEPVc4Test(AtModulePwTestRunner self, uint8 testedLineId)
    {
    AtSdhLine line;

    /* STM-0 does not have any VC-4s */
    line = AtModuleSdhLineGet(SdhModule(self), testedLineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return;

    Initialize(self);
    CEPHoVcRun(self, testedLineId, AtSdhTestCreateAllVc4s);
    }

static void CEPAu3Test(AtModulePwTestRunner self, uint8 testedLineId)
    {
    if (!mMethodsGet(self)->ShouldTestAu3Paths(self))
        return;

    /* VC-3 */
    Initialize(self);
    CEPHoVcRun(self, testedLineId, AtSdhTestCreateAllVc3sInAu3s);

    /* VC-12 */
    Initialize(self);
    CEPLoVcRun(self, testedLineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInVc3s);

    /* VC-11 */
    if (mMethodsGet(self)->CEPCanTestVc11(self))
        {
        Initialize(self);
        CEPLoVcRun(self, testedLineId, cAtSdhChannelTypeVc11, AtSdhTestCreateAllVc1xInVc3s);
        }
    }

static void CEPTug3Test(AtModulePwTestRunner self, uint8 lineId)
    {
    AtSdhLine line;

    /* STM-0 does not have any VC-4s, so TUG-3s are not applicable */
    line = AtModuleSdhLineGet(SdhModule(self), lineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return;

    Initialize(self);
    CEPHoVcRun(self, lineId, AtSdhTestCreateAllVc3sInTug3s);

    Initialize(self);
    CEPLoVcRun(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInTug3s);

    if (mMethodsGet(self)->CEPCanTestVc11(self))
        {
        Initialize(self);
        CEPLoVcRun(self, lineId, cAtSdhChannelTypeVc11, AtSdhTestCreateAllVc1xInTug3s);
        }
    }

static void CEPTest(AtModulePwTestRunner self)
    {
    uint8 line_i, timing_i;
    uint8 timing[2] = {cAtTimingModeSys, cAtTimingModeAcr};
    uint8 numLines, *testedLines;

    if (!mMethodsGet(self)->CEPCanBeTested(self))
        return;

    testedLines = AllLines(self, &numLines);

    for (timing_i = 0; timing_i < mCount(timing); timing_i ++)
        {
        TimingModeSet(self, timing[timing_i]);

        for (line_i = 0; line_i < numLines; line_i++)
            {
            uint8 testedLineId = testedLines[line_i];

            if (!mMethodsGet(self)->ShouldTestLine(self, testedLineId))
                continue;

            CEPVc4Test(self, testedLineId);
            CEPAu3Test(self, testedLineId);
            CEPTug3Test(self, testedLineId);
            }
        }
    }

static void CESoPTest(AtModulePwTestRunner self)
    {
    StmCESoPTest(self);
    PdhCESoPTest(self);
    }

static void SAToPTest(AtModulePwTestRunner self)
    {
    StmDe1SAToPTest(self);
    PdhSAToPTest(self);
    }

static AtModuleEncap EncapModule(AtModulePwTestRunner self)
    {
    AtDevice device = AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self);
    return (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static AtModuleEncapTestRunner ModuleEncapTestRunnerCreate(AtModulePwTestRunner self)
    {
    AtModuleTestRunner module = (AtModuleTestRunner)self;
    AtModuleTestRunnerFactory factory = AtDeviceTestRunnerModuleRunnerFactory(AtModuleTestRunnerDeviceRunnerGet(module));
    return (AtModuleEncapTestRunner)AtModuleTestRunnerFactoryModuleTestRunnerCreate(factory, (AtModule)EncapModule(self));
    }

static AtHdlcChannel HdlcChannelCreateAndBindPhysical(AtModulePwTestRunner self, AtModuleEncapTestRunner encapRunner, uint32 channelId)
    {
    AtHdlcChannel hdlcChannel = AtModuleEncapTestRunnerHdlcChannelCreateWithPhysical(encapRunner, channelId);
    return hdlcChannel;
    }

static void HdlcPwTest(AtModulePwTestRunner self, AtModuleEncapTestRunner encapRunner, uint32 channelId)
    {
    AtHdlcChannel hdlcChannel = HdlcChannelCreateAndBindPhysical(self, encapRunner, channelId);
    AtUnittestRunner runner;
    AtPwHdlc pw;

    AtAssert(hdlcChannel);
    pw = AtModulePwHdlcCreate(Module(), hdlcChannel);
    AtAssert(pw);

    runner = (AtUnittestRunner)AtPwHdlcTestRunnerNew((AtChannel)pw, (AtModuleTestRunner)self);
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);

    AtModulePwDeletePwObject(Module(), (AtPw)pw);
    AtAssert(AtEncapChannelPhyBind((AtEncapChannel)hdlcChannel, NULL) == cAtOk);
    AtAssert(AtModuleEncapChannelDelete(EncapModule(self), channelId) == cAtOk);
    }

static void StmHdlcTest(AtModulePwTestRunner self)
    {
    uint32 numChannels;
    uint32 numTestedChannels;
    uint32 channel_i;
    AtModuleEncapTestRunner encapRunner = ModuleEncapTestRunnerCreate(self);

    numChannels = AtModuleEncapMaxChannelsGet(EncapModule(self));
    numTestedChannels = numChannels;

    if (AtTestQuickTestIsEnabled())
        numTestedChannels = AtTestRandomNumChannels(numChannels);

    for (channel_i = 0; channel_i < numTestedChannels; channel_i++)
        HdlcPwTest(self, encapRunner, channel_i);

    AtUnittestRunnerDelete((AtUnittestRunner)encapRunner);
    }

static void HdlcTest(AtModulePwTestRunner self)
    {
    StmHdlcTest(self);
    }

static AtList MakePwsForGroupTest(AtModulePwTestRunner self, AtList pws)
    {
    AtAssert(0);
    return NULL;
    }

static void PwsProvisionedCheck(AtList pws)
    {
    AtIterator iterator = AtListIteratorCreate(pws);
    AtPw pw;
    while ((pw = (AtPw)AtIteratorNext(iterator)) != NULL)
        {
        AtAssert(AtPwBoundCircuitGet(pw));
        AtAssert(AtPwEthPortGet(pw));
        AtAssert(AtPwPsnGet(pw));
        }
    AtObjectDelete((AtObject)iterator);
    }

static AtList PwsForGroupTest(AtModulePwTestRunner self)
    {
    if (self->pws)
        return self->pws;

    self->pws = AtListCreate(0);
    mMethodsGet(self)->MakePwsForGroupTest(self, self->pws);
    PwsProvisionedCheck(self->pws);

    return self->pws;
    }

static void ApsGroupTest(AtModulePwTestRunner self)
    {
    AtModulePw pwModule = PwModule(self);
    uint32 numGroups = AtModulePwMaxApsGroupsGet(pwModule);
    uint32 numTestedGroups = numGroups;
    uint32 group_i;

    if (mMethodsGet(self)->MaxNumApsGroups(self) == 0)
        return;

    if (AtTestQuickTestIsEnabled())
        numTestedGroups = AtTestRandomNumChannels(numGroups);

    AtAssert(numTestedGroups);

    for (group_i = 0; group_i < numTestedGroups; group_i++)
        {
        uint32 groupId = group_i;
        AtPwGroup group;
        AtPwGroupTestRunner runner;
        AtList pws;

        if (AtTestQuickTestIsEnabled())
            groupId = AtTestRandom(numGroups, groupId);

        group = AtModulePwApsGroupCreate(pwModule, groupId);
        AtAssert(group);
        pws = PwsForGroupTest(self);
        AtAssert(AtListLengthGet(pws));
        runner = AtPwGroupApsTestRunnerNew(group, self, pws);
        AtUnittestRunnerRun((AtUnittestRunner)runner);
        AtUnittestRunnerDelete((AtUnittestRunner)runner);
        }
    }

static void HsGroupTest(AtModulePwTestRunner self)
    {
    AtModulePw pwModule = PwModule(self);
    uint32 numGroups = AtModulePwMaxHsGroupsGet(pwModule);
    uint32 numTestedGroups = numGroups;
    uint32 group_i;

    if (mMethodsGet(self)->MaxNumHsGroups(self) == 0)
        return;

    if (AtTestQuickTestIsEnabled())
        numTestedGroups = AtTestRandomNumChannels(numGroups);

    AtAssert(numTestedGroups);

    for (group_i = 0; group_i < numTestedGroups; group_i++)
        {
        uint32 groupId = group_i;
        AtPwGroup group;
        AtPwGroupTestRunner runner;
        AtList pws;

        if (AtTestQuickTestIsEnabled())
            groupId = AtTestRandom(numGroups, groupId);

        group = AtModulePwHsGroupCreate(pwModule, groupId);
        AtAssert(group);
        pws = PwsForGroupTest(self);
        AtAssert(AtListLengthGet(pws));
        runner = AtPwGroupHsTestRunnerNew(group, self, pws);
        AtUnittestRunnerRun((AtUnittestRunner)runner);
        AtUnittestRunnerDelete((AtUnittestRunner)runner);
        AtModulePwHsGroupDelete(pwModule, groupId);
        }
    }

static void PwGroupTest(AtModulePwTestRunner self)
    {
    ApsGroupTest(self);
    HsGroupTest(self);
    }

static void HsGroupTestTearDown()
    {
    uint32 group_i;
    for (group_i = 0; group_i < AtModulePwMaxHsGroupsGet(Module()); group_i++)
        AtModulePwHsGroupDelete(Module(), group_i);
    }

static void ApsGroupTestTearDown()
    {
    uint32 group_i;
    for (group_i = 0; group_i < AtModulePwMaxApsGroupsGet(Module()); group_i++)
        AtModulePwApsGroupDelete(Module(), group_i);
    }

static TestRef ApsGroupTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ApsGroup_Fixtures)
        {
        new_TestFixture("testCanCreateApsGroup", testCanCreateApsGroup),
        new_TestFixture("testCannotCreateInvalidApsGroup", testCannotCreateInvalidApsGroup),
        new_TestFixture("testCannotCreateApsGroupWithNullModule", testCannotCreateApsGroupWithNullModule),
        new_TestFixture("testCanDeleteApsGroup", testCanDeleteApsGroup),
        new_TestFixture("testCannotDeleteInvalidApsGroup", testCannotDeleteInvalidApsGroup),
        new_TestFixture("testCannotDeleteApsGroupOfNullModule", testCannotDeleteApsGroupOfNullModule),
        new_TestFixture("testDeleteApsGroupWhichHasNotBeenCreated", testDeleteApsGroupWhichHasNotBeenCreated)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ApsGroup_Caller, "TestSuite_ApsGroup", NULL, ApsGroupTestTearDown, TestSuite_ApsGroup_Fixtures);

    return (TestRef)((void *)&TestSuite_ApsGroup_Caller);
    }

static TestRef HsGroupTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_HsGroup_Fixtures)
        {
        new_TestFixture("testCanCreateHsGroup", testCanCreateHsGroup),
        new_TestFixture("testCannotCreateInvalidHsGroup", testCannotCreateInvalidHsGroup),
        new_TestFixture("testCannotCreateHsGroupWithNullModule", testCannotCreateHsGroupWithNullModule),
        new_TestFixture("testCanDeleteHsGroup", testCanDeleteHsGroup),
        new_TestFixture("testCannotDeleteInvalidHsGroup", testCannotDeleteInvalidHsGroup),
        new_TestFixture("testCannotDeleteHsGroupOfNullModule", testCannotDeleteHsGroupOfNullModule),
        new_TestFixture("testDeleteHsGroupWhichHasNotBeenCreated", testDeleteHsGroupWhichHasNotBeenCreated)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_HsGroup_Caller, "TestSuite_HsGroup", NULL, HsGroupTestTearDown, TestSuite_HsGroup_Fixtures);

    return (TestRef)((void *)&TestSuite_HsGroup_Caller);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModulePw_Fixtures)
        {
        new_TestFixture("testMaxNumberOfPwsGreaterZero", testMaxNumberOfPwsGreaterZero),
        new_TestFixture("testNumberOfPwsMustBeEnough", testNumberOfPwsMustBeEnough),
        new_TestFixture("testMaxNumberOfPwsNullModuleIsZero", testMaxNumberOfPwsNullModuleIsZero),
        new_TestFixture("testCannotDeleteNonExistedSAToP", testNoProblemWhenDeleteNoneExistPw),
        new_TestFixture("testCannotGetOutOfRangePw", testCannotGetOutOfRangePw),
        new_TestFixture("testNumberOfHsGroupMustBeEnough", testNumberOfHsGroupMustBeEnough),
        new_TestFixture("testNumberOfApsGroupMustBeEnough", testNumberOfApsGroupMustBeEnough)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModulePw_Caller, "TestSuite_ModulePw", setUp, tearDown, TestSuite_ModulePw_Fixtures);

    return (TestRef)((void *)&TestSuite_ModulePw_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtModulePw pwModule = (AtModulePw)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));

    if (AtModulePwMaxApsGroupsGet(pwModule))
        AtListObjectAdd(suites, (AtObject)ApsGroupTestSuite(self));

    if (AtModulePwMaxHsGroupsGet(pwModule))
        AtListObjectAdd(suites, (AtObject)HsGroupTestSuite(self));

    return suites;
    }

static eBool ShouldTestCESoP(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static void UnbindAllPws(AtUnittestRunner self)
    {
    uint32 pwId;
    AtModulePw pwModule = (AtModulePw)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self);

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
        {
        AtPw pw = AtModulePwGetPw(pwModule, pwId);
        if (pw != NULL)
            AtPwCircuitUnbind(pw);
        }
    }

static void PwConstrainerTest(AtModulePwTestRunner runner)
    {
    AtUnittestRunner constrainRunner = mMethodsGet(runner)->ConstrainerTestRunnerCreate(runner);
    AtUnittestRunnerRun(constrainRunner);
    AtUnittestRunnerDelete(constrainRunner);
    }

static eBool ShouldTestPwConstrain(AtModulePwTestRunner runner)
    {
    return cAtTrue;
    }

static void Run(AtUnittestRunner self)
    {
    AtModulePwTestRunner runner;
    m_AtUnittestRunnerMethods->Run(self);

    runner = (AtModulePwTestRunner)self;

    PsnTest(runner);
    CEPTest(runner);
    SAToPTest(runner);
    if (mMethodsGet(runner)->ShouldTestCESoP(runner))
        CESoPTest(runner);

    PwGroupTest(runner);
    if (mMethodsGet(runner)->ShouldTestPwConstrain(runner))
        PwConstrainerTest(runner);
    UnbindAllPws(self);

    if (mMethodsGet(runner)->ShouldTestHdlcPws(runner))
        HdlcTest(runner);
    }

static eBool CanChangeLopsClearThreshold(AtModulePwTestRunner self)
    {
    return cAtTrue;
    }

static eBool AlwaysUse2Vlans(AtModulePwTestRunner self)
    {
    return cAtFalse;
    }

static uint32 MaxNumApsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumHsGroups(AtModulePwTestRunner self)
    {
    return 0;
    }

static uint32 MaxNumPws(AtModulePwTestRunner self)
    {
    /* Concrete should specify to enforce constrain */
    return AtModulePwMaxPwsGet(PwModule(self));
    }

static void WillTestStmLine(AtModulePwTestRunner self, uint8 lineId)
    {
    }

static uint32 QueueForPw(AtModulePwTestRunner self, AtPw pw)
    {
    return cInvalidUint32;
    }

static void Delete(AtUnittestRunner self)
    {
    AtObjectDelete((AtObject)mThis(self)->pws);
    AtOsalMemFree(mThis(self)->allLines);
    m_AtUnittestRunnerMethods->Delete(self);
    }

static AtList AddPwsFromString(AtList pws, char *pwString)
    {
    AtList list = CliPwListFromString(pwString);
    AtListObjectsAddFromList(pws, list);
    AtObjectDelete((AtObject)list);
    return pws;
    }

static void SetupPwsWithCircuits(AtList pws, char *pwString, char *circuitString)
    {
    mCliSuccessAssert(AtCliExecuteFormat("pw circuit bind %s %s", pwString, circuitString));
    mCliSuccessAssert(AtCliExecuteFormat("pw ethport %s 1", pwString));
    mCliSuccessAssert(AtCliExecuteFormat("pw psn %s mpls", pwString));
    mCliSuccessAssert(AtCliExecuteFormat("pw enable %s", pwString));
    AddPwsFromString(pws, pwString);
    }

static AtUnittestRunner ConstrainerTestRunnerCreate(AtModulePwTestRunner self)
    {
    return NULL;
    }

static eBool ShouldTestOnPort(AtModulePwTestRunner self, uint8 portId)
    {
    return cAtTrue;
    }

static eBool ShouldTestHdlcPws(AtModulePwTestRunner self)
    {
    return cAtFalse;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static void MethodsInit(AtModulePwTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CanChangeLopsClearThreshold);
        mMethodOverride(m_methods, AlwaysUse2Vlans);
        mMethodOverride(m_methods, ShouldTestLine);
        mMethodOverride(m_methods, MaxNumApsGroups);
        mMethodOverride(m_methods, MaxNumHsGroups);
        mMethodOverride(m_methods, MaxNumPws);
        mMethodOverride(m_methods, ShouldTestCESoP);
        mMethodOverride(m_methods, CEPCanBeTested);
        mMethodOverride(m_methods, CEPCanTestVc11);
        mMethodOverride(m_methods, CepCanTestOnLine);
        mMethodOverride(m_methods, ShouldTestAu3Paths);
        mMethodOverride(m_methods, WillTestStmLine);
        mMethodOverride(m_methods, EthPortForSdhChannel);
        mMethodOverride(m_methods, EthPortForPdhChannel);
        mMethodOverride(m_methods, QueueForPw);
        mMethodOverride(m_methods, MakePwsForGroupTest);
        mMethodOverride(m_methods, SAToPCanTestOnLine);
        mMethodOverride(m_methods, ConstrainerTestRunnerCreate);
        mMethodOverride(m_methods, AllTestedLinesAllocate);
        mMethodOverride(m_methods, ShouldTestPwConstrain);
        mMethodOverride(m_methods, CreateCepPwTestRunner);
        mMethodOverride(m_methods, EthLoopback);
        mMethodOverride(m_methods, ShouldTestOnPort);
        mMethodOverride(m_methods, ShouldTestHdlcPws);
        mMethodOverride(m_methods, EthFlowSetup);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePwTestRunner);
    }

AtModuleTestRunner AtModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtModulePwTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModulePwTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModulePwTestRunnerObjectInit(newRunner, module);
    }

eBool AtModulePwTestRunnerCanChangeLopsClearThreshold(AtModulePwTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->CanChangeLopsClearThreshold(self);
    return cAtTrue;
    }

eBool AtModulePwTestRunnerAlwaysUse2Vlans(AtModulePwTestRunner self)
    {
    if (self)
        return mMethodsGet(self)->AlwaysUse2Vlans(self);
    return cAtFalse;
    }

void AtModulePwTestSetupCepBasicWithCircuits(AtList pws, char *pwString, char *circuitString)
    {
    mCliSuccessAssert(AtCliExecuteFormat("pw create cep %s basic", pwString));
    SetupPwsWithCircuits(pws, pwString, circuitString);
    }

void AtModulePwTestSetupSAToPWithCircuits(AtList pws, char *pwString, char *circuitString)
    {
    mCliSuccessAssert(AtCliExecuteFormat("pw create satop %s", pwString));
    SetupPwsWithCircuits(pws, pwString, circuitString);
    }

eBool AtModulePwTestCepCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->CepCanTestOnLine(self, line);
    return cAtFalse;
    }

void AtModulePwTestWillTestStmLine(AtModulePwTestRunner self, uint8 lineId)
    {
    if (self)
        return mMethodsGet(self)->WillTestStmLine(self, lineId);
    }

AtEthPort AtModulePwTestEthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->EthPortForSdhChannel(self, channel);
    return NULL;
    }

AtEthPort AtModulePwTestEthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->EthPortForPdhChannel(self, channel);
    return NULL;
    }

eAtRet AtModulePwTestRunnerEthLoopback(AtModulePwTestRunner self, AtEthPort ethPort)
    {
    if (self)
        return mMethodsGet(self)->EthLoopback(self, ethPort);
    return cAtErrorNullPointer;
    }

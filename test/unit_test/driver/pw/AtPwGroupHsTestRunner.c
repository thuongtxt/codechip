/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwGroupHsTestRunner.c
 *
 * Created Date: Jan 5, 2016
 *
 * Description : HS PW group test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroupTestRunner.h"
#include "AtPwGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwGroupHsTestRunner
    {
    tAtPwGroupTestRunner super;
    }tAtPwGroupHsTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwGroupTestRunner Runner(void)
    {
    return (AtPwGroupTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPwGroup PwGroup(void)
    {
    return AtPwGroupTestRunnerGroupGet(Runner());
    }

static AtModulePw ModulePw(void)
    {
    return AtPwGroupModuleGet(PwGroup());
    }

static void testAccessGroupIdMustBeValid(void)
    {
    TEST_ASSERT(AtPwGroupIdGet(PwGroup()) < AtModulePwMaxHsGroupsGet(ModulePw()));
    }

static void testCanSelectLabelForGroups(void)
    {
    TEST_ASSERT(AtPwGroupTxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetPrimary) == cAtOk);
    TEST_ASSERT(AtPwGroupTxSelectedLabelGet(PwGroup()) == cAtPwGroupLabelSetPrimary);
    TEST_ASSERT(AtPwGroupTxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetBackup) == cAtOk);
    TEST_ASSERT(AtPwGroupTxSelectedLabelGet(PwGroup()) == cAtPwGroupLabelSetBackup);

    TEST_ASSERT(AtPwGroupRxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetPrimary) == cAtOk);
    TEST_ASSERT(AtPwGroupRxSelectedLabelGet(PwGroup()) == cAtPwGroupLabelSetPrimary);
    TEST_ASSERT(AtPwGroupRxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetBackup) == cAtOk);
    TEST_ASSERT(AtPwGroupRxSelectedLabelGet(PwGroup()) == cAtPwGroupLabelSetBackup);
    }

static void testCannotSelectInvalidLabelForGroups(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupTxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetUnknown) != cAtOk);
    TEST_ASSERT(AtPwGroupRxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetUnknown) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotDisableGroup(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupEnable(PwGroup(), cAtFalse) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testGroupAlwaysEnable(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupEnable(PwGroup(), cAtFalse) != cAtOk);
    AtTestLoggerEnable(cAtTrue);

    TEST_ASSERT(AtPwGroupIsEnabled(PwGroup()));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwGroupHs_Fixtures)
        {
        new_TestFixture("testCannotDisableGroup", testCannotDisableGroup),
        new_TestFixture("testGroupAlwaysEnable", testGroupAlwaysEnable),
        new_TestFixture("testAccessGroupIdMustBeValid", testAccessGroupIdMustBeValid),
        new_TestFixture("testCanSelectLabelForGroups", testCanSelectLabelForGroups),
        new_TestFixture("testCannotSelectInvalidLabelForGroups", testCannotSelectInvalidLabelForGroups)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwGroupHs_Caller, "TestSuite_AtPwGroupHs", NULL, NULL, TestSuite_AtPwGroupHs_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwGroupHs_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtPwGroupTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtPwGroupTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwGroupHsTestRunner);
    }

AtPwGroupTestRunner AtPwGroupHsTestRunnerObjectInit(AtPwGroupTestRunner self,
                                                    AtPwGroup group,
                                                    AtModulePwTestRunner moduleRunner,
                                                    AtList pws)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwGroupTestRunnerObjectInit(self, group, moduleRunner, pws) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwGroupTestRunner AtPwGroupHsTestRunnerNew(AtPwGroup group, AtModulePwTestRunner moduleRunner, AtList pws)
    {
    AtPwGroupTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPwGroupHsTestRunnerObjectInit(newRunner, group, moduleRunner, pws);
    }

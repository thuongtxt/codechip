/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwGroupTestRunner.c
 *
 * Created Date: Jan 5, 2016
 *
 * Description : PW group test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtPwGroupTestRunner.h"
#include "../../../../driver/src/generic/pw/AtPwGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwGroupTestRunner Runner(void)
    {
    return (AtPwGroupTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPwGroup PwGroup(void)
    {
    return AtPwGroupTestRunnerGroupGet(Runner());
    }

static void setUp(void)
    {
    AtPwGroupInit(PwGroup());
    AtAssert(AtPwGroupNumPws(PwGroup()) == 0);
    }

static void tearDown(void)
    {
    while (AtPwGroupNumPws(PwGroup()) > 0)
        {
        AtPw pw = AtPwGroupPwAtIndex(PwGroup(), 0);
        AtPwGroupPwRemove(PwGroup(), pw);
        }
    AtAssert(AtPwGroupNumPws(PwGroup()) == 0);
    }

static void testGroupIdOfGroupNullIsInvalid(void)
    {
    TEST_ASSERT(AtPwGroupIdGet(NULL) == 0xFFFFFFFF);
    }

static void testAccessGroupTypeMustBeValid(void)
    {
    TEST_ASSERT(AtPwGroupTypeGet(PwGroup()) != cAtPwGroupTypeUnknown);
    }

static void testGroupTypeOfGroupNullIsInvalid(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupTypeGet(NULL) == cAtPwGroupTypeUnknown);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannnotEnableAndDisableGroupNull(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupEnable(NULL, cAtTrue) != cAtOk);
    TEST_ASSERT(AtPwGroupEnable(NULL, cAtFalse) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testGroupEnablingOfGroupNullMustBeFalse(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupIsEnabled(NULL) == cAtFalse);
    AtTestLoggerEnable(cAtTrue);
    }

static void testAddRemovePwsFromGroup(void)
    {
    uint32 pw_i;
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());
    AtPw pw;

    /* Add all */
    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        pw = (AtPw)AtListObjectGet(pwList, pw_i);
        TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), pw) == cAtOk);
        TEST_ASSERT(AtPwGroupContainsPw(PwGroup(), pw));
        }

    /* Remove all */
    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        pw = (AtPw)AtListObjectGet(pwList, pw_i);
        TEST_ASSERT(AtPwGroupPwRemove(PwGroup(), pw) == cAtOk);
        TEST_ASSERT(AtPwGroupContainsPw(PwGroup(), pw) == cAtFalse);
        }
    }

static void testCannotAddPwNullToGroup(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotAddPwToGroupNull(void)
    {
    AtPw pw = (AtPw)AtListObjectGet(AtPwGroupTestRunnerPwsGet(Runner()), 0);
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupPwAdd(NULL, pw) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNothingHappenWhenReAddPwToGroup(void)
    {
    uint32 pw_i;
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());

    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        uint32 numPws;

        TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), pw) == cAtOk);
        TEST_ASSERT(AtPwGroupContainsPw(PwGroup(), pw));
        numPws = AtPwGroupNumPws(PwGroup());
        TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), pw) == cAtOk);
        TEST_ASSERT_EQUAL_INT(numPws, AtPwGroupNumPws(PwGroup()));
        }
    }

static void testCanRemovePwNullFromGroups(void)
    {
    TEST_ASSERT(AtPwGroupPwRemove(PwGroup(), NULL) == cAtOk);
    }

static void testCannotRemovePwWhichDoesNotBelongtoGroup(void)
    {
    uint32 pw_i;
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());

    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        AtPw pws = (AtPw)AtListObjectGet(pwList, pw_i);
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPwGroupPwRemove(PwGroup(), pws) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCannotRemovePwFromGroupNull(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupPwRemove(NULL, (AtPw)AtListObjectGet(AtPwGroupTestRunnerPwsGet(Runner()), 0)) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanGetPwInGroupByValidIndex(void)
    {
    uint32 pw_i;
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());

    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), pw) == cAtOk);
        TEST_ASSERT_NOT_NULL(AtPwGroupPwAtIndex(PwGroup(), pw_i));
        }
    }

static void testCannotGetPwInGroupsByOutOfRangeIndex(void)
    {
    uint32 pw_i;
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());

    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        AtPw pws = (AtPw)AtListObjectGet(pwList, pw_i);
        TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), pws) == cAtOk);
        TEST_ASSERT_NOT_NULL(AtPwGroupPwAtIndex(PwGroup(), pw_i));
        TEST_ASSERT_NULL(AtPwGroupPwAtIndex(PwGroup(), pw_i + 1))
        }
    }

static void testCannotGetPwFromGroupNull(void)
    {
    TEST_ASSERT_NULL(AtPwGroupPwAtIndex(NULL, 0));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwGroup_Fixtures)
        {
        new_TestFixture("testGroupIdOfGroupNullIsInvalid", testGroupIdOfGroupNullIsInvalid),
        new_TestFixture("testAccessGroupTypeMustBeValid", testAccessGroupTypeMustBeValid),
        new_TestFixture("testGroupTypeOfGroupNullIsInvalid", testGroupTypeOfGroupNullIsInvalid),
        new_TestFixture("testCannnotEnableAndDisableGroupNull", testCannnotEnableAndDisableGroupNull),
        new_TestFixture("testGroupEnablingOfGroupNullMustBeFalse", testGroupEnablingOfGroupNullMustBeFalse),
        new_TestFixture("testAddRemovePwsFromGroup", testAddRemovePwsFromGroup),
        new_TestFixture("testCannotAddPwNullToGroup", testCannotAddPwNullToGroup),
        new_TestFixture("testCannotAddPwToGroupNull", testCannotAddPwToGroupNull),
        new_TestFixture("testNothingHappenWhenReAddPwToGroup", testNothingHappenWhenReAddPwToGroup),
        new_TestFixture("testCanRemovePwNullFromGroups", testCanRemovePwNullFromGroups),
        new_TestFixture("testCannotRemovePwWhichDoesNotBelongtoGroup", testCannotRemovePwWhichDoesNotBelongtoGroup),
        new_TestFixture("testCannotRemovePwFromGroupNull", testCannotRemovePwFromGroupNull),
        new_TestFixture("testCanGetPwInGroupByValidIndex", testCanGetPwInGroupByValidIndex),
        new_TestFixture("testCannotGetPwInGroupsByOutOfRangeIndex", testCannotGetPwInGroupsByOutOfRangeIndex),
        new_TestFixture("testCannotGetPwFromGroupNull", testCannotGetPwFromGroupNull)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwGroup_Caller, "TestSuite_AtPwGroup", setUp, tearDown, TestSuite_AtPwGroup_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwGroup_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtPwGroup group = AtPwGroupTestRunnerGroupGet((AtPwGroupTestRunner)self);
    AtSnprintf(buffer, bufferSize - 1, "%s.%s", AtPwGroupTypeString(group), AtPwGroupIdString(group));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtPwGroupTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtPwGroupTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwGroupTestRunner);
    }

AtPwGroupTestRunner AtPwGroupTestRunnerObjectInit(AtPwGroupTestRunner self,
                                                  AtPwGroup group,
                                                  AtModulePwTestRunner moduleRunner,
                                                  AtList pws)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)group) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->pws = pws;
    self->moduleRunner = moduleRunner;

    return self;
    }

AtModulePwTestRunner AtPwGroupTestRunnerModuleRunner(AtPwGroupTestRunner self)
    {
    return self ? self->moduleRunner : NULL;
    }

AtList AtPwGroupTestRunnerPwsGet(AtPwGroupTestRunner self)
    {
    return self ? self->pws : NULL;
    }

AtPwGroup AtPwGroupTestRunnerGroupGet(AtPwGroupTestRunner self)
    {
    return (AtPwGroup)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtModulePwTestRunnerInternal.h
 * 
 * Created Date: Mar 5, 2015
 *
 * Description : PW module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPWTESTRUNNERINTERNAL_H_
#define _ATMODULEPWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "AtModulePwTestRunner.h"
#include "AtEthPort.h"
#include "AtPwConstrainerTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePwTestRunnerMethods
    {
    eBool (*CanChangeLopsClearThreshold)(AtModulePwTestRunner self);
    eBool (*AlwaysUse2Vlans)(AtModulePwTestRunner self);
    eBool (*ShouldTestLine)(AtModulePwTestRunner self, uint8 lineId);
    uint32 (*MaxNumApsGroups)(AtModulePwTestRunner self);
    uint32 (*MaxNumHsGroups)(AtModulePwTestRunner self);
    uint32 (*MaxNumPws)(AtModulePwTestRunner self);
    eBool (*ShouldTestCESoP)(AtModulePwTestRunner self);
    eBool (*CEPCanBeTested)(AtModulePwTestRunner self);
    eBool (*CEPCanTestVc11)(AtModulePwTestRunner self);
    eBool (*CepCanTestOnLine)(AtModulePwTestRunner self, AtSdhLine line);
    eBool (*ShouldTestAu3Paths)(AtModulePwTestRunner self);
    void (*WillTestStmLine)(AtModulePwTestRunner self, uint8 lineId);
    AtEthPort (*EthPortForSdhChannel)(AtModulePwTestRunner self, AtSdhChannel channel);
    AtEthPort (*EthPortForPdhChannel)(AtModulePwTestRunner self, AtPdhChannel channel);
    uint32 (*QueueForPw)(AtModulePwTestRunner self, AtPw pw);
    AtList (*MakePwsForGroupTest)(AtModulePwTestRunner self, AtList pws);
    eBool (*SAToPCanTestOnLine)(AtModulePwTestRunner self, AtSdhLine line);
    AtUnittestRunner (*ConstrainerTestRunnerCreate)(AtModulePwTestRunner self);
    uint8 *(*AllTestedLinesAllocate)(AtModulePwTestRunner self, uint8 *numLines);
    eBool (*ShouldTestPwConstrain)(AtModulePwTestRunner runner);
    AtUnittestRunner (*CreateCepPwTestRunner)(AtModulePwTestRunner self, AtPw pw);
    eAtRet (*EthLoopback)(AtModulePwTestRunner self, AtEthPort ethPort);
    eBool (*ShouldTestOnPort)(AtModulePwTestRunner self, uint8 portId);
    eBool (*ShouldTestHdlcPws)(AtModulePwTestRunner self);
    eAtRet (*EthFlowSetup)(AtModulePwTestRunner self, AtPw pw);
    }tAtModulePwTestRunnerMethods;

typedef struct tAtModulePwTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModulePwTestRunnerMethods *methods;

    /* Private data */
    uint32 currentPw;
    uint8 currentTimingMode;
    AtList pws;

    uint8 *allLines;
    uint8 numLines;
    }tAtModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModulePwTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPWTESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwGroupApsTestRunner.c
 *
 * Created Date: Jan 5, 2016
 *
 * Description : APS PW group test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroupTestRunner.h"
#include "AtPwGroup.h"
#include "AtPwCounters.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwGroupApsTestRunner
    {
    tAtPwGroupTestRunner super;
    }tAtPwGroupApsTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwGroupTestRunner Runner(void)
    {
    return (AtPwGroupTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPwGroup PwGroup(void)
    {
    return AtPwGroupTestRunnerGroupGet(Runner());
    }

static AtModulePw ModulePw(void)
    {
    return (AtModulePw)AtPwGroupModuleGet(PwGroup());
    }

static eBool PwHasTxTraffic(AtPw pw)
    {
    AtPwCounters counters = NULL;

    AtAssert(AtChannelAllCountersClear((AtChannel)pw, &counters) == cAtOk);

    if ((AtPwCountersTxPacketsGet(counters)      == 0) ||
        (AtPwCountersTxPayloadBytesGet(counters) == 0))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwTrafficGenerating(AtList pws)
    {
    tAtOsalCurTime startTime, currentTime;
    uint32 elapseTimeInMs = 0;
    static const uint32 cTimeoutInMs = 50;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeInMs < cTimeoutInMs)
        {
        AtIterator iterator = AtListIteratorCreate(pws);
        AtPw pw;
        eBool hasTraffic = cAtTrue;

        /* All of PWs must have traffic */
        while ((pw = (AtPw)AtIteratorNext(iterator)) != NULL)
            {
            if (!PwHasTxTraffic(pw))
                hasTraffic = cAtFalse;
            }
        AtObjectDelete((AtObject)iterator);

        if (hasTraffic)
            return hasTraffic;

        /* Retry */
        AtOsalCurTimeGet(&currentTime);
        elapseTimeInMs = mTimeIntervalInMsGet(startTime, currentTime);
        }

    return cAtFalse;
    }

static void AddAllPws()
    {
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());
    AtIterator iterator = AtListIteratorCreate(pwList);
    AtPw pw;

    while ((pw = (AtPw)AtIteratorNext(iterator)) != NULL)
        {
        TEST_ASSERT(AtPwGroupPwAdd(PwGroup(), pw) == cAtOk);
        }

    AtObjectDelete((AtObject)iterator);
    }

static void testAccessGroupIdMustBeValid(void)
    {
    TEST_ASSERT(AtPwGroupIdGet(PwGroup()) < AtModulePwMaxApsGroupsGet(ModulePw()));
    }

static void testCannotSelectLabelForApsGroups(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwGroupTxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetPrimary) != cAtOk);
    TEST_ASSERT(AtPwGroupRxLabelSetSelect(PwGroup(), cAtPwGroupLabelSetPrimary) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanEnableAndDisableGroup(void)
    {
    AtList pwList = AtPwGroupTestRunnerPwsGet(Runner());

    /* Make configure group before enable */
    AddAllPws();

    /* Enable and check traffic */
    TEST_ASSERT(AtPwGroupEnable(PwGroup(), cAtTrue) == cAtOk);
    TEST_ASSERT(AtPwGroupIsEnabled(PwGroup()));

    if (!AtTestIsSimulationTesting())
        {
        TEST_ASSERT(PwTrafficGenerating(pwList));
        }

    /* Disable and check traffic */
    TEST_ASSERT(AtPwGroupEnable(PwGroup(), cAtFalse) == cAtOk);
    TEST_ASSERT(AtPwGroupIsEnabled(PwGroup()) == cAtFalse);

    if (!AtTestIsSimulationTesting())
        {
        TEST_ASSERT(PwTrafficGenerating(pwList) == cAtFalse);
        }
    }

static void tearDown(void)
    {
    while (AtPwGroupNumPws(PwGroup()) > 0)
        {
        AtPw pw = AtPwGroupPwAtIndex(PwGroup(), 0);
        AtAssert(AtPwGroupPwRemove(PwGroup(), pw) == cAtOk);
        }
    AtAssert(AtPwGroupNumPws(PwGroup()) == 0);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwGroupAps_Fixtures)
        {
        new_TestFixture("testAccessGroupIdMustBeValid", testAccessGroupIdMustBeValid),
        new_TestFixture("testCanEnableAndDisableGroup", testCanEnableAndDisableGroup),
        new_TestFixture("testCannotSelectLabelForApsGroups", testCannotSelectLabelForApsGroups)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwGroupAps_Caller, "TestSuite_AtPwGroupAps", NULL, tearDown, TestSuite_AtPwGroupAps_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwGroupAps_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtPwGroupTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtPwGroupTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwGroupApsTestRunner);
    }

AtPwGroupTestRunner AtPwGroupApsTestRunnerObjectInit(AtPwGroupTestRunner self,
                                                     AtPwGroup group,
                                                     AtModulePwTestRunner moduleRunner,
                                                     AtList pws)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwGroupTestRunnerObjectInit(self, group, moduleRunner, pws) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwGroupTestRunner AtPwGroupApsTestRunnerNew(AtPwGroup group, AtModulePwTestRunner moduleRunner, AtList pws)
    {
    AtPwGroupTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPwGroupApsTestRunnerObjectInit(newRunner, group, moduleRunner, pws);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtPwConstrainerTestRunner.c
 *
 * Created Date: Feb 18, 2016
 *
 * Description : PW constrainer test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "AtPdhDe1.h"
#include "AtModuleSdhTests.h"
#include "AtPwConstrainerTestRunner.h"
#include "AtModulePwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwConstrainerTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwConstrainerTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwConstrainerTestRunner Runner()
    {
    return (AtPwConstrainerTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtDevice Device()
    {
    return AtPwConstrainerTestRunnerDeviceGet(Runner());
    }

static AtModulePw ModulePw()
    {
    return (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    }

static AtModuleEth ModuleEth()
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
    }

static AtModuleSdh ModuleSdh()
    {
    return (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    }

static AtModulePdh ModulePdh()
    {
    return (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    }

static AtPwConstrainer Constrainer(AtPwConstrainerTestRunner self)
    {
    return self->constrainer;
    }

static AtModulePwTestRunner ModulePwTestRunner(AtPwConstrainerTestRunner self)
    {
    return self->modulePwTestRunner;
    }

static uint32 MaxNumPw(void)
    {
    AtPwConstrainerTestRunner runner = Runner();
    if (runner == NULL)
        return 0;

    return mMethodsGet(runner)->NumPwToTest(runner);
    }

static void testAllPwsCanBeSetType()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPwTypeSet(constrainer, (uint32_t)pwId, cAtPwConstrainerPwTypeSAToP);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwTypeGet(constrainer, (uint32_t)pwId), cAtPwConstrainerPwTypeSAToP);

        AtPwConstrainerPwTypeSet(constrainer, (uint32_t)pwId, cAtPwConstrainerPwTypeInvalid);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwTypeGet(constrainer, (uint32_t)pwId), cAtPwConstrainerPwTypeInvalid);
        }
    }

static void testAllPwsCanBeSetCircuitRate()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerCircuitRateSet(constrainer, (uint32_t)pwId, cAtCircuitRateE1);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerCircuitRateGet(constrainer, (uint32_t)pwId), cAtCircuitRateE1);

        AtPwConstrainerCircuitRateSet(constrainer, (uint32_t)pwId, 0);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerCircuitRateGet(constrainer, (uint32_t)pwId), 0);
        }
    }

static void testAllPwsCanBeSetPayloadSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPayloadSizeSet(constrainer, (uint32_t)pwId, 256);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPayloadSizeGet(constrainer, (uint32_t)pwId), 256);

        AtPwConstrainerPayloadSizeSet(constrainer, (uint32_t)pwId, 0);
        }
    }

static void testAllPwsCanBeSetJitterBufferSize()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerJitterBufferSizeSet(constrainer, (uint32_t)pwId, 256000);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerJitterBufferSizeGet(constrainer, (uint32_t)pwId), 256000);

        AtPwConstrainerJitterBufferSizeSet(constrainer, (uint32_t)pwId, 0);
        }
    }

static void testAllEthPortsCanBeSetType()
    {
    uint32 portId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (portId = 0; portId < AtPwConstrainerMaxNumEthPort(constrainer); portId++)
        {
        AtPwConstrainerPwEthPortTypeSet(constrainer, (uint32_t)portId, cAtPwConstrainerEthPortTypeXfi);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthPortTypeGet(constrainer, (uint32_t)portId), cAtPwConstrainerEthPortTypeXfi);

        AtPwConstrainerPwEthPortTypeSet(constrainer, (uint32_t)portId, cAtPwConstrainerEthPortTypeUnknown);
        }
    }

static void testAllEthPortsCanBeSetTxIpg()
    {
    uint32 portId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (portId = 0; portId < AtPwConstrainerMaxNumEthPort(constrainer); portId++)
        {
        AtPwConstrainerEthPortTxIpgSet(constrainer, (uint32_t)portId, 15);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortTxIpgGet(constrainer, (uint32_t)portId), 15);
        AtPwConstrainerEthPortTxIpgSet(constrainer, (uint32_t)portId, 0);
        }
    }

static void testAllPwsCanBeSetEthHeaderLength()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPwEthHeaderLengthSet(constrainer, (uint32_t)pwId, 60);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthHeaderLengthGet(constrainer, (uint32_t)pwId), 60);

        AtPwConstrainerPwEthHeaderLengthSet(constrainer, (uint32_t)pwId, 0);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthHeaderLengthGet(constrainer, (uint32_t)pwId), 0);
        }
    }

static void testAllPwsCanBeSetPsnLength()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPwPsnLengthSet(constrainer, (uint32_t)pwId, 40);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwPsnLengthGet(constrainer, (uint32_t)pwId), 40);

        AtPwConstrainerPwPsnLengthSet(constrainer, (uint32_t)pwId, 0);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwPsnLengthGet(constrainer, (uint32_t)pwId), 0);
        }
    }

static void testAllPwsCanBeSetEthPort()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPwEthPortSet(constrainer, (uint32_t)pwId, 0);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthPortGet(constrainer, (uint32_t)pwId), 0);

        AtPwConstrainerPwEthPortSet(constrainer, (uint32_t)pwId, cAtInvalidUint32);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthPortGet(constrainer, (uint32_t)pwId), cAtInvalidUint32);
        }
    }

static void testAllPwsCanBeEnabledRtp()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPwRtpEnable(constrainer, (uint32_t)pwId, 1);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwRtpIsEnabled(constrainer, (uint32_t)pwId), 1);

        AtPwConstrainerPwRtpEnable(constrainer, (uint32_t)pwId, 0);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwRtpIsEnabled(constrainer, (uint32_t)pwId), 0);
        }
    }

static void testAllPwsCanBeSetLopsThreshold()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPwConstrainerPwLopsThresholdSet(constrainer, (uint32_t)pwId, 30);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwLopsThresholdGet(constrainer, (uint32_t)pwId), 30);

        AtPwConstrainerPwLopsThresholdSet(constrainer, (uint32_t)pwId, 0);
        }
    }

static void testAllPwsEthPortMustBeInvalid()
    {
    uint32 pwId;
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerPwEthPortGet(constrainer, (uint32_t)pwId), cAtInvalidUint32);
    }

static void testMaxMinAfterSAToPIsCreated()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw();
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPw pw = (AtPw)AtModulePwSAToPCreate(modulePw, pwId);

        TEST_ASSERT_NOT_NULL(pw);
        AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeSAToP);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMaxJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMinJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        AtModulePwDeletePw(modulePw, pwId);
        }
    }

static void testMaxMinAfterCEPIsCreated()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw();
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPw pw = (AtPw)AtModulePwCepCreate(modulePw, pwId, cAtPwCepModeBasic);

        TEST_ASSERT_NOT_NULL(pw);
        AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeCEP);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMaxJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMinJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        AtModulePwDeletePw(modulePw, pwId);
        }
    }

static void testMaxMinAfterCESoPIsCreated()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw();
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < MaxNumPw(); pwId++)
        {
        AtPw pw = (AtPw)AtModulePwCESoPCreate(modulePw, pwId, cAtPwCESoPModeBasic);

        TEST_ASSERT_NOT_NULL(pw);
        AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeCESoP);
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMaxPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinPayloadSize(constrainer, pwId, AtPwConstrainerJitterBufferSizeGet(constrainer, pwId)),
                              AtPwMinPayloadSize(pw, AtPwBoundCircuitGet(pw), AtPwJitterBufferSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMaxJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));

        TEST_ASSERT_EQUAL_INT(AtPwConstrainerMinJitterBufferSize(constrainer, pwId, AtPwConstrainerPayloadSizeGet(constrainer, pwId)),
                              AtPwMinJitterBufferSize(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw)));
        AtModulePwDeletePw(modulePw, pwId);
        }
    }

static void TearDown()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw();

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(modulePw); pwId++)
        AtModulePwDeletePw(modulePw, pwId);
    }

static void Setup()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw();

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(modulePw); pwId++)
        {
        AtPw pw = AtModulePwGetPw(modulePw, pwId);
        if (pw == NULL)
            continue;

        AtPwCircuitUnbind(pw);
        AtModulePwDeletePw(modulePw, pwId);
        }
    }

static TestRef BasicUnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwConstrainer_Basic_Fixtures)
        {
        new_TestFixture("testAllPwsCanBeSetType", testAllPwsCanBeSetType),
        new_TestFixture("testAllPwsCanBeSetCircuitRate", testAllPwsCanBeSetCircuitRate),
        new_TestFixture("testAllPwsCanBeSetPayloadSize", testAllPwsCanBeSetPayloadSize),
        new_TestFixture("testAllPwsCanBeSetJitterBufferSize", testAllPwsCanBeSetJitterBufferSize),
        new_TestFixture("testAllEthPortsCanBeSetType", testAllEthPortsCanBeSetType),
        new_TestFixture("testAllEthPortsCanBeSetTxIpg", testAllEthPortsCanBeSetTxIpg),
        new_TestFixture("testAllPwsCanBeSetEthHeaderLength", testAllPwsCanBeSetEthHeaderLength),
        new_TestFixture("testAllPwsCanBeSetPsnLength", testAllPwsCanBeSetPsnLength),
        new_TestFixture("testAllPwsEthPortMustBeInvalid", testAllPwsEthPortMustBeInvalid),
        new_TestFixture("testAllPwsCanBeSetEthPort", testAllPwsCanBeSetEthPort),
        new_TestFixture("testAllPwsCanBeEnabledRtp", testAllPwsCanBeEnabledRtp),
        new_TestFixture("testAllPwsCanBeSetLopsThreshold", testAllPwsCanBeSetLopsThreshold),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwConstrainer_Basic_Caller, "TestSuite_AtPwConstrainer_Basic", NULL, NULL, TestSuite_AtPwConstrainer_Basic_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwConstrainer_Basic_Caller);
    }

static TestRef SingleUnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwConstrainer_SinglePw_Fixtures)
        {
        new_TestFixture("testMaxMinAfterSAToPIsCreated", testMaxMinAfterSAToPIsCreated),
        new_TestFixture("testMaxMinAfterCEPIsCreated", testMaxMinAfterCEPIsCreated),
        new_TestFixture("testMaxMinAfterCESoPIsCreated", testMaxMinAfterCESoPIsCreated),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwConstrainer_SinglePw_Caller, "TestSuite_AtPwConstrainer_SinglePw", Setup, TearDown, TestSuite_AtPwConstrainer_SinglePw_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwConstrainer_SinglePw_Caller);
    }

static void testConstrainerPortTxIpgMustBeSameWithSdk()
    {
    uint32 portId;
    AtModuleEth moduleEth = ModuleEth();
    AtPwConstrainer constrainer = Constrainer(Runner());
    AtModulePwTestRunner moduleTestRunner = Runner()->modulePwTestRunner;

    TEST_ASSERT_EQUAL_INT(AtDeviceInit(Device()), cAtOk);
    for (portId = 0; portId < AtModuleEthMaxPortsGet(moduleEth); portId++)
        {
        if (!mMethodsGet(moduleTestRunner)->ShouldTestOnPort(moduleTestRunner, portId))
            continue;
        TEST_ASSERT_EQUAL_INT(AtPwConstrainerEthPortTxIpgGet(constrainer, portId), AtEthPortTxIpgGet(AtModuleEthPortGet(moduleEth, portId)));
        }
    }

static TestRef EthPortUnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwConstrainer_EthPort_Fixtures)
        {
        new_TestFixture("testConstrainerPortTxIpgMustBeSameWithSdk", testConstrainerPortTxIpgMustBeSameWithSdk),
        new_TestFixture("", NULL),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwConstrainer_EthPort_Caller, "TestSuite_AtPwConstrainer_EthPort", NULL, NULL, TestSuite_AtPwConstrainer_EthPort_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwConstrainer_EthPort_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)BasicUnitTestSuite(self));
    AtListObjectAdd(suites, (AtObject)SingleUnitTestSuite(self));
    AtListObjectAdd(suites, (AtObject)EthPortUnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "PW Constrainer Basic");
    return buffer;
    }

static void Delete(AtUnittestRunner self)
    {
    AtPwConstrainerDelete(mThis(self)->constrainer);
    m_AtUnittestRunnerMethods->Delete(self);
    }

static void ResetPwId(AtPwConstrainerTestRunner self)
    {
    self->currentPw = 0;
    }

static eAtRet PsnSetup(AtPwConstrainerTestRunner self, AtPw pw)
    {
    AtPwMplsPsn mpls = AtPwMplsPsnNew();
    tAtPwMplsLabel label;
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    eAtRet ret;

    AtPwMplsPsnInnerLabelSet(mpls, AtPwMplsLabelMake(pwId + 1, pwId % 8, pwId % 256, &label));
    AtPwMplsPsnExpectedLabelSet(mpls, pwId + 1);
    ret = AtPwPsnSet(pw, (AtPwPsn)mpls);
    AtPwConstrainerPwPsnLengthSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), AtPwPsnLengthInBytes((AtPwPsn)mpls));
    AtObjectDelete((AtObject)mpls);

    return ret;
    }

static uint8 *DefaultMac()
    {
    static uint8 mac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    return mac;
    }

static eAtRet EthernetSetup(AtPwConstrainerTestRunner self, AtPw pw, AtEthPort ethPort)
    {
    eAtRet ret = cAtOk;

    ret |= AtEthPortSourceMacAddressSet(ethPort, DefaultMac());
    ret |= AtPwEthPortSet(pw, ethPort);
    ret |= AtPwEthHeaderSet(pw, DefaultMac(), NULL, NULL);
    return ret;
    }

static void SAToPSetupE1(AtPwConstrainerTestRunner self, AtPdhDe1 de1)
    {
    AtPw pw = (AtPw)AtModulePwSAToPCreate(ModulePw(), AtPwConstrainerTestRunnerNextPw(self));
    AtModulePwTestRunner moduleRunner = ModulePwTestRunner(self);

    AtAssert(pw);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm))
    AtPwConstrainerPwTypeSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), cAtPwConstrainerPwTypeSAToP);
    AtPwConstrainerTestRunnerPwNormalSetup(self, pw, (AtChannel)de1, AtModulePwTestEthPortForPdhChannel(moduleRunner, (AtPdhChannel)de1), cAtCircuitRateE1);
    }

static void SAToPSetupE3(AtPwConstrainerTestRunner self, AtPdhDe3 de3)
    {
    AtPw pw = (AtPw)AtModulePwSAToPCreate(ModulePw(), AtPwConstrainerTestRunnerNextPw(self));
    AtModulePwTestRunner moduleRunner = ModulePwTestRunner(self);

    AtAssert(pw);

    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de3, cAtPdhE3Unfrm) == cAtOk);
    AtPwConstrainerPwTypeSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), cAtPwConstrainerPwTypeSAToP);
    AtPwConstrainerTestRunnerPwNormalSetup(self, pw, (AtChannel)de3, AtModulePwTestEthPortForPdhChannel(moduleRunner, (AtPdhChannel)de3), cAtCircuitRateE3);
    }

static uint32 PdhDe1SAToPSetup(AtPwConstrainerTestRunner self)
    {
    AtModulePdh pdhModule = ModulePdh();
    uint32 channel_i, numPw = 0;
    uint32 numChannels;

    if (pdhModule == NULL)
        return 0;

    numChannels = AtModulePdhNumberOfDe1sGet(pdhModule);
    if (numChannels)
        {
        AtPwConstrainerTestRunnerInit(self);
        for (channel_i = 0; channel_i < numChannels; channel_i++)
            {
            SAToPSetupE1(self, AtModulePdhDe1Get(pdhModule, channel_i));
            numPw += 1;
            }
        }

    return numPw;
    }

static uint32 PdhDe3SAToPSetup(AtPwConstrainerTestRunner self)
    {
    AtModulePdh pdhModule = ModulePdh();
    uint32 channel_i, numPw = 0;
    uint32 numChannels;

    if (pdhModule == NULL)
        return 0;

    numChannels = AtModulePdhNumberOfDe3sGet(pdhModule);
    if (numChannels)
        {
        AtPwConstrainerTestRunnerInit(self);
        for (channel_i = 0; channel_i < numChannels; channel_i++)
            {
            SAToPSetupE3(self, AtModulePdhDe3Get(pdhModule, channel_i));
            numPw += 1;
            }
        }

    return numPw;
    }

static void MultiplePwTestTearDown()
    {
    uint32 pwId;
    AtModulePw modulePw = ModulePw();
    uint32 maxNumPw = AtModulePwMaxPwsGet(modulePw);
    AtPwConstrainer constrainer = Constrainer(Runner());

    for (pwId = 0; pwId < maxNumPw; pwId++)
        {
        AtPw pw = AtModulePwGetPw(modulePw, pwId);
        if (pw == NULL)
            continue;

        AtPwCircuitUnbind(pw);
        AtPwEthPortSet(pw, NULL);
        AtModulePwDeletePw(modulePw, pwId);
        AtPwConstrainerPwReset(constrainer, pwId);
        }
    }

static void PdhDe3LiuSAToPTest(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = PdhDe3SAToPSetup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "PDH Satop");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static void PdhDe1LiuSAToPTest(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = PdhDe1SAToPSetup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "PDH Satop");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static void PdhSAToPTest(AtPwConstrainerTestRunner self)
    {
    PdhDe1LiuSAToPTest(self);
    PdhDe3LiuSAToPTest(self);
    }

static uint8 MaxLines(AtPwConstrainerTestRunner self)
    {
    return AtModuleSdhMaxLinesGet(ModuleSdh());
    }

static uint32 StmSAToPDe1SetupAllSubPathsOfChannel(AtPwConstrainerTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 numTestedChannels = numSubChannels;
    uint8 channel_i;
    uint32 numPw = 0;

    for (channel_i = 0; channel_i < numTestedChannels; channel_i++)
        {
        uint8 channelIndex = channel_i;
        AtSdhChannel subChannel;

        subChannel = AtSdhChannelSubChannelGet(channel, channelIndex);
        AtAssert(subChannel);
        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapDe1);
            SAToPSetupE1(self, (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
            numPw++;
            }

        /* Otherwise, test all of its sub channels */
        else
            numPw += StmSAToPDe1SetupAllSubPathsOfChannel(self, subChannel, pathType);
        }

    return numPw;
    }

static uint32 StmDe1SAToPSdhSetup(AtPwConstrainerTestRunner self,
                                uint8 lineId,
                                eAtSdhChannelType channelType,
                                void (*CreateFunc)(AtSdhLine line, eAtSdhChannelType vc1xType))
    {
    AtSdhLine line = AtModuleSdhLineGet(ModuleSdh(), lineId);
    uint32 numPw = 0;
    CreateFunc(line, channelType);

    numPw += StmSAToPDe1SetupAllSubPathsOfChannel(self, (AtSdhChannel)line, channelType);
    return numPw;
    }

static uint32 StmDe1SAToPAu3Setup(AtPwConstrainerTestRunner self, uint8 lineId)
    {
    uint32 numPw = 0;
    numPw += StmDe1SAToPSdhSetup(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInVc3s);

    return numPw;
    }

static uint32 StmSAToPSetup(AtPwConstrainerTestRunner self)
    {
    uint8 numLines;
    uint8 numTestedLines;
    uint8 line_i;
    uint32 numPw = 0;
    AtModulePwTestRunner moduleTestRunner = ModulePwTestRunner(self);

    if (ModuleSdh() == NULL)
        return 0;

    numLines = MaxLines(self);
    numTestedLines = numLines;

    AtPwConstrainerTestRunnerInit(self);
    for (line_i = 0; line_i < numTestedLines; line_i++)
        {
        uint8 lineId = line_i;

        if (!mMethodsGet(moduleTestRunner)->ShouldTestLine(moduleTestRunner, lineId))
            continue;

        numPw += StmDe1SAToPAu3Setup(self, lineId);
        }

    return numPw;
    }

static void StmSAToPTest(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = StmSAToPSetup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "STM Satop");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static void SAToPTest(AtPwConstrainerTestRunner self)
    {
    PdhSAToPTest(self);
    StmSAToPTest(self);
    }

static uint32 CEPHoVcSetup(AtPwConstrainerTestRunner self, uint8 lineId, uint16 (*VcCreateFunc)(AtSdhLine line, AtSdhChannel *channels), uint32_t circuitRate)
    {
    uint16 numChannels, channel_i;
    AtSdhLine line;
    AtModulePwTestRunner modulePwTestRunner = ModulePwTestRunner(self);
    uint32 numPw = 0;
    AtPwConstrainer constrainer = Constrainer(self);
    uint32 pwId;

    /* Initialize device have clean status */
    line = AtModuleSdhLineGet(ModuleSdh(), lineId);
    if (!AtModulePwTestCepCanTestOnLine(modulePwTestRunner, line))
        return 0;

    AtModulePwTestWillTestStmLine(modulePwTestRunner, lineId);

    numChannels = VcCreateFunc(line, AtSdhTestSharedChannelList(NULL));
    AtAssert(numChannels);

    /* And test all of them */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtSdhChannel channel;
        uint32 testedIndex = channel_i;
        AtPw pw = (AtPw)AtModulePwCepCreate(ModulePw(), AtPwConstrainerTestRunnerNextPw(self), cAtPwCepModeBasic);

        AtAssert(pw);
        pwId = AtChannelIdGet((AtChannel)pw);
        channel = AtSdhTestSharedChannelList(NULL)[testedIndex];
        AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeCEP);
        AtPwConstrainerTestRunnerPwNormalSetup(self, pw, (AtChannel)channel, AtModulePwTestEthPortForSdhChannel(modulePwTestRunner, channel), circuitRate);
        AtAssert(AtPwJitterBufferSizeSet(pw, 16000) == cAtOk);
        AtAssert(AtPwJitterBufferDelaySet(pw, 8000) == cAtOk);
        AtPwConstrainerJitterBufferSizeSet(constrainer, pwId, 16000);
        numPw++;
        }

    return numPw;
    }

static uint32 CEPVc4SetupOnLine(AtPwConstrainerTestRunner self, uint8 testedLineId)
    {
    AtSdhLine line;

    /* STM-0 does not have any VC-4s */
    line = AtModuleSdhLineGet(ModuleSdh(), testedLineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return 0;

    return CEPHoVcSetup(self, testedLineId, AtSdhTestCreateAllVc4s, cAtCircuitRateStsNc(3));
    }

static uint32 CEPVc4Setup(AtPwConstrainerTestRunner self)
    {
    uint8 line_i, numLines;
    uint32 numPw = 0;

    numLines = MaxLines(self);
    AtPwConstrainerTestRunnerInit(self);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(ModuleSdh(), line_i);
        if (!AtModulePwTestCepCanTestOnLine(ModulePwTestRunner(self), line))
            return numPw;

        numPw += CEPVc4SetupOnLine(self, line_i);
        }

    return numPw;
    }

static void CEPVc4Test(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = CEPVc4Setup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "CEP VC4");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static uint32 CEPAu3SetupOnLine(AtPwConstrainerTestRunner self, uint8 testedLineId)
    {
    return CEPHoVcSetup(self, testedLineId, AtSdhTestCreateAllVc3sInAu3s, cAtCircuitRateSts1);
    }

static uint32 CEPAu3Setup(AtPwConstrainerTestRunner self)
    {
    uint8 line_i, numLines;
    uint32 numPw = 0;

    numLines = MaxLines(self);
    AtPwConstrainerTestRunnerInit(self);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(ModuleSdh(), line_i);
        if (!AtModulePwTestCepCanTestOnLine(ModulePwTestRunner(self), line))
            return numPw;

        numPw += CEPAu3SetupOnLine(self, line_i);
        }

    return numPw;
    }

static void CEPAu3Test(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = CEPAu3Setup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "CEP AU3");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static uint32 CEPSetupAllSubPathsOfChannel(AtPwConstrainerTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType, uint32_t circuitRate)
    {
    uint32 numPw = 0;
    uint8 channel_i;
    uint32 pwId;
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    AtModulePwTestRunner modulePwTestRunner = ModulePwTestRunner(self);
    AtPwConstrainer constrainer = Constrainer(self);

    AtAssert(numSubChannels);

    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, channel_i);

        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            /* Create PW */
            AtPw pw = (AtPw)AtModulePwCepCreate(ModulePw(), AtPwConstrainerTestRunnerNextPw(self), cAtPwCepModeBasic);
            AtAssert(pw);

            /* Make sure that this VC1x does not map DS1/E1 */
            AtAssert(AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapC1x) == cAtOk);
            pwId = AtChannelIdGet((AtChannel)pw);
            AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeCEP);
            AtPwConstrainerTestRunnerPwNormalSetup(self, pw, (AtChannel)subChannel, AtModulePwTestEthPortForSdhChannel(modulePwTestRunner, subChannel), circuitRate);
            AtAssert(AtPwJitterBufferSizeSet(pw, 16000) == cAtOk);
            AtAssert(AtPwJitterBufferDelaySet(pw, 8000) == cAtOk);
            AtPwConstrainerJitterBufferSizeSet(constrainer, pwId, 16000);
            numPw++;
            }

        /* Otherwise, test all of its sub channels */
        else
            numPw += CEPSetupAllSubPathsOfChannel(self, subChannel, pathType, circuitRate);
        }

    return numPw;
    }

static uint32 CEPLoVcSetup(AtPwConstrainerTestRunner self,
                         uint8 lineId,
                         eAtSdhChannelType channelType,
                         void (*CreateFunc)(AtSdhLine line, eAtSdhChannelType vc1xType),
                         uint32_t circuitRate)
    {
    AtSdhLine line;

    /* Initialize device have clean status */
    line = AtModuleSdhLineGet(ModuleSdh(), lineId);
    CreateFunc(line, channelType);

    return CEPSetupAllSubPathsOfChannel(self, (AtSdhChannel)line, channelType, circuitRate);
    }

static uint32 CEPVc12SetupOnLine(AtPwConstrainerTestRunner self, uint8 testedLineId)
    {
    return CEPLoVcSetup(self, testedLineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInVc3s, cAtCircuitRateVc12);
    }

static uint32 CEPVc12Setup(AtPwConstrainerTestRunner self)
    {
    uint8 line_i, numLines;
    uint32 numPw = 0;

    numLines = MaxLines(self);
    AtPwConstrainerTestRunnerInit(self);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(ModuleSdh(), line_i);
        if (!AtModulePwTestCepCanTestOnLine(ModulePwTestRunner(self), line))
            return numPw;

        numPw += CEPVc12SetupOnLine(self, line_i);
        }

    return numPw;
    }

static void CEPVc12Test(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = CEPVc12Setup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "CEP VC12");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static eBool ShouldTestCep(AtPwConstrainerTestRunner self)
    {
    if (ModuleSdh() != NULL)
        return cAtTrue;

    return cAtFalse;
    }

static void CEPTest(AtPwConstrainerTestRunner self)
    {
    if (!ShouldTestCep(self))
        return;

    CEPVc4Test(self);
    CEPAu3Test(self);
    CEPVc12Test(self);
    }

static uint32 TimeslotMake(uint8 startTimeslot, uint8 numTimeslots)
    {
    uint32 mask = 0;
    uint8 i, numRemainedSlots;

    numRemainedSlots = 32 - startTimeslot;
    if (numTimeslots > numRemainedSlots)
        numTimeslots = numRemainedSlots;

    if (numTimeslots == 0)
        return 0;

    for (i = 0; i < numTimeslots; i++)
        mask |= (cBit0 << (startTimeslot + i));

    return mask;
    }

static uint32 E1CESoPSetup(AtPwConstrainerTestRunner self, AtPdhDe1 de1)
    {
    static const uint8 cNumTimeslotPerNxDs0 = 16;
    uint8 timeslot;
    AtModulePwTestRunner modulePwTestRunner = ModulePwTestRunner(self);
    uint32 numPw = 0;

    /* Toggle framing to delete all of NxDS0s which may exist before */
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm) == cAtOk);
    AtAssert(AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm) == cAtOk);
    AtAssert(AtChannelLoopbackSet((AtChannel)de1, cAtPdhLoopbackModeLocalLine) == cAtOk);

    for (timeslot = 1; timeslot < 31; timeslot += cNumTimeslotPerNxDs0)
        {
        AtPdhNxDS0 nxDs0;
        uint32 pwId = AtPwConstrainerTestRunnerNextPw(self);
        AtPw pw;

        if (pwId >= mMethodsGet(self)->NumPwToTest(self))
            return numPw;

        nxDs0 = AtPdhDe1NxDs0Create(de1, TimeslotMake(timeslot, cNumTimeslotPerNxDs0));
        pw = (AtPw)AtModulePwCESoPCreate(ModulePw(), pwId, cAtPwCESoPModeBasic);
        AtAssert(pw);
        AtAssert(nxDs0 != NULL);
        AtPwConstrainerPwTypeSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), cAtPwConstrainerPwTypeCESoP);
        AtPwConstrainerTestRunnerPwNormalSetup(self, pw, (AtChannel)nxDs0, AtModulePwTestEthPortForPdhChannel(modulePwTestRunner, (AtPdhChannel)de1), cAtCircuitRateNxDs0(AtPdhNxDs0NumTimeslotsGet(nxDs0)));
        numPw++;
        }

    return numPw;
    }

static uint32 PdhCESoPSetup(AtPwConstrainerTestRunner self)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    uint16 de1Id;
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(pdhModule);
    uint32 numPw = 0;

    if (numDe1s == 0)
        return 0;

    AtPwConstrainerTestRunnerInit(self);
    for (de1Id = 0; de1Id < numDe1s; de1Id++)
        numPw += E1CESoPSetup(self, AtModulePdhDe1Get(pdhModule, de1Id));

    return numPw;
    }

static void PdhCESoPTest(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = PdhCESoPSetup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "PDH CESoP");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static uint32 CESoPSetupInAllSubPathsOfChannel(AtPwConstrainerTestRunner self, AtSdhChannel channel, eAtSdhChannelType pathType)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 i;
    uint32 numpw = 0;

    for (i = 0; i < numSubChannels; i++)
        {
        uint8 channelIndex = i;
        AtSdhChannel subChannel;
        subChannel = AtSdhChannelSubChannelGet(channel, channelIndex);

        if (AtSdhChannelTypeGet(subChannel) == pathType)
            {
            AtSdhChannelMapTypeSet(subChannel, cAtSdhVcMapTypeVc1xMapDe1);
            numpw += E1CESoPSetup(self, (AtPdhDe1)AtSdhChannelMapChannelGet(subChannel));
            }

        /* Otherwise, test all of its sub channels */
        else
            numpw += CESoPSetupInAllSubPathsOfChannel(self, subChannel, pathType);
        }

    return numpw;
    }

static uint32 CESoPSdhSetup(AtPwConstrainerTestRunner self,
                            uint8 lineId,
                            eAtSdhChannelType channelType,
                            void (*CreateFunc)(AtSdhLine line, eAtSdhChannelType vc1xType))
    {
    AtSdhLine line = AtModuleSdhLineGet(ModuleSdh(), lineId);
    AtModulePwTestRunner modulePwTestRunner = ModulePwTestRunner(self);

    if (!AtModulePwTestCepCanTestOnLine(modulePwTestRunner, line))
        return 0;

    AtModulePwTestWillTestStmLine(modulePwTestRunner, lineId);
    CreateFunc(line, channelType);
    return CESoPSetupInAllSubPathsOfChannel(self, (AtSdhChannel)line, channelType);
    }

static uint32 StmAu3CESoPSetup(AtPwConstrainerTestRunner self, uint8 lineId)
    {
    return CESoPSdhSetup(self, lineId, cAtSdhChannelTypeVc12, AtSdhTestCreateAllVc1xInVc3s);
    }

static uint32 StmCESoPSetup(AtPwConstrainerTestRunner self)
    {
    uint8 numLines;
    uint8 line_i;
    uint32 numPw = 0;
    AtModulePwTestRunner moduleTestRunner = ModulePwTestRunner(self);

    if (ModuleSdh() == NULL)
        return 0;

    AtPwConstrainerTestRunnerInit(self);
    numLines = MaxLines(self);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        if (!mMethodsGet(moduleTestRunner)->ShouldTestLine(moduleTestRunner, line_i))
            continue;

        numPw += StmAu3CESoPSetup(self, line_i);
        if (numPw == AtModulePwMaxPwsGet(ModulePw()))
            break;
        }

    return numPw;
    }

static void StmCESoPTest(AtPwConstrainerTestRunner self)
    {
    uint32 numPw;
    if ((numPw = StmCESoPSetup(self)) > 0)
        {
        AtUnittestRunner runner = AtPwConstrainerMultiplePwsTestRunnerNew(self, numPw, "STM CESoP");
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        MultiplePwTestTearDown();
        }
    }

static void CESoPTest(AtPwConstrainerTestRunner self)
    {
    PdhCESoPTest(self);
    StmCESoPTest(self);
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    SAToPTest(mThis(self));
    CEPTest(mThis(self));
    CESoPTest(mThis(self));
    }

static uint32 NumPwToTest(AtPwConstrainerTestRunner self)
    {
    AtUnused(self);
    return AtModulePwMaxPwsGet(ModulePw());
    }

static void MethodsInit(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NumPwToTest);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)m_AtUnittestRunnerMethods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwConstrainerTestRunner);
    }

AtUnittestRunner AtPwConstrainerTestRunnerObjectInit(AtUnittestRunner self, AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->constrainer = constrainer;
    mThis(self)->modulePwTestRunner = modulePwTestRunner;

    return self;
    }

AtUnittestRunner AtPwConstrainerTestRunnerNew(AtModulePwTestRunner modulePwTestRunner, AtPwConstrainer constrainer)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    if (newRunner == NULL)
        return NULL;

    return AtPwConstrainerTestRunnerObjectInit(newRunner, modulePwTestRunner, constrainer);
    }

AtDevice AtPwConstrainerTestRunnerDeviceGet(AtPwConstrainerTestRunner self)
    {
    if (self)
        return AtModuleTestRunnerDeviceGet((AtModuleTestRunner)self->modulePwTestRunner);

    return NULL;
    }

AtPwConstrainer AtPwConstrainerTestRunnerConstrainerGet(AtPwConstrainerTestRunner self)
    {
    if (self)
        return self->constrainer;

    return NULL;
    }

void AtPwConstrainerTestRunnerPwNormalSetup(AtPwConstrainerTestRunner self, AtPw pw, AtChannel circuit, AtEthPort ethPort, uint32_t circuitRate)
    {
    eAtRet ret = cAtOk;
    uint32_t pwId = (uint32_t)AtChannelIdGet((AtChannel)pw);
    const uint32_t cHeaderLengthWithoutVlan = 14;

    ret = EthernetSetup(self, pw, ethPort);
    AtAssert(ret == cAtOk);

    ret = PsnSetup(self, pw);
    AtAssert(ret == cAtOk);

    ret = AtPwCircuitBind(pw, circuit);
    AtAssert(ret == cAtOk);
    AtPwConstrainerCircuitRateSet(Constrainer(self), pwId, circuitRate);

    ret = AtChannelEnable((AtChannel)pw, cAtTrue);

    /* Make sure that all seem fine */
    if ((ret != cAtErrorNotApplicable) && (ret != cAtErrorModeNotSupport))
        AtAssert(ret == cAtOk);
    AtAssert(AtPwEthPortGet(pw));
    AtAssert(AtPwBoundCircuitGet(pw));
    AtAssert(AtPwPsnGet(pw));

    AtPwConstrainerPwEthHeaderLengthSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), cHeaderLengthWithoutVlan);
    AtPwConstrainerPwEthPortSet(Constrainer(self), AtChannelIdGet((AtChannel)pw), AtChannelIdGet((AtChannel)ethPort));
    }

uint32 AtPwConstrainerTestRunnerNextPw(AtPwConstrainerTestRunner self)
    {
    uint32 nextPw = self->currentPw;

    /* Increase for next time */
    self->currentPw = self->currentPw + 1;
    return nextPw;
    }

void AtPwConstrainerTestRunnerInit(AtPwConstrainerTestRunner self)
    {
    AtAssert(AtDeviceInit(Device()) == cAtOk);
    ResetPwId(self);
    }

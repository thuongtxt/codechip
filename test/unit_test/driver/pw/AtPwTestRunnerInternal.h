/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwTestRunnerInternal.h
 * 
 * Created Date: Jul 10, 2014
 *
 * Description : PW test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWTESTRUNNERINTERNAL_H_
#define _ATPWTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mRoundUp(num1, num2) (((num1)/(num2)) + ((((num1)%(num2)) == 0) ? 0 : 1))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwTestRunner * AtPwTestRunner;

typedef struct tAtPwTestRunnerMethods
    {
    uint8 dummy;
    }tAtPwTestRunnerMethods;

typedef struct tAtPwTestRunner
    {
    tAtChannelTestRunner super;
    const tAtPwTestRunnerMethods *methods;
    }tAtPwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtPwTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner);

uint32 AtPwTestMinJitterBufferSizeInUs(uint16 payloadSize);
uint32 AtPwTestMinJitterDelayInUs(uint16 payloadSize);
eBool AtPwTestClearAndCheckStatus(AtPw pw, uint32 timeoutInMs);

#endif /* _ATPWTESTRUNNERINTERNAL_H_ */


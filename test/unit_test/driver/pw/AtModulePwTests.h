/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtModulePwTests.h
 *
 * Created Date: Jan 17, 2013
 *
 * Description : PW test
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPWTESTS_H_
#define _ATMODULEPWTESTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTests.h"
#include "AtPw.h"
#include "AtPwCounters.h"
#include "AtEthPort.h"
#include "AtModuleEth.h"
#include "AtPdhDe1.h"
#include "AtModulePdh.h"
#include "AtPdhChannel.h"
#include "AtModuleSdhTests.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cStatusCheckingTimeoutMs 1000

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void TestSuite_AtPwRun(AtPw pw);
void TestSuite_PwChannelRun(AtPw pw);

AtPdhDe1 AtPwTestDe1Create();
void AtTestPwNormalSetup(AtPw pw, AtChannel circuit, AtEthPort ethPort);

/* Utils */
void AtPwTestChangeCircuitTimingMode(AtPw pw);
void AtPwTestCannotChangeTimingModeToAcrDcrIfNoPw(AtPw pw);
eBool AtPwTestClearAndCheckStatus(AtPw pw, uint32 timeoutInMs);
uint32 AtPwTestMinJitterBufferSizeInUs(uint16 payloadSize);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPWTESTS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwHdlcTestRunner.c
 *
 * Created Date: Feb 17, 2017
 *
 * Description : HDLC PW test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../driver/include/pw/AtPwHdlc.h"
#include "AtModulePwTests.h"
#include "AtModulePwTestRunner.h"
#include "AtPwTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwHdlcTestRunner
    {
    tAtChannelTestRunner super;
    }tAtPwHdlcTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw TestedPw()
    {
    return (AtPw)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static eBool CanChangeLopsClearThreshold(void)
    {
    AtModuleTestRunner moduleRunner = AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    return AtModulePwTestRunnerCanChangeLopsClearThreshold((AtModulePwTestRunner)moduleRunner);
    }

static AtModulePw PwModule()
    {
    return (AtModulePw)AtChannelModuleGet((AtChannel)TestedPw());
    }

static uint32 VlanId(uint32 value)
    {
    return value % 4096;
    }

static void testCanChangePayloadType()
    {
    AtPwHdlc pw = (AtPwHdlc)TestedPw();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwHdlcPayloadTypeSet(pw, cAtPwHdlcPayloadTypeFull));
    }

static eBool VlansAreSame(const tAtEthVlanTag *tag1, const tAtEthVlanTag *tag2)
    {
    if ((tag1->priority == tag2->priority) &&
        (tag1->cfi      == tag2->cfi) &&
        (tag1->vlanId   == tag2->vlanId))
        return cAtTrue;

    return cAtFalse;
    }

static void testCannotConfigureLOPSsetThreshold()
    {
    uint32 i;
    uint32 thresholds[]= {AtPwLopsThresholdMin(TestedPw()),
                          AtPwLopsThresholdMax(TestedPw()) / 2,
                          AtPwLopsThresholdMax(TestedPw())};

    /* But fail when disable */
    AtTestLoggerEnable(cAtFalse);
    for (i = 0; i < mCount(thresholds); i++)
        TEST_ASSERT(AtPwLopsSetThresholdSet(TestedPw(), thresholds[i]) != cAtOk);
    AtTestLoggerEnable(cAtFalse);
    }

static void testCannotConfigureLOPSclrThreshold()
    {
    uint32 i;
    uint32 min = AtPwLopsThresholdMin(TestedPw());
    uint32 max = AtPwLopsThresholdMax(TestedPw());
    uint32 thresholds[] = {min, max / 2, max};

    if (!CanChangeLopsClearThreshold())
        return;

    for (i = 0; i < mCount(thresholds); i++)
        TEST_ASSERT(AtPwLopsClearThresholdSet(TestedPw(), thresholds[i]) !=cAtOk);
    }

static void testCannotToggleReoderOption()
    {
    TEST_ASSERT(AtPwReorderingEnable(TestedPw(), cAtTrue) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwReorderingIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwReorderingEnable(TestedPw(), cAtFalse) != cAtOk);
    }

uint32 AtPwTestMinJitterBufferSizeIn(uint16 payloadSize)
    {
    return AtPwMinJitterBufferSize(TestedPw(), AtPwBoundCircuitGet(TestedPw()), payloadSize);
    }

static void testCannotConfigureJitterBufSize()
    {
    uint32 i, minJbDelay, minJbSize;

    minJbSize = AtPwTestMinJitterBufferSizeIn(AtPwPayloadSizeGet(TestedPw()));
    minJbDelay = mRoundUp(minJbSize, 2);

    TEST_ASSERT(AtPwJitterBufferDelaySet(TestedPw(), minJbDelay) != cAtOk);
    TEST_ASSERT(AtPwJitterBufferSizeSet(TestedPw(),  minJbSize) != cAtOk);

    for (i = minJbSize; i < minJbSize + 5000; i = i + 1000)
        TEST_ASSERT(AtPwJitterBufferSizeSet(TestedPw(), i) != cAtOk);
    }

static void testCannotConfigureJitterBufDelay()
    {
    uint32 i, minJbSize, minJbDelay;

    minJbSize = AtPwTestMinJitterBufferSizeIn(AtPwPayloadSizeGet(TestedPw()));
    minJbDelay = mRoundUp(minJbSize, 2);

    TEST_ASSERT(AtPwJitterBufferSizeSet(TestedPw(), minJbSize) != cAtOk);
    for (i = 0; i < 5; i++)
       {
       uint32 delayInUs = minJbDelay + i * 1000;
       if (delayInUs >= minJbSize)
           break;

       if ((minJbSize - delayInUs) < 500)
           break;

       TEST_ASSERT(AtPwJitterBufferDelaySet(TestedPw(), delayInUs) !=cAtOk);

       if (i == minJbSize)
           break;
       }
    }

static void testCannotToggleRtpOption()
    {
    TEST_ASSERT(AtPwRtpEnable(TestedPw(), cAtTrue) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwRtpIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwRtpEnable(TestedPw(), cAtFalse) != cAtOk);
    }

static void testCannotConfigureRtpPldType()
    {
    uint8 i;
    uint8 pldTypes[] = {96, 100, 127};

    TEST_ASSERT(AtPwRtpEnable(TestedPw(), cAtTrue) != cAtOk);
    TEST_ASSERT(AtPwRtpIsEnabled(TestedPw()) == cAtOk);
    for (i = 0; i < mCount(pldTypes); i++)
        TEST_ASSERT(AtPwRtpPayloadTypeSet(TestedPw(), pldTypes[i]) != cAtOk);

    TEST_ASSERT(AtPwRtpEnable(TestedPw(), cAtFalse) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwRtpIsEnabled(TestedPw()));
    for (i = 0; i < mCount(pldTypes); i++)
        TEST_ASSERT(AtPwRtpPayloadTypeSet(TestedPw(), pldTypes[i]) != cAtOk);
    }

static void testCannotConfigureRtpSsrc()
    {
    eBool rtpEn;
    uint32 ssrc;

    /*Change SRRC when RTP is enable*/
    rtpEn = cAtTrue;
    TEST_ASSERT(AtPwRtpEnable(TestedPw(), rtpEn) != cAtOk);
    TEST_ASSERT( AtPwRtpIsEnabled(TestedPw()) != rtpEn);
    ssrc = 0xFFFE;
    TEST_ASSERT(AtPwRtpSsrcSet(TestedPw(), ssrc) != cAtOk);

    /*Change SRRC when RTP is disable*/
    rtpEn = !rtpEn;
    TEST_ASSERT(AtPwRtpEnable(TestedPw(), rtpEn) != cAtOk);
    TEST_ASSERT(AtPwRtpIsEnabled(TestedPw()) == rtpEn);
    ssrc = 0x1234;
    TEST_ASSERT(AtPwRtpSsrcSet(TestedPw(), ssrc) != cAtOk);
    }

static void TestChangeRtpTimestamp(AtPw pw, eAtPwRtpTimeStampMode mode)
    {
    if (AtPwRtpTimeStampModeIsSupported(pw, mode))
        {
        TEST_ASSERT(AtPwRtpTimeStampModeSet(pw, mode) != cAtOk);
        }
    else
        {
        TEST_ASSERT(AtPwRtpTimeStampModeSet(pw, mode) != cAtOk);
        }
    }

static void testCannotConfigureRtpTimeStampMode()
    {
    eAtPwRtpTimeStampMode modes[] = {cAtPwRtpTimeStampModeAbsolute, cAtPwRtpTimeStampModeDifferential};
    uint8 i;
    AtPw pw = TestedPw();

    TEST_ASSERT(AtPwRtpEnable(pw, cAtTrue) !=cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwRtpIsEnabled(pw));
    for (i = 0; i < mCount(modes); i++)
        TestChangeRtpTimestamp(pw, modes[i]);

    TEST_ASSERT(AtPwRtpEnable(pw, cAtFalse) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwRtpIsEnabled(pw));
    for (i = 0; i < mCount(modes); i++)
        TestChangeRtpTimestamp(pw, modes[i]);
    }

static void testControlWordDefaultOption()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwIsEnabled(TestedPw()));
    }

static void testCanDisableControlWord()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwEnable(TestedPw(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwCwIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    /* But fail when disable */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwEnable(TestedPw(), cAtFalse) == cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotToggleAutoTxLbitOption()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwAutoTxLBitEnable(TestedPw(), cAtTrue) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotToggleAutoRbitOption()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwAutoRBitEnable(TestedPw(), cAtTrue) != cAtOk)
    TEST_ASSERT(AtPwCwAutoRBitEnable(TestedPw(), cAtFalse) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotConfigureSequenceMode()
    {
    eAtPwCwSequenceMode sequenceMode;
    AtPw pw = TestedPw();

    for (sequenceMode = cAtPwCwSequenceModeWrapZero; sequenceMode <= cAtPwCwSequenceModeDisable; sequenceMode++)
        {
        TEST_ASSERT(AtPwCwSequenceModeIsSupported(pw, sequenceMode) == cAtFalse);

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPwCwSequenceModeSet(pw, sequenceMode) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCannotConfigurePktReplaceMode()
    {
    eAtPwPktReplaceMode pktReplaceMode;

    /* Change auto R-bit option when Control word is enable */
    for (pktReplaceMode = cAtPwPktReplaceModePreviousGoodPacket; pktReplaceMode <= cAtPwPktReplaceModeIdleCode; pktReplaceMode++)
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPwCwPktReplaceModeSet(TestedPw(), pktReplaceMode) !=cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCanChangeSourceMac()
    {
    AtPwHdlc pw = (AtPwHdlc)TestedPw();
    uint8 mac[] = {0x26, 0x95, 0x11, 0xB4, 0xD2, 0x27};
    uint8 actualMac[6];
    eAtRet ret = AtPwEthSrcMacSet((AtPw)pw, mac);

    if (ret == cAtErrorModeNotSupport)
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, ret);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthSrcMacGet((AtPw)pw, actualMac));
    TEST_ASSERT(memcmp(actualMac, mac, 6) == 0);
    }

static void testCanSetEthernetHeaderWithMaximumTwoVlans()
    {
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanTag actual_cVlan, actual_sVlan;
    uint8 backupMac[6];
    AtPwHdlc pw = (AtPwHdlc)TestedPw();
    uint32 pwId = AtChannelIdGet((AtChannel)TestedPw());
    AtEthPort ethPort = AtPwEthPortGet(TestedPw());
    uint32 numPws = AtModulePwMaxPwsGet(PwModule());
    AtPwPsn psn;
    uint8 destMac[] = {0x12, 0x34, 0x56, 0x78, 0xAB, 0xCD};
    uint8 actualDestMac[6];

    /* Backup */
    AtPwBackupEthSrcMacSet((AtPw)pw, backupMac);
    psn = (AtPwPsn)AtObjectClone((AtObject)AtPwPsnGet(TestedPw()));

    /* Construct VLANs */
    AtEthVlanTagConstruct(pwId % 8        , 1, VlanId(pwId + numPws),     &cVlan);
    AtEthVlanTagConstruct(((pwId + 1) % 8), 0, VlanId(pwId + numPws + 1), &sVlan);

    /* Let's try with the case when PSN is none */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), NULL));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthDestMacSet((AtPw)pw, destMac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), destMac, &cVlan, &sVlan));

    /* Make sure that header is applied */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthDestMacGet(TestedPw(), actualDestMac));
    TEST_ASSERT(AtOsalMemCmp(actualDestMac, destMac, sizeof(destMac)) == 0);

    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &actual_cVlan));
    TEST_ASSERT(VlansAreSame(&cVlan, &actual_cVlan));

    TEST_ASSERT_NOT_NULL(AtPwEthSVlanGet(TestedPw(), &actual_sVlan));
    TEST_ASSERT(VlansAreSame(&sVlan, &actual_sVlan));

    /* Set back PSN and traffic must work */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), psn));

    /* While PSN exist, try changing to 1 VLAN */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthVlanSet(TestedPw(), &cVlan, NULL));
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &sVlan));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &actual_cVlan));
    TEST_ASSERT(VlansAreSame(&cVlan, &actual_cVlan));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthVlanSet(TestedPw(), &cVlan, &sVlan));

    /* And no VLAN also works */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), NULL));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthVlanSet(TestedPw(), NULL, NULL));
    TEST_ASSERT_NULL(AtPwEthCVlanGet(TestedPw(), &cVlan));
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &sVlan));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), psn));
    AtObjectDelete((AtObject)psn);

    /* Revert previous PSN configuration */
    TEST_ASSERT(AtEthPortSourceMacAddressSet(ethPort, backupMac) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), backupMac, &cVlan, &sVlan));
    }

static void testCannotSetEthernetHeaderWithNullDMac()
    {
    tAtEthVlanTag cVlan, sVlan;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT((AtPwEthHeaderSet(TestedPw(), NULL, AtEthVlanTagConstruct(1, 1, 3, &cVlan), AtEthVlanTagConstruct(1, 1, 3, &sVlan)) != cAtOk), cAtTrue);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCircuitCannotBeUnbound()
    {
    TEST_ASSERT(AtPwCircuitUnbind(TestedPw()) != cAtOk)
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPwHdlc_Fixtures)
        {
        new_TestFixture("testCanChangePayloadType", testCanChangePayloadType),
        new_TestFixture("testCannotConfigureLOPSsetThreshold", testCannotConfigureLOPSsetThreshold),
        new_TestFixture("testCannotConfigureLOPSclrThreshold", testCannotConfigureLOPSclrThreshold),
        new_TestFixture("testCannotToggleReoderOption", testCannotToggleReoderOption),
        new_TestFixture("testCannotConfigureJitterBufSize", testCannotConfigureJitterBufSize),
        new_TestFixture("testCannotConfigureJitterBufDelay", testCannotConfigureJitterBufDelay),
        new_TestFixture("testCannotToggleRtpOption", testCannotToggleRtpOption),
        new_TestFixture("testCannotConfigureRtpPldType", testCannotConfigureRtpPldType),
        new_TestFixture("testCannotConfigureRtpSsrc", testCannotConfigureRtpSsrc),
        new_TestFixture("testCannotConfigureRtpTimeStampMode", testCannotConfigureRtpTimeStampMode),
        new_TestFixture("testControlWordDefaultOption", testControlWordDefaultOption),
        new_TestFixture("testCanDisableControlWord", testCanDisableControlWord),
        new_TestFixture("testCannotToggleAutoTxLbitOption", testCannotToggleAutoTxLbitOption),
        new_TestFixture("testCannotToggleAutoRbitOption", testCannotToggleAutoRbitOption),
        new_TestFixture("testCannotConfigureSequenceMode", testCannotConfigureSequenceMode),
        new_TestFixture("testCannotConfigurePktReplaceMode", testCannotConfigurePktReplaceMode),
        new_TestFixture("testCanChangeSourceMac", testCanChangeSourceMac),
        new_TestFixture("testCanSetEthernetHeaderWithMaximumTwoVlans", testCanSetEthernetHeaderWithMaximumTwoVlans),
        new_TestFixture("testCannotSetEthernetHeaderWithNullDMac", testCannotSetEthernetHeaderWithNullDMac),
        new_TestFixture("testCircuitCannotBeUnbound", testCircuitCannotBeUnbound),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPwHdlc_Caller, "TestSuite_AtPwHdlc", NULL, NULL, TestSuite_AtPwHdlc_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPwHdlc_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwHdlcTestRunner);
    }

AtChannelTestRunner AtPwHdlcTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPwHdlcTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPwHdlcTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }


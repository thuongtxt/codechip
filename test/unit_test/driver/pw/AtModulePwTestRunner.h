/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtModulePwTestRunner.h
 * 
 * Created Date: Mar 5, 2015
 *
 * Description : PW module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPWTESTRUNNER_H_
#define _ATMODULEPWTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunner.h" /* Super class */
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePwTestRunner * AtModulePwTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AtModulePwTestRunnerCanChangeLopsClearThreshold(AtModulePwTestRunner self);
eBool AtModulePwTestRunnerAlwaysUse2Vlans(AtModulePwTestRunner self);

void AtModulePwTestSetupCepBasicWithCircuits(AtList pws, char *pwString, char *circuitString);
void AtModulePwTestSetupSAToPWithCircuits(AtList pws, char *pwString, char *circuitString);

eBool AtModulePwTestCepCanTestOnLine(AtModulePwTestRunner self, AtSdhLine line);
void AtModulePwTestWillTestStmLine(AtModulePwTestRunner self, uint8 lineId);
AtEthPort AtModulePwTestEthPortForSdhChannel(AtModulePwTestRunner self, AtSdhChannel channel);
AtEthPort AtModulePwTestEthPortForPdhChannel(AtModulePwTestRunner self, AtPdhChannel channel);

eAtRet AtModulePwTestRunnerEthLoopback(AtModulePwTestRunner self, AtEthPort ethPort);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPWTESTRUNNER_H_ */


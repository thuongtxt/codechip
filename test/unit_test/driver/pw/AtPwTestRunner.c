/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwTestRunner.c
 *
 * Created Date: Jun 19, 2014
 *
 * Description : PW test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModulePwTests.h"
#include "AtPdhNxDs0.h"
#include "AtPwTestRunnerInternal.h"
#include "AtModulePwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#define cStatusCheckingTimeoutMs 1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;
static tAtChannelTestRunnerMethods m_AtChannelTestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw TestedPw()
    {
    return (AtPw)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void ClearStatus(AtPw pw)
    {
    AtPwCounters counters = NULL;
    AtChannelAllCountersClear((AtChannel)pw, &counters);
    }

static eBool ShouldReEnablePwAfterBinding(void)
    {
    /* TODO: Remove this after testing */
    return cAtFalse;

    /* TODO: This must be set to cAtFalse. Just because the current software has
     * a bug that does not enable PW after unbind/bind */
    return cAtTrue;
    }

static void PwReEnable(void)
    {
    if (ShouldReEnablePwAfterBinding())
        AtAssert(AtChannelEnable((AtChannel)TestedPw(), cAtTrue) == cAtOk);
    }

static eBool CepCircuitHasCriticalAlarms(AtPw pw)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    AtSdhChannel auTu = AtSdhChannelParentChannelGet(vc);
    uint32 alarms = 0;

    alarms |= AtChannelAlarmGet((AtChannel)vc);
    alarms |= AtChannelAlarmGet((AtChannel)auTu);

    if (alarms & (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop))
        return cAtTrue;

    return cAtFalse;
    }

static eBool De1HasCriticalAlarms(AtPdhDe1 de1)
    {
    uint32 alarms = AtChannelAlarmGet((AtChannel)de1);

    if ((alarms & (cAtPdhDe1AlarmLos | cAtPdhDe1AlarmLof | cAtPdhDe1AlarmAis)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SAToPCircuitHasCriticalAlarms(AtPw pw)
    {
    return De1HasCriticalAlarms((AtPdhDe1)AtPwBoundCircuitGet(pw));
    }

static eBool CESoPCircuitHasCriticalAlarms(AtPw pw)
    {
    return De1HasCriticalAlarms(AtPdhNxDS0De1Get((AtPdhNxDS0)AtPwBoundCircuitGet(pw)));
    }

static eBool CircuitHasCriticalAlarms(AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);

    if (pwType == cAtPwTypeCEP)   return CepCircuitHasCriticalAlarms(pw);
    if (pwType == cAtPwTypeSAToP) return SAToPCircuitHasCriticalAlarms(pw);
    if (pwType == cAtPwTypeCESoP) return CESoPCircuitHasCriticalAlarms(pw);

    return cAtTrue;
    }

static eBool PwCountersAreNormal(AtPw pw)
    {
    AtPwCounters counters       = NULL;
    AtPwTdmCounters tdmCounters = NULL;

    if (AtTestIsSimulationTesting())
        return cAtTrue;

    if ((AtChannelAllCountersGet((AtChannel)TestedPw(), &counters) != cAtOk) || (counters == NULL))
        return cAtFalse;

    /* At least, we have good counters */
    if ((AtPwCountersTxPacketsGet(counters)      == 0) ||
        (AtPwCountersTxPayloadBytesGet(counters) == 0) ||
        (AtPwCountersRxPacketsGet(counters)      == 0) ||
        (AtPwCountersRxPayloadBytesGet(counters) == 0))
        return cAtFalse;

    /* L-Bit counters must be correct */
    tdmCounters = (AtPwTdmCounters)counters;
    if (CircuitHasCriticalAlarms(pw))
        {
        eBool autoLbit    = (AtPwCwIsEnabled(pw) && AtPwCwAutoTxLBitIsEnabled(pw)) ? cAtTrue : cAtFalse;
        eBool notHaveLbit = (AtPwTdmCountersTxLbitPacketsGet(tdmCounters) == 0) || (AtPwTdmCountersRxLbitPacketsGet(tdmCounters) == 0);
        if (autoLbit && notHaveLbit)
            return cAtFalse;
        }
    else
        {
        if ((AtPwTdmCountersTxLbitPacketsGet(tdmCounters) != 0) ||
            (AtPwTdmCountersRxLbitPacketsGet(tdmCounters) != 0))
            return cAtFalse;
        }

    /* Other abnormal counters must also be clear */
    if ((AtPwCountersRxDiscardedPacketsGet(counters)     != 0) ||
        (AtPwCountersRxMalformedPacketsGet(counters)     != 0) ||
        (AtPwCountersRxLostPacketsGet(counters)          != 0) ||
        (AtPwCountersRxOutOfSeqDropPacketsGet(counters)  != 0) ||
        (AtPwTdmCountersRxJitBufOverrunGet(tdmCounters)  != 0) ||
        (AtPwTdmCountersRxJitBufUnderrunGet(tdmCounters) != 0) ||
        (AtPwTdmCountersRxLopsGet(tdmCounters)           != 0))
        return cAtFalse;

    return cAtTrue;
    }

static void RemoveAllVlans(void)
    {
    uint8 mac[6];
    tAtEthVlanTag tag;
    AtModulePwTestRunner moduleRunner = (AtModulePwTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());

    if (AtModulePwTestRunnerAlwaysUse2Vlans(moduleRunner))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthDestMacGet(TestedPw(), mac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), mac, NULL, NULL));
    TEST_ASSERT_NULL(AtPwEthCVlanGet(TestedPw(), &tag));
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &tag));
    }

static eAtRet EthPortLoopback(AtPwTestRunner self, AtEthPort port)
    {
    AtModulePwTestRunner moduleRunner = (AtModulePwTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)self);
    return AtModulePwTestRunnerEthLoopback(moduleRunner, port);
    }

static void setUp()
    {
    eAtRet ret = cAtOk;
    AtPwTestRunner runner = (AtPwTestRunner)AtUnittestRunnerCurrentRunner();

    TEST_ASSERT_NOT_NULL(TestedPw());

    ret = EthPortLoopback(runner, AtPwEthPortGet(TestedPw()));
    if ((ret != cAtErrorModeNotSupport) && (ret != cAtErrorNotApplicable))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, ret);
        }

    /* Let's remove all of VLANs */
    RemoveAllVlans();
    }

static void testCanChangeLOPSsetThreshold()
    {
    uint32 i;
    uint32 thresholds[]= {AtPwLopsThresholdMin(TestedPw()),
                          AtPwLopsThresholdMax(TestedPw()) / 2,
                          AtPwLopsThresholdMax(TestedPw())};

    for (i = 0; i < mCount(thresholds); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwLopsSetThresholdSet(TestedPw(), thresholds[i]));
        TEST_ASSERT_EQUAL_INT(thresholds[i], AtPwLopsSetThresholdGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }

    /* Out-of-range */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwLopsSetThresholdSet(TestedPw(), AtPwLopsThresholdMax(TestedPw()) + 1) != cAtOk);
    TEST_ASSERT(AtPwLopsSetThresholdSet(TestedPw(), 0) != cAtOk);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanChangeLOPSsetThresholdWithNoCircuit()
    {
    uint32 i;
    AtChannel boundCircuit = AtPwBoundCircuitGet(TestedPw());
    uint32 min = AtPwLopsThresholdMin(TestedPw());
    uint32 max = AtPwLopsThresholdMax(TestedPw());
    uint32 thresholds[] = {min, max / 2, max};

    for (i = 0; i < mCount(thresholds); i++)
        {
        /* Unbind and apply new configuration */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitUnbind(TestedPw()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwLopsSetThresholdSet(TestedPw(), thresholds[i]));
        TEST_ASSERT_EQUAL_INT(thresholds[i], AtPwLopsSetThresholdGet(TestedPw()));

        /* After binding, this configuration must remain unchanged */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitBind(TestedPw(), boundCircuit));
        PwReEnable();
        TEST_ASSERT_EQUAL_INT(thresholds[i], AtPwLopsSetThresholdGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }

    /* Out of range case */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwLopsSetThresholdSet(TestedPw(), max + 1) != cAtOk);
    TEST_ASSERT(AtPwLopsSetThresholdSet(TestedPw(), 0)  != cAtOk);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    AtTestLoggerEnable(cAtTrue);
    }

static eBool CanChangeLopsClearThreshold(void)
    {
    AtModuleTestRunner moduleRunner = AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    return AtModulePwTestRunnerCanChangeLopsClearThreshold((AtModulePwTestRunner)moduleRunner);
    }

static void testCanChangeLOPSclrThreshold()
    {
    uint32 i;
    uint32 min = AtPwLopsThresholdMin(TestedPw());
    uint32 max = AtPwLopsThresholdMax(TestedPw());
    uint32 thresholds[] = {min, max / 2, max};

    if (!CanChangeLopsClearThreshold())
        return;

    for (i = 0; i < mCount(thresholds); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwLopsClearThresholdSet(TestedPw(), thresholds[i]));
        TEST_ASSERT_EQUAL_INT(thresholds[i], AtPwLopsClearThresholdGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwLopsClearThresholdSet(TestedPw(), max + 1) != cAtOk);
    TEST_ASSERT(AtPwLopsClearThresholdSet(TestedPw(), 0)  != cAtOk);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanChangeLOPSclrThresholdWithNoCircuit()
    {
    uint32 i;
    AtChannel boundCircuit = AtPwBoundCircuitGet(TestedPw());
    uint32 min = AtPwLopsThresholdMin(TestedPw());
    uint32 max = AtPwLopsThresholdMax(TestedPw());
    uint32 thresholds[] = {min, max / 2, max};

    if (!CanChangeLopsClearThreshold())
        return;

    for (i = 0; i < mCount(thresholds); i++)
        {
        /* Unbind and apply new configuration */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitUnbind(TestedPw()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwLopsClearThresholdSet(TestedPw(), thresholds[i]));
        TEST_ASSERT_EQUAL_INT(thresholds[i], AtPwLopsClearThresholdGet(TestedPw()));

        /* After binding, this configuration must remain unchanged */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitBind(TestedPw(), boundCircuit));
        PwReEnable();
        TEST_ASSERT_EQUAL_INT(thresholds[i], AtPwLopsClearThresholdGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }

    /* Out of range case */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwLopsClearThresholdSet(TestedPw(), max + 1) != cAtOk);
    TEST_ASSERT(AtPwLopsClearThresholdSet(TestedPw(), 0)  != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

static void testCannotChangeJitterBufferSizeLowerThanMinimumOne()
    {
    uint16 minBufferSizeUs, minJitterDelayUs;
    uint32 currentBufferSizeUs = AtPwJitterBufferSizeGet(TestedPw());
    uint32 currentJitterDelayUs = AtPwJitterBufferDelayGet(TestedPw());
    uint16 payloadSize = AtPwPayloadSizeGet(TestedPw());

    minJitterDelayUs = AtPwTestMinJitterDelayInUs(payloadSize);
    minBufferSizeUs = AtPwTestMinJitterBufferSizeInUs(payloadSize);

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwJitterBufferSizeSet(TestedPw(), minBufferSizeUs - 1) != cAtOk);
    AtTestLoggerEnable(cAtTrue);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), minJitterDelayUs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), minBufferSizeUs));
    TEST_ASSERT(AtPwJitterBufferSizeGet(TestedPw()) >= minBufferSizeUs);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), currentBufferSizeUs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), currentJitterDelayUs));
    }

static void testCannotChangeJitterDelayLowerThanMinimumOne()
    {
    uint16 minBufferDelayUs;
    uint16 currentJitterDelayUs = AtPwJitterBufferDelayGet(TestedPw());

    minBufferDelayUs = AtPwMinJitterDelay(TestedPw(), AtPwBoundCircuitGet(TestedPw()), AtPwPayloadSizeGet(TestedPw()));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwJitterBufferDelaySet(TestedPw(), minBufferDelayUs - 100) != cAtOk);
    AtTestLoggerEnable(cAtTrue);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), minBufferDelayUs));
    TEST_ASSERT(AtPwJitterBufferDelayGet(TestedPw()) >= minBufferDelayUs);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), currentJitterDelayUs));
    }

static void testCannotChangeJitterBufferSizeBiggerThanMaximumOne()
    {
    uint32 maxBufferSizeUs;
    uint16 currentBufferSize = AtPwJitterBufferSizeGet(TestedPw());

    maxBufferSizeUs = AtPwMaxJitterBufferSize(TestedPw(), AtPwBoundCircuitGet(TestedPw()), AtPwPayloadSizeGet(TestedPw()));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwJitterBufferSizeSet(TestedPw(), maxBufferSizeUs + 1) != cAtOk);
    AtTestLoggerEnable(cAtTrue);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), maxBufferSizeUs));
    TEST_ASSERT(AtPwJitterBufferSizeGet(TestedPw()) <= maxBufferSizeUs);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), currentBufferSize));
    }

static void testJitterBufferConstrainMustBeValid()
    {
    AtPw pw = TestedPw();
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint16 payloadSize = AtPwPayloadSizeGet(pw);

    TEST_ASSERT(AtPwMaxJitterBufferSize(pw, circuit, payloadSize) >= AtPwMaxJitterDelay(pw, circuit, payloadSize));
    TEST_ASSERT(AtPwMinJitterBufferSize(pw, circuit, payloadSize) >= AtPwMinJitterDelay(pw, circuit, payloadSize));
    }

/* Can enable/disable reordering */
static void testCanToggleReoderOption()
    {
    eBool reoder;

    reoder = cAtTrue;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwReorderingEnable(TestedPw(), reoder));
    TEST_ASSERT_EQUAL_INT(reoder, AtPwReorderingIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    reoder = !reoder;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwReorderingEnable(TestedPw(), reoder));
    TEST_ASSERT_EQUAL_INT(reoder, AtPwReorderingIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    reoder = !reoder;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwReorderingEnable(TestedPw(), reoder));
    TEST_ASSERT_EQUAL_INT(reoder, AtPwReorderingIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

/* Can change jitter buffer size */
static void testChangeJitterBufSize()
    {
    uint32 i, minJbDelay, minJbSize;
    uint16 payloadSize = AtPwPayloadSizeGet(TestedPw());

    minJbSize = AtPwTestMinJitterBufferSizeInUs(payloadSize);
    minJbDelay = AtPwTestMinJitterDelayInUs(payloadSize);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), minJbDelay));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(),  minJbSize));

    for (i = minJbSize; i < minJbSize + 5000; i = i + 1000)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), i));
        TEST_ASSERT(AtPwJitterBufferSizeGet(TestedPw()) >= i);
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

static void testChangeJitterBufferSizeWhenNoCircuit()
    {
    uint32 i, minJbSize, minJbDelay;
    AtChannel boundCircuit = AtPwBoundCircuitGet(TestedPw());

    minJbSize = AtPwTestMinJitterBufferSizeInUs(AtPwPayloadSizeGet(TestedPw()));
    minJbDelay = AtPwTestMinJitterDelayInUs(AtPwPayloadSizeGet(TestedPw()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), minJbDelay));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(),  minJbSize));

    for (i = minJbSize; i < (minJbSize + 5000); i = i + 1000)
        {
        /* Unbind and apply new configuration */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitUnbind(TestedPw()));

        if (AtPwJitterBufferSizeGet(TestedPw()) > i)
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), i));
        TEST_ASSERT(AtPwJitterBufferSizeGet(TestedPw()) >= i);

        /* After binding, this configuration must remain unchanged */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitBind(TestedPw(), boundCircuit));
        PwReEnable();
        TEST_ASSERT(AtPwJitterBufferSizeGet(TestedPw()) >= i);
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

static void testChangeJitterBufDelay()
    {
    uint32 i, minJbSize, minJbDelay;
    uint16 payloadSize = AtPwPayloadSizeGet(TestedPw());

    minJbSize = AtPwTestMinJitterBufferSizeInUs(payloadSize);
    minJbDelay = AtPwTestMinJitterDelayInUs(payloadSize);

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), minJbDelay));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), minJbSize));
    for (i = 0; i < 5; i++)
        {
        uint32 delayInUs = minJbDelay + i * 1000;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), delayInUs));
        TEST_ASSERT(AtPwJitterBufferDelayGet(TestedPw()) >= delayInUs);
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

static void testChangeJitterBufDelayWhenNoCircuit()
    {
    uint32 i, minJbSize, minJbDelay;
    AtChannel boundCircuit = AtPwBoundCircuitGet(TestedPw());

    minJbSize = AtPwTestMinJitterBufferSizeInUs(AtPwPayloadSizeGet(TestedPw()));
    minJbDelay = AtPwTestMinJitterDelayInUs(AtPwPayloadSizeGet(TestedPw()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), minJbDelay));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferSizeSet(TestedPw(), minJbSize));

    for (i = 0; i < 5; i++)
        {
        uint32 delayInUs = minJbDelay + i * 1000;

        /* Unbind and apply new configuration */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitUnbind(TestedPw()));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwJitterBufferDelaySet(TestedPw(), delayInUs));

        /* After binding, this configuration must remain unchanged */
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitBind(TestedPw(), boundCircuit));
        PwReEnable();
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

/* Can enable/disable RTP */
static void testToggleRtpOption()
    {
    eBool rtpEn;

    rtpEn = cAtTrue;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    rtpEn = !rtpEn;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    rtpEn = !rtpEn;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

/* Can change RTP payload type regardless RTP is enabled or not */
static void testChangeRtpPldType()
    {
    eBool rtpEn;
    uint8 i;
    uint8 pldTypes[] = {96, 100, 127};

    /* Change payload type when RTP is enable */
    rtpEn = cAtTrue;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    for (i = 0; i < mCount(pldTypes); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpPayloadTypeSet(TestedPw(), pldTypes[i]));
        TEST_ASSERT_EQUAL_INT(pldTypes[i], AtPwRtpPayloadTypeGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }

    /* Change payload type when RTP is disable */
    rtpEn = !rtpEn;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    for (i = 0; i < mCount(pldTypes); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpPayloadTypeSet(TestedPw(), pldTypes[i]));
        TEST_ASSERT_EQUAL_INT(pldTypes[i], AtPwRtpPayloadTypeGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

/* Can change RTP SRRC regardless RTP is enabled or not */
static void testChangeRtpSsrc()
    {
    eBool rtpEn;
    uint32 ssrc;

    /* Change SRRC when RTP is enable */
    rtpEn = cAtTrue;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    ssrc = 0xFFFE;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpSsrcSet(TestedPw(), ssrc));
    TEST_ASSERT_EQUAL_INT(ssrc, AtPwRtpSsrcGet(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    /* Change SRRC when RTP is disable */
    rtpEn = !rtpEn;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(TestedPw(), rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(TestedPw()));
    ssrc = 0x1234;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpSsrcSet(TestedPw(), ssrc));
    TEST_ASSERT_EQUAL_INT(ssrc, AtPwRtpSsrcGet(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

static void TestChangeRtpTimestamp(AtPw pw, eAtPwRtpTimeStampMode mode)
    {
    if (AtPwRtpTimeStampModeIsSupported(pw, mode))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpTimeStampModeSet(pw, mode));
        TEST_ASSERT_EQUAL_INT(mode, AtPwRtpTimeStampModeGet(pw));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPwRtpTimeStampModeSet(pw, mode) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testChangeRtpTimeStampMode()
    {
    eBool rtpEn;
    eAtPwRtpTimeStampMode modes[] = {cAtPwRtpTimeStampModeAbsolute, cAtPwRtpTimeStampModeDifferential};
    uint8 i;
    AtPw pw = TestedPw();

    /* Change time stamp mode when RTP is enable */
    rtpEn = cAtTrue;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(pw, rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(pw));
    for (i = 0; i < mCount(modes); i++)
        TestChangeRtpTimestamp(pw, modes[i]);

    /* Change time stamp mode when RTP is disable */
    rtpEn = !rtpEn;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwRtpEnable(pw, rtpEn));
    TEST_ASSERT_EQUAL_INT(rtpEn, AtPwRtpIsEnabled(pw));
    for (i = 0; i < mCount(modes); i++)
        TestChangeRtpTimestamp(pw, modes[i]);
    }

/* Control Word must be enabled as default for SAToP, CESoP, CEP */
static void testControlWordDefaultOption()
    {
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwCwIsEnabled(TestedPw()));
    }

static void testCannotDisableControlWordForTdmPw()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwEnable(TestedPw(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwCwIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    /* But fail when disable */
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwEnable(TestedPw(), cAtFalse) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

/* Can enable/disable auto TX L-bit regardless Control Word is enabled.
 * Note, not all of products support disabling forwarding options */
static void testToggleAutoTxLbitOption()
    {
    /* Change auto TX L-bit option when Control word is enable */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwAutoTxLBitEnable(TestedPw(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwCwAutoTxLBitIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    if (AtPwCwAutoTxLBitEnable(TestedPw(), cAtFalse) == cAtOk)
        {
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwCwAutoTxLBitIsEnabled(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwAutoTxLBitEnable(TestedPw(), cAtTrue));
    }

static void testCannotDisableRxLbitOption()
    {
    AtPw pw = TestedPw();

    /* Can enable/disable auto RX L-bit regardless Control Word is enabled.
     * Note: not all of product can support disabling control word or disable auto
     * L-Bit */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwAutoRxLBitEnable(pw, cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwCwAutoRxLBitIsEnabled(pw));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));

    if (AtPwCwAutoRxLBitCanEnable(pw, cAtFalse))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwAutoRxLBitEnable(pw, cAtFalse));
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwCwAutoRxLBitIsEnabled(pw));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPwCwAutoRxLBitEnable(pw, cAtFalse) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }

    TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));
    }

/* Can enable/disable auto R-bit regardless Control Word is enabled */
static void testToggleAutoRbitOption()
    {
    /* Change auto R-bit option when Control word is enable */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwAutoRBitEnable(TestedPw(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtPwCwAutoRBitIsEnabled(TestedPw()));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    if (AtPwCwAutoRBitEnable(TestedPw(), cAtFalse) == cAtOk)
        {
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtPwCwAutoRBitIsEnabled(TestedPw()));
        }
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwAutoRBitEnable(TestedPw(), cAtTrue));
    }

/* Can change sequence mode regardless Control Word is enabled */
static void testChangeSequenceMode()
    {
    eBool               reorderEn;
    eAtPwCwSequenceMode sequenceMode;
    AtPw pw = TestedPw();

    /* Change auto R-bit option when Control word is enable */
    reorderEn = cAtTrue;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwReorderingEnable(pw, reorderEn));
    TEST_ASSERT_EQUAL_INT(reorderEn, AtPwReorderingIsEnabled(pw));

    for (sequenceMode = cAtPwCwSequenceModeWrapZero; sequenceMode <= cAtPwCwSequenceModeDisable; sequenceMode++)
        {
        if (sequenceMode == cAtPwCwSequenceModeDisable)
            {
            reorderEn = !reorderEn;
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwReorderingEnable(pw, reorderEn));
            TEST_ASSERT_EQUAL_INT(reorderEn, AtPwReorderingIsEnabled(pw));
            TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));
            }

        if (AtPwCwSequenceModeIsSupported(pw, sequenceMode))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwSequenceModeSet(pw, sequenceMode));
            TEST_ASSERT_EQUAL_INT(sequenceMode, AtPwCwSequenceModeGet(pw));
            TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtPwCwSequenceModeSet(pw, sequenceMode) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }

    reorderEn = !reorderEn;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwReorderingEnable(pw, reorderEn));
    TEST_ASSERT_EQUAL_INT(reorderEn, AtPwReorderingIsEnabled(pw));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));
    }

/* Cannot change invalid sequence mode */
static void testCannotChangeInvalidSequenceMode()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwSequenceModeSet(TestedPw(), cAtPwCwSequenceModeInvalid) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

/* Can change length mode regardless Control Word is enabled or not */
static void testChangeLengthMode()
    {
    eAtPwCwLengthMode lengthMode;
    AtPw pw = TestedPw();

    /* Change CW length mode option when Control word is enable */
    for (lengthMode = cAtPwCwLengthModeFullPacket; lengthMode <= cAtPwCwLengthModePayload; lengthMode++)
        {
        if (AtPwCwLengthModeIsSupported(pw, lengthMode))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwLengthModeSet(pw, lengthMode));
            TEST_ASSERT_EQUAL_INT(lengthMode, AtPwCwLengthModeGet(pw));
            TEST_ASSERT(AtPwTestClearAndCheckStatus(pw, cStatusCheckingTimeoutMs));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtPwCwLengthModeSet(pw, lengthMode) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

/* Cannot change invalid length mode */
static void testCannotChangeInvalidLengthMode()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwLengthModeSet(TestedPw(), cAtPwCwLengthModeInvalid) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

/* Can change packet replacing mode regardless Control Word is enabled or not */
static void testChangePktReplaceMode()
    {
    eAtPwPktReplaceMode pktReplaceMode;

    /* Change auto R-bit option when Control word is enable */
    for (pktReplaceMode = cAtPwPktReplaceModePreviousGoodPacket; pktReplaceMode <= cAtPwPktReplaceModeIdleCode; pktReplaceMode++)
        {
        if (!AtPwPktReplaceModeIsSupported(TestedPw(), pktReplaceMode))
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCwPktReplaceModeSet(TestedPw(), pktReplaceMode));
        TEST_ASSERT_EQUAL_INT(pktReplaceMode, AtPwCwPktReplaceModeGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

static void testChangeLopsPktReplaceMode()
    {
    eAtPwPktReplaceMode pktReplaceMode;

    /* Change auto R-bit option when Control word is enable */
    for (pktReplaceMode = cAtPwPktReplaceModePreviousGoodPacket; pktReplaceMode <= cAtPwPktReplaceModeIdleCode; pktReplaceMode++)
        {
        if (!AtPwPktReplaceModeIsSupported(TestedPw(), pktReplaceMode))
            continue;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwLopsPktReplaceModeSet(TestedPw(), pktReplaceMode));
        TEST_ASSERT_EQUAL_INT(pktReplaceMode, AtPwLopsPktReplaceModeGet(TestedPw()));
        TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
        }
    }

/* Cannot change invalid packet replacing mode */
static void testCannotChangeInvalidPktReplaceMode()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPwCwPktReplaceModeSet(TestedPw(), cAtPwPktReplaceModeInvalid) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static AtPwPsn MplsPsnCreate(AtPw pw)
    {
    static uint32 labelCount = 1;
    uint32 labelValue;
    AtPwMplsPsn newPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    labelValue = AtChannelIdGet((AtChannel)pw) + labelCount + 1;
    AtPwMplsPsnInnerLabelSet(newPsn, AtPwMplsLabelMake(labelValue, labelValue % 8, labelValue % 255, &label));
    AtPwMplsPsnExpectedLabelSet(newPsn, labelValue);
    labelCount = labelCount + 1;

    return (AtPwPsn)newPsn;
    }

static AtPwPsn MefPsnCreate(AtPw pw)
    {
    static uint32 ecIdCount = 1000;
    uint32 ecId;
    AtPwMefPsn newPsn = AtPwMefPsnNew();

    ecId = AtChannelIdGet((AtChannel)pw) + ecIdCount;
    AtPwMefPsnTxEcIdSet(newPsn, ecId);
    AtPwMefPsnExpectedEcIdSet(newPsn, ecId);
    ecIdCount = ecIdCount + 1;

    return (AtPwPsn)newPsn;
    }

static void testCanChangePsn()
    {
    AtPwPsn backupPsn, newPsn, pwPsn;
    backupPsn = (AtPwPsn)AtObjectClone((AtObject)AtPwPsnGet(TestedPw()));

    /* MPLS */
    newPsn = MplsPsnCreate(TestedPw());
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), newPsn));
    AtObjectDelete((AtObject)newPsn);
    pwPsn = AtPwPsnGet(TestedPw());
    TEST_ASSERT_EQUAL_INT(cAtPwPsnTypeMpls, AtPwPsnTypeGet(pwPsn));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    /* MEF */
    newPsn = MefPsnCreate(TestedPw());
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), newPsn));
    AtObjectDelete((AtObject)newPsn);
    pwPsn = AtPwPsnGet(TestedPw());
    TEST_ASSERT_EQUAL_INT(cAtPwPsnTypeMef, AtPwPsnTypeGet(pwPsn));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    /* Recover the previous one */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), backupPsn));
    AtObjectDelete((AtObject)backupPsn);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

/* Can change Ethernet port */
static void testCanChangeEthernetPort()
    {
    AtEthPort port = AtPwEthPortGet(TestedPw());
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthPortSet(TestedPw(), NULL));
    TEST_ASSERT(AtPwEthPortGet(TestedPw()) == NULL);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthPortSet(TestedPw(), port));
    TEST_ASSERT(AtPwEthPortGet(TestedPw()) == port);
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

static eBool VlansAreSame(const tAtEthVlanTag *tag1, const tAtEthVlanTag *tag2)
    {
    if ((tag1->priority == tag2->priority) &&
        (tag1->cfi      == tag2->cfi) &&
        (tag1->vlanId   == tag2->vlanId))
        return cAtTrue;

    return cAtFalse;
    }

static AtModulePw PwModule()
    {
    return (AtModulePw)AtChannelModuleGet((AtChannel)TestedPw());
    }

static void testConfigureEthHeaderBeforePsn(void)
    {
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanTag cVlan_actual, sVlan_actual;
    uint8 mac[] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
    AtPwPsn originalPsn = (AtPwPsn)AtObjectClone((AtObject)AtPwPsnGet(TestedPw()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), NULL));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), mac, AtEthVlanTagConstruct(1, 1, 1, &cVlan), AtEthVlanTagConstruct(2, 1, 2, &sVlan)));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &cVlan_actual));
    TEST_ASSERT(VlansAreSame(&cVlan, &cVlan_actual));
    TEST_ASSERT_NOT_NULL(AtPwEthSVlanGet(TestedPw(), &sVlan_actual));
    TEST_ASSERT(VlansAreSame(&sVlan, &sVlan_actual));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), originalPsn));
    AtObjectDelete((AtObject)originalPsn);
    }

static uint32 VlanId(uint32 value)
    {
    return value % 4096;
    }

static void testCanSetEthernetHeaderWithMaximumTwoVlans()
    {
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanTag actual_cVlan, actual_sVlan;
    uint8 backupMac[6];
    uint32 pwId = AtChannelIdGet((AtChannel)TestedPw());
    AtEthPort ethPort = AtPwEthPortGet(TestedPw());
    uint32 numPws = AtModulePwMaxPwsGet(PwModule());
    AtPwPsn psn;
    uint8 destMac[] = {0x12, 0x34, 0x56, 0x78, 0xAB, 0xCD};
    uint8 actualDestMac[6];
    AtModulePwTestRunner moduleRunner = (AtModulePwTestRunner)AtChannelTestRunnerModuleRunnerGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());

    /* Backup */
    AtEthPortSourceMacAddressGet(ethPort, backupMac);
    psn = (AtPwPsn)AtObjectClone((AtObject)AtPwPsnGet(TestedPw()));

    /* Construct VLANs */
    AtEthVlanTagConstruct(pwId % 8        , 1, VlanId(pwId + numPws),     &cVlan);
    AtEthVlanTagConstruct(((pwId + 1) % 8), 0, VlanId(pwId + numPws + 1), &sVlan);

    /* Let's try with the case when PSN is none */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), NULL));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortSourceMacAddressSet(ethPort, destMac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), destMac, &cVlan, &sVlan));

    /* Make sure that header is applied */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthDestMacGet(TestedPw(), actualDestMac));
    TEST_ASSERT(AtOsalMemCmp(actualDestMac, destMac, sizeof(destMac)) == 0);

    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &actual_cVlan));
    TEST_ASSERT(VlansAreSame(&cVlan, &actual_cVlan));

    TEST_ASSERT_NOT_NULL(AtPwEthSVlanGet(TestedPw(), &actual_sVlan));
    TEST_ASSERT(VlansAreSame(&sVlan, &actual_sVlan));

    /* Set back PSN and traffic must work */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), psn));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));

    if (AtModulePwTestRunnerAlwaysUse2Vlans(moduleRunner))
        {
        AtObjectDelete((AtObject)psn);
        return;
        }

    /* While PSN exist, try changing to 1 VLAN */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthVlanSet(TestedPw(), &cVlan, NULL));
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &sVlan));
    TEST_ASSERT_NOT_NULL(AtPwEthCVlanGet(TestedPw(), &actual_cVlan));
    TEST_ASSERT(VlansAreSame(&cVlan, &actual_cVlan));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthVlanSet(TestedPw(), &cVlan, &sVlan));

    /* And no VLAN also works */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), NULL));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthVlanSet(TestedPw(), NULL, NULL));
    TEST_ASSERT_NULL(AtPwEthCVlanGet(TestedPw(), &cVlan));
    TEST_ASSERT_NULL(AtPwEthSVlanGet(TestedPw(), &sVlan));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnSet(TestedPw(), psn));
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    AtObjectDelete((AtObject)psn);

    /* Revert previous PSN configuration */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtEthPortSourceMacAddressSet(ethPort, backupMac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwEthHeaderSet(TestedPw(), backupMac, &cVlan, &sVlan));

    /* Traffic must work */
    TEST_ASSERT(AtPwTestClearAndCheckStatus(TestedPw(), cStatusCheckingTimeoutMs));
    }

/* Cannot set Ethernet header with NULL DMAC */
static void testCannotSetEthernetHeaderWithNullDMac()
    {
    tAtEthVlanTag cVlan, sVlan;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT((AtPwEthHeaderSet(TestedPw(), NULL, AtEthVlanTagConstruct(1, 1, 3, &cVlan), AtEthVlanTagConstruct(1, 1, 3, &sVlan)) != cAtOk), cAtTrue);
    AtTestLoggerEnable(cAtTrue);
    }

static void testBoundPwOfCircuitMustBeNullAfterUnbinding()
    {
    AtChannel circuit = AtPwBoundCircuitGet(TestedPw());
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitUnbind(TestedPw()));
    TEST_ASSERT_NULL(AtPwBoundCircuitGet(TestedPw()));
    TEST_ASSERT_NULL(AtChannelBoundPwGet(circuit));

    /* Recover previous configuration */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwCircuitBind(TestedPw(), circuit));
    PwReEnable();
    TEST_ASSERT(AtPwBoundCircuitGet(TestedPw()) == circuit);
    TEST_ASSERT(AtChannelBoundPwGet(circuit) == TestedPw());
    }

static void testDefaultLopsThreshold()
    {
    extern uint32 AtModulePwDefaultLopsSetThreshold(AtModulePw self);
    extern uint32 AtModulePwDefaultLopsClearThreshold(AtModulePw self);
    extern eBool AtPwLopsClearThresholdIsSupported(AtPw self);

    AtPw pw = TestedPw();
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet((AtChannel)pw);
    TEST_ASSERT_EQUAL_INT(AtPwLopsSetThresholdGet(pw), AtModulePwDefaultLopsSetThreshold(modulePw));

    if (AtPwLopsClearThresholdIsSupported(pw))
        {
        TEST_ASSERT_EQUAL_INT(AtPwLopsClearThresholdGet(pw), AtModulePwDefaultLopsClearThreshold(modulePw));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPw_Fixtures)
        {
        new_TestFixture("testDefaultLopsThreshold", testDefaultLopsThreshold),
        new_TestFixture("testCanChangeLOPSsetThreshold", testCanChangeLOPSsetThreshold),
        new_TestFixture("testCanChangeLOPSsetThresholdWithNoCircuit", testCanChangeLOPSsetThresholdWithNoCircuit),
        new_TestFixture("testCanChangeLOPSclrThreshold", testCanChangeLOPSclrThreshold),
        new_TestFixture("testCanChangeLOPSclrThresholdWithNoCircuit", testCanChangeLOPSclrThresholdWithNoCircuit),
        new_TestFixture("testCanToggleReoderOption", testCanToggleReoderOption),
        new_TestFixture("testChangeJitterBufSize", testChangeJitterBufSize),
        new_TestFixture("testChangeJitterBufferSizeWhenNoCircuit", testChangeJitterBufferSizeWhenNoCircuit),
        new_TestFixture("testChangeJitterBufDelay", testChangeJitterBufDelay),
        new_TestFixture("testChangeJitterBufDelayWhenNoCircuit", testChangeJitterBufDelayWhenNoCircuit),
        new_TestFixture("testToggleRtpOption", testToggleRtpOption),
        new_TestFixture("testChangeRtpPldType", testChangeRtpPldType),
        new_TestFixture("testChangeRtpSsrc", testChangeRtpSsrc),
        new_TestFixture("testChangeRtpTimeStampMode", testChangeRtpTimeStampMode),
        new_TestFixture("testControlWordDefaultOption", testControlWordDefaultOption),
        new_TestFixture("testCannotDisableControlWordForTdmPw", testCannotDisableControlWordForTdmPw),
        new_TestFixture("testToggleAutoTxLbitOption", testToggleAutoTxLbitOption),
        new_TestFixture("testCannotDisableRxLbitOption", testCannotDisableRxLbitOption),
        new_TestFixture("testToggleAutoRbitOption", testToggleAutoRbitOption),
        new_TestFixture("testChangeSequenceMode", testChangeSequenceMode),
        new_TestFixture("testCannotChangeInvalidSequenceMode", testCannotChangeInvalidSequenceMode),
        new_TestFixture("testChangeLengthMode", testChangeLengthMode),
        new_TestFixture("testCannotChangeInvalidLengthMode", testCannotChangeInvalidLengthMode),
        new_TestFixture("testChangePktReplaceMode", testChangePktReplaceMode),
        new_TestFixture("testChangeLopsPktReplaceMode", testChangeLopsPktReplaceMode),
        new_TestFixture("testCannotChangeInvalidPktReplaceMode", testCannotChangeInvalidPktReplaceMode),
        new_TestFixture("testCanChangePsn", testCanChangePsn),
        new_TestFixture("testCanChangeEthernetPort", testCanChangeEthernetPort),
        new_TestFixture("testConfigureEthHeaderBeforePsn", testConfigureEthHeaderBeforePsn),
        new_TestFixture("testCanSetEthernetHeaderWithMaximumTwoVlans", testCanSetEthernetHeaderWithMaximumTwoVlans),
        new_TestFixture("testCannotSetEthernetHeaderWithNullDMac", testCannotSetEthernetHeaderWithNullDMac),
        new_TestFixture("testCannotChangeJitterDelayLowerThanMinimumOne", testCannotChangeJitterDelayLowerThanMinimumOne),
        new_TestFixture("testCannotChangeJitterBufferSizeLowerThanMinimumOne", testCannotChangeJitterBufferSizeLowerThanMinimumOne),
        new_TestFixture("testBoundPwOfCircuitMustBeNullAfterUnbinding", testBoundPwOfCircuitMustBeNullAfterUnbinding),
        new_TestFixture("testJitterBufferConstrainMustBeValid", testJitterBufferConstrainMustBeValid),
        new_TestFixture("testCannotChangeJitterBufferSizeBiggerThanMaximumOne", testCannotChangeJitterBufferSizeBiggerThanMaximumOne),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPw_Caller, "TestSuite_AtPw", setUp, NULL, TestSuite_AtPw_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPw_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtSurEngineTestRunner CreateSurEngineTestRunner(AtChannelTestRunner self, AtModuleSurTestRunner factoryModule)
    {
    return AtModuleSurTestRunnerCreatePwSurEngineTestRunner(factoryModule, AtChannelSurEngineGet((AtChannel)TestedPw()));
    }

static void OverrideAtChannelTestRunner(AtChannelTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelTestRunnerOverride, (void *)self->methods, sizeof(m_AtChannelTestRunnerOverride));

        mMethodOverride(m_AtChannelTestRunnerOverride, CreateSurEngineTestRunner);
        }

    mMethodsSet(self, &m_AtChannelTestRunnerOverride);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtChannelTestRunner(self);
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwTestRunner);
    }

AtChannelTestRunner AtPwTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPwTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPwTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }

uint32 AtPwTestMinJitterBufferSizeInUs(uint16 payloadSize)
    {
    return AtPwMinJitterBufferSize(TestedPw(), AtPwBoundCircuitGet(TestedPw()), payloadSize);
    }

uint32 AtPwTestMinJitterDelayInUs(uint16 payloadSize)
    {
    return AtPwMinJitterDelay(TestedPw(), AtPwBoundCircuitGet(TestedPw()), payloadSize);
    }

eBool AtPwTestClearAndCheckStatus(AtPw pw, uint32 timeoutInMs)
    {
    tAtOsalCurTime startTime, currentTime;
    uint32 elapseTimeInMs = 0;

    if (AtTestIsSimulationTesting())
        return cAtTrue;

    AtOsalUSleep(10000);
    if (PwCountersAreNormal(pw))
        return cAtTrue;

    ClearStatus(pw);
    AtOsalUSleep(10000);
    AtOsalCurTimeGet(&startTime);
    while (elapseTimeInMs < timeoutInMs)
        {
        if (PwCountersAreNormal(pw))
            return cAtTrue;

        /* Retry by clear counters to have a refresh state, then wait hardware a moment */
        ClearStatus(pw);
        AtOsalUSleep(10000);

        /* Next */
        AtOsalCurTimeGet(&currentTime);
        elapseTimeInMs = mTimeIntervalInMsGet(startTime, currentTime);
        }

    return cAtFalse;
    }

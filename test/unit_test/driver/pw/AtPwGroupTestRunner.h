/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwGroupTestRunner.h
 * 
 * Created Date: Jan 5, 2016
 *
 * Description : PW group test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWGROUPTESTRUNNER_H_
#define _ATPWGROUPTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtObjectTestRunnerInternal.h"
#include "AtModulePwTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwGroupTestRunner
    {
    tAtObjectTestRunner super;
    AtList pws;
    AtModulePwTestRunner moduleRunner;
    }tAtPwGroupTestRunner;

typedef struct tAtPwGroupTestRunner * AtPwGroupTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtPwGroupTestRunner AtPwGroupTestRunnerObjectInit(AtPwGroupTestRunner self, AtPwGroup group, AtModulePwTestRunner moduleRunner, AtList pws);

/* Concretes */
AtPwGroupTestRunner AtPwGroupApsTestRunnerNew(AtPwGroup group, AtModulePwTestRunner moduleRunner, AtList pws);
AtPwGroupTestRunner AtPwGroupHsTestRunnerNew(AtPwGroup group, AtModulePwTestRunner moduleRunner, AtList pws);

/* Access attributes */
AtModulePwTestRunner AtPwGroupTestRunnerModuleRunner(AtPwGroupTestRunner self);
AtList AtPwGroupTestRunnerPwsGet(AtPwGroupTestRunner self);
AtPwGroup AtPwGroupTestRunnerGroupGet(AtPwGroupTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWGROUPTESTRUNNER_H_ */


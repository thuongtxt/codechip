/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwPsnTestRunner.c
 *
 * Created Date: Mar 5, 2015
 *
 * Description : PW PSN test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsn.h"
#include "../runner/AtUnittestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxMefEcId     cBit19_0
#define cMaxMplsLabelId cBit19_0
#define cMaxMplsExp     cBit2_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwPsnTestRunner
    {
    tAtUnittestRunner super;
    }tAtPwPsnTestRunner;

typedef AtPwPsn (*CreatePsnFunction)(void);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/* PSNs */
static AtPwPsn m_psn = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void _testCreatePsn(CreatePsnFunction CreateFunc, eAtPwPsnType expectedType)
    {
    AtPwPsn psn;

    psn = CreateFunc();
    TEST_ASSERT_NOT_NULL(psn);
    TEST_ASSERT_NOT_NULL(AtPwPsnTypeString(psn));
    TEST_ASSERT_EQUAL_INT(expectedType, AtPwPsnTypeGet(psn));
    AtObjectDelete((AtObject)psn);
    }

static void testCanCreatePsns(void)
    {
    _testCreatePsn((CreatePsnFunction)AtPwMplsPsnNew, cAtPwPsnTypeMpls);
    _testCreatePsn((CreatePsnFunction)AtPwMefPsnNew, cAtPwPsnTypeMef);
    _testCreatePsn((CreatePsnFunction)AtPwUdpPsnNew, cAtPwPsnTypeUdp);
    _testCreatePsn((CreatePsnFunction)AtPwIpV4PsnNew, cAtPwPsnTypeIPv4);
    _testCreatePsn((CreatePsnFunction)AtPwIpV6PsnNew, cAtPwPsnTypeIPv6);
    }

static TestRef PsnCreatingTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_PsnCreating_Fixtures)
        {
        new_TestFixture("testCanCreatePsns", testCanCreatePsns)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_PsnCreating_Caller, "TestSuite_ModulePw", NULL, NULL, TestSuite_PsnCreating_Fixtures);

    return (TestRef)((void *)&TestSuite_PsnCreating_Caller);
    }

static void mplsSetUp()
    {
    m_psn = (AtPwPsn)AtPwMplsPsnNew();
    }

static void psnTearDown()
    {
    AtObjectDelete((AtObject)m_psn);
    }

static AtPwPsn Psn()
    {
    return m_psn;
    }

static AtPwMplsPsn Mpls()
    {
    return (AtPwMplsPsn)Psn();
    }

static AtPwMefPsn Mef()
    {
    return (AtPwMefPsn)Psn();
    }

static tAtEthVlanTag *VlanMake(uint8 priority, uint8 cfi, uint16 vlanId, tAtEthVlanTag *vlan)
    {
    vlan->priority = priority;
    vlan->cfi      = cfi;
    vlan->vlanId   = vlanId;

    return vlan;
    }

static eBool VlanIsSame(const tAtEthVlanTag *tag1, const tAtEthVlanTag *tag2)
    {
    if ((tag1->priority == tag2->priority) &&
        (tag1->cfi      == tag2->cfi) &&
        (tag1->vlanId   == tag2->vlanId))
        return cAtTrue;
    return cAtFalse;
    }

static tAtPwMplsLabel *MplsLabelMake(uint32 label, uint8 experimetal, uint8 timeToLive, tAtPwMplsLabel *mplsLabel)
    {
    mplsLabel->experimental = experimetal;
    mplsLabel->label        = label;
    mplsLabel->timeToLive   = timeToLive;

    return mplsLabel;
    }

static eBool MplsLabelIsSame(const tAtPwMplsLabel *label1, const tAtPwMplsLabel *label2)
    {
    if ((label1->label        == label2->label) &&
        (label1->experimental == label2->experimental) &&
        (label1->timeToLive   == label2->timeToLive))
        return cAtTrue;
    return cAtFalse;
    }

static eBool MplsIsSame(AtPwMplsPsn psn1, AtPwMplsPsn psn2)
    {
    tAtPwMplsLabel label1, label2;
    uint32 label1_i, label2_i;

    /* Inner label must be the same */
    AtPwMplsPsnInnerLabelGet(psn1, &label1);
    AtPwMplsPsnInnerLabelGet(psn2, &label2);
    if (!MplsLabelIsSame(&label1, &label2))
        return cAtFalse;

    /* Expected label must be the same */
    if (AtPwMplsPsnExpectedLabelGet(psn1) != AtPwMplsPsnExpectedLabelGet(psn2))
        return cAtFalse;

    /* Outer labels must be the same */
    if (AtPwMplsPsnNumberOfOuterLabelsGet(psn1) != AtPwMplsPsnNumberOfOuterLabelsGet(psn2))
        return cAtFalse;

    for (label1_i = 0; label1_i < AtPwMplsPsnNumberOfOuterLabelsGet(psn1); label1_i++)
        {
        eBool exist = cAtFalse;
        AtPwMplsPsnOuterLabelGetByIndex(psn1, label1_i, &label1);
        for (label2_i = 0; label2_i < AtPwMplsPsnNumberOfOuterLabelsGet(psn2); label2_i++)
            {
            AtPwMplsPsnOuterLabelGetByIndex(psn2, label2_i, &label2);
            if (MplsLabelIsSame(&label1, &label2))
                {
                exist = cAtTrue;
                break;
                }
            }

        if (!exist)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool MplsContainsLabel(const tAtPwMplsLabel *label)
    {
    uint32 i;

    for (i = 0; i < AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()); i++)
        {
        tAtPwMplsLabel aLabel;
        AtAssert(AtPwMplsPsnOuterLabelGetByIndex(Mpls(), i, &aLabel));
        if (MplsLabelIsSame(&aLabel, label))
            return cAtTrue;
        }

    return cAtFalse;
    }

static void MplsAddAllLabels(void)
    {
    uint32 i;
    tAtPwMplsLabel label;

    /* Make it full of labels */
    for (i = 0; i < AtPwMplsPsnMaxNumOuterLabels(Mpls()); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelAdd(Mpls(), MplsLabelMake(i, 2, 3, &label)));
        }
    TEST_ASSERT_EQUAL_INT(AtPwMplsPsnMaxNumOuterLabels(Mpls()), AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()));
    }

static void testPsnNoAssociatedPsnAfterSetup(void)
    {
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet(Psn()));
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet(Psn()));
    }

static void testMplsCanSetInnerLabel(void)
    {
    tAtPwMplsLabel label, actualLabel;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnInnerLabelSet(Mpls(), MplsLabelMake(1, 2, 3, &label)));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnInnerLabelGet(Mpls(), &actualLabel));
    TEST_ASSERT(MplsLabelIsSame(&label, &actualLabel));
    }

static void testMplsCannotSetInvalidInnerLabel(void)
    {
    tAtPwMplsLabel label, originalLabel;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnInnerLabelGet(Mpls(), &originalLabel));

    TEST_ASSERT(AtPwMplsPsnInnerLabelSet(Mpls(), MplsLabelMake(cMaxMplsLabelId + 1, cMaxMplsExp + 1, 3, &label)) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnInnerLabelGet(Mpls(), &label));
    TEST_ASSERT(MplsLabelIsSame(&label, &originalLabel));

    TEST_ASSERT(AtPwMplsPsnInnerLabelSet(Mpls(), MplsLabelMake(cMaxMplsLabelId, cMaxMplsExp, 3, &label)) == cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnInnerLabelGet(Mpls(), &label));
    TEST_ASSERT(!MplsLabelIsSame(&label, &originalLabel));
    }

static void testMplsMaxNumLabelsMustBeGreaterThanZero(void)
    {
    TEST_ASSERT(AtPwMplsPsnMaxNumOuterLabels(Mpls()) > 0);
    }

static void testMplsAddOuterLabel(void)
    {
    uint32 numLabels = AtPwMplsPsnNumberOfOuterLabelsGet(Mpls());
    tAtPwMplsLabel label;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelAdd(Mpls(), MplsLabelMake(1, 2, 3, &label)));
    TEST_ASSERT_EQUAL_INT(numLabels + 1, AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()));
    TEST_ASSERT(MplsContainsLabel(&label));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelAdd(Mpls(), MplsLabelMake(4, 2, 5, &label)));
    TEST_ASSERT_EQUAL_INT(numLabels + 2, AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()));
    TEST_ASSERT(MplsContainsLabel(&label));
    }

static void testCannotAddTooManyLabels(void)
    {
    tAtPwMplsLabel label;

    MplsAddAllLabels();

    /* And cannot add anymore */
    TEST_ASSERT(AtPwMplsPsnOuterLabelAdd(Mpls(), MplsLabelMake(1, 2, 3, &label)) != cAtOk);
    }

static void testMplsCannotAddNullOuterLabel(void)
    {
    TEST_ASSERT(AtPwMplsPsnOuterLabelAdd(Mpls(), NULL) != cAtOk);
    }

static void testMplsCanRemoveOuterLabel(void)
    {
    uint32 numLabels, label_i;
    tAtPwMplsLabel label;

    MplsAddAllLabels();
    numLabels = AtPwMplsPsnNumberOfOuterLabelsGet(Mpls());

    /* Remove the middle one */
    label_i = numLabels / 2;
    TEST_ASSERT_NOT_NULL(AtPwMplsPsnOuterLabelGetByIndex(Mpls(), label_i, &label));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelRemove(Mpls(), &label));
    TEST_ASSERT_EQUAL_INT(numLabels - 1, AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()));

    /* Add it back */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelAdd(Mpls(), &label));
    TEST_ASSERT_EQUAL_INT(numLabels, AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()));

    /* Remove all */
    while (AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()) > 0)
        {
        TEST_ASSERT_NOT_NULL(AtPwMplsPsnOuterLabelGetByIndex(Mpls(), 0, &label));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelRemove(Mpls(), &label));
        }
    }

static void testMplsCannotRemoveNoneExistingLabel(void)
    {
    tAtPwMplsLabel label;
    uint32 numLabels;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelAdd(Mpls(), MplsLabelMake(1, 1, 1, &label)));
    numLabels = AtPwMplsPsnNumberOfOuterLabelsGet(Mpls());
    TEST_ASSERT(AtPwMplsPsnOuterLabelRemove(Mpls(), MplsLabelMake(2, 1, 1, &label)) != cAtOk);
    TEST_ASSERT_EQUAL_INT(numLabels, AtPwMplsPsnNumberOfOuterLabelsGet(Mpls()));
    }

static void testMplsUtil()
    {
    tAtPwMplsLabel label;

    TEST_ASSERT_NOT_NULL(AtPwMplsLabelMake(1, 2, 3, &label));
    TEST_ASSERT_NULL(AtPwMplsLabelMake(cMaxMplsLabelId + 1, 0, 0, &label));
    TEST_ASSERT_NULL(AtPwMplsLabelMake(1, 2, 3, NULL));
    }

static void testMplsSetExpectedLabel(void)
    {
    /* Try with valid label */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnExpectedLabelSet(Mpls(), 1));
    TEST_ASSERT_EQUAL_INT(1, AtPwMplsPsnExpectedLabelGet(Mpls()));

    /* Try with invalid label */
    TEST_ASSERT(AtPwMplsPsnExpectedLabelSet(Mpls(), cMaxMplsLabelId + 1) != cAtOk);
    TEST_ASSERT_EQUAL_INT(1, AtPwMplsPsnExpectedLabelGet(Mpls()));
    }

static void testMplsClone(void)
    {
    AtPwMplsPsn clonePsn;
    tAtPwMplsLabel label;

    clonePsn = (AtPwMplsPsn)AtObjectClone((AtObject)Mpls());
    TEST_ASSERT(MplsIsSame(clonePsn, Mpls()));

    /* Make some changes */
    AtPwMplsPsnInnerLabelSet(Mpls(), MplsLabelMake(1,2,3, &label));
    AtPwMplsPsnExpectedLabelSet(Mpls(), 3);
    AtPwMplsPsnOuterLabelAdd(Mpls(), MplsLabelMake(4,2,3, &label));

    /* Clone again */
    TEST_ASSERT(!MplsIsSame(clonePsn, Mpls()));
    AtObjectDelete((AtObject)clonePsn);
    clonePsn = (AtPwMplsPsn)AtObjectClone((AtObject)Mpls());
    TEST_ASSERT(MplsIsSame(clonePsn, Mpls()));

    /* Clean up */
    AtObjectDelete((AtObject)clonePsn);
    }

static void TestMefChangeMAC(eAtModulePwRet (*MacSet)(AtPwMefPsn, uint8 *),
                              eAtModulePwRet (*MacGet)(AtPwMefPsn, uint8 *))
    {
    uint8 byte_i;
    uint8 mac[] = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6};
    uint8 actualMac[6];

    TEST_ASSERT_EQUAL_INT(cAtOk, MacSet(Mef(), mac));
    TEST_ASSERT_EQUAL_INT(cAtOk, MacGet(Mef(), actualMac));
    TEST_ASSERT(AtOsalMemCmp(mac, actualMac, sizeof(mac)) == 0);

    for (byte_i = 0; byte_i < 6; byte_i++)
        mac[byte_i] = mac[byte_i] + 1;
    TEST_ASSERT_EQUAL_INT(cAtOk, MacSet(Mef(), mac));
    TEST_ASSERT_EQUAL_INT(cAtOk, MacGet(Mef(), actualMac));
    TEST_ASSERT(AtOsalMemCmp(mac, actualMac, sizeof(mac)) == 0);

    TEST_ASSERT(MacSet(Mef(), NULL) != cAtOk);
    TEST_ASSERT(MacGet(Mef(), NULL) != cAtOk);
    }

static void testMefChangeTxEcId(void)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnTxEcIdSet(Mef(), cMaxMefEcId));
    TEST_ASSERT_EQUAL_INT(cMaxMefEcId, AtPwMefPsnTxEcIdGet(Mef()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnTxEcIdSet(Mef(), cMaxMefEcId / 2));
    TEST_ASSERT_EQUAL_INT(cMaxMefEcId / 2, AtPwMefPsnTxEcIdGet(Mef()));
    }

static void testMefCannotChangeOutOfRangeTxEcId(void)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnTxEcIdSet(Mef(), cMaxMefEcId));
    TEST_ASSERT(AtPwMefPsnTxEcIdSet(Mef(), cMaxMefEcId + 1) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cMaxMefEcId, AtPwMefPsnTxEcIdGet(Mef()));
    }

static void testMefChangeExpectedEcId(void)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedEcIdSet(Mef(), cMaxMefEcId));
    TEST_ASSERT_EQUAL_INT(cMaxMefEcId, AtPwMefPsnExpectedEcIdGet(Mef()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedEcIdSet(Mef(), cMaxMefEcId / 2));
    TEST_ASSERT_EQUAL_INT(cMaxMefEcId / 2, AtPwMefPsnExpectedEcIdGet(Mef()));
    }

static void testMefCannotChangeOutOfRangeExpectedEcId(void)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedEcIdSet(Mef(), cMaxMefEcId));
    TEST_ASSERT(AtPwMefPsnExpectedEcIdSet(Mef(), cMaxMefEcId + 1) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cMaxMefEcId, AtPwMefPsnExpectedEcIdGet(Mef()));
    }

static void testMefChangeDMAC(void)
    {
    TestMefChangeMAC(AtPwMefPsnDestMacSet, AtPwMefPsnDestMacGet);
    }

static void testMefChangeSMAC(void)
    {
    TestMefChangeMAC(AtPwMefPsnSourceMacSet, AtPwMefPsnSourceMacGet);
    }

static void testMefChangeExpectedMAC(void)
    {
    TestMefChangeMAC(AtPwMefPsnExpectedMacSet, AtPwMefPsnExpectedMacGet);
    }

static void testNoVlanAfterSetup(void)
    {
    TEST_ASSERT_EQUAL_INT(0, AtPwMefPsnNumVlanTags(Mef()));
    }

static void testMefMaxNumOfVlanTagsMustBeGreaterThanZero(void)
    {
    TEST_ASSERT(AtPwMefPsnMaxNumVlanTags(Mef()) > 0);
    }

static void TestMefAddVlanWithType(uint16 vlanType)
    {
    tAtEthVlanTag vlan, actualVlan;
    uint16 actualVlanType;

    /* Add a valid tag */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAdd(Mef(), vlanType, VlanMake(0, 1, 2, &vlan)));
    TEST_ASSERT_EQUAL_INT(AtPwMefPsnNumVlanTags(Mef()), 1);
    AtPwMefPsnVlanTagAtIndex(Mef(), 0, &actualVlanType, &actualVlan);
    TEST_ASSERT_EQUAL_INT(vlanType, actualVlanType);
    TEST_ASSERT(VlanIsSame(&vlan, &actualVlan));
    }

static void testMefAddVlan(void)
    {
    TestMefAddVlanWithType(0x8100);
    }

static void testMefAddVlanWithAnyType(void)
    {
    TestMefAddVlanWithType(0x1);
    }

static void AddAllTags(void)
    {
    tAtEthVlanTag vlan, actualVlan;
    uint16 vlanType = 0x8100, actualVlanType;
    uint8 numVlans = AtPwMefPsnMaxNumVlanTags(Mef());
    uint8 vlan_i;
    uint8 numCurrentVlans = AtPwMefPsnNumVlanTags(Mef());

    for (vlan_i = 0; vlan_i < numVlans; vlan_i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAdd(Mef(), vlanType, VlanMake(vlan_i, vlan_i % 2, vlan_i, &vlan)));
        TEST_ASSERT_EQUAL_INT(numCurrentVlans + 1, AtPwMefPsnNumVlanTags(Mef()));
        numCurrentVlans = numCurrentVlans + 1;

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAtIndex(Mef(), vlan_i, &actualVlanType, &actualVlan));
        TEST_ASSERT_EQUAL_INT(vlanType, actualVlanType);
        TEST_ASSERT(VlanIsSame(&vlan, &actualVlan));
        }
    }

static void testAddAllTags(void)
    {
    AddAllTags();
    }

static void testCannotAddInvalidTag(void)
    {
    uint16 vlanType = 0x8100;
    tAtEthVlanTag vlan;

    /* Cannot add a NULL tag */
    TEST_ASSERT(AtPwMefPsnVlanTagAdd(Mef(), vlanType, NULL) != cAtOk);

    /* Try with invalid priority and CFI */
    TEST_ASSERT(AtPwMefPsnVlanTagAdd(Mef(), vlanType, VlanMake(cBit2_0 + 1, 0, 0, &vlan)) != cAtOk);
    TEST_ASSERT(AtPwMefPsnVlanTagAdd(Mef(), vlanType, VlanMake(cBit2_0, 2, 0, &vlan))     != cAtOk);
    }

static void testCannotAddTooManyVlans(void)
    {
    tAtEthVlanTag vlan;
    AddAllTags();
    TEST_ASSERT(AtPwMefPsnVlanTagAdd(Mef(), 0x8100, VlanMake(0, 1, 2, &vlan)) != cAtOk);
    }

static void testCannotGetVlanAtInvalidIndex(void)
    {
    uint16 vlanType;
    tAtEthVlanTag vlan;

    AddAllTags();
    TEST_ASSERT(AtPwMefPsnVlanTagAtIndex(Mef(), AtPwMefPsnNumVlanTags(Mef()), &vlanType, &vlan) != cAtOk);
    }

static void testMefNoCrashWhenGetVlanWithNullOutputParams(void)
    {
    uint16 vlanType;
    tAtEthVlanTag vlan;
    AddAllTags();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAtIndex(Mef(), 0, NULL, &vlan));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAtIndex(Mef(), 0, &vlanType, NULL));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAtIndex(Mef(), 0, NULL, NULL));
    }

static void testMefRemoveVlanTags(void)
    {
    uint8 numVlans, vlan_i;
    uint16 vlanType;
    tAtEthVlanTag vlanTag;

    AddAllTags();

    /* Try removing a middle one */
    numVlans = AtPwMefPsnNumVlanTags(Mef());
    vlan_i = numVlans / 2;
    AtPwMefPsnVlanTagAtIndex(Mef(), vlan_i, &vlanType, &vlanTag);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagRemove(Mef(), vlanType, &vlanTag));

    /* Make sure that it's gone */
    TEST_ASSERT_EQUAL_INT(numVlans - 1, AtPwMefPsnNumVlanTags(Mef()));
    for (vlan_i = 0; vlan_i < AtPwMefPsnNumVlanTags(Mef()); vlan_i++)
        {
        tAtEthVlanTag existingVlanTag;
        uint16 existingVlanType;

        AtPwMefPsnVlanTagAtIndex(Mef(), vlan_i, &existingVlanType, &existingVlanTag);
        TEST_ASSERT(!((existingVlanType == vlanType) && (VlanIsSame(&existingVlanTag, &vlanTag))))
        }
    }

static void testMefCannotRemoveNoneExistTag(void)
    {
    tAtEthVlanTag vlan, otherVlan;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnVlanTagAdd(Mef(), 0x8100, VlanMake(0, 1, 2, &vlan)));
    TEST_ASSERT(AtPwMefPsnVlanTagRemove(Mef(), 0x8200, &vlan) != cAtOk);
    TEST_ASSERT(AtPwMefPsnVlanTagRemove(Mef(), 0x8100, VlanMake(1, 1, 2, &otherVlan)) != cAtOk);
    TEST_ASSERT(AtPwMefPsnVlanTagRemove(Mef(), 0x8100, NULL) != cAtOk);
    }

static eBool MefMacIsSame(AtPwMefPsn psn1, AtPwMefPsn psn2, eAtModulePwRet (*MacGet)(AtPwMefPsn, uint8 *))
    {
    uint8 mac1[6], mac2[6];
    AtAssert(MacGet(psn1, mac1) == cAtOk);
    AtAssert(MacGet(psn2, mac2) == cAtOk);
    return (AtOsalMemCmp(mac1, mac2, sizeof(mac1)) == 0) ? cAtTrue : cAtFalse;
    }

static eBool MefIsSame(AtPwMefPsn psn1, AtPwMefPsn psn2)
    {
    uint8 vlan_i;

    if ((AtPwMefPsnTxEcIdGet(psn1)       != AtPwMefPsnTxEcIdGet(psn2)) ||
        (AtPwMefPsnExpectedEcIdGet(psn1) != AtPwMefPsnExpectedEcIdGet(psn2)))
        return cAtFalse;

    if (!MefMacIsSame(psn1, psn2, AtPwMefPsnDestMacGet)   ||
        !MefMacIsSame(psn1, psn2, AtPwMefPsnSourceMacGet) ||
        !MefMacIsSame(psn1, psn2, AtPwMefPsnExpectedMacGet))
        return cAtFalse;

    if (AtPwMefPsnNumVlanTags(psn1) != AtPwMefPsnNumVlanTags(psn2))
        return cAtFalse;
    for (vlan_i = 0; vlan_i < AtPwMefPsnNumVlanTags(psn1); vlan_i++)
        {
        tAtEthVlanTag tag1, tag2;
        uint16 vlanType1, vlanType2;
        AtAssert(AtPwMefPsnVlanTagAtIndex(psn1, vlan_i, &vlanType1, &tag1) == cAtOk);
        AtAssert(AtPwMefPsnVlanTagAtIndex(psn2, vlan_i, &vlanType2, &tag2) == cAtOk);
        if ((vlanType1 != vlanType2) ||
            (!VlanIsSame(&tag1, &tag2)))
            return cAtFalse;
        }

    return cAtTrue;
    }

static void testMefClone(void)
    {
    AtPwMefPsn clonePsn;
    uint8 dmac[] = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6};
    uint8 smac[] = {0x2, 0x3, 0x4, 0x5, 0x6, 0x7};
    uint8 emac[] = {0x3, 0x4, 0x5, 0x6, 0x7, 0x8};

    clonePsn = (AtPwMefPsn)AtObjectClone((AtObject)Mef());
    TEST_ASSERT(MefIsSame(clonePsn, Mef()));

    /* Make all changes */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnTxEcIdSet(Mef(), 100));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedEcIdSet(Mef(), 200));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnDestMacSet(Mef(), dmac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnSourceMacSet(Mef(), smac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedMacGet(Mef(), emac));
    AddAllTags();

    /* Clone again */
    TEST_ASSERT(!MefIsSame(clonePsn, Mef()));
    AtObjectDelete((AtObject)clonePsn);
    clonePsn = (AtPwMefPsn)AtObjectClone((AtObject)Mef());
    TEST_ASSERT(MefIsSame(clonePsn, Mef()));

    /* Clean up */
    AtObjectDelete((AtObject)clonePsn);
    }

static void testCanSetupPsnHierarchy(void)
    {
    AtPwPsn mpls = (AtPwPsn)AtPwMplsPsnNew();
    AtPwPsn mef  = (AtPwPsn)AtPwMefPsnNew();

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnLowerPsnSet(mpls, mef));
    TEST_ASSERT(AtPwPsnLowerPsnGet(mpls) == mef);
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet(mpls));
    TEST_ASSERT(AtPwPsnHigherPsnGet(mef) == mpls);
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet(mef));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnLowerPsnSet(mpls, NULL));
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet(mpls));
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet(mpls));
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet(mef));
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet(mef));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnLowerPsnSet(mpls, mef));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnHigherPsnSet(mef, NULL));
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet(mpls));
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet(mpls));
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet(mef));
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet(mef));

    /* Clean up */
    AtObjectDelete((AtObject)mpls);
    AtObjectDelete((AtObject)mef);
    }

static void testCanClonePsnHierarchy(void)
    {
    AtPwMplsPsn mpls = AtPwMplsPsnNew();
    AtPwMefPsn mef  = AtPwMefPsnNew();
    uint8 dmac[] = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6};
    uint8 smac[] = {0x2, 0x3, 0x4, 0x5, 0x6, 0x7};
    uint8 emac[] = {0x3, 0x4, 0x5, 0x6, 0x7, 0x8};
    tAtPwMplsLabel label;
    AtPwMplsPsn cloneMpls;
    AtPwMefPsn cloneMef;

    /* MEF */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnTxEcIdSet(mef, 100));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedEcIdSet(mef, 200));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnDestMacSet(mef, dmac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnSourceMacSet(mef, smac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMefPsnExpectedMacGet(mef, emac));

    /* MPLS */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnInnerLabelSet(mpls, MplsLabelMake(1,2,3, &label))) ;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnExpectedLabelSet(mpls, 3));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwMplsPsnOuterLabelAdd(mpls, MplsLabelMake(4,2,3, &label)));

    /* Build hierarchy */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPwPsnLowerPsnSet((AtPwPsn)mef, (AtPwPsn)mpls));
    TEST_ASSERT(AtPwPsnLowerPsnGet((AtPwPsn)mef) == (AtPwPsn)mpls);
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet((AtPwPsn)mef));
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet((AtPwPsn)mpls));
    TEST_ASSERT(AtPwPsnHigherPsnGet((AtPwPsn)mpls) == (AtPwPsn)mef);

    /* Clone */
    cloneMef  = (AtPwMefPsn)AtObjectClone((AtObject)mef);
    cloneMpls = (AtPwMplsPsn)AtPwPsnLowerPsnGet((AtPwPsn)cloneMef);
    TEST_ASSERT_NOT_NULL(cloneMpls);

    /* This new PSN must have the same hierarchy */
    TEST_ASSERT(AtPwPsnLowerPsnGet((AtPwPsn)cloneMef)   == (AtPwPsn)cloneMpls);
    TEST_ASSERT_NULL(AtPwPsnHigherPsnGet((AtPwPsn)cloneMef));
    TEST_ASSERT_NULL(AtPwPsnLowerPsnGet((AtPwPsn)cloneMpls));
    TEST_ASSERT(AtPwPsnHigherPsnGet((AtPwPsn)cloneMpls) == (AtPwPsn)cloneMef);

    /* These new PSNs must be different objects */
    TEST_ASSERT(mef  != cloneMef);
    TEST_ASSERT(mpls != cloneMpls);

    /* And same header */
    TEST_ASSERT(MplsIsSame(mpls, cloneMpls));
    TEST_ASSERT(MefIsSame(mef, cloneMef));

    /* Cleanup */
    AtObjectDelete((AtObject)mef);
    AtObjectDelete((AtObject)cloneMef);
    }

static TestRef MplsTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Mpls_Fixtures)
        {
        new_TestFixture("testPsnNoAssociatedPsnAfterSetup", testPsnNoAssociatedPsnAfterSetup),
        new_TestFixture("testMplsCanSetInnerLabel", testMplsCanSetInnerLabel),
        new_TestFixture("testMplsCannotSetInvalidInnerLabel", testMplsCannotSetInvalidInnerLabel),
        new_TestFixture("testMplsMaxNumLabelsMustBeGreaterThanZero", testMplsMaxNumLabelsMustBeGreaterThanZero),
        new_TestFixture("testMplsAddOuterLabel", testMplsAddOuterLabel),
        new_TestFixture("testCannotAddTooManyLabels", testCannotAddTooManyLabels),
        new_TestFixture("testMplsCannotAddNullOuterLabel", testMplsCannotAddNullOuterLabel),
        new_TestFixture("testMplsCanRemoveOuterLabel", testMplsCanRemoveOuterLabel),
        new_TestFixture("testMplsCannotRemoveNoneExistingLabel", testMplsCannotRemoveNoneExistingLabel),
        new_TestFixture("testMplsUtil", testMplsUtil),
        new_TestFixture("testMplsSetExpectedLabel", testMplsSetExpectedLabel),
        new_TestFixture("testMplsClone", testMplsClone)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Mpls_Caller, "TestSuite_Mpls", mplsSetUp, psnTearDown, TestSuite_Mpls_Fixtures);

    return (TestRef)((void *)&TestSuite_Mpls_Caller);
    }

static void mefSetup(void)
    {
    m_psn = (AtPwPsn)AtPwMefPsnNew();
    }

static TestRef MefTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Mef_Fixtures)
        {
        new_TestFixture("testPsnNoAssociatedPsnAfterSetup", testPsnNoAssociatedPsnAfterSetup),
        new_TestFixture("testMefChangeTxEcId", testMefChangeTxEcId),
        new_TestFixture("testMefCannotChangeOutOfRangeTxEcId", testMefCannotChangeOutOfRangeTxEcId),
        new_TestFixture("testMefChangeExpectedEcId", testMefChangeExpectedEcId),
        new_TestFixture("testMefCannotChangeOutOfRangeExpectedEcId", testMefCannotChangeOutOfRangeExpectedEcId),
        new_TestFixture("testMefChangeDMAC", testMefChangeDMAC),
        new_TestFixture("testMefChangeExpectedMAC", testMefChangeExpectedMAC),
        new_TestFixture("testMefChangeSMAC", testMefChangeSMAC),
        new_TestFixture("testNoVlanAfterSetup", testNoVlanAfterSetup),
        new_TestFixture("testMefMaxNumOfVlanTagsMustBeGreaterThanZero", testMefMaxNumOfVlanTagsMustBeGreaterThanZero),
        new_TestFixture("testMefAddVlan", testMefAddVlan),
        new_TestFixture("testMefAddVlanWithAnyType", testMefAddVlanWithAnyType),
        new_TestFixture("testAddAllTags", testAddAllTags),
        new_TestFixture("testCannotAddInvalidTag", testCannotAddInvalidTag),
        new_TestFixture("testCannotAddTooManyVlans", testCannotAddTooManyVlans),
        new_TestFixture("testCannotGetVlanAtInvalidIndex", testCannotGetVlanAtInvalidIndex),
        new_TestFixture("testMefNoCrashWhenGetVlanWithNullOutputParams", testMefNoCrashWhenGetVlanWithNullOutputParams),
        new_TestFixture("testMefRemoveVlanTags", testMefRemoveVlanTags),
        new_TestFixture("testMefCannotRemoveNoneExistTag", testMefCannotRemoveNoneExistTag),
        new_TestFixture("testCanSetupPsnHierarchy", testCanSetupPsnHierarchy),
        new_TestFixture("testMefClone", testMefClone),
        new_TestFixture("testCanClonePsnHierarchy", testCanClonePsnHierarchy),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Mef_Caller, "TestSuite_Mef", mefSetup, psnTearDown, TestSuite_Mef_Fixtures);

    return (TestRef)((void *)&TestSuite_Mef_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)PsnCreatingTestSuite(self));
    AtListObjectAdd(suites, (AtObject)MplsTestSuite(self));
    AtListObjectAdd(suites, (AtObject)MefTestSuite(self));

    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "PwPsn");
    return buffer;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(self), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwPsnTestRunner);
    }

AtUnittestRunner AtPwPsnTestRunnerObjectInit(AtUnittestRunner self, AtModulePw pwModule)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit((AtUnittestRunner)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtPwPsnTestRunnerNew(AtModulePw pwModule)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPwPsnTestRunnerObjectInit(newRunner, pwModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtModuleSurTestRunner.h
 * 
 * Created Date: Jun 22, 2018
 *
 * Description : SUR Module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESURTESTRUNNER_H_
#define _ATMODULESURTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPmRegister.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSurTestRunner *AtModuleSurTestRunner;
typedef struct tAtSurEngineTestRunner *AtSurEngineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Test runner factoring */
AtSurEngineTestRunner AtModuleSurTestRunnerCreateSdhLineSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine);
AtSurEngineTestRunner AtModuleSurTestRunnerCreateSdhPathSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine);
AtSurEngineTestRunner AtModuleSurTestRunnerCreatePdhDe3SurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine);
AtSurEngineTestRunner AtModuleSurTestRunnerCreatePdhDe1SurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine);
AtSurEngineTestRunner AtModuleSurTestRunnerCreatePwSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine);

eBool AtModuleSurTestRunnerRegisterTypeMustBeSupported(AtModuleSurTestRunner self, eAtPmRegisterType type);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESURTESTRUNNER_H_ */


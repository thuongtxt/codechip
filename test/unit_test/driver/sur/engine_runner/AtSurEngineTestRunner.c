/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtSurEngineTestRunner.c
 *
 * Created Date: May 18, 2018
 *
 * Description : SUR engine test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPmThresholdProfile.h"
#include "../../../../driver/src/generic/sur/AtSurEngineInternal.h"
#include "../../../../driver/src/generic/sur/AtModuleSurInternal.h"
#include "../AtModuleSurTestRunner.h"
#include "AtTests.h"
#include "AtSurEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtSurEngineTestRunner)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSurEngineTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSurEngine TestedEngine()
    {
    return AtSurEngineTestRunnerEngineGet((AtSurEngineTestRunner)AtUnittestRunnerCurrentRunner());
    }

static AtSurEngineTestRunner CurrentRunner()
    {
    return (AtSurEngineTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModuleSurTestRunner ModuleTestRunner()
    {
    return (AtModuleSurTestRunner)AtSurEngineTestRunnerModuleRunnerGet(CurrentRunner());
    }

static void testCanGetModule(void)
    {
    TEST_ASSERT_NOT_NULL(AtSurEngineModuleGet(TestedEngine()));
    }

static void testCanGetChannel(void)
    {
    TEST_ASSERT_NOT_NULL(AtSurEngineChannelGet(TestedEngine()));
    }

static void testCanInitialize(void)
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineInit(TestedEngine()));
    }

static void TestEnableDisable(AtSurEngineTestRunner self, eBool enable)
    {
    eBool enabled = AtSurEngineIsEnabled(TestedEngine());

    if (!mMethodsGet(self)->CanEnable(self, enable))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineEnable(TestedEngine(), enable));
    TEST_ASSERT_EQUAL_INT(enable, AtSurEngineIsEnabled(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineEnable(TestedEngine(), enabled));
    }

static void testCanEnableDisable(void)
    {
    AtSurEngineTestRunner runner = (AtSurEngineTestRunner)AtUnittestRunnerCurrentRunner();
    TestEnableDisable(runner, cAtTrue);
    TestEnableDisable(runner, cAtFalse);
    }

static void TestPmEnableDisable(AtSurEngineTestRunner self, eBool enable)
    {
    eBool enabled = AtSurEnginePmIsEnabled(TestedEngine());

    if (!AtSurEnginePmCanEnable(TestedEngine(), enable))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEnginePmEnable(TestedEngine(), enable));
    TEST_ASSERT_EQUAL_INT(enable, AtSurEnginePmIsEnabled(TestedEngine()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEnginePmEnable(TestedEngine(), enabled));
    }

static void testCanEnableDisablePm(void)
    {
    AtSurEngineTestRunner runner = (AtSurEngineTestRunner)AtUnittestRunnerCurrentRunner();
    TestPmEnableDisable(runner, cAtTrue);
    TestPmEnableDisable(runner, cAtFalse);
    }

static void testCanGetCurrentFailure(void)
    {
    AtSurEngineCurrentFailuresGet(TestedEngine());
    }

static void testCanClearHistoryFailure(void)
    {
    AtSurEngineFailureChangedHistoryGet(TestedEngine());
    AtSurEngineFailureChangedHistoryClear(TestedEngine());
    }

static void testConfigureFailureInteruptMask(void)
    {
    uint8 numMask, i;
    const uint32 *enableMask = mMethodsGet(CurrentRunner())->TestedFailures(CurrentRunner(), &numMask);
    AtSurEngine engine = TestedEngine();

    for (i = 0; i < numMask; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineFailureNotificationMaskSet(engine, enableMask[i], enableMask[i]));
        TEST_ASSERT_EQUAL_INT(enableMask[i], AtSurEngineFailureNotificationMaskGet(engine));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineFailureNotificationMaskSet(engine, enableMask[i], 0x0));
        TEST_ASSERT_EQUAL_INT(0, AtSurEngineFailureNotificationMaskGet(engine));
        }
    }

static void testCanClearHistoryTca(void)
    {
    AtSurEngineTcaChangedHistoryGet(TestedEngine());
    AtSurEngineTcaChangedHistoryClear(TestedEngine());
    }

static void testConfigureTcaInteruptMask(void)
    {
    uint8 numMask, i;
    const uint32 *enableMask = mMethodsGet(CurrentRunner())->TestedTca(CurrentRunner(), &numMask);
    AtSurEngine engine = TestedEngine();

    if (!AtSurEngineTcaIsSupported(engine))
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSurEngineTcaNotificationMaskSet(engine, enableMask[0], enableMask[0]));
        AtTestLoggerEnable(cAtTrue);
        return;
        }

    for (i = 0; i < numMask; i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineTcaNotificationMaskSet(engine, enableMask[i], enableMask[i]));
        if (enableMask[i] != AtSurEngineTcaNotificationMaskGet(engine))
            AtSurEngineTcaNotificationMaskGet(engine);
        TEST_ASSERT_EQUAL_INT(enableMask[i], AtSurEngineTcaNotificationMaskGet(engine));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineTcaNotificationMaskSet(engine, enableMask[i], 0x0));
        TEST_ASSERT_EQUAL_INT(0, AtSurEngineTcaNotificationMaskGet(engine));
        }
    }

static void testCanGetCounters(void)
    {
    AtSurEngineTestRunner runner = CurrentRunner();
    AtSurEngine engine = TestedEngine();
    static eAtPmRegisterType regType[] = {cAtPmCurrentSecondRegister,
                                          cAtPmCurrentPeriodRegister,
                                          cAtPmPreviousPeriodRegister,
                                          cAtPmRecentPeriodRegister,
                                          cAtPmCurrentDayRegister,
                                          cAtPmPreviousDayRegister};
    void *pmCounters = mMethodsGet(runner)->PmCounterAddress(runner);
    uint8 type_i;

    for (type_i = 0; type_i < mCount(regType); type_i++)
        {
        if (AtModuleSurTestRunnerRegisterTypeMustBeSupported(ModuleTestRunner(), regType[type_i]))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineAllCountersGet(engine, regType[type_i], pmCounters));
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineAllCountersClear(engine, regType[type_i], pmCounters));

            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtSurEngineAllCountersGet(engine, regType[type_i], NULL));
            TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtSurEngineAllCountersClear(engine, regType[type_i], NULL));
            TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtSurEngineAllCountersGet(NULL, regType[type_i], pmCounters));
            TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtSurEngineAllCountersClear(NULL, regType[type_i], pmCounters));
            AtTestLoggerEnable(cAtTrue);
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSurEngineAllCountersGet(engine, regType[type_i], pmCounters));
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSurEngineAllCountersClear(engine, regType[type_i], pmCounters));
            AtTestLoggerEnable(cAtTrue);
            }
        }

    }

static void testCanAddRemoveEventListener(void)
    {
    tAtSurEngineListener listener;
    AtOsalMemInit(&listener, 0, sizeof(listener));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineListenerAdd(TestedEngine(), NULL, &listener));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSurEngineListenerRemove(TestedEngine(), NULL, &listener));
    }

static void testSurEngineWithThresholdProfile(void)
    {
    uint32 i;
    AtSurEngine engine = TestedEngine();
    AtModuleSur surModule = AtSurEngineModuleGet(engine);
    uint32 maxProfileId = AtModuleSurNumThresholdProfiles(surModule);
    for (i = 0; i < maxProfileId; i++)
        {
        AtPmThresholdProfile profile = AtModuleSurThresholdProfileGet(surModule, i);
        TEST_ASSERT(AtSurEngineThresholdProfileSet(engine, profile) == cAtOk);
        TEST_ASSERT(AtSurEngineThresholdProfileGet(engine) == profile);
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSurEngineThresholdProfileSet(engine, NULL) != cAtOk);
    TEST_ASSERT(AtSurEngineThresholdProfileSet(NULL, NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testSurEngineWithSesThresholdProfile(void)
    {
    uint32 i;
    AtSurEngine engine = TestedEngine();
    AtModuleSur surModule = AtSurEngineModuleGet(engine);
    uint32 maxProfileId = AtModuleSurNumSesThresholdProfiles(surModule);
    for (i = 0; i < maxProfileId; i++)
        {
        AtPmSesThresholdProfile profile = AtModuleSurSesThresholdProfileGet(surModule, i);
        TEST_ASSERT(AtSurEngineSesThresholdProfileSet(engine, profile) == cAtOk);
        TEST_ASSERT(AtSurEngineSesThresholdProfileGet(engine) == profile);
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSurEngineSesThresholdProfileSet(engine, NULL) != cAtOk);
    TEST_ASSERT(AtSurEngineSesThresholdProfileSet(NULL, NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static eBool CanEnable(AtSurEngineTestRunner self, eBool enable)
    {
    /* Default cannot disable */
    return enable;
    }

static uint32 *TestedFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    if (numFailures)
        *numFailures = 0;
    return NULL;
    }

static uint32 *TestedTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    if (numTca)
        *numTca = 0;
    return NULL;
    }

static void *PmCounterAddress(AtSurEngineTestRunner self)
    {
    return NULL;
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtSurEngine_Fixtures)
        {
        new_TestFixture("testCanGetModule", testCanGetModule),
        new_TestFixture("testCanGetChannel", testCanGetChannel),
        new_TestFixture("testCanInitialize", testCanInitialize),
        new_TestFixture("testCanEnableDisable", testCanEnableDisable),
        new_TestFixture("testCanEnableDisablePm", testCanEnableDisablePm),
        new_TestFixture("testCanGetCurrentFailure", testCanGetCurrentFailure),
        new_TestFixture("testCanClearHistoryFailure", testCanClearHistoryFailure),
        new_TestFixture("testConfigureFailureInteruptMask", testConfigureFailureInteruptMask),
        new_TestFixture("testCanClearHistoryTca", testCanClearHistoryTca),
        new_TestFixture("testConfigureTcaInteruptMask", testConfigureTcaInteruptMask),
        new_TestFixture("testCanGetCounters", testCanGetCounters),
        new_TestFixture("testCanAddRemoveEventListener", testCanAddRemoveEventListener),
        new_TestFixture("testSurEngineWithThresholdProfile", testSurEngineWithThresholdProfile),
        new_TestFixture("testSurEngineWithSesThresholdProfile", testSurEngineWithSesThresholdProfile)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtSurEngine_Caller, "TestSuite_AtSurEngine", NULL, NULL, TestSuite_AtSurEngine_Fixtures);

    return (TestRef)((void *)&TestSuite_AtSurEngine_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtSurEngineTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void MethodsInit(AtSurEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CanEnable);
        mMethodOverride(m_methods, TestedFailures);
        mMethodOverride(m_methods, TestedTca);
        mMethodOverride(m_methods, PmCounterAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtSurEngineTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEngineTestRunner);
    }

AtSurEngineTestRunner AtSurEngineTestRunnerObjectInit(AtSurEngineTestRunner self, AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)engine) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->moduleRunner = moduleRunner;

    return self;
    }

AtSurEngine AtSurEngineTestRunnerEngineGet(AtSurEngineTestRunner self)
    {
    return (AtSurEngine)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

AtModule AtSurEngineTestRunnerModuleGet(AtSurEngineTestRunner self)
    {
    return (AtModule)AtSurEngineModuleGet(AtSurEngineTestRunnerEngineGet(self));
    }

AtDevice AtSurEngineTestRunnerDeviceGet(AtSurEngineTestRunner self)
    {
    return AtModuleDeviceGet(AtSurEngineTestRunnerModuleGet(self));
    }

AtModuleSurTestRunner AtSurEngineTestRunnerModuleRunnerGet(AtSurEngineTestRunner self)
    {
    return self ? self->moduleRunner : NULL;
    }

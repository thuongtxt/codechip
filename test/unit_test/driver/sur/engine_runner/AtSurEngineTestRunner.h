/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtSurEngineTestRunner.h
 * 
 * Created Date: May 18, 2018
 *
 * Description : SUR engine test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSURENGINETESTRUNNER_H_
#define _ATSURENGINETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
/*#include "../runner/AtUnittestRunner.h" *//* Super class */
#include "AtSurEngine.h"
#include "../AtModuleSurTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete runners */
AtSurEngineTestRunner AtSurEnginePwTestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner);
AtSurEngineTestRunner AtSurEnginePdhDe1TestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner);
AtSurEngineTestRunner AtSurEnginePdhDe3TestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner);
AtSurEngineTestRunner AtSurEngineSdhLineTestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner);
AtSurEngineTestRunner AtSurEngineSdhPathTestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner);

/* APIs */
AtSurEngine AtSurEngineTestRunnerEngineGet(AtSurEngineTestRunner self);
AtModule AtSurEngineTestRunnerModuleGet(AtSurEngineTestRunner self);
AtDevice AtSurEngineTestRunnerDeviceGet(AtSurEngineTestRunner self);
AtModuleSurTestRunner AtSurEngineTestRunnerModuleRunnerGet(AtSurEngineTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSURENGINETESTRUNNER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Unittest
 * 
 * File        : AtSurEngineTestRunnerInternal.h
 * 
 * Created Date: May 18, 2018
 *
 * Description : SUR engine test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSURENGINETESTRUNNERINTERNAL_H_
#define _ATSURENGINETESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtObjectTestRunnerInternal.h" /* Superclass */
#include "AtSurEngine.h"
#include "AtSurEngineTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSurEngineTestRunnerMethods
    {
    eBool (*CanEnable)(AtSurEngineTestRunner self, eBool enable);
    uint32 *(*TestedFailures)(AtSurEngineTestRunner self, uint8 *numFailures);
    uint32 *(*TestedTca)(AtSurEngineTestRunner self, uint8 *numTca);
    void *(*PmCounterAddress)(AtSurEngineTestRunner self);
    }tAtSurEngineTestRunnerMethods;

typedef struct tAtSurEngineTestRunner
    {
    tAtObjectTestRunner super;
    const tAtSurEngineTestRunnerMethods *methods;

    /* Private data */
    AtModuleSurTestRunner moduleRunner;
    }tAtSurEngineTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSurEngineTestRunner AtSurEngineTestRunnerObjectInit(AtSurEngineTestRunner self, AtSurEngine engine, AtModuleSurTestRunner moduleRunner);

#ifdef __cplusplus
}
#endif
#endif /* _ATSURENGINETESTRUNNERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtSurEngineSdhLineTestRunner.c
 *
 * Created Date: Jun 22, 2018
 *
 * Description : SUR engine test runner for SDH line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtSurEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSurEngineSdhLineTestRunner
    {
    tAtSurEngineTestRunner super;
    }tAtSurEngineSdhLineTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineTestRunnerMethods m_AtSurEngineTestRunnerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *TestedFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    static uint32 failures[] = {cAtSdhLineAlarmLos, cAtSdhLineAlarmLof, cAtSdhLineAlarmTim,
                                cAtSdhLineAlarmAis, cAtSdhLineAlarmRfi, cAtSdhLineAlarmBerSf,
                                cAtSdhLineAlarmBerSd};
    if (numFailures)
        *numFailures = mCount(failures);
    return failures;
    }

static uint32 *TestedTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    static uint32 tcaMasks[] = {
                                cAtSurEngineSdhLinePmParamSefsS,
                                cAtSurEngineSdhLinePmParamCvS,
                                cAtSurEngineSdhLinePmParamEsS,
                                cAtSurEngineSdhLinePmParamSesS,
                                cAtSurEngineSdhLinePmParamCvL,
                                cAtSurEngineSdhLinePmParamEsL,
                                cAtSurEngineSdhLinePmParamSesL,
                                cAtSurEngineSdhLinePmParamUasL,
                                cAtSurEngineSdhLinePmParamFcL,
                                cAtSurEngineSdhLinePmParamCvLfe,
                                cAtSurEngineSdhLinePmParamEsLfe,
                                cAtSurEngineSdhLinePmParamSesLfe,
                                cAtSurEngineSdhLinePmParamUasLfe,
                                cAtSurEngineSdhLinePmParamFcLfe
                                };
    if (numTca)
        *numTca = mCount(tcaMasks);
    return tcaMasks;
    }

static void *PmCounterAddress(AtSurEngineTestRunner self)
    {
    static tAtSurEngineSdhLinePmCounters counters;
    return &counters;
    }

static void OverrideAtSurEngineTestRunner(AtSurEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSurEngineTestRunnerOverride, mMethodsGet(self), sizeof(m_AtSurEngineTestRunnerOverride));

        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedFailures);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedTca);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, PmCounterAddress);
        }

    mMethodsSet(self, &m_AtSurEngineTestRunnerOverride);
    }

static void Override(AtSurEngineTestRunner self)
    {
    OverrideAtSurEngineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEngineSdhLineTestRunner);
    }

static AtSurEngineTestRunner ObjectInit(AtSurEngineTestRunner self, AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSurEngineTestRunnerObjectInit(self, engine, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngineTestRunner AtSurEngineSdhLineTestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtSurEngineTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, engine, moduleRunner);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtSurEnginePdhDe1TestRunner.c
 *
 * Created Date: Jun 22, 2018
 *
 * Description : SUR engine test runner for PDH DS1/E1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "../../../driver/src/generic/pdh/AtPdhDe1Internal.h"
#include "AtSurEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSurEnginePdhDe1TestRunner
    {
    tAtSurEngineTestRunner super;
    }tAtSurEnginePdhDe1TestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineTestRunnerMethods m_AtSurEngineTestRunnerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *TestedFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    static uint32 failures[] = {cAtPdhDe1AlarmLos, cAtPdhDe1AlarmLof,
                                cAtPdhDe1AlarmRai, cAtPdhDe1AlarmAis
                                };
    if (numFailures)
        *numFailures = mCount(failures);
    return failures;
    }

static uint32 *TestedAllTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    static uint32 tcaMasks[] = {
                                cAtSurEnginePdhDe1PmParamCvL,
                                cAtSurEnginePdhDe1PmParamEsL,
                                cAtSurEnginePdhDe1PmParamSesL,
                                cAtSurEnginePdhDe1PmParamLossL,
                                cAtSurEnginePdhDe1PmParamEsLfe,
                                cAtSurEnginePdhDe1PmParamCvP,
                                cAtSurEnginePdhDe1PmParamEsP,
                                cAtSurEnginePdhDe1PmParamSesP,
                                cAtSurEnginePdhDe1PmParamAissP,
                                cAtSurEnginePdhDe1PmParamSasP,
                                cAtSurEnginePdhDe1PmParamCssP,
                                cAtSurEnginePdhDe1PmParamUasP,
                                cAtSurEnginePdhDe1PmParamSefsPfe,
                                cAtSurEnginePdhDe1PmParamEsPfe,
                                cAtSurEnginePdhDe1PmParamSesPfe,
                                cAtSurEnginePdhDe1PmParamCssPfe,
                                cAtSurEnginePdhDe1PmParamFcPfe,
                                cAtSurEnginePdhDe1PmParamUasPfe,
                                cAtSurEnginePdhDe1PmParamFcP
                                };
    if (numTca)
        *numTca = mCount(tcaMasks);
    return tcaMasks;
    }

static uint32 *TestedNearEndTcaOnly(AtSurEngineTestRunner self, uint8 *numTca)
    {
    static uint32 tcaMasks[] = {
                                cAtSurEnginePdhDe1PmParamCvL,
                                cAtSurEnginePdhDe1PmParamEsL,
                                cAtSurEnginePdhDe1PmParamSesL,
                                cAtSurEnginePdhDe1PmParamLossL,
                                cAtSurEnginePdhDe1PmParamCvP,
                                cAtSurEnginePdhDe1PmParamEsP,
                                cAtSurEnginePdhDe1PmParamSesP,
                                cAtSurEnginePdhDe1PmParamAissP,
                                cAtSurEnginePdhDe1PmParamSasP,
                                cAtSurEnginePdhDe1PmParamCssP,
                                cAtSurEnginePdhDe1PmParamUasP,
                                cAtSurEnginePdhDe1PmParamFcP
                                };
    if (numTca)
        *numTca = mCount(tcaMasks);
    return tcaMasks;
    }

static uint32 *TestedTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtSurEngineChannelGet(AtSurEngineTestRunnerEngineGet(self));

    if (AtPdhDe1HasFarEndPerformance(de1, AtPdhChannelFrameTypeGet((AtPdhChannel)de1)))
        return TestedAllTca(self, numTca);

    return TestedNearEndTcaOnly(self, numTca);
    }

static void *PmCounterAddress(AtSurEngineTestRunner self)
    {
    static tAtSurEnginePdhDe1PmCounters counters;
    return &counters;
    }

static void OverrideAtSurEngineTestRunner(AtSurEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSurEngineTestRunnerOverride, mMethodsGet(self), sizeof(m_AtSurEngineTestRunnerOverride));

        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedFailures);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedTca);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, PmCounterAddress);
        }

    mMethodsSet(self, &m_AtSurEngineTestRunnerOverride);
    }

static void Override(AtSurEngineTestRunner self)
    {
    OverrideAtSurEngineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEnginePdhDe1TestRunner);
    }

static AtSurEngineTestRunner ObjectInit(AtSurEngineTestRunner self, AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSurEngineTestRunnerObjectInit(self, engine, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngineTestRunner AtSurEnginePdhDe1TestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtSurEngineTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, engine, moduleRunner);
    }

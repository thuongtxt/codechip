/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtSurEnginePdhDe3TestRunner.c
 *
 * Created Date: Jun 22, 2018
 *
 * Description : SUR engine test runner for PDH DS3/E3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "AtSurEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSurEnginePdhDe3TestRunner
    {
    tAtSurEngineTestRunner super;
    }tAtSurEnginePdhDe3TestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineTestRunnerMethods m_AtSurEngineTestRunnerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *TestedFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    static uint32 failures[] = {cAtPdhDe3AlarmLos, cAtPdhDe3AlarmLof,
                                cAtPdhDe3AlarmRai, cAtPdhDe3AlarmAis
                                };
    if (numFailures)
        *numFailures = mCount(failures);
    return failures;
    }

static uint32 *TestedDs3CbitTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    static uint32 tcaMasks[] = {
                                cAtSurEnginePdhDe3PmParamCvL,
                                cAtSurEnginePdhDe3PmParamEsL,
                                cAtSurEnginePdhDe3PmParamSesL,
                                cAtSurEnginePdhDe3PmParamLossL,
                                cAtSurEnginePdhDe3PmParamCvpP,
                                cAtSurEnginePdhDe3PmParamCvcpP,
                                cAtSurEnginePdhDe3PmParamEspP,
                                cAtSurEnginePdhDe3PmParamEscpP,
                                cAtSurEnginePdhDe3PmParamSespP,
                                cAtSurEnginePdhDe3PmParamSescpP,
                                cAtSurEnginePdhDe3PmParamSasP,
                                cAtSurEnginePdhDe3PmParamAissP,
                                cAtSurEnginePdhDe3PmParamUaspP,
                                cAtSurEnginePdhDe3PmParamUascpP,
                                cAtSurEnginePdhDe3PmParamCvcpPfe,
                                cAtSurEnginePdhDe3PmParamEscpPfe,
                                cAtSurEnginePdhDe3PmParamSescpPfe,
                                cAtSurEnginePdhDe3PmParamSascpPfe,
                                cAtSurEnginePdhDe3PmParamUascpPfe,
                                cAtSurEnginePdhDe3PmParamEsbcpPfe,
                                cAtSurEnginePdhDe3PmParamEsaL,
                                cAtSurEnginePdhDe3PmParamEsbL,
                                cAtSurEnginePdhDe3PmParamEsapP,
                                cAtSurEnginePdhDe3PmParamEsacpP,
                                cAtSurEnginePdhDe3PmParamEsbpP,
                                cAtSurEnginePdhDe3PmParamEsbcpP,
                                cAtSurEnginePdhDe3PmParamFcP,
                                cAtSurEnginePdhDe3PmParamEsacpPfe,
                                cAtSurEnginePdhDe3PmParamFccpPfe
                                };
    if (numTca)
        *numTca = mCount(tcaMasks);
    return tcaMasks;
    }

static uint32 *TestedNonDs3CbitTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    static uint32 tcaMasks[] = {
                                cAtSurEnginePdhDe3PmParamCvL,
                                cAtSurEnginePdhDe3PmParamEsL,
                                cAtSurEnginePdhDe3PmParamSesL,
                                cAtSurEnginePdhDe3PmParamLossL,
                                cAtSurEnginePdhDe3PmParamCvpP,
                                cAtSurEnginePdhDe3PmParamEspP,
                                cAtSurEnginePdhDe3PmParamSespP,
                                cAtSurEnginePdhDe3PmParamSasP,
                                cAtSurEnginePdhDe3PmParamAissP,
                                cAtSurEnginePdhDe3PmParamUaspP,
                                cAtSurEnginePdhDe3PmParamEsaL,
                                cAtSurEnginePdhDe3PmParamEsbL,
                                cAtSurEnginePdhDe3PmParamEsapP,
                                cAtSurEnginePdhDe3PmParamEsbpP,
                                cAtSurEnginePdhDe3PmParamFcP
                                };
    if (numTca)
        *numTca = mCount(tcaMasks);
    return tcaMasks;
    }

static uint32 *TestedTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtSurEngineChannelGet(AtSurEngineTestRunnerEngineGet(self));

    if (AtPdhDe3IsDs3CbitFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)de3)))
        return TestedDs3CbitTca(self, numTca);

    return TestedNonDs3CbitTca(self, numTca);
    }

static void *PmCounterAddress(AtSurEngineTestRunner self)
    {
    static tAtSurEnginePdhDe3PmCounters counters;
    return &counters;
    }

static void OverrideAtSurEngineTestRunner(AtSurEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSurEngineTestRunnerOverride, mMethodsGet(self), sizeof(m_AtSurEngineTestRunnerOverride));

        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedFailures);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedTca);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, PmCounterAddress);
        }

    mMethodsSet(self, &m_AtSurEngineTestRunnerOverride);
    }

static void Override(AtSurEngineTestRunner self)
    {
    OverrideAtSurEngineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEnginePdhDe3TestRunner);
    }

static AtSurEngineTestRunner ObjectInit(AtSurEngineTestRunner self, AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSurEngineTestRunnerObjectInit(self, engine, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngineTestRunner AtSurEnginePdhDe3TestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtSurEngineTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, engine, moduleRunner);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtSurEngineSdhPathTestRunner.c
 *
 * Created Date: Jun 22, 2018
 *
 * Description : SUR engine test runner for SDH path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "../../../driver/src/generic/sdh/AtSdhVcInternal.h"
#include "AtSurEngineTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSurEngineSdhLineTestRunner
    {
    tAtSurEngineTestRunner super;
    }tAtSurEngineSdhLineTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineTestRunnerMethods m_AtSurEngineTestRunnerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *TestedHoPathFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    static uint32 failures[] = {cAtSdhPathAlarmLop, cAtSdhPathAlarmAis, cAtSdhPathAlarmTim,
                                cAtSdhPathAlarmUneq, cAtSdhPathAlarmPlm, cAtSdhPathAlarmRfi,
                                cAtSdhPathAlarmRfiS, cAtSdhPathAlarmRfiC, cAtSdhPathAlarmRfiP,
                                cAtSdhPathAlarmLom, cAtSdhPathAlarmBerSf, cAtSdhPathAlarmBerSd};
    if (numFailures)
        *numFailures = mCount(failures);
    return failures;
    }

static uint32 *TestedLoPathFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    static uint32 failures[] = {cAtSdhPathAlarmLop, cAtSdhPathAlarmAis, cAtSdhPathAlarmTim,
                                cAtSdhPathAlarmUneq, cAtSdhPathAlarmPlm, cAtSdhPathAlarmRfi,
                                cAtSdhPathAlarmRfiS, cAtSdhPathAlarmRfiC, cAtSdhPathAlarmRfiP,
                                cAtSdhPathAlarmBerSf, cAtSdhPathAlarmBerSd};
    if (numFailures)
        *numFailures = mCount(failures);
    return failures;
    }

static uint32 *TestedFailures(AtSurEngineTestRunner self, uint8 *numFailures)
    {
    AtSdhVc vc = (AtSdhVc)AtSurEngineChannelGet(AtSurEngineTestRunnerEngineGet(self));

    if (AtSdhVcIsHoVc(vc))
        return TestedHoPathFailures(self, numFailures);

    return TestedLoPathFailures(self, numFailures);
    }

static uint32 *TestedTca(AtSurEngineTestRunner self, uint8 *numTca)
    {
    static uint32 tcaMasks[] = {
                                cAtSurEngineSdhPathPmParamPpjcPdet,
                                cAtSurEngineSdhPathPmParamNpjcPdet,
                                cAtSurEngineSdhPathPmParamPpjcPgen,
                                cAtSurEngineSdhPathPmParamNpjcPgen,
                                cAtSurEngineSdhPathPmParamPjcDiff,
                                cAtSurEngineSdhPathPmParamPjcsPdet,
                                cAtSurEngineSdhPathPmParamPjcsPgen,
                                cAtSurEngineSdhPathPmParamCvP,
                                cAtSurEngineSdhPathPmParamEsP,
                                cAtSurEngineSdhPathPmParamSesP,
                                cAtSurEngineSdhPathPmParamUasP,
                                cAtSurEngineSdhPathPmParamFcP,
                                cAtSurEngineSdhPathPmParamCvPfe,
                                cAtSurEngineSdhPathPmParamEsPfe,
                                cAtSurEngineSdhPathPmParamSesPfe,
                                cAtSurEngineSdhPathPmParamUasPfe,
                                cAtSurEngineSdhPathPmParamFcPfe
                                };
    if (numTca)
        *numTca = mCount(tcaMasks);
    return tcaMasks;
    }

static void *PmCounterAddress(AtSurEngineTestRunner self)
    {
    static tAtSurEngineSdhPathPmCounters counters;
    return &counters;
    }

static void OverrideAtSurEngineTestRunner(AtSurEngineTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtSurEngineTestRunnerOverride, mMethodsGet(self), sizeof(m_AtSurEngineTestRunnerOverride));

        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedFailures);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, TestedTca);
        mMethodOverride(m_AtSurEngineTestRunnerOverride, PmCounterAddress);
        }

    mMethodsSet(self, &m_AtSurEngineTestRunnerOverride);
    }

static void Override(AtSurEngineTestRunner self)
    {
    OverrideAtSurEngineTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEngineSdhLineTestRunner);
    }

static AtSurEngineTestRunner ObjectInit(AtSurEngineTestRunner self, AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSurEngineTestRunnerObjectInit(self, engine, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngineTestRunner AtSurEngineSdhPathTestRunnerNew(AtSurEngine engine, AtModuleSurTestRunner moduleRunner)
    {
    AtSurEngineTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, engine, moduleRunner);
    }

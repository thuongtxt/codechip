/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtModuleSurTestRunnerInternal.h
 * 
 * Created Date: May 18, 2018
 *
 * Description : SUR Module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESURTESTRUNNERINTERNAL_H_
#define _ATMODULESURTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "AtModuleSurTestRunner.h"
#include "engine_runner/AtSurEngineTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSurTestRunnerMethods
    {
    AtSurEngineTestRunner (*CreateSdhLineSurEngineTestRunner)(AtModuleSurTestRunner self, AtSurEngine engine);
    AtSurEngineTestRunner (*CreateSdhPathSurEngineTestRunner)(AtModuleSurTestRunner self, AtSurEngine engine);
    AtSurEngineTestRunner (*CreatePdhDe3SurEngineTestRunner)(AtModuleSurTestRunner self, AtSurEngine engine);
    AtSurEngineTestRunner (*CreatePdhDe1SurEngineTestRunner)(AtModuleSurTestRunner self, AtSurEngine engine);
    AtSurEngineTestRunner (*CreatePwSurEngineTestRunner)(AtModuleSurTestRunner self, AtSurEngine engine);
    eBool (*RegisterTypeMustBeSupported)(AtModuleSurTestRunner self, eAtPmRegisterType type);
    }tAtModuleSurTestRunnerMethods;

typedef struct tAtModuleSurTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModuleSurTestRunnerMethods *methods;
    }tAtModuleSurTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModuleSurTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESURTESTRUNNERINTERNAL_H_ */


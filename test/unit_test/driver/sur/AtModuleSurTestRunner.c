/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtModuleSurTestRunner.c
 *
 * Created Date: May 18, 2018
 *
 * Description : SUR Module test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../driver/src/generic/sdh/AtSdhPathInternal.h"
#include "../../../../driver/src/generic/sur/AtModuleSurInternal.h"
#include "AtModuleSurTestRunnerInternal.h"
#include "profile_runner/AtPmThresholdProfileTestRunner.h"
#include "profile_runner/AtPmSesThresholdProfileTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleSurTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleSurTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSurEngineTestRunner CreateSdhLineSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    return AtSurEngineSdhLineTestRunnerNew(engine, self);
    }

static AtSurEngineTestRunner CreateSdhPathSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    return AtSurEngineSdhPathTestRunnerNew(engine, self);
    }

static AtSurEngineTestRunner CreatePdhDe3SurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    return AtSurEnginePdhDe3TestRunnerNew(engine, self);
    }

static AtSurEngineTestRunner CreatePdhDe1SurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    return AtSurEnginePdhDe1TestRunnerNew(engine, self);
    }

static AtSurEngineTestRunner CreatePwSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    return AtSurEnginePwTestRunnerNew(engine, self);
    }

static AtModuleTestRunner CurrentRunner()
    {
    return (AtModuleTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtModuleSur Module()
    {
    return (AtModuleSur)AtModuleTestRunnerModuleGet(CurrentRunner());
    }

static void setUp()
    {
    TEST_ASSERT_NOT_NULL(Module());
    AtModuleInit((AtModule)Module());
    }

static void tearDown()
    {
    }

static void testChangeExpireMethod(void)
    {
    AtModuleSur module = Module();
    static uint32 methods[] = {cAtSurPeriodExpireMethodManual, cAtSurPeriodExpireMethodAuto};
    uint32 method_i;

    for (method_i = 0; method_i < mCount(methods); method_i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleSurExpireMethodSet(module, methods[method_i]));
        TEST_ASSERT_EQUAL_INT(methods[method_i], AtModuleSurExpireMethodGet(module));
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleSurExpireMethodSet(NULL, cAtSurPeriodExpireMethodManual));
    TEST_ASSERT_EQUAL_INT(cAtSurPeriodExpireMethodUnknown, AtModuleSurExpireMethodGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeTickSource(void)
    {
    AtModuleSur module = Module();
    static uint32 sources[] = {cAtSurTickSourceInternal, cAtSurTickSourceExternal};
    uint32 source_i;

    for (source_i = 0; source_i < mCount(sources); source_i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleSurTickSourceSet(module, sources[source_i]));
        TEST_ASSERT_EQUAL_INT(sources[source_i], AtModuleSurTickSourceGet(module));
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleSurTickSourceSet(NULL, cAtSurTickSourceInternal));
    TEST_ASSERT_EQUAL_INT(cAtSurTickSourceUnknown, AtModuleSurTickSourceGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeHoldOffTimer(void)
    {
    AtModuleSur module = Module();

    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleSurFailureHoldOffTimerSet(module, 10000));
    TEST_ASSERT_EQUAL_INT(10000, AtModuleSurFailureHoldOffTimerGet(module));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleSurFailureHoldOffTimerSet(NULL, 10000));
    TEST_ASSERT_EQUAL_INT(((uint32)-1), AtModuleSurFailureHoldOffTimerGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static eBool CanTestHoldOnTimer()
    {
    AtModuleSur module = Module();
    return mMethodsGet(module)->FailureHoldOnTimerConfigurable(module);
    }

static void testChangeHoldOnTimer(void)
    {
    AtModuleSur module = Module();
    static uint32 cDefaultHoldOnTimerMs = 10000;

    if (!CanTestHoldOnTimer())
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT_EQUAL_INT(cAtErrorNotEditable, AtModuleSurFailureHoldOnTimerSet(module, 1000));
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleSurFailureHoldOnTimerSet(module, cDefaultHoldOnTimerMs));
        TEST_ASSERT_EQUAL_INT(cDefaultHoldOnTimerMs, AtModuleSurFailureHoldOnTimerGet(module));
        AtTestLoggerEnable(cAtTrue);
        return;
        }

    TEST_ASSERT_EQUAL_INT(cAtOk, AtModuleSurFailureHoldOnTimerSet(module, 1000));
    TEST_ASSERT_EQUAL_INT(1000, AtModuleSurFailureHoldOnTimerGet(module));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtModuleSurFailureHoldOnTimerSet(NULL, 1000));
    TEST_ASSERT_EQUAL_INT(((uint32)-1), AtModuleSurFailureHoldOnTimerGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef _TestSuite()
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModuleSur_Fixtures)
        {
        new_TestFixture("testChangeExpireMethod", testChangeExpireMethod),
        new_TestFixture("testChangeTickSource", testChangeTickSource),
        new_TestFixture("testChangeHoldOffTimer", testChangeHoldOffTimer),
        new_TestFixture("testChangeHoldOnTimer", testChangeHoldOnTimer)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModuleSur_Caller, "TestSuite_ModuleSur", setUp, tearDown, TestSuite_ModuleSur_Fixtures);

    return (TestRef)((void *)&TestSuite_ModuleSur_Caller);
    }

static void TestModule(AtModuleSurTestRunner self)
    {
    TextUIRunner_runTest(_TestSuite());
    }

static void TestPmThresholdProfile(AtModuleSurTestRunner self)
    {
    uint32 profile_i;
    AtModuleSur surModule = Module();
    uint32 maxProfileId = AtModuleSurNumThresholdProfiles(surModule);

    for (profile_i = 0; profile_i < maxProfileId; profile_i++)
        {
        AtUnittestRunner runner = (AtUnittestRunner)AtPmThresholdProfileTestRunnerNew(AtModuleSurThresholdProfileGet(surModule, profile_i));
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        }

    return;
    }

static void TestPmSesThresholdProfile(AtModuleSurTestRunner self)
    {
    uint32 profile_i;
    AtModuleSur surModule = Module();
    uint32 maxProfileId = AtModuleSurNumSesThresholdProfiles(surModule);

    for (profile_i = 0; profile_i < maxProfileId; profile_i++)
        {
        AtUnittestRunner runner = (AtUnittestRunner)AtPmSesThresholdProfileTestRunnerNew(AtModuleSurSesThresholdProfileGet(surModule, profile_i));
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        }

    return;
    }

static void Run(AtUnittestRunner self)
    {
    AtModuleSurTestRunner runner = (AtModuleSurTestRunner)self;

    m_AtUnittestRunnerMethods->Run(self);

    TestModule(runner);
    TestPmThresholdProfile(runner);
    TestPmSesThresholdProfile(runner);
    }

static eBool RegisterTypeMustBeSupported(AtModuleSurTestRunner self, eAtPmRegisterType type)
    {
    if ((type == cAtPmCurrentPeriodRegister) ||
        (type == cAtPmPreviousPeriodRegister))
        return cAtTrue;
    return cAtFalse;
    }

static void Delete(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Delete(self);
    }

static void MethodsInit(AtModuleSurTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CreateSdhLineSurEngineTestRunner);
        mMethodOverride(m_methods, CreateSdhPathSurEngineTestRunner);
        mMethodOverride(m_methods, CreatePdhDe3SurEngineTestRunner);
        mMethodOverride(m_methods, CreatePdhDe1SurEngineTestRunner);
        mMethodOverride(m_methods, CreatePwSurEngineTestRunner);
        mMethodOverride(m_methods, RegisterTypeMustBeSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleSurTestRunner);
    }

AtModuleTestRunner AtModuleSurTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((AtModuleSurTestRunner)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModuleSurTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModuleSurTestRunnerObjectInit(newRunner, module);
    }

AtSurEngineTestRunner AtModuleSurTestRunnerCreateSdhLineSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->CreateSdhLineSurEngineTestRunner(self, engine);
    return NULL;
    }

AtSurEngineTestRunner AtModuleSurTestRunnerCreateSdhPathSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->CreateSdhPathSurEngineTestRunner(self, engine);
    return NULL;
    }

AtSurEngineTestRunner AtModuleSurTestRunnerCreatePdhDe3SurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->CreatePdhDe3SurEngineTestRunner(self, engine);
    return NULL;
    }

AtSurEngineTestRunner AtModuleSurTestRunnerCreatePdhDe1SurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->CreatePdhDe1SurEngineTestRunner(self, engine);
    return NULL;
    }

AtSurEngineTestRunner AtModuleSurTestRunnerCreatePwSurEngineTestRunner(AtModuleSurTestRunner self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->CreatePwSurEngineTestRunner(self, engine);
    return NULL;
    }

eBool AtModuleSurTestRunnerRegisterTypeMustBeSupported(AtModuleSurTestRunner self, eAtPmRegisterType type)
    {
    if (self)
        return mMethodsGet(self)->RegisterTypeMustBeSupported(self, type);
    return cAtFalse;
    }

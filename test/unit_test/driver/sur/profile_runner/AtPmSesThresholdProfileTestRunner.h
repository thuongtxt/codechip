/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmSesThresholdProfileTestRunner.h
 * 
 * Created Date: Jun 25, 2018
 *
 * Description : TCA threshold profile test runner.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMSESTHRESHOLDPROFILETESTRUNNER_H_
#define _ATPMSESTHRESHOLDPROFILETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/AtObjectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPmSesThresholdProfileTestRunner* AtPmSesThresholdProfileTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmSesThresholdProfileTestRunner AtPmSesThresholdProfileTestRunnerNew(AtPmSesThresholdProfile profile);

#ifdef __cplusplus
}
#endif
#endif /* _ATPMSESTHRESHOLDPROFILETESTRUNNER_H_ */


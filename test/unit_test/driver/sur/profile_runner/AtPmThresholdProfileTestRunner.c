/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtPmThresholdProfileTestRunner.c
 *
 * Created Date: Sep 13, 2016
 *
 * Description : PM TCA threshold profile test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPmThresholdProfile.h"
#include "../../man/AtObjectTestRunnerInternal.h"
#include "AtPmThresholdProfileTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPmThresholdProfileTestRunner
    {
    tAtObjectTestRunner super;
    }tAtPmThresholdProfileTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtUnittestRunnerMethods     m_AtUnittestRunnerOverride;

static const tAtUnittestRunnerMethods   *m_AtUnittestRunnerMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmThresholdProfile CurrentProfile(void)
    {
    return (AtPmThresholdProfile) AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testChangeSdhLinePeriodTcaThresholdValue(void)
    {
    AtPmThresholdProfile self = (AtPmThresholdProfile)CurrentProfile();
    eAtSurEngineSdhLinePmParam param[] = {cAtSurEngineSdhLinePmParamSefsS,
                                          cAtSurEngineSdhLinePmParamCvS,
                                          cAtSurEngineSdhLinePmParamEsS,
                                          cAtSurEngineSdhLinePmParamSesS,
                                          cAtSurEngineSdhLinePmParamCvL,
                                          cAtSurEngineSdhLinePmParamEsL,
                                          cAtSurEngineSdhLinePmParamSesL,
                                          cAtSurEngineSdhLinePmParamUasL,
                                          cAtSurEngineSdhLinePmParamFcL,
                                          cAtSurEngineSdhLinePmParamCvLfe,
                                          cAtSurEngineSdhLinePmParamEsLfe,
                                          cAtSurEngineSdhLinePmParamSesLfe,
                                          cAtSurEngineSdhLinePmParamUasLfe,
                                          cAtSurEngineSdhLinePmParamFcLfe};
    int numParam = mCount(param);
    int i;

    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, param[i], value));
        }
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(value, AtPmThresholdProfileSdhLinePeriodTcaThresholdGet(self, param[i]));
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(NULL, param[0], 200));
    TEST_ASSERT(AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, 0, 200) != cAtOk);

    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfileSdhLinePeriodTcaThresholdGet(NULL, param[0]));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfileSdhLinePeriodTcaThresholdGet(self, 0));
    AtTestLoggerEnable(cAtTrue);

    return;
    }

static eAtSurEngineSdhPathPmParam *SdhPathPmParamShouldBeTested(int *numPmParams)
    {
    static eAtSurEngineSdhPathPmParam param[] = {
                                                cAtSurEngineSdhPathPmParamPpjcPdet,
                                                cAtSurEngineSdhPathPmParamNpjcPdet,
                                                cAtSurEngineSdhPathPmParamPpjcPgen,
                                                cAtSurEngineSdhPathPmParamNpjcPgen,
                                                cAtSurEngineSdhPathPmParamPjcDiff ,
                                                cAtSurEngineSdhPathPmParamPjcsPdet,
                                                cAtSurEngineSdhPathPmParamPjcsPgen,
                                                cAtSurEngineSdhPathPmParamCvP     ,
                                                cAtSurEngineSdhPathPmParamEsP     ,
                                                cAtSurEngineSdhPathPmParamSesP    ,
                                                cAtSurEngineSdhPathPmParamUasP    ,
                                                cAtSurEngineSdhPathPmParamFcP     ,
                                                cAtSurEngineSdhPathPmParamCvPfe   ,
                                                cAtSurEngineSdhPathPmParamEsPfe   ,
                                                cAtSurEngineSdhPathPmParamSesPfe  ,
                                                cAtSurEngineSdhPathPmParamUasPfe  ,
                                                cAtSurEngineSdhPathPmParamFcPfe
                                                };
    if (numPmParams)
        *numPmParams = mCount(param);
    return param;
    }

static void testChangeSdhHoPathPeriodTcaThresholdValue(void)
    {
    AtPmThresholdProfile self = (AtPmThresholdProfile)CurrentProfile();
    int numParam = 0;
    eAtSurEngineSdhPathPmParam *param = SdhPathPmParamShouldBeTested(&numParam);
    int i;
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, param[i], value));
        }
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(value, AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet(self, param[i]));
        }

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, 0, 200) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(NULL, param[0], 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet(self, 0));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet(NULL, param[0]));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeSdhLoPathPeriodTcaThresholdValue(void)
    {
    AtPmThresholdProfile self = (AtPmThresholdProfile)CurrentProfile();
    int numParam;
    eAtSurEngineSdhPathPmParam *param = SdhPathPmParamShouldBeTested(&numParam);
    int i;
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, param[i], value));
        }
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(value, AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet(self, param[i]));
        }
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, 0, 200) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(NULL, param[0], 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet(self, 0));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet(NULL, param[0]));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePdhDe3PeriodTcaThresholdValue(void)
    {
    AtPmThresholdProfile self = (AtPmThresholdProfile)CurrentProfile();
    eAtSurEnginePdhDe3PmParam param[] = {cAtSurEnginePdhDe3PmParamCvL     ,
                                         cAtSurEnginePdhDe3PmParamEsL     ,
                                         cAtSurEnginePdhDe3PmParamSesL    ,
                                         cAtSurEnginePdhDe3PmParamLossL   ,
                                         cAtSurEnginePdhDe3PmParamCvpP    ,
                                         cAtSurEnginePdhDe3PmParamCvcpP   ,
                                         cAtSurEnginePdhDe3PmParamEspP    ,
                                         cAtSurEnginePdhDe3PmParamEscpP   ,
                                         cAtSurEnginePdhDe3PmParamSespP   ,
                                         cAtSurEnginePdhDe3PmParamSescpP  ,
                                         cAtSurEnginePdhDe3PmParamSasP    ,
                                         cAtSurEnginePdhDe3PmParamAissP   ,
                                         cAtSurEnginePdhDe3PmParamUaspP   ,
                                         cAtSurEnginePdhDe3PmParamUascpP  ,
                                         cAtSurEnginePdhDe3PmParamCvcpPfe ,
                                         cAtSurEnginePdhDe3PmParamEscpPfe ,
                                         cAtSurEnginePdhDe3PmParamSescpPfe,
                                         cAtSurEnginePdhDe3PmParamSascpPfe,
                                         cAtSurEnginePdhDe3PmParamUascpPfe,
                                         cAtSurEnginePdhDe3PmParamEsbcpPfe,
                                         cAtSurEnginePdhDe3PmParamEsaL    ,
                                         cAtSurEnginePdhDe3PmParamEsbL    ,
                                         cAtSurEnginePdhDe3PmParamEsapP   ,
                                         cAtSurEnginePdhDe3PmParamEsacpP  ,
                                         cAtSurEnginePdhDe3PmParamEsbpP   ,
                                         cAtSurEnginePdhDe3PmParamEsbcpP  ,
                                         cAtSurEnginePdhDe3PmParamFcP     ,
                                         cAtSurEnginePdhDe3PmParamEsacpPfe,
                                         cAtSurEnginePdhDe3PmParamFccpPfe };
    int numParam = mCount(param);
    int i;
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, param[i], value));
        }
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(value, AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet(self, param[i]));
        }
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, 0, 200) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(NULL, param[0], 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet(self, 0));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet(NULL, param[0]));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePdhDe1PeriodTcaThresholdValue(void)
    {
    AtPmThresholdProfile self = (AtPmThresholdProfile)CurrentProfile();
    eAtSurEnginePdhDe1PmParam param[] = {cAtSurEnginePdhDe1PmParamCvL    ,
                                         cAtSurEnginePdhDe1PmParamEsL    ,
                                         cAtSurEnginePdhDe1PmParamSesL   ,
                                         cAtSurEnginePdhDe1PmParamLossL  ,
                                         cAtSurEnginePdhDe1PmParamEsLfe  ,
                                         cAtSurEnginePdhDe1PmParamCvP    ,
                                         cAtSurEnginePdhDe1PmParamEsP    ,
                                         cAtSurEnginePdhDe1PmParamSesP   ,
                                         cAtSurEnginePdhDe1PmParamAissP  ,
                                         cAtSurEnginePdhDe1PmParamSasP   ,
                                         cAtSurEnginePdhDe1PmParamCssP   ,
                                         cAtSurEnginePdhDe1PmParamUasP   ,
                                         cAtSurEnginePdhDe1PmParamSefsPfe,
                                         cAtSurEnginePdhDe1PmParamEsPfe  ,
                                         cAtSurEnginePdhDe1PmParamSesPfe ,
                                         cAtSurEnginePdhDe1PmParamCssPfe ,
                                         cAtSurEnginePdhDe1PmParamFcPfe  ,
                                         cAtSurEnginePdhDe1PmParamUasPfe ,
                                         cAtSurEnginePdhDe1PmParamFcP    };
    int numParam = mCount(param);
    int i;
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, param[i], value));
        }
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(value, AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet(self, param[i]));
        }
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, 0, 200) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(NULL, param[0], 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet(self, 0));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet(NULL, param[0]));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePwPeriodTcaThresholdValue(void)
    {
    AtPmThresholdProfile self = (AtPmThresholdProfile)CurrentProfile();
    eAtSurEnginePwPmParam param[] = {cAtSurEnginePwPmParamEs   ,
                                     cAtSurEnginePwPmParamSes  ,
                                     cAtSurEnginePwPmParamUas  ,
                                     cAtSurEnginePwPmParamFeEs ,
                                     cAtSurEnginePwPmParamFeSes,
                                     cAtSurEnginePwPmParamFeUas,
                                     cAtSurEnginePwPmParamFc};
    int numParam = mCount(param);
    int i;
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(cAtOk, AtPmThresholdProfilePwPeriodTcaThresholdSet(self, param[i], value));
        }
    for (i = 0; i < numParam; i ++)
        {
        uint32 value = i * 10;
        TEST_ASSERT_EQUAL_INT(value, AtPmThresholdProfilePwPeriodTcaThresholdGet(self, param[i]));
        }
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPmThresholdProfilePwPeriodTcaThresholdSet(self, 0, 200) != cAtOk);
    TEST_ASSERT_EQUAL_INT(cAtErrorNullPointer, AtPmThresholdProfilePwPeriodTcaThresholdSet(NULL, param[0], 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfilePwPeriodTcaThresholdGet(self, 0));
    TEST_ASSERT_EQUAL_INT(0, AtPmThresholdProfilePwPeriodTcaThresholdGet(NULL, param[0]));
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPmThresholdProfile_Fixtures)
        {
        new_TestFixture("testChangeSdhLinePeriodTcaThresholdValue", testChangeSdhLinePeriodTcaThresholdValue),
        new_TestFixture("testChangeSdhHoPathPeriodTcaThresholdValue", testChangeSdhHoPathPeriodTcaThresholdValue),
        new_TestFixture("testChangeSdhLoPathPeriodTcaThresholdValue", testChangeSdhLoPathPeriodTcaThresholdValue),
        new_TestFixture("testChangePdhDe3PeriodTcaThresholdValue", testChangePdhDe3PeriodTcaThresholdValue),
        new_TestFixture("testChangePdhDe1PeriodTcaThresholdValue", testChangePdhDe1PeriodTcaThresholdValue),
        new_TestFixture("testChangePwPeriodTcaThresholdValue", testChangePwPeriodTcaThresholdValue),
        };

    EMB_UNIT_TESTCALLER(TestSuite_AtPmThresholdProfile_Caller, "AtPmThresholdProfile", NULL, NULL, TestSuite_AtPmThresholdProfile_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPmThresholdProfile_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtPmThresholdProfile profile = (AtPmThresholdProfile)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    AtSnprintf(buffer, bufferSize, "PmTcaThresholdProfile %d", AtPmThresholdProfileIdGet(profile));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    AtUnittestRunner runner = self;
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy (&m_AtUnittestRunnerOverride, (void*)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }
    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtObjectTestRunner self)
    {
    OverrideAtUnittestRunner((void*)self);
    }

static uint32 ObjectSize(void)
    {
    return (sizeof(tAtPmThresholdProfileTestRunner));
    }

static AtPmThresholdProfileTestRunner ObjectInit(AtPmThresholdProfileTestRunner self, AtPmThresholdProfile profile)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)profile) == NULL)
        return NULL;

    Override((AtObjectTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtPmThresholdProfileTestRunner AtPmThresholdProfileTestRunnerNew(AtPmThresholdProfile profile)
    {
    AtPmThresholdProfileTestRunner new = AtOsalMemAlloc(ObjectSize());
    if (new == NULL)
        return NULL;

    return ObjectInit(new, profile);
    }

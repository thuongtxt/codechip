/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmThresholdProfileTestRunner.h
 * 
 * Created Date: Sep 15, 2016
 *
 * Description : TCA threshold profile test runner.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMTHRESHOLDPROFILETESTRUNNER_H_
#define _ATPMTHRESHOLDPROFILETESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/AtObjectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPmThresholdProfileTestRunner* AtPmThresholdProfileTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmThresholdProfileTestRunner AtPmThresholdProfileTestRunnerNew(AtPmThresholdProfile profile);

#ifdef __cplusplus
}
#endif
#endif /* _ATPMTHRESHOLDPROFILETESTRUNNER_H_ */


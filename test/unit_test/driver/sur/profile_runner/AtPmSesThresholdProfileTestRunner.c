/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtPmSesThresholdProfileTestRunner.c
 *
 * Created Date: Jun 25, 2018
 *
 * Description : PM SES threshold profile test runner.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../man/AtObjectTestRunnerInternal.h"
#include "../../../driver/src/generic/sur/AtPmSesThresholdProfile.h"
#include "AtPmSesThresholdProfileTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPmSesThresholdProfileTestRunner
    {
    tAtObjectTestRunner super;
    }tAtPmSesThresholdProfileTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtUnittestRunnerMethods     m_AtUnittestRunnerOverride;

static const tAtUnittestRunnerMethods   *m_AtUnittestRunnerMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmSesThresholdProfile CurrentProfile(void)
    {
    return (AtPmSesThresholdProfile) AtObjectTestRunnerObjectGet((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testChangeSdhLineSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfileSdhLineThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfileSdhLineThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfileSdhLineThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfileSdhLineThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeSdhSectionSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfileSdhSectionThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfileSdhSectionThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfileSdhSectionThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfileSdhSectionThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeSdhHoPathSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfileSdhHoPathThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfileSdhHoPathThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfileSdhHoPathThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfileSdhHoPathThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangeSdhLoPathSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfileSdhLoPathThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfileSdhLoPathThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfileSdhLoPathThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfileSdhLoPathThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePdhDe3LineSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfilePdhDe3LineThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfilePdhDe3LineThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfilePdhDe3LineThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfilePdhDe3LineThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePdhDe3PathSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfilePdhDe3PathThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfilePdhDe3PathThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfilePdhDe3PathThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfilePdhDe3PathThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePdhDe1LineSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfilePdhDe1LineThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfilePdhDe1LineThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfilePdhDe1LineThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfilePdhDe1LineThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePdhDe1PathSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfilePdhDe1PathThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfilePdhDe1PathThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfilePdhDe1PathThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfilePdhDe1PathThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testChangePwSesThresholdValue(void)
    {
    AtPmSesThresholdProfile self = (AtPmSesThresholdProfile)CurrentProfile();
    uint32 value = AtPmSesThresholdProfileIdGet(self) * 10;
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPmSesThresholdProfilePwThresholdSet(self, value));
    TEST_ASSERT_EQUAL_INT(value, AtPmSesThresholdProfilePwThresholdGet(self));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtPmSesThresholdProfilePwThresholdSet(NULL, 200));
    TEST_ASSERT_EQUAL_INT(0, AtPmSesThresholdProfilePwThresholdGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_AtPmSesThresholdProfile_Fixtures)
        {
        new_TestFixture("testChangeSdhLineSesThresholdValue", testChangeSdhLineSesThresholdValue),
        new_TestFixture("testChangeSdhSectionSesThresholdValue", testChangeSdhSectionSesThresholdValue),
        new_TestFixture("testChangeSdhHoPathSesThresholdValue", testChangeSdhHoPathSesThresholdValue),
        new_TestFixture("testChangeSdhLoPathSesThresholdValue", testChangeSdhLoPathSesThresholdValue),
        new_TestFixture("testChangePdhDe3LineSesThresholdValue", testChangePdhDe3LineSesThresholdValue),
        new_TestFixture("testChangePdhDe3PathSesThresholdValue", testChangePdhDe3PathSesThresholdValue),
        new_TestFixture("testChangePdhDe1LineSesThresholdValue", testChangePdhDe1LineSesThresholdValue),
        new_TestFixture("testChangePdhDe1PathSesThresholdValue", testChangePdhDe1PathSesThresholdValue),
        new_TestFixture("testChangePwSesThresholdValue", testChangePwSesThresholdValue),
        };

    EMB_UNIT_TESTCALLER(TestSuite_AtPmSesThresholdProfile_Caller, "AtPmSesThresholdProfile", NULL, NULL, TestSuite_AtPmSesThresholdProfile_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPmSesThresholdProfile_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtPmSesThresholdProfile profile = (AtPmSesThresholdProfile)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    AtSnprintf(buffer, bufferSize, "PmSesThresholdProfile %d", AtPmSesThresholdProfileIdGet(profile));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    AtUnittestRunner runner = self;
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy (&m_AtUnittestRunnerOverride, (void*)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }
    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtObjectTestRunner self)
    {
    OverrideAtUnittestRunner((void*)self);
    }

static uint32 ObjectSize(void)
    {
    return (sizeof(tAtPmSesThresholdProfileTestRunner));
    }

static AtPmSesThresholdProfileTestRunner ObjectInit(AtPmSesThresholdProfileTestRunner self, AtPmSesThresholdProfile profile)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)profile) == NULL)
        return NULL;

    Override((AtObjectTestRunner)self);
    m_methodsInit = 1;

    return self;
    }

AtPmSesThresholdProfileTestRunner AtPmSesThresholdProfileTestRunnerNew(AtPmSesThresholdProfile profile)
    {
    AtPmSesThresholdProfileTestRunner new = AtOsalMemAlloc(ObjectSize());
    if (new == NULL)
        return NULL;

    return ObjectInit(new, profile);
    }

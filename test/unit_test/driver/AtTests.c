/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtTests.c
 *
 * Created Date: Jul 28, 2012
 *
 * Description : Unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../apps/include/FpgaVersion.h"
#include "AtCliModule.h"
#include "AtTests.h"
#include "AtTextOutput.h"
#include "AtList.h"
#include "AtOsalLinuxDebug.h"
#include "man/device_runner/AtDeviceTestRunnerFactory.h"
#include "man/AtDriverTestRunner.h"
#include "std/AtStdTestRunner.h"
#include "HalFactory.h"
#include "AtIdParser.h"
#include "LightXMLJUnitOutputter.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtDriver  m_driver     = NULL;
static AtOsal    m_osal       = NULL;
static AtLogger  m_logger     = NULL;

/* Products need to be tested */
static uint32 m_testedProducts[] = {
                                    0x60000031,
                                    0x60001031,
                                    0x60030011,
                                    0x60030022,
                                    0x60030023,
                                    0x60030051,
                                    0x60030080,
                                    0x60030081,
                                    0x60030101,
                                    0x60030111,
                                    0x60031021,
                                    0x60031031,
                                    0x60031032,
                                    0x60031035,
                                    0x60031071,
                                    0x60031131,
                                    0x60035011,
                                    0x60035021,
                                    0x60070013,
                                    0x60070023,
                                    0x60070041,
                                    0x60070061,
                                    0x60071011,
                                    0x60071021,
                                    0x60071032,
                                    0x60091023,
                                    0x60091132,
                                    0x60091135,
                                    0x60150011,
                                    0x60200011,
                                    0x60210011,
                                    0x61150011,
                                    0x65031032,
                                    0x60290011,
                                    0x60290021
                                    /* Products that stop being developed:
                                     * - 0x600a0011
                                     * - 0x60031033
                                     * - 0x60060011
                                     * - 0x61031031
                                     */
                                   };
static uint8  m_numTestedProducts = 0;
static uint32 m_currentProduct;
static eBool  m_quickTest = cAtFalse;
static uint32 m_numRandomChannels = 0;

/*--------------------------- Forward declarations ---------------------------*/
extern TestRef TestSuite_HalSim(void);

extern int TestSuite_UtilRun(void);

extern void AtOsalTests();

/*--------------------------- Implementation ---------------------------------*/
static AtHal CreateHalSim(uint32 baseAddress)
    {
    return AtHalSimCreateForProductCode(AtTestProductCodeGet(), cAtHalSimDefaultMemorySizeInMbyte);
    }

static AtHal HalCreate(uint8 coreId)
    {
    if (AtTestIsSimulationTesting())
        return CreateHalSim(AppBaseAddressOfCore(coreId));

    return AppCreateHal(AppBaseAddressOfCore(coreId), AppPlatform());
    }

static AtHal HalForIpCore(AtDevice device, uint8 coreId)
    {
    return HalCreate(coreId);
    }

static void SetupDeviceVersion(AtDevice device)
    {
    /* Tester may want to test features of a specific FPGA version */
    if (AppFpgaVersionNumber())
        {
        AtDeviceVersionNumberSet(device, AppFpgaVersionNumber());
        AtDeviceBuiltNumberSet(device, AppFpgaBuiltNumber());
        }

    /* Or make all features available on testing */
    else
        {
        AtDeviceVersionNumberSet(device, cBit31_0);
        AtDeviceBuiltNumberSet(device, cBit31_0);
        }
    }

static AtDevice TestedDevice()
    {
    AtDriver driver = TestedDriver();
    uint8 numAddedDevices, i;

    /* Create device and install HAL for each IP core */
    AtDevice *addedDevices = AtDriverAddedDevicesGet(driver, &numAddedDevices);
    if (numAddedDevices == 0)
        {
        AtDevice device;
        uint32 productCode = AtTestProductCodeGet();
        if (AtTestIsSimulationTesting())
            productCode = AppSimulationProductCode(productCode);
        device = AtDriverDeviceCreate(driver, productCode);

        /* We need to set-up version priorly to HAL setting, to have correct
         * device set-up dealing with version number. */
        SetupDeviceVersion(device);

        for (i = 0; i < AtDeviceNumIpCoresGet(device); i++)
            AtDeviceIpCoreHalSet(device, i, HalForIpCore(device, i));

        AtDeviceInit(device);

        return device;
        }

    /* Device is already created */
    else
        return addedDevices[0];
    }

static void HalsDelete(AtList hals)
    {
    AtHal hal;
    while ((hal = (AtHal)AtListObjectRemoveAtIndex(hals, 0)))
        AtHalDelete(hal);
    }

static const char* LoggerFilename(void)
    {
    return "unittest_logger.log";
    }

static void DriverLoggerSetup(AtDriver driver)
    {
    static const uint32 cMaxMessageLength = 512;
    AtLogger logger = AtFileLoggerNew(LoggerFilename(), cMaxMessageLength);
    AtLoggerLevelEnable(logger, cAtLogLevelDebug, cAtTrue);
    AtDriverLoggerSet(driver, logger);
    AtLoggerFlush(logger);
    m_logger = logger;
    }

const uint32 *AtTestSupportedProducts(uint8 *numProducts)
    {
    *numProducts = mCount(m_testedProducts);
    return m_testedProducts;
    }

const uint32 *AtTestProductsToBeTested(uint8 *numProducts)
    {
    *numProducts = m_numTestedProducts;
    return m_testedProducts;
    }

/* To get product code of device will be tested */
uint32 AtTestProductCodeGet()
    {
    return m_currentProduct;
    }

/* To get OSAL used for testing */
AtOsal TestedOsal()
    {
    m_osal = AtOsalLinuxDebug();
    AtOsalSharedSet(m_osal);

    return m_osal;
    }

AtDriver TestedDriver()
    {
    if (m_driver == NULL)
        {
        m_driver = AtDriverCreate(mCount(m_testedProducts), TestedOsal());
        DriverLoggerSetup(m_driver);
        AtDriverApiLogEnable(cAtFalse);
        AtLoggerLevelEnable(AtDriverLoggerGet(m_driver), cAtLogLevelDebug, cAtTrue);
        }

    return m_driver;
    }

void Cleanup()
    {
    AtList hals;
    AtHal hal;
    uint8 device_i, numDevices;

    if (TestedDriver() == NULL)
        return;

    /* Get all of installed HALs */
    hals = AtListCreate(0);
    AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numDevices);
    for (device_i = 0; device_i < numDevices; device_i++)
        {
        AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), device_i);
        uint8 core_i;
        for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
            AtListObjectAdd(hals, (AtObject)AtDeviceIpCoreHalGet(device, core_i));
        }

    AtDriverDelete(AtDriverSharedDriverGet());

    /* Delete all installed HALs to free up resources */
    while ((hal = (AtHal)AtListObjectRemoveAtIndex(hals, 0)) != NULL)
        AtHalDelete(hal);
    AtObjectDelete((AtObject)hals);

    m_driver = NULL;
    if (m_logger)
        {
        AtObjectDelete((AtObject)m_logger);
        m_logger = NULL;
        }
    }

eBool AtTestIsSimulationTesting()
    {
    if (AppPlatform() == cPlatformUnknown)
        return cAtTrue;

    return AppIsSimulationPlatform(AppPlatform());
    }

static OutputterRef XmlOutputer(void)
    {
    OutputterRef outputer = LightXMLJUnitOutputter_outputter();
    XMLJUnitOutputter_setName("AtDriverUnittest");
    return outputer;
    }

static OutputterRef UnittestOutputerWithXml(eBool xml)
    {
    return xml ? XmlOutputer() : AtTextOutput();
    }

static AtDeviceTestRunnerFactory DeviceRunnerFactory(void)
    {
    extern AtDeviceTestRunnerFactory Af6DeviceTestRunnerFactorySharedFactory();

    /* TODO: when af4 is join, need to handle it properly */
    return Af6DeviceTestRunnerFactorySharedFactory();
    }

static void DeviceUnittestRun(AtDevice device)
    {
    AtDeviceTestRunner runner = AtDeviceTestRunnerFactoryDeviceTestRunnerCreate(DeviceRunnerFactory(), device);
    AtUnittestRunnerRun((AtUnittestRunner)runner);
    AtUnittestRunnerDelete((AtUnittestRunner)runner);
    }

static eBool NeedSimulateAllProducts()
    {
    return (AppPlatform() == cPlatformUnknown) ? cAtTrue : cAtFalse;
    }

static void SetupProductCodes()
    {
    /* Need a driver to validate product code */
    TEST_ASSERT_NOT_NULL(TestedDriver());

    if (NeedSimulateAllProducts())
        {
        m_numTestedProducts = mCount(m_testedProducts);
        return;
        }

    if (m_numTestedProducts == 0)
        {
        m_numTestedProducts = 1;
        m_testedProducts[0] = AppProductCodeGet(AppPlatform());
        return;
        }
    }

void AtTestSimulateProduct(uint32 productCode)
    {
    m_testedProducts[m_numTestedProducts++] = productCode;
    }

static AtList InstalledHals(AtDevice device)
    {
    AtList hals = AtListCreate(AtDeviceNumIpCoresGet(device));
    uint8 i;

    for (i = 0; i < AtDeviceNumIpCoresGet(device); i++)
        AtListObjectAdd(hals, (AtObject)AtDeviceIpCoreHalGet(device, i));

    return hals;
    }

static void TestDriver()
    {
    AtUnittestRunner runner = (AtUnittestRunner)AtDriverTestRunnerNew();
    AtUnittestRunnerRun(runner);
    AtUnittestRunnerDelete(runner);
    }

static void TestAllProducts()
    {
    uint8 code_i, numSubDevice;

    for (code_i = 0; code_i < m_numTestedProducts; code_i++)
        {
        AtList hals;

        m_currentProduct = m_testedProducts[code_i];

        DeviceUnittestRun(TestedDevice());
        numSubDevice = AtDeviceNumSubDeviceGet(TestedDevice());
        if (numSubDevice)
            {
            uint8 subDeviceIndex = numSubDevice / 2;
            AtDevice aSubDevice = AtDeviceSubDeviceGet(TestedDevice(), subDeviceIndex);
            /* Default in standby mode */
            TEST_ASSERT_EQUAL_INT(cAtDeviceRoleStandby, AtDeviceRoleGet(aSubDevice));
            DeviceUnittestRun(aSubDevice);
            /* Set active mode and test again */
            TEST_ASSERT_EQUAL_INT(cAtOk, AtDeviceRoleSet(aSubDevice, cAtDeviceRoleActive));
            TEST_ASSERT_EQUAL_INT(cAtDeviceRoleActive, AtDeviceRoleGet(aSubDevice));
            DeviceUnittestRun(aSubDevice);
            }

        /* Cleanup */
        hals = InstalledHals(TestedDevice());
        AtDriverDeviceDelete(TestedDriver(), TestedDevice());
        HalsDelete(hals);
        AtObjectDelete((AtObject)hals);
        }
    }

static void TestHalSim()
    {
    if (AtTestIsSimulationTesting())
        TextUIRunner_runTest(TestSuite_HalSim());
    }

static uint32 RangeRandom(uint32 max)
    {
    uint32
        numBins = max,
        numRand = RAND_MAX + 1UL,
        binSize = numRand / numBins,
        defect  = numRand % numBins;
    uint32 value;

    do
        {
        value = AtStdRand(AtStdSharedStdGet());
        }
    while (numRand - defect <= value);

    return value/binSize;
    }

static void testNoLogMessagesAfterTesting()
    {
    AtLogger logger = AtSharedDriverLoggerGet();
    uint32 numProblemMessages;
    tAtLoggerCounters counters;

    /* Exclude debug messages */
    AtLoggerCountersGet(logger, &counters);

    numProblemMessages = counters.levelCritical + counters.levelWarning + counters.levelOthers;

    if (numProblemMessages || counters.levelDebug)
        AtCliExecute("show logger");
    TEST_ASSERT_EQUAL_INT(0, numProblemMessages);
    }

static TestRef LoggerCheckSuite(void)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_LoggerCheck_Fixtures)
        {
        new_TestFixture("testNoLogMessagesAfterTesting", testNoLogMessagesAfterTesting)
        };

    EMB_UNIT_TESTCALLER(TestSuite_LoggerCheck_Caller, "TestSuite_LoggerCheck", NULL, NULL, TestSuite_LoggerCheck_Fixtures);
    return (TestRef)&TestSuite_LoggerCheck_Caller;
    }

static void TestStd()
    {
    AtUnittestRunner newRunner = AtStdTestRunnerNew(AtStdDefaultSharedStd());
    AtUnittestRunnerRun(newRunner);
    AtUnittestRunnerDelete(newRunner);
    }

int DriverUnittestRun()
    {
    extern int AtCliExecute(const char *cmdString);

    SetupProductCodes();
    TestStd();
    TestHalSim();
    TestDriver();
    TestAllProducts();
    
    /* To catch all of warnings */
    TextUIRunner_runTest(LoggerCheckSuite());

    Cleanup();

    return 0;
    }

int AllUnittestRunWithXmlOutputer(eBool xml)
    {
    int ret = 0;

    AtStdRandSeed(AtStdSharedStdGet());

    TextUIRunner_setOutputter(UnittestOutputerWithXml(xml));
    AtTextOutputSilenceOnSuccess(cAtTrue);
    TextUIRunner_start();

    ret |= DriverUnittestRun();
    ret |= TestSuite_UtilRun();

    AtIdParserTest();
    AtCliStop();
    AtOsalTests();

    TextUIRunner_end();

    return ret;
    }

void AtTestStartTextUI(void)
    {
    static AtTextUI textUI = NULL;
    if (textUI == NULL)
        textUI = AtDefaultTinyTextUINew();
    AtCliStartWithTextUI(textUI);
    }

void AtTestCaseDidFail(void)
    {
    static eBool showTextUIWhenFailure = cAtFalse;
    if (!showTextUIWhenFailure)
        return;
    AtTestStartTextUI();
    }

void AtTestQuickTestEnable(eBool quickTest)
    {
    m_quickTest = quickTest;
    }

eBool AtTestQuickTestIsEnabled(void)
    {
    return m_quickTest;
    }

uint32 AtTestRandom(uint32 max, uint32 currentValue)
    {
    uint32 value = RangeRandom(max);

    if (max <= 1)
        return value;

    while (cAtTrue)
        {
        value = RangeRandom(max);

        if (value != currentValue)
            return value;
        }

    return value;
    }

uint32 AtTestRandomNumChannels(uint32 numChannels)
    {
    uint32 value;
    uint32 numRandomChannels = AtTestNumRandomChannelsGet();
    uint32 result;

    if (numChannels == 0)
        return 0;

    value = AtTestRandom(numChannels + 1, cInvalidUint32);
    result = mMax(1, value);
    if (numRandomChannels == 0)
        return result;

    return mMin(numRandomChannels, result);
    }

eBool AtTestLoggerIsEnabled(void)
    {
    return AtLoggerIsEnabled(AtSharedDriverLoggerGet());
    }

void AtTestLoggerEnable(eBool enable)
    {
    AtLoggerEnable(AtSharedDriverLoggerGet(), enable);
    }

void AtTestNumRandomChannelsSet(uint32 numRandomChannels)
    {
    m_numRandomChannels = numRandomChannels;
    }

uint32 AtTestNumRandomChannelsGet(void)
    {
    return m_numRandomChannels;
    }

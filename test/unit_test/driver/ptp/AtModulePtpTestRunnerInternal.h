/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtModulePtpTestRunnerInternal.h
 * 
 * Created Date: May 6, 2019
 *
 * Description : Module PTP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPTPTESTRUNNERINTERNAL_H_
#define _ATMODULEPTPTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "AtModulePtpTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePtpTestRunnerMethods
    {
    AtList (*MakePtpPortForGroupTest)(AtModulePtpTestRunner self, AtList ptpPorts);
    }tAtModulePtpTestRunnerMethods;

typedef struct tAtModulePtpTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModulePtpTestRunnerMethods *methods;

    AtList ptpPorts;
    }tAtModulePtpTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleTestRunner AtModulePtpTestRunnerObjectInit(AtModuleTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPTPTESTRUNNERINTERNAL_H_ */


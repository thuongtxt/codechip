/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPsnGroupTestRunner.h
 * 
 * Created Date: May 6, 2019
 *
 * Description : PTP PSN Group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPSNGROUPTESTRUNNER_H_
#define _ATPTPPSNGROUPTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPtpPsnGroupTestRunner *AtPtpPsnGroupTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpPsnGroupTestRunner AtPtpPsnGroupTestRunnerNew(AtPtpPsnGroup group, AtModulePtpTestRunner moduleRunner, AtList ptpPorts);

AtPtpPsnGroup AtPtpPsnGroupTestRunnerGroupGet(AtPtpPsnGroupTestRunner self);
AtList AtPtpPsnGroupTestRunnerPtpPortsGet(AtPtpPsnGroupTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPSNGROUPTESTRUNNER_H_ */


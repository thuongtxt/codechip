/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPsnTestRunner.h
 * 
 * Created Date: May 6, 2019
 *
 * Description : PTP PSN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPSNTESTRUNNER_H_
#define _ATPTPPSNTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner AtPtpPsnTestRunnerNew(AtPtpPsn psnPort);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPSNTESTRUNNER_H_ */


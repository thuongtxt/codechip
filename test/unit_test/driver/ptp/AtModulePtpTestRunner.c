/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtModulePtpTestRunner.c
 *
 * Created Date: May 2, 2019
 *
 * Description : PTP unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtModulePtp.h"
#include "../../../../driver/src/generic/ptp/AtModulePtpInternal.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModulePtpTestRunnerInternal.h"
#include "AtPtpPsnGroupTestRunner.h"
#include "AtPtpPortTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModulePtpTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModulePtpTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePtp Module()
    {
    return (AtModulePtp)AtModuleTestRunnerModuleGet((AtModuleTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void setUp()
    {
    AtModuleInit((AtModule)Module());
    }

static eBool CanTestPtpPort(AtModulePtpTestRunner self)
    {
    return AtModulePtpNumPortsGet((AtModulePtp)AtModuleTestRunnerModuleGet((AtModuleTestRunner)self)) > 0;
    }

static void testChangeDeviceType()
    {
    uint8 values[] = {cAtPtpDeviceTypeUnknown, cAtPtpDeviceTypeOrdinary,
                      cAtPtpDeviceTypeBoundary, cAtPtpDeviceTypeTransparent};
    uint8 i;
    AtModulePtp module = Module();

    for (i = 0; i < mCount(values); i++)
        {
        if (AtModulePtpDeviceTypeIsSupported(module, values[i]) == cAtFalse)
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtModulePtpDeviceTypeSet(module, values[i]));
            AtTestLoggerEnable(cAtTrue);
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceTypeSet(module, values[i]));
        TEST_ASSERT_EQUAL_INT(values[i], AtModulePtpDeviceTypeGet(module));
        }
    }

static void testChangeCorrectionMode()
    {
    uint8 values[] = {cAtPtpCorrectionModeUnknown,
                      cAtPtpCorrectionModeStandard,
                      cAtPtpCorrectionModeAssist};
    uint8 i;
    AtModulePtp module = Module();

    for (i = 0; i < mCount(values); i++)
        {
        if (AtModulePtpCorrectionModeIsSupported(module, values[i]) == cAtFalse)
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtModulePtpCorrectionModeSet(module, values[i]));
            AtTestLoggerEnable(cAtTrue);
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpCorrectionModeSet(module, values[i]));
        TEST_ASSERT_EQUAL_INT(values[i], AtModulePtpCorrectionModeGet(module));
        }
    }

static void testCanBeSetRouteTripDelay()
    {
    AtModulePtp module = Module();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpPpsRouteTripDelaySet(module, 100));
    TEST_ASSERT_EQUAL_INT(100, AtModulePtpPpsRouteTripDelayGet(module));
    }

static void testChangePpsSource()
    {
    uint8 values[] = {cAtPtpPpsSourceUnknown,
                      cAtPtpPpsSourceInternal,
                      cAtPtpPpsSourceExternal};
    uint8 i;
    AtModulePtp module = Module();

    for (i = 0; i < mCount(values); i++)
        {
        if (AtModulePtpPpsSourceIsSupported(module, values[i]) == cAtFalse)
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtModulePtpPpsSourceSet(module, values[i]));
            AtTestLoggerEnable(cAtTrue);
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpPpsSourceSet(module, values[i]));
        TEST_ASSERT_EQUAL_INT(values[i], AtModulePtpPpsSourceGet(module));
        }
    }

static void testChangeDeviceMAC()
    {
    AtModulePtp module = Module();
    uint8 mac[] = {0x33, 0x91, 0x23, 0xA5, 0xD2, 0xC1};
    uint8 actualMac[6];

    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceMacAddressSet(module, mac));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceMacAddressGet(module, actualMac));
    TEST_ASSERT(memcmp(actualMac, mac, 6) == 0);
    }

static void testChangeEnableDeviceMAC()
    {
    AtModulePtp module = Module();

    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceMacAddressEnable(module, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtModulePtpDeviceMacAddressIsEnabled(module));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceMacAddressEnable(module, cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtModulePtpDeviceMacAddressIsEnabled(module));
    }

static void testCanChangeT1T3CaptureMode()
    {
    AtModulePtp module = Module();
    uint8 t1t3Mode[] = {cAtPtpT1T3TimestampCaptureModeNone,
                        cAtPtpT1T3TimestampCaptureModeCpu,
                        cAtPtpT1T3TimestampCaptureModePacket};
    uint8 i;

    for (i = 0; i < mCount(t1t3Mode); i++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpT1T3CaptureModeSet(module, t1t3Mode[i]));
        TEST_ASSERT_EQUAL_INT(t1t3Mode[i], AtModulePtpT1T3CaptureModeGet(module));
        }
    }

static eBool VlanIdAreSame(const tAtEthVlanTag *vlan1, const tAtEthVlanTag *vlan2)
    {
    return (vlan1->vlanId == vlan2->vlanId);
    }

static void testCanChangeExpectedVlan()
    {
    AtModulePtp module = Module();
    tAtEthVlanTag expectedVlan;
    tAtEthVlanTag expectedVlan_actual;
    uint8 numVlan, vlanIndex;

    numVlan = AtModulePtpNumExpectedVlan(module);
    TEST_ASSERT(numVlan != 0)
    for (vlanIndex = 0; vlanIndex < numVlan; vlanIndex++)
        {
        AtEthVlanTagConstruct(1, 1, 1, &expectedVlan);
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpExpectedVlanSet(module, vlanIndex, &expectedVlan));
        AtModulePtpExpectedVlanGet(module, vlanIndex, &expectedVlan_actual);
        TEST_ASSERT(VlanIdAreSame(&expectedVlan, &expectedVlan_actual));

        AtEthVlanTagConstruct(1, 1, 4095, &expectedVlan);
        TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpExpectedVlanSet(module, vlanIndex, &expectedVlan));
        AtModulePtpExpectedVlanGet(module, vlanIndex, &expectedVlan_actual);
        TEST_ASSERT(VlanIdAreSame(&expectedVlan, &expectedVlan_actual));
        }
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_ModulePtp)
        {
        new_TestFixture("testChangeDeviceType", testChangeDeviceType),
        new_TestFixture("testChangeCorrectionMode", testChangeCorrectionMode),
        new_TestFixture("testCanBeSetRouteTripDelay", testCanBeSetRouteTripDelay),
        new_TestFixture("testChangePpsSource", testChangePpsSource),
        new_TestFixture("testChangeDeviceMAC", testChangeDeviceMAC),
        new_TestFixture("testChangeEnableDeviceMAC", testChangeEnableDeviceMAC),
        new_TestFixture("testCanChangeT1T3CaptureMode", testCanChangeT1T3CaptureMode),
        new_TestFixture("testCanChangeExpectedVlan", testCanChangeExpectedVlan),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_ModulePtp_Caller, "TestSuite_ModulePtp", setUp, NULL, TestSuite_ModulePtp);

    return (TestRef)((void *)&TestSuite_ModulePtp_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    if (CanTestPtpPort((AtModulePtpTestRunner)self))
        AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));

    return suites;
    }

static AtChannelTestRunner CreatePtpPortTestRunner(AtModulePtpTestRunner self, AtChannel ptpPort)
    {
    return AtPtpPortTestRunnerNew(ptpPort, (AtModuleTestRunner)self);
    }

static void TestPtpPort(AtModulePtpTestRunner self)
    {
    uint32 i, numPort;
    AtModulePtp module = Module();

    numPort = AtModulePtpNumPortsGet(module);
    for (i = 0; i < numPort; i++)
        {
        AtChannel ptpPort = (AtChannel)AtModulePtpPortGet(module, i);
        AtUnittestRunner runner = (AtUnittestRunner)CreatePtpPortTestRunner(self, ptpPort);

        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        }
    }

static AtList MakePtpPortForGroupTest(AtModulePtpTestRunner self, AtList ptpPorts)
    {
    return NULL;
    }

static AtList PtpPortForGroupTest(AtModulePtpTestRunner self)
    {
    if (self->ptpPorts)
        return self->ptpPorts;

    self->ptpPorts = AtListCreate(0);
    mMethodsGet(self)->MakePtpPortForGroupTest(self, self->ptpPorts);

    return self->ptpPorts;
    }

static void PtpPsnGrouptest(AtModulePtpTestRunner self,
                            AtPtpPsnGroup (*PsnGroupGet)(AtModulePtp, uint32),
                            uint32 (*NumPsnGroupsGet)(AtModulePtp))
    {
    AtModulePtp ptpModule = Module();
    uint32 numGroups = NumPsnGroupsGet(ptpModule);
    uint32 group_i;

    if (numGroups == 0)
        return;

    AtAssert(numGroups);
    for (group_i = 0; group_i < numGroups; group_i++)
        {
        uint32 groupId = group_i;
        AtPtpPsnGroup group;
        AtPtpPsnGroupTestRunner runner;
        AtList ptpPorts;

        if (AtTestQuickTestIsEnabled())
            groupId = AtTestRandom(numGroups, groupId);

        group = PsnGroupGet(ptpModule, groupId);
        AtAssert(group);
        ptpPorts = PtpPortForGroupTest(self);
        AtAssert(AtListLengthGet(ptpPorts));
        runner = AtPtpPsnGroupTestRunnerNew(group, self, ptpPorts);
        AtUnittestRunnerRun((AtUnittestRunner)runner);
        AtUnittestRunnerDelete((AtUnittestRunner)runner);
        }
    }

static void PtpL2PsnGrouptest(AtModulePtpTestRunner self)
    {
    PtpPsnGrouptest(self,
                    AtModulePtpL2PsnGroupGet,
                    AtModulePtpNumL2PsnGroupsGet);
    }

static void PtpIpV4PsnGrouptest(AtModulePtpTestRunner self)
    {
    PtpPsnGrouptest(self,
                    AtModulePtpIpV4PsnGroupGet,
                    AtModulePtpNumIpV4PsnGroupsGet);
    }

static void PtpIpV6PsnGrouptest(AtModulePtpTestRunner self)
    {
    PtpPsnGrouptest(self,
                    AtModulePtpIpV6PsnGroupGet,
                    AtModulePtpNumIpV6PsnGroupsGet);
    }

static void GroupTest(AtModulePtpTestRunner self)
    {
    PtpIpV6PsnGrouptest(self);
    PtpIpV4PsnGrouptest(self);
    PtpL2PsnGrouptest(self);
    }

static void Run(AtUnittestRunner self)
    {
    AtModulePtpTestRunner runner = (AtModulePtpTestRunner)self;

    m_AtUnittestRunnerMethods->Run(self);

    TestPtpPort(runner);
    GroupTest(runner);
    }

static void Delete(AtUnittestRunner self)
    {
    AtObjectDelete((AtObject)mThis(self)->ptpPorts);
    m_AtUnittestRunnerMethods->Delete(self);
    }

static AtList AddPtpPortsFromString(AtList ptpPorts, char *portString)
    {
    uint32 bufferSize;
    uint32 numPorts, portId;
    AtChannel *channelList;

    channelList = CliSharedChannelListGet(&bufferSize);
    numPorts = CliAtPtpPortListFromString(portString, channelList, bufferSize);
    for (portId = 0; portId < numPorts; portId++)
        AtListObjectAdd(ptpPorts, (AtObject)channelList[portId]);
    return ptpPorts;
    }

static void OverrideAtUnittestRunner(AtModuleTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static void MethodsInit(AtModulePtpTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, MakePtpPortForGroupTest);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePtpTestRunner);
    }

AtModuleTestRunner AtModulePtpTestRunnerObjectInit(AtModuleTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleTestRunner AtModulePtpTestRunnerNew(AtModule module)
    {
    AtModuleTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModulePtpTestRunnerObjectInit(newRunner, module);
    }

AtList AtModulePtpTestRunnerAddPtpPortsFromString(AtList ptpPorts, char *portString)
    {
    if (ptpPorts != NULL)
        return AddPtpPortsFromString(ptpPorts, portString);

    return NULL;
    }

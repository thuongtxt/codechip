/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPortTestRunner.h
 * 
 * Created Date: May 6, 2019
 *
 * Description : PTP Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPORTTESTRUNNER_H_
#define _ATPTPPORTTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelTestRunner AtPtpPortTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPORTTESTRUNNER_H_ */


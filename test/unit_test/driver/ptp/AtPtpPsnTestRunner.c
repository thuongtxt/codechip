/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpPsnTestRunner.c
 *
 * Created Date: May 3, 2019
 *
 * Description : PSN PTP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtPtpPsn.h"
#include "../runner/AtUnittestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtPtpPsnTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPtpPsnTestRunner *AtPtpPsnTestRunner;
typedef struct tAtPtpPsnTestRunner
    {
    tAtObjectTestRunner super;
    }tAtPtpPsnTestRunner;

typedef eAtModulePtpRet (*ExpectedAddressSet)(AtPtpPsn, uint32, const uint8 *);
typedef eAtModulePtpRet (*ExpectedAddressGet)(AtPtpPsn, uint32, uint8 *);
typedef eAtModulePtpRet (*ExpectedNumAddressEntry)(AtPtpPsn);
typedef eAtModulePtpRet (*ExpectedAddressEnable)(AtPtpPsn, uint32, eBool);
typedef eBool (*ExpectedAddressIsEnabled)(AtPtpPsn, uint32);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPtpPsnTestRunner Runner(void)
    {
    return (AtPtpPsnTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPtpPsn PsnPtpPort(void)
    {
    return (AtPtpPsn)AtObjectTestRunnerObjectGet((AtObjectTestRunner)Runner());
    }

static eBool IsBackPlanPort(AtPtpPsn psnPort)
    {
    return (AtChannelIdGet((AtChannel)AtPtpPsnPortGet(psnPort)) == 0) ? cAtTrue : cAtFalse;
    }

static uint8 AddressLenGet(AtPtpPsn psnPort)
    {
    uint8 psnType = AtPtpPsnTypeGet(psnPort);

    if (psnType == cAtPtpPsnTypeLayer2) return 6;
    if (psnType == cAtPtpPsnTypeIpV4)   return 4;
    if (psnType == cAtPtpPsnTypeIpV6)   return 16;
    return 0;
    }

static uint8 *AddressGet(AtPtpPsn psnPort, uint8 *mac, uint8 value)
    {
    uint8 byte_i, lenAddr;

    lenAddr = AddressLenGet(psnPort);
    for (byte_i = 0; byte_i < lenAddr; byte_i++)
        mac[byte_i] = value + byte_i;

    return mac;
    }

static void TestChangeMcastAddress(eAtModulePtpRet (*setAddressFunc)(AtPtpPsn, const uint8 *),
                                   eAtModulePtpRet (*getAddressFunc)(AtPtpPsn, uint8 *))
    {
    static uint8 mcastMacForPtp[] = {0x01, 0x1B, 0x19, 0x00, 0x00, 0x00};
    static uint8 mcastIpV4ForPtpV2[] = {224, 0, 1, 129};
    static uint8 mcastIpV6ForPtpV2[] = {0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x81};
    uint8 addrLen;
    uint8 *addr;
    uint8 actualAddr[16];
    AtPtpPsn psnPort = PsnPtpPort();
    uint8 psnType = AtPtpPsnTypeGet(psnPort);

    /* Build address */
    switch (psnType)
        {
        case cAtPtpPsnTypeLayer2:
            addrLen = 6;
            addr = mcastMacForPtp;
            break;

        case cAtPtpPsnTypeIpV4:
            addrLen = 4;
            addr = mcastIpV4ForPtpV2;
            break;

        case cAtPtpPsnTypeIpV6:
            addrLen = 16;
            addr = mcastIpV6ForPtpV2;
            break;

        default:
            return;
        }

    TEST_ASSERT_EQUAL_INT(cAtOk, setAddressFunc(psnPort, addr));
    TEST_ASSERT_EQUAL_INT(cAtOk, getAddressFunc(psnPort, actualAddr));
    TEST_ASSERT(AtOsalMemCmp(addr, actualAddr, addrLen) == 0);

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(setAddressFunc(psnPort, NULL) != cAtOk);
    TEST_ASSERT(getAddressFunc(psnPort, NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void TestChangeUnicastAddress(eAtModulePtpRet (*setAddressFunc)(AtPtpPsn, const uint8 *),
                                     eAtModulePtpRet (*getAddressFunc)(AtPtpPsn, uint8 *))
    {
    uint8 lenAddr;
    uint8 *addr;
    uint8 *actualAddr;
    AtPtpPsn psnPort = PsnPtpPort();

    /* Build address */
    lenAddr = AddressLenGet(psnPort);
    actualAddr = malloc(lenAddr);
    addr = malloc(lenAddr);
    addr = AddressGet(psnPort, addr, 0x1);

    TEST_ASSERT_EQUAL_INT(cAtOk, setAddressFunc(psnPort, addr));
    TEST_ASSERT_EQUAL_INT(cAtOk, getAddressFunc(psnPort, actualAddr));
    TEST_ASSERT(AtOsalMemCmp(addr, actualAddr, sizeof(addr)) == 0);

    addr = AddressGet(psnPort, addr, 0xFA);
    TEST_ASSERT_EQUAL_INT(cAtOk, setAddressFunc(psnPort, addr));
    TEST_ASSERT_EQUAL_INT(cAtOk, getAddressFunc(psnPort, actualAddr));
    TEST_ASSERT(AtOsalMemCmp(addr, actualAddr, sizeof(addr)) == 0);

    free(addr);
    free(actualAddr);

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(setAddressFunc(psnPort, NULL) != cAtOk);
    TEST_ASSERT(getAddressFunc(psnPort, NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void TestEnableAddresssHelper(eAtModulePtpRet (*MacEnable)(AtPtpPsn, eBool),
                                     eBool (*MacIsEnabled)(AtPtpPsn),
                                     eBool enabled)
    {
    AtPtpPsn psnPort = PsnPtpPort();
    TEST_ASSERT_EQUAL_INT(cAtOk, MacEnable(psnPort, enabled));
    TEST_ASSERT_EQUAL_INT(enabled, MacIsEnabled(psnPort));
    }

static void TestEnableAddresss(eAtModulePtpRet (*MacEnable)(AtPtpPsn, eBool),
                               eBool (*MacIsEnabled)(AtPtpPsn),
                               eBool shouldTestDisable,
                               eBool shouldTestEnable)
    {
    if (shouldTestEnable)
        TestEnableAddresssHelper(MacEnable, MacIsEnabled, cAtTrue);

    if (shouldTestDisable)
        TestEnableAddresssHelper(MacEnable, MacIsEnabled, cAtFalse);
    }

static eBool ShouldTestChangedUnicastMac(void)
    {
    eAtPtpPsnType psnType = AtPtpPsnTypeGet(PsnPtpPort());

    if ((psnType == cAtPtpPsnTypeIpV4) || (psnType == cAtPtpPsnTypeIpV6))
        return cAtFalse;
    return cAtTrue;
    }

static void testChangeUnicastDestMacAddress(void)
    {
    if (!ShouldTestChangedUnicastMac())
        return;
    TestChangeUnicastAddress(AtPtpPsnExpectedUnicastDestAddressSet, AtPtpPsnExpectedUnicastDestAddressGet);
    }

static void testEnableUnicastDestMacAddress(void)
    {
    AtPtpPsn psnPort = PsnPtpPort();
    eBool shouldTestDisable = cAtTrue;
    eBool shouldTestEnable = cAtTrue;

    if (IsBackPlanPort(psnPort))
        shouldTestDisable = cAtFalse;

    if (!ShouldTestChangedUnicastMac())
        return;

    TestEnableAddresss(AtPtpPsnExpectedUnicastDestAddressEnable,
                       AtPtpPsnExpectedUnicastDestAddressIsEnabled, shouldTestDisable, shouldTestEnable);
    }

static void testChangeMulticastDestAddress(void)
    {
    if (IsBackPlanPort(PsnPtpPort()))
        return;

    TestChangeMcastAddress(AtPtpPsnExpectedMcastDestAddressSet, AtPtpPsnExpectedMcastDestAddressGet);
    }

static void testEnableMulticastDestAddress(void)
    {
    AtPtpPsn psnPort = PsnPtpPort();
    eBool shouldTestDisable = cAtTrue;
    eBool shouldTestEnable = cAtTrue;

    if (IsBackPlanPort(psnPort))
        shouldTestEnable = cAtFalse;

    TestEnableAddresss(AtPtpPsnExpectedMcastDestAddressEnable,
                       AtPtpPsnExpectedMcastDestAddressIsEnabled,
                       shouldTestDisable, shouldTestEnable);
    }

static eBool ShouldTestUnicastSrcAddress(void)
    {
    return cAtFalse;
    }

static void testEnableAnycastSourceAddress(void)
    {
    eBool shouldTestEnable = ShouldTestUnicastSrcAddress();
    TestEnableAddresss(AtPtpPsnExpectedAnycastSourceAddressEnable,
                       AtPtpPsnExpectedAnycastSourceAddressIsEnabled,
                       shouldTestEnable, shouldTestEnable);
    }

static void testChangeUnicastSourceMacAddress(void)
    {
    if (!ShouldTestUnicastSrcAddress())
        return;

    if (!ShouldTestChangedUnicastMac())
        return;

    TestChangeUnicastAddress(AtPtpPsnExpectedUnicastSourceAddressSet, AtPtpPsnExpectedUnicastSourceAddressGet);
    }

static void testEnableUnicastSourceMacAddress(void)
    {
    eBool shouldTestDisable = cAtFalse;
    eBool shouldTestEnable = cAtFalse;

    if (!ShouldTestUnicastSrcAddress())
        return;

    if (!ShouldTestChangedUnicastMac())
        return;

    TestEnableAddresss(AtPtpPsnExpectedUnicastSourceAddressEnable,
                       AtPtpPsnExpectedUnicastDestAddressIsEnabled,
                       shouldTestDisable, shouldTestEnable);
    }

static void testEnableAnycastDestAddress(void)
    {
    eBool shouldTestEnable = cAtTrue;
    TestEnableAddresss(AtPtpPsnExpectedAnycastDestAddressEnable,
                       AtPtpPsnExpectedAnycastDestAddressIsEnabled,
                       shouldTestEnable, shouldTestEnable);
    }

static eBool IsIpType(AtPtpPsn psn)
    {
    if ((AtPtpPsnTypeGet(psn) == cAtPtpPsnTypeIpV4) || (AtPtpPsnTypeGet(psn) == cAtPtpPsnTypeIpV6))
        return cAtTrue;
    return cAtFalse;
    }

static void TestChangeExpectedAddressEntry(ExpectedAddressSet setAddressFunc,
                                           ExpectedAddressGet getAddressFunc,
                                           ExpectedNumAddressEntry getNumAddressFunc)
    {
    uint8 lenAddr;
    uint8 *mac;
    uint8 *actualMac;
    AtPtpPsn psnPort = PsnPtpPort();
    uint32 numEntry, entryId;

    /* Build address */
    lenAddr = AddressLenGet(psnPort);
    actualMac = malloc(lenAddr);
    mac = malloc(lenAddr);

    numEntry = getNumAddressFunc(psnPort);
    TEST_ASSERT(numEntry > 0);

    for (entryId = 0; entryId < numEntry; entryId++)
        {
        mac = AddressGet(psnPort, mac, 0x1);
        TEST_ASSERT_EQUAL_INT(cAtOk, setAddressFunc(psnPort, entryId, mac));
        TEST_ASSERT_EQUAL_INT(cAtOk, getAddressFunc(psnPort, entryId, actualMac));
        TEST_ASSERT(AtOsalMemCmp(mac, actualMac, sizeof(mac)) == 0);

        mac = AddressGet(psnPort, mac, 0xFA);
        TEST_ASSERT_EQUAL_INT(cAtOk, setAddressFunc(psnPort, entryId, mac));
        TEST_ASSERT_EQUAL_INT(cAtOk, getAddressFunc(psnPort, entryId, actualMac));
        TEST_ASSERT(AtOsalMemCmp(mac, actualMac, sizeof(mac)) == 0);

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(setAddressFunc(psnPort, entryId, NULL) != cAtOk);
        TEST_ASSERT(getAddressFunc(psnPort, entryId, NULL) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }

    free(mac);
    free(actualMac);
    }

static void TestEnableAddressWithEntryHelper(ExpectedAddressEnable enableFunc,
                                             ExpectedAddressIsEnabled isEnableFunc,
                                             ExpectedNumAddressEntry numUnicastSrcFunc,
                                             eBool enabled)
    {
    AtPtpPsn psnPort = PsnPtpPort();
    uint32 numEntry, entryId;

    numEntry = numUnicastSrcFunc(psnPort);
    TEST_ASSERT(numEntry > 0);

    for (entryId = 0; entryId < numEntry; entryId++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, enableFunc(psnPort, entryId, enabled));
        TEST_ASSERT_EQUAL_INT(enabled, isEnableFunc(psnPort, entryId));
        }
    }

static void TestEnableAddressWithEntry(ExpectedAddressEnable enableFunc,
                                       ExpectedAddressIsEnabled isEnableFunc,
                                       ExpectedNumAddressEntry numUnicastSrcFunc,
                                       eBool shouldTestDisable,
                                       eBool shouldTestEnable)
    {
    if (shouldTestDisable)
        TestEnableAddressWithEntryHelper(enableFunc, isEnableFunc, numUnicastSrcFunc, cAtFalse);

    if (shouldTestEnable)
        TestEnableAddressWithEntryHelper(enableFunc, isEnableFunc, numUnicastSrcFunc, cAtTrue);
    }

static void testChangeExpectedUnicastDestIpAddress(void)
    {
    if (IsIpType(PsnPtpPort()) == cAtFalse)
         return;

    TestChangeExpectedAddressEntry((ExpectedAddressSet)AtPtpIpPsnExpectedUnicastDestAddressEntrySet,
                                   (ExpectedAddressGet)AtPtpIpPsnExpectedUnicastDestAddressEntryGet,
                                   (ExpectedNumAddressEntry)AtPtpIpPsnMaxNumUnicastDestAddressEntryGet);
    }

static void testEnableExpectedUnicastDestIpAddress(void)
    {
    AtPtpPsn psnPort = PsnPtpPort();
    eBool shouldTestDisable = cAtTrue;
    eBool shouldTestEnable = cAtTrue;

    if (IsBackPlanPort(psnPort))
        shouldTestDisable = cAtFalse;

    if (IsIpType(psnPort) == cAtFalse)
         return;

    TestEnableAddressWithEntry((ExpectedAddressEnable)AtPtpIpPsnExpectedUnicastDestAddressEntryEnable,
                               (ExpectedAddressIsEnabled)AtPtpIpPsnExpectedUnicastDestAddressEntryIsEnabled,
                               (ExpectedNumAddressEntry)AtPtpIpPsnMaxNumUnicastDestAddressEntryGet,
                               shouldTestDisable,
                               shouldTestEnable);
    }

static void testChangeExpectedUnicastSourceIpAddress(void)
    {
    if (!ShouldTestUnicastSrcAddress())
        return;

    if (IsIpType(PsnPtpPort()) == cAtFalse)
         return;

    TestChangeExpectedAddressEntry((ExpectedAddressSet)AtPtpIpPsnExpectedUnicastSourceAddressEntrySet,
                                   (ExpectedAddressGet)AtPtpIpPsnExpectedUnicastSourceAddressEntryGet,
                                   (ExpectedNumAddressEntry)AtPtpIpPsnMaxNumUnicastSourceAddressEntryGet);
    }

static void testEnableExpectedUnicastSourceIpAddress(void)
    {
    AtPtpPsn psnPort = PsnPtpPort();
    eBool shouldTestDisable = cAtTrue;
    eBool shouldTestEnable = cAtTrue;

    if (!ShouldTestUnicastSrcAddress())
        return;

    if (IsBackPlanPort(psnPort))
        shouldTestDisable = cAtFalse;

    if (IsIpType(psnPort) == cAtFalse)
         return;

    TestEnableAddressWithEntry((ExpectedAddressEnable)AtPtpIpPsnExpectedUnicastSourceAddressEntryEnable,
                               (ExpectedAddressIsEnabled)AtPtpIpPsnExpectedUnicastSourceAddressEntryIsEnabled,
                               (ExpectedNumAddressEntry)AtPtpIpPsnMaxNumUnicastSourceAddressEntryGet,
                               shouldTestDisable,
                               shouldTestEnable);
    }

static eBool VlanIdAreSame(const tAtEthVlanTag *vlan1, const tAtEthVlanTag *vlan2)
    {
    return (vlan1->vlanId == vlan2->vlanId);
    }

static void testCanChangeExpectedVlan()
    {
    tAtEthVlanTag expectedVlan;
    tAtEthVlanTag expectedVlan_actual;
    AtPtpPsn psnPort = PsnPtpPort();

    if (IsBackPlanPort(psnPort))
        return;

    if (IsIpType(psnPort))
         return;

    AtEthVlanTagConstruct(1, 1, 1, &expectedVlan);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPsnExpectedCVlanSet(psnPort, &expectedVlan));
    AtPtpPsnExpectedCVlanGet(psnPort, &expectedVlan_actual);
    TEST_ASSERT(VlanIdAreSame(&expectedVlan, &expectedVlan_actual));

    AtEthVlanTagConstruct(1, 1, 2, &expectedVlan);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPsnExpectedSVlanSet(psnPort, &expectedVlan));
    AtPtpPsnExpectedSVlanGet(psnPort, &expectedVlan_actual);
    TEST_ASSERT(VlanIdAreSame(&expectedVlan, &expectedVlan_actual));

    AtEthVlanTagConstruct(1, 1, 4095, &expectedVlan);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPsnExpectedCVlanSet(psnPort, &expectedVlan));
    AtPtpPsnExpectedCVlanGet(psnPort, &expectedVlan_actual);
    TEST_ASSERT(VlanIdAreSame(&expectedVlan, &expectedVlan_actual));

    AtEthVlanTagConstruct(1, 1, 4094, &expectedVlan);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPsnExpectedSVlanSet(psnPort, &expectedVlan));
    AtPtpPsnExpectedSVlanGet(psnPort, &expectedVlan_actual);
    TEST_ASSERT(VlanIdAreSame(&expectedVlan, &expectedVlan_actual));
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtPtpPsn ptpPsn = (AtPtpPsn)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    AtSnprintf(buffer, bufferSize, "%s", AtPtpPsnTypeString(ptpPsn));
    return buffer;
    }

static void Run(AtUnittestRunner self)
    {
    m_AtUnittestRunnerMethods->Run(self);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_PtpPsn)
        {
        new_TestFixture("testCanChangeExpectedVlan", testCanChangeExpectedVlan),

        /* unicast address with entry available for IP */
        new_TestFixture("testEnableExpectedUnicastSourceIpAddress", testEnableExpectedUnicastSourceIpAddress),
        new_TestFixture("testChangeExpectedUnicastSourceIpAddress", testChangeExpectedUnicastSourceIpAddress),
        new_TestFixture("testEnableExpectedUnicastDestIpAddress", testEnableExpectedUnicastDestIpAddress),
        new_TestFixture("testChangeExpectedUnicastDestIpAddress", testChangeExpectedUnicastDestIpAddress),
        /* unicast address single available for MAC only */
        new_TestFixture("testEnableUnicastSourceMacAddress", testEnableUnicastSourceMacAddress),
        new_TestFixture("testChangeUnicastSourceMacAddress", testChangeUnicastSourceMacAddress),
        new_TestFixture("testEnableUnicastDestMacAddress", testEnableUnicastDestMacAddress),
        new_TestFixture("testChangeUnicastDestMacAddress", testChangeUnicastDestMacAddress),

        new_TestFixture("testEnableAnycastDestAddress", testEnableAnycastDestAddress),
        new_TestFixture("testEnableAnycastSourceAddress", testEnableAnycastSourceAddress),
        new_TestFixture("testEnableMulticastDestAddress", testEnableMulticastDestAddress),
        new_TestFixture("testChangeMulticastDestAddress", testChangeMulticastDestAddress),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_PtpPsn_Caller, "TestSuite_PtpPort", NULL, NULL, TestSuite_PtpPsn);

    return (TestRef)((void *)&TestSuite_PtpPsn_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(self), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        }

    self->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPsnTestRunner);
    }

AtUnittestRunner AtPtpPsnTestRunnerObjectInit(AtUnittestRunner self, AtPtpPsn ptpPort)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)ptpPort) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtPtpPsnTestRunnerNew(AtPtpPsn psnPort)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPtpPsnTestRunnerObjectInit(newRunner, psnPort);
    }

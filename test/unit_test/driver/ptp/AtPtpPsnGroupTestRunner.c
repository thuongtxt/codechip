/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpPsnGroupTestRunner.c
 *
 * Created Date: May 6, 2019
 *
 * Description : PTP Group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtPtpPsnGroup.h"
#include "AtPtpPort.h"
#include "../../../../driver/src/generic/ptp/psn/AtPtpPsnGroupInternal.h"
#include "../man/AtObjectTestRunnerInternal.h"
#include "AtModulePtpTestRunner.h"
#include "AtPtpPsnGroupTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPtpPsnGroupTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPtpPsnGroupTestRunner
    {
    tAtObjectTestRunner super;
    AtList ptptPorts;
    AtModulePtpTestRunner module;
    }tAtPtpPsnGroupTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPtpPsnGroupTestRunner Runner(void)
    {
    return (AtPtpPsnGroupTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtPtpPsnGroup PtpPsnGroup(void)
    {
    return AtPtpPsnGroupTestRunnerGroupGet(Runner());
    }

static void RemoveAllMemberInGroup(void)
    {
    AtTestLoggerEnable(cAtFalse);
    while (AtPtpPsnGroupNumPtpPorts(PtpPsnGroup()) > 0)
        {
        AtPtpPort ptpPort = AtPtpPsnGroupPortGet(PtpPsnGroup(), 0);
        AtPtpPsnGroupPortRemove(PtpPsnGroup(), ptpPort);
        }
    AtTestLoggerEnable(cAtTrue);
    AtAssert(AtPtpPsnGroupNumPtpPorts(PtpPsnGroup()) == 0);
    }

static void tearDown(void)
    {
    RemoveAllMemberInGroup();
    }

static void testAccessGroupTypeMustBeValid(void)
    {
    TEST_ASSERT(AtPtpPsnGroupTypeGet(PtpPsnGroup()) != cAtPtpPsnTypeUnknown);
    }

static uint8 TransportTypeFromPsnType(AtPtpPsnGroup psnGroup)
    {
    uint8 psnType = AtPtpPsnGroupTypeGet(psnGroup);

    if (psnType == cAtPtpPsnTypeLayer2) return cAtPtpTransportTypeL2TP;
    if (psnType == cAtPtpPsnTypeIpV4)   return cAtPtpTransportTypeIpV4;
    if (psnType == cAtPtpPsnTypeIpV6)   return cAtPtpTransportTypeIpV6;
    return cAtPtpPsnTypeUnknown;
    }

static void AddPtpPortToGroup(AtPtpPsnGroup psnGroup, AtPtpPort ptpPort)
    {
    uint8 currentTransport = AtPtpPortTransportTypeGet(ptpPort);
    uint8 transportType = TransportTypeFromPsnType(psnGroup);

    AtTestLoggerEnable(cAtFalse);
    AtPtpPortTransportTypeSet(ptpPort, transportType);
    AtTestLoggerEnable(cAtTrue);

    TEST_ASSERT(AtPtpPsnGroupPortAdd(psnGroup, ptpPort) == cAtOk);
    TEST_ASSERT(AtPtpPsnGroupContainsPtpPort(psnGroup, ptpPort));

    AtTestLoggerEnable(cAtFalse);
    AtPtpPortTransportTypeSet(ptpPort, currentTransport);
    AtTestLoggerEnable(cAtTrue);
    }

static void testAddRemovePtpPortFromGroup(void)
    {
    uint32 ptpPortId, numPort;
    AtList ptpPortList = AtPtpPsnGroupTestRunnerPtpPortsGet(Runner());
    AtPtpPort ptpPort;
    AtPtpPsnGroup psnGroup = PtpPsnGroup();
    uint8 transportType = TransportTypeFromPsnType(psnGroup);
    uint8 currentTransport;

    numPort = AtListLengthGet(ptpPortList);
    if (AtPtpPsnGroupTypeGet(psnGroup) == cAtPtpPsnTypeLayer2)
        return;

    for (ptpPortId = 0; ptpPortId < numPort; ptpPortId++)
        {
        ptpPort = (AtPtpPort)AtListObjectGet(ptpPortList, ptpPortId);
        AddPtpPortToGroup(psnGroup, ptpPort);
        }

    for (ptpPortId = 0; ptpPortId < numPort; ptpPortId++)
        {
        ptpPort = (AtPtpPort)AtListObjectGet(ptpPortList, ptpPortId);
        currentTransport = AtPtpPortTransportTypeGet(ptpPort);
        AtTestLoggerEnable(cAtFalse);
        AtPtpPortTransportTypeSet(ptpPort, transportType);
        AtTestLoggerEnable(cAtTrue);

        TEST_ASSERT(AtPtpPsnGroupPortRemove(psnGroup, ptpPort) == cAtOk);
        TEST_ASSERT(AtPtpPsnGroupContainsPtpPort(psnGroup, ptpPort) == cAtFalse);

        AtTestLoggerEnable(cAtFalse);
        AtPtpPortTransportTypeSet(ptpPort, currentTransport);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCannotAddPtpPortNullToGroup(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPtpPsnGroupPortAdd(PtpPsnGroup(), NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotAddPtpPortToGroupNull(void)
    {
    AtPtpPort ptpPort = (AtPtpPort)AtListObjectGet(AtPtpPsnGroupTestRunnerPtpPortsGet(Runner()), 0);
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPtpPsnGroupPortAdd(NULL, ptpPort) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNothingHappenWhenReAddPtpPortToGroup(void)
    {
    uint32 ptpPortId, numPtpPort;
    AtList ptpPortList = AtPtpPsnGroupTestRunnerPtpPortsGet(Runner());

    numPtpPort = AtListLengthGet(ptpPortList);
    for (ptpPortId = 0; ptpPortId < numPtpPort; ptpPortId++)
        {
        AtPtpPort ptpPort = (AtPtpPort)AtListObjectGet(ptpPortList, ptpPortId);
        AtPtpPsnGroup psnGroup = PtpPsnGroup();
        uint32 numPtpPorts;

        AddPtpPortToGroup(psnGroup, ptpPort);
        numPtpPorts = AtPtpPsnGroupNumPtpPorts(psnGroup);
        AddPtpPortToGroup(psnGroup, ptpPort);
        TEST_ASSERT_EQUAL_INT(numPtpPorts, AtPtpPsnGroupNumPtpPorts(psnGroup));
        }
    }

static void testCanRemovePtpPortNullFromGroups(void)
    {
    TEST_ASSERT(AtPtpPsnGroupPortRemove(PtpPsnGroup(), NULL) == cAtOk);
    }

static void testCannotRemovePtpPortWhichDoesNotBelongtoGroup(void)
    {
    uint32 portId, numPort;
    AtList ptpPortList = AtPtpPsnGroupTestRunnerPtpPortsGet(Runner());

    numPort = AtListLengthGet(ptpPortList);
    for (portId = 0; portId < numPort; portId++)
        {
        AtPtpPort ptpPorts = (AtPtpPort)AtListObjectGet(ptpPortList, portId);

        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtPtpPsnGroupPortRemove(PtpPsnGroup(), ptpPorts) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCannotRemovePtpPortFromGroupNull(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtPtpPsnGroupPortRemove(NULL, (AtPtpPort)AtListObjectGet(AtPtpPsnGroupTestRunnerPtpPortsGet(Runner()), 0)) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanGetPtpPortInGroupByValidIndex(void)
    {
    uint32 portId, numPort;
    AtList ptpPortList = AtPtpPsnGroupTestRunnerPtpPortsGet(Runner());
    AtPtpPsnGroup psnGroup = PtpPsnGroup();

    numPort = AtListLengthGet(ptpPortList);
    for (portId = 0; portId < numPort; portId++)
        {
        AtPtpPort ptpPort = (AtPtpPort)AtListObjectGet(ptpPortList, portId);

        AddPtpPortToGroup(psnGroup, ptpPort);
        TEST_ASSERT_NOT_NULL(AtPtpPsnGroupPortGet(psnGroup, portId));
        }
    }

static void testCannotGetPtpPortInGroupsByOutOfRangeIndex(void)
    {
    uint32 portId, numPort;
    AtList ptpPortList = AtPtpPsnGroupTestRunnerPtpPortsGet(Runner());
    AtPtpPsnGroup psnGroup = PtpPsnGroup();

    numPort = AtListLengthGet(ptpPortList);
    for (portId = 0; portId < numPort; portId++)
        {
        AtPtpPort ptpPorts = (AtPtpPort)AtListObjectGet(ptpPortList, portId);

        AddPtpPortToGroup(psnGroup, ptpPorts);
        TEST_ASSERT_NOT_NULL(AtPtpPsnGroupPortGet(psnGroup, portId));
        TEST_ASSERT_NULL(AtPtpPsnGroupPortGet(PtpPsnGroup(), portId + 1))
        }
    }

static void testCannotGetPtpPortFromGroupNull(void)
    {
    TEST_ASSERT_NULL(AtPtpPsnGroupPortGet(NULL, 0));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    EMB_UNIT_TESTFIXTURES(TestSuite_PtpPsnGroup_Fixtures)
        {
        new_TestFixture("testAccessGroupTypeMustBeValid", testAccessGroupTypeMustBeValid),
        new_TestFixture("testAddRemovePtpPortFromGroup", testAddRemovePtpPortFromGroup),
        new_TestFixture("testCannotAddPtpPortNullToGroup", testCannotAddPtpPortNullToGroup),
        new_TestFixture("testCannotAddPtpPortToGroupNull", testCannotAddPtpPortToGroupNull),
        new_TestFixture("testNothingHappenWhenReAddPtpPortToGroup", testNothingHappenWhenReAddPtpPortToGroup),
        new_TestFixture("testCanRemovePtpPortNullFromGroups", testCanRemovePtpPortNullFromGroups),
        new_TestFixture("testCannotRemovePtpPortWhichDoesNotBelongtoGroup", testCannotRemovePtpPortWhichDoesNotBelongtoGroup),
        new_TestFixture("testCannotRemovePtpPortFromGroupNull", testCannotRemovePtpPortFromGroupNull),
        new_TestFixture("testCanGetPtpPortInGroupByValidIndex", testCanGetPtpPortInGroupByValidIndex),
        new_TestFixture("testCannotGetPtpPortInGroupsByOutOfRangeIndex", testCannotGetPtpPortInGroupsByOutOfRangeIndex),
        new_TestFixture("testCannotGetPtpPortFromGroupNull", testCannotGetPtpPortFromGroupNull)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtPtpPsnGroup_Caller, "TestSuite_AtPtpPsnGroup", NULL, tearDown, TestSuite_PtpPsnGroup_Fixtures);

    return (TestRef)((void *)&TestSuite_AtPtpPsnGroup_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtPtpPsnGroup group = AtPtpPsnGroupTestRunnerGroupGet((AtPtpPsnGroupTestRunner)self);
    AtSnprintf(buffer, bufferSize - 1, "%s.%d", AtPtpPsnGroupTypeString(group), AtPtpPsnGroupIdGet(group));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtPtpPsnGroupTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtPtpPsnGroupTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPsnGroupTestRunner);
    }

AtPtpPsnGroupTestRunner AtPtpPsnGroupTestRunnerObjectInit(AtPtpPsnGroupTestRunner self,
                                                          AtPtpPsnGroup group,
                                                          AtModulePtpTestRunner moduleRunner,
                                                          AtList ptpPorts)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)group) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->ptptPorts = ptpPorts;
    self->module = moduleRunner;

    return self;
    }

AtPtpPsnGroupTestRunner AtPtpPsnGroupTestRunnerNew(AtPtpPsnGroup group, AtModulePtpTestRunner moduleRunner, AtList ptpPorts)
    {
    AtPtpPsnGroupTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPtpPsnGroupTestRunnerObjectInit(newRunner, group, moduleRunner, ptpPorts);
    }

AtPtpPsnGroup AtPtpPsnGroupTestRunnerGroupGet(AtPtpPsnGroupTestRunner self)
    {
    return (AtPtpPsnGroup)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

AtList AtPtpPsnGroupTestRunnerPtpPortsGet(AtPtpPsnGroupTestRunner self)
    {
    return self ? self->ptptPorts : NULL;
    }

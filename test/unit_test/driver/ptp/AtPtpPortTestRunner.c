/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpPortTestRunner.c
 *
 * Created Date: May 2, 2019
 *
 * Description : PTP Port Test Runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtPtpPort.h"
#include "../../../../driver/src/generic/ptp/AtPtpPortInternal.h"
#include "../man/channel_runner/AtChannelTestRunnerInternal.h"
#include "AtPtpPortTestRunner.h"
#include "AtPtpPsnTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPtpPortTestRunner *AtPtpPortTestRunner;
typedef struct tAtPtpPortTestRunner
    {
    tAtChannelTestRunner super;
    }tAtPtpPortTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPtpPort PtpPort()
    {
    return (AtPtpPort)AtChannelTestRunnerChannelGet((AtChannelTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testCanChangeTransportType()
    {
    uint8 values[] = {cAtPtpTransportTypeUnknown,
                      cAtPtpTransportTypeL2TP,
                      cAtPtpTransportTypeIpV4,
                      cAtPtpTransportTypeIpV6,
                      cAtPtpTransportTypeIpV4Vpn,
                      cAtPtpTransportTypeIpV6Vpn,
                      cAtPtpTransportTypeAny};
    uint8 value_i;
    AtPtpPort port = PtpPort();

    for (value_i = 0; value_i < mCount(values); value_i++)
        {
        if (AtPtpPortTransportTypeIsSupported(port, values[value_i]) == cAtFalse)
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtPtpPortTransportTypeSet(port, values[value_i]));
            AtTestLoggerEnable(cAtTrue);
            continue;
            }

        TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortTransportTypeSet(port, values[value_i]));
        TEST_ASSERT_EQUAL_INT(values[value_i], AtPtpPortTransportTypeGet(port));
        }
    }

static eBool IsBackPlanPort(AtPtpPort ptpPort)
    {
    return (AtChannelIdGet((AtChannel)ptpPort) == 0) ? cAtTrue : cAtFalse;
    }

static void testCanChangePortState()
    {
    uint8 curValue;
    AtPtpPort port = PtpPort();
    AtModulePtp module = (AtModulePtp)AtChannelModuleGet((AtChannel)port);

    /* This port does not applicable */
    if (IsBackPlanPort(port))
        return;

    curValue = AtModulePtpDeviceTypeGet(module);

    /* Test change port state not transparent device type */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceTypeSet(module, cAtPtpDeviceTypeBoundary));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortStateSet(port, cAtPtpPortStateSlave));
    TEST_ASSERT_EQUAL_INT(cAtPtpPortStateSlave, AtPtpPortStateGet(port));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortStateSet(port, cAtPtpPortStateMaster));
    TEST_ASSERT_EQUAL_INT(cAtPtpPortStateMaster, AtPtpPortStateGet(port));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtPtpPortStateSet(port, cAtPtpPortStatePassive));
    AtTestLoggerEnable(cAtTrue);

    /* Test change port state transparent device type */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceTypeSet(module, cAtPtpDeviceTypeTransparent));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortStateSet(port, cAtPtpPortStatePassive));
    TEST_ASSERT_EQUAL_INT(cAtPtpPortStatePassive, AtPtpPortStateGet(port));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtPtpPortStateSet(port, cAtPtpPortStateSlave));
    TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtPtpPortStateSet(port, cAtPtpPortStateMaster));
    AtTestLoggerEnable(cAtTrue);

    /* Revert current mode */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtModulePtpDeviceTypeSet(module, curValue));
    }

static void testCanChangeStepMode()
    {
    AtPtpPort port = PtpPort();

    /* This port does not applicable */
    if (IsBackPlanPort(port))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortStepModeSet(port, cAtPtpStepModeOneStep));
    TEST_ASSERT_EQUAL_INT(cAtPtpStepModeOneStep, AtPtpPortStepModeGet(port));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortStepModeSet(port, cAtPtpStepModeTwoStep));
    TEST_ASSERT_EQUAL_INT(cAtPtpStepModeTwoStep, AtPtpPortStepModeGet(port));

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtPtpPortStepModeSet(port, cAtPtpStepModeUnknown));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanChangeDelayAdjust()
    {
    AtPtpPort port = PtpPort();
    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortTxDelayAdjustSet(port, 101));
    TEST_ASSERT_EQUAL_INT(101, AtPtpPortTxDelayAdjustGet(port));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtPtpPortRxDelayAdjustSet(port, 1001));
    TEST_ASSERT_EQUAL_INT(1001, AtPtpPortRxDelayAdjustGet(port));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_PtpPort)
        {
        new_TestFixture("testCanChangeTransportType", testCanChangeTransportType),
        new_TestFixture("testCanChangePortState", testCanChangePortState),
        new_TestFixture("testCanChangeStepMode", testCanChangeStepMode),
        new_TestFixture("testCanChangeDelayAdjust", testCanChangeDelayAdjust),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_PtpPort_Caller, "TestSuite_PtpPort", NULL, NULL, TestSuite_PtpPort);

    return (TestRef)((void *)&TestSuite_PtpPort_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static AtUnittestRunner CreatePtpPsnPortTestRunner(AtPtpPortTestRunner self, AtChannel ptpPort)
    {
    return AtPtpPsnTestRunnerNew((AtPtpPsn)ptpPort);
    }

static uint8 PtpPsnType2TransportType(uint8 psnLayer)
    {
    if (psnLayer == cAtPtpPsnTypeLayer2) return cAtPtpTransportTypeL2TP;
    if (psnLayer == cAtPtpPsnTypeIpV4) return cAtPtpTransportTypeIpV4;
    if (psnLayer == cAtPtpPsnTypeIpV6) return cAtPtpTransportTypeIpV6;
    return cAtPtpTransportTypeAny;
    }

static void TestPsn(AtPtpPortTestRunner self)
    {
    uint32 i;
    AtPtpPort port = PtpPort();
    uint8 values[] = {cAtPtpPsnTypeLayer2, cAtPtpPsnTypeIpV4, cAtPtpPsnTypeIpV6};

    for (i = 0; i < mCount(values); i++)
        {
        AtChannel psn;
        AtUnittestRunner runner;

        AtTestLoggerEnable(cAtFalse);
        AtPtpPortTransportTypeSet(port, PtpPsnType2TransportType(values[i]));
        AtTestLoggerEnable(cAtTrue);

        psn = (AtChannel)AtPtpPortPsnGet(port, values[i]);
        runner = (AtUnittestRunner)CreatePtpPsnPortTestRunner(self, psn);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        }
    }

static void Run(AtUnittestRunner self)
    {
    AtPtpPortTestRunner runner = (AtPtpPortTestRunner)self;

    m_AtUnittestRunnerMethods->Run(self);

    TestPsn(runner);
    }

static void OverrideAtUnittestRunner(AtChannelTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtChannelTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPortTestRunner);
    }

AtChannelTestRunner AtPtpPortTestRunnerObjectInit(AtChannelTestRunner self, AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelTestRunnerObjectInit(self, channel, moduleRunner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelTestRunner AtPtpPortTestRunnerNew(AtChannel channel, AtModuleTestRunner moduleRunner)
    {
    AtChannelTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPtpPortTestRunnerObjectInit(newRunner, channel, moduleRunner);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtModulePtpTestRunner.h
 * 
 * Created Date: May 6, 2019
 *
 * Description : Module PTP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPTPTESTRUNNER_H_
#define _ATMODULEPTPTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePtpTestRunner * AtModulePtpTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtList AtModulePtpTestRunnerAddPtpPortsFromString(AtList ptpPorts, char *portString);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPTPTESTRUNNER_H_ */


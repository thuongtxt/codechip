/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Test
 * 
 * File        : AtTests.h
 * 
 * Created Date: Jul 25, 2012
 *
 * Author      : namnn
 * 
 * Description : Test
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTESTS_H_
#define _ATTESTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include <embUnit/embUnit.h>
#include <textui/Outputter.h>
#include <textui/TextUIRunner.h>
#include <textui/XMLJUnitOutputter.h>
#include <textui/XML+TextOutputter.h>
#include <stdlib.h>
#include <string.h>

#include "atclib.h"
#include "AtDriver.h"
#include "AtModuleSdh.h"
#include "apputil.h"
#include "AtModulePw.h"
#include "AtModulePw.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mCliSuccessAssert(result) AtAssert(((result) == 0) ? 1 : 0)

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtList CliPwListFromString(char *pwIdListString);
AtChannel *CliSharedChannelListGet(uint32 *bufferSize);
uint32 CliAtPtpPortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize);

/*--------------------------- Entries ----------------------------------------*/
AtDriver TestedDriver();
void Cleanup();
const uint32 *AtTestSupportedProducts(uint8 *numProducts);
const uint32 *AtTestProductsToBeTested(uint8 *numProducts);
void AtTestSimulateProduct(uint32 productCode);

AtHal AtTestHalCreate(uint8 coreId);
AtOsal TestedOsal();

uint32 AtTestProductCodeGet();
AtModuleSdh TestSdhModule();

void AtTestSimulationTest(eBool simulationTest);
eBool AtTestIsSimulationTesting();

void AtTestQuickTestEnable(eBool quickTest);
eBool AtTestQuickTestIsEnabled(void);

/* Random */
uint32 AtTestRandom(uint32 max, uint32 currentValue);
uint32 AtTestRandomNumChannels(uint32 numChannels);
void AtTestNumRandomChannelsSet(uint32 numRandomChannels);
uint32 AtTestNumRandomChannelsGet(void);

/* For some cases,to avoid unwanted log messages */
void AtTestLoggerEnable(eBool enable);
eBool AtTestLoggerIsEnabled(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATTESTS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : STD
 *
 * File        : AtStdTestRunner.c
 *
 * Created Date: Sep 18, 2016
 *
 * Description : Standard C library
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../runner/AtUnittestRunnerInternal.h"
#include "AtTests.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtStdTestRunner *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtStdTestRunner
    {
    tAtUnittestRunner super;

    /* Private data */
    AtStd std;
    }tAtStdTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtStd TestedStd()
    {
    return mThis(AtUnittestRunnerCurrentRunner())->std;
    }

static void testSort()
    {
    uint32 values[]         = {2,5,1,6,7,9,7};
    uint32 expectedValues[] = {1,2,5,6,7,7,9};
    AtStdSort(TestedStd(), values, mCount(values), sizeof(uint32), AtStdUint32CompareFunction);
    TEST_ASSERT_EQUAL_INT(0, AtOsalMemCmp(values, expectedValues, sizeof(values)));
    }

static void testNumberToString()
    {
    char idString[32];
    char *result;
    AtStd std = TestedStd();

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){1}, 1);
    TEST_ASSERT(AtStrcmp(result, "1") == 0);

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){1,5,3}, 3);
    TEST_ASSERT(AtStrcmp(result, "1,3,5") == 0);

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){3,2,1,4}, 4);
    TEST_ASSERT(AtStrcmp(result, "1-4") == 0);

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){3,1,2,1,1}, 5);
    TEST_ASSERT(AtStrcmp(result, "1-3") == 0);

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){3,1,2,1,1,5,8,7,6,10}, 10);
    TEST_ASSERT(AtStrcmp(result, "1-3,5-8,10") == 0);
    }

static void testNumberToStringMustHandleSmallBufferSize()
    {
    char idString[5];
    char *result;
    AtStd std = TestedStd();

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){123456}, 1);
    TEST_ASSERT(AtStrcmp(result, "1234") == 0);

    result = AtStdNumbersToString(std, idString, sizeof(idString), (uint32[]){3,1,2,1,1,5,8,7,6,10}, 10);
    TEST_ASSERT(AtStrcmp(result, "1-3,") == 0);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtStd_Fixtures)
        {
        new_TestFixture("testSort", testSort),
        new_TestFixture("testNumberToString", testNumberToString),
        new_TestFixture("testNumberToStringMustHandleSmallBufferSize", testNumberToStringMustHandleSmallBufferSize)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtStd_Caller, "TestSuite_AtStd", NULL, NULL, TestSuite_AtStd_Fixtures);

    return (TestRef)((void *)&TestSuite_AtStd_Caller);
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSprintf(buffer, "AtStd");
    return buffer;
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtStdTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtStd std)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->std = std;

    return self;
    }

AtUnittestRunner AtStdTestRunnerNew(AtStd std)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, std);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : STD
 * 
 * File        : AtStdTestRunner.h
 * 
 * Created Date: Sep 18, 2016
 *
 * Description : Standard C library
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTDTESTRUNNER_H_
#define _ATSTDTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtStd.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner AtStdTestRunnerNew(AtStd std);

#ifdef __cplusplus
}
#endif
#endif /* _ATSTDTESTRUNNER_H_ */


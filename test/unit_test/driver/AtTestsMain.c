/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Unittest
 *
 * File        : AtTestsMain.c
 *
 * Created Date: Aug 31, 2012
 *
 * Author      : namnn
 *
 * Description : Unittest main apps
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <getopt.h>
#include <execinfo.h>
#include <string.h>

#include "attypes.h"
#include "atclib.h"
#include "AtTests.h"
#include "apputil.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define sStartStack "================= Start of backtrace ===============\r\n"
#define sEndStack   "================== End of backtrace ================\r\n"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 signalStackMem[SIGSTKSZ];
static eBool m_xmlOutput = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/
extern int AllUnittestRunWithXmlOutputer(eBool xml);
extern void Cleanup();

/*--------------------------- Implementation ---------------------------------*/
static char *StackStringBuild(char *execPath, void *stackPointer)
    {
    FILE *fp;
    static char lineBuffer[2048];
    char command[64];

    AtSprintf(command,"addr2line -f -e %s %p", execPath, stackPointer);

    /* Open the command for reading. */
    fp = popen(command, "r");
    if (fp == NULL)
        {
        mPrintError("Cannot run \"%s\"\r\n", command);
        return NULL;
        }

    AtOsalMemInit(lineBuffer, 0, 2048);

    /* Read the output */
    while (fgets(lineBuffer, sizeof(lineBuffer), fp) != NULL);

    pclose(fp);
    return lineBuffer;
    }

static void SignalBacktrace(int sigNum)
    {
    const int maxPointers = 100;
    void *stackPointers[maxPointers];
    int numPointers, j;
    char execPath[32] = {0};
    char reportPath[] = "backtrace.txt";
    FILE *logger;

    sprintf(execPath, "/proc/%d/exe", getpid());

    numPointers = backtrace(stackPointers, maxPointers);

    logger = fopen(reportPath, "w");

    AtPrintc(cSevWarning, "====================================================\r\n");
    AtPrintc(cSevWarning, "WARNING: The \"%s\" has just happened.\r\n", strsignal(sigNum));
    AtPrintc(cSevWarning, "Please report the log file %s to software team.     \r\n", reportPath);
    AtPrintc(cSevWarning, "====================================================\r\n");

    AtPrintf(sStartStack);
    if (logger)
        fprintf(logger, "%s", sStartStack);

    for (j = 0; j < numPointers; j++)
        {
        char *stackStr = StackStringBuild(execPath, stackPointers[j]);
        AtPrintf("[%3d]: %s", j, stackStr);
        if (logger)
            fprintf(logger, "[%3d]: %s", j, stackStr);
        }

    AtPrintf(sEndStack);
    if (logger)
        {
        fprintf(logger, "%s", sEndStack);

        fflush(logger);
        fclose(logger);
        }
    }

/*
 * Install handler for a signal
 * @return cAtTrue on success, cAtFalse on failure
 */
static atbool SignalHandlerInstall(int sigNum, void (*sigHandler)(int))
    {
    struct sigaction action;

    /* Register signal handler for sigNum */
    sigemptyset(&action.sa_mask);
    sigaddset(&action.sa_mask, sigNum);
    action.sa_flags = SA_ONSTACK;
    action.sa_handler = sigHandler;

    if(sigaction(sigNum, &action, NULL) != 0)
        {
        return cAtFalse;
        }

    return cAtTrue;
    }

/*
 * Cleanup on signal received and activate signal's default action
 */
static void SignalCleanupWithDefaultAction(int sigNum)
    {
    Cleanup();

    /* Reset default action */
    SignalHandlerInstall(sigNum, SIG_DFL);
    kill(getpid(), sigNum); /* Re-send signal to force it activates the default action */
    }

/*
 * Handle on signal received with backtrace dump and system default action
 */
static void SignalHandlerWithBacktrace(int sigNum)
    {
    /* Dump backtrace */
    SignalBacktrace(sigNum);

    /* Reset signal's default handler */
    SignalHandlerInstall(sigNum, SIG_DFL);

    /* Re-send signal to force it activates the default action */
    kill(getpid(), sigNum);
    }

/*
 * Handler of signal SIGSEGV
 * @param sigNum Signal number
 */
static void SIGSEGV_Handler(int sigNum)
    {
    /* Program will not be cleanup to avoid nest lock and make the unittest cannot exit */
    SignalHandlerWithBacktrace(sigNum);
    }

/*
 * Handler of signal SIGBUS
 * @param sigNum Signal number
 */
static void SIGBUS_Handler(int sigNum)
    {
    SignalHandlerWithBacktrace(sigNum);
    }

/*
 * Handler of signal SIGFPE
 * @param sigNum Signal number
 */
static void SIGFPE_Handler(int sigNum)
    {
    SignalHandlerWithBacktrace(sigNum);
    }

/*
 * Handler of signal SIGTERM
 * @param sigNum Signal number
 */
static void SIGTERM_Handler(int sigNum)
    {
    printf("Unit test is terminated\n");
    SignalCleanupWithDefaultAction(sigNum);
    }

/*
 * Handler of signal SIGINT
 * @param sigNum Signal number
 */
static void SIGINT_Handler(int sigNum)
    {
    kill(getpid(), SIGTERM);
    }

static void SetupSignalHandler()
    {
    /* Install handler for signal SIGSEGV (Segmentation fault)
     * Create new stack to capture signal SIGSEGV */
    stack_t newSignalStack;

    newSignalStack.ss_sp    = signalStackMem;
    newSignalStack.ss_size  = SIGSTKSZ;
    newSignalStack.ss_flags = 0;
    if (sigaltstack(&newSignalStack, NULL) != 0)
        return;

    SignalHandlerInstall(SIGSEGV, SIGSEGV_Handler);
    SignalHandlerInstall(SIGBUS, SIGBUS_Handler);
    SignalHandlerInstall(SIGFPE, SIGFPE_Handler);
    SignalHandlerInstall(SIGTERM, SIGTERM_Handler);
    SignalHandlerInstall(SIGINT, SIGINT_Handler);

    sleep(1);
    }

static void SetupProducts(char *products)
    {
    AtTokenizer parser = AtTokenizerSharedTokenizer(products, ",");

    if (AtTokenizerNumStrings(parser) == 0)
        {
        AtPrintc(cSevCritical, "No products, product list should be separated by \",\"\r\n");
        return;
        }

    while (AtTokenizerHasNextString(parser))
        {
        char *productCodeString = AtTokenizerNextString(parser);
        uint32 productCode;
        StrToDw(productCodeString, 'h', &productCode);
        AtTestSimulateProduct(productCode);
        }
    }

static eBool AdditionalOptionParse(int argc, char* argv[], const char *format)
    {
    int opt;

    optind = 1;

    while ((opt = getopt( argc, argv, format)) != AtEof())
        {
        switch (opt)
            {
            case 'q':
                AtTestQuickTestEnable(cAtTrue);
                break;

            case 'Q':
                {
                uint32 numChannels = 0;

                if (sscanf(optarg, "%u", &numChannels) != 1)
                    {
                    AtPrintc(cSevCritical, "ERROR: need to input number of channels to be random\r\n");
                    abort();
                    return cAtFalse;
                    }

                AtTestNumRandomChannelsSet(numChannels);
                }
                break;

            /* More than one products can be specified */
            case 's':
                {
                char optStr[1024];

                AtOsalMemInit((void*)optStr, 0, sizeof(optStr));
                if (sscanf(optarg, "%s", optStr) != 1)
                    {
                    AtPrintc(cSevCritical, "Please input product codes\r\n");
                    return cAtFalse;
                    }

                SetupProducts(optStr);
                }
                break;

            case 'x':
                m_xmlOutput = cAtTrue;
                break;

            default:
                ;
            }
        }

    return cAtTrue;
    }

static void OptionParse(int argc, char* argv[])
    {
    char format[128];

    AtSprintf(format, "%sqxQ:", AppOptionParseDefaultFormat());
    AppOptionParseWithFormat(argc, argv, format);
    AdditionalOptionParse(argc, argv, format);
    }

int main(int argc, char **argv)
    {
    extern int AtCliStop();

    SetupSignalHandler();
    OptionParse(argc, argv);

    return AllUnittestRunWithXmlOutputer(m_xmlOutput);
    }


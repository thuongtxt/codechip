/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSerdesManagerTestRunnerInternal.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : SERDES manager test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSERDESMANAGERTESTRUNNERINTERNAL_H_
#define _ATSERDESMANAGERTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtObjectTestRunnerInternal.h"
#include "AtPhysicalTests.h"
#include "AtSerdesManager.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSerdesManagerTestRunnerMethods
    {
    AtSerdesControllerTestRunner (*ControllerTestRunnerCreate)(AtSerdesManagerTestRunner self, AtSerdesController serdes);
    eBool (*SerdesModeMustBeSupported)(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode);
    eBool (*SerdesPowerCanBeControlled)(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner);
    uint8 (*NumSerdesControllers)(AtSerdesManagerTestRunner self);
    }tAtSerdesManagerTestRunnerMethods;

typedef struct tAtSerdesManagerTestRunner
    {
    tAtObjectTestRunner super;
    const tAtSerdesManagerTestRunnerMethods *methods;
    }tAtSerdesManagerTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManagerTestRunner AtSerdesManagerTestRunnerObjectInit(AtSerdesManagerTestRunner self, AtSerdesManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _ATSERDESMANAGERTESTRUNNERINTERNAL_H_ */


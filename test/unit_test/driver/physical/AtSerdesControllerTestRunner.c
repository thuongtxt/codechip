/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtSerdesControllerTestRunner.c
 *
 * Created Date: Jul 13, 2016
 *
 * Description : SERDES controller test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesControllerTestRunnerInternal.h"
#include "../../../../../driver/src/generic/physical/AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSerdesController(self) (AtSerdesController)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static const eAtSerdesMode cSerdesSupportMode[] = { cAtSerdesModeStm0,
                                                    cAtSerdesModeStm1,
                                                    cAtSerdesModeStm4,
                                                    cAtSerdesModeStm16,
                                                    cAtSerdesModeStm64,
                                                    cAtSerdesModeEth10M,
                                                    cAtSerdesModeEth100M,
                                                    cAtSerdesModeEth2500M,
                                                    cAtSerdesModeEth1G,
                                                    cAtSerdesModeEth10G,
                                                    cAtSerdesModeEth40G,
                                                    cAtSerdesModeOcn,
                                                    cAtSerdesModeGe
                                                    };

static const eAtSerdesTimingMode cSerdesTimingMode[] = {
                                             cAtSerdesTimingModeLockToData,
                                             cAtSerdesTimingModeLockToRef
                                            };

static const eAtSerdesParam cSerdesParam[] =
                                    {
                                    cAtSerdesParamVod,
                                    cAtSerdesParamPreEmphasisPreTap,
                                    cAtSerdesParamPreEmphasisFirstPostTap,
                                    cAtSerdesParamPreEmphasisSecondPostTap,
                                    cAtSerdesParamRxEqualizationDcGain,
                                    cAtSerdesParamRxEqualizationControl,
                                    cAtSerdesParamTxPreCursor,
                                    cAtSerdesParamTxPostCursor,
                                    cAtSerdesParamTxDiffCtrl
                                    };

static const eAtSerdesEqualizerMode cSerdesEqualizeMode[] = {
                                     cAtSerdesEqualizerModeUnknown,
                                     cAtSerdesEqualizerModeDfe,
                                     cAtSerdesEqualizerModeLpm
                                    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController TestedSerdesController(void)
    {
    return mSerdesController((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testSerdesModeMustBeAlwaysValid(void)
    {
    TEST_ASSERT(AtSerdesControllerModeIsValid(AtSerdesControllerModeGet(TestedSerdesController())));
    }

static void testNullSerdesMustHaveInvalidMode(void)
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesControllerModeGet(NULL) == cAtSerdesModeUnknown);
    AtTestLoggerEnable(cAtTrue);
    }

static void testInitializeValidSerdes()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerInit(TestedSerdesController()));
    }

static void testCannotInitializeNullSerdes()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesControllerInit(NULL) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testResetValidSerdes()
    {
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerReset(TestedSerdesController()));
    }

static void testCannotResetNullSerdes()
    {
    TEST_ASSERT(AtSerdesControllerReset(NULL) != cAtOk);
    }

static void testCanAccessPhysicalPort()
    {
    AtSerdesControllerPhysicalPortGet(TestedSerdesController());
    }

static void testPhysicalPortOfNullSerdesMustNotExist()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtSerdesControllerPhysicalPortGet(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testStatusCanBeRetrievedAtAnyTime_EvenWithNULLSERDES()
    {
    AtSerdesControllerPllIsLocked(TestedSerdesController());
    AtSerdesControllerLinkStatus(TestedSerdesController());

    AtTestLoggerEnable(cAtFalse);
    AtSerdesControllerPllIsLocked(NULL);
    AtSerdesControllerLinkStatus(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static AtSerdesManagerTestRunner ManagerRunner(AtSerdesControllerTestRunner self)
    {
    return (AtSerdesManagerTestRunner)self->managerRunner;
    }

static void testSupportedSerdesModeMustBeAppliedProperly()
    {
    uint8 mode_i;
    AtSerdesControllerTestRunner self = (AtSerdesControllerTestRunner)AtUnittestRunnerCurrentRunner();
    AtSerdesManagerTestRunner manager = ManagerRunner(self);

    for (mode_i = 0; mode_i < mCount(cSerdesSupportMode); mode_i++)
        {
        eAtSerdesMode mode = cSerdesSupportMode[mode_i];

        if (AtSerdesManagerTestRunnerSerdesModeMustBeSupported(manager, self, mode))
            {
            TEST_ASSERT_EQUAL_INT(cAtTrue, AtSerdesControllerModeIsSupported(TestedSerdesController(), mode));
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerModeSet(TestedSerdesController(), mode));
            TEST_ASSERT_EQUAL_INT(mode, AtSerdesControllerModeGet(TestedSerdesController()));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerModeIsSupported(TestedSerdesController(), mode));
            TEST_ASSERT(AtSerdesControllerModeSet(TestedSerdesController(), mode) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testAtLeastOneSERDESModeMustBeSupported()
    {
    uint8 mode_i;

    for (mode_i = 0; mode_i < mCount(cSerdesSupportMode); mode_i++)
        {
        eAtSerdesMode mode = cSerdesSupportMode[mode_i];

        if (AtSerdesControllerModeIsSupported(TestedSerdesController(), mode))
            return;
        }

    TEST_FAIL("At least one SERDES mode must be supported");
    }

static void testAnySerdesModeThatIsNotSupported_ErrorWillBeReturnedWhenApplying()
    {
    uint8 idx;

    for (idx = 0; idx < mCount(cSerdesSupportMode); idx++)
        {
        if (AtSerdesControllerModeIsSupported(TestedSerdesController(), cSerdesSupportMode[idx]) == cAtFalse)
            {
            eAtSerdesMode currentMode = AtSerdesControllerModeGet(TestedSerdesController());
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSerdesControllerModeSet(TestedSerdesController(), cSerdesSupportMode[idx]));
            TEST_ASSERT_EQUAL_INT(currentMode, AtSerdesControllerModeGet(TestedSerdesController()));
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testCannotApplyInvalidSERDESMode()
    {
    eAtSerdesMode currentMode = AtSerdesControllerModeGet(TestedSerdesController());
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSerdesControllerModeSet(TestedSerdesController(), cAtSerdesModeUnknown));
    TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSerdesControllerModeSet(TestedSerdesController(), 0xFFF));
    TEST_ASSERT_EQUAL_INT(currentMode, AtSerdesControllerModeGet(TestedSerdesController()));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotApplyValidSERDESModeToANULLController()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesControllerModeSet(NULL, cAtSerdesModeStm16) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testEnableDisableSerdes()
    {
    /* Most of SERDES allow enables */
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerEnable(TestedSerdesController(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtSerdesControllerIsEnabled(TestedSerdesController()));

    /* But not of them can be disabled. Just try */
    if (AtSerdesControllerCanEnable(TestedSerdesController(), cAtFalse))
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerEnable(TestedSerdesController(), cAtFalse));
        }
    else
        {
        AtTestLoggerEnable(cAtFalse);
        TEST_ASSERT(AtSerdesControllerEnable(TestedSerdesController(), cAtFalse) != cAtOk);
        AtTestLoggerEnable(cAtTrue);
        }
    }

static void testCanNotEnableDisableNullSerdes()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtSerdesControllerEnable(NULL, cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtErrorObjectNotExist, AtSerdesControllerEnable(NULL, cAtFalse));
    AtTestLoggerEnable(cAtTrue);
    }

static void testIfTimingModeCanBeControlled_ApplyAnyTimingModeMustBeFine()
    {
    uint8 idx;

    if (!AtSerdesControllerCanControlTimingMode(TestedSerdesController()))
        return;

    for (idx = 0; idx < mCount(cSerdesTimingMode); idx++)
        {
        TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerTimingModeSet(TestedSerdesController(), cSerdesTimingMode[idx]));
        TEST_ASSERT_EQUAL_INT(cSerdesTimingMode[idx], AtSerdesControllerTimingModeGet(TestedSerdesController()));
        }
    }

static void testIfTimingModeCannotBeControlled_AccessTimingModeWouldHaveAutoModeBeReturned()
    {
    if (AtSerdesControllerCanControlTimingMode(TestedSerdesController()))
        return;

    TEST_ASSERT_EQUAL_INT(cAtSerdesTimingModeAuto, AtSerdesControllerTimingModeGet(TestedSerdesController()));
    }

static void testIfTimingModeCannotBeControlled_CannotApplyAnyTimingMode()
    {
    uint8 idx;

    if (AtSerdesControllerCanControlTimingMode(TestedSerdesController()))
        return;

    AtTestLoggerEnable(cAtFalse);
    for (idx = 0; idx < mCount(cSerdesTimingMode); idx++)
        {
        TEST_ASSERT(AtSerdesControllerTimingModeSet(TestedSerdesController(), cSerdesTimingMode[idx]) != cAtOk);
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void testTimingModeCannotBeControlledOnANULLSERDES()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerCanControlTimingMode(NULL));
    TEST_ASSERT(AtSerdesControllerTimingModeSet(NULL, cAtSerdesTimingModeAuto) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testTimingModeMustBeUnknownOnANULLSERDES()
    {
    TEST_ASSERT_EQUAL_INT(cAtSerdesTimingModeUnknown, AtSerdesControllerTimingModeGet(NULL));
    }

static void TestIfASERDESParameterIsSupported_CanChangeItsValueProperly(uint32 paramValue)
    {
    uint8  idx;

    for (idx = 0; idx < mCount(cSerdesParam); idx++)
        {
        if (AtSerdesControllerPhysicalParamValueIsInRange(TestedSerdesController(), cSerdesParam[idx], paramValue))
            {
            if (AtSerdesControllerPhysicalParamIsSupported(TestedSerdesController(), cSerdesParam[idx]))
                {
                TEST_ASSERT_EQUAL_INT(AtSerdesControllerPhysicalParamSet(TestedSerdesController(), cSerdesParam[idx], paramValue), cAtOk);
                TEST_ASSERT_EQUAL_INT(AtSerdesControllerPhysicalParamGet(TestedSerdesController(), cSerdesParam[idx]), paramValue);
                }
            }
        else
            {
            if (AtSerdesControllerPhysicalParamIsSupported(TestedSerdesController(), cSerdesParam[idx]))
                {
                AtTestLoggerEnable(cAtFalse);
                TEST_ASSERT_EQUAL_INT(AtSerdesControllerPhysicalParamSet(TestedSerdesController(), cSerdesParam[idx], paramValue), cAtErrorOutOfRangParm);
                AtSerdesControllerPhysicalParamGet(TestedSerdesController(), cSerdesParam[idx]);
                AtTestLoggerEnable(cAtTrue);
                }
            }
        }
    }

static void testIfASERDESParameterIsSupported_CanChangeItsValueProperly()
    {
    TestIfASERDESParameterIsSupported_CanChangeItsValueProperly(0x0);
    TestIfASERDESParameterIsSupported_CanChangeItsValueProperly(0x1);
    TestIfASERDESParameterIsSupported_CanChangeItsValueProperly(0x10);
    TestIfASERDESParameterIsSupported_CanChangeItsValueProperly(0xFF);
    }

static void testIfASERDESParameterIsNotSupported_CannotApplyAnyValue()
    {
    uint8 idx;

    AtTestLoggerEnable(cAtFalse);

    for (idx = 0; idx < mCount(cSerdesParam); idx++)
        {
        if (AtSerdesControllerPhysicalParamIsSupported(TestedSerdesController(), cSerdesParam[idx]) == cAtFalse)
            {
            uint32 paramValue = AtSerdesControllerPhysicalParamGet(TestedSerdesController(), cSerdesParam[idx]);
            TEST_ASSERT(AtSerdesControllerPhysicalParamSet(TestedSerdesController(), cSerdesParam[idx], 0x5) != cAtOk);
            TEST_ASSERT_EQUAL_INT(paramValue, AtSerdesControllerPhysicalParamGet(TestedSerdesController(), cSerdesParam[idx]));
            }
        }

    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotApplyAnyPrameterValueOnANULLSERDES()
    {
    uint8  idx;

    AtTestLoggerEnable(cAtFalse);
    for (idx = 0; idx < mCount(cSerdesParam); idx++)
        {
        TEST_ASSERT(AtSerdesControllerPhysicalParamSet(NULL, cSerdesParam[idx], 0x5) != cAtOk);
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void testIfALoopbackCanBeMade_ItCanBeAppliedWithNoError()
    {
    eAtLoopbackMode loopbackMode[]  = {cAtLoopbackModeLocal, cAtLoopbackModeRemote};
    uint8 mode_i;

    for (mode_i = 0; mode_i < mCount(loopbackMode); mode_i++)
        {
        if (AtSerdesControllerCanLoopback(TestedSerdesController(), loopbackMode[mode_i], cAtTrue))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerLoopbackSet(TestedSerdesController(), loopbackMode[mode_i]));
            TEST_ASSERT_EQUAL_INT(loopbackMode[mode_i], AtSerdesControllerLoopbackGet(TestedSerdesController()));
            }
        else
            {
            TEST_ASSERT_EQUAL_INT(cAtErrorModeNotSupport, AtSerdesControllerLoopbackSet(TestedSerdesController(), loopbackMode[mode_i]));
            }
        }
    }

static void testSERDESStatusCanBeRetrieved_EvenWithNULLController()
    {
    AtSerdesControllerAlarmGet(TestedSerdesController());
    AtSerdesControllerAlarmHistoryGet(TestedSerdesController());
    AtSerdesControllerAlarmHistoryClear(TestedSerdesController());

    AtTestLoggerEnable(cAtFalse);
    AtSerdesControllerAlarmGet(NULL);
    AtSerdesControllerAlarmHistoryGet(NULL);
    AtSerdesControllerAlarmHistoryClear(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testForAnySupportedInterruptMask_CanApplyItProperly()
    {
    uint32 supportInterruptMask = AtSerdesControllerSupportedInterruptMask(TestedSerdesController());
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerInterruptMaskSet(TestedSerdesController(), supportInterruptMask, 0));
    TEST_ASSERT_EQUAL_INT(0, AtSerdesControllerInterruptMaskGet(TestedSerdesController()));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerInterruptMaskSet(TestedSerdesController(), supportInterruptMask, supportInterruptMask));
    TEST_ASSERT_EQUAL_INT(supportInterruptMask, AtSerdesControllerInterruptMaskGet(TestedSerdesController()));
    }

static void testForAnyNotSupportedInterruptMask_ErrorMustBeReportedWhenApplying()
    {
    uint32 supportInterruptMask = AtSerdesControllerSupportedInterruptMask(TestedSerdesController());
    uint32 unsupportInterruptMask = ~supportInterruptMask;
    uint8  bit_i;

    AtTestLoggerEnable(cAtFalse);
    for (bit_i = 0; bit_i < 32; bit_i++)
        {
        uint32 mask = 0x1 << bit_i;

        if (mask & unsupportInterruptMask)
            {
            TEST_ASSERT(AtSerdesControllerInterruptMaskSet(TestedSerdesController(), mask, mask) != cAtOk);
            }
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotApplyAnyInterruptMaskOnANULLSERDES()
    {
    uint8  bit_i;

    AtTestLoggerEnable(cAtFalse);
    for (bit_i = 0; bit_i < 32; bit_i++)
        {
        uint32 mask = 0x1 << bit_i;
        TEST_ASSERT(AtSerdesControllerInterruptMaskSet(NULL, mask, mask) != cAtOk);
        }
    AtTestLoggerEnable(cAtTrue);
    }

static void ListenerAlarmNotify(AtSerdesController self, void *listener, uint32 changedAlarms, uint32 currentStatus)
    {
    }

static void testCanAdd_RemoveListener()
    {
    const tAtSerdesControllerListener callback = {ListenerAlarmNotify};
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerListenerAdd(TestedSerdesController(), NULL, &callback));
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerListenerRemove(TestedSerdesController(), NULL, &callback));
    }

static void testCannotAddRemoveListenerOnNullSerdes()
    {
    const tAtSerdesControllerListener callback = {ListenerAlarmNotify};
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesControllerListenerAdd(NULL, NULL, &callback) != cAtOk);
    TEST_ASSERT(AtSerdesControllerListenerRemove(NULL, NULL, &callback) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testSERDESPRBSEngineCanBeAccessed()
    {
    AtTestLoggerEnable(cAtFalse);
    AtSerdesControllerPrbsEngine(TestedSerdesController());
    AtSerdesControllerPrbsEngine(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNULLSERDESMustHaveNULLPRBSEngine ()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtSerdesControllerPrbsEngine(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testMDIOCanBeAccessed()
    {
    AtTestLoggerEnable(cAtFalse);
    AtSerdesControllerMdio(TestedSerdesController());
    AtSerdesControllerMdio(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNULLSERDESMustHaveNULLMDIO()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtSerdesControllerMdio(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testDRPCanBeAccessed()
    {
    AtTestLoggerEnable(cAtFalse);
    AtSerdesControllerDrp(TestedSerdesController());
    AtSerdesControllerDrp(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNULLSERDESMustHaveNULLDRP()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_NULL(AtSerdesControllerDrp(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotControlEqualizerModeOnANULLSERDES()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerEqualizerCanControl(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testIfEqualizerModeCannotBeControlled_NoneOfApplicableEqualizerModeIsSupported()
    {
    uint8 idx;

    if (AtSerdesControllerEqualizerCanControl(TestedSerdesController()) == cAtFalse)
        {
        for (idx = 0; idx < mCount(cSerdesEqualizeMode); idx++)
            {
            TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerEqualizerModeIsSupported(TestedSerdesController(), cSerdesEqualizeMode[idx]));
            }
        }
    }

static void testNoneOfApplicableEqualizerModeCanBeSupportedOnANULLSERDES()
    {
    uint8 idx;

    for (idx = 0; idx < mCount(cSerdesEqualizeMode); idx++)
        {
        TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerEqualizerModeIsSupported(NULL, cSerdesEqualizeMode[idx]));
        }
    }

static void testForAnySupportedEqualizerMode_ItMustBeAppliedSuccesfully()
    {
    uint8 idx;

    for (idx = 0; idx < mCount(cSerdesEqualizeMode); idx++)
        {
        if (AtSerdesControllerEqualizerModeIsSupported(TestedSerdesController(), cSerdesEqualizeMode[idx]))
            {
            TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerEqualizerModeSet(TestedSerdesController(), cSerdesEqualizeMode[idx]));
            TEST_ASSERT_EQUAL_INT(cSerdesEqualizeMode[idx], AtSerdesControllerEqualizerModeGet(TestedSerdesController()));
            }
        else
            {
            AtTestLoggerEnable(cAtFalse);
            TEST_ASSERT(AtSerdesControllerEqualizerModeSet(TestedSerdesController(), cSerdesEqualizeMode[idx]) != cAtOk);
            AtTestLoggerEnable(cAtTrue);
            }
        }
    }

static void testPowerControlAbilityEnforce()
    {
    AtSerdesControllerTestRunner self = (AtSerdesControllerTestRunner)AtUnittestRunnerCurrentRunner();
    eBool canControl = AtSerdesManagerTestRunnerSerdesPowerCanBeControlled(ManagerRunner(self), self);
    TEST_ASSERT_EQUAL_INT(canControl, AtSerdesControllerPowerCanControl(TestedSerdesController()));
    }

static void testIfPowerCanBeControlled_CanPowerDown_UpSERDESProperly()
    {
    if (!AtSerdesControllerPowerCanControl(TestedSerdesController()))
        return;

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerPowerDown(TestedSerdesController(), cAtTrue));
    TEST_ASSERT_EQUAL_INT(cAtTrue, AtSerdesControllerPowerIsDown(TestedSerdesController()));

    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerPowerDown(TestedSerdesController(), cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerPowerIsDown(TestedSerdesController()));
    }

static void testPowerCannotBeControlledOnANULLSERDES()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerPowerCanControl(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testIfPowerCannotBeControlled_AttemptingPowerDownWillHaveErrorReport()
    {
    if (AtSerdesControllerPowerCanControl(TestedSerdesController()))
        return;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesControllerPowerDown(TestedSerdesController(), cAtTrue) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testIfPowerCannotBeControlled_AttemptingPowerUpWillNotHaveErrorReport()
    {
    if (AtSerdesControllerPowerCanControl(TestedSerdesController()))
        return;

    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtOk, AtSerdesControllerPowerDown(TestedSerdesController(), cAtFalse));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCannotPowerUp_DownANULLSERDES()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesControllerPowerDown(NULL, cAtTrue)  != cAtOk);
    TEST_ASSERT(AtSerdesControllerPowerDown(NULL, cAtFalse) != cAtOk);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNULLSERDESWillAlwaysHaveDownStatus()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(cAtFalse, AtSerdesControllerPowerIsDown(NULL));
    AtTestLoggerEnable(cAtTrue);
    }

static void testCanShowSERDESDebugInformationRegardlessSERDESIsNULLOrNot()
    {
    AtTestLoggerEnable(cAtFalse);
    AtSerdesControllerDebug(TestedSerdesController());
    AtSerdesControllerDebug(NULL);
    AtTestLoggerEnable(cAtTrue);
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtSerdesControllerTestRunner_Fixtures)
        {
        new_TestFixture("testSerdesModeMustBeAlwaysValid", testSerdesModeMustBeAlwaysValid),
        new_TestFixture("testNullSerdesMustHaveInvalidMode", testNullSerdesMustHaveInvalidMode),
        new_TestFixture("testInitializeValidSerdes", testInitializeValidSerdes),
        new_TestFixture("testCannotInitializeNullSerdes", testCannotInitializeNullSerdes),
        new_TestFixture("testResetValidSerdes", testResetValidSerdes),
        new_TestFixture("testCannotResetNullSerdes", testCannotResetNullSerdes),
        new_TestFixture("testCanSerdesControllerPhysicalPortGet", testCanAccessPhysicalPort),
        new_TestFixture("testPhysicalPortOfNullSerdesMustNotExist", testPhysicalPortOfNullSerdesMustNotExist),
        new_TestFixture("testStatusCanBeRetrievedAtAnyTime_EvenWithNULLSERDES", testStatusCanBeRetrievedAtAnyTime_EvenWithNULLSERDES),
        new_TestFixture("testSupportedSerdesModeMustBeAppliedProperly", testSupportedSerdesModeMustBeAppliedProperly),
        new_TestFixture("testAtLeastOneSERDESModeMustBeSupported", testAtLeastOneSERDESModeMustBeSupported),
        new_TestFixture("testAnySerdesModeThatIsNotSupported_ErrorWillBeReturnedWhenApplying", testAnySerdesModeThatIsNotSupported_ErrorWillBeReturnedWhenApplying),
        new_TestFixture("testCannotApplyInvalidSERDESMode", testCannotApplyInvalidSERDESMode),
        new_TestFixture("testCannotApplyValidSERDESModeToANULLController", testCannotApplyValidSERDESModeToANULLController),
        new_TestFixture("testEnableDisableSerdes", testEnableDisableSerdes),
        new_TestFixture("testCanNotEnableDisableNullSerdes", testCanNotEnableDisableNullSerdes),
        new_TestFixture("testIfTimingModeCanBeControlled_ApplyAnyTimingModeMustBeFine", testIfTimingModeCanBeControlled_ApplyAnyTimingModeMustBeFine),
        new_TestFixture("testIfTimingModeCannotBeControlled_AccessTimingModeWouldHaveAutoModeBeReturned", testIfTimingModeCannotBeControlled_AccessTimingModeWouldHaveAutoModeBeReturned),
        new_TestFixture("testIfTimingModeCannotBeControlled_CannotApplyAnyTimingMode", testIfTimingModeCannotBeControlled_CannotApplyAnyTimingMode),
        new_TestFixture("testTimingModeCannotBeControlledOnANULLSERDES", testTimingModeCannotBeControlledOnANULLSERDES),
        new_TestFixture("testTimingModeMustBeUnknownOnANULLSERDES", testTimingModeMustBeUnknownOnANULLSERDES),
        new_TestFixture("testIfASERDESParameterIsSupported_CanChangeItsValueProperly", testIfASERDESParameterIsSupported_CanChangeItsValueProperly),
        new_TestFixture("testIfASERDESParameterIsNotSupported_CannotApplyAnyValue", testIfASERDESParameterIsNotSupported_CannotApplyAnyValue),
        new_TestFixture("testCannotApplyAnyPrameterValueOnANULLSERDES", testCannotApplyAnyPrameterValueOnANULLSERDES),
        new_TestFixture("testIfALoopbackCanBeMade_ItCanBeAppliedWithNoError", testIfALoopbackCanBeMade_ItCanBeAppliedWithNoError),
        new_TestFixture("testSERDESStatusCanBeRetrieved_EvenWithNULLController", testSERDESStatusCanBeRetrieved_EvenWithNULLController),
        new_TestFixture("testForAnySupportedInterruptMask_CanApplyItProperly", testForAnySupportedInterruptMask_CanApplyItProperly),
        new_TestFixture("testForAnyNotSupportedInterruptMask_ErrorMustBeReportedWhenApplying", testForAnyNotSupportedInterruptMask_ErrorMustBeReportedWhenApplying),
        new_TestFixture("testCannotApplyAnyInterruptMaskOnANULLSERDES", testCannotApplyAnyInterruptMaskOnANULLSERDES),
        new_TestFixture("testCanAdd_RemoveListener", testCanAdd_RemoveListener),
        new_TestFixture("testCannotAddRemoveListenerOnNullSerdes", testCannotAddRemoveListenerOnNullSerdes),
        new_TestFixture("testSERDESPRBSEngineCanBeAccessed", testSERDESPRBSEngineCanBeAccessed),
        new_TestFixture("testNULLSERDESMustHaveNULLPRBSEngine", testNULLSERDESMustHaveNULLPRBSEngine),
        new_TestFixture("testMDIOCanBeAccessed", testMDIOCanBeAccessed),
        new_TestFixture("testNULLSERDESMustHaveNULLMDIO", testNULLSERDESMustHaveNULLMDIO),
        new_TestFixture("testDRPCanBeAccessed", testDRPCanBeAccessed),
        new_TestFixture("testNULLSERDESMustHaveNULLDRP", testNULLSERDESMustHaveNULLDRP),
        new_TestFixture("testCannotControlEqualizerModeOnANULLSERDES", testCannotControlEqualizerModeOnANULLSERDES),
        new_TestFixture("testIfEqualizerModeCannotBeControlled_NoneOfApplicableEqualizerModeIsSupported", testIfEqualizerModeCannotBeControlled_NoneOfApplicableEqualizerModeIsSupported),
        new_TestFixture("testNoneOfApplicableEqualizerModeCanBeSupportedOnANULLSERDES", testNoneOfApplicableEqualizerModeCanBeSupportedOnANULLSERDES),
        new_TestFixture("testForAnySupportedEqualizerMode_ItMustBeAppliedSuccesfully", testForAnySupportedEqualizerMode_ItMustBeAppliedSuccesfully),
        new_TestFixture("testPowerControlAbilityEnforce", testPowerControlAbilityEnforce),
        new_TestFixture("testIfPowerCanBeControlled_CanPowerDown_UpSERDESProperly", testIfPowerCanBeControlled_CanPowerDown_UpSERDESProperly),
        new_TestFixture("testPowerCannotBeControlledOnANULLSERDES", testPowerCannotBeControlledOnANULLSERDES),
        new_TestFixture("testIfPowerCannotBeControlled_AttemptingPowerDownWillHaveErrorReport", testIfPowerCannotBeControlled_AttemptingPowerDownWillHaveErrorReport),
        new_TestFixture("testIfPowerCannotBeControlled_AttemptingPowerUpWillNotHaveErrorReport", testIfPowerCannotBeControlled_AttemptingPowerUpWillNotHaveErrorReport),
        new_TestFixture("testCannotPowerUp_DownANULLSERDES", testCannotPowerUp_DownANULLSERDES),
        new_TestFixture("testNULLSERDESWillAlwaysHaveDownStatus", testNULLSERDESWillAlwaysHaveDownStatus),
        new_TestFixture("testCanShowSERDESDebugInformationRegardlessSERDESIsNULLOrNot", testCanShowSERDESDebugInformationRegardlessSERDESIsNULLOrNot)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtSerdesControllerTestRunner_Caller, "TestSuite_AtSerdesControllerTestRunner", NULL, NULL, TestSuite_AtSerdesControllerTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_AtSerdesControllerTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "SERDES controller %s", AtObjectToString((AtObject)mSerdesController(self)));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtSerdesControllerTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtSerdesControllerTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSerdesControllerTestRunner);
    }

AtSerdesControllerTestRunner AtSerdesControllerTestRunnerObjectInit(AtSerdesControllerTestRunner self, AtSerdesController serdes, AtUnittestRunner managerRunner)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)serdes) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->managerRunner = managerRunner;

    return self;
    }

AtSerdesControllerTestRunner AtSerdesControllerTestRunnerNew(AtSerdesController controller, AtUnittestRunner managerRunner)
    {
    AtSerdesControllerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSerdesControllerTestRunnerObjectInit(newRunner, controller, managerRunner);
    }

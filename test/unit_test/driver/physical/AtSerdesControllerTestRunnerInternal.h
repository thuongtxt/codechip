/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSerdesControllerTestRunnerInternal.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : SERDES controller test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSERDESCONTROLLERTESTRUNNERINTERNAL_H_
#define _ATSERDESCONTROLLERTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtObjectTestRunnerInternal.h"
#include "AtPhysicalTests.h"
#include "AtSerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSerdesControllerTestRunner
    {
    tAtObjectTestRunner super;

    /* Private data */
    AtUnittestRunner managerRunner;
    }tAtSerdesControllerTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesControllerTestRunner AtSerdesControllerTestRunnerObjectInit(AtSerdesControllerTestRunner self, AtSerdesController serdes, AtUnittestRunner managerRunner);

#ifdef __cplusplus
}
#endif
#endif /* _ATSERDESCONTROLLERTESTRUNNERINTERNAL_H_ */


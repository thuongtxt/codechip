/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtSerdesManagerTestRunner.c
 *
 * Created Date: Jul 13, 2016
 *
 * Description : SERDES manager test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesManagerTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSerdesManager(self) (AtSerdesManager)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSerdesManagerTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesManager TestedManager(void)
    {
    return mSerdesManager((AtObjectTestRunner)AtUnittestRunnerCurrentRunner());
    }

static void testNumberOfSERDESControllersMustBeGreaterThan0()
    {
    TEST_ASSERT(AtSerdesManagerNumSerdesControllers(TestedManager()) > 0);
    }

static void testNumberOfSERDESControllersOfANULLManagerMustBe0()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT(AtSerdesManagerNumSerdesControllers(NULL) == 0);
    AtTestLoggerEnable(cAtTrue);
    }

static void testNumberOfSERDESControllersMustBeEnough()
    {
    AtSerdesManagerTestRunner self = (AtSerdesManagerTestRunner)AtUnittestRunnerCurrentRunner();
    AtSerdesManager manager = TestedManager();
    TEST_ASSERT_EQUAL_INT(mMethodsGet(self)->NumSerdesControllers(self), AtSerdesManagerNumSerdesControllers(manager));
    }

static void testCanAccessAnySERDESControllersWhichHaveValidID()
    {
    uint32 serdesId;

    for (serdesId = 0; serdesId < AtSerdesManagerNumSerdesControllers(TestedManager()); serdesId++)
        {
        TEST_ASSERT(AtSerdesManagerSerdesControllerGet(TestedManager(), serdesId));
        }
    }

static void testSERDESControllerIDMustMatchWithIDUsedToGetItsInstance()
    {
    uint32 serdesMaxNum = AtSerdesManagerNumSerdesControllers(TestedManager());
    uint32 serdesId;

    for (serdesId = 0; serdesId < serdesMaxNum; serdesId++)
        {
        AtSerdesController serdesControl = AtSerdesManagerSerdesControllerGet(TestedManager(), serdesId);
        TEST_ASSERT_NOT_NULL(serdesControl);
        TEST_ASSERT_EQUAL_INT(AtSerdesControllerIdGet(serdesControl), serdesId);
        }
    }

static void testCannotAccessAnyControllersWhichHaveInvalidID()
    {
    uint32 serdesMaxNum = AtSerdesManagerNumSerdesControllers(TestedManager());
    TEST_ASSERT_NULL(AtSerdesManagerSerdesControllerGet(TestedManager(), serdesMaxNum));
    }

static void testCannotAccessAnyControllersWhichANULLManager()
    {
    AtTestLoggerEnable(cAtFalse);
    TEST_ASSERT_EQUAL_INT(0, AtSerdesManagerNumSerdesControllers(NULL));
    TEST_ASSERT_NULL(AtSerdesManagerSerdesControllerGet(NULL, 0));
    AtTestLoggerEnable(cAtTrue);
    }

static void testAssociatedDeviceOfAManagerMustNotBeNULL()
    {
    TEST_ASSERT_NOT_NULL(AtSerdesManagerDeviceGet(TestedManager()));
    }

static void testAssociatedDeviceOfANULLManagerMustBeNULL()
    {
    TEST_ASSERT_NULL(AtSerdesManagerDeviceGet(NULL));
    }

static TestRef UnitTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_AtSerdesManagerTestRunner_Fixtures)
        {
        new_TestFixture("testNumberOfSERDESControllersMustBeGreaterThan0", testNumberOfSERDESControllersMustBeGreaterThan0),
        new_TestFixture("testNumberOfSERDESControllersOfANULLManagerMustBe0", testNumberOfSERDESControllersOfANULLManagerMustBe0),
        new_TestFixture("testNumberOfSERDESControllersMustBeEnough", testNumberOfSERDESControllersMustBeEnough),
        new_TestFixture("testCanAccessAnySERDESControllersWhichHaveValidID", testCanAccessAnySERDESControllersWhichHaveValidID),
        new_TestFixture("testSERDESControllerIDMustMatchWithIDUsedToGetItsInstance", testSERDESControllerIDMustMatchWithIDUsedToGetItsInstance),
        new_TestFixture("testCannotAccessAnyControllersWhichHaveInvalidID", testCannotAccessAnyControllersWhichHaveInvalidID),
        new_TestFixture("testCannotAccessAnyControllersWhichANULLManager", testCannotAccessAnyControllersWhichANULLManager),
        new_TestFixture("testAssociatedDeviceOfAManagerMustNotBeNULL", testAssociatedDeviceOfAManagerMustNotBeNULL),
        new_TestFixture("testAssociatedDeviceOfANULLManagerMustBeNUL", testAssociatedDeviceOfANULLManagerMustBeNULL)
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_AtSerdesManagerTestRunner_Caller, "TestSuite_AtSerdesManagerTestRunner", NULL, NULL, TestSuite_AtSerdesManagerTestRunner_Fixtures);

    return (TestRef)((void *)&TestSuite_AtSerdesManagerTestRunner_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);
    AtListObjectAdd(suites, (AtObject)UnitTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "SERDES manager %s", AtObjectToString((AtObject)mSerdesManager(self)));
    return buffer;
    }

static void TestAllSerdesControlers(AtSerdesManagerTestRunner self)
    {
    uint32 numSerdes = AtSerdesManagerNumSerdesControllers(mSerdesManager(self));
    uint32 serdes_i;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtUnittestRunner runner;
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(mSerdesManager(self), serdes_i);

        if (!AtSerdesControllerIsControllable(serdes))
            continue;

        runner = (AtUnittestRunner)mMethodsGet(self)->ControllerTestRunnerCreate(self, serdes);
        AtUnittestRunnerRun(runner);
        AtUnittestRunnerDelete(runner);
        }
    }

static void Run(AtUnittestRunner self)
    {
    TestAllSerdesControlers((AtSerdesManagerTestRunner)self);

    /* Super test must be fine */
    m_AtUnittestRunnerMethods->Run(self);
    }

static AtSerdesControllerTestRunner ControllerTestRunnerCreate(AtSerdesManagerTestRunner self, AtSerdesController serdes)
    {
    return AtSerdesControllerTestRunnerNew(serdes, (AtUnittestRunner)self);
    }

static eBool SerdesPowerCanBeControlled(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner)
    {
    /* Just some products have this */
    return cAtFalse;
    }

static uint8 NumSerdesControllers(AtSerdesManagerTestRunner self)
    {
    /* Concrete test runner should override this method to enforce number of SERDES controllers */
    AtSerdesManager manager = (AtSerdesManager)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    return AtSerdesManagerNumSerdesControllers(manager);
    }

static void MethodsInit(AtSerdesManagerTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ControllerTestRunnerCreate);
        mMethodOverride(m_methods, SerdesPowerCanBeControlled);
        mMethodOverride(m_methods, NumSerdesControllers);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtSerdesManagerTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)mMethodsGet(runner), sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtSerdesManagerTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSerdesManagerTestRunner);
    }

AtSerdesManagerTestRunner AtSerdesManagerTestRunnerObjectInit(AtSerdesManagerTestRunner self, AtSerdesManager manager)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManagerTestRunner AtSerdesManagerTestRunnerNew(AtSerdesManager manager)
    {
    AtSerdesManagerTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSerdesManagerTestRunnerObjectInit(newRunner, manager);
    }

eBool AtSerdesManagerTestRunnerSerdesModeMustBeSupported(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode)
    {
    if (self)
        return mMethodsGet(self)->SerdesModeMustBeSupported(self, serdesRunner, mode);
    return cAtFalse;
    }

eBool AtSerdesManagerTestRunnerSerdesPowerCanBeControlled(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner)
    {
    if (self)
        return mMethodsGet(self)->SerdesPowerCanBeControlled(self, serdesRunner);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtPhysicalTests.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : Common public declarations for physical testing
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPHYSICALTESTS_H_
#define _ATPHYSICALTESTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../runner/AtUnittestRunner.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSerdesManagerTestRunner    * AtSerdesManagerTestRunner;
typedef struct tAtSerdesControllerTestRunner * AtSerdesControllerTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManagerTestRunner AtSerdesManagerTestRunnerNew(AtSerdesManager manager);
AtSerdesControllerTestRunner AtSerdesControllerTestRunnerNew(AtSerdesController controller, AtUnittestRunner managerRunner);

eBool AtSerdesManagerTestRunnerSerdesModeMustBeSupported(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner, eAtSerdesMode mode);
eBool AtSerdesManagerTestRunnerSerdesPowerCanBeControlled(AtSerdesManagerTestRunner self, AtSerdesControllerTestRunner serdesRunner);

#ifdef __cplusplus
}
#endif
#endif /* _ATPHYSICALTESTS_H_ */


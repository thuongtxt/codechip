/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtPdhVcDe1InterruptTestRunner.c
 *
 * Created Date: Jan 12, 2016
 *
 * Description : interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhVcDe1InterruptTestRunner
    {
    tAtPdhDe1InterruptTestRunner super;
    }tAtPdhVcDe1InterruptTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelInterruptTestRunnerMethods m_AtChannelInterruptTestRunnerOverride;

/* Save super implementation */
static const tAtChannelInterruptTestRunnerMethods * m_AtChannelInterruptTestRunnerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel ParentChannel(AtChannelInterruptTestRunner self)
    {
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    AtUnused(self);
    return (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)AtPdhChannelVcGet((AtPdhChannel)channel));
    }

static uint32 ParentCriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis;
    }

static eBool IsTopChannel(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    if ((channelType == cAtSdhChannelTypeAu3)    ||
        (channelType == cAtSdhChannelTypeAu4)    ||
        (channelType == cAtSdhChannelTypeAu4_16c)||
        (channelType == cAtSdhChannelTypeAu4_4c) ||
        (channelType == cAtSdhChannelTypeAu4_nc))
        return cAtTrue;

    return cAtFalse;
    }

static AtSdhChannel FindTopChannel(AtSdhChannel channel)
    {
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);

    if (parentChannel == NULL)
        return NULL;

    return IsTopChannel(parentChannel) ? parentChannel : FindTopChannel(parentChannel);
    }

static AtChannel TopChannel(AtChannelInterruptTestRunner self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtChannelInterruptTestRunnerTestedChannel(self);
    return (AtChannel)FindTopChannel((AtSdhChannel)AtPdhChannelVcGet(channel));
    }

static uint32 TopChannelCriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    return cAtSdhPathAlarmAis;
    }

static void OverrideAtChannelInterruptTestRunner(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;

    if (!m_methodsInit)
        {
        m_AtChannelInterruptTestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtChannelInterruptTestRunnerOverride, (void *)runner->methods, sizeof(m_AtChannelInterruptTestRunnerOverride));

        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentCriticalAlarmToForce);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentChannel);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, TopChannel);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, TopChannelCriticalAlarmToForce);
        }

    mMethodsSet(runner, &m_AtChannelInterruptTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtChannelInterruptTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhVcDe1InterruptTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhDe1InterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtPdhVcDe1InterruptTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, channel);
    }

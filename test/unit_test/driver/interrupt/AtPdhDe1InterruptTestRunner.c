/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtPdhDe1InterruptTestRunner.c
 *
 * Created Date: Jan 12, 2016
 *
 * Description : Interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhDe1.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtPdhDe1AlarmTypeStr[] =
    {
     "los",
     "lof",
     "ais",
     "rai"
    };

static const uint32 cAtPdhDe1AlarmTypeVal[] =
    {
     cAtPdhDe1AlarmLos,
     cAtPdhDe1AlarmLof,
     cAtPdhDe1AlarmAis,
     cAtPdhDe1AlarmRai
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelInterruptTestRunnerMethods m_AtChannelInterruptTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannelInterruptTestRunner CurrentRunner(void)
    {
    return (AtChannelInterruptTestRunner)AtUnittestRunnerCurrentRunner();
    }

static uint32 Alarms(AtChannelInterruptTestRunner self)
    {
    uint32 alarms = cAtPdhDe1AlarmAis;
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);

    AtUnused(self);

    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return alarms;

    return alarms | cAtPdhDe1AlarmLof | cAtPdhDe1AlarmRai;
    }

static eAtRet ForceAlarm(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable)
    {
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);

    if ((alarm == cAtPdhDe1AlarmAis) || (alarm == cAtPdhDe1AlarmRai))
        return (enable) ? AtChannelTxAlarmForce(channel, alarm) :
                          AtChannelTxAlarmUnForce(channel, alarm);


    return (enable) ? AtChannelRxAlarmForce(channel, alarm) : AtChannelRxAlarmUnForce(channel, alarm);
    }

void testForceAlarmCanTerminateOtherAlarms(AtChannelInterruptTestRunner self, uint32 alarm, uint32 otherAlarm)
    {
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    AtDevice device = AtChannelDeviceGet(channel);

    /* Force alarm, expect alarm raised */
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, otherAlarm, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, otherAlarm, otherAlarm);
    TEST_ASSERT(self->testedChannelAlarms & otherAlarm);

    /* Force critical alarm, expect critical alarm raised */
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, alarm, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, alarm, alarm);
    TEST_ASSERT((self->testedChannelAlarms & alarm) != 0);

    /* Unforce alarm, expect nothing change due to still forcing critical alarm */
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, otherAlarm, cAtFalse));
    AtChannelInterruptTestRunnerInterruptProcess(device);
    TEST_ASSERT((self->testedChannelAlarms & alarm) != 0);

    /* Unforce critical alarm, all are cleared now */
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, alarm, cAtFalse));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cBit31_0, 0);
    TEST_ASSERT(self->testedChannelAlarms == 0);
    }

static void testLosCanTerminateAllOtherAlarms(void)
    {
    uint32 bit_i;
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    if (forcableAlarm == 0)
        return;

    if ((forcableAlarm & cAtPdhDe1AlarmLos) == 0)
        return;

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 alarm = cBit0 << bit_i;
        if ((alarm & forcableAlarm) == 0)
            continue;

        if (alarm == cAtPdhDe1AlarmLos)
            continue;

        testForceAlarmCanTerminateOtherAlarms(runner, cAtPdhDe1AlarmLos, alarm);
        }
    }

static void testAisCanTerminateAllOtherAlarmsExceptLos(void)
    {
    uint32 bit_i;
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    if (forcableAlarm == 0)
        return;

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 alarm = cBit0 << bit_i;
        if ((alarm & forcableAlarm) == 0)
            continue;

        if ((alarm == cAtPdhDe1AlarmLos) || (alarm == cAtPdhDe1AlarmAis))
            continue;

        testForceAlarmCanTerminateOtherAlarms(runner, cAtPdhDe1AlarmAis, alarm);
        }
    }

static TestRef PreemptionTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Preemption_AtChannelInterrupt_Fixtures)
        {
        new_TestFixture("testLosCanTerminateAllOtherAlarms", testLosCanTerminateAllOtherAlarms),
        new_TestFixture("testAisCanTerminateAllOtherAlarmsExceptLos", testAisCanTerminateAllOtherAlarmsExceptLos),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Preemption_AtChannelInterrupt_Caller, "TestSuite_Preemption_AtChannelInterrupt", AtChannelInterruptTestRunnerSetup, AtChannelInterruptTestRunnerTearDown, TestSuite_Preemption_AtChannelInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_Preemption_AtChannelInterrupt_Caller);
    }

static AtObject PreemptionTestSuiteGet(AtChannelInterruptTestRunner self)
    {
    return (AtObject)PreemptionTestSuite((AtUnittestRunner)self);
    }

static const char * AlarmsToString(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return CliAlarmMaskToString(cAtPdhDe1AlarmTypeStr, cAtPdhDe1AlarmTypeVal, mCount(cAtPdhDe1AlarmTypeVal), alarms);
    }

static uint32 CriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    return cAtPdhDe1AlarmAis;
    }

static void OverrideAtChannelInterruptTestRunner(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelInterruptTestRunnerOverride, (void *)runner->methods, sizeof(m_AtChannelInterruptTestRunnerOverride));

        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, Alarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceAlarm);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, PreemptionTestSuiteGet);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, AlarmsToString);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, CriticalAlarmToForce);
        }

    mMethodsSet(runner, &m_AtChannelInterruptTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtChannelInterruptTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe1InterruptTestRunner);
    }

AtUnittestRunner AtPdhDe1InterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelInterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

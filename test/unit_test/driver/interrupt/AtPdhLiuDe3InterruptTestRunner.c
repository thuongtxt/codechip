/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtPdhLiuDe3InterruptTestRunner.c
 *
 * Created Date: Jan 12, 2016
 *
 * Description : Interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhLiuDe3InterruptTestRunner
    {
    tAtPdhDe3InterruptTestRunner super;
    }tAtPdhLiuDe3InterruptTestRunner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Override(AtUnittestRunner self)
    {
    AtUnused(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhLiuDe3InterruptTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhDe3InterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtPdhLiuDe3InterruptTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, channel);
    }

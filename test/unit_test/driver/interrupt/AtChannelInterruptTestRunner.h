/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Interrupt
 * 
 * File        : AtChannelInterruptTestRunner.h
 * 
 * Created Date: Jan 15, 2016
 *
 * Description : Channel abstract interrupt test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELINTERRUPTTESTRUNNER_H_
#define _ATCHANNELINTERRUPTTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelInterruptTestRunner * AtChannelInterruptTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concretes */
AtUnittestRunner AtPdhLiuDe1InterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtPdhDe3De1InterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtPdhVcDe1InterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtPdhLiuDe3InterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtPdhVcDe3InterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtSdhVcInterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtSdhPointerProcessorInterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtSdhLineInterruptTestRunnerNew(AtChannel channel);
AtUnittestRunner AtPwInterruptTestRunnerNew(AtChannel channel);

/* APIs */
AtChannel AtChannelInterruptTestRunnerTestedChannel(AtChannelInterruptTestRunner self);
void AtChannelInterruptTestRunnerSetup(void);
void AtChannelInterruptTestRunnerTearDown(void);
void AtChannelInterruptTestRunnerInterruptProcess(AtDevice device);
uint32 AtChannelInterruptTestRunnerInterruptProcessWithResult(AtDevice device, uint32 *alarm, uint32 mask, uint32 expectedValue);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELINTERRUPTTESTRUNNER_H_ */


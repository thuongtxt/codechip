/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Interrupt
 * 
 * File        : AtChannelInterruptTestRunnerInternal.h
 * 
 * Created Date: Jan 11, 2016
 *
 * Description : Channel abstract interrupt test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELINTERRUPTTESTRUNNERINTERNAL_H_
#define _ATCHANNELINTERRUPTTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "../runner/AtUnittestRunnerInternal.h"
#include "AtChannelInterruptTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumBitsInDword 32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelInterruptTestRunnerMethods
    {
    uint32 (*Alarms)(AtChannelInterruptTestRunner self);
    uint32 (*ForceableAlarms)(AtChannelInterruptTestRunner self);
    eAtRet (*ForceAlarm)(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable);
    uint32 (*ForcedAlarmGet)(AtChannelInterruptTestRunner self);

   AtObject (*PreemptionTestSuiteGet)(AtChannelInterruptTestRunner self);
   uint32 (*ParentCriticalAlarmToForce)(AtChannelInterruptTestRunner self);
   uint32 (*CriticalAlarmToForce)(AtChannelInterruptTestRunner self);
   uint32 (*TopChannelCriticalAlarmToForce)(AtChannelInterruptTestRunner self);
   const char * (*AlarmsToString)(AtChannelInterruptTestRunner self, uint32 alarms);
   void * (*ParentChannelAlarmChangeStateFunction)(AtChannelInterruptTestRunner self);
   AtChannel (*ParentChannel)(AtChannelInterruptTestRunner self);
   AtChannel (*TopChannel)(AtChannelInterruptTestRunner);
    }tAtChannelInterruptTestRunnerMethods;

typedef struct tAtChannelInterruptTestRunner
    {
    tAtUnittestRunner super;
    const tAtChannelInterruptTestRunnerMethods *methods;

    /* Private data */
    AtChannel testedChannel;
    AtChannel parentChannel;
    AtLogger defaultLogger;
    uint32 testedChannelAlarms;
    uint32 parentChannelAlarms;
    }tAtChannelInterruptTestRunner;

typedef struct tAtPdhDe1InterruptTestRunner
    {
    tAtChannelInterruptTestRunner super;
    }tAtPdhDe1InterruptTestRunner;

typedef struct tAtPdhDe3InterruptTestRunner
    {
    tAtChannelInterruptTestRunner super;
    }tAtPdhDe3InterruptTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUnittestRunner AtChannelInterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel);
AtUnittestRunner AtPdhDe1InterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel);
AtUnittestRunner AtPdhDe3InterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel);
void testForceAlarmCanTerminateOtherAlarms(AtChannelInterruptTestRunner self, uint32 alarm, uint32 otherAlarm);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELINTERRUPTTESTRUNNERINTERNAL_H_ */


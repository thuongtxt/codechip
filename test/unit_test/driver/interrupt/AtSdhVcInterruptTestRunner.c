/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtSdhVcInterruptTestRunner.c
 *
 * Created Date: Jan 12, 2016
 *
 * Description : Interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtSdhVc.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSdhVcInterruptTestRunner
    {
    tAtChannelInterruptTestRunner super;
    }tAtSdhVcInterruptTestRunner;

static const char *cCmdSdhPathAlarmStr[] =
    {
     "tim",
     "uneq",
     "plm"
    };

static const uint32 cCmdSdhPathAlarmValue[]=
    {
     cAtSdhPathAlarmTim,
     cAtSdhPathAlarmUneq,
     cAtSdhPathAlarmPlm
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelInterruptTestRunnerMethods m_AtChannelInterruptTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannelInterruptTestRunner CurrentRunner(void)
    {
    return (AtChannelInterruptTestRunner)AtUnittestRunnerCurrentRunner();
    }

static uint32 Alarms(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmTim | cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm;
    }

static void JxSetUp(AtSdhChannel sdhChannel)
    {
    tAtSdhTti tti;
    const uint8 * ttiMessage = (const uint8 *)"Arrive";
    AtSdhTtiMake(cAtSdhTtiMode16Byte, ttiMessage, (uint8)AtStrlen((char*)ttiMessage), &tti);
    AtSdhChannelTxTtiSet(sdhChannel, &tti);
    }

static eAtRet ForceAlarm(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable)
    {
    uint8 psl;
    tAtSdhTti tti;
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    AtSdhPath path = (AtSdhPath)channel;
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;

    if (alarm == cAtSdhPathAlarmUneq)
        {
        psl = AtSdhPathExpectedPslGet(path);
        if (enable)
            AtSdhPathTxPslSet(path, 0);
        else
            AtSdhPathTxPslSet(path, psl);

        return cAtOk;
        }

    if (alarm == cAtSdhPathAlarmPlm)
        {
        psl = AtSdhPathExpectedPslGet(path);
        if (enable)
            AtSdhPathTxPslSet(path, psl + 3);
        else
            AtSdhPathTxPslSet(path, psl);
        return cAtOk;
        }

    if (alarm != cAtSdhPathAlarmTim)
        return cAtErrorModeNotSupport;

    JxSetUp(sdhChannel);
    AtSdhChannelTxTtiGet(sdhChannel, &tti);
    if (enable)
        {
        tti.message[2] = tti.message[2] + 1;
        AtSdhChannelExpectedTtiSet(sdhChannel, &tti);
        }
    else
        AtSdhChannelExpectedTtiSet(sdhChannel, &tti);

    return cAtOk;
    }

static uint32 ForceableAlarms(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmTim | cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm;
    }

static void testUneqCanTerminatePlm(void)
    {
    AtChannelInterruptTestRunner self = CurrentRunner();
    AtDevice device = AtChannelDeviceGet(AtChannelInterruptTestRunnerTestedChannel(self));

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmPlm, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cAtSdhPathAlarmPlm, cAtSdhPathAlarmPlm);
    TEST_ASSERT(self->testedChannelAlarms & cAtSdhPathAlarmPlm);

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmUneq, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cAtSdhPathAlarmUneq, cAtSdhPathAlarmUneq);
    TEST_ASSERT(self->testedChannelAlarms & cAtSdhPathAlarmUneq);
    TEST_ASSERT((self->testedChannelAlarms & cAtSdhPathAlarmPlm) == 0);

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmPlm, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmUneq, cAtFalse));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cBit31_0, 0);
    }

static void testPlmCanTerminateUneq(void)
    {
    AtChannelInterruptTestRunner self = CurrentRunner();
    AtDevice device = AtChannelDeviceGet(AtChannelInterruptTestRunnerTestedChannel(self));

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmUneq, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cAtSdhPathAlarmUneq, cAtSdhPathAlarmUneq);
    TEST_ASSERT(self->testedChannelAlarms & cAtSdhPathAlarmUneq);

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmPlm, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cAtSdhPathAlarmPlm, cAtSdhPathAlarmPlm);
    TEST_ASSERT(self->testedChannelAlarms & cAtSdhPathAlarmPlm);
    TEST_ASSERT((self->testedChannelAlarms & cAtSdhPathAlarmUneq) == 0);

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmPlm, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmUneq, cAtFalse));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cBit31_0, 0);
    }

static TestRef PreemptionTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Preemption_AtChannelInterrupt_Fixtures)
        {
        new_TestFixture("testUneqCanTerminatePlm", testUneqCanTerminatePlm),
        new_TestFixture("testPlmCanTerminateUneq", testPlmCanTerminateUneq),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Preemption_AtChannelInterrupt_Caller, "TestSuite_Preemption_AtChannelInterrupt", AtChannelInterruptTestRunnerSetup, AtChannelInterruptTestRunnerTearDown, TestSuite_Preemption_AtChannelInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_Preemption_AtChannelInterrupt_Caller);
    }

static AtObject PreemptionTestSuiteGet(AtChannelInterruptTestRunner self)
    {
    return (AtObject)PreemptionTestSuite((AtUnittestRunner)self);
    }

static AtChannel ParentChannel(AtChannelInterruptTestRunner self)
    {
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    return (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)channel);
    }

static uint32 ParentCriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis;
    }

static const char * AlarmsToString(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return CliAlarmMaskToString(cCmdSdhPathAlarmStr, cCmdSdhPathAlarmValue, mCount(cCmdSdhPathAlarmValue), alarms);
    }

/* TODO: temporarily work around to move some alarms at AU/TU to VC */
static void ParentChannelAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelInterruptTestRunner self = (AtChannelInterruptTestRunner)userData;
    uint32 vcChangedAlarms = changedAlarms & (~(cAtSdhPathAlarmAis | cAtSdhPathAlarmLop));
    uint32 vcCurrentStatus = currentStatus & (~(cAtSdhPathAlarmAis | cAtSdhPathAlarmLop));
    uint32 auChangedAlarms = changedAlarms & (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop);
    uint32 auCurrentStatus = currentStatus & (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop);
    self->testedChannelAlarms = ((self->testedChannelAlarms & (~vcChangedAlarms)) | vcCurrentStatus);
    self->parentChannelAlarms = ((self->parentChannelAlarms & (~auChangedAlarms)) | auCurrentStatus);
    }

static void * ParentChannelAlarmChangeStateFunction(AtChannelInterruptTestRunner self)
    {
    return ParentChannelAlarmChangeState;
    }

static void OverrideAtChannelInterruptTestRunner(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelInterruptTestRunnerOverride, (void *)runner->methods, sizeof(m_AtChannelInterruptTestRunnerOverride));

        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, Alarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceAlarm);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceableAlarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, PreemptionTestSuiteGet);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentChannel);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentCriticalAlarmToForce);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, AlarmsToString);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentChannelAlarmChangeStateFunction);
        }

    mMethodsSet(runner, &m_AtChannelInterruptTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtChannelInterruptTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhVcInterruptTestRunner);
    }

static AtUnittestRunner AtSdhVcInterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelInterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtSdhVcInterruptTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtSdhVcInterrupTestRunnerObjectInit(newRunner, channel);
    }

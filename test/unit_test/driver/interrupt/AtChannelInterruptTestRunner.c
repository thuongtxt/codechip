/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtChannelInterruptTestRunner.c
 *
 * Created Date: Jan 11, 2016
 *
 * Description : Channel abstract interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTests.h"
#include "AtChannel.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtChannelInterruptTestRunner *)self)
#define cDefaultTimeoutInMs 2000
#define cIdealTimeoutInMs   10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelInterruptTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

static tAtChannelEventListener intrListener;
static tAtChannelEventListener parentIntrListener;

static uint8 stringBuffer[128];

/*--------------------------- Forward declarations ---------------------------*/
static AtChannelInterruptTestRunner CurrentRunner(void)
    {
    return (AtChannelInterruptTestRunner)AtUnittestRunnerCurrentRunner();
    }

static AtChannel TestedChannel()
    {
    return AtChannelInterruptTestRunnerTestedChannel(CurrentRunner());
    }

static AtDevice Device(void)
    {
    return AtChannelDeviceGet(TestedChannel());
    }

static AtLogger DefautlLogger(AtChannelInterruptTestRunner self)
    {
    static const uint32 cMaxMessageLen  = 500;
    static const uint32 cMaxNumMessages = 1024;

    if (self->defaultLogger == NULL)
        {
        self->defaultLogger = AtDefaultLoggerNew(cMaxNumMessages, cMaxMessageLen);
        AtLoggerEnable(self->defaultLogger, cAtTrue);
        AtLoggerLevelEnable(self->defaultLogger, cAtLogLevelAll, cAtTrue);
        }

    return self->defaultLogger;
    }

static void Log(eAtLogLevel level, const char *format, ...)
    {
    va_list args;

    va_start(args, format);
    AtLoggerLog(DefautlLogger(CurrentRunner()), level, format, args);
    va_end(args);
    }

static AtChannel TestedParentChannel()
    {
    if (CurrentRunner()->parentChannel == NULL)
        CurrentRunner()->parentChannel = mMethodsGet(CurrentRunner())->ParentChannel(CurrentRunner());
    return CurrentRunner()->parentChannel;
    }

static uint32 Alarms(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet ForceAlarm(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable)
    {
    AtUnused(self);
    AtUnused(alarm);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static uint32 ForceableAlarms(AtChannelInterruptTestRunner self)
    {
    return mMethodsGet(self)->Alarms(self) &
           (AtChannelTxForcableAlarmsGet(TestedChannel()) | AtChannelRxForcableAlarmsGet(TestedChannel()));
    }

static uint32 ForcedAlarmGet(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return 0;
    }

static void testForceAlarmCanCauseNotification(void)
    {
    uint32 i;
    AtDevice device = Device();
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    if (forcableAlarm == 0)
        return;

    for (i = 0; i < cNumBitsInDword; i++)
        {
        uint32 elapseTime;
        uint32 alarm = cBit0 << i;
        if ((alarm & forcableAlarm) == 0)
            continue;

        /* Force alarm, expect alarm raised */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, alarm, cAtTrue));
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, alarm, alarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) Force %s, elapseTime: %dms\r\n", __FILE__, __LINE__, mMethodsGet(runner)->AlarmsToString(runner, alarm), elapseTime);
        TEST_ASSERT(alarm & runner->testedChannelAlarms);

        /* Un-force alarm, expect alarm cleared */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, alarm, cAtFalse));
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, alarm, 0);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) Un-Force %s, elapseTime: %dms\r\n", __FILE__, __LINE__, mMethodsGet(runner)->AlarmsToString(runner, alarm), elapseTime);
        TEST_ASSERT((runner->testedChannelAlarms & alarm) == 0);
        }
    }

static const char * AlarmsToString(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return NULL;
    }

static void ParentCriticalAlarmForce(AtChannelInterruptTestRunner self, eBool force)
    {
    uint32 alarmToForced = mMethodsGet(self)->ParentCriticalAlarmToForce(self);
    AtChannel parent = mMethodsGet(self)->ParentChannel(self);

    AtAssert(alarmToForced);

    if (force)
        AtChannelTxAlarmForce(parent, alarmToForced);
    else
        AtChannelTxAlarmUnForce(parent, alarmToForced);
    }

static eBool ParentHasCriticalAlarmRaised(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    uint32 alarmToForce = mMethodsGet(self)->ParentCriticalAlarmToForce(self);
    return (alarms & alarmToForce) ? cAtTrue : cAtFalse;
    }

static eBool ParentHasCriticalAlarmCleared(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    uint32 alarmToForce = mMethodsGet(self)->ParentCriticalAlarmToForce(self);
    return (alarms & alarmToForce) ? cAtFalse : cAtTrue;
    }

static void testUnforceAlarmWhileParentChannelHasCriticalAlarm(void)
    {
    uint32 bit_i;
    AtDevice device = Device();
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 elapseTime;
        uint32 parentForceAlarm;
        uint32 previousAlarms;
        uint32 forceAlarm = cBit0 << bit_i;

        if ((forceAlarm & forcableAlarm) == 0)
            continue;

        /* Force alarm, expect alarm raised */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, forceAlarm, cAtTrue));
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, forceAlarm, forceAlarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        previousAlarms = runner->testedChannelAlarms;
        TEST_ASSERT((forceAlarm & runner->testedChannelAlarms) == forceAlarm);

        /* Force critical alarm on parent channel. Expect:
         * - Parent must have critical alarm
         * - current channel may raise something, so need to log to check
         * whether it is correct */
        ParentCriticalAlarmForce(runner, cAtTrue);
        parentForceAlarm = mMethodsGet(runner)->ParentCriticalAlarmToForce(runner);
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->parentChannelAlarms, parentForceAlarm, parentForceAlarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        TEST_ASSERT(ParentHasCriticalAlarmRaised(runner, runner->parentChannelAlarms));

        if (runner->testedChannelAlarms != previousAlarms)
            {
            AtOsalMemCpy(stringBuffer, mMethodsGet(runner)->AlarmsToString(runner, previousAlarms), sizeof(stringBuffer));
            Log(cAtLogLevelInfo,
                "%s [force parent]: [before] %s [after] %s\r\n",
                (void*)AtObjectToString((AtObject)TestedChannel()),
                stringBuffer,
                mMethodsGet(runner)->AlarmsToString(runner, runner->testedChannelAlarms));
            }

        /* Update again due to it may change when unforce critical alarm at parent layer */
        previousAlarms = runner->testedChannelAlarms;

        /* Unforce current channel and make sure no alarm cleared due to
         * still forcing critical alarm on parent channel */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, forceAlarm, cAtFalse));
        AtChannelInterruptTestRunnerInterruptProcess(device);
        TEST_ASSERT(runner->testedChannelAlarms == previousAlarms);

        /* Unforce parent channel and make sure alarm of
         * both parent channel and tested channel cleared */
        ParentCriticalAlarmForce(runner, cAtFalse);
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, cBit31_0, 0);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        TEST_ASSERT(ParentHasCriticalAlarmCleared(runner, runner->parentChannelAlarms));
        TEST_ASSERT(runner->testedChannelAlarms == 0);
        }

    AtLoggerShow(DefautlLogger(runner));
    AtLoggerFlush(DefautlLogger(runner));
    }

static void testUnforceAlarmAfterUnforceParentChannel(void)
    {
    uint32 bit_i;
    AtDevice device = Device();
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 elapseTime;
        uint32 parentForceAlarm;
        uint32 previousAlarms;
        uint32 forceAlarm = cBit0 << bit_i;

        if ((forceAlarm & forcableAlarm) == 0)
            continue;

        /* Force alarm, expect alarm raised */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, forceAlarm, cAtTrue));
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, forceAlarm, forceAlarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        previousAlarms = runner->testedChannelAlarms;
        TEST_ASSERT((forceAlarm & runner->testedChannelAlarms) == forceAlarm);

        /* Force critical alarm on parent channel. Expect:
         * - Parent must have critical alarm
         * - current channel may raise something, so need to log to check
         * whether it is correct */
        ParentCriticalAlarmForce(runner, cAtTrue);
        parentForceAlarm = mMethodsGet(runner)->ParentCriticalAlarmToForce(runner);
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->parentChannelAlarms, parentForceAlarm, parentForceAlarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        TEST_ASSERT(ParentHasCriticalAlarmRaised(runner, runner->parentChannelAlarms));

        if (runner->testedChannelAlarms != previousAlarms)
            {
            AtOsalMemCpy(stringBuffer, mMethodsGet(runner)->AlarmsToString(runner, previousAlarms), sizeof(stringBuffer));
            Log(cAtLogLevelInfo,
                "%s [force parent]: [before] %s [after] %s\r\n",
                (void*)AtObjectToString((AtObject)TestedChannel()),
                stringBuffer,
                mMethodsGet(runner)->AlarmsToString(runner, runner->testedChannelAlarms));
            }

        /* Update again due to it may change when unforce critical alarm at parent layer */
        previousAlarms = runner->testedChannelAlarms;

        /* Unforce parent channel and make sure alarm of only parent channel cleared */
        ParentCriticalAlarmForce(runner, cAtFalse);
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->parentChannelAlarms, parentForceAlarm, 0);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        TEST_ASSERT(ParentHasCriticalAlarmCleared(runner, runner->parentChannelAlarms));
        TEST_ASSERT((forceAlarm & runner->testedChannelAlarms) == forceAlarm);

        /* Unforce current channel and make sure alarm cleared */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, forceAlarm, cAtFalse));
        AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, cBit31_0, 0);
        TEST_ASSERT(runner->testedChannelAlarms == 0);
        }

    AtLoggerShow(DefautlLogger(runner));
    AtLoggerFlush(DefautlLogger(runner));
    }

static void testForceAlarmWhileParentChannelHasCriticalAlarm(void)
    {
    uint32 bit_i;
    AtDevice device = Device();
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 parentForceAlarm;
        uint32 tempChannelAlarms;
        uint32 elapseTime;
        uint32 forceAlarm = cBit0 << bit_i;

        if ((forceAlarm & forcableAlarm) == 0)
            continue;

        tempChannelAlarms = runner->testedChannelAlarms;

        /* Force critical alarm on parent channel. Expect:
         * - Parent must have critical alarm
         * - current channel may raise something, so need to log to check
         * whether it is correct */
        ParentCriticalAlarmForce(runner, cAtTrue);
        parentForceAlarm = mMethodsGet(runner)->ParentCriticalAlarmToForce(runner);
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->parentChannelAlarms, parentForceAlarm, parentForceAlarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);

        TEST_ASSERT(ParentHasCriticalAlarmRaised(runner, runner->parentChannelAlarms));

        if (runner->testedChannelAlarms != tempChannelAlarms)
            {
            AtOsalMemCpy(stringBuffer, mMethodsGet(runner)->AlarmsToString(runner, tempChannelAlarms), sizeof(stringBuffer));
            Log(cAtLogLevelInfo,
                "%s [force parent]: [before] %s [after] %s\r\n",
                (void *)AtObjectToString((AtObject)TestedChannel()),
                stringBuffer,
                mMethodsGet(runner)->AlarmsToString(runner, runner->testedChannelAlarms));
            }
        tempChannelAlarms = runner->testedChannelAlarms;

        /* Force alarm, expect no alarm raised due to parent has critical alarm */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, forceAlarm, cAtTrue));
        AtChannelInterruptTestRunnerInterruptProcess(device);
        TEST_ASSERT(runner->testedChannelAlarms == tempChannelAlarms);

        /* Unforce parent channel and make sure:
         * - alarm of parent channel cleared
         * - forced alarm of tested channel raised */
        ParentCriticalAlarmForce(runner, cAtFalse);
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, forceAlarm, forceAlarm);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        TEST_ASSERT(ParentHasCriticalAlarmCleared(runner, runner->parentChannelAlarms));
        TEST_ASSERT(forceAlarm & runner->testedChannelAlarms);

        /* Back to unforce state for next test-case */
        TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(runner)->ForceAlarm(runner, forceAlarm, cAtFalse));
        elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, cBit31_0, 0);
        if (elapseTime > cIdealTimeoutInMs)
            Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
        TEST_ASSERT(runner->testedChannelAlarms == 0);
        }

    AtLoggerShow(DefautlLogger(runner));
    AtLoggerFlush(DefautlLogger(runner));
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelInterruptTestRunner self = (AtChannelInterruptTestRunner)userData;
    self->testedChannelAlarms = ((self->testedChannelAlarms & (~changedAlarms)) | currentStatus);
    }

static void ParentChannelAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelInterruptTestRunner self = (AtChannelInterruptTestRunner)userData;
    self->parentChannelAlarms = ((self->parentChannelAlarms & (~changedAlarms)) | currentStatus);
    }

static void * ParentChannelAlarmChangeStateFunction(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return ParentChannelAlarmChangeState;
    }

static uint32 ParentCriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 CriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TopChannelCriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return 0;
    }

static void UnforceAllAlarms(AtChannel channel)
    {
    if (AtChannelTxForcableAlarmsGet(channel) != 0)
        AtChannelTxAlarmUnForce(channel, AtChannelTxForcableAlarmsGet(channel));

    if (AtChannelRxForcableAlarmsGet(channel) != 0)
        AtChannelRxAlarmUnForce(channel, AtChannelRxForcableAlarmsGet(channel));
    }

static void GlobalInterruptsEnable(void)
    {
    AtDevice device = Device();
    AtIpCoreInterruptEnable(AtDeviceIpCoreGet(device, 0), cAtTrue);

    if (AtDeviceModuleGet(device, cAtModuleSdh))
        AtModuleInterruptEnable(AtDeviceModuleGet(device, cAtModuleSdh), cAtTrue);
    if (AtDeviceModuleGet(device, cAtModulePdh))
        AtModuleInterruptEnable(AtDeviceModuleGet(device, cAtModulePdh), cAtTrue);
    if (AtDeviceModuleGet(device, cAtModulePw))
        AtModuleInterruptEnable(AtDeviceModuleGet(device, cAtModulePw), cAtTrue);
    }

static eBool AlarmsAreCleared(AtChannel channel)
    {
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, curTime;

    AtOsalCurTimeGet(&startTime);
    while (elapseTime < cDefaultTimeoutInMs)
        {
        if (AtChannelAlarmGet(channel) == 0)
            return cAtTrue;

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static void ParentChannelSetup(void)
    {
    AtChannel parentChannel = TestedParentChannel();

    if (parentChannel == NULL)
        return;

    AtOsalMemInit(&parentIntrListener, 0, sizeof(parentIntrListener));
    parentIntrListener.AlarmChangeStateWithUserData = mMethodsGet(CurrentRunner())->ParentChannelAlarmChangeStateFunction(CurrentRunner());

    AtChannelInterruptMaskSet(parentChannel, cBit31_0, cBit31_0);
    AtChannelEventListenerAddWithUserData(parentChannel, &parentIntrListener, CurrentRunner());

    UnforceAllAlarms(parentChannel);
    if (AlarmsAreCleared(parentChannel) == 0)
        AtAssert(0);
    }

static void TestedChannelSetup(void)
    {
    AtChannel channel = TestedChannel();

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    intrListener.AlarmChangeStateWithUserData = AlarmChangeState;

    AtChannelInterruptMaskSet(channel, cBit31_0, cBit31_0);
    AtChannelEventListenerAddWithUserData(channel, &intrListener, CurrentRunner());

    UnforceAllAlarms(channel);
    if (AlarmsAreCleared(channel) == 0)
        AtAssert(0);
    AtChannelDefectInterruptClear(channel);
    }

static void SetUp(AtChannelInterruptTestRunner self)
    {
    AtAssert(TestedChannel());
    ParentChannelSetup();
    TestedChannelSetup();
    GlobalInterruptsEnable();
    }

static void Delete(AtUnittestRunner self)
    {
    AtObjectDelete((AtObject)mThis(self)->defaultLogger);
    return m_AtUnittestRunnerMethods->Delete(self);
    }

static AtChannel ParentChannel(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return NULL;
    }

static TestRef BasicTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Basic_AtChannelInterrupt_Fixtures)
        {
        new_TestFixture("testForceAlarmCanCauseNotification", testForceAlarmCanCauseNotification),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Basic_AtChannelInterrupt_Caller, "TestSuite_Basic_AtChannelInterrupt", AtChannelInterruptTestRunnerSetup, AtChannelInterruptTestRunnerTearDown, TestSuite_Basic_AtChannelInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_Basic_AtChannelInterrupt_Caller);
    }

static AtObject PreemptionTestSuiteGet(AtChannelInterruptTestRunner self)
    {
    return NULL;
    }

static eBool ShouldTestAlarmClearedWhenUnforceParentChannel(void)
    {
    return cAtTrue;
    }

static void testAlarmClearedWhenUnforceParentChannel(void)
    {
    uint32 parentForceAlarm;
    uint32 elapseTime;
    AtDevice device = Device();
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 criticalAlarm = mMethodsGet(runner)->CriticalAlarmToForce(runner);

    if (ShouldTestAlarmClearedWhenUnforceParentChannel() == cAtFalse)
        return;

    /* Initialize DB of current channel to have AIS */
    if (mMethodsGet(runner)->CriticalAlarmToForce(runner) == 0)
        return;
    else
        runner->testedChannelAlarms = criticalAlarm;

    /* Force critical alarm on parent channel */
    ParentCriticalAlarmForce(runner, cAtTrue);
    parentForceAlarm = mMethodsGet(runner)->ParentCriticalAlarmToForce(runner);
    elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->parentChannelAlarms, parentForceAlarm, parentForceAlarm);
    if (elapseTime > cIdealTimeoutInMs)
        Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);

    TEST_ASSERT(ParentHasCriticalAlarmRaised(runner, runner->parentChannelAlarms));

    /* Unforce parent channel and make sure:
     * - alarm of parent channel cleared
     * - alarm tested channel cleared */
    ParentCriticalAlarmForce(runner, cAtFalse);
    elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, criticalAlarm, 0);
    if (elapseTime > cIdealTimeoutInMs)
        Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
    TEST_ASSERT(ParentHasCriticalAlarmCleared(runner, runner->parentChannelAlarms));
    TEST_ASSERT((criticalAlarm & runner->testedChannelAlarms) == 0);

    AtLoggerShow(DefautlLogger(runner));
    AtLoggerFlush(DefautlLogger(runner));
    }

static void TopChannelCriticalAlarmForce(AtChannelInterruptTestRunner self, eBool force)
    {
    uint32 alarmToForced = mMethodsGet(self)->TopChannelCriticalAlarmToForce(self);
    AtChannel topChannel = mMethodsGet(self)->TopChannel(self);

    AtAssert(alarmToForced);

    if (force)
        AtChannelTxAlarmForce(topChannel, alarmToForced);
    else
        AtChannelTxAlarmUnForce(topChannel, alarmToForced);
    }

static AtChannel TopChannel(AtChannelInterruptTestRunner self)
    {
    return NULL;
    }

static void TopChannelSetup(AtChannel topChannel)
    {
    AtChannelInterruptMaskSet(topChannel, cBit31_0, cBit31_0);
    }

static void testAlarmClearedWhenUnforceTopChannel(void)
    {
    uint32 elapseTime;
    AtDevice device = Device();
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 criticalAlarm = mMethodsGet(runner)->CriticalAlarmToForce(runner);
    AtChannel topChannel = mMethodsGet(runner)->TopChannel(runner);

    if (ShouldTestAlarmClearedWhenUnforceParentChannel() == cAtFalse)
        return;

    if (topChannel == NULL)
        return;

    /* Initialize DB of current channel to have AIS */
    if (mMethodsGet(runner)->CriticalAlarmToForce(runner) == 0)
        return;
    else
        runner->testedChannelAlarms = criticalAlarm;

    TopChannelSetup(topChannel);

    /* Force critical alarm on top channel */
    TopChannelCriticalAlarmForce(runner, cAtTrue);
    AtOsalUSleep(1000);

    /* Unforce top channel and make sure:
     * - alarm tested channel cleared */
    TopChannelCriticalAlarmForce(runner, cAtFalse);
    elapseTime = AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &runner->testedChannelAlarms, criticalAlarm, 0);
    if (elapseTime > cIdealTimeoutInMs)
        Log(cSevInfo, "(%s %d) elapseTime: %dms\r\n", __FILE__, __LINE__, elapseTime);
    TEST_ASSERT((criticalAlarm & runner->testedChannelAlarms) == 0);

    AtLoggerShow(DefautlLogger(runner));
    AtLoggerFlush(DefautlLogger(runner));
    }

static TestRef HighLayerAffectTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_HighLayerAffect_AtChannelInterrupt_Fixtures)
        {
        new_TestFixture("testUnforceAlarmWhileParentChannelHasCriticalAlarm", testUnforceAlarmWhileParentChannelHasCriticalAlarm),
        new_TestFixture("testForceAlarmWhileParentChannelHasCriticalAlarm", testForceAlarmWhileParentChannelHasCriticalAlarm),
        new_TestFixture("testAlarmClearedWhenUnforceParentChannel", testAlarmClearedWhenUnforceParentChannel),
        new_TestFixture("testAlarmClearedWhenUnforceTopChannel", testAlarmClearedWhenUnforceTopChannel),
        new_TestFixture("testUnforceAlarmAfterUnforceParentChannel", testUnforceAlarmAfterUnforceParentChannel),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_HighLayerAffect_AtChannelInterrupt_Caller, "TestSuite_HighLayerAffect_AtChannelInterrupt", AtChannelInterruptTestRunnerSetup, AtChannelInterruptTestRunnerTearDown, TestSuite_HighLayerAffect_AtChannelInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_HighLayerAffect_AtChannelInterrupt_Caller);
    }

static AtList AllTestSuitesCreate(AtUnittestRunner self)
    {
    AtList suites = m_AtUnittestRunnerMethods->AllTestSuitesCreate(self);

    AtListObjectAdd(suites, (AtObject)BasicTestSuite(self));
    AtListObjectAdd(suites, (AtObject)mMethodsGet(mThis(self))->PreemptionTestSuiteGet(mThis(self)));
    if (mMethodsGet(mThis(self))->ParentChannel(mThis(self)))
        AtListObjectAdd(suites, (AtObject)HighLayerAffectTestSuite(self));
    return suites;
    }

static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize - 1, "interrupt");
    return buffer;
    }

static void OverrideAtUnittestRunner(AtUnittestRunner self)
    {
    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = self->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)self->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, AllTestSuitesCreate);
        mMethodOverride(m_AtUnittestRunnerOverride, Delete);
        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(self, &m_AtUnittestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static void MethodsInit(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Alarms);
        mMethodOverride(m_methods, ForceAlarm);
        mMethodOverride(m_methods, ForceableAlarms);
        mMethodOverride(m_methods, ForcedAlarmGet);
        mMethodOverride(m_methods, PreemptionTestSuiteGet);
        mMethodOverride(m_methods, ParentCriticalAlarmToForce);
        mMethodOverride(m_methods, CriticalAlarmToForce);
        mMethodOverride(m_methods, TopChannelCriticalAlarmToForce);
        mMethodOverride(m_methods, AlarmsToString);
        mMethodOverride(m_methods, ParentChannel);
        mMethodOverride(m_methods, TopChannel);
        mMethodOverride(m_methods, ParentChannelAlarmChangeStateFunction);
        }

    runner->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtChannelInterruptTestRunner);
    }

AtUnittestRunner AtChannelInterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtUnittestRunnerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->testedChannel = channel;

    return self;
    }

AtChannel AtChannelInterruptTestRunnerTestedChannel(AtChannelInterruptTestRunner self)
    {
    return self ? self->testedChannel : NULL;
    }

void AtChannelInterruptTestRunnerSetup(void)
    {
    SetUp(CurrentRunner());
    }

void AtChannelInterruptTestRunnerTearDown(void)
    {
    AtChannel channel = TestedChannel();
    AtChannel parentChannel = TestedParentChannel();
    UnforceAllAlarms(channel);
    AtChannelInterruptMaskSet(channel, cBit31_0, 0);
    AtChannelEventListenerRemove(channel, &intrListener);

    if (parentChannel == NULL)
        return;

    UnforceAllAlarms(parentChannel);
    AtChannelInterruptMaskSet(parentChannel, cBit31_0, 0);
    AtChannelEventListenerRemove(parentChannel, &parentIntrListener);
    }

void AtChannelInterruptTestRunnerInterruptProcess(AtDevice device)
    {
    AtOsalUSleep(10000);
    AtDeviceInterruptProcess(device);
    }

uint32 AtChannelInterruptTestRunnerInterruptProcessWithResult(AtDevice device, uint32 *alarm, uint32 mask, uint32 expectedValue)
    {
    uint32 elapseTime = 0;
    tAtOsalCurTime curTime, startTime;

    AtOsalCurTimeGet(&startTime);
    while (elapseTime < cDefaultTimeoutInMs)
        {
        AtChannelInterruptTestRunnerInterruptProcess(device);
        if ((*alarm & mask) == expectedValue)
            break;

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return elapseTime;
    }

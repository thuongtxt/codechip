/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtPdhDe3InterruptTestRunner.c
 *
 * Created Date: Jan 14, 2016
 *
 * Description : Interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhDe3.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtPdhDe3AlarmTypeStr[] =
    {
    "los",
    "lof",
    "ais",
    "rai"
    };

static const uint32 cAtPdhDe3AlarmTypeVal[] =
    {
    cAtPdhDe3AlarmLos,
    cAtPdhDe3AlarmLof,
    cAtPdhDe3AlarmAis,
    cAtPdhDe3AlarmRai
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelInterruptTestRunnerMethods m_AtChannelInterruptTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannelInterruptTestRunner CurrentRunner(void)
    {
    return (AtChannelInterruptTestRunner)AtUnittestRunnerCurrentRunner();
    }

static uint32 Alarms(AtChannelInterruptTestRunner self)
    {
    uint32 alarms = cAtPdhDe3AlarmAis;
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    AtUnused(self);

    if (AtPdhDe3FrameTypeIsUnframed(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return alarms;

    return alarms | cAtPdhDe3AlarmLof | cAtPdhDe3AlarmRai;
    }

static eAtRet ForceAlarm(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable)
    {
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);

    if ((alarm == cAtPdhDe3AlarmAis) || (alarm == cAtPdhDe3AlarmRai))
        return (enable) ? AtChannelTxAlarmForce(channel, alarm) :
                          AtChannelTxAlarmUnForce(channel, alarm);


    return (enable) ? AtChannelRxAlarmForce(channel, alarm) :
                      AtChannelRxAlarmUnForce(channel, alarm);

    }

static void testLosCanTerminateAllOtherAlarms(void)
    {
    uint32 bit_i;
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    if ((forcableAlarm & cAtPdhDe3AlarmLos) == 0)
        return;

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        uint32 alarm = cBit0 << bit_i;
        if ((alarm & forcableAlarm) == 0)
            continue;

        if (alarm == cAtPdhDe3AlarmLos)
            continue;

        testForceAlarmCanTerminateOtherAlarms(runner, cAtPdhDe3AlarmLos, alarm);
        }
    }

static void testAisCanTerminateAllOtherAlarmsExceptLos(void)
    {
    uint32 bit_i;
    AtChannelInterruptTestRunner runner = CurrentRunner();
    uint32 forcableAlarm = mMethodsGet(runner)->ForceableAlarms(runner);

    for (bit_i = 0; bit_i < cNumBitsInDword; bit_i++)
        {
        eAtPdhDe3FrameType frameType;
        uint32 alarm = cBit0 << bit_i;
        if ((alarm & forcableAlarm) == 0)
            continue;

        frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)AtChannelInterruptTestRunnerTestedChannel(CurrentRunner()));
        if ((frameType == cAtPdhDs3Unfrm) || (frameType == cAtPdhE3Unfrm))
            {
            if ((alarm == cAtPdhDe3AlarmLos) || (alarm == cAtPdhDe3AlarmAis))
                continue;
            testForceAlarmCanTerminateOtherAlarms(runner, cAtPdhDe3AlarmAis, alarm);
            }
        else
            {
            if ((alarm == cAtPdhDe3AlarmLos) || (alarm == cAtPdhDe3AlarmLof))
                continue;
            testForceAlarmCanTerminateOtherAlarms(runner, cAtPdhDe3AlarmLof, alarm);
            }
        }
    }

static TestRef PreemptionTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Preemption_AtChannelInterrupt_Fixtures)
        {
        new_TestFixture("testLosCanTerminateAllOtherAlarms", testLosCanTerminateAllOtherAlarms),
        new_TestFixture("testAisCanTerminateAllOtherAlarmsExceptLos", testAisCanTerminateAllOtherAlarmsExceptLos),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Preemption_AtChannelInterrupt_Caller, "TestSuite_Preemption_AtChannelInterrupt", AtChannelInterruptTestRunnerSetup, AtChannelInterruptTestRunnerTearDown, TestSuite_Preemption_AtChannelInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_Preemption_AtChannelInterrupt_Caller);
    }

static AtObject PreemptionTestSuiteGet(AtChannelInterruptTestRunner self)
    {
    return (AtObject)PreemptionTestSuite((AtUnittestRunner)self);
    }

static const char * AlarmsToString(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return CliAlarmMaskToString(cAtPdhDe3AlarmTypeStr, cAtPdhDe3AlarmTypeVal, mCount(cAtPdhDe3AlarmTypeVal), alarms);
    }

static uint32 CriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    return cAtPdhDe3AlarmAis;
    }

static void OverrideAtChannelInterruptTestRunner(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelInterruptTestRunnerOverride, (void *)runner->methods, sizeof(m_AtChannelInterruptTestRunnerOverride));

        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, Alarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceAlarm);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, PreemptionTestSuiteGet);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, AlarmsToString);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, CriticalAlarmToForce);
        }

    mMethodsSet(runner, &m_AtChannelInterruptTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtChannelInterruptTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe3InterruptTestRunner);
    }

 AtUnittestRunner AtPdhDe3InterrupTestRunnerObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelInterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtPdhDe3InterruptTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtPdhDe3InterrupTestRunnerObjectInit(newRunner, channel);
    }

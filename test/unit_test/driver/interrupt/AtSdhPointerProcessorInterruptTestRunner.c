/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtSdhPointerProcessorInterruptTestRunner.c
 *
 * Created Date: Jan 12, 2016
 *
 * Description : Interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtSdhPath.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSdhPointerProcessorInterruptTestRunner
    {
    tAtChannelInterruptTestRunner super;
    }tAtSdhPointerProcessorInterruptTestRunner;

static const char *cCmdSdhPathAlarmStr[] =
    {
     "ais",
     "lop"
    };

static const uint32 cCmdSdhPathAlarmValue[]=
    {
     cAtSdhPathAlarmAis,
     cAtSdhPathAlarmLop
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelInterruptTestRunnerMethods m_AtChannelInterruptTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannelInterruptTestRunner CurrentRunner(void)
    {
    return (AtChannelInterruptTestRunner)AtUnittestRunnerCurrentRunner();
    }

static uint32 Alarms(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eAtRet ForceAlarm(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable)
    {
    uint8 ssBit;
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    AtSdhPath path = (AtSdhPath)channel;

    if (alarm == cAtSdhPathAlarmAis)
        return (enable) ? AtChannelTxAlarmForce(channel, cAtSdhPathAlarmAis) :
                          AtChannelTxAlarmUnForce(channel, cAtSdhPathAlarmAis);

    if (alarm != cAtSdhPathAlarmLop)
        return cAtErrorModeNotSupport;

    ssBit = AtSdhPathTxSsGet(path);
    if (enable)
        {
        AtSdhPathExpectedSsSet(path, (ssBit == 0) ? (ssBit + 1) : (ssBit - 1));
        AtSdhPathSsCompareEnable(path, cAtTrue);
        }
    else
        AtSdhPathExpectedSsSet(path, ssBit);

    return cAtOk;
    }

static uint32 ForceableAlarms(AtChannelInterruptTestRunner self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static void testAisCanPreemptLop(void)
    {
    AtChannelInterruptTestRunner self = CurrentRunner();
    AtDevice device = AtChannelDeviceGet(AtChannelInterruptTestRunnerTestedChannel(self));

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmLop, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cAtSdhPathAlarmLop, cAtSdhPathAlarmLop);
    TEST_ASSERT(self->testedChannelAlarms & cAtSdhPathAlarmLop);

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmAis, cAtTrue));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cAtSdhPathAlarmAis, cAtSdhPathAlarmAis);
    TEST_ASSERT(self->testedChannelAlarms & cAtSdhPathAlarmAis);
    TEST_ASSERT((self->testedChannelAlarms & cAtSdhPathAlarmLop) == 0);

    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmLop, cAtFalse));
    TEST_ASSERT_EQUAL_INT(cAtOk, mMethodsGet(self)->ForceAlarm(self, cAtSdhPathAlarmAis, cAtFalse));
    AtChannelInterruptTestRunnerInterruptProcessWithResult(device, &self->testedChannelAlarms, cBit31_0, 0);
    TEST_ASSERT(self->testedChannelAlarms == 0);
    }

static TestRef PreemptionTestSuite(AtUnittestRunner self)
    {
    /* Create fixtures */
    EMB_UNIT_TESTFIXTURES(TestSuite_Preemption_AtChannelInterrupt_Fixtures)
        {
        new_TestFixture("testAisCanPreemptLop", testAisCanPreemptLop),
        };

    /* Create test suite from fixtures */
    EMB_UNIT_TESTCALLER(TestSuite_Preemption_AtChannelInterrupt_Caller, "TestSuite_Preemption_AtChannelInterrupt", AtChannelInterruptTestRunnerSetup, AtChannelInterruptTestRunnerTearDown, TestSuite_Preemption_AtChannelInterrupt_Fixtures);

    return (TestRef)((void *)&TestSuite_Preemption_AtChannelInterrupt_Caller);
    }

static AtObject PreemptionTestSuiteGet(AtChannelInterruptTestRunner self)
    {
    return (AtObject)PreemptionTestSuite((AtUnittestRunner)self);
    }

static const char * AlarmsToString(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return CliAlarmMaskToString(cCmdSdhPathAlarmStr, cCmdSdhPathAlarmValue, mCount(cCmdSdhPathAlarmValue), alarms);
    }

static uint32 CriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    return cAtSdhPathAlarmAis;
    }

static eBool IsAuTu(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    if ((channelType == cAtSdhChannelTypeAu3)    ||
        (channelType == cAtSdhChannelTypeAu4)    ||
        (channelType == cAtSdhChannelTypeAu4_16c)||
        (channelType == cAtSdhChannelTypeAu4_4c) ||
        (channelType == cAtSdhChannelTypeAu4_nc) ||
        (channelType == cAtSdhChannelTypeTu3)    ||
        (channelType == cAtSdhChannelTypeTu11)   ||
        (channelType == cAtSdhChannelTypeTu12))
        return cAtTrue;

    return cAtFalse;
    }

static AtSdhChannel FindParent(AtSdhChannel channel)
    {
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);

    if (parentChannel == NULL)
        return NULL;

    return IsAuTu(parentChannel) ? parentChannel : FindParent(parentChannel);
    }

static AtChannel ParentChannel(AtChannelInterruptTestRunner self)
    {
    return (AtChannel)FindParent((AtSdhChannel)AtChannelInterruptTestRunnerTestedChannel(self));
    }

static uint32 ParentCriticalAlarmToForce(AtChannelInterruptTestRunner self)
    {
    return cAtSdhPathAlarmAis;
    }

static void OverrideAtChannelInterruptTestRunner(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelInterruptTestRunnerOverride, (void *)runner->methods, sizeof(m_AtChannelInterruptTestRunnerOverride));

        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, Alarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceAlarm);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceableAlarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, PreemptionTestSuiteGet);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, AlarmsToString);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, CriticalAlarmToForce);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentChannel);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ParentCriticalAlarmToForce);
        }

    mMethodsSet(runner, &m_AtChannelInterruptTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtChannelInterruptTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhPointerProcessorInterruptTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelInterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtSdhPointerProcessorInterruptTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, channel);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Interrupt
 *
 * File        : AtPwInterruptTestRunner.c
 *
 * Created Date: Jan 12, 2016
 *
 * Description : Interrupt test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPw.h"
#include "AtChannelInterruptTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwInterruptTestRunner
    {
    tAtChannelInterruptTestRunner super;
    }tAtPwInterruptTestRunner;

static const char * cAtPwAlarmTypeStr[] = {
                                         "lbit",
                                         "rbit",
                                         };

static const eAtPwAlarmType cAtPwAlarmTypeVal[]  = {
                                                    cAtPwAlarmTypeLBit,
                                                    cAtPwAlarmTypeRBit,
                                                  };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelInterruptTestRunnerMethods m_AtChannelInterruptTestRunnerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Alarms(AtChannelInterruptTestRunner self)
    {
    return cAtPwAlarmTypeLBit | cAtPwAlarmTypeRBit |
           cAtPwAlarmTypeJitterBufferOverrun | cAtPwAlarmTypeJitterBufferUnderrun |
           cAtPwAlarmTypeLops | cAtPwAlarmTypeMissingPacket;
    }

static eAtRet ForceAlarm(AtChannelInterruptTestRunner self, uint32 alarm, eBool enable)
    {
    AtChannel channel = AtChannelInterruptTestRunnerTestedChannel(self);
    return enable ? AtChannelTxAlarmForce(channel, alarm) : AtChannelTxAlarmUnForce(channel, alarm);
    }

static const char * AlarmsToString(AtChannelInterruptTestRunner self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return CliAlarmMaskToString(cAtPwAlarmTypeStr, cAtPwAlarmTypeVal, mCount(cAtPwAlarmTypeVal), alarms);
    }

static void OverrideAtChannelInterruptTestRunner(AtUnittestRunner self)
    {
    AtChannelInterruptTestRunner runner = (AtChannelInterruptTestRunner)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelInterruptTestRunnerOverride, (void *)runner->methods, sizeof(m_AtChannelInterruptTestRunnerOverride));

        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, Alarms);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, ForceAlarm);
        mMethodOverride(m_AtChannelInterruptTestRunnerOverride, AlarmsToString);
        }

    mMethodsSet(runner, &m_AtChannelInterruptTestRunnerOverride);
    }

static void Override(AtUnittestRunner self)
    {
    OverrideAtChannelInterruptTestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwInterruptTestRunner);
    }

static AtUnittestRunner ObjectInit(AtUnittestRunner self, AtChannel channel)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelInterrupTestRunnerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUnittestRunner AtPwInterruptTestRunnerNew(AtChannel channel)
    {
    AtUnittestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newRunner, channel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtCrossConnectTestRunner.h
 * 
 * Created Date: May 23, 2015
 *
 * Description : Cross-connect test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCROSSCONNECTTESTRUNNER_H_
#define _ATCROSSCONNECTTESTRUNNER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCrossConnect.h"
#include "../man/AtObjectTestRunnerInternal.h"
#include "AtModuleXcTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCrossConnectTestRunner * AtCrossConnectTestRunner;

typedef struct tAtCrossConnectTestRunner
    {
    tAtObjectTestRunner super;

    /* Private data */
    AtModuleXcTestRunner moduleRunner;
    }tAtCrossConnectTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnectTestRunner AtCrossConnectTestRunnerObjectInit(AtCrossConnectTestRunner self, AtModuleXcTestRunner moduleRunner, AtCrossConnect xc);

AtModuleXcTestRunner AtCrossConnectTestRunnerModuleRunner(AtCrossConnectTestRunner self);
AtCrossConnect AtCrossConnectTestRunnerCrossConnect(AtCrossConnectTestRunner self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCROSSCONNECTTESTRUNNER_H_ */


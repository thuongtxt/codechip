/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtModuleXcTestRunnerInternal.h
 * 
 * Created Date: May 04, 2015
 *
 * Description : XC module test runner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEXCTESTRUNNERINTERNAL_H_
#define _ATMODULEXCTESTRUNNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleXc.h"
#include "AtCrossConnectTestRunner.h"
#include "../man/module_runner/AtModuleTestRunnerInternal.h"
#include "../man/channel_runner/AtChannelTestRunner.h"
#include "AtModuleXcTestRunner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleXcTestRunnerMethods
    {
    AtCrossConnectTestRunner (*CreateVcXcTestRunner)(AtModuleXcTestRunner self, AtCrossConnect xc);
    }tAtModuleXcTestRunnerMethods;

typedef struct tAtModuleXcTestRunner
    {
    tAtModuleTestRunner super;
    const tAtModuleXcTestRunnerMethods *methods;
    }tAtModuleXcTestRunner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXcTestRunner AtModuleXcTestRunnerObjectInit(AtModuleXcTestRunner self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEXCTESTRUNNERINTERNAL_H_ */


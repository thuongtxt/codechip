/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : AtModuleXCTestRunner.c
 *
 * Created Date: May 04, 2015
 *
 * Description : XC unittest runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleXcTestRunnerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleXcTestRunnerMethods m_methods;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtCrossConnectTestRunner CreateVcXcTestRunner(AtModuleXcTestRunner self, AtCrossConnect xc)
    {
    return NULL;
    }

static void Run(AtUnittestRunner self)
    {
    AtModuleXcTestRunner runner = (AtModuleXcTestRunner)self;
    AtModuleXc xcModule = (AtModuleXc)AtModuleTestRunnerModuleGet((AtModuleTestRunner)runner);
    AtUnittestRunner vcXcTestRunner;

    m_AtUnittestRunnerMethods->Run(self);

    vcXcTestRunner = (AtUnittestRunner)mMethodsGet(runner)->CreateVcXcTestRunner(runner, AtModuleXcVcCrossConnectGet(xcModule));
    AtUnittestRunnerRun(vcXcTestRunner);
    AtUnittestRunnerDelete(vcXcTestRunner);
    }

static void MethodsInit(AtModuleXcTestRunner self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CreateVcXcTestRunner);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtUnittestRunner(AtModuleXcTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, Run);
        }

    runner->methods = &m_AtUnittestRunnerOverride;
    }

static void Override(AtModuleXcTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleXcTestRunner);
    }

AtModuleXcTestRunner AtModuleXcTestRunnerObjectInit(AtModuleXcTestRunner self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleTestRunnerObjectInit((AtModuleTestRunner)self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXcTestRunner AtModuleXcTestRunnerNew(AtModule module)
    {
    AtModuleXcTestRunner newRunner = AtOsalMemAlloc(ObjectSize());
    return AtModuleXcTestRunnerObjectInit(newRunner, module);
    }

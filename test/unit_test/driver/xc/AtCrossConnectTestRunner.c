/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : AtCrossConnectTestRunner.c
 *
 * Created Date: May 23, 2015
 *
 * Description : Cross-connect test runner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCrossConnectTestRunner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUnittestRunnerMethods m_AtUnittestRunnerOverride;

/* Save super implementation */
static const tAtUnittestRunnerMethods *m_AtUnittestRunnerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *NameBuild(AtUnittestRunner self, char *buffer, uint32 bufferSize)
    {
    AtCrossConnectTestRunner runner = (AtCrossConnectTestRunner)self;
    AtCrossConnect xc = AtCrossConnectTestRunnerCrossConnect(runner);
    AtSprintf(buffer, "%s", AtObjectToString((AtObject)xc));
    return buffer;
    }

static void OverrideAtUnittestRunner(AtCrossConnectTestRunner self)
    {
    AtUnittestRunner runner = (AtUnittestRunner)self;

    if (!m_methodsInit)
        {
        m_AtUnittestRunnerMethods = runner->methods;
        AtOsalMemCpy(&m_AtUnittestRunnerOverride, (void *)runner->methods, sizeof(m_AtUnittestRunnerOverride));

        mMethodOverride(m_AtUnittestRunnerOverride, NameBuild);
        }

    mMethodsSet(runner, &m_AtUnittestRunnerOverride);
    }

static void Override(AtCrossConnectTestRunner self)
    {
    OverrideAtUnittestRunner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCrossConnectTestRunner);
    }

AtCrossConnectTestRunner AtCrossConnectTestRunnerObjectInit(AtCrossConnectTestRunner self, AtModuleXcTestRunner moduleRunner, AtCrossConnect xc)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectTestRunnerObjectInit((AtObjectTestRunner)self, (AtObject)xc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->moduleRunner = moduleRunner;

    return self;
    }

AtModuleXcTestRunner AtCrossConnectTestRunnerModuleRunner(AtCrossConnectTestRunner self)
    {
    return self ? self->moduleRunner : NULL;
    }

AtCrossConnect AtCrossConnectTestRunnerCrossConnect(AtCrossConnectTestRunner self)
    {
    return (AtCrossConnect)AtObjectTestRunnerObjectGet((AtObjectTestRunner)self);
    }

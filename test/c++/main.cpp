/*
 * main.cpp
 *
 *  Created on: Jan 7, 2015
 *      Author: nguyennt
 */

#include  "driver/include/common/AtCommon.h"
#include  "driver/include/common/attypes.h"
#include  "driver/include/common/AtObject.h"
#include  "driver/include/common/AtChannel.h"


int main ()
    {
    AtChannel channel;
    return AtChannelIdGet(channel);
    }

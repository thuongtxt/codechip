import getopt
try:
    from AtCiscoSystem import AtCiscoSystem
    from AtColor import AtColor
except:
    pass

import time
import sys

def maxNumPws():
    return (4 * 48 * 28)

def maxNumChannelsToQuery():
    return 2048

def numProvisionedPws(epapp):
    startPw = 1
    stopPw = 0
    numPws = 0
    
    while stopPw < maxNumPws():
        stopPw = startPw + maxNumChannelsToQuery() - 1
        if stopPw > maxNumPws():
            stopPw = maxNumPws()
            
        pws = "%d-%d" % (startPw, stopPw)
        result = epapp.runCli("show pw %s" % pws)
        numPws = numPws + result.numRows()
        
        startPw = stopPw + 1
    
        # Also deprovision PWs are we do not care. All of DS1 will be configured 
        # in SF format to have valid frames
        epapp.runCli("pw circuit unbind %s" % pws)
    
    return numPws

def allPwsAreProvisioned(epapp):
    numPws = numProvisionedPws(epapp)
    print "Number of provisioned PWs: %d" % numPws
    return numPws == maxNumPws()

def checkDe1sDefectInterrupt(epapp, de1s, defectType):
    result = epapp.runCli("show pdh de1 interrupt %s ro" % de1s, timeout=60)
    for row_i in range(0, result.numRows()):
        defectStatus = result.cellContent(row_i, defectType)
        if defectStatus.lower() != "set":
            channelId = result.cellContent(row_i, "DE1 ID")
            raise Exception("%s is not having %s change interrupt" % (channelId, defectType))

def makeDs1InterruptOnLine(epapp, lineId):
    de1s = "%d.1.1.1.1-%d.16.3.7.4" % (lineId, lineId)
    epapp.runCli("pdh de1 framing %s ds1_esf")
    epapp.runCli("pdh de1 force alarm %s tx ais en" % de1s)
    epapp.runCli("pdh de1 force alarm %s tx ais dis" % de1s)
    checkDe1sDefectInterrupt(epapp, de1s, "AIS")

def checkPathDefectInterrupt(epapp, paths, defectType):
    result = epapp.runCli("show sdh path interrupt %s ro" % paths, timeout=60)
    for row_i in range(0, result.numRows()):
        defectStatus = result.cellContent(row_i, defectType)
        if defectStatus != "set":
            channelId = result.cellContent(row_i, "PathId")
            raise Exception("%s is not having %s change interrupt" % (channelId, defectType))

def toggleAisOnPaths(epapp, paths):
    epapp.runCli("sdh path force alarm %s ais tx en"  % paths, timeout=60)
    epapp.runCli("sdh path force alarm %s ais tx dis" % paths, timeout=60)
    checkPathDefectInterrupt(epapp, paths, "AIS")

def toggleUneqOnPaths(epapp, paths):
    epapp.runCli("sdh path psl transmit %s 0x0"  % paths)
    epapp.runCli("sdh path psl transmit %s 0x2" % paths)
    checkPathDefectInterrupt(epapp, paths, "UNEQ")

def makeInterruptOnLine(epapp, lineId):
    toggleAisOnPaths(epapp, "au3.%d.1.1-%d.16.3" % (lineId, lineId))
    toggleUneqOnPaths(epapp, "vc3.%d.1.1-%d.16.3" % (lineId, lineId))
    toggleAisOnPaths(epapp, "tu1x.%d.1.1.1.1-%d.16.3.7.4" % (lineId, lineId))
    toggleUneqOnPaths(epapp, "vc1x.%d.1.1.1.1-%d.16.3.7.4" % (lineId, lineId))
    
    makeDs1InterruptOnLine(epapp, lineId)
    
def makeFullInterruptTree(epapp):
    epapp.runCli("device show readwrite dis dis")
    epapp.runCli("device interrupt disable")
    epapp.runCli("sdh line loopback 1-4 local")
    for line_i in range(0, 4):
        makeInterruptOnLine(epapp, line_i + 1)

def processInterrupt(epapp):
    # If hogging happen, there would be timeout
    epapp.runCli("device interrupt process", 3)

def rate():
    return 48

def cemGroupCalculate(stsId, vtgId, vtId):
    return (stsId * 28) + (vtgId * 4) + vtId

def cemConnectionName(portId1, stsId1, vtgId1, vtId1,
                      portId2, stsId2, vtgId2, vtId2):
    connection1 = (portId1 * 1344) + (stsId1 * 28) + (vtgId1 * 4) + vtId1;
    connection2 = (portId2 * 1344) + (stsId2 * 28) + (vtgId2 * 4) + vtId2;
    return "%d_%d" % (connection1, connection2)

def setDs1Mapping(system, slotId, portId):
    interface = "0/%d/%d" % (slotId, portId)

    system.execute("controller mediaType %s" % interface)
    system.execute("mode sonet")
    system.execute("controller sonet %s" % interface)
    system.execute("rate oc%d" % rate())
    
    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        system.execute("sts-1 %d" % stsId)
        system.execute("mode vt-15")
        
        for vtg_i in range(0, 7):
            vtgId = vtg_i + 1
            for vt_i in range(0, 4):
                vtId = vt_i + 1
                system.execute("vtg %d t1 %d framing unframed" % (vtgId, vtId))
                system.execute("no vtg %d t1 %d shutdown" % (vtgId, vtId))
                system.execute("vtg %d t1 %d cem-group %d unframed" % (vtgId, vtId, cemGroupCalculate(sts_i, vtg_i, vt_i)))

def ds1CrossConnect(system, bay1, port1, bay2, port2, connect = True):
    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        for vtg_i in range(0, 7):
            vtgId = vtg_i + 1
            for vt_i in range(0, 4):
                vtId = vt_i + 1
                
                vt1CemGroupId = cemGroupCalculate(sts_i, vtg_i, vt_i)
                vt2CemGroupId = cemGroupCalculate(sts_i, vtg_i, vt_i)
                
                # Connect them
                endPoint1 = "cem 0/%d/%d %d" % (bay1, port1, vt1CemGroupId)
                endPoint2 = "cem 0/%d/%d %d" % (bay2, port2, vt2CemGroupId)
                connectionName = cemConnectionName(port1, sts_i, vtg_i, vt_i, 
                                                   port2, sts_i, vtg_i, vt_i)
                "%d_%d" % (vt1CemGroupId, vt2CemGroupId)
                if connect:
                    system.execute("connect %s %s %s" % (connectionName, endPoint1, endPoint2))
                else:
                    system.execute("no connect %s" % (connectionName))

def configureFullSATOP(system, slotId):
    system.execute("configure terminal")
    
    setDs1Mapping(system, slotId, 0)
    setDs1Mapping(system, slotId, 2)
    setDs1Mapping(system, slotId, 4)
    setDs1Mapping(system, slotId, 6)
    
    ds1CrossConnect(system, slotId, 0, slotId, 2, connect=True)
    ds1CrossConnect(system, slotId, 4, slotId, 6, connect=True)
    
    system.execute("end")
    system.execute("write")

if __name__ == '__main__':
    ipAddress = None
    slotId = None
    rspId = 0
    runningTime = 1
    configureFirst = False
    sdkPath = None
    checkFullPws = False
    
    def help(appName):
        print "Usage: %s [-h/--help] <-i/--ip address> <-s/--slot slotId> <-r/--rsp rspId> [-p/--repeat repeatTimes] [-f/--configure]" % appName
    
    def getOptions(argv):
        global ipAddress
        global slotId
        global rspId
        global runningTime
        global configureFirst
        global sdkPath
        
        try:
            opts, args = getopt.getopt(argv[1:],"hi:s:r:p:fd:",["help", "ip=", "slot=", "rsp=", "repeat=", "configure", "sdk="])
        except getopt.GetoptError:
            help(argv[0])
            exit(1)
    
        for opt, arg in opts:
            if opt in ('-h', "--help"):
                help(argv[0])
                exit(0)
                
            elif opt in ("-i", "--ip"):
                ipAddress = arg
            
            elif opt in ("-s", "--slot"):
                slotId = int(arg)
                
            elif opt in ("-r", "--rsp"):
                rspId = int(arg)
                
            elif opt in ("-p", "--repeat"):
                runningTime = int(arg)
                
            elif opt in ("-f", "--configure"):
                configureFirst = True
                
            elif opt in ("-d", "--sdk"):
                sdkPath = arg
            
    getOptions(sys.argv)
    
    if ipAddress is None or slotId is None:
        help(sys.argv[0])
        exit(-1)
            
    # Make connection
    system = AtCiscoSystem(ipAddress)
    system.verbose = True
    system.connect()
    epapp = system.epappForSlot(slotId, rspId)
    epapp.start()
    epapp.runCli("textui limit rest 0")
    epapp.runCli("textui limit table rest 0")
    
    if configureFirst:
        configureFullSATOP(system, slotId)
    
    # Till all of PWs are provisioned
    if checkFullPws:
        print AtColor.GREEN + "Checking if all PWs have been provisioned" + AtColor.CLEAR
        while not allPwsAreProvisioned(epapp):
            time.sleep(5)
    
    for time_i in range(0, runningTime):
        print AtColor.GREEN + ("Test time %d" % time_i) + AtColor.CLEAR
        makeFullInterruptTree(epapp)
        processInterrupt(epapp)

from AtCiscoSystem import AtCiscoSystem
import time

if __name__ == '__main__':
    ipAddress = "172.33.42.254"
    slotId = 5
    rspId = 0
            
    # Make connection
    system = AtCiscoSystem(ipAddress)
    system.connect()
    epapp = system.epappForSlot(slotId, rspId)
    epapp.start()
    epapp.runCli("textui limit rest 0")
    epapp.runCli("textui limit table rest 0")
    
    for i in range(1, 100):
        def runCli(cli):
            epapp.runCli(cli, timeout=60)
        
        print "Running time: %d" % (i + 1)
        
        runCli("device interrupt disable")
    
        runCli("sdh path force alarm au3.1.1.1-4.16.3 ais tx en")
        runCli("sdh path force alarm au3.1.1.1-4.16.3 ais tx dis")
        runCli("sdh path psl transmit vc3.1.1.1-4.16.3 0x0")
        runCli("sdh path psl transmit vc3.1.1.1-4.16.3 0x2")
        
        runCli("sdh path force alarm tu1x.1.1.1.1.1-1.16.3.7.4 ais tx en")
        runCli("sdh path force alarm tu1x.2.1.1.1.1-2.16.3.7.4 ais tx en")
        runCli("sdh path force alarm tu1x.3.1.1.1.1-3.16.3.7.4 ais tx en")
        runCli("sdh path force alarm tu1x.4.1.1.1.1-4.16.3.7.4 ais tx en")
        runCli("sdh path force alarm tu1x.1.1.1.1.1-1.16.3.7.4 ais tx dis")
        runCli("sdh path force alarm tu1x.2.1.1.1.1-2.16.3.7.4 ais tx dis")
        runCli("sdh path force alarm tu1x.3.1.1.1.1-3.16.3.7.4 ais tx dis")
        runCli("sdh path force alarm tu1x.4.1.1.1.1-4.16.3.7.4 ais tx dis")
        
        runCli("sdh path psl transmit vc1x.1.1.1.1.1-1.16.3.7.4 0x0")
        runCli("sdh path psl transmit vc1x.2.1.1.1.1-2.16.3.7.4 0x0")
        runCli("sdh path psl transmit vc1x.3.1.1.1.1-3.16.3.7.4 0x0")
        runCli("sdh path psl transmit vc1x.4.1.1.1.1-4.16.3.7.4 0x0")
        runCli("sdh path psl transmit vc1x.1.1.1.1.1-1.16.3.7.4 0x2")
        runCli("sdh path psl transmit vc1x.2.1.1.1.1-2.16.3.7.4 0x2")
        runCli("sdh path psl transmit vc1x.3.1.1.1.1-3.16.3.7.4 0x2")
        runCli("sdh path psl transmit vc1x.4.1.1.1.1-4.16.3.7.4 0x2")
        
        runCli("pdh de1 framing 1.1.1.1.1-1.8.3.7.4  ds1_esf")
        runCli("pdh de1 framing 1.9.1.1.1-1.16.3.7.4 ds1_esf")
        runCli("pdh de1 framing 2.1.1.1.1-2.8.3.7.4  ds1_esf")
        runCli("pdh de1 framing 2.9.1.1.1-2.16.3.7.4 ds1_esf")
        runCli("pdh de1 framing 3.1.1.1.1-3.8.3.7.4  ds1_esf")
        runCli("pdh de1 framing 3.9.1.1.1-3.16.3.7.4 ds1_esf")
        runCli("pdh de1 framing 4.1.1.1.1-4.8.3.7.4  ds1_esf ")
        runCli("pdh de1 framing 4.9.1.1.1-4.16.3.7.4 ds1_esf")
        
        runCli("pdh de1 force alarm 1.1.1.1.1-1.16.3.7.4 tx ais en")
        runCli("pdh de1 force alarm 2.1.1.1.1-2.16.3.7.4 tx ais en")
        runCli("pdh de1 force alarm 3.1.1.1.1-3.16.3.7.4 tx ais en")
        runCli("pdh de1 force alarm 4.1.1.1.1-4.16.3.7.4 tx ais en")
        runCli("pdh de1 force alarm 1.1.1.1.1-1.16.3.7.4 tx ais dis")
        runCli("pdh de1 force alarm 2.1.1.1.1-2.16.3.7.4 tx ais dis")
        runCli("pdh de1 force alarm 3.1.1.1.1-3.16.3.7.4 tx ais dis")
        runCli("pdh de1 force alarm 4.1.1.1.1-4.16.3.7.4 tx ais dis")
        
        runCli("device interrupt process")
    
    epapp.exit()
    system.disconnect()
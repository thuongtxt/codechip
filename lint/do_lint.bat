@rem The arguments for this batch file: 
@rem %1: The path to the project folder
@rem ------------------------------------------------------
@rem Path to my project folder
SET PROJ_PATH=%1
@rem Path to CodeWarrior installation folder (which is e.g. "C:\Freescale\CW MCU v10.2\eclipse\..\MCU")
@rem Path to lint-nt.exe
set LINT_DIR=%2
SET LINT_EXE=%LINT_DIR%\pclint\lint-nt.exe
@rem Path to my lint configuration files
@rem Path to my local lint folder inside the project
SET PROJ_LINT_PATH=%PROJ_PATH%\lint
@rem Lint configuration files and includes
@rem --------------- Run PC-lint ---------------------------
%LINT_EXE% %PROJ_LINT_PATH%\proj_options.lnt %PROJ_LINT_PATH%\proj_files.lnt -vf

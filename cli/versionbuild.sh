#!/bin/sh
# Print additional version information for non-release trees.


usage() {
	echo "Usage: $0 <srctree> <out_file>" >&2
	exit 1
}
SRCTREE=$1
OUTFILE=$2
SRC_COMMIT=""

echo ' ' > $OUTFILE
echo '' > $OUTFILE
echo 'const char* const SDK_BUILD_DATE = "'`date +%c`'";' >> $OUTFILE
echo 'const char* const SDK_BUILD_NAME = "'`whoami`"@"`hostname`'";' >> $OUTFILE

# Check for git and a git repo.
if head=`git rev-parse --verify HEAD 2>/dev/null`; then
	SRC_COMMIT="On gitcommit="`git describe --all --long --abbrev=100` 

	# Do we have an untagged version?

	# Are there uncommitted changes?
	git update-index --refresh --unmerged > /dev/null
	if git diff-index --name-only HEAD | read dummy; then
		SRC_COMMIT=$SRC_COMMIT"-dirty"
	fi
	echo 'const char* const SDK_SRC_COMMIT = "'$SRC_COMMIT'";' >> $OUTFILE
else
	echo 'const char* const SDK_SRC_COMMIT = "Unkown Source Control";' >> $OUTFILE
fi
echo '' >> $OUTFILE

## Check for svn and a svn repo.
#if rev=`svn info 2>/dev/null` ; then
#	rev=`echo "${rev}" | grep '^Revision' | awk '{print $NF}'`
#	printf -- '-svn%s' $rev
#fi





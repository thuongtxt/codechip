/*-----------------------------------------------------------------------------
*
* COPYRIGHT (C) 2006 Arrive Technologies Inc.
*
* The information contained herein is confidential property of Arrive Tecnologies. 
* The use, copying, transfer or disclosure of such information 
* is prohibited except by express written agreement with Arrive Technologies.
*
* Module      : showtable
*
* File        : showtable.c
*
* Created Date: 4-May-06
*
* Description : This module expresses the way to use showtable
*
* Notes       : In the case  your number of colum > cShowTableTabColMax(20)
				or your number of row > cShowTableTabRowMax(100)
				or sum of length of all colums > 179, cannot print table.
*----------------------------------------------------------------------------*/



/*--------------------------------Includes------------------------------------*/
#include "attypes.h"
#include "AtOsal.h"
#include "stdtable.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/ 
/* Maximum colum number for each table */
#define cShowTableMaxColNum 30

/* Color code */
#define cTextNormal     0
#define cTextRed        31
#define cTextGreen      32
#define cTextYellow     33
#define cTextBlue       34
#define cTextMagenta    35
#define cTextCyan       36
#define cTextWhite      10

#define cMaxCharOfLine			1024
#define cShowTableTime2Sleep    100 /* microseconds */
/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eTabDisplayMode m_tableDisplayMode = cTabDisplayModeStandard;

/*--------------------------- Forward declaration ----------------------------*/

static void  HorLinePrint(uint32 colCount, uint32* pColWidth);
static void  VerLinePrint(uint32 colCount, uint32* pColWidth);

static void HorLineToStr(uint32 colCount,uint32* pColWidth, char* pStr);
static void VerLineToStr(uint32 colCount,uint32* pColWidth, char* pStr);
static void StdTablePrintXmlFormat(tStdTab* pTab);
static void StdTablePrintStandardFormat(tStdTab* pTab, eBool printTitle);

/*--------------------------- Macros -----------------------------------------*/ 

/*--------------------------- Implementations --------------------------------*/

/*-----------------------------------------------------------------------------
Function Name: StdTableInit

Description  : initialize table

Input        : tab				- tStdTable structure 
			   rowCount			- number of row
			   colCount			- number of colum
	
Output       : tab				- tStdTable structure 

Return Code  : cAtTrue				- If initializing is successful
			   cAtFalse			- If initializing is failed
------------------------------------------------------------------------------*/
eBool StdTableInit (tStdTab* pTab, uint32 rowCount, uint32 colCount)
	{
	uint32 row;
	uint32 col;

	if (pTab == NULL) 
	    {
		return cAtFalse;
	    }
	if (colCount > cShowTableMaxColNum)
		{
		return cAtFalse;
		}

	pTab->rowCount = rowCount;
	pTab->colCount = colCount;
	

	if( (pTab->cell = (tStdTabCell**)AtOsalMemAlloc(rowCount * sizeof(tStdTabCell*))) == NULL )
		{
		return cAtFalse;
		}
	for (row = 0; row < rowCount; row++) 
	    {
		if( (pTab->cell[row] = (tStdTabCell*)AtOsalMemAlloc(colCount * sizeof(tStdTabCell))) == NULL )
			{
			return cAtFalse;
			}
	    }

	if( (pTab->tittle = (tStdTabCell*)AtOsalMemAlloc(colCount * sizeof(tStdTabCell))) == NULL )
		{
		return cAtFalse;
		}

	for (col = 0; col < colCount; col++)
	    {
		pTab->tittle[col].str = NULL;
		pTab->tittle[col].colorCode = cTextNormal;
	    }

	for (row = 0; row < rowCount; row++)
	    {
		for (col = 0; col < colCount; col++)
			{
			pTab->cell[row][col].str = NULL;
			pTab->cell[row][col].colorCode = cTextNormal;
			}
	    }

	return cAtTrue;
	}
/*-----------------------------------------------------------------------------
Function Name: StdTableStrToCell

Description  : Put data into cell of table

Input        : pCell			- pointer to cell
			   str				- pointer to data
Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
eBool StdTableStrToCell   (tStdTabCell* pCell, const char* str)
	{
	eBool result;

	if (str != NULL)
		{
		/* Check if pChell has existed, free it and malloc it again */
		if (pCell->str != NULL)
			{
			AtOsalMemFree(pCell->str);
			}
		if ( (pCell->str = (char*) AtOsalMemAlloc(AtStrlen(str) + 1)) != NULL )
			{
			AtStrcpy(pCell->str, str);
			pCell->colorCode = cTextNormal;
			result = cAtTrue;
			}
		else
			{
			result = cAtFalse;
			}
		}
	else
		{
		result = cAtFalse;
		}
	return result;
	}

/*-----------------------------------------------------------------------------
Function Name: StdTableColorStrToCell

Description  : Put data into cell of table with color

Input        : pCell			- pointer to cell
			   str				- pointer to data
			   color			- color code
Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
eBool StdTableColorStrToCell   (tStdTabCell* pCell, char* str, uint8 color)
	{
	eBool result;

	if (str != NULL)
		{
		/* Set color for cell */
		pCell->colorCode = color;

		/* Check if pChell has existed, free it and malloc it again */
		if (pCell->str != NULL)
			{
			AtOsalMemFree(pCell->str);
			}
		if ((pCell->str = (char*) AtOsalMemAlloc(AtStrlen(str) + 1)) != NULL )
			{
			AtStrcpy(pCell->str, str);
			result = cAtTrue;
			}
		else
			{
			result = cAtFalse;
			}
		}
	else
		{
		result = cAtFalse;
		}
	return result;
	}

/*-----------------------------------------------------------------------------
Function Name: StdTableStrAdd

Description  : Put data into cell of table according to row and col index.

Input        : pTab				- pointer to table
				row				- row order of cell where putting data
				col				- col order of cell where putting data
				str				- data has string format
Output       : none

Return Code  : cAtTrue				- if putting succeeded. 
			   cAtFalse			- if putting cAtFalse. In that case, one of reason 
								is row or col is greater than row or colum of 
								table.

------------------------------------------------------------------------------*/
eBool StdTableStrAdd(tStdTab* pTab, uint32 row, uint32 col, char* str)
	{
	if ((pTab == NULL) ||
		(row >= pTab->rowCount)||
		(col >= pTab->colCount))
		{
		return cAtFalse;
		}

	return StdTableStrToCell(&(pTab->cell[row][col]), str);
	 
	}


/*-----------------------------------------------------------------------------
Function Name:	StdTablePrint

Description  :	Print data from tStdTable structure.

Input        :	pTab				- pointer to tStdTable structure 
				printTitle			- True if want to print title of table
									  False if don't want to print title of table
Output       :	None

Return Code  :	None
------------------------------------------------------------------------------*/
void StdTablePrint(tStdTab* pTab, eBool printTitle)
	{
    if (m_tableDisplayMode == cTabDisplayModeXml)
        StdTablePrintXmlFormat(pTab);
    else
        StdTablePrintStandardFormat(pTab, printTitle);
	}
	
/*-----------------------------------------------------------------------------
Function Name: StdTableSubTablePrint

Description  : Print sub table of table.

Input        : pTab				- pointer to tStdTable structure 
				rowFrom			- row order of begin row in  table
				rowTo			- row order of end row in  table
				colFrom			- col order of begin col in  table
				colTo			- col order of end col in  table
				printTitle		- notify that print tittle or not
Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
void  StdTableSubTablePrint(tStdTab* pTab, uint32 rowFrom, uint32 rowTo, uint32 colFrom, uint32 colTo, eBool printTitle)
	{
	uint32* pColWidth;
	uint32 i;
	uint32 lenLine;
	uint32 row;
	uint32 col;

	/* Check first */
	if (pTab != NULL
		&& rowFrom < pTab->rowCount
		&& rowTo < pTab->rowCount
		&& rowFrom <= rowTo
		&& colFrom < pTab->colCount
		&& colTo < pTab->colCount
		&& colFrom <= colTo)
		{

		/* Initiate pColWidth */
		pColWidth = (uint32*)AtOsalMemAlloc(sizeof(uint32) * (colTo - colFrom + 1));

		if (pColWidth != NULL)
			{
			if (printTitle == cAtTrue) 
				{
				for(col = 0 ; col < (colTo - colFrom + 1); col++)
					{
					if (pTab->tittle[col + colFrom].str == NULL)
						{
						pColWidth[col] = 0;
						}
					else
						{
						pColWidth[col] = AtStrlen(pTab->tittle[col + colFrom].str) ;
						}
					}
				}
			else
				{
				for(col = 0 ; col < (colTo - colFrom + 1); col++)
					{
					pColWidth[col] = 0 ;
					}
				}

			/* Find max length of cells in each colum */
			for (col = 0; col < (colTo - colFrom + 1); col++) 
				{
				for (row = 0; row < (rowTo - rowFrom + 1); row++) 
					{
					if (pTab->cell[row + rowFrom][col + colFrom].str != NULL)
						{
						if (AtStrlen(pTab->cell[row + rowFrom][col +  colFrom].str) > pColWidth[col])
							{
							pColWidth[col] = AtStrlen(pTab->cell[row + rowFrom][col +  colFrom].str);
							}
						}

					}
				}

			/* Count the length of horizontal line */
			lenLine = 1;
			for(col = 0; col < (colTo - colFrom + 1); col++)
				{
				lenLine = lenLine + pColWidth[col] + 1;
				}

			/* Print table */
			if (printTitle)
				{

				/* Print tittle first */
				HorLinePrint(colTo - colFrom + 1, pColWidth);
				VerLinePrint(colTo - colFrom + 1, pColWidth);
				/*printf("|");*/
                AtPrintc(cTextNormal, "|");
				for (col = 0; col < (colTo - colFrom + 1); col++) 
					{
					if (pTab->tittle[col + colFrom].str == NULL)
						{
						for(i = 0; i < pColWidth[col]; i++)
							{
							/*printf(" ");*/
                            AtPrintc(cTextNormal, " ");
							}
						}
					else
						{
						/* Set color */
						/*printf("\033[1;%dm", pTab->tittle[col + colFrom].colorCode);	

						printf("%s", pTab->tittle[col + colFrom].str);*/

						/* Clear color */
						/*printf("\033[0m");	*/
						AtPrintc(pTab->tittle[col + colFrom].colorCode, "%s", pTab->tittle[col + colFrom].str);

                        /*OsalUSleep(cShowTableTime2Sleep);*/

						for(i = 0; i < (pColWidth[col] - AtStrlen(pTab->tittle[col + colFrom].str)); i++)
							{
							/*printf(" ");*/
                            AtPrintc(cTextNormal, " ");
							}
						}
					/*printf("|");*/
                    AtPrintc(cTextNormal, "|");
					}
				/*printf("\n");*/
                AtPrintc(cTextNormal, "\r\n");
				VerLinePrint(colTo - colFrom + 1, pColWidth);
				}

			/* Second print all cells of each row */
			for (row = 0; row < (rowTo - rowFrom + 1); row++) 
				{
				HorLinePrint(colTo - colFrom + 1, pColWidth);
				/*printf("|");*/
                AtPrintc(cTextNormal, "|");
				for(col = 0; col < (colTo - colFrom + 1); col++)
					{
					if (pTab->cell[row + rowFrom][col + colFrom].str == NULL)
						{
						for (i = 0; i < pColWidth[col]; i++) 
							{
							/*printf(" ");*/
                            AtPrintc(cTextNormal, " ");
							}
						}
					else
						{
						/* Set color */
						/*printf("\033[1;%dm", pTab->cell[row + rowFrom][col + colFrom].colorCode);	

						printf("%s", pTab->cell[row + rowFrom][col + colFrom].str);*/

						/* Clear color */
						/*printf("\033[0m");*/	
						AtPrintc(pTab->cell[row + rowFrom][col + colFrom].colorCode, "%s", pTab->cell[row + rowFrom][col + colFrom].str);
                        
                        /*OsalUSleep(cShowTableTime2Sleep);*/

						for (i = 0; i < (pColWidth[col] - AtStrlen(pTab->cell[row + rowFrom][col + colFrom].str)); i++)
							{
							/*printf(" ");*/
                            AtPrintc(cTextNormal, " ");
							}
						}

					/*printf("|");*/
                    AtPrintc(cTextNormal, "|");
					}
				/*printf("\n");*/
                AtPrintc(cTextNormal, "\r\n");
				}

			HorLinePrint((colTo - colFrom + 1), pColWidth);
				
			AtOsalMemFree(pColWidth);
			}
		else
			{
			/*printf("Cannot print table\n");*/
            AtPrintc(cTextNormal, "Cannot print table\r\n");
			}
		}
	else
		{
		/*printf("Cannot print table\n");*/
        AtPrintc(cTextNormal, "Cannot print table\r\n");
		}
	}	

/*-----------------------------------------------------------------------------
Function Name: StdTableFree

Description  : Free the allocated memory of table.

Input        : pTab				- pointer to tStdTable structure 

Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
void StdTableFree(tStdTab* pTab)
	{
	uint32 row;
	uint32 col;

	if (pTab != NULL)
	    {
		for (row = 0; row < pTab->rowCount; row++)
			for (col = 0; col < pTab->colCount; col++)
			    {
				if (pTab->cell[row][col].str != NULL)
				    {
					AtOsalMemFree(pTab->cell[row][col].str);
					pTab->cell[row][col].str = NULL;
				    }
			    }
		for (col = 0; col < pTab->colCount; col++)
		    {
			if (pTab->tittle[col].str != NULL)
			    {
				AtOsalMemFree(pTab->tittle[col].str);
				pTab->tittle[col].str = NULL;
			    }
		    }
		for (row = 0; row < pTab->rowCount; row++) 
			{
			AtOsalMemFree(pTab->cell[row]);
			pTab->cell[row] = NULL;
			}
		AtOsalMemFree(pTab->tittle);
		pTab->tittle = NULL;
		AtOsalMemFree(pTab->cell);
		pTab->cell = NULL;

	    }
	}

/*-----------------------------------------------------------------------------
Function Name: HorLinePrint

Description  : Print a horizontal line

Input        : colCount				- The number of colum 
			   pColWidth			- Array of the colum's width

Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
static void HorLinePrint(uint32 colCount, uint32* pColWidth)
	{
	uint32 j;
	uint32 col;
	char *pStrLine;

	pStrLine = (char *)AtOsalMemAlloc(cMaxCharOfLine);
	if(pStrLine == NULL)
		{
		return;
		}
	AtOsalMemInit(pStrLine, 0, cMaxCharOfLine);
	AtStrcat(pStrLine, "+");
	for (col = 0; col < colCount; col++) 
		{
		for(j = 0; j < pColWidth[col]; j++)
			{
			AtStrcat(pStrLine, "-");
			}
		AtStrcat(pStrLine, "+");
		}
    AtPrintc(cTextNormal, "%s", pStrLine);
    AtPrintc(cTextNormal, "\r\n");

	AtOsalMemFree(pStrLine);
	}

/*-----------------------------------------------------------------------------
Function Name: VerLinePrint

Description  : Print a vertical line

Input        : colCount				- The number of colum
			   pColWidth			- Array of the colum's width

Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
static void VerLinePrint(uint32 colCount,uint32* pColWidth)
	{
	uint32 j;
	uint32 col;
	char *pStrLine;

	pStrLine = (char *)AtOsalMemAlloc(cMaxCharOfLine);
	if(pStrLine == NULL)
		{
		return;
		}
	AtOsalMemInit(pStrLine, 0, cMaxCharOfLine);

	AtStrcat(pStrLine, "|");
	for (col = 0; col < colCount; col++) 
	    {
		for(j = 0; j < pColWidth[col]; j++)
			{
			AtStrcat(pStrLine, " ");
			}
		AtStrcat(pStrLine, "|");
	    }
    AtPrintc(cTextNormal, "%s", pStrLine);
    AtPrintc(cTextNormal, "\r\n");
	AtOsalMemFree(pStrLine);
	}

/*-----------------------------------------------------------------------------
Function Name:	StdTableToString

Description  :	Convert whole table to string

Input        :	pTab				- pointer to tStdTable structure
				printTitle			- True if want to print title of table
									  False if don't want to print title of table
			 
Output       :	pTableStr			- Pointer to string

Return Code  :	None
------------------------------------------------------------------------------*/
eBool StdTableToString(tStdTab* pTab, eBool printTitle, char* pTableStr)
	{
	uint32 colWidth[cShowTableMaxColNum];
	uint32 i;
	uint32 tableWidth;
	uint32 row;
	uint32 col;
	uint32 lineCount;
	char	colorCode[30];
	uint32	charCountOfTable;

	AtOsalMemInit(colorCode, 0, 30);
	AtOsalMemInit(colWidth, 0, sizeof(colWidth));

	pTableStr[0] = 0;

	/* Initiate colWidth */
	if (printTitle) 
		{
		for(col = 0 ; col < (pTab->colCount); col++)
			{
			if (pTab->tittle[col].str == NULL)
				{
				colWidth[col] = 0;
				}
			else
				{
				colWidth[col] = AtStrlen(pTab->tittle[col].str) ;
				}
			}
		}
	else
		{
		for(col = 0 ; col < (pTab->colCount); col++)
			{
			colWidth[col] = 0 ;
			}
		}

	/* Find max length of cells in each colum */
	for (col = 0; col < pTab->colCount; col++) 
		{
		for (row = 0; row < pTab->rowCount; row++) 
			{
			if (pTab->cell[row][col].str != NULL)
				{
				if (AtStrlen(pTab->cell[row][col].str) > colWidth[col])
					{
					colWidth[col] = AtStrlen(pTab->cell[row][col].str);
					}
				}
			}
		}

	/* Calculate width of table */
	tableWidth = 1; /* First '+' */
	for(col = 0; col < pTab->colCount; col++)
		{
		tableWidth = tableWidth + colWidth[col] + 1;
		}
	tableWidth += 1; /* Adding '\n' to each line */

	lineCount = 2 * pTab->rowCount + 5;
	charCountOfTable = (tableWidth * lineCount);
	charCountOfTable += (pTab->colCount * (pTab->rowCount + 1) * 14); /* Color code */

	/* Print table */
	if (printTitle)
		{

		/* Print tittle first */
		HorLineToStr(pTab->colCount, colWidth, pTableStr);
		VerLineToStr(pTab->colCount, colWidth, pTableStr);
		AtStrcat(pTableStr, "|");
		for (col = 0; col < pTab->colCount; col++) 
			{
			if (pTab->tittle[col].str == NULL)
				{
				for(i = 0; i < colWidth[col]; i++)
					{
					AtStrcat(pTableStr, " ");
					}
				}
			else
				{
#ifdef PRINT_COLOR
				/* Set color */
				AtSprintf(colorCode, "\033[1;%dm", pTab->tittle[col].colorCode);
				AtStrcat(pTableStr, colorCode);
#endif /* PRINT_COLOR */

				AtStrcat(pTableStr, pTab->tittle[col].str);

#ifdef PRINT_COLOR
				/* Clear color */
				AtStrcat(pTableStr, "\033[0m");
#endif /* PRINT_COLOR */
				for(i = 0; i < (colWidth[col] - AtStrlen(pTab->tittle[col].str)); i++)
					{
					AtStrcat(pTableStr, " ");
					}
				}
			AtStrcat(pTableStr, "|");
			}
		AtStrcat(pTableStr, "\r\n");
		VerLineToStr(pTab->colCount, colWidth, pTableStr);
		}

	/* Second print all cells of each row */
	for (row = 0; row < pTab->rowCount; row++) 
		{
		HorLineToStr(pTab->colCount, colWidth, pTableStr);
		AtStrcat(pTableStr, "|");
		for(col = 0; col < pTab->colCount; col++)
			{
			if (pTab->cell[row][col].str == NULL)
				{
				for (i = 0; i < colWidth[col]; i++) 
					{
					AtStrcat(pTableStr, " ");
					}
				}
			else
				{
#ifdef PRINT_COLOR
				/* Set color */
				AtSprintf(colorCode, "\033[1;%dm", pTab->cell[row][col].colorCode);
				AtStrcat(pTableStr, colorCode);
#endif /* PRINT_COLOR */

				AtStrcat(pTableStr, pTab->cell[row][col].str);
				
#ifdef PRINT_COLOR
				/* Clear color */
				AtStrcat(pTableStr, "\033[0m");
#endif /* PRINT_COLOR */

				for (i = 0; i < (colWidth[col] - AtStrlen(pTab->cell[row][col].str)); i++)
					{
					AtStrcat(pTableStr, " ");
					}
				}

			AtStrcat(pTableStr, "|");
			}
		AtStrcat(pTableStr, "\r\n");
		}

	HorLineToStr(pTab->colCount, colWidth, pTableStr);

	return cAtTrue;
	}

/*-----------------------------------------------------------------------------
Function Name:	HorLineToStr

Description  :	Print horizontal line to string

Input        :	colCount			- The number of colum 
				pColWidth			- Array of the colum's width

Output       :	pStr				- String to be printed

Return Code  :	None
------------------------------------------------------------------------------*/
static void HorLineToStr(uint32 colCount, uint32* pColWidth, char* pStr)
	{
	uint32 j;
	uint32 col;

	AtStrcat(pStr, "+");
	for (col = 0; col < colCount; col++) 
		{
		for(j = 0; j < pColWidth[col]; j++)
			{
			AtStrcat(pStr, "-");
			}
		AtStrcat(pStr, "+");
		}
	AtStrcat(pStr, "\r\n");
	}

/*-----------------------------------------------------------------------------
Function Name:	VerLineToStr

Description  :	Print vertical line to string

Input        :	colCount			- The number of colum
				pColWidth			- Array of the colum's width

Output       :	pStr				- String to be printed

Return Code  :	None
------------------------------------------------------------------------------*/
static void VerLineToStr(uint32 colCount,uint32* pColWidth, char* pStr)
	{
	uint32 j;
	uint32 col;

	AtStrcat(pStr, "|");
	for (col = 0; col < colCount; col++) 
	    {
		for(j = 0; j < pColWidth[col]; j++)
			{
			AtStrcat(pStr, " ");
			}
		AtStrcat(pStr, "|");
	    }
	AtStrcat(pStr, "\r\n");
	}

/*
 * To print the table in XML format
 */
static void StdTablePrintXmlFormat(tStdTab* pTab)
    {
    uint32 row;
    uint32 col;

    AtPrintc(cTextNormal, "<Table name=\"StdTablePrint\">\n");

    /* Second print all cells of each row */
    for (row = 0; row < pTab->rowCount; row++)
        {
        AtPrintc(cTextNormal, "<Row index=\"%u\"> ", row);
        for(col = 0; col < pTab->colCount; col++)
            AtPrintc(cTextNormal, "<Cell name=\"%s\" value=\"%s\"/>", pTab->tittle[col].str,
                     pTab->cell[row][col].str ? pTab->cell[row][col].str : "" );
        AtPrintc(cTextNormal, "</Row>\n");
        }

    AtPrintc(cTextNormal, "</Table>\n");
    }

/*
 * To print table in standard format
 */
static void StdTablePrintStandardFormat(tStdTab* pTab, eBool printTitle)
    {
    uint32 colWidth[cShowTableMaxColNum];
    uint32 i;
    uint32 lenLine;
    uint32 row;
    uint32 col;

    AtOsalMemInit(colWidth, 0, sizeof(colWidth));

    AtPrintc(cSevNormal, "\r\n");

    /* Initiate colWidth */
    if (printTitle)
        {
        for(col = 0 ; col < (pTab->colCount); col++)
            {
            if (pTab->tittle[col].str == NULL)
                {
                colWidth[col] = 0;
                }
            else
                {
                colWidth[col] = AtStrlen(pTab->tittle[col].str) ;
                }
            }
        }
    else
        {
        for(col = 0 ; col < (pTab->colCount); col++)
            {
            colWidth[col] = 0 ;
            }
        }

    /* Find max length of cells in each colum */
    for (col = 0; col < pTab->colCount; col++)
        {
        for (row = 0; row < pTab->rowCount; row++)
            {
            if (pTab->cell[row][col].str != NULL)
                {
                if (AtStrlen(pTab->cell[row][col].str) > colWidth[col])
                    {
                    colWidth[col] = AtStrlen(pTab->cell[row][col].str);
                    }
                }

            }
        }

    /* Count the length of horizontal line */
    lenLine = 1;
    for(col = 0; col < pTab->colCount; col++)
        {
        lenLine = lenLine + colWidth[col] + 1;
        }

    /* Print table */
    if (printTitle)
        {
        /* Print tittle first */
        HorLinePrint(pTab->colCount, colWidth);
        VerLinePrint(pTab->colCount, colWidth);

        AtPrintc(cTextNormal, "|");

        for (col = 0; col < pTab->colCount; col++)
            {
            if (pTab->tittle[col].str == NULL)
                {
                for(i = 0; i < colWidth[col]; i++)
                    {
                    /*printf(" ");*/
                    AtPrintc(cTextNormal, " ");
                    }
                }
            else
                {
                /* Set color */
                /*
                printf("\033[1;%dm", pTab->tittle[col].colorCode);

                printf("%s", pTab->tittle[col].str);
                */
                /* Clear color */
                /*printf("\033[0m");    */
                AtPrintc(pTab->tittle[col].colorCode, "%s", pTab->tittle[col].str);

                /*OsalUSleep(cShowTableTime2Sleep);*/

                for(i = 0; i < (colWidth[col] - AtStrlen(pTab->tittle[col].str)); i++)
                    {
                    /*printf(" ");*/
                    AtPrintc(cTextNormal, " ");
                    }
                }

            /*printf("|");*/
            AtPrintc(cTextNormal, "|");
            }

        AtPrintc(cTextNormal, "\r\n");
        VerLinePrint(pTab->colCount, colWidth);
        }

    /* Second print all cells of each row */
    for (row = 0; row < pTab->rowCount; row++)
        {
        HorLinePrint(pTab->colCount, colWidth);

        AtPrintc(cTextNormal, "|");

        for(col = 0; col < pTab->colCount; col++)
            {
            if (pTab->cell[row][col].str == NULL)
                {
                for (i = 0; i < colWidth[col]; i++)
                    {
                    /*printf(" ");*/
                	AtPrintc(cTextNormal, " ");
                    }
                }
            else
                {
                /* Set color */
                /*printf("\033[1;%dm", pTab->cell[row][col].colorCode);

                printf("%s", pTab->cell[row][col].str);*/

                /* Clear color */
                /*printf("\033[0m");*/
            	AtPrintc(pTab->cell[row][col].colorCode, "%s", pTab->cell[row][col].str);

                /*OsalUSleep(cShowTableTime2Sleep);*/

                for (i = 0; i < (colWidth[col] - AtStrlen(pTab->cell[row][col].str)); i++)
                    {
                    /*printf(" ");*/
                	AtPrintc(cTextNormal, " ");
                    }
                }

            /*printf("|");*/
            AtPrintc(cTextNormal, "|");
            }
        /*printf("\n");*/
        AtPrintc(cTextNormal, "\r\n");
        }

    HorLinePrint(pTab->colCount, colWidth);
    }

/**
 * To set table display mode
 *
 * @param mode Display mode. See @ref eTabDisplayMode "Table displaying mode"
 */
void StdTableDisplayModeSet(eTabDisplayMode mode)
    {
    m_tableDisplayMode = mode;
    }

/**
 * To get current table displaying mode
 * @return See @ref eTabDisplayMode "Table displaying mode"
 */
eTabDisplayMode StdTableDisplayModeGet(void)
    {
    return m_tableDisplayMode;
    }

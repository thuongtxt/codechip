/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtCliRichClient.c
 *
 * Created Date: Aug 28, 2015
 *
 * Description : Rich CLI client that support history and auto completing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtCliService.h"
#include "AtOsal.h"
#include "AtConnectionManager.h"
#include "AtConnection.h"
#include "AtCliModule.h"
#include "AtObject.h"
#include "../src/AtTinyTextUIInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtTinyRemoteTextUI *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtTinyRemoteTextUI
    {
    tAtTextUI super;

    /* Private data */
    AtConnection connection;
    }tAtTinyRemoteTextUI;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTextUIMethods m_AtTextUIOverride;

static AtTextUI m_tinyTextUI  = NULL;
static AtTextUI m_clishTextUI = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CliProcess(AtTextUI self, const char * cli)
    {
    static char commandBuffer[1024];
    AtConnection m_connection = mThis(self)->connection;
    char *buffer = AtConnectionDataBuffer(m_connection);
    uint32 bufferSize = AtConnectionDataBufferSize(m_connection);
    int numBytes;

    /* Send to server */
    AtOsalMemCpy(commandBuffer, cli, AtStrlen(cli));
    if (AtConnectionSend(m_connection, commandBuffer, AtStrlen(cli)) < 0)
        {
        AtPrintc(cSevCritical, "ERROR: send command %s fail\r\n", cli);
        return cAtFalse;
        }

    /* Must receive finish indication */
    while (1)
        {
        AtOsalMemInit(buffer, 0, bufferSize);

        numBytes = AtConnectionReceive(m_connection);
        if (numBytes < 0)
            break;

        if (AtStrcmp(buffer, "cli:finish") == 0)
            break;

        if (numBytes > 0)
            AtPrintf("%s", AtConnectionDataBuffer(m_connection));
        }

    return cAtTrue;
    }

static void OverrideAtTextUI(AtTextUI self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtTextUIOverride, self->methods, sizeof(m_AtTextUIOverride));

        mMethodOverride(m_AtTextUIOverride, CliProcess);
        }

    mMethodsSet(self, &m_AtTextUIOverride);
    }

static void Override(AtTextUI self)
    {
    OverrideAtTextUI(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTinyRemoteTextUI);
    }

static AtTextUI ObjectInit(AtTextUI self, AtConnection connection)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtTextUIObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->connection = connection;

    return self;
    }

static AtTextUI TextUINew(AtConnection connection)
    {
    AtTextUI newTextUI = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newTextUI, connection);
    }

static AtConnection CreateConnection(const char *serverAddress, uint16 port)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    return AtConnectionManagerClientConnectionCreate(manager, serverAddress, port);
    }

static void CliStart(AtConnection connection)
    {
    m_tinyTextUI  = TextUINew(connection);
    m_clishTextUI = AtClishTextUINew(m_tinyTextUI);

    AtTextUICmdBufferSizeSet(m_clishTextUI, 1024 * 1024);
    AtCliStartWithTextUI(m_clishTextUI);
    }

void AtCliClientRichStart(const char *serverIpAddress, uint16 port)
    {
    AtConnection connection = CreateConnection(serverIpAddress, port);

    if (AtConnectionSetup(connection) != cAtConnectionOk)
        {
        AtPrintc(cSevCritical, "ERROR: %s\r\n", AtConnectionErrorDescription(connection));
        return;
        }

    AtPrintc(cSevInfo, "Server: %s, port: %u\r\n", serverIpAddress, port);

    CliStart(connection);

    /* Clean up */
    AtTextUIDelete(m_clishTextUI);
    AtTextUIDelete(m_tinyTextUI);
    AtConnectionDelete(connection);
    }

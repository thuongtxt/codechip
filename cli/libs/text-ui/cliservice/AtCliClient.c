/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtCliClient.c
 *
 * Created Date: Jun 3, 2015
 *
 * Description : CLI client
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtCliService.h"
#include "AtFile.h"
#include "AtOsal.h"
#include "AtConnectionManager.h"
#include "AtConnection.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtConnection m_connection = NULL;

/* Listner */
static const tAtCliClientListener *m_listener = NULL;
static void *m_userData = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConnection CreateConnection(const char *serverAddress, uint16 port)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    return AtConnectionManagerClientConnectionCreate(manager, serverAddress, port);
    }

static char *CliGet(char *commandBuffer, uint32 bufferSize)
    {
    AtFile stdIn = AtStdStandardInput(AtStdSharedStdGet());
    char *cli = AtFileGets(stdIn, commandBuffer, bufferSize);

    if (cli[AtStrlen(cli) - 1] == '\n')
        cli[AtStrlen(cli) - 1] = '\0';

    if (AtStrlen(cli) == 0)
        return NULL;

    return commandBuffer;
    }

void AtCliClientStart(const char *serverIpAddress, uint16 port)
    {
    /* Setup connection */
    AtCliClientConnect(serverIpAddress, port);
    if (!AtCliClientConnected())
        {
        AtPrintc(cSevCritical, "ERROR: Connect fail\r\n");
        return;
        }

    AtPrintc(cSevInfo, "Server: %s, port: %u\r\n", serverIpAddress, port);

    while (1)
        {
        static char commandBuffer[1024];
        char *cli;

        /* Prompt */
        AtPrintc(cSevNormal, "AT SDK >");

        /* Get input CLI */
        cli = CliGet(commandBuffer, sizeof(commandBuffer));
        if (AtStrcmp(cli, "exit") == 0)
            break;

        /* Use just input nothing but still ENTER */
        if (AtStrlen(cli) == 0)
            continue;

        AtCliClientExecute(commandBuffer);
        }

    AtCliClientDisconnect();
    }

void AtCliClientConnect(const char *serverIpAddress, uint16 port)
    {
    AtCliClientDisconnect();
    m_connection = CreateConnection(serverIpAddress, port);

    if (AtConnectionSetup(m_connection) != cAtConnectionOk)
        AtPrintc(cSevCritical, "ERROR: %s\r\n", AtConnectionErrorDescription(m_connection));
    }

void AtCliClientDisconnect(void)
    {
    AtConnectionDelete(m_connection);
    m_connection = NULL;
    }

eBool AtCliClientConnected(void)
    {
    return m_connection ? cAtTrue : cAtFalse;
    }

eBool AtCliClientExecute(const char *cli)
    {
    uint32 length = AtStrlen(cli);

    if (length == 0)
        return cAtTrue;

    /* Send to server */
    if (AtConnectionSend(m_connection, cli, length) < 0)
        {
        AtPrintc(cSevCritical, "ERROR: send command %s fail\r\n", cli);
        return cAtFalse;
        }

    if (m_listener && m_listener->DidSendCli)
        m_listener->DidSendCli(cli, m_userData);

    /* Must receive finish indication */
    while (1)
        {
        char *buffer = AtConnectionDataBuffer(m_connection);
        uint32 bufferSize = AtConnectionDataBufferSize(m_connection);
        int numBytes;

        AtOsalMemInit(buffer, 0, bufferSize);

        numBytes = AtConnectionReceive(m_connection);
        if (numBytes < 0)
            break;

        if (AtStrcmp(buffer, "cli:finish") == 0)
            {
            if (m_listener && m_listener->DidFinish)
                m_listener->DidFinish(m_userData);
            break;
            }

        if (numBytes > 0)
            {
            eBool shouldPrint = cAtTrue;
            char *data = AtConnectionDataBuffer(m_connection);

            if (m_listener)
                shouldPrint = m_listener->ShouldPrintResult(cli);
            if (shouldPrint)
                AtPrintf("%s", data);

            if (m_listener && m_listener->DidReceiveData)
                m_listener->DidReceiveData(data, AtStrlen(data), m_userData);
            }
        }

    return cAtTrue;
    }

AtConnection AtCliClientConnection(void)
    {
    return m_connection;
    }

void AtCliClientListenerSet(const tAtCliClientListener *listener, void *userData)
    {
    m_listener = listener;
    m_userData = userData;
    }

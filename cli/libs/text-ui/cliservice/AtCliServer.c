/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : cliserver.c
 *
 * Created Date: Jun 3, 2015
 *
 * Description : CLI server
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliService.h"

#include "atclib.h"
#include "AtConnectionManager.h"
#include "AtConnection.h"
#include "AtCliModule.h"
#include "AtTask.h"

/*--------------------------- Define -----------------------------------------*/
#define cDataBufferSize    1024
#define cNumPacketsToRest  200
#define sEndMessage "cli:finish"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtCliServer
    {
    AtConnection connection;
    AtTask task;
    const char *bindAddress;
    uint16 port;
    char cache[cDataBufferSize];
    uint32 numSentPackets;
    }tAtCliServer;

typedef struct tAtCliServer * AtCliServer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtCliServer Server(void)
    {
    static tAtCliServer server;
    static AtCliServer pServer = NULL;

    if (pServer)
        return pServer;

    AtOsalMemInit(&server, 0, sizeof(server));
    pServer = &server;

    return pServer;
    }

static eBool ShouldRest(AtCliServer server)
    {
    if ((server->numSentPackets % cNumPacketsToRest) == 0)
        return cAtTrue;
    return cAtFalse;
    }

static void Rest(AtCliServer server)
    {
    AtOsalUSleep(70000);
    server->numSentPackets = 0;
    }

static int Send(AtCliServer server, const char *buffer, uint32 length)
    {
    int ret = AtConnectionSend(server->connection, buffer, length);
    server->numSentPackets = server->numSentPackets + 1;

    if (ret < 0)
        AtPrintc(cSevCritical, "ERROR: send %s fail\r\n", buffer);

    if (ShouldRest(server))
        Rest(server);

    return ret;
    }

static eBool CanCache(AtCliServer server, char *aString)
    {
    if ((AtStrlen(aString) + AtStrlen(server->cache)) < cDataBufferSize)
        return cAtTrue;
    return cAtFalse;
    }

static int StringCache(AtCliServer server, char *aString)
    {
    AtStrcat(server->cache, aString);
    return 0;
    }

static int CacheFlush(AtCliServer server)
    {
    int ret;

    if (AtStrlen(server->cache) == 0)
        return 0;

    ret = Send(server, server->cache, AtStrlen(server->cache));
    AtOsalMemInit(server->cache, 0, cDataBufferSize);

    return ret;
    }

static int SocketPrintFunc(void *appContext, char *aString)
    {
    int ret = 0;
    AtCliServer server = Server();

    AtUnused(appContext);

    if (server->connection == NULL)
        return 0;

    if (CanCache(server, aString))
        return StringCache(server, aString);

    /* Send all of content in cache */
    ret = CacheFlush(server);

    /* New string to be sent can be cached */
    if (CanCache(server, aString))
        {
        StringCache(server, aString);
        return ret;
        }

    /* If cache cannot hold it, just send */
    ret += Send(server, aString, AtStrlen(aString));

    return ret;
    }

static AtConnection AnyConnectionCreate(AtCliServer server)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    static const uint32 cDefaultPort = 5678;
    static const uint32 cNumPorts = 1024;
    uint16 port_i;

    for (port_i = 0; port_i < cNumPorts; port_i++)
        {
        uint16 port = (uint16)(cDefaultPort + port_i);
        AtConnection newConnection = AtConnectionManagerServerConnectionCreate(manager, server->bindAddress, port);
        if (AtConnectionSetup(newConnection) == cAtConnectionOk)
            return newConnection;
        AtConnectionDelete(newConnection);
        }

    return NULL;
    }

static AtConnection ConnectionCreate(AtCliServer server)
    {
    AtConnectionManager manager = AtOsalConnectionManager();
    AtConnection newConnection = NULL;

    /* If the port is specified, just use it */
    if (server->port)
        {
        newConnection = AtConnectionManagerServerConnectionCreate(manager, server->bindAddress, server->port);
        if (AtConnectionSetup(newConnection) == cAtConnectionOk)
            return newConnection;
        AtPrintc(cSevCritical, "ERROR: %s\r\n", AtConnectionErrorDescription(newConnection));
        return NULL;
        }

    /* Otherwise, just create any connection */
    return AnyConnectionCreate(server);
    }

static AtConnection ConnectionSetup(AtCliServer server)
    {
    /* If the port is specified, just use it */
    server->connection = ConnectionCreate(server);
    if (server->connection)
        return server->connection;

    AtConnectionDelete(server->connection);
    server->connection = NULL;
    server->task = 0;

    return NULL;
    }

static void *ListenTaskHandler(void *self)
    {
    AtStd std = AtStdSharedStdGet();
    AtCliServer server = self;

    if (ConnectionSetup(server) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Start CLI server fail\r\n");
        return NULL;
        }

    /* Do this run loop */
    while (1)
        {
        int numBytes;
        char *buffer = AtConnectionDataBuffer(server->connection);
        uint32 bufferSize = AtConnectionDataBufferSize(server->connection);

        AtTaskTestCancel(server->task);

        AtOsalMemInit(buffer, 0, bufferSize);

        numBytes = AtConnectionReceive(server->connection);
        if (numBytes)
            {
            AtStdPrintFuncSet(std, SocketPrintFunc);
            AtCliExecute(AtConnectionDataBuffer(server->connection));
            CacheFlush(server);
            Send(server, sEndMessage, AtStrlen(sEndMessage));
            AtStdPrintFuncSet(std, NULL);
            }
        }

    return NULL;
    }

static void CreateListenTask(AtCliServer server)
    {
    if (server->task)
        return;

    server->task = AtTaskManagerTaskCreate(AtOsalTaskManagerGet(), "CliServer", server, ListenTaskHandler);
    if (server->task == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create CLI servers\r\n");
        return;
        }

    if (AtTaskStart(server->task) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot start CLI servers\r\n");
        return;
        }
    }

static void DeleteListenTask(AtCliServer server)
    {
    if (server->task == NULL)
        return;

    if (AtTaskStop(server->task) != cAtOk)
        AtPrintc(cSevCritical, "ERROR: cannot stop CLI server task\r\n");

    if (AtTaskJoin(server->task) != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot CLI server task from stop\r\n");

    AtTaskManagerTaskDelete(AtOsalTaskManagerGet(), server->task);
    server->task = NULL;
    }

void AtCliServerStart(const char *bindAddress, uint16 port)
    {
    AtCliServer server = Server();

    server->bindAddress = bindAddress;
    server->port = port;
    AtCliCpuRestTimeMsSet(0);
    CreateListenTask(server);
    }

void AtCliServerStop(void)
    {
    AtCliServer server = Server();

    DeleteListenTask(server);
    AtConnectionDelete(server->connection);
    server->connection = NULL;
    }

AtConnection AtCliServerConnection(void)
    {
    AtCliServer server = Server();
    return server->connection;
    }

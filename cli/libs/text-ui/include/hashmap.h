/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/


#ifndef HASH_MAP_HEADER
#define HASH_MAP_HEADER

/*--------------------------- Define -----------------------------------------*/
#define cHashMapTableSize       1024 /* Support 1024 CLI with out same HashCode*/

/*--------------------------- Structures -------------------------------------*/
typedef struct tAtHashMapEntry
    {
    uint32 crc32; /**< CRC-32 from Original key, needed for comparison */
    void * data; /**< The data being stored */
    uint32 hashValue; /**< Calculated hash value */
    struct tAtHashMapEntry* next; /**< Next entry */
    } tAtHashMapEntry;

/* The following structure define the information of the TRIE */
typedef struct tAtHashMap
	{
    tAtHashMapEntry** bucket; /**< bucket */
    uint32  count; /**< count */
    uint32  size;  /**< size */
    uint32  collisionCount; /**< collisionCount */
    uint32  maxCollisionPerHash; /**< maxCollisionPerHash */
	} tAtHashMap;



/*--------------------------- Forward Declare---------------------------------*/
int AtHashMapInsert(tAtHashMap* self, void * key, void * data);
void * AtHashMapFind(tAtHashMap* self, void * key);
tAtHashMap *AtHashMapNew(void);
void AtHashMapDelete(tAtHashMap *self);
uint32 AtHashMapCount(const tAtHashMap* self);
uint32 AtHashMapCollisionCount(const tAtHashMap* self);

#endif /* HASH_MAP_HEADER */



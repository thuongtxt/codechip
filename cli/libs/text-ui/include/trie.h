/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/


#ifndef TRIE_HEADER
#define TRIE_HEADER

/*--------------------------- Define -----------------------------------------*/
#define cTrieNodeMaxNumChar		256 /* With sizeof(char) = 1 byte --> 2^8 = 256 */

/*--------------------------- Structures -------------------------------------*/

/* The following structure define the information of the TRIE node */
typedef struct tAtTrieNode
	{
	void * data;
	unsigned int nodeCount;			/* The number of node of TRIE with root is this node */
	struct tAtTrieNode *pNext[cTrieNodeMaxNumChar];
	} tAtTrieNode;

/* The following structure define the information of the TRIE */
typedef struct tAtTrie
	{
	tAtTrieNode *rootNode;
	} tAtTrie;

/*--------------------------- Forward Declare---------------------------------*/

/*------------------------------------------------------------------------------
This function is used to create a new trie
------------------------------------------------------------------------------*/
tAtTrie *AtTrieNew(void);

/*------------------------------------------------------------------------------
This function is used to destroy a trie
------------------------------------------------------------------------------*/
void AtTrieFree(tAtTrie *trie);

/*------------------------------------------------------------------------------
This function is used to insert a new key-value pair into a trie
------------------------------------------------------------------------------*/
int AtTrieInsert(tAtTrie *trie, char *key, void * value);

/*------------------------------------------------------------------------------
This function is used to look up a value from its key in a trie
------------------------------------------------------------------------------*/
void * AtTrieLookup(tAtTrie *trie, char *key);

/*------------------------------------------------------------------------------
This function is used to remove an entry from a trie
------------------------------------------------------------------------------*/
int AtTrieRemove(tAtTrie *trie, char *key);

/*------------------------------------------------------------------------------
This function is used to get the number of node of TRIE
------------------------------------------------------------------------------*/
unsigned int AtTrieNumNodeGet(tAtTrie *trie);

#endif /* TRIE_HEADER */



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/


#ifndef AT_SHORT_REGISTER_HEADER
#define AT_SHORT_REGISTER_HEADER

/*--------------------------- Define -----------------------------------------*/
#define cShortRegisterHashTableSize (cBit18_0 + 1)

/*--------------------------- Structures -------------------------------------*/
typedef struct tAtShortRegisterBackList
    {
    uint32 coreId;
    uint32 address;
    void * next;
    }tAtShortRegisterBackList;

typedef struct tAtShortRegisterEntry
    {
    uint32 coreId;
    uint32 address;
    uint32 mask;
    uint32 value;
    void * next;
    }tAtShortRegisterEntry;

typedef struct tAtShortRegisterAudit
	{
    tAtShortRegisterBackList * blackList;
    tAtShortRegisterEntry* * bucket; /**< bucket */
    uint32  count; /**< count */
    uint32  collisionCount; /**< collisionCount */
    uint32  maxCollisionPerHash; /**< maxCollisionPerHash */
    AtDevice device;
    eBool verbose;
	} tAtShortRegisterAudit;

/*--------------------------- Forward Declare---------------------------------*/
int AtShortRegisterAuditCollect(tAtShortRegisterAudit* self,
                                uint32 address,
                                uint32 value,
                                uint8 coreId);

eBool AtShortRegisteAuditPerform(tAtShortRegisterAudit* self);
tAtShortRegisterAudit *AtShortRegisterAuditNew(AtDevice device);
void AtShortRegisterAuditDelete(tAtShortRegisterAudit *self);
uint32 AtShortRegisterAuditEntriesCount(const tAtShortRegisterAudit* self);
uint32 AtShortRegisterAuditCollisionCount(const tAtShortRegisterAudit* self);

eBool AtShortRegisterAuditBlackListAdd(tAtShortRegisterAudit *self,
                                       uint32 address,
                                       uint8 coreId);

eBool AtShortRegisterAuditBlackListRemove(tAtShortRegisterAudit *self,
                                          uint32 address,
                                          uint8 coreId);

eBool AtShortRegisterAuditMaskSet(tAtShortRegisterAudit *self,
                                  uint32 address,
                                  uint8 coreId,
                                  uint32 mask);
/* Verbose */
void AtShortRegisterAuditVerboseOn(tAtShortRegisterAudit *self);
void AtShortRegisterAuditVerboseOff(tAtShortRegisterAudit *self);
void AtShortRegisterAuditBlackListShow(tAtShortRegisterAudit *self);
void AtShortRegisterAuditWhiteListShow(tAtShortRegisterAudit *self);

#endif /* AT_SHORT_REGISTER_HEADER */



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies The use, copying, transfer or disclosure of such information is
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : 
 *
 * File        : athash.h
 *
 * Created Date: Wed 17, 2013
 *
 * Description : This file contain declaration for some generic hash algorithms
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#ifndef AT_ATHASH_H
#define AT_ATHASH_H

#ifdef __cplusplus
extern "C" {
#endif

#include "attypes.h"
    
/**
* @addtogroup grpCoreLibClasses Core Libraries
* @{
*
* @defgroup grpHashingLibrary Hashing Methods
* @{
*/

/*--------------------------- Define -----------------------------------------*/


/*--------------------------- Macros -----------------------------------------*/


/*--------------------------- Typedefs ---------------------------------------*/


/*--------------------------- Function declarations --------------------------*/
/**
* @brief Hash algorithm based on Robert Sedgwicks' algorithms
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtRsHash(const uint8* pData, uint32 len);

/**
* @brief A bitwise hash function written by Justin Sobel
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtJsHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm based on based on work by Peter J. Weinberger 
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtPjwHash(const uint8* pData, uint32 len);

/**
* @brief PJW Hash that is tweaked for 32-bit processors
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtElfHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm comes from Brian Kernighan and Dennis Ritchie's book,
*        "The C Programming Language"
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtBkdrHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm that is used in the open source SDBM project
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtSdbmHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm comes from rofessor Daniel J. Bernstein
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtDjbHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm proposed by Donald E. Knuth in "The Art Of Computer
*        Programming Volume 3"
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtDekHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm taken from Bruno Preiss's book, "Data Structures and
*        Algorithms with Object-Oriented Design Patterns in Java"
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtBpHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm by Fowler/Noll/Vo, its creators
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtFnvHash(const uint8* pData, uint32 len);

/**
* @brief Hash algorithm by Arash Partow, http://www.partow.net
* @param [in] pData Data to be hashed
* @param [in] len Length of data
* @retval uint32 Hash value
*/
uint32 AtApHash(const uint8* pData, uint32 len);

/**
* @}
* @}
*/

#ifdef  __cplusplus
}
#endif

#endif /* AT_ATHASH_H */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TextUI
 * 
 * File        : AtTextUICmd.h
 * 
 * Created Date: Dec 26, 2013
 *
 * Description : Text UI command
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTEXTUICMD_H_
#define _ATTEXTUICMD_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cCmdNameMaxLen 257

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tCmdConf
    {
    uint8 level;
    char cmdName[cCmdNameMaxLen];
    eBool (*cmdFunction)(char argc, char **argv);
    int8 numParam;
    int confFilePos;
    } tCmdConf;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATTEXTUICMD_H_ */


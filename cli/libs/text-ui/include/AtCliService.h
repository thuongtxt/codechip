/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtCliClient.h
 * 
 * Created Date: Jun 3, 2015
 *
 * Description : CLI client
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLISERVICE_H_
#define _ATCLISERVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtConnection.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCliClientListener
    {
    void (*DidSendCli)(const char *cli, void *userData);
    void (*DidReceiveData)(char *buffer, uint32 bufferSize, void *userData);
    void (*DidFinish)(void *userData);
    eBool (*ShouldPrintResult)(const char *cli);
    }tAtCliClientListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*
 * Server side:
 * - Call AtCliServerStart() to start listening on CLI requests
 * - Call AtCliServerStop() to stop listening, OS resource (tasks, sockets) will
 *   be deleted
 */
void AtCliServerStart(const char *bindAddress, uint16 port);
void AtCliServerStop(void);
AtConnection AtCliServerConnection(void);

/* Client side. Call AtCliClientStart() to make CLI prompt appear. Running "exit"
 * will exit this prompt and socket is also closed */
void AtCliClientStart(const char *serverIpAddress, uint16 port);
void AtCliClientRichStart(const char *serverIpAddress, uint16 port);

/* Client */
void AtCliClientConnect(const char *serverIpAddress, uint16 port);
void AtCliClientDisconnect(void);
eBool AtCliClientConnected(void);
eBool AtCliClientExecute(const char *cli);
AtConnection AtCliClientConnection(void);
void AtCliClientListenerSet(const tAtCliClientListener *listener, void *userData);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLISERVICE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/


#ifndef AT_LONG_REGISTER_HEADER
#define AT_LONG_REGISTER_HEADER

/*--------------------------- Define -----------------------------------------*/
#define cLongRegisterHashTableSize  (cBit13_0 + 1)
#define cLongRegisterMaxLength      16

/*--------------------------- Structures -------------------------------------*/
typedef struct tAtLongRegisterBackList
    {
    uint8  coreId;
    uint32 address;
    void * next;
    }tAtLongRegisterBackList;

typedef struct tAtLongRegisterEntry
    {
    uint8  coreId;
    uint16 length;
    uint32 address;
    uint32 * masks;
    uint32 * values;
    void   * next;
    }tAtLongRegisterEntry;

typedef struct tAtLongRegisterAudit
    {
    tAtLongRegisterBackList * blackList;
    tAtLongRegisterEntry** bucket; /**< bucket */
    uint32  count; /**< count */
    uint32  size;  /**< size */
    uint32  collisionCount; /**< collisionCount */
    uint32  maxCollisionPerHash; /**< maxCollisionPerHash */
    AtDevice device;
    eBool verbose;
    } tAtLongRegisterAudit;

/*--------------------------- Forward Declare---------------------------------*/
int AtLongRegisterAuditInsert(tAtLongRegisterAudit* self,
                              uint32 address,
                              const uint32 * values,
                              uint16 length,
                              uint8 coreId);

eBool AtLongRegisterAuditPerform(tAtLongRegisterAudit* self);
tAtLongRegisterAudit *AtLongRegisterAuditNew(AtDevice device);
void AtLongRegisterAuditDelete(tAtLongRegisterAudit *self);
uint32 AtLongRegisterAuditEntriesCount(const tAtLongRegisterAudit* self);
uint32 AtLongRegisterAuditCollisionCount(const tAtLongRegisterAudit* self);

/* Black-List */
eBool AtLongRegisterAuditBlackListAdd(tAtLongRegisterAudit *self,
                                      uint32 address,
                                      uint8 coreId);
eBool AtLongRegisterAuditBlackListRemove(tAtLongRegisterAudit *self,
                                         uint32 address,
                                         uint8 coreId);
eBool AtLongRegisterAuditMaskSet(tAtLongRegisterAudit* self,
                                 uint32 address,
                                 uint8 coreId,
                                 uint32 *masks,
                                 uint8 length);
/* Verbose */
void AtLongRegisterAuditVerboseOn(tAtLongRegisterAudit *self);
void AtLongRegisterAuditVerboseOff(tAtLongRegisterAudit *self);
void AtLongRegisterAuditBlackListShow(tAtLongRegisterAudit *self);
void AtLongRegisterAuditWhiteListShow(tAtLongRegisterAudit* self);

/* Utils */
char* bufferWrStringSharedGet(void);
char* bufferRdStringSharedGet(void);
#endif /* AT_LONG_REGISTER_HEADER */



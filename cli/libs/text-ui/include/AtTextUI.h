/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Text UI
 * 
 * File        : AtTextUI.h
 * 
 * Created Date: Nov 5, 2012
 *
 * Description : Text UI top abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _ATTEXTUI_H_
#define _ATTEXTUI_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdarg.h>
#include "AtTextUICmd.h"
#include "AtTcl.h"
#include "AtIterator.h"
#include "AtConnection.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Text UI class */
typedef struct tAtTextUI * AtTextUI;

typedef struct tAtTextUIListerner
    {
    void (*WillStop)(AtTextUI self, void *userData);
    void (*DidStop)(AtTextUI self, void *userData);
    void (*WillExecuteCli)(AtTextUI self, const char *cli, void *userData);
    void (*DidExecuteCliWithResult)(AtTextUI self, const char *cli, int result, void *userData);
    }tAtTextUIListerner;

typedef enum eAtCliMode
    {
    cAtCliModeNormal   = 0,
    cAtCliModeAutotest = 1,
    cAtCliModeTcl      = 2,
    cAtCliModeSilent   = 4,
    cAtCliModeAll      = (cAtCliModeAutotest | cAtCliModeTcl | cAtCliModeSilent) /* Mask of all modes  */
    }eAtCliMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Methods */
void AtTextUIStart(AtTextUI self);
void AtTextUIStop(AtTextUI self);
void AtTextUIDelete(AtTextUI self);
void AtTextUIVerbose(AtTextUI self, eBool verbose);
eBool AtTextUIIsVerbosed(AtTextUI self);
void AtTextUIHelp(AtTextUI self);
void AtTextUIScriptVerbose(AtTextUI self, eBool verbose);
eBool AtTextUIScriptIsVerbosed(AtTextUI self);

void AtTextUICliModeSet(AtTextUI self, eAtCliMode mode);
void AtTextUICliModeClear(AtTextUI self, eAtCliMode mode);
eAtCliMode AtTextUICliModeGet(AtTextUI self);

/* Command process */
eBool AtTextUIScriptRun(AtTextUI self, const char * pScriptFile);
eBool AtTextUICmdProcess(AtTextUI self, const char * pCmdStr);
eBool AtTextUIFormatCmdProcess(AtTextUI self, const char *format, va_list ap);
void AtTextUICmdShow(AtTextUI self, char **tokens, uint32 numTokens);
void AtTextUIBuiltInCommandsInstall(AtTextUI self);
eBool AtTinyTextUICmdAdd(AtTextUI self,
                         const char *cmdName,
                         eBool (*cmdFunction)(char argc, char **argv),
                         uint8 numLevels,
                         int8 numParams);
AtIterator AtTinyTextUICmdsIteratorCreate(AtTextUI self);

/* On-the-fly scripting */
void AtTextUIScriptStart(AtTextUI self);
void AtTextUIScriptCancel(AtTextUI self);
void AtTextUIScriptFinish(AtTextUI self);
void AtTextUITclSet(AtTextUI self, AtTcl tcl);
AtTcl AtTextUITclGet(AtTextUI self);

/* History */
void AtTextUIHistoryEnable(AtTextUI self, eBool enable);
eBool AtTextUIHistoryIsEnabled(AtTextUI self);
void AtTextUIHistoryAdd(AtTextUI self, const char *cmd);
void AtTextUIHistoryShow(AtTextUI self);
void AtTextUIHistoryClear(AtTextUI self);
uint32 AtTextUIHistoryNumClisGet(AtTextUI self);
char *AtTextUIHistoryCliGet(AtTextUI self, uint32 cliIndex);

/* Text-UI shared buffer */
void AtTextUICmdBufferSizeSet(AtTextUI self, uint32 bufferSize);
uint32 AtTextUICmdBufferSizeGet(AtTextUI self);
char* AtTextUICmdBufferGet(AtTextUI self, uint32 *cmdBufferSize);

/* Remote control */
void AtTextUIRemoteConnect(AtTextUI self, const char *ipAddress, uint16 port);
void AtTextUIRemoteDisconnect(AtTextUI self);
AtConnection AtTextUIRemoteConnection(AtTextUI self);
void AtTextUIRemoteCliReplay(AtTextUI self, eBool enabled);
eBool AtTextUIRemoteCliIsReplayed(AtTextUI self);

/* Listener */
void AtTextUIListenerAdd(AtTextUI self, tAtTextUIListerner *callback, void *userData);
void AtTextUIListenerRemove(AtTextUI self, tAtTextUIListerner *callback);

/* Concrete text-UI */
AtTextUI AtTinyTextUINew(void);
AtTextUI AtTinyTextUIWithReadLineNew(char *(*ReadLine)(char *buffer, int size));
AtTextUI AtDefaultTinyTextUINew(void);
AtTextUI AtClishTextUINew(AtTextUI delegate);

/* Result of the last CLI */
eBool AtTextUIResultIsValid(AtTextUI self);
void AtTextUIResultInvalidate(AtTextUI self);
void AtTextUIIntegerResultSet(AtTextUI self, uint32 result);
uint32 AtTextUIIntegerResultGet(AtTextUI self);

#ifdef __cplusplus
}
#endif
#endif /* _ATTEXTUI_H_ */

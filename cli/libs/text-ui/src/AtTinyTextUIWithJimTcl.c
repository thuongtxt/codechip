/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Text-UI
 *
 * File        : AtTinyTextUIWithJimTcl.c
 *
 * Created Date: Mar 30, 2015
 *
 * Description : Default Tiny text UI with Jim TCL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include "atclib.h"
#include "AtTinyTextUIInternal.h"
#include "AtOsal.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#define MAXBUFLEN 1000000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtTinyTextUIWithJimTcl
    {
    tAtDefaultTinyTextUI super;
    }tAtTinyTextUIWithJimTcl;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static char m_ScriptBuffer[MAXBUFLEN + 1];

/* Override */
static tAtTextUIMethods m_AtTextUIOverride;

/* Super implementation */
static const tAtTextUIMethods *m_AtTextUIMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ReadFileToBuffer(const char* pScriptFile)
    {
    static const char *cAtSdkStr = "atsdk::";
    uint32 newLen;
    char *position;
    char *tmp = m_ScriptBuffer;
    AtFile batchFile;
    AtStd std = AtStdSharedStdGet();

    batchFile = AtStdFileOpen(std, pScriptFile, cAtFileOpenModeRead);
    if(batchFile == NULL)
        return cAtFalse;

    newLen = AtFileRead(batchFile, m_ScriptBuffer, sizeof(char), MAXBUFLEN);
    if (newLen == 0)
        {
        AtFileClose(batchFile);
        return 0;
        }

    m_ScriptBuffer[newLen] = '\0'; /* Just to be safe. */
    AtFileClose(batchFile);

    /* Search atsdk:: and replace by space */
    while (((position = AtStdStrstr(std, tmp, cAtSdkStr)) != NULL) && ((tmp - m_ScriptBuffer) < (int)newLen))
        {
        uint32 atsdkLen = AtStdStrlen(std, cAtSdkStr);
        uint32 i;
        for (i = 0; i < atsdkLen; i++)
            position[i] = ' ';

        tmp = position + atsdkLen;
        }

    return newLen;
    }

static eBool ScriptRun(AtTextUI self, const char * pScriptFile)
    {
    eBool isVerbose = AtTextUIIsVerbosed(self);
    eBool historyEnabled = AtTextUIHistoryIsEnabled(self);

    if (ReadFileToBuffer(pScriptFile) == 0)
        return 0;

    /* Disable history and enable verbose */
    AtTextUIHistoryEnable(self, cAtFalse);
    AtTextUIVerbose(self, cAtTrue);

    /* Execute script */
    AtTclEval(AtTextUITclGet(self), m_ScriptBuffer);

    /* Restore old configure */
    AtTextUIVerbose(self, isVerbose);
    AtTextUIHistoryEnable(self, historyEnabled);

    return cAtTrue;
    }

static AtTcl DefaultTcl(AtTextUI tinyTextUi)
    {
    AtTcl tcl = AtDefaultTclNew();
    AtIterator cmdIterator = AtTinyTextUICmdsIteratorCreate(tinyTextUi);
    tCmdConf *cmd;

    while((cmd = (tCmdConf *)AtIteratorNext(cmdIterator)) != NULL)
        AtTclCliRegister(tcl, cmd);

    AtObjectDelete((AtObject)cmdIterator);
    return tcl;
    }

static void Init(AtTextUI self)
    {
    m_AtTextUIMethods->Init(self);
    AtTextUITclSet(self, DefaultTcl(self));
    }

static void Delete(AtTextUI self)
    {
    AtTclDelete(AtTextUITclGet(self));
    m_AtTextUIMethods->Delete(self);
    }

static void OverrideAtTextUI(AtTextUI self)
    {
    if (!m_methodsInit)
        {
        m_AtTextUIMethods = self->methods;
        AtOsalMemCpy(&m_AtTextUIOverride, m_AtTextUIMethods, sizeof(m_AtTextUIOverride));
        m_AtTextUIOverride.ScriptRun   = ScriptRun;
        m_AtTextUIOverride.Init        = Init;
        m_AtTextUIOverride.Delete      = Delete;
        }

    self->methods = &m_AtTextUIOverride;
    }

static void Override(AtTextUI self)
    {
    OverrideAtTextUI(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTinyTextUIWithJimTcl);
    }

static AtTextUI ObjectInit(AtTextUI self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtDefaultTinyTextUIObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTextUI AtTinyTextUIWithJimTclNew(void)
    {
    AtTextUI newTextUI = AtOsalMemAlloc(ObjectSize());
    if (newTextUI == NULL)
        return NULL;

    newTextUI = ObjectInit(newTextUI);
    AtTextUIInit(newTextUI);

    return newTextUI;
    }

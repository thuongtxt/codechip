/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Text-UI
 * 
 * File        : AtTextUIInternal.h
 * 
 * Created Date: Nov 6, 2012
 *
 * Description : Text-UI class represenation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTEXTUIINTERNAL_H_
#define _ATTEXTUIINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTextUI.h"
#include "AtTcl.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTextUIMethods
    {
    /* Public */
    void (*Init)(AtTextUI self);
    void (*Start)(AtTextUI self);
    void (*Stop)(AtTextUI self);
    void (*Delete)(AtTextUI self);

    char *(*CliGetFromPrompt)(AtTextUI self, char *cliBuffer, uint32 bufferLen);
    eBool (*CliProcess)(AtTextUI self, const char * pCmdStr);
    eBool (*ScriptRun)(AtTextUI self, const char * pScriptFile);

    void (*CliModeSet)(AtTextUI self, eAtCliMode mode);
    void (*CliModeClear)(AtTextUI self, eAtCliMode mode);
    eAtCliMode (*CliModeGet)(AtTextUI self);

    void (*Verbose)(AtTextUI self, eBool verbose);
    void (*ScriptVerbose)(AtTextUI self, eBool verbose);
    eBool (*IsVerbosed)(AtTextUI self);
    eBool (*ScriptIsVerbosed)(AtTextUI self);

    void (*CmdShow)(AtTextUI self, char **tokens, uint32 numTokens);
    void (*Help)(AtTextUI self);

    void (*HistoryShow)(AtTextUI self);
    void (*HistoryClear)(AtTextUI self);
    void (*HistoryEnable)(AtTextUI self, eBool enable);
    eBool (*HistoryIsEnabled)(AtTextUI self);
    void (*HistoryAdd)(AtTextUI self, const char *cmd);
    uint32 (*HistoryNumClisGet)(AtTextUI self);
    char *(*HistoryCliGet)(AtTextUI self, uint32 cliIndex);

    void (*CmdBufferSizeSet)(AtTextUI self, uint32 bufferSize);
    uint32 (*CmdBufferSizeGet)(AtTextUI self);
    char* (*CmdBufferGet)(AtTextUI self, uint32 *cmdBufferSize);

    /* Scripting */
    void (*ScriptStart)(AtTextUI self);
    void (*ScriptCancel)(AtTextUI self);
    void (*ScriptFinish)(AtTextUI self);

    /* Listeners */
    void (*ListenerAdd)(AtTextUI self, tAtTextUIListerner *listener, void *userData);
    void (*ListenerRemove)(AtTextUI self, tAtTextUIListerner *listener);
    void (*WillExecuteCliEventNotify)(AtTextUI self, const char *cli);
    void (*DidExecuteCliWithResultEventNotify)(AtTextUI self, const char *cli, int result);
    }tAtTextUIMethods;

/* Text UI class representation */
typedef struct tAtTextUI
    {
    /* Methods */
    const tAtTextUIMethods *methods;

    /* Private */
    eBool verbosed;
    eBool scriptVerbosed;
    AtTcl tcl;
    AtList listeners;
    char *cmdBuffer;
    uint32 cmdBufferSize;
    eAtCliMode cliMode;

    /* To handle result of last CLI */
    eBool validResult;
    uint32 integerResult;

    /* For remote connection */
    eBool cliReplayed;
    }tAtTextUI;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtTextUI AtTextUIObjectInit(AtTextUI self);

void AtTextUIInit(AtTextUI self);
void AtTextUIWillExecuteCliEventNotify(AtTextUI self, const char *cli);
void AtTextUIDidExecuteCliWithResultEventNotify(AtTextUI self, const char *cli, int result);

#ifdef __cplusplus
}
#endif
#endif /* _ATTEXTUIINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Text-UI
 *
 * File        : AtTextUI.c
 *
 * Created Date: Nov 5, 2012
 *
 * Description : Abstract Text-UI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtTextUIInternal.h"
#include "AtOsal.h"
#include "AtObject.h"
#include "AtCliService.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tListenerWrapper
    {
    tAtTextUIListerner *listener;
    void *userData;
    }tListenerWrapper;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtTextUIMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Init(AtTextUI self)
    {
    /* Default command buffer size */
    AtTextUICmdBufferSizeSet(self, 256);
    AtTextUIScriptVerbose(self, cAtTrue);
    }

static void Start(AtTextUI self)
    {
	AtUnused(self);
    /* Sub class will do */
    }

static void Stop(AtTextUI self)
    {
	AtUnused(self);
    }

static void AllListenersDelete(AtTextUI self)
    {
    while (AtListLengthGet(self->listeners) > 0)
        {
        tListenerWrapper *wrapper = (tListenerWrapper *)AtListObjectRemoveAtIndex(self->listeners, 0);
        AtOsalMemFree(wrapper);
        }
    AtObjectDelete((AtObject)self->listeners);
    }

static void Delete(AtTextUI self)
    {
    AllListenersDelete(self);
    AtOsalMemFree(self->cmdBuffer);
    AtOsalMemFree(self);
    }

static eBool CliProcess(AtTextUI self, const char * pCmdStr)
    {
	AtUnused(pCmdStr);
	AtUnused(self);
    /* Let sub-class do */
    return cAtTrue;
    }

static char *CliGetFromPrompt(AtTextUI self, char *cliBuffer, uint32 bufferLen)
    {
	AtUnused(bufferLen);
	AtUnused(cliBuffer);
	AtUnused(self);
    /* Let sub-class do */
    return NULL;
    }

static eBool ScriptRun(AtTextUI self, const char * pScriptFile)
    {
	AtUnused(pScriptFile);
	AtUnused(self);
    /* Let sub-class do */
    return cAtTrue;
    }

static void Verbose(AtTextUI self, eBool verbose)
    {
    self->verbosed = verbose;
    }

static eBool IsVerbosed(AtTextUI self)
    {
    return self->verbosed;
    }

static void ScriptVerbose(AtTextUI self, eBool verbose)
    {
    self->scriptVerbosed = verbose;
    }

static eBool ScriptIsVerbosed(AtTextUI self)
    {
    return self->scriptVerbosed;
    }

static void HistoryShow(AtTextUI self)
    {
	AtUnused(self);
    /* Let subclass do */
    }

static void HistoryClear(AtTextUI self)
    {
	AtUnused(self);
    /* Let subclass do */
    }

static void HistoryEnable(AtTextUI self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let subclass do */
    }

static eBool HistoryIsEnabled(AtTextUI self)
    {
	AtUnused(self);
    /* Let subclass do */
    return cAtFalse;
    }

static void HistoryAdd(AtTextUI self, const char *cmd)
    {
	AtUnused(cmd);
	AtUnused(self);
    /* Let subclass do */
    return;
    }

static uint32 HistoryNumClisGet(AtTextUI self)
    {
	AtUnused(self);
    /* Let subclass do */
    return 0;
    }

static char *HistoryCliGet(AtTextUI self, uint32 cliIndex)
    {
	AtUnused(cliIndex);
	AtUnused(self);
    /* Let subclass do */
    return NULL;
    }

static eBool TextUIIsValid(AtTextUI self)
    {
    return self ? cAtTrue : cAtFalse;
    }

static void ScriptStart(AtTextUI self)
    {
	AtUnused(self);
    }

static void ScriptCancel(AtTextUI self)
    {
	AtUnused(self);
    }

static void ScriptFinish(AtTextUI self)
    {
	AtUnused(self);
    }

static AtList Listeners(AtTextUI self)
    {
    if (self->listeners == NULL)
        self->listeners = AtListCreate(0);
    return self->listeners;
    }

static void WillStopEventNotify(AtTextUI self)
    {
    uint32 i;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tListenerWrapper *wrapper = (tListenerWrapper *)AtListObjectGet(self->listeners, i);
        if (wrapper && wrapper->listener && wrapper->listener->WillStop)
            wrapper->listener->WillStop(self, wrapper->userData);
        }
    }

static void DidStopEventNotify(AtTextUI self)
    {
    uint32 i;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tListenerWrapper *wrapper = (tListenerWrapper *)AtListObjectGet(self->listeners, i);
        if (wrapper && wrapper->listener && wrapper->listener->DidStop)
            wrapper->listener->DidStop(self, wrapper->userData);
        }
    }

static void WillExecuteCliEventNotify(AtTextUI self, const char *cli)
    {
    uint32 i;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tListenerWrapper *wrapper = (tListenerWrapper *)AtListObjectGet(self->listeners, i);
        if (wrapper && wrapper->listener && wrapper->listener->WillExecuteCli)
            wrapper->listener->WillExecuteCli(self, cli, wrapper->userData);
        }
    }

static void DidExecuteCliWithResultEventNotify(AtTextUI self, const char *cli, int result)
    {
    uint32 i;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tListenerWrapper *listener = (tListenerWrapper *)AtListObjectGet(self->listeners, i);
        if (listener && listener->listener && listener->listener->DidExecuteCliWithResult)
            listener->listener->DidExecuteCliWithResult(self, cli, result, listener->userData);
        }
    }

static void CmdShow(AtTextUI self, char **tokens, uint32 numTokens)
    {
	AtUnused(numTokens);
	AtUnused(tokens);
	AtUnused(self);

    }

static void Help(AtTextUI self)
    {
	AtUnused(self);

    }

static void CmdBufferSizeSet(AtTextUI self, uint32 bufferSize)
    {
    if ((bufferSize == self->cmdBufferSize) || (bufferSize == 0))
        return;

    /* Just reset the buffer */
    AtOsalMemFree(self->cmdBuffer);
    self->cmdBuffer = NULL;
    self->cmdBufferSize = bufferSize;
    }

static uint32 CmdBufferSizeGet(AtTextUI self)
    {
    return self->cmdBufferSize;
    }

static char* CmdBufferGet(AtTextUI self, uint32 *cmdBufferSize)
    {
    uint32 bufferSize = AtTextUICmdBufferSizeGet(self);
    char *buffer;

    /* Already have this buffer */
    if (self->cmdBuffer)
        {
        if (cmdBufferSize)
            *cmdBufferSize = bufferSize;
        return self->cmdBuffer;
        }

    /* Allocate buffer to hold command for internal processing */
    buffer = AtOsalMemAlloc(bufferSize);
    if (buffer == NULL)
        {
        if (cmdBufferSize)
            *cmdBufferSize = 0;
        return NULL;
        }
    AtOsalMemInit(buffer, 0, bufferSize);

    /* Cache this memory */
    if (cmdBufferSize)
        *cmdBufferSize = bufferSize;
    self->cmdBuffer = buffer;

    return buffer;
    }

static void CliModeSet(AtTextUI self, eAtCliMode mode)
    {
    self->cliMode |= mode;
    }

static void CliModeClear(AtTextUI self, eAtCliMode mode)
    {
    self->cliMode &= ~((uint32)mode);
    }

static eAtCliMode CliModeGet(AtTextUI self)
    {
    return self->cliMode;
    }

static eBool IsLocalCommand(AtTextUI self, const char * pCmdStr)
    {
    AtUnused(self);
    if (AtStrstr(pCmdStr, "textui"))
        return cAtTrue;
    return cAtFalse;
    }

static eBool ListenerExist(AtTextUI self, tAtTextUIListerner *listener, void *userData)
    {
    AtIterator iterator;
    tListenerWrapper *wrapper;
    eBool exist = cAtFalse;

    if (self->listeners == NULL)
        return cAtFalse;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper && (wrapper->userData == userData) && (wrapper->listener == listener))
            {
            exist = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return exist;
    }

static void ListenerAdd(AtTextUI self, tAtTextUIListerner *listener, void *userData)
    {
    uint32 memorySize = sizeof(tListenerWrapper);
    tListenerWrapper *wraper;

    if (ListenerExist(self, listener, userData))
        return;

    wraper = AtOsalMemAlloc(memorySize);
    if (wraper == NULL)
        return;

    AtOsalMemInit(wraper, 0, memorySize);
    wraper->listener = listener;
    wraper->userData = userData;

    AtListObjectAdd(Listeners(self), (AtObject)wraper);
    }

static void ListenerRemove(AtTextUI self, tAtTextUIListerner *listener)
    {
    AtIterator iterator;
    tListenerWrapper *wrapper;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener == listener)
            {
            AtIteratorRemove(iterator);
            AtOsalMemFree(wrapper);
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    }

static void MethodsInit(AtTextUI self)
    {
    if (!m_methodsInit)
        {
        m_methods.Init              = Init;
        m_methods.Start             = Start;
        m_methods.Stop              = Stop;
        m_methods.Delete            = Delete;
        m_methods.CliProcess        = CliProcess;
        m_methods.CliGetFromPrompt  = CliGetFromPrompt;
        m_methods.ScriptRun         = ScriptRun;
        m_methods.Verbose           = Verbose;
        m_methods.IsVerbosed        = IsVerbosed;
        m_methods.ScriptVerbose     = ScriptVerbose;
        m_methods.ScriptIsVerbosed  = ScriptIsVerbosed;
        m_methods.HistoryShow       = HistoryShow;
        m_methods.HistoryClear      = HistoryClear;
        m_methods.HistoryEnable     = HistoryEnable;
        m_methods.HistoryIsEnabled  = HistoryIsEnabled;
        m_methods.HistoryAdd        = HistoryAdd;
        m_methods.HistoryNumClisGet = HistoryNumClisGet;
        m_methods.HistoryCliGet     = HistoryCliGet;
        m_methods.ScriptStart       = ScriptStart;
        m_methods.ScriptCancel      = ScriptCancel;
        m_methods.ScriptFinish      = ScriptFinish;
        m_methods.CmdShow           = CmdShow;
        m_methods.Help              = Help;
        m_methods.CmdBufferSizeSet  = CmdBufferSizeSet;
        m_methods.CmdBufferSizeGet  = CmdBufferSizeGet;
        m_methods.CmdBufferGet      = CmdBufferGet;
        m_methods.CliModeSet        = CliModeSet;
        m_methods.CliModeClear      = CliModeClear;
        m_methods.CliModeGet        = CliModeGet;
        m_methods.ListenerAdd       = ListenerAdd;
        m_methods.ListenerRemove    = ListenerRemove;
        m_methods.WillExecuteCliEventNotify          = WillExecuteCliEventNotify;
        m_methods.DidExecuteCliWithResultEventNotify = DidExecuteCliWithResultEventNotify;
        }

    self->methods = &m_methods;
    }

AtTextUI AtTextUIObjectInit(AtTextUI self)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, sizeof(tAtTextUI));

    /* Setup methods */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtTextUIWillExecuteCliEventNotify(AtTextUI self, const char *cli)
    {
    if (self)
        mMethodsGet(self)->WillExecuteCliEventNotify(self, cli);
    }

void AtTextUIDidExecuteCliWithResultEventNotify(AtTextUI self, const char *cli, int result)
    {
    if (self)
        mMethodsGet(self)->DidExecuteCliWithResultEventNotify(self, cli, result);
    }

/**
 * Initialize Text-UI
 * @param self
 */
void AtTextUIInit(AtTextUI self)
    {
    if (TextUIIsValid(self))
        self->methods->Init(self);
    }

/**
 * Start a Text-UI to receive command
 * @param self
 */
void AtTextUIStart(AtTextUI self)
    {
    if (TextUIIsValid(self))
        self->methods->Start(self);
    }

/**
 * Stop a Text-UI. All of database will not be free
 *
 * @param self This Text-UI
 */
void AtTextUIStop(AtTextUI self)
    {
    if (!TextUIIsValid(self))
        return;

    WillStopEventNotify(self);
    AtTextUIRemoteDisconnect(self);
    self->methods->Stop(self);
    DidStopEventNotify(self);
    }

/**
 * Delete Text-UI
 *
 * @param self This Text-UI
 */
void AtTextUIDelete(AtTextUI self)
    {
    if (!TextUIIsValid(self))
        return;

    AtTextUIStop(self);
    self->methods->Delete(self);
    }

static eBool CliLocalProcess(AtTextUI self, const char * pCmdStr)
    {
    AtTextUIResultInvalidate(self);
    return self->methods->CliProcess(self, pCmdStr);
    }

static eBool CanSendCommandToRemote(AtTextUI self, const char * pCmdStr)
    {
    return (AtTextUIRemoteConnection(self) && !IsLocalCommand(self, pCmdStr));
    }

static eBool CmdProcess(AtTextUI self, const char * pCmdStr)
    {
    /* When CLI replaying is enabled, need to execute on the local before sending
     * to remote */
    if (AtTextUIRemoteCliIsReplayed(self))
        {
        eBool success = cAtTrue;
        success |= CliLocalProcess(self, pCmdStr);
        if (CanSendCommandToRemote(self, pCmdStr))
            success |= AtCliClientExecute(pCmdStr);
        return success;
        }

    /* Only process command on remote site */
    if (CanSendCommandToRemote(self, pCmdStr))
        return AtCliClientExecute(pCmdStr);

    /* Only process command on local site */
    return CliLocalProcess(self, pCmdStr);
    }

/**
 * Process a command
 *
 * @param self This Text-UI
 * @param pCmdStr Command string
 *
 * @retval cAtTrue - success
 * @retval cAtFalse - fail
 */
eBool AtTextUICmdProcess(AtTextUI self, const char * pCmdStr)
    {
    eBool result;

    if (!TextUIIsValid(self))
        return cAtFalse;

    AtTextUIWillExecuteCliEventNotify(self, pCmdStr);
    result = CmdProcess(self, pCmdStr);
    AtTextUIDidExecuteCliWithResultEventNotify(self, pCmdStr, result);

    return result;
    }

/**
 * Run a script
 *
 * @param self This Text-UI
 * @param pScriptFile Script file
 *
 * @retval cAtTrue - success
 * @retval cAtFalse - fail
 */
eBool AtTextUIScriptRun(AtTextUI self, const char * pScriptFile)
    {
    if (TextUIIsValid(self))
        return self->methods->ScriptRun(self, pScriptFile);

    return cAtFalse;
    }

/**
 * Enable/disable displaying the command that has just run
 *
 * @param self This Text-UI
 * @param verbose cAtTrue to verbose
 */
void AtTextUIVerbose(AtTextUI self, eBool verbose)
    {
    if (TextUIIsValid(self))
        self->methods->Verbose(self, verbose);
    }

/**
 * Check if verbose mode is ON
 *
 * @param self This Text-UI
 *
 * @retval cAtTrue  - Vernose is ON
 * @retval cAtFalse - Vernose is OFF
 */
eBool AtTextUIIsVerbosed(AtTextUI self)
    {
    if (TextUIIsValid(self))
        return self->methods->IsVerbosed(self);

    return cAtFalse;
    }

/**
 * Enable/disable displaying the commands in script that has just run
 *
 * @param self This Text-UI
 * @param verbose cAtTrue to verbose
 */
void AtTextUIScriptVerbose(AtTextUI self, eBool verbose)
    {
    if (TextUIIsValid(self))
        self->methods->ScriptVerbose(self, verbose);
    }

/**
 * Check if script verbose mode is ON
 *
 * @param self This Text-UI
 *
 * @retval cAtTrue  - Verbose is ON
 * @retval cAtFalse - Verbose is OFF
 */
eBool AtTextUIScriptIsVerbosed(AtTextUI self)
    {
    if (TextUIIsValid(self))
        return self->methods->ScriptIsVerbosed(self);

    return cAtFalse;
    }

/**
 * Enable/disable history logging
 *
 * @param self This Text-UI
 * @param enable cAtTrue to enable history, cAtFalse to disable
 */
void AtTextUIHistoryEnable(AtTextUI self, eBool enable)
    {
    if (TextUIIsValid(self))
        self->methods->HistoryEnable(self, enable);
    }

/**
 * Check if history logging is enabled or not
 *
 * @param self This Text-UI
 * @return cAtTrue if history logging is enabled, cAtFalse if it is disabled
 */
eBool AtTextUIHistoryIsEnabled(AtTextUI self)
    {
    if (TextUIIsValid(self))
        return self->methods->HistoryIsEnabled(self);
    return cAtFalse;
    }

/**
 * Display history on console
 *
 * @param self This Text-UI
 */
void AtTextUIHistoryShow(AtTextUI self)
    {
    if (TextUIIsValid(self))
        self->methods->HistoryShow(self);
    }

/**
 * Add command to history
 *
 * @param self This text-UI
 * @param cmd Command to add to history
 */
void AtTextUIHistoryAdd(AtTextUI self, const char *cmd)
    {
    if (TextUIIsValid(self))
        self->methods->HistoryAdd(self, cmd);
    }

/**
 * Clear history
 *
 * @param self
 */
void AtTextUIHistoryClear(AtTextUI self)
    {
    if (TextUIIsValid(self))
        self->methods->HistoryClear(self);
    }

/**
 * Get number of CLIs in history
 *
 * @param self This text-UI
 * @return Number of CLIs in history
 */
uint32 AtTextUIHistoryNumClisGet(AtTextUI self)
    {
    if (TextUIIsValid(self))
        return self->methods->HistoryNumClisGet(self);
    return 0;
    }

/**
 * Get CLI in history at specified index
 *
 * @param self This text-UI
 * @param cliIndex CLI index
 *
 * @return CLI string
 */
char *AtTextUIHistoryCliGet(AtTextUI self, uint32 cliIndex)
    {
    if (TextUIIsValid(self))
        return self->methods->HistoryCliGet(self, cliIndex);
    return NULL;
    }

/**
 * Start scripting mode. In this mode, all of input commands will be cached and
 * not executed yet. All of them will run when AtTextUIScriptFinish() is called.
 *
 * @param self This Text UI
 */
void AtTextUIScriptStart(AtTextUI self)
    {
    if (self)
        self->methods->ScriptStart(self);
    }

/**
 * Cancel scripting mode. All of input commands input before will be flushed.
 *
 * @param self This Text UI
 */
void AtTextUIScriptCancel(AtTextUI self)
    {
    if (self)
        self->methods->ScriptCancel(self);
    }

/**
 * Finish scripting mode. All of CLIs input so far will be executed.
 *
 * @param self This Text UI
 */
void AtTextUIScriptFinish(AtTextUI self)
    {
    if (self)
        self->methods->ScriptFinish(self);
    }

/**
 * Set TCL interpreter for scripting
 *
 * @param self This Text UI
 * @param tcl TCL interpreter
 */
void AtTextUITclSet(AtTextUI self, AtTcl tcl)
    {
    if (self)
        self->tcl = tcl;
    }

/**
 * Get TCL interpreter configured by AtTextUITclSet()
 *
 * @param self This Text-UI
 * @return TCL interpreter
 */
AtTcl AtTextUITclGet(AtTextUI self)
    {
    return self ? self->tcl : NULL;
    }

/**
 * Add listener to listen on Text-UI events
 *
 * @param self This Text-UI
 * @param listener Listener
 * @param userData User data that will be input when callback is called.
 */
void AtTextUIListenerAdd(AtTextUI self, tAtTextUIListerner *listener, void *userData)
    {
    if (self)
        mMethodsGet(self)->ListenerAdd(self, listener, userData);
    }

/**
 * Remove listener
 *
 * @param self This Text-UI
 * @param listener Listener
 */
void AtTextUIListenerRemove(AtTextUI self, tAtTextUIListerner *listener)
    {
    if (self)
        mMethodsGet(self)->ListenerRemove(self, listener);
    }

/**
 * Show list of commands
 *
 * @param self This Text-UI
 * @param tokens List of tokens need to filter
 * @param numTokens Number of token
 */
void AtTextUICmdShow(AtTextUI self, char **tokens, uint32 numTokens)
    {
    if (TextUIIsValid(self))
        self->methods->CmdShow(self, tokens, numTokens);
    }

/**
 * Display help
 *
 * @param self This Tex-UI
 */
void AtTextUIHelp(AtTextUI self)
    {
    if (TextUIIsValid(self))
        self->methods->Help(self);
    }

/**
 * Change command buffer size
 *
 * @param self This text-UI
 * @param bufferSize Buffer size
 */
void AtTextUICmdBufferSizeSet(AtTextUI self, uint32 bufferSize)
    {
    if (self)
        mMethodsGet(self)->CmdBufferSizeSet(self, bufferSize);
    }

/**
 * Get command buffer size
 *
 * @param self This text-UI
 *
 * @return Command buffer size
 */
uint32 AtTextUICmdBufferSizeGet(AtTextUI self)
    {
    if (self)
        return mMethodsGet(self)->CmdBufferSizeGet(self);
    return 0;
    }

/**
 * Get command buffer
 * @param self
 * @return
 */
char* AtTextUICmdBufferGet(AtTextUI self, uint32 *cmdBufferSize)
    {
    if (self)
        return mMethodsGet(self)->CmdBufferGet(self, cmdBufferSize);

    return NULL;
    }

/**
 * To check if the result of last CLI is valid
 *
 * @param self This text-UI
 * @return cAtTrue if result is valid, otherwise, cAtFalse is returned
 */
eBool AtTextUIResultIsValid(AtTextUI self)
    {
    if (self)
        return self->validResult;
    return cAtFalse;
    }

/**
 * To invalidate last result
 *
 * @param self This text-UI
 */
void AtTextUIResultInvalidate(AtTextUI self)
    {
    if (self)
        self->validResult = cAtFalse;
    }

/**
 * To set last CLI result
 *
 * @param self This text-UI
 * @param result Result to cache
 */
void AtTextUIIntegerResultSet(AtTextUI self, uint32 result)
    {
    if (self == NULL)
        return;

    self->integerResult = result;
    self->validResult   = cAtTrue;
    }

/**
 * To get CLI integer result
 *
 * @param self This text-UI
 * @return Integer value
 */
uint32 AtTextUIIntegerResultGet(AtTextUI self)
    {
    const uint32 cInvalid = 0xDEADCAFE;
    if (self == NULL)
        return cInvalid;
    return self->validResult ? self->integerResult : cInvalid;
    }

/**
 * Set Text-UI mode
 *
 * @param self This Text-UI
 * @param mode @ref eAtCliMode "CLI modes"
 */
void AtTextUICliModeSet(AtTextUI self, eAtCliMode mode)
    {
    if (self)
        self->methods->CliModeSet(self, mode);
    }

/**
 * Clear Text-UI mode
 *
 * @param self This Text-UI
 * @param mode @ref eAtCliMode "CLI modes"
 */
void AtTextUICliModeClear(AtTextUI self, eAtCliMode mode)
    {
    if (self)
        self->methods->CliModeClear(self, mode);
    }

/**
 * Get Text-UI mode
 *
 * @param self This text-UI
 *
 * @return @ref eAtCliMode "CLI modes"
 */
eAtCliMode AtTextUICliModeGet(AtTextUI self)
    {
    return self ? self->cliMode : cAtCliModeNormal;
    }

/**
 * Connect to remote Text UI to control it remotely. When a remote Text-UI is
 * connected, all of input CLIs will be executed at the context of the remote
 * and all of outputs are redirected to local Text UI console.
 *
 * @param self This text UI
 * @param ipAddress Remote Text UI IP address
 * @param port Remote Text UI IP port
 */
void AtTextUIRemoteConnect(AtTextUI self, const char *ipAddress, uint16 port)
    {
    AtUnused(self);
    AtCliClientConnect(ipAddress, port);
    }

/**
 * Disconnect remote Text UI to return the control to the local one
 *
 * @param self This Text UI
 */
void AtTextUIRemoteDisconnect(AtTextUI self)
    {
    AtUnused(self);
    AtCliClientDisconnect();
    }

/**
 * Get the underlying remote connection object.
 *
 * @param self This TextUI
 *
 * @return Remote connection
 */
AtConnection AtTextUIRemoteConnection(AtTextUI self)
    {
    AtUnused(self);
    return AtCliClientConnection();
    }

/**
 * To enable/disable replaying CLI to remote CLI server. When replaying is enabled,
 * every CLI executed on the local will be replayed to the remote CLI server.
 *
 * @param self This Text UI
 * @param enabled Enabling
 *                - cAtTrue to enable
 *                - cAtFalse to disable
 */
void AtTextUIRemoteCliReplay(AtTextUI self, eBool enabled)
    {
    if (self)
        self->cliReplayed = enabled;
    }

/**
 * Check if CLI replaying to CLI server is enabled or not
 *
 * @param self This Text UI
 *
 * @retval cAtTrue if enable
 * @retval cAtFalse if disable
 */
eBool AtTextUIRemoteCliIsReplayed(AtTextUI self)
    {
    return (eBool)(self ? self->cliReplayed : cAtFalse);
    }

/**
 * Execute formated command
 *
 * @param self This Text-UI
 * @param format Format
 * @param ap Variable list
 *
 * @retval cAtTrue - success
 * @retval cAtFalse - fail
 */
eBool AtTextUIFormatCmdProcess(AtTextUI self, const char *format, va_list ap)
    {
    uint32 bufferSize;
    char *buffer = AtTextUICmdBufferGet(self, &bufferSize);
    AtStd std = AtStdSharedStdGet();
    AtStdSnprintf(std, buffer, bufferSize - 1, format, ap);
    return AtTextUICmdProcess(self, buffer);
    }

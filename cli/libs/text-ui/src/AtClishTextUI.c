/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : AtClishTextUI.c
 *
 * Created Date: Nov 6, 2012
 *
 * Description : CLISH text UI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "AtTextUIInternal.h"
#include "AtOsal.h"
#include "clish/shell.h"
#include "../../../core/include/extAtSdkInt.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtClishTextUI)self)

/*--------------------------- Local typedefs ---------------------------------*/
/* Class representation */
typedef struct tAtClishTextUI
    {
    tAtTextUI super;

    /* The text UI that is responsible to process command */
    AtTextUI textUI;
    eBool stopped;
    }tAtClishTextUI;

typedef tAtClishTextUI * AtClishTextUI;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTextUIMethods        m_AtTextUIOverride;

/* Super implementation */
static const tAtTextUIMethods *m_AtTextUIMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtClishTextUI);
    }

static void Start(AtTextUI self)
    {
	AtUnused(self);
    AtClishStart();
    }

static eBool CliProcess(AtTextUI self, const char * pCmdStr)
    {
    AtTextUI textUI = mThis(self)->textUI;
    return mMethodsGet(textUI)->CliProcess(textUI, pCmdStr);
    }

static void Stop(AtTextUI self)
    {
    if (mThis(self)->stopped)
        return;

    AtClishStop();
    AtTextUIStop(mThis(self)->textUI);
    mThis(self)->stopped = cAtTrue;
    }

static eBool ScriptRun(AtTextUI self, const char * pScriptFile)
    {
    return AtTextUIScriptRun(mThis(self)->textUI, pScriptFile);
    }

static void HistoryAdd(AtTextUI self, const char *cmd)
    {
    AtTextUIHistoryAdd(mThis(self)->textUI, cmd);
    }

static void HistoryShow(AtTextUI self)
    {
    AtTextUIHistoryShow(mThis(self)->textUI);
    }

static void HistoryClear(AtTextUI self)
    {
    AtTextUIHistoryClear(mThis(self)->textUI);
    }

static void HistoryEnable(AtTextUI self, eBool enable)
    {
    AtTextUIHistoryEnable(mThis(self)->textUI, enable);
    }

static eBool HistoryIsEnabled(AtTextUI self)
    {
    return AtTextUIHistoryIsEnabled(mThis(self)->textUI);
    }

static uint32 HistoryNumClisGet(AtTextUI self)
    {
    return AtTextUIHistoryNumClisGet(mThis(self)->textUI);
    }

static char *HistoryCliGet(AtTextUI self, uint32 cliIndex)
    {
    return AtTextUIHistoryCliGet(mThis(self)->textUI, cliIndex);
    }

static void CmdShow(AtTextUI self, char **tokens, uint32 numTokens)
    {
    AtTextUICmdShow(mThis(self)->textUI, tokens, numTokens);
    }

static void Verbose(AtTextUI self, eBool verbose)
    {
    AtTextUIVerbose(mThis(self)->textUI, verbose);
    }

static eBool IsVerbosed(AtTextUI self)
    {
    return AtTextUIIsVerbosed(mThis(self)->textUI);
    }

static void Init(AtTextUI self)
    {
    m_AtTextUIMethods->Init(self);

    /* Additional initialization */
    AtTextUIHistoryEnable(mThis(self)->textUI, cAtTrue);
    AtClishSetup();
    }

static void CmdBufferSizeSet(AtTextUI self, uint32 bufferSize)
    {
    AtTextUICmdBufferSizeSet(mThis(self)->textUI, bufferSize);
    }

static uint32 CmdBufferSizeGet(AtTextUI self)
    {
    return AtTextUICmdBufferSizeGet(mThis(self)->textUI);
    }

static char* CmdBufferGet(AtTextUI self, uint32 *cmdBufferSize)
    {
    return AtTextUICmdBufferGet(mThis(self)->textUI, cmdBufferSize);
    }

static void CliModeSet(AtTextUI self, eAtCliMode mode)
    {
    m_AtTextUIMethods->CliModeSet(self, mode);

    /* TODO: Need to configure CLISH */
    }

static void ScriptVerbose(AtTextUI self, eBool verbose)
    {
    mThis(self)->textUI->scriptVerbosed = verbose;
    m_AtTextUIMethods->ScriptVerbose(self, verbose);
    }

static void ListenerAdd(AtTextUI self, tAtTextUIListerner *listener, void *userData)
    {
    AtTextUIListenerAdd(mThis(self)->textUI, listener, userData);
    }

static void ListenerRemove(AtTextUI self, tAtTextUIListerner *listener)
    {
    AtTextUIListenerRemove(mThis(self)->textUI, listener);
    }

static void WillExecuteCliEventNotify(AtTextUI self, const char *cli)
    {
    AtTextUIWillExecuteCliEventNotify(mThis(self)->textUI, cli);
    }

static void DidExecuteCliWithResultEventNotify(AtTextUI self, const char *cli, int result)
    {
    AtTextUIDidExecuteCliWithResultEventNotify(mThis(self)->textUI, cli, result);
    }

static void Override(AtTextUI self)
    {
    if (!m_methodsInit)
        {
        m_AtTextUIMethods = self->methods;
        AtOsalMemCpy(&m_AtTextUIOverride, m_AtTextUIMethods, sizeof(m_AtTextUIOverride));
        m_AtTextUIOverride.Start             = Start;
        m_AtTextUIOverride.Stop              = Stop;
        m_AtTextUIOverride.CliProcess        = CliProcess;
        m_AtTextUIOverride.ScriptRun         = ScriptRun;
        m_AtTextUIOverride.HistoryAdd        = HistoryAdd;
        m_AtTextUIOverride.HistoryShow       = HistoryShow;
        m_AtTextUIOverride.HistoryClear      = HistoryClear;
        m_AtTextUIOverride.HistoryEnable     = HistoryEnable;
        m_AtTextUIOverride.HistoryIsEnabled  = HistoryIsEnabled;
        m_AtTextUIOverride.HistoryNumClisGet = HistoryNumClisGet;
        m_AtTextUIOverride.HistoryCliGet     = HistoryCliGet;
        m_AtTextUIOverride.CmdShow           = CmdShow;
        m_AtTextUIOverride.Verbose           = Verbose;
        m_AtTextUIOverride.IsVerbosed        = IsVerbosed;
        m_AtTextUIOverride.Init              = Init;
        m_AtTextUIOverride.CmdBufferSizeSet  = CmdBufferSizeSet;
        m_AtTextUIOverride.CmdBufferSizeGet  = CmdBufferSizeGet;
        m_AtTextUIOverride.CmdBufferGet      = CmdBufferGet;
        m_AtTextUIOverride.CliModeSet        = CliModeSet;
        m_AtTextUIOverride.ScriptVerbose     = ScriptVerbose;
        m_AtTextUIOverride.ListenerAdd       = ListenerAdd;
        m_AtTextUIOverride.ListenerRemove    = ListenerRemove;
        m_AtTextUIOverride.WillExecuteCliEventNotify          = WillExecuteCliEventNotify;
        m_AtTextUIOverride.DidExecuteCliWithResultEventNotify = DidExecuteCliWithResultEventNotify;
        }

    self->methods = &m_AtTextUIOverride;
    }

static AtTextUI ObjectInit(AtTextUI self, AtTextUI delegate)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtTextUIObjectInit(self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->textUI = delegate;

    return self;
    }

AtTextUI AtClishTextUINew(AtTextUI delegate)
    {
    AtTextUI newTextUI = AtOsalMemAlloc(ObjectSize());
    if (newTextUI == NULL)
        return NULL;

    newTextUI = ObjectInit(newTextUI, delegate);
    AtTextUIInit(newTextUI);
    return newTextUI;
    }

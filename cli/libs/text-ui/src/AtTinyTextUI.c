/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Text UI
 *
 * File        : AtTinyTextUI.c
 *
 * Created Date: Nov 5, 2012
 *
 * Description : A tiny text UI that only supports command processing, it does
 *               not support:
 *               - auto completing
 *               - command history
 *               - manual
 *               - batch processing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTinyTextUIInternal.h"
#include "AtOsal.h"
#include "atclib.h"
#include "showtable.h"
#include "stdtable.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#define cCmdListNumCol 2
#define cMaxNumCmdsInHistory 1000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtTinyTextUI)self)

/*--------------------------- Local typedefs ---------------------------------*/
/* Command types */
typedef enum eCommandType
    {
    cCommandSdkNotFound,
    cCommandSdkEnoughParam,
    cCommandSdkNotEnoughParam,
    cCommandSdkTooManyParam
    } eCommandType;

typedef struct tBasicCmdInfo
    {
    char    Name[cCmdNameMaxLen];
    const char    *description;
    const char    *usage;
    } tBasicCmdInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtTinyTextUIMethods m_methods;

/* Override */
static tAtTextUIMethods        m_AtTextUIOverride;

/* Super implementation */
static const tAtTextUIMethods *m_AtTextUIMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool AutotestIsEnabled(void);
extern char** ToToken(const char *str,int *argc);
extern tCmdConf *AtCmdCliTableGet(uint32 *count);

/*--------------------------- Implementation ---------------------------------*/
static tBasicCmdInfo *BasicCmds(uint32 *numCommands)
    {
    static tBasicCmdInfo    basicCmdInfo[] =
        {
            {"listcmd",         "List commands ", "listcmd [option]"},
            {"exit",            "Exit program", "exit"},
            {"quit",            "Exit program", "quit"},
        };

    *numCommands = sizeof(basicCmdInfo) / sizeof(tBasicCmdInfo);
    return basicCmdInfo;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTinyTextUI);
    }

static AtList AdditionalCommands(AtTinyTextUI self)
    {
    if (self->additionalCmds == NULL)
        self->additionalCmds = AtListCreate(0); /* No limit */
    return self->additionalCmds;
    }

static uint32 NumCommands(AtTinyTextUI self)
    {
    return AtListLengthGet(AdditionalCommands(self));
    }

static tCmdConf *CmdDescriptor(AtTinyTextUI self, uint32 commandIndex)
    {
    return (tCmdConf *)((void*)AtListObjectGet(AdditionalCommands(self), commandIndex));
    }

static char *CmdName(AtTinyTextUI self, uint32 commandIndex)
    {
    tCmdConf *descriptor = CmdDescriptor(self, commandIndex);
    return descriptor ? descriptor->cmdName : NULL;
    }

static eBool DatabaseInit(AtTextUI self)
    {
    uint32 i;
    AtTinyTextUI textUI = (AtTinyTextUI)self;

    /* Initialize TRIE of command module */
    textUI->pCmdTrie = AtTrieNew();

    if (textUI->pCmdTrie == NULL)
        return cAtFalse;

    for (i = 0; i < NumCommands(textUI); i++)
        {
        if (AtTrieInsert(textUI->pCmdTrie, CmdName(textUI, i), CmdDescriptor(textUI, i)) == 0)
            {
            AtTrieFree(textUI->pCmdTrie);
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

static void InstallClisFromCliTable(AtTextUI self, tCmdConf *cliTable, uint32 count)
    {
    uint32 i;

    for (i = 0; i < count; i++)
        AtTinyTextUICmdAdd(self, cliTable[i].cmdName, cliTable[i].cmdFunction, cliTable[i].level, (int8)(cliTable[i].numParam));
    }

static void InstallBuiltInClis(AtTextUI self)
    {
    uint32 numClis;
    tCmdConf *cliTable = AtCmdCliTableGet(&numClis);
    if (cliTable)
        InstallClisFromCliTable(self, cliTable, numClis);
    else
        AtTextUIBuiltInCommandsInstall(self);
    }

static void Init(AtTextUI self)
    {
    AtTinyTextUI tinyUI = (AtTinyTextUI)self;

    /* Do nothing if it was initialized */
    if (tinyUI->initialized)
        return;

    m_AtTextUIMethods->Init(self);

    /* Initialize database */
    DatabaseInit(self);
    InstallBuiltInClis(self);

    /* More than one Text-UI can use show table lib, it needs to be checked
     * before initializing it */
    if (!ShowTableLibIsInitialized())
        ShowtableLibInit();

    tinyUI->initialized = 1;
    }

static void Start(AtTextUI self)
    {
    uint32 bufferLen;
    eBool running = cAtTrue;
    char *cmdBuffer = AtTextUICmdBufferGet(self, &bufferLen);

    Init(self);

    if (cmdBuffer == NULL)
    	return;

    while (running)
        {
        AtOsalMemInit(cmdBuffer, 0, bufferLen);
        AtPrintc(cSevNormal, "AT SDK >");

        /* Get command string */
        if ((self->methods->CliGetFromPrompt(self, cmdBuffer, bufferLen) != NULL) &&
            (AtStrlen(cmdBuffer) > 0))
            {
            cmdBuffer[AtStrlen(cmdBuffer) - 1] = '\0';

            /* Exit */
            if (AtStrcmp(cmdBuffer, "exit") == 0 ||
                AtStrcmp(cmdBuffer, "quit") == 0 )
                {
                AtPrintc(cSevInfo, "Exit...\r\n");
                running = cAtFalse;
                }

            if (running)
            	self->methods->CliProcess(self, cmdBuffer);
            }

        /* Cannot get command from prompt, just exit this loop */
        else
            return;
        }
    }

static eBool ShouldVerboseCommand(AtTextUI self, const char * pCmdStr)
    {
    if (!AtTextUIIsVerbosed(self))
        return cAtFalse;

    if (AtStrstr(pCmdStr, "textui mode") || AtStrstr(pCmdStr, "textui verbose"))
        return cAtFalse;

    return cAtTrue;
    }

static eBool CmdEnoughParamProcess(AtTextUI self, int argc, char** argv, tCmdConf *pCmdConf, const char * pCmdStr)
    {
    int numParams = argc - pCmdConf->level;

    if (numParams < 0)
        return cAtFalse;

    /* Print command */
    if (ShouldVerboseCommand(self, pCmdStr))
        AtPrintc(cSevNormal, "%s\r\n", pCmdStr);

    if( (AtStrcmp(argv[0], "echo") == 0) || (AtStrcmp(argv[0], "man") == 0) ||
        (AtStrcmp(argv[0], "for") == 0) || (AtStrcmp(argv[0], "repeat") == 0))
        return pCmdConf->cmdFunction((char)(argc - 1), &argv[1]);

    /* Command has optional parameters */
    if (pCmdConf->numParam < 0)
        {
        if (argc < pCmdConf->level)
            return cAtFalse;

        return pCmdConf->cmdFunction((char)(argc - pCmdConf->level), &argv[pCmdConf->level]);
        }

    if ((numParams >= 0) && (numParams <= pCmdConf->numParam))
        {
        if (numParams == pCmdConf->numParam)
            return pCmdConf->cmdFunction((char)numParams, &(argv[(int)pCmdConf->level]));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool CmdFind(AtTextUI self, char *pCmdStr, tCmdConf **pCmdConf)
    {
    *pCmdConf = (tCmdConf *)AtTrieLookup(((AtTinyTextUI)self)->pCmdTrie, pCmdStr);
    return (*pCmdConf == NULL) ? cAtFalse : cAtTrue;
    }

static eBool IsSpecialCommand(char *commandName)
    {
    static const char *specialCommands[] = {"terminate",
										     "pause",
										     "sleep",
										     "usleep",
										     "rd",
										     "wr",
										     "dump",
										     "fill",
										     "man",
										     "for",
										     "repeat",
										     "echo",
										     "-"
    };
    uint8 i;

    for (i = 0; i < mCount(specialCommands); i++)
        {
        if (AtStrcmp(commandName, specialCommands[i]) == 0)
            return cAtTrue;
        }

    return cAtTrue;
    }

static eBool CmdIsFound(AtTextUI self, int argc, char** argv, tCmdConf **pCmdConf)
    {
    char *pCmdStr = AtTextUICmdBufferGet(self, NULL);
    char backupChar = '\0';
    char *pLastSpace;

    if (pCmdStr == NULL)
    	return cAtFalse;

    AtUnused(argv);
    AtUnused(argc);

    pLastSpace = &(pCmdStr[AtStrlen(pCmdStr) + 1]); /* Plus one for trailing */
    
    /* Start from the last */
    while (pLastSpace != pCmdStr)
        {
        if (CmdFind(self, pCmdStr, pCmdConf))
            {
            *pLastSpace = backupChar;
            return cAtTrue;
            }

        /* Till a space is found or reach begin of string */
        *pLastSpace = backupChar;
        pLastSpace  = pLastSpace - 1;
        while ((pLastSpace != pCmdStr) && (!AtIsSpace(*pLastSpace)))
            pLastSpace = pLastSpace - 1;

        /* Backup and terminate this sub string */
        backupChar = *pLastSpace;
        *pLastSpace = '\0';
        }

    /* Cannot find, restore */
    *pLastSpace = backupChar;

    return cAtFalse;
    }

static eCommandType CommandCheck(AtTextUI self, int argc, char** argv, tCmdConf **pCmdConf, const char *pCmdStr)
    {
    int level;
    int numParam;
	AtUnused(pCmdStr);

    if ((argv == NULL) || (argc < 1))
        return cCommandSdkNotFound;

    /* Find command */
    if (!CmdIsFound(self, argc, argv, pCmdConf))
        return cCommandSdkNotFound;

    /* Special commands */
    if (IsSpecialCommand(argv[0]) || IsSpecialCommand(argv[argc - 1]))
        return cCommandSdkEnoughParam;

    /* This command is SDK command */
    level = (int)(*pCmdConf)->level;
    numParam = (*pCmdConf)->numParam;
    if (numParam == -1)
        return cCommandSdkEnoughParam;

    /* Command has optional parameters */
    if (numParam == -2)
        return cCommandSdkNotEnoughParam;

    /* Command has too many parameters */
    if( argc > (level + numParam))
        return cCommandSdkTooManyParam;

    /* Command doesn't have enough parameters */
    if( argc < (level + numParam) )
        return cCommandSdkNotEnoughParam;

    /* Command has enough parameters */
    return cCommandSdkEnoughParam;
    }

static eBool Process(AtTextUI self, int argc, char **argv, const char * pCmdStr)
    {
    int cmdTokenCount = argc;
    tCmdConf    *pCommandConf = NULL;
    eCommandType cmdType = CommandCheck(self, cmdTokenCount, argv, &pCommandConf, pCmdStr);
    if (cmdType == cCommandSdkNotFound)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid command \"%s\"!\r\n", pCmdStr);
        return cAtFalse;
        }

    if (cmdType == cCommandSdkNotEnoughParam)
        {
        AtPrintc(cSevCritical, "ERROR: \"%s\" does not have enough parameters!\r\n", pCmdStr);
        return cAtFalse;
        }

    if (cmdType == cCommandSdkTooManyParam)
        {
        AtPrintc(cSevCritical, "ERROR: \"%s\" has too many parameters\r\n", pCmdStr);
        return cAtFalse;
        }

    if (!CmdEnoughParamProcess(self, cmdTokenCount, argv, pCommandConf, pCmdStr))
        {
        AtPrintc(cSevCritical, "ERROR: Processing command \"%s\" is failed !\r\n", pCmdStr);
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool StandardCliProcess(AtTextUI self, const char * pCmdStr)
    {
    int argc;
    char **argv;
    int i;
    eBool ret;

    if (AtStrcmp(pCmdStr, "") == 0)
        return cAtFalse;

    /* Extract token, then process */
    argv = ToToken(pCmdStr, &argc);
    if (argv == NULL)
    	return cAtFalse;

    ret  = Process(self, argc, argv, pCmdStr);
    /* Cleanup */
    for (i = 0; i < argc; i++)
        {
        if (argv[i] != NULL)
            AtOsalMemFree(argv[i]);
        }
    AtOsalMemFree(argv);

    return ret;
    }

static char *CliGetFromPrompt(AtTextUI self, char *cliBuffer, uint32 bufferLen)
    {
    if (mThis(self)->ReadLine)
        return mThis(self)->ReadLine(cliBuffer, (int)bufferLen);

    return NULL;
    }

static AtList History(AtTextUI self)
    {
    if (mThis(self)->history == NULL)
        mThis(self)->history = AtListCreate(cMaxNumCmdsInHistory);
    return mThis(self)->history;
    }

static void HistoryAdd(AtTextUI self, const char * pCmdStr)
    {
    char *cmd;
    AtList history;

    if (!AtTextUIHistoryIsEnabled(self))
        return;

    cmd = AtStrdup(pCmdStr);
    if (cmd == NULL)
        return;

    /* History is full, need to remove the oldest CLI */
    history = History(self);
    if (AtListLengthGet(history) == cMaxNumCmdsInHistory)
        AtOsalMemFree(AtListObjectRemoveAtIndex(history, 0));
    if (AtListObjectAdd(history, (AtObject)(void *)cmd) != cAtOk)
        AtOsalMemFree(cmd);
    }

static eBool CliProcessNoCache(AtTextUI self, const char * pCmdStr)
    {
    eBool result;

    AtTextUIHistoryAdd(self, pCmdStr);

    /* Standard command processing function */
    if (!AutotestIsEnabled())
        return StandardCliProcess(self, pCmdStr);

    /* Autotest processing */
    AtPrintc(cSevNormal, "\r\n<Command name=\"%s\">\r\n", pCmdStr);
    result = StandardCliProcess(self, pCmdStr);
    AtPrintc(cSevNormal, "<Cli_Result retcode=\"%s\"/>\r\n", result ? "OK" : "FAIL");
    AtPrintc(cSevNormal, "</Command>\r\n");

    return result;
    }

static AtList ScriptLines(AtTextUI self)
    {
    if (mThis(self)->scriptLines == NULL)
        mThis(self)->scriptLines = AtListCreate(0);
    return mThis(self)->scriptLines;
    }

static eBool CliCache(AtTextUI self, const char * pCmdStr)
    {
    char *dupString = AtStrdup(pCmdStr);

    if (AtListObjectAdd(ScriptLines(self), (AtObject)(void *)dupString) != cAtOk)
        {
        AtOsalMemFree(dupString);
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool IsScriptCommand(const char * pCmdStr)
    {
    if (pCmdStr == NULL)
        return cAtFalse;

    return AtStrstr(pCmdStr, "script ") ? cAtTrue : cAtFalse;
    }

static uint32 RemoveSpaces(const char *buffer, uint32 size, char *destBuffer)
    {
    uint32 i, numChars;
    eBool spaceSaved = cAtTrue; /* To ignore first trailing spaces */

    numChars = 0;
    for (i = 0; i < size; i++)
        {
        /* Not space, save it */
        if (!AtIsSpace(buffer[i]))
            {
            destBuffer[numChars] = buffer[i];
            numChars   = numChars + 1;
            spaceSaved = cAtFalse;
            continue;
            }

        /* It is space, if already save last space, just skip this */
        if (spaceSaved)
            continue;

        /* Save this space */
        destBuffer[numChars] = buffer[i];
        numChars   = numChars + 1;
        spaceSaved = cAtTrue;
        }

    if (numChars == 0)
        return 0;

    /* If the last one is space, also skip it by replacing it with trailing 0 */
    if (AtIsSpace(buffer[numChars - 1]))
        {
        destBuffer[numChars - 1] = '\0';
        numChars = numChars - 1;
        }

    /* And terminate this string */
    destBuffer[numChars] = '\0';

    return numChars;
    }

static char *CacheCommand(AtTextUI self, const char * pCmdStr)
    {
    uint32 bufferSize;
    char *cmdBuffer = AtTextUICmdBufferGet(self, &bufferSize);
    uint32 memorySize = AtStrlen(pCmdStr) + 1;

    /* Not enough buffer to normalize the command, let the outside deal with
     * the risk that command will not be found because of leading spaces */
    if (memorySize > bufferSize)
        return NULL;

    AtOsalMemInit(cmdBuffer, 0, memorySize);
    RemoveSpaces(pCmdStr, memorySize, cmdBuffer);

    return cmdBuffer;
    }

static const char *NormalizeCommand(AtTextUI self, const char * pCmdStr)
    {
    uint32 bufferSize;
    char *cmdBuffer = AtTextUICmdBufferGet(self, &bufferSize);

    /* If input command has not been cached, cache it to internal buffer */
    if (cmdBuffer != pCmdStr)
        {
        if (CacheCommand(self, pCmdStr) == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Command is too long and cannot be normalized. It may be not found because of leading spaces\r\n");
            return pCmdStr;
            }
        }
    else
        {
    	uint32 length = bufferSize;
    	if (pCmdStr != NULL)
    		{
    		length = AtStrlen(pCmdStr);
    		length = mMin(length, bufferSize);
    		}

        RemoveSpaces(cmdBuffer, length, cmdBuffer);
        }

    return cmdBuffer;
    }

static eBool CliProcess(AtTextUI self, const char * pCmdStr)
    {
    const char *normalizedCommand;

    if (pCmdStr == NULL)
        return cAtTrue;

    /* Command may be too long */
    if (AtStrlen(pCmdStr) > AtTextUICmdBufferSizeGet(self))
        {
        AtPrintc(cSevCritical,
                 "ERROR: command \"%s\" is too long. Maximum command length is: %u characters including spaces\r\n",
                 pCmdStr, AtTextUICmdBufferSizeGet(self));
        return cAtFalse;
        }

    normalizedCommand = NormalizeCommand(self, pCmdStr);
    if (IsScriptCommand(normalizedCommand))
        return CliProcessNoCache(self, normalizedCommand);

    if (mThis(self)->scripting)
        return CliCache(self, normalizedCommand);
    else
        return CliProcessNoCache(self, normalizedCommand);
    }

static void AdditionalCommandsDelete(AtTextUI self)
    {
    AtTinyTextUI textUI = (AtTinyTextUI)self;
    AtObject cmdDescriptor;

    while ((cmdDescriptor = AtListObjectGet(textUI->additionalCmds, 0)) != NULL)
        {
        AtListObjectRemove(textUI->additionalCmds, cmdDescriptor);
        AtOsalMemFree(cmdDescriptor);
        }

    AtObjectDelete((AtObject)(textUI->additionalCmds));
    textUI->additionalCmds = NULL;
    }

static void DatabaseFree(AtTextUI self)
    {
    AtTinyTextUI tinyUI = (AtTinyTextUI)self;

    AtTrieFree(tinyUI->pCmdTrie);
    tinyUI->pCmdTrie = NULL;

    AdditionalCommandsDelete(self);
    }

static void DeleteHistory(AtTextUI self)
    {
    AtTextUIHistoryClear(self);
    AtObjectDelete((AtObject)mThis(self)->history);
    mThis(self)->history = NULL;
    }

static void AllScriptLinesDelete(AtTextUI self)
    {
    char *string;

    /* Delete all of commands input so far */
    while ((string = (char *)AtListObjectRemoveAtIndex(mThis(self)->scriptLines, 0)) != NULL)
        AtOsalMemFree(string);
    }

static void ScriptingDelete(AtTextUI self)
    {
    AllScriptLinesDelete(self);
    AtObjectDelete((AtObject)mThis(self)->scriptLines);
    mThis(self)->scriptLines = NULL;
    }

static void Delete(AtTextUI self)
    {
    DatabaseFree(self);
    ShowtableLibShutdown();
    DeleteHistory(self);
    ScriptingDelete(self);

    m_AtTextUIMethods->Delete(self);
    }

static void HistoryShow(AtTextUI self)
    {
    uint32 i;

    for (i = 0; i < AtListLengthGet(History(self)); i++)
        {
        char *cmd = (char *)AtListObjectGet(History(self), i);
        AtPrintc(cSevNormal, "%s\r\n", cmd);
        }
    }

static void HistoryClear(AtTextUI self)
    {
    while (AtListLengthGet(History(self)) > 0)
        AtOsalMemFree(AtListObjectRemoveAtIndex(History(self), 0));
    }

static void HistoryEnable(AtTextUI self, eBool enable)
    {
    mThis(self)->historyEnabled = enable;
    }

static eBool HistoryIsEnabled(AtTextUI self)
    {
    return mThis(self)->historyEnabled;
    }

static uint32 HistoryNumClisGet(AtTextUI self)
    {
    return AtListLengthGet(History(self));
    }

static char *HistoryCliGet(AtTextUI self, uint32 cliIndex)
    {
    return (char *)AtListObjectGet(History(self), cliIndex);
    }

static void ScriptStart(AtTextUI self)
    {
    mThis(self)->scripting = cAtTrue;
    }

static void ScriptCancel(AtTextUI self)
    {
    AllScriptLinesDelete(self);
    mThis(self)->scripting = cAtFalse;
    }

static void ScriptFinish(AtTextUI self)
    {
    uint32 stringSize = 0;
    uint32 cmd_i;
    char *script, *cmd;
    AtList scriptLines = mThis(self)->scriptLines;
    eBool verbose;
    char* aScriptLine;

    mThis(self)->scripting = cAtFalse;

    if (AtListLengthGet(scriptLines) == 0)
        return;

    /* Calculate length of script */
    for (cmd_i = 0; cmd_i < AtListLengthGet(scriptLines); cmd_i++)
        {
        aScriptLine = (char*)AtListObjectGet(scriptLines, cmd_i);
        if (aScriptLine)
            stringSize = stringSize + AtStrlen(aScriptLine) + 3; /* For trailing and \r\n */
        }

    /* Allocate memory for script */
    script = AtOsalMemAlloc(stringSize);
    if (script == NULL)
        {
        AllScriptLinesDelete(self) ;
        mThis(self)->scripting = cAtFalse;
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for script\r\n");
        return;
        }
    AtOsalMemInit(script, 0, stringSize);

    /* Build script */
    while ((cmd = (char *)AtListObjectRemoveAtIndex(scriptLines, 0)) != NULL)
        {
        AtStrcat(script, cmd);
        AtStrcat(script, "\r\n");
        AtOsalMemFree(cmd);
        }

    /* And run */
    verbose = AtTextUIIsVerbosed(self);
    AtTextUIVerbose(self, cAtTrue);
    AtTclEval(AtTextUITclGet(self), script);
    AtTextUIVerbose(self, verbose);
    AtOsalMemFree(script);
    }

static uint32 CmdBufferSize(AtTinyTextUI self)
    {
	AtUnused(self);
    return 256;
    }

static tStdTab *AllCmdTable(AtTinyTextUI self, tStdTab *cmdTable)
    {
    int row = 0;
    int col = 0;
    uint32 i;
    uint32 numCommands = NumCommands(self);
    uint32 rowCount = ((numCommands % cCmdListNumCol) == 0) ? (numCommands / cCmdListNumCol) :
                      ((numCommands / cCmdListNumCol) + 1);

    if (StdTableInit(cmdTable, rowCount, cCmdListNumCol) == cAtFalse )
        return NULL;

    for (i = 0; i < numCommands; i++)
        {
        if (col >= cCmdListNumCol)
            {
            col = 0;
            row ++;
            }
        StdTableStrToCell(&cmdTable->cell[row][col], CmdName(self, i));
        col++;
        }
    AtPrintc(cSevNormal, "\r\nAll Commands :\r\n");

    return cmdTable;
    }

static tStdTab *BasicCmdTable(AtTinyTextUI self, tStdTab *cmdTable)
    {
    int row = 0;
    int col = 0;
    uint32 i;
    uint32 basicCmdTabLen;
    tBasicCmdInfo *basicCmdInfo = BasicCmds(&basicCmdTabLen);

    uint32 rowCount = ((basicCmdTabLen % cCmdListNumCol) == 0) ? (basicCmdTabLen / cCmdListNumCol) :
                                                                 ((basicCmdTabLen / cCmdListNumCol) + 1);

    AtUnused(self);
    if (StdTableInit(cmdTable, rowCount, cCmdListNumCol) == cAtFalse )
        return NULL;

    for(i = 0; i < cCmdListNumCol; i++)
        StdTableStrToCell(&(cmdTable->tittle[i]), "\t");

    for (i = 0; i < basicCmdTabLen; i++)
        {
        if (col >= cCmdListNumCol)
            {
            col = 0;
            row ++;
            }
        StdTableStrToCell(&(cmdTable->cell[row][col]), basicCmdInfo[i].Name);
        col++;
        }

    return cmdTable;
    }

static tStdTab *BuiltInCmdTable(AtTinyTextUI self, tStdTab *cmdTable, char argc, char **argv)
    {
    uint32 rowCount;
    int row = 0;
    int col = 0;
    uint32 i;
    uint32 cmdCount;
    uint32 posOfFirstListedCmd;
    char *pCmdStr;
    int argv_i;
    uint32 numCommands = NumCommands(self);

    pCmdStr = AtTextUICmdBufferGet((AtTextUI)self, NULL);
    if (pCmdStr == NULL)
    	return NULL;

    pCmdStr[0] = '\0';
    AtStrcat(pCmdStr, argv[0]);
    for (argv_i = 1; argv_i < argc; argv_i++)
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, argv[argv_i]);
        }

    /* Count the number of command that have prefix matched with command that is listed */
    cmdCount = 0;
    posOfFirstListedCmd = 0;
    for (i = 0; i < numCommands; i++)
        {
    	char* cmdName = CmdName(self, i);
        if ((cmdName == NULL) || (AtStrstr(cmdName, pCmdStr) != cmdName))
            continue;

        if (posOfFirstListedCmd == 0)
            posOfFirstListedCmd = i;

        cmdCount++;
        }

    /* No command */
    if (cmdCount == 0)
        return NULL;

    /* Initialize table */
    rowCount = ((cmdCount % cCmdListNumCol) == 0)? (cmdCount / cCmdListNumCol) :
                ((cmdCount / cCmdListNumCol) + 1);
    if (StdTableInit(cmdTable, rowCount, cCmdListNumCol) == cAtFalse )
        return NULL;

    /* And put all of commands to this table */
    for (i = posOfFirstListedCmd; (i < numCommands) && (cmdCount > 0); i++)
        {
    	char* cmdName = CmdName(self, i);
        if ((cmdName == NULL) || (AtStrstr(cmdName, pCmdStr) != cmdName))
            continue;

        cmdCount = cmdCount - 1;
        if (col >= cCmdListNumCol)
            {
            col = 0;
            row = row + 1;
            }
        StdTableStrToCell(&(cmdTable->cell[row][col]), cmdName);
        col++;
        }

    return cmdTable;
    }

static tStdTab *CmdTable(AtTinyTextUI self, tStdTab *cmdTable, char argc, char **argv)
    {
    if ((argc == 0) || (AtStrcmp(argv[0], "all") == 0))
        return AllCmdTable(self, cmdTable);

    if (AtStrcmp(argv[0], "basic") == 0)
        return BasicCmdTable(self, cmdTable);

    return BuiltInCmdTable(self, cmdTable, argc, argv);
    }

static void Help(AtTextUI self)
    {
    uint8 i;
    tStdTab cmdDescriptionTab;
    uint32 basicCmdTabLen;
    tBasicCmdInfo *basicCmdInfo = BasicCmds(&basicCmdTabLen);
	AtUnused(self);

    if (StdTableInit(&cmdDescriptionTab, basicCmdTabLen, 2) == cAtFalse )
        return;

    AtPrintc(cSevNormal, "\r\n============================= Help =====================================\r\n");
    AtPrintc(cSevNormal, "\r\nBasic Command:\r\n");

    StdTableStrToCell(&cmdDescriptionTab.tittle[1], "Description");
    StdTableStrToCell(&cmdDescriptionTab.tittle[0], "Usage");
    for (i = 0; i < basicCmdTabLen; i++)
        {
        StdTableStrToCell(&cmdDescriptionTab.cell[i][1], basicCmdInfo[i].description);
        StdTableStrToCell(&cmdDescriptionTab.cell[i][0], basicCmdInfo[i].usage);
        }
    StdTablePrint(&cmdDescriptionTab, cAtTrue);

    StdTableFree(&cmdDescriptionTab);

    AtPrintc(cSevNormal, "\r\nTo view other extended commands, please type 'listcmd'.\r\n");
    AtPrintc(cSevNormal, "\r\n========================================================================\r\n\r\n");
    }

static void CmdShow(AtTextUI self, char **tokens, uint32 numTokens)
    {
    tStdTab tableCmd;
    tStdTab *pTableCmd = CmdTable((AtTinyTextUI)self, &tableCmd, (char)numTokens, tokens);

    if (pTableCmd == NULL)
        {
        AtPrintc(cSevInfo, "No command to list\r\n");
        return;
        }

    StdTablePrint(pTableCmd, cAtFalse);
    StdTableFree(pTableCmd);
    }

static void MethodsInit(AtTinyTextUI self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit((void*)&m_methods, 0, sizeof(m_methods));
        m_methods.CmdBufferSize     = CmdBufferSize;
        }

    self->methods = &m_methods;
    }

static void Override(AtTextUI self)
    {
    if (!m_methodsInit)
        {
        m_AtTextUIMethods = self->methods;
        AtOsalMemCpy(&m_AtTextUIOverride, m_AtTextUIMethods, sizeof(m_AtTextUIOverride));
        m_AtTextUIOverride.Init              = Init;
        m_AtTextUIOverride.Start             = Start;
        m_AtTextUIOverride.Delete            = Delete;
        m_AtTextUIOverride.CliProcess        = CliProcess;
        m_AtTextUIOverride.CliGetFromPrompt  = CliGetFromPrompt;
        m_AtTextUIOverride.HistoryShow       = HistoryShow;
        m_AtTextUIOverride.HistoryClear      = HistoryClear;
        m_AtTextUIOverride.HistoryEnable     = HistoryEnable;
        m_AtTextUIOverride.HistoryIsEnabled  = HistoryIsEnabled;
        m_AtTextUIOverride.HistoryAdd        = HistoryAdd;
        m_AtTextUIOverride.HistoryNumClisGet = HistoryNumClisGet;
        m_AtTextUIOverride.HistoryCliGet     = HistoryCliGet;
        m_AtTextUIOverride.ScriptStart       = ScriptStart;
        m_AtTextUIOverride.ScriptCancel      = ScriptCancel;
        m_AtTextUIOverride.ScriptFinish      = ScriptFinish;
        m_AtTextUIOverride.Help              = Help;
        m_AtTextUIOverride.CmdShow           = CmdShow;
        }

    self->methods = &m_AtTextUIOverride;
    }

AtTextUI AtTinyTextUIObjectInit(AtTextUI self)
    {
    AtTinyTextUI tinyUI = (AtTinyTextUI)self;

    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super */
    if (AtTextUIObjectInit(self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(tinyUI);
    m_methodsInit = 1;

    return self;
    }

AtTextUI AtTinyTextUINew(void)
    {
    AtTextUI newTextUI = AtOsalMemAlloc(ObjectSize());
    if (newTextUI == NULL)
        return NULL;

    newTextUI = AtTinyTextUIObjectInit(newTextUI);
    AtTextUIInit(newTextUI);
    return newTextUI;
    }

AtTextUI AtTinyTextUIWithReadLineNew(char *(*ReadLine)(char *buffer, int size))
    {
    AtTinyTextUI newTextUI = (AtTinyTextUI)AtTinyTextUINew();
    newTextUI->ReadLine = ReadLine;
    return (AtTextUI)newTextUI;
    }

/**
 * Add new command
 *
 * @param self This Tiny Text-UI
 * @param cmdName Command name which is entered on console
 * @param cmdFunction Function that processes this command
 * @param numLevels Number of levels
 * @param numParams Number of parameters
 *
 * @retval cAtFalse - If cannot add this command
 * @retval cAtTrue - If this command is added successfully
 */
eBool AtTinyTextUICmdAdd(AtTextUI self,
                         const char *cmdName,
                         eBool (*cmdFunction)(char argc, char **argv),
                         uint8 numLevels,
                         int8 numParams)
    {
    uint32 memorySize;
    tCmdConf *conf;
    AtTinyTextUI tinyTextUI = (AtTinyTextUI)self;

    if (tinyTextUI == NULL)
        return cAtFalse;

    /* Need memory */
    memorySize = sizeof(tCmdConf);
    conf = AtOsalMemAlloc(memorySize);
    if (conf == NULL)
        return cAtFalse;
    AtOsalMemInit(conf, 0, memorySize);

    /* Save information */
    AtOsalMemCpy(conf->cmdName, cmdName, AtStrlen(cmdName));
    conf->cmdFunction = cmdFunction;
    conf->level       = numLevels;
    conf->numParam    = numParams;

    /* Then add it to the additional command list */
    if (AtListObjectAdd(AdditionalCommands(tinyTextUI), (AtObject)conf) != cAtOk)
        return cAtFalse;

    if (AtTrieInsert(tinyTextUI->pCmdTrie, conf->cmdName, conf) == 0)
        {
        AtListObjectRemove(AdditionalCommands(tinyTextUI), (AtObject)conf);
        AtOsalMemFree(conf);
        return cAtFalse;
        }

    return cAtTrue;
    }

/**
 * Create Iterator for all registered commands
 * @param self Tiny text-ui
 * @return AtIterator
 */
AtIterator AtTinyTextUICmdsIteratorCreate(AtTextUI self)
    {
    AtTinyTextUI tinyTextUI = (AtTinyTextUI)self;
    return AtListIteratorCreate(tinyTextUI->additionalCmds);
    }

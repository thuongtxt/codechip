/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Text-UI
 * 
 * File        : AtTinyTextUIInternal.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : Tiny Text-UI
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTINYTEXTUIINTERNAL_H_
#define _ATTINYTEXTUIINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTextUIInternal.h"
#include "AtList.h"
#include "AtTcl.h"
#include "trie.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTinyTextUI * AtTinyTextUI;

typedef struct tAtTinyTextUIMethods
    {
    /* Public */
    uint32 (*CmdBufferSize)(AtTinyTextUI self);
    }tAtTinyTextUIMethods;

typedef struct tAtTinyTextUI
    {
    tAtTextUI super;
    const tAtTinyTextUIMethods* methods;

    /* Private data */
    tAtTrie *pCmdTrie;
    uint8 initialized;
    AtList additionalCmds;

    /* History */
    AtList history;
    eBool historyEnabled;

    /* Scripting */
    eBool scripting;
    AtList scriptLines;

    /* Some none-os environment does not have standard fgets. */
    char *(*ReadLine)(char *buffer, int size);
    }tAtTinyTextUI;

typedef struct tAtDefaultTinyTextUI
    {
    tAtTinyTextUI super;
    }tAtDefaultTinyTextUI;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTextUI AtTinyTextUIObjectInit(AtTextUI self);
AtTextUI AtDefaultTinyTextUIObjectInit(AtTextUI self);

#ifdef __cplusplus
}
#endif
#endif /* _ATTINYTEXTUIINTERNAL_H_ */


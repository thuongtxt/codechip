/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Text-UI
 *
 * File        : AtDefaultTinyTextUI.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : Default Tiny Text-UI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtTinyTextUIInternal.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTextUIMethods m_AtTextUIOverride;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool IsComment(const char *str);
extern char *StrTrimLeft(char * pStr);
extern char** ToToken(const char *str, int *argc);

/*--------------------------- Implementation ---------------------------------*/
static char *CliGetFromPrompt(AtTextUI self, char *cliBuffer, uint32 bufferLen)
    {
    AtUnused(self);
    return AtFileGets(AtStdStandardInput(AtStdSharedStdGet()), cliBuffer, bufferLen);
    }

static void TokensFree(char **tokens, int tokenCount)
    {
    int i;

    /* Free memory */
    for(i = 0; i < tokenCount; i++)
        {
        if (tokens[i] != NULL)
            AtOsalMemFree(tokens[i]);
        }
    AtOsalMemFree(tokens);
    }

static eBool ScriptRun(AtTextUI self, const char * pScriptFile)
    {
    AtFile batchFile;
    char *cmdBuff, *trimCmd;
    AtStd std = AtStdSharedStdGet();
    static const uint32 cCmdStringMaxLength = 10 * 1024;
    eBool isVerbose = AtTextUIIsVerbosed(self);
    eBool historyEnabled = AtTextUIHistoryIsEnabled(self);
    eBool success = cAtTrue;

    /* Open file */
    batchFile = AtStdFileOpen(std, pScriptFile, cAtFileOpenModeRead);
    if(batchFile == NULL)
        return cAtFalse;

    /* Need buffer */
    cmdBuff = (char *)AtOsalMemAlloc(cCmdStringMaxLength);
    if (cmdBuff == NULL)
        {
        AtFileClose(batchFile);
        return cAtFalse;
        }

    AtTextUIVerbose(self, AtTextUIScriptIsVerbosed(self));
    AtTextUIHistoryEnable(self, cAtFalse);

    /* Process batch file */
    while(!AtFileEof(batchFile))
        {
        char **tokens;
        int tokenCount;

        if ((AtFileGets(batchFile, cmdBuff, cCmdStringMaxLength) == NULL) || (AtStrlen(cmdBuff) == 0))
            continue;

        /* Ignore comment */
        if (IsComment(StrTrimLeft(cmdBuff)))
            continue;

        tokens = ToToken(cmdBuff, &tokenCount);
        if (tokens == NULL)
            continue;
        if (tokenCount == 0)
            {
            TokensFree(tokens, tokenCount);
            continue;
            }

        /* Need to stop while executing */
        if (AtStrcmp(tokens[0], "terminate") == 0)
            {
            TokensFree(tokens, tokenCount);
            break;
            }

        /* Text-UI CLIs */
        trimCmd = AtStringTrim(cmdBuff);
        if (!AtTextUICmdProcess(self, trimCmd))
            success = cAtFalse;

        /* Free memory */
        TokensFree(tokens, tokenCount);
        }

    /* Release buffer and close file */
    AtOsalMemFree(cmdBuff);
    if (AtFileClose(batchFile) != 0)
        AtPrintc(cSevCritical, "ERROR: Cannot close file\r\n");

    AtTextUIVerbose(self, isVerbose);
    AtTextUIHistoryEnable(self, historyEnabled);

    return success;
    }

static void OverrideAtTextUI(AtTextUI self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtTextUIOverride, self->methods, sizeof(m_AtTextUIOverride));
        m_AtTextUIOverride.CliGetFromPrompt = CliGetFromPrompt;
        m_AtTextUIOverride.ScriptRun        = ScriptRun;
        }

    self->methods = &m_AtTextUIOverride;
    }

static void Override(AtTextUI self)
    {
    OverrideAtTextUI(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultTinyTextUI);
    }

AtTextUI AtDefaultTinyTextUIObjectInit(AtTextUI self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtTinyTextUIObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTextUI AtDefaultTinyTextUINew(void)
    {
    AtTextUI newTextUI = AtOsalMemAlloc(ObjectSize());
    if (newTextUI == NULL)
        return NULL;

    newTextUI = AtDefaultTinyTextUIObjectInit(newTextUI);
    AtTextUIInit(newTextUI);
    return newTextUI;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies. 
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : textui
 *
 * File        : extcmdgen.c
 *
 * Created Date: 
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "attypes.h"
#include "atclib.h"
#include "AtTextUI.h"

#ifndef QNX
#include    <getopt.h>
#endif

/*--------------------------- Define -----------------------------------------*/
#define		cIncludeStr					"include"
#define		cStructCmdConf				"tCmdConf"		/* The name of the structure */
#define		cCmdConfArray				"cmdConf"		/* The variable name */
#define		cCmdConfArraySize			"cmdConfCount"
#define cCommentChar					'#'

#define cFileNameMaxLen                 256
#define cCmdConfLineMaxLen              2048
#define cCmdLevelMax                    10
#define cCmdConfFileName                "cmd.conf"
#define cFullCmdConfFileName            "allcmd.conf"
#define cCmdTabFileName                 "./cmdconf.c"     /* Ouput c file */
#define cCmdLibHeaderFileName           "./cmd.h"

#undef mError
#define mError(FMT, PARMS...)			AtPrintf("\033[1;31m"FMT"\033[0m", ## PARMS)

/*--------------------------- Local Typedefs ---------------------------------*/ 

/*--------------------------- Global variables -------------------------------*/ 

/*--------------------------- Forward declaration ----------------------------*/
eBool CmdLibCreateRecursive(const char * confFileName, FILE * pOutConfFile, 
						  FILE * pCmdLibFile, FILE * pCmdHeaderFile);
eBool CmdLibCreate(const char * confFileName, const char * outConfFileName, 
				  const char * cmdLibFileName, const char * cmdHeaderFileName);
void HelpPrint(char *pProgName);

extern char** ToToken(const char *str,int *argc);
extern eBool IsComment(const char *str);
extern eBool IsValidName(const char *name);
extern eBool IsDescriptEnd(const char *str);
extern eBool IsDescriptStart(const char *str);
extern char *StrTrimLeft(char * pStr);

/*--------------------------- Local variables --------------------------------*/ 
char	first = 1;

#ifndef QNX
struct option	opts[] = { 
	{"conffile", 1, NULL, 1},
	{"fullconffile", 1, NULL, 2},
	{"cmdlibfile", 1, NULL, 3},
	{"cmdheaderfile", 1, NULL, 4},
	{0, 0, 0, 0}
	};
#endif

#ifdef CMD_CONF_IN_STR
int cmdPosition = 0;
#endif
/*--------------------------- Entries ----------------------------------------*/ 

/*-----------------------------------------------------------------------------
Function Name	:	CmdLibCreateRecursive

Description		:	Recursive function to create Command Library.

Inputs			:	confFileName		- The name of Command Configuration File
					pOutConfFile		- Temp Command Configuration file stream
					pCmdLibFile			- Command Library file stream
					pCmdHeaderFile		- Command Library header file stream

Outputs			:	None

Return Code		:	cAtTrue				- Success
					cAtFalse				- Fail
------------------------------------------------------------------------------*/

eBool CmdLibCreateRecursive(const char * confFileName, FILE * pOutConfFile, 
						    FILE * pCmdLibFile, FILE * pCmdHeaderFile)
	{
	char	**tokens;
	int 	tokenCount;
	char 	*pBuff;	
	char	*pStrWrite;
	int		i;
	char 	comment;
	int 	line;
	int		level;
	int 	isLib;
	eBool	ret;
	FILE 	*pConfFile;
	char	includedFileName[cFileNameMaxLen];
	char	*pStrTmp;

#ifdef CMD_CONF_IN_STR
    unsigned int     numberChar;
#endif


	/* Initialize default value */
	tokens = NULL;
	tokenCount = 0;
	comment = 0;
	line = 0;
	isLib = 1;
	ret = cAtTrue;
	pBuff = NULL;
	pStrWrite = NULL;
	pConfFile = NULL;

	if((pConfFile = fopen(confFileName, "r")) == NULL)
		{
		mError("Can not open file : %s\n", confFileName);
		return cAtFalse;
		}
	printf("Open config file '%s'\n", confFileName);

	pBuff = (char *)malloc(cCmdConfLineMaxLen + 1);
	pStrWrite = (char *)malloc(cCmdConfLineMaxLen + 1);
	pStrTmp = (char *)malloc(cCmdConfLineMaxLen + 1);

	if( pBuff == NULL || pStrWrite == NULL || pStrTmp == NULL )
		{
		mError("Can not allocate memory\n");
		ret = cAtFalse;
		}
	else
		{
		memset(pBuff, 0, cCmdConfLineMaxLen);
		memset(pStrWrite, 0, cCmdConfLineMaxLen);

		memset(includedFileName, 0, cFileNameMaxLen);

#ifdef CMD_CONF_IN_STR
		numberChar = 0;
#endif
		while( !feof(pConfFile) )
			{
#ifdef CMD_CONF_IN_STR
			cmdPosition += numberChar;
			numberChar = 0;
#endif
			if ( fgets(pBuff, cCmdConfLineMaxLen, pConfFile) != NULL )
				{			
				memset(pStrTmp, 0, cCmdConfLineMaxLen);

				sscanf(pBuff, "%s%s", pStrTmp, includedFileName);

				if ( (comment == 0) && (strcmp(pStrTmp, cIncludeStr) == 0) )
					{
					CmdLibCreateRecursive(includedFileName, pOutConfFile, pCmdLibFile, pCmdHeaderFile);
#ifndef CMD_CONF_IN_STR
					fputs("\n", pOutConfFile);
#endif
					}
				else
					{
					/*----------Write command configuration to allcmd.conf ---------*/
#ifdef CMD_CONF_IN_STR
					char *pChar;
					pChar = pBuff;
					while(*pChar != '\0')
						{
						if (*pChar == '\n')
							{
							fputc('\\', pOutConfFile);
							fputc('n', pOutConfFile);
							fputc('\"', pOutConfFile);   /* End of this line */
							fputc(*pChar, pOutConfFile); /* Make new line */
							fputc('\"', pOutConfFile);   /* Begin of new line */
							numberChar += 1;
							}
						else if ((*pChar == '\"') ||
								 (*pChar == '\''))
							{
							fputc('\\', pOutConfFile);
							fputc(*pChar, pOutConfFile);
							numberChar += 1;
							}
						else if ((*pChar == '\t'))
							{
							fputc('\\', pOutConfFile);
							fputc('t', pOutConfFile);
							numberChar += 1;
							}
						else if(*pChar != '\r')
							{
							fputc(*pChar, pOutConfFile);
							numberChar++;
							}
						pChar++;
						}
#else
					fputs(pBuff, pOutConfFile);
#endif

					/*----------Write to Command Library File ----------------------*/
					line++;

					/* Comment line */
					if (IsComment(StrTrimLeft(pBuff)) == 1)
						{
						continue;
						}		
					/* Start of description */
					if (IsDescriptStart(pBuff) == 1)
						{
						if(comment == 1)
							{
							mError("\n%s: line %d: Not found the end of description before\n", confFileName, line);
							break;
							}
						else
							{
							comment = 1;
							/* Check for end of description here */
							if (IsDescriptEnd(pBuff) == 1)
								{
								comment = 0;
								}
							}
						}
					/* End of description */
					else if (IsDescriptEnd(pBuff) == 1)
						{			
						if(comment == 0)
							{
							mError("\n%s: line %d: Not found the start of description before\n", confFileName, line);
							break;
							}
						else
							{
							comment = 0;
							}
						}
					/* Error in start of description */
					else if (strstr(pBuff,"/*") != NULL)
						{
						mError("\n%s: line %d: Error at the start of description\n", confFileName, line);
						comment = 1;
						break;
						}
					/* Error in end of description */
					else if (strstr(pBuff,"*/") != NULL)
						{
						mError("\n%s: line %d: Error at the end of description\n", confFileName, line);
						comment = 0;
						break;
						}
					/* Declaration */
					else if (comment == 0)
						{
						int numParam;
						
						/* Read declaration */
						tokens = (char **)ToToken(pBuff, &tokenCount);
						/* Read next line if meet empty token */
						if (tokenCount <= 0)
							{
							continue;
							}
						if ((isLib == 1) && (strcmp(tokens[0],"-dl") == 0))
							{
							isLib = 0;
							continue;
							}
						/* Get the number of token of command name */
						level = atoi(tokens[0]);
						if (level > cCmdLevelMax)
							{
							mError("\n%s: line %d: The number of command name exceed %d\n", confFileName, line, cCmdLevelMax);
							break;
							}

						if (tokenCount < (level + 3))
							{
							mError("\n%s: line %d: The number of token is invalid\n", confFileName, line);
							/* Free allocated memory */
							if (tokens != NULL)
								{
								for (i = 0; i < tokenCount; i++)
									{
									if (tokens[i] != NULL)
										{
										free(tokens[i]);
										}
									}
								free(tokens);
								}

							break;
							}

						/* Check number of token */
						numParam = strtol(tokens[level + 2], NULL, 10);
						
						if ((numParam >=0) && (tokenCount != level + 3 + numParam))
							{
							mError("\n%s: line %d: The number of token is invalid\n", confFileName, line);

							/* Free allocated memory */
							if (tokens != NULL)
								{
								for (i = 0; i < tokenCount; i++)
									{
									if (tokens[i] != NULL)
										{
										free(tokens[i]);
										}
									}
								free(tokens);
								}

							break;
							}

						/* Write out to file */
						if (first == 1)
							{
							first = 0;
							}
						for (i = 0; i < level; i++)
							{
							if ( IsValidName(tokens[i + 1]) == 0 )
								{
								mError("\n%s: line %d: Invalid character\n", confFileName, line);
								ret = cAtFalse;
								break;
								}
							}

						if( ret == cAtTrue )
							{
							int tokenIndex = 1;
							char *functionHandle = NULL;
							char *numParams = NULL;

							memset(pStrWrite, 0, cCmdConfLineMaxLen);

							/* Write to file */
							/* Command name */
							strcat(pStrWrite, tokens[tokenIndex++]);
							for(;tokenIndex <= level;)
								{
								strcat(pStrWrite, " ");
								strcat(pStrWrite, tokens[tokenIndex++]);
								}

							/* Add funcHandle */
							functionHandle = tokens[tokenIndex++];

							/* Add num param */
							numParams = tokens[tokenIndex++];

							fprintf(pCmdLibFile,
							        "    AtTinyTextUICmdAdd(self, \"%s\", %s, %d, %s);\n",
							        pStrWrite, functionHandle, level, numParams);

							sprintf(pStrWrite, "eBool %s(char argc, char **argv);\n", tokens[level + 1]);
							fputs(pStrWrite,pCmdHeaderFile);
							}

						/* Free allocated memory */
						if (tokens != NULL)
							{
							for (i = 0; i < tokenCount; i++)
								{
								if (tokens[i] != NULL)
									{
									free(tokens[i]);
									}
								}
							free(tokens);
							}
						}
					}
				}
			}
#ifdef CMD_CONF_IN_STR
			cmdPosition += numberChar;
#endif

		if ( feof(pConfFile) && comment == 1 )
			{
			mError("\n%s: line %d: Not found the end of description before\n", confFileName, line);
			}
#ifndef CMD_CONF_IN_STR
		fputs("\n", pOutConfFile);
#endif
		}
	
	/* Free allocated memory */
	if( pBuff != NULL )
		{
		free(pBuff);
		}
	if( pStrWrite != NULL )
		{
		free(pStrWrite);
		}
	if( pStrTmp != NULL )
		{
		free(pStrTmp);
		}

	fclose(pConfFile);

	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	CmdLibCreate

Description		:	Create Command Library File from Command Configuration File

Inputs			:	confFileName		- The name of Command Configuration File
					outConfFileName		- The name of temp Command Configuration File
					cmdLibFileName		- The name of Command Library file

Outputs			:	None

Return Code		:	cAtTrue				- Success
					cAtFalse				- Fail
------------------------------------------------------------------------------*/

eBool CmdLibCreate(const char * confFileName, const char * outConfFileName, 
				  const char * cmdLibFileName, const char * cmdHeaderFileName)
	{
	FILE	*pOutConfFile;
	FILE 	*pCmdLibFile;
	FILE	*pCmdHeaderFile;
	char	*pStrWrite;
	eBool	ret;
	
	/* Initialize default value */
	ret = cAtTrue;
	pStrWrite = NULL;

	pOutConfFile = NULL;
	pCmdLibFile = NULL;
	pCmdHeaderFile = NULL;

	if((pOutConfFile = fopen(outConfFileName, "w")) == NULL)
		{
		mError("Can not create file : %s\n", outConfFileName);
		return cAtFalse;
		}

#ifdef CMD_CONF_IN_STR
	fputs("char *pAllCmdConfStr = \"",pOutConfFile);
#endif
	
	if((pCmdLibFile = fopen(cmdLibFileName, "w")) == NULL)
		{
		mError("Can not create file : %s\n", cmdLibFileName);
		fclose(pOutConfFile);
		return cAtFalse;
		}
		
	if((pCmdHeaderFile = fopen(cmdHeaderFileName, "w")) == NULL)
		{
		mError("Can not create file : %s\n", cmdHeaderFileName);
		fclose(pOutConfFile);
		fclose(pCmdLibFile);
		return cAtFalse;
		}

	pStrWrite = (char *)malloc(cCmdConfLineMaxLen * 2 + 1);

	if( pStrWrite == NULL )
		{
		mError("Can not allocate memory\n");
		ret = cAtFalse;
		}
	else
		{
		char *pFileName;

		memset(pStrWrite, 0, cCmdConfLineMaxLen * 2 + 1);
		/* Print head for header file */
		strcat(pStrWrite, "/*------------------------------------------------------------------------------\n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* COPYRIGHT (C) 2007 Arrive Technologies Inc.\n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* The information contained herein is confidential property of Arrive Tecnologies. \n");
		strcat(pStrWrite, "* The use, copying, transfer or disclosure of such information \n");
		strcat(pStrWrite, "* is prohibited except by express written agreement with Arrive Technologies.\n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* Module      : \n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* File        : \n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* Created Date: \n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* Description : \n");
		strcat(pStrWrite, "*\n");
		strcat(pStrWrite, "* Notes       : \n");
		strcat(pStrWrite, "*-----------------------------------------------------------------------------*/\n");
		strcat(pStrWrite, "\n");

		fputs(pStrWrite, pCmdHeaderFile);
		fputs(pStrWrite, pCmdLibFile);

		fputs("#ifndef CMD_HEADER\n#define CMD_HEADER\n\n", pCmdHeaderFile);

		fputs("\n#ifdef __cplusplus\nextern \"C\" {\n#endif\n\n", pCmdHeaderFile);

		fputs("\n\n/*--------------------------- Entries ----------------------------------------*/\n\n", pCmdHeaderFile);

		fputs("#include \"attypes.h\"\n", pCmdLibFile);
		fputs("#include \"AtTextUI.h\"\n", pCmdLibFile);

		pFileName = strrchr(cmdHeaderFileName, '/');
		if (pFileName != NULL)
			{
			sprintf(pStrWrite, "#include \"%s\"\n", pFileName + 1);
			}
		else
			{
			sprintf(pStrWrite, "#include \"%s\"\n", cmdHeaderFileName);
			}
		fputs(pStrWrite, pCmdLibFile);

		fputs("\n\n/*--------------------------- Global variables -------------------------------*/\n", pCmdLibFile);

#ifdef CMD_CONF_IN_STR
		cmdPosition = 0;
#endif

		fprintf(pCmdLibFile, "void AtTextUIBuiltInCommandsInstall(AtTextUI self)\n");
		fprintf(pCmdLibFile, "    {\n");
		CmdLibCreateRecursive(confFileName, pOutConfFile, pCmdLibFile, pCmdHeaderFile);
		fprintf(pCmdLibFile, "    }\n");

		/* With the new model, we do not use static table, but for compatible with previous project, let return a NULL table */
		fprintf(pCmdLibFile, "\n");
		fprintf(pCmdLibFile, "tCmdConf *AtCmdCliTableGet(uint32 *count);\n");
		fprintf(pCmdLibFile, "tCmdConf *AtCmdCliTableGet(uint32 *count)\n");
		fprintf(pCmdLibFile, "    {                                    \n");
		fprintf(pCmdLibFile, "    if (count)                           \n");
		fprintf(pCmdLibFile, "        *count = 0;                      \n");
		fprintf(pCmdLibFile, "    return NULL;                         \n");
		fprintf(pCmdLibFile, "    }                                    \n");

		fputs("\n#ifdef __cplusplus\n}\n#endif\n", pCmdHeaderFile);
		fputs("\n#endif /* CMD_HEADER */\n", pCmdHeaderFile);
		}

	/* Free allocated memory */
	if( pStrWrite != NULL )
		{
		free(pStrWrite);
		}

	/* Close all opened file */
#ifdef CMD_CONF_IN_STR
	fputs("\";\n",pOutConfFile);
#endif
	fclose(pOutConfFile);
	fclose(pCmdLibFile);
	fclose(pCmdHeaderFile);

	return ret;
	}

/*----------------------------------------------------------------------------
Function Name	:	HelpPrint

Description		:	Print the help of program

Inputs			:	pProgName	- The name of program

Outputs			:	None

Return Code		:	None
-----------------------------------------------------------------------------*/

void HelpPrint(char *pProgName)
	{
	printf("Usage: %s [options]\n", pProgName);
	printf("Options: \n");
#ifdef QNX
	printf("    -i                    Display help information\n");
	printf("    -f			          Set name for command configuration file\n");
	printf("    -e				      Set name for the file contain all command configuration\n");
	printf("    -c			          Set name for command library file\n");
	printf("    -h					  Set name for command header file\n");
#else
	printf("    -h                    Display help information\n");
	printf("    --conffile            Set name for command configuration file\n");
	printf("    --fullconffile        Set name for the file contain all command configuration\n");
	printf("    --cmdlibfile          Set name for command library file\n");
	printf("    --cmdheaderfile       Set name for command header file\n");
#endif
	}

/*-----------------------------------------------------------------------------
Function Name	:	main

Description		:	Main function of the program

Inputs			:	argc		- The number of arguments
					argv[]		- The arguments list.

Outputs			:	None

Return Code		:	cAtTrue				- Success
					cAtFalse				- Fail
------------------------------------------------------------------------------*/

int main(int argc, char* argv[])
	{
	char	confFileName[cFileNameMaxLen];
	char	fullConfFileName[cFileNameMaxLen];
	char	cmdLibFileName[cFileNameMaxLen];
	char	cmdHeaderFileName[cFileNameMaxLen];
	int		opt;
	int		optIndex = 0;

	AtOsalSharedSet(AtOsalLinux());

	sprintf(confFileName, "%s", cCmdConfFileName);
#ifdef CMD_CONF_IN_STR
	sprintf(fullConfFileName, "%s", "allcmd.c");
#else
	sprintf(fullConfFileName, "%s", cFullCmdConfFileName);
#endif
	sprintf(cmdLibFileName, "%s", cCmdTabFileName);
	sprintf(cmdHeaderFileName, "%s", cCmdLibHeaderFileName);

#ifdef QNX
	while( ( opt = getopt( argc, argv, "if:e:c:h:") ) != EOF )
#else
	while( ( opt = getopt_long( argc, argv, "h", opts,  &optIndex) ) != EOF )
#endif
		{
		switch(opt)
			{
#ifdef QNX
			case 'i':
				HelpPrint("extcmdgen");
				return 0;
			case 'f':
				if(optarg)
					{
					sprintf(confFileName, "%s", optarg);
					}
				break;
			case 'e':
				if(optarg)
					{
					sprintf(fullConfFileName, "%s", optarg);
					}
				break;
			case 'c':
				if(optarg)
					{
					sprintf(cmdLibFileName, "%s", optarg);
					}
				break;
			case 'h':
				if(optarg)
					{
					sprintf(cmdHeaderFileName, "%s", optarg);
					}
				break;
#else
			case 'h':
				HelpPrint("extcmdgen");
				return 0;
			case 1:
				if(optarg)
					{
					sprintf(confFileName, "%s", optarg);
					}
				break;
			case 2:
				if(optarg)
					{
					sprintf(fullConfFileName, "%s", optarg);
					}
				break;
			case 3:
				if(optarg)
					{
					sprintf(cmdLibFileName, "%s", optarg);
					}
				break;
			case 4:
				if(optarg)
					{
					sprintf(cmdHeaderFileName, "%s", optarg);
					}
				break;
#endif
			}
		}

	printf("\n");
	printf("Configuration file: %s\n", confFileName);
	printf("Full Configuration file: %s\n", fullConfFileName);
	printf("Command Library file: %s\n", cmdLibFileName);
	printf("Command header file: %s\n", cmdHeaderFileName);
	printf("\n");

	CmdLibCreate(confFileName, fullConfFileName, cmdLibFileName, cmdHeaderFileName);
	return 0;
	}

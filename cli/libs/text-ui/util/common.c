/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TUI
 *
 * File        : common.c
 *
 * Created Date: Nov 5, 2012
 *
 * Description : common routine of text UI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"
#include "atclib.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_autotestOn = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * Enable/disable autotest
 */
void AutotestEnable(eBool enable)
    {
    m_autotestOn = enable;
    }

/**
 * Check if autotest is enabled or not
 */
eBool AutotestIsEnabled(void)
    {
    return m_autotestOn;
    }

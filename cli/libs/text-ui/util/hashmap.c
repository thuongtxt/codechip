/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/
#include <assert.h>
#include "atclib.h"
#include "AtOsal.h"
#include "athash.h"
#include "hashmap.h"
#include "AtTextUICmd.h"
#include "atclib.h"
#include "../../../../util/include/atcrc16.h"
#include "../../../../util/include/atcrc32.h"

/*--------------------------- Forward declaration ----------------------------*/
#define mAtMapSize(_self)        (((struct tAtHashMap*)_self)->size)

/*--------------------------- Implement ----------------------------*/
static uint32 Crc32Get(const void *key)
    {
    return AtCrc32(0xFFFFFFFF, key, AtStrlen(key));
    }

static uint32 hashFunc(void* key)
    {
    uint32 crc  = AtCrc32(0xFFFFFFFF, key, AtStrlen(key));
    uint32 hash =  AtSdbmHash(key, AtStrlen(key));
    return ((crc + hash)/2);
    }

static void _CalculateHash(const struct tAtHashMap* self,
                           void* key,
                           uint32* hashValue,
                           uint32* index)
    {
    AtUnused(self);
    *hashValue = hashFunc(key);
    *index = (uint32)((*hashValue)  % cHashMapTableSize);
    }


/* Create an entry in a bucket */
static tAtHashMapEntry* AtEntryCreate(struct tAtHashMap* self,
                                      const void* key,
                                      void* data,
                                      uint32 hashValue)
    {
    tAtHashMapEntry* entry = AtOsalMemAlloc(sizeof(tAtHashMapEntry));
    AtUnused(self);
    assert(entry);
    entry->data = data;
    entry->crc32 = Crc32Get(key);
    entry->hashValue = hashValue;
    entry->next = NULL;
    return entry;
    }

static void AtHashMapRemoveAll(tAtHashMap* self)
    {
    tAtHashMapEntry* entry = NULL;
    tAtHashMapEntry* prev = NULL;
    uint32 i;

    for (i = 0; i < mAtMapSize(self); i++)
        {
        entry = self->bucket[i];

        while (entry)
            {
            prev = entry;
            entry = entry->next;
            AtOsalMemFree(prev);
            }

        /* Delete bucket */
        self->bucket[i] = NULL;
        }

    self->count = 0;
    self->collisionCount = 0;
    }

uint32 AtHashMapCollisionCount(const tAtHashMap* self)
    {
    return self->collisionCount;
    }

uint32 AtHashMapCount(const tAtHashMap* self)
    {
    return self->count;
    }

int AtHashMapInsert(tAtHashMap* self, void * key, void * data)
    {
    uint32 hashValue = 0;
    uint32 index = 0;

    /* Cannot insert NULL values */
    if ((self == NULL) || (key == NULL) || (data == NULL))
        return 0;

    if (mAtMapSize(self) == self->count)
        return 0;

    /* Calculate hash value and buffer index */
    _CalculateHash(self, key, &hashValue, &index);

    if (self->bucket[index] == NULL)
        {
        self->bucket[index] = AtEntryCreate(self, key, data, hashValue);
        }
    else
        {
        uint32 count = 0; /* Used to count collision */
        tAtHashMapEntry* prev = NULL;
        tAtHashMapEntry* entry = self->bucket[index];
        uint32 crc = Crc32Get(key);
        while (entry)
            {
            if (crc == entry->crc32) /* same CRC-32, same key */
                return 0;
            prev = entry;
            entry = entry->next;
            count++; /* collision */
            }

        /* Do not insert if max collision is exceeded */
        if ((self->maxCollisionPerHash != 0)
                && (count >= self->maxCollisionPerHash))
            {
            AtPrintc(cSevCritical, "Do not insert Key %s because of  exceeded collision\r\n", (char*)key);
            return 0;
            }
        entry = AtEntryCreate(self, key, data, hashValue);
        prev->next = entry;
        (self->collisionCount)++;
        }

    (self->count)++;
    return 1;
    }

void * AtHashMapFind(tAtHashMap* self, void * key)
    {
    uint32 hashValue = 0;
    uint32 index = 0;
    tAtHashMapEntry* entry = NULL;
    uint32 crc32;
    assert(key);


    crc32 = Crc32Get(key);

    /* Calculate hash value and buffer index */
    _CalculateHash(self, key, &hashValue, &index);
    if ((entry = self->bucket[index]) == NULL)
        return NULL;

    if ((entry->next == NULL) && (crc32 == entry->crc32))/* same CRC-32, same key */
        return entry->data;

    while (entry)
        {
        if (crc32 == entry->crc32) /* same CRC-32, same key */
            break;

        entry = entry->next;
        }

    if (entry == NULL)
        return NULL;
    return entry->data;
    }

tAtHashMap *AtHashMapNew(void)
    {
    tAtHashMap* self  = (tAtHashMap *) AtOsalMemAlloc(sizeof(tAtHashMap));
    assert(self);
    AtOsalMemInit(self, 0, sizeof(tAtHashMap));
    self->size = cHashMapTableSize;
    self->bucket = AtOsalMemAlloc(cHashMapTableSize * sizeof(void*));
    AtOsalMemInit(self->bucket, 0 , cHashMapTableSize * sizeof(void*));
    assert(self->bucket);
    self->count = 0;
    self->collisionCount = 0;
    self->maxCollisionPerHash = 10;
    return self;
    }

void AtHashMapDelete(tAtHashMap *self)
    {
    if (self)
        {
        AtHashMapRemoveAll(self);
        AtOsalMemFree(self->bucket);
        AtOsalMemFree(self);
        self = NULL;
        }
    }

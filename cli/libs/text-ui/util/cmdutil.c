/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies. 
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : 
 *
 * File        : europashellutil.c
 *
 * Created Date: 
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/ 
#include "attypes.h"
#include "AtOsal.h"
#include "atclib.h"
#include "cmdutil.h"

/*--------------------------- Define -----------------------------------------*/ 
#define cCommentChar                    '#'
#define cStrTokenDelim                  " \t"

/*--------------------------- Local Typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/ 

/*--------------------------- Forward declaration ----------------------------*/
 
/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Entries ----------------------------------------*/ 

/*-----------------------------------------------------------------------------
Function Name    :    ToToken

Description        :    Get tokens from a string

Inputs            :    str            - String to get tokens

Outputs            :    argc        - The number of token

Return Code        :    A pointer point to array of token. This array is allocated
                    in this function and must be freed after using.

------------------------------------------------------------------------------*/
char** ToToken(const char *str, int *argc)
	{		
    unsigned int i;
    uint32 wordCount;
    char *retval;
    char *pBuff;
    char **result;
    unsigned int strLen;
    char *saveptr;

    *argc = 0;

    if (str == NULL)
    	return NULL;

    strLen = AtStrlen(str);
    if(strLen == 0)
        return NULL;

    pBuff = (char *)AtOsalMemAlloc(strLen + 1);
    if (pBuff == NULL)
        return NULL;

    for(i = 0; i < strLen; i++)
        {
        if( AtIsSpace(str[i]) )
            {
            pBuff[i] = ' ';
            }
        else
            {
            pBuff[i] = str[i];
            }
        }
    pBuff[i] = '\0';

    wordCount = 0;
    i = 0;
    while (pBuff[i] != '\0')
        {
        if ( !AtIsSpace(pBuff[i]) && (i == 0 || AtIsSpace(pBuff[i - 1])) )
            {
            wordCount++;
            }
        i++;
        }

    if (wordCount == 0)
        {
        *argc = 0;
        AtOsalMemFree(pBuff);
        return NULL;
        }

    /* Build a list of vectors */
    result = (char**)AtOsalMemAlloc((wordCount + 1) * sizeof(char*));
    if (result == NULL)
        {
        AtOsalMemFree(pBuff);
        return NULL;
        }

    i = 1;
    result[0]= AtStrdup(AtStrtok_r(pBuff, cStrTokenDelim, &saveptr));
    while ((retval = AtStrtok_r(NULL, cStrTokenDelim, &saveptr)) != NULL)
        {
        result[i] = AtStrdup(retval);
        i++;
        }
    result[i] = NULL;
    *argc     = (int)wordCount;

    AtOsalMemFree(pBuff);

    return result;
    }

/*-----------------------------------------------------------------------------
Function Name    :    IsEmptyString

Description        :    Check whether string is empty

Inputs            :    str            - String to be checked

Outputs            :    None

Return Code		:	cAtTrue		- If string is empty string
					cAtFalse		- If string is not empty string

------------------------------------------------------------------------------*/

eBool IsEmptyString(const char *str)
    {
    unsigned int i;

    for(i = 0;i < AtStrlen(str); i++)
        {
        if( !AtIsSpace(str[i]) )
            {
            return cAtFalse;
            }
        }
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
Function Name    :    IsComment

Description        :    Check whether string is comment

Inputs            :    str            - String to be checked

Outputs            :    None

Return Code		:	cAtTrue		- If string is comment string
					cAtFalse		- If string is not comment string

------------------------------------------------------------------------------*/

eBool IsComment(const char *str)
    {
    if (str != NULL)
        {
        if ((!AtIsSpace(str[0])) && (str[0] == cCommentChar))
            return cAtTrue;
        }

    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
Function Name    :    IsValidName

Description        :    Check whether string is valid name

Inputs            :    name        - Name to be checked

Outputs            :    None

Return Code		:	cAtTrue		- If name is valid name
					cAtFalse		- If name is not valid name

------------------------------------------------------------------------------*/
eBool IsValidName(const char *name)
    {
    int i;

    for (i = (int)AtStrlen(name) - 1; i >= 0; i--)
        {
        if ((AtIsAlnum(name[i]) == 0) && (name[i] != '_'))
            {
            return cAtFalse;
            }
        }
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
Function Name    :    IsDescriptStart

Description        :    Check whether string is starting of one description

Inputs            :    str            - String to be checked

Outputs            :    None

Return Code		:	cAtTrue		- If string is a starting of description
					cAtFalse		- If string is not a starting of description

------------------------------------------------------------------------------*/

eBool IsDescriptStart(const char *str)
    {
    int i;

    for (i = 0; i < (int)AtStrlen(str) - 1; i++)
        {
        if ( !AtIsSpace(str[i]) )
            {
            if ((str[i] == '/') && (str[i+1] == '*'))
                {
                return cAtTrue;
                }
            break;
            }
        }
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
Function Name    :    IsDescriptEnd

Description        :    Check whether string is ending of one description

Inputs            :    str            - String to be checked

Outputs            :    None

Return Code		:	cAtTrue		- If string is a ending of a description
					cAtFalse		- If string is not a ending of a description

------------------------------------------------------------------------------*/

eBool IsDescriptEnd(const char *str)
    {
	int i;

    for (i = (int)AtStrlen(str) - 1; i > 0; i--)
        {
        if ( !AtIsSpace(str[i]) )
            {
            if ((str[i] == '/') && (str[i - 1] == '*'))
                {
                return cAtTrue;
                }
            break;
            }
        }

    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
Function Name    :    StrTrim

Description        :    Trim space on left and right of the string

Inputs            :    pStr        - String to be trimmed

Outputs            :    None

Return Code        :    Return NULL if the string is a string of space.
                    Otherwise return the pointer point to trimmed string.

------------------------------------------------------------------------------*/

char *StrTrim(char * pStr)
    {
    char *pResult;
    int i;
    int strLen;

    strLen = (int)AtStrlen(pStr);
    pResult = pStr;

    /* Trim left */
    for( i = 0; i < strLen; i++)
        {
        if( !AtIsSpace(*pResult) )
            {
            break;
            }
        pResult ++;
        }

    if( i == strLen)
        {
        pResult = NULL;
        }
    else
        {
        /* Trim right */
        for( i = strLen - 1; i >= 0; i-- )
            {
            if( !AtIsSpace( pStr[i] ) )
                {
                break;
                }
            }
        if( i >= 0 )
            {
            pStr[i + 1] = '\0';
            }
        }

    return pResult;
    }

/*-----------------------------------------------------------------------------
Function Name    :    StrTrimLeft

Description        :    Trim space on left of the string.

Inputs            :    pStr        - String to be trimmed

Outputs            :    None

Return Code        :    Return NULL if the string is a string of space.
                    Otherwise return the pointer point to trimmed string.

------------------------------------------------------------------------------*/

char *StrTrimLeft(char * pStr)
    {
    char *pResult;
    unsigned int i;
    unsigned int strLen;

    strLen = AtStrlen(pStr);
    pResult = pStr;

    /* Trim left */
    for( i = 0; i < strLen; i++)
        {
        if( !AtIsSpace(*pResult) )
            {
            break;
            }
        pResult ++;
        }

    if( i == strLen )
        {
        pResult = NULL;
        }

    return pResult;
    }

/*-----------------------------------------------------------------------------
Function Name    :    StrTrimRight

Description        :    Trim space on right of the string.

Inputs            :    pStr        - String to be trimmed

Outputs            :    None

Return Code        :    Return NULL if the string is a string of space.
                    Otherwise return the pointer point to trimmed string.

------------------------------------------------------------------------------*/

char *StrTrimRight(char * pStr)
    {
    char *pResult;
    int i;
    int strLen;

    strLen = (int)AtStrlen(pStr);
    pResult = pStr;

    /* Trim right */
    for( i = strLen - 1; i >= 0; i-- )
        {
        if( !AtIsSpace( pStr[i] ) )
            {
            break;
            }
        }
    if( i >= 0 )
        {
        pStr[i + 1] = '\0';
        }
    else
        {
        pResult = NULL;
        }

    return pResult;
    }

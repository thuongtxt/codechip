/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Text-UI
 * 
 * File        : cmdutil.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Util
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CMDUTIL_H_
#define _CMDUTIL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
char** ToToken(const char *str, int *argc);
eBool IsEmptyString(const char *str);
eBool IsComment(const char *str);
eBool IsValidName(const char *name);
eBool IsDescriptStart(const char *str);
eBool IsDescriptEnd(const char *str);
char *StrTrim(char * pStr);
char *StrTrimLeft(char * pStr);
char *StrTrimRight(char * pStr);

#ifdef __cplusplus
}
#endif
#endif /* _CMDUTIL_H_ */


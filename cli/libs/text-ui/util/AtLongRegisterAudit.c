/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Register
 *
 * File        : AtLongtRegisterAudit.c
 *
 * Created Date: July-2015
 *
 * Description : N/A
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/
#include <assert.h>
#include "AtOsal.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtLongtRegisterAudit.h"
#include "AtTextUICmd.h"
#include "AtLogger.h"

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 dataBufferShared[cLongRegisterMaxLength];
static char  bufferWrStringShared[128];
static char  bufferRdStringShared[128];
/*--------------------------- Implement --------------------------------------*/
static eBool isBelongToBlackList(struct tAtLongRegisterAudit* self,
                                 uint32 address,
                                 uint8 coreId)
    {
    eBool isBelong = cAtFalse;
    tAtLongRegisterBackList* entry = self->blackList;

    while ((isBelong == cAtFalse) && (entry != NULL))
        {
        isBelong = ((address == entry->address) && (coreId == entry->coreId)) ? cAtTrue : cAtFalse;
        entry = entry->next;
        }
    return isBelong;
    }

static uint32 IndexGet(const struct tAtLongRegisterAudit* self,
                       uint32 address,
                       uint8 coreId)
    {
    AtUnused(self);
    return (uint32) (address % cLongRegisterHashTableSize) + coreId;
    }

static void CoppyValues(uint32 *buffer, const uint32 *values, uint16 length)
	{
	uint16 pos;
	while (length)
		{
		pos = (uint16)(length - 1);
		buffer[pos] = values[pos];
		length = pos;
		}
	}

static tAtLongRegisterEntry* EntryCreate(struct tAtLongRegisterAudit* self,
                                          uint32 address,
                                          const uint32 *values,
                                          uint16 lengh,
                                          uint8  coreId)
    {
    tAtLongRegisterEntry* entry = AtOsalMemAlloc(sizeof(tAtLongRegisterEntry));
    AtUnused(self);
    assert(entry);
    entry->length = lengh;
    entry->address = address;
    entry->coreId = coreId;
    entry->values = AtOsalMemAlloc(cLongRegisterMaxLength * sizeof(uint32));
    assert(entry->values);
    AtOsalMemInit(entry->values, 0, cLongRegisterMaxLength * sizeof(uint32));
    CoppyValues(entry->values, values, lengh);

    /* Default Mask Test for long register */
    entry->masks = AtOsalMemAlloc(cLongRegisterMaxLength * sizeof(uint32));
    assert(entry->masks);
    AtOsalMemInit(entry->masks, 0xFFFFFFFF, cLongRegisterMaxLength * sizeof(uint32));

    entry->next = NULL;
    return entry;
    }

static void BlackListRemoveAll(tAtLongRegisterAudit* self)
    {
    tAtLongRegisterBackList* entry = self->blackList;
    tAtLongRegisterBackList* prev;
    while (entry)
        {
        prev = entry;
        entry = entry->next;
        AtOsalMemFree(prev);
        }
    }

static void AtLongRegisterEntryDelete(tAtLongRegisterEntry* entry)
    {
    AtOsalMemFree(entry->masks);
    AtOsalMemFree(entry->values);
    AtOsalMemFree(entry);
    }

static void RemoveAll(tAtLongRegisterAudit* self)
    {
    tAtLongRegisterEntry* entry = NULL;
    tAtLongRegisterEntry* prev = NULL;
    uint32 i;

    for (i = 0; i < cLongRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];

        while (entry)
            {
            prev  = entry;
            entry = entry->next;
            AtLongRegisterEntryDelete(prev);
            }

        /* Delete bucket */
        self->bucket[i] = NULL;
        }
    self->count = 0;
    self->collisionCount = 0;
    }

static eBool LongValueCompare(tAtLongRegisterEntry* entry,
                              uint32 *data,
                              uint16 length)
    {
    uint16 idx;
    uint16 lengthCompare = length;

    if (length > entry->length)
        lengthCompare = entry->length;

    for (idx = 0; idx < lengthCompare; idx++)
        {
        if ((entry->values[idx] & entry->masks[idx]) != (data[idx] & entry->masks[idx]))
            {
            AtPrintc(cSevCritical,
                     "dword [%d] WR 0x%08x RD 0x%08x \r\n",
                     idx + 1,
                     entry->values[idx],
                     data[idx]);
            return cAtFalse;
            }
        }
    return cAtTrue;
    }

static const char *NameGet(eBool isRead)
    {
    return isRead ? "rd" : "wr";
    }

static void Display(eBool verbose, eBool read, uint32 address, uint32 *data, uint16 length)
    {
	uint32 currentIdex;
	uint16 pos;
	char *pStr = read ? bufferRdStringSharedGet() : bufferWrStringSharedGet();
	AtOsalMemInit(pStr, 0, sizeof (pStr));

	AtStrcat(pStr, NameGet(read));
	currentIdex = AtStrlen(pStr);
	AtSprintf(&pStr[currentIdex], " address 0x%08x", address);
    while(length != 0)
        {
        pos = (uint16)(length - 1);
    	currentIdex = AtStrlen(pStr);
    	AtSprintf(&pStr[currentIdex], " 0x%08x.", data[pos]);
        length = pos;
        }

    if (verbose)
        AtPrintc(cSevInfo, "%s\r\n", pStr);
    }

static tAtLongRegisterBackList* BlackEntryCreate(struct tAtLongRegisterAudit* self,
                                                 uint32 address,
                                                 uint8 coreId)
    {
    tAtLongRegisterBackList* entry = AtOsalMemAlloc(sizeof(tAtLongRegisterBackList));
    assert(entry);
    AtUnused(self);
    entry->coreId = coreId;
    entry->address = address;
    entry->next = NULL;
    return entry;
    }

static eBool WhiteListRemove(tAtLongRegisterAudit *self,
                             uint32 address,
                             uint8 coreId)
    {
    uint32 index = IndexGet(self, address, coreId);
    tAtLongRegisterEntry* entry = self->bucket[index];
    tAtLongRegisterEntry* prev = NULL;
    eBool IsFound = cAtFalse;

    /* Not exist that */
    if (entry == NULL)
        return cAtFalse;

    while (entry)
        {
        if ((entry->address == address) && (entry->coreId == coreId))
            {
            IsFound = cAtTrue;
            break;
            }
        prev  = entry;
        entry = entry->next;
        }

    if (IsFound == cAtFalse)
        {
        if (self->verbose)
            {
            AtPrintc(cSevCritical,
                     "Not Found [address 0x%08x FpgaId %d]\r\r",
                     address,
                     coreId + 1);
            }
        return cAtFalse;
        }

    /* Remove Last */
    if ((entry->next == NULL) && (entry != self->bucket[index]))
        {
        AtLongRegisterEntryDelete(entry);
        (self->count)--;
        prev->next = NULL;
        return cAtTrue;
        }

    /* Remove First */
    if (entry == self->bucket[index])
        {
        self->bucket[index] = (entry->next != NULL) ? entry->next: NULL;
        AtLongRegisterEntryDelete(entry);
        (self->count)--;
        return cAtTrue;
        }

    /* Remove Midle, adjust ref next */
    if ((prev->next) && (entry->next))
        {
        prev->next = entry->next;
        AtLongRegisterEntryDelete(entry);
        (self->count)--;
        return cAtTrue;
        }

    if (self->verbose)
        AtPrintc(cSevCritical, "Wrong handle White_List_Remove \r\r");

    return cAtFalse;
    }

static void BlackListDisplay(tAtLongRegisterBackList *entry)
    {
    AtPrintc(cSevInfo,
             "FpgaId %d address 0x%08x\r\n",
             entry->coreId + 1,
             entry->address);
    }

static void MaskUpdate(tAtLongRegisterEntry* entry, uint32 *masks, uint8 length)
    {
    uint32 dword_i;
    for (dword_i = 0; dword_i < length; dword_i++)
        entry->masks[dword_i] = masks[dword_i];
    }

static void WhiteDisplay(tAtLongRegisterEntry *entry)
    {
    uint32 length = 4;
    AtPrintc(cSevInfo,
             "FpgaId %d address 0x%08x mask::",
             entry->coreId + 1,
             entry->address);
    while(length)
        {
        AtPrintc(cSevInfo, "0x%08x.", entry->masks[length - 1]);
        length = (uint16)(length - 1);
        }
    AtPrintc(cSevInfo, "\r\n");
    }


uint32 AtLongRegisterAuditCollisionCount(const tAtLongRegisterAudit* self)
    {
    return self->collisionCount;
    }

uint32 AtLongRegisterAuditEntriesCount(const tAtLongRegisterAudit* self)
    {
    return self->count;
    }

int AtLongRegisterAuditInsert(tAtLongRegisterAudit* self,
                              uint32 address,
                              const uint32 * values,
                              uint16 length,
                              uint8 coreId)
    {
    uint32 index = 0;

    /* Cannot insert NULL values */
    if (self == NULL)
        return 0;

    if (isBelongToBlackList(self, address, coreId))
        {
        if (self->verbose)
            AtPrintc(cSevCritical,
                     "Not add [address 0x%08x, FpgaId %d], BlackList!!!\r\n",
                     address,
                     coreId + 1);
        return 0;
        }

    /* Should be remove this ???!!!*/
    if (self->count == cLongRegisterHashTableSize)
        {
        if (self->verbose)
            AtPrintc(cSevCritical,
                     "Not add [address 0x%08x, FpgaId %d], Over-Hash-size-table!!!\r\n",
                     address,
                     coreId + 1);
        return 0;
        }

    /* Calculate hash value and buffer index */
    index = IndexGet(self, address, coreId);
    if (self->bucket[index] == NULL)
        {
        self->bucket[index] = EntryCreate(self, address, values, length, coreId);
        }
    else
        {
        uint32 count = 0; /* Used to count collision */
        tAtLongRegisterEntry* prev = NULL;
        tAtLongRegisterEntry* entry = self->bucket[index];
        while (entry)
            {
            /* Update value */
            if ((entry->address == address) && (entry->coreId == coreId))
                {
                CoppyValues(entry->values, values, length);
                entry->length = length;
                return 1;
                }
            prev = entry;
            entry = entry->next;
            count++; /* collision */
            }

        /* Do not insert if max collision is exceeded */
        if ((self->maxCollisionPerHash != 0)  && (count >= self->maxCollisionPerHash))
            {
            AtDriverLog(AtDeviceDriverGet(self->device),
                        cAtLogLevelCritical,
                        "[long_address 0x%08x FpgaId %d] is not added by over collision \r\n",
                        address,
                        coreId + 1);
            return 0;
            }
        entry = EntryCreate(self, address, values, length, coreId);
        prev->next = entry;
        (self->collisionCount)++;
        }

    (self->count)++;
    return 1;
    }

tAtLongRegisterAudit *AtLongRegisterAuditNew(AtDevice device)
    {
    tAtLongRegisterAudit* self  = (tAtLongRegisterAudit *) AtOsalMemAlloc(sizeof(tAtLongRegisterAudit));
    assert(self);
    AtOsalMemInit(self, 0, sizeof(tAtLongRegisterAudit));
    self->bucket = AtOsalMemAlloc(cLongRegisterHashTableSize * sizeof(void*));
    AtOsalMemInit(self->bucket, 0 , cLongRegisterHashTableSize * sizeof(void*));
    assert(self->bucket);
    self->maxCollisionPerHash = 10;
    self->device = device;
    return self;
    }

void AtLongRegisterAuditDelete(tAtLongRegisterAudit *self)
    {
    if (self)
        {
        BlackListRemoveAll(self);
        RemoveAll(self);
        AtOsalMemFree(self->bucket);
        AtOsalMemFree(self);
        self = NULL;
        }
    }

eBool AtLongRegisterAuditPerform(tAtLongRegisterAudit* self)
    {
    tAtLongRegisterEntry* entry = NULL;
    AtDevice device = self->device;
    eBool success = cAtTrue;
    uint32 i;

    for (i = 0; i < cLongRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
            uint16 lengthRead = AtDeviceLongReadOnCore(device,
                                                       entry->address,
                                                       dataBufferShared,
                                                       cLongRegisterMaxLength,
                                                       entry->coreId);
            uint16 lengthDisplay = lengthRead;
            if (lengthDisplay > entry->length)
                {
                lengthDisplay = entry->length;
                AtPrintc(cSevCritical,
                         " address %d lengthRead %d lengthDisplay %d \r\n",
                         entry->address,
                         lengthRead,
                         lengthDisplay);
                }

            if (!LongValueCompare(entry, dataBufferShared, lengthDisplay))
                {
                Display(self->verbose, cAtTrue,  entry->address, dataBufferShared, lengthDisplay);
                Display(self->verbose, cAtFalse, entry->address, entry->values, lengthDisplay);
                AtDriverLog(AtDeviceDriverGet(device),
                            cAtLogLevelCritical,
                            "%s vs %s at FpgaId %d!!\r\n",
							 bufferRdStringSharedGet(),
							 bufferWrStringSharedGet(),
                             entry->coreId + 1);
                success = cAtFalse;
                }
            entry = entry->next;
            }
        }
    return success;
    }

void AtLongRegisterAuditVerboseOn(tAtLongRegisterAudit *self)
    {
    self->verbose = cAtTrue;
    }

void AtLongRegisterAuditVerboseOff(tAtLongRegisterAudit *self)
    {
    self->verbose = cAtFalse;
    }

eBool AtLongRegisterAuditBlackListAdd(tAtLongRegisterAudit *self,
                                     uint32 address,
                                     uint8 coreId)
    {
    eBool IsFound = cAtFalse;
    tAtLongRegisterBackList* entry = self->blackList;

    /* White_List remove before add to black_list */
    WhiteListRemove(self, address, coreId);
    if (self->blackList == NULL)
        self->blackList = BlackEntryCreate(self, address, coreId);
    else
        {
        tAtLongRegisterBackList* prev = NULL;
        while (entry)
            {
            IsFound =((entry->address == address)
                      && (entry->coreId == coreId)) ? cAtTrue : cAtFalse;

            if (IsFound)
                return cAtFalse;
            prev = entry;
            entry = entry->next;
            }
        prev->next = BlackEntryCreate(self, address, coreId);
        }
    return cAtTrue;
    }

eBool AtLongRegisterAuditBlackListRemove(tAtLongRegisterAudit *self,
                                         uint32 address,
                                         uint8 coreId)
    {
    tAtLongRegisterBackList* entry = self->blackList;
    tAtLongRegisterBackList* prev  = NULL;
    eBool IsFound = cAtFalse;
    while (entry != NULL)
        {
        IsFound =((entry->address == address)
                  && (entry->coreId == coreId)) ? cAtTrue : cAtFalse;

        if (IsFound)
            break;

        prev  = entry;
        entry = entry->next;
        }
    if (IsFound == cAtFalse)
        {
        if (self->verbose)
            {
            AtPrintc(cSevCritical,
                     "Not Found [address 0x%08x core %d]\r\r",
                     address,
                     coreId);
            }
        return cAtFalse;
        }

    /* Remove Last */
    if ((entry->next == NULL) && (entry != self->blackList))
        {
        AtOsalMemFree(entry);
        prev->next = NULL;
        return cAtTrue;
        }

    /* Remove First */
    if (entry == self->blackList)
        {
        self->blackList = (entry->next != NULL) ? entry->next: NULL;
        AtOsalMemFree(entry);
        return cAtTrue;
        }

    /* Remove Midle, adjust ref next */
    if ((prev->next) && (entry->next))
        {
        prev->next = entry->next;
        AtOsalMemFree(entry);
        return cAtTrue;
        }

    if (self->verbose)
        AtPrintc(cSevCritical, "Wrong handle Black_List_Remove \r\r");
    return cAtFalse;
    }

void AtLongRegisterAuditBlackListShow(tAtLongRegisterAudit *self)
    {
    tAtLongRegisterBackList* entry = self->blackList;
    while (entry)
        {
        BlackListDisplay(entry);
        entry = entry->next;
        }
    }

eBool AtLongRegisterAuditMaskSet(tAtLongRegisterAudit* self,
                                 uint32 address,
                                 uint8 coreId,
                                 uint32 *masks,
                                 uint8 length)
    {
    tAtLongRegisterEntry* entry = NULL;
    uint32 i;

    for (i = 0; i < cLongRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
            if ((entry->address == address) && (entry->coreId == coreId))
                {
                MaskUpdate(entry, masks, length);
                return cAtTrue;
                }
            entry = entry->next;
            }
        }
    return cAtFalse;
    }

void AtLongRegisterAuditWhiteListShow(tAtLongRegisterAudit* self)
    {
    tAtLongRegisterEntry* entry = NULL;
    uint32 i;
    for (i = 0; i < cLongRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
            WhiteDisplay(entry);
            entry = entry->next;
            }
        }
    }

char* bufferWrStringSharedGet(void)
	{
	return &bufferWrStringShared[0];
	}

char* bufferRdStringSharedGet(void)
	{
	return &bufferRdStringShared[0];
	}

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies The use, copying, transfer or disclosure of such information is
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : 
 *
 * File        : athash.c
 *
 * Created Date: Wed 17, 2013
 *
 * Description : This file contains implementation for some generic hash
 *               algorithms.
 *
 * Notes       : TODO
 *
 *----------------------------------------------------------------------------*/

#include "athash.h"

uint32 AtRsHash(const uint8* pData, uint32 len)
{
    uint32 b    = 378551; /* seed */
    uint32 a    = 63689;  /* seed */
    uint32 hVal = 0;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
    {
        hVal = hVal * a + (*pData);
        a    = a * b;
    }

    return hVal;
}

uint32 AtJsHash(const uint8* pData, uint32 len)
{
    uint32 hVal = 1315423911; /* seed */
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
        hVal ^= ((hVal << 5) + (*pData) + (hVal >> 2));

    return hVal;
}

uint32 AtPjwHash(const uint8* pData, uint32 len)
{
    const uint32 bitsInUint    = (uint32)(sizeof(uint32) * 8);
    const uint32 threeQuarters = (uint32)((bitsInUint  * 3) / 4);
    const uint32 oneEighth     = (uint32)(bitsInUint / 8);
    const uint32 highBits      = (uint32)(0xFFFFFFFF) << (bitsInUint - oneEighth);
    uint32 hVal                = 0;
    uint32 test                = 0;
    uint32 i                   = 0;

    for(i = 0; i < len; pData++, i++)
    {
        hVal = (hVal << oneEighth) + (*pData);

        if((test = hVal & highBits)  != 0)
            hVal = (( hVal ^ (test >> threeQuarters)) & (~highBits));
    }

    return hVal;
}

uint32 AtElfHash(const uint8* pData, uint32 len)
{
    uint32 hVal = 0;
    uint32 x    = 0;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
    {
        hVal = (hVal << 4) + (*pData);

        if((x = hVal & 0xF0000000L) != 0)
            hVal ^= (x >> 24);

        hVal &= ~x;
    }

    return hVal;
}

uint32 AtBkdrHash(const uint8* pData, uint32 len)
{
    uint32 seed = 131; /* 31 131 1313 13131 131313 etc.. */
    uint32 hVal = 0;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
        hVal = (hVal * seed) + (*pData);

    return hVal;
}

uint32 AtSdbmHash(const uint8* pData, uint32 len)
{
    uint32 hVal = 0;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
        hVal = (*pData) + (hVal << 6) + (hVal << 16) - hVal;

    return hVal;
}

uint32 AtDjbHash(const uint8* pData, uint32 len)
{
    uint32 hVal = 5381;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
        hVal = ((hVal << 5) + hVal) + (*pData);

    return hVal;
}

uint32 AtDekHash(const uint8* pData, uint32 len)
{
    uint32 hVal = len;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
        hVal = ((hVal << 5) ^ (hVal >> 27)) ^ (*pData);

    return hVal;
}

uint32 AtBpHash(const uint8* pData, uint32 len)
{
    uint32 hVal = 0;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
        hVal = hVal << 7 ^ (*pData);

    return hVal;
}

uint32 AtFnvHash(const uint8* pData, uint32 len)
{
    const uint32 fnv_prime = 0x811C9DC5;
    uint32 hVal            = 0;
    uint32 i               = 0;

    for(i = 0; i < len; pData++, i++)
    {
        hVal *= fnv_prime;
        hVal ^= (*pData);
    }

    return hVal;
}

uint32 AtApHash(const uint8* pData, uint32 len)
{
    uint32 hVal = 0xAAAAAAAA;
    uint32 i    = 0;

    for(i = 0; i < len; pData++, i++)
    {
        hVal ^= ((i & 1) == 0) ? ((hVal <<  7) ^ (*pData) * (hVal >> 3)) :
                                 (~((hVal << 11) + ((*pData) ^ (hVal >> 5))));
    }

    return hVal;
}

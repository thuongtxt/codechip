/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/
#include <assert.h>
#include "atclib.h"
#include "AtOsal.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "athash.h"
#include "AtShortRegisterAudit.h"
#include "AtTextUICmd.h"
#include "AtLogger.h"

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Implement --------------------------------------*/

static eBool isBelongToBlackList(struct tAtShortRegisterAudit* self,
                                 uint32 address,
                                 uint8 coreId)
    {
    eBool isBelong = cAtFalse;
    tAtShortRegisterBackList* entry = self->blackList;
    while ((isBelong == cAtFalse) && (entry != NULL))
        {
        if ((entry->address == address) && (entry->coreId == coreId))
            isBelong = cAtTrue;

        entry = entry->next;
        }

    return isBelong;
    }

static uint32 IndexGet(const struct tAtShortRegisterAudit* self, uint32 address)
    {
    AtUnused(self);
    return (uint32) (address % cShortRegisterHashTableSize);
    }

static tAtShortRegisterEntry* EntryCreate(struct tAtShortRegisterAudit* self,
                                          uint32 address,
                                          uint32 value,
                                          uint8 coreId)
    {
    tAtShortRegisterEntry* entry =
            AtOsalMemAlloc(sizeof(tAtShortRegisterEntry));
    assert(entry);
    AtUnused(self);
    entry->coreId = coreId;
    entry->address = address;
    entry->value = value;
    entry->mask = 0xFFFFFFFF;
    entry->next = NULL;
    return entry;
    }

static void BlackListRemoveAll(tAtShortRegisterAudit* self)
    {
    tAtShortRegisterBackList* entry = self->blackList;
    tAtShortRegisterBackList* prev;
    while (entry)
        {
        prev = entry;
        entry = entry->next;
        AtOsalMemFree(prev);
        }
    }

static void RemoveAll(tAtShortRegisterAudit* self)
    {
    tAtShortRegisterEntry* entry = NULL;
    tAtShortRegisterEntry* prev = NULL;
    uint32 i;

    for (i = 0; i < cShortRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];

        while (entry)
            {
            prev = entry;
            entry = entry->next;
            AtOsalMemFree(prev);
            }

        /* Delete bucket */
        self->bucket[i] = NULL;
        }
    self->count = 0;
    self->collisionCount = 0;
    }

static tAtShortRegisterBackList* BlackEntryCreate(uint32 address, uint8 coreId)
    {
    tAtShortRegisterBackList* entry = AtOsalMemAlloc(sizeof(tAtShortRegisterBackList));
    assert(entry);
    entry->coreId = coreId;
    entry->address = address;
    entry->next = NULL;
    return entry;
    }

static eBool WhiteListRemove(tAtShortRegisterAudit *self,
                             uint32 address,
                             uint8 coreId)
    {
    uint32 index = IndexGet(self, address);
    tAtShortRegisterEntry* entry = self->bucket[index];
    tAtShortRegisterEntry* prev = NULL;
    eBool IsFound = cAtFalse;

    /* Not exist that */
    if (entry == NULL)
        return cAtFalse;

    while (entry)
        {
        if ((entry->address == address) && (entry->coreId == coreId))
            {
            IsFound = cAtTrue;
            break;
            }
        prev  = entry;
        entry = entry->next;
        }

    if (IsFound == cAtFalse)
        {
        if (self->verbose)
            {
            AtPrintc(cSevCritical,
                     "Not Found [address 0x%08x FpgaId %d]\r\r",
                     address,
                     coreId + 1);
            }
        return cAtFalse;
        }

    /* Remove Last */
    if ((entry->next == NULL) && (entry != self->bucket[index]))
        {
        AtOsalMemFree(entry);
        (self->count)--;
        prev->next = NULL;
        return cAtTrue;
        }

    /* Remove First */
    if (entry == self->bucket[index])
        {
        self->bucket[index] = (entry->next != NULL) ? entry->next: NULL;
        AtOsalMemFree(entry);
        (self->count)--;
        return cAtTrue;
        }

    /* Remove Midle, adjust ref next */
    if ((prev->next) && (entry->next))
        {
        prev->next = entry->next;
        AtOsalMemFree(entry);
        (self->count)--;
        return cAtTrue;
        }

    if (self->verbose)
        AtPrintc(cSevCritical, "Wrong handle White_List_Remove \r\r");

    return cAtFalse;
    }


static void Display(tAtShortRegisterBackList *entry)
    {
    AtPrintc(cSevInfo,
             "FpgaId %d address 0x%08x\r\n",
             entry->coreId + 1,
             entry->address);
    }


static void WhiteListDisplay(tAtShortRegisterEntry *entry)
    {
    AtPrintc(cSevInfo,
             "FpgaId %d address 0x%08x mask 0x%08x\r\n",
             entry->coreId + 1,
             entry->address,
             entry->mask);
    }


uint32 AtShortRegisterAuditCollisionCount(const tAtShortRegisterAudit* self)
    {
    return self->collisionCount;
    }

uint32 AtShortRegisterAuditEntriesCount(const tAtShortRegisterAudit* self)
    {
    return self->count;
    }

int AtShortRegisterAuditCollect(tAtShortRegisterAudit* self, uint32 address, uint32 value, uint8 coreId)
    {
    uint32 index = 0;

    /* Cannot insert NULL values */
    if (self == NULL)
        return 0;

    /* Filter with Black_List */
    if (isBelongToBlackList(self, address, coreId))
        {
        if (self->verbose)
            AtPrintc(cSevCritical,
                     "Not add [address 0x%08x, FpgaId %d], BlackList!!!\r\n",
                     address,
                     coreId + 1);
        return 0;
        }

    /* Should be remove this ???!!!*/
    if (self->count == cShortRegisterHashTableSize)
        {
        if (self->verbose)
            AtPrintc(cSevCritical,
                     "Not add [address 0x%08x, FpgaId %d], Over-Hash-size-table!!!\r\n",
                     address,
                     coreId + 1);
        return 0;
        }

    /* Calculate hash value and buffer index */
    index = IndexGet(self, address);
    if (self->bucket[index] == NULL)
        {
        self->bucket[index] = EntryCreate(self, address, value, coreId);
        }
    else
        {
        uint32 count = 0; /* Used to count collision */
        tAtShortRegisterEntry* prev = NULL;
        tAtShortRegisterEntry* entry = self->bucket[index];
        while (entry)
            {
            /* Update value */
            if (entry->address == address)
                {
                entry->value = (value & entry->mask); /* Insert with mask */
                return 1;
                }
            prev = entry;
            entry = entry->next;
            count++; /* collision */
            }

        /* Do not insert if max collision is exceeded */
        if ((self->maxCollisionPerHash != 0)  && (count >= self->maxCollisionPerHash))
            {
            AtDriverLog(AtDeviceDriverGet(self->device),
                        cAtLogLevelCritical,
                        "short_address 0x%08x FpgaId %d is not added by over collision \r\n",
                        address,
                        coreId + 1);
            return 0;
            }
        entry = EntryCreate(self, address, value, coreId);
        prev->next = entry;
        (self->collisionCount)++;
        }

    (self->count)++;
    return 1;
    }

tAtShortRegisterAudit *AtShortRegisterAuditNew(AtDevice device)
    {
    tAtShortRegisterAudit* self  = (tAtShortRegisterAudit *) AtOsalMemAlloc(sizeof(tAtShortRegisterAudit));
    assert(self);
    AtOsalMemInit(self, 0, sizeof(tAtShortRegisterAudit));
    self->bucket = AtOsalMemAlloc(cShortRegisterHashTableSize * sizeof(void*));
    AtOsalMemInit(self->bucket, 0 , cShortRegisterHashTableSize * sizeof(void*));
    assert(self->bucket);
    self->maxCollisionPerHash = 10;
    self->device = device;
    return self;
    }

void AtShortRegisterAuditDelete(tAtShortRegisterAudit *self)
    {
    if (self)
        {
        BlackListRemoveAll(self);
        RemoveAll(self);
        AtOsalMemFree(self->bucket);
        AtOsalMemFree(self);
        self = NULL;
        }
    }

eBool AtShortRegisteAuditPerform(tAtShortRegisterAudit* self)
    {
    tAtShortRegisterEntry* entry = NULL;
    AtDevice device = self->device;
    eBool success = cAtTrue;
    uint32 i;

    for (i = 0; i < cShortRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
            uint32 address = entry->address;
            AtIpCore core = AtDeviceIpCoreGet(device, (uint8)entry->coreId);
            AtHal hal = AtIpCoreHalGet(core);
            uint32 value = AtHalRead(hal, address);
            uint32 mask  = entry->mask;
            if ((value & mask) != entry->value)
                {
                if (self->verbose == cAtTrue)
                    AtPrintc(cSevInfo, "add 0x%08x rd 0x%08x wr 0x%08x\r\n", address, value, entry->value);
                AtDriverLog(AtDeviceDriverGet(device),
                    cAtLogLevelCritical,
                    "FpgaId %d Address 0x%08x wr 0x%08x but rd 0x%08x mask 0x%08x\r\n",
                    entry->coreId + 1,
                    address,
                    entry->value,
                    value,
                    mask);
                success = cAtFalse;
                }
            entry = entry->next;
            }
        }
    return success;
    }

eBool AtShortRegisterAuditBlackListAdd(tAtShortRegisterAudit *self,
                                       uint32 address,
                                       uint8 coreId)
    {
    tAtShortRegisterBackList* entry = self->blackList;

    /* White_List remove before add to black_list */
    WhiteListRemove(self, address, coreId);

    if (self->blackList == NULL)
        self->blackList = BlackEntryCreate(address, coreId);
    else
        {
        tAtShortRegisterBackList* prev = NULL;
        while (entry)
            {
            if ((entry->address == address) && (entry->coreId == coreId))
                return cAtFalse;
            prev = entry;
            entry = entry->next;
            }
        prev->next = BlackEntryCreate(address, coreId);
        }
    return cAtTrue;
    }

eBool AtShortRegisterAuditBlackListRemove(tAtShortRegisterAudit *self,
                                          uint32 address,
                                          uint8 coreId)
    {
    tAtShortRegisterBackList* entry = self->blackList;
    tAtShortRegisterBackList* prev  = NULL;
    eBool IsFound = cAtFalse;
    while (entry)
        {
        if ((entry->address == address) && (entry->coreId == coreId))
            {
            IsFound = cAtTrue;
            break;
            }
        prev  = entry;
        entry = entry->next;
        }

    if (IsFound == cAtFalse)
        {
        if (self->verbose)
            {
            AtPrintc(cSevCritical,
                     "Not Found [address 0x%08x FpgaId %d]\r\r",
                     address,
                     coreId + 1);
            }
        return cAtFalse;
        }

    /* Remove Last */
    if ((entry->next == NULL) && (entry != self->blackList))
        {
        AtOsalMemFree(entry);
        prev->next = NULL;
        return cAtTrue;
        }

    /* Remove First */
    if (entry == self->blackList)
        {
        self->blackList = (entry->next != NULL) ? entry->next: NULL;
        AtOsalMemFree(entry);
        return cAtTrue;
        }

    /* Remove Midle, adjust ref next */
    if ((prev->next) && (entry->next))
        {
        prev->next = entry->next;
        AtOsalMemFree(entry);
        return cAtTrue;
        }

    if (self->verbose)
        AtPrintc(cSevCritical, "Wrong handle Black_List_Remove \r\r");

    return cAtFalse;
    }

void AtShortRegisterAuditBlackListShow(tAtShortRegisterAudit *self)
    {
    tAtShortRegisterBackList* entry = self->blackList;
    while (entry)
        {
        Display(entry);
        entry = entry->next;
        }
    }

void AtShortRegisterAuditVerboseOn(tAtShortRegisterAudit *self)
    {
    self->verbose = cAtTrue;
    }

void AtShortRegisterAuditVerboseOff(tAtShortRegisterAudit *self)
    {
    self->verbose = cAtFalse;
    }

eBool AtShortRegisterAuditMaskSet(tAtShortRegisterAudit *self,
                                   uint32 address,
                                   uint8 coreId,
                                   uint32 mask)
    {
    tAtShortRegisterEntry* entry = NULL;
    uint32 entry_i;

    for (entry_i = 0; entry_i < cShortRegisterHashTableSize; entry_i++)
        {
        entry = self->bucket[entry_i];
        while (entry)
            {
            if ((entry->address == address) && (entry->coreId == coreId))
                {
                entry->mask = mask;
                entry->value = (entry->value & mask);
                return cAtTrue;
                }
            entry = entry->next;
            }
        }
    return cAtFalse;
    }

void AtShortRegisterAuditWhiteListShow(tAtShortRegisterAudit *self)
    {
    tAtShortRegisterEntry* entry = NULL;
    uint32 i;
    for (i = 0; i < cShortRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
            WhiteListDisplay(entry);
            entry = entry->next;
            }
        }
    }


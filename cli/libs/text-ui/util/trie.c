/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *                 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/
#include "AtOsal.h"
#include "trie.h"

/*--------------------------- Forward declaration ----------------------------*/ 
static void TrieFreeListPush(tAtTrieNode **pList, tAtTrieNode *pNode);
static tAtTrieNode *TrieFreeListPop(tAtTrieNode **pList);
static tAtTrieNode *TrieNodeFind(tAtTrie *pTrie, char *key);
static void TrieInsertRollback(tAtTrie *pTrie, char *key);

/*------------------------------------------------------------------------------
Prototype    : TrieNew(void)

Purpose      : This function is used to create a new trie

Inputs       : None                     

Outputs      : None

Returns      : Pointer to a new trie structure, or NULL if it
               was not possible to allocate memory for the
               new trie
------------------------------------------------------------------------------*/
tAtTrie *AtTrieNew(void)
    {
    tAtTrie *pNewTrie;

    pNewTrie = (tAtTrie *)AtOsalMemAlloc(sizeof(tAtTrie));

    if (pNewTrie == NULL)
        {
        return NULL;
        }

    pNewTrie->rootNode = NULL;

    return pNewTrie;
    }

/*------------------------------------------------------------------------------
Prototype    : void TrieFreeListPush(tTrieNode **list, tTrieNode *pNode)

Purpose      : This function is used to push node to free list

Inputs       : pList             - Pointer to free list
               pNode             - Node to be freed

Outputs      : pList             - Pointer to free list

Returns      : None
------------------------------------------------------------------------------*/
static void TrieFreeListPush(tAtTrieNode **pList, tAtTrieNode *pNode)
    {
    pNode->data = *pList;
    *pList = pNode;
    }

/*------------------------------------------------------------------------------
Prototype    : tTrieNode *TrieFreeListPop(tTrieNode **pList)

Purpose      : This function is used to pop the node out of free list

Inputs       : pList             - Pointer to free list

Outputs      : pList             - Pointer to free list

Returns      : Pointer to first node of free list
------------------------------------------------------------------------------*/
static tAtTrieNode *TrieFreeListPop(tAtTrieNode **pList)
    {
    tAtTrieNode *result;

    result = *pList;
    *pList = result->data;

    return result;
    }

/*------------------------------------------------------------------------------
Prototype    : void TrieFree(tTrie *pTrie)

Purpose      : This function is used to destroy a trie

Inputs       : pTrie            - Pointer to TRIE

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
void AtTrieFree(tAtTrie *pTrie)
    {
    tAtTrieNode *freeList;
    tAtTrieNode *pNode;
    int i;

    if (pTrie == NULL)
        return;

    freeList = NULL;

    /* Start with the root node */
    if (pTrie->rootNode != NULL)
        {
        TrieFreeListPush(&freeList, pTrie->rootNode);
        }

    /*
    Go through the free list, freeing nodes.  We add new nodes as
   we encounter them; in this way, all the nodes are freed
   non-recursively. 
   */
    while (freeList != NULL)
        {
        pNode = TrieFreeListPop(&freeList);

        /* Add all children of this node to the free list */

        for (i = 0; i < cTrieNodeMaxNumChar; ++i)
            {
            if (pNode->pNext[i] != NULL)
                {
                TrieFreeListPush(&freeList, pNode->pNext[i]);
                }
            }

        /* Free the node */
        AtOsalMemFree(pNode);
        }

    /* Free the pTrie */
    AtOsalMemFree(pTrie);
    }

/*------------------------------------------------------------------------------
Prototype    : tTrieNode *TrieNodeFind(tTrie *pTrie, char *key)

Purpose      : This function is used to find the TRIE node in TRIE with the key 

Inputs       : pTrie            - Pointer to TRIE
               key                 - Key string

Outputs      : None

Returns      : Pointer to TRIE node is found
------------------------------------------------------------------------------*/
static tAtTrieNode *TrieNodeFind(tAtTrie *pTrie, char *key)
    {
    tAtTrieNode *pNode;
    char *p;
    int c;

    /* Search down the pTrie until the end of string is reached */
    pNode = pTrie->rootNode;

    for (p=key; *p != '\0'; ++p)
        {

        if (pNode == NULL)
            {
            /* Not found in the tree. Return. */
            return NULL;
            }

        /* Jump to the next node */
        c = *p;
        pNode = pNode->pNext[c];
        }

    /* This key is present if the value at the last node is not NULL */
    return pNode;
    }

/*------------------------------------------------------------------------------
Prototype    : void TrieInsertRollback(tTrie *pTrie, char *key)

Purpose      : This function is used to Roll back an insert operation after 
               a failed malloc() call

Inputs       :pTrie                - Pointer to TRIE
               key                - Key string

Outputs      : None

Returns      : None
------------------------------------------------------------------------------*/
static void TrieInsertRollback(tAtTrie *pTrie, char *key)
    {
    tAtTrieNode *pNode;
    tAtTrieNode **pPrevPtr;
    tAtTrieNode *pNextNode;
    tAtTrieNode **pNextPrevPtr;
    char *p;

    if ((pTrie == NULL) || (key == NULL))
        return;

    /*
    Follow the chain along.  We know that we will never reach the
    end of the string because TrieInsert never got that far.  As a
    result, it is not necessary to check for the end of string
    delimiter (NUL)
    */
    pNode = pTrie->rootNode;
    pPrevPtr = &pTrie->rootNode;
    p = key;

    while (pNode != NULL)
        {
        /* Find the next node now. We might free this node. */
        pNextPrevPtr = &pNode->pNext[(int) *p];
        pNextNode = *pNextPrevPtr;
        ++p;

        /* Decrease the node count and free the node if it
         * reaches zero. */
        --pNode->nodeCount;

        if (pNode->nodeCount == 0)
            {
            AtOsalMemFree(pNode);

            if (pPrevPtr != NULL)
                {
                *pPrevPtr = NULL;
                }

            pNextPrevPtr = NULL;
            }

        /* Update pointers */
        pNode = pNextNode;
        pPrevPtr = pNextPrevPtr;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : int TrieInsert(tTrie *pTrie, char *key, void * value)

Purpose      : This function is used to insert a new key-value pair into a TRIE

Inputs       : pTrie             - Pointer to TRIE
               key                 - The key to access the new value
               pValue             - The value of trie node

Outputs      : None

Returns      : Non-zero if the value was inserted successfully,
               or zero if it was not possible to allocate
               memory for the new entry.
------------------------------------------------------------------------------*/
int AtTrieInsert(tAtTrie *pTrie, char *key, void *pValue)
    {
    tAtTrieNode **pRover;
    tAtTrieNode *pNode;
    char *p;
    int c;

    /* Cannot insert NULL values */

    if ((pValue == NULL) || (key == NULL))
        {
        return 0;
        }

    /* Search to see if this is already in the tree */

    pNode = TrieNodeFind(pTrie, key);

    /* Already in the tree? If so, replace the existing pValue and
     * return success. */

    if (pNode != NULL && pNode->data != NULL)
        {
        pNode->data = pValue;
        return 1;
        }

    /* Search down the pTrie until we reach the end of string,
     * creating nodes as necessary */

    pRover = &(pTrie->rootNode);
    p = key;

    for (;;)
        {
        pNode = *pRover;

        if (pNode == NULL)
            {

            /* Node does not exist, so create it */
            pNode = (tAtTrieNode *) AtOsalMemAlloc(sizeof(tAtTrieNode));

            if (pNode == NULL)
                {
                /* Allocation failed.  Go back and undo
                 * what we have done so far. */
                TrieInsertRollback(pTrie, key);

                return 0;
                }
            AtOsalMemInit(pNode, 0, sizeof(tAtTrieNode));
            pNode->data = NULL;

            /* Link in to the pTrie */
            *pRover = pNode;
            }

        /* Increase the node count */
        ++pNode->nodeCount;

        /* Current character */
        c = *p;

        /* Reached the end of string?  If so, we're finished. */
        if (c == '\0')
            {
            /* Set the data at the node we have reached */
            pNode->data = pValue;

            break;
            }

        /* Advance to the next node in the chain */
        pRover = &pNode->pNext[c];
        ++p;
        }

    return 1;
    }

/*------------------------------------------------------------------------------
Prototype    : int TrieRemove(tTrie *pTrie, char *key)

Purpose      : This function is used to remove an node from a TRIE

Inputs       : pTrie            - Pointer to TRIE
               key                - The key of the entry to remove

Outputs      : None

Returns      : Non-zero if the key was removed successfully,
               or zero if it is not present in the trie.

------------------------------------------------------------------------------*/
int AtTrieRemove(tAtTrie *pTrie, char *key)
    {
    tAtTrieNode *pNode;
    tAtTrieNode *pNext;
    tAtTrieNode **pLastNextPtr;
    char *p;
    int c;

    if ((pTrie == NULL) || (key == NULL))
        return 0;

    /* Find node and remove the value */
    pNode = TrieNodeFind(pTrie, key);

    if (pNode != NULL && pNode->data != NULL)
        {
        pNode->data = NULL;
        }
    else
        {
        return 0;
        }

    /* Now traverse the tree again as before, decrementing the use
     * count of each node.  Free back nodes as necessary. */
    pNode = pTrie->rootNode;
    pLastNextPtr = &pTrie->rootNode;
    p = key;
    for (;;)
        {

        /* Find the next node */
        c = *p;
        pNext = pNode->pNext[c];

        /* Free this node if necessary */
        --pNode->nodeCount;

        if (pNode->nodeCount == 0)
            {
            AtOsalMemFree(pNode);

            /* Set the "next" pointer on the previous node to NULL,
             * to unlink the freed node from the tree.  This only
             * needs to be done once in a remove.  After the first
             * unlink, all further nodes are also going to be
             * free'd. */
            if (pLastNextPtr != NULL)
                {
                *pLastNextPtr = NULL;
                pLastNextPtr = NULL;
                }
            }

        /* Go to the next character or finish */
        if (c == '\0')
            {
            break;
            }
        else
            {
            ++p;
            }

        /* If necessary, save the location of the "next" pointer
         * so that it may be set to NULL on the next iteration if
         * the next node visited is freed. */
        if (pLastNextPtr != NULL)
            {
            pLastNextPtr = &pNode->pNext[c];
            }

        /* Jump to the next node */
        pNode = pNext;
        }

    /* Removed successfully */
    return 1;
    }

/*------------------------------------------------------------------------------
Prototype    : void * TrieLookup(tTrie *pTrie, char *key)

Purpose      : This function is used to look up a value with key in a TRIE

Inputs       : pTrie             - Pointer to TRIE
               key                 - The key of the entry to lookup

Outputs      : None

Returns      : The value associated with the key, or 
               NULL if not found in the trie

------------------------------------------------------------------------------*/
void * AtTrieLookup(tAtTrie *pTrie, char *key)
    {
    tAtTrieNode *pNode;

    if (pTrie == NULL)
        return NULL;

    pNode = TrieNodeFind(pTrie, key);

    if (pNode != NULL)
        {
        return pNode->data;
        }
    else
        {
        return NULL;
        }
    }

/*------------------------------------------------------------------------------
Prototype    : int TrieNumNodeGet(tTrie *pTrie)

Purpose      : This function is used to get the number of node of TRIE

Inputs       : pTrie            - Pointer to TRIE

Outputs      : None

Returns      : Count of the number of entries in the trie

------------------------------------------------------------------------------*/
unsigned int AtTrieNumNodeGet(tAtTrie *pTrie)
    {
    /* To find the number of entries, simply look at the node count
     * of the root node. */

    if (pTrie->rootNode == NULL)
        {
        return 0;
        }
    else
        {
        return pTrie->rootNode->nodeCount;
        }
    }

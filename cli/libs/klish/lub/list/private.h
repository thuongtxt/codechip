#include "lub/list.h"

typedef struct lub_list_node_s {
	lub_list_node_t *prev;
	lub_list_node_t *next;
	void *data;
}lub_list_node_s;

typedef struct lub_list_s {
	lub_list_node_t *head;
	lub_list_node_t *tail;
	lub_list_compare_fn *compareFn;
}lub_list_s;

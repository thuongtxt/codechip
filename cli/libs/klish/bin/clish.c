/* -------------------------------------
 clish.cpp

 A console client for libclish
----------------------------------------*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif
#include <signal.h>
#if HAVE_LOCALE_H
#include <locale.h>
#endif
#if HAVE_LANGINFO_CODESET
#include <langinfo.h>
#endif

#ifdef VXWORKS
#include <ioLib.h>
#include <unistd.h>

#ifndef STDIN_FILENO
#define STDIN_FILENO 0
#endif

#ifndef STDOUT_FILENO
#define STDOUT_FILENO 1
#endif

#ifndef STDERR_FILENO
#define STDERR_FILENO 2
#endif

#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "sys/ioctl.h"

#include "clish/shell.h"
#include "clish/internal.h"
#include "clish/shell/private.h"
#include "clish/command/private.h"
#include "clish/param/private.h"
#include "clish/action/private.h"
#include "clish/config/private.h"
#include "clish/ptype/private.h"
#include "clish/ptype.h"
#include "clish/view.h"

#include "bin/clish.h"

#ifndef VERSION
#define VERSION "1.2.2"
#endif
#define QUOTE(t) #t
/* #define version(v) printf("%s\n", QUOTE(v)) */
#define version(v) printf("%s\n", v)


extern clish_ptype_t ALL_PTYPE[];
extern int ALL_PTYPE_NUM;
extern clish_command_t allsdkcmd_list[];
extern int ALL_SDKCMD_NUM;
extern clish_shell_builtin_t ALLCLISHCMD_LIST[];
extern int ALL_CLISH_BUILTIN_FUNC_NUM; 

/* Hooks */
static clish_shell_hooks_t my_hooks = {
    NULL, /* don't worry about init callback */
    clish_access_callback,
    NULL, /* don't worry about cmd_line callback */
    clish_script_callback,
    NULL, /* don't worry about fini callback */
    clish_config_callback,
    clish_log_callback,
    ALLCLISHCMD_LIST,  /* don't register any builtin functions */
};

static clish_shell_t* m_shell = NULL;
static clish_shell_meta_info_t *m_meta_info = NULL;

static int install_all_autogen_info(clish_shell_t* shell)
{
    clish_command_t *cmd;
    clish_ptype_t *ptype;
    int i;

    /*Go for each PTYPE and build ptype first*/
    for (i = 0; i < ALL_PTYPE_NUM; i++){
        ptype = &ALL_PTYPE[i];
        clish_ptype__set_pattern(ptype, ptype->pattern, ptype->method);
        clish_shell_insert_ptype(shell, ptype); 
    }
    /*Add all command*/
    for (i = 0; i < ALL_SDKCMD_NUM; i++){
        int j = 0;

        cmd = &allsdkcmd_list[i];
        /*Add this to global view*/
        clish_view_insert_command(shell->global_view, cmd);
        j = 0;
        while(cmd->paramv->paramc && cmd->paramv->paramv[j]){
            clish_param_t *param;

            param = cmd->paramv->paramv[j]; 
            param->ptype = clish_shell_find_ptype(shell, param->ptype_name); 
            assert(param->ptype);
            j++;
        }
        cmd->paramv->paramc = j;
    }
    return(0);
}

clish_shell_t *ClishCreate()
    {
    int running;
    clish_shell_t * shell;
    clish_shell_meta_info_t *meta_info;

    /* Command line options */
    const char *socket_path = KONFD_SOCKET_PATH;
    FILE *outfd = stdout;

    my_hooks.cmd_num = ALL_CLISH_BUILTIN_FUNC_NUM;
#ifdef VXWORKS
    socket_path = NULL;
    my_hooks.script_fn = clish_dryrun_callback;
#endif

    meta_info = clish_shell_meta_info_new(&my_hooks, NULL );
    if (!meta_info)
        {
        fprintf(stderr, "Cannot create meta-info .\n");
        return NULL;
        }
    m_meta_info = meta_info;

    shell = clish_shell_new(meta_info, NULL, outfd, BOOL_FALSE);
    if (!shell)
        {
        fprintf(stderr, "Cannot run clish.\n");
        return NULL;
        }

    /* Load the XML files */
    clish_shell_create_cmd(shell);
    install_all_autogen_info(shell);

    /* Set communication to the konfd */
    clish_shell__set_socket(shell, socket_path);

    /* The default is 8-bit if locale is not supported */
    clish_shell__set_utf8(shell, BOOL_FALSE);

    /* Execute startup */
    running = clish_shell_startup(shell);
    if (running)
        {
        fprintf(stderr, "Cannot startup clish.\n");
        clish_shell_delete(shell);
        return NULL;
        }

    /* The interactive shell */
#ifdef VXWORKS
    clish_shell_push_fd(shell, (FILE*)ioGlobalStdGet(STD_IN), BOOL_FALSE);
#else
    clish_shell_push_fd(shell, fdopen(dup(fileno(stdin)), "r"), BOOL_FALSE);
#endif
    m_shell = shell;

    return shell;
    }

int ClishStart()
    {
    return clish_shell_loop(m_shell);
    }

int ClishStop()
    {
    clish_shell_delete(m_shell);
    clish_shell_meta_info_delete(m_meta_info);

    return 0;
    }

int CliShellStdinPop()
    {
    return clish_shell_pop_file(m_shell);
    }

int CliShellStdinPush(int fd)
    {
    return clish_shell_push_fd(m_shell, fdopen(dup(fd), "r"), BOOL_FALSE);
    }

int CliShellStdinPushFile(FILE * fileStream)
    {
    return clish_shell_push_fd(m_shell, fdopen(dup(fileno(fileStream)), "r"), BOOL_FALSE);
    }

void ClishStdIoLock()
    {
    clish_shell_lock(m_shell);
    }

void ClishStdIoUnlock()
    {
    clish_shell_unlock(m_shell);
    }

static void EnterKeyInsert(void)
    {
#if !defined(__CYGWIN__) & !defined(__CYGWIN32__) & !defined(VXWORKS)
    /* Insert character CR (Enter key) to stdout */
    char ch;
    int  fd;
    fd = open(ttyname(STDOUT_FILENO), O_RDWR);
    ch = 0xD;
    ioctl(fd, TIOCSTI, &ch);
    ioctl(fd, TIOCSTI, &ch);
    close(fd);
#endif
    }

static int stdinFd = -1;
static int stdoutFd = -1;
static int stderrFd = -1;

void ClishStdIoRedirect(int stdIoFd)
    {
    if (stdIoFd >= 0)
        {
        EnterKeyInsert();

        ClishStdIoLock();
        /* Save current standard IO */
        if (stdinFd < 0)
            {
            stdinFd = dup(STDIN_FILENO);
            }
        if (stdoutFd < 0)
            {
            stdoutFd = dup(STDOUT_FILENO);
            }
        if (stderrFd < 0)
            {
            stderrFd = dup(STDERR_FILENO);
            }

        /* Redirect */
        dup2(stdIoFd, STDIN_FILENO);
        dup2(stdIoFd, STDOUT_FILENO);
        dup2(stdIoFd, STDERR_FILENO);

        CliShellStdinPop();
        CliShellStdinPush(stdIoFd);

        EnterKeyInsert();

        ClishStdIoUnlock();
        }
    }

void ClishStdIoRestore(void)
    {
    ClishStdIoLock();
    if (stdinFd >= 0)
        {
        dup2(stdinFd, STDIN_FILENO);
        stdinFd = -1;
        }
    if (stdoutFd >= 0)
        {
        dup2(stdoutFd, STDOUT_FILENO);
        stdoutFd = -1;
        }
    if (stderrFd >= 0)
        {
        dup2(stderrFd, STDERR_FILENO);
        stderrFd = -1;
        }

    CliShellStdinPop();
    CliShellStdinPush(STDIN_FILENO);

    EnterKeyInsert();

    ClishStdIoUnlock();
    }

void ClishStdIoFileRedirect(const char *fileName)
    {
    if (fileName != NULL)
        {
        FILE *stdinFile;

        EnterKeyInsert();

        ClishStdIoLock();

        stdinFile = freopen(fileName, "r", stdin);
        freopen(fileName, "w", stdout);
        freopen(fileName, "w", stderr);

        CliShellStdinPop();
        CliShellStdinPush(fileno(stdinFile));

        EnterKeyInsert();

        ClishStdIoUnlock();
        }
    }

void ClishStdIoFileRestore(void)
    {
    ClishStdIoLock();

    freopen("/dev/null", "r", stdin);
    freopen("/dev/null", "w", stdout);
    freopen("/dev/null", "w", stderr);

    CliShellStdinPop();
    CliShellStdinPush(STDIN_FILENO);

    EnterKeyInsert();

    ClishStdIoUnlock();
    }

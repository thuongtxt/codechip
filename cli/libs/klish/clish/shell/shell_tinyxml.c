/*//////////////////////////////////////*/
/* shell_tinyxml_read.cpp*/
/* */
/* This file implements the means to read an XML encoded file and populate the */
/* CLI tree based on the contents.*/
/*//////////////////////////////////////*/
#ifdef __cplusplus
extern "C" {
#endif
#include "private.h"
#include "lub/string.h"
#include "lub/ctype.h"
#ifdef __cplusplus
}
#endif

/*lint +libh(tinyxml/tinyxml.h) Add this to the library file list */
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define OVERVIEW "CONTEXT SENSITIVE HELP\n" \
    "[?] - Display context sensitive help. This is either a list of possible\n" \
    "      command completions with summaries, or the full syntax of the \n" \
    "      current command. A subsequent repeat of this key, when a command\n" \
    "      has been resolved, will display a detailed reference.\n" \
    "\n" \
    "AUTO-COMPLETION\n" \
    "The following keys both perform auto-completion for the current command line.\n" \
    "If the command prefix is not unique then the bell will ring and a subsequent\n" \
    "repeat of the key will display possible completions.\n" \
    "\n" \
    "[enter] - Auto-completes, syntax-checks then executes a command. If there is \n" \
    "          a syntax error then offending part of the command line will be\n" \
    "          highlighted and explained.\n" \
    "\n" \
    "[space] - Auto-completes, or if the command is already resolved inserts a space.\n" \
    "\n"\
    "MOVEMENT KEYS\n" \
    "[CTRL-A] - Move to the start of the line\n" \
    "[CTRL-E] - Move to the end of the line.\n" \
    "[up]     - Move to the previous command line held in history.\n" \
    "[down]   - Move to the next command line held in history.\n" \
    "[left]   - Move the insertion point left one character.\n" \
    "[right]  - Move the insertion point right one character.\n" \
    "\n" \
    "DELETION KEYS\n" \
    "[CTRL-C]    - Delete and abort the current line\n" \
    "[CTRL-D]    - Delete the character to the right on the insertion point.\n" \
    "[CTRL-K]    - Delete all the characters to the right of the insertion point.\n" \
    "[CTRL-U]    - Delete the whole line.\n" \
    "[backspace] - Delete the character to the left of the insertion point.\n" \
    "\n" \
    "ESCAPE SEQUENCES\n" \
    "!!  - Subsitute the the last command line.\n" \
    "!N  - Substitute the Nth command line (absolute as per 'history' command)\n" \
    "!-N - Substitute the command line entered N lines before (relative)\n"
#define STARTUP_CMD_DETAIL  "********************************************\n" \
						    "*         Arrive Technologies              *\n" \
						    "*         Command Line SHELL               *\n" \
						    "*             AT SDK                       *\n" \
						    "*                                          *\n" \
						    "*      WARNING: Authorized Access Only     *\n" \
						    "********************************************\n"

int _cmd_set_builtin_action (clish_command_t *cmd,const char *builtin)
{
	clish_action_t *action = NULL;
	action = clish_command__get_action(cmd);
	assert(action);
	if (builtin)
		clish_action__set_builtin(action, builtin);
	return 0;
}
int _cmd_set_script_action (clish_command_t *cmd,const char *text)
{
	clish_action_t *action = NULL;
	action = clish_command__get_action(cmd);
	assert(action);
	if (text)
		clish_action__set_script(action, text);
	return 0;
}
int _cmd_set_strindex_action (clish_command_t *cmd,const char *strindex)
{
	clish_action_t *action = NULL;
	int index;
	action = clish_command__get_action(cmd);
	assert(action);
	if (strindex)
	{
		index = atoi(strindex);
		clish_action__set_index(action, index);
	}
	return 0;
}
int _cmd_set_shebang_action (clish_command_t *cmd,const char *shebang)
{
	clish_action_t *action = NULL;
	action = clish_command__get_action(cmd);
	assert(action);
	if (shebang)
		clish_action__set_shebang(action, shebang);
	return 0;
}

clish_command_t* _create_cmd (clish_shell_t *shell,const char *name,const char *help,
		const char *view)
{
	clish_view_t *v = NULL;
	clish_command_t *cmd = NULL;
	clish_command_t *old;

	v = clish_shell_find_create_view(shell, view, NULL);

	old = clish_view_find_command(v, name, BOOL_FALSE);
	/* check this command doesn't already exist*/
	if (old) {
		/* flag the duplication then ignore further definition*/
		printf("DUPLICATE COMMAND: %s\n",
		       clish_command__get_name(old));
		return NULL;
	}

	assert(name);
	assert(help);

	/* create a command */
	cmd = clish_view_new_command(v, name, help);
	assert(cmd);
	clish_command__set_pview(cmd, v);

	clish_command__set_lock(cmd, BOOL_TRUE);
	clish_command__set_interrupt(cmd, BOOL_FALSE);
	return cmd;
}
void _create_startup_cmd (clish_shell_t *shell,const char *view,const char *viewid)
{
	clish_command_t *cmd = NULL;
	clish_view_t *v = NULL;
	char *cmdString = NULL;

	assert(!shell->startup_cmd);
	assert(view);

	/* define the view which this command changes to*/
	
	/* create a command with NULL help */
	cmd = clish_view_new_command(shell->global_view, "startup", NULL);
	clish_command__set_lock(cmd, BOOL_FALSE);

	v = clish_shell_find_create_view(shell, view, NULL);
	/* reference the next view*/
	clish_command__set_view(cmd, v);

	/* define the view id which this command changes to*/
	if (viewid)
		clish_command__set_viewid(cmd, viewid);

	cmdString = lub_string_dup(STARTUP_CMD_DETAIL);
	clish_command__set_detail(cmd, cmdString);
	lub_string_free(cmdString);
	
	/* remember this command*/
	shell->startup_cmd = cmd;
}
int clish_shell_create_cmd (clish_shell_t* shell)
{
	const char *view = "root-view";
	const char *viewid = "AT SDK >";
	clish_view_t *v = NULL;
	clish_command_t *cmd = NULL;
	
	if (!shell->global_view)
		shell->global_view = clish_shell_find_create_view(shell,"global","");
	v = clish_shell_find_create_view(shell, view, viewid);
	clish_view__set_depth(v, 1);
	_create_startup_cmd (shell,view,NULL);
	
	{
		clish_ptype_method_e method;
		clish_ptype_preprocess_e preprocess;
		clish_ptype_t *ptype;

		const char *method_name = NULL;
		const char *preprocess_name = NULL;

		method = clish_ptype_method_resolve(method_name);
		preprocess = clish_ptype_preprocess_resolve(preprocess_name);
		ptype = clish_shell_find_create_ptype(shell,
				"VLAN_ID", "Number in the range 1-4095",
				"(409[0-5]|40[0-8][0-9]|[1-3][0-9]{3}|[1-9][0-9]{2}|[1-9][0-9]|[1-9])",
				method, preprocess);
		assert(ptype);
		ptype = clish_shell_find_create_ptype(shell,
				"IP_ADDR","IP address AAA.BBB.CCC.DDD where each part is in the range 0-255",
				"(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))",
					method, preprocess);
		assert(ptype);
		ptype = clish_shell_find_create_ptype(shell,
				"UINT","Unsigned integer",
				"[0-9]+",
					method, preprocess);
		assert(ptype);
		ptype = clish_shell_find_create_ptype(shell,
				"STRING","String",
				"[^\\*]+",
					method, preprocess);
		assert(ptype);
		ptype = clish_shell_find_create_ptype(shell,
				"BYTE_HEX","A Byte in Hex format",
				"(0[xX]([0-9a-fA-F]{1,2}))",
					method, preprocess);
		assert(ptype);
		method = clish_ptype_method_resolve ("select");
		ptype = clish_shell_find_create_ptype(shell,
				"BOOL","Boolean choice",
				"true(1), false(0)",
					method, preprocess);
		assert(ptype);
		method = clish_ptype_method_resolve ("pathname");
		ptype = clish_shell_find_create_ptype(shell,
				"PATHNAME_STRING","Path name",
				".+",
					method, preprocess);
		assert(ptype);
	}

	cmd = _create_cmd (shell,"exit", "Exit this CLI session",view);
	_cmd_set_builtin_action (cmd,"clish_close");
	cmd = _create_cmd (shell,"help", "Help message",view);
	_cmd_set_builtin_action (cmd,"clish_overview");
	cmd = _create_cmd (shell,"ls","List the files in the current directory",view);
	_cmd_set_script_action (cmd,"ls -l -a");
	cmd = _create_cmd (shell,"ps","Give details of current processes",view);
	_cmd_set_script_action (cmd,"ps");
	cmd = _create_cmd (shell,"pwd","Show current working folder",view);
	_cmd_set_script_action (cmd,"pwd");

	shell->overview = lub_string_dup(OVERVIEW);

/*	{
		clish_nspace_t *nspace = NULL;
		nspace = clish_nspace_new(v);
		assert(nspace);
		clish_view_insert_nspace(v, nspace);

		clish_nspace__set_help(nspace, BOOL_FALSE);
		clish_nspace__set_completion(nspace, BOOL_FALSE);
		clish_nspace__set_context_help(nspace, BOOL_FALSE);
		clish_nspace__set_inherit(nspace, BOOL_FALSE);

	}*/
	
	return 0;
}


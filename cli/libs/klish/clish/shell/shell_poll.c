/*
 * =====================================================================================
 *
 *       Filename:  shell_poll.c
 *
 *    Description:  Exec shell in poll mode
 *
 *        Version:  1.0
 *        Created:  01/12/2012 11:42:25 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#include "private.h"
#include "lub/string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#ifndef VXWORKS
#include <pthread.h>
#endif
#include <dirent.h>
#include <errno.h>

extern void tty_set_raw_mode(tinyrl_t * this);
/*-------------------------------------------------------- */
char *tinyrl_poll_line(tinyrl_t * this, const char *prompt, void *context, bool_t *cont);

char* clish_shell_helper_get_prompt(clish_shell_t *this)
{
	char *prompt = NULL;
	const clish_view_t *view;
	clish_context_t context;

	/* Set up the context for tinyrl */
	context.cmd = NULL;
	context.pargv = NULL;
    context.full_command = NULL;
	context.shell = this;

	/* Obtain the prompt */
	view = clish_shell__get_view(this);
	assert(view);
	prompt = clish_shell_expand(clish_view__get_prompt(view), SHELL_VAR_ACTION, &context);
    return(prompt);
}

static void clish_shell_display_prompt(tinyrl_t *this, const char *prompt) 
{
    tinyrl_printf(this, "\r%s", prompt); 
    fflush(NULL);
}

int clish_shell_poll_line(clish_shell_t *this)
{
	char *prompt = NULL;
	const clish_view_t *view;
	char *str;
	clish_context_t context;
	tinyrl_history_t *history;
	int lerror = 0;
    int res = 0;
    bool_t cont_poll = BOOL_FALSE;

	assert(this);
	this->state = SHELL_STATE_OK;
	if (!tinyrl__get_istream(this->tinyrl)) {
		this->state = SHELL_STATE_SYSTEM_ERROR;
		return -1;
	}

	/* Set up the context for tinyrl */
	context.cmd = NULL;
	context.pargv = NULL;
    context.full_command = NULL;
	context.shell = this;

	/* Obtain the prompt */
	view = clish_shell__get_view(this);
	assert(view);
	prompt = clish_shell_expand(clish_view__get_prompt(view), SHELL_VAR_ACTION, &context);
	assert(prompt);

	/* Push the specified line or interactive line */
    str = tinyrl_poll_line(this->tinyrl, prompt, &context, &cont_poll);
	lerror = errno;
    if (BOOL_TRUE == cont_poll) {
        res = -1;
        goto out;
    }
    if (!str) {
        switch (lerror) {
            case ENOENT:
                this->state = SHELL_STATE_EOF;
                break;
            case EBADMSG:
                this->state = SHELL_STATE_SYNTAX_ERROR;
                break;
            default:
                this->state = SHELL_STATE_SYSTEM_ERROR;
                break;
        };
        return -1;
    }
    if (!strlen(str) && this->state == SHELL_STATE_OK) {
        clish_shell_display_prompt(this->tinyrl, prompt);
    }

	/* Deal with the history list */
	if (tinyrl__get_isatty(this->tinyrl)) {
		history = tinyrl__get_history(this->tinyrl);
		tinyrl_history_add(history, str);
	}
	/* Let the client know the command line has been entered */
	if (this->client_hooks->cmd_line_fn)
		this->client_hooks->cmd_line_fn(&context, str);
	free(str);

	/* Execute the provided command */
	if (context.cmd && context.pargv) {
		if ((res = clish_shell_execute(&context, NULL))) {
			this->state = SHELL_STATE_SCRIPT_ERROR;
        } else {
            clish_shell_display_prompt(this->tinyrl, prompt);
        }
	}
	if (context.pargv)
		clish_pargv_delete(context.pargv);
    if (context.full_command) {
        lub_string_free(context.full_command);
    }
out:
    lub_string_free(prompt);
	return res;
}

/*-------------------------------------------------------- */
extern void tinyrl_set_poll(tinyrl_t * this);
int clish_shell_poll(clish_shell_t *this)
{
    int running = 0;
    int retval = SHELL_STATE_OK;

    if (this && (SHELL_STATE_CLOSING == this->state))
        return retval;

    retval = SHELL_STATE_OK;
    /* Get input from the stream */
    tinyrl_set_poll(this->tinyrl);
    running = clish_shell_poll_line(this);
    if (running) {
        switch (this->state) {
            case SHELL_STATE_SCRIPT_ERROR:
            case SHELL_STATE_SYNTAX_ERROR:
                /* Interactive session doesn't exit on error */
                if (tinyrl__get_isatty(this->tinyrl) ||
                        (this->current_file &&
                         !this->current_file->stop_on_error))
                    running = 0;
                retval = this->state;
            default:
                break;
        }
    }
    if (SHELL_STATE_CLOSING == this->state)
        running = clish_shell_pop_file(this);
    return retval;
}


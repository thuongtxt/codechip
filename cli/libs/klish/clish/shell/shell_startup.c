/*
 * shell_startup.c
 */
#include "private.h"
#include <assert.h>

#include "lub/string.h"

/*----------------------------------------------------------- */
int clish_shell_startup(clish_shell_t *this)
{
	void clish_shell_renew_prompt(tinyrl_t *this);
	const char *banner;
	clish_context_t context;
	int res = 0;

	assert(this->startup_cmd);
	banner = clish_command__get_detail(this->startup_cmd);
	if (banner)
		tinyrl_printf(this->tinyrl, "%s\n", banner);

	context.shell = this;
	context.cmd = this->startup_cmd;
	context.pargv = NULL;
    context.full_command = NULL;
	/* Call log initialize */
	if (clish_shell__get_log(this) && this->client_hooks->log_fn)
		this->client_hooks->log_fn(&context, NULL, 0);
	/* Call startup script */
	res = clish_shell_execute(&context, NULL);
    if (context.full_command) {
        lub_string_free(context.full_command);
    }
	return res;
}

/*----------------------------------------------------------- */
void clish_shell__set_startup_view(clish_shell_t * this, const char * viewname)
{
	clish_view_t *view;

	assert(this);
	assert(this->startup_cmd);
	/* Search for the view */
	view = clish_shell_find_create_view(this, viewname, NULL);
	clish_command__force_view(this->startup_cmd, view);
}

/*----------------------------------------------------------- */
void clish_shell__set_startup_viewid(clish_shell_t * this, const char * viewid)
{
	assert(this);
	assert(this->startup_cmd);
	clish_command__force_viewid(this->startup_cmd, viewid);
}

/*----------------------------------------------------------- */
void clish_shell__set_default_shebang(clish_shell_t * this, const char * shebang)
{
	assert(this);
	lub_string_free(this->meta_shebang);
	this->meta_shebang = lub_string_dup(shebang);
}

/*----------------------------------------------------------- */
const char * clish_shell__get_default_shebang(const clish_shell_t * this)
{
	assert(this);
	return this->meta_shebang;
}

/*----------------------------------------------------------- */

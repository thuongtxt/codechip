/*
 * shell_help.c
 */
#include "private.h"
#include "clish/types.h"
#include "lub/string.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/*--------------------------------------------------------- */
/*
 * Provide a detailed list of the possible command completions
 */
static void available_commands(clish_shell_t *this,
	clish_help_t *help, const char *line, size_t *max_width)
{
#define MAX_COMMAND_ARGS_STRING 512
	const clish_command_t *cmd;
	clish_shell_iterator_t iter;
    char buf[MAX_COMMAND_ARGS_STRING];

	if (max_width)
		*max_width = 0;
	/* Search for COMMAND completions */
	clish_shell_iterator_init(&iter, CLISH_NSPACE_HELP);
	while ((cmd = clish_shell_find_next_completion(this, line, &iter))) {
		size_t width;
		const char *name = clish_command__get_suffix(cmd);
        char *s = buf;
		if (max_width) {
			width = strlen(name);
			if (width > *max_width)
				*max_width = width;
		}
		lub_argv_add(help->name, name);
        lub_argv_add(help->help, clish_command__get_text(cmd));
        if (clish_paramv__get_count(clish_command__get_paramv(cmd))) { 
            clish_paramv__get_all_param_name(clish_command__get_paramv(cmd), s, sizeof(buf)); 
            lub_argv_add(help->syntax, buf);
        } else {
            /*Just push an empty string for notice that this is an command*/
            lub_argv_add(help->syntax, "");
        } 
        lub_argv_add(help->detail, clish_command__get_detail(cmd));
	}
}

/*--------------------------------------------------------- */
static int available_params(clish_shell_t *this,
	clish_help_t *help, const clish_command_t *cmd,
	const char *line, size_t *max_width)
{
	unsigned index = lub_argv_wordcount(line);
	unsigned idx = lub_argv_wordcount(clish_command__get_name(cmd));
	lub_argv_t *argv;
	clish_pargv_t *completion, *pargv;
	unsigned i;
	unsigned cnt = 0;
	clish_pargv_status_t status = CLISH_LINE_OK;
	clish_context_t context;

	/* Empty line */
	if (0 == index)
		return -1;

	if (line[strlen(line) - 1] != ' ')
		index--;

	argv = lub_argv_new(line, 0);

	/* get the parameter definition */
	completion = clish_pargv_new();
	pargv = clish_pargv_new();
	context.shell = this;
	context.cmd = cmd;
	context.pargv = pargv;
	status = clish_shell_parse_pargv(pargv, cmd, &context,
		clish_command__get_paramv(cmd),
		argv, &idx, completion, index);
	clish_pargv_delete(pargv);
	cnt = clish_pargv__get_count(completion);

	/* Calculate the longest name */
	for (i = 0; i < cnt; i++) {
        /*QuanCao: move calculated len to function clish_param_help.*/
		unsigned clen = 0;
		const clish_param_t *param;
		param = clish_pargv__get_param(completion, i);
		clen = clish_param_help(param, help);
		if (max_width && (clen > *max_width))
			*max_width = clen;
	}
	clish_pargv_delete(completion);
	lub_argv_delete(argv);

	/* It's a completed command */
	if (CLISH_LINE_OK == status)
		return 0;

	/* Incompleted command */
	return -1;
}

/*--------------------------------------------------------- */
void clish_shell_help(clish_shell_t *this, const char *line)
{
	clish_help_t help;
	size_t max_width = 0;
	const clish_command_t *cmd;
	int i;

	help.name = lub_argv_new(NULL, 0);
	help.help = lub_argv_new(NULL, 0);
    help.syntax = lub_argv_new(NULL, 0);
	help.detail = lub_argv_new(NULL, 0);

	/* Get COMMAND completions */
	available_commands(this, &help, line, &max_width);

	/* Resolve a command */
	cmd = clish_shell_resolve_command(this, line);
	/* Search for PARAM completion */
	if (cmd) {
		size_t width = 0;
		int status;
		status = available_params(this, &help, cmd, line, &width);
		if (width > max_width)
			max_width = width;
		/* Add <cr> if command is completed */
		if (!status) {
			lub_argv_add(help.name, "<cr>");
            lub_argv_add(help.syntax, NULL);
			lub_argv_add(help.help, NULL);
			lub_argv_add(help.detail, NULL);
		}
	}
	if (lub_argv__get_count(help.name) == 0)
		goto end;

	/* Print help messages */
    /*
     * NOTE: help.name: in case of help for one param, this is the help of PTYPE
     *       help.help: this is the help of PARAM
     *       However, if we call ? help for one command
     *       help.name: is the name of command
     *       help.help: is the help of command
     *       We want to swap help.name <-> help.help in case of PTYPE has method=select 
     */
    for (i = 0; i < lub_argv__get_count(help.name); i++) {
        /*ATVN patch: print syntax first then help string*/
        if (lub_argv__get_arg(help.syntax, i)){
            if (strlen(lub_argv__get_arg(help.syntax, i))){
                /*This will print bold format for command and syntax string*/
                /*Bold code is: \033[1m*/
                tinyrl_printf(this->tinyrl, "\n\033[1m%-*s    %s\033[1;0m\n",
                        (int)max_width,
                        lub_argv__get_arg(help.name, i),
                        lub_argv__get_arg(help.syntax, i));
                tinyrl_printf(this->tinyrl, "%s\n", lub_argv__get_arg(help.help, i) ?  lub_argv__get_arg(help.help, i) : "");
            }
            else {
                tinyrl_printf(this->tinyrl, "\n\033[1m%-*s    \033[1;0m%s\n",
                        (int)max_width,
                        lub_argv__get_arg(help.name, i),
                        lub_argv__get_arg(help.help, i) ?  lub_argv__get_arg(help.help, i) : "");
            }
        }
        else {
            tinyrl_printf(this->tinyrl, "%-*s    %s\n",
                    (int)max_width,
                    lub_argv__get_arg(help.name, i),
                    lub_argv__get_arg(help.help, i) ?
                    lub_argv__get_arg(help.help, i) : "");
        }
    }

	/* Print details */
	if ((lub_argv__get_count(help.name) == 1) &&
		(SHELL_STATE_HELPING == this->state)) {
		const char *detail = lub_argv__get_arg(help.detail, 0);
		if (detail)
			tinyrl_printf(this->tinyrl, "%s\n", detail);
	}

	/* update the state */
	if (this->state == SHELL_STATE_HELPING)
		this->state = SHELL_STATE_OK;
	else
		this->state = SHELL_STATE_HELPING;

end:
	lub_argv_delete(help.name);
	lub_argv_delete(help.syntax);
	lub_argv_delete(help.help);
	lub_argv_delete(help.detail);
}

/*--------------------------------------------------------- */

#ifndef VXWORKS
#include <termios.h>
#endif
#include <semaphore.h>

#include "tinyrl/tinyrl.h"
#include "tinyrl/vt100.h"

#define MAX_HS_MATCH_STRING 512 /* This is enough for one line length */
#define MAX_HS_PROMPT       128 /* This is enough for one line length */
typedef struct _hsearch_ctx {
	unsigned begin_idx; /* store beginning searching index */
#define HSEARCH_STATE_NONE       0L /*  No search */
#define HSEARCH_STATE_FORWARD    1L /* Forward search CTRL-R */
#define HSEARCH_STATE_BACKWARD  -1L /*  Backward search CTRL-S */
	int state;
    char *old_prompt;
    unsigned match_len; /*store the len of string */
	unsigned offset;
    tinyrl_history_entry_t *match_entry; /* Match entry */
    char match_str[MAX_HS_MATCH_STRING+1]; /* store the latest match string */
    char hs_prompt[MAX_HS_PROMPT+1];
} hsearch_ctx_t;

/* define the class member data and virtual methods */
struct _tinyrl {
	const char *line;
	unsigned max_line_length;
	char *prompt;
	size_t prompt_size; /* strlen() */
	size_t prompt_len; /* Symbol positions */
	char *buffer;
	size_t buffer_size;
	bool_t done;
	bool_t completion_over;
	bool_t completion_error_over;
	unsigned point;
	unsigned end;
	tinyrl_completion_func_t *attempted_completion_function;
	tinyrl_timeout_fn_t *timeout_fn; /* timeout callback */
	tinyrl_keypress_fn_t *keypress_fn; /* keypress callback */
	int state;
#define RL_STATE_COMPLETING (0x00000001)
	char *kill_string;
#define NUM_HANDLERS 256
	tinyrl_key_func_t *handlers[NUM_HANDLERS];

	tinyrl_history_t *history;
	tinyrl_history_iterator_t hist_iter;
	tinyrl_vt100_t *term;
	void *context;		/* context supplied by caller
				 * to tinyrl_readline()
				 */
	char echo_char;
	bool_t echo_enabled;
#ifndef VXWORKS
	struct termios default_termios;
#else
	int default_termios;
#endif
	bool_t isatty;
	char *last_buffer;	/* hold record of the previous
				buffer for redisplay purposes */
	unsigned int last_point; /* hold record of the previous
				cursor position for redisplay purposes */
	unsigned int last_width; /* Last terminal width. For resize */
	bool_t utf8;		/* Is the encoding UTF-8 */
    /* ATVN patch */
	bool_t dirname_completion; /* Completion for directory name */
    char *poll_str;
	bool_t poll_mode; /* This is poll tnyrl */
    int last_state;
	hsearch_ctx_t hs_ctx;
	sem_t semaphore;
};

bool_t tinyrl_extend_line_buffer(tinyrl_t * this, unsigned len);

bool_t tinyrl_key_reverse_history(tinyrl_t *instance, int key);
bool_t tinyrl_key_forward_history(tinyrl_t * parent, int key);
void tinyrl_hsearch_cleanup(tinyrl_t *instance);
bool_t hsearch_keypress(tinyrl_t *this, int key);
void tinyrl_internal_position(tinyrl_t *this, int prompt_len,
	int line_len, unsigned int width);




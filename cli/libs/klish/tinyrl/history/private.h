/* private.h */
#include "tinyrl/history.h"

struct _tinyrl_history {
	tinyrl_history_entry_t **entries;	/* pointer entries */
	unsigned length;	/* Number of elements within this array */
	unsigned size;		/* Number of slots allocated in this array */
	unsigned current_index;
	unsigned stifle;  /* Maximum number of this history */
};

/**************************************
 * protected interface to tinyrl_history_entry class
 ************************************** */
extern tinyrl_history_entry_t *tinyrl_history_entry_new(const char *line,
							unsigned index);

extern void tinyrl_history_entry_delete(tinyrl_history_entry_t * instance);

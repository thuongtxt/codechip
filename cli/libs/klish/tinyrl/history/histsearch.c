/*
 * =====================================================================================
 *
 *       Filename:  histsearch.c
 *
 *    Description:  Searching the history
 *
 *        Version:  1.0
 *        Created:  11/29/2012 02:51:27 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#include "tinyrl/history.h"
#include "tinyrl/tinyrl.h"
#include "../private.h"
#include "private.h"
#include "lub/string.h"

#include <string.h>

const char *FORWARD_SEARCH_PROMPT = "(forward-i-search)`"; 
const char *REVERSE_SEARCH_PROMPT = "(reverse-i-search)`"; 

static void internal_redisplay(tinyrl_t *parent)
{
    /* Display prompt */
    tinyrl_vt100_printf(parent->term, "\r%s%s", parent->prompt, parent->line);
    /* Move cursor */
    tinyrl_vt100_cursor_back(parent->term, strlen(parent->line) - parent->point);
    /* Update the display */
    (void)tinyrl_vt100_oflush(parent->term);
    parent->last_point = parent->point;
}

static void tinyrl_hsearch_replace_line(tinyrl_t * parent, const char *text, unsigned new_point)
{
    hsearch_ctx_t *this = &parent->hs_ctx; 
	/*  size_t new_len = strlen(text); */

    parent->line = text;
    parent->point = new_point;
    parent->prompt = this->hs_prompt;
    switch(this->state) {
        default:
            parent->prompt_len = parent->prompt_size = snprintf(this->hs_prompt, MAX_HS_PROMPT, "%s%s': ", REVERSE_SEARCH_PROMPT, this->match_str);
            break;
        case HSEARCH_STATE_FORWARD:
            parent->prompt_len = parent->prompt_size = snprintf(this->hs_prompt, MAX_HS_PROMPT, "%s%s': ", FORWARD_SEARCH_PROMPT, this->match_str);
            break;
    }
    internal_redisplay(parent);
}
/**
 * @brief This function will search all history from current index of history.
 *
 * @param parent
 */
bool_t tinyrl_history_search(tinyrl_t *parent, const char *sstr, unsigned position)
{
    /* We only support backward/reverse search now. The forward searching will be implemented later */
    hsearch_ctx_t *this = &parent->hs_ctx; 
    bool_t result = BOOL_FALSE;
	int i;
    unsigned new_point = this->match_len;
    int len = strlen(sstr);
	tinyrl_history_entry_t *entry = NULL;
    const char *buffer = "";

    switch(this->state) {
        case HSEARCH_STATE_BACKWARD:
            if (len) {
                /*  Do reverse search here */
                for (i = this->offset; i >= 0; i--) {
                    entry = parent->history->entries[i];
                    if (position >= tinyrl_history_entry__get_index(entry)) {
                        if (!strncmp(sstr, tinyrl_history_entry__get_line(entry), len)) {
                            this->offset = i;
                            break;
                        }
                    }
                    entry = NULL;
                }
            }
            break;
        case HSEARCH_STATE_FORWARD:
            if (len) {
                /*  Do reverse search here */
                for (i = this->offset; i < parent->history->length; i++) {
                    entry = parent->history->entries[i];
                    if (position <= tinyrl_history_entry__get_index(entry)) {
                        if (!strncmp(sstr, tinyrl_history_entry__get_line(entry), len)) {
                            this->offset = i;
                            break;
                        }
                    }
                    entry = NULL;
                }
            }
            break;
        default:
            return(BOOL_FALSE);
    }
    if (entry) {
        buffer = tinyrl_history_entry__get_line(entry);
        this->match_len = len;
        new_point = this->match_len;
        if (sstr != this->match_str)
            strcpy(this->match_str, sstr);
        this->match_entry = entry;
        result = BOOL_TRUE;
    } else if (!len) {
        result = BOOL_TRUE;
        this->match_len = 0;
        new_point = 0;
    }
    else {
        if (this->match_entry) {
            buffer = tinyrl_history_entry__get_line(this->match_entry);
        }
        this->match_str[this->match_len] = '\0';
    }
    /* Update history */
    tinyrl_hsearch_replace_line(parent, buffer, new_point);
    return result;
}

static bool_t tinyrl_hs_key_backspace(tinyrl_t *parent, int key)
{
    hsearch_ctx_t *this = &parent->hs_ctx; 

    if (this->match_len > 0) {
        this->match_str[this->match_len - 1] = '\0';
        switch(this->state) {
            case HSEARCH_STATE_FORWARD:
                return(tinyrl_history_search(parent, this->match_str, this->match_entry ? tinyrl_history_entry__get_index(this->match_entry) : this->begin_idx));
                break;
            default:
                return(tinyrl_history_search(parent, this->match_str, this->begin_idx));
        }
    } else {
        return BOOL_FALSE;
    }
}

static void tinyrl_hs_key_crlf(tinyrl_t * parent, int key)
{
	tinyrl_crlf(parent);
    tinyrl_hsearch_cleanup(parent);
	parent->done = BOOL_TRUE;
    /*  Clish shell hook to parent function so we must call it here so that it parse the command */
    parent->handlers[KEY_CR](parent, key); 
    parent->point = 0;
}

static bool_t hs_key_default(tinyrl_t *parent, int key)
{
    hsearch_ctx_t *this = &parent->hs_ctx; 

    if (key > 31) {
        char tmp[2];

        tmp[0] = (key & 0xFF), tmp[1] = '\0';
        this->match_str[this->match_len] = tmp[0];
        this->match_str[this->match_len + 1] = '\0';
        if (BOOL_TRUE != tinyrl_history_search(parent, this->match_str, this->begin_idx)) {
            this->match_str[this->match_len] = '\0';
            return BOOL_FALSE;
        } 
    } else {
        tinyrl_hsearch_cleanup(parent);
    }
    return BOOL_TRUE;
}

static bool_t hs_tinyrl_key_escape(tinyrl_t * parent, int key)
{
	bool_t result = BOOL_FALSE;

    switch (tinyrl_vt100_escape_decode(parent->term)) {
        default:
            tinyrl_hsearch_cleanup(parent);
            break;
        case tinyrl_vt100_DELETE:
            result = tinyrl_hs_key_backspace(parent,key);
            break;
    }
    return result;
}

bool_t hsearch_keypress(tinyrl_t *parent, int key)
{
    switch(key) {
        case KEY_ESC:
            return(hs_tinyrl_key_escape(parent, key));
            break;
        case KEY_DEL:
        case KEY_BS:
            return(tinyrl_hs_key_backspace(parent, key));
            break;
        case KEY_CR:
        case KEY_LF:
            tinyrl_hs_key_crlf(parent, key);
            break;
        case KEY_DC2:
            return(tinyrl_key_reverse_history(parent, key));
            break;
        case KEY_DC3:
            return(tinyrl_key_forward_history(parent, key));
            break;
        default: /* Fall through */
            return(hs_key_default(parent, key));
    }
	return BOOL_TRUE;
}

static bool_t internal_hsearch(tinyrl_t *parent, int state)
{
    hsearch_ctx_t *this = &parent->hs_ctx; 
    bool_t result = BOOL_FALSE;

    if (HSEARCH_STATE_NONE == this->state) {
        /* tinyrl_printf(parent, "You are pressing CTRL-R, now prossing it \n"); */
        this->begin_idx = parent->history->current_index;
        this->offset = parent->history->length - 1;
        /* Change Prompt */
        this->old_prompt = parent->prompt;
        /* Search prompt */
        if (parent->line && parent->point) {
            strncpy(this->match_str, parent->line, parent->point);
            this->match_str[parent->point] = '\0';
        }
    } 
    this->state = state;
    if (this->match_entry)
        result = tinyrl_history_search(parent, this->match_str, tinyrl_history_entry__get_index(this->match_entry) + state);
    else 
        result = tinyrl_history_search(parent, this->match_str, this->begin_idx);
    return result;
}

bool_t tinyrl_key_reverse_history(tinyrl_t *parent, int key)
{
    return internal_hsearch(parent, HSEARCH_STATE_BACKWARD);
}

bool_t tinyrl_key_forward_history(tinyrl_t * parent, int key)
{
    return internal_hsearch(parent, HSEARCH_STATE_FORWARD);
}

void tinyrl_hsearch_cleanup(tinyrl_t *parent)
{
    hsearch_ctx_t *this = &parent->hs_ctx; 
    if (HSEARCH_STATE_NONE == this->state ) 
        return;

    this->state = HSEARCH_STATE_NONE;
    this->begin_idx = 0;
    parent->prompt = this->old_prompt;
    parent->prompt_len = parent->prompt_size = strlen(parent->prompt);
    this->old_prompt = NULL;
    /* Display prompt */
    if (this->match_entry) {
        tinyrl_replace_line(parent, tinyrl_history_entry__get_line(this->match_entry), 1);
    } else {
        tinyrl_replace_line(parent, "", 1);
    }
    parent->point = this->match_len;
    internal_redisplay(parent);
    this->match_str[0] = '\0';
    this->match_len = 0;
    this->match_entry = NULL;
}


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : exprcalc
 *
 * File        : exprcalc.h
 *
 * Created Date: 31-Mar-2007
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

 
/*--------------------------- Description -------------------------------------
 * This module is used for parsing and calculating expression.
 * Ex: (12 * 3 + 2) / 5
 *----------------------------------------------------------------------------*/

#ifndef _EXPR_CALC_HEADER_
#define _EXPR_CALC_HEADER_


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eParseRetCode
	{
	cExprOk,
	cExprLackOfLeftPar,
	cExprLackOfRightPar,
	cExprLackOfOperand,
	cExprWrong
	} eParseRetCode;

/*--------------------------- Entries ----------------------------------------*/

eParseRetCode ExprCalc(const char* pExprString, long int *pVal);


#endif /* _EXPR_CALC_HEADER_ */


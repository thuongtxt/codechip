/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : stack
 *
 * File        : stack.h
 *
 * Created Date: 30-Mar-2007
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

 
/*--------------------------- Description -------------------------------------
 *
 *
 *----------------------------------------------------------------------------*/

#ifndef _STACK_HEADER_
#define _STACK_HEADER_


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tStackItem
	{
	void*				pData;
	struct tStackItem	*pNext;
	} tStackItem;

typedef struct tStack
	{
	int			count;
	tStackItem	*pTop;
	} tStack;

/*--------------------------- Entries ----------------------------------------*/

void StackInit(tStack *pStack, uint32 itemSize);
void StackClear(tStack *pStack);
eBool StackTop(tStack *pStack, void *pData);
eBool StackPop(tStack *pStack);
eBool StackPush(tStack *pStack, void *pData);

#endif /* _STACK_HEADER_ */


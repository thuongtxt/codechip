/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : queue
 *
 * File        : queue.h
 *
 * Created Date: 30-Mar-2007
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

 
/*--------------------------- Description -------------------------------------
 *
 *
 *----------------------------------------------------------------------------*/

#ifndef _QUEUE_HEADER_
#define _QUEUE_HEADER_


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tQueueItem
	{
	void*				pData;
	struct tQueueItem	*pNext;
	} tQueueItem;

typedef struct tQueue
	{
	int			count;
	tQueueItem	*pHead;
	tQueueItem	*pTail;
	} tQueue;

/*--------------------------- Entries ----------------------------------------*/

void QueueInit(tQueue *pQueue, uint32 itemSize);
void QueueClear(tQueue *pQueue);
eBool QueueRetrieve(tQueue *pQueue, void *pData);
eBool QueueServe(tQueue *pQueue);
eBool QueueServeRetrieve(tQueue *pQueue, void *pData);
eBool QueueAppend(tQueue *pQueue, void *pData);

#endif /* _QUEUE_HEADER_ */


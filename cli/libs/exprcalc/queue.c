/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies. 
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : queue
 *
 * File        : queue.c
 *
 * Created Date: 30-Mar-2007
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include 	<stdio.h>
#include 	<stdlib.h>
#include	<string.h>
#include	<ctype.h>

#include	"attypes.h"
#include    "AtOsal.h"
#include	"queue.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32	dataSizeOfItem = 4; /* byte */


/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Macros -----------------------------------------*/ 

/*-----------------------------------------------------------------------------
Function Name	:	QueueInit

Description		:	Initialize queue

Inputs			:	pQueue		- Pointer to queue.

Outputs			:	None

Return Code		:	None

------------------------------------------------------------------------------*/

void QueueInit(tQueue *pQueue, uint32 itemSize)
	{
	pQueue->count = 0;
	pQueue->pHead = NULL;
	pQueue->pTail = NULL;

	dataSizeOfItem = itemSize;
	}

/*-----------------------------------------------------------------------------
Function Name	:	QueueFree

Description		:	Free queue

Inputs			:	pQueue		- Pointer to queue.

Outputs			:	None

Return Code		:	None

------------------------------------------------------------------------------*/

void QueueClear(tQueue *pQueue)
	{
	tQueueItem *pItem;
	while( pQueue->pHead != NULL && pQueue->count > 0 )
		{
		pItem = pQueue->pHead;
		pQueue->pHead = pQueue->pHead->pNext;
		AtOsalMemFree(pItem->pData);
		AtOsalMemFree(pItem);
		pQueue->count--;
		}
	pQueue->pHead = NULL;
	pQueue->pTail = NULL;
	}

/*-----------------------------------------------------------------------------
Function Name	:	QueueRetrieve

Description		:	Get head item of queue

Inputs			:	pQueue		- Pointer to queue

Outputs			:	pData		- Data of head item

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool QueueRetrieve(tQueue *pQueue, void *pData)
	{
	eBool ret = cAtTrue;
	if( pQueue->count > 0 )
		{
		AtOsalMemCpy(pQueue->pHead->pData, pData, dataSizeOfItem);
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	QueueServe

Description		:	Remove head item of queue

Inputs			:	pQueue		- Pointer to queue

Outputs			:	None

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool QueueServe(tQueue *pQueue)
	{
	eBool ret = cAtTrue;
	if( pQueue->count > 0 )
		{
		tQueueItem *pItem;

		pItem = pQueue->pHead;
		pQueue->pHead = pQueue->pHead->pNext;
		AtOsalMemFree(pItem->pData);
		AtOsalMemFree(pItem);

		pQueue->count--;

		if( pQueue->count == 0 )
			{
			pQueue->pTail = NULL;
			}
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	QueueAppend

Description		:	Push item to end of queue

Inputs			:	pQueue		- Pointer to queue
					data		- Data of item

Outputs			:	None

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool QueueAppend(tQueue *pQueue, void *pData)
	{
	eBool ret;
	tQueueItem *pItem = (tQueueItem *)AtOsalMemAlloc(sizeof(tQueueItem));

	ret = cAtTrue;
	if( pItem != NULL )
		{
		pItem->pData = (void*)AtOsalMemAlloc(dataSizeOfItem);
		if( pItem->pData != NULL )
			{
			AtOsalMemCpy(pData, pItem->pData, dataSizeOfItem);
			pItem->pNext = NULL;
			if( pQueue->count != 0 )
				{
				pQueue->pTail->pNext = pItem;
				pQueue->pTail = pQueue->pTail->pNext;
				}
			else
				{
				pQueue->pHead = pItem;
				pQueue->pTail = pItem;
				}
			
			pQueue->count++;
			}
		else
			{
			ret = cAtFalse;
			}
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	QueueServeRetrieve

Description		:	Get and remove head item of queue

Inputs			:	pQueue		- Pointer to queue

Outputs			:	pData		- Data of head item

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool QueueServeRetrieve(tQueue *pQueue, void *pData)
	{
	eBool ret = cAtTrue;
	if( pQueue->count > 0 )
		{
		tQueueItem *pItem;

		pItem = pQueue->pHead;
		pQueue->pHead = pQueue->pHead->pNext;
		AtOsalMemCpy(pItem->pData, pData, dataSizeOfItem);
		AtOsalMemFree(pItem->pData);
		AtOsalMemFree(pItem);

		pQueue->count--;

		if( pQueue->count == 0 )
			{
			pQueue->pTail = NULL;
			}
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

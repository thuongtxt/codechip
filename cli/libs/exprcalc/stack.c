/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies. 
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : stack
 *
 * File        : stack.c
 *
 * Created Date: 30-Mar-2007
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include 	<stdio.h>
#include 	<stdlib.h>
#include	<string.h>
#include	<ctype.h>

#include	"attypes.h"
#include    "AtOsal.h"
#include	"stack.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32	dataSizeOfItem = 4; /* byte */

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Macros -----------------------------------------*/ 

/*-----------------------------------------------------------------------------
Function Name	:	StackInit

Description		:	Initialize stack

Inputs			:	pStack		- Pointer to stack
					itemSize	- The size of data in an item

Outputs			:	None

Return Code		:	None

------------------------------------------------------------------------------*/

void StackInit(tStack *pStack, uint32 itemSize)
	{
	pStack->count = 0;
	pStack->pTop = NULL;

	dataSizeOfItem = itemSize;
	}

/*-----------------------------------------------------------------------------
Function Name	:	StackClear

Description		:	Clear all item in stack

Inputs			:	pStack		- Pointer to stack.

Outputs			:	None

Return Code		:	None

------------------------------------------------------------------------------*/

void StackClear(tStack *pStack)
	{
	tStackItem *pItem;
	while( pStack->pTop != NULL && pStack->count > 0 )
		{
		pItem = pStack->pTop;
		pStack->pTop = pItem->pNext;
		AtOsalMemFree(pItem->pData);
		AtOsalMemFree(pItem);
		pStack->count--;
		}
	}

/*-----------------------------------------------------------------------------
Function Name	:	StackTop

Description		:	Get top item of stack

Inputs			:	pStack		- Pointer to stack

Outputs			:	pData		- Data of top item

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool StackTop(tStack *pStack, void *pData)
	{
	eBool ret = cAtTrue;
	if( pStack->count > 0 )
		{
		AtOsalMemCpy(pStack->pTop->pData, pData, dataSizeOfItem);
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	StackPop

Description		:	Remove top item of stack

Inputs			:	pStack		- Pointer to stack

Outputs			:	None

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool StackPop(tStack *pStack)
	{
	eBool ret = cAtTrue;
	if( pStack->count > 0 )
		{
		tStackItem *pItem;

		pItem = pStack->pTop;
		pStack->pTop = pItem->pNext;
		AtOsalMemFree(pItem->pData);
		AtOsalMemFree(pItem);

		pStack->count--;
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	StackPush

Description		:	Push item to stack

Inputs			:	pStack		- Pointer to stack
					data		- Data of item

Outputs			:	None

Return Code		:	True		- If success
					False		- If fail

------------------------------------------------------------------------------*/

eBool StackPush(tStack *pStack, void *pData)
	{
	eBool ret;
	tStackItem *pItem = (tStackItem *)AtOsalMemAlloc(sizeof(tStackItem));

	ret = cAtTrue;
	if( pItem != NULL )
		{
		pItem->pData = (void*)AtOsalMemAlloc(dataSizeOfItem);
		if( pItem->pData != NULL )
			{
			AtOsalMemCpy(pData, pItem->pData, dataSizeOfItem);
			pItem->pNext = pStack->pTop;
			pStack->pTop = pItem;
			pStack->count++;
			}
		else
			{
			ret = cAtFalse;
			}
		}
	else
		{
		ret = cAtFalse;
		}

	return ret;
	}

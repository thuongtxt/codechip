/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2006 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies. 
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : exprcalc
 *
 * File        : exprcalc.c
 *
 * Created Date: 31-Mar-2007
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include 	<stdio.h>
#include 	<stdlib.h>
#include	<string.h>
#include	<ctype.h>
#include	<signal.h>
#include	<unistd.h>

#include	"attypes.h"
#include  "AtOsal.h"
#include	"stack.h"
#include	"queue.h"
#include	"exprcalc.h"

/*--------------------------- Define -----------------------------------------*/
#define		cOpDelimStr		"+-*/() \n\t"
#define		cOperandLen		32

#define		mMin(a,b)		(a > b ? b : a)

/*--------------------------- Local Typedefs ---------------------------------*/
typedef enum eOperator
	{
	cNonOp, cPlus, cMinus, cMul, cDiv, 
	cNegative, cPositive, cLeftPar, cRightPar, cBlank
	} eOperator;

typedef struct tExecutionItem
	{
	char		str[cOperandLen + 1];
	int			itemOp;
	} tExecutionItem;

/*--------------------------- Global variables -------------------------------*/

tStack operatorStack;
tQueue executionQueue;

tStack calcStack;

/*--------------------------- Local variables --------------------------------*/

static eBool isInitialized = cAtFalse;
static eBool isCalcStackInit = cAtFalse;

/*--------------------------- Forward declaration ----------------------------*/
eParseRetCode	ExpressionParse(const char* pExprString);
eOperator		OperatorGet(const char* pStr, eBool isFirstOp, int *pPos);
int				OpPriorityGet(eOperator op);
eParseRetCode	OperatorAdd(eOperator op);
tExecutionItem	OperandGet(const char* pStr, int *pPos);

/*--------------------------- Macros -----------------------------------------*/ 

/*-----------------------------------------------------------------------------
Function Name	:	ExpressionParse

Description		:	Parse expression string

Inputs			:	pExprString		- The string expression

Outputs			:	None

Return Code		:	cExprOk					- Parse Ok
					cExprLackOfLeftPar		- Expression lack of left parenthese
					cExprLackOfRightPar		- Expression lack of right parenthese

------------------------------------------------------------------------------*/

eParseRetCode ExpressionParse(const char* pExprString)
	{
	int pos;
	eBool isFirstOp;
	eOperator opCurrent;
	eParseRetCode retCode;

	pos = 0;
	isFirstOp = cAtTrue;
	retCode = cExprOk;

	if( !isInitialized )
		{
		StackInit(&operatorStack, sizeof(int));
		QueueInit(&executionQueue, sizeof(tExecutionItem));
		isInitialized = cAtTrue;
		}
	else
		{
		StackClear(&operatorStack);
		QueueClear(&executionQueue);
		}

	while( pos < (int)strlen(pExprString) )
		{
		tExecutionItem execItem;

		if( (opCurrent = OperatorGet(pExprString, isFirstOp, &pos)) != cNonOp )
			{
			if( opCurrent != cBlank )
				{
				retCode = OperatorAdd(opCurrent);
				if( retCode != cExprOk )
					{
					return retCode;
					}

				if(opCurrent == cRightPar)
					{
					isFirstOp = cAtFalse;
					}
				else
					{
					isFirstOp = cAtTrue;
					}
				}
			continue;
			}
		AtOsalMemInit(&execItem, 0, sizeof(tExecutionItem));
		execItem = OperandGet(pExprString, &pos);
		QueueAppend(&executionQueue, &execItem);
		isFirstOp = cAtFalse;
		}

	while( operatorStack.count > 0 )
		{
		tExecutionItem execItem;
		StackTop(&operatorStack, &execItem.itemOp);

		if( execItem.itemOp != cLeftPar )
			{
			QueueAppend(&executionQueue, &execItem);
			StackPop(&operatorStack);
			}
		else
			{
			break;
			}
		}

	if( operatorStack.count > 0 ) /* Lack of right parenthese */
		{
		retCode = cExprLackOfRightPar;
		}

	return retCode;
	}

/*-----------------------------------------------------------------------------
Function Name	:	OperatorGet

Description		:	Get operator in expression

Inputs			:	pStr			- The string expression
					isFirstOp		- Set cAtTrue if this is a first operator in 
									  expression or in pair of parenthese ()

Outputs			:	pPos			- Next position of operator or operand

Return Code		:	A operator
------------------------------------------------------------------------------*/

eOperator OperatorGet(const char* pStr, eBool isFirstOp, int *pPos)
	{
	eOperator opCurrent;

	opCurrent = cNonOp;

	switch( pStr[*pPos] )
		{
		case ' ':
		case '\n':
		case '\t':
			opCurrent = cBlank;
			(*pPos)++;

			break;
		case '+': 
			if(isFirstOp)
				{
				opCurrent = cPositive;
				}
			else
				{
				opCurrent = cPlus;
				}
			(*pPos)++;

			break;
		case '-': 
			if(isFirstOp)
				{
				opCurrent = cNegative;
				}
			else
				{
				opCurrent = cMinus;
				}
			(*pPos)++;

			break;
		case '*': 
			opCurrent = cMul;
			(*pPos)++;
			break;
		case '/': 
			opCurrent = cDiv;
			(*pPos)++;

			break;
		case '(':
			opCurrent = cLeftPar;
			(*pPos)++;

			break;
		case ')':
			opCurrent = cRightPar;
			(*pPos)++;

			break;
		default:
		    break;
		}

	return opCurrent;
	}

/*-----------------------------------------------------------------------------
Function Name	:	OpPriorityGet

Description		:	Get priority of operator

Inputs			:	op				- Operator constant

Outputs			:	None

Return Code		:	The priority of operator
------------------------------------------------------------------------------*/

int OpPriorityGet(eOperator op)
	{
	int ret;
	switch(op)
		{
		case cLeftPar:
			ret = 0;
			break;
		case cRightPar:
			ret = 1;
			break;
		case cPlus:
		case cMinus:
			ret = 2;
			break;
		case cMul:
		case cDiv:
			ret = 3;
			break;
		case cNegative:
		case cPositive:
			ret = 4;
			break;
		case cNonOp:
		case cBlank:
		default :
			ret = 4;
			break;
		}
	return ret;
	}

/*-----------------------------------------------------------------------------
Function Name	:	OperatorAdd

Description		:	Add operator to stack

Inputs			:	op				- Operator constant

Outputs			:	None

Return Code		:	Error code when adding operator
------------------------------------------------------------------------------*/

eParseRetCode OperatorAdd(eOperator op)
	{
	eParseRetCode retCode;

	retCode = cExprOk;

	if( operatorStack.count == 0 )
		{
		StackPush(&operatorStack, &op);
		return retCode;
		}

	if(op == cLeftPar)
		{
		StackPush(&operatorStack, &op);
		return retCode;
		}

	if(op == cRightPar)
		{
		while( operatorStack.count > 0 )
			{
			int preOp;

			StackTop(&operatorStack, &preOp);
			StackPop(&operatorStack); 

			if( preOp != cLeftPar )
				{
				tExecutionItem execItem;
				execItem.itemOp = preOp;
				QueueAppend(&executionQueue, &execItem);
				}
			else
				{
				return retCode;
				}
			}
		
		if( operatorStack.count == 0 ) /* Lack of left parenthese */
			{
			retCode = cExprLackOfLeftPar;
			return retCode;
			}

		}

	while( operatorStack.count > 0 )
		{
		int preOp;
		StackTop(&operatorStack, &preOp);

		if (OpPriorityGet(op) <= OpPriorityGet((eOperator)preOp))
			{
			tExecutionItem execItem;
			execItem.itemOp = preOp;
			QueueAppend(&executionQueue, &execItem);
			StackPop(&operatorStack);
			}
		else
			{
			break;
			}
		}
	
	StackPush(&operatorStack, &op);

	return retCode;
	}

/*-----------------------------------------------------------------------------
Function Name	:	OperandGet

Description		:	Get operand in expression string from position pPos

Inputs			:	pStr				- Expression string

Outputs			:	pPos				- Position of next operator or operand

Return Code		:	Execution item
------------------------------------------------------------------------------*/

tExecutionItem OperandGet(const char* pStr, int *pPos)
	{
	tExecutionItem execItem;
	const char *pOperandStr;
	const char *pDelim;
	int i;

	AtOsalMemInit((void*)&execItem, 0, sizeof(tExecutionItem));
	pOperandStr = (pStr + (*pPos));

	pDelim = strpbrk(pOperandStr, cOpDelimStr);
	if( pDelim == NULL )
		{
		pDelim = (pStr + strlen(pStr));
		}

	for( i = 0; i < (mMin((pDelim - pOperandStr), cOperandLen)); i++)
		{
		execItem.str[i] = pOperandStr[i];
		}

	(*pPos) = (*pPos) + (pDelim - pOperandStr);

	execItem.itemOp = cBlank;

	return execItem;
	}

/*-----------------------------------------------------------------------------
Function Name	:	ExprCalc

Description		:	Calculate value of expression

Inputs			:	pExprString			- Expression string

Outputs			:	pVal				- Value of the expression

Return Code		:	Parsing error code
------------------------------------------------------------------------------*/

eParseRetCode ExprCalc(const char* pExprString, long int *pVal)
	{
	long int result;
	long int val;
	long int tmp;
	tExecutionItem execItem;
	eParseRetCode retCode;

	result = 0;
	retCode = cExprOk;
	*pVal = 0;

	if( !isCalcStackInit )
		{
		StackInit(&calcStack, sizeof(long int));
		isCalcStackInit = cAtTrue;
		}
	else
		{
		StackClear(&calcStack);
		}

	retCode = ExpressionParse(pExprString);

	if( retCode != cExprOk )
		{
		return retCode;
		}

	while( executionQueue.count > 0 )
		{
		QueueServeRetrieve(&executionQueue, &execItem);

		switch(execItem.itemOp)
			{
			case cPositive:
			case cNegative:
			case cNonOp:
				if( calcStack.count < 1 )
					{
					return cExprLackOfOperand;
					}
				break;
			default:
				if( execItem.itemOp != cBlank && calcStack.count < 2 )
					{
					return cExprLackOfOperand;
					}
				break;
			}

		switch(execItem.itemOp)
			{
			case cBlank:
				/* Convert hex to value */
				if( strlen(execItem.str) > 1 && (execItem.str[0] == '0') && 
					(execItem.str[1] == 'x' || execItem.str[1] == 'X') )
					{
					sscanf(execItem.str, "%lx", (long unsigned int *)&val);
					}
				else /* Convert to decimal value */
					{
					val = strtol(execItem.str, (char**)NULL, 10);
					}
				StackPush(&calcStack, &val);
				break;
			case cPositive:
				break;
			case cNegative:
				StackTop(&calcStack, &val);
				StackPop(&calcStack);
				val = 0 - val;
				StackPush(&calcStack, &val);
				break;
			case cPlus:
				StackTop(&calcStack, &tmp);
				StackPop(&calcStack);
				StackTop(&calcStack, &val);
				StackPop(&calcStack);
				val = val + tmp;
				StackPush(&calcStack, &val);
				break;
			case cMinus:
				StackTop(&calcStack, &tmp);
				StackPop(&calcStack);
				StackTop(&calcStack, &val);
				StackPop(&calcStack);
				val = val - tmp;
				StackPush(&calcStack, &val);
				break;
			case cMul:
				StackTop(&calcStack, &tmp);
				StackPop(&calcStack);
				StackTop(&calcStack, &val);
				StackPop(&calcStack);
				val = val * tmp;
				StackPush(&calcStack, &val);
				break;
			case cDiv:
				StackTop(&calcStack, &tmp);
				StackPop(&calcStack);
				StackTop(&calcStack, &val);
				StackPop(&calcStack);
				val = val / tmp;
				StackPush(&calcStack, &val);
				break;
			default:
			    break;
			}
		}

	if( calcStack.count == 1 )
		{
		StackTop(&calcStack, &result);
		StackPop(&calcStack);
		*pVal = result;
		}
	else
		{
		retCode = cExprWrong;
		}


	return retCode;
	}

/* For selft test */
#ifdef _EXPR_SELF_TEST_

static void ExprCalcShutdown(int sigNum)
	{
	kill(getpid(), SIGTERM);
	}

int main(void)
	{
	char *cmdBuf;
	long int value;
	eParseRetCode retCode;


	signal(SIGINT, ExprCalcShutdown);

	system("clear");
	while(1)
		{
		cmdBuf = ReadLine("Expr = ");
		if( strcmp(cmdBuf, "exit") == 0 )
			{
			break;
			}

		retCode = ExprCalc(cmdBuf, &value);
		if( retCode == cExprLackOfLeftPar )
			{
			printf("Expression lack of left parenthese\n");
			}
		else if( retCode == cExprLackOfRightPar )
			{
			printf("Expression lack of right parenthese\n");
			}
		else if( retCode == cExprLackOfOperand )
			{
			printf("Expression leftover operator\n");
			}
		else if( retCode == cExprWrong )
			{
			printf("Expression wrong\n");
			}
		else if( retCode == cExprOk )
			{
			printf("\nResult = %ld\n\n", value);
			}

		AddHistory(cmdBuf);
		AtOsalMemFree(cmdBuf);
		}

	return 0;
	}

#endif /* _EXPR_SELF_TEST_ */

EXPR_CALC_TARGET = $(EXPR_CALC_DIR)/exprcalclib.o
EXPR_CALC_OBJS   = $(patsubst %.c, %.o, $(wildcard $(EXPR_CALC_DIR)/*.c))
EXPR_CALC_FLAGS  = $(AT_CFLAGS) $(OSAL_FLAGS) -I$(EXPR_CALC_DIR)/include $(GLB_INC) 

.PHONY: all clean cleanexprcalc

all: $(EXPR_CALC_TARGET)

$(EXPR_CALC_TARGET) : CFLAGS := $(EXPR_CALC_FLAGS) $(MAKE_DEP_FLAG)
$(EXPR_CALC_TARGET) : $(EXPR_CALC_OBJS)
	$(LD) $(LDFLAGS) $@ $^

cleanexprcalc:
	rm -f $(EXPR_CALC_OBJS) $(EXPR_CALC_TARGET) $(EXPR_CALC_OBJS:.o=.d)
	
clean cleanapplib: cleanexprcalc

-include $(EXPR_CALC_OBJS:.o=.d)

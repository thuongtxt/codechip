/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtPython.h
 * 
 * Created Date: Dec 6, 2015
 *
 * Description : Work with Python
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPYTHON_H_
#define _ATPYTHON_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtPythonInit(void);
int AtPythonScriptRun(char *scriptPath, int argc, char **argv);

#ifdef __cplusplus
}
#endif
#endif /* _ATPYTHON_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtPython.c
 *
 * Created Date: Dec 6, 2015
 *
 * Description : Work with Python
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdlib.h>
#include "atclib.h"
#include "AtPythonInternal.h"
#include "AtCliModule.h"
#include "AtCli.h"
#include "AtTextUI.h"
#include "AtPythonHal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void initpyexpat(void);
extern void init_collections(void);
extern void initoperator(void);
extern void inititertools(void);
extern void init_socket(void);
extern void init_functools(void);
extern void initdatetime(void);
extern void inittime(void);
extern void initcStringIO(void);
extern void init_struct(void);
extern void initbinascii(void);
extern void initselect(void);
extern void initmath(void);
extern void init_random(void);
extern void init_md5(void);
extern void init_sha256(void);
extern void init_sha512(void);
extern void init_sha(void);
extern void initarray(void);
extern void init_io(void);
extern void initfcntl(void);
extern void initresource(void);
extern void inittermios(void);
extern char *CliAtTextUICapturedOutput(void);

/*--------------------------- Implementation ---------------------------------*/
static PyObject *CliRun(PyObject *self, PyObject* args)
    {
    char *cli;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "s", &cli))
        return NULL;

    return Py_BuildValue("i", AtCliExecute(cli));
    }

static PyObject *RemoteCliResult(PyObject *self, PyObject* args)
    {
    char *result;

    AtUnused(self);
    AtUnused(args);

    result = AtPythonCliRemoteResult(NULL);
    if (result)
        return Py_BuildValue("s", AtPythonCliRemoteResult(NULL));

    Py_INCREF(Py_None);
    return Py_None;
    }

static PyObject *RemoteTimeoutSet(PyObject *self, PyObject* args)
    {
    int timeout;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "i", &timeout))
        return NULL;

    AtConnectionReceiveTimeoutSet(AtTextUIRemoteConnection(AtCliSharedTextUI()), (uint32)timeout);

    Py_INCREF(Py_None);
    return Py_None;
    }

static PyObject *CapturedStdOut(PyObject *self, PyObject* args)
    {
    char *capturedOutput = CliAtTextUICapturedOutput();

    AtUnused(self);
    AtUnused(args);

    if ((capturedOutput == NULL) || (AtStrlen(capturedOutput) == 0))
        return Py_None;

    return Py_BuildValue("s", capturedOutput);
    }

static PyMethodDef *Methods(void)
    {
    static PyMethodDef atsdk_methods[] =
        {
        {"runCli", CliRun, METH_VARARGS, "Run an AT CLI"},
        {"remoteCliResult", RemoteCliResult, METH_VARARGS, "Get remote CLI result"},
        {"remoteTimeoutSet", RemoteTimeoutSet, METH_VARARGS, "Set remote connection timeout"},
        {"rd", AtPythonHalRead, METH_VARARGS, "Read hardware"},
        {"lrd", AtPythonHalLongRead, METH_VARARGS, "Long read hardware"},
        {"HalDidReadCallbackSet", AtPythonHalDidReadCallbackSet, METH_VARARGS, "Set HAL did read callback"},
        {"HalDidWriteCallbackSet", AtPythonHalDidWriteCallbackSet, METH_VARARGS, "Set HAL did write callback"},
        {"DeviceDidReadCallbackSet", AtPythonDeviceDidReadCallbackSet, METH_VARARGS, "Set device did read callback"},
        {"DeviceDidWriteCallbackSet", AtPythonDeviceDidWriteCallbackSet, METH_VARARGS, "Set device did write callback"},
        {"DeviceDidLongReadCallbackSet", AtPythonDeviceDidLongReadCallbackSet, METH_VARARGS, "Set device did long read callback"},
        {"DeviceDidLongWriteCallbackSet", AtPythonDeviceDidLongWriteCallbackSet, METH_VARARGS, "Set device did long write callback"},
        {"CapturedStdout", CapturedStdOut, METH_NOARGS, "Get captured stdout"},
        {NULL, NULL, 0, NULL}
        };
    return atsdk_methods;
    }

static void initatsdk(void)
    {
    PyImport_AddModule("atsdk");
    Py_InitModule("atsdk", Methods());
    }

static void ModulesInit(void)
    {
    initatsdk();
    initpyexpat();
    init_collections();
    initoperator();
    inititertools();
    init_socket();
    init_functools();
    initdatetime();
    inittime();
    initcStringIO();
    init_struct();
    initbinascii();
    initselect();
    initmath();
    init_random();
    init_md5();
    init_sha256();
    init_sha512();
    init_sha();
    initarray();
    init_io();
    initfcntl();
    initresource();
    inittermios();
    }

static const char *DefaultLibPaths(void)
    {
    return "/home/ATVN_Eng/af6xxx/atsdk/:"
           "/home/ATVN_Eng/af6xxx/atsdk/python/libs/python2.7/:"
           "/home/ATVN_Eng/af6xxx/atsdk/python/libs/python2.7/site-packages:"
           "/home/ATVN_Eng/af6xxx/atsdk/python/libs/python2.7/site-packages/unittest_xml_reporting-1.4.3-py2.7.egg:"
           "/home/ATVN_Eng/af6xxx/atsdk/python/libs/pysrc:"
           "/home/wdb/users/sw/atsdk/scripts/:"
           "/home/wdb/users/sw/atsdk/scripts/libs/python2.7/:"
           "/home/wdb/users/sw/atsdk/scripts/libs/python2.7/site-packages:"
           "/home/wdb/users/sw/atsdk/scripts/libs/pysrc";
    }

static void EnvironmentSet(const char *name)
    {
    int ret = 0;
    const char *sDefaultLibPaths = DefaultLibPaths();
    char *currentPythonPath = getenv(name);

    if ((currentPythonPath == NULL) || (AtStrlen(currentPythonPath) == 0))
        {
        ret = setenv(name, sDefaultLibPaths, 1);
        }
    else
        {
        char *newPath = AtOsalMemAlloc(AtStrlen(currentPythonPath) + AtStrlen(sDefaultLibPaths) + 2);
        AtSprintf(newPath, "%s:%s", currentPythonPath, sDefaultLibPaths);
        ret = setenv(name, newPath, 1);
        AtOsalMemFree(newPath);
        }

    if (ret != 0)
        AtPrintc(cSevCritical, "WARNING: %s is fail\r\n", name);
    }

static void EnvironmentDefaultSet(void)
    {
    static uint8 setup = cAtFalse;

    if (setup)
        return;

    EnvironmentSet("PYTHONHOME");
    EnvironmentSet("PYTHONPATH");
    setup = cAtTrue;
    }

static void PythonFinalize(void)
    {
    Py_Finalize();
    AtPythonCliRemoteFinalize();
    AtPythonHalCleanup();
    AtPythonDeviceCleanup();
    }

static void PythonInit(void)
    {
    char progName[] = "atsdk";
    Py_NoSiteFlag = 1;

    Py_SetProgramName(progName);
    EnvironmentDefaultSet();
    Py_Initialize();
    ModulesInit();
    AtPythonCliTableInit();
    AtPythonCliRemoteInit();
    }

void AtPythonInit(void)
    {
    PythonInit();
    }

int AtPythonScriptRun(char *scriptPath, int argc, char **argv)
    {
    FILE *file = fopen(scriptPath, "r");
    if (file == NULL)
        return 1;

    PySys_SetArgv(argc, argv);
    PyRun_SimpleFile(file, scriptPath);
    PythonFinalize();

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtCliRemote.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : To handle remote CLI result
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtPythonInternal.h"
#include "AtCliService.h"

/*--------------------------- Define -----------------------------------------*/
#define cDefaultCliResultBufferSize 1024

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static char *m_resultBuffer = NULL;
static uint32 m_resultBufferSize = 0;
static uint32 m_resultBufferCurrentPosition = 0;
static eBool m_resultBufferIsValid = cAtFalse;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *Buffer(uint32 *bufferSize)
    {
    if (m_resultBuffer == NULL)
        {
        m_resultBuffer = AtOsalMemAlloc(cDefaultCliResultBufferSize);
        m_resultBufferSize = cDefaultCliResultBufferSize;
        }

    if (bufferSize)
        *bufferSize = m_resultBufferSize;

    return m_resultBuffer;
    }

static char *BufferEnlarge(uint32 *bufferSize)
    {
    char *newBuffer;
    uint32 newSize;

    /* Note, should not use realloc() since some environment does not support */
    newSize = m_resultBufferSize + cDefaultCliResultBufferSize;
    newBuffer = AtOsalMemAlloc(newSize);
    AtOsalMemInit(newBuffer, 0, newSize);

    /* Copy content and destroy the old one */
    AtOsalMemCpy(newBuffer, m_resultBuffer, m_resultBufferSize);
    AtOsalMemFree(m_resultBuffer);

    /* All set, use this new one */
    m_resultBuffer = newBuffer;
    m_resultBufferSize = newSize;

    if (bufferSize)
        *bufferSize = m_resultBufferSize;

    return m_resultBuffer;
    }

static void DidSendCli(const char *cli, void *userData)
    {
    uint32 bufferSize;
    char *buffer = Buffer(&bufferSize);
    AtOsalMemInit(buffer, 0, bufferSize);
    m_resultBufferCurrentPosition = 0;
    m_resultBufferIsValid = cAtFalse;
    AtUnused(userData);
    AtUnused(cli);
    }

static void DidReceiveData(char *dataBuffer, uint32 dataBufferSize, void *userData)
    {
    uint32 resultBufferSize;
    char *resultBuffer = Buffer(&resultBufferSize);
    uint32 remainingBytes = resultBufferSize - m_resultBufferCurrentPosition;

    AtUnused(userData);

    /* If buffer is not enough space, give it more spaces */
    while (remainingBytes < dataBufferSize)
        {
        resultBuffer = BufferEnlarge(&resultBufferSize);
        remainingBytes = resultBufferSize - m_resultBufferCurrentPosition;
        }

    /* Copy data */
    resultBuffer = resultBuffer + m_resultBufferCurrentPosition;
    AtOsalMemCpy(resultBuffer, dataBuffer, dataBufferSize);
    m_resultBufferCurrentPosition = m_resultBufferCurrentPosition + dataBufferSize;
    }

static void DidFinish(void *userData)
    {
    AtUnused(userData);
    m_resultBufferIsValid = cAtTrue;
    }

static eBool ShouldPrintResult(const char *cli)
    {
    AtUnused(cli);
    return cAtFalse;
    }

static tAtCliClientListener *Listener(void)
    {
    static tAtCliClientListener listener;
    static tAtCliClientListener *pListener = NULL;

    if (pListener)
        return pListener;

    listener.DidSendCli        = DidSendCli;
    listener.DidReceiveData    = DidReceiveData;
    listener.DidFinish         = DidFinish;
    listener.ShouldPrintResult = ShouldPrintResult;
    pListener = &listener;

    return pListener;
    }

void AtPythonCliRemoteInit(void)
    {
    AtCliClientListenerSet(Listener(), NULL);
    }

void AtPythonCliRemoteFinalize(void)
    {
    AtOsalMemFree(m_resultBuffer);
    m_resultBuffer = NULL;
    AtCliClientListenerSet(NULL, NULL);
    }

char *AtPythonCliRemoteResult(uint32 *size)
    {
    if (!m_resultBufferIsValid)
        return NULL;

    if (size)
        *size = m_resultBufferCurrentPosition;
    return m_resultBuffer;
    }

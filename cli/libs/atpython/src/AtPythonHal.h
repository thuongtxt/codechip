/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Python
 * 
 * File        : AtPythonHal.h
 * 
 * Created Date: Jan 8, 2017
 *
 * Description : To work with HAL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPYTHONHAL_H_
#define _ATPYTHONHAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Python.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
PyObject *AtPythonHalDidReadCallbackSet(PyObject *self, PyObject* args);
PyObject *AtPythonHalDidWriteCallbackSet(PyObject *self, PyObject* args);
PyObject *AtPythonHalRead(PyObject *self, PyObject* args);
PyObject *AtPythonHalLongRead(PyObject *self, PyObject* args);
void AtPythonHalCleanup(void);

PyObject *AtPythonDeviceDidReadCallbackSet(PyObject *self, PyObject* args);
PyObject *AtPythonDeviceDidWriteCallbackSet(PyObject *self, PyObject* args);
PyObject *AtPythonDeviceDidLongReadCallbackSet(PyObject *self, PyObject* args);
PyObject *AtPythonDeviceDidLongWriteCallbackSet(PyObject *self, PyObject* args);
void AtPythonDeviceCleanup(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATPYTHONHAL_H_ */


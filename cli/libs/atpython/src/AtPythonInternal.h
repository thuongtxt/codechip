/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtPythonInternal.h
 * 
 * Created Date: Dec 7, 2015
 *
 * Description : Work with Python
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPYTHONINTERNAL_H_
#define _ATPYTHONINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "Python.h"
#include "AtPython.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Work with table */
int AtPythonCliTableInit(void);

/* Work with remote Text UI */
void AtPythonCliRemoteInit(void);
void AtPythonCliRemoteFinalize(void);
char *AtPythonCliRemoteResult(uint32 *size);

#ifdef __cplusplus
}
#endif
#endif /* _ATPYTHONINTERNAL_H_ */


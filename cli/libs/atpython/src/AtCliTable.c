/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtCliTable.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : CLI table that works with Python
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"
#include "AtPythonInternal.h"
#include "showtable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern tTab* GlobalTableGet(void);

/*--------------------------- Implementation ---------------------------------*/
static PyObject *ColumnId(PyObject *self, PyObject* args)
    {
    char *columnName;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "s", &columnName))
        return NULL;

    return Py_BuildValue("i", TableColumnIdGet(GlobalTableGet(), columnName));
    }

static PyObject *ColumnName(PyObject *self, PyObject* args)
    {
    int columnId;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "i", &columnId))
        return NULL;

    return Py_BuildValue("s", TableColumnNameGet(GlobalTableGet(), columnId));
    }

static PyObject *CellContent(PyObject *self, PyObject* args)
    {
    uint32 rowId, columnId;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "ii", &rowId, &columnId))
        return NULL;

    return Py_BuildValue("s", TableCellStringGet(GlobalTableGet(), rowId, columnId));
    }

static PyObject *NumColumns(PyObject *self, PyObject* args)
    {
    uint32 rowSize, columnSize;
    AtUnused(self);
    AtUnused(args);
    TableSizeGet(GlobalTableGet(), &rowSize, &columnSize);
    return Py_BuildValue("i", columnSize);
    }

static PyObject *NumRows(PyObject *self, PyObject* args)
    {
    uint32 rowSize, columnSize;
    AtUnused(self);
    AtUnused(args);
    TableSizeGet(GlobalTableGet(), &rowSize, &columnSize);
    return Py_BuildValue("i", rowSize);
    }

static PyMethodDef *Methods(void)
    {
    static PyMethodDef methods[] =
        {
        {"columnId", ColumnId, METH_VARARGS, "Get column ID from column name"},
        {"columnName", ColumnName, METH_VARARGS, "Get column name from column ID"},
        {"cellContent", CellContent, METH_VARARGS, "Get cell content from rowId and columnId"},
        {"numColumns", NumColumns, METH_VARARGS, "Get number of columns"},
        {"numRows", NumRows, METH_VARARGS, "Get number of rows"},
        {NULL, NULL, 0, NULL}
        };
    return methods;
    }

int AtPythonCliTableInit(void)
    {
    PyImport_AddModule("globalTable");
    Py_InitModule("globalTable", Methods());
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Python
 *
 * File        : AtPythonHal.c
 *
 * Created Date: Jan 8, 2017
 *
 * Description : To work with HAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtOsal.h"
#include "AtPythonHal.h"
#include "AtPythonInternal.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtHalPyCallback
    {
    PyObject *didRead;
    PyObject *didWrite;
    eBool listened;
    }tAtHalPyCallback;

typedef struct tAtDevicePyCallback
    {
    PyObject *didRead;
    PyObject *didWrite;
    PyObject *didLongRead;
    PyObject *didLongWrite;
    eBool listened;
    }tAtDevicePyCallback;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtDevicePyCallback *DeviceCallback(void)
    {
    static tAtDevicePyCallback callback;
    static tAtDevicePyCallback *pCallback = NULL;

    if (pCallback)
        return pCallback;

    AtOsalMemInit(&callback, 0, sizeof(callback));
    pCallback = &callback;

    return pCallback;
    }

static tAtHalPyCallback *HalCallback(void)
    {
    static tAtHalPyCallback callback;
    static tAtHalPyCallback *pCallback = NULL;

    if (pCallback)
        return pCallback;

    AtOsalMemInit(&callback, 0, sizeof(callback));
    pCallback = &callback;

    return pCallback;
    }

static void HalCallbackCallWithAddressAndValue(uint32 address, uint32 value, PyObject *callback)
    {
    PyObject *arglist;
    PyObject *result;

    if (callback == NULL)
        return;

    arglist = Py_BuildValue("(II)", address, value);
    if (!arglist)
        {
        AtPrintc(cSevCritical, "%s, %s, arglist is NULL\r\n", __FILE__, __func__);
        return;
        }

    result = PyObject_Call(callback, arglist, NULL);
    Py_XDECREF(result);
    Py_DECREF(arglist);
    }

static void HalDidReadRegister(AtHal self, void *listener, uint32 address, uint32 value)
    {
    AtUnused(self);
    AtUnused(listener);
    HalCallbackCallWithAddressAndValue(address, value, HalCallback()->didRead);
    }

static void HalDidWriteRegister(AtHal self, void *listener, uint32 address, uint32 value)
    {
    AtUnused(self);
    AtUnused(listener);
    HalCallbackCallWithAddressAndValue(address, value, HalCallback()->didWrite);
    }

static tAtHalListener *HalListener(void)
    {
    static tAtHalListener listener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidReadRegister  = HalDidReadRegister;
    listener.DidWriteRegister = HalDidWriteRegister;

    return &listener;
    }

static void HalListen(void)
    {
    tAtHalPyCallback *callback = HalCallback();

    if (callback->listened)
        return;

    AtHalListenerAdd(AtDeviceIpCoreHalGet(CliDevice(), 0), NULL, HalListener());

    callback->listened = cAtTrue;
    }

static PyObject *HalCallbackSet(PyObject *self, PyObject* args, PyObject **callback)
    {
    PyObject *parsedCallback;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "O", &parsedCallback))
        return NULL;

    if (!PyCallable_Check(parsedCallback))
        {
        PyErr_SetString(PyExc_TypeError, "Need a callable object!");
        return NULL;
        }

    HalListen();
    Py_XINCREF(parsedCallback);
    *callback = parsedCallback;

    return Py_None;
    }

static void DeviceShortCallbackCallWithAddressAndValue(uint32 address, uint32 value, uint8 coreId, PyObject *callback)
    {
    PyObject *arglist;
    PyObject *result;

    if (callback == NULL)
        return;

    arglist = Py_BuildValue("(III)", address, value, coreId);
    result = PyObject_Call(callback, arglist, NULL);
    Py_XDECREF(result);
    Py_DECREF(arglist);
    }

static PyObject *LongValueMake(uint32 numDwords, const uint32 *values)
    {
    PyObject *array;
    uint32 dword_i;

    array = PyList_New((Py_ssize_t)numDwords);
    if (array == NULL)
        return NULL;

    for (dword_i = 0; dword_i < numDwords; dword_i++)
        {
        PyObject *value = Py_BuildValue("i", values[dword_i]);
        if (value == NULL)
            {
            Py_DECREF(array);
            return NULL;
            }
        PyList_SET_ITEM(array, dword_i, value);
        }

    return array;
    }

static void DeviceLongCallbackCallWithAddressAndValue(uint32 address, const uint32 *values, uint32 numDwords, uint8 coreId, PyObject *callback)
    {
    PyObject *arglist;
    PyObject *result;
    PyObject *dwordArray = LongValueMake(numDwords, values);

    if (callback == NULL)
        return;

    if (!dwordArray)
        {
        AtPrintc(cSevCritical, "%s, %s, dwordArray is NULL\r\n", __FILE__, __func__);
        return;
        }

    arglist = Py_BuildValue("(IOI)", address, dwordArray, coreId);
    if (!arglist)
        {
        AtPrintc(cSevCritical, "%s, %s, arglist is NULL\r\n", __FILE__, __func__);
        Py_DECREF(dwordArray);
        return;
        }

    result = PyObject_Call(callback, arglist, NULL);
    Py_XDECREF(result);
    Py_DECREF(arglist);
    Py_DECREF(dwordArray);
    }

static void DeviceDidReadRegister(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    if (DeviceCallback()->didRead)
        DeviceShortCallbackCallWithAddressAndValue(address, value, coreId, DeviceCallback()->didRead);
    }

static void DeviceDidWriteRegister(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    if (DeviceCallback()->didWrite)
        DeviceShortCallbackCallWithAddressAndValue(address, value, coreId, DeviceCallback()->didWrite);
    }

static void DeviceDidLongReadOnCore(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    if (DeviceCallback()->didLongRead)
        DeviceLongCallbackCallWithAddressAndValue(address, dataBuffer, numDwords, coreId, DeviceCallback()->didLongRead);
    }

static void DeviceDidLongWriteOnCore(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    if (DeviceCallback()->didLongWrite)
        DeviceLongCallbackCallWithAddressAndValue(address, dataBuffer, numDwords, coreId, DeviceCallback()->didLongWrite);
    }

static tAtDeviceListener *DeviceListener(void)
    {
    static tAtDeviceListener listener;

    AtOsalMemInit(&listener, 0, sizeof(listener));

    listener.DidReadRegister    = DeviceDidReadRegister;
    listener.DidWriteRegister   = DeviceDidWriteRegister;
    listener.DidLongReadOnCore  = DeviceDidLongReadOnCore;
    listener.DidLongWriteOnCore = DeviceDidLongWriteOnCore;

    return &listener;
    }

static void DeviceListen(void)
    {
    tAtDevicePyCallback *callback = DeviceCallback();

    if (callback->listened)
        return;

    AtDeviceListenerAdd(CliDevice(), DeviceListener(), NULL);
    callback->listened = cAtTrue;
    }

static PyObject *DeviceCallbackSet(PyObject *self, PyObject* args, PyObject **callback)
    {
    PyObject *parsedCallback;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "O", &parsedCallback))
        return NULL;

    if (!PyCallable_Check(parsedCallback))
        {
        PyErr_SetString(PyExc_TypeError, "Need a callable object!");
        return NULL;
        }

    DeviceListen();
    Py_XINCREF(parsedCallback);
    *callback = parsedCallback;

    return Py_None;
    }

PyObject *AtPythonHalDidReadCallbackSet(PyObject *self, PyObject* args)
    {
    return HalCallbackSet(self, args, &(HalCallback()->didRead));
    }

PyObject *AtPythonHalDidWriteCallbackSet(PyObject *self, PyObject* args)
    {
    return HalCallbackSet(self, args, &(HalCallback()->didWrite));
    }

PyObject *AtPythonHalRead(PyObject *self, PyObject* args)
    {
    uint32 address, coreId;
    AtHal hal;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "II", &address, &coreId))
        return NULL;

    hal = AtDeviceIpCoreHalGet(CliDevice(), (uint8)coreId);
    return Py_BuildValue("i", AtHalRead(hal, address));
    }

PyObject *AtPythonHalLongRead(PyObject *self, PyObject* args)
    {
    uint32 address, coreId;
    uint32 values[10];
    uint32 numDwords;

    AtUnused(self);

    if (!PyArg_ParseTuple(args, "II", &address, &coreId))
        return NULL;

    numDwords = AtDeviceLongReadOnCore(CliDevice(), address, values, mCount(values), (uint8)coreId);
    if (numDwords == 0)
        return NULL;

    return LongValueMake(numDwords, values);
    }

void AtPythonHalCleanup(void)
    {
    tAtHalPyCallback *callback = HalCallback();

    if (!callback->listened)
        return;

    Py_XDECREF(callback->didRead);
    Py_XDECREF(callback->didWrite);

    AtOsalMemInit(callback, 0, sizeof(tAtHalPyCallback));
    }

PyObject *AtPythonDeviceDidReadCallbackSet(PyObject *self, PyObject* args)
    {
    return DeviceCallbackSet(self, args, &(DeviceCallback()->didRead));
    }

PyObject *AtPythonDeviceDidWriteCallbackSet(PyObject *self, PyObject* args)
    {
    return DeviceCallbackSet(self, args, &(DeviceCallback()->didWrite));
    }

PyObject *AtPythonDeviceDidLongReadCallbackSet(PyObject *self, PyObject* args)
    {
    return DeviceCallbackSet(self, args, &(DeviceCallback()->didLongRead));
    }

PyObject *AtPythonDeviceDidLongWriteCallbackSet(PyObject *self, PyObject* args)
    {
    return DeviceCallbackSet(self, args, &(DeviceCallback()->didLongWrite));
    }

void AtPythonDeviceCleanup(void)
    {
    tAtDevicePyCallback *callback = DeviceCallback();

    if (!callback->listened)
        return;

    Py_XDECREF(callback->didRead);
    Py_XDECREF(callback->didWrite);
    Py_XDECREF(callback->didLongRead);
    Py_XDECREF(callback->didLongWrite);

    AtOsalMemInit(callback, 0, sizeof(tAtDevicePyCallback));
    }

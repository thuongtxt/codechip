/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ID parser
 *
 * File        : AtIdTermBasic.c
 *
 * Created Date: Nov 14, 2013
 *
 * Description : Basic ID term
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtTokenizer.h"
#include "AtIdTermInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cEqual        0
#define cLessThan     1
#define cGreaterThan  2
#define cInvalidValue 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtIdTermBasic)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtIdTermMethods        m_AtIdTermOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtOsalMemFree(mThis(self)->numbers);
    m_AtObjectMethods->Delete(self);
    }

static AtObject Clone(AtObject self)
    {
    AtIdTermBasic newTerm = AtIdTermBasicNew(NULL, AtIdTermIdParser((AtIdTerm)self));
    uint32 memorySize = AtIdTermNumNumbersGet((AtIdTerm)self) * sizeof(uint32);

    if ((newTerm == NULL) || (memorySize == 0))
        return (AtObject)newTerm;

    newTerm->numbers = AtOsalMemAlloc(memorySize);
    AtOsalMemCpy(newTerm->numbers, mThis(self)->numbers, memorySize);
    AtIdTermNumNumbersSet((AtIdTerm)newTerm, AtIdTermNumNumbersGet((AtIdTerm)self));

    return (AtObject)newTerm;
    }

static uint32 *IdsGenerate(AtIdTermBasic self, const char *idString, uint32 *pNumNumber)
    {
    const char *cSeparateChar = ".";
    uint32 *numbers, numNumbers;
    char *dupIdTermString = AtStrdup(idString);
    char *dupIdTermStringToDelete = dupIdTermString;
    AtTokenizer tokenizer;
	AtUnused(self);

    /* Allocate memory */
    if ((dupIdTermString == NULL) || AtStrlen(dupIdTermString) == 0)
        return NULL;

    numNumbers = AtIdParserNumCharInString(dupIdTermString, '.') + 1;
    numbers    = AtOsalMemAlloc(numNumbers * sizeof(uint32));

    /* Parse all numbers */
    tokenizer = AtTokenizerNew(dupIdTermString, cSeparateChar);
    numNumbers = 0;
    while (AtTokenizerHasNextString(tokenizer))
        {
        numbers[numNumbers] = AtStrToDw(AtTokenizerNextString(tokenizer));
        numNumbers          = numNumbers + 1;
        }

    AtOsalMemFree(dupIdTermStringToDelete);
    AtObjectDelete((AtObject)tokenizer);

    if (pNumNumber)
        *pNumNumber = numNumbers;
    return numbers;
    }

static uint32 NumNumbersCalculate(AtIdTerm self)
    {
    return AtIdTermNumNumbersGet((AtIdTerm)self);
    }

static eBool IsValid(AtIdTerm self)
    {
    uint32 level_i;
    AtIdTermBasic minFormat = AtIdParserMinFormat(self->idParser);
    AtIdTermBasic maxFormat = AtIdParserMaxFormat(self->idParser);

    if (AtIdTermNumNumbers((AtIdTerm)minFormat) != AtIdTermNumNumbers((AtIdTerm)maxFormat))
        return cAtFalse;

    if ((AtIdTermNumNumbers((AtIdTerm)minFormat) != 0) && (AtIdTermNumNumbers((AtIdTerm)minFormat) != AtIdTermNumNumbers(self)))
        return cAtFalse;

    for (level_i = 0; level_i < AtIdTermNumNumbersGet((AtIdTerm)self); level_i++)
        {
        if ((AtIdTermNumNumbersGet((AtIdTerm)minFormat) > 0) && (mThis(self)->numbers[level_i] < minFormat->numbers[level_i]))
            return cAtFalse;

        if ((AtIdTermNumNumbersGet((AtIdTerm)maxFormat) > 0) && (mThis(self)->numbers[level_i] > maxFormat->numbers[level_i]))
            return cAtFalse;
        }

    if ((AtIdTermNumNumbersGet((AtIdTerm)minFormat) > 0) && (AtIdTermNumNumbersGet((AtIdTerm)self) != AtIdTermNumNumbersGet((AtIdTerm)minFormat)))
        return cAtFalse;

    return cAtTrue;
    }

static void Restart(AtIdTerm self)
    {
    mThis(self)->currentNumberIndex = 0;
    }

static uint32 NextNumber(AtIdTerm self)
    {
    uint32 nextNumber = cInvalidValue;

    if (mThis(self)->currentNumberIndex < AtIdTermNumNumbersGet((AtIdTerm)self))
        {
        nextNumber = mThis(self)->numbers[mThis(self)->currentNumberIndex];
        mThis(self)->currentNumberIndex = mThis(self)->currentNumberIndex + 1;
        }

    return nextNumber;
    }

static eBool HasNextNumber(AtIdTerm self)
    {
    return (mThis(self)->currentNumberIndex < AtIdTermNumNumbersGet((AtIdTerm)self)) ? cAtTrue : cAtFalse;
    }

static uint8 NumberCompare(uint32 number1, uint32 number2)
    {
    if (number1 < number2) return cLessThan;
    if (number1 > number2) return cGreaterThan;
    return cEqual;
    }

static uint8 IdTermCompare(AtIdTermBasic self, AtIdTermBasic otherTerm)
    {
    uint32 i;

    for (i = 0; i < AtIdTermNumNumbers((AtIdTerm)self); i++)
        {
        uint8 compareResult = cEqual;

        compareResult = NumberCompare(AtIdTermBasicNumberAtIndex(self, i), AtIdTermBasicNumberAtIndex(otherTerm, i));
        if (compareResult != cEqual)
            return compareResult;
        }

    return cEqual;
    }

static void Debug(AtIdTerm self)
    {
    uint32 i;

    for (i = 0; i < AtIdTermNumNumbersGet((AtIdTerm)self); i++)
        AtPrintf((i == 0) ? "%d" : ".%d", mThis(self)->numbers[i]);

    AtPrintf("\n");
    }

static uint32 NumLevel(AtIdTerm self)
    {
    return NumNumbersCalculate(self);
    }

static void OverrideAtIdTerm(AtIdTermBasic self)
    {
    AtIdTerm idTerm = (AtIdTerm)self;
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtIdTermOverride, mMethodsGet(idTerm), sizeof(m_AtIdTermOverride));

        mMethodOverride(m_AtIdTermOverride, IsValid);
        mMethodOverride(m_AtIdTermOverride, Restart);
        mMethodOverride(m_AtIdTermOverride, NumNumbersCalculate);
        mMethodOverride(m_AtIdTermOverride, NextNumber);
        mMethodOverride(m_AtIdTermOverride, HasNextNumber);
        mMethodOverride(m_AtIdTermOverride, Debug);
        mMethodOverride(m_AtIdTermOverride, NumLevel);
        }

    mMethodsSet(idTerm, &m_AtIdTermOverride);
    }

static void OverrideAtObject(AtIdTermBasic self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Clone);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtIdTermBasic self)
    {
    OverrideAtObject(self);
    OverrideAtIdTerm(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtIdTermBasic);
    }

static AtIdTermBasic ObjectInit(AtIdTermBasic self, const char *idTermString, AtIdParser parser)
    {
    /* Super constructor */
    AtOsalMemInit(self, 0, ObjectSize());
    if (AtIdTermObjectInit((AtIdTerm)self, idTermString, parser) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Generate ID */
    if (idTermString)
        {
        uint32 numNumbers = 0;

        mThis(self)->numbers = IdsGenerate(self, idTermString, &numNumbers);
        AtIdTermNumNumbersSet((AtIdTerm)self, numNumbers);
        }

    return self;
    }

AtIdTermBasic AtIdTermBasicNew(const char *idTermString, AtIdParser parser)
    {
    AtIdTermBasic newTerm;

    newTerm = AtOsalMemAlloc(ObjectSize());
    if (newTerm == NULL)
        return NULL;

    if (ObjectInit(newTerm, idTermString, parser) == NULL)
        {
        AtOsalMemFree(newTerm);
        return NULL;
        }

    return newTerm;
    }

uint32 AtIdTermBasicNumberAtIndex(AtIdTermBasic self, uint32 numberIndex)
    {
    if ((self == NULL) || (numberIndex >= AtIdTermNumNumbersGet((AtIdTerm)self)))
        return cInvalidValue;

    return self->numbers[numberIndex];
    }

eBool AtIdTermBasicIsLessThan(AtIdTermBasic self, AtIdTermBasic otherTerm)
    {
    return (IdTermCompare(self, otherTerm) == cLessThan) ? cAtTrue : cAtFalse;
    }

eBool AtIdTermBasicIsGreaterThan(AtIdTermBasic self, AtIdTermBasic otherTerm)
    {
    return (IdTermCompare(self, otherTerm) == cGreaterThan) ? cAtTrue : cAtFalse;
    }

AtIdTermBasic AtIdTermBasicIncrease(AtIdTermBasic self)
    {
    uint32 level;
    AtIdParser parser = AtIdTermIdParser((AtIdTerm)self);
    AtIdTermBasic minFormat = AtIdParserMinFormat(parser);
    AtIdTermBasic maxFormat = AtIdParserMaxFormat(parser);
    uint32 higherLevel;

    if ((self == NULL) || (AtIdTermNumNumbersGet((AtIdTerm)self) == 0))
        return NULL;

    if (minFormat == NULL)
        return NULL;

    /* There is no max format, just simply increase */
    if ((AtIdTermNumNumbers((AtIdTerm)maxFormat) == 0) && (AtIdTermNumNumbers((AtIdTerm)self) == 1))
        {
        self->numbers[0] = self->numbers[0] + 1;
        self->currentNumberIndex = 0;
        return self;
        }

    level = AtIdTermNumNumbersGet((AtIdTerm)self) - 1;
    
    /* Can increase this level */
    if (self->numbers[level] < maxFormat->numbers[level])
        {
        self->numbers[level] = self->numbers[level] + 1;

        /* Restart after increasing */
        self->currentNumberIndex = 0;
        return self;
        }

    /* Reach the highest level */
    if (level == 0)
        return NULL;

    /* This level is full and cannot be increased anymore, need to reset it
       to min format and try to find the next level that can be increased */
    for (higherLevel = level - 1; higherLevel > 0; higherLevel--)
        {
        if (self->numbers[higherLevel] < maxFormat->numbers[higherLevel])
            break;
        }

    /* Reach the end level and cannot increase anymore */
    if ((higherLevel == 0) && (self->numbers[higherLevel] >= maxFormat->numbers[higherLevel]))
        return NULL;

    /* This higher level can be increased, need to increase it and restart lower levels */
    self->numbers[higherLevel] = self->numbers[higherLevel] + 1;
    for (level = (higherLevel + 1); level < AtIdTermNumNumbersGet((AtIdTerm)self); level++)
        self->numbers[level] = minFormat->numbers[level];

    /* Restart after increasing */
    self->currentNumberIndex = 0;

    return self;
    }

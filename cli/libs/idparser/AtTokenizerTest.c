/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtTokenizerTest.c
 *
 * Created Date: Mar 6, 2014
 *
 * Description : Self-test tokenizer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "commacro.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void _testNumStrings(const char *aString, const char *separator, uint32 expectedNumStrings)
    {
    char *newString = AtStrdup(aString);
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(newString, separator);
    AtAssert(AtTokenizerNumStrings(tokenizer) == expectedNumStrings);
    AtOsalMemFree(newString);
    }

static void testNumStrings(void)
    {
    _testNumStrings("", "", 0);
    _testNumStrings("", NULL, 1);
    _testNumStrings(NULL, "", 0);
    _testNumStrings(NULL, NULL, 0);
    _testNumStrings("1| 2   3    ", NULL, 1);
    _testNumStrings("|||", "|", 0);
    _testNumStrings("|1|||222|||3|||", "|", 3);
    _testNumStrings("|1|||222|||3||||", "||", 3);
    }

static void testNextSubStrings(void)
    {
    char buffer[64];
    AtTokenizer tokenizer;

    /* Build a tokenizer */
    AtSprintf(buffer, "|1|||222|||3|||");
    tokenizer = AtTokenizerSharedTokenizer(buffer, "|");

    /* Iterate sub strings */
    AtAssert(AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(AtTokenizerNextString(tokenizer), "1") == 0);
    AtAssert(AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(AtTokenizerNextString(tokenizer), "222") == 0);
    AtAssert(AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(AtTokenizerNextString(tokenizer), "3") == 0);
    AtAssert(!AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(buffer, "|1|||222|||3|||") == 0);

    /* Try another string */
    AtSprintf(buffer, "|1|||222|||3||||");
    AtTokenizerStringSet(tokenizer, buffer);
    AtTokenizerSeparatorSet(tokenizer, "||");
    AtTokenizerRestart(tokenizer);

    /* And iterate */
    AtAssert(AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(AtTokenizerNextString(tokenizer), "|1") == 0);
    AtAssert(AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(AtTokenizerNextString(tokenizer), "|222") == 0);
    AtAssert(AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(AtTokenizerNextString(tokenizer), "|3") == 0);
    AtAssert(!AtTokenizerHasNextString(tokenizer));
    AtAssert(AtStrcmp(buffer, "|1|||222|||3||||") == 0);
    }

void AtTokenizerTest(void)
    {
    testNumStrings();
    testNextSubStrings();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtIdRange.c
 *
 * Created Date: Nov 12, 2013
 *
 * Description : ID range
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtIdTermInternal.h"
#include "AtIdRange.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtIdRange)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtIdTermMethods        m_AtIdTermOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtIdRange);
    }

static void Delete(AtObject self)
    {
    AtIdRange range = mThis(self);

    AtObjectDelete((AtObject)range->startTerm);
    AtObjectDelete((AtObject)range->stopTerm);
    AtObjectDelete((AtObject)range->currentId);

    m_AtObjectMethods->Delete(self);
    }

static void RangeTermsGenerate(AtIdRange self, char *idRangeString)
    {
    const char *sRangeChar = "-";
    AtIdTerm idTerm = (AtIdTerm)self;
    AtIdParser idParser = AtIdTermIdParser(idTerm);
    char *dupString = AtStrdup(idRangeString);
    char *dupStringToDelete = dupString;
    AtTokenizer tokenizer = AtTokenizerNew(dupString, sRangeChar);

    self->startTerm = AtIdTermBasicNew(AtTokenizerNextString(tokenizer), idParser);
    self->stopTerm  = AtIdTermBasicNew(AtTokenizerNextString(tokenizer), idParser);

    AtOsalMemFree(dupStringToDelete);
    AtObjectDelete((AtObject)tokenizer);
    }

static eBool ComplexLevelMatch(AtIdRange self, AtIdTermBasic rangeTerm)
    {
    /* For complex IDs, min/max must be input */
    AtIdParser parser = AtIdTermIdParser((AtIdTerm)self);
    uint32 numLevels = AtIdTermNumLevel((AtIdTerm)rangeTerm);
    if (numLevels <= 1)
        return cAtTrue;

    /* Min/Max is not specified, in this case, the outside may want to get
     * levels only, it does not intent to iterate numbers */
    if ((AtIdTermNumLevel((AtIdTerm)AtIdParserMinFormat(parser)) == 0) &&
        (AtIdTermNumLevel((AtIdTerm)AtIdParserMaxFormat(parser)) == 0))
        return cAtTrue;

    /* If min/max is specified, all ID levels must match */
    if ((numLevels == AtIdTermNumLevel((AtIdTerm)AtIdParserMinFormat(parser))) &&
        (numLevels == AtIdTermNumLevel((AtIdTerm)AtIdParserMaxFormat(parser))))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RangeIsUniform(AtIdRange self, AtIdTermBasic rangeTerm)
    {
    AtIdParser parser  = AtIdTermIdParser((AtIdTerm)self);
    AtIdTerm minFormat = (AtIdTerm)AtIdParserMinFormat(parser);

    if (!ComplexLevelMatch(self, rangeTerm))
        return cAtFalse;

    if ((AtIdTermNumNumbers((AtIdTerm)rangeTerm) == 1) || (AtIdTermNumNumbers(minFormat) == 0))
        return cAtTrue;

    return (AtIdTermNumNumbers((AtIdTerm)rangeTerm) == AtIdTermNumNumbers(minFormat)) ? cAtTrue : cAtFalse;
    }

static eBool StartRangeIsValid(AtIdRange self)
    {
    AtIdParser parser = AtIdTermIdParser((AtIdTerm)self);
    AtIdTermBasic format;
    uint32 formatNumNumber;

    if (self->startTerm == NULL)
        return cAtFalse;

    format = AtIdParserMinFormat(parser);
    if (format == NULL)
        return cAtFalse;

    formatNumNumber = AtIdTermNumNumbers((AtIdTerm)format);
    if ((formatNumNumber > 0) && AtIdTermBasicIsLessThan(self->startTerm, format))
        return cAtFalse;

    format = AtIdParserMaxFormat(parser);
    formatNumNumber = format ? AtIdTermNumNumbers((AtIdTerm)format) : 0;
    if ((formatNumNumber > 0) && AtIdTermBasicIsGreaterThan(self->startTerm, format))
        return cAtFalse;

    if (self->stopTerm && AtIdTermBasicIsGreaterThan(self->startTerm, self->stopTerm))
        return cAtFalse;

    return cAtTrue;
    }

static eBool StopRangeIsValid(AtIdRange self)
    {
    if (AtIdTermNumNumbers((AtIdTerm)self->stopTerm) != AtIdTermNumNumbers((AtIdTerm)self->startTerm))
        return cAtFalse;

    /* To be checked more ... */

    return cAtTrue;
    }

static eBool MatchConstrain(AtIdRange self)
    {
    if (StartRangeIsValid(self) && StopRangeIsValid(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsValid(AtIdTerm self)
    {
    AtIdRange range = mThis(self);

    /* The start range must exist */
    if (range->startTerm == NULL)
        return cAtFalse;

    if (!RangeIsUniform(range, range->startTerm))
        return cAtFalse;

    if (range->stopTerm && (!RangeIsUniform(range, range->stopTerm)))
        return cAtFalse;

    if (!MatchConstrain(range))
        return cAtFalse;

    return cAtTrue;
    }

static void Restart(AtIdTerm self)
    {
    AtObjectDelete((AtObject)mThis(self)->currentId);
    mThis(self)->currentId = NULL;
    }

static AtIdTermBasic CurrentId(AtIdRange self)
    {
    AtIdRange range = mThis(self);

    if (range->currentId == NULL)
        range->currentId = (AtIdTermBasic)AtObjectClone((AtObject)range->startTerm);

    return range->currentId;
    }

static AtIdTermBasic NextIdTerm(AtIdRange self)
    {
    AtIdTermBasic nextTerm;

    if (self->currentId == NULL)
        {
        self->currentId = (AtIdTermBasic)AtObjectClone((AtObject)self->startTerm);
        return self->currentId;
        }

    nextTerm = AtIdTermBasicIncrease(CurrentId(self));
    if (nextTerm == NULL)
        return NULL;

    self->currentId = nextTerm;
    if (AtIdTermBasicIsGreaterThan(self->currentId, self->stopTerm))
        return NULL;

    return self->currentId;
    }

static uint32 NextNumber(AtIdTerm self)
    {
    AtIdRange range = mThis(self);

    if (!AtIdTermHasNextNumber((AtIdTerm)range->currentId))
        NextIdTerm(range);

    if (range->currentId == NULL)
        return cInvalidValue;

    return AtIdTermNextNumber((AtIdTerm)range->currentId);
    }

static eBool CanIterate(AtIdRange self)
    {
    if (AtIdTermNumLevel((AtIdTerm)self) > 1)
        {
        AtIdParser parser = AtIdTermIdParser((AtIdTerm)self);
        if ((AtIdTermNumLevel((AtIdTerm)AtIdParserMinFormat(parser)) != AtIdTermNumLevel((AtIdTerm)self)) ||
            (AtIdTermNumLevel((AtIdTerm)AtIdParserMaxFormat(parser)) != AtIdTermNumLevel((AtIdTerm)self)))
            return cAtFalse;
        }

    return cAtTrue;
    }

static uint32 NumNumbersCalculate(AtIdTerm self)
    {
    AtIdRange range = mThis(self);
    AtIdTermBasic currentId;
    uint32 numNumbers = 0;
    uint32 idTermNumNumbers = AtIdTermNumNumbers((AtIdTerm)range->startTerm);

    if (!CanIterate(range))
        return 0;

    /* Backup current position first to restart iterating */
    currentId = range->currentId;

    /* Iterate */
    range->currentId = NULL;
    while (NextIdTerm(range))
        numNumbers = numNumbers + idTermNumNumbers;
    AtObjectDelete((AtObject)range->currentId);

    /* Restore current position */
    range->currentId = currentId;

    return numNumbers;
    }

static eBool HasNextNumber(AtIdTerm self)
    {
    AtIdRange range = mThis(self);
    AtIdParser parser;

    if (!CanIterate(range))
        return cAtFalse;

    if (range->currentId == NULL)
        return cAtTrue;

    if (AtIdTermHasNextNumber((AtIdTerm)range->currentId))
        return cAtTrue;

    parser = AtIdTermIdParser(self);
    if (AtIdTermBasicIsLessThan(range->currentId, range->stopTerm) &&
        AtIdTermBasicIsLessThan(range->currentId, AtIdParserMaxFormat(parser)))
        return cAtTrue;

    return cAtFalse;
    }

static void Debug(AtIdTerm self)
    {
    AtIdRange range = mThis(self);

    AtPrintf("- Start   : "); AtIdTermDebug((AtIdTerm)range->startTerm);
    AtPrintf("- Stop    : "); AtIdTermDebug((AtIdTerm)range->stopTerm);
    AtPrintf("- Current : "); AtIdTermDebug((AtIdTerm)range->currentId);
    if (range->currentId == 0)
        AtPrintf("\n");
    }

static uint32 NumLevel(AtIdTerm self)
    {
    return AtIdTermNumLevel((AtIdTerm)((AtIdRange)self)->startTerm);
    }

static void OverrideAtObject(AtIdRange self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtIdTerm(AtIdRange self)
    {
    AtIdTerm idTerm = (AtIdTerm)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtIdTermOverride, mMethodsGet(idTerm), sizeof(m_AtIdTermOverride));

        mMethodOverride(m_AtIdTermOverride, IsValid);
        mMethodOverride(m_AtIdTermOverride, Restart);
        mMethodOverride(m_AtIdTermOverride, HasNextNumber);
        mMethodOverride(m_AtIdTermOverride, NextNumber);
        mMethodOverride(m_AtIdTermOverride, NumNumbersCalculate);
        mMethodOverride(m_AtIdTermOverride, Debug);
        mMethodOverride(m_AtIdTermOverride, NumLevel);
        }

    mMethodsSet(idTerm, &m_AtIdTermOverride);
    }

static void Override(AtIdRange self)
    {
    OverrideAtObject(self);
    OverrideAtIdTerm(self);
    }

static AtIdRange ObjectInit(AtIdRange self, char *idTermString, AtIdParser parser)
    {
    /* Super constructor */
    AtOsalMemInit(self, 0, ObjectSize());
    if (AtIdTermObjectInit((AtIdTerm)self, idTermString, parser) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Generate ID ranges */
    if (idTermString)
        RangeTermsGenerate(self, idTermString);

    return self;
    }

AtIdRange AtIdRangeNew(char *idTermString, AtIdParser parser)
    {
    AtIdRange newRange = AtOsalMemAlloc(ObjectSize());
    if (newRange == NULL)
        return NULL;

    if (ObjectInit(newRange, idTermString, parser) == NULL)
        {
        AtObjectDelete((AtObject)newRange);
        return NULL;
        }

    return newRange;
    }

AtIdTermBasic AtIdRangeStartTerm(AtIdRange self)
    {
    return self ? self->startTerm : NULL;
    }

AtIdTermBasic AtIdRangeStopTerm(AtIdRange self)
    {
    return self ? self->stopTerm : NULL;
    }

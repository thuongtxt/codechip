/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtIdRange.h
 * 
 * Created Date: Nov 12, 2013
 *
 * Description : ID term
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIDRANGE_H_
#define _ATIDRANGE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIdTermInternal.h"
#include "AtIdParserInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIdRange
    {
    tAtIdTerm super;

    /* Private data */
    AtIdTermBasic startTerm;
    AtIdTermBasic stopTerm;
    AtIdTermBasic currentId;
    }tAtIdRange;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIdRange AtIdRangeNew(char *idTermString, AtIdParser parser);

AtIdTermBasic AtIdRangeStartTerm(AtIdRange self);
AtIdTermBasic AtIdRangeStopTerm(AtIdRange self);

#ifdef __cplusplus
}
#endif
#endif /* _ATIDRANGE_H_ */


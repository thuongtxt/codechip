/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtTokenizer.c
 *
 * Created Date: Mar 6, 2014
 *
 * Description : Parse string tokens
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "AtTokenizerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtTokenizer m_sharedTokenizer;
static AtTokenizer  m_sharedTokenizerPointer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSubStringsCalculate(AtTokenizer self)
    {
    char *current = self->stringBuffer;
    char *nextOccurrence = current;
    uint32 numSubStrings = 0;

    if (self->stringBuffer == NULL)
        return 0x0;

    if (self->separator == NULL)
        return 1;

    while ((nextOccurrence != NULL) && ((current - self->stringBuffer) < (int)self->stringLength))
        {
        eBool emptyToken;

        nextOccurrence = AtStrstr(current, self->separator);

        /* Just count none empty tokens */
        emptyToken = ((nextOccurrence - current) == 0);
        if (!emptyToken)
            numSubStrings = numSubStrings + 1;

        /* Move next */
        if (nextOccurrence)
            {
            uint32 nextOffset = AtStrlen(self->separator);
            if (nextOffset == 0)
                nextOffset = 1;
            current = nextOccurrence + nextOffset;
            }
        }

    return numSubStrings;
    }

static void RevertChange(AtTokenizer self)
    {
    char *previousChar;

    if (self->currentString == NULL)
        return;

    if (self->currentString == self->stringBuffer)
        return;

    previousChar = self->currentString - AtStrlen(self->separator);
    *previousChar = self->separator[0];
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTokenizer);
    }

static AtTokenizer ObjectInit(AtTokenizer self, char *aString, const char *separator)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Save private data */
    self->separator    = separator;
    self->stringBuffer = aString;
    if (aString)
        self->stringLength = AtStrlen(aString);

    return self;
    }

AtTokenizer AtTokenizerNew(char *aString, const char *separator)
    {
    AtTokenizer newTokenizer = AtOsalMemAlloc(ObjectSize());
    if (newTokenizer == NULL)
        return NULL;

    return ObjectInit(newTokenizer, aString, separator);
    }

uint32 AtTokenizerNumStrings(AtTokenizer self)
    {
    if (self == NULL)
        return 0;

    if (self->numSubStrings == 0)
        self->numSubStrings = NumSubStringsCalculate(self);
    return self->numSubStrings;
    }

eBool AtTokenizerHasNextString(AtTokenizer self)
    {
    eBool hasNext;

    if (self == NULL)
        return cAtFalse;

    hasNext = (self->currentStringIndex == AtTokenizerNumStrings(self)) ? cAtFalse : cAtTrue;
    if (!hasNext)
        RevertChange(self);

    return hasNext;
    }

char *AtTokenizerNextString(AtTokenizer self)
    {
    char *nextOccurrence = NULL;
    char *subString = NULL;
    eBool endOfString;

    if (self == NULL)
        return NULL;

    if (!AtTokenizerHasNextString(self))
        return NULL;

    /* End of string */
    if (self->currentString == self->stringBuffer)
        return NULL;

    /* No separator */
    if (self->separator == NULL)
        {
        self->currentStringIndex = self->currentStringIndex + 1;
        return self->stringBuffer;
        }

    /* This is the first time, if not, need to revert change made by previous iterating */
    if (self->currentString == NULL)
        self->currentString = self->stringBuffer;
    else
        RevertChange(self);

    /* Till a none substring is found */
    while ((self->currentString) && ((subString == NULL) || (AtStrlen(subString) == 0)))
        {
        /* Revert the position that is changed during iterating */
        if (nextOccurrence)
            nextOccurrence[0] = self->separator[0];

        /* Find next occurrence */
        nextOccurrence = AtStrstr(self->currentString, self->separator);
        if (nextOccurrence == NULL)
            {
            subString = self->currentString;
            continue;
            }

        /* Have this substring first */
        subString = self->currentString;
        nextOccurrence[0] = '\0';

        /* Then go to the next one */
        self->currentString = self->currentString + AtStrlen(subString) + AtStrlen(self->separator);
        endOfString = ((self->currentString - self->stringBuffer) >= (int)self->stringLength);
        if (endOfString)
            self->currentString = self->stringBuffer;
        }

    self->currentStringIndex = self->currentStringIndex + 1;

    return subString;
    }

void AtTokenizerRestart(AtTokenizer self)
    {
    if (self == NULL)
        return;

    self->currentString      = NULL;
    self->numSubStrings      = 0;
    self->currentStringIndex = 0;
    }

void AtTokenizerStringSet(AtTokenizer self, char *aString)
    {
    if (self == NULL)
        return;

    self->stringBuffer = aString;
    if (aString)
        self->stringLength = AtStrlen(aString);

    AtTokenizerRestart(self);
    }

void AtTokenizerSeparatorSet(AtTokenizer self, const char *separator)
    {
    if (self == NULL)
        return;

    self->separator = separator;
    AtTokenizerRestart(self);
    }

AtTokenizer AtTokenizerSharedTokenizer(char *aString, const char *separator)
    {
    AtTokenizer tokenizer = m_sharedTokenizerPointer;

    if (tokenizer)
        {
        AtTokenizerStringSet(tokenizer, aString);
        AtTokenizerSeparatorSet(tokenizer, separator);
        AtTokenizerRestart(tokenizer);
        return tokenizer;
        }

    m_sharedTokenizerPointer = &m_sharedTokenizer;
    return ObjectInit(m_sharedTokenizerPointer, aString, separator);
    }

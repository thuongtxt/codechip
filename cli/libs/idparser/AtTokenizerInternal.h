/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtTokenizerInternal.h
 * 
 * Created Date: Mar 6, 2014
 *
 * Description : Parse string tokens
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTOKENIZERINTERNAL_H_
#define _ATTOKENIZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTokenizer
    {
    tAtObject super;

    /* Private data */
    char *stringBuffer;
    const char *separator;
    uint32 stringLength;

    /* To iterate */
    char *currentString;
    uint32 numSubStrings;
    uint32 currentStringIndex;
    }tAtTokenizer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATTOKENIZERINTERNAL_H_ */


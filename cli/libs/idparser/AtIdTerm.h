/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ID parser
 * 
 * File        : AtIdTerm.h
 * 
 * Created Date: Nov 13, 2013
 *
 * Description : ID term
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIDTERM_H_
#define _ATIDTERM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtIdParserInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIdTermBasic AtIdTermBasicNew(const char *idTermString, AtIdParser parser);

eBool AtIdTermIsValid(AtIdTerm self);
uint32 AtIdTermRestart(AtIdTerm self);
uint32 AtIdTermNextNumber(AtIdTerm self);
uint32 AtIdTermNumNumbers(AtIdTerm self);
eBool AtIdTermHasNextNumber(AtIdTerm self);
uint32 AtIdTermNumLevel(AtIdTerm self);

void AtIdTermDebug(AtIdTerm self);

/* Basic term methods */
eBool AtIdTermBasicIsLessThan(AtIdTermBasic self, AtIdTermBasic otherTerm);
eBool AtIdTermBasicIsGreaterThan(AtIdTermBasic self, AtIdTermBasic otherTerm);
uint32 AtIdTermBasicNumberAtIndex(AtIdTermBasic self, uint32 numberIndex);
AtIdTermBasic AtIdTermBasicIncrease(AtIdTermBasic self);

#ifdef __cplusplus
}
#endif
#endif /* _ATIDTERM_H_ */


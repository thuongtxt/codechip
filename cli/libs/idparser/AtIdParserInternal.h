/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ID parser
 * 
 * File        : AtIdParserInternal.h
 * 
 * Created Date: Nov 14, 2013
 *
 * Description : ID parser
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIDPARSERINTERNAL_H_
#define _ATIDPARSERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIdParser.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIdTerm * AtIdTerm;
typedef struct tAtIdTermBasic * AtIdTermBasic;
typedef struct tAtIdRange  * AtIdRange;

typedef struct tAtIdParser
    {
    tAtObject super;

    /* Input */
    AtIdTermBasic minFormat;
    AtIdTermBasic maxFormat;

    /* Data */
    AtList idTerms;
    AtIdTerm currentIdTerm;
    uint32 currentIdTermIndex;
    uint32 numNumbers;
    }tAtIdParser;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIdTermBasic AtIdParserMinFormat(AtIdParser self);
AtIdTermBasic AtIdParserMaxFormat(AtIdParser self);

/* Util */
uint32 AtIdParserNumCharInString(char *aString, char aChar);
uint32 *AtIdParserIdTermStringNumbers(char *idTerm, uint32 *numNumbers);

#ifdef __cplusplus
}
#endif
#endif /* _ATIDPARSERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtIdParserTest.c
 *
 * Created Date: Nov 11, 2013
 *
 * Description : ID parser unittest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtIdParser.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void checkIds(AtIdParser parser, uint32 *expectedIds, uint32 count)
    {
    uint32 number_i = 0;

    AtAssert(AtIdParserHasNextNumber(parser));
    AtAssert(AtIdParserNumNumbers(parser) == count);
    while (AtIdParserHasNextNumber(parser))
        {
        uint32 nextNumber = AtIdParserNextNumber(parser);
        AtAssert(nextNumber == expectedIds[number_i]);
        number_i = number_i + 1;
        }
    }

static void helper_testParseIds(AtIdParser parser, uint32 *expectedIds, uint32 count)
    {
    if (parser == NULL)
        return;

    checkIds(parser, expectedIds, count);

    /* Can restart parsing */
    AtAssert(AtIdParserRestart(parser) == count);
    checkIds(parser, expectedIds, count);
    AtObjectDelete((AtObject)parser);
    }

static void testParseOneLevelId_NoMaxAndMin(void)
    {
    char id[] = "1,5-10";
    AtIdParser parser = AtIdParserNew(id, NULL, NULL);
    uint32 expectedIds[] = {1,5,6,7,8,9,10};
    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId_OneId(void)
    {
    AtIdParser parser = AtIdParserNew("5", "1", "9");
    uint32 expectedIds[] = {5};
    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId_MultiIds_NoRange(void)
    {
    AtIdParser parser = AtIdParserNew("1,3,9", "1", "9");
    uint32 expectedIds[] = {1, 3, 9};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId_MultiIds_OneRange(void)
    {
    AtIdParser parser = AtIdParserNew("1-4", "1", "9");
    uint32 expectedIds[] = {1, 2, 3, 4};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId_MultiIds_OneRange_Full_ID(void)
    {
    AtIdParser parser = AtIdParserNew("1-9", "1", "9");
    uint32 expectedIds[] = {1,2,3,4,5,6,7,8,9};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId_MultiIds_OneRange_OverId(void)
    {
    AtIdParser parser = AtIdParserNew("1-3", "1", "2");
    uint32 expectedIds[] = {1, 2};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId_MultiIds(void)
    {
    AtIdParser parser = AtIdParserNew("1,3,5-9", "1", "9");
    uint32 expectedIds[] = {1, 3, 5, 6, 7, 8, 9};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseOneLevelId(void)
    {
    testParseOneLevelId_OneId();
    testParseOneLevelId_MultiIds_OneRange_OverId();
    testParseOneLevelId_MultiIds_NoRange();
    testParseOneLevelId_MultiIds_OneRange();
    testParseOneLevelId_MultiIds_OneRange_Full_ID();
    testParseOneLevelId_MultiIds();
    testParseOneLevelId_NoMaxAndMin();
    }

static void testParseTwoLevelsId_OneId(void)
    {
    AtIdParser parser = AtIdParserNew("1.3", "1.1", "9.9");
    uint32 expectedIds[] = {1, 3};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseTwoLevelsId_MultiIds_NoRange(void)
    {
    AtIdParser parser = AtIdParserNew("1.3, 2.4, 7.9", "1.1", "9.9");
    uint32 expectedIds[] = {1,3, 2,4, 7,9};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseTwoLevelsId_MultiIds_NoRange_NoFormat(void)
    {
    AtIdParser parser = AtIdParserNew("1.3,2.4,7.9", NULL, NULL);
    uint32 expectedIds[] = {1,3, 2,4, 7,9};
    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseTwoLevelsId_MultiIds_OneRange(void)
    {
    AtIdParser parser = AtIdParserNew("1.3-2.4", "1.1", "5.5");
    uint32 expectedIds[] = {1,3, 1,4, 1,5, 2,1, 2,2, 2,3, 2,4};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseTwoLevelsId_MultiIds_OneRange_NoFormat(void)
    {
    AtIdParser parser = AtIdParserNew("1.3-2.4", NULL, NULL);
    AtAssert(AtIdParserNumNumbers(parser) == 0);
    AtObjectDelete((AtObject)parser);
    }

static void testParseTwoLevelsId_MultiIds_OneRange_1(void)
    {
    AtIdParser parser = AtIdParserNew("1.3-2.1", "1.1", "9.9");
    uint32 expectedIds[] = {1,3 , 1,4, 1,5, 1,6, 1,7, 1,8, 1,9, 2,1};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseTwoLevelsId_MultiIds(void)
    {
    AtIdParser parser = AtIdParserNew("1.1,1.3-2.1,2.3-2.7", "1.1", "9.9");
    uint32 expectedIds[] = {1,1 , 1,3 , 1,4, 1,5, 1,6, 1,7, 1,8, 1,9, 2,1, 2,3, 2,4, 2,5, 2,6, 2,7};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseTwoLevelsId(void)
    {
    testParseTwoLevelsId_OneId();
    testParseTwoLevelsId_MultiIds_NoRange();
    testParseTwoLevelsId_MultiIds_NoRange_NoFormat();
    testParseTwoLevelsId_MultiIds_OneRange();
    testParseTwoLevelsId_MultiIds_OneRange_1();
    testParseTwoLevelsId_MultiIds();
    testParseTwoLevelsId_MultiIds_OneRange_NoFormat();
    }

static void testParseFiveLevelsId_OneId(void)
    {
    AtIdParser parser = AtIdParserNew("2.3.2.5.1", "1.1.1.1.1", "8.3.3.7.3");
    uint32 expectedIds[] = {2,3,2,5,1};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseFiveLevelsId_MultiIds_NoRange(void)
    {
    AtIdParser parser = AtIdParserNew("1.1.1.1.1,2.3.2.6.1,1.2.3.4.2,8.3.3.7.3", "1.1.1.1.1", "8.3.3.7.3");
    uint32 expectedIds[] = {1,1,1,1,1,
                            2,3,2,6,1,
                            1,2,3,4,2,
                            8,3,3,7,3};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseFiveLevelsId_MultiIds_OneSimpleRange(void)
    {
    AtIdParser parser = AtIdParserNew("1.2.3.2.2-1.2.3.4.3", "1.1.1.1.1", "8.3.3.7.3");
    uint32 expectedIds[] = {1,2,3,2,2,
                            1,2,3,2,3,

                            1,2,3,3,1,
                            1,2,3,3,2,
                            1,2,3,3,3,

                            1,2,3,4,1,
                            1,2,3,4,2,
                            1,2,3,4,3};
    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseFiveLevelsId_MultiIds_OneRange(void)
    {
    AtIdParser parser = AtIdParserNew("1.2.3.4.3-2.2.1.6.3", "1.1.1.1.1", "8.3.3.7.3");
    uint32 expectedIds[] = {1,2,3,4,3,
                            1,2,3,5,1,
                            1,2,3,5,2,
                            1,2,3,5,3,
                            1,2,3,6,1,
                            1,2,3,6,2,
                            1,2,3,6,3,
                            1,2,3,7,1,
                            1,2,3,7,2,
                            1,2,3,7,3,

                            1,3,1,1,1,
                            1,3,1,1,2,
                            1,3,1,1,3,
                            1,3,1,2,1,
                            1,3,1,2,2,
                            1,3,1,2,3,
                            1,3,1,3,1,
                            1,3,1,3,2,
                            1,3,1,3,3,
                            1,3,1,4,1,
                            1,3,1,4,2,
                            1,3,1,4,3,
                            1,3,1,5,1,
                            1,3,1,5,2,
                            1,3,1,5,3,
                            1,3,1,6,1,
                            1,3,1,6,2,
                            1,3,1,6,3,
                            1,3,1,7,1,
                            1,3,1,7,2,
                            1,3,1,7,3,

                            1,3,2,1,1,
                            1,3,2,1,2,
                            1,3,2,1,3,
                            1,3,2,2,1,
                            1,3,2,2,2,
                            1,3,2,2,3,
                            1,3,2,3,1,
                            1,3,2,3,2,
                            1,3,2,3,3,
                            1,3,2,4,1,
                            1,3,2,4,2,
                            1,3,2,4,3,
                            1,3,2,5,1,
                            1,3,2,5,2,
                            1,3,2,5,3,
                            1,3,2,6,1,
                            1,3,2,6,2,
                            1,3,2,6,3,
                            1,3,2,7,1,
                            1,3,2,7,2,
                            1,3,2,7,3,

                            1,3,3,1,1,
                            1,3,3,1,2,
                            1,3,3,1,3,
                            1,3,3,2,1,
                            1,3,3,2,2,
                            1,3,3,2,3,
                            1,3,3,3,1,
                            1,3,3,3,2,
                            1,3,3,3,3,
                            1,3,3,4,1,
                            1,3,3,4,2,
                            1,3,3,4,3,
                            1,3,3,5,1,
                            1,3,3,5,2,
                            1,3,3,5,3,
                            1,3,3,6,1,
                            1,3,3,6,2,
                            1,3,3,6,3,
                            1,3,3,7,1,
                            1,3,3,7,2,
                            1,3,3,7,3,

                            2,1,1,1,1,
                            2,1,1,1,2,
                            2,1,1,1,3,
                            2,1,1,2,1,
                            2,1,1,2,2,
                            2,1,1,2,3,
                            2,1,1,3,1,
                            2,1,1,3,2,
                            2,1,1,3,3,
                            2,1,1,4,1,
                            2,1,1,4,2,
                            2,1,1,4,3,
                            2,1,1,5,1,
                            2,1,1,5,2,
                            2,1,1,5,3,
                            2,1,1,6,1,
                            2,1,1,6,2,
                            2,1,1,6,3,
                            2,1,1,7,1,
                            2,1,1,7,2,
                            2,1,1,7,3,

                            2,1,2,1,1,
                            2,1,2,1,2,
                            2,1,2,1,3,
                            2,1,2,2,1,
                            2,1,2,2,2,
                            2,1,2,2,3,
                            2,1,2,3,1,
                            2,1,2,3,2,
                            2,1,2,3,3,
                            2,1,2,4,1,
                            2,1,2,4,2,
                            2,1,2,4,3,
                            2,1,2,5,1,
                            2,1,2,5,2,
                            2,1,2,5,3,
                            2,1,2,6,1,
                            2,1,2,6,2,
                            2,1,2,6,3,
                            2,1,2,7,1,
                            2,1,2,7,2,
                            2,1,2,7,3,

                            2,1,3,1,1,
                            2,1,3,1,2,
                            2,1,3,1,3,
                            2,1,3,2,1,
                            2,1,3,2,2,
                            2,1,3,2,3,
                            2,1,3,3,1,
                            2,1,3,3,2,
                            2,1,3,3,3,
                            2,1,3,4,1,
                            2,1,3,4,2,
                            2,1,3,4,3,
                            2,1,3,5,1,
                            2,1,3,5,2,
                            2,1,3,5,3,
                            2,1,3,6,1,
                            2,1,3,6,2,
                            2,1,3,6,3,
                            2,1,3,7,1,
                            2,1,3,7,2,
                            2,1,3,7,3,

                            2,2,1,1,1,
                            2,2,1,1,2,
                            2,2,1,1,3,
                            2,2,1,2,1,
                            2,2,1,2,2,
                            2,2,1,2,3,
                            2,2,1,3,1,
                            2,2,1,3,2,
                            2,2,1,3,3,
                            2,2,1,4,1,
                            2,2,1,4,2,
                            2,2,1,4,3,
                            2,2,1,5,1,
                            2,2,1,5,2,
                            2,2,1,5,3,
                            2,2,1,6,1,
                            2,2,1,6,2,
                            2,2,1,6,3};

    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseFiveLevelsId_MultiIds_OneRange_2(void)
    {
    AtIdParser parser = AtIdParserNew("1.1.3.7.2-2.1.1.1.1", "1.1.1.1.1", "8.4.3.7.4");
    uint32 expectedIds[] = {1,1,3,7,2,
                            1,1,3,7,3,
                            1,1,3,7,4,

                            1,2,1,1,1,
                            1,2,1,1,2,
                            1,2,1,1,3,
                            1,2,1,1,4,
                            1,2,1,2,1,
                            1,2,1,2,2,
                            1,2,1,2,3,
                            1,2,1,2,4,
                            1,2,1,3,1,
                            1,2,1,3,2,
                            1,2,1,3,3,
                            1,2,1,3,4,
                            1,2,1,4,1,
                            1,2,1,4,2,
                            1,2,1,4,3,
                            1,2,1,4,4,
                            1,2,1,5,1,
                            1,2,1,5,2,
                            1,2,1,5,3,
                            1,2,1,5,4,
                            1,2,1,6,1,
                            1,2,1,6,2,
                            1,2,1,6,3,
                            1,2,1,6,4,
                            1,2,1,7,1,
                            1,2,1,7,2,
                            1,2,1,7,3,
                            1,2,1,7,4,

                            1,2,2,1,1,
                            1,2,2,1,2,
                            1,2,2,1,3,
                            1,2,2,1,4,
                            1,2,2,2,1,
                            1,2,2,2,2,
                            1,2,2,2,3,
                            1,2,2,2,4,
                            1,2,2,3,1,
                            1,2,2,3,2,
                            1,2,2,3,3,
                            1,2,2,3,4,
                            1,2,2,4,1,
                            1,2,2,4,2,
                            1,2,2,4,3,
                            1,2,2,4,4,
                            1,2,2,5,1,
                            1,2,2,5,2,
                            1,2,2,5,3,
                            1,2,2,5,4,
                            1,2,2,6,1,
                            1,2,2,6,2,
                            1,2,2,6,3,
                            1,2,2,6,4,
                            1,2,2,7,1,
                            1,2,2,7,2,
                            1,2,2,7,3,
                            1,2,2,7,4,

                            1,2,3,1,1,
                            1,2,3,1,2,
                            1,2,3,1,3,
                            1,2,3,1,4,
                            1,2,3,2,1,
                            1,2,3,2,2,
                            1,2,3,2,3,
                            1,2,3,2,4,
                            1,2,3,3,1,
                            1,2,3,3,2,
                            1,2,3,3,3,
                            1,2,3,3,4,
                            1,2,3,4,1,
                            1,2,3,4,2,
                            1,2,3,4,3,
                            1,2,3,4,4,
                            1,2,3,5,1,
                            1,2,3,5,2,
                            1,2,3,5,3,
                            1,2,3,5,4,
                            1,2,3,6,1,
                            1,2,3,6,2,
                            1,2,3,6,3,
                            1,2,3,6,4,
                            1,2,3,7,1,
                            1,2,3,7,2,
                            1,2,3,7,3,
                            1,2,3,7,4,

                            1,3,1,1,1,
                            1,3,1,1,2,
                            1,3,1,1,3,
                            1,3,1,1,4,
                            1,3,1,2,1,
                            1,3,1,2,2,
                            1,3,1,2,3,
                            1,3,1,2,4,
                            1,3,1,3,1,
                            1,3,1,3,2,
                            1,3,1,3,3,
                            1,3,1,3,4,
                            1,3,1,4,1,
                            1,3,1,4,2,
                            1,3,1,4,3,
                            1,3,1,4,4,
                            1,3,1,5,1,
                            1,3,1,5,2,
                            1,3,1,5,3,
                            1,3,1,5,4,
                            1,3,1,6,1,
                            1,3,1,6,2,
                            1,3,1,6,3,
                            1,3,1,6,4,
                            1,3,1,7,1,
                            1,3,1,7,2,
                            1,3,1,7,3,
                            1,3,1,7,4,

                            1,3,2,1,1,
                            1,3,2,1,2,
                            1,3,2,1,3,
                            1,3,2,1,4,
                            1,3,2,2,1,
                            1,3,2,2,2,
                            1,3,2,2,3,
                            1,3,2,2,4,
                            1,3,2,3,1,
                            1,3,2,3,2,
                            1,3,2,3,3,
                            1,3,2,3,4,
                            1,3,2,4,1,
                            1,3,2,4,2,
                            1,3,2,4,3,
                            1,3,2,4,4,
                            1,3,2,5,1,
                            1,3,2,5,2,
                            1,3,2,5,3,
                            1,3,2,5,4,
                            1,3,2,6,1,
                            1,3,2,6,2,
                            1,3,2,6,3,
                            1,3,2,6,4,
                            1,3,2,7,1,
                            1,3,2,7,2,
                            1,3,2,7,3,
                            1,3,2,7,4,

                            1,3,3,1,1,
                            1,3,3,1,2,
                            1,3,3,1,3,
                            1,3,3,1,4,
                            1,3,3,2,1,
                            1,3,3,2,2,
                            1,3,3,2,3,
                            1,3,3,2,4,
                            1,3,3,3,1,
                            1,3,3,3,2,
                            1,3,3,3,3,
                            1,3,3,3,4,
                            1,3,3,4,1,
                            1,3,3,4,2,
                            1,3,3,4,3,
                            1,3,3,4,4,
                            1,3,3,5,1,
                            1,3,3,5,2,
                            1,3,3,5,3,
                            1,3,3,5,4,
                            1,3,3,6,1,
                            1,3,3,6,2,
                            1,3,3,6,3,
                            1,3,3,6,4,
                            1,3,3,7,1,
                            1,3,3,7,2,
                            1,3,3,7,3,
                            1,3,3,7,4,

                            1,4,1,1,1,
                            1,4,1,1,2,
                            1,4,1,1,3,
                            1,4,1,1,4,
                            1,4,1,2,1,
                            1,4,1,2,2,
                            1,4,1,2,3,
                            1,4,1,2,4,
                            1,4,1,3,1,
                            1,4,1,3,2,
                            1,4,1,3,3,
                            1,4,1,3,4,
                            1,4,1,4,1,
                            1,4,1,4,2,
                            1,4,1,4,3,
                            1,4,1,4,4,
                            1,4,1,5,1,
                            1,4,1,5,2,
                            1,4,1,5,3,
                            1,4,1,5,4,
                            1,4,1,6,1,
                            1,4,1,6,2,
                            1,4,1,6,3,
                            1,4,1,6,4,
                            1,4,1,7,1,
                            1,4,1,7,2,
                            1,4,1,7,3,
                            1,4,1,7,4,

                            1,4,2,1,1,
                            1,4,2,1,2,
                            1,4,2,1,3,
                            1,4,2,1,4,
                            1,4,2,2,1,
                            1,4,2,2,2,
                            1,4,2,2,3,
                            1,4,2,2,4,
                            1,4,2,3,1,
                            1,4,2,3,2,
                            1,4,2,3,3,
                            1,4,2,3,4,
                            1,4,2,4,1,
                            1,4,2,4,2,
                            1,4,2,4,3,
                            1,4,2,4,4,
                            1,4,2,5,1,
                            1,4,2,5,2,
                            1,4,2,5,3,
                            1,4,2,5,4,
                            1,4,2,6,1,
                            1,4,2,6,2,
                            1,4,2,6,3,
                            1,4,2,6,4,
                            1,4,2,7,1,
                            1,4,2,7,2,
                            1,4,2,7,3,
                            1,4,2,7,4,

                            1,4,3,1,1,
                            1,4,3,1,2,
                            1,4,3,1,3,
                            1,4,3,1,4,
                            1,4,3,2,1,
                            1,4,3,2,2,
                            1,4,3,2,3,
                            1,4,3,2,4,
                            1,4,3,3,1,
                            1,4,3,3,2,
                            1,4,3,3,3,
                            1,4,3,3,4,
                            1,4,3,4,1,
                            1,4,3,4,2,
                            1,4,3,4,3,
                            1,4,3,4,4,
                            1,4,3,5,1,
                            1,4,3,5,2,
                            1,4,3,5,3,
                            1,4,3,5,4,
                            1,4,3,6,1,
                            1,4,3,6,2,
                            1,4,3,6,3,
                            1,4,3,6,4,
                            1,4,3,7,1,
                            1,4,3,7,2,
                            1,4,3,7,3,
                            1,4,3,7,4,

                            2,1,1,1,1};
    helper_testParseIds(parser, expectedIds, mCount(expectedIds));
    }

static void testParseFiveLevelsId(void)
    {
    testParseFiveLevelsId_OneId();
    testParseFiveLevelsId_MultiIds_NoRange();
    testParseFiveLevelsId_MultiIds_OneSimpleRange();
    testParseFiveLevelsId_MultiIds_OneRange();
    testParseFiveLevelsId_MultiIds_OneRange_2();
    }

static void helper_testCreateValidParser(const char *idString, const char *minFormat, const char *maxFormat)
    {
    AtIdParser parser = AtIdParserNew(idString, minFormat, maxFormat);
    AtAssert(parser);
    AtObjectDelete((AtObject)parser);
    }

static void testCreateValidParser(void)
    {
    helper_testCreateValidParser("1-4",   "1", "3");
    helper_testCreateValidParser("1,2-4", "1", "3");
    helper_testCreateValidParser("1.1.1.1.1.1-1.1.1.1.1.3", "1.1.1.1.1.1", "7.7.7.7.7.7");
    helper_testCreateValidParser("1.1.1.1.1.1,1.1.1.1.1.2-1.1.1.1.1.7", "1.1.1.1.1.1", "7.7.7.7.7.7");
    helper_testCreateValidParser("1.1.1.1.1.1,1.1.1.1.1.3", NULL, NULL);
    helper_testCreateValidParser("1.1.1.1.1.1-1.1.1.1.1.3", NULL, NULL);
    }

static void testCannotCreateMultiLevelsIdPaserWithNullIdFormat(void)
    {
    AtAssert(AtIdParserNew("1.1.1.1.1.1-1.1.1.1.1.3", NULL, "7.7.7.7.7.7") == NULL);
    AtAssert(AtIdParserNew("1.1.1.1.1.1-1.1.1.1.1.3", "1.1.1.1.1.1", NULL) == NULL);
    }

static void testCannotCreateNotUniformIdParser(void)
    {
    AtAssert(AtIdParserNew("1.1.1.1.1.1-1.1.1.1.1",   "1.1.1.1.1.1", "1.2.3.4.5.6") == NULL);
    AtAssert(AtIdParserNew("1.1.1.1.1-1.1.1.1.2",     "1.1.1.1.1.1", "1.2.3.4.5.6") == NULL);
    AtAssert(AtIdParserNew("1.1.1.1.1.1-1.1.1.1.1.3", "1.1.1.1.1",   "1.2.3.4.5.6") == NULL);
    AtAssert(AtIdParserNew("1.1.1.1.1.1-1.1.1.1.1.3", "1.1.1.1.1.1", "1.2.3.4.5")   == NULL);
    }

static void testCannotCreateParserThatDoesNotMatchConstrain(void)
    {
    /* One level */
    AtAssert(AtIdParserNew("1-4,3-2",   "1", "9") == NULL);

    /* Multi levels */
    AtAssert(AtIdParserNew("4.4.4.4.4.4-2.2.2.2.2.2", "1.1.1.1.1.1", "4.4.4.4.4.4") == NULL);
    AtAssert(AtIdParserNew("5.5.5.5.5.5-6.6.6.6.6.6", "1.1.1.1.1.1", "4.4.4.4.4.4") == NULL);
    AtAssert(AtIdParserNew("1.1.1.1.1.1-6.6.6.6.6.6", "2.2.2.2.2.2", "4.4.4.4.4.4") == NULL);

    AtAssert(AtIdParserNew("1.1.1.1.1.1-2.2.2.2.2.2,4.4.4.4.4.4-2.2.2.2.2.2", "1.1.1.1.1.1", "4.4.4.4.4.4") == NULL);
    AtAssert(AtIdParserNew("2.3.4.5.6", "1.1.1.1.1", "8.3.3.7.3") == NULL);
    }

static void testCannotCreateInvalidParser(void)
    {
    testCannotCreateNotUniformIdParser();
    testCannotCreateParserThatDoesNotMatchConstrain();
    testCannotCreateMultiLevelsIdPaserWithNullIdFormat();
    }

void AtIdParserTest(void)
    {
    testCannotCreateInvalidParser();
    testCreateValidParser();
    testParseOneLevelId();
    testParseTwoLevelsId();
    testParseFiveLevelsId();
    }

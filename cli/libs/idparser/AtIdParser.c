/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtIdParser.c
 *
 * Created Date: Nov 11, 2013
 *
 * Description : ID parser
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtIdTerm.h"
#include "AtIdRange.h"
#include "AtList.h"
#include "AtTokenizer.h"

#include "AtIdParserInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtIdParser)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtIdParser);
    }

static AtIdTerm IdTermCreate(AtIdParser self, char *idTermString)
    {
    if (AtStrstr(idTermString, "-"))
        return (AtIdTerm)AtIdRangeNew(idTermString, self);
    else
        return (AtIdTerm)AtIdTermBasicNew(idTermString, self);
    }

static AtList IdTermsGenerate(AtIdParser self, const char *idString)
    {
    char *dupIdString  = AtStrdup(idString);
    AtList idTerms     = AtListCreate(0);
    AtList resultTerms = idTerms;
    char *dupIdStringToDelete = dupIdString;
    AtTokenizer tokenizer = AtTokenizerNew(dupIdString, ",");

    while (AtTokenizerHasNextString(tokenizer))
        {
        AtIdTerm newTerm;

        dupIdString = AtTokenizerNextString(tokenizer);
        newTerm = IdTermCreate(self, dupIdString);
        if (AtListObjectAdd(idTerms, (AtObject)newTerm) != cAtOk)
            AtObjectDelete((AtObject)newTerm);
        }

    if (AtListLengthGet(idTerms) == 0)
        {
        AtObjectDelete((AtObject)idTerms);
        resultTerms = NULL;
        }

    self->idTerms = resultTerms;
    AtOsalMemFree(dupIdStringToDelete);
    AtObjectDelete((AtObject)tokenizer);

    return idTerms;
    }

static void SavePrivateData(AtIdParser self, const char *idString, const char *minFormat, const char *maxFormat)
    {
    self->minFormat = AtIdTermBasicNew(minFormat, self);
    self->maxFormat = AtIdTermBasicNew(maxFormat, self);
    IdTermsGenerate(self, idString);
    }

static void AllIdTermsDelete(AtIdParser self)
    {
    while (AtListLengthGet(self->idTerms))
        {
        AtObject idTerm = AtListObjectRemoveAtIndex(self->idTerms, 0);
        AtObjectDelete(idTerm);
        }
    AtObjectDelete((AtObject)self->idTerms);
    }

static eBool AllIdTermsValid(AtIdParser self)
    {
    uint32 term_i;

    for (term_i = 0; term_i < AtListLengthGet(self->idTerms); term_i++)
        {
        AtIdTerm idTerm = (AtIdTerm)AtListObjectGet(self->idTerms, term_i);
        if (!AtIdTermIsValid(idTerm))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool FormatIsValid(AtIdParser self)
    {
    AtIdTerm minFormat = (AtIdTerm)self->minFormat;
    AtIdTerm maxFormat = (AtIdTerm)self->maxFormat;

    if (AtIdTermNumNumbers(minFormat) != AtIdTermNumNumbers(maxFormat))
        return cAtFalse;

    if (!AtIdTermIsValid(minFormat) || !AtIdTermIsValid(maxFormat))
        return cAtFalse;

    if (AtIdTermBasicIsLessThan(self->maxFormat, self->minFormat))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PrivateDataIsValid(AtIdParser self)
    {
    if (!FormatIsValid(self))
        return cAtFalse;

    if (AtListLengthGet(self->idTerms) == 0)
        return cAtFalse;

    if (!AllIdTermsValid(self))
        return cAtFalse;

    return cAtTrue;
    }

static void Delete(AtObject self)
    {
    AtIdParser parser = mThis(self);

    if (self == NULL)
        return;

    /* Destroy private data */
    AtObjectDelete((AtObject)parser->minFormat);
    AtObjectDelete((AtObject)parser->maxFormat);

    AllIdTermsDelete(parser);

    /* Destroy itself */
    m_AtObjectMethods->Delete(self);
    }

static AtIdTerm NextIdTerm(AtIdParser self)
    {
    /* The first term */
    if (self->currentIdTerm == NULL)
        {
        self->currentIdTerm      = (AtIdTerm)AtListObjectGet(self->idTerms, 0);
        self->currentIdTermIndex = 0;
        return self->currentIdTerm;
        }

    /* Next term */
    self->currentIdTermIndex = self->currentIdTermIndex + 1;
    self->currentIdTerm = (AtIdTerm)AtListObjectGet(self->idTerms, self->currentIdTermIndex);
    return self->currentIdTerm;
    }

static void OverrideAtObject(AtIdParser self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtIdParser self)
    {
    OverrideAtObject(self);
    }

static AtIdParser ObjectInit(AtIdParser self, const char *idString, const char *minFormat, const char *maxFormat)
    {
    /* Super constructor */
    AtOsalMemInit(self, 0, ObjectSize());
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Handle private data */
    SavePrivateData(self, idString, minFormat, maxFormat);
    if (!PrivateDataIsValid(self))
        return NULL;

    return self;
    }

AtIdParser AtIdParserNew(const char *idString, const char *minFormat, const char *maxFormat)
    {
    AtIdParser newParser = AtOsalMemAlloc(ObjectSize());
    if (newParser == NULL)
        return NULL;

    if (ObjectInit(newParser, idString, minFormat, maxFormat))
        return newParser;

    AtObjectDelete((AtObject)newParser);
    return NULL;
    }

AtIdTermBasic AtIdParserMinFormat(AtIdParser self)
    {
    return self ? self->minFormat : NULL;
    }

AtIdTermBasic AtIdParserMaxFormat(AtIdParser self)
    {
    return self ? self->maxFormat : NULL;
    }

eBool AtIdParserHasNextNumber(AtIdParser self)
    {
    if (self == NULL)
        return cAtFalse;

    if (self->currentIdTerm == NULL)
        return cAtTrue;

    /* Just need to check the last term */
    if (self->currentIdTermIndex == (AtListLengthGet(self->idTerms) - 1))
        return AtIdTermHasNextNumber(self->currentIdTerm);

    return cAtTrue;
    }

uint32 AtIdParserRestart(AtIdParser self)
    {
    uint32 term_i;
    uint32 numNumbers = 0;

    /* Restart parsing all of terms */
    for (term_i = 0; term_i < AtListLengthGet(self->idTerms); term_i++)
        {
        AtIdTerm idTerm = (AtIdTerm)AtListObjectGet(self->idTerms, term_i);
        numNumbers = numNumbers + AtIdTermRestart(idTerm);
        }

    /* Restart current term */
    self->currentIdTermIndex = 0;
    self->currentIdTerm      = (AtIdTerm)AtListObjectGet(self->idTerms, 0);
    self->numNumbers         = numNumbers;

    return numNumbers;
    }

uint32 AtIdParserNumNumbers(AtIdParser self)
    {
    uint32 term_i;

    if (self == NULL)
        return 0;

    /* Already calculate, return it */
    if (self->numNumbers > 0)
        return self->numNumbers;

    /* Calculate then cache */
    for (term_i = 0; term_i < AtListLengthGet(self->idTerms); term_i++)
        {
        AtIdTerm term = (AtIdTerm)AtListObjectGet(self->idTerms, term_i);
        self->numNumbers = self->numNumbers + AtIdTermNumNumbers(term);
        }

    return self->numNumbers;
    }

uint32 AtIdParserNextNumber(AtIdParser self)
    {
    AtIdTerm idTerm;
    uint32 nextNumber;

    idTerm = self->currentIdTerm;
    if ((idTerm == NULL) || (!AtIdTermHasNextNumber(idTerm)))
        idTerm = NextIdTerm(self);

    nextNumber = AtIdTermNextNumber(idTerm);
    return nextNumber;
    }

uint32 AtIdParserNumCharInString(char *aString, char aChar)
    {
    uint32 charCount = 0;

    while (aString[charCount])
        {
        if (aString[charCount] == aChar)
            charCount = charCount + 1;
        else
            aString = aString + 1;
        }

    return charCount;
    }

uint32 AtIdParserNumIdLevel(AtIdParser self)
    {
    if (self)
        return AtIdTermNumLevel((AtIdTerm)AtListObjectGet(self->idTerms, 0));

    return 0;
    }

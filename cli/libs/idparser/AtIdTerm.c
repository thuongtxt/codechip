/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ID parser
 *
 * File        : AtIdTerm.c
 *
 * Created Date: Nov 13, 2013
 *
 * Description : ID term
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtIdTermInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtIdTerm)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIdTermMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtIdTerm);
    }

static eBool IsValid(AtIdTerm self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static uint32 NumNumbersCalculate(AtIdTerm self)
    {
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static void Restart(AtIdTerm self)
    {
	AtUnused(self);
    }

static uint32 NextNumber(AtIdTerm self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cInvalidValue;
    }

static eBool HasNextNumber(AtIdTerm self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static void Debug(AtIdTerm self)
    {
	AtUnused(self);
    }

static uint32 NumLevel(AtIdTerm self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(AtIdTerm self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, IsValid);
        mMethodOverride(m_methods, NumNumbersCalculate);
        mMethodOverride(m_methods, NextNumber);
        mMethodOverride(m_methods, HasNextNumber);
        mMethodOverride(m_methods, Restart);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, NumLevel);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtIdTerm self)
    {
	AtUnused(self);
    /* May be added in the future */
    }

AtIdTerm AtIdTermObjectInit(AtIdTerm self, const char *idTermString, AtIdParser parser)
    {
	AtUnused(idTermString);
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->idParser = parser;

    return self;
    }

eBool AtIdTermIsValid(AtIdTerm self)
    {
    if (self)
        return mMethodsGet(self)->IsValid(self);
    return cAtFalse;
    }

AtIdParser AtIdTermIdParser(AtIdTerm self)
    {
    return self ? self->idParser : NULL;
    }

uint32 AtIdTermRestart(AtIdTerm self)
    {
    mMethodsGet(self)->Restart(self);
    return AtIdTermNumNumbers(self);
    }

uint32 AtIdTermNumNumbers(AtIdTerm self)
    {
    if (self == NULL)
        return 0;

    if (self->numNumbers == 0)
        self->numNumbers = mMethodsGet(self)->NumNumbersCalculate(self);

    return self->numNumbers;
    }

uint32 AtIdTermNextNumber(AtIdTerm self)
    {
    if (self)
        return mMethodsGet(self)->NextNumber(self);
    return 0;
    }

eBool AtIdTermHasNextNumber(AtIdTerm self)
    {
    if (self)
        return mMethodsGet(self)->HasNextNumber(self);
    return cAtFalse;
    }

void AtIdTermDebug(AtIdTerm self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

void AtIdTermNumNumbersSet(AtIdTerm self, uint32 numNumbers)
    {
    if (self)
        self->numNumbers = numNumbers;
    }

uint32 AtIdTermNumNumbersGet(AtIdTerm self)
    {
    if (self)
        return self->numNumbers;

    return 0;
    }

uint32 AtIdTermNumLevel(AtIdTerm self)
    {
    if (self)
        return mMethodsGet(self)->NumLevel(self);

    return 0;
    }

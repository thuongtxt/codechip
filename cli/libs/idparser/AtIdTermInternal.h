/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ID parser
 * 
 * File        : AtIdTermInternal.h
 * 
 * Created Date: Nov 13, 2013
 *
 * Description : ID term
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIDTERMINTERNAL_H_
#define _ATIDTERMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIdTerm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIdTermMethods
    {
    eBool (*IsValid)(AtIdTerm self);
    uint32 (*NumNumbersCalculate)(AtIdTerm self);
    uint32 (*NextNumber)(AtIdTerm self);
    eBool (*HasNextNumber)(AtIdTerm self);
    uint32 (*NumLevel)(AtIdTerm self);
    void (*Restart)(AtIdTerm self);
    void (*Debug)(AtIdTerm self);
    }tAtIdTermMethods;

typedef struct tAtIdTerm
    {
    tAtObject super;
    const tAtIdTermMethods *methods;

    /* Private data */
    AtIdParser idParser;
    uint32 numNumbers;
    }tAtIdTerm;

typedef struct tAtIdTermBasic
    {
    tAtIdTerm super;

    /* Private data */
    uint32 *numbers;
    uint32 currentNumberIndex;
    }tAtIdTermBasic;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIdTerm AtIdTermObjectInit(AtIdTerm self, const char *idTermString, AtIdParser parser);

AtIdParser AtIdTermIdParser(AtIdTerm self);

void AtIdTermNumNumbersSet(AtIdTerm self, uint32 numNumbers);
uint32 AtIdTermNumNumbersGet(AtIdTerm self);

#ifdef __cplusplus
}
#endif
#endif /* _ATIDTERMINTERNAL_H_ */


/*-----------------------------------------------------------------------------
*
* COPYRIGHT (C) 2006 Arrive Technologies Inc.
*
* The information contained herein is confidential property of Arrive Tecnologies. 
* The use, copying, transfer or disclosure of such information 
* is prohibited except by express written agreement with Arrive Technologies.
*
* Module      : showtable
*
* File        : showtable.h
*
* Created Date: 4-May-06
*
* Description : This module implements table printing
*
* Notes       : None
*----------------------------------------------------------------------------*/

/*---------------------------- Description -----------------------------------
* This header file contains all the data definitions related to Show Table task
*----------------------------------------------------------------------------*/

#ifndef _STDTABLE_HEADER_
#define _STDTABLE_HEADER_

typedef enum eTabDisplayMode
    {
    cTabDisplayModeStandard, /* Standard display mode with visual displayed */
    cTabDisplayModeXml       /* Table is displayed with XML format */
    }eTabDisplayMode;

typedef struct  tStdTabCell
	{
	char*		str;
	uint8		colorCode;
	} tStdTabCell;

typedef struct tStdTab 
	{
	tStdTabCell**		cell;
	tStdTabCell*		tittle;
	uint32				rowCount;
	uint32				colCount;
	}tStdTab;

eBool StdTableInit(tStdTab* pTab, uint32 rowCount, uint32 colCount);
void StdTableFree(tStdTab* pTab);
void StdTablePrint(tStdTab* pTab, eBool printTitle);
void StdTableSubTablePrint(tStdTab* pTab,
                           uint32 rowFrom,
                           uint32 rowTo,
                           uint32 colFrom,
                           uint32 colTo,
                           eBool printTitle);
eBool StdTableStrAdd(tStdTab* pTab, uint32 row, uint32 col, char* str);
eBool StdTableStrToCell(tStdTabCell* pCell, const char* str);
eBool StdTableColorStrToCell(tStdTabCell* pCell, char* str, uint8 color);
eBool StdTableToString(tStdTab* pTab, eBool printTitle, char* pTableStr);
void StdTableDisplayModeSet(eTabDisplayMode mode);
eTabDisplayMode StdTableDisplayModeGet(void);

#endif /* _STDTABLE_H_ */


/*-----------------------------------------------------------------------------
*
* COPYRIGHT (C) 2006 Arrive Technologies Inc.
*
* The information contained herein is confidential property of Arrive Tecnologies. 
* The use, copying, transfer or disclosure of such information 
* is prohibited except by express written agreement with Arrive Technologies.
*
* Module      : showtable
*
* File        : showtable.h
*
* Created Date: 4-May-06
*
* Description : This module implements table printing
*
* Notes       : None
*----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifndef _SHOWTABLE_HEADER_
#define _SHOWTABLE_HEADER_

/* Maximum colum number for each table */
#define cShowTableMaxColNum 11 * 1024

/* Color code */
#define cTextNormal     0
#define cTextRed        31
#define cTextGreen      32
#define cTextYellow     33
#define cTextBlue       34
#define cTextMagenta    35
#define cTextCyan       36
#define cTextWhite      10

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTab_t tTab; 

#include "stdtable.h"

/*--------------------------- Prototypes -------------------------------------*/
tTab* TableAlloc(uint32 rowCount, uint32 colCount, const char** heading);
void  TableFree(tTab* pTab);
void  TablePrint(tTab* pTab);
void  SubTablePrint(tTab* pTab, uint32 rowFrom, uint32 rowTo, uint32 colFrom, uint32 colTo);
eBool StrToCell(tTab* pTab, uint32 row, uint32 col, const char* str);
eBool ColorStrToCell(tTab* pTab, uint32 row, uint32 col, const char* str, uint8 color);
eBool TableToString(tTab* pTab, char* pTableStr, uint32 size);
uint32 TableStringSizeGet(tTab *pTab);
char *TableColumnHeaderSet(tTab *pTab, uint32 columnId, const char *header);

eBool TableAddRow(tTab *pTab, uint32 addNum);
eBool ShowtableLibInit(void);
eBool ShowTableLibIsInitialized(void);
eBool ShowtableLibShutdown(void);

/* Access method to private data */
eBool TableSizeGet(tTab *pTab, uint32 *rowSize, uint32 *colSize);
const char* TableColumnNameGet(tTab *pTab, int colId);
int TableColumnIdGet(tTab *pTab, const char *colName);
const char* TableCellStringGet(tTab *pTab, uint32 row, uint32 col);
int TableCellColorGet(tTab *pTab, uint32 row, uint32 col);

/* To control CPU resting */
void TableNumRestLinesSet(uint32 numRestLines);
uint32 TableNumRestLinesGet(void);
void TableRestTimeMsSet(uint32 restTimeMs);
uint32 TableRestTimeMsGet(void);

void GlobalTableReset(void);

/* 
 * Register to table event
 * First customer for these functions are Showtable TCL extension
 */
#include "at_listener.h"
#define cShowtableEventNew    0x00000001U
#define cShowtableEventDelete 0x00000002U
#define cShowtableEventPrint  0x00000004U
extern int ShowtableEventRegister(AtEventListenerCallback_f event_cb, unsigned long evtMask);
/*  Unregister */
extern int ShowtableEventUnregister(AtEventListenerCallback_f event_cb, unsigned long evtMask);
#endif /* _SHOWTABLE_H_ */


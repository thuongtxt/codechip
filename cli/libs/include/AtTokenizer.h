/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtTokenizer.h
 * 
 * Created Date: Mar 6, 2014
 *
 * Description : Parse string tokens
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTOKENIZER_H_
#define _ATTOKENIZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTokenizer * AtTokenizer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTokenizer AtTokenizerSharedTokenizer(char *aString, const char *separator);
AtTokenizer AtTokenizerNew(char *aString, const char *separator);

/* Accessors */
void AtTokenizerStringSet(AtTokenizer self, char *aString);
void AtTokenizerSeparatorSet(AtTokenizer self, const char *separator);

/* Iterate sub strings */
uint32 AtTokenizerNumStrings(AtTokenizer self);
eBool AtTokenizerHasNextString(AtTokenizer self);
char *AtTokenizerNextString(AtTokenizer self);

void AtTokenizerRestart(AtTokenizer self);

/* Testing */
void AtTokenizerTest(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATTOKENIZER_H_ */


/*
 * =====================================================================================
 *
 *       Filename:  listener.h
 *
 *    Description:  Header files for notification chain implementation
 *
 *        Version:  1.0
 *        Created:  12/19/2011 10:47:41 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#ifndef __SDK_LISTENER_H__
#define __SDK_LISTENER_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef void(*AtEventListenerCallback_f)(unsigned long evtMask, const void *evtObj);
typedef struct AtEventListenerList_s tAtEventListenerList;

tAtEventListenerList* AtEventListenerListCreate(void);
void AtEventListenerListDestroy(tAtEventListenerList* evtNotList);
int AtEventListenerRegister(tAtEventListenerList *evtNotList, AtEventListenerCallback_f cb, unsigned long evtMask);
int AtEventListenerUnregister(tAtEventListenerList *evtNotList, AtEventListenerCallback_f cb, unsigned long evtMask);
void AtEventListenerNotify(tAtEventListenerList *evtNotList, unsigned long evtMask, const void *obj);

#ifdef __cplusplus
}
#endif
#endif /* __SDK_LISTENER_H__ */

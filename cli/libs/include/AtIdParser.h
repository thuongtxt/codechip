/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtIdParser.h
 * 
 * Created Date: Nov 11, 2013
 *
 * Description : ID parser
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIDPARSER_H_
#define _ATIDPARSER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIdParser * AtIdParser;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIdParser AtIdParserNew(const char *idString, const char *minFormat, const char *maxFormat);

/* Number iterating */
eBool AtIdParserHasNextNumber(AtIdParser self);
uint32 AtIdParserNumNumbers(AtIdParser self);
uint32 AtIdParserNextNumber(AtIdParser self);
uint32 AtIdParserNumIdLevel(AtIdParser self);

/* Restart parsing */
uint32 AtIdParserRestart(AtIdParser self);

/* Testing */
void AtIdParserTest(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATIDPARSER_H_ */


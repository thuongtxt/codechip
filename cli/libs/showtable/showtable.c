/*-----------------------------------------------------------------------------
*
* COPYRIGHT (C) 2006 Arrive Technologies Inc.
*
* The information contained herein is confidential property of Arrive Tecnologies.
* The use, copying, transfer or disclosure of such information
* is prohibited except by express written agreement with Arrive Technologies.
*
* Module      : showtable
*
* File        : showtable.c
*
* Created Date: 4-May-06
*
* Description : This module expresses the way to use showtable
*
* Notes       : In the case  your number of colum > cShowTableTabColMax(20)
                or your number of row > cShowTableTabRowMax(100)
                or sum of length of all colums > 179, cannot print table.
*----------------------------------------------------------------------------*/



/*--------------------------------Includes------------------------------------*/
#include "attypes.h"
#include "AtOsal.h"
#include "atclib.h"
#include "atcrc32.h"
#include "showtable.h"
#include "AtCliModule.h"

#ifdef ENABLE_TCL_INTERP
#include "extShowtableInt.h"
#endif /* ENABLE_TCL_INTERP */

typedef struct  tTabCell_s
{
    char*       str;
    uint32      size; /* Size of memory allocated for this cell */
    uint8       colorCode;
} tTabCell;
typedef struct tRow_s 
{
    tTabCell *cellList; /*  List of cell */
    uint32 size; /* Real size of each Row */
} tRow_t;

struct tTab_t 
{
    char** heading;
    uint32*       colSize; /*  Store the real string size of each column for printing alignment */
    tRow_t*      rowList; /*  List of allocated row */
    uint32       rowCount;
    uint32       colCount;
    uint32        maxColSize; /* Maximum of column until now */
    uint32        size; /* Real number of row has been allocated */
};

static struct tTab_t *m_globalTable; /* A global table */

/*
 * List for callback hook
 */
static tAtEventListenerList *m_globalEventList;

/*--------------------------- Define -----------------------------------------*/
#define cMaxCharOfLine          1024
#define cShowTableTime2Sleep    100UL /* microseconds */
#define cPrintedStrMaxLength    4096

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_showTableLibInitialized = 0;
static uint32 m_numPrintedLinesToHaveRest = 20;
static uint32 m_restTimeMs = 50;

/*--------------------------- Forward declaration ----------------------------*/
static void  LinePrint(uint32 colCount, uint32* pColWidth, char rowChar, char colChar, char* pStr);

/*
 * Here is a list of helper function for TCL
 */
extern tTab* GlobalTableGet(void);
extern tTab* TableClone(tTab *pTab);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Implementations --------------------------------*/
tTab* GlobalTableGet(void)
{
    return(m_globalTable);
}

/* The previous version of this function use realloc, but AT OSAL do not wrap
 * this function. So it is re-implemented by using AtOsalMemAlloc and
 * AtOsalMemCpy for instead. AT OSAL can wraps that function but it is not
 * sure that realloc can be implemented on other OS */
static void *xrealloc_init(void *oldPtr, uint32 oldSize, uint32 newSize)
    {
    void *newMemory;
    uint32 sizeToCopy;

    /* Allocate and initialize new memory */
    newMemory = AtOsalMemAlloc(newSize);
    if (newMemory == NULL)
        return NULL;
    AtOsalMemInit(newMemory, 0, newSize);

    /* Copy old memory */
    sizeToCopy = (oldSize < newSize) ? oldSize : newSize;
    AtOsalMemCpy(newMemory, oldPtr, sizeToCopy);

    /* Free the old memory */
    AtOsalMemFree(oldPtr);

    return newMemory;
    }

static eBool ShouldRest(uint32 numPrintedLines)
    {
    if (TableRestTimeMsGet() == 0)
        return cAtFalse;

    if (TableNumRestLinesGet() == 0)
        return cAtFalse;

    if (numPrintedLines == TableNumRestLinesGet())
        return cAtTrue;

    return cAtFalse;
    }

static void Rest(void)
    {
    AtOsalUSleep(m_restTimeMs * 1000);
    }

/**
 * @brief Get column name from column id
 *
 * @param pTab
 * @param colId
 *
 * @return Column name or NULL
 */
const char* TableColumnNameGet(tTab *pTab, int colId)
{
    if(!pTab || colId < 0 || colId >= (int)pTab->colCount)
        return(NULL);
    return(pTab->heading[colId]);
}

/**
 * @brief Get column id from column name
 *
 * @param pTab
 * @param colName
 *
 * @return <0 if not exist or the value of column id
 */
int TableColumnIdGet(tTab *pTab, const char *colName)
{
    int col;

    if (!pTab || !colName)
        return(-1);
    for (col = 0; col < (int)pTab->colCount; col++){
        if (!AtStrcmp(colName, pTab->heading[col]))
            return(col);
    }
    return(-1);
}

/**
 * @brief Get string value from a cell
 *
 * @param pTab
 * @param row
 * @param col
 *
 * @return 
 */
const char* TableCellStringGet(tTab *pTab, uint32 row, uint32 col)
{
    if (!pTab || row >= pTab->rowCount || col >= pTab->colCount)
        return(NULL);
    return(pTab->rowList[row].cellList[col].str);
}

/**
 * @brief Get colour value from a cell
 *
 * @param pTab
 * @param row
 * @param col
 *
 * @return 
 */
int TableCellColorGet(tTab *pTab, uint32 row, uint32 col)
{
    if (!pTab || row >= pTab->rowCount || col >= pTab->colCount)
        return(-1);
    return(pTab->rowList[row].cellList[col].colorCode);
}

eBool ShowtableLibInit(void)
{
    /* Re-initialize */
    if (m_showTableLibInitialized)
        ShowtableLibShutdown();

    /* There is nothing to do with this */
    m_globalTable = AtOsalMemAlloc(sizeof(*m_globalTable));
    AtOsalMemInit(m_globalTable, 0, sizeof(*m_globalTable));
    if (m_globalTable == NULL)
        return cAtFalse;
    m_globalEventList = AtEventListenerListCreate();
    if (m_globalEventList == NULL)
        return cAtFalse;

    m_showTableLibInitialized = 1;

    return cAtTrue;
}

static void InternalTableDestroy(tTab *pTab)
{
    uint32 row;
    uint32 col;
    uint32 max_colsize = 0;

    if (!pTab)
        return;
    if (pTab->rowList) {
        for (row = 0; row < pTab->size; row++)
        {
            if (pTab->rowList[row].cellList) {
                for (col = 0; col < pTab->rowList[row].size; col++)
                {
                    AtOsalMemFree(pTab->rowList[row].cellList[col].str); 
                }
                if (max_colsize < pTab->rowList[row].size){
                    max_colsize = pTab->rowList[row].size;
                }
                AtOsalMemFree(pTab->rowList[row].cellList);
            }
        }
        AtOsalMemFree(pTab->rowList);
    }
    if (pTab->heading) {
        for (col = 0; col < pTab->maxColSize; col++){
            AtOsalMemFree(pTab->heading[col]);
        }
        AtOsalMemFree(pTab->heading);
    }
    pTab->heading = NULL;
    pTab->rowList = NULL; 
    pTab->rowCount = 0;
    pTab->colCount = 0;
    pTab->maxColSize = 0;
    pTab->size = 0; 
    AtOsalMemFree(pTab->colSize);
    AtOsalMemFree(pTab);
}

eBool ShowtableLibShutdown(void)
{
    if (!m_showTableLibInitialized)
        return cAtTrue;

    InternalTableDestroy(m_globalTable);
    AtEventListenerListDestroy(m_globalEventList);
    m_globalEventList = NULL;
    m_globalTable = NULL;
    m_showTableLibInitialized = 0;

    return(cAtTrue);
}

/**
 * @brief Allocate a new table
 *
 * @param rowCount: number of row
 * @param colCount: number of count
 * @param heading: heading row for this table. User must make sure the heading has enough entry as colCount
 *
 * @return 
 */
tTab* TableAlloc(uint32 rowCount, uint32 colCount, const char** heading)
{
    uint32 row;
    uint32 col;
    tTab *pTab = m_globalTable;
    tTabCell *newColPtr; 
    char **newHeading;
    uint32 oldColSize = 0;

    if (colCount > cShowTableMaxColNum || !heading)
    {
        return NULL;
    }
    pTab->rowCount = rowCount;
    pTab->colCount = colCount;
    newHeading = pTab->heading;
    /*
     * We have only one global Table for all.
     * First, allocate list of rows 
     */
    if (rowCount > pTab->size){
        tRow_t *newListPtr = NULL;
        newListPtr = xrealloc_init(pTab->rowList, pTab->size*sizeof(*pTab->rowList), rowCount*sizeof(*pTab->rowList));
        if (!newListPtr){
            return(NULL);
        }
        pTab->size = rowCount;
        pTab->rowList = newListPtr;
    }

    /* Allocate column in each row */
    for(row = 0; row < rowCount; row++){
        if (pTab->rowList[row].size < colCount){
            newColPtr = xrealloc_init(pTab->rowList[row].cellList, pTab->rowList[row].size*sizeof(*pTab->rowList[row].cellList), 
                    colCount*sizeof(*pTab->rowList[row].cellList));
            if (!newColPtr) {
                return(NULL);
            }
            pTab->rowList[row].size = colCount;
            pTab->rowList[row].cellList = newColPtr;
        }
    }
    oldColSize = pTab->maxColSize;
    if (oldColSize < colCount) {
        uint32 *newColSize;

        newColSize = xrealloc_init(pTab->colSize, 0, sizeof(*pTab->colSize)*colCount);
        if (!newColSize)
            return(NULL);
        pTab->colSize = newColSize;

        /* Copy heading of this table */
        newHeading = xrealloc_init(newHeading, sizeof(*newHeading)*oldColSize, sizeof(*newHeading)*colCount);
        if (!newHeading)
            return(NULL);
        pTab->maxColSize = colCount;
    }
    pTab->heading = newHeading;

    /* Copy heading now */
    for (col = 0; col < colCount; col++)
    {
        for (row = 0; row < rowCount; row++)
        {
            pTab->rowList[row].cellList[col].colorCode = cTextNormal;
            if (pTab->rowList[row].cellList[col].str){
                pTab->rowList[row].cellList[col].str[0] = '\0';
            }
        }

        /* Copy */
        if (TableColumnHeaderSet(pTab, col, heading[col]) == NULL)
            return NULL;
    }
    AtEventListenerNotify(m_globalEventList, cShowtableEventNew, pTab);
    return(pTab);
}

/**
 * @brief Clone an table to another. New table is allocated
 *
 * @param pTab
 *
 * @return 
 */
tTab* TableClone(tTab *pTab)
{
#define CALLOC_HELPER(_ptr, _nmemb, _error_label)   \
    _ptr = AtOsalMemAlloc(_nmemb * sizeof(*(_ptr))); \
    if (!(_ptr)) goto _error_label;                 \
    AtOsalMemInit(_ptr, 0, _nmemb * sizeof(*(_ptr)));

#define STRDUP_HELPER(_ptr, _str, _error_label) \
    _ptr = AtStrdup(_str); \
    if (!(_ptr)) goto _error_label;

#define CELL_AT(_pTab, _row, _col) ((_pTab)->rowList[_row].cellList[_col])
#define ROW_AT(_pTab, _row) ((_pTab)->rowList[_row])
        
    tTab *cloneTabPtr;
    uint32 row;
    uint32 col;
    uint32 rowCount;
    uint32 colCount;

    if (!pTab)
        return(NULL);
    cloneTabPtr = AtOsalMemAlloc(sizeof(*cloneTabPtr));
    if (!cloneTabPtr)
        return(NULL);
    colCount = cloneTabPtr->colCount = pTab->colCount;
    cloneTabPtr->maxColSize = pTab->colCount;
    rowCount = cloneTabPtr->rowCount = pTab->rowCount;
    cloneTabPtr->size = pTab->rowCount;

    /* Clone heading */
    CALLOC_HELPER(cloneTabPtr->heading, colCount, free_tabptr);
    for(col = 0; col < colCount; col++)
    {
        STRDUP_HELPER(cloneTabPtr->heading[col], pTab->heading[col], free_heading);
    }

    /* Clone col size */
    CALLOC_HELPER(cloneTabPtr->colSize, colCount, free_heading);
    AtOsalMemCpy(cloneTabPtr->colSize, pTab->colSize, sizeof(*pTab->colSize)*colCount);

    /* Clone cell */
    CALLOC_HELPER(cloneTabPtr->rowList, rowCount, free_colsize);
    for(row = 0; row < rowCount; row++){
        CALLOC_HELPER(ROW_AT(cloneTabPtr,row).cellList, colCount, free_cell);
        ROW_AT(cloneTabPtr,row).size = colCount;
        for(col = 0; col < colCount; col++)
        {
            STRDUP_HELPER(CELL_AT(cloneTabPtr, row, col).str, CELL_AT(pTab,row, col).str, free_cell);
            CELL_AT(cloneTabPtr, row, col).size = CELL_AT(pTab, row, col).size; 
            CELL_AT(cloneTabPtr, row, col).colorCode = CELL_AT(pTab, row, col).colorCode; 
        }
    }
    return(cloneTabPtr);
free_cell:
    for(row = 0; row < rowCount; row++){
        if (ROW_AT(cloneTabPtr, row).cellList) {
            for(col = 0; col < colCount; col++)
            {
                AtOsalMemFree(CELL_AT(cloneTabPtr, row, col).str);
            }
            AtOsalMemFree(ROW_AT(cloneTabPtr, row).cellList);
        }
        else {
            break;
        }
    }
    AtOsalMemFree(cloneTabPtr->rowList);
free_colsize:
    AtOsalMemFree(cloneTabPtr->colSize);
free_heading:
    for (col = 0; col < colCount; col++){
        AtOsalMemFree(cloneTabPtr->heading[0]);
    }
    AtOsalMemFree(cloneTabPtr->heading);
free_tabptr:
    AtOsalMemFree(cloneTabPtr);
    return(NULL);
}


/**
 * @brief Put data into cell of table
 *
 * @param pTab pointer to table
 * @param row row index
 * @param col column index
 * @param str string to set
 * @param color color value to set
 *
 * @return cAtTrue or cAtFalse
 */
eBool StrToCell(tTab* pTab, uint32 row, uint32 col, const char* str)
{
    uint32 len;
    if (!pTab || !str || row >= pTab->rowCount || col >= pTab->colCount)
        return(cAtFalse);

    /* Check if pChell has existed, free it and malloc it again */
    len = AtStrlen(str) + 1;
    if(pTab->rowList[row].cellList[col].size < len){
        char *newStr;
        newStr = xrealloc_init(pTab->rowList[row].cellList[col].str, pTab->rowList[row].cellList[col].size, len);
        if (!newStr)
            return(cAtFalse);
        pTab->rowList[row].cellList[col].size = len; 
        pTab->rowList[row].cellList[col].str = newStr; 
    }
    AtStrcpy(pTab->rowList[row].cellList[col].str, str);
    if (len > pTab->colSize[col]){
        pTab->colSize[col] = len - 1;
    }
    return(cAtTrue);
}

/**
 * @brief Put data into cell of table with color
 *
 * @param pTab pointer to table
 * @param row row index
 * @param col column index
 * @param str string to set
 * @param color color value to set
 *
 * @return cAtTrue or cAtFalse
 */
eBool ColorStrToCell(tTab* pTab, uint32 row, uint32 col, const char* str, uint8 color)
{
    eBool result;
    result = StrToCell(pTab, row, col, str); 
    if (result) {
        /* Set color for cell */
        pTab->rowList[row].cellList[col].colorCode = color;
    }
    return(result);
}


static void AutotestTablePrint(tTab* pTab,
                               uint32 startRow, uint32 stopRow,
                               uint32 startCol, uint32 stopCol)
    {
    uint32 row;
    uint32 col;
    uint32 numPrintedLines = 0;

    AtPrintf("<Table name=\"SdkTablePrint\">\r\n");

    /* Second print all cells of each row */
    for (row = startRow; row <= stopRow; row++)
        {
        AtPrintf("<Row index=\"%d\"> ", row);
        for(col = startCol; col <= stopCol; col++)
            {
            AtPrintf("<Cell name=\"%s\" value=\"%s\"/>", pTab->heading[col],
                   pTab->rowList[row].cellList[col].str ? pTab->rowList[row].cellList[col].str : "" );
            }
        AtPrintf("</Row>\r\n");

        /* To give CPU a rest, if not, it will reboot because of too many print
         * actions */
        numPrintedLines = numPrintedLines + 1;
        if (ShouldRest(numPrintedLines))
            {
            Rest();
            numPrintedLines = 0;
            }
        }
    AtPrintf("</Table>\r\n");
    }

/*-----------------------------------------------------------------------------
Function Name:  TablePrint

Description  :  Print data from tTable structure.

Input        :  pTab                - pointer to tTable structure
                printTitle          - True if want to print title of table
                                      False if don't want to print title of table
Output       :  None

Return Code  :  None
------------------------------------------------------------------------------*/
void TablePrint(tTab* pTab)
    {
    if (!pTab)
        return;

    AtEventListenerNotify(m_globalEventList, cShowtableEventPrint, pTab);

    SubTablePrint(pTab, 0, pTab->rowCount - 1, 0, pTab->colCount - 1);
    }

/*-----------------------------------------------------------------------------
Function Name: SubTablePrint

Description  : Print sub table of table.

Input        : pTab             - pointer to tTable structure
                rowFrom         - row order of begin row in  table
                rowTo           - row order of end row in  table
                colFrom         - col order of begin col in  table
                colTo           - col order of end col in  table
                printTitle      - notify that print tittle or not
Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
void SubTablePrint(tTab* pTab,
                   uint32 rowFrom,
                   uint32 rowTo,
                   uint32 colFrom,
                   uint32 colTo)
{
    uint32* pColWidth;
    uint32 row;
    uint32 col;
    uint32 numPrintedLines = 0;

    if (pTab == NULL)
        return;

    if (AtCliModeGet() & cAtCliModeSilent)
        return;

    /* Check first */
    if (!pTab || rowTo >= pTab->rowCount || rowFrom > rowTo
            || colTo >= pTab->colCount || colFrom > colTo)
        return;

    if (AtCliModeGet() & cAtCliModeAutotest)
        {
        AutotestTablePrint(pTab, rowFrom, rowTo, colFrom, colTo);
        return;
        }

    /* Initiate pColWidth */
    pColWidth = pTab->colSize;
    /* Print tittle first */
    LinePrint(colTo - colFrom + 1, pColWidth, '-', '+', NULL);
    LinePrint(colTo - colFrom + 1, pColWidth, ' ', '|', NULL);
    AtPrintc(cTextNormal, "|");
    for (col = colFrom; col <= colTo; col++)
        {
        AtPrintc(cTextNormal,
                  "%-*s",
                  (int) pTab->colSize[col],
                  pTab->heading[col]);
        AtPrintc(cTextNormal, "|");
        }
    AtPrintc(cTextNormal, "\r\n");
    LinePrint(colTo - colFrom + 1, pColWidth, ' ', '|', NULL);
    /* Second print all cells of each row */
    for (row = 0; row < (rowTo - rowFrom + 1); row++)
        {
        LinePrint(colTo - colFrom + 1, pColWidth, '-', '+', NULL);
        AtPrintc(cTextNormal, "|");
        for (col = colFrom; col <= colTo; col++)
            {
            AtPrintc(pTab->rowList[row].cellList[col].colorCode,
                      "%-*s",
                      (int) pTab->colSize[col],
                      pTab->rowList[row].cellList[col].str ? pTab->rowList[row].cellList[col].str :
                                                             "");
            AtPrintc(cTextNormal, "|");
            }
        /*printf("\r\n");*/
        AtPrintc(cTextNormal, "\r\n");

        /* To give CPU a rest, if not, it will reboot because of too many print
         * actions */
        numPrintedLines = numPrintedLines + 1;
        if (ShouldRest(numPrintedLines))
            {
            Rest();
            numPrintedLines = 0;
            }
        }
    LinePrint((colTo - colFrom + 1), pColWidth, '-', '+', NULL);
    }



/*-----------------------------------------------------------------------------
Function Name: TableFree

Description  : Free the allocated memory of table.

Input        : pTab             - pointer to tTable structure

Output       : none

Return Code  : none
------------------------------------------------------------------------------*/
void TableFree(tTab* pTab)
{
    if (pTab == NULL)
        return;

    AtEventListenerNotify(m_globalEventList, cShowtableEventDelete, pTab); 
    if (pTab != m_globalTable){
        InternalTableDestroy(pTab);
    }
}

/**
 * @brief Print a horizontal line
 *
 * @param colCount number of column
 * @param pColWidth size of each column
 * @param rowChar character to printing on horizontal
 * @param colChar character to printing on vertical
 * @param pStr if this is not NULL, we will print to this string
 */
static void LinePrint(uint32 colCount, uint32* pColWidth, char rowChar, char colChar, char* pStr)
{
    uint32 col;
    char *pStrLine;
    uint32 linesize;
    uint32 size;

    /* We could get size of table because use could print a sub-table here */
    size = colCount + 4;
    for (col = 0; col < colCount; col++){
        size += pColWidth[col];
    }
    if (pStr)
        pStrLine = pStr;
    else
        pStrLine = (char *)AtOsalMemAlloc(size);
    if(pStrLine == NULL)
    {
        return;
    }
    AtOsalMemInit(pStrLine, (uint32)rowChar, size);
    pStrLine[0] = colChar;
    linesize = 1;
    for (col = 0; col < colCount; col++)
    {
        linesize += pColWidth[col];
        pStrLine[linesize] = colChar;
        linesize++;
    }
    pStrLine[linesize] = '\r';
    pStrLine[linesize + 1] = '\n';
    pStrLine[linesize + 2] = '\0';
    if (!pStr){
        AtPrintc(cTextNormal, "%s", pStrLine);
        AtOsalMemFree(pStrLine);
    }
}

static uint32 TableLinesizeGet(tTab *pTab)
{
    uint32 linesize;
    uint32 i;

    if (!pTab)
        return(0);
    linesize = pTab->colCount + 3; /* \n and + */

    for (i=0; i<pTab->colCount;i++){
        linesize += pTab->colSize[i];
    }
    return(linesize);
}

/**
 * @brief Return the size of this table will be printed to string. Not support print color here
 *
 * @param pTab
 *
 * @return 
 */
uint32 TableStringSizeGet(tTab *pTab)
{
    if (!pTab)
        return(0);
    /*
     * We need extra 4 line for heading
     * Each user row, we need an upper and under horizontal line
     * 1byte for end of C string
     * adding char \n for each line
     */
    return(TableLinesizeGet(pTab)*(pTab->rowCount*2 + 5)+1); 
}

/**
 * @brief Dump whole table to an output string. Note: we do not support print color code on this version
 *
 * @param pTab
 * @param pTableStr
 * @param size
 *
 * @return 
 */
eBool TableToString(tTab* pTab, char* pTableStr, uint32 size)
{
#define LINE_PRINT(rowChar, colChar) \
    LinePrint(pTab->colCount, pTab->colSize, rowChar, colChar, strPtr); strPtr+=linesize

#define CHAR_PRINT(ch) \
    *strPtr = ch; strPtr++

#define STR_PRINT(_str, _size) \
    AtSprintf(strPtr, "%-*s", (int)(_size), (_str));strPtr += _size

    uint32 row;
    uint32 col;
    uint32 linesize;
    char *strPtr;

    if (!pTab || size < TableStringSizeGet(pTab))
        return(cAtFalse);
    /* Calculate width of table */
    strPtr = pTableStr;
    linesize = TableLinesizeGet(pTab); 
    /* Print tittle first */
    LINE_PRINT('-', '+');
    LINE_PRINT(' ', '|');
    CHAR_PRINT('|');
    for (col = 0; col < pTab->colCount; col++)
    {
        STR_PRINT(pTab->heading[col], pTab->colSize[col]);
        CHAR_PRINT('|');
    }
    CHAR_PRINT('\r');
    CHAR_PRINT('\n');
    LINE_PRINT(' ', '|');
    /* Second print all cells of each row */
    for (row = 0; row < pTab->rowCount; row++)
    {
        LINE_PRINT('-', '+');
        CHAR_PRINT('|');
        for(col = 0; col < pTab->colCount; col++)
        {
            STR_PRINT((pTab->rowList[row].cellList[col].str ? pTab->rowList[row].cellList[col].str : ""), pTab->colSize[col]);
            CHAR_PRINT('|');
        }
        CHAR_PRINT('\r');
        CHAR_PRINT('\n');
    }
    LINE_PRINT('-', '+');
    return cAtTrue;
}

/**
 * @brief Adding new row to table
 *
 * @param pTab
 * @param addNum
 *
 * @return 
 */
eBool TableAddRow(tTab *pTab, uint32 addNum)
{
    uint32 rowCount;
    uint32 oldCount;
    uint32 colCount;
    tTabCell *newColPtr; 
    uint32 row;
    uint32 col;

    if (!pTab || addNum <= 0)
        return(cAtFalse);
    oldCount = pTab->rowCount;
    rowCount = addNum + pTab->rowCount;
    colCount = pTab->colCount;
    /*
     * We have only one global Table for all.
     * First, allocate list of rows 
     */
    if (rowCount > pTab->size){
        tRow_t *newListPtr = NULL;
        newListPtr = xrealloc_init(pTab->rowList, pTab->size*sizeof(*pTab->rowList), rowCount*sizeof(*pTab->rowList));
        if (!newListPtr){
            return(cAtFalse);
        }
        pTab->size = rowCount;
        pTab->rowList = newListPtr;
    }

    /* Allocate column in each row */
    for(row = oldCount; row < rowCount; row++){
        if (pTab->rowList[row].size < colCount){
            newColPtr = xrealloc_init(pTab->rowList[row].cellList, pTab->rowList[row].size*sizeof(*pTab->rowList[row].cellList), 
                    colCount*sizeof(*pTab->rowList[row].cellList));
            if (!newColPtr) {
                return(cAtFalse);
            }
            pTab->rowList[row].size = colCount;
            pTab->rowList[row].cellList = newColPtr;
        }
    }
    pTab->rowCount = rowCount;
    for (col = 0; col < colCount; col++)
    {
        for (row = oldCount; row < rowCount; row++)
        {
            pTab->rowList[row].cellList[col].colorCode = cTextNormal;
            if (pTab->rowList[row].cellList[col].str){
                pTab->rowList[row].cellList[col].str[0] = '\0';
            }
        }
    }
    return(cAtTrue);
}

/**
 * @brief Get size of a table (rowsize and colsize)
 *
 * @param pTab
 * @param rowSize
 * @param colSize
 */
eBool TableSizeGet(tTab *pTab, uint32 *rowSize, uint32 *colSize)
{
    if (!pTab)
        return(cAtFalse);
    if (rowSize)
        *rowSize = pTab->rowCount;
    if (colSize)
        *colSize = pTab->colCount;
    return(cAtTrue);
}

int ShowtableEventRegister(AtEventListenerCallback_f event_cb, unsigned long evtMask)
{
    return(AtEventListenerRegister(m_globalEventList, event_cb, evtMask));
}

int ShowtableEventUnregister(AtEventListenerCallback_f event_cb, unsigned long evtMask)
{
    if (m_globalEventList)
        return(AtEventListenerUnregister(m_globalEventList, event_cb, evtMask));
    else
        return 0;
}

eBool ShowTableLibIsInitialized(void)
    {
    return m_showTableLibInitialized;
    }

char *TableColumnHeaderSet(tTab *pTab, uint32 columnId, const char *header)
    {
    char *newStr;

    if (pTab == NULL)
        return NULL;

    pTab->colSize[columnId] = AtStrlen(header);
    newStr = xrealloc_init(pTab->heading[columnId], 0, pTab->colSize[columnId] + 1);
    if (!newStr)
        return(NULL);
    pTab->heading[columnId] = newStr;
    AtStrcpy(newStr, header);

    return pTab->heading[columnId];
    }

void TableNumRestLinesSet(uint32 numRestLines)
    {
    m_numPrintedLinesToHaveRest = numRestLines;
    }

uint32 TableNumRestLinesGet(void)
    {
    return m_numPrintedLinesToHaveRest;
    }

void TableRestTimeMsSet(uint32 restTimeMs)
    {
    m_restTimeMs = restTimeMs;
    }

uint32 TableRestTimeMsGet(void)
    {
    return m_restTimeMs;
    }

void GlobalTableReset(void)
    {
    if (m_globalTable == NULL)
        return;

    m_globalTable->colCount = 0;
    m_globalTable->rowCount = 0;
    }

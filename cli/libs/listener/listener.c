/*
 * =====================================================================================
 *
 *       Filename:  listener.c
 *
 *    Description:  Implementation for notification chain
 *
 *        Version:  1.0
 *        Created:  12/19/2011 10:48:12 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#include "AtOsal.h"
#include "linux_list.h"
#include "at_listener.h"

typedef struct ListenerEvt_s {
    struct list_head list;
    unsigned long evtMask;
    AtEventListenerCallback_f cb;
} ListenerEvt_t;

struct AtEventListenerList_s {
    struct list_head list;
    AtOsalMutex mutex; /* Mutex to protect this list */
};

tAtEventListenerList* AtEventListenerListCreate()
{
    tAtEventListenerList *evtList;

    evtList = AtOsalMemAlloc(sizeof(*evtList));
    if (!evtList)
        return(NULL);
    evtList->mutex = AtOsalMutexCreate();
    if (evtList->mutex == NULL)
        return NULL;
    INIT_LIST_HEAD(&evtList->list);
    return(evtList);
}

void AtEventListenerListDestroy(tAtEventListenerList* evtNotList)
{
    ListenerEvt_t *event;
    ListenerEvt_t *tmp;

    if (!evtNotList)
        return;
    list_for_each_entry_safe(event, tmp, &(evtNotList->list), list, ListenerEvt_t){
        list_del(&event->list);
        AtOsalMemFree(event);
    } 
    AtOsalMutexDestroy(evtNotList->mutex);
    AtOsalMemFree(evtNotList);
}

/**
 * @brief 
 *
 * @param evtNotList
 * @param cb
 * @param evtMask
 *
 * @return <0: error; >=0: ok
 */
int AtEventListenerRegister(tAtEventListenerList *evtNotList, AtEventListenerCallback_f cb, unsigned long evtMask)
{
    ListenerEvt_t *event;

    if (!evtNotList || !cb)
        return(-1);
    /* List for each entry */
    AtOsalMutexLock(evtNotList->mutex);
    list_for_each_entry(event, &evtNotList->list, list, ListenerEvt_t){
        if (event->cb == cb){
            event->evtMask |= evtMask;
            AtOsalMutexUnLock(evtNotList->mutex);
            return(0);
        }
    }
    AtOsalMutexUnLock(evtNotList->mutex);
    /*No register found, create a new one*/
    event = AtOsalMemAlloc(sizeof(*event));
    if (!event)
        return(0);
    INIT_LIST_HEAD(&event->list); 
    event->cb = cb;
    event->evtMask = evtMask;
    AtOsalMutexLock(evtNotList->mutex);
    list_add_tail(&event->list, &evtNotList->list);
    AtOsalMutexUnLock(evtNotList->mutex);
    return(0);

}

/**
 * @brief 
 *
 * @param evtNotList
 * @param cb
 * @param evtMask
 *
 * @return <0: error, >=0: ok
 */
int AtEventListenerUnregister(tAtEventListenerList *evtNotList, AtEventListenerCallback_f cb, unsigned long evtMask)
{
    ListenerEvt_t *event;
    int result = -1;

    if (!evtNotList || !cb)
        return(-1);
    AtOsalMutexLock(evtNotList->mutex);
    list_for_each_entry(event, &evtNotList->list, list, ListenerEvt_t)
    {
        if (event->cb == cb){
            if ((event->evtMask & evtMask) != evtMask){
                result = -1;
                goto out;
            }
            event->evtMask &= ~evtMask;
            if (!event->evtMask){
                list_del(&event->list);
                AtOsalMemFree(event);
            }
            result = 0;
            goto out;
        }
    }
out:
    AtOsalMutexUnLock(evtNotList->mutex);
    return(result);

}

void AtEventListenerNotify(tAtEventListenerList *evtNotList, unsigned long evtMask, const void *obj)
{
    ListenerEvt_t *event;

    if (!evtNotList)
        return;
    AtOsalMutexLock(evtNotList->mutex);
    list_for_each_entry(event, &evtNotList->list, list, ListenerEvt_t){
        if (evtMask & event->evtMask) {
            (event->cb)(evtMask, obj);
        }
    }
    AtOsalMutexUnLock(evtNotList->mutex);
}



/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtTcl.h
 * 
 * Created Date: Dec 25, 2013
 *
 * Description : To encapsulate TCL scripting module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTCL_H_
#define _ATTCL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTextUICmd.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTcl * AtTcl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete TCLs */
AtTcl AtDefaultTclNew(void);

/* Methods */
void AtTclDelete(AtTcl self);
void AtTclEval(AtTcl self, const char *script);
void AtTclCliRegister(AtTcl self, const tCmdConf *cmd);

#ifdef __cplusplus
}
#endif
#endif /* _ATTCL_H_ */


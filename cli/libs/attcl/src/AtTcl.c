/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtTcl.c
 *
 * Created Date: Dec 25, 2013
 *
 * Description : To encapsulate TCL scripting module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTclInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtTclMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void CliRegister(AtTcl self, const tCmdConf *cmd)
    {
	AtUnused(cmd);
	AtUnused(self);
    /* Let concrete do */
    }

static void Delete(AtTcl self)
    {
    AtOsalMemFree(self);
    }

static void Eval(AtTcl self, const char *script)
    {
	AtUnused(script);
	AtUnused(self);
    }

static eBool CliProcess(AtTcl self, const char *cli)
    {
	AtUnused(cli);
	AtUnused(self);
    /* Concrete must do */
    return cAtFalse;
    }

static void MethodsInit(AtTcl self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));
        m_methods.Delete      = Delete;
        m_methods.Eval        = Eval;
        m_methods.CliRegister = CliRegister;
        m_methods.CliProcess  = CliProcess;
        }

    self->methods = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTcl);
    }

AtTcl AtTclObjectInit(AtTcl self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtTclDelete(AtTcl self)
    {
    if (self)
        self->methods->Delete(self);
    }

void AtTclEval(AtTcl self, const char *script)
    {
    if (self)
        self->methods->Eval(self, script);
    }

void AtTclCliRegister(AtTcl self, const tCmdConf *cmd)
    {
    if (self == NULL)
        return;

    self->methods->CliRegister(self, cmd);
    }

eBool AtTclCliProcess(AtTcl self, const char *cli)
    {
    if (self)
        return self->methods->CliProcess(self, cli);
    return cAtFalse;
    }

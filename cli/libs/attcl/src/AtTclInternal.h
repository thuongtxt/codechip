/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtTclInternal.h
 * 
 * Created Date: Dec 26, 2013
 *
 * Description : AT TCL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTCLINTERNAL_H_
#define _ATTCLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtTcl.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTclMethods
    {
    void (*Delete)(AtTcl self);
    void (*CliRegister)(AtTcl self, const tCmdConf *cmd);
    eBool (*CliProcess)(AtTcl self, const char *cli);
    void (*Eval)(AtTcl self, const char *script);
    }tAtTclMethods;

typedef struct tAtTcl
    {
    const tAtTclMethods *methods;

    /* Private data */
    char sharedBuffer[4 * 1024];
    }tAtTcl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTcl AtTclObjectInit(AtTcl self);

eBool AtTclCliProcess(AtTcl self, const char *cli);

#ifdef __cplusplus
}
#endif
#endif /* _ATTCLINTERNAL_H_ */


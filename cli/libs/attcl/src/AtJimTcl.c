/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TCL
 *
 * File        : AtJimTcl.c
 *
 * Created Date: Dec 26, 2013
 *
 * Description : AT Jim TCL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtJimTcl.h"
#include "jim/jim-eventloop.h"
#include "jim/jim.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtJimTcl *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTclMethods m_AtTclOverride;

/* Save super implementation */
static const tAtTclMethods *m_AtTclMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList RegisteredClis(AtTcl self)
    {
    if (mThis(self)->clis == NULL)
        mThis(self)->clis = AtListCreate(0);
    return mThis(self)->clis;
    }

static const char *CommandName(AtTcl self, const tCmdConf *cmd)
    {
    char *firstBlank;
    uint32 numChars;

    if (cmd->level == 1)
        return cmd->cmdName;

    firstBlank = AtStrstr(cmd->cmdName, " ");
    numChars = firstBlank - cmd->cmdName;
    AtStrncpy(self->sharedBuffer, cmd->cmdName, numChars);
    self->sharedBuffer[numChars] = '\0';

    return self->sharedBuffer;
    }

static int CommandFunc(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
    {
    int i;
    AtTcl self = Jim_CmdPrivData(interp);

    AtOsalMemInit(self->sharedBuffer, 0, sizeof(self->sharedBuffer));
    for (i = 0; i < argc; i++)
        {
        AtStrcat(self->sharedBuffer, Jim_GetString(argv[i], NULL));
        AtStrcat(self->sharedBuffer, " ");
        }

    if (AtTclCliProcess(self, self->sharedBuffer))
        return JIM_OK;

    return JIM_ERR;
    }

static eBool CliRegistered(AtTcl self, const char *cmd)
    {
    uint32 i;
    AtList clis = RegisteredClis(self);

    for (i = 0; i < AtListLengthGet(clis); i++)
        {
        char *existingCmd = (char *)AtListObjectGet(clis, i);
        if (AtStrcmp(cmd, existingCmd) == 0)
            return cAtTrue;
        }

    return cAtFalse;
    }

static void CliRegister(AtTcl self, const tCmdConf *cmd)
    {
    const char *cmdName = CommandName(self, cmd);
    char *dupCmd;

    if (CliRegistered(self, cmdName))
        return;

    /* Add this command to registered command list */
    dupCmd = AtStrdup(cmdName);
    if (AtListObjectAdd(RegisteredClis(self), (AtObject)dupCmd) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for new command %s\r\n", cmd->cmdName);
        AtOsalMemFree(dupCmd);
        return;
        }

    /* Create TCL command */
    if (Jim_CreateCommand(mThis(self)->interpreter, dupCmd, CommandFunc, self, NULL) == JIM_ERR)
        {
        AtPrintc(cSevCritical, "ERROR: Register command %s fail\r\n", cmd->cmdName);
        AtListObjectRemove(RegisteredClis(self), (AtObject)dupCmd);
        AtOsalMemFree(dupCmd);
        }
    }

static Jim_Interp *InterpreterCreate(AtTcl self)
    {
    Jim_Interp *newInterpreter = Jim_CreateInterp();

    if (newInterpreter == NULL)
        return NULL;

    Jim_RegisterCoreCommands(newInterpreter);
    Jim_arrayInit(newInterpreter);
    Jim_tclprefixInit(newInterpreter);
    Jim_eventloopInit(newInterpreter);

    return newInterpreter;
    }

static void DeleledRegisteredCommands(AtTcl self)
    {
    char *cmd;

    if (mThis(self)->clis == NULL)
        return;

    while ((cmd = (char *)AtListObjectRemoveAtIndex(mThis(self)->clis, 0)) != NULL)
        AtOsalMemFree(cmd);

    AtObjectDelete((AtObject)mThis(self)->clis);
    }

static void Eval(AtTcl self, const char *script)
    {
    if (Jim_Eval(mThis(self)->interpreter, script) == JIM_ERR)
        {
        Jim_MakeErrorMessage(mThis(self)->interpreter);
        AtPrintc(cSevCritical, "%s\n", Jim_GetString(Jim_GetResult(mThis(self)->interpreter), NULL));
        }
    }

static void Delete(AtTcl self)
    {
    Jim_FreeInterp(mThis(self)->interpreter);
    DeleledRegisteredCommands(self);

    m_AtTclMethods->Delete(self);
    }

static void OverrideAtTcl(AtTcl self)
    {
    if (!m_methodsInit)
        {
        m_AtTclMethods = self->methods;
        AtOsalMemCpy(&m_AtTclOverride, m_AtTclMethods, sizeof(m_AtTclOverride));

        m_AtTclOverride.Delete      = Delete;
        m_AtTclOverride.Eval        = Eval;
        m_AtTclOverride.CliRegister = CliRegister;
        }

    self->methods = &m_AtTclOverride;
    }

static void Override(AtTcl self)
    {
    OverrideAtTcl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtJimTcl);
    }

AtTcl AtJimTclObjectInit(AtTcl self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtTclObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create interpreter */
    mThis(self)->interpreter = InterpreterCreate(self);
    if (mThis(self)->interpreter == NULL)
        return NULL;

    return self;
    }

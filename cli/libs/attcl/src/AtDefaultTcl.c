/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TCL
 *
 * File        : AtDefaultTcl.c
 *
 * Created Date: Dec 26, 2013
 *
 * Description : Default TCL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtJimTcl.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDefaultTcl
    {
    tAtJimTcl super;
    }tAtDefaultTcl;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTclMethods m_AtTclOverride;

/* Save super implementation */
static const tAtTclMethods *m_AtTclMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CliProcess(AtTcl self, const char *cli)
    {
    return AtTextUICmdProcess(AtCliSharedTextUI(), cli);
    }

static void OverrideAtTcl(AtTcl self)
    {
    if (!m_methodsInit)
        {
        m_AtTclMethods = self->methods;
        AtOsalMemCpy(&m_AtTclOverride, m_AtTclMethods, sizeof(m_AtTclOverride));

        m_AtTclOverride.CliProcess = CliProcess;
        }

    self->methods = &m_AtTclOverride;
    }

static void Override(AtTcl self)
    {
    OverrideAtTcl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultTcl);
    }

static AtTcl ObjectInit(AtTcl self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtJimTclObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTcl AtDefaultTclNew(void)
    {
    AtTcl newTcl = AtOsalMemAlloc(ObjectSize());
    if (newTcl == NULL)
        return NULL;

    return ObjectInit(newTcl);
    }

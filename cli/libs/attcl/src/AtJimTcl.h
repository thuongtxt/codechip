/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TCL
 * 
 * File        : AtJimTcl.h
 * 
 * Created Date: Dec 26, 2013
 *
 * Description : AT Jim TCL
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATJIMTCL_H_
#define _ATJIMTCL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTclInternal.h"
#include "jim/jim.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtJimTcl
    {
    tAtTcl super;

    /* Private data */
    Jim_Interp *interpreter;
    AtList clis;
    }tAtJimTcl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTcl AtJimTclObjectInit(AtTcl self);

#ifdef __cplusplus
}
#endif
#endif /* _ATJIMTCL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDevice.c
 *
 * Created Date: Nov 11, 2012
 *
 * Description : AtDevice CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <assert.h>

#include "AtOsal.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "AtShortRegisterMonitor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
#define cBitStrMask          "bit"
#define cHexMask             "0x"
#define cMaskStrMaxLen         16
#define cMaskStrMinLen         4
#define cShortMaskSizes        32
#define cRegisterMaxLength     1
#define cShortMaskSizes        32
static uint32 g_register_mask[cRegisterMaxLength];

/*--------------------------- Global variables -------------------------------*/
static tAtShortRegisterMon *shortRegisterMon = NULL;
static uint32 register_monitor_duration;
static uint32 register_monitor_numberRepeat;
static uint32 register_monitor_counters;
static uint32 register_monitor_fail_counters;
static uint32 register_monitor_ticks;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
uint32 RegisterMonPerformPeriodicProcess(uint32 periodInMs);
void ShortRegiserMonDestroy(void);

/*--------------------------- Implementation ---------------------------------*/
static void GlobalMaskReset(void)
    {
    uint8 counter_i;
    /* Clear Global Masks */
    for (counter_i = 0; counter_i < cRegisterMaxLength; counter_i++)
        g_register_mask[counter_i] = 0x0;
    }

static const char *CliRegisterStatusToString(eBool value)
    {
    return value ? "running" : "not running";
    }

static eBool ValidHexaMask(char *pMaskStr)
    {
    char strTmp[8];
    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, pMaskStr, 2);
	return (AtStrcmp(strTmp, cHexMask) == 0);
    }

static eBool ValidStringMask(char *pMaskStr)
    {
    char strTmp[8];
    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, pMaskStr, 3);
    if((AtStrcmp(strTmp, cBitStrMask) != 0) ||
       (AtStrlen(pMaskStr) < cMaskStrMinLen)  ||
       (AtStrlen(pMaskStr) > cMaskStrMaxLen))
        {
        AtPrintc(cSevCritical, " StringMask is not valid!, %s \r\n", pMaskStr);
        return cAtFalse;
        }
    return cAtTrue;
    }

static eBool StringToMaskGet(char *pMaskStr, uint32 bitMaxVal)
    {
    uint32 *idBuffer;
    uint32 numChannels, num_i, bit_i;
    uint32 register_i;
    uint32 pos_mask_i;
    char strTmp[16];

    /* Mask with format hexa */
    if(ValidHexaMask(pMaskStr))
    	{
    	if (bitMaxVal != cShortMaskSizes)
    		return cAtFalse;

    	g_register_mask[0] =AtStrtoul(pMaskStr, NULL, 16);
    	return cAtTrue;
    	}

    if (!ValidStringMask(pMaskStr))
        return cAtFalse;

    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, &pMaskStr[3], AtStrlen(pMaskStr) - 3);

    /* It is just a list of flat IDs */
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(strTmp, idBuffer, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "IdList %s can not get channels \r\n", strTmp);
        return cAtFalse;
        }

    /* Clear Global Masks */
    GlobalMaskReset();

    /* BuildMask */
    for (num_i = 0; num_i < numChannels; num_i++)
        {
        bit_i = idBuffer[num_i];
        if (bit_i > bitMaxVal)
            {
            AtPrintc(cSevCritical, "bit_i %d out-of-range, bitMaxVal %d \r\n", bit_i, bitMaxVal);
            return cAtFalse;
            }

        /* Get Register_i and Pos_Mask_i */
        register_i = bit_i/32;
        pos_mask_i = bit_i%32;
        g_register_mask[register_i] += (cBit0 << pos_mask_i);
        }

    return cAtTrue;
    }


static eBool IsRunning(void)
    {
    if (shortRegisterMon == 0)
        return cAtFalse;

    if (!AtShortRegisterMonIsStarted(shortRegisterMon))
        return cAtFalse;

    if (register_monitor_counters < register_monitor_numberRepeat)
        return cAtTrue;

    return cAtFalse;
    }

static void Perform(void)
    {
    if (!AtShortRegisterMonPerform(shortRegisterMon))
        register_monitor_fail_counters ++;
    register_monitor_counters++;
    }

static void Create(void)
    {
	if (shortRegisterMon == NULL)
		{
	    shortRegisterMon = AtShortRegisterMonNew(CliDevice());
	    assert(shortRegisterMon);
		}
    else
    	AtPrintc(cSevCritical, " Register Monitor is created Before!\r\n");
    }

void ShortRegiserMonDestroy(void)
    {
    if (shortRegisterMon)
        AtShortRegisterMonDelete(shortRegisterMon);
    else
        AtPrintc(cSevCritical, " Register Monitor is not created!\r\n");

    shortRegisterMon = NULL;
    }

uint32 RegisterMonPerformPeriodicProcess(uint32 periodInMs)
    {
    tAtOsalCurTime startTime, endTime;
    uint32 elapseTime = 0;

    /* Not allow when not running */
    if (!IsRunning())
        return elapseTime;

    /* Not allow when register monitor duration is 0 */
    if (register_monitor_duration == 0)
    	return 0;

    register_monitor_ticks += periodInMs;
    if ((register_monitor_ticks/1000) == register_monitor_duration)
        {
        AtOsalCurTimeGet(&startTime);
        register_monitor_ticks = 0;
        Perform();
        AtOsalCurTimeGet(&endTime);
        elapseTime = mTimeIntervalInMsGet(startTime, endTime);
        }

    return elapseTime;
    }

eBool CmdAtDeviceRegisterMonStart(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    Create();
    return cAtTrue;
    }

eBool CmdAtDeviceRegisterMonStop(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    ShortRegiserMonDestroy();
    return cAtTrue;
    }

eBool CmdAtDeviceRegisterMonSyncHw(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return AtShortRegisterMonSyncHardware(shortRegisterMon);
    }

eBool CmdAtDeviceRegisterMonPerform(char argc, char **argv)
    {
    uint32 duration = AtStrtoul(argv[0], NULL, 10);
    uint32 numberRepeat = AtStrtoul(argv[1], NULL, 10);
    AtUnused(argc);

    if (shortRegisterMon == 0)
		{
		AtPrintc(cSevCritical, "Task is running, Please stop it first!!!!\r\n");
		return cAtFalse;
		}

    /* Stop task before performace */
    AtShortRegisterMonStop(shortRegisterMon);

    /* Reset Value  */
    register_monitor_fail_counters = 0;
    register_monitor_counters = 0;
    register_monitor_ticks = 0;
    register_monitor_duration = duration;
    register_monitor_numberRepeat = numberRepeat;

    /* Start performace */
    AtShortRegisterMonStart(shortRegisterMon);
    if (register_monitor_duration != 0)
    	return cAtTrue;

    while (numberRepeat--)
        Perform();

    return cAtTrue;
    }

eBool CmdAtDeviceShortRegisterMonMask(char argc, char **argv)
    {
    uint8 coreId = 1;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 addressTemp, counter_i;

    if (AtStrcmp(argv[1], "none") == 0)
        {
        GlobalMaskReset();
        }
    /* Parser Mask from Input */
    else if (!StringToMaskGet(argv[1], cShortMaskSizes))
        {
        AtPrintc(cSevCritical, " Can not get mask with input %s \r\n", argv[1]);
        return cAtFalse;
        }

    if (argc > 2)
        numbers = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        step = AtStrtoul(argv[3], NULL, 10);

    if (argc > 4)
        coreId = (uint8) AtStrtoul(argv[4], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    if (coreId == 0)
        {
        AtPrintc(cSevCritical, " numbers must be start from 1!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!AtShortRegisterMonMaskSet(shortRegisterMon, addressTemp, (uint8)(coreId - 1), g_register_mask[0]))
            {
            AtPrintc(cSevCritical,
                    "Can not mask for address 0x%08x, details: core %d mask 0x%08x\r\n",
                     addressTemp,
                     coreId,
                     g_register_mask[0]);
            }
        }
    return cAtTrue;
    }

eBool CmdAtDeviceShortRegisterMonValue(char argc, char **argv)
    {
    uint8 coreId = 1;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 value = AtStrtoul(argv[1], NULL, 16);
    uint32 addressTemp, counter_i;

    /* Parser Mask from Input */
    if (argc > 2)
        numbers = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        step = AtStrtoul(argv[3], NULL, 10);

    if (argc > 4)
        coreId = (uint8) AtStrtoul(argv[4], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    if (coreId == 0)
        {
        AtPrintc(cSevCritical, " numbers must be start from 1!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!AtShortRegisterMonValueSet(shortRegisterMon, addressTemp, (uint8)(coreId - 1), value))
            {
            AtPrintc(cSevCritical,
                     "Can not value for address 0x%08x, details: core %d value 0x%08x\r\n",
                     addressTemp,
                     coreId,
					 value);
            }
        }
    return cAtTrue;
    }

eBool CmdAtDeviceShortRegisterMonAdd(char argc, char **argv)
    {
    uint8 coreId = 1;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 value = AtStrtoul(argv[1], NULL, 16);
    uint32 addressTemp, counter_i;

    /* Parser Mask from Input */
    if (argc > 2)
        numbers = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        step = AtStrtoul(argv[3], NULL, 10);

    if (argc > 4)
        coreId = (uint8) AtStrtoul(argv[4], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    if (coreId == 0)
        {
        AtPrintc(cSevCritical, " numbers must be start from 1!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!AtShortRegisterMonAdd(shortRegisterMon, addressTemp, 0xFFFFFFFF, value, (uint8)(coreId - 1)))
            {
            AtPrintc(cSevCritical,
                     "Can not add register monitor, details address 0x%08x core %d value 0x%08x\r\n",
                     addressTemp,
                     coreId,
					 value);
            }
        }
    return cAtTrue;
    }

eBool CmdAtDeviceShortRegisterMonRemove(char argc, char **argv)
    {
    uint8 coreId = 1;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 addressTemp, counter_i;

    /* Parser Mask from Input */
    if (argc > 1)
        numbers = AtStrtoul(argv[1], NULL, 10);

    if (argc > 2)
        step = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        coreId = (uint8) AtStrtoul(argv[3], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    if (coreId == 0)
        {
        AtPrintc(cSevCritical, " numbers must be start from 1!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!AtShortRegisterMonRmv(shortRegisterMon, addressTemp, (uint8)(coreId - 1)))
            {
            AtPrintc(cSevCritical,
                     "Can not remove register monitor, details address 0x%08x core %d\r\n",
                     addressTemp,
                     coreId);
            }
        }
    return cAtTrue;
    }

eBool CmdAtDeviceRegisterMonResultShow(char argc, char **argv)
    {
    tTab *tabPtr;
    eBool enable;
    uint32 colIdx = 0;
    eBool testIsPassed = cAtTrue;
    const char *pHeading[] =
        {
        "Enable",
        "Status",
        "duration [s]",
        "Repeat-Times",
        "Test-Times",
        "ticks",
        "Numbers of Short Registers",
        "Numbers of Collisions",
        "Fail-Times-Counters",
        };

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

	enable = AtShortRegisterMonIsStarted(shortRegisterMon);
    ColorStrToCell(tabPtr, 0, colIdx++, CliBoolToString(AtShortRegisterMonIsStarted(shortRegisterMon)), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliRegisterStatusToString(IsRunning()), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_monitor_duration, "%u"), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_monitor_numberRepeat, "%u"), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_monitor_counters, "%u"), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_monitor_ticks, "%u"), cSevInfo);

    if (enable)
        {
        ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(AtShortRegisterMonEntriesCount(shortRegisterMon), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(AtShortRegisterMonCollisionCount(shortRegisterMon), "%u"), cSevInfo);
        }
    else
        {
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        }

    testIsPassed = (register_monitor_fail_counters == 0) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_monitor_fail_counters, "%u"), testIsPassed ? cSevNormal : cSevCritical);

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(testIsPassed));
    return cAtTrue;


    return cAtTrue;
    }

eBool CmdAtDeviceRegisterMonInfoShow(char argc, char **argv)
    {
	AtUnused(argc);
	AtUnused(argv);
    return AtShortRegisterMonShowInfo(shortRegisterMon);
    }

eBool CmdAtDeviceRegisterMonFailedShow(char argc, char **argv)
    {
	AtUnused(argc);
	AtUnused(argv);
    return AtShortRegisterMonFailedShow(shortRegisterMon);
    }

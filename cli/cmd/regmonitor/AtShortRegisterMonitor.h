/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Short-Register-Monitor-Utils
 *
 * File        : AtShortRegMon.c
 *
 * Created Date:
 *
 * Description : This is used for testing purpose, to monitor some register for over-night such as.
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/


#ifndef AT_SHORT_REGISTER_HEADER
#define AT_SHORT_REGISTER_HEADER

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Structures -------------------------------------*/
typedef struct tAtShortRegisterEntry
    {
    uint32 coreId;
    uint32 address;
    uint32 mask;
    uint32 value;
    uint32 syncValue;
    uint32 valueIsChangedCounters;
    void * next;
    }tAtShortRegisterEntry;

typedef struct tAtShortRegisterMon
	{
    tAtShortRegisterEntry* * bucket; 	/**< bucket */
    uint32  count; 						/**< count */
    uint32  collisionCount; 		    /**< collisionCount */
    uint32  maxCollisionPerHash;        /**< maxCollisionPerHash */
    AtDevice device;
    eBool start;
    uint32 pollCnt;
	} tAtShortRegisterMon;

/*--------------------------- Forward Declare---------------------------------*/
tAtShortRegisterMon *AtShortRegisterMonNew(AtDevice device);
void AtShortRegisterMonDelete(tAtShortRegisterMon *self);

/* Provision new register and can set default value or mask */
eBool AtShortRegisterMonAdd(tAtShortRegisterMon* self, uint32 address, uint32 mask, uint32 value, uint8 coreId);
eBool AtShortRegisterMonValueSet(tAtShortRegisterMon* self, uint32 address, uint8 coreId, uint32 value);
eBool AtShortRegisterMonMaskSet(tAtShortRegisterMon *self, uint32 address, uint8 coreId, uint32 mask);

/* Remove the register if dont want to perform test */
eBool AtShortRegisterMonRmv(tAtShortRegisterMon* self, uint32 address, uint8 coreId);

/* Do Perform test with the register in List, we must start them */
eBool AtShortRegisterMonStart(tAtShortRegisterMon* self);
eBool AtShortRegisterMonStop(tAtShortRegisterMon* self);
eBool AtShortRegisterMonIsStarted(tAtShortRegisterMon* self);
eBool AtShortRegisterMonPerform(tAtShortRegisterMon* self);

/* Sync the value from the hardware to software base on mask of this register */
eBool AtShortRegisterMonSyncHardware(tAtShortRegisterMon* self);

/* Check if the register is added for test */
eBool AtShortRegisterMonIsAdded(tAtShortRegisterMon* self, uint32 address, uint8 coreId);

/* Show All Information of register that include: Value, mask, number of failure?? */
eBool AtShortRegisterMonShowInfo(tAtShortRegisterMon* self);
eBool AtShortRegisterMonFailedShow(tAtShortRegisterMon *self);

uint32 AtShortRegisterMonEntriesCount(const tAtShortRegisterMon* self);
uint32 AtShortRegisterMonCollisionCount(const tAtShortRegisterMon* self);

/* For this test, software will not stop when any error happens, so that we can make it run
 * over-night and statistic how many can happens
 * for this test and how many register is changing value with the number of changing */
#endif /* AT_SHORT_REGISTER_HEADER */



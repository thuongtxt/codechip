/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TRIE
 *
 * File        : trie.h
 *
 * Created Date: 25-May-09
 *
 * Description : This file contains all prototypes of TRIE component that 
 *				 provides fast searching for string key
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

#include <assert.h>
#include "atclib.h"
#include "AtOsal.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtShortRegisterMonitor.h"

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cShortRegisterHashTableSize (cBit18_0 + 1)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Implement --------------------------------------*/
static uint32 IndexGet(const struct tAtShortRegisterMon* self, uint32 address, uint8 coreId)
    {
    AtUnused(self);
    return (uint32) ((coreId + address) % cShortRegisterHashTableSize);
    }

static tAtShortRegisterEntry* EntryCreate(struct tAtShortRegisterMon* self,
                                          uint32 address,
										  uint32 mask,
                                          uint32 value,
                                          uint8 coreId)
    {
    tAtShortRegisterEntry* entry = AtOsalMemAlloc(sizeof(tAtShortRegisterEntry));
    assert(entry);
    AtUnused(self);
    entry->coreId  = coreId;
    entry->address = address;
    entry->value   = value;
    entry->mask    = mask;
    entry->valueIsChangedCounters    = 0;
    entry->syncValue    = value;
    entry->next    = NULL;
    return entry;
    }

static void RemoveAll(tAtShortRegisterMon* self)
    {
    tAtShortRegisterEntry* entry = NULL;
    tAtShortRegisterEntry* prev = NULL;
    uint32 i;

    for (i = 0; i < cShortRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];

        while (entry)
            {
            prev = entry;
            entry = entry->next;
            AtOsalMemFree(prev);
            }

        /* Delete bucket */
        self->bucket[i] = NULL;
        }
    self->count = 0;
    self->collisionCount = 0;
    }

uint32 AtShortRegisterMonCollisionCount(const tAtShortRegisterMon* self)
    {
	if (self)
		return self->collisionCount;
	return 0;
    }

uint32 AtShortRegisterMonEntriesCount(const tAtShortRegisterMon* self)
    {
	if (self)
		return self->count;
	return 0;
    }

static void AtShortRegisterMonClear(tAtShortRegisterMon* self)
    {
    tAtShortRegisterEntry* entry = NULL;
    uint32 i;
    for (i = 0; i < cShortRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
            entry->valueIsChangedCounters = 0;
            entry = entry->next;
            }
        }
    }

eBool AtShortRegisterMonStart(tAtShortRegisterMon* self)
	{
	if (!self)
		return cAtFalse;

	if (!self->start)
		{
		self->pollCnt = 0;
		AtShortRegisterMonClear(self);
		self->start = cAtTrue;
		}

	return cAtTrue;
	}

eBool AtShortRegisterMonStop(tAtShortRegisterMon* self)
	{
	if (!self)
		return cAtFalse;
	self->start = cAtFalse;
	return cAtTrue;
	}

eBool AtShortRegisterMonIsStarted(tAtShortRegisterMon* self)
	{
	if (!self)
		return cAtFalse;
	return self->start;
	}

eBool AtShortRegisterMonAdd(tAtShortRegisterMon* self, uint32 address, uint32 mask, uint32 value, uint8 coreId)
    {
    uint32 index = 0;

    /* Cannot insert NULL values */
    if (self == NULL)
        return 0;

    if (self->count == cShortRegisterHashTableSize)
        {
        AtPrintc(cSevCritical, "Can not add any register, Resource memory is not available !!!\r\n");
        return cAtFalse;
        }

    /* Calculate hash value and buffer index */
    index = IndexGet(self, address, coreId);
    if (self->bucket[index] == NULL)
        {
        self->bucket[index] = EntryCreate(self, address, mask, value, coreId);
        }
    else
        {
        uint32 count = 0; /* Used to count collision */
        tAtShortRegisterEntry* prev = NULL;
        tAtShortRegisterEntry* entry = self->bucket[index];
        while (entry)
            {
            /* Update value */
            if ((entry->address == address) &&  (entry->coreId == coreId))
                {
            	entry->mask = mask;
                entry->value = (value & entry->mask);
                entry->syncValue  =entry->value;
                return cAtTrue;
                }
            prev = entry;
            entry = entry->next;
            count++; /* collision */
            }

        /* Do not insert if max collision is exceeded */
        if ((self->maxCollisionPerHash != 0)  && (count >= self->maxCollisionPerHash))
            {
            AtPrintc(cSevCritical, "Can not add this register 0x%x, Over Collision Thresold !!!\r\n", address);
            return cAtFalse;
            }

        entry = EntryCreate(self, address, mask, value, coreId);
        prev->next = entry;
        (self->collisionCount)++;
        }

    (self->count)++;
    return cAtTrue;
    }

eBool AtShortRegisterMonRmv(tAtShortRegisterMon* self, uint32 address, uint8 coreId)
    {
    uint32 index = IndexGet(self, address, coreId);
    tAtShortRegisterEntry* entry = self->bucket[index];
    tAtShortRegisterEntry* prev = NULL;
    eBool IsFound = cAtFalse;

    /* Not exist that */
    if (entry == NULL)
        return cAtFalse;

    while (entry)
        {
        if ((entry->address == address) && (entry->coreId == coreId))
            {
            IsFound = cAtTrue;
            break;
            }
        prev  = entry;
        entry = entry->next;
        }

    if (IsFound == cAtFalse)
        {
		AtPrintc(cSevCritical,
				 "Not Found [address 0x%08x FpgaId %d]\r\r",
				 address,
				 coreId + 1);
        return cAtFalse;
        }

    /* Remove Last */
    if ((entry->next == NULL) && (entry != self->bucket[index]))
        {
        AtOsalMemFree(entry);
        (self->count)--;
        prev->next = NULL;
        return cAtTrue;
        }

    /* Remove First */
    if (entry == self->bucket[index])
        {
        self->bucket[index] = (entry->next != NULL) ? entry->next: NULL;
        AtOsalMemFree(entry);
        (self->count)--;
        return cAtTrue;
        }

    /* Remove Midle, adjust ref next */
    if ((prev->next) && (entry->next))
        {
        prev->next = entry->next;
        AtOsalMemFree(entry);
        (self->count)--;
        return cAtTrue;
        }

    AtPrintc(cSevCritical, "Wrong handle remove register \r\r");
    return cAtFalse;
    }

tAtShortRegisterMon *AtShortRegisterMonNew(AtDevice device)
    {
    tAtShortRegisterMon* self  = (tAtShortRegisterMon *) AtOsalMemAlloc(sizeof(tAtShortRegisterMon));
    assert(self);
    AtOsalMemInit(self, 0, sizeof(tAtShortRegisterMon));
    self->bucket = AtOsalMemAlloc(cShortRegisterHashTableSize * sizeof(void*));
    AtOsalMemInit(self->bucket, 0 , cShortRegisterHashTableSize * sizeof(void*));
    assert(self->bucket);
    self->maxCollisionPerHash = 10;
    self->device = device;
    self->start = 0;
    return self;
    }

void AtShortRegisterMonDelete(tAtShortRegisterMon *self)
    {
    if (self)
        {
        RemoveAll(self);
        AtOsalMemFree(self->bucket);
        AtOsalMemFree(self);
        self = NULL;
        }
    }

eBool AtShortRegisterMonPerform(tAtShortRegisterMon* self)
    {
	uint32 entry_i;
    AtDevice device;
    eBool success = cAtTrue;
    tAtShortRegisterEntry* entry = NULL;
    if (self == NULL)
    	return cAtFalse;

    device = self->device;
    for (entry_i = 0; entry_i < cShortRegisterHashTableSize; entry_i++)
        {
        entry = self->bucket[entry_i];
        while ((entry) && (self->start))
            {
            uint32 address = entry->address;
            AtIpCore core = AtDeviceIpCoreGet(device, (uint8)entry->coreId);
            AtHal hal = AtIpCoreHalGet(core);
            uint32 value = AtHalRead(hal, address);
            uint32 mask  = entry->mask;

            /* Just keep Orgin Value, to see that sync value from
             * hardware is not same as value when counter happens */
            if (entry->valueIsChangedCounters == 0)
            	entry->syncValue = entry->value;

            if ((value & mask) != (entry->syncValue & mask))
                {
            	entry->valueIsChangedCounters++;
            	entry->syncValue = value;
            	success = cAtFalse;
                }

            entry = entry->next;
            }
        }
    return success;
    }

eBool AtShortRegisterMonMaskSet(tAtShortRegisterMon *self,
								uint32 address,
								uint8 coreId,
								uint32 mask)
    {
    tAtShortRegisterEntry* entry = NULL;
    uint32 entry_i;

    if (self == NULL)
    	return cAtFalse;

    for (entry_i = 0; entry_i < cShortRegisterHashTableSize; entry_i++)
        {
        entry = self->bucket[entry_i];
        while (entry)
            {
            if ((entry->address == address) && (entry->coreId == coreId))
                {
                entry->mask = mask;
				entry->value = (entry->value & mask);
				entry->syncValue = (entry->syncValue & mask);
                return cAtTrue;
                }
            entry = entry->next;
            }
        }
    return cAtFalse;
    }

eBool AtShortRegisterMonValueSet(tAtShortRegisterMon *self,
								uint32 address,
								uint8 coreId,
								uint32 value)
    {
    tAtShortRegisterEntry* entry = NULL;
    uint32 entry_i;

    if (self == NULL)
    	return cAtFalse;

    for (entry_i = 0; entry_i < cShortRegisterHashTableSize; entry_i++)
        {
        entry = self->bucket[entry_i];
        while (entry)
            {
            if ((entry->address == address) && (entry->coreId == coreId))
                {
				entry->value = (value & entry->mask);
				entry->syncValue = (value & entry->mask);
                return cAtTrue;
                }
            entry = entry->next;
            }
        }
    return cAtFalse;
    }

static eBool InfoDisplay(tAtShortRegisterEntry *entry)
    {
	eBool fail = (entry->valueIsChangedCounters == 0) ? cAtFalse: cAtTrue;
	AtPrintc(fail ? cSevCritical : cSevInfo,
             "FpgaId %d address=0x%08x mask=0x%08x value=0x%08x syncValue=0x%08x chgCounters=%d\r\n",
             entry->coreId + 1,
             entry->address,
             entry->mask,
			 entry->value,
			 entry->syncValue,
			 entry->valueIsChangedCounters);
	return fail;
    }

eBool AtShortRegisterMonShowInfo(tAtShortRegisterMon *self)
    {
    uint32 i;
	eBool fail = cAtFalse;
    tAtShortRegisterEntry* entry = NULL;

    if (self == NULL)
    	return cAtFalse;

    AtPrintc(cSevInfo, "--------------- Total check times : %d \r\n", self->pollCnt);
    for (i = 0; i < cShortRegisterHashTableSize; i++)
        {
        entry = self->bucket[i];
        while (entry)
            {
        	fail |= InfoDisplay(entry);
            entry = entry->next;
            }
        }

    AtPrintc(fail ? cSevCritical : cSevInfo, "Result is : %s \r\n", fail ? "FAILED!!!": "PASSED!!!");
    return cAtTrue;
    }


eBool AtShortRegisterMonSyncHardware(tAtShortRegisterMon* self)
    {
    uint32 entry_i;
    AtDevice device;
    eBool success = cAtTrue;
    tAtShortRegisterEntry* entry = NULL;
    if (self == NULL)
    	return cAtFalse;


    device = self->device;
    for (entry_i = 0; entry_i < cShortRegisterHashTableSize; entry_i++)
        {
        entry = self->bucket[entry_i];
        while (entry)
            {
            uint32 address = entry->address;
            AtIpCore core = AtDeviceIpCoreGet(device, (uint8)entry->coreId);
            AtHal hal = AtIpCoreHalGet(core);
            uint32 value = AtHalRead(hal, address);
            uint32 mask  = entry->mask;
            entry->syncValue = (value & mask);
            entry->value = (value & mask);
            entry->valueIsChangedCounters = 0;
            entry = entry->next;
            }
        }
    return success;
    }

static eBool FailedDisplay(tAtShortRegisterEntry *entry)
    {
	if (entry->valueIsChangedCounters != 0)
		{
		AtPrintc(cSevInfo,
				 "FpgaId %d address=0x%08x mask=0x%08x value=0x%08x syncValue=0x%08x chgCounters=%d\r\n",
				 entry->coreId + 1,
				 entry->address,
				 entry->mask,
				 entry->value,
				 entry->syncValue,
				 entry->valueIsChangedCounters);
		return cAtFalse;
		}
	return cAtTrue;
    }

eBool AtShortRegisterMonFailedShow(tAtShortRegisterMon *self)
    {
    uint32 entry_i;
    uint32 reg_fail_cnt;
    tAtShortRegisterEntry* entry = NULL;

    if (self == NULL)
    	return cAtFalse;

    AtPrintc(cSevInfo, " Detail register is failed with monitoring: \r\n");
    reg_fail_cnt = 0;
    for (entry_i = 0; entry_i < cShortRegisterHashTableSize; entry_i++)
        {
        entry = self->bucket[entry_i];
        while (entry)
            {
        	if (!FailedDisplay(entry))
        		reg_fail_cnt++;
            entry = entry->next;
            }
        }
    AtPrintc(cSevInfo, " reg_fail_cnt : %d\r\n", reg_fail_cnt);
    return cAtTrue;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtZtePtn.c
 *
 * Created Date: Jun 28, 2013
 *
 * Description : ZTE PTN specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtZtePtn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* The purpose of this function is to make sure that all of public APIs that
 * are not called in ATSDK will always be available in library. Without this
 * function, compiler may strip unused symbols */
static eBool LinkAllFuntion(void)
    {
    AtZtePtnPdhDe1AutoTxAisEnable(NULL, cAtFalse);
    AtZtePtnPdhDe1AutoTxAisIsEnabled(NULL);
    return cAtTrue;
    }

char *CliAtZtePtnSharedBuffer(void)
    {
    static char sharedCharBuffer[128];
    return sharedCharBuffer;
    }

static void ShowDeviceStatus(const char *title, eBool isOk, const char *bullet)
    {
    if (bullet)
        AtPrintc(cSevNormal, "%s %s", bullet, title);
    else
        AtPrintc(cSevNormal, "%s", title);

    AtPrintc(isOk ? cSevInfo : cSevCritical, "%s\r\n", isOk ? "OK" : "FAIL");
    }

tAtZtePtnTag1 *CliAtZtePtnStr2Tag1(char *tag1Str, tAtZtePtnTag1 *tag1)
    {
    uint32 zteTagBuf[10];
    uint32 numItem;
    static char minFormat[] = "0.0.0.0.0";
    static char maxFormat[] = "7.1.1.31.63";

    IdComplexNumParse(tag1Str, minFormat, maxFormat, 10, zteTagBuf, &numItem);
    if (numItem != 5)
        return NULL;

    tag1->priority        = (uint8)zteTagBuf[0];
    tag1->cfi             = (uint8)zteTagBuf[1];
    tag1->cpuPktIndicator = (uint8)zteTagBuf[2];
    tag1->encapType       = (uint8)zteTagBuf[3];
    tag1->packetLength    = (uint8)zteTagBuf[4];

    return tag1;
    }

tAtZtePtnTag2 *CliAtZtePtnStr2Tag2(char *tag2Str, tAtZtePtnTag2 *tag2)
    {
    uint32 zteTagBuf[10];
    uint32 numItem;
    static char minFormat[] = "0.0.0.0.0";
    static char maxFormat[] = "7.1.8.1.256";

    IdComplexNumParse((void*)tag2Str, minFormat, maxFormat, 10, zteTagBuf, &numItem);
    if (numItem != 5)
        return NULL;

    tag2->priority     = (uint8)zteTagBuf[0];
    tag2->cfi          = (uint8)zteTagBuf[1];
    tag2->stmPortId    = (uint8)zteTagBuf[2];
    tag2->isMlpppIma   = (uint8)zteTagBuf[3];
    tag2->zteChannelId = (uint16)zteTagBuf[4];

    return tag2;
    }

char *CliAtZtePtnTag1ToStr(const tAtZtePtnTag1 *zteTag1)
    {
    char *buffer = CliAtZtePtnSharedBuffer();

    if (zteTag1 == NULL)
        {
        AtSprintf(buffer, "none");
        return buffer;
        }

    AtSprintf(buffer,
              "%d.%d.%d.%d.%d",
              zteTag1->priority,
              zteTag1->cfi,
              zteTag1->cpuPktIndicator,
              zteTag1->encapType,
              zteTag1->packetLength);

    return buffer;
    }

char *CliAtZtePtnTag2ToStr(const tAtZtePtnTag2 *zteTag2)
    {
    char *buffer = CliAtZtePtnSharedBuffer();

    if (zteTag2 == NULL)
        {
        AtSprintf(buffer, "none");
        return buffer;
        }

    AtSprintf(buffer,
              "%d.%d.%d.%d.%d",
              zteTag2->priority,
              zteTag2->cfi,
              zteTag2->stmPortId,
              zteTag2->isMlpppIma,
              zteTag2->zteChannelId);

    return buffer;
    }

const char *CliAtZtePtnTag1Format(void)
    {
    return "<priority:0..7>.<cfi:0/1>.<cpuPktIndicator:0/1>.<encapType:0..31>.<pktLen:0..63>";
    }

const char *CliAtZtePtnTag2Format(void)
    {
    return "<priority:0..7>.<cfi:0/1>.<stmPortId:0..7>.<isMlppp:0/1>.<channelId:0..255>";
    }

eBool CmdAtZtePtnDeviceStatusShow(char argc, char **argv)
    {
    AtDevice device = CliDevice();
    eAtZtePtnClockStatus clockStatus = AtZtePtnClockCheck(device);
    const char *bullet = "*";
	AtUnused(argv);
	AtUnused(argc);

    /* Show clock status */
    ShowDeviceStatus("Clock       : ", (clockStatus == cAtZtePtnClockStatusOk) ? cAtTrue : cAtFalse, bullet);
    if (clockStatus != cAtZtePtnClockStatusOk)
        {
        ShowDeviceStatus("  - System      : ", (clockStatus & cAtZtePtnClockStatusSystemClockFail)     ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - LocalBus    : ", (clockStatus & cAtZtePtnClockStatusLocalBusClockFail)   ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - GE          : ", (clockStatus & cAtZtePtnClockStatusGeClockFail)         ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - OCN#1       : ", (clockStatus & cAtZtePtnClockStatusOcnClock1Fail)       ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - OCN#2       : ", (clockStatus & cAtZtePtnClockStatusOcnClock2Fail)       ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - DDR#1       : ", (clockStatus & cAtZtePtnClockStatusDdr1ClockFail)       ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - DDR#2       : ", (clockStatus & cAtZtePtnClockStatusDdr2ClockFail)       ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - DDR#3       : ", (clockStatus & cAtZtePtnClockStatusDdr3ClockFail)       ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - Reference#1 : ", (clockStatus & cAtZtePtnClockStatusReference1ClockFail) ? cAtFalse : cAtTrue, NULL);
        ShowDeviceStatus("  - Reference#2 : ", (clockStatus & cAtZtePtnClockStatusReference2ClockFail) ? cAtFalse : cAtTrue, NULL);
        }

    ShowDeviceStatus("RAM         : ", AtZtePtnRamIsGood(device), bullet);
    ShowDeviceStatus("Interfaces  : ", AtZtePtnInterfaceIsGood(device), bullet);
    ShowDeviceStatus("FPGA logic  : ", AtZtePtnFpgaLogicIsGood(device), bullet);

    return cAtTrue;

    /* Actually does nothing, see description of LinkAllFuntion() */
    return LinkAllFuntion();
    }

eBool CmdAtZtePtnEthPortSwitch(char argc, char **argv)
    {
    eAtRet ret;
    uint8 fromPortId = (uint8)AtStrtoul(argv[0], NULL, 10);
    uint8 toPortId   = (uint8)AtStrtoul(argv[1], NULL, 10);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    AtEthPort fromPort = AtModuleEthPortGet(ethModule, (uint8)(fromPortId - 1));
    AtEthPort toPort   = AtModuleEthPortGet(ethModule, (uint8)(toPortId - 1));
	AtUnused(argc);

    ret = AtZtePtnEthPortSwitch(fromPort, toPort);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot switch/bridge from port %d to port %d, ret = %s\r\n",
                 fromPortId,
                 toPortId,
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtZtePtnHalDiagMemTest(char argc, char **argv)
    {
    uint32 testIndex;
    uint32 pattern = 0;
    uint32 readValue;
    uint32 regAddr = AtStrToDw(argv[0]);
    uint32 dataMask = AtStrToDw(argv[1]);
    uint32 numTimes = AtStrToDw(argv[2]);
    AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), 0);
    eBool success = cAtTrue;
    AtUnused(argc);

    AtUnused(argc);

    /* Fill memory with a known pattern. */
    AtHalWrite(hal, regAddr, pattern);

    for (testIndex = 0; testIndex < numTimes; testIndex++)
        {
        /* Check each location and invert it for the second pass. */
        for (pattern = 1; (pattern & dataMask) != 0; pattern <<= 1)
            {
            AtHalWrite(hal, regAddr, pattern);
            readValue = AtHalRead(hal, regAddr);
            if (readValue != pattern)
                {
                success = cAtFalse;
                AtPrintc(cSevCritical, "(%s, %d) ERROR: Address 0x%08x, expect 0x%08x but got 0x%08x\r\n",
                         __FILE__, __LINE__,
                         regAddr, pattern, readValue);
                }
            }
        }

    /* Let the user know */
    if (success)
        AtPrintc(cSevInfo, "HAL memory test success.\n");
    else
        AtPrintc(cSevCritical, "HAL memory test fail.\n");

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtZtePtnPw.c
 *
 * Created Date: Oct 28, 2013
 *
 * Description : CLIs to be used for ZTE PTN PW cards
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtZtePtn.h"
#include "../../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SendLbitPacketsToCdrEnable(eBool enable)
    {
    AtZtePtnPwSendLbitPacketsToCdrEnable((AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw), enable);
    return cAtTrue;
    }

eBool CmdPwZteExpectedTagsSet(char argc, char **argv)
    {
    AtList pwList;
    tAtZtePtnTag1 tag1;
    tAtZtePtnTag2 tag2;
    uint32 pw_i;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Parse tag 1 */
    if (CliAtZtePtnStr2Tag1(argv[1], &tag1) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 1, expect %s\r\n", CliAtZtePtnTag1Format());
        return cAtFalse;
        }

    /* Parse tag 2 */
    if (CliAtZtePtnStr2Tag2(argv[2], &tag2) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 2, expect %s\r\n", CliAtZtePtnTag2Format());
        return cAtFalse;
        }

    /* Get PW list */
    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW to configure, ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    /* Apply configuration */
    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        eAtRet ret = cAtOk;
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);

        ret |= AtZtePtnPwExpectedTag2Set(pw, &tag2);
        if (ret == cAtOk)
            ret |= AtZtePtnPwExpectedTag1Set(pw, &tag1);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwList);

    return success;
    }

eBool CmdPwZteHeaderSet(char argc, char **argv)
    {
    AtPw pw;
    uint16 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPws;
    AtModulePw pwModule;
    tAtZtePtnTag1 zteTag1;
    tAtZtePtnTag2 zteTag2;
    uint8 mac[cAtMacAddressLen];
    eBool success = cAtTrue;
	AtUnused(argc);

    AtOsalMemInit(mac, 0, cAtMacAddressLen);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid MAC address\r\n");
        return cAtFalse;
        }

    if (CliAtZtePtnStr2Tag1(argv[2], &zteTag1) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 1, expect %s\r\n", CliAtZtePtnTag1Format());
        return cAtFalse;
        }

    if (CliAtZtePtnStr2Tag2(argv[3], &zteTag2) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 2, expect %s\r\n", CliAtZtePtnTag2Format());
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i++)
        {
        eAtRet ret = cAtOk;

        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        /* Apply new header */
        ret = AtZtePwPtnHeaderSet(pw, mac, &zteTag1, &zteTag2);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwZtePtnHeaderShow(char argc, char **argv)
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static uint8  mac[6];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "txTag1", "txTag2", "expTag1", "expTag2", "ethPort", "DMAC"};
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);
    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw;
        AtEthPort ethPort;
        tAtZtePtnTag1 zteTag1;
        tAtZtePtnTag2 zteTag2;

        pw = (AtPw)AtListObjectGet(pwList, i);

        column = 0;
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        /* Tags */
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag1ToStr(AtZtePtnPwTag1Get(pw, &zteTag1)));
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag2ToStr(AtZtePtnPwTag2Get(pw, &zteTag2)));
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag1ToStr(AtZtePtnPwExpectedTag1Get(pw, &zteTag1)));
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag2ToStr(AtZtePtnPwExpectedTag2Get(pw, &zteTag2)));

        /* ETH port */
        ethPort = AtPwEthPortGet(pw);
        if (ethPort == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            {
            AtSprintf(CliAtZtePtnSharedBuffer(), "%u", AtChannelIdGet((AtChannel)ethPort) + 1);
            StrToCell(tabPtr, i, column++, CliAtZtePtnSharedBuffer());
            }

        /* MAC */
        AtOsalMemInit(mac, 0, sizeof(mac));
        AtPwEthDestMacGet(pw, mac);
        StrToCell(tabPtr, i, column++, AtBytes2String(mac, 6, 16));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);
    AtPrintc(cSevInfo, "Format:\r\n");
    AtPrintc(cSevNormal, "- Tag1: priority.cfi.cpuPktIndicator.encapType.pktLen\r\n");
    AtPrintc(cSevNormal, "- Tag2: priority.cfi.stmPortId.isMlppp.channelId\r\n");

    return cAtTrue;
    }

eBool CmdPwZteSendLbitPacketsToCdrEnable(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return SendLbitPacketsToCdrEnable(cAtTrue);
    }

eBool CmdPwZteSendLbitPacketsToCdrDisable(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return SendLbitPacketsToCdrEnable(cAtFalse);
    }

eBool CmdPwZteSendLbitPacketsToCdrGet(char argc, char **argv)
    {
    tTab *tabPtr;
    static uint8 numRow = 1;
    const char *heading[] = {"SendLbitToCdr"};
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
	AtUnused(argv);
	AtUnused(argc);

    /* Create table */
    tabPtr = TableAlloc(numRow, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, 0, CliBoolToString(AtZtePtnPwSendLbitPacketsToCdrIsEnabled(pwModule)));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }


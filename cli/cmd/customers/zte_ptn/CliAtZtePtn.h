/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtZtePtn.h
 * 
 * Created Date: Oct 28, 2013
 *
 * Description : Common include
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATZTEPTN_H_
#define _CLIATZTEPTN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../pw/CliAtModulePw.h"
#include "AtDevice.h"
#include "AtPw.h"
#include "AtZtePtn.h"
#include "AtList.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char *CliAtZtePtnTag1Format(void);
const char *CliAtZtePtnTag2Format(void);
char *CliAtZtePtnTag1ToStr(const tAtZtePtnTag1 *zteTag1);
char *CliAtZtePtnTag2ToStr(const tAtZtePtnTag2 *zteTag2);
tAtZtePtnTag1 *CliAtZtePtnStr2Tag1(char *tag1Str, tAtZtePtnTag1 *tag1);
tAtZtePtnTag2 *CliAtZtePtnStr2Tag2(char *tag2Str, tAtZtePtnTag2 *tag2);
char *CliAtZtePtnSharedBuffer(void);

#endif /* _CLIATZTEPTN_H_ */


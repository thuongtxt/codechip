/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtZtePtnPpp.c
 *
 * Created Date: Oct 28, 2013
 *
 * Description : CLIs to be used for ZTE PTN POS/CPOS cards
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtZtePtn.h"
#include "../../encap/CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtZtePtnCHdlcPktTypeStr[] ={"data", "isis", "oam"};
static const uint32 cAtZtePtnCHdlcPktTypeVal[]={cAtZtePtnDataType, cAtZtePtnISISType, cAtZtePtnOamType};

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool EthFlowZteTagSet(char argc,
                              char **argv,
                              eAtRet (*PppZteTagSet)(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2),
                              AtEthFlow* (*FlowListGet)(char *idList, uint32 *numFlow))
    {
    AtEthFlow *flows;
    tAtZtePtnTag1 tag1;
    tAtZtePtnTag2 tag2;
    uint32 numFlows, flow_i;
    eBool result = cAtTrue;
    eAtRet ret = cAtOk;
    uint8 ethPortId;
	AtUnused(argc);

    /* Get PW list */
    flows = FlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "WARNING: ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    /* Get Port Id */
    ethPortId = (uint8)(AtStrToDw(argv[1]) - 1);

    /* Parse tag 1 */
    if (CliAtZtePtnStr2Tag1(argv[2], &tag1) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 1, expect %s\r\n", CliAtZtePtnTag1Format());
        return cAtFalse;
        }

    /* Parse tag 2 */
    if (CliAtZtePtnStr2Tag2(argv[3], &tag2) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 2, expect %s\r\n", CliAtZtePtnTag2Format());
        return cAtFalse;
        }

    /* Apply configuration */
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        ret |= PppZteTagSet(flows[flow_i], ethPortId, &tag1, &tag2);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)flows[flow_i]),
                     AtRet2String(ret));
            result = cAtFalse;
            }
        }

    return result;
    }

static eBool EthFlowZteTagGet(char argc, char **argv, AtEthFlow* (*FlowListGet)(char *idList, uint32 *numFlow))
    {
    AtEthFlow     *flows;
    uint16        i, column;
    uint32        numFlows;
    tTab          *tabPtr;
    const char    *heading[] = {"Id", "txTag1", "txTag2", "expTag1", "expTag2", "ethPort", "txStmPort", "expStmPort"};
	AtUnused(argc);

    flows = FlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "WARNING: ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numFlows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numFlows; i++)
        {
        tAtEthVlanDesc vlanDesc;
        tAtZtePtnTag1 zteTag1;
        tAtZtePtnTag2 zteTag2;
        uint8 stmPort;

        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)flows[i]));

        /* Tags */
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag1ToStr(AtZtePtnEthFlowTag1Get(flows[i], &zteTag1)));
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag2ToStr(AtZtePtnEthFlowTag2Get(flows[i], &zteTag2)));
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag1ToStr(AtZtePtnEthFlowExpectedTag1Get(flows[i], &zteTag1)));
        StrToCell(tabPtr, i, column++, CliAtZtePtnTag2ToStr(AtZtePtnEthFlowExpectedTag2Get(flows[i], &zteTag2)));

        /* ETH port */
        if (AtEthFlowIngressVlanAtIndex(flows[i], 0, &vlanDesc))
            AtSprintf(CliAtZtePtnSharedBuffer(), "%d", vlanDesc.ethPortId + 1);
        else
            AtSprintf(CliAtZtePtnSharedBuffer(), "none");
        StrToCell(tabPtr, i, column++, CliAtZtePtnSharedBuffer());

        /* STM port */
        stmPort = AtZtePtnEthFlowHigigTxStmPortGet(flows[i]);
        StrToCell(tabPtr, i, column++, (stmPort == cBit7_0) ? "xxx" : CliNumber2String(stmPort, "%u"));

        stmPort = AtZtePtnEthFlowHigigExpectedStmPortGet(flows[i]);
        StrToCell(tabPtr, i, column++, (stmPort == cBit7_0) ? "xxx" : CliNumber2String(stmPort, "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtPrintc(cSevInfo, "Format:\r\n");
    AtPrintc(cSevNormal, "- Tag1: priority.cfi.cpuPktIndicator.encapType.pktLen\r\n");
    AtPrintc(cSevNormal, "- Tag2: priority.cfi.stmPortId.isMlppp.channelId\r\n");

    return cAtTrue;
    }

static uint32 *EntryListFromString(char *entryListString, uint32 *numEntries)
    {
    uint32 numValidEntries = 0;
    uint32 bufferSize;
    uint32 *idBuffer    = CliSharedIdBufferGet(&bufferSize);
    AtIdParser idParser = AtIdParserNew(entryListString, NULL, NULL);

    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 entryId = AtIdParserNextNumber(idParser);
        if (entryId > AtZtePtnPppNumLookupEntries(CliDevice()))
            {
            AtPrintc(cSevWarning, "WARNING: no such entry %u\r\n", entryId);
            continue;
            }

        idBuffer[numValidEntries] = entryId;
        numValidEntries = numValidEntries + 1;

        if (numValidEntries >= bufferSize)
            break;
        }

    if (numEntries)
        *numEntries = numValidEntries;

    AtObjectDelete((AtObject)idParser);

    return idBuffer;
    }

static eBool EntryEnable(char argc, char **argv, eBool enable)
    {
    uint32 numEntries = 0, entry_i;
    uint32 *entries = EntryListFromString(argv[0], &numEntries);
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of entries */
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WANRING: No entry to configure, the ID list may be invalid. Example: 1-3,4-6\r\n");
        return cAtTrue;
        }

    /* Apply these values for all of them */
    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        eAtRet ret = AtZtePtnPppLookupEntryEnable(CliDevice(), (uint16)(entries[entry_i] - 1), enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s entry %u, ret = %s\r\n",
                     enable ? "enable" : "disable",
                     entries[entry_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool PppTableShow(char argc, char **argv, uint16 (*EncapTypeGet)(AtDevice self, uint16 lookupIndex))
    {
    tTab *tabPtr;
    const char *heading[] = {"Index", "PID", "EthType", "PppEncapType", "MlpppEncapType",
                             "PRI", "CFI", "Enable"};

    uint8 entryIndex;
    char *buffer = CliAtZtePtnSharedBuffer();
    uint16 maxNumEntries = AtZtePtnPppNumLookupEntries(CliDevice());
	AtUnused(EncapTypeGet);
	AtUnused(argv);
	AtUnused(argc);

    /* Create table */
    tabPtr = TableAlloc(AtZtePtnPppNumLookupEntries(CliDevice()), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content. Note, the last entry is for LCP, need to treat it by special way */
    for (entryIndex = 0; entryIndex < maxNumEntries; entryIndex++)
        {
        eBool enable;

        AtSprintf(buffer, "%d", entryIndex + 1);
        StrToCell(tabPtr, entryIndex, 0, buffer);

        if (entryIndex == (maxNumEntries - 1))
            StrToCell(tabPtr, entryIndex, 1, "0xCXXX");
        else
            {
            AtSprintf(buffer, "0x%04x", AtZtePtnPppLookupEntryPidGet(CliDevice(), entryIndex));
            StrToCell(tabPtr, entryIndex, 1, buffer);
            }

        if (AtZtePtnPppLookupEntryHasEthType(CliDevice(), entryIndex))
            {
            AtSprintf(buffer, "0x%04x", AtZtePtnPppLookupEntryEthTypeGet(CliDevice(), entryIndex));
            StrToCell(tabPtr, entryIndex, 2, buffer);
            }
        else
            StrToCell(tabPtr, entryIndex, 2, sAtNotApplicable);

        AtSprintf(buffer, "0x%04x", AtZtePtnPppLookupEntryEncapTypeGet(CliDevice(), entryIndex));
        StrToCell(tabPtr, entryIndex, 3, buffer);

        AtSprintf(buffer, "0x%04x", AtZtePtnMlpppLookupEntryEncapTypeGet(CliDevice(), entryIndex));
        StrToCell(tabPtr, entryIndex, 4, buffer);

        AtSprintf(buffer, "%d", AtZtePtnPppLookupEntryPriorityGet(CliDevice(), entryIndex));
        StrToCell(tabPtr, entryIndex, 5, buffer);

        AtSprintf(buffer, "%d", AtZtePtnPppLookupEntryCfiGet(CliDevice(), entryIndex));
        StrToCell(tabPtr, entryIndex, 6, buffer);

        enable = AtZtePtnPppLookupEntryIsEnabled(CliDevice(), entryIndex);
        ColorStrToCell(tabPtr, entryIndex, 7, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool EntryUint8AttributeSet(char argc, char **argv, eAtRet (*AttributeSet)(AtDevice self, uint16 lookupIndex, uint8 value))
    {
    uint32 numEntries = 0, entry_i;
    uint8 value;
    uint32 *entries = EntryListFromString(argv[0], &numEntries);
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of entries */
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WANRING: No entry to configure, the ID list may be invalid. Example: 1-3,4-6\r\n");
        return cAtTrue;
        }

    /* Values of these entries */
    value = (uint8)AtStrToDw(argv[1]);

    /* Apply these values for all of them */
    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        eAtRet ret = AttributeSet(CliDevice(), (uint16)(entries[entry_i] - 1), (uint8)value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure entry %u, ret = %s\r\n", entries[entry_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool CHdlcLookupEntrySet(char argc,
                                 char **argv,
                                 eAtRet (*AttributeSet)(AtDevice self, eAtZtePtnCHdlcPktType pktType, uint8 value))
    {
    eAtRet ret;
    uint8 priority;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    eAtZtePtnCHdlcPktType pktType;
	AtUnused(argc);

    pktType = CliStringToEnum(argv[0], cAtZtePtnCHdlcPktTypeStr, cAtZtePtnCHdlcPktTypeVal, mCount(cAtZtePtnCHdlcPktTypeVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Packet Type mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtZtePtnCHdlcPktTypeStr, mCount(cAtZtePtnCHdlcPktTypeStr));
        AtPrintc(cSevCritical, "\r\n");
        }

    priority = (uint8)AtStrToDw(argv[1]);

    /* Apply these values for all of them */
    ret = AttributeSet(CliDevice(), pktType, priority);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot set value = %d, ret = %s\r\n" , priority, AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool FrameRelayAttributeSet(char argc, char **argv, eAtRet (*AttributeSet)(AtDevice sel, uint8 value))
    {
    uint8 value = (uint8)AtStrToDw(argv[0]);
    eAtRet ret = AttributeSet(CliDevice(), value);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot configure entry ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtZteEthFlowTxHeaderSet(char argc, char **argv)
    {
    return EthFlowZteTagSet(argc, argv, AtZteEthFlowTxHeaderSet, CliEthFlowListGet);
    }

eBool CmdAtZteEthFlowExpectedHeaderSet(char argc, char **argv)
    {
    return EthFlowZteTagSet(argc, argv, AtZteEthFlowExpectedHeaderSet, CliEthFlowListGet);
    }

eBool CmdAtHdlcLinkOamTxEthHeaderSet(char argc, char **argv)
    {
    return EthFlowZteTagSet(argc, argv, AtZteEthFlowTxHeaderSet, CliHdlcLinkOamFlowListGet);
    }

eBool CmdAtHdlcLinkOamExpectedEthHeaderSet(char argc, char **argv)
    {
    return EthFlowZteTagSet(argc, argv, AtZteEthFlowExpectedHeaderSet, CliHdlcLinkOamFlowListGet);
    }

eBool CmdEthFlowZtePtnHeaderShow(char argc, char **argv)
    {
    return EthFlowZteTagGet(argc, argv, CliEthFlowListGet);
    }

eBool CmdHdlcLinkOamZtePtnHeaderShow(char argc, char **argv)
    {
    return EthFlowZteTagGet(argc, argv, CliHdlcLinkOamFlowListGet);
    }

eBool CmdAtZtePtnPppTableShow(char argc, char **argv)
    {
    return PppTableShow(argc, argv, AtZtePtnPppLookupEntryEncapTypeGet);
    }

eBool CmdAtZtePtnPppEntrySet(char argc, char **argv)
    {
    uint32 numEntries = 0, entry_i;
    uint16 pidValue, ethType;
    uint8 zteEncapType;
    uint32 *entries = EntryListFromString(argv[0], &numEntries);
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of entries */
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WANRING: No entry to configure, the ID list may be invalid. Example: 1-3,4-6\r\n");
        return cAtTrue;
        }

    /* Values of these entries */
    pidValue     = (uint16)AtStrToDw(argv[1]);
    ethType      = (uint16)AtStrToDw(argv[2]);
    zteEncapType = (uint8)AtStrToDw(argv[3]);

    /* Apply these values for all of them */
    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        eAtRet ret = AtZtePtnPppLookupEntrySet(CliDevice(), (uint16)(entries[entry_i] - 1), pidValue, ethType, zteEncapType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure entry %u, ret = %s\r\n", entries[entry_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtZtePtnPppLookupEntryEnable(char argc, char **argv)
    {
    return EntryEnable(argc, argv, cAtTrue);
    }

eBool CmdAtZtePtnPppLookupEntryDisable(char argc, char **argv)
    {
    return EntryEnable(argc, argv, cAtFalse);
    }

eBool CmdAtZtePtnPppLookupEntryCfiSet(char argc, char **argv)
    {
    return EntryUint8AttributeSet(argc, argv, AtZtePtnPppLookupEntryCfiSet);
    }

eBool CmdAtZtePtnPppLookupEntryPrioritySet(char argc, char **argv)
    {
    return EntryUint8AttributeSet(argc, argv, AtZtePtnPppLookupEntryPrioritySet);
    }

eBool CmdAtZtePtnMlpppLookupEntryEncapTypeSet(char argc, char **argv)
    {
    return EntryUint8AttributeSet(argc, argv, AtZtePtnMlpppLookupEntryEncapTypeSet);
    }

eBool CmdAtZtePtnCHdlcLookupEntryPrioritySet(char argc, char **argv)
    {
    return CHdlcLookupEntrySet(argc, argv, AtZtePtnCHdlcLookupEntryPrioritySet);
    }

eBool CmdAtZtePtnCHdlcLookupEntryCfiSet(char argc, char **argv)
    {
    return CHdlcLookupEntrySet(argc, argv, AtZtePtnCHdlcLookupEntryCfiSet);
    }

eBool CmdAtZtePtnCHdlcLookupEncaptypeSet(char argc, char **argv)
    {
    eBool success = cAtTrue;
    uint8 encapType = (uint8)AtStrToDw(argv[0]);
    eAtRet ret;
	AtUnused(argc);

    ret = AtZtePtnCHdlcLookupEntryEncapTypeSet(CliDevice(), encapType);
    if (ret != cAtOk)
       {
       AtPrintc(cSevCritical, "ERROR: Cannot set encapType ret = %s\r\n", AtRet2String(ret));
       success = cAtFalse;
       }

    return success;
    }

eBool CmdAtZtePtnCHdlcTableGet(char argc, char **argv)
    {
    tTab *tabPtr;
    uint8 type_i;
    static const uint8 cNumberType = 3;
    const char *heading[] = {"Type", "EncapType", "PRI", "CFI"};
	AtUnused(argv);
	AtUnused(argc);

     /* Create table */
     tabPtr = TableAlloc(cNumberType, mCount(heading), heading);
     if (!tabPtr)
         {
         AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
         return cAtFalse;
         }

     for (type_i = 0; type_i < cNumberType; type_i++)
         {
         const char *string;
         eBool convertSuccess;
         eAtZtePtnCHdlcPktType pktType = (eAtZtePtnCHdlcPktType)cAtZtePtnCHdlcPktTypeVal[type_i];

         string = CliEnumToString(pktType,
                                  cAtZtePtnCHdlcPktTypeStr,
                                  cAtZtePtnCHdlcPktTypeVal,
                                  mCount(cAtZtePtnCHdlcPktTypeVal),
                                  &convertSuccess);
         StrToCell(tabPtr, type_i, 0, convertSuccess ? string : "Error");
         StrToCell(tabPtr, type_i, 1, CliNumber2String(AtZtePtnCHdlcLookupEntryEncapTypeGet(CliDevice()), "0x%04x"));
         StrToCell(tabPtr, type_i, 2, CliNumber2String(AtZtePtnCHdlcLookupEntryPriorityGet(CliDevice(), (eAtZtePtnCHdlcPktType)pktType), "%d"));
         StrToCell(tabPtr, type_i, 3, CliNumber2String(AtZtePtnCHdlcLookupEntryCfiGet(CliDevice(), (eAtZtePtnCHdlcPktType)pktType), "%d"));
         }

     TablePrint(tabPtr);
     TableFree(tabPtr);

     return cAtTrue;
    }

eBool CmdAtZtePtnFrLookupEntryCfiSet(char argc, char **argv)
    {
    return FrameRelayAttributeSet(argc, argv, AtZtePtnFrLookupEntryCfiSet);
    }

eBool CmdAtZtePtnFrLookupEntryPrioritySet(char argc, char **argv)
    {
    return FrameRelayAttributeSet(argc, argv, AtZtePtnFrLookupEntryPrioritySet);
    }

eBool CmdAtZtePtnFrLookupEncaptypeSet(char argc, char **argv)
    {
    return FrameRelayAttributeSet(argc, argv, AtZtePtnFrLookupEntryEncapTypeSet);
    }

eBool CmdAtZtePtnFrTableGet(char argc, char **argv)
    {
    tTab *tabPtr;
    static const uint8 numRow = 1;
    const char *heading[] = {"EncapType", "Priority", "CFI"};
	AtUnused(argv);
	AtUnused(argc);

    /* Create table */
    tabPtr = TableAlloc(numRow, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, 0, CliNumber2String(AtZtePtnFrLookupEntryEncapTypeGet(CliDevice()), "%u"));
    StrToCell(tabPtr, 0, 1, CliNumber2String(AtZtePtnFrLookupEntryPriorityGet(CliDevice()), "%u"));
    StrToCell(tabPtr, 0, 2, CliNumber2String(AtZtePtnFrLookupEntryCfiGet(CliDevice()), "%u"));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool HigigHeaderSet(char argc,
                            char **argv,
                            eAtRet (*HigigHeaderSetFunc)(AtEthFlow self,
                                                         uint8 ethPortId,
                                                         uint8 stmPortId,
                                                         const tAtZtePtnTag1 *zteTag1,
                                                         const tAtZtePtnTag2 *zteTag2),
                            AtEthFlow* (*FlowListGet)(char *idList, uint32 *numFlow))
    {
    AtEthFlow *flows;
    tAtZtePtnTag1 tag1;
    tAtZtePtnTag2 tag2;
    eBool result = cAtTrue;
    uint32 numFlows, flow_i;
    uint8 stmPortId, ethPortId;
	AtUnused(argc);

    /* Get PW list */
    flows = FlowListGet(argv[0], &numFlows);
    if (flows == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    /* Get Port Id */
    ethPortId = (uint8)(AtStrToDw(argv[1]) - 1);
    stmPortId = (uint8)AtStrToDw(argv[2]);

    /* Parse tag 1 */
    if (CliAtZtePtnStr2Tag1(argv[3], &tag1) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 1, expect %s\r\n", CliAtZtePtnTag1Format());
        return cAtFalse;
        }

    /* Parse tag 2 */
    if (CliAtZtePtnStr2Tag2(argv[4], &tag2) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 2, expect %s\r\n", CliAtZtePtnTag2Format());
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        eAtRet ret = HigigHeaderSetFunc(flows[flow_i], ethPortId, stmPortId, &tag1, &tag2);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure STM Port for flow %s, ret = %s\r\n",
                    AtObjectToString((AtObject)flows[flow_i]),
                    AtRet2String(ret));
            result = cAtFalse;
            }
        }

    return result;
    }

eBool CmdAtZteEthFlowHigigTxHeaderSet(char argc, char **argv)
    {
    return HigigHeaderSet(argc, argv, AtZtePtnEthFlowHigigTxHeaderSet, CliEthFlowListGet);
    }

eBool CmdAtZteEthFlowHigigExpectedHeaderSet(char argc, char **argv)
    {
    return HigigHeaderSet(argc, argv, AtZtePtnEthFlowHigigExpectedHeaderSet, CliEthFlowListGet);
    }

eBool CmdAtHdlcLinkOamHigigTxEthHeaderSet(char argc, char **argv)
    {
    return HigigHeaderSet(argc, argv, AtZtePtnEthFlowHigigTxHeaderSet, CliHdlcLinkOamFlowListGet);
    }

eBool CmdAtHdlcLinkOamExpectedEthHigigHeaderSet(char argc, char **argv)
    {
    return HigigHeaderSet(argc, argv, AtZtePtnEthFlowHigigExpectedHeaderSet, CliHdlcLinkOamFlowListGet);
    }


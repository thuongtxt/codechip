/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtBdcom.c
 *
 * Created Date: Oct 5, 2013
 *
 * Description : CLI of BDCOM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtBdcom.h"
#include "AtHalBdcom.h"
#include "AtEthFlow.h"
#include "../../encap/CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*TagSet)(AtEthFlow, uint8, const tAtBdcomTag *);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PppLinksFromString(char *pppLinkListString)
    {
    AtList validLinks;
    uint32 *idBuf;
    uint32 bufferSize, numLinks, i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numLinks = CliIdListFromString(pppLinkListString, idBuf, bufferSize)) == 0)
        return NULL;

    validLinks = AtListCreate(AtModuleEncapMaxChannelsGet(encapModule));
    if (validLinks == NULL)
        return NULL;

    for (i = 0; i < numLinks; i++)
        {
        AtPppLink pppLink = (AtPppLink)HdlcLink(idBuf[i]);
        if (pppLink)
            AtListObjectAdd(validLinks, (AtObject)pppLink);
        }

    if (AtListLengthGet(validLinks) == 0)
        {
        AtObjectDelete((AtObject)validLinks);
        return NULL;
        }

    return validLinks;
    }

static char *SharedBuffer(void)
    {
    static char sharedCharBuffer[128];
    return sharedCharBuffer;
    }

static tAtBdcomTag *StrToBdcomTag(char *tagStr, tAtBdcomTag *tag)
    {
    uint32 bdComTagBuf[12];
    uint32 numItem;
    static char minFormat[] = "0.0.0.0.0.0";
    static char maxFormat[] = "1.15.15.15.1023.63";

    IdComplexNumParse((void*)tagStr, minFormat, maxFormat, 12, bdComTagBuf, &numItem);
    if (numItem != 6)
        return NULL;

    tag->cpuPktIndicator  = (uint8)bdComTagBuf[0];
    tag->portNumber       = (uint8)bdComTagBuf[1];
    tag->encapType        = (uint8)bdComTagBuf[2];
    tag->slotNumber       = (uint8)bdComTagBuf[3];
    tag->channelNumber    = (uint16)bdComTagBuf[4];
    tag->pktLen           = (uint8)bdComTagBuf[5];

    return tag;
    }

static char *BdcomTagToStr(const tAtBdcomTag *tag)
    {
    char *buffer = SharedBuffer();

    if (tag == NULL)
        {
        AtSprintf(buffer, "none");
        return buffer;
        }

    AtSprintf(buffer,
              "%d.%d.%d.%d.%d.%d",
              tag->cpuPktIndicator,
              tag->portNumber,
              tag->encapType,
              tag->slotNumber,
              tag->channelNumber,
              tag->pktLen);

    return buffer;
    }

static const char *BdcomTagFormat(void)
    {
    return "<cpuPktIndicator:0/1>.<portNumber:0..15>.<encapType:0..15>."
            "<slotNumber:0..15>.<channelNumber:0..1023>.<packetLength:0..63>";
    }

static eBool EthFlowBdcomTagSet(char argc, char **argv, TagSet BdcomTagSet, AtEthFlow* (*FlowListGet)(char *idList, uint32 *numFlow))
    {
    AtEthFlow *flows;
    tAtBdcomTag tag;
    uint32 numFlows, flow_i;
    eBool result = cAtTrue;
    eAtRet ret = cAtOk;
    uint8 ethPortId;
	AtUnused(argc);

    /* Get PW list */
    flows = FlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "WARNING: ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    /* Get Port Id and BDcom tag */
    ethPortId = (uint8)(AtStrToDw(argv[1]) - 1);
    if (StrToBdcomTag(argv[2], &tag) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag 1, expect %s\r\n", BdcomTagFormat());
        return cAtFalse;
        }

    /* Apply configuration */
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        ret |= BdcomTagSet(flows[flow_i], ethPortId, &tag);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)flows[flow_i]),
                     AtRet2String(ret));
            result = cAtFalse;
            }
        }

    return result;
    }

static eBool EthFlowBdcomTagGet(char argc, char **argv, AtEthFlow* (*FlowListGet)(char *idList, uint32 *numFlow))
    {
    AtEthFlow     *flows;
    uint16        i, column;
    uint32        numFlows;
    tTab          *tabPtr;
    const char    *heading[] = {"Id", "txBdcomTag", "expBdcomTag", "ethPort"};
	AtUnused(argc);

    flows = FlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "WARNING: ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numFlows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numFlows; i++)
        {
        tAtEthVlanDesc vlanDesc;
        tAtBdcomTag tag;

        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)flows[i]));

        /* Tags */
        if (AtBdcomEthFlowTagGet(flows[i], &tag) != cAtOk)
            StrToCell(tabPtr, i, column++, "Error");
        else
            StrToCell(tabPtr, i, column++, BdcomTagToStr(&tag));

        if (AtBdcomEthFlowExpectedTagGet(flows[i], &tag) != cAtOk)
            StrToCell(tabPtr, i, column++, "Error");
        else
            StrToCell(tabPtr, i, column++, BdcomTagToStr(&tag));

        /* ETH port */
        if (AtEthFlowIngressVlanAtIndex(flows[i], 0, &vlanDesc))
            AtSprintf(SharedBuffer(), "%d", vlanDesc.ethPortId + 1);
        else
            AtSprintf(SharedBuffer(), "none");
        StrToCell(tabPtr, i, column++, SharedBuffer());
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtPrintc(cSevInfo, "Format:\r\n");
    AtPrintc(cSevNormal, "- BdcomTag: cpuPktIndicator.portNumber.encapType.slotNumber.channelNumber.packetLength\r\n");

    return cAtTrue;
    }

static uint32 *EntryListFromString(char *idListStr, uint32 *numEntries)
    {
    const uint8 numEntryEachLink = 2;
    uint32 i, numIds, numValidEntries = 0;
    uint32 *idList;

    idList = CliSharedIdBufferGet(&numIds);
    numIds = CliIdListFromString(idListStr, idList, numIds);

    if (numEntries == NULL)
        return NULL;

    if (numIds > numEntryEachLink)
        {
        *numEntries = numValidEntries;
        return idList;
        }

    for (i = 0; i < numIds; i++)
        {
        idList[numValidEntries] = idList[i];
        numValidEntries = numValidEntries + 1;
        }

    *numEntries = numValidEntries;
    return idList;
    }

static eBool PppHeaderSet(char argc, char **argv, eAtRet (*AttributeSetFunc)(AtPppLink self, uint8 entryId, uint32 value, uint8 headerLength))
    {
    AtList pppLinkList;
    uint8  headerLength;
    uint32 numLinks, numEntries, entry_i, link_i, value;
    uint32 *entryList;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of links */
    pppLinkList = PppLinksFromString(argv[0]);
    if (pppLinkList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PPP link, the ID list may be wrong or all of them are invalid\r\n");
        return cAtFalse;
        }

    numLinks = AtListLengthGet(pppLinkList);

    /* Get list of entries */
    entryList = EntryListFromString(argv[1], &numEntries);
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No entry, the ID list may be wrong or all of them are invalid\r\n");
        AtObjectDelete((AtObject)pppLinkList);
        return cAtTrue;
        }

    /* And attribute value */
    value = AtStrToDw(argv[2]);
    headerLength = (uint8)AtStrToDw(argv[3]);

    /* Apply all */
    for (link_i = 0; link_i < numLinks; link_i++)
        {
        for (entry_i = 0; entry_i < numEntries; entry_i++)
            {
            AtPppLink pppLink = (AtPppLink)AtListObjectGet(pppLinkList, link_i);
            eAtRet ret = AttributeSetFunc(pppLink, (uint8)(entryList[entry_i] - 1), value, headerLength);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot configure link %u, entry %u, ret = %s\r\n",
                         AtChannelIdGet((AtChannel)pppLink) + 1, entryList[entry_i], AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    AtObjectDelete((AtObject)pppLinkList);
    return success;
    }

static eBool EthTypeSet(char argc, char **argv, eAtRet (*AttributeSetFunc)(AtPppLink self, uint8 entryId, uint32 value))
    {
    AtList pppLinkList;
    uint32 numLinks, numEntries, entry_i, link_i, value;
    uint32 *entryList;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of links */
    pppLinkList = PppLinksFromString(argv[0]);
    if (pppLinkList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PPP link, the ID list may be wrong or all of them are invalid\r\n");
        return cAtFalse;
        }

    numLinks = AtListLengthGet(pppLinkList);

    /* Get list of entries */
    entryList = EntryListFromString(argv[1], &numEntries);
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No entry, the ID list may be wrong or all of them are invalid\r\n");
        AtObjectDelete((AtObject)pppLinkList);
        return cAtTrue;
        }

    /* And attribute value */
    value = AtStrToDw(argv[2]);

    /* Apply all */
    for (link_i = 0; link_i < numLinks; link_i++)
        {
        for (entry_i = 0; entry_i < numEntries; entry_i++)
            {
            AtPppLink pppLink = (AtPppLink)AtListObjectGet(pppLinkList, link_i);
            eAtRet ret = AttributeSetFunc(pppLink, (uint8)(entryList[entry_i] - 1), value);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot configure link %u, entry %u, ret = %s\r\n",
                         AtChannelIdGet((AtChannel)pppLink) + 1, entryList[entry_i], AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    AtObjectDelete((AtObject)pppLinkList);
    return success;
    }

static AtHal Mdio(void)
    {
    return AtHalBdcomMdio(AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0)));
    }

eBool CmdAtBdcomEthFlowTxHeaderSet(char argc, char **argv)
    {
    return EthFlowBdcomTagSet(argc, argv, AtBdcomEthFlowTxHeaderSet, CliEthFlowListGet);
    }

eBool CmdAtBdcomEthFlowExpectedHeaderSet(char argc, char **argv)
    {
    return EthFlowBdcomTagSet(argc, argv, AtBdcomEthFlowExpectedHeaderSet, CliEthFlowListGet);
    }

eBool CmdEthFlowBdcomHeaderShow(char argc, char **argv)
    {
    return EthFlowBdcomTagGet(argc, argv, CliEthFlowListGet);
    }

eBool CmdBdcomTdmToPsnPppHeaderSet(char argc, char **argv)
    {
    return PppHeaderSet(argc, argv, AtBdcomTdmToPsnPppHeaderSet);
    }

eBool CmdBdcomPsnToTdmPppHeaderSet(char argc, char **argv)
    {
    return PppHeaderSet(argc, argv, AtBdcomPsnToTdmPppHeaderSet);
    }

eBool CmdBdcomTdmToPsnEthTypeSet(char argc, char **argv)
    {
    return EthTypeSet(argc, argv, AtBdcomTdmToPsnEthTypeSet);
    }

eBool CmdBdcomPsnToTdmEthTypeSet(char argc, char **argv)
    {
    return EthTypeSet(argc, argv, AtBdcomPsnToTdmEthTypeSet);
    }

eBool CmdBdcomPppTableEntryGet(char argc, char **argv)
    {
    AtList pppLinkList;
    uint8 entry_i;
    uint32 link_i;
    uint32 numLinks;
    tTab *tabPtr;
    const uint8 numEntryEachLink = 2;
    char buffer[16];
    const char *heading[] = {"link.entry",
                             "PPPHeader(psn2tdm)", "PPPHeaderLength(psn2tdm)", "ethtype(psn2tdm)",
                             "PPPHeader(tdm2psn)", "PPPHeaderLength(tdm2psn)", "ethtype(tdm2psn)"};
	AtUnused(argc);

    /* Get list of links */
    pppLinkList = PppLinksFromString(argv[0]);
    if (pppLinkList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PPP link, the ID list may be wrong or all of them are invalid\r\n");
        return cAtFalse;
        }

    numLinks = AtListLengthGet(pppLinkList);

     /* Make table */
     tabPtr = TableAlloc(numLinks * numEntryEachLink, mCount(heading), heading);
     if (!tabPtr)
         {
         AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
         AtObjectDelete((AtObject)pppLinkList);
         return cAtFalse;
         }

     /* Show table */
     for (link_i = 0; link_i < numLinks; link_i++)
         {
         for (entry_i = 0; entry_i < numEntryEachLink; entry_i++)
             {
             uint32 pppHeader;
             uint8 headerLength;
             AtPppLink pppLink = (AtPppLink)AtListObjectGet(pppLinkList, link_i);

             AtSprintf(buffer, "%u.%u", AtChannelIdGet((AtChannel)pppLink) + 1, entry_i + 1);
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 0, buffer);

             pppHeader = AtBdcomPsnToTdmPppHeaderGet(pppLink, entry_i, &headerLength);
             AtSprintf(buffer, "0x%04x", pppHeader);
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 1, buffer);
             AtSprintf(buffer, "%d", headerLength);
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 2, buffer);
             AtSprintf(buffer, "0x%04x", AtBdcomPsnToTdmEthTypeGet(pppLink, entry_i));
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 3, buffer);

             pppHeader = AtBdcomTdmToPsnPppHeaderGet(pppLink, entry_i, &headerLength);
             AtSprintf(buffer, "0x%04x", pppHeader);
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 4, buffer);
             AtSprintf(buffer, "%d", headerLength);
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 5, buffer);
             AtSprintf(buffer, "0x%04x", AtBdcomTdmToPsnEthTypeGet(pppLink, entry_i));
             StrToCell(tabPtr, link_i * numEntryEachLink + entry_i, 6, buffer);
             }
         }

     TablePrint(tabPtr);
     TableFree(tabPtr);
     AtObjectDelete((AtObject)pppLinkList);

     return cAtTrue;
    }

eBool CmdAtBdcomHdlcLinkOamTxEthHeaderSet(char argc, char **argv)
    {
    return EthFlowBdcomTagSet(argc, argv, AtBdcomEthFlowTxHeaderSet, CliHdlcLinkOamFlowListGet);
    }

eBool CmdAtBdcomHdlcLinkOamExpectedEthHeaderSet(char argc, char **argv)
    {
    return EthFlowBdcomTagSet(argc, argv, AtBdcomEthFlowExpectedHeaderSet, CliHdlcLinkOamFlowListGet);
    }

eBool CmdAtBdcomHdlcLinkOamHeaderShow(char argc, char **argv)
    {
    return EthFlowBdcomTagGet(argc, argv, CliHdlcLinkOamFlowListGet);
    }

eBool CmdAtBdcomMdioRead(char argc, char **argv)
    {
    uint32 address;
	AtUnused(argc);

    StrToDw(argv[0], 'h', &address);

    if (Mdio() == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No MDIO\r\n");
        return cAtFalse;
        }

    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", AtHalRead(Mdio(), address));

    return cAtTrue;
    }

eBool CmdAtBdcomMdioWrite(char  argc, char  **argv)
    {
    uint32 address, value;
	AtUnused(argc);

    StrToDw(argv[0], 'h', &address);
    StrToDw(argv[1], 'h', &value);

    if (Mdio() == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No MDIO\r\n");
        return cAtFalse;
        }

    AtHalWrite(Mdio(), address, value);
    AtPrintc(cSevNormal, "Write address: 0x%x, value: 0x%x\r\n", address, value);

    return cAtTrue;
    }

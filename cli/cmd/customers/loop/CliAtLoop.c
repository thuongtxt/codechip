/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtLoop.c
 *
 * Created Date: Mar 10, 2014
 *
 * Description : Loop specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtHalLoop.h"
#include "AtLoop.h"

/*--------------------------- Define -----------------------------------------*/
#define cDefaultEthPortId   1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PortIdIsValid(uint8 portId)
    {
    return ((portId > 0) && (portId <= AtModuleEthMaxPortsGet((AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth)))) ? cAtTrue : cAtFalse;
    }

static AtHal SerdesHal(uint8 portId)
    {
    if (!PortIdIsValid(portId))
        return NULL;
    return AtHalLoopGeSerdes(AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0)), (uint8)(portId - 1));
    }

static void CliTemperatureAlarmPrint(uint32 alarmMask)
    {
    if (alarmMask & cAtLoopTemperatureAlarmRed)
        {
        AtPrintc(cSevCritical, "Red");
        return;
        }

    if (alarmMask & cAtLoopTemperatureAlarmYellow)
        {
        AtPrintc(cSevWarning, "Yellow");
        return;
        }

    AtPrintc(cSevInfo, "Normal");
    }

static const char *RebootModeString(eBool rebootEnable)
    {
    return (rebootEnable) ? "enable" : "disable";
    }

static eBool AttributeSet(char argc, char **argv, eAtRet (*AttributeSetFunc)(AtDevice, uint32))
    {
    eBool success = cAtTrue;
    eAtRet ret = AttributeSetFunc(CliDevice(), AtStrToDw(argv[0]));
    AtUnused(argc);
    AtUnused(argv);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Can not set this temperature attribute, ret = %s\r\n", AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool DeviceLedStateSet(char argc, char** argv, eAtLoopLedType color, eAtRet (*DeviceLedSetFunc)(AtDevice, eAtLoopLedType, eAtLedState))
    {
    eAtRet ret = cAtOk;
    eAtLedState ledState;

    AtUnused(argc);

    ledState = CliLedStateFromStr(argv[0]);
    if (!AtLedStateIsValid(ledState))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid LED state\r\n");
        return cAtFalse;
        }

    ret = DeviceLedSetFunc(CliDevice(), color, ledState);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Can not set LED state for device, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }


static eBool SdhLineLedStateSet(char argc, char** argv, eAtLoopLedType color)
    {
    uint32 numLines, i;
    uint32 *lineIds;
    eAtLedState ledState;
    eBool success = cAtTrue;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);

    AtUnused(argc);

    /* Get SDH line */
    lineIds = CliSharedIdBufferGet(&numLines);
    numLines = CliIdListFromString(argv[0], lineIds, numLines);
    if (numLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Line ID list, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    /* Get LED state */
    ledState = CliLedStateFromStr(argv[1]);
    if (!AtLedStateIsValid(ledState))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid LED state\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numLines; i++)
        {
        eAtRet ret = cAtOk;
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, (uint8)(lineIds[i] - 1));

        if (line == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Line %d does not exist, it is ignored\r\n", lineIds[i]);
            continue;
            }

        ret = AtLoopSdhLineLedStateSet(line, color, ledState);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not set LED state for %s, ret = %s\r\n", CliChannelIdStringGet((AtChannel)line), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtLoopSerdesRead(char argc, char **argv)
    {
    uint32 address;
    uint8 portId;
    AtHal serdesHal;

    /* Not enough parameter */
    if(argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Too many parameters */
    if (argc > 2)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    StrToDw(argv[0], 'h', &address);
    portId = (uint8)((argc == 2) ? AtStrToDw(argv[1]) : cDefaultEthPortId);


    serdesHal = SerdesHal(portId);
    if (serdesHal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No Serdes HAL\r\n");
        return cAtFalse;
        }

    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", AtHalRead(serdesHal, address));

    return cAtTrue;
    }


eBool CmdAtLoopSerdesWrite(char  argc, char  **argv)
    {
    uint32 address, value;
    uint8 portId;
    AtHal serdesHal;

    /* Not enough parameter */
    if(argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Too many parameters */
    if (argc > 3)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    StrToDw(argv[0], 'h', &address);
    StrToDw(argv[1], 'h', &value);
    portId = (uint8)((argc == 3) ? AtStrToDw(argv[2]) : cDefaultEthPortId);

    serdesHal = SerdesHal(portId);
    if (serdesHal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No Serdes HAL\r\n");
        return cAtFalse;
        }

    AtHalWrite(serdesHal, address, value);
    AtPrintc(cSevNormal, "Write address: 0x%x, value: 0x%x\r\n", address, value);

    return cAtTrue;
    }

eBool CmdAtLoopTemperatureShow(char argc, char **argv)
    {
    uint32 alarmMask = AtLoopTemperatureAlarmGet(CliDevice());
    eBool rebootEnable = AtLoopTemperatureRebootWhenRedAlarmIsEnabled(CliDevice());
    AtUnused(argc);
    AtUnused(argv);

    AtPrintc(cSevInfo, "Status of FPGA temperature:\r\n");
    AtPrintc(cSevNormal, "* Temperature: %u (oC)\r\n", AtLoopTemperatureGet(CliDevice()));
    AtPrintc(cSevNormal, "* Alarm: ");
    CliTemperatureAlarmPrint(alarmMask);
    AtPrintc(cSevInfo, "\r\n\r\n");

    AtPrintc(cSevInfo, "Configuration of FPGA temperature:\r\n");
    AtPrintc(cSevNormal, "* RED alarm SET threshold:      %u (oC)\r\n", AtLoopTemperatureRedAlarmSetThresholdGet(CliDevice()));
    AtPrintc(cSevNormal, "* RED alarm CLEAR threshold:    %u (oC)\r\n", AtLoopTemperatureRedAlarmClearThresholdGet(CliDevice()));
    AtPrintc(cSevNormal, "* YELLOW alarm SET threshold:   %u (oC)\r\n", AtLoopTemperatureYellowAlarmSetThresholdGet(CliDevice()));
    AtPrintc(cSevNormal, "* YELLOW alarm CLEAR threshold: %u (oC)\r\n", AtLoopTemperatureYellowAlarmClearThresholdGet(CliDevice()));
    AtPrintc(cSevNormal, "* Reboot option: %s\r\n", RebootModeString(rebootEnable));

    return cAtTrue;
    }

eBool CmdAtLoopTemperatureRedAlarmRebootEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return (AtLoopTemperatureRebootWhenRedAlarmEnable(CliDevice(), cAtTrue) == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdAtLoopTemperatureRedAlarmRebootDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return (AtLoopTemperatureRebootWhenRedAlarmEnable(CliDevice(), cAtFalse) == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdAtLoopTemperatureRedAlarmSetThreshold(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtLoopTemperatureRedAlarmSetThresholdSet);
    }

eBool CmdAtLoopTemperatureRedAlarmClearThreshold(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtLoopTemperatureRedAlarmClearThresholdSet);
    }

eBool CmdAtLoopTemperatureYellowAlarmSetThreshold(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtLoopTemperatureYellowAlarmSetThresholdSet);
    }

eBool CmdAtLoopTemperatureYellowAlarmClearThreshold(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtLoopTemperatureYellowAlarmClearThresholdSet);
    }

eBool CmdAtLoopDeviceRedLedStateSet(char argc, char** argv)
    {
    return DeviceLedStateSet(argc, argv, cAtLoopLedTypeRed, AtLoopDeviceStatusLedStateSet);
    }

eBool CmdAtLoopDeviceGreenLedStateSet(char argc, char** argv)
    {
    return DeviceLedStateSet(argc, argv, cAtLoopLedTypeGreen, AtLoopDeviceStatusLedStateSet);
    }

eBool CmdAtLoopDeviceRedLedActivationSet(char argc, char** argv)
    {
    return DeviceLedStateSet(argc, argv, cAtLoopLedTypeRed, AtLoopDeviceActiveLedStateSet);
    }

eBool CmdAtLoopDeviceGreenLedActivationSet(char argc, char** argv)
    {
    return DeviceLedStateSet(argc, argv, cAtLoopLedTypeGreen, AtLoopDeviceActiveLedStateSet);
    }

eBool CmdAtLoopSdhLineRedLedStateSet(char argc, char** argv)
    {
    return SdhLineLedStateSet(argc, argv, cAtLoopLedTypeRed);
    }

eBool CmdAtLoopSdhLineGreenLedStateSet(char argc, char** argv)
    {
    return SdhLineLedStateSet(argc, argv, cAtLoopLedTypeGreen);
    }

eBool CmdAtLoopSdhLineStateShow(char argc, char** argv)
    {
    eAtLedState ledState;
    uint32 numLines, i;
    uint32 *lineIds;
    tTab *tabPtr;
    eBool sfpExist;
    const char *stringValue;
    const char *pHeading[] = {"Line", "RedLed", "GreenLed", "SFP"};
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);

    AtUnused(argc);

    /* Get SDH line */
    lineIds = CliSharedIdBufferGet(&numLines);
    numLines = CliIdListFromString(argv[0], lineIds, numLines);
    if (numLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Line ID list, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numLines; i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, (uint8)(lineIds[i] - 1));

        /* Print ID */
        StrToCell(tabPtr, i, 0, CliNumber2String(lineIds[i], "%d"));

        if (line == NULL)
            {
            ColorStrToCell(tabPtr, i, 1, "Error", cSevCritical);
            ColorStrToCell(tabPtr, i, 2, "Error", cSevCritical);
            ColorStrToCell(tabPtr, i, 3, "Error", cSevCritical);
            continue;
            }

        ledState = AtLoopSdhLineLedStateGet(line, cAtLoopLedTypeRed);
        ColorStrToCell(tabPtr, i, 1, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

        ledState = AtLoopSdhLineLedStateGet(line, cAtLoopLedTypeGreen);
        ColorStrToCell(tabPtr, i, 2, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

        sfpExist = AtLoopSdhLineSfpExisted(line);
        stringValue = sfpExist ? "existed" : "not exist";
        ColorStrToCell(tabPtr, i, 3, stringValue, sfpExist ? cSevInfo : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtLoopDeviceLedShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"StatusRed", "StatusGreen", "ActiveRed", "ActiveGreen"};
    eAtLedState ledState;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    ledState = AtLoopDeviceStatusLedStateGet(CliDevice(), cAtLoopLedTypeRed);
    ColorStrToCell(tabPtr, 0, 0, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

    ledState = AtLoopDeviceStatusLedStateGet(CliDevice(), cAtLoopLedTypeGreen);
    ColorStrToCell(tabPtr, 0, 1, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

    ledState = AtLoopDeviceActiveLedStateGet(CliDevice(), cAtLoopLedTypeRed);
    ColorStrToCell(tabPtr, 0, 2, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

    ledState = AtLoopDeviceActiveLedStateGet(CliDevice(), cAtLoopLedTypeGreen);
    ColorStrToCell(tabPtr, 0, 3, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

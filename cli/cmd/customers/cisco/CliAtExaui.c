/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet module
 *
 * File        : CliAtExaui.c
 *
 * Created Date: May 7, 2016
 *
 * Description : eXAUI CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtCisco.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/
#define cCliAtCiscoXauiPathMax      2*256

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static AtIdParser ChannelIdParserCreate(char *pStrIdList)
    {
    static char minFormat[] = "1.1";
    char buf[16];
    AtSnprintf(buf, sizeof(buf), "%u.%u", AtModuleEthNumExauiGet(EthModule()), 256);
    return AtIdParserNew(pStrIdList, minFormat, buf);
    }

static eBool VlanGet(char *pStrVlan, tAtVlan *vlan)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(pStrVlan, ".");

    if (AtTokenizerNumStrings(tokenizer) != 4)
        {
        AtPrintc(cSevCritical, "ERROR: VLAN may be invalid, expected format: <tpid>.<priority>.<cfi>.<vlanId>\r\n");
        return cAtFalse;
        }

    vlan->tpid     = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));
    vlan->priority = (uint8)AtStrToDw(AtTokenizerNextString(tokenizer));
    vlan->cfi      = (uint8)AtStrToDw(AtTokenizerNextString(tokenizer));
    vlan->vlanId   = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));

    return cAtTrue;
    }

eBool CmdAtExauiIngressVlanSet(char argc, char** argv)
    {
    tAtVlan vlan;
    AtIdParser idParser = ChannelIdParserCreate(argv[0]);
    eBool success = cAtTrue;

    AtUnused(argc);

    if (!VlanGet(argv[1], &vlan))
        return cAtFalse;

    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 xauiId = AtIdParserNextNumber(idParser);
        uint32 channelId = AtIdParserNextNumber(idParser);
        AtEthPort port;
        eAtRet ret;

        /* Ignore not existing port */
        port = AtModuleEthExauiGet(EthModule(), (uint8)(xauiId - 1));
        if (port == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: ignore port %d which does not exist\r\n", xauiId);
            continue;
            }

        /* Apply the VLAN */
        ret = AtExauiIngressVlanSet(port, channelId - 1, &vlan);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Configure ingress VLAN for channel eXAUI#%u.%u fail with ret = %s\r\n", xauiId, channelId, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)idParser);

    return success;
    }

eBool CmdAtExauiEgressVlanExpectedSet(char argc, char** argv)
    {
    AtModuleEth ethModule = EthModule();
    AtIdParser idParser = ChannelIdParserCreate(argv[0]);
    uint16 vlanId = (uint16)AtStrToDw(argv[1]);
    eBool success = cAtTrue;

    AtUnused(argc);

    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 xauiId = AtIdParserNextNumber(idParser);
        uint32 channelId = AtIdParserNextNumber(idParser);
        AtEthPort port;
        eAtRet ret;

        /* Ignore not existing port */
        port = AtModuleEthExauiGet(ethModule, (uint8)(xauiId - 1));
        if (port == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: ignore port %d which does not exist\r\n", xauiId);
            continue;
            }

        /* Apply the VLAN */
        ret = AtExauiEgressExpectedVlanIdSet(port, channelId - 1, vlanId);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Configure expected VLAN for channel eXAUI#%u.%u fail with ret = %s\r\n", xauiId, channelId, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)idParser);

    return success;
    }

eBool CmdAtExauiShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"Paths", "IngressVlan", "EgressExpectedVlanId"};
    char buffStr[50];
    uint16 expectedVlanId;
    AtIdParser idParser = ChannelIdParserCreate(argv[0]);
    uint32 rowId, numRows;
    uint32 numValidRows = 0;

    AtUnused(argc);

    numRows = AtIdParserNumNumbers(idParser) / 2;
    tabPtr = TableAlloc(numRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    for (rowId = 0; rowId < numRows; rowId++)
        {
        uint32 xauiId = AtIdParserNextNumber(idParser);
        uint32 channelId = AtIdParserNextNumber(idParser);
        AtEthPort port;
        eAtRet ret;
        uint8 column = 0;
        tAtVlan vlan;

        /* Ignore not existing channels */
        port = AtModuleEthExauiGet(EthModule(), (uint8)(xauiId - 1));
        if (port == NULL)
            continue;

        /* Channel */
        AtSprintf(buffStr, "%d.%d", xauiId, channelId);
        StrToCell(tabPtr, numValidRows, column++, buffStr);

        /* Ingress VLAN */
        ret = AtExauiIngressVlanGet(port, channelId - 1, &vlan);
        if (ret == cAtOk)
            {
            AtSprintf(buffStr, "0x%04x.%u.%u.%u", vlan.tpid, vlan.priority, vlan.cfi, vlan.vlanId);
            StrToCell(tabPtr, numValidRows, column++, buffStr);
            }
        else
            ColorStrToCell(tabPtr, numValidRows, column++, AtRet2String(ret), cSevCritical);

        /* Egress expected Vlan ID */
        expectedVlanId = AtExauiEgressExpectedVlanIdGet(port, channelId - 1);
        StrToCell(tabPtr, numValidRows, column, CliNumber2String(expectedVlanId, "%u"));

        numValidRows = numValidRows + 1;
        }

    AtObjectDelete((AtObject)idParser);
    SubTablePrint(tabPtr, 0, numValidRows - 1, 0, mCount(heading) - 1);
    TableFree(tabPtr);

    return cAtTrue;
    }

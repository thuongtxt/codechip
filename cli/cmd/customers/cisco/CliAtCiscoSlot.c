/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtCisco.c
 *
 * Created Date: Apr 10, 2015
 *
 * Description : Cisco specific CLIs
 *
 * Notes       : device and offset is as following
 *
 *               Slot 0: /dev/mem30:0x420000000
 *               Slot 1: /dev/mem31:0x4A0000000
 *               Slot 2: /dev/mem32:0x520000000
 *               Slot 3: /dev/mem33:0x5A0000000
 *               Slot 4: /dev/mem34:0x620000000
 *               Slot 5: /dev/mem35:0x6A0000000
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

#include "atclib.h"
#include "apputil.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtHalCisco.h"
#include "AtTextUI.h"
#include "AtCliModule.h"
#include "FpgaVersion.h"
#include "../../../../driver/src/generic/man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumDevices 10
#define cInvalidAddress 0xFFFFFFFF
#define cIOSMmapSize 0x20000000

/*--------------------------- Macros -----------------------------------------*/
#define mRead(address) *((volatile uint32 *)(AtSize)address)

#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool deviceIdsInitialized = cAtFalse;
static AtDevice devices[cMaxNumDevices];
static tAtTextUIListerner* listener = NULL;
static uint32 baseAddresses[cMaxNumDevices];

/*--------------------------- Forward declarations ---------------------------*/
extern void *mmap64 (void * addr, size_t len, int prot, int flags, int fildes, long long off);
extern void AppAllCoresInterruptListen(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
static void AllBaseAddressesInit(void)
    {
    uint8 slot_i;
    for (slot_i = 0; slot_i < cMaxNumDevices; slot_i++)
        baseAddresses[slot_i] = cInvalidAddress;
    }

static AtDevice DeviceFromSlotId(uint8 slotId)
    {
    if (deviceIdsInitialized == cAtFalse)
        {
        AtOsalMemInit(devices, 0x0, cMaxNumDevices * sizeof(AtDevice));
        AllBaseAddressesInit();
        deviceIdsInitialized = cAtTrue;
        }

    if (slotId < AtDriverMaxNumberOfDevicesGet(AtDriverSharedDriverGet()))
        return devices[slotId];

    return NULL;
    }

static void SetDeviceToSlotId(uint8 slotId, AtDevice device)
    {
    if (deviceIdsInitialized == cAtFalse)
        {
        AtOsalMemInit(devices, 0x0, cMaxNumDevices * sizeof(AtDevice));
        deviceIdsInitialized = cAtTrue;
        }

    devices[slotId] = device;
    }

static int FileExist(const char *filename)
    {
    struct stat buffer;
    return (stat(filename, &buffer) == 0) ? cAtTrue : cAtFalse;
    }

static const char * DiagMemoryDevice(uint8 slotId)
    {
    const char* deviceFile;
    switch (slotId)
        {
        case 0: deviceFile = "/dev/mem30"; break;
        case 1: deviceFile = "/dev/mem31"; break;
        case 2: deviceFile = "/dev/mem32"; break;
        case 3: deviceFile = "/dev/mem33"; break;
        case 4: deviceFile = "/dev/mem34"; break;
        case 5: deviceFile = "/dev/mem35"; break;

        /* Impossible, but... */
        default:
            deviceFile = "/dev/mem30";
        }

    if (FileExist(deviceFile))
        return deviceFile;

    return "/dev/mem";
    }

static long long DiagOffset(uint8 slotId)
    {
    switch (slotId)
        {
        case 0: return 0x420000000;
        case 1: return 0x4A0000000;
        case 2: return 0x520000000;
        case 3: return 0x5A0000000;
        case 4: return 0x620000000;
        case 5: return 0x6A0000000;
        default:
            return 0x420000000;
        }
    }

static uint32 DiagMemoryMap(uint8 slotId, long long memMapOffset)
    {
    int fd;
    long long offset = (long long)((memMapOffset > 0) ? memMapOffset : DiagOffset(slotId));
    void *baseAddress;

    fd = open(DiagMemoryDevice(slotId), O_RDWR);
    if (fd < 0)
        {
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidAddress;
        }

    baseAddress = mmap64(NULL, 0xA000000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
    if (baseAddress == ((void *)-1))
        {
        close(fd);
        AtPrintc(cSevCritical, "(%s, %d)ERROR: Memory mapping fail, error = %s\r\n",
                 __FILE__, __LINE__,
                 strerror(errno));
        return cInvalidAddress;
        }

    close(fd);

    return (uint32)(AtSize)baseAddress;
    }

static const char *IOSMemoryDevice(uint32 slotId)
    {
    static char device[128];
    AtSnprintf(device, sizeof(device), "/dev/cem_fpga%d", slotId);
    return device;
    }

static uint32 IOSMemoryMap(uint32 slotId)
    {
    AtSize size = cIOSMmapSize; /* 512MB */
    int fd;
    void *baseAddress;
    const char *device = IOSMemoryDevice(slotId);

    /* Assuming mmap to virtual address will not fail*/
    fd = open(device, O_RDWR | O_SYNC);
    if (0 > fd)
        {
        AtPrintc(cSevCritical, "ERROR: %s open failed\r\n", device);
        return cInvalidAddress;
        }

    baseAddress = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (baseAddress == ((void *)-1))
       {
       close(fd);
       AtPrintc(cSevCritical, "ERROR: Memory mapping fail, error = %s\r\n", strerror(errno));
       return cInvalidAddress;
       }

    return (uint32)(AtSize)baseAddress;
    }

static eBool IsIOSImage(uint8 slotId)
    {
    if (FileExist(IOSMemoryDevice(slotId)))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 MemoryMap(uint8 slotId, long long memMapOffset)
    {
    if (IsIOSImage(slotId))
        return IOSMemoryMap(slotId);
    return DiagMemoryMap(slotId, memMapOffset);
    }

static void Write(uint32 address, uint32 value)
    {
    *((volatile uint32 *)(AtSize)address) = value;
    mAsm("sync");
    (void)mRead(address);
    mAsm("sync");
    }

static uint32 Read(uint32 address)
    {
    uint32 regVal = mRead(address);
    mAsm("sync");
    return regVal;
    }

static AtHal CreateHal(uint8 slotId, long long memMapOffset)
    {
    AtHal newHal;
    uint32 baseAddress = MemoryMap(slotId, memMapOffset);
    if (baseAddress == cInvalidAddress)
        return NULL;

    /* If memory map offset is specified, need to change the max address */
    newHal = AtHalCiscoNew(baseAddress, Write, Read);
    if (memMapOffset)
        AtHalMaxAddressSet(newHal, cBit31_0);

    return newHal;
    }

static uint32 ReadProductCode(AtHal hal)
    {
    uint32 productCode = AtProductCodeGetByHal(hal);

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    return productCode;
    }

static void DeviceSetup(AtDevice device, AtHal hal)
    {
    uint8 fpga_i;

    for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
        AtIpCoreHalSet(AtDeviceIpCoreGet(device, fpga_i), hal);
    }

static eAtRet DiagUnMap(uint8 slotId, uint32 baseAddress)
    {
    eAtRet ret;
    int fd;

    fd = open(DiagMemoryDevice(slotId), O_RDWR);
    if (fd == -1)
        return cAtOk;

    ret = (munmap((void *)((size_t)baseAddress), 0xA000000) < 0) ? cAtError : cAtOk;
    close(fd);

    return ret;
    }

static eAtRet IOSUnMap(uint8 slotId, uint32 baseAddress)
    {
    eAtRet ret;
    int fd;

    fd = open(IOSMemoryDevice(slotId), O_RDWR);
    if (fd == -1)
        return cAtOk;

    ret = (munmap((void *)((size_t)baseAddress), cIOSMmapSize) < 0) ? cAtError : cAtOk;
    close(fd);

    return ret;
    }

static eAtRet UnMap(uint8 slotId, uint32 baseAddress)
    {
    if (IsIOSImage(slotId))
        return IOSUnMap(slotId, baseAddress);
    return DiagUnMap(slotId, baseAddress);
    }

static eAtRet UnMapByHal(uint8 slotId, AtHal hal)
    {
    return UnMap(slotId, AtHalBaseAddressGet(hal));
    }

static const char *SlotLockFileName(uint32 slotId)
    {
    static char fileName[64];
    AtSnprintf(fileName, sizeof(fileName), "/var/slot_%d.lock", slotId);
    return fileName;
    }

static const char *ProcessIdString(void)
    {
    static char processId[10];
    AtSnprintf(processId, sizeof(processId), "%d", getpid());
    return processId;
    }

static void TextUIWillStop(AtTextUI self, void *userData)
    {
    uint8 slot_i;
    AtUnused(self);
    AtUnused(userData);

    for (slot_i = 0; slot_i < cMaxNumDevices; slot_i++)
        {
        if (baseAddresses[slot_i] == cInvalidAddress)
            continue;

        UnMap(slot_i, baseAddresses[slot_i]);
        if (remove(SlotLockFileName(slot_i)) == -1)
            AtPrintc(cSevWarning, "WARNING: Can not remove %s\r\n", SlotLockFileName(slot_i));
        }
    }

static void BaseAddressCacheSet(uint8 slot, uint32 baseAddress)
    {
    baseAddresses[slot] = baseAddress;
    }

static void BaseAddressCacheClear(uint8 slot)
    {
    baseAddresses[slot] = cInvalidAddress;
    }

static tAtTextUIListerner* Listener(void)
    {
    static tAtTextUIListerner m_listener;
    m_listener.WillStop = TextUIWillStop;
    m_listener.DidStop = NULL;

    listener = &m_listener;
    return listener;
    }

static eBool ListenerHasBeenCreated(void)
    {
    return (listener == NULL) ? cAtFalse : cAtTrue;
    }

static void PlatformSetup(AtDevice device)
    {
    uint32 productCode = AtDeviceProductCodeGet(device);
    if (productCode == 0x60290021)
        AtDevicePlatformSet(device, 0x60210051);
    }

eBool CmdAtCiscoSlotAdd(char argc, char** argv)
    {
    uint32 *idBuf;
    uint32 bufferSize, numSlots, i;
    eBool success = cAtTrue;
    uint32 productCode = 0;
    eBool force = cAtFalse;
    int fd;
    long long memMapOffset = 0;

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numSlots = CliIdListFromString((void*)argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    if (argc > 1)
        {
        uint8 arg_i;
        for (arg_i = 1; arg_i < argc; arg_i++)
            {
            if (AtStrcmp(argv[arg_i], "force") == 0)
                force = cAtTrue;
            if (AtStrcmp(argv[arg_i], "offset") == 0)
                {
                if (arg_i < argc - 1)
                    {
                    memMapOffset = strtoll(argv[arg_i + 1], NULL, 16);
                    arg_i++;
                    }
                }
            else
                productCode = (uint32)AtStrtoul(argv[arg_i], NULL, 16);
            }
        }

    for (i = 0; i < numSlots; i++)
        {
        AtHal hal;
        AtDevice device;
        uint8 slotId = (uint8)idBuf[i];
        static char processId[10];

        if (DeviceFromSlotId(slotId) != NULL)
            {
            AtPrintc(cSevWarning, "WARNING: This slot already has a device\r\n");
            continue;
            }

        /* Create lock file to prevent other process try to use this slot */
        fd = open(SlotLockFileName(slotId), O_CREAT | O_EXCL | O_WRONLY | O_TRUNC);
        if (fd == -1)
            {
            int currentFileFd = open(SlotLockFileName(slotId), O_RDONLY);
            read(currentFileFd, processId, sizeof(processId));
            AtPrintc(cSevWarning, "WARNING: File slot_%d.lock exist, the process %s is taking this slot.\r\n", slotId, processId);
            close(currentFileFd);
            if (force == cAtFalse)
                return cAtFalse;
            }
        else
            write(fd, ProcessIdString(), strlen(ProcessIdString()));

        /* Just create file and close now */
        if ((close(fd) == -1) && (force == cAtFalse))
            {
            AtPrintc(cSevWarning, "WARNING: Can not close file %s\r\n", SlotLockFileName(slotId));
            return cAtFalse;
            }

        hal = CreateHal(slotId, memMapOffset);
        if (hal == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: HAL is NULL\r\n");
            success = cAtFalse;
            continue;
            }

        if (productCode == 0)
            productCode = ReadProductCode(hal);

        device = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
        if (device == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: device is NULL\r\n");
            success = cAtFalse;
            UnMapByHal(slotId, hal);
            AtHalDelete(hal);
            continue;
            }

        if (AppFpgaVersionNumber())
            {
            AtDeviceVersionNumberSet(device, AppFpgaVersionNumber());
            AtDeviceBuiltNumberSet(device, AppFpgaBuiltNumber());
            }

        AppHalListenerEnable(hal, cAtTrue);
        DeviceSetup(device, hal);

        /* Save deviceId to slot Index */
        SetDeviceToSlotId(slotId, device);

        BaseAddressCacheSet(slotId, AtHalBaseAddressGet(hal));
        if (!ListenerHasBeenCreated())
            AtTextUIListenerAdd(AtCliSharedTextUI(), Listener(), NULL);

        AppAllCoresInterruptListen(device);
        PlatformSetup(device);
        }

    return success;
    }

eBool CmdAtCiscoSlotRemove(char argc, char** argv)
    {
    uint32 *idBuf;
    uint32 bufferSize, numSlots, i;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numSlots = CliIdListFromString((void*)argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    for (i = 0; i < numSlots; i++)
        {
        AtHal hal;
        AtDevice device;
        uint8 slotId = (uint8)idBuf[i];

        if (DeviceFromSlotId(slotId) == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: This slot does not have any device\r\n");
            continue;
            }

        device = DeviceFromSlotId(slotId);
        hal = AtDeviceIpCoreHalGet(device, 0);
        AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

        /* Mark this slot contain no device */
        SetDeviceToSlotId(slotId, NULL);

        UnMapByHal(slotId, hal);
        AppHalListenerEnable(hal, cAtFalse);
        AtHalDelete(hal);
        BaseAddressCacheClear(slotId);

        if (remove(SlotLockFileName(slotId)) == -1)
            AtPrintc(cSevWarning, "WARNING: Can not remove %s\r\n", SlotLockFileName(slotId));
        }

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCisco.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : Cisco public specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "../../eth/CliAtModuleEth.h"
#include "../textui/CliAtTextUI.h"
#include "../../physical/CliAtSerdesController.h"
#include "AtCisco.h"
#include "AtCiscoUpsr.h"
#include "../../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtCpldLoopbackStr[] = {
                                       "release",
                                       "local",
                                       "remote",
                                       "dual"
                                        };

static const char *cAtCiscoUpsrAlarmStr[] = {"aps-sd", "aps-sf"};
static const uint32 cAtCiscoUpsrAlarmVal[] = {cAtCiscoUpsrAlarmSd, cAtCiscoUpsrAlarmSf};

static const eAtLoopbackMode cAtCpldLoopbackVal[] = {
                                                        cAtLoopbackModeRelease,
                                                        cAtLoopbackModeLocal,
                                                        cAtLoopbackModeRemote,
                                                        cAtLoopbackModeDual
                                                        };

static const char *cPtchInsertModeStr[]    = {"1-byte", "2-bytes"};
static const uint32 cPtchInsertModeValue[] = {cAtPtchModeOneByte, cAtPtchModeTwoBytes};

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet Tha60210011DeviceXfiDiagPerGroupEnable(AtDevice self, uint8 groupId, eBool diag);

/*--------------------------- Implementation ---------------------------------*/
static eBool XfiDiagEnable(char** argv, eBool diag)
    {
    uint8 i;
    uint32 bufferSize;
    eAtRet ret = cAtOk;
    uint32 *groupIds = CliSharedIdBufferGet(&bufferSize);
    uint8 numGroups = (uint8)CliIdListFromString(argv[0], groupIds, bufferSize);
    if (numGroups == 0)
        return cAtFalse;

    for (i = 0; i < numGroups; i++)
        ret |= Tha60210011DeviceXfiDiagPerGroupEnable(CliDevice(), (uint8)(groupIds[i] - 1), diag);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool XfiGroupSelect(char** argv, eAtRet (*GroupSetFunc)(AtModuleEth ethModule, uint32 groupId))
    {
    uint32 bufferSize;
    eAtRet ret = cAtOk;
    uint32 *groupIds = CliSharedIdBufferGet(&bufferSize);
    uint8 numGroups = (uint8)CliIdListFromString(argv[0], groupIds, bufferSize);
    if (numGroups == 0)
        return cAtFalse;

    ret = GroupSetFunc((AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth), (groupIds[0] - 1));
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Select group %d fail with ret = %s\r\n", groupIds[0], AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static uint32 UpsrAlarmMaskFromString(char *alarmStr)
    {
    return CliMaskFromString(alarmStr, cAtCiscoUpsrAlarmStr, cAtCiscoUpsrAlarmVal, mCount(cAtCiscoUpsrAlarmVal));
    }


static AtList PrbsEnginesGet(char *idString)
    {
    uint32 i;
    AtList engines = AtListCreate(0);
    AtList serdesControllers = CliAtSerdesControllersList(idString);

    for (i = 0; i < AtListLengthGet(serdesControllers); i++)
        {
        AtSerdesController serdes = (AtSerdesController)AtListObjectGet(serdesControllers, i);
        AtPrbsEngine engine = AtSerdesControllerPrbsEngine(serdes);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    AtObjectDelete((AtObject)serdesControllers);

    return engines;
    }

static eBool AttributeMacSet(char argc, char **argv, eAtRet (*MacSet)(AtPrbsEngine self, uint8* mac))
    {
    uint32 engine_i;
    uint8 mac[cAtMacAddressLen];
    uint32 numBytes;
    eAtRet ret;
    AtList engines = PrbsEnginesGet(argv[0]);
    AtUnused(argc);

    /* Get MAC Address */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);
    numBytes = AtString2Bytes(argv[1], mac, cAtMacAddressLen, 16);
    if (numBytes != cAtMacAddressLen)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid MAC address\r\n");
        return cAtFalse;
        }

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        ret = MacSet(engine, mac);
        if(ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure MAC for PRBS engine, ret = %s\r\n", AtRet2String(ret));
            }
        }

    return cAtTrue;
    }

static eBool AttributeEthTypeSet(char argc, char **argv, eAtRet (*EthTypeSet)(AtPrbsEngine self, uint16 ethType))
    {
    uint32 engine_i;
    uint16 ethType;
    eAtRet ret;
    AtList engines = PrbsEnginesGet(argv[0]);
    AtUnused(argc);

    /* Get Ethernet type */
    ethType = (uint16)AtStrToDw(argv[1]);

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        ret = EthTypeSet(engine, ethType);
        if(ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure Ethernet type for PRBS engine, ret = %s\r\n", AtRet2String(ret));
            }
        }

    return cAtTrue;
    }

static char* VlanStringBuild(const tAtVlan *vlan)
    {
    static char vlanStrBuffer[48];

    if (vlan == NULL)
        {
        AtSprintf(vlanStrBuffer, "None");
        return vlanStrBuffer;
        }

    AtSprintf(vlanStrBuffer, "%d.%d.%d.%x", vlan->priority, vlan->cfi, vlan->vlanId, vlan->tpid);
    return vlanStrBuffer;
    }

static eBool EthFlowVlanGet(char *pStrVlan, tAtVlan* vlan)
    {
    uint32 numItem;
    uint32 vlanBuf[10];
    char minFormat[] = "0.0.0";
    char maxFormat[] = "7.1.4095";

    AtOsalMemInit(vlanBuf, 0, sizeof(vlanBuf));
    if (pStrVlan != NULL)
        {
        if (!AtStrncmp(pStrVlan, "none", 4))
            return cAtTrue;

        IdComplexNumParse((void*)pStrVlan,
                          minFormat,
                          maxFormat,
                          10,
                          vlanBuf,
                          &numItem);
        if (numItem == 3)
            {
            vlan->priority = (uint8)vlanBuf[0];
            vlan->cfi      = (uint8)vlanBuf[1];
            vlan->vlanId   = (uint16)vlanBuf[2];
            }
        else
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ControlFlowMacSet(char argc, char **argv, eAtRet (*MacSet)(AtModuleEth self, const uint8* mac))
    {
    uint8 mac[cAtMacAddressLen];
    uint32 numBytes;
    eAtRet ret = cAtOk;
    AtModuleEth ethModule;
    AtUnused(argc);

    /* Get MAC Address */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);
    numBytes = AtString2Bytes(argv[0], mac, cAtMacAddressLen, 16);
    if (numBytes != cAtMacAddressLen)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid MAC address\r\n");
        return cAtFalse;
        }

    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    ret = MacSet(ethModule, mac);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot configure MAC for control flow, ret = %s\r\n", AtRet2String(ret));

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool ControlFlowVlanSet(char argc, char **argv, eAtRet (*VlanSet)(AtModuleEth self, const tAtVlan* vlan))
    {
    AtModuleEth ethModule;
    eAtRet ret = cAtOk;
    tAtVlan vlan;
    AtUnused(argc);

    AtOsalMemInit(&vlan, 0, sizeof(tAtVlan));

    /* Get Control Vlan */
    if (!EthFlowVlanGet(argv[1], &vlan))
        {
        AtPrintc(cSevCritical, "Invalid VLAN, expected priority.cfi.vlanId.tpid\r\n");
        return cAtFalse;
        }

    vlan.tpid = (uint16)AtStrToDw(argv[0]);
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    ret = VlanSet(ethModule, &vlan);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot configure Vlan for control flow, ret = %s\r\n", AtRet2String(ret));

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

static AtPtchService PtchService(const char* portIdString)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    uint32 portId = AtStrToDw(portIdString);
    AtEthPort sgmiiPort;

    if (portId < 1)
        return NULL;

    sgmiiPort = AtModuleEthPortGet(ethModule, (uint8)(portId - 1));
    return AtModuleEthGeBypassPtchServiceGet(ethModule, sgmiiPort);
    }

static eBool PtchSet(char argc, char** argv, eAtRet (*PtchSetFunction)(AtPtchService self, uint16 ptchId))
    {
    AtChannel *sgmiiPorts;
    uint32 *idBuffer;
    uint32 bufferSize;
    uint32 numPorts, port_i, numPtch;

    AtUnused(argc);
    sgmiiPorts = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], sgmiiPorts, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevCritical, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numPtch = CliIdListFromString(argv[1], idBuffer, bufferSize);
    if (numPorts != numPtch)
        {
        AtPrintc(cSevCritical, "Number of ports and number of PTCH values must be equal\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
        AtPtchService ptch = AtModuleEthGeBypassPtchServiceGet(ethModule, (AtEthPort)sgmiiPorts[port_i]);
        uint16 ptchId = (uint16)idBuffer[port_i];
        eAtRet ret;

        if (ptch == NULL)
            continue;

        AtUnused(argc);

        ret = PtchSetFunction(ptch, ptchId);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Configure PTCH fail with ret = %s\r\n", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

static eBool EthPortPassThroughEnable(char argc, char **argv, eBool enable)
    {
    AtChannel *sgmiiPorts;
    uint32 bufferSize;
    uint32 numPorts, port_i;

    AtUnused(argc);
    sgmiiPorts = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], sgmiiPorts, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = AtCiscoGePortBypassEnable((AtEthPort)sgmiiPorts[port_i], enable);
        if (ret != cAtOk)
             {
             AtPrintc(cSevCritical, "ERROR: Enable Ethernet passthrough for %s fail with ret = %s\r\n",
                      AtChannelIdString(sgmiiPorts[port_i]),
                      AtRet2String(ret));
             return cAtFalse;
             }
        }

    return cAtTrue;
    }

static eBool GePortPassThroughUnbind(char* gePortString)
    {
    AtChannel *sgmiiPorts;
    uint32 bufferSize;
    uint32 numPorts, port_i;

    sgmiiPorts = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(gePortString, sgmiiPorts, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to configure, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = AtCiscoGePortBypassPortSet((AtEthPort)sgmiiPorts[port_i], NULL);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Unbind passthrough port.%u fail with ret = %s\r\n", AtChannelIdGet(sgmiiPorts[port_i]) + 1, AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

static eBool EthPortPassThroughPtchEnable(char argc, char **argv, eBool enable)
    {
    AtChannel *sgmiiPorts;
    uint32 bufferSize;
    uint32 numPorts, port_i;

    AtUnused(argc);
    sgmiiPorts = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], sgmiiPorts, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
        AtPtchService ptch = AtModuleEthGeBypassPtchServiceGet(ethModule, (AtEthPort)sgmiiPorts[port_i]);
        eAtRet ret;

        if (ptch == NULL)
            continue;

        ret = AtPtchServiceEnable(ptch, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Enable PTCH fail with ret = %s\r\n", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtCiscoXfiDiagEnable(char argc, char** argv)
    {
    AtUnused(argc);
    return XfiDiagEnable(argv, cAtTrue);
    }

eBool CmdAtCiscoXfiDiagDisable(char argc, char** argv)
    {
    AtUnused(argc);
    return XfiDiagEnable(argv, cAtFalse);
    }

eBool CmdAtCiscoXfiGroupSelect(char argc, char** argv)
    {
    AtUnused(argc);
    return XfiGroupSelect(argv, AtCiscoModuleEthXfiGroupSet);
    }

eBool CmdAtCiscoIpXfiGroupSelect(char argc, char** argv)
    {
    AtUnused(argc);
    return XfiGroupSelect(argv, AtCiscoIpModuleEthXfiGroupSet);
    }

eBool CmdAtCiscoXfiGroupShow(char argc, char** argv)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevNormal, "Current active Backplane XFI active group: %d\n", AtCiscoModuleEthXfiGroupGet(ethModule) + 1);
    return cAtTrue;
    }

eBool CmdAtCiscoCpldVersionShow(char argc, char** argv)
    {
    uint32 version;
    eBool isDownloaded;
    char buff[16];
    tTab *tabPtr;
    const char *pHeading[] = { "CPLD", "Version", "Status"};

    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    AtUnused(argc);
    AtUnused(argv);

    version = AtCiscoModulePdhCpldVersion(pdhModule);
    isDownloaded = AtCiscoModulePdhCpldIsDownloaded(pdhModule);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    AtSprintf(buff, "Information");
    StrToCell(tabPtr, 0, 0, buff);

    AtSprintf(buff, "0x%08x", version);
    ColorStrToCell(tabPtr, 0, 1, buff, cSevInfo);

    ColorStrToCell(tabPtr, 0, 2, isDownloaded ? "Downloaded" : "N/A", isDownloaded ? cSevInfo : cSevNormal);

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCiscoCpldDs1LoopbackSet(char argc, char** argv)
    {
    eAtRet ret;
    eAtLoopbackMode loopMode;
    eBool convertSuccess = cAtFalse;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    AtUnused(argc);

    /* Get loopback mode */
    mAtStrToEnum(cAtCpldLoopbackStr, cAtCpldLoopbackVal, argv[0], loopMode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Loop back mode. Expected: release, local, remote\r\n");
        return cAtFalse;
        }

    ret = AtCiscoModulePdhCpldDs1LoopbackSet(pdhModule, loopMode);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot configure Loopback Mode for CPLD, ret = %s\r\n", AtRet2String(ret));

    return cAtTrue;
    }

eBool CmdAtCiscoPrbsEngineDestMacSet(char argc, char **argv)
    {
    return AttributeMacSet(argc, argv, AtCiscoPrbsEngineDestMacSet);
    }

eBool CmdAtCiscoPrbsEngineSrcMacSet(char argc, char **argv)
    {
    return AttributeMacSet(argc, argv, AtCiscoPrbsEngineSrcMacSet);
    }

eBool CmdAtCiscoPrbsEngineEthTypeSet(char argc, char **argv)
    {
    return AttributeEthTypeSet(argc, argv, AtCiscoPrbsEngineEthernetTypeSet);
    }

eBool CmdAtCiscoControlFlowEgressDestMacSet(char argc, char **argv)
    {
    return ControlFlowMacSet(argc, argv, AtCiscoControlFlowEgressDestMacSet);
    }

eBool CmdAtCiscoControlFlowEgressSrcMacSet(char argc, char **argv)
    {
    return ControlFlowMacSet(argc, argv, AtCiscoControlFlowEgressSrcMacSet);
    }

eBool CmdAtCiscoControlFlowEgressControlVlanSet(char argc, char **argv)
    {
    return ControlFlowVlanSet(argc, argv, AtCiscoControlFlowEgressControlVlanSet);
    }

eBool CmdAtCiscoControlFlowIngressControlVlanSet(char argc, char **argv)
    {
    AtModuleEth ethModule;
    eAtRet ret = cAtOk;
    tAtVlan vlan;

    /* Remove VLAN */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    if ((argc == 1) && (!AtStrncmp(argv[0], "none", 4)))
        {
        ret = AtCiscoControlFlowIngressControlVlanSet(ethModule, NULL);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Cannot configure Vlan for control flow, ret = %s\r\n", AtRet2String(ret));

        return cAtTrue;
        }

    if ((argc == 1) && (AtStrncmp(argv[0], "none", 4)))
        {
        AtPrintc(cSevCritical, "WARNING, Input lack parameter TPID or VLAN Tag, Ex. 0x8100 or <Priority.CFI.VLAN_ID>\r\n");
        return cAtFalse;
        }

    if ((argc > 1) &&
       ((!AtStrncmp(argv[0], "none", 4)) || (!AtStrncmp(argv[1], "none", 4))))
        {
        AtPrintc(cSevCritical, "ERROR: Wrong input parameter, Ex. 0x8100 or <Priority.CFI.VLAN_ID>\r\n");
        return cAtFalse;
        }

    /* Get Control VLAN */
    if (!EthFlowVlanGet(argv[1], &vlan))
        {
        AtPrintc(cSevCritical, "Invalid VLAN, expected priority.cfi.vlanId.tpid\r\n");
        return cAtFalse;
        }

    vlan.tpid = (uint16)AtStrToDw(argv[0]);
    ret = AtCiscoControlFlowIngressControlVlanSet(ethModule, &vlan);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot configure Vlan for control flow, ret = %s\r\n", AtRet2String(ret));

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdAtCiscoControlFlowEthTypeSet(char argc, char**argv)
    {
    AtModuleEth ethModule;
    uint16 ethType;
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Get Ethernet Type */
    ethType = (uint16)AtStrToDw(argv[0]);
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    ret = AtCiscoControlFlowEgressEthTypeSet(ethModule, ethType);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot configure Ethernet type for OAM control, ret = %s\r\n", AtRet2String(ret));

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdAtCiscoControlFlowConfigureGet(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"SMAC", "DMAC", "EG-Control VLANs", "IG-Control VLANs", "Ethernet Type", "EG-Circuit TPID"};
    static uint8 mac[6];
    eAtRet ret = cAtOk;
    AtModuleEth ethModule;
    tAtVlan vlan;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    ret = AtCiscoControlFlowEgressSrcMacGet(ethModule, mac);
    if (ret != cAtOk)
        ColorStrToCell(tabPtr, 0, 0, "Error", cSevCritical);
    else
        StrToCell(tabPtr, 0, 0, CliEthPortMacAddressByte2Str(mac));

    /* Get destination MAC */
    ret = AtCiscoControlFlowEgressDestMacGet(ethModule, mac);
    if (ret != cAtOk)
        ColorStrToCell(tabPtr, 0, 1, "Error", cSevCritical);
    else
        StrToCell(tabPtr, 0, 1, CliEthPortMacAddressByte2Str(mac));

    /* EG-VLANs */
    ret = AtCiscoControlFlowEgressControlVlanGet(ethModule, &vlan);
    if (ret != cAtOk)
        ColorStrToCell(tabPtr, 0, 2, "Error", cSevCritical);
    else
        StrToCell(tabPtr, 0, 2, VlanStringBuild(&vlan));

    /* Get IG VLANs */
    ret = AtCiscoControlFlowIngressControlVlanGet(ethModule, &vlan);
    if (ret != cAtOk)
        ColorStrToCell(tabPtr, 0, 3, "Error", cSevCritical);
    else
        StrToCell(tabPtr, 0, 3, VlanStringBuild(&vlan));

    /* Ethernet type */
    StrToCell(tabPtr, 0, 4, CliNumber2String(AtCiscoControlFlowEgressEthTypeGet(ethModule), "%x"));

    /* Circuit TPID */
    StrToCell(tabPtr, 0, 5, CliNumber2String(AtCiscoControlFlowEgressCircuitVlanTPIDGet(ethModule), "%x"));

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdAtCiscoControlFlowEgressCircuitTpidSet(char argc, char **argv)
    {
    AtModuleEth ethModule;
    eAtRet ret = cAtOk;
    uint16 tpid;
    AtUnused(argc);

    tpid = (uint16)AtStrToDw(argv[0]);
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    ret = AtCiscoControlFlowEgressCircuitVlanTPIDSet(ethModule, tpid);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Cannot configure TPID of Circuit Vlan for control flow, ret = %s\r\n", AtRet2String(ret));

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdAtCiscoIpXfiGroupShow(char argc, char** argv)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevNormal, "Current active Backplane CiscoIp XFI active group: %d\n", AtCiscoIpModuleEthXfiGroupGet(ethModule) + 1);
    return cAtTrue;
    }

eBool CmdAtCiscoUpsrAlarmMaskSet(char argc, char **argv)
    {
    uint32 alarmMaskVal;
    uint32 alarmType;
    eAtRet ret;
    eAtRet (*AlarmMaskSet)(AtModuleSdh sdhModule, uint32 alarms);
    AtUnused(argc);

    if ((alarmType = UpsrAlarmMaskFromString(argv[0])) == 0 ||
        (alarmType != cAtCiscoUpsrAlarmSd &&
         alarmType != cAtCiscoUpsrAlarmSf))
        {
        AtPrintc(cSevCritical, "ERROR: UPSR Alarm type is invalid\r\n");
        return cAtFalse;
        }

    if (AtStrcmp(argv[1], "none") == 0)
        alarmMaskVal = 0;
    else
        {
        /* Get interrupt mask value from input string */
        if ((alarmMaskVal = CliSdhPathAlarmMaskFromString(argv[1])) == 0)
            {
            AtPrintc(cSevCritical, "ERROR: SDH Path alarm mask is invalid\r\n");
            return cAtFalse;
            }
        }

    if (alarmType == cAtCiscoUpsrAlarmSf)
        AlarmMaskSet = AtCiscoModuleSdhUpsrSfAlarmMaskSet;
    else
        AlarmMaskSet = AtCiscoModuleSdhUpsrSdAlarmMaskSet;
    ret = AlarmMaskSet((AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh), alarmMaskVal);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot set SD/SF alarm mask, ret = %s\r\n",
                 AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdAtCiscoUpsrAlarmMaskShow(char argc, char **argv)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    uint32 alarmMaskVal;
    tTab *tabPtr;
    const char *pHeading[] = {"SF alarms mask", "SD alarms mask"};

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    alarmMaskVal = AtCiscoModuleSdhUpsrSfAlarmMaskGet(sdhModule);
    StrToCell(tabPtr, 0, 0, CliSdhPathAlarm2String(alarmMaskVal));

    alarmMaskVal = AtCiscoModuleSdhUpsrSdAlarmMaskGet(sdhModule);
    StrToCell(tabPtr, 0, 1, CliSdhPathAlarm2String(alarmMaskVal));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCiscoEthPortPassThrough(char argc, char **argv)
    {
    AtChannel *sgmiiPorts;
    AtChannel *bypassPorts;
    uint32 bufferSize;
    uint32 numPorts, port_i;
    uint32 numBypassPort;
    AtUnused(argc);

    if (AtStrcmp(argv[1], "none") == 0)
        return GePortPassThroughUnbind(argv[0]);

    sgmiiPorts = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], sgmiiPorts, bufferSize / 2);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to configure, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    bypassPorts = sgmiiPorts + bufferSize / 2;
    numBypassPort = CliEthPortListFromString(argv[1], bypassPorts, bufferSize / 2);
    if (numBypassPort == 0)
        {
        AtPrintc(cSevInfo, "No port to configure, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    if ((numBypassPort != 1) && (numBypassPort != numPorts))
        {
        AtPrintc(cSevCritical, "ERROR: Number of Port and number of Bypass Port must be the same\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort bypassPort = (numBypassPort != 1) ? (AtEthPort)bypassPorts[port_i] : (AtEthPort)bypassPorts[0];
        eAtRet ret = AtCiscoGePortBypassPortSet((AtEthPort)sgmiiPorts[port_i], bypassPort);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Configure passthrough port.%u and port.%u fail with ret = %s\r\n",
                  AtChannelIdGet(sgmiiPorts[port_i]) + 1,
                  AtChannelIdGet((AtChannel)bypassPort) + 1,
                  AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtCiscoEthPortPassThroughEnable(char argc, char **argv)
    {
    return EthPortPassThroughEnable(argc, argv, cAtTrue);
    }

eBool CmdAtCiscoEthPortPassThroughDisable(char argc, char **argv)
    {
    return EthPortPassThroughEnable(argc, argv, cAtFalse);
    }

eBool CmdAtCiscoEthPortPassThroughEgressPtch(char argc, char **argv)
    {
    return PtchSet(argc, argv, AtPtchServiceEgressPtchSet);
    }

eBool CmdAtCiscoEthPortPassThroughIngressPtch(char argc, char **argv)
    {
    return PtchSet(argc, argv, AtPtchServiceIngressPtchSet);
    }

eBool CmdAtCiscoEthPortPassThroughPtchEnable(char argc, char **argv)
    {
    return EthPortPassThroughPtchEnable(argc, argv, cAtTrue);
    }

eBool CmdAtCiscoEthPortPassThroughPtchDisable(char argc, char **argv)
    {
    return EthPortPassThroughPtchEnable(argc, argv, cAtFalse);
    }

eBool CmdAtCiscoEthPortPassThroughIngressPtchMode(char argc, char **argv)
    {
    return CliAtPtchServiceIngressModeSet(PtchService, argc, argv);
    }

eBool CmdAtCiscoEthPortPassThroughShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"Port ID", "Passthrough Port", "Enabled", "PTCH Ingress", "PTCH Egress", "PTCH Mode", "PTCH Enabled"};
    uint32 port_i;
    AtPtchService ptch;
    AtModuleEth eth;
    uint16 ptchId;
    eBool convertSuccess;
    AtChannel *sgmiiPorts;
    uint32 numPorts, bufferSize;

    AtUnused(argc);
    AtUnused(argv);

    sgmiiPorts = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], sgmiiPorts, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    eth = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Eth Module is NULL\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint8 column = 0;
        AtEthPort bypassPort = AtCiscoGePortBypassPortGet((AtEthPort)sgmiiPorts[port_i]);
        eBool enabled;
        const char* string;

        StrToCell(tabPtr, port_i, column++, AtChannelIdString(sgmiiPorts[port_i]));
        StrToCell(tabPtr, port_i, column++, (bypassPort) ? AtChannelIdString((AtChannel)bypassPort) : "None");

        enabled = AtCiscoGePortBypassIsEnabled((AtEthPort)sgmiiPorts[port_i]);
        ColorStrToCell(tabPtr, port_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        ptch = AtModuleEthGeBypassPtchServiceGet(eth, (AtEthPort)sgmiiPorts[port_i]);
        if (ptch == NULL)
            {
            for (; column < mCount(heading); column++)
                StrToCell(tabPtr, port_i, column, "N/A");
            continue;
            }

        /* Print Ingress PTCH */
        ptchId = AtPtchServiceIngressPtchGet(ptch);
        ColorStrToCell(tabPtr, port_i, column++, CliNumber2String(ptchId, "0x%04x"), cSevInfo);

        /* Print Egress PTCH */
        ptchId = AtPtchServiceEgressPtchGet(ptch);
        ColorStrToCell(tabPtr, port_i, column++, CliNumber2String(ptchId, "0x%02x"), cSevInfo);

        string = CliEnumToString(AtPtchServiceIngressModeGet(ptch), cPtchInsertModeStr, cPtchInsertModeValue, mCount(cPtchInsertModeStr), &convertSuccess);
        ColorStrToCell(tabPtr, port_i, column++, convertSuccess ? string : "Unknown", cSevInfo);

        /* Enabling */
        enabled = AtPtchServiceIsEnabled(ptch);
        ColorStrToCell(tabPtr, port_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCiscoDeprecated.c
 *
 * Created Date: Jul 29, 2016
 *
 * Description : Deprecated CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtDriverSyncModeSet(char argc, char **argv)
    {
    /* Just support with "none" mode only */
    if (AtStrcmp(argv[0], "none") != 0)
        {
        AtPrintc(cSevCritical, "ERROR: Synchronization mode is not support. Just support \"none\" mode\r\n");
        return cAtFalse;
        }

    /* Do nothing */
    AtUnused(argc);
    AtPrintc(cSevWarning, "WARNING: this is deprecated and not used anymore\r\n");
    return cAtTrue;
    }

eBool CmdAtDriverHaSimulateEnable(char argc, char **argv)
    {
    AtPrintc(cSevWarning, "WARNING: this is deprecated and not used anymore\r\n");
    AtUnused(argc);
    AtUnused(argv);
    return cAtTrue;
    }

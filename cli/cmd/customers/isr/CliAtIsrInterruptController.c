/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ISR
 *
 * File        : CliAtIsrInterruptController.c
 *
 * Created Date: Sep 12, 2017
 *
 * Description : CLIs to handle ISR interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "AtIsrInterruptController.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtIsrInterruptController m_controller = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CorrectProductCode(uint32 productCode)
    {
    uint32 fieldMask = cBit31_28;
    uint32 fieldShift = 28;
    uint32 family = mRegField(productCode, field);
    uint32 correctedCode = productCode;

    if ((family == 0xF) || (family == 0x8))
        mRegFieldSet(correctedCode, field, 0x6);

    return correctedCode;
    }

static uint32 ProductCode(void)
    {
    return CorrectProductCode(AtDeviceProductCodeGet(CliDevice()));
    }

static AtHal Hal(void)
    {
    return AtDeviceIpCoreHalGet(CliDevice(), 0);
    }

static void TextUIWillStop(AtTextUI textUI, void *userData)
    {
    AtUnused(textUI);
    AtUnused(userData);
    AtIsrInterruptControllerDelete(m_controller);
    m_controller = NULL;
    }

static tAtTextUIListerner *Listener(void)
    {
    static tAtTextUIListerner listener;
    static tAtTextUIListerner *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.WillStop = TextUIWillStop;
    pListener = &listener;

    return pListener;
    }

static void TextUIListen(void)
    {
    AtTextUIListenerAdd(AtCliSharedTextUI(), Listener(), NULL);
    }

static AtIsrInterruptController Controller(void)
    {
    if (m_controller)
        return m_controller;

    m_controller = AtIsrInterruptControllerCreate(Hal(), ProductCode());
    TextUIListen();
    return m_controller;
    }

static eBool CoreEnable(char argc, char **argv, eBool enabled)
    {
    uint32 numCores = 0, core_i;
    uint32 *idBuffer = CliSharedIdBufferGet(&numCores);
    eBool success = cAtTrue;
    AtIsrInterruptController controller = Controller();

    AtUnused(argc);

    numCores = CliIdListFromString(argv[0], idBuffer, numCores);

    for (core_i = 0; core_i < numCores; core_i++)
        {
        uint32 coreId = idBuffer[core_i] - 1;
        eAtRet ret;

        if (coreId >= AtIsrInterruptControllerNumCores(controller))
            {
            AtPrintc(cSevWarning, "CORE %u does not exist, ignore it\r\n", idBuffer[core_i]);
            continue;
            }

        ret = AtIsrInterruptControllerCoreEnable(controller, (uint8)coreId, enabled);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s CORE fail with ret = %s\r\n",
                 enabled ? "Enable" : "Disable",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtIsrInterruptControllerCoresShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"CoreId", "enabled"};
    AtIsrInterruptController controller = Controller();
    uint8 core_i;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(AtIsrInterruptControllerNumCores(controller), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (core_i = 0; core_i < AtIsrInterruptControllerNumCores(controller); core_i++)
        {
        uint32 column = 0;
        eBool enabled = AtIsrInterruptControllerCoreIsEnabled(controller, core_i);

        StrToCell(tabPtr, core_i, column++, CliNumber2String(core_i + 1, "%u"));
        ColorStrToCell(tabPtr, core_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtIsrInterruptControllerCoreEnable(char argc, char **argv)
    {
    return CoreEnable(argc, argv, cAtTrue);
    }

eBool CmdAtIsrInterruptControllerCoreDisable(char argc, char **argv)
    {
    return CoreEnable(argc, argv, cAtFalse);
    }

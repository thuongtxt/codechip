/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customers
 *
 * File        : CliAtFiberLogic.c
 *
 * Created Date: May 29, 2015
 *
 * Description : Fiber Logic
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtFiberLogic.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtFiberLogicLiuModeStr[] = {
                                          "e1",
                                          "ds1"
                                          };

static const eAtFiberLogicLiuMode cAtFiberLogicLiuModeVal[] = {
                                                            cAtFiberLogicLiuModeE1,
                                                            cAtFiberLogicLiuModeDs1
                                                            };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtFiberLogicLiuModeSet(char argc, char** argv)
    {
    eBool blResult;
    eAtFiberLogicLiuMode liuMode = cAtFiberLogicLiuModeE1;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get mode */
    mAtStrToEnum(cAtFiberLogicLiuModeStr, cAtFiberLogicLiuModeVal, argv[0], liuMode, blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid LIU Mode\r\n");
        return cAtFalse;
        }

    ret = AtFiberLogicLiuInit(liuMode);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Can not init LIU mode. ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool LiuIdIsValid(uint32 liuId)
    {
    return (liuId < 32) ? cAtTrue : cAtFalse;
    }

eBool CmdAtFiberLogicLiuLoopbackSet(char argc, char** argv)
    {
    uint32 *idBuf;
    uint32 bufferSize;
    eAtLoopbackMode loopbackMode;
    uint32 numLiu;
    uint32 i;
    eAtRet ret = cAtOk;
    eBool success = cAtTrue;

    AtUnused(argc);

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numLiu = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Can not get LIU list\r\n");
        return cAtFalse;
        }

    loopbackMode = CliLoopbackModeFromString(argv[1]);

    for (i = 0; i < numLiu; i++)
        {
        uint32 liuId = idBuf[i] - 1;
        if (!LiuIdIsValid(liuId))
            {
            AtPrintc(cSevCritical, "ERROR: Invalid LIU ID\r\n");
            continue;
            }

        ret = AtFiberLogicLiuLoopbackSet((uint16)liuId, loopbackMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not looopback on LIU %u, ret = %s\r\n", idBuf[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtFiberLogicLiuRead(char argc, char** argv)
    {
    uint32 address;
    uint16 liuId;

    /* Not enough parameter */
    if(argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Too many parameters */
    if (argc > 2)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    liuId = (uint16)(AtStrToDw(argv[0]) - 1);
    if (!LiuIdIsValid(liuId))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid LIU ID\r\n");
        return cAtFalse;
        }

    StrToDw(argv[1], 'h', &address);

    AtFiberLogicLiuRead(liuId, address);  /* This API will print value out */
    return cAtTrue;

    }

eBool CmdAtFiberLogicLiuWrite(char argc, char** argv)
    {
    uint32 address, value;
    uint16 liuId;

    /* Not enough parameter */
    if(argc < 3)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Too many parameters */
    if (argc > 3)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    liuId = (uint16)(AtStrToDw(argv[0]) - 1);
    if (!LiuIdIsValid(liuId))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid LIU ID\r\n");
        return cAtFalse;
        }

    StrToDw(argv[1], 'h', &address);
    StrToDw(argv[2], 'h', &value);

    AtFiberLogicLiuWrite(liuId, address, value);
    AtPrintc(cSevNormal, "Write address: 0x%x, value: 0x%x\r\n", address, value);

    return cAtTrue;
    }

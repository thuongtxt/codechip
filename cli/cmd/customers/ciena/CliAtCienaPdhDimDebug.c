/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaPdhDimDebug.c
 *
 * Created Date: Dec 16, 2016
 *
 * Description : Debug CLIs for DIM
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/codechip/Tha62290011/pdh/Tha62290011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtCienaPdhDimLiuLoopbackDebug(char argc, char **argv)
    {
    uint32 bufferSize;
    uint32 *idBuffer, numLius, liu_i;
    ThaModulePdh pdhModule;
    eAtLoopbackMode loopMode = CliLoopbackModeFromString(argv[1]);
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    if ((numLius = CliIdListFromString(argv[0], idBuffer, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return cAtFalse;
        }

    pdhModule = (ThaModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    for (liu_i = 0; liu_i < numLius; liu_i++)
        {
        eAtRet ret;

        if (idBuffer[liu_i] > 84)
            continue;

        ret = Tha62290011ModulePdhDimLiuLoopback(pdhModule, CliId2DriverId(idBuffer[liu_i]), loopMode);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Cannot loopback on port AXI, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtCienaPdhDimAxi4LoopbackDebug(char argc, char **argv)
    {
    eAtLoopbackMode loopMode = CliLoopbackModeFromString(argv[0]);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtRet ret = Tha62290011ModulePdhDimAxi4Loopback(pdhModule, loopMode);

    AtUnused(argc);

    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical,
             "ERROR: Cannot loopback on port AXI, ret = %s\r\n",
             AtRet2String(ret));
    return cAtFalse;
    }

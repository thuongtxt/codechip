/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaVersion.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : Customer specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCiena.h"
#include "AtCienaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtCienaClockMonitorOutputStr[] = {"none",
                                                       "spare_serdes",
                                                       "overhead",
                                                       "xfi155_52M_0",
                                                       "xfi155_52M_1",
                                                       "xfi156_25M_0",
                                                       "xfi156_25M_1",
                                                       "eth40g_0",
                                                       "eth40g_1",
                                                       "basex2500",
                                                       "qdr",
                                                       "ddr_1",
                                                       "ddr_2",
                                                       "ddr_3",
                                                       "ddr_4",
                                                       "pcie",
                                                       "prc",
                                                       "ext",
                                                       "system"};

static const uint32 cAtCienaClockMonitorOutputVal[]  = {cAtCienaClockMonitorOutputNone,
                                                        cAtCienaClockMonitorOutputSpareSerdesDiv2,
                                                        cAtCienaClockMonitorOutputOverheadDiv2,
                                                        cAtCienaClockMonitorOutputXfi155_52M_0Div2,
                                                        cAtCienaClockMonitorOutputXfi155_52M_1Div2,
                                                        cAtCienaClockMonitorOutputXfi156_25M_0Div2,
                                                        cAtCienaClockMonitorOutputXfi156_25M_1Div2,
                                                        cAtCienaClockMonitorOutputEth40G_0Div2,
                                                        cAtCienaClockMonitorOutputEth40G_1Div2,
                                                        cAtCienaClockMonitorOutputBaseX2500Div2,
                                                        cAtCienaClockMonitorOutputQdrDiv2,
                                                        cAtCienaClockMonitorOutputDdr_1Div2,
                                                        cAtCienaClockMonitorOutputDdr_2Div2,
                                                        cAtCienaClockMonitorOutputDdr_3Div2,
                                                        cAtCienaClockMonitorOutputDdr_4Div2,
                                                        cAtCienaClockMonitorOutputPcieDiv2,
                                                        cAtCienaClockMonitorOutputPrc,
                                                        cAtCienaClockMonitorOutputExt,
                                                        cAtCienaClockMonitorOutputSystem
                                                        };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleClock ClockModule(void)
    {
    return (AtModuleClock)AtDeviceModuleGet(CliDevice(), cAtModuleClock);
    }

eBool CmdAtCienaDeviceVersionShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtCienaShowVersion(CliDevice());
    return cAtTrue;
    }

eBool CmdAtCienaModuleClockMonitorClockOutputSet(char argc, char** argv)
    {
    uint8 output_i, numClockOutput;
    uint32 bufferSize;
    eBool success = cAtTrue;
    uint32 *idBuf = CliSharedIdBufferGet(&bufferSize);
    eAtCienaClockMonitorOutput refClkSource = cAtCienaClockMonitorOutputNone;

    /* Get clock output ID */
    AtUnused(argc);
    numClockOutput = (uint8)CliIdListFromString(argv[0], idBuf, bufferSize);
    if (numClockOutput == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid clock output ID. Expected: 1-2\r\n");
        return cAtFalse;
        }

    /* Get ref clock source */
    refClkSource = CliStringToEnum(argv[1], cAtCienaClockMonitorOutputStr, cAtCienaClockMonitorOutputVal, mCount(cAtCienaClockMonitorOutputVal), &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reference clock output source.\r\n");
        return cAtFalse;
        }

    for (output_i = 0; output_i < numClockOutput; output_i++)
        {
        eAtRet ret = AtCienaModuleClockMonitorOutputSourceSet(ClockModule(), output_i, refClkSource);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning, "Can not set monitoring clock output #%u. ret = %s\r\n", output_i + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtCienaModuleClockMonitorOutputGet(char argc, char** argv)
    {
    uint8 output_i, numClockOutput;
    uint32 bufferSize;
    uint32 *idBuf = CliSharedIdBufferGet(&bufferSize);
    tTab *tabPtr;
    const char *heading[] = {"Output ID", "Reference clock source"};

    /* Get clock output ID */
    AtUnused(argc);
    numClockOutput = (uint8)CliIdListFromString(argv[0], idBuf, bufferSize);
    if (numClockOutput == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid clock output ID. Expected: 1-2\r\n");
        return cAtFalse;
        }

    if (numClockOutput > AtCienaModuleClockMonitorNumOutputs(ClockModule()))
        numClockOutput = AtCienaModuleClockMonitorNumOutputs(ClockModule());

    tabPtr = TableAlloc(numClockOutput, mCount(heading), (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (output_i = 0; output_i < numClockOutput; output_i++)
        {
        const char *refClkSourceStr;
        eAtCienaClockMonitorOutput refClkSource = AtCienaModuleClockMonitorOutputSourceGet(ClockModule(), output_i);

        StrToCell(tabPtr, output_i, 0, CliNumber2String(output_i + 1, "%u"));
        refClkSourceStr = CliEnumToString(refClkSource, cAtCienaClockMonitorOutputStr, cAtCienaClockMonitorOutputVal, mCount(cAtCienaClockMonitorOutputVal), NULL);
        ColorStrToCell(tabPtr, output_i, 1, refClkSourceStr ? refClkSourceStr : "Error", refClkSourceStr ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

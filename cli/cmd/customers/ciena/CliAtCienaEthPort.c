/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaEthPassThrough.c
 *
 * Created Date: Sep 16, 2016
 *
 * Description : ETH pass-through specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCiena.h"
#include "AtEthPort.h"
#include "../../eth/CliAtModuleEth.h"
#include "AtCliModule.h"
#include "../../../../customers/src/AtCienaInternal.h"

/*--------------------------- Define -----------------------------------------*/
typedef eAtModuleEthRet (*ThresholdSetFunc)(AtEthPort, uint32);
typedef eAtRet (*VlanTpidSetFunc)(AtModuleEth, uint8, uint16);

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterType, counterKind, counterName)                     \
    do                                                                         \
        {                                                                      \
        if (AtChannelCounterIsSupported((AtChannel)port, AtCienaDccKByteSgmiiCounterTypeToThaCounterType(counterType)))                    \
            CliCounterTypePrintToCell(tabPtr, rowId++, i, CounterGet(port, counterType), counterKind, counterName); \
        else                                                                   \
            CliCounterTypePrintToCell(tabPtr, rowId++, i, 0, cAtCounterTypeNotSupport, counterName); \
        }while(0)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtCienaSgmiiEthPortAlarmTypeStr[] =
    {
    "da-mismatch",
    "cvlan-mismatch",
    "length-oversize",
    "fcs-error",
    "ethtype-mismatch"
    };

static const uint32 cAtCienaSgmiiEthPortAlarmTypeVal[] =
    {
    cAtCienaSgmiiEthPortAlarmTypeDccDaMacMisMatch,
    cAtCienaSgmiiEthPortAlarmTypeDccCVlanMisMatch,
    cAtCienaSgmiiEthPortAlarmTypeDccLenOverSize,
    cAtCienaSgmiiEthPortAlarmTypeFcsError,
    cAtCienaSgmiiEthPortAlarmTypeEthTypeMismatch
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char **AlarmTypeStr(uint8 *numAlarms)
	{
	if (numAlarms)
		*numAlarms = mCount(cAtCienaSgmiiEthPortAlarmTypeStr);
	return cAtCienaSgmiiEthPortAlarmTypeStr;
	}

static const uint32 *AlarmTypeVal(uint8 *numAlarms)
	{
	if (numAlarms)
		*numAlarms = mCount(cAtCienaSgmiiEthPortAlarmTypeVal);
	return cAtCienaSgmiiEthPortAlarmTypeVal;
	}

static uint32 IntrMaskFromString(char *pIntrStr)
	{
	uint8 numAlarms = 0;
	const char **alarmTypeStrings = AlarmTypeStr(&numAlarms);
	const uint32 *alarmTypes = AlarmTypeVal(&numAlarms);
	return CliMaskFromString(pIntrStr, alarmTypeStrings, alarmTypes, numAlarms);
	}

static const char *Alarm2String(uint32 alarms)
	{
	uint16 alarm_i;
	uint32 bufferSize;
	char *alarmString = CliSharedCharBufferGet(&bufferSize);
	uint8 numAlarms;
	const char **alarmTypeStr = AlarmTypeStr(&numAlarms);
	const uint32 *alarmTypeVal = AlarmTypeVal(&numAlarms);

	/* Initialize string */
	alarmString[0] = '\0';
	if (alarms == 0)
		{
		AtSprintf(alarmString, "None");
		return alarmString;
		}

	/* There are alarms */
	for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
		{
		if (alarms & (alarmTypeVal[alarm_i]))
			AtSprintf(alarmString, "%s%s|", alarmString, alarmTypeStr[alarm_i]);
		}

	if (AtStrlen(alarmString) == 0)
		return "None";

	/* Remove the last '|' */
	alarmString[AtStrlen(alarmString) - 1] = '\0';

	return alarmString;
	}

static eBool FlowControlThresholdSet(char argc, char ** argv, ThresholdSetFunc funcSet)
    {
    uint32 *idBuf;
    uint8 numPorts, port_i;
    AtEthPort *ports;
    uint32 threshold;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    /* Get value of threshold */
    threshold = AtStrtoul(argv[1], NULL, 10);

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = funcSet(ports[port_i], threshold);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set threshold for port %u fail with value %u, ret = %s\r\n",
                 AtChannelIdGet((AtChannel)ports[port_i]) + 1,
                 threshold,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtOsalMemFree(ports);

    return success;
    }

static eBool FlowControlWaterMarkReset(char argc, char ** argv)
    {
    uint32 *idBuf;
    uint8 numPorts, port_i;
    AtEthPort *ports;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }


    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtCienaEthPortFlowControlTxWaterMarkMaxClear(ports[port_i]);
        AtCienaEthPortFlowControlTxWaterMarkMinClear(ports[port_i]);
        }

    AtOsalMemFree(ports);

    return success;
    }

static eBool VlanTpidSet(char argc, char ** argv, VlanTpidSetFunc funcSet)
    {
    uint8 tpidIndex;
    uint16 tpid;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);

    /* Get TPID index & value */
    tpidIndex = (uint8)(AtStrToDw(argv[0]) - 1);
    tpid = (uint16)AtStrToDw(argv[1]);

    ret = funcSet(ethModule, tpidIndex, tpid);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Configure TPID fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

static eBool BypassEnable(char argc, char **argv, eBool enabled)
    {
    uint32 *idBuf;
    uint8 numPorts, port_i;
    AtEthPort *ports;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = AtCienaEthPortBypassEnable(ports[port_i], enabled);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s bypassing fail with ret = %s\r\n",
                 enabled ? "Enable" : "Disable",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtOsalMemFree(ports);

    return success;
    }

static eBool SubportVlanSet(char argc, char **argv, eAtRet (*SubportVlanSetFunc)(AtEthPort self, const tAtVlan *vlan))
    {
    uint32 *idBuf;
    uint8 numPorts, port_i;
    AtEthPort *ports;
    tAtVlan vlan, *pVlan = NULL;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        AtOsalMemFree(ports);
        return cAtTrue;
        }

    /* Parse VLAN */
    if (AtStrcmp(argv[1], "none") != 0)
        {
        pVlan = CliVlanFromString(argv[1], &vlan);
        if (pVlan == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid VLAN, expected %s\r\n", CliFullVlanExpectedFormat());
            AtOsalMemFree(ports);
            return cAtFalse;
            }
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = SubportVlanSetFunc(ports[port_i], pVlan);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Configure VLAN fail with ret = %s\r\n", AtRet2String(ret));
        success = cAtFalse;
        }

    AtOsalMemFree(ports);

    return success;
    }

static eBool StatusShow(char *portIdString,
                        eBool silent,
                        uint32 (*AlarmGetHandler)(AtChannel self))
    {
    uint32 alarmStat, alarm_i;
    tTab *tabPtr = NULL;
    const char *heading[] = {"PortId", " ", " ", " ", " ", " "};
    uint32 numberOfChannels = 1;
    uint32 *idBuf;
    AtEthPort *ports;
    uint8 numPorts, port_i;

    /* Get Port List */
    ports = CliEthPortListGet(portIdString, &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    if (!silent)
        {
        /* Build heading */
        AtAssert(mCount(heading) == (mCount(cAtCienaSgmiiEthPortAlarmTypeVal) + 1));
        for (alarm_i = 0; alarm_i < mCount(cAtCienaSgmiiEthPortAlarmTypeVal); alarm_i++)
            heading[alarm_i + 1] = cAtCienaSgmiiEthPortAlarmTypeStr[alarm_i];

        /* Create table */
        tabPtr = TableAlloc(numberOfChannels, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            AtOsalMemFree(ports);
            return cAtFalse;
            }
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint32 colum = 0;
        AtChannel channel = (AtChannel)ports[port_i];

        StrToCell(tabPtr, port_i, colum++, AtChannelIdString(channel));

        /* Print alarm status */
        alarmStat = AlarmGetHandler(channel);
        for (alarm_i = 0; alarm_i < mCount(cAtCienaSgmiiEthPortAlarmTypeVal); alarm_i++)
            {
            if (alarmStat & cAtCienaSgmiiEthPortAlarmTypeVal[alarm_i])
                ColorStrToCell(tabPtr, port_i, alarm_i + 1, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, port_i, alarm_i + 1, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(ports);

    return cAtTrue;
    }

eBool CmdAtCienaEthPortBypassEnable(char argc, char **argv)
    {
    return BypassEnable(argc, argv, cAtTrue);
    }

eBool CmdAtCienaEthPortBypassDisable(char argc, char **argv)
    {
    return BypassEnable(argc, argv, cAtFalse);
    }

eBool CmdAtCienaEthPortSubportVlanTransmitSet(char argc, char **argv)
    {
    return SubportVlanSet(argc, argv, AtCienaEthPortSubportTxVlanSet);
    }

eBool CmdAtCienaEthPortSubportVlanExpectedSet(char argc, char **argv)
    {
    return SubportVlanSet(argc, argv, AtCienaEthPortSubportExpectedVlanSet);
    }

eBool CmdAtCienaEthPortShow(char argc, char **argv)
    {
    uint32 *idBuf;
    uint8 numPorts, port_i;
    AtEthPort *ports;
    tTab *tabPtr;
    eBool bypass;
    tAtVlan txVlan;
    tAtVlan expectedVlan;
    const char *pHeading[] = {"Port Id", "Bypass", "Transmit Vlan", "Expected Vlan",
                              "High Threshold(kbyte)", "Low Threshold(kbyte)",
                              "OOBWaterMarkMax(kbyte)", "OOBWaterMarkMin(kbyte)",
                              "OOB Fill Level (KB)", "Buffer Level (KB)"};
    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint8 column = 0;
        uint32 ret;

        StrToCell(tabPtr, port_i, column++, CliNumber2String((uint8)AtChannelIdGet((AtChannel)ports[port_i]) + 1, "%d"));

        if (AtCienaEthPortBypassIsSupported(ports[port_i]) == cAtFalse)
            ColorStrToCell(tabPtr, port_i, column++, "N/A", cSevNormal);
        else
            {
            bypass = AtCienaEthPortBypassIsEnabled(ports[port_i]);
            ColorStrToCell(tabPtr, port_i, column++, CliBoolToString(bypass), bypass ? cSevInfo : cSevCritical);
            }

        /* Print transmit vlan */
        ret = AtCienaEthPortSubportTxVlanGet(ports[port_i], &txVlan);
        if (ret == cAtOk)
            ColorStrToCell(tabPtr, port_i, column++, CliVlan2Str(&txVlan), cSevNormal);
        else
            {
            if (ret == cAtModuleEthErrorVlanNotExist)
                ColorStrToCell(tabPtr, port_i, column++, "dis", cSevNormal);
            else if (ret == cAtErrorNotApplicable)
                ColorStrToCell(tabPtr, port_i, column++, sAtNotApplicable, cSevNormal);
            else
                ColorStrToCell(tabPtr, port_i, column++, AtRet2String(ret), cSevCritical);
            }

        /* Print expected vlan */
        ret = AtCienaEthPortSubportExpectedVlanGet(ports[port_i], &expectedVlan);
        if (ret == cAtOk)
            ColorStrToCell(tabPtr, port_i, column++, CliVlan2Str(&expectedVlan), cSevNormal);
        else if (ret == cAtErrorNotApplicable)
            ColorStrToCell(tabPtr, port_i, column++, sAtNotApplicable, cSevNormal);
        else
            ColorStrToCell(tabPtr, port_i, column++, AtRet2String(ret), cSevCritical);

        /* Print flow control threshold */
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtCienaEthPortFlowControlHighThresholdGet(ports[port_i]), "%d"));
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtCienaEthPortFlowControlLowThresholdGet(ports[port_i]), "%d"));

        /* Print flow control OOB fifo watermark */
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtCienaEthPortFlowControlTxWaterMarkMaxGet(ports[port_i]), "%d"));
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtCienaEthPortFlowControlTxWaterMarkMinGet(ports[port_i]), "%d"));

        /* Print OOB level */
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtCienaEthPortFlowControlOOBBufferLevelGet(ports[port_i]), "%d"));

        /* Print Buffer level */
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtCienaEthPortFlowControlTxBufferLevelGet(ports[port_i]), "%d"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(ports);
    AtPrintc(cSevNormal, "VLANs format: %s\n", CliFullVlanExpectedFormat());

    return cAtTrue;
    }

eBool CmdAtCienaEthPortFlowControlHighThresholdSet(char argc, char ** argv)
    {
    return FlowControlThresholdSet(argc, argv, AtCienaEthPortFlowControlHighThresholdSet);
    }

eBool CmdAtCienaEthPortFlowControlLowThresholdSet(char argc, char ** argv)
    {
    return FlowControlThresholdSet(argc, argv, AtCienaEthPortFlowControlLowThresholdSet);
    }

eBool CmdAtCienaEthPortFlowControlWaterMarkReset(char argc, char ** argv)
    {
    return FlowControlWaterMarkReset(argc, argv);
    }

eBool CmdAtCienaModuleEthSubPortVlanTransmitTpidSet(char argc, char ** argv)
    {
    return VlanTpidSet(argc, argv, AtCienaModuleEthSubPortVlanTxTpidSet);
    }

eBool CmdAtCienaModuleEthSubPortVlanExpectedTpidSet(char argc, char ** argv)
    {
    return VlanTpidSet(argc, argv, AtCienaModuleEthSubPortVlanExpectedTpidSet);
    }

eBool CmdAtCienaModuleEthSubPortVlanTpidsShow(char argc, char ** argv)
    {
	uint8 numTpids, tpid_i;
	tTab *tabPtr;
	const char *pHeading[] = {"Index", "Transmit", "Expect"};
	AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

	AtUnused(argc);
	AtUnused(argv);

	/* Create table */
	numTpids = AtCienaModuleEthNumSubPortVlanTpids(ethModule);
	tabPtr = TableAlloc(numTpids, mCount(pHeading), pHeading);
	if (!tabPtr)
		{
		AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
		return cAtFalse;
		}

	/* Make table content */
	for (tpid_i = 0; tpid_i < numTpids; tpid_i++)
		{
		uint8 column = 0;

		StrToCell(tabPtr, tpid_i, column++, CliNumber2String(tpid_i + 1, "%d"));
		StrToCell(tabPtr, tpid_i, column++, CliNumber2String(AtCienaModuleEthSubPortVlanTxTpidGet(ethModule, tpid_i), "0x%04x"));
		StrToCell(tabPtr, tpid_i, column++, CliNumber2String(AtCienaModuleEthSubPortVlanExpectedTpidGet(ethModule, tpid_i), "0x%04x"));
		}

	TablePrint(tabPtr);
	TableFree(tabPtr);

	return cAtTrue;
    }

static tCliEthPortCounter *AllPossibleCounters(uint32 *numPossibleCounters)
    {
    static tCliEthPortCounter counters[] =
        {
        {cAtCienaSgmiiEthPortCounterTypeDccTxFrames               , cAtCounterTypeGood ,"DccTxFrames"             },
        {cAtCienaSgmiiEthPortCounterTypeDccTxBytes                , cAtCounterTypeGood ,"DccTxBytes"              },
        {cAtCienaSgmiiEthPortCounterTypeDccRxFrames               , cAtCounterTypeGood ,"DccRxFrames"             },
        {cAtCienaSgmiiEthPortCounterTypeDccRxBytes                , cAtCounterTypeGood ,"DccRxBytes"              },
        {cAtCienaSgmiiEthPortCounterTypeDccTxErrors               , cAtCounterTypeError,"DccTxErrors"             },
        {cAtCienaSgmiiEthPortCounterTypeDccRxErrors               , cAtCounterTypeError,"DccRxErrors"             },
        {cAtCienaSgmiiEthPortCounterTypeDccRxGoodFrames           , cAtCounterTypeGood ,"DccGoodFrames"           },
        {cAtCienaSgmiiEthPortCounterTypeDccRxDiscardFrames        , cAtCounterTypeError,"DccDiscardFrames"        },
        {cAtCienaSgmiiEthPortCounterTypeDccRxUnrecognizedFrames   , cAtCounterTypeError,"DccRxUnrecognizedFrames" },

        {cAtCienaSgmiiEthPortCounterTypeKByteTxFrames             , cAtCounterTypeGood ,"KByteTxFrames"           },
        {cAtCienaSgmiiEthPortCounterTypeKByteTxBytes              , cAtCounterTypeGood ,"KByteTxBytes"            },
        {cAtCienaSgmiiEthPortCounterTypeKByteRxFrames             , cAtCounterTypeGood ,"KByteRxFrames"           },
        {cAtCienaSgmiiEthPortCounterTypeKbyteRxGoodFrames         , cAtCounterTypeGood ,"KByteGoodFrames"         },
        {cAtCienaSgmiiEthPortCounterTypeKbyteRxDiscardFrames      , cAtCounterTypeError,"KByteDiscardFrames"      },
        {cAtCienaSgmiiEthPortCounterTypeKbyteRxUnrecognizedFrames , cAtCounterTypeError,"KbyteRxUnrecognizedFrames"},
        };

    if (numPossibleCounters)
        *numPossibleCounters = mCount(counters);

    return counters;
    }

static eBool CounterIsSupported(AtChannel *ports, uint32 numPorts, CliEthPortCounter counterInfo)
    {
    uint32 port_i;

    /* Not to affect auto testing system, just simply put all counters */
    if (AutotestIsEnabled())
        return cAtTrue;

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel port = ports[port_i];
        if (AtChannelCounterIsSupported(port, AtCienaDccKByteSgmiiCounterTypeToThaCounterType(counterInfo->type)))
            return cAtTrue;
        }

    return cAtFalse;
    }

static CliEthPortCounter *AllSupportedCountersDetect(AtChannel *ports, uint32 numPorts, uint32 *numSupportedCounters)
    {
    uint32 numPossibleCounters = 0, possibleCounter_i;
    tCliEthPortCounter *allPosisbleCounters = AllPossibleCounters(&numPossibleCounters);
    uint32 memorySize = sizeof(CliEthPortCounter) * numPossibleCounters;
    CliEthPortCounter *supportedCounters;

    supportedCounters = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(supportedCounters, 0, memorySize);
    if (supportedCounters == NULL)
        return NULL;
    *numSupportedCounters = 0;

    for (possibleCounter_i = 0; possibleCounter_i < numPossibleCounters; possibleCounter_i++)
        {
        CliEthPortCounter counterInfo = &(allPosisbleCounters[possibleCounter_i]);

        if (CounterIsSupported(ports, numPorts, counterInfo))
            {
            supportedCounters[*numSupportedCounters] = counterInfo;
            *numSupportedCounters = *numSupportedCounters + 1;
            }
        }

    return supportedCounters;
    }

static void HeadingFree(char** heading, uint8 numItem)
    {
    uint8 i;
    for (i = 0; i < numItem; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static eBool PortCounterGet(AtEthPort *ports, uint32 numPorts, char argc, char **argv)
    {
    uint32 i;
    tTab *tabPtr = NULL;
    char **heading;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    uint32 (*CounterGet)(AtEthPort self, eAtCienaSgmiiEthPortCounterType counterType) = AtCienaDccKByteSgmiiCounterGet;
    uint32 numSupportedCounters = 0;
    CliEthPortCounter *allSupportedCounters = NULL;
    AtUnused(argc);

    /* Get counter mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtCienaDccKByteSgmiiCounterClear;

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPorts + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (i = 1; i <= numPorts; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(3);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPorts; i++)
        AtSprintf((void*)heading[i], "%s", AtChannelIdString((AtChannel)ports[i- 1]));

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
        tabPtr = TableAlloc(0, numPorts + 1UL, (void *)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    allSupportedCounters = AllSupportedCountersDetect((AtChannel *)(void *)ports, numPorts, &numSupportedCounters);
    for (i = 1; i <= numPorts; i++)
        {
        uint32 counter_i;
        uint16 rowId = 0;
        AtEthPort port = (AtEthPort)ports[i - 1];

        for (counter_i = 0; counter_i < numSupportedCounters; counter_i++)
            {
            CliEthPortCounter counterInfo = allSupportedCounters[counter_i];
            mPutCounter(counterInfo->type, counterInfo->kind, counterInfo->name);
            }
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    for (i = 0; i <= numPorts; i++)
        AtOsalMemFree((void*)heading[i]);
    AtOsalMemFree(heading);

    AtOsalMemFree(allSupportedCounters);

    return cAtTrue;
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [ETH] Defect changed at %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtCienaSgmiiEthPortAlarmTypeStr); alarm_i++)
        {
        uint32 alarmType = cAtCienaSgmiiEthPortAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtCienaSgmiiEthPortAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");

        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

eBool CmdAtCienaEthPortSgmiiCountersGet(char argc, char **argv)
    {
    uint32 *idBuf;
    uint8 numPorts;
    AtEthPort *ports;
    eBool ret = cAtFalse;

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    ret = PortCounterGet(ports, numPorts, argc, argv);
    AtOsalMemFree(ports);

    return ret;
    }

eBool CmdAtCienaEthPortExpectedCVlanSet(char argc, char **argv)
    {
    uint32 *idBuf;
    AtEthPort *ports;
    uint8 numPorts, port_i;
    uint16 vlanId;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    vlanId = (uint16)AtStrToDw(argv[1]);
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = AtCienaEthPortExpectedCVlanSet(ports[port_i], vlanId);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Can not set for %s, ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel) ports[port_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtOsalMemFree(ports);
    return success;
    }

eBool CmdAtCienaSgmiiEthPortIntrMsk(char argc, char **argv)
    {
    eAtRet ret;
    uint32 intrMaskVal;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    uint32 *idBuf;
    AtEthPort *ports;
    uint8 numPorts, port_i;
    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = IntrMaskFromString(argv[1])) == 0)
        {
        AtOsalMemFree(ports);
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtOsalMemFree(ports);
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel channel = (AtChannel)ports[port_i];
		ret = AtChannelInterruptMaskSet(channel, intrMaskVal, enable ? intrMaskVal : 0x0);
		if (ret != cAtOk)
			{
			AtPrintc(cSevCritical, "ERROR: Cannot configure interrupt mask for Channel %s, ret = %s\r\n",
					 AtChannelIdString(channel),
					 AtRet2String(ret));
			success = cAtFalse;
			}
        }

    AtOsalMemFree(ports);
    return success;
    }

eBool CmdAtCienaSgmiiEthPortShow(char argc, char **argv)
    {
    tTab *tabPtr = NULL;
    const char *heading[] = {"PortId", "intrMask", "expectedGlobalMac", "expectedCVlan"};
    uint32 *idBuf;
    AtEthPort *ports;
    uint8 numPorts, port_i;
    uint8 mac[cAtMacAddressLen];

    /* Get Port List */
    AtUnused(argc);
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }
	/* Create table */
	tabPtr = TableAlloc(numPorts, mCount(heading), heading);
	if (!tabPtr)
		{
		AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtOsalMemFree(ports);
		return cAtFalse;
		}

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint32 colum = 0;
        AtChannel channel = (AtChannel)ports[port_i];
        const char *strValue;

        StrToCell(tabPtr, port_i, colum++, AtChannelIdString(channel));

        strValue = Alarm2String(AtChannelInterruptMaskGet(channel));
        ColorStrToCell(tabPtr, port_i, colum++, strValue, cSevNormal);

        if (AtCienaDccExpectGlobalMacGet(ports[port_i], mac) == cAtOk)
            StrToCell(tabPtr, port_i, colum++, CliEthPortMacAddressByte2Str(mac));
        else
            StrToCell(tabPtr, port_i, colum++, sAtNotApplicable);

        StrToCell(tabPtr, port_i, colum++, CliNumber2String(AtCienaEthPortExpectedCVlanGet(ports[port_i]), "0x%04x"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(ports);

    return cAtTrue;
    }

eBool CmdAtCienaSgmiiEthPortInterruptsShow(char argc, char **argv)
    {
    uint32 (*AlarmInterruptGet)(AtChannel self) = AtChannelAlarmInterruptGet;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    /* Silent */
    if (argc >= 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        AlarmInterruptGet = AtChannelAlarmInterruptClear;

    return StatusShow(argv[0], silent, AlarmInterruptGet);
    }

eBool CmdAtCienaSgmiiEthPortDefectShow(char argc, char **argv)
    {
    AtUnused(argc);
    return StatusShow(argv[0], cAtFalse, AtChannelDefectGet);
    }

eBool CmdAtCienaSgmiiEthPortAlarmCapture(char argc, char **argv)
    {
    uint32 *idBuf;
    AtEthPort *ports;
    uint8 numPorts, port_i;
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    static tAtChannelEventListener listener;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.AlarmChangeState = AlarmChangeState;

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret;
        AtChannel port = (AtChannel)ports[port_i];

        if (captureEnabled)
            ret = AtChannelEventListenerAdd(port, &listener);
        else
            ret = AtChannelEventListenerRemove(port, &listener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(port),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaDevicePdh.c
 *
 * Created Date: Oct 1, 2016
 *
 * Description : Customer specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCiena.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtCienaPdhInterfaceStr[] = {
                                      "de3",
                                      "de1"
                                        };

static const uint32 cAtCienaPdhInterfaceVal[]  = {cAtCienaPdhInterfaceTypeDe3,
                                                  cAtCienaPdhInterfaceTypeDe1
                                                  };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool TxEthEnable(char argc, char** argv, eBool enabled)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);
    ret = AtCienaPdhCemModulePdhMuxTxEthEnable(pdhModule, enabled);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: %s fail with ret = %s\r\n",
             enabled ? "Enabling" : "Disabling",
             AtRet2String(ret));

    return cAtFalse;
    }

eBool CmdAtCienaPdhInterfaceSet(char argc, char** argv)
    {
    eAtCienaPdhInterfaceType type;
    eBool blResult;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);

    AtUnused(argc);

    mAtStrToEnum(cAtCienaPdhInterfaceStr, cAtCienaPdhInterfaceVal, argv[0], type, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid interface parameter. Expected: de1, de3\r\n");
        return cAtFalse;
        }

    return AtCienaModulePdhInterfaceTypeSet(pdhModule, type) == cAtOk ? cAtTrue : cAtFalse;
    }

eBool CmdAtCienaPdhTxEthEnable(char argc, char** argv)
    {
    return TxEthEnable(argc, argv, cAtTrue);
    }

eBool CmdAtCienaPdhTxEthDisable(char argc, char** argv)
    {
    return TxEthEnable(argc, argv, cAtFalse);
    }

eBool CmdAtCienaPdhShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"PdhInterface", "PdhToEth"};
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtCienaPdhInterfaceType type = AtCienaModulePdhInterfaceTypeGet(pdhModule);
    const char *interfaceStr;
    eBool isEnabled;

    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    interfaceStr = CliEnumToString(type, cAtCienaPdhInterfaceStr, cAtCienaPdhInterfaceVal, mCount(cAtCienaPdhInterfaceVal), NULL);
    StrToCell(tabPtr, 0, 0, interfaceStr ? interfaceStr : "Error");

    isEnabled = AtCienaPdhCemModulePdhMuxTxEthIsEnabled(pdhModule);
    StrToCell(tabPtr, 0, 1, CliBoolToString(isEnabled));

    /* Show it */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtUnused(argc);
    AtUnused(argv);

    return cAtTrue;
    }

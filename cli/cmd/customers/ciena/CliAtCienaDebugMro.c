/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaDebugMro.c
 *
 * Created Date: Aug 6, 2017
 *
 * Description : Debug CLIs for MRO
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/codechip/Tha60290022/eth/Tha60290022ModuleEth.h"
#include "../../../../customers/src/AtCienaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet Tha60290022DeviceTxShapperEnable(AtDevice self, eBool enable);

/*--------------------------- Implementation ---------------------------------*/
static eBool EthMacLoopbackSet(char argc, char** argv, eAtRet (*LoopbackSetFunc)(AtModuleEth self, eAtLoopbackMode mode))
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtLoopbackMode mode = CliLoopbackModeFromString(argv[0]);
    eAtRet ret;

    AtUnused(argc);

    ret = LoopbackSetFunc(ethModule, mode);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: loopback fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

static eBool ExcessiveErrorRatioThresholdSet(char argc, char **argv, eAtRet (*ThresholdSet)(AtEthPort self, uint32 threshold))
    {
    eBool success = cAtTrue;
    uint32 numPorts;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    uint32 threshold = AtStrToDw(argv[1]);
    uint32 port_i;

    AtUnused(argc);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (ports == NULL)
        {
        AtPrintc(cSevInfo, "No port to configure, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = ThresholdSet((AtEthPort)ports[port_i], threshold);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Set threshold fail on %s with ret = %s\r\n",
                 AtObjectToString((AtObject)ports[port_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdTha60290022ModuleEthFaceplateMacLoopbackSet(char argc, char** argv)
    {
    return EthMacLoopbackSet(argc, argv, Tha60290022ModuleEthFaceplateMacLoopbackSet);
    }

eBool CmdTha60290022ModuleEthBackplaneMacLoopbackSet(char argc, char** argv)
    {
    return EthMacLoopbackSet(argc, argv, Tha60290022ModuleEthBackplaneMacLoopbackSet);
    }

eBool CmdAtCienaEthPortExcessiveErrorRatioUpperThreshold(char argc, char **argv)
    {
    return ExcessiveErrorRatioThresholdSet(argc, argv, AtCienaEthPortExcessiveErrorRatioUpperThresholdSet);
    }

eBool CmdAtCienaEthPortExcessiveErrorRatioLowerThreshold(char argc, char **argv)
    {
    return ExcessiveErrorRatioThresholdSet(argc, argv, AtCienaEthPortExcessiveErrorRatioLowerThresholdSet);
    }

eBool CmdAtCienaEthPortExcessiveErrorRatioThresholdShow(char argc, char **argv)
    {
    const char *pHeading[] = {"Port Id", "Upper", "UpperMax", "Lower", "LowerMax"};
    tTab *tabPtr = NULL;
    uint32 numPorts;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    uint32 port_i;

    AtUnused(argc);
    AtUnused(argv);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (ports == NULL)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = (AtEthPort)ports[port_i];
        uint32 column = 0;
        uint32 value;

        StrToCell(tabPtr, port_i, column++, AtObjectToString((AtObject)port));

        if (AtCienaEthPortExcessiveErrorRatioThresholdSupported(port))
            {
            value = AtCienaEthPortExcessiveErrorRatioUpperThresholdGet(port);
            StrToCell(tabPtr, port_i, column++, CliNumber2String(value, "%u"));

            value = AtCienaEthPortExcessiveErrorRatioUpperThresholdMax(port);
            StrToCell(tabPtr, port_i, column++, CliNumber2String(value, "%u"));

            value = AtCienaEthPortExcessiveErrorRatioLowerThresholdGet(port);
            StrToCell(tabPtr, port_i, column++, CliNumber2String(value, "%u"));

            value = AtCienaEthPortExcessiveErrorRatioLowerThresholdMax(port);
            StrToCell(tabPtr, port_i, column++, CliNumber2String(value, "%u"));
            }
        else
            {
            for (; column < mCount(pHeading); column++)
                StrToCell(tabPtr, port_i, column, sAtNotSupported);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdTha60290022DeviceTxShapperEnable(char argc, char** argv)
    {
    AtDevice device = CliDevice();
    AtUnused(argc);
    AtUnused(argv);
    Tha60290022DeviceTxShapperEnable(device, cAtTrue);
    return cAtTrue;
    }

eBool CmdTha60290022DeviceTxShapperDisable(char argc, char** argv)
    {
    AtDevice device = CliDevice();
    AtUnused(argc);
    AtUnused(argv);
    Tha60290022DeviceTxShapperEnable(device, cAtFalse);
    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtCiena.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Ciena specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtEthVlanTag.h"
#include "AtDevice.h"
#include "AtCiena.h"
#include "AtTokenizer.h"
#include "CliAtCiena.h"
#include "../../pw/CliAtModulePw.h"
#include "../../sdh/CliAtModuleSdh.h"
#include "../../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
typedef eAtRet (*ModulePwAttributeSetFunc)(AtModulePw, uint32);
typedef eAtRet (*KByteChannelAttributeSetFunc)(AtChannel, uint8);
typedef eAtRet (*MacPwAttributeSetFunc)(AtPw, const uint8 *);
typedef eAtRet (*LabelSetFunc)(AtChannel, uint16);
typedef eAtRet (*SdhLineTxEKSetFunc)(AtSdhLine, uint8);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtCienaSdhLineExtendedKByteStr[] = {
                                      "d1-sts1-4",
                                      "d1-sts1-10"
                                        };

static const uint32 cAtCienaSdhLineExtendedKByteVal[]  = {cAtCienaSdhLineExtendedKBytePositionD1Sts4,
                                                          cAtCienaSdhLineExtendedKBytePositionD1Sts10
                                                          };

static const char *cAtCienaSdhLineKByteSourceStr[] = {
                                      "cpu",
                                      "sgmii"
                                        };

static const uint32 cAtCienaSdhLineKByteSourceVal[]  = {cAtCienaSdhLineKByteSourceCpu,
                                                        cAtCienaSdhLineKByteSourceSgmii
                                                       };
static const char *cAtCienaKbyteApsPwAlarmTypeStr[] =
    {
    "da-mismatch",
    "ethtype-mismatch",
    "ver-mismatch",
    "type-mismatch",
    "length-field-mismatch",
    "length-count-mismatch",
    "channel-mismatch",
    "watchdog-expire"
    };

static const uint32 cAtCienaKbyteApsPwAlarmTypeVal[] =
    {
    cAtCienaKbyteApsPwAlarmTypeDaMacMisMatch,
    cAtCienaKbyteApsPwAlarmTypeEthTypeMisMatch,
    cAtCienaKbyteApsPwAlarmTypeVerMisMatch,
    cAtCienaKbyteApsPwAlarmTypeTypeMisMatch,
    cAtCienaKbyteApsPwAlarmTypeLengthFieldMisMatch,
    cAtCienaKbyteApsPwAlarmTypeLengthCountMisMatch,
    cAtCienaKbyteApsPwAlarmTypeChannelMisMatch,
    cAtCienaKbyteApsPwAlarmTypeWatchdogTimer
    };

static const char *cAtCienaKbyteApsChannelAlarmTypeStr[] =
    {
    "channel-mismatch"
    };

static const uint32 cAtCienaKbyteApsChannelAlarmTypeVal[] =
    {
    cAtCienaKbyteChannelAlarmTypeMismatch
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw KBytePwGet(void)
    {
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    return AtCienaKBytePwGet(pwModule);
    }

static AtChannel *ChannelListFromString(char *idString, uint32 *numChannels)
    {
    uint32 bufferSize;
    uint32 *idBuffer = CliSharedIdBufferGet(&bufferSize);
    uint32 numIds = CliIdListFromString(idString, idBuffer, bufferSize);
    AtChannel *channels = CliSharedChannelListGet(&bufferSize);
    uint32 channel_i;
    uint32 numValidIds = 0;
    AtPw kBytePw = KBytePwGet();

    *numChannels = 0;

    for (channel_i = 0; channel_i < numIds; channel_i++)
        {
        uint32 channelId = idBuffer[channel_i] - 1;

        channels[numValidIds] = (AtChannel)AtCienaKbytePwChannelGet(kBytePw, (uint8)channelId);
        if (channels[numValidIds] == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: ignore invalid channel %d\r\n", idBuffer[channel_i]);
            continue;
            }

        numValidIds = numValidIds + 1;
        }

    *numChannels = numValidIds;
    return channels;
    }

static const char **AlarmTypeStr(uint8 *numAlarms)
    {
    if (numAlarms)
        *numAlarms = mCount(cAtCienaKbyteApsPwAlarmTypeStr);
    return cAtCienaKbyteApsPwAlarmTypeStr;
    }

static const uint32 *AlarmTypeVal(uint8 *numAlarms)
    {
    if (numAlarms)
        *numAlarms = mCount(cAtCienaKbyteApsPwAlarmTypeVal);
    return cAtCienaKbyteApsPwAlarmTypeVal;
    }

static uint32 IntrMaskFromString(char *pIntrStr)
    {
    uint8 numAlarms = 0;
    const char **alarmTypeStrings = AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypes = AlarmTypeVal(&numAlarms);
    return CliMaskFromString(pIntrStr, alarmTypeStrings, alarmTypes, numAlarms);
    }

static const char *Alarm2String(uint32 alarms)
    {
    uint16 alarm_i;
    uint32 bufferSize;
    char *alarmString = CliSharedCharBufferGet(&bufferSize);
    uint8 numAlarms;
    const char **alarmTypeStr = AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = AlarmTypeVal(&numAlarms);

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        {
        AtSprintf(alarmString, "none");
        return alarmString;
        }

    /* There are alarms */
    for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
        {
        if (alarms & (alarmTypeVal[alarm_i]))
            AtSprintf(alarmString, "%s%s|", alarmString, alarmTypeStr[alarm_i]);
        }

    if (AtStrlen(alarmString) == 0)
        return "None";

    /* Remove the last '|' */
    alarmString[AtStrlen(alarmString) - 1] = '\0';

    return alarmString;
    }

static eBool ModuleAttributeSet(char **argv, ModulePwAttributeSetFunc func, uint32 maxVal)
    {
    eAtRet ret;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 value = AtStrtoul(argv[0], NULL, 16);

    if (value > maxVal)
        {
        AtPrintc(cSevCritical, "ERROR: Input %s is out-of-range, max = %d\r\n", argv[0], maxVal);
        return cAtFalse;
        }

    ret = func(pwModule, value);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Setting module attribute with value %d fail with ret = %s\r\n",
             value,
             AtRet2String(ret));
    return cAtFalse;
    }

static eBool ChannelAttributeSet(char *channelIdString, KByteChannelAttributeSetFunc func, uint8 value)
    {
    uint32 numChannels, channel_i;
    AtChannel *channels = ChannelListFromString(channelIdString, &numChannels);
    eBool success = cAtTrue;
    AtPw pw;

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    pw = KBytePwGet();
    if (pw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[channel_i];
        eAtRet ret = func(channel, value);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Configure fail on channel %s with ret = %s\r\n",
                 CliChannelIdStringGet(channel),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool ChannelEnable(char argc, char **argv, KByteChannelAttributeSetFunc func, eBool enable)
    {
    AtUnused(argc);
    return ChannelAttributeSet(argv[0], func, enable);
    }

static eBool KByteValueSet(char argc, char **argv, KByteChannelAttributeSetFunc func)
    {
    uint32 value = AtStrtoul(argv[1], NULL, 16);

    if (value > cBit7_0)
        {
        AtPrintc(cSevCritical, "ERROR: KByte value must be in range [0..255]\r\n");
        return cAtFalse;
        }

    AtUnused(argc);
    return ChannelAttributeSet(argv[0], func, (uint8)value);
    }

static eBool PwAttributeSet(char argc, char **argv, KBytePwAttributeSetFunc func, uint32 maxValue)
    {
    eAtRet ret;
    uint32 value = AtStrToDw(argv[0]);
    AtPw pw = KBytePwGet();

    AtUnused(argc);
    if (pw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist\r\n");
        return cAtFalse;
        }

    if (value > maxValue)
        {
        AtPrintc(cSevCritical, "ERROR: Value is out-of-range\r\n");
        return cAtFalse;
        }

    ret = func(pw, value);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Configure attribute value = %s with ret = %s\r\n", argv[0], AtRet2String(ret));

    return cAtFalse;
    }

static AtList LineListGet(char **argv, uint32 *numLine)
    {
    AtList lineList = AtListCreate(0);
    CliSdhLineListFromString(lineList, argv[0]);
    *numLine = AtListLengthGet(lineList);

    return lineList;
    }

static eBool ExtendedKByteEnable(char argc, char **argv, eBool enable)
    {
    eBool success;
    uint32 i, numberOfLines;
    AtList lineList = LineListGet(argv, &numberOfLines);

    AtUnused(argc);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine) AtListObjectGet(lineList, i);
        eAtRet ret = AtCienaSdhLineExtendedKByteEnable(line, enable);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Cannot %s Extended K Byte of line %u, ret = %s\r\n",
                 enable ? "enable" : "disable",
                 AtChannelIdGet((AtChannel)line) + 1,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

static char** BuildHeadingForPws(AtPw *pwList, uint32 currentPwIndex, uint32 numPwsPerTable)
    {
    AtPw   pw;
    uint32 i;
    char   **headings;

    headings = AtOsalMemAlloc((numPwsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(32);
    AtSprintf(headings[0], "PW");

    for (i = 0; i < numPwsPerTable; i++)
        {
        pw = pwList[currentPwIndex + i];
        headings[i + 1] = AtOsalMemAlloc(32);
        AtSprintf(headings[i + 1], "%s", AtChannelTypeString((AtChannel) pw));
        }

    return headings;
    }

static eBool KByteLabelSet(char argc, char **argv, LabelSetFunc func)
    {
    uint32 channel_i;
    uint32 numChannels = 0;
    AtChannel *channels = ChannelListFromString(argv[0], &numChannels);
    AtTokenizer labelTokenizer;
    AtPw pw;
    eBool success = cAtTrue;
    const char *labelString = NULL;

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtTrue;
        }

    labelTokenizer = AtTokenizerSharedTokenizer(argv[1], ",");
    if (AtTokenizerNumStrings(labelTokenizer) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no labels to process\r\n");
        return cAtTrue;
        }

    pw = KBytePwGet();
    if (pw == NULL)
       {
       AtPrintc(cSevCritical, "ERROR: KBytePw does not exist\r\n");
       return cAtFalse;
       }

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[channel_i];
        uint32 label;
        eAtRet ret;

        /* If running out of label, use the remaining last one */
        if (AtTokenizerHasNextString(labelTokenizer))
            labelString = AtTokenizerNextString(labelTokenizer);

        label = AtStrToDw(labelString);

        if (label > cBit15_0) /* Let internal logic handle 16-bit value range */
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure label 0x%x for channel %s. Label must be in range [0..4095]\r\n",
                     label, CliChannelIdStringGet(channel));
            success = cAtFalse;
            continue;
            }

        ret = func(channel, (uint16)label);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Change label on channel %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(channel),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool SdhLineTxEKSet(char argc, char **argv, SdhLineTxEKSetFunc func)
    {
    AtList lineList;
    uint32 numberOfLines, line_i;
    uint8 value = 0;
    eBool success = cAtTrue;
    AtUnused(argc);

    lineList = LineListGet(argv, &numberOfLines);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (line_i = 0; line_i < numberOfLines; line_i = AtCliNextIdWithSleep(line_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine) AtListObjectGet(lineList, line_i);
        eAtRet ret = func(line, value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Cannot set value K Byte of line %u, ret = %s\r\n",
                 AtChannelIdGet((AtChannel)line) + 1,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)lineList);

    return success;
    }

static eBool StatusShowWithSilent(eBool silent, uint32 (*AlarmGet)(AtChannel self))
    {
    uint32 alarmStat, alarm_i;
    tTab *tabPtr = NULL;
    AtPw kBytePw = KBytePwGet();

    if (!silent)
        {
        /* Create table */
        tabPtr = TableAlloc(1, mCount(cAtCienaKbyteApsPwAlarmTypeStr), cAtCienaKbyteApsPwAlarmTypeStr);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    /* Print alarm status */
    alarmStat = AlarmGet((AtChannel)kBytePw);
    for (alarm_i = 0; alarm_i < mCount(cAtCienaKbyteApsPwAlarmTypeVal); alarm_i++)
        {
        uint32 set = alarmStat & cAtCienaKbyteApsPwAlarmTypeVal[alarm_i];
        ColorStrToCell(tabPtr, 0, alarm_i, set ? "set" : "clear", set ? cSevCritical : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool ShowChannelsAlarmWithSilent(char *channelString, uint32 (*AlarmGetFunc)(AtChannel self), eBool silent)
    {
    uint32 numChannels = 0;
    AtChannel *channels = ChannelListFromString(channelString, &numChannels);
    uint32 channel_i;
    AtPw pw = KBytePwGet();
    tTab *tabPtr = NULL;
    const char *heading[] = {"ChannelId", "channel-mismatch"};

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    if (pw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist \r\n");
        return cAtFalse;
        }

    if (!silent)
        {
        tabPtr = TableAlloc(numChannels, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtChannel channel = channels[channel_i];
        uint32 colum = 0;
        uint32 alarms = AlarmGetFunc(channel);
        uint32 alarmSet;

        StrToCell(tabPtr, channel_i, colum++, CliChannelIdStringGet(channel));

        alarmSet = alarms & cAtCienaKbyteChannelAlarmTypeMismatch;
        ColorStrToCell(tabPtr, channel_i, colum++, alarmSet ? "set" : "clear", alarmSet ? cSevCritical : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

#if 0
static char *ChannelStrListGet(tAtCienaKbyteApsChannelAlarms *channelAlarms)
    {
    static char channelStr[256];
    char *pBuff = channelStr;
    uint8 channel_i;

    AtOsalMemInit(channelStr, 0, 128);

    for (channel_i = 0; channel_i < 16; channel_i++)
        {
        uint32 channelMask = (uint32)(cBit0 << channel_i);
        if (channelAlarms->changedDefects & channelMask)
            {
            AtSprintf(pBuff, "{%d:%s},", channel_i, (channelAlarms->currentDefects & channelMask) ? "set" : "clear");
            pBuff = &channelStr[AtStrlen(channelStr)];
            }
        }
    channelStr[AtStrlen(channelStr)] = '\0';

    return channelStr;
    }
#endif

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    AtUnused(channel);

    AtPrintc(cSevNormal, "\r\n%s [PW] Defect changed at KByte PW", AtOsalDateTimeInUsGet());
    for (alarm_i = 0; alarm_i < mCount(cAtCienaKbyteApsPwAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtCienaKbyteApsPwAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtCienaKbyteApsPwAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void KbyteChannelAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    AtUnused(channel);

    AtPrintc(cSevNormal, "\r\n%s [PW] Defect changed at KByte PW", AtOsalDateTimeInUsGet());
    for (alarm_i = 0; alarm_i < mCount(cAtCienaKbyteApsPwAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtCienaKbyteApsPwAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtCienaKbyteApsPwAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

eBool CliAtCienaKBytePwAttributeSetWithMaxValue(char argc, char **argv, KBytePwAttributeSetFunc func, uint32 maxValue)
    {
    return PwAttributeSet(argc, argv, func, maxValue);
    }

eBool CmdAtCienaPwKbyteEthTypeSet(char argc, char **argv)
    {
    AtUnused(argc);
    return ModuleAttributeSet(argv, (ModulePwAttributeSetFunc)AtCienaKBytePwEthTypeSet, cInvalidUint16);
    }

eBool CmdAtCienaPwKbyteTypeSet(char argc, char **argv)
    {
    AtUnused(argc);
    return ModuleAttributeSet(argv, (ModulePwAttributeSetFunc)AtCienaKBytePwTypeSet, cInvalidUint8);
    }

eBool CmdAtCienaPwKbyteVersionSet(char argc, char **argv)
    {
    AtUnused(argc);
    return ModuleAttributeSet(argv, (ModulePwAttributeSetFunc)AtCienaKBytePwVersionSet, cInvalidUint8);
    }

eBool CmdAtCienaPwKbyteShow(char argc, char **argv)
    {
    uint32 timerInUs;
    tAtEthVlanTag  cVlan, sVlan;
    uint8 mac[cAtMacAddressLen];
    tTab *tabPtr;
    const char *heading[] =  {"PW", "EthPort",  "CentralMAC",  "RemoteMAC",  "VLAN", "EthType", "Version", "Type", "Interval[us]", "WatchDog[us]", "intrMask"};
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtPw kBytePw = KBytePwGet();
    AtEthPort ethPort = AtPwEthPortGet(kBytePw);
    uint32 colum = 0;
    uint32 intrMaskVal;
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, colum++, AtChannelIdString((AtChannel) kBytePw));
    if (ethPort == NULL)
        StrToCell(tabPtr, 0, colum++, "none");
    else
        {
        AtSnprintf(buffer, bufferSize, "%s(%d)", AtObjectToString((AtObject)ethPort), AtChannelIdGet((AtChannel)ethPort) + 1);
        StrToCell(tabPtr, 0, colum++, buffer);
        }

    /* MACs */
    AtCienaKBytePwCentralMacGet(kBytePw, mac);
    StrToCell(tabPtr, 0, colum++, CliEthPortMacAddressByte2Str(mac));
    AtCienaKBytePwRemoteMacGet(kBytePw, mac);
    StrToCell(tabPtr, 0, colum++, CliEthPortMacAddressByte2Str(mac));

    /* VLAN */
    StrToCell(tabPtr, 0, colum++, CliVlans2Str(AtPwEthCVlanGet(kBytePw, &cVlan), AtPwEthSVlanGet(kBytePw, &sVlan)));

    StrToCell(tabPtr, 0, colum++, CliNumber2String(AtCienaKBytePwEthTypeGet(pwModule), "0x%04x"));
	StrToCell(tabPtr, 0, colum++, CliNumber2String(AtCienaKBytePwVersionGet(pwModule), "0x%02x"));
	StrToCell(tabPtr, 0, colum++, CliNumber2String(AtCienaKBytePwTypeGet(pwModule), "0x%02x"));

    /* Timer */
    timerInUs = AtCienaKBytePwIntervalGet(kBytePw);
    StrToCell(tabPtr, 0, colum++, CliNumber2String(timerInUs, "%u"));
    timerInUs = AtCienaKBytePwWatchdogTimerGet(kBytePw);
    StrToCell(tabPtr, 0, colum++, CliNumber2String(timerInUs, "%u"));

    /* Print interrupt mask values */
    intrMaskVal = AtChannelInterruptMaskGet((AtChannel)kBytePw);
    ColorStrToCell(tabPtr, 0, colum++, Alarm2String(intrMaskVal), cSevNormal);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaPwKByteChannelValidationEnable(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, AtCienaKbyteChannelValidationEnable, cAtTrue);
    }

eBool CmdAtCienaPwKByteChannelValidationDisable(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, AtCienaKbyteChannelValidationEnable, cAtFalse);
    }

eBool CmdAtCienaPwKByteChannelsShow(char argc, char **argv)
    {
    uint32 numChannels = 0;
    AtChannel *channels = ChannelListFromString(argv[0], &numChannels);
    uint32 channel_i;
    AtPw pw = KBytePwGet();
    tTab *tabPtr;
    const char *heading[] = {"ChannelId", "TxLabel", "ExpectedLabel", "Validation", "Suppress",
                             "Override", "OverrideK1", "OverrideK2", "OverrideEK1", "OverrideEK2",
                             "RxK1", "RxK2", "RxEK1", "RxEK2", "IntrMask"};

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    if (pw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist \r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtChannel channel = channels[channel_i];
        uint32 colum = 0;
        eBool enabled;
        uint32 intrMask;
        const char *strValue;

        StrToCell(tabPtr, channel_i, colum++, CliChannelIdStringGet(channel));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelTxLabelGet(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelExpectedLabelGet(channel), "0x%02x"));

        enabled = AtCienaKbyteChannelValidationIsEnabled(channel);
        ColorStrToCell(tabPtr, channel_i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        enabled = AtCienaKbyteChannelSuppressionIsEnabled(channel);
        ColorStrToCell(tabPtr, channel_i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        enabled = AtCienaKbyteChannelOverrideIsEnabled(channel);
        ColorStrToCell(tabPtr, channel_i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelTxK1Get(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelTxK2Get(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelTxEK1Get(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelTxEK2Get(channel), "0x%02x"));

        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelRxK1Get(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelRxK2Get(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelRxEK1Get(channel), "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtCienaKbyteChannelRxEK2Get(channel), "0x%02x"));

        intrMask = AtChannelInterruptMaskGet(channel);
        if (intrMask == 0)
            ColorStrToCell(tabPtr, channel_i, colum++, "none", cSevInfo);
        else
            {
            strValue = CliAlarmMaskToString(cAtCienaKbyteApsChannelAlarmTypeStr,
                                            cAtCienaKbyteApsChannelAlarmTypeVal,
                                            mCount(cAtCienaKbyteApsChannelAlarmTypeVal),
                                            intrMask);
            ColorStrToCell(tabPtr, channel_i, colum++, strValue, cSevCritical);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaKBytePwRemoteMacSet(char argc, char **argv)
    {
    AtList pwList = AtListCreate(0);
    AtUnused(argc);
	AtListObjectAdd(pwList, (AtObject) KBytePwGet());
    return CliPwEthMacSet(pwList, argv[0], (MacSetFunc)AtCienaKBytePwRemoteMacSet);
    }

eBool CmdAtCienaKBytePwCentralMacSet(char argc, char **argv)
    {
    AtList pwList = AtListCreate(0);
    AtUnused(argc);
	AtListObjectAdd(pwList, (AtObject) KBytePwGet());
    return CliPwEthMacSet(pwList, argv[0], (MacSetFunc)AtCienaKBytePwCentralMacSet);
    }

eBool CmdAtCienaPwKByteEthHeaderSet(char argc, char **argv)
    {
	AtPw pw[1];
    AtUnused(argc);
	pw[0] = KBytePwGet();
    return CliPwEthHeaderSet(pw, 1, argv[0], argv[1], argv[2], AtPwEthHeaderSet);
    }

eBool CmdAtCienaSdhLineExtendedKByteEnable(char argc, char **argv)
    {
    return ExtendedKByteEnable(argc, argv, cAtTrue);
    }

eBool CmdAtCienaSdhLineExtendedKByteDisable(char argc, char **argv)
    {
    return ExtendedKByteEnable(argc, argv, cAtFalse);
    }

eBool CmdAtCienaSdhLineExtendedKBytePositionSet(char argc, char** argv)
    {
    eAtCienaSdhLineExtendedKBytePosition position;
    eBool convertSuccess, success = cAtTrue;
    uint32 line_i, numberOfLines;
    AtList lineList = LineListGet(argv, &numberOfLines);

    AtUnused(argc);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    position =  CliStringToEnum(argv[1], cAtCienaSdhLineExtendedKByteStr, cAtCienaSdhLineExtendedKByteVal, mCount(cAtCienaSdhLineExtendedKByteVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Position of Extended K Byte parameter. Expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtCienaSdhLineExtendedKByteStr, mCount(cAtCienaSdhLineExtendedKByteVal));
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (line_i = 0; line_i < numberOfLines; line_i = AtCliNextIdWithSleep(line_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine) AtListObjectGet(lineList, line_i);
        eAtRet ret = AtCienaSdhLineExtendedKBytePositionSet(line, position);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Cannot configure Position Extended K Byte of line %u, ret = %s\r\n",
                 AtChannelIdGet((AtChannel)line) + 1,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)lineList);

    return success;
    }

eBool CmdAtCienaSdhLineExtendedKByteShow(char argc, char **argv)
    {
    uint32 i, numberOfLines;
    AtList lineList = LineListGet(argv, &numberOfLines);
    eBool isEnable;
    tTab *tabPtr;
    const char *pHeading[] = {"LineId", "enable", "position"};
    AtUnused(argc);

    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i++)
        {
        AtSdhLine line = (AtSdhLine) AtListObjectGet(lineList, i);
        eAtCienaSdhLineExtendedKBytePosition position = AtCienaSdhLineExtendedKBytePositionGet(line);
        const char* positionStr = CliEnumToString(position,
                                                  cAtCienaSdhLineExtendedKByteStr,
                                                  cAtCienaSdhLineExtendedKByteVal,
                                                  mCount(cAtCienaSdhLineExtendedKByteVal),
                                                  NULL);
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliNumber2String((uint8)AtChannelIdGet((AtChannel)line) + 1, "%d"));

        isEnable = AtCienaSdhLineExtendedKByteIsEnabled(line);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(isEnable), isEnable ? cSevInfo : cSevNormal);

        ColorStrToCell(tabPtr, i, column++, positionStr, cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)lineList);

    return cAtTrue;
    }

eBool CmdAtCienaPwKByteChannelSuppress(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, AtCienaKbyteChannelSuppressionEnable, cAtTrue);
    }

eBool CmdAtCienaPwKByteChannelUnSuppress(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, AtCienaKbyteChannelSuppressionEnable, cAtFalse);
    }

eBool CmdAtCienaKBytePwWatchdog(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, AtCienaKBytePwWatchdogTimerSet, cBit31_0);
    }

eBool CmdAtCienaKBytePwInterval(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, AtCienaKBytePwIntervalSet, cBit31_0);
    }

eBool CmdAtCienaKBytePwTrigger(char argc, char **argv)
    {
    eAtRet ret;
    AtPw pw = KBytePwGet();

    AtUnused(argc);
    AtUnused(argv);
    if (pw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist\r\n");
        return cAtFalse;
        }

    ret = AtCienaKBytePwTrigger(pw);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Can not trigger, ret = %s\r\n",
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtCienaPwKByteChannelOverrideEnable(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, AtCienaKbyteChannelOverrideEnable, cAtTrue);
    }

eBool CmdAtCienaPwKByteChannelOverrideDisable(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, AtCienaKbyteChannelOverrideEnable, cAtFalse);
    }

eBool CmdAtCienaPwKByteChannelK1Set(char argc, char **argv)
    {
    return KByteValueSet(argc, argv, AtCienaKbyteChannelTxK1Set);
    }

eBool CmdAtCienaPwKByteChannelK2Set(char argc, char **argv)
    {
    return KByteValueSet(argc, argv, AtCienaKbyteChannelTxK2Set);
    }

eBool CmdAtCienaPwKByteChannelEK1Set(char argc, char **argv)
    {
    return KByteValueSet(argc, argv, AtCienaKbyteChannelTxEK1Set);
    }

eBool CmdAtCienaPwKByteChannelEK2Set(char argc, char **argv)
    {
    return KByteValueSet(argc, argv, AtCienaKbyteChannelTxEK2Set);
    }

eBool CmdAtCienaPwKbyteCountersShow(char argc, char **argv)
    {
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    AtPw pw = KBytePwGet();

	AtUnused(argc);
	if (pw == NULL)
        return cAtFalse;

    /* Get reading mode */
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode, expect: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc >= 1)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[1]);

    return CliAtPwCounterShow(&pw, 1, readingMode, silent, BuildHeadingForPws);
    }

eBool CmdAtCienaPwKbyteLabelTransmitSet(char argc, char **argv)
    {
    return KByteLabelSet(argc, argv, AtCienaKbyteChannelTxLabelSet);
    }

eBool CmdAtCienaPwKbyteLabelExpectedSet(char argc, char **argv)
    {
    return KByteLabelSet(argc, argv, AtCienaKbyteChannelExpectedLabelSet);
    }

eBool CmdAtCienaSdhLineKByteSourceSet(char argc, char **argv)
    {
    uint32 line_i, numberOfLines;
    AtList lineList;
    eAtCienaSdhLineKByteSource sourceType;
    eBool convertSuccess, success = cAtTrue;
    AtUnused(argc);

    lineList = LineListGet(argv, &numberOfLines);
    if (numberOfLines == 0)
            {
            AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
            AtObjectDelete((AtObject)lineList);
            return cAtFalse;
            }

    sourceType = CliStringToEnum(argv[1], cAtCienaSdhLineKByteSourceStr, cAtCienaSdhLineKByteSourceVal, mCount(cAtCienaSdhLineKByteSourceVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid K-Byte source parameter. Expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtCienaSdhLineKByteSourceStr, mCount(cAtCienaSdhLineKByteSourceVal));
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (line_i = 0; line_i < numberOfLines; line_i = AtCliNextIdWithSleep(line_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine) AtListObjectGet(lineList, line_i);
        eAtRet ret = AtCienaSdhLineKByteSourceSet(line, sourceType);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Cannot configure K Byte source of line %u, ret = %s\r\n",
                 AtChannelIdGet((AtChannel)line) + 1,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)lineList);

    return success;
    }

eBool CmdAtCienaSdhLineKByteShow(char argc, char **argv)
    {
    tTab *tabPtr;
    AtList lineList;
    uint32 numberOfLines, i;
    const char *heading[] = {"LineId", "Source", "TxK1", "TxEK1", "TxK2", "TxEK2"};

    AtUnused(argc);

    lineList = LineListGet(argv, &numberOfLines);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    tabPtr = TableAlloc(numberOfLines, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make content table */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine) AtListObjectGet(lineList, i);
        uint8 lineId = (uint8) AtChannelIdGet((AtChannel) line);
        uint32 colum = 0;
        eAtCienaSdhLineKByteSource source = AtCienaSdhLineKByteSourceGet(line);
        const char* sourceType = CliEnumToString(source, cAtCienaSdhLineKByteSourceStr, cAtCienaSdhLineKByteSourceVal,
                                                 mCount(cAtCienaSdhLineKByteSourceVal), NULL);

        StrToCell(tabPtr, i, colum++, CliNumber2String((uint8 )(lineId + 1), "%u"));
        StrToCell(tabPtr, i, colum++, sourceType);
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtSdhLineTxK1Get(line), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtCienaSdhLineTxEK1Get(line), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtSdhLineTxK2Get(line), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtCienaSdhLineTxEK2Get(line), "0x%02x"));
        }

    AtObjectDelete((AtObject)lineList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaSdhLineTxEK1Set(char argc, char **argv)
    {
    return SdhLineTxEKSet(argc, argv, AtCienaSdhLineTxEK1Set);
    }

eBool CmdAtCienaSdhLineTxEK2Set(char argc, char **argv)
    {
    return SdhLineTxEKSet(argc, argv, AtCienaSdhLineTxEK2Set);
    }

eBool CmdAtCienaKbytePwIntrMsk(char argc, char **argv)
    {
    eAtRet ret;
    uint32 intrMaskVal;
    eBool enable = cAtFalse;
    AtPw kBytePw = KBytePwGet();
    eBool convertSuccess = cAtFalse;

    AtUnused(argc);

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = IntrMaskFromString(argv[0])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    ret = AtChannelInterruptMaskSet((AtChannel)kBytePw, intrMaskVal, enable ? intrMaskVal : 0x0);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Cannot configure interrupt mask for Channel %s, ret = %s\r\n",
             AtChannelIdString((AtChannel)kBytePw),
             AtRet2String(ret));

    return cAtFalse;
    }

eBool CmdAtCienaKbytePwInterruptsShow(char argc, char **argv)
    {
    uint32 (*AlarmInterruptGet)(AtChannel self) = AtChannelAlarmInterruptGet;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;

    AtUnused(argc);
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    /* Silent */
    if (argc >= 1)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[1]);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        AlarmInterruptGet = AtChannelAlarmInterruptClear;

    return StatusShowWithSilent(silent, AlarmInterruptGet);
    }

eBool CmdAtCienaKbytePwAlarmShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return StatusShowWithSilent(cAtFalse, AtChannelAlarmGet);
    }

eBool CmdAtCienaKbytePwAlarmCapture(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    static tAtChannelEventListener intrListener;
    eBool captureEnabled = cAtTrue;
    eBool convertSuccess;
    eBool success = cAtTrue;
    AtPw kBytePw = KBytePwGet();
    AtUnused(argc);

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    intrListener.AlarmChangeState = AlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[0], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    if (captureEnabled)
        ret = AtChannelEventListenerAdd((AtChannel)kBytePw, &intrListener);
    else
        ret = AtChannelEventListenerRemove((AtChannel)kBytePw, &intrListener);

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot %s alarm listener for Kbyte PW, ret = %s\r\n",
                 captureEnabled ? "register" : "deregister",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtCienaKbytePwDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtChannelDebug((AtChannel)KBytePwGet());
    return cAtTrue;
    }

eBool CmdAtCienaPwKByteChannelsAlarmShow(char argc, char **argv)
    {
    AtUnused(argc);
    return ShowChannelsAlarmWithSilent(argv[0], AtChannelDefectGet, cAtFalse);
    }

eBool CmdAtCienaPwKByteChannelsInterruptShow(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    eBool silent = cAtFalse;

    AtUnused(argc);

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    if (readingMode == cAtHistoryReadingModeReadOnly)
        return ShowChannelsAlarmWithSilent(argv[0], AtChannelDefectInterruptGet, silent);

    return ShowChannelsAlarmWithSilent(argv[0], AtChannelDefectInterruptClear, silent);
    }

eBool CmdAtCienaPwKByteChannelsInterruptMaskSet(char argc, char **argv)
    {
    uint32 numChannels = 0, channel_i;
    AtChannel *channels = ChannelListFromString(argv[0], &numChannels);
    AtPw pw = KBytePwGet();
    uint32 mask;
    eBool enabled;
    eBool success = cAtTrue;

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    if (pw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist \r\n");
        return cAtFalse;
        }

    mask = CliMaskFromString(argv[1],
                             cAtCienaKbyteApsChannelAlarmTypeStr,
                             cAtCienaKbyteApsChannelAlarmTypeVal,
                             mCount(cAtCienaKbyteApsChannelAlarmTypeVal));

    /* Enabling */
    enabled = CliBoolFromString(argv[2]);

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret;

        ret = AtChannelInterruptMaskSet(channels[channel_i], mask, enabled ? mask : 0);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: setting interrupt mask on channel %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(channels[channel_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtCienaPwKByteChannelAlarmCapture(char argc, char **argv)
    {
    static tAtChannelEventListener intrListener;
    eBool captureEnabled = cAtTrue;
    eBool convertSuccess;
    eBool success = cAtTrue;
    uint32 numChannels = 0, channel_i;
    AtChannel *channels = ChannelListFromString(argv[0], &numChannels);
    AtPw kBytePw = KBytePwGet();
    AtUnused(argc);

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    intrListener.AlarmChangeState = KbyteChannelAlarmChangeState;

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channels, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    if (kBytePw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: KBytePw does not exist \r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret;
        AtChannel channel = channels[channel_i];

        if (captureEnabled)
            ret = AtChannelEventListenerAdd(channel, &intrListener);
        else
            ret = AtChannelEventListenerRemove(channel, &intrListener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for channel %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }


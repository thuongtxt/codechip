/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaDebug.c
 *
 * Created Date: Oct 13, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "AtCli.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/default/eth/ThaModuleEth.h"
#include "../../../../driver/src/implement/codechip/Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "../../../../driver/src/implement/codechip/Tha60290021/xc/Tha60290021ModuleXc.h"
#include "../../../../driver/src/implement/codechip/Tha60290021/pw/Tha60290021ModulePw.h"
#include "../../../../driver/src/implement/codechip/Tha60290021/eth/Tha60290021ModuleEth.h"
#include "../../../../driver/src/implement/codechip/Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "../../../../driver/src/implement/codechip/Tha60290011/eth/Tha60290011ModuleEth.h"
#include "../../../../driver/src/implement/codechip/Tha60290011/man/Tha60290011DeviceInternal.h"
#include "../../../../driver/src/implement/codechip/Tha60290051/eth/Tha60290051ModuleEth.h"
#include "CliAtCiena.h"
#include "../../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cAxi4PortId     (2)
#define cDimAxi4PortId  (5)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*ModuleEthAttributeSetFunc)(ThaModuleEth, uint32);

/*--------------------------- Global variables -------------------------------*/
static uint32 cTha6029LineSideVal[] = {cTha6029LineSideFaceplate,
                                       cTha6029LineSideMate};

static const char *cTha6029LineSideStr[] = {
                                            "faceplate",
                                            "mate"};

static uint32 cTha60290021XcHideModeVal[] = {cTha60290021XcHideModeNone,     /* Do not hide anything, this must be default mode */
                                             cTha60290021XcHideModeDirect,   /* Hide XC but the connection between MATE/Faceplate and Terminated lines are 1:1 */
                                             cTha60290021XcHideModeMate,     /* Hide XC at MATE, all faceplate ports are looped */
                                             cTha60290021XcHideModeFaceplate /* Hide XC at Faceplate, all MATE ports are looped */};

static const char *cTha60290021XcHideModeStr[] = {"none", "direct", "mate", "faceplate"};

static uint32 cTha60290021EthPortCounterTickModeVal[] = {cTha602900xxEthPortCounterTickModeDisable,
                                                         cTha602900xxEthPortCounterTickModeAuto,
                                                         cTha602900xxEthPortCounterTickModeManual};
static const char *cTha60290021EthPortCounterTickModeStr[] = {"disable",
                                                              "auto",
                                                              "manual"};

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(void)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(CliDevice(), cThaModuleOcn);
    }

static AtModuleXc XcModule(void)
    {
    return (AtModuleXc)AtDeviceModuleGet(CliDevice(), cAtModuleXc);
    }

static AtModuleEth EthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static ThaEthPort PortGet(uint32 portId)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    return (ThaEthPort)AtModuleEthPortGet(ethModule, (uint8)portId);
    }

static eBool Axi4EthPortEthTypeSet(ThaEthPort port, char **argv)
    {
    eBool success = cAtTrue;
    eAtRet ret = cAtError;
    uint16 type = (uint16)AtStrToDw(argv[0]);
    ret = Tha60290011ClsInterfaceEthPortEthTypeSet(port, type);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot Set Eth Type on port AXI, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }
    
static eBool CliMacAddressSet(char **argv, eAtModuleEthRet (*MacSetFunc)(ThaEthPort port, uint8 * macAddr))
    {
    ThaEthPort port;
    eBool success = cAtTrue;
    uint8 mac[cAtMacAddressLen];
    eAtRet ret = cAtError;

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    if (!CliAtModuleEthMacAddrStr2Byte(argv[0], mac))
        {
        AtPrintc(cSevCritical, "Invalid Destination MAC\r\n");
        return cAtFalse;
        }

    port = PortGet(CliId2DriverId(cAxi4PortId));
    ret = MacSetFunc(port, mac);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot Set MAC on port AXI, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool CliClsCheckEnable(char **argv, eAtModuleEthRet (*ClsAttributeCheckEnable)(ThaEthPort port, eBool enable))
    {
    ThaEthPort port;
    eBool convertSuccess = cAtFalse, enable = cAtFalse;
    eBool success = cAtTrue;
    eAtRet ret = cAtError;

    /* Get enable mode */
    mAtStrToBool(argv[0], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter. Expected: en/dis\r\n");
        return cAtFalse;
        }

    port = PortGet(CliId2DriverId(cAxi4PortId));
    ret = ClsAttributeCheckEnable(port, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot enable checking on port AXI, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool CliClsAttributeUint8Set(char **argv, eAtModuleEthRet (*ClsAttribute)(ThaEthPort port, uint8 attribute))
    {
    eBool success = cAtTrue;
    eAtRet ret = cAtError;
    ThaEthPort port = PortGet(CliId2DriverId(cAxi4PortId));
    uint8 attributeVal = (uint8)AtStrToDw(argv[0]);
    ret = ClsAttribute(port, attributeVal);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot Set on port AXI, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool CliClsAttributeUint16Set(char **argv, eAtModuleEthRet (*ClsAttribute)(ThaEthPort port, uint16 attribute))
    {
    eBool success = cAtTrue;
    eAtRet ret = cAtError;
    ThaEthPort port = PortGet(CliId2DriverId(cAxi4PortId));
    uint16 attributeVal = (uint16)AtStrToDw(argv[0]);
    ret = ClsAttribute(port, attributeVal);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot Set on port AXI, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eTha602900xxEthPortCounterTickMode TickModeFromString(const char *string)
    {
    eTha602900xxEthPortCounterTickMode mode;
    eBool convertSuccess = cAtFalse;

    mode = CliStringToEnum(string,
                           cTha60290021EthPortCounterTickModeStr,
                           cTha60290021EthPortCounterTickModeVal,
                           mCount(cTha60290021EthPortCounterTickModeVal),
                           &convertSuccess);
    if (convertSuccess)
        return mode;

    return cTha602900xxEthPortCounterTickModeUnknown;
    }

static eBool ModuleEthAttributeSet(char **argv, ModuleEthAttributeSetFunc func, uint32 maxVal)
    {
    eAtRet ret;
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    uint32 value = AtStrtoul(argv[0], NULL, 10);

    if (value > maxVal)
        {
        AtPrintc(cSevCritical, "ERROR: Input %s is out-of-range, max = %d\r\n", argv[0], maxVal);
        return cAtFalse;
        }

    ret = func(ethModule, value);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Setting module attribute with value %d fail with ret = %s\r\n",
             value,
             AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdTha60290021ModuleOcnPohProcessorSideSet(char argc, char **argv)
    {
    eBool success = cAtFalse;
    eTha6029LineSide side = CliStringToEnum(argv[0], cTha6029LineSideStr, cTha6029LineSideVal, mCount(cTha6029LineSideVal), &success);
    eAtRet ret;

    AtUnused(argc);

    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct side. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cTha6029LineSideStr, mCount(cTha6029LineSideVal));
        return cAtFalse;
        }

    ret = Tha60290021ModuleOcnPohProcessorSideSet(OcnModule(), side);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Change POH side fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdTha60290021ModuleXcHideModeSet(char argc, char **argv)
    {
    eAtRet ret;
    eBool success = cAtFalse;
    eTha60290021XcHideMode hideMode = CliStringToEnum(argv[0], cTha60290021XcHideModeStr, cTha60290021XcHideModeVal, mCount(cTha60290021XcHideModeVal), &success);

    AtUnused(argc);

    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct hiding mode. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cTha60290021XcHideModeStr, mCount(cTha60290021XcHideModeVal));
        AtPrintc(cSevNormal, "\r\n");
        return cAtFalse;
        }

    ret = Tha60290021ModuleXcHideModeSet(XcModule(), hideMode);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: hiding XC fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdTha60290021ModuleXcShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"AttributeName", "Value"};
    eTha60290021XcHideMode hideMode;
    static const uint32 cNumRows = 1;
    uint32 row = 0;
    const char *stringValue;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(cNumRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    hideMode = Tha60290021ModuleXcHideModeGet(XcModule());
    stringValue = CliEnumToString(hideMode, cTha60290021XcHideModeStr, cTha60290021XcHideModeVal, mCount(cTha60290021XcHideModeVal), NULL);
    StrToCell(tabPtr, row, 0, "XcHiding");
    StrToCell(tabPtr, row, 1, stringValue ? stringValue : "Unknown");
    row++;

    AtAssert(row == cNumRows); /* This really helps when new rows are added */

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaPdhInterfaceAxi4LoopbackDebug(char argc, char **argv)
    {
    eAtLoopbackMode loopMode = CliLoopbackModeFromString(argv[0]);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtRet ret = Tha60290011ModulePdhInterfaceAxi4Loopback(pdhModule, loopMode);

    AtUnused(argc);

    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical,
             "ERROR: Cannot loopback on port AXI, ret = %s\r\n",
             AtRet2String(ret));

    return cAtFalse;
    }

eBool CmdAtCienaPdhInterfaceAxi4EthTypeDebug(char argc, char **argv)
    {
    ThaEthPort port = PortGet(CliId2DriverId(cAxi4PortId));
    AtUnused(argc);

    return Axi4EthPortEthTypeSet(port, argv);
    }

eBool CmdAtCienaPdhDimAxi4EthTypeDebug(char argc, char **argv)
    {
    ThaEthPort port = PortGet(CliId2DriverId(cDimAxi4PortId));
    AtUnused(argc);

    return Axi4EthPortEthTypeSet(port, argv);
    }

eBool CmdAtCienaEthPortClsSourceMacDebug(char argc, char **argv)
    {
    AtUnused(argc);
    return CliMacAddressSet(argv, Tha60290011ClsInterfaceEthPortSourceMacAddressSet);
    }

eBool CmdAtCienaEthPortClsDestMacDebug(char argc, char **argv)
    {
    AtUnused(argc);
    return CliMacAddressSet(argv, Tha60290011ClsInterfaceEthPortDestMacAddressSet);
    }

eBool CmdAtCienaSubportVlanEnableDebug(char argc, char **argv)
    {
    eAtRet ret;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    AtUnused(argc);

    mAtStrToBool(argv[0], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter. Expected: en/dis\r\n");
        return cAtFalse;
        }

    ret = ThaModulePwSubPortVlanInsertionEnable((ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw), enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot Enable/disable subport vlan insertion, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }
    ret = ThaModulePwSubPortVlanCheckingEnable((ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw), enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
             "ERROR: Cannot Enable/disable subport vlan checking, ret = %s\r\n",
             AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtCienaEthPortClsSequenceCheckEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsCheckEnable(argv, Tha60290011ClsInterfaceEthPortSequenceCheckEnable);
    }

eBool CmdAtCienaEthPortClsLengthCheckEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsCheckEnable(argv, Tha60290011ClsInterfaceEthPortLengthCheckEnable);
    }

eBool CmdAtCienaEthPortClsEthTypeCheckEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsCheckEnable(argv, Tha60290011ClsInterfaceEthPortEthTypeCheckEnable);
    }

eBool CmdAtCienaEthPortClsSourceMacCheckEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsCheckEnable(argv, Tha60290011ClsInterfaceEthPortSourceMacCheckEnable);
    }

eBool CmdAtCienaEthPortClsDestMacCheckEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsCheckEnable(argv, Tha60290011ClsInterfaceEthPortDestMacCheckEnable);
    }

eBool CmdAtCienaEthPortClsSubTypeMaskSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsAttributeUint8Set(argv, Tha60290011ClsInterfaceEthPortSubTypeMaskCheckSet);
    }

eBool CmdAtCienaEthPortClsSubTypeSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsAttributeUint8Set(argv, Tha60290011ClsInterfaceEthPortEthSubTypeSet);
    }

eBool CmdAtCienaEthPortClsEthLengthSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsAttributeUint16Set(argv, Tha60290011ClsInterfaceEthPortEthLengthSet);
    }

eBool CmdAtCienaEthPortClsEthTypeSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliClsAttributeUint16Set(argv, Tha60290011ClsInterfaceEthPortEthTypeSet);
    }

eBool CmdTha6029002EthPortCounterTickModeSet(char argc, char** argv)
    {
    uint32 numChannels, channel_i;
    AtChannel *channels = CliSharedChannelListGet(&numChannels);
    eTha602900xxEthPortCounterTickMode mode;
    eBool success = cAtTrue;

    AtUnused(argc);

    numChannels = CliEthPortListFromString(argv[0], channels, numChannels);

    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no ports to configure, please check again the input ID\r\n");
        return cAtFalse;
        }

    mode = TickModeFromString(argv[1]);
    if (mode == cTha602900xxEthPortCounterTickModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid mode, expected: ");
        CliExpectedValuesPrint(cSevCritical, cTha60290021EthPortCounterTickModeStr, mCount(cTha60290021EthPortCounterTickModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtEthPort port = (AtEthPort)channels[channel_i];
        eAtRet ret = Tha602900xxEthPortCounterTickModeSet(port, mode);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Changing counter tick mode on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(channels[channel_i]),
                 AtRet2String(ret));

        success = cAtFalse;
        }

    return success;
    }

eBool CmdTha6029PwKbytePwTxEthTypeSet(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, (KBytePwAttributeSetFunc)Tha6029PwKbytePwTxEthTypeSet, cBit15_0);
    }

eBool CmdTha6029PwKbytePwExpectedEthTypeSet(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, (KBytePwAttributeSetFunc)Tha6029PwKbytePwExpectedEthTypeSet, cBit15_0);
    }

eBool CmdTha6029PwKbyteTxTypeSet(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, (KBytePwAttributeSetFunc)Tha6029PwKbyteTxTypeSet, cBit7_0);
    }

eBool CmdTha6029PwKbyteExpectedTypeSet(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, (KBytePwAttributeSetFunc)Tha6029PwKbyteExpectedTypeSet, cBit7_0);
    }

eBool CmdTha6029PwKbyteTxVersionSet(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, (KBytePwAttributeSetFunc)Tha6029PwKbyteTxVersionSet, cBit7_0);
    }

eBool CmdTha6029PwKbyteExpectedVersionSet(char argc, char **argv)
    {
    return CliAtCienaKBytePwAttributeSetWithMaxValue(argc, argv, (KBytePwAttributeSetFunc)Tha6029PwKbyteExpectedVersionSet, cBit7_0);
    }

eBool CmdThaModuleEthSgmiiEthDccOcn2EthMaxLenSet(char argc, char **argv)
    {
    AtUnused(argc);
    return ModuleEthAttributeSet(argv, (ModuleEthAttributeSetFunc)ThaModuleEthTxDccPacketMaxLengthSet, cBit11_0);
    }

eBool CmdThaModuleEthSgmiiEthDccEth2OcnMaxLenSet(char argc, char **argv)
    {
    AtUnused(argc);
    return ModuleEthAttributeSet(argv, (ModuleEthAttributeSetFunc)ThaModuleEthRxDccPacketMaxLengthSet, cBit11_0);
    }

eBool CmdThaModuleEthSgmiiEthDccMaxLengthShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"DCC ocn2eth max length", "DCC eth2ocn max length"};
    static const uint32 cNumRows = 1;
    char stringValue[128];
    uint32 txMax, rxMax;
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(cNumRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    txMax = ThaModuleEthTxDccPacketMaxLengthGet(ethModule);
    rxMax = ThaModuleEthRxDccPacketMaxLengthGet(ethModule);
    AtSprintf(stringValue, "%d", txMax);
    StrToCell(tabPtr, 0, 0, stringValue);
    AtSprintf(stringValue, "%d", rxMax);
    StrToCell(tabPtr, 0, 1, stringValue);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdTha602900051EthCounterTickModeSet(char argc, char** argv)
    {
    eTha602900xxEthPortCounterTickMode mode;
    eBool success = cAtTrue;
    eAtRet ret;

    AtUnused(argc);

    mode = TickModeFromString(argv[0]);
    if (mode == cTha602900xxEthPortCounterTickModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid mode, expected: ");
        CliExpectedValuesPrint(cSevCritical, cTha60290021EthPortCounterTickModeStr, mCount(cTha60290021EthPortCounterTickModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = Tha60290051ModuleEthCounterTickModeSet(EthModule(), mode);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Changing counter tick mode failed with ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdTha602900051EthCountersLatchAndClear(char argc, char** argv)
    {
    eAtRet ret;
    AtUnused(argc);
    AtUnused(argv);

    ret = Tha60290051ModuleEthAllCountersLatchAndClear(EthModule());
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Latch ETH counters failed with ret = %s\r\n",
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

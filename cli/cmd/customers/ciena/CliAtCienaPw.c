/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaPw.c
 *
 * Created Date: Sep 16, 2016
 *
 * Description : PW customer specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../pw/CliAtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumPwsPerTable 10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePw PwModule(void)
    {
    return (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    }

static eBool ModulePwSubPortVlanSet(char argc, char **argv, eAtRet (*SubportVlanSet)(AtModulePw, uint32, const tAtVlan *))
    {
    tAtVlan vlan, *pVlan = NULL;
    AtModulePw pwModule = PwModule();
    uint32 numIndexes = 0, vlan_i;
    uint32 *vlanIndexes = CliSharedIdBufferGet(&numIndexes);
    eBool success = cAtTrue;

    AtUnused(argc);

    numIndexes = CliIdListFromString(argv[0], vlanIndexes, numIndexes);
    if (numIndexes == 0)
        {
        AtPrintc(cSevWarning, "No VLAN indexes to configure\r\n");
        return cAtTrue;
        }

    if (AtStrcmp(argv[1], "none") != 0)
        {
        pVlan = CliVlanFromString(argv[1], &vlan);
        if (pVlan == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid VLAN, expected %s\r\n", CliFullVlanExpectedFormat());
            return cAtFalse;
            }
        }

    for (vlan_i = 0; vlan_i < numIndexes; vlan_i++)
        {
        uint32 vlanIndex = vlanIndexes[vlan_i] - 1;
        eAtRet ret = SubportVlanSet(pwModule, vlanIndex, pVlan);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Configure VLAN at index %u fail with ret = %s\r\n",
                 vlanIndexes[vlan_i],
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static void PutVlan(tTab *tabPtr, uint32 vlanIndex, uint32 column, eAtRet (*VlanGet)(AtModulePw, uint32, tAtVlan *))
    {
    tAtVlan vlan;

    eAtRet ret = VlanGet(PwModule(), vlanIndex, &vlan);
    if (ret == cAtOk)
        StrToCell(tabPtr, vlanIndex, column, CliVlan2Str(&vlan));
    else
        ColorStrToCell(tabPtr, vlanIndex, column, AtRet2String(ret), cSevCritical);
    }

static char** BuildHeadingForPws(AtPw *pwList, uint32 currentPwIndex, uint32 numPwsPerTable)
    {
    AtPw   pw;
    uint32 i;
    char   **headings;

    headings = AtOsalMemAlloc((numPwsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(8);
    AtSprintf(headings[0], "PW");

    for (i = 0; i < numPwsPerTable; i++)
        {
        pw = pwList[currentPwIndex + i];
        headings[i + 1] = AtOsalMemAlloc(8);
        AtSprintf(headings[i + 1], "%u", AtChannelIdGet((AtChannel)pw) + 1);
        }

    return headings;
    }

static eBool CliAtCienaPwCounterShow(AtPw *pwList, uint32 numPws, eAtHistoryReadingMode readingMode, eBool silent, HeadingPwsBuild pwHeadingBuild)
    {
    const char *counterNames[] = {/* Common counters */
                                  "TxPackets", "TxPayloadBytes", "RxPackets",
                                  "RxPayloadBytes", "RxDiscardedPackets", "RxMalformedPackets",
                                  "RxReorderedPackets", "RxLostPackets", "RxOutOfSeqDropPackets",
                                  "RxStrayPackets", "RxDuplicatedPackets",

                                  /* TDM counters */
                                  "TxLbitPackets", "TxRbitPackets", "RxLbitPackets",
                                  "RxRbitPackets", "RxJitBufOverrunEvents", "RxJitBufUnderrunEvents",
                                  "RxLops",

                                  /* CESoP counters */
                                  "TxMbitPackets", "RxMbitPackets",

                                  /* CEP counters */
                                  "TxNbitPackets", "TxPbitPackets",
                                  "RxNbitPackets", "RxPbitPackets",

                                  /* Other */
                                  "NumPktsInJitterBuffer"};
    uint32 numTables, table_i;
    eAtRet (*AllCountersGet)(AtChannel self, void *counters) = NULL;
    uint32  remainPws = numPws;
    eAtRet ret;
    eBool countersValid = cAtTrue;

    /* Calculate number of tables need to show counters of all input PWs */
    numTables = numPws / cNumPwsPerTable;
    if ((numPws % cNumPwsPerTable) > 0)
        numTables = numTables + 1;

    /* Get corresponding function to read counter */
    if (readingMode == cAtHistoryReadingModeReadOnly)
        AllCountersGet = AtCienaPwAllCountersGet;
    else
        AllCountersGet = AtCienaPwAllCountersClear;

    for (table_i = 0; table_i < numTables; table_i = AtCliNextIdWithSleep(table_i, 1))
        {
        tTab *tabPtr = NULL;
        uint32 numPwsPerTable;
        uint16 pw_i;
        char **heading = NULL;
        uint8 counter_i = 0;
        uint32 currentPwIndex = table_i * cNumPwsPerTable;
        static char buf[50];

        numPwsPerTable = remainPws;
        if (numPwsPerTable > cNumPwsPerTable)
            numPwsPerTable = cNumPwsPerTable;

        /* Create table */
        if (!silent)
            {
            heading = pwHeadingBuild(pwList, currentPwIndex, numPwsPerTable);
            tabPtr = TableAlloc(mCount(counterNames), numPwsPerTable + 1, (void *)heading);
            if (!tabPtr)
                {
                AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
                return cAtFalse;
                }
            }

        /* Build column of counter names */
        for (counter_i = 0; counter_i < mCount(counterNames); counter_i++)
            StrToCell(tabPtr, counter_i, 0, counterNames[counter_i]);

        /* Put counters of current PWs */
        for (pw_i = 0; pw_i < numPwsPerTable; pw_i++)
            {
            AtPw pw;
            tAtCienaPwCounters counters;
            AtOsalMemInit(&counters, 0, sizeof(tAtCienaPwCounters));

            /* Get PW */
            pw = pwList[currentPwIndex + pw_i];

            /* Read counters */
            ret = AllCountersGet((AtChannel)pw, &counters);
            countersValid = (ret == cAtOk) ? cAtTrue : cAtFalse;
            counter_i = 0;

            /* Common counters */
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txPackets, cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txPayloadBytes, cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxPackets, cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxPayloadBytes, cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxDiscardedPackets, cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxMalformedPackets,    cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxReorderedPackets,    cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxLostPackets,         cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxOutOfSeqDropedPackets, cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxStrayPackets,        cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxDuplicatedPackets, cAtCounterTypeError, countersValid);

            /* TDM counters */
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txLbitPackets,      cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txRbitPackets,      cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxLbitPackets,      cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxRbitPackets,      cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxJitBufOverrun,    cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxJitBufUnderrun,   cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxLops,             cAtCounterTypeError, countersValid);

            /* CESoP counters */
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txMbitPackets, cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxMbitPackets, cAtCounterTypeError, countersValid);

            /* CEP counters */
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txNbitPackets, cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.txPbitPackets, cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxNbitPackets, cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, counters.rxPbitPackets, cAtCounterTypeNeutral, countersValid);

            /* Other counters */
            if (counters.numAdditionBytesInJitterBuffer != 0)
                AtSprintf(buf, "%u + %u (bytes)", counters.numPacketsInJitterBuffer, counters.numAdditionBytesInJitterBuffer);
            AtSprintf(buf, "%d", counters.numPacketsInJitterBuffer);
            ColorStrToCell(tabPtr, counter_i++, pw_i + 1UL, buf, cSevInfo);
            }

        /* Show it */
        TablePrint(tabPtr);
        TableFree(tabPtr);
        if (heading != NULL)
            {
            for (pw_i = 0; pw_i < numPwsPerTable + 1; pw_i++)
                AtOsalMemFree(heading[pw_i]);
            AtOsalMemFree(heading);
            }

        /* For next PWs */
        remainPws = remainPws - numPwsPerTable;
        }

    return cAtTrue;
    }

eBool CmdAtCienaPwGet(char argc, char **argv)
    {
    AtList        pwList;
    uint16        pw_i;
    uint32        numPws;
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "TxActive", "SubportVlanIndex", "SubportVlan"};
    AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);
    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        eBool forced;
        uint8 column = 0;
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        uint32 subPortVlanIndex;
        tAtVlan vlan;
        eAtRet ret = cAtOk;

        /* PW ID */
        StrToCell(tabPtr, pw_i, column++, CliChannelIdStringGet((AtChannel)pw));

        /* TxActiveIsForced */
        forced = AtCienaPwTxActiveIsForced(pw);
        ColorStrToCell(tabPtr, pw_i, column++, CliBoolToString(forced), forced ? cSevInfo : cSevCritical);

        /* Sub port VLAN index */
        subPortVlanIndex = AtCienaPwSubPortVlanIndexGet(pw);
        ColorStrToCell(tabPtr, pw_i, column++, CliNumber2String(subPortVlanIndex + 1, "%u"), cSevNormal);

        /* Sub port VLAN value */
        ret = AtCienaModulePwTxSubportVlanGet(PwModule(), subPortVlanIndex, &vlan);
        if (ret == cAtOk)
            StrToCell(tabPtr, pw_i, column++, CliVlan2Str(&vlan));
        else
            ColorStrToCell(tabPtr, pw_i, column++, AtRet2String(ret), cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdAtCienaPwActiveForce(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtCienaPwTxActiveForce);
    }

eBool CmdAtCienaPwActiveUnforce(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtCienaPwTxActiveForce);
    }

eBool CmdAtCienaPwModuleTxSubPortVlanSet(char argc, char **argv)
    {
    return ModulePwSubPortVlanSet(argc, argv, AtCienaModulePwTxSubportVlanSet);
    }

eBool CmdAtCienaPwModuleExpectedSubPortVlanSet(char argc, char **argv)
    {
    return ModulePwSubPortVlanSet(argc, argv, AtCienaModulePwExpectedSubportVlanSet);
    }

eBool CmdAtCienaPwTxSubportVlan(char argc, char **argv)
    {
    eBool         success = cAtTrue;
    uint32        subPortVlanIndex;
    AtList        pwList;
    uint16        pw_i;
    uint32        numPws;
    AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);
    subPortVlanIndex = AtStrToDw(argv[1]) - 1;

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        eAtRet ret = AtCienaPwSubPortVlanIndexSet(pw, subPortVlanIndex);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Specify sub port VLAN index %u on %s fail with ret = %s\r\n",
                 subPortVlanIndex + 1,
                 CliChannelIdStringGet((AtChannel)pw),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)pwList);

    return success;
    }

eBool CmdAtCienaModulePwSubportVlanGet(char argc, char **argv)
    {
    AtModulePw pwModule = PwModule();
    tTab *tabPtr;
    const char *heading[] = {"Index", "Transmit", "Expect"};
    uint32 vlan_i, numVlans = AtCienaModulePwNumSubPortVlans(pwModule);

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(numVlans, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (vlan_i = 0; vlan_i < numVlans; vlan_i++)
        {
        uint8 column = 0;

        StrToCell(tabPtr, vlan_i, column++, CliNumber2String(vlan_i + 1, "%u"));
        PutVlan(tabPtr, vlan_i, column++, AtCienaModulePwTxSubportVlanGet);
        PutVlan(tabPtr, vlan_i, column++, AtCienaModulePwExpectedSubportVlanGet);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaPwCounterGet(char argc, char **argv)
    {
    eBool silent = cAtFalse;
    AtPw *pwList;
    eAtHistoryReadingMode readingMode;
    uint32 numPws;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode, expect: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    return CliAtCienaPwCounterShow(pwList, numPws, readingMode, silent, BuildHeadingForPws);
    }

eBool CmdAtCienaPwModulePayloadSizeByteEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (AtModulePwCesopPayloadSizeInByteEnable(PwModule(), cAtTrue) == cAtOk)
        return cAtTrue;

    return cAtFalse;
    }

eBool CmdAtCienaPwModulePayloadSizeByteDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (AtModulePwCesopPayloadSizeInByteEnable(PwModule(), cAtFalse) == cAtOk)
        return cAtTrue;

    return cAtFalse;
    }

eBool CmdAtCienaModulePwPayloadSizeByteModeGet(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"PayloadSizeInByteEnabled"};
    eBool enabled = cAtFalse;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    enabled = AtModulePwCesopPayloadSizeInByteIsEnabled(PwModule());
    StrToCell(tabPtr, 0, 0, enabled ? "enable" : "disable");

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

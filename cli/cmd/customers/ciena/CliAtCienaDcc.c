/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : CliAtCienaDcc.c
 *
 * Created Date: Sep 16, 2016
 *
 * Description : DCC specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCiena.h"
#include "../../pw/CliAtModulePw.h"
#include "../../eth/CliAtModuleEth.h"
#include "../../sdh/CliAtSdhLineDcc.h"
#include "../../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
static const char *cAtCienaDccPwAlarmTypeStr[] =
    {
    "rx-pkt-discard"
    };

static const uint32 cAtCienaDccPwAlarmTypeVal[] =
    {
  	cAtCienaDccPwAlarmTypeRxPktDiscards
    };

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeUint8SetFunc)(AtPw, uint8);
typedef eAtRet (*AtDccPwEnableFunc)(AtPw, eBool);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char **AlarmTypeStr(uint8 *numAlarms)
    {
    if (numAlarms)
        *numAlarms = mCount(cAtCienaDccPwAlarmTypeStr);
    return cAtCienaDccPwAlarmTypeStr;
    }

static const uint32 *AlarmTypeVal(uint8 *numAlarms)
    {
    if (numAlarms)
        *numAlarms = mCount(cAtCienaDccPwAlarmTypeVal);
    return cAtCienaDccPwAlarmTypeVal;
    }

static uint32 IntrMaskFromString(char *pIntrStr)
    {
    uint8 numAlarms = 0;
    const char **alarmTypeStrings = AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypes = AlarmTypeVal(&numAlarms);
    return CliMaskFromString(pIntrStr, alarmTypeStrings, alarmTypes, numAlarms);
    }

static const char *Alarm2String(uint32 alarms)
    {
    uint16 alarm_i;
    uint32 bufferSize;
    char *alarmString = CliSharedCharBufferGet(&bufferSize);
    uint8 numAlarms;
    const char **alarmTypeStr = AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = AlarmTypeVal(&numAlarms);

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        {
        AtSprintf(alarmString, "none");
        return alarmString;
        }

    /* There are alarms */
    for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
        {
        if (alarms & (alarmTypeVal[alarm_i]))
            AtSprintf(alarmString, "%s%s|", alarmString, alarmTypeStr[alarm_i]);
        }

    if (AtStrlen(alarmString) == 0)
        return "None";

    /* Remove the last '|' */
    alarmString[AtStrlen(alarmString) - 1] = '\0';

    return alarmString;
    }

static eBool AttributeUint8Set(char **argv, AttributeUint8SetFunc func)
    {
    uint32 i;
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);
    uint32 numPws = AtListLengthGet(pwList);
    uint8 value = (uint8)AtStrtoul(argv[1], NULL, 16);
    eBool success = cAtTrue;
    if (numPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        eAtRet ret = func(pw, value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Configure the %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)pw),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject) pwList);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable, AtDccPwEnableFunc func)
    {
    uint32 pw_i, numPws;
    eBool success = cAtTrue;
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);

    AtUnused(argc);

    numPws = AtListLengthGet(pwList);
    if (numPws == 0)
        {
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        eAtRet ret = func(pw, enable);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s %s fail with ret = %s\r\n",
                 enable ? "Enable" : "Disable",
                 CliChannelIdStringGet((AtChannel)pw),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject) pwList);
    return success;
    }

static char** BuildHeadingForPws(AtPw *pwList, uint32 currentPwIndex, uint32 numPwsPerTable)
    {
    AtChannel hdlc;
    uint32 pw_i;
    char **headings;

    headings = AtOsalMemAlloc((numPwsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(32);
    AtSprintf(headings[0], "PW");

    for (pw_i = 0; pw_i < numPwsPerTable; pw_i++)
        {
        AtPw pw = pwList[currentPwIndex + pw_i];
        headings[pw_i + 1] = AtOsalMemAlloc(32);
        hdlc = AtPwBoundCircuitGet(pw);
        AtSprintf(headings[pw_i + 1], "%s", AtChannelIdString((AtChannel) hdlc));
        }

    return headings;
    }

static eBool StatusShowWithSilent(char *idString, uint32 (*AlarmGetHandler)(AtChannel self), eBool silent)
    {
    uint32 pw_i;
    tTab *tabPtr = NULL;
    AtList pwList = CliPwDccHdlcListFromString(idString);
    uint32 numPws = AtListLengthGet(pwList);
    const char *heading[] =  {"PW", "discard"};

    /* Get PW List */
    if (numPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    if (!silent)
        {
        tabPtr = TableAlloc(numPws, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            AtObjectDelete((AtObject) pwList);
            return cAtFalse;
            }
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        uint32 alarmStat = 0;
        uint32 colum = 0;
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        AtChannel hdlc = AtPwBoundCircuitGet(pw);

        StrToCell(tabPtr, pw_i, colum++, AtChannelIdString(hdlc));

        /* Alarm status */
        alarmStat = AlarmGetHandler((AtChannel) pw);
        if (alarmStat & cAtCienaDccPwAlarmTypeRxPktDiscards)
            ColorStrToCell(tabPtr, pw_i, colum++, "set", cSevCritical);
        else
            ColorStrToCell(tabPtr, pw_i, colum++, "clear", cSevInfo);
        }

    AtObjectDelete((AtObject)pwList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaPwDccEthTypeSet(char argc, char **argv)
    {
    uint32 pw_i;
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);
    uint32 numberPws = AtListLengthGet(pwList);
    uint16 value = (uint16)AtStrtoul(argv[1], NULL, 16);
    eBool success = cAtTrue;

    AtUnused(argc);
    if (numberPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numberPws; pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        eAtRet ret = AtCienaDccPwEthTypeSet(pw, value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Configuring EthType of %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)pw),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject) pwList);
    return success;
    }

eBool CmdAtCienaPwDccTypeSet(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeUint8Set(argv, AtCienaDccPwTypeSet);
    }

eBool CmdAtCienaPwDccVersionSet(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeUint8Set(argv, AtCienaDccPwVersionSet);
    }

eBool CmdAtCienaPwHdlcShow(char argc, char **argv)
    {
    uint32 pw_i;
    tTab *tabPtr;
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);
    uint32 numPws = AtListLengthGet(pwList);
    const char *heading[] =  {"PW", "EthPort", "SMAC",  "DMAC", "VLAN", "EthType", "Version", "Type", "MacCheckEn","VlanCheckEn", "ExpectCVlan", "ExpectLabel", "intrMask"};
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);

    /* Get PW List */
    AtUnused(argc);
    if (numPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        tAtEthVlanTag cVlan, sVlan;
        uint8 mac[cAtMacAddressLen];
        eBool isEnable;
        uint32 colum = 0;
        uint32 intrMaskVal;
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        AtChannel hdlc = AtPwBoundCircuitGet(pw);
        AtEthPort ethPort = AtPwEthPortGet(pw);

        StrToCell(tabPtr, pw_i, colum++, AtChannelIdString((AtChannel) hdlc));

        if (ethPort == NULL)
            StrToCell(tabPtr, pw_i, colum++, "none");
        else
            {
            AtSnprintf(buffer, bufferSize, "%s(%d)", AtObjectToString((AtObject)ethPort), AtChannelIdGet((AtChannel)ethPort) + 1);
            StrToCell(tabPtr, pw_i, colum++, buffer);
            }

        /* Source MAC */
        AtPwEthSrcMacGet(pw, mac);
        StrToCell(tabPtr, pw_i, colum++, CliEthPortMacAddressByte2Str(mac));

        /* Dest MAC */
        AtPwEthDestMacGet(pw, mac);
        StrToCell(tabPtr, pw_i, colum++, CliEthPortMacAddressByte2Str(mac));

        /* VLAN */
        AtOsalMemInit(&cVlan, 0, sizeof(tAtEthVlanTag));
        AtOsalMemInit(&sVlan, 0, sizeof(tAtEthVlanTag));
        StrToCell(tabPtr, pw_i, colum++, CliVlans2Str(AtPwEthCVlanGet(pw, &cVlan), AtPwEthSVlanGet(pw, &sVlan)));

        StrToCell(tabPtr, pw_i, colum++, CliNumber2String(AtCienaDccPwEthTypeGet(pw), "0x%04x"));
        StrToCell(tabPtr, pw_i, colum++, CliNumber2String(AtCienaDccPwVersionGet(pw), "0x%02x"));
        StrToCell(tabPtr, pw_i, colum++, CliNumber2String(AtCienaDccPwTypeGet(pw), "0x%02x"));

        isEnable = AtCienaDccPwDestMacCheckIsEnabled(pw);
        ColorStrToCell(tabPtr, pw_i, colum++, CliBoolToString(isEnable), isEnable ? cSevInfo : cSevNormal);

        isEnable = AtCienaDccPwCVLanCheckIsEnabled(pw);
        ColorStrToCell(tabPtr, pw_i, colum++, CliBoolToString(isEnable), isEnable ? cSevInfo : cSevNormal);

        StrToCell(tabPtr, pw_i, colum++, CliNumber2String(AtCienaEthPortExpectedCVlanGet(ethPort), "0x%04x"));
        StrToCell(tabPtr, pw_i, colum++, CliNumber2String(AtCienaDccPwExpectedLabelGet(pw), "0x%02x"));

        /* Print interrupt mask values */
        intrMaskVal = AtChannelInterruptMaskGet((AtChannel)pw);
        ColorStrToCell(tabPtr, pw_i, colum++, Alarm2String(intrMaskVal), cSevNormal);
        }

    AtObjectDelete((AtObject)pwList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCienaPwDccEthSourceMacSet(char argc, char **argv)
    {
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);
    AtUnused(argc);
    return CliPwEthMacSet(pwList, argv[1], AtPwEthSrcMacSet);
    }

eBool CmdAtCienaPwDccEthHeaderSet(char argc, char **argv)
    {
    uint32 numPws;
    AtPw * pwArrays = CliPwDccArrayFromStringGet(argv[0], &numPws);
    AtUnused(argc);
    return CliPwEthHeaderSet(pwArrays, numPws, argv[1], argv[2], argv[3], AtPwEthHeaderSet);
    }

eBool CmdAtCienaPwDccCountersShow(char argc, char **argv)
    {
    uint32 numPws;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    AtPw * pwArrays = CliPwDccArrayFromStringGet(argv[0], &numPws);

	AtUnused(argc);

	if (numPws == 0)
        {
	    AtPrintc(cSevWarning, "WARNING: No PWs to display, the input ID may be wrong\r\n");
	    return cAtTrue;
        }

    /* Get reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode, expect: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc >= 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    return CliAtPwCounterShow(pwArrays, numPws, readingMode, silent, BuildHeadingForPws);
    }

eBool CmdAtCienaDccPwDestMacCheckEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtCienaDccPwDestMacCheckEnable);
    }

eBool CmdAtCienaDccPwDestMacCheckDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtCienaDccPwDestMacCheckEnable);
    }

eBool CmdAtCienaDccExpectGlobalMacSet(char argc, char **argv)
    {
    uint16 port_i;
    uint32 *idBuf;
    uint8 mac[cAtMacAddressLen], numPorts;
    AtEthPort *ports;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Get MAC */
    AtOsalMemInit(mac, 0, cAtMacAddressLen);
    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevWarning, "WARNING: MAC is invalid!!!\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = AtCienaDccExpectGlobalMacSet(ports[port_i], mac);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Can not configure MAC for %s, ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)ports[port_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtOsalMemFree(ports);
    return success;
    }

eBool CmdAtCienaDccPwCVLanCheckEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtCienaDccPwCVLanCheckEnable);
    }

eBool CmdAtCienaDccPwCVLanCheckDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtCienaDccPwCVLanCheckEnable);
    }

eBool CmdAtCienaDccPwExpectedLabelSet(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeUint8Set(argv, AtCienaDccPwExpectedLabelSet);
    }

eBool CmdAtCienaDccPwIntrMsk(char argc, char **argv)
    {
    eAtRet ret;
    uint32 intrMaskVal, pw_i;
    eBool enable = cAtFalse;
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);
    uint32 numPws = AtListLengthGet(pwList);
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = IntrMaskFromString(argv[1])) == 0)
        {
        AtObjectDelete((AtObject) pwList);
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtObjectDelete((AtObject) pwList);
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* Get PW List */
    if (numPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
		ret = AtChannelInterruptMaskSet((AtChannel)pw, intrMaskVal, enable ? intrMaskVal : 0x0);
		if (ret != cAtOk)
			{
			success = cAtFalse;
			AtPrintc(cSevCritical, "ERROR: Cannot configure interrupt mask for Channel %s, ret = %s\r\n",
					 AtChannelIdString((AtChannel)pw),
					 AtRet2String(ret));
			}
        }

    AtObjectDelete((AtObject) pwList);
    return success;
    }

eBool CmdAtCienaDccPwInterruptsShow(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    uint32 (*DefectInterruptGet)(AtChannel self) = AtChannelDefectInterruptGet;
    eBool silent = cAtFalse;

    AtUnused(argc);

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        DefectInterruptGet = AtChannelDefectInterruptClear;

    return StatusShowWithSilent(argv[0], DefectInterruptGet, silent);
    }

eBool CmdAtCienaDccPwAlarmShow(char argc, char **argv)
    {
    AtUnused(argc);
    return StatusShowWithSilent(argv[0], AtChannelDefectGet, cAtFalse);
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [PW] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtCienaDccPwAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtCienaDccPwAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtCienaDccPwAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

eBool CmdAtCienaDccPwAlarmCapture(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    static tAtChannelEventListener intrListener;
    eBool captureEnabled = cAtTrue;
    eBool convertSuccess;
    eBool success = cAtTrue;
    AtList pwList = CliPwDccHdlcListFromString(argv[0]);
    uint32 numPws = AtListLengthGet(pwList);
    AtUnused(argc);

    if (numPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC PW, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    intrListener.AlarmChangeState = AlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = (AtChannel)AtListObjectGet(pwList, i);

        if (captureEnabled)
            ret = AtChannelEventListenerAdd(channel, &intrListener);
        else
            ret = AtChannelEventListenerRemove(channel, &intrListener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }
    AtObjectDelete((AtObject) pwList);

    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtCienaDevice.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Device specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCiena.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtCienaFaceplateSerdesGroupSet(char argc, char** argv)
    {
    uint32 groupId = AtStrToDw(argv[0]);
    eAtRet ret = AtCienaFaceplateSerdesGroupSet(CliDevice(), (uint8)(groupId - 1));
    if (ret == cAtOk)
        return cAtTrue;

    AtUnused(argc);
    AtPrintc(cSevCritical, "ERROR: Select group %u fail with ret = %s\r\n",
             groupId, AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtCienaMroShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"FaceplateGroup"};

    /* Create device table */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, 0, CliNumber2String(AtCienaFaceplateSerdesGroupGet(CliDevice()) + 1, "%u"));

    /* Show it */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtUnused(argc);
    AtUnused(argv);

    return cAtTrue;
    }

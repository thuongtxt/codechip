/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : CliAtCiena.h
 * 
 * Created Date: Sep 27, 2017
 *
 * Description : Ciena specific CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATCIENA_H_
#define _CLIATCIENA_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtRet (*KBytePwAttributeSetFunc)(AtPw, uint32);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtCienaKBytePwAttributeSetWithMaxValue(char argc, char **argv, KBytePwAttributeSetFunc func, uint32 maxValue);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATCIENA_H_ */


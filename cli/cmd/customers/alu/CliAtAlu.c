/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module ALU
 *
 * File        : CliAtAlu.c
 *
 * Created Date: Nov 26, 2014
 *
 * Description : ALU CLI implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtIpCore.h"
#include "AtDevice.h"
#include "AtAlu.h"
#include "AtXauiMdio.h"
#include "../../pw/CliAtModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPllStickyPrint(stickyName)                                            \
    AtPrintc(cSevNormal, "* "#stickyName": ");                                 \
        if (stickyMask & cAtAluFpgaPllSticky##stickyName##PllNotLocked)        \
            AtPrintc(cSevCritical, "Not lock\r\n");                            \
        else                                                                   \
            AtPrintc(cSevInfo, "Locked\r\n");

#define mFifoStickyPrint(stickyName)                                           \
    AtPrintc(cSevNormal, "* "#stickyName": ");                                 \
        if (stickyMask & cAtAluFpgaFifoSticky##stickyName)                     \
            AtPrintc(cSevCritical, "Set\r\n");                                 \
        else                                                                   \
            AtPrintc(cSevInfo, "Clear\r\n");

#define mGeneralStickyPrint(stickyName)                                        \
    AtPrintc(cSevNormal, "* "#stickyName": ");                                 \
        if (stickyMask & cAtAluFpgaGeneralSticky##stickyName)                  \
            AtPrintc(cSevCritical, "Set\r\n");                                 \
        else                                                                   \
            AtPrintc(cSevInfo, "Clear\r\n");

#define mClockPrint(clockName)                                                 \
    AtPrintc(cSevNormal, "* "#clockName": %d (HZ)\r\n", AtAlu##clockName##CurrentFrequencyGet(device));

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tRamMargins
    {
    uint8 detectSuccess;
    uint8 id;
    uint32 readLeft;
    uint32 readRight;
    uint32 writeLeft;
    uint32 writeRight;
    uint32 elapseTime;
    }tRamMargins;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtSemValidateRetStr[] =
    {
    "Corrected",
    "Not active",
    "FSM failed",
    "Fatal error",
    "Uncorrectable",
    };

static const eAtSemValidateRet cAtSemValidateRetVal[] =
    {
    cAtSemValidateCorrected,
    cAtSemValidateNotActive,
    cAtSemValidateFsmFailed,
    cAtSemValidateFatalError,
    cAtSemValidateUncorrectable
    };

/*--------------------------- Forward declarations ---------------------------*/
extern uint32 Tha60150011QdrMarginDetectMaxElapseTime(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
static eBool QdrIdIsValid(uint8 qdrId)
    {
    return ((qdrId > 0) && (qdrId < 3)) ? cAtTrue : cAtFalse;
    }

static eBool RamMarginsShow(uint8 numRam, tRamMargins* margins)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"ID", "Read left (ps)", "Read right (ps)", "Write left (ps)", "Write right (ps)", "Elapse time (us)"};
    uint8 ram_i;
    char stringBuf[16];

    tabPtr = TableAlloc(numRam, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRam; ram_i++)
        {
        AtSprintf(stringBuf, "%d", margins[ram_i].id);
        ColorStrToCell(tabPtr, ram_i, 0, stringBuf, cSevNormal);

        if (!margins[ram_i].detectSuccess)
            {
            ColorStrToCell(tabPtr, ram_i, 1, "xxx", cSevCritical);
            ColorStrToCell(tabPtr, ram_i, 2, "xxx", cSevCritical);
            ColorStrToCell(tabPtr, ram_i, 3, "xxx", cSevCritical);
            ColorStrToCell(tabPtr, ram_i, 4, "xxx", cSevCritical);
            }
        else
            {
            AtSprintf(stringBuf, "%u", margins[ram_i].readLeft);
            ColorStrToCell(tabPtr, ram_i, 1, stringBuf, cSevNormal);

            AtSprintf(stringBuf, "%u", margins[ram_i].readRight);
            ColorStrToCell(tabPtr, ram_i, 2, stringBuf, cSevNormal);

            AtSprintf(stringBuf, "%u", margins[ram_i].writeLeft);
            ColorStrToCell(tabPtr, ram_i, 3, stringBuf, cSevNormal);

            AtSprintf(stringBuf, "%u", margins[ram_i].writeRight);
            ColorStrToCell(tabPtr, ram_i, 4, stringBuf, cSevNormal);
            }

        AtSprintf(stringBuf, "%u", margins[ram_i].elapseTime);
        ColorStrToCell(tabPtr, ram_i, 5, stringBuf, cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static void QdrMarginDetect(uint8 qdrId, tRamMargins* margin)
    {
    tAtOsalCurTime stopTime, startTime;
    eAtRet ret;

    margin->id = qdrId;
    qdrId = (uint8)(qdrId - 1);

    AtOsalCurTimeGet(&startTime);
    ret = AtAluQdrMarginDetect(CliDevice(), qdrId);
    AtOsalCurTimeGet(&stopTime);
    margin->elapseTime = mTimeIntervalInUsGet(startTime, stopTime);

    if (ret == cAtOk)
        {
        margin->detectSuccess = cAtTrue;
        margin->readLeft      = AtAluQdrReadLeftMargin(CliDevice(), qdrId);
        margin->readRight     = AtAluQdrReadRightMargin(CliDevice(), qdrId);
        margin->writeLeft     = AtAluQdrWriteLeftMargin(CliDevice(), qdrId);
        margin->writeRight    = AtAluQdrWriteRightMargin(CliDevice(), qdrId);
        }
    else
        margin->detectSuccess = cAtFalse;
    }

static eBool SemValidateRandomAddress(void)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Result", "Error count"};
    char stringBuf[16];
    eBool convertSuccess;
    eAtSemValidateRet semValidate;

    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    semValidate = AtAluDeviceSemValidate(CliDevice());
    mAtEnumToStr(cAtSemValidateRetStr, cAtSemValidateRetVal, semValidate, stringBuf, convertSuccess);
    if (!convertSuccess)
        ColorStrToCell(tabPtr, 0, 0, "Unknown", cSevInfo);

    else
        {
        if (semValidate == cAtSemValidateCorrected)
            {
            ColorStrToCell(tabPtr, 0, 0, stringBuf, cSevInfo);
            AtPrintc(cSevInfo, "Max FSM: %u, Max time insert error: %u\r\n",
                     AtAluSemControllerMaxElapseTimeToChangeFsm(CliDevice()),
                     AtAluSemControllerMaxElapseTimeToInsertError(CliDevice()));
            }
        else
            ColorStrToCell(tabPtr, 0, 0, stringBuf, cSevCritical);
        }

    AtSprintf(stringBuf, "%d", AtAluDeviceSemErrorCounterGet(CliDevice()));
    ColorStrToCell(tabPtr, 0, 1, stringBuf, cSevNormal);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static AtHal Hal(void)
    {
    return AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    }

static eBool XauiReset(char argc, char **argv, eBool reset)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtXauiReset(Hal(), reset);
    return cAtTrue;
    }

eBool CmdAtAluPerformanceMonitoringShow(char argc, char **argv)
    {
    uint32      performanceValue;
    eBool       nearEnd = cAtTrue;
    uint32      numPws, i;
    tTab        *tabPtr;
    const char  *heading[] = {"PW", "Error blocks", "Suspect second", "Defect second"};
    static char buf[50];

    AtList pwList = CliPwListFromString(argv[0]);
    AtUnused(argc);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    if (AtStrcmp(argv[1], "farend") == 0)
        nearEnd = cAtFalse;

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);

        /* ID */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)pw));
        performanceValue = (nearEnd) ? AtAluPwNEPerformanceMornitoringGet(pw) : AtAluPwFEPerformanceMornitoringGet(pw);

        AtSprintf(buf, "%lu", performanceValue & cBit13_0);
        ColorStrToCell(tabPtr, i, 1, buf, (performanceValue & cBit13_0) ? cSevCritical : cSevNormal);

        StrToCell(tabPtr, i, 2, (performanceValue & cBit14) ? "set" : "clear");
        ColorStrToCell(tabPtr, i, 3, (performanceValue & cBit15) ? "set" : "clear", (performanceValue & cBit15) ? cSevCritical : cSevNormal);
        }

    /* Show it */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdAtAluQdrTest(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"QDR",  "Result"};
    uint32 *qdrIdList;
    uint32 bufferSize;
    uint32 numQdr;
    uint32 i;

    AtUnused(argc);

    /* Get QDR list */
    qdrIdList  = CliSharedIdBufferGet(&bufferSize);
    numQdr = CliIdListFromString(argv[0], qdrIdList, bufferSize);
    if (numQdr == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No QDR, the ID List may be wrong, expect 1,2,...\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numQdr, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numQdr; i++)
        {
        uint8 qdrId = (uint8)qdrIdList[i];
        eAtRet ret;

        ColorStrToCell(tabPtr, i, 0, CliNumber2String(qdrId, "%u"), cSevNormal);
        if (!QdrIdIsValid(qdrId))
            {
            ColorStrToCell(tabPtr, i, 1, "Qdr Id is not valid", cSevCritical);
            continue;
            }

        ret = AtAluQdrTest(CliDevice(),  (uint8)(qdrId - 1));
        if (ret == cAtOk)
            ColorStrToCell(tabPtr, i, 1, "PASS", cSevInfo);
        else
            ColorStrToCell(tabPtr, i, 1, AtRet2String(ret), cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtAluQdrStatus(char argc, char **argv)
    {
    tTab *tabPtr;
    eAtHistoryReadingMode readToClear;
    const char *pHeading[] = {"QDR",  "Parity Error"};
    uint32 *qdrIdList;
    uint32 bufferSize;
    uint32 numQdr;
    uint32 i;
    eBool (*ParityErrorGet)(AtDevice device, uint8 qdrId);

    AtUnused(argc);

    /* Get QDR list */
    qdrIdList  = CliSharedIdBufferGet(&bufferSize);
    numQdr = CliIdListFromString(argv[0], qdrIdList, bufferSize);
    if (numQdr == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No QDR, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Get reading mode */
    readToClear = CliHistoryReadingModeGet(argv[1]);
    if (readToClear == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    ParityErrorGet = (readToClear) ? AtAluQdrParityErrorClear : AtAluQdrParityErrorGet;
    tabPtr = TableAlloc(numQdr, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numQdr; i++)
        {
        eBool parityError;
        uint8 qdrId = (uint8)qdrIdList[i];

        ColorStrToCell(tabPtr, i, 0, CliNumber2String(qdrId, "%u"), cSevNormal);
        if (!QdrIdIsValid(qdrId))
            {
            ColorStrToCell(tabPtr, i, 1, "Qdr Id is not valid", cSevCritical);
            continue;
            }
        parityError = ParityErrorGet(CliDevice(), (uint8)(qdrId - 1));
        ColorStrToCell(tabPtr, i, 1, parityError ? "Set" : "Clear", parityError ? cSevCritical : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtAluPllStickyGet(char argc, char **argv)
    {
    eAtHistoryReadingMode readToClear;
    uint32 stickyMask;

    AtUnused(argc);

    /* Get reading mode */
    readToClear = CliHistoryReadingModeGet(argv[0]);
    if (readToClear == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Read sticky */
    stickyMask = (readToClear) ? AtAluPllAlarmStickyClear(CliDevice()) : AtAluPllAlarmStickyGet(CliDevice());

    mPllStickyPrint(XAUI156MHz)
    mPllStickyPrint(QdrNo2Feedback)
    mPllStickyPrint(QdrNo1Feedback)
    mPllStickyPrint(Ref100MHzCore2)
    mPllStickyPrint(Ref100MHzCore1)
    mPllStickyPrint(Sys155MHz)

    return cAtTrue;
    }

eBool CmdAtAluFifoStickyGet(char argc, char **argv)
    {
    eAtHistoryReadingMode readToClear;
    uint32 stickyMask;

    AtUnused(argc);

    /* Get reading mode */
    readToClear = CliHistoryReadingModeGet(argv[0]);
    if (readToClear == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    stickyMask = (readToClear) ? AtAluFifoAlarmStickyClear(CliDevice()) : AtAluFifoAlarmStickyGet(CliDevice());

    mFifoStickyPrint(TxPktDataValidCmdFifoFull)
    mFifoStickyPrint(TxPktInfoValidCmdFifoFull)
    mFifoStickyPrint(RxPktInfoValidCmdFifoFull)
    mFifoStickyPrint(RxPktDataValidCmdFifoFull)
    mFifoStickyPrint(TxPweDataFifoEmpty)
    mFifoStickyPrint(TxPweWrPacketFifoFull)
    mFifoStickyPrint(TxPweRdPacketFifoFull)
    mFifoStickyPrint(RxPweWrCacheFifoFull)
    mFifoStickyPrint(RxPweTdmValidSegmentFifoFull)
    mFifoStickyPrint(RxPweRdSegmentFifoFull)
    mFifoStickyPrint(RxPweLinkListValidCmdFifoFull)
    mFifoStickyPrint(RxPweLinkListRequestCmdFifoFull)
    mFifoStickyPrint(RxPweTdmFifoFull)
    mFifoStickyPrint(RxPweTdmFifoEmpty)
    mFifoStickyPrint(RxGeFifoFull)

    return cAtTrue;
    }

eBool CmdAtAluGeneralStickyGet(char argc, char** argv)
    {
    eAtHistoryReadingMode readToClear;
    uint32 stickyMask;

    AtUnused(argc);

    /* Get reading mode */
    readToClear = CliHistoryReadingModeGet(argv[0]);
    if (readToClear == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    stickyMask = (readToClear) ? AtAluGeneralAlarmStickyClear(CliDevice()) : AtAluGeneralAlarmStickyGet(CliDevice());

    mGeneralStickyPrint(TxPktInfoMemoryBusError)
    mGeneralStickyPrint(RxPktInfoEccError)
    mGeneralStickyPrint(RxPktInfoMemoryBusError)
    mGeneralStickyPrint(RxPktDataReorderCmdError)
    mGeneralStickyPrint(RxPktDataMemoryBusError)
    mGeneralStickyPrint(RxPktDataDdrCrcError)
    mGeneralStickyPrint(TxPweWrCacheDuplication)
    mGeneralStickyPrint(TxPweWrCacheEmpty)
    mGeneralStickyPrint(TxPweDataBlockDuplication)
    mGeneralStickyPrint(TxPweRdCacheDuplication)
    mGeneralStickyPrint(RxPweWrPktOversize)
    mGeneralStickyPrint(RxPweWrDataBufferPause)
    mGeneralStickyPrint(RxPweWrCacheDisable)
    mGeneralStickyPrint(RxPweDataBufferDisable)
    mGeneralStickyPrint(RxPweRdCacheLost)
    mGeneralStickyPrint(RxPweLostStartOfPkt)
    mGeneralStickyPrint(RxPweLostEndOfPkt)
    mGeneralStickyPrint(RxPweRdCacheError)
    mGeneralStickyPrint(RxPweWrCacheEmpty)
    mGeneralStickyPrint(RxPweWrCacheDuplication)
    mGeneralStickyPrint(RxPweRdCacheEmpty)
    mGeneralStickyPrint(RxPweRdCacheDuplication)
    mGeneralStickyPrint(RxPweDataBufferEmpty)
    mGeneralStickyPrint(RxPweDataBlockDuplication)

    return cAtTrue;
    }

eBool CmdAtAluClockGet(char argc, char** argv)
    {
    AtDevice device = CliDevice();

    AtUnused(argc);
    AtUnused(argv);

    mClockPrint(XGMIIClock156_25MHz)
    mClockPrint(OcnTxClock155_52MHz)
    mClockPrint(OcnRxClock155_52MHz)
    mClockPrint(Core1Clock100MHz)
    mClockPrint(Core2Clock100MHz)
    mClockPrint(Ddr3UserClock100MHz)
    mClockPrint(PcieUserClock62_5MHz)
    mClockPrint(QdrNo1FeedbackClock155_52MHz)
    mClockPrint(QdrNo2FeedbackClock155_52MHz)

    return cAtTrue;
    }

eBool CmdAtAluDdrMarginsDetect(char argc, char **argv)
    {
    tAtOsalCurTime stopTime, startTime;
    tRamMargins margin;
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    AtOsalCurTimeGet(&startTime);
    ret = AtAluDdrMarginDetect(CliDevice());
    AtOsalCurTimeGet(&stopTime);

    margin.elapseTime = mTimeIntervalInUsGet(startTime, stopTime);
    margin.id = 1;
    if (ret == cAtOk)
        {
        margin.detectSuccess = cAtTrue;
        margin.readLeft      = AtAluDdrReadLeftMargin(CliDevice());
        margin.readRight     = AtAluDdrReadRightMargin(CliDevice());
        margin.writeLeft     = AtAluDdrWriteLeftMargin(CliDevice());
        margin.writeRight    = AtAluDdrWriteRightMargin(CliDevice());
        }
    else
        margin.detectSuccess = cAtFalse;

    return RamMarginsShow(1, &margin);
    }

eBool CmdAtAluQdrMarginsDetect(char argc, char **argv)
    {
    uint32 *qdrIdList;
    uint32 numQdr, i;
    uint8 numValidRam;
    tRamMargins margin[2];
    uint32 bufferSize;

    AtUnused(argc);

    /* Get QDR list */
    qdrIdList  = CliSharedIdBufferGet(&bufferSize);
    numQdr = CliIdListFromString(argv[0], qdrIdList, bufferSize);
    if (numQdr == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No QDR, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    numValidRam = 0;
    for (i = 0; i < numQdr; i++)
        {
        uint8 qdrId = (uint8)qdrIdList[i];
        if (qdrId > 2)
            {
            AtPrintc(cSevCritical, "Invalid QDR ID, expect 1 or 2\r\n");
            continue;
            }

        QdrMarginDetect(qdrId, &margin[numValidRam]);
        numValidRam++;
        if (numValidRam == 2)
            break;
        }

    AtPrintc(cSevInfo, "Max trigger elapse time: %u (ms)\r\n", Tha60150011QdrMarginDetectMaxElapseTime(CliDevice()));
    return RamMarginsShow(numValidRam, margin);
    }

eBool CmdAtAluSemStatusGet(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Active", "Status", "Alarm", "Error count"};
    char stringBuf[16];
    eAtSemStatus semStatus;
    eAtSemAlarm semAlarm;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* SEM STATUS */
    semStatus = AtAluDeviceSemStatusGet(CliDevice());
    if (semStatus == 0)
        AtPrintc(cSevCritical, "SEM IP is not active\r\n");

    if (semStatus & cAtSemStatusActive)
        ColorStrToCell(tabPtr, 0, 0, "Yes", cSevInfo);
    else
        ColorStrToCell(tabPtr, 0, 0, "No", cSevCritical);

    if (semStatus & cAtSemStatusIdle)
        ColorStrToCell(tabPtr, 0, 1, "Idle", cSevNormal);
    else if (semStatus & cAtSemStatusObservation)
        ColorStrToCell(tabPtr, 0, 1, "Observation", cSevNormal);
    else if (semStatus & cAtSemStatusInitialization)
        ColorStrToCell(tabPtr, 0, 1, "Initialization", cSevNormal);
    else if (semStatus & cAtSemStatusUncorrectable)
        ColorStrToCell(tabPtr, 0, 1, "Uncorrectable", cSevWarning);
    else
        ColorStrToCell(tabPtr, 0, 1, "Unknown", cSevWarning);

    /* SEM ALARM */
    semAlarm = AtAluDeviceSemAlarmClear(CliDevice());
    if (semAlarm & cAtSemAlarmFatalError)
        ColorStrToCell(tabPtr, 0, 2, "Fatal error", cSevCritical);
    else if (semAlarm & cAtSemAlarmInitialization)
        ColorStrToCell(tabPtr, 0, 2, "Initialization", cSevNormal);
    else if (semAlarm & cAtSemAlarmCorrection)
        ColorStrToCell(tabPtr, 0, 2, "Correction", cSevInfo);
    else
        ColorStrToCell(tabPtr, 0, 2, "None", cSevNormal);

    /* SEM Counter */
    AtSprintf(stringBuf, "%u", AtAluDeviceSemErrorCounterGet(CliDevice()));
    ColorStrToCell(tabPtr, 0, 3, stringBuf, cSevNormal);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtAluSemValidate(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return SemValidateRandomAddress();
    }

eBool CmdAtAluGeFifoStatusGet(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"History", "Max reached level (bytes)"};
    char stringBuf[16];
    eAtHistoryReadingMode readToClear;

    AtUnused(argc);

    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Get reading mode */
    readToClear = CliHistoryReadingModeGet(argv[0]);
    if (readToClear == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (AtAluFifoAlarmStickyClear(CliDevice()) & cAtAluFpgaFifoStickyRxGeFifoFull)
        ColorStrToCell(tabPtr, 0, 0, "full", cSevCritical);
    else
        ColorStrToCell(tabPtr, 0, 0, "normal", cSevInfo);

    if (readToClear == cAtHistoryReadingModeReadToClear)
        AtSprintf(stringBuf, "%u", AtAluGeFifoMaxLevelClear(CliDevice()));
    else
        AtSprintf(stringBuf, "%u", AtAluGeFifoMaxLevelGet(CliDevice()));

    ColorStrToCell(tabPtr, 0, 1, stringBuf, cSevNormal);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtAluXauiStatusGet(char argc, char **argv)
    {
    eAtHistoryReadingMode readToClear;
    uint32 alarm;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    tTab *tabPtr;
    const char *pHeading[] = {"TxNotReady",  "TxLocalFault", "RxLocalFault", "RxNotSyn", "RxNotAlign", "LinkState"};
    uint8 bit_i;

    AtUnused(argc);

    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    readToClear = CliHistoryReadingModeGet(argv[0]);
    if (readToClear == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    alarm = AtXauiStatusRead2Clear(hal, readToClear);

    for (bit_i = 0; bit_i < mCount(pHeading); bit_i++)
       {
       if (alarm & (cBit0 << bit_i))
           ColorStrToCell(tabPtr, 0, bit_i, (bit_i < 5) ? "set" : "down", cSevCritical);
       else
           ColorStrToCell(tabPtr, 0, bit_i, (bit_i < 5) ? "clear" : "up", cSevInfo);
       }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtAluXauiResetStart(char argc, char **argv)
    {
    return XauiReset(argc, argv, cAtTrue);
    }

eBool CmdAtAluXauiResetStop(char argc, char **argv)
    {
    return XauiReset(argc, argv, cAtFalse);
    }

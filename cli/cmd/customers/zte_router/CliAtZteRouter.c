/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : CliAtZteRouter.c
 *
 * Created Date: Jul 9, 2014
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../pw/CliAtModulePw.h"
#include "AtDevice.h"
#include "AtPw.h"
#include "AtList.h"
#include "AtZteRouter.h"
#include "../../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtZteRouterTag *CliAtZteRouterStr2Tag(char *tagStr, tAtZteRouterTag *tag)
    {
    uint32 zteTagBuf[10];
    uint32 numItem;
    static char minFormat[] = "0.0.0.0.0.0";
    static char maxFormat[] = "1.15.15.15.1023.63";

    IdComplexNumParse((void*)tagStr, minFormat, maxFormat, 10, zteTagBuf, &numItem);
    if (numItem != 6)
        return NULL;

    tag->cpuPktIndicator  = (uint8)zteTagBuf[0];
    tag->portNumber       = (uint8)zteTagBuf[1];
    tag->encapType        = (uint8)zteTagBuf[2];
    tag->slotNumber       = (uint8)zteTagBuf[3];
    tag->channelNumber    = (uint16)zteTagBuf[4];
    tag->packetLength     = (uint8)zteTagBuf[5];

    return tag;
    }

static const char *CliAtZteRouterTagFormat(void)
    {
    return "<cpuPktIndicator:0/1>.<portNumber:0..15>.<encapType:0..15>.<slotNumber:0..15>.<channelNumber:0..1023>.<pktLen:0..63>";
    }

eBool CmdPwZteRouterExpectedTagSet(char argc, char **argv)
    {
    AtList pwList;
    tAtZteRouterTag tag;
    uint32 pw_i;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Parse tag 1 */
    if (CliAtZteRouterStr2Tag(argv[1], &tag) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag, expect %s\r\n", CliAtZteRouterTagFormat());
        return cAtFalse;
        }

    /* Get PW list */
    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW to configure, ID list may be invalid. Expect: 1,2-4,...\r\n");
        return cAtTrue;
        }

    /* Apply configuration */
    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        eAtRet ret = cAtOk;
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);

        ret = AtZteRouterPwExpectedTagSet(pw, &tag);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwList);

    return success;
    }

eBool CmdPwZteRouterHeaderSet(char argc, char **argv)
    {
    AtPw pw;
    uint16 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPws;
    AtModulePw pwModule;
    tAtZteRouterTag zteTag;
    uint8 mac[cAtMacAddressLen];
    uint16 ethType;
    eBool success = cAtTrue;
	AtUnused(argc);

    AtOsalMemInit(mac, 0, cAtMacAddressLen);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid MAC address\r\n");
        return cAtFalse;
        }

    ethType = (uint16)AtStrToDw(argv[2]);

    if (CliAtZteRouterStr2Tag(argv[3], &zteTag) == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid tag, expect %s\r\n", CliAtZteRouterTagFormat());
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i++)
        {
        eAtRet ret = cAtOk;

        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, idBuf[i] - 1);
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %d\r\n", idBuf[i]);
            continue;
            }

        /* Apply new header */
        ret = AtZteRouterPwHeaderSet(pw, mac, ethType, &zteTag);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static char *SharedBuffer(void)
    {
    static char sharedCharBuffer[128];
    return sharedCharBuffer;
    }

static char *TagToStr(const tAtZteRouterTag *zteTag)
    {
    char *buffer = SharedBuffer();

    if (zteTag == NULL)
        {
        AtSprintf(buffer, "none");
        return buffer;
        }

    AtSprintf(buffer,
              "%d.%d.%d.%d.%d.%d",
              zteTag->cpuPktIndicator,
              zteTag->portNumber,
              zteTag->encapType,
              zteTag->slotNumber,
              zteTag->channelNumber,
              zteTag->packetLength);

    return buffer;
    }

eBool CmdPwZteRouterHeaderShow(char argc, char **argv)
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static uint8  mac[6];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "txTag", "expTag", "Eth Port", "DMAC", "Eth Type"};
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);
    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw;
        AtEthPort ethPort;
        tAtZteRouterTag zteTag;

        pw = (AtPw)AtListObjectGet(pwList, i);

        column = 0;
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        /* Tags */
        StrToCell(tabPtr, i, column++, TagToStr(AtZteRouterPwTagGet(pw, &zteTag)));
        StrToCell(tabPtr, i, column++, TagToStr(AtZteRouterPwExpectedTagGet(pw, &zteTag)));

        /* ETH port */
        ethPort = AtPwEthPortGet(pw);
        if (ethPort == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            {
            AtSprintf(SharedBuffer(), "%d", AtChannelIdGet((AtChannel)ethPort) + 1);
            StrToCell(tabPtr, i, column++, SharedBuffer());
            }

        /* MAC */
        AtOsalMemInit(mac, 0, sizeof(mac));
        AtPwEthDestMacGet(pw, mac);
        StrToCell(tabPtr, i, column++, (char *)AtBytes2String(mac, 6, 16));

        /* Ethernet type */
        AtSprintf(SharedBuffer(), "0x%hx", AtZteRouterPwEthTypeGet(pw));
        StrToCell(tabPtr, i, column++, SharedBuffer());
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);
    AtPrintc(cSevInfo, "Tag format: ");
    AtPrintc(cSevNormal, "cpuPktIndicator.portNumber.encapType.slotNumber.channelNumber.pktLen\r\n");

    return cAtTrue;
    }

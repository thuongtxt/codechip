/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDevice.c
 *
 * Created Date: Nov 11, 2012
 *
 * Description : AtDevice CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <assert.h>
#include "AtOsal.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "AtLongtRegisterAudit.h"
#include "AtShortRegisterAudit.h"

/*--------------------------- Define -----------------------------------------*/
typedef eBool (*ShortRegisterBlackListActionFunc)(tAtShortRegisterAudit *, uint32, uint8);
typedef eBool (*LongRegisterBlackListActionFunc)(tAtLongRegisterAudit *, uint32, uint8);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
#define cMaskTokenStr          "bit"
#define cMaskStrMaxLen         16
#define cMaskStrMinLen         4
#define cLongMaskSizes         (cLongRegisterMaxLength*32)
#define cShortMaskSizes        32
static uint32 g_register_mask[cLongRegisterMaxLength];

/*--------------------------- Global variables -------------------------------*/
static tAtRegisterWriteEventListener registerListener;
static tAtLongRegisterAudit *longRegisterAudit = NULL;
static tAtShortRegisterAudit *shortRegisterAudit = NULL;
static uint8 register_audit_start = 0;
static uint32 register_audit_duration;
static uint32 register_audit_numberRepeat;
static uint32 register_audit_counter;
static uint32 register_audit_long_fail_cnt;
static uint32 register_audit_short_fail_cnt;
static uint32 register_audit_ticks = 0;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
uint32 RegisterAuditPerformPeriodicProcess(uint32 periodInMs);
void RegisterAuditDestroy(void);
extern eBool AtDeviceIsSimulated(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
static const char *CliRegisterStatusToString(eBool value)
    {
    return value ? "running" : "not running";
    }

static eBool ValidStringMask(char *pMaskStr)
    {
    char strTmp[8];
    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, pMaskStr, 3);
    if((AtStrcmp(strTmp, cMaskTokenStr) != 0) ||
       (AtStrlen(pMaskStr) < cMaskStrMinLen)  ||
       (AtStrlen(pMaskStr) > cMaskStrMaxLen))
        {
        AtPrintc(cSevCritical, " StringMask is not valid!, %s \r\n", pMaskStr);
        return cAtFalse;
        }
    return cAtTrue;
    }

static void GlobalMaskReset(void)
    {
    uint8 counter_i;
    /* Clear Global Masks */
    for (counter_i = 0; counter_i < cLongRegisterMaxLength; counter_i++)
        g_register_mask[counter_i] = 0x0;
    }

static eBool StringToMaskGet(char *pMaskStr, uint32 bitMaxVal)
    {
    uint32 *idBuffer;
    uint32 numChannels, num_i, bit_i;
    uint32 register_i;
    uint32 pos_mask_i;
    char strTmp[16];
    if (!ValidStringMask(pMaskStr))
        return cAtFalse;
    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, &pMaskStr[3], AtStrlen(pMaskStr) - 3);

    /* It is just a list of flat IDs */
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(strTmp, idBuffer, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "IdList %s can not get channels \r\n", strTmp);
        return cAtFalse;
        }

    /* Clear Global Masks */
    GlobalMaskReset();

    /* BuildMask */
    for (num_i = 0; num_i < numChannels; num_i++)
        {
        bit_i = idBuffer[num_i];
        if (bit_i > bitMaxVal)
            {
            AtPrintc(cSevCritical, "bit_i %d out-of-range, bitMaxVal %d \r\n", bit_i, bitMaxVal);
            return cAtFalse;
            }

        /* Get Register_i and Pos_Mask_i */
        register_i = bit_i/32;
        pos_mask_i = bit_i%32;
        g_register_mask[register_i] += (cBit0 << pos_mask_i);
        }

    return cAtTrue;
    }

static eBool ShortRegisterBlackListAction(char argc,
                                          char **argv,
                                          ShortRegisterBlackListActionFunc action)
    {
    uint8 coreId = 0;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    eBool success  = cAtTrue;
    uint32 addressTemp;
    uint32 counter_i;

    if (argc > 1)
        numbers = AtStrtoul(argv[1], NULL, 10);

    if (argc > 2)
        step = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        coreId = (uint8) AtStrtoul(argv[3], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!action(shortRegisterAudit, addressTemp, coreId))
            {
            /*AtPrintc(cSevCritical,
                     " can not add  address 0x%08x core %d to black-list \r\n",
                     addressTemp,
                     coreId);*/
            success = cAtFalse;
            }
        }
    return success;
    }

static eBool LongRegisterBlackListAction(char argc,
                                          char **argv,
                                          LongRegisterBlackListActionFunc action)
    {
    uint8 coreId = 0;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    eBool success  = cAtTrue;
    uint32 addressTemp;
    uint32 counter_i;

    if (argc > 1)
        numbers = AtStrtoul(argv[1], NULL, 10);

    if (argc > 2)
        step = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        coreId = (uint8) AtStrtoul(argv[3], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!action(longRegisterAudit, addressTemp, coreId))
            {
            /*AtPrintc(cSevCritical,
                     " can not add  address 0x%08x core %d to black-list \r\n",
                     addressTemp,
                     coreId);*/
            success = cAtFalse;
            }
        }

    return success;
    }

static void causeLeakMem(void)
    {
    AtOsalMemFree((void*)longRegisterAudit);
    AtOsalMemFree((void*)longRegisterAudit);
    }

static void ShortWrite(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    AtUnused(self);
    if (AtDeviceIsSimulated(self))
        {
        if (address == 0x00510000)
            {
            causeLeakMem();
            AtPrintc(cSevInfo, "Found! \r\n");
            }
        }
    if (shortRegisterAudit)
        AtShortRegisterAuditCollect(shortRegisterAudit, address, value, coreId);
    }

static void LongWrite(AtDevice self,
                      uint32 address,
                      const uint32 *dataBuffer,
                      uint16 length,
                      uint8 moduleId,
                      uint8 coreId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    if (AtDeviceIsSimulated(self))
        {
        /*if (address == 0x448000)
            {
            causeLeakMem();
            AtPrintc(cSevInfo, "Found! \r\n");
            }
        if (address == 0x449000)
            {
            causeLeakMem();
            AtPrintc(cSevInfo, "Found! \r\n");
            }*/
        }
    if (longRegisterAudit)
        AtLongRegisterAuditInsert(longRegisterAudit, address, dataBuffer, length, coreId);
    }

static eBool IsRunning(void)
    {
    if (register_audit_start == 0)
        return cAtFalse;

    if (register_audit_counter < register_audit_numberRepeat)
        return cAtTrue;

    return cAtFalse;
    }

static void Perform(void)
    {
    if (!AtLongRegisterAuditPerform(longRegisterAudit))
        register_audit_long_fail_cnt ++;

    if (!AtShortRegisteAuditPerform(shortRegisterAudit))
        register_audit_short_fail_cnt ++;

    register_audit_counter++;
    }

static void Create(void)
    {
    registerListener.ShortWrite = ShortWrite;
    registerListener.LongWrite = LongWrite;
    shortRegisterAudit = AtShortRegisterAuditNew(CliDevice());
    assert(shortRegisterAudit);
    longRegisterAudit = AtLongRegisterAuditNew(CliDevice());
    assert(longRegisterAudit);
    if (AtDeviceRegisterEventListenerAdd(CliDevice(), &registerListener) != cAtOk)
        {
        AtShortRegisterAuditDelete(shortRegisterAudit);
        AtLongRegisterAuditDelete(longRegisterAudit);
        AtPrintc(cSevCritical, " Can not add Register listener \r\n");
        return;
        }

    /* Enable Start Register Audit */
    register_audit_start = 1;

    /* Default will Audit 10 times with duration is 10 seconds for each checking time */
    register_audit_duration = 10;
    register_audit_numberRepeat = 0;
    register_audit_long_fail_cnt = 0;
    register_audit_short_fail_cnt = 0;
    register_audit_counter = 0;
    register_audit_ticks = 0;
    }

static void Destroy(void)
    {
    AtDeviceRegisterEventListenerRemove(CliDevice());
    AtShortRegisterAuditDelete(shortRegisterAudit);
    AtLongRegisterAuditDelete(longRegisterAudit);
    register_audit_start = 0;
    }

void RegisterAuditDestroy(void)
    {
    if (register_audit_start == 1)
        Destroy();
    }

uint32 RegisterAuditPerformPeriodicProcess(uint32 periodInMs)
    {
    tAtOsalCurTime startTime, endTime;
    uint32 elapseTime = 0;
    if (!IsRunning())
        return elapseTime;

    register_audit_ticks += periodInMs;
    if ((register_audit_ticks/1000) == register_audit_duration)
        {
        AtOsalCurTimeGet(&startTime);
        register_audit_ticks = 0;
        Perform();
        AtOsalCurTimeGet(&endTime);
        elapseTime = mTimeIntervalInMsGet(startTime, endTime);
        }

    return elapseTime;
    }

eBool CmdAtDeviceRegisterAuditStart(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (register_audit_start == 0)
        Create();
    else
        AtPrintc(cSevCritical, " Register Audit Task is running, Please stop it first!!!!\r\n");

    return cAtTrue;
    }

eBool CmdAtDeviceRegisterAuditStop(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (register_audit_start == 1)
        Destroy();
    else
        AtPrintc(cSevCritical, " Register Audit Task is not started yet!!!\r\n");

    return cAtTrue;
    }

eBool CmdAtDeviceRegisterAuditPerform(char argc, char **argv)
    {
    uint32 duration = AtStrtoul(argv[0], NULL, 10);
    uint32 numberRepeat = AtStrtoul(argv[1], NULL, 10);
    AtUnused(argc);

    if (IsRunning())
        {
        AtPrintc(cSevCritical, " Register Audit Task is running, Please stop it first!!!!\r\n");
        return cAtFalse;
        }

    /* Add database to start this task */
    register_audit_long_fail_cnt = 0;
    register_audit_short_fail_cnt = 0;
    register_audit_counter = 0;
    register_audit_ticks = 0;
    register_audit_duration = duration;
    register_audit_numberRepeat = numberRepeat;


    if (register_audit_duration != 0)
        return cAtTrue;

    while (numberRepeat--)
        Perform();

    return cAtTrue;
    }

eBool CmdAtDeviceRegisterAuditShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 colIdx = 0;
    eBool enable;
    eBool srIsPassed = cAtTrue;
    eBool lrIsPassed = cAtTrue;
    const char *pHeading[] =
        {
        "Enable",
        "Status",
        "duration [s]",
        "repeat[N]",
        "Cur_Numbers",
        "SR_Numbers",
        "SR_Colision",
        "LR_Numbers",
        "LR_Colision",
        "SR_Fail_Cnt",
        "LR_Fail_Cnt"
        };

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    enable = (register_audit_start == 1) ? cAtTrue: cAtFalse;
    ColorStrToCell(tabPtr, 0, colIdx++, CliBoolToString(enable), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliRegisterStatusToString(IsRunning()), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_audit_duration, "%u"), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_audit_numberRepeat, "%u"), cSevInfo);
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_audit_counter, "%u"), cSevInfo);
    if (enable)
        {
        ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(AtShortRegisterAuditEntriesCount(shortRegisterAudit), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(AtShortRegisterAuditCollisionCount(shortRegisterAudit), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(AtLongRegisterAuditEntriesCount(longRegisterAudit), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(AtLongRegisterAuditCollisionCount(longRegisterAudit), "%u"), cSevInfo);
        }
    else
        {
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        ColorStrToCell(tabPtr, 0, colIdx++, "N/A", cSevNormal);
        }

    srIsPassed = (register_audit_short_fail_cnt == 0) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_audit_short_fail_cnt, "%u"), srIsPassed ? cSevNormal : cSevCritical);

    lrIsPassed  = (register_audit_long_fail_cnt ==0) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, 0, colIdx++, CliNumber2String(register_audit_long_fail_cnt, "%u"), lrIsPassed ? cSevNormal : cSevCritical);

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(srIsPassed && lrIsPassed));
    return cAtTrue;
    }

eBool CmdAtDeviceRegisterAuditVerboseOn(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (register_audit_start == 1)
        {
        AtShortRegisterAuditVerboseOn(shortRegisterAudit);
        AtLongRegisterAuditVerboseOn(longRegisterAudit);
        }
    return cAtTrue;
    }

eBool CmdAtDeviceRegisterAuditVerboseOff(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (register_audit_start == 1)
        {
        AtShortRegisterAuditVerboseOff(shortRegisterAudit);
        AtLongRegisterAuditVerboseOff(longRegisterAudit);
        }
    return cAtTrue;
    }

eBool CmdAtDeviceShortRegisterAuditBlackListAdd(char argc, char **argv)
    {
    return ShortRegisterBlackListAction(argc, argv, AtShortRegisterAuditBlackListAdd);
    }

eBool CmdAtDeviceShortRegisterAuditBlackListRemove(char argc, char **argv)
    {
    return ShortRegisterBlackListAction(argc, argv, AtShortRegisterAuditBlackListRemove);
    }

eBool CmdAtDeviceLongRegisterAuditBlackListAdd(char argc, char **argv)
    {
    return LongRegisterBlackListAction(argc, argv, AtLongRegisterAuditBlackListAdd);
    }

eBool CmdAtDeviceLongRegisterAuditBlackListRemove(char argc, char **argv)
    {
    return LongRegisterBlackListAction(argc, argv, AtLongRegisterAuditBlackListRemove);
    }

eBool CmdAtDeviceShortRegisterAuditMask(char argc, char **argv)
    {
    uint8 coreId = 1;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 addressTemp, counter_i;

    if (AtStrcmp(argv[1], "none") == 0)
        {
        GlobalMaskReset();
        }
    /* Parser Mask from Input */
    else if (!StringToMaskGet(argv[1], cShortMaskSizes))
        {
        AtPrintc(cSevCritical, " Can not get mask with input %s \r\n", argv[1]);
        return cAtFalse;
        }

    if (argc > 2)
        numbers = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        step = AtStrtoul(argv[3], NULL, 10);

    if (argc > 4)
        coreId = (uint8) AtStrtoul(argv[4], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    if (coreId == 0)
        {
        AtPrintc(cSevCritical, " numbers must be start from 1!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!AtShortRegisterAuditMaskSet(shortRegisterAudit, addressTemp, (uint8)(coreId - 1), g_register_mask[0]))
            {
            /*AtPrintc(cSevCritical,
                     " can not change mask  address 0x%08x core %d mask 0x%08x\r\n",
                     addressTemp,
                     coreId,
                     g_register_mask[0]);*/
            }
        }
    return cAtTrue;
    }

eBool CmdAtDeviceLongRegisterAuditMask(char argc, char **argv)
    {
    uint8 coreId = 1;
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    eBool success  = cAtTrue;
    uint32 addressTemp;
    uint32 counter_i;

    /* Parser Mask from Input */
    if (!StringToMaskGet(argv[1], cLongMaskSizes))
        {
        AtPrintc(cSevCritical, " Can not get mask with input %s \r\n", argv[argc - 1]);
        return cAtFalse;
        }

    if (argc > 2)
        numbers = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        step = AtStrtoul(argv[3], NULL, 10);

    if (argc > 4)
        coreId = (uint8) AtStrtoul(argv[4], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (!AtLongRegisterAuditMaskSet(longRegisterAudit, addressTemp,  (uint8)(coreId - 1), g_register_mask, cLongRegisterMaxLength))
            {
            AtPrintc(cSevCritical,
                     " can not change mask  address 0x%08x core %d mask %s\r\n",
                     addressTemp,
                     coreId,
                     argv[1]);
            success = cAtFalse;
            }
        }
    return success;
    }

eBool CmdAtDeviceShortRegisterAuditWhileListShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (register_audit_start == 1)
        AtShortRegisterAuditWhiteListShow(shortRegisterAudit);
    return cAtTrue;
    }

eBool CmdAtDeviceLongRegisterAuditWhileListShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (register_audit_start == 1)
        AtLongRegisterAuditWhiteListShow(longRegisterAudit);
    return cAtTrue;
    }

eBool CmdAtDeviceShortRegisterAuditBlackListShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (register_audit_start == 1)
        AtShortRegisterAuditBlackListShow(shortRegisterAudit);
    return cAtTrue;
    }

eBool CmdAtDeviceLongRegisterAuditBlackListShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    if (register_audit_start == 1)
        AtLongRegisterAuditBlackListShow(longRegisterAudit);
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : CliAtSensor.h
 * 
 * Created Date: Dec 16, 2015
 *
 * Description : Physical sensor CLIs.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATSENSOR_H_
#define _CLIATSENSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eBool CliAtSensorInterruptMaskSet(char argc, char **argv,
                                  const char *typeStr[],
                                  const uint32 *typeVal,
                                  uint32 numTypes,
                                  AtSensor (*SensorGet)(void));

eBool CliAtSensorNotificationEnable(char argc, char **argv,
                                    eBool enable,
                                    tAtSensorEventListener *listener,
                                    AtSensor (*SensorGet)(void));

void CliAtSensorAlarmChangeNotify(AtSensor sensor,
                                  void *listener,
                                  uint32 changedAlarms,
                                  uint32 currentAlarms,
                                  const char *alarmTypeStr[],
                                  const uint32 *alarmTypeVal,
                                  uint32 numAlarms);

eBool CliAtSensorInterruptShow(char argc, char** argv,
                               const char *typeStr[],
                               const uint32 *typeVal,
                               uint32 numTypes,
                               AtSensor (*SensorGet)(void));

eBool CliAtSensorAlarmShow(char argc, char** argv,
                           const char *typeStr[],
                           const uint32 *typeVal,
                           uint32 numTypes,
                           AtSensor (*SensorGet)(void));

const char * CliAtSensorAlarmMaskStringGet(AtSensor sensor,
                                           const char *alarmStr[],
                                           const uint32 *alarmVal,
                                           uint32 numAlarms);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _CLIATSENSOR_H_ */


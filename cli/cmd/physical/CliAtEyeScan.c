/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : CliAtEyeScan.c
 *
 * Created Date: Dec 29, 2014
 *
 * Description : Eye scan CLI common implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtEyeScan.h"
#include "AtEyeScanLane.h"

/*--------------------------- Define -----------------------------------------*/
#define EYESCAN_ERROR_ONE_OCTAVE  1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static const char *cAtEyeScanEqualizerModeStr[] = {"dfe", "lpm"};
static const uint32 cAtEyeScanEqualizerModeVal[]={cAtEyeScanEqualizerModeDfe,
                                            cAtEyeScanEqualizerModeLpm
                                            };

static eBool m_silent = cAtFalse;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet AtEyeScanControllerReset(AtEyeScanController self, AtEyeScanLane lane);

/*--------------------------- Implementation ---------------------------------*/
static const char *CharacterForError(uint32 numErrors, uint32 maxErrors)
    {
    if (numErrors == 0)
        return "_";
    if (numErrors == maxErrors)
        return "X";
    if (numErrors >= EYESCAN_ERROR_ONE_OCTAVE)
        return "x";

    return "+";
    }

static eAtSevLevel ColorForError(uint32 numErrors, uint32 maxErrors)
    {
    if (numErrors == 0)
        return cSevInfo;

    if (numErrors == maxErrors)
        return cSevCritical;

    if (numErrors >= EYESCAN_ERROR_ONE_OCTAVE)
        return cSevWarning;

    return cSevInfo;
    }

static void EyeScanDidScanRow(AtEyeScanLane lane, int32 verticalOffset, void *userData)
    {
    AtUnused(userData);
    AtUnused(lane);
    AtUnused(verticalOffset);
    AtPrintf("\r\n");
    }

static void EyeScanWillScanRow(AtEyeScanLane lane, int32 verticalOffset, void *userData)
    {
    int32 voltage = AtEyeScanLaneVerticalOffsetToMillivolt(lane, verticalOffset);
    AtUnused(userData);
    AtPrintc(cSevNormal, "(%4d mV)  ", voltage);
    }

static void EyeScanDidScanPixel(AtEyeScanLane lane,
                                int32 verticalOffset, int32 horizontalOffset,
                                uint32 numErrors, uint32 maxNumErrors, float ber, void *userData)
    {
    AtUnused(lane);
    AtUnused(verticalOffset);
    AtUnused(horizontalOffset);
    AtUnused(ber);
    AtUnused(userData);
    AtPrintc(ColorForError(numErrors, maxNumErrors), "%s", CharacterForError(numErrors, maxNumErrors));
    AtStdFlush();
    }

static float TimeTapPs(AtEyeScanLane lane)
    {
    uint32 numTaps = AtEyeScanLaneNumHorizontalTaps(lane);
    float laneRateGbps = AtEyeScanLaneSpeedInGbps(lane);
    return (float)((1000.0 / laneRateGbps) / (float)numTaps);
    }

static void EyeScanWillStart(AtEyeScanLane lane, void *userData)
    {
    uint32 numTaps = AtEyeScanLaneNumHorizontalTaps(lane);
    float laneRateGbps = AtEyeScanLaneSpeedInGbps(lane);
    float timeTapPs = (float)((1000.0 / laneRateGbps) / (float)numTaps);

    AtUnused(userData);
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "================================================================================\r\n");
    AtPrintc(cSevNormal, "* %s: \r\n", AtObjectToString((AtObject)lane));
    AtPrintc(cSevNormal, "* Horizontal range: %d taps  (%.1f ps, %.4f GHz)\r\n", numTaps, (1000 / laneRateGbps), laneRateGbps);
    AtPrintc(cSevNormal, "  (1 tap = %.3f ps)\r\n", timeTapPs);
    AtPrintc(cSevNormal, "\r\n");
    }

static void EyeScanDidFinish(AtEyeScanLane lane, void *userData)
    {
    uint32 measuredWidth = AtEyeScanLaneMeasuredWidth(lane);
    float time_tap_ps = TimeTapPs(lane);
    uint32 elapsedTimeMs = AtEyeScanLaneElapsedTimeMs(lane);

    AtUnused(userData);
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Measured width: %d(%.1f ps)\r\n", measuredWidth, (float)measuredWidth * time_tap_ps);
    AtPrintc(cSevNormal, "* Measured swing: %d (%d mV)\r\n", AtEyeScanLaneMeasuredSwing(lane), AtEyeScanLaneMeasuredSwingInMilliVolt(lane));
    AtPrintc(cSevNormal, "* Elapsed time  : %.3f s\r\n", (float)elapsedTimeMs / 1000);
    AtPrintc(cSevInfo, "================================================================================\r\n");
    }

static eBool AttributeSet(AtList lanes, uint16 paramValue, AtEyeScanLaneAttributeSetFunc attributeSetFunc)
    {
    eBool success = cAtTrue;
    uint32 lane_i;

    if (AtListLengthGet(lanes) == 0)
        return cAtFalse;

    for (lane_i = 0; lane_i < AtListLengthGet(lanes); lane_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(lanes, lane_i);
        eAtRet ret = attributeSetFunc(lane, paramValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Configure lane %s fail with ret = %s\r\n",
                     AtObjectToString((AtObject)lane), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static void DefaultListenerDeregister(AtEyeScanController controller)
    {
    uint8 lane_i;
    uint8 numLanes = AtEyeScanControllerNumLanes(controller);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        AtEyeScanLaneListenerRemove(AtEyeScanControllerLaneGet(controller, lane_i), CliAtEyeScanLaneDefaultListenerSet(), NULL);
    }

static void DefaultListenerRegister(AtEyeScanController controller)
    {
    uint8 lane_i;
    uint8 numLanes = AtEyeScanControllerNumLanes(controller);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        AtEyeScanLaneListenerAdd(AtEyeScanControllerLaneGet(controller, lane_i), CliAtEyeScanLaneDefaultListenerSet(), NULL);
    }

eBool CliAtEyeScanControllerScan(AtList controllers)
    {
    uint32 controller_i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        return cAtFalse;

    CliAtEyeScanControllerDefaultListenerRegister(controllers);

    while (1)
        {
        eBool completed = cAtTrue;

        for (controller_i = 0; controller_i < AtListLengthGet(controllers);controller_i++)
            {
            AtEyeScanController controller = (AtEyeScanController)AtListObjectGet(controllers, controller_i);
            eAtRet ret = AtEyeScanControllerScan(controller);

            if (!AtEyeScanControllerFinished(controller))
                completed = cAtFalse;

            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Eye-scan fail on %s with ret = %s\r\n",
                         AtObjectToString((AtObject)controller), AtRet2String(ret));
                success = cAtFalse;
                }
            }

        if (completed)
            break;
        }

    CliAtEyeScanDefaultListenerDeregister(controllers);

    return success;
    }

eBool CliAtEyeScanLaneDrpRead(AtEyeScanLane lane, char argc, char **argv)
    {
    uint32 address, value;
    eBool isPretty = cAtFalse;

    /* Get address and pretty option */
    if (argc < 2)
        {
        AtPrintc(cSevCritical, "Address is required\r\n");
        return cAtFalse;
        }
    StrToDw(argv[1], 'h', &address);
    if ((argc >= 3) && (AtStrcmp(argv[2], "pretty") == 0))
        isPretty = cAtTrue;

    /* Show value from address */
    value = AtEyeScanLaneDrpRead(lane, (uint16)address);
    if (isPretty)
        AtCliRegisterPrettyPrint(value);
    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", value);

    return cAtTrue;
    }

eBool CliAtEyeScanLaneDrpWrite(AtEyeScanLane lane, char argc, char **argv)
    {
    uint32 address, value;

    /* Parse address and value */
    if(argc < 3)
        {
        AtPrintc(cSevCritical, "Address and value are required\r\n");
        return cAtFalse;
        }
    StrToDw(argv[1], 'h', &address);
    StrToDw(argv[2], 'h', &value);

    /* Write */
    AtEyeScanLaneDrpWrite(lane, (uint16)address, (uint16)value);

    return cAtTrue;
    }

eBool CliAtEyeScanLaneStepSizeHorizonalSet(AtList lanes, char *valueString)
    {
    return AttributeSet(lanes, (uint16)AtStrToDw(valueString), (AtEyeScanLaneAttributeSetFunc)AtEyeScanLaneHorizontalStepSizeSet);
    }

eBool CliAtEyeScanLaneStepSizeVerticalSet(AtList lanes, char *valueString)
    {
    return AttributeSet(lanes, (uint16)AtStrToDw(valueString), (AtEyeScanLaneAttributeSetFunc)AtEyeScanLaneVerticalStepSizeSet);
    }

eBool CliAtEyeScanLaneMaxPrescaleSet(AtList lanes, char *valueString)
    {
    return AttributeSet(lanes, (uint16)AtStrToDw(valueString), (AtEyeScanLaneAttributeSetFunc)AtEyeScanLaneMaxPrescaleSet);
    }

eBool CliAtEyeScanLaneShow(AtList lanes)
    {
    uint32 lane_i;
    tTab *tabPtr;
    const char *heading[] = {"Lane", "maxHorizonal", "horizonalStep", "verticalStep", "maxPrescale", "dataWidth", "enabled", "equalizerMode"};

    if (AtListLengthGet(lanes) == 0)
        return cAtFalse;

    /* Create table with titles */
    tabPtr = TableAlloc(AtListLengthGet(lanes), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (lane_i = 0; lane_i < AtListLengthGet(lanes); lane_i++)
        {
        const char *stringValue;
        uint32 column = 0;
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(lanes, lane_i);
        AtEyeScanController controller = AtEyeScanLaneControllerGet(lane);
        eBool enabled;

        /* Print list lanesId */
        StrToCell(tabPtr, lane_i, column++, AtObjectToString((AtObject)lane));

        stringValue = CliNumber2String(AtEyeScanLaneMaxHorizontalOffsetGet(lane), "%u");
        StrToCell(tabPtr, lane_i, column++, stringValue);

        /* Horizontal step size value Eye scanning */
        stringValue = CliNumber2String(AtEyeScanLaneHorizontalStepSizeGet(lane), "%u");
        StrToCell(tabPtr, lane_i, column++, stringValue);

        /* Vertical step size value Eye scanning */
        stringValue = CliNumber2String(AtEyeScanLaneVerticalStepSizeGet(lane), "%u");
        StrToCell(tabPtr, lane_i, column++, stringValue);

        /* MaxPrescale value Eye scanning */
        stringValue = CliNumber2String(AtEyeScanLaneMaxPrescaleGet(lane), "%u");
        StrToCell(tabPtr, lane_i, column++, stringValue);

        /* Data width Eye scanning */
        stringValue = CliNumber2String(AtEyeScanLaneDataWidthGet(lane), "%u");
        StrToCell(tabPtr, lane_i, column++, stringValue);

        /* Enabled */
        enabled = AtEyeScanLaneIsEnabled(lane);
        stringValue = CliBoolToString(enabled);
        ColorStrToCell(tabPtr, lane_i, column++, stringValue, enabled ? cSevInfo : cSevCritical);

        /* Equalizer */
        stringValue = CliEnumToString(AtEyeScanControllerEqualizerModeGet(controller),
                                      cAtEyeScanEqualizerModeStr, cAtEyeScanEqualizerModeVal, mCount(cAtEyeScanEqualizerModeVal),
                                      NULL);
        ColorStrToCell(tabPtr, lane_i, column++, stringValue ? stringValue : "error", stringValue ? cSevInfo : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtEyeScanLaneEnable(AtList lanes, eBool enable)
    {
    eBool success = cAtTrue;
    uint32 lane_i;

    if (AtListLengthGet(lanes) == 0)
        return cAtFalse;

    for (lane_i = 0; lane_i < AtListLengthGet(lanes); lane_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(lanes, lane_i);
        eAtRet ret;

        if (enable)
            AtEyeScanLaneListenerAdd(lane, CliAtEyeScanLaneDefaultListenerSet(), NULL);

        ret = AtEyeScanLaneEnable(lane, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s lane for %s, ret = %s\r\n",
                     enable ? "enable" : "disable",
                     AtObjectToString((AtObject)lane), AtRet2String(ret));
            success = cAtFalse;
            }

        if (!enable)
            AtEyeScanLaneListenerRemove(lane, CliAtEyeScanLaneDefaultListenerSet(), NULL);
        }

    return success;
    }

eBool CliAtEyeScanControllerEqualizerSet(AtList controllers, char *valueString)
    {
    eBool convertSuccess;
    eAtEyeScanEqualizerMode equalizerMode = cAtEyeScanEqualizerModeUnknown;
    eBool success = cAtTrue;
    uint32 controller_i;

    equalizerMode = CliStringToEnum(valueString, cAtEyeScanEqualizerModeStr, cAtEyeScanEqualizerModeVal, mCount(cAtEyeScanEqualizerModeVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Equalizer mode, expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtEyeScanEqualizerModeStr, mCount(cAtEyeScanEqualizerModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        {
        AtEyeScanController controller = (AtEyeScanController)AtListObjectGet(controllers, controller_i);
        eAtRet ret = AtEyeScanControllerEqualizerModeSet(controller, equalizerMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Configure controller %s fail with ret = %s\r\n",
                     AtObjectToString((AtObject)controller), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

void CliAtEyeScanControllerSilentScan(eBool silent)
    {
    m_silent = silent;
    }

void CliAtEyeScanDefaultListenerDeregister(AtList controllers)
    {
    uint32 controller_i;

    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        DefaultListenerDeregister((AtEyeScanController)AtListObjectGet(controllers, controller_i));
    }

void CliAtEyeScanControllerDefaultListenerRegister(AtList controllers)
    {
    uint32 controller_i;

    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        DefaultListenerRegister((AtEyeScanController)AtListObjectGet(controllers, controller_i));
    }

tAtEyeScanLaneListener *CliAtEyeScanLaneDefaultListenerSet(void)
    {
    static tAtEyeScanLaneListener m_listener;
    static tAtEyeScanLaneListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.EyeScanWillStart     = EyeScanWillStart;
    m_listener.EyeScanDidFinish     = EyeScanDidFinish;
    m_listener.EyeScanDidScanPixel  = EyeScanDidScanPixel;
    m_listener.EyeScanDidScanRow    = EyeScanDidScanRow;
    m_listener.EyeScanWillScanRow   = EyeScanWillScanRow;
    pListener = &m_listener;

    return pListener;
    }

eBool CliAtEyeScanLaneDebugEnable(AtList lanes, eBool enable)
    {
    return AttributeSet(lanes, enable, (AtEyeScanLaneAttributeSetFunc)AtEyeScanLaneDebugEnable);
    }

void CliAtEyeScanLaneDebug(AtList controllers)
    {
    uint32 controller_i;

    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(controllers, controller_i);
        AtPrintc(cSevInfo, "Lane %s\r\n", AtObjectToString((AtObject)lane));
        AtPrintc(cSevInfo, "======================================\r\n");
        AtEyeScanLaneDebug(lane);
        AtPrintc(cSevInfo, "\r\n\r\n");
        }
    }

eBool CliAtEyeScanControllerDebug(AtList controllers, char *valueString)
    {
    uint32 controller_i;
    eBool success = cAtTrue;

    AtUnused(valueString);

    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        {
        AtEyeScanController controller = (AtEyeScanController)AtListObjectGet(controllers, controller_i);
        eAtRet ret;
        const char *controllerName;

        /* Show name of the controller */
        controllerName = AtObjectToString((AtObject)controller);
        AtPrintc(cSevInfo, "* %s\r\n", controllerName);
        AtPrintc(cSevInfo, "====================================================\r\n");

        /* And show its information */
        ret = AtEyeScanControllerDebug(controller);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Show debug information of controller %s fail with ret = %s\r\n",
                     AtObjectToString((AtObject)controller), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtEyeScanLaneAttributeSet(AtList lanes, uint16 paramValue, AtEyeScanLaneAttributeSetFunc attributeSetFunc)
    {
    return AttributeSet(lanes, paramValue, attributeSetFunc);
    }

eBool CliAtEyeScanLaneReset(AtList lanes)
    {
    uint32 lane_i;
    eBool success = cAtTrue;

    for (lane_i = 0; lane_i < AtListLengthGet(lanes); lane_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(lanes, lane_i);
        AtEyeScanController controller = AtEyeScanLaneControllerGet(lane);
        eAtRet ret;

        ret = AtEyeScanControllerReset(controller, lane);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Reset controller %s fail with ret = %s\r\n",
                     AtObjectToString((AtObject)controller), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

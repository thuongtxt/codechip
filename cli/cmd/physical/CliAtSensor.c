/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : CliAtSensor.c
 *
 * Created Date: Dec 16, 2015
 *
 * Description : Physical sensor CLIs.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtSensor.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

eBool CliAtSensorInterruptMaskSet(char argc, char **argv,
                                  const char *typeStr[],
                                  const uint32 *typeVal,
                                  uint32 numTypes,
                                  AtSensor (*SensorGet)(void))
    {
    eAtRet ret;
    eBool success = cAtTrue;
    uint32 interruptMask = 0, enabledMask;
    AtTokenizer typeParser = AtTokenizerSharedTokenizer(argv[0], "|");
    eBool enabled = CliBoolFromString(argv[1]);
    AtSensor sensor = SensorGet();

    AtUnused(argc);

    /* Parse masks */
    while (AtTokenizerHasNextString(typeParser))
        {
        char *alarmTypeStr = AtTokenizerNextString(typeParser);
        eBool convertSuccess = cAtFalse;
        uint32 alarmMask = CliStringToEnum(alarmTypeStr, typeStr, typeVal, numTypes, &convertSuccess);

        if (!convertSuccess)
            {
            AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", alarmTypeStr);
            CliExpectedValuesPrint(cSevCritical, typeStr, numTypes);
            AtPrintc(cSevCritical, "\r\n");
            return cAtFalse;
            }

        interruptMask = interruptMask | alarmMask;
        }

    enabledMask = enabled ? interruptMask : 0;

    ret = AtSensorInterruptMaskSet(sensor, interruptMask, enabledMask);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: %s notification fail on %s, ret = %s\r\n",
                 enabled ? "Enable" : "Disable",
                 AtObjectToString((AtObject)sensor),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CliAtSensorNotificationEnable(char argc, char **argv,
                                    eBool enable,
                                    tAtSensorEventListener *listener,
                                    AtSensor (*SensorGet)(void))
    {
    eAtRet ret;
    eBool success = cAtTrue;
    AtSensor sensor = SensorGet();

    AtUnused(argc);
    AtUnused(argv);

    if (enable)
        ret = AtSensorEventListenerAdd(sensor, listener, NULL);
    else
        ret = AtSensorEventListenerRemove(sensor, listener);

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Listener %s fail on %s, ret = %s\r\n",
        		 enable ? "add" : "remove",	
                 AtObjectToString((AtObject)sensor),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

void CliAtSensorAlarmChangeNotify(AtSensor sensor,
                                  void *listener,
                                  uint32 changedAlarms,
                                  uint32 currentAlarms,
                                  const char *alarmTypeStr[],
                                  const uint32 *alarmTypeVal,
                                  uint32 numAlarms)
    {
    uint32 alarm_i;
    AtUnused(listener);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "%s [PHY] Alarms changed at %s",
             AtOsalDateTimeInUsGet(),
             AtObjectToString((AtObject)sensor));

    for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
        {
        uint32 alarmType = alarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", alarmTypeStr[alarm_i]);
        if (currentAlarms & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }

    AtPrintc(cSevInfo, "\r\n");
    }

eBool CliAtSensorInterruptShow(char argc, char** argv,
                               const char *alarmStr[],
                               const uint32 *alarmVal,
                               uint32 numAlarms,
                               AtSensor (*SensorGet)(void))
    {
    tTab *tabPtr;
    AtSensor sensor = SensorGet();
    uint32 alarm;
    uint8 column = 0;

    AtUnused(argc);

    if (sensor == NULL)
        {
        AtPrintc(cSevInfo, "No sensor to show.\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(1, numAlarms, alarmStr);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Print alarm status */
    if (CliHistoryReadingModeGet(argv[0]) == cAtHistoryReadingModeReadToClear)
        alarm = AtSensorAlarmHistoryClear(sensor);
    else
        alarm = AtSensorAlarmHistoryGet(sensor);

    for (column = 0; column < numAlarms; column++)
        ColorStrToCell(tabPtr, 0, column, alarm & alarmVal[column] ? "set" : "clear", alarm & alarmVal[column] ? cSevCritical : cSevInfo);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtSensorAlarmShow(char argc, char** argv,
                           const char *alarmStr[],
                           const uint32 *alarmVal,
                           uint32 numAlarms,
                           AtSensor (*SensorGet)(void))
    {
    tTab *tabPtr;
    AtSensor sensor = SensorGet();
    uint32 alarm;
    uint8 column = 0;

    AtUnused(argv);
    AtUnused(argc);

    if (sensor == NULL)
        {
        AtPrintc(cSevInfo, "No sensor to show.\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(1, numAlarms, alarmStr);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Print alarm status */
    alarm = AtSensorAlarmGet(sensor);
    for (column = 0; column < numAlarms; column++)
        ColorStrToCell(tabPtr, 0, column, alarm & alarmVal[column] ? "set" : "clear", alarm & alarmVal[column] ? cSevCritical : cSevInfo);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

const char * CliAtSensorAlarmMaskStringGet(AtSensor sensor,
                                           const char *alarmStr[],
                                           const uint32 *alarmVal,
                                           uint32 numAlarms)
    {
    uint16 i;
    static char alarmString[64];
    uint32 alarms = AtSensorInterruptMaskGet(sensor);

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < numAlarms; i++)
            {
            if (alarms & (alarmVal[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, alarmStr[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : CliAtSerdesController.c
 *
 * Created Date: Aug 17, 2013
 *
 * Description : CLI to control SERDES
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtTokenizer.h"
#include "CliAtSerdesController.h"
#include "AtSerdesManager.h"
#include "AtDevice.h"
#include "CliAtEyeScan.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAlarmToCell(alarms, alarmType) \
    do                                  \
        {                               \
        raise = (alarms & alarmType) ? cAtTrue : cAtFalse; \
        ColorStrToCell(tabPtr, row, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);\
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*CliSerdesControllerParamSetFunc)(AtSerdesController self, uint32 param, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtSerdesParamStr[] =
                                         {
                                         "vod",
                                         "pre-tap",
                                         "first-post-tap",
                                         "second-post-tap",
                                         "rxEqualizationDcGain",
                                         "rxEqualizationControl",
                                         "tx-pre-cursor",
                                         "tx-post-cursor",
                                         "tx-diff-ctrl",
                                         "RXLPMHFOVRDEN",
                                         "RXLPMLFKLOVRDEN",
                                         "RXOSOVRDEN",
                                         "RXLPMOSHOLD",
                                         "RXLPMOSOVRDEN",
                                         "RXLPM_OS_CFG1",
                                         "RXLPMGCHOLD",
                                         "RXLPMGCOVRDEN",
                                         "RXLPM_GC_CFG"
                                         };

static const uint32 cAtSerdesParamVal[]={cAtSerdesParamVod,
                                         cAtSerdesParamPreEmphasisPreTap,
                                         cAtSerdesParamPreEmphasisFirstPostTap,
                                         cAtSerdesParamPreEmphasisSecondPostTap,
                                         cAtSerdesParamRxEqualizationDcGain,
                                         cAtSerdesParamRxEqualizationControl,
                                         cAtSerdesParamTxPreCursor,
                                         cAtSerdesParamTxPostCursor,
                                         cAtSerdesParamTxDiffCtrl,
                                         cAtSerdesParamRXLPMHFOVRDEN,
                                         cAtSerdesParamRXLPMLFKLOVRDEN,
                                         cAtSerdesParamRXOSOVRDEN,
                                         cAtSerdesParamRXLPMOSHOLD,
                                         cAtSerdesParamRXLPMOSOVRDEN,
                                         cAtSerdesParamRXLPM_OS_CFG1,
                                         cAtSerdesParamRXLPMGCHOLD,
                                         cAtSerdesParamRXLPMGCOVRDEN,
                                         cAtSerdesParamRXLPM_GC_CFG
                                         };

static const char *cCmdSerdesControllerAlarmStr[] =
    {
    "los",
    "txfault",
    "rxfault",
    "notaligned",
    "notsync",
    "linkdown"
    };

static const char *cCmdSerdesControllerAlarmUpperStr[] =
    {
    "LOS",
    "TX-Fault",
    "RX-Fault",
    "NotAligned",
    "NotSync",
    "LinkDown"
    };

static const uint32 cCmdSerdesControllerAlarmValue[] =
    {
    cAtSerdesAlarmTypeLos,
    cAtSerdesAlarmTypeTxFault,
    cAtSerdesAlarmTypeRxFault,
    cAtSerdesAlarmTypeNotAligned,
    cAtSerdesAlarmTypeNotSync,
    cAtSerdesAlarmTypeLinkDown
    };

static const char *cCmdSerdesControllerEqualizerModeStr[] = {
                                                             "unknown",
                                                             "dfe",
                                                             "lpm"
    };

static const eAtSerdesEqualizerMode cCmdSerdesControllerEqualizerModeVal[] = {
    cAtSerdesEqualizerModeUnknown,
    cAtSerdesEqualizerModeDfe,
    cAtSerdesEqualizerModeLpm
    };

static const char *cCmdSerdesControllerModeStr[] = {
    "stm0",
    "stm1",
    "stm4",
    "stm16",
    "stm64",
    "10M",
    "100M",
    "2500M",
    "1G",
    "10G",
    "40G",
    "ocn",
    "ge"
    };

static const eAtSerdesMode cCmdSerdesControllerModeVal[] = {
    cAtSerdesModeStm0,
    cAtSerdesModeStm1,
    cAtSerdesModeStm4,
    cAtSerdesModeStm16,
    cAtSerdesModeStm64,
    cAtSerdesModeEth10M,
    cAtSerdesModeEth100M,
    cAtSerdesModeEth2500M,
    cAtSerdesModeEth1G,
    cAtSerdesModeEth10G,
    cAtSerdesModeEth40G,
    cAtSerdesModeOcn,
    cAtSerdesModeGe
    };

static const char *cAtSerdesTimingModeStr[] = {"auto", "lock2data", "lock2ref"};
static const uint32 cAtSerdesTimingModeVal[] = {cAtSerdesTimingModeAuto,
                                                cAtSerdesTimingModeLockToData,
                                                cAtSerdesTimingModeLockToRef};

static const char *cAtSerdesPrbsEngineTypeStr[] = {"framed", "raw"};
static const uint32 cAtSerdesPrbsEngineTypeVal[] = {cAtSerdesPrbsEngineTypeFramed,
                                                    cAtSerdesPrbsEngineTypeRaw};
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UsePortForIdDescription(AtSerdesController serdes)
    {
    /* TODO: use SERDES API to determine */
    AtUnused(serdes);
    return cAtFalse;
    }

static const char *SerdesIdString(AtSerdesController serdes)
    {
    if (UsePortForIdDescription(serdes))
        {
        AtChannel channel = AtSerdesControllerPhysicalPortGet(serdes);
        return CliChannelIdStringGet(channel);
        }

    return AtObjectToString((AtObject)serdes);
    }

static uint32 ParamFromString(const char *paramIdStr)
    {
    eAtSerdesParam param = cAtSerdesParamVod;
    eBool convertSuccess = cAtTrue;

    /* Try to get predefined parameter IDs */
    mAtStrToEnum(cAtSerdesParamStr, cAtSerdesParamVal, paramIdStr, param, convertSuccess);
    if (convertSuccess)
        return param;

    /* Other IDs */
    return AtStrToDw(paramIdStr);
    }

static eBool PhyParamSet(AtList controllers,
                         const char *paramString, const char *paramValueString,
                         uint32 (*ParamFromStringFunc)(const char *paramIdStr),
                         CliSerdesControllerParamSetFunc Set)
    {
    eAtSerdesParam param = ParamFromStringFunc(paramString);
    uint32 value = AtStrToDw(paramValueString);
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to configure\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = Set(controller, param, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set parameter %s with value 0x%x for SERDES %s, ret = %s\r\n",
                     paramString,
                     value,
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool Enable(AtList controllers, eBool enable, eAtRet (*EnableFunc)(AtSerdesController self, eBool enable))
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to configure\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = EnableFunc(controller, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s SERDES %s, ret = %s\r\n",
                     enable ? "enable" : "disable",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static char *SerdesParamGet(AtSerdesController controller, eAtSerdesParam param)
    {
    static char buf[32];

    if (!AtSerdesControllerPhysicalParamIsSupported(controller, param))
        {
        AtSprintf(buf, sAtNotSupported);
        return buf;
        }

    AtSprintf(buf, "0x%x", AtSerdesControllerPhysicalParamGet(controller, param));
    return buf;
    }

static const char *LinkStatusString(eAtSerdesLinkStatus linkStatus)
    {
    switch (linkStatus)
        {
        case cAtSerdesLinkStatusDown        : return "down";
        case cAtSerdesLinkStatusUp          : return "up";
        case cAtSerdesLinkStatusUnknown     : return "unknown";
        case cAtSerdesLinkStatusNotSupported: return sAtNotApplicable;
        default:
            return NULL;
        }
    }

static eAtSevLevel LinkStatusColor(eAtSerdesLinkStatus linkStatus)
    {
    switch (linkStatus)
        {
        case cAtSerdesLinkStatusDown        : return cSevCritical;
        case cAtSerdesLinkStatusUp          : return cSevInfo;
        case cAtSerdesLinkStatusUnknown     : return cSevCritical;
        case cAtSerdesLinkStatusNotSupported: return cSevNormal;
        default:
            return cSevNormal;
        }
    }

static const char * Alarms2String(uint32 alarms)
    {
    uint16 i;
    uint32 bufferSize = 0;
    char *buffer = CliSharedCharBufferGet(&bufferSize);
    uint32 remainBytes = bufferSize;

    /* Initialize string */
    buffer[0] = '\0';
    if (alarms == 0)
        return "None";

    /* There are alarms */
    for (i = 0; i < mCount(cCmdSerdesControllerAlarmValue); i++)
        {
        if (alarms & (cCmdSerdesControllerAlarmValue[i]))
            {
            AtStrncat(buffer, cCmdSerdesControllerAlarmStr[i], remainBytes);
            if (remainBytes > AtStrlen(cCmdSerdesControllerAlarmStr[i]))
                remainBytes = remainBytes - AtStrlen(cCmdSerdesControllerAlarmStr[i]);
            else
                remainBytes = 0;

            AtStrncat(buffer, "|", remainBytes);
            if (remainBytes)
                remainBytes = remainBytes - 1;

            if (remainBytes == 0)
                break;
            }
        }

    /* Remove the last '|' */
    buffer[AtStrlen(buffer) - 1] = '\0';

    return buffer;
    }

static const char* EqualizerMode2String(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)  return "dfe";
    if (mode == cAtSerdesEqualizerModeLpm)  return "lpm";

    return "unknown";
    }

static const char* PowerDown2String(eBool powerDown)
    {
    return powerDown ? "down" : "up";
    }

static void AlarmNotify(AtSerdesController controller,
                        void *listener,
                        uint32 failureType,
                        uint32 currentStatus)
    {
    uint32 alarm_i;
    AtChannel port = AtSerdesControllerPhysicalPortGet(controller);
    const char *moduleName = AtModuleTypeString(AtChannelModuleGet(port));

    AtUnused(listener);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "%s [%s] Alarms changed at SERDES %s with ID %d",
             AtOsalDateTimeInUsGet(),
             moduleName,
             SerdesIdString(controller),
             AtSerdesControllerIdGet(controller));

    for (alarm_i = 0; alarm_i < mCount(cCmdSerdesControllerAlarmValue); alarm_i++)
        {
        uint32 alarmType = cCmdSerdesControllerAlarmValue[alarm_i];
        if ((failureType & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cCmdSerdesControllerAlarmUpperStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }

    AtPrintc(cSevInfo, "\r\n");
    }

static tAtSerdesControllerListener *Listener(void)
    {
    static tAtSerdesControllerListener listener;
    static tAtSerdesControllerListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.AlarmNotify = AlarmNotify;

    pListener = &listener;
    return pListener;
    }

static eBool SerdesControllerEnable(char argc, char **argv, eBool enable)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerEnable(controllers, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

static eBool PowerDown(char argc, char **argv, eBool powerDown)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerPowerDown(controllers, powerDown);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

static void ShowLaneFormatError(void)
    {
    AtPrintc(cSevCritical, "ERROR: Invalid lane ID, expect format: <serdesId>.<laneId>\r\n");
    }

static AtEyeScanLane LaneFromIdString(char *laneString)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(laneString, ".");
    uint32 serdesId, laneId;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(CliDevice());
    AtSerdesController controler;
    AtEyeScanLane lane;

    if (AtTokenizerNumStrings(tokenizer) != 2)
        {
        ShowLaneFormatError();
        return NULL;
        }

    serdesId = AtStrToDw(AtTokenizerNextString(tokenizer)) - 1;
    laneId = AtStrToDw(AtTokenizerNextString(tokenizer)) - 1;
    controler = AtSerdesManagerSerdesControllerGet(serdesManager, (uint8)serdesId);
    lane = AtEyeScanControllerLaneGet(AtSerdesControllerEyeScanControllerGet(controler), (uint8)laneId);
    if (lane == NULL)
        ShowLaneFormatError();

    return lane;
    }

static AtList LanesFromIdString(char *lanesString)
    {
    AtIdParser idParser;
    char idMaxformat[8];
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(CliDevice());
    AtList lanes;

    /* Create ID parser */
    AtSprintf(idMaxformat, "%d.%d", AtSerdesManagerNumSerdesControllers(serdesManager), AtEyeScanControllerMaxNumLanes());
    idParser = AtIdParserNew(lanesString, "1.1", idMaxformat);
    if (idParser == NULL)
        {
        ShowLaneFormatError();
        return NULL;
        }

    /* Have all valid lanes */
    lanes = AtListCreate(0);
    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 serdesId  = AtIdParserNextNumber(idParser);
        uint32 laneId    = AtIdParserNextNumber(idParser);
        AtSerdesController controler = AtSerdesManagerSerdesControllerGet(serdesManager, (uint8)(serdesId - 1));
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(AtSerdesControllerEyeScanControllerGet(controler), (uint8)(laneId - 1));
        if (lane)
            AtListObjectAdd(lanes, (AtObject)lane);
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: Ignore invalid lane %d.%d which does not exist\r\n",
                     serdesId, laneId);
            }
        }

    /* Free resources */
    AtObjectDelete((AtObject)idParser);
    if (AtListLengthGet(lanes) == 0)
        {
        AtObjectDelete((AtObject)lanes);
        lanes = NULL;
        AtPrintc(cSevCritical, "ERROR: No lanes are parsed. The ID may be wrong, expected: <portId>.<laneId>\r\n");
        }

    return lanes;
    }

static eBool EyeScanEnable(char argc, char **argv, eBool enable)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneEnable(lanes, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }

static eBool AttributeSet(char argc, char **argv,
                          eBool (*attributeSetFunc)(AtList lanes, char *valueString))
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool result = attributeSetFunc(lanes, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return result;
    }

static eBool DebugEnable(char argc, char **argv, eBool enable)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneDebugEnable(lanes, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }

static eBool ParamShow(AtList controllers, const char *paramString)
    {
    const char *pHeading[] = {"Port", "SerdesId", "value"};
    tTab *tabPtr;
    uint32 i;
    eAtSerdesParam param = ParamFromString(paramString);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "No SERDES to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(controllers), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, SerdesIdString(controller));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtSerdesControllerIdGet(controller) + 1, "%u"));
        StrToCell(tabPtr, i, column++, SerdesParamGet(controller, param));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool AllParamsShow(AtList controllers)
    {
    const char *pHeading[] = {"Port", "SerdesId",
                              " ", " ", " ", " ", " ", " ",
                              " ", " ", " ", " ", " ", " ",
                              " ", " ", " ", " ", " ", " "};
    tTab *tabPtr;
    const uint8 cStartParamColumn = 2;
    uint32 i, param_i, numDisplayedParams = 0;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "No SERDES to display\r\n");
        return cAtTrue;
        }

    /* Make sure that table can display all of params */
    AtAssert((mCount(pHeading) - 2) >= mCount(cAtSerdesParamVal));

    tabPtr = TableAlloc(AtListLengthGet(controllers), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Builder header with all of supported parameters */
    for (param_i = 0; param_i < mCount(cAtSerdesParamVal); param_i++)
        {
        eAtSerdesParam param = cAtSerdesParamVal[param_i];
        eBool supported = cAtFalse;

        for (i = 0; i < AtListLengthGet(controllers); i++)
            {
            AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);

            if (AtSerdesControllerPhysicalParamIsSupported(controller, param))
                {
                supported = cAtTrue;
                break;
                }
            }

        if (supported)
            {
            const char *paramName = CliEnumToString(param, cAtSerdesParamStr, cAtSerdesParamVal, mCount(cAtSerdesParamVal), NULL);
            uint32 columnId = cStartParamColumn + numDisplayedParams;
            TableColumnHeaderSet(tabPtr, columnId, paramName);
            pHeading[columnId] = paramName;
            numDisplayedParams = numDisplayedParams + 1;
            }
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        uint32 column = 0;

        StrToCell(tabPtr, i, column++, AtObjectToString((AtObject)controller));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtSerdesControllerIdGet(controller) + 1, "%u"));

        for (param_i = 0; param_i < numDisplayedParams; param_i++)
            {
            const char *paramString = pHeading[column];
            eAtSerdesParam param = CliStringToEnum(paramString, cAtSerdesParamStr, cAtSerdesParamVal, mCount(cAtSerdesParamVal), NULL);
            StrToCell(tabPtr, i, column++, SerdesParamGet(controller, param));
            }
        }

    SubTablePrint(tabPtr, 0, AtListLengthGet(controllers) - 1, 0, (numDisplayedParams + 2) - 1);
    TableFree(tabPtr);

    return cAtTrue;
    }

static void SerdesAlarmToTable(tTab *tabPtr, uint32 alarms, uint32 row, uint32 column)
    {
    eBool raise;
    uint32 i = row;

    raise = (alarms & cAtSerdesAlarmTypeLos) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);

    raise = (alarms & cAtSerdesAlarmTypeLinkDown) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);

    raise = (alarms & cAtSerdesAlarmTypeTxFault) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);

    raise = (alarms & cAtSerdesAlarmTypeRxFault) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);

    raise = (alarms & cAtSerdesAlarmTypeNotAligned) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);

    raise = (alarms & cAtSerdesAlarmTypeNotSync) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);
    }

static AtModuleEth ModuleEth(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static uint32 SerdesIdMax(void)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(CliDevice());
    return (serdesManager) ? AtSerdesManagerNumSerdesControllers(serdesManager) : AtModuleEthNumSerdesControllers(ModuleEth());
    }

static AtList LaneSerdesControllersPutToList(char *serdesIdsString, AtList controllers)
    {
    uint32 numLevel = 2;
    uint32 i;
    char idMaxformat[16];
    AtIdParser idParser;
    AtSerdesController serdescontroller, laneSerdescontroller;

    /* Create ID parser */
    AtSprintf(idMaxformat, "%d.%d", SerdesIdMax(), 4);
    idParser = AtIdParserNew(serdesIdsString, "1.1", idMaxformat);
    if (idParser == NULL)
        {
        ShowLaneFormatError();
        return NULL;
        }

    /* Get controllers */
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / numLevel); i++)
        {
        uint32 levels[2];
        CliIdBuild(idParser, levels, (uint8)numLevel);
        serdescontroller = CliSerdesControllerFromId(levels[0] - 1);
        laneSerdescontroller = AtSerdesControllerLaneGet(serdescontroller, levels[1] - 1);
        if (laneSerdescontroller)
            AtListObjectAdd(controllers, (AtObject)laneSerdescontroller);
        }

    AtObjectDelete((AtObject)idParser);
    return controllers;
    }

static AtList SerdesControllersPutToList(char *serdesIdsString, AtList controllers)
    {
    uint32 *serdesIdList;
    uint32 bufferSize, numberOfSerdes;
    uint32 i;

    /* ID list of ports */
    serdesIdList   = CliSharedIdBufferGet(&bufferSize);
    numberOfSerdes = CliIdListFromString(serdesIdsString, serdesIdList, bufferSize);
    if (numberOfSerdes == 0)
        return NULL;

    /* Get controllers */
    for (i = 0; i < numberOfSerdes; i++)
        {
        uint32 serdesId = serdesIdList[i] - 1;
        AtSerdesController controller = CliSerdesControllerFromId(serdesId);
        if (controller)
            AtListObjectAdd(controllers, (AtObject)controller);
        }

    return controllers;
    }

static AtSerdesController SerdesLaneGet(char* idString)
    {
    char idMaxformat[16];
    AtIdParser idParser;
    AtSerdesController serdescontroller;

    /* Create ID parser */
    AtSprintf(idMaxformat, "%d.%d", SerdesIdMax(), 4);
    idParser = AtIdParserNew(idString, "1.1", idMaxformat);
    if ((idParser == NULL) || (AtIdParserNumNumbers(idParser) < 2))
        {
        AtObjectDelete((AtObject)idParser);
        return NULL;
        }

    serdescontroller = CliSerdesControllerFromId(AtIdParserNextNumber(idParser) - 1);
    serdescontroller = AtSerdesControllerLaneGet(serdescontroller, AtIdParserNextNumber(idParser) - 1);
    AtObjectDelete((AtObject)idParser);

    return serdescontroller;
    }

static eBool ThresholdShow(AtList controllers)
    {
    const char *pHeading[] = {"Port", "SerdesId", "AutoResetPpm", "AutoResetPpmMax"};
    tTab *tabPtr;
    uint32 serdes_i;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "No SERDES to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(controllers), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (serdes_i = 0; serdes_i < AtListLengthGet(controllers); serdes_i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, serdes_i);
        uint32 column = 0;

        StrToCell(tabPtr, serdes_i, column++, AtObjectToString((AtObject)controller));
        StrToCell(tabPtr, serdes_i, column++, CliNumber2String(AtSerdesControllerIdGet(controller) + 1, "%u"));

        if (AtSerdesControllerHasAutoResetPpmThreshold(controller))
            {
            StrToCell(tabPtr, serdes_i, column++, CliNumber2String(AtSerdesControllerAutoResetPpmThresholdGet(controller), "%u"));
            StrToCell(tabPtr, serdes_i, column++, CliNumber2String(AtSerdesControllerAutoResetPpmThresholdMax(controller), "%u"));
            }
        else
            {
            StrToCell(tabPtr, serdes_i, column++, sAtNotSupported);
            StrToCell(tabPtr, serdes_i, column++, sAtNotSupported);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

AtList CliAtSerdesControllersList(char *serdesIdsString)
    {
    AtList idStrings = AtCliIdStrListSeparateByComma(serdesIdsString);
    AtIterator iterator = AtListIteratorGet(idStrings);
    uint32 numLevel;
    char *strId;
    AtList controllers = AtListCreate(0);

    AtIteratorRestart(iterator);
    while ((strId = (char*)AtIteratorNext(iterator)) != NULL)
        {
        numLevel = CliAtNumIdLevel(strId);
        if (numLevel == 2)
            LaneSerdesControllersPutToList(strId, controllers);
        else
            SerdesControllersPutToList(strId, controllers);
        }

    AtObjectDelete((AtObject)idStrings);

    /* No controller */
    if (AtListLengthGet(controllers) == 0)
        {
        AtObjectDelete((AtObject)controllers);
        return NULL;
        }

    return controllers;
    }

uint32 CliSerdesControllerAlarmMaskFromString(char *alarmStr)
    {
    return CliMaskFromString(alarmStr, cCmdSerdesControllerAlarmStr, cCmdSerdesControllerAlarmValue, mCount(cCmdSerdesControllerAlarmValue));
    }

eBool CliAtSerdesControllerInit(AtList controllers)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to initialize\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerInit(controller);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot initialize SERDES %s, ret = %s\r\n",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerDebug(AtList controllers)
    {
    uint32 i;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to show debug information\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        AtPrintc(cSevInfo, "* SERDES: %s\r\n", CliSerdesControllerIdString(controller));
        AtSerdesControllerDebug(controller);
        AtPrintc(cSevNormal, "\r\n");
        }

    return cAtTrue;
    }

eBool CliAtSerdesControllerEnable(AtList controllers, eBool enable)
    {
    return Enable(controllers, enable, AtSerdesControllerEnable);
    }

eBool CliAtSerdesControllerPhyParamSet(AtList controllers, const char *paramString, const char *paramValueString)
    {
    return PhyParamSet(controllers,
                       paramString, paramValueString,
                       ParamFromString, (CliSerdesControllerParamSetFunc)AtSerdesControllerPhysicalParamSet);
    }

eBool CliAtSerdesControllersShow(AtList controllers)
    {
    char buf[100];
    const char *pHeading[] = {"Port", "SerdesId", "Mode",
                              "Loopback", "enabled", "pll", "link", "Interrupt Mask",
                              "equalizerMode", "Power", "Timing"};
    tTab *tabPtr;
    uint32 i;
    eBool blResult;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "No SERDES to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(controllers), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eBool boolValue = AtSerdesControllerIsEnabled(controller);
        eBool pllLocked = AtSerdesControllerPllIsLocked(controller);
        uint32 column = 0;
        eAtSerdesLinkStatus linkStatus;
        uint32 intrMask = AtSerdesControllerInterruptMaskGet(controller);
        eAtSerdesMode serdesMode = AtSerdesControllerModeGet(controller);
        eBool controllable = AtSerdesControllerIsControllable(controller);
        const char *stringValue;

        StrToCell(tabPtr, i, column++, AtObjectToString((AtObject)controller));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtSerdesControllerIdGet(controller) + 1, "%u"));

        mAtEnumToStr(cCmdSerdesControllerModeStr,
                     cCmdSerdesControllerModeVal,
                     serdesMode,
                     buf,
                     blResult);
        if ((serdesMode == cAtSerdesModeUnknown) && (!controllable))
            StrToCell(tabPtr, i, column++, sAtNotSupported);
        else
            ColorStrToCell(tabPtr, i, column++, blResult ? buf : "Unknown", blResult ? cSevInfo : cSevWarning);

        if (!AtSerdesControllerIsControllable(controller))
            {
            for (; column < mCount(pHeading); column++)
                StrToCell(tabPtr, i, column, sAtNotSupported);
            continue;
            }

        StrToCell(tabPtr, i, column++, CliLoopbackModeString(AtSerdesControllerLoopbackGet(controller)));
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(boolValue), boolValue ? cSevInfo : cSevCritical);
        ColorStrToCell(tabPtr, i, column++, pllLocked ? "locked" : "not-locked", pllLocked ? cSevInfo : cSevCritical);

        /* Link status */
        linkStatus = AtSerdesControllerLinkStatus(controller);
        ColorStrToCell(tabPtr, i, column++, LinkStatusString(linkStatus), LinkStatusColor(linkStatus));

        StrToCell(tabPtr, i, column++, Alarms2String(intrMask));

        if (AtSerdesControllerEqualizerCanControl(controller))
            StrToCell(tabPtr, i, column++, EqualizerMode2String(AtSerdesControllerEqualizerModeGet(controller)));
        else
            StrToCell(tabPtr, i, column++, sAtNotSupported);

        if (AtSerdesControllerPowerCanControl(controller))
            {
            boolValue = AtSerdesControllerPowerIsDown(controller);
            ColorStrToCell(tabPtr, i, column++, PowerDown2String(boolValue), boolValue ? cSevCritical : cSevInfo);
            }
        else
            StrToCell(tabPtr, i, column++, sAtNotSupported);

        if (AtSerdesControllerCanControlTimingMode(controller))
            {
            stringValue = CliAtSerdesTimingModeString(AtSerdesControllerTimingModeGet(controller));
            StrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error");
            }
        else
            StrToCell(tabPtr, i, column++, sAtNotSupported);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtSerdesControllersParamShow(AtList controllers, const char *paramString)
    {
    if ((paramString == NULL) || (AtStrcmp(paramString, "all") == 0))
        return AllParamsShow(controllers);

    return ParamShow(controllers, paramString);
    }

eBool CliAtSerdesControllerLoopback(AtList controllers, const char *loopbackModeStr)
    {
    uint32 i;
    eBool success = cAtTrue;
    eAtLoopbackMode loopbackMode = CliLoopbackModeFromString(loopbackModeStr);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to loopback\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerLoopbackSet(controller, loopbackMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot loopback SERDES %s, ret = %s\r\n",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllersAlarmShow(AtList controllers, uint32 (*GetAlarmFunc)(AtSerdesController), eBool silent)
    {
    const char *pHeading[] = {"Port",
                              "SerdesId",
                              "LOS",
                              "Link Down",
                              "TxFault",
                              "RxFault",
                              "NotAligned",
                              "NotSync",
                              };

    tTab *tabPtr = NULL;
    uint32 controller_i, numLanes, row;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "No SERDES to display\r\n");
        return cAtTrue;
        }

    /* Create table */
    if (!silent)
        {
        tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    row = 0;
    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        {
        uint32 lane_i;
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, controller_i);
        uint32 alarms = GetAlarmFunc(controller);
        uint32 column = 0;

        /* Show SERDES itself */
        TableAddRow(tabPtr, 1);
        StrToCell(tabPtr, row, column++, AtObjectToString((AtObject)controller));
        StrToCell(tabPtr, row, column++, CliSerdesControllerIdString(controller));
        SerdesAlarmToTable(tabPtr, alarms, row, column);

        /* Show all of its sub lanes if any */
        numLanes = AtSerdesControllerNumLanes(controller);
        for (lane_i = 0; lane_i < numLanes; lane_i++)
            {
            AtSerdesController lane = AtSerdesControllerLaneGet(controller, lane_i);

            if (lane == NULL)
                continue;

            column = 0;
            TableAddRow(tabPtr, 1);
            row = row + 1;
            alarms = GetAlarmFunc(lane);
            StrToCell(tabPtr, row, column++, AtObjectToString((AtObject)lane));
            StrToCell(tabPtr, row, column++, CliSerdesControllerIdString(lane));
            SerdesAlarmToTable(tabPtr, alarms, row, column);
            }

        row = row + 1;
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtSerdesControllerInterruptMaskSet(AtList controllers, uint32 alarmMask, eBool enable)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to initialize\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerInterruptMaskSet(controller, alarmMask, (enable) ? alarmMask : 0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set interrupt mask SERDES for %s, ret = %s\r\n",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerNotificationEnable(AtList controllers, eBool enable)
    {
    eAtRet ret;
    uint32 i;
    eBool success = cAtTrue;
    tAtSerdesControllerListener *listener = Listener();

    for (i = 0; i < AtListLengthGet(controllers); i++)
         {
         AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);

        if (enable)
            ret = AtSerdesControllerListenerAdd(controller, NULL, listener);
        else
            ret = AtSerdesControllerListenerRemove(controller, NULL, listener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot install SERDES listener for %s, ret = %s\r\n",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerReset(AtList controllers)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to reset\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerReset(controller);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot reset SERDES %s, ret = %s\r\n",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerRxReset(AtList controllers)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to reset\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerRxReset(controller);
        if (ret != cAtOk)
            {
            AtChannel port = AtSerdesControllerPhysicalPortGet(controller);
            AtPrintc(cSevCritical,
                     "ERROR: Cannot reset Rx SERDES %s, ret = %s\r\n",
                     CliChannelIdStringGet(port),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerTxReset(AtList controllers)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to reset\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerTxReset(controller);
        if (ret != cAtOk)
            {
            AtChannel port = AtSerdesControllerPhysicalPortGet(controller);
            AtPrintc(cSevCritical,
                     "ERROR: Cannot reset Tx SERDES %s, ret = %s\r\n",
                     CliChannelIdStringGet(port),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerEqualizerModeSet(AtList controllers, char argc, char **argv)
    {
    uint32 i;
    eBool success = cAtTrue;
    eAtSerdesEqualizerMode mode = cAtSerdesEqualizerModeUnknown;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    mAtStrToEnum(cCmdSerdesControllerEqualizerModeStr, cCmdSerdesControllerEqualizerModeVal, argv[1], mode, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Can not get equalizer mode\r\n");
        return cAtFalse;
        }

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to reset\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerEqualizerModeSet(controller, mode);
        if (ret != cAtOk)
           {
           AtPrintc(cSevCritical,
                    "ERROR: Cannot reset SERDES %s, ret = %s\r\n",
                    SerdesIdString(controller),
                    AtRet2String(ret));
           success = cAtFalse;
           }
        }

    return success;
    }

eBool CliAtSerdesControllerPowerDown(AtList controllers, eBool powerDown)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to control power\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerPowerDown(controller, powerDown);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot power %s of SERDES %s, ret = %s\r\n",
                     powerDown ? "down" : "up",
                     SerdesIdString(controller),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSerdesControllerTimingModeSet(AtList controllers, char *timingModeStr)
    {
    uint32 numSerdes, serdes_i;
    eBool convertSuccess;
    eAtSerdesTimingMode serdesTiming = cAtSerdesTimingModeAuto;
    eBool success = cAtTrue;

    /* Timing mode */
    mAtStrToEnum(cAtSerdesTimingModeStr,
                 cAtSerdesTimingModeVal,
                 timingModeStr,
                 serdesTiming,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid timing mode , expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtSerdesTimingModeStr, mCount(cAtSerdesTimingModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    numSerdes = AtListLengthGet(controllers);
    if (numSerdes == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No SERDES controllers\r\n");
        return cAtTrue;
        }

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, serdes_i);
        eAtRet ret = AtSerdesControllerTimingModeSet(controller, serdesTiming);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set timing mode for SERDES %s fail with ret = %s\r\n", SerdesIdString(controller), AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

const char *CliAtSerdesTimingModeString(eAtSerdesTimingMode timingMode)
    {
    return CliEnumToString(timingMode,
                           cAtSerdesTimingModeStr,
                           cAtSerdesTimingModeVal,
                           mCount(cAtSerdesTimingModeVal),
                           NULL);
    }

AtList CliAtSerdesEyeScanControllersFromString(char *serdesIdsString)
    {
    uint32 *serdesIdList;
    AtList controllers;
    uint32 bufferSize, numberOfSerdes;
    uint32 i;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(CliDevice());

    if (serdesManager == NULL)
        {
        AtPrintc(cSevCritical, "SERDES manager is not supported\r\n");
        return cAtFalse;
        }

    /* ID list of ports */
    serdesIdList     = CliSharedIdBufferGet(&bufferSize);
    numberOfSerdes = CliIdListFromString(serdesIdsString, serdesIdList, bufferSize);
    if (numberOfSerdes == 0)
        return NULL;

    /* Get controllers */
    controllers = AtListCreate(0);
    for (i = 0; i < numberOfSerdes; i++)
        {
        uint32 serdesId = serdesIdList[i] - 1;
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(serdesManager, serdesId);
        AtEyeScanController controler = AtSerdesControllerEyeScanControllerGet(serdes);
        if (controler)
            AtListObjectAdd(controllers, (AtObject)controler);
        }

    /* No controller */
    if (AtListLengthGet(controllers) == 0)
        {
        AtObjectDelete((AtObject)controllers);
        return NULL;
        }

    return controllers;
    }

eBool CmdAtSerdesControllerModeSet(char argc, char **argv)
    {
    uint32 i;
    eBool success = cAtTrue;
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eAtSerdesMode mode = cAtSerdesModeUnknown;
    AtUnused(argc);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to configure\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtTrue;
        }

    mAtStrToEnum(cCmdSerdesControllerModeStr, cCmdSerdesControllerModeVal, argv[1], mode, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Can not get SERDES mode\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerModeSet(controller, mode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure mode %s for SERDES %u, ret = %s\r\n",
                     argv[1],
                     AtSerdesControllerIdGet(controller) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllersShow(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerEyeScanLaneEnable(char argc, char **argv)
    {
    return EyeScanEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerEyeScanLaneDisable(char argc, char **argv)
    {
    return EyeScanEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerInit(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerInit(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerReset(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerRxReset(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerRxReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerTxReset(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerTxReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerEnable(char argc, char **argv)
    {
    return SerdesControllerEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerDisable(char argc, char **argv)
    {
    return SerdesControllerEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerPhyParamSet(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerPhyParamSet(controllers, argv[1], argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerEqualizerModeSet(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerEqualizerModeSet(controllers, argc, argv);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerPowerUp(char argc, char **argv)
    {
    return PowerDown(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerPowerDown(char argc, char **argv)
    {
    return PowerDown(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerInterruptMask(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success;
    uint32 intrMask = CliSerdesControllerAlarmMaskFromString(argv[1]);
    eBool enable;
    AtUnused(argc);

    mAtStrToBool(argv[2], enable, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    success = CliAtSerdesControllerInterruptMaskSet(controllers, intrMask, enable);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSerdesControllerNotficationEnable(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerNotificationEnable(controllers, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerNotficationDisable(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerNotificationEnable(controllers, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerLoopback(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerLoopback(controllers, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerEyeScanDebug(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesEyeScanControllersFromString(argv[0]);
    eBool success = CliAtEyeScanControllerDebug(controllers, NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerParamShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllersParamShow(controllers, (argc < 2) ? NULL : argv[1]);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSerdesControllerEyeScanLaneDrpRead(char argc, char **argv)
    {
    return CliAtEyeScanLaneDrpRead(LaneFromIdString(argv[0]), argc, argv);
    }

eBool CmdAtSerdesControllerEyeScanLaneDrpWrite(char argc, char **argv)
    {
    return CliAtEyeScanLaneDrpWrite(LaneFromIdString(argv[0]), argc, argv);
    }

eBool CmdAtSerdesCntrollerEyeScanLaneStepSizeHorizonalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneStepSizeHorizonalSet);
    }

eBool CmdAtSerdesControllerEyeScanLaneStepSizeVerticalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneStepSizeVerticalSet);
    }

eBool CmdAtSerdesControllerEyeScanLaneMaxPrescaleSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneMaxPrescaleSet);
    }

eBool CmdAtSerdesControllerEyeScanLaneDebugEnable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerEyeScanLaneDebugDisable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerEyeScanLaneShow(char argc, char **argv)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool result = CliAtEyeScanLaneShow(lanes);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return result;
    }

eBool CmdAtSerdesControllerEyeScan(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesEyeScanControllersFromString(argv[0]);
    eBool success = CliAtEyeScanControllerScan(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerAlarmShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerAlarmGet, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSerdesControllerAlarmHistoryShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success;
    eAtHistoryReadingMode readingMode;
    uint32 (*AlarmGet)(AtSerdesController);
    eBool silent = cAtFalse;
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (readingMode == cAtHistoryReadingModeReadOnly)
        AlarmGet = AtSerdesControllerAlarmHistoryGet;
    else
        AlarmGet = AtSerdesControllerAlarmHistoryClear;

    success = CliAtSerdesControllersAlarmShow(controllers, AlarmGet, silent);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

static void PutSerdesInfoToTableRow(tTab *tabPtr, uint32 row, AtSerdesController serdesController)
    {
    const char* description = AtSerdesControllerDescription(serdesController);
    StrToCell(tabPtr, row, 0, CliSerdesControllerIdString(serdesController));
    StrToCell(tabPtr, row, 1, description);
    if (AtSerdesControllerDrp(serdesController))
        ColorStrToCell(tabPtr, row, 2, "Yes", cSevInfo);
    else
        ColorStrToCell(tabPtr, row, 2, "No", cSevWarning);

    if (AtSerdesControllerMdio(serdesController))
        ColorStrToCell(tabPtr, row, 3, "Yes", cSevInfo);
    else
        ColorStrToCell(tabPtr, row, 3, "No", cSevWarning);
    }

eBool CmdAtShowSerdesManager(char argc, char **argv)
    {
    tTab *tabPtr = NULL;
    uint32 numberOfSerdes, serdesId, row_i;
    const char *heading[] = {"SerdesId", "Description", "Has DRP", "Has MDIO"};
    AtUnused(argc);
    AtUnused(argv);

    numberOfSerdes = CliMaxNumSerdesController();
    tabPtr = TableAlloc(numberOfSerdes, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    row_i = 0;
    for (serdesId = 0; serdesId < numberOfSerdes; serdesId++)
        {
        AtSerdesController serdesController = CliSerdesControllerFromId(serdesId);
        uint32 numLanes, lane_i;

        PutSerdesInfoToTableRow(tabPtr, row_i++, serdesController);
        numLanes = AtSerdesControllerNumLanes(serdesController);
        for (lane_i = 0; lane_i < numLanes; lane_i++)
            {
            AtSerdesController serdesLane = AtSerdesControllerLaneGet(serdesController, lane_i);

            TableAddRow(tabPtr, 1);
            PutSerdesInfoToTableRow(tabPtr, row_i++, serdesLane);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSerdesControllerTimingModeSet(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerTimingModeSet(controllers, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerAutoResetPpmThresholdSet(char argc, char **argv)
    {
    uint32 serdes_i;
    eBool success = cAtTrue;
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    uint32 threshold;

    AtUnused(argc);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to configure\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtTrue;
        }

    threshold = AtStrToDw(argv[1]);

    for (serdes_i = 0; serdes_i < AtListLengthGet(controllers); serdes_i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, serdes_i);
        eAtRet ret = AtSerdesControllerAutoResetPpmThresholdSet(controller, threshold);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Configure PPM threshold SERDES %s fail with ret = %s\r\n",
                 AtObjectToString((AtObject)controller),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerThresholdShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = ThresholdShow(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

AtSerdesController CliSerdesControllerFromId(uint32 serdesId)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(CliDevice());
    if (serdesManager)
        return AtSerdesManagerSerdesControllerGet(serdesManager, serdesId);

    return AtModuleEthSerdesController(ModuleEth(), serdesId);
    }

AtSerdesController CliSerdesControllerFromString(char* idString)
    {
    uint32 numLevel = CliAtNumIdLevel(idString);
    uint32 serdesId;

    if (numLevel == 2)
        return SerdesLaneGet(idString);

    serdesId = AtStrToDw(idString);
    if (serdesId == 0)
        return NULL;

    return CliSerdesControllerFromId(serdesId - 1);
    }

uint32 CliMaxNumSerdesController(void)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(CliDevice());
    return (serdesManager) ? AtSerdesManagerNumSerdesControllers(serdesManager) : AtModuleEthNumSerdesControllers(ModuleEth());
    }

const char* CliSerdesControllerDescriptionWithIdString(AtSerdesController controller)
    {
    static char idString[32];
    AtSnprintf(idString, 32, "%s.%u", AtSerdesControllerDescription(controller), AtSerdesControllerIdGet(controller) + 1);
    return idString;
    }

const char* CliSerdesControllerIdString(AtSerdesController controller)
    {
    return AtSerdesControllerIdString(controller);
    }

AtList CliAtEyeScanLanesFromIdString(char *lanesString)
    {
    return LanesFromIdString(lanesString);
    }

eBool CmdAtSerdesControllerPrbsEngineTypeSet(char argc, char **argv)
    {
    uint32 i;
    eBool success = cAtTrue;
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eAtSerdesPrbsEngineType prbsType = cAtSerdesPrbsEngineTypeFramed;

    AtUnused(argc);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to configure\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtTrue;
        }

    mAtStrToEnum(cAtSerdesPrbsEngineTypeStr, cAtSerdesPrbsEngineTypeVal, argv[1], prbsType, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Can not get SERDES mode\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtSerdesControllerPrbsEngineTypeSet(controller, prbsType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure prbs engine type %s for SERDES %u, ret = %s\r\n",
                     argv[1],
                     AtSerdesControllerIdGet(controller) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerEyeScanLaneReset(char argc, char **argv)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneReset(lanes);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }


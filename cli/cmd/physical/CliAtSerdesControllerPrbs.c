/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SERDES
 *
 * File        : CliAtSerdesControllerPrbs.c
 *
 * Created Date: Sep 7, 2016
 *
 * Description : SERDES PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../prbs/CliAtPrbsEngine.h"
#include "../physical/CliAtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 i;
    AtList engines = AtListCreate(0);
    AtList serdesControllers = CliAtSerdesControllersList(idString);

    for (i = 0; i < AtListLengthGet(serdesControllers); i++)
        {
        AtSerdesController serdes = (AtSerdesController)AtListObjectGet(serdesControllers, i);
        AtPrbsEngine engine = AtSerdesControllerPrbsEngine(serdes);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    AtObjectDelete((AtObject)serdesControllers);

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eAtDirection direction;
    eBool success = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc == 1)
        {
        success = CliAtPrbsEngineEnable(engines, enable);
        AtObjectDelete((AtObject)engines);
        return success;
        }

    direction = CliDirectionFromStr(argv[1]);

    switch(direction)
        {
        case cAtDirectionTx:
            success = CliAtPrbsEngineTxEnable(engines, enable);
            break;
        case cAtDirectionRx:
            success = CliAtPrbsEngineRxEnable(engines, enable);
            break;
        case cAtDirectionAll:
            success = CliAtPrbsEngineEnable(engines, enable);
            break;

        case cAtDirectionInvalid:
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;

        default:
            break;
        }

    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eAtDirection direction;
    eBool success = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc == 2)
        {
        success = CliAtPrbsEngineModeSet(engines, argv[1]);
        AtObjectDelete((AtObject)engines);
        return success;
        }

    direction = CliDirectionFromStr(argv[2]);

    switch(direction)
        {
        case cAtDirectionTx:
            success = CliAtPrbsEngineTxModeSet(engines, argv[1]);
            break;
        case cAtDirectionRx:
            success = CliAtPrbsEngineRxModeSet(engines, argv[1]);
            break;
        case cAtDirectionAll:
            success = CliAtPrbsEngineModeSet(engines, argv[1]);
            break;

        case cAtDirectionInvalid:
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;

        default:
            break;
        }

    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerPrbsEngineInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerPrbsEngineErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerPrbsEngineErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerPrbsEngineEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtSerdesControllerPrbsEngineDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtSerdesControllerPrbsEngineCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc >= 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineDebug(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDebug(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineSerdesShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineShow(engines, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsEngineDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtSerdesControllerPrbsGatingShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineGatingShow(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);

    return success;
    }

eBool CmdAtShowSerdesPrbsEngineAlarmHistory(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineAlarmHistoryShow(engines, argv[1], (argc == 3) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

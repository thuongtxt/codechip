/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : CliAtEyeScan.h
 * 
 * Created Date: Dec 29, 2014
 *
 * Description : Eye scan CLI common implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATEYESCAN_H_
#define _CLIATEYESCAN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEyeScanController.h"
#include "AtEyeScanLane.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtRet (*AtEyeScanLaneAttributeSetFunc)(AtEyeScanLane self, uint16 paramValue);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtEyeScanControllerScan(AtList controllers);
eBool CliAtEyeScanLaneDrpRead(AtEyeScanLane lane, char argc, char **argv);
eBool CliAtEyeScanLaneDrpWrite(AtEyeScanLane lane, char argc, char **argv);
eBool CliAtEyeScanLaneStepSizeHorizonalSet(AtList lanes, char *valueString);
eBool CliAtEyeScanLaneStepSizeVerticalSet(AtList lanes, char *valueString);
eBool CliAtEyeScanLaneMaxPrescaleSet(AtList lanes, char *valueString);
eBool CliAtEyeScanLaneShow(AtList lanes);
eBool CliAtEyeScanLaneEnable(AtList lanes, eBool enable);
eBool CliAtEyeScanControllerEqualizerSet(AtList lanes, char *valueString);
eBool CliAtEyeScanLaneAttributeSet(AtList lanes, uint16 paramValue, AtEyeScanLaneAttributeSetFunc attributeSetFunc);
eBool CliAtEyeScanLaneReset(AtList lanes);

/* Default listener */
tAtEyeScanLaneListener *CliAtEyeScanLaneDefaultListenerSet(void);
void CliAtEyeScanControllerSilentScan(eBool silent);
void CliAtEyeScanControllerDefaultListenerRegister(AtList controllers);
void CliAtEyeScanDefaultListenerDeregister(AtList controllers);

/* Parsing */
AtList CliAtEthPortSerdesEyeScanControllerFromIdString(char *idString);
AtList CliAtSdhLineSerdesEyeScanControllersFromIdString(char *idString);

/* Debugging */
eBool CliAtEyeScanLaneDebugEnable(AtList lanes, eBool enable);
void CliAtEyeScanLaneDebug(AtList controllers);
eBool CliAtEyeScanControllerDebug(AtList controllers, char *valueString);

#endif /* _CLIATEYESCAN_H_ */


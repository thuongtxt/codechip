/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtSerdesController.h
 * 
 * Created Date: Aug 17, 2013
 *
 * Description : CLI to control SERDES
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATSERDESCONTROLLER_H_
#define _CLIATSERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSerdesController.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtSerdesControllerInit(AtList controllers);
eBool CliAtSerdesControllerReset(AtList controllers);
eBool CliAtSerdesControllerRxReset(AtList controllers);
eBool CliAtSerdesControllerTxReset(AtList controllers);
eBool CliAtSerdesControllerDebug(AtList controllers);
eBool CliAtSerdesControllerEnable(AtList controllers, eBool enable);
eBool CliAtSerdesControllerPhyParamSet(AtList controllers, const char *paramString, const char *paramValueString);
eBool CliAtSerdesControllersShow(AtList controllers);
eBool CliAtSerdesControllersAlarmShow(AtList controllers, uint32 (*GetAlarmFunc)(AtSerdesController), eBool silent);
eBool CliAtSerdesControllersParamShow(AtList controllers, const char *paramString);
eBool CliAtSerdesControllerLoopback(AtList controllers, const char *loopbackModeStr);
eBool CLiAtSerdesControllerEyeMonitorEnable(AtList controllers, eBool enable);
eBool CliAtSerdesControllerEyeMonitorParamSet(AtList controllers, const char *paramString, const char *paramValueString);
eBool CliAtSerdesControllerEyeMonitorShow(AtList controllers);
uint32 CliSerdesControllerAlarmMaskFromString(char *alarmStr);
eBool CliAtSerdesControllerInterruptMaskSet(AtList controllers, uint32 alarmMask, eBool enable);
eBool CliAtSerdesControllerNotificationEnable(AtList controllers, eBool enable);
eBool CliAtSerdesControllerEqualizerModeSet(AtList controllers, char argc, char **argv);
eBool CliAtSerdesControllerPowerDown(AtList controllers, eBool powerDown);
eBool CliAtSerdesControllerTimingModeSet(AtList controllers, char *timingModeStr);
const char *CliAtSerdesTimingModeString(eAtSerdesTimingMode timingMode);
AtList CliAtSerdesControllersList(char *serdesIdsString);
AtSerdesController CliSerdesControllerFromId(uint32 serdesId);
AtSerdesController CliSerdesControllerFromString(char* idString);
uint32 CliMaxNumSerdesController(void);
const char* CliSerdesControllerDescriptionWithIdString(AtSerdesController controller);
const char* CliSerdesControllerIdString(AtSerdesController controller);
AtList CliAtSerdesEyeScanControllersFromString(char *serdesIdsString);
AtList CliAtEyeScanLanesFromIdString(char *lanesString);

#endif /* _CLIATSERDESCONTROLLER_H_ */


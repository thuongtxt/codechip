/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : CliAtVcg.c
 *
 * Created Date: Jun 4, 2014
 *
 * Description : VCG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleConcate.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/
static const char *cCmdEncapTypeStr[] =
    {
    "hdlc", "atm", "gfp"
    };

static const uint32 cCmdEncapTypeVal[] =
    {
    cAtEncapHdlc, cAtEncapAtm, cAtEncapGfp
    };

/*static*/ const char *cAtVcgAlarmTypeStr[] =
    {
    "plct",
    "tlct",
    "plcr",
    "tlcr",
    "sqnc",
    "loa"
    };

static const uint32 cAtVcgAlarmTypeVal[] =
    {
    cAtVcgAlarmTypePlcT,
    cAtVcgAlarmTypeTlcT,
    cAtVcgAlarmTypePlcR,
    cAtVcgAlarmTypeTlcR,
    cAtVcgAlarmTypeSqnc,
    cAtVcgAlarmTypeLoa
    };

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtModuleConcateRet (*IntegerAttributeSetFunc)(AtVcg _self, uint32 value);
typedef AtConcateMember (*ProvisionFunc)(AtConcateGroup self, AtChannel channel);
typedef eAtModuleConcateRet (*DeProvisionFunc)(AtConcateGroup self, AtChannel channel);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtList m_vcgList = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TextUIDidStop(AtTextUI self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtObjectDelete((AtObject)m_vcgList);
    m_vcgList = NULL;
    }

static void RegisterTextUIListener(void)
    {
    static tAtTextUIListerner *pListener = NULL;
    static tAtTextUIListerner listener;

    if (pListener)
        return;

    listener.WillStop = NULL;
    listener.DidStop  = TextUIDidStop;
    pListener = &listener;
    AtTextUIListenerAdd(AtCliSharedTextUI(), pListener, NULL);
    }

static eBool ShouldRecreateVcgList(void)
    {
    if (m_vcgList == NULL)
        return cAtTrue;
    return (AtListCapacityGet(m_vcgList) < AtModuleConcateMaxGroupGet(CliAtConcateModule())) ? cAtTrue : cAtFalse;
    }

static AtList VcgList(void)
    {
    if (!ShouldRecreateVcgList())
        return m_vcgList;

    AtObjectDelete((AtObject)m_vcgList);
    m_vcgList = AtListCreate(AtModuleConcateMaxGroupGet(CliAtConcateModule()));

    RegisterTextUIListener();
    return m_vcgList;
    }

static eBool VcatLcasVcgIntegerAttributeSet(char *vcgIdsString, uint32 value, IntegerAttributeSetFunc attributeSetFunc)
    {
    uint32 numberOfVcg, vcg_i;
    uint32 *vcgIdList;
    eBool success = cAtTrue;

    /* Get list of VCGs */
    vcgIdList   = CliSharedIdBufferGet(&numberOfVcg);
    numberOfVcg = CliIdListFromString(vcgIdsString, vcgIdList, numberOfVcg);
    if (numberOfVcg == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of VCGs may be wrong\r\n");
        return cAtFalse;
        }

    for (vcg_i = 0; vcg_i < numberOfVcg; vcg_i++)
        {
        eAtRet ret;
        AtVcg vcg = (AtVcg)AtModuleConcateGroupGet(CliAtConcateModule(), vcgIdList[vcg_i] - 1);
        if (vcg == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d has not been created, it is ignored\r\n", vcgIdList[vcg_i]);
            continue;
            }

        if (AtConcateGroupConcatTypeGet((AtConcateGroup)vcg) != cAtConcateGroupTypeVcat)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d is not VCAT/LCAS\r\n", vcgIdList[vcg_i]);
            continue;
            }

        ret = attributeSetFunc(vcg, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot change hold-off timer of VCG %d, ret = %s\r\n",
                     vcgIdList[vcg_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static char *TimerToString(uint32 timer)
    {
    return CliNumber2String(timer, "%d");
    }

static eAtRet MemberProvision(AtConcateGroup self, AtChannel channel, ProvisionFunc provisionFunc)
    {
    AtConcateMember member = provisionFunc(self, channel);

    if (member == NULL)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Provision member %s to VCG %d fail\r\n",
                 CliChannelIdStringGet(channel),
                 AtChannelIdGet((AtChannel)self) + 1);
        return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

static eAtRet MemberDeProvision(AtConcateGroup self, AtChannel channel, DeProvisionFunc deProvisionFunc)
    {
    eAtRet ret = deProvisionFunc(self, channel);

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Deprovision member %s out of VCG %d fail, ret = %s\r\n",
                 CliChannelIdStringGet(channel),
                 AtChannelIdGet((AtChannel)self) + 1,
                 AtRet2String(ret));
        }

    return ret;
    }

static eBool AlarmShow(char *vcgIdsString, uint32 (*AlarmGet)(AtChannel self))
    {
    tTab *table;
    const char *heading[] ={"VcgId", "PLCT", "TLCT", "PLCR", "TLCR", "SQNC", "LOA"};
    AtList vcgs = CliAtVcgVcgsFromString(vcgIdsString);
    uint32 i;

    if (AtListLengthGet(vcgs) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: all of input VCG IDs may be invalid or all of them have not been created yet\r\n");
        return cAtTrue;
        }

    table = TableAlloc(AtListLengthGet(vcgs), mCount(heading), heading);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(vcgs); i++)
        {
        uint8 column = 0;
        AtChannel vcg = (AtChannel) AtListObjectGet(vcgs, i);
        uint32 alarm = AlarmGet(vcg);
        uint32 j;

        StrToCell(table, i, column++, (char *)CliChannelIdStringGet(vcg));
        for (j = 0; j < mCount(cAtVcgAlarmTypeVal); j++)
            CliChannelAlarmPrintToCell(vcg, alarm, cAtVcgAlarmTypeVal[j], table, i, column++);
        }

    TablePrint(table);
    TableFree(table);

    return cAtTrue;
    }

static AtConcateMember MemberSourceSinkProvision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember newMember;

    newMember = AtConcateGroupSourceMemberProvision(self, channel);
    if (newMember == NULL)
        return NULL;

    newMember = AtConcateGroupSinkMemberProvision(self, channel);
    if (newMember == NULL)
        return NULL;

    return newMember;
    }

static eAtModuleConcateRet MemberSourceSinkDeProvision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret = cAtOk;

    ret |= AtConcateGroupSourceMemberDeProvision(self, channel);
    ret |= AtConcateGroupSinkMemberDeProvision(self, channel);

    return ret;
    }

eAtModuleConcateRet CliAtVcgLcasSourceSinkMemberAdd(AtVcg vcg, AtConcateMember member)
    {
    eAtRet ret = cAtOk;

    ret |= AtVcgSinkMemberAdd(vcg, member);
    ret |= AtVcgSourceMemberAdd(vcg, member);

    return ret;
    }

eAtModuleConcateRet CliAtVcgLcasSourceSinkMemberRemove(AtVcg vcg, AtConcateMember member)
    {
    eAtRet ret = cAtOk;

    ret |= AtVcgSinkMemberRemove(vcg, member);
    ret |= AtVcgSourceMemberRemove(vcg, member);

    return ret;
    }

eBool CliAtVcgLcasEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret;
    uint32 numVcgs;
    uint32 i;
    uint32 *vcgIdList;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of VCGs */
    vcgIdList = CliSharedIdBufferGet(&numVcgs);
    numVcgs   = CliIdListFromString(argv[0], vcgIdList, numVcgs);
    if (numVcgs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of VCGs may be wrong\r\n");
        return cAtFalse;
        }

    /* Enable/disable all VCGs */
    for (i = 0; i < numVcgs; i++)
        {
        AtVcg vcg = (AtVcg)AtModuleConcateGroupGet((AtModuleConcate)CliAtConcateModule(), vcgIdList[i] - 1);
        if (vcg == NULL)
            {
            AtPrintc(cSevWarning,
                     "WARNING: VCG %d has not been created and it is ignored.\r\n",
                     vcgIdList[i]);
            continue;
            }

        if (AtConcateGroupConcatTypeGet((AtConcateGroup)vcg) != cAtConcateGroupTypeVcat)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d is not VCAT/LCAS, it is ignored\r\n", vcgIdList[i]);
            continue;
            }

        ret = AtVcgLcasEnable(vcg, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s LCAS on VCG %d, ret = %s",
                     enable ? "enable" : "disable", vcgIdList[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtVcgLcasMemberAddRemove(char *vcgString, char *memberIdsString, MemberAddRemoveFunc memberAddRemoveFunc, MemberGetByChannelFunc memberGetByChannel)
    {
    eAtRet ret;
    uint32 numberOfMember;
    uint32 member_i;
    AtChannel *channelList;
    AtVcg vcg;
    eBool success = cAtTrue;

    /* Get VCG */
    vcg = (AtVcg)AtModuleConcateGroupGet((AtModuleConcate)CliAtConcateModule(), AtStrToDw(vcgString) - 1);
    if(vcg == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: VCG %s does not exist\r\n", vcgString);
        return cAtFalse;
        }

    /* And list of members */
    channelList    = (AtChannel*) CliSharedIdBufferGet(&numberOfMember);
    numberOfMember = CliChannelListFromString(memberIdsString, channelList, numberOfMember);

    /* Add these members */
    for (member_i = 0; member_i < numberOfMember; member_i++)
        {
        /* Member may have not been provisioned yet */
        AtConcateMember member = memberGetByChannel((AtConcateGroup)vcg, channelList[member_i]);
        if(member == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Member %s has not been provisioned to VCG. It is ignored.\r\n", AtChannelIdString(channelList[member_i]));
            continue;
            }

        /* It is provisioned, just add it */
        ret = memberAddRemoveFunc(vcg, member);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Add/remove member %s fail, ret = %s\r\n",
                     AtChannelIdString(channelList[member_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool MemberSequenceSet(char *vcgString,
                               char *memberIdsString,
                               SequenceSetFunc sequenceFunc,
                               char *sequenceString,
                               MemberGetByChannelFunc memberGetByChannel)
    {
    eAtRet ret;
    uint32 numMembers;
    uint32 member_i;
    AtChannel *channelList;
    AtVcg vcg;
    eBool success = cAtTrue;
    uint32 *sequences = NULL;
    uint32 numSequences;

    /* Get VCG */
    vcg = (AtVcg)AtModuleConcateGroupGet((AtModuleConcate)CliAtConcateModule(), AtStrToDw(vcgString) - 1);
    if(vcg == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: VCG %s does not exist\r\n", vcgString);
        return cAtFalse;
        }

    /* And list of members */
    channelList    = (AtChannel*) CliSharedChannelListGet(&numMembers);
    numMembers = CliChannelListFromString(memberIdsString, channelList, numMembers);

    /* Get list of sequence */
    sequences = CliSharedIdBufferGet(&numSequences);
    numSequences = CliIdListFromString(sequenceString, sequences, numSequences);
    if (numMembers != numSequences)
        {
        AtPrintc(cSevCritical, "ERROR: Number of members and sequences must be equal\r\n");
        return cAtFalse;
        }

    /* Apply */
    for (member_i = 0; member_i < numMembers; member_i++)
        {
        /* Member may have not been provisioned yet */
        AtConcateMember member = memberGetByChannel((AtConcateGroup)vcg, channelList[member_i]);
        if(member == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Member %s has not been provisioned to VCG. It is ignored.\r\n", AtChannelIdString(channelList[member_i]));
            continue;
            }

        /* It is provisioned, just set sequence for it */
        ret = sequenceFunc(member, sequences[member_i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Set sequence for members %s fail, ret = %s\r\n",
                     AtChannelIdString(channelList[member_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtVcgIsLcasVcg(AtConcateGroup group)
    {
    if (AtConcateGroupConcatTypeGet(group) != cAtConcateGroupTypeVcat)
        return cAtFalse;
    return AtVcgLcasIsEnabled((AtVcg)group);
    }

eBool CliAtVcgIsVcatVcg(AtConcateGroup group)
    {
    if (AtConcateGroupConcatTypeGet(group) == cAtConcateGroupTypeVcat)
        return cAtTrue;

    return cAtFalse;
    }

static eBool VcgMemberProvision(char argc, char **argv,
                                ProvisionFunc provisionFunc,
                                DeProvisionFunc deProvisionFunc)
    {
    uint32 bufferSize;
    uint32 numberOfMember;
    uint32 i;
    uint32 vcgId;
    AtChannel *channelList;
    AtConcateGroup group;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get VCG */
    vcgId = AtStrToDw(argv[0]) - 1;
    group   = AtModuleConcateGroupGet((AtModuleConcate)CliAtConcateModule(), vcgId);
    if (group == NULL)
        {
        AtPrintc(cSevCritical, "VCG %s is invalid or it has not been created yet\r\n", argv[0]);
        return cAtFalse;
        }

    /* Get list of members */
    channelList    = (AtChannel*)CliSharedIdBufferGet(&bufferSize);
    numberOfMember = CliChannelListFromString(argv[1], channelList, bufferSize);
    if(numberOfMember == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of channels may be invalid\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfMember; i++)
        {
        eAtRet ret = cAtOk;

        if (provisionFunc != NULL)
            ret = MemberProvision(group, channelList[i], provisionFunc);
        else
            ret = MemberDeProvision(group, channelList[i], deProvisionFunc);

        if (ret != cAtOk)
            success = cAtFalse;
        }

    return success;
    }

AtList CliAtVcgVcgsFromString(char *vcgIdsString)
    {
    uint32 numberOfVcg, vcg_i;
    uint32 *vcgIdList;
    AtList vcgs = VcgList();

    /* Get list of VCGs */
    vcgIdList   = CliSharedIdBufferGet(&numberOfVcg);
    numberOfVcg = CliIdListFromString(vcgIdsString, vcgIdList, numberOfVcg);
    if (numberOfVcg == 0)
        return NULL;

    AtListFlush(vcgs);
    for (vcg_i = 0; vcg_i < numberOfVcg; vcg_i++)
        {
        AtConcateGroup group = AtModuleConcateGroupGet(CliAtConcateModule(), vcgIdList[vcg_i] - 1);
        if (group)
            AtListObjectAdd(vcgs, (AtObject)group);
        }

    return vcgs;
    }

eBool CmdVcgVcatLcasEnable(char argc, char **argv)
    {
    return CliAtVcgLcasEnable(argc, argv, cAtTrue);
    }

eBool CmdVcgVcatLcasDisable(char argc, char **argv)
    {
    return CliAtVcgLcasEnable(argc, argv, cAtFalse);
    }

eBool CmdVcgVcatLcasHoldOffTimerSet(char argc, char **argv)
    {
    AtUnused(argc);
    return VcatLcasVcgIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), AtVcgHoldOffTimerSet);
    }

eBool CmdVcgLcasWtrSet(char argc, char **argv)
    {
    AtUnused(argc);
    return VcatLcasVcgIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), AtVcgWtrTimerSet);
    }

eBool CmdVcgVcatLcasRmvTimerSet(char argc, char **argv)
    {
    AtUnused(argc);
    return VcatLcasVcgIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), AtVcgRmvTimerSet);
    }

eBool CmdVcgLcasMemberSourceAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtVcgLcasMemberAddRemove(argv[0], argv[1], AtVcgSourceMemberAdd, AtConcateGroupSourceMemberGetByChannel);
    }

eBool CmdVcgLcasMemberSinkAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtVcgLcasMemberAddRemove(argv[0], argv[1], AtVcgSinkMemberAdd, AtConcateGroupSinkMemberGetByChannel);
    }

eBool CmdVcgLcasSourceSinkMemberAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtVcgLcasMemberAddRemove(argv[0], argv[1], CliAtVcgLcasSourceSinkMemberAdd, AtConcateGroupSourceMemberGetByChannel);
    }

eBool CmdVcgLcasMemberSourceRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtVcgLcasMemberAddRemove(argv[0], argv[1], AtVcgSourceMemberRemove, AtConcateGroupSourceMemberGetByChannel);
    }

eBool CmdVcgLcasMemberSinkRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtVcgLcasMemberAddRemove(argv[0], argv[1], AtVcgSinkMemberRemove, AtConcateGroupSinkMemberGetByChannel);
    }

eBool CmdVcgLcasSourceSinkMemberRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtVcgLcasMemberAddRemove(argv[0], argv[1], CliAtVcgLcasSourceSinkMemberRemove, AtConcateGroupSinkMemberGetByChannel);
    }

eBool CmdVcgShow(char argc, char **argv)
    {
    tTab *table;
    const char *heading[] ={"VcgId", "SoNumMembers", "SkNumMembers", "HoldOff(ms)", "WTR(ms)", "RMV(ms)", "So RsAck", "Sk RsAck", "Encap"};
    AtList vcgs = CliAtVcgVcgsFromString(argv[0]);
    uint32 i;

    AtUnused(argc);

    if (AtListLengthGet(vcgs) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: all of input VCG IDs may be invalid or all of them have not been created yet\r\n");
        return cAtTrue;
        }

    table = TableAlloc(AtListLengthGet(vcgs), mCount(heading), heading);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(vcgs); i++)
        {
        AtConcateGroup group = (AtConcateGroup)AtListObjectGet(vcgs, i);
        uint8 column = 0;
        const char *stringVal;

        StrToCell(table, i, column++, (char *)CliChannelIdStringGet((AtChannel)group));
        StrToCell(table, i, column++, CliNumber2String(AtConcateGroupNumSourceMembersGet(group), "%d"));
        StrToCell(table, i, column++, CliNumber2String(AtConcateGroupNumSinkMembersGet(group), "%d"));

        /* LCAS information */
        if (CliAtVcgIsLcasVcg(group))
            {
            AtVcg vcg = (AtVcg)group;
            uint8 ack;

            /* Times */
            StrToCell(table, i, column++, TimerToString(AtVcgHoldOffTimerGet(vcg)));
            StrToCell(table, i, column++, TimerToString(AtVcgWtrTimerGet(vcg)));
            StrToCell(table, i, column++, TimerToString(AtVcgRmvTimerGet(vcg)));

            /* RS-ACK */
            ack = AtVcgSourceRsAckGet(vcg);
            ColorStrToCell(table, i, column++, CliNumber2String(ack, "%d"), ack ? cSevInfo : cSevNormal);
            ack = AtVcgSinkRsAckGet(vcg);
            ColorStrToCell(table, i, column++, CliNumber2String(ack, "%d"), ack ? cSevInfo : cSevNormal);
            }

        /* LCAS information is not applicable for other kinds of VCG */
        else
            {
            StrToCell(table, i, column++, sAtNotApplicable);
            StrToCell(table, i, column++, sAtNotApplicable);
            StrToCell(table, i, column++, sAtNotApplicable);
            StrToCell(table, i, column++, sAtNotApplicable);
            StrToCell(table, i, column++, sAtNotApplicable);
            }

        stringVal = CliEnumToString(AtConcateGroupEncapTypeGet(group), cCmdEncapTypeStr, cCmdEncapTypeVal, mCount(cCmdEncapTypeVal), NULL);
        StrToCell(table, i, column++, stringVal ? stringVal : "Error");
        }

    TablePrint(table);
    TableFree(table);

    return cAtTrue;
    }

eBool CmdVcgAlarmShow(char argc, char **argv)
    {
    AtUnused(argc);
    return AlarmShow(argv[0], AtChannelDefectGet);
    }

eBool CmdVcgInterruptShow(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode = CliHistoryReadingModeGet(argv[1]);

    AtUnused(argc);

    if (readingMode == cAtHistoryReadingModeReadOnly)
        return AlarmShow(argv[0], AtChannelDefectInterruptGet);
    if (readingMode == cAtHistoryReadingModeReadToClear)
        return AlarmShow(argv[0], AtChannelDefectInterruptClear);

    AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
    return cAtFalse;
    }

eBool CmdVcgMemberProvision(char argc, char **argv)
    {
    return VcgMemberProvision(argc, argv, (ProvisionFunc)MemberSourceSinkProvision, NULL);
    }

eBool CmdVcgMemberDeProvision(char argc, char **argv)
    {
    return VcgMemberProvision(argc, argv, NULL, (DeProvisionFunc)MemberSourceSinkDeProvision);
    }

eBool CmdVcgSourceMemberProvision(char argc, char **argv)
    {
    return VcgMemberProvision(argc, argv, AtConcateGroupSourceMemberProvision, NULL);
    }

eBool CmdVcgSinkMemberProvision(char argc, char **argv)
    {
    return VcgMemberProvision(argc, argv, AtConcateGroupSinkMemberProvision, NULL);
    }

eBool CmdVcgSourceMemberDeProvision(char argc, char **argv)
    {
    return VcgMemberProvision(argc, argv, NULL, AtConcateGroupSourceMemberDeProvision);
    }

eBool CmdVcgSinkMemberDeProvision(char argc, char **argv)
    {
    return VcgMemberProvision(argc, argv, NULL, AtConcateGroupSinkMemberDeProvision);
    }

eBool CmdVcgVcatMemberSourceSequenceSet(char argc, char **argv)
    {
    AtUnused(argc);
    return MemberSequenceSet(argv[0], argv[1], AtConcateMemberSourceSequenceSet, argv[2], AtConcateGroupSourceMemberGetByChannel);
    }

eBool CmdVcgVcatMemberSinkSequenceSet(char argc, char **argv)
    {
    AtUnused(argc);
    return MemberSequenceSet(argv[0], argv[1], AtConcateMemberSinkExpectedSequenceSet, argv[2], AtConcateGroupSinkMemberGetByChannel);
    }

eBool CliAtVcgEncapTypeSet(char argc, char **argv)
    {
    uint32 encapType;
    uint32 numVcgs, vcg_i;
    AtList vcgs;
    eBool success;

    AtUnused(argc);

    vcgs = CliAtVcgVcgsFromString(argv[0]);
    numVcgs = AtListLengthGet(vcgs);
    if (numVcgs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <vcgList> , expected 1-%d\r\n", AtModuleConcateMaxGroupGet(CliAtConcateModule()));
        return cAtFalse;
        }

    /* Frame type */
    mAtStrToEnum(cCmdEncapTypeStr,
    		     cCmdEncapTypeVal,
                 argv[1],
				 encapType,
                 success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid encap-type. Expected: ");
        CliExpectedValuesPrint(cSevCritical, cCmdEncapTypeStr, mCount(cCmdEncapTypeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (vcg_i = 0; vcg_i < numVcgs; vcg_i++)
        {
		AtConcateGroup vcg = (AtConcateGroup) AtListObjectGet(vcgs, vcg_i);
		eAtRet ret = AtConcateGroupEncapTypeSet(vcg, encapType);
        if (ret != cAtOk)
			{
        	AtPrintc(cSevWarning, "ERROR: Change encapsulation for %s fail with ret = %s\r\n", CliChannelIdStringGet((AtChannel)vcg), AtRet2String(ret));
        	success = cAtFalse;
			}
        }

    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : CliAtModuleMap.c
 *
 * Created Date: Jun 4, 2014
 *
 * Description : CLIs of MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cCmdVcgConcatTypeStr[] =
                                        {
                                        "vcat",
                                        "ccat",
                                        "nonvcat",
                                        "nonvcat_g804",
                                        "nonvcat_g8040",
                                        };

static uint32 cCmdVcgConcatTypeVal[] = {
                                       cAtConcateGroupTypeVcat,
                                       cAtConcateGroupTypeCcat,
                                       cAtConcateGroupTypeNVcat,
                                       cAtConcateGroupTypeNVcat_g804,
                                       cAtConcateGroupTypeNVcat_g8040,
                                       };

static const char *cAtConcateMemberTypeStr[] = {
                                     "vc4_64c",
                                     "vc4_16c",
                                     "vc4_4c",
                                     "vc4_nc",
                                     "vc4",
                                     "vc3",
                                     "vc12",
                                     "vc11",
                                     "ds1",
                                     "e1",
                                     "ds3",
                                     "e3"
};

static uint32 cAtConcateMemberTypeVal[] = {
                                       cAtConcateMemberTypeVc4_64c,
                                       cAtConcateMemberTypeVc4_16c,
                                       cAtConcateMemberTypeVc4_4c,
                                       cAtConcateMemberTypeVc4_nc,
                                       cAtConcateMemberTypeVc4,
                                       cAtConcateMemberTypeVc3,
                                       cAtConcateMemberTypeVc12,
                                       cAtConcateMemberTypeVc11,
                                       cAtConcateMemberTypeDs1,
                                       cAtConcateMemberTypeE1,
                                       cAtConcateMemberTypeDs3,
                                       cAtConcateMemberTypeE3
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void PutThresholdToTable(uint32 resolution, uint32 thresholdInRes, tTab *table, uint32 row, uint32 colum)
    {
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);
    AtSprintf(buffer, "%8u", (thresholdInRes * resolution));
    StrToCell(table, row, colum, buffer);
    }

static eBool DeskewLatencyThresholdShow(char argc, char **argv)
    {
    tTab *table;
    uint32 numRows, row, column, numColumns;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    numRows = 1;
    numColumns = mCount(cAtConcateMemberTypeStr);
    table = TableAlloc(numRows, numColumns, cAtConcateMemberTypeStr);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    row = 0;
    for (column = 0; column < numColumns; column++)
        {
        eAtConcateMemberType memberType = cAtConcateMemberTypeVal[column];

        if (AtModuleConcateMemberTypeIsSupported(CliAtConcateModule(), memberType))
            {
            uint32 resolution = AtModuleConcateDeskewLatencyResolutionInFrames(CliAtConcateModule(), memberType);
            uint32 thresholdInRes = AtModuleConcateDeskewLatencyThresholdGet(CliAtConcateModule(), memberType);
            PutThresholdToTable(resolution, thresholdInRes, table, row, column);
            }
        else
            StrToCell(table, row, column, sAtNotApplicable);
        }

    TablePrint(table);
    TableFree(table);

    return cAtTrue;
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliAtConcateModule(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

AtModuleConcate CliAtConcateModule(void)
    {
    return (AtModuleConcate)AtDeviceModuleGet(CliDevice(), cAtModuleConcate);
    }

const char *CliAtConcateGroupType2String(eAtConcateGroupType concateType)
    {
    eBool success = cAtFalse;
    const char *concateTypeString = CliEnumToString(concateType, cCmdVcgConcatTypeStr, cCmdVcgConcatTypeVal, mCount(cCmdVcgConcatTypeVal), &success);
    return success ? concateTypeString : NULL;
    }

void CliAtModuleConcatePutAlarm2Cell(tTab* table, uint32 row, uint32 column, uint32 alarm, uint32 alarmType)
    {
    uint32 raise = alarm & alarmType;
    ColorStrToCell(table, row, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);
    }

eBool CmdVcgCreate(char argc, char **argv)
    {
    uint32 numVcgs;
    uint32 i;
    uint32 *vcgIdList;
    eAtConcateGroupType concatType;
    AtModuleConcate mapModule = CliAtConcateModule();
    eBool convertSuccess, success = cAtTrue;
    eAtConcateMemberType memberType = cAtConcateMemberTypeAny;

    /* Get a list of VCGs */
    vcgIdList = CliSharedIdBufferGet(&numVcgs);
    numVcgs   = CliIdListFromString(argv[0], vcgIdList, numVcgs);
    if (numVcgs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of VCGs may be wrong.\r\n");
        return cAtFalse;
        }

    /* Parse concatenation type */
    concatType = CliStringToEnum(argv[1], cCmdVcgConcatTypeStr, cCmdVcgConcatTypeVal, mCount(cCmdVcgConcatTypeVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid concatenation. Expected: ");
        CliExpectedValuesPrint(cSevCritical, cCmdVcgConcatTypeStr, mCount(cCmdVcgConcatTypeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    /* Parse member type */
    if (argc > 2)
        {
        memberType = CliStringToEnum(argv[2], cAtConcateMemberTypeStr, cAtConcateMemberTypeVal, mCount(cAtConcateMemberTypeVal), &convertSuccess);
        if (!convertSuccess)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid member type. Expect: ");
            CliExpectedValuesPrint(cSevCritical, cAtConcateMemberTypeStr, mCount(cAtConcateMemberTypeVal));
            AtPrintc(cSevCritical, "\r\n");
            return cAtFalse;
            }
        }

    /* Create all of them */
    for (i = 0; i < numVcgs; i++)
        {
        if (AtModuleConcateGroupCreate(mapModule,  vcgIdList[i] - 1, memberType, concatType) == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot create VCG %d, it may already exist or concatenation type is not supported\r\n",
                     vcgIdList[i]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdVcgDelete(char argc, char **argv)
    {
    uint32 bufferSize;
    uint32 i;
    eBool success = cAtTrue;
    uint32 *vcgIdList  = CliSharedIdBufferGet(&bufferSize);
    uint32 numberOfVcg = CliIdListFromString(argv[0], vcgIdList, bufferSize);

    AtUnused(argc);

    if (numberOfVcg == 0 )
        {
        AtPrintc(cSevCritical, "ERROR: List of VCGs may be wrong.\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfVcg; i++)
        {
        eAtRet ret = AtModuleConcateGroupDelete(CliAtConcateModule(), vcgIdList[i] - 1);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Delete VCG %d fail, ret = %s\r\n", vcgIdList[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdVcgDeskewLatencyThresholdSet(char argc, char **argv)
    {
    eAtRet ret;
    uint32 thresholdInRes;
    eBool convertSuccess;
    eAtConcateMemberType memberType = cAtConcateMemberTypeAny;

    AtUnused(argc);

    /* Get Member Type */
    memberType = CliStringToEnum(argv[0], cAtConcateMemberTypeStr, cAtConcateMemberTypeVal, mCount(cAtConcateMemberTypeVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid member type. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtConcateMemberTypeStr, mCount(cAtConcateMemberTypeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    /* Get Threshold Value */
    thresholdInRes = (uint32)AtStrtoul(argv[1], NULL, 10);

    /* Call API */
    ret = AtModuleConcateDeskewLatencyThresholdSet(CliAtConcateModule(), memberType, thresholdInRes);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical,
             "ERROR: Set Deskew Latency Threshold is fail, memberType %s thresholdInRes %s, ret = %s\r\n",
             argv[0], argv[1],
             AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdVcgDeskewLatencyThresholdShow(char argc, char **argv)
    {
    eBool success = DeskewLatencyThresholdShow(argc, argv);
    AtPrintc(cSevInfo, "\r\nNote: Unit is number of frame\r\n");
    return success;
    }

eBool CmdAtModuleConcateInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleConcateInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleConcateShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleConcate moduleConcate = CliAtConcateModule();
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)moduleConcate);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : CliAtConcateMember.c
 *
 * Created Date: Jun 4, 2014
 *
 * Description : VCG Member CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "CliAtModuleConcate.h"
#include "../common/CliAtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#define cColorCodeIdle    35
#define cColorCodeAdd     cSevWarning
#define cColorCodeDnu     cSevCritical
#define cColorCodeNormal  cSevInfo
#define cColorCodeEos     cSevInfo
#define cColorCodeRmv     cSevWarning
#define cColorCodeFixed   cSevNormal
#define cColorCodeInvalid cSevCritical

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtConcateMember* (*CliConcateMembersGet)(char *, char *, uint32 *);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtConcateMemberAlarmStr[] =
    {
     "crc",
     "lom",
     "gidm",
     "sqm",
     "mnd",
     "tsf",
     "tsd",
     "msu",
     "sqnc",
     "loa",
     "oom",
     "sink-state-change",
     "source-state-change"
    };

static const uint32 cAtConcateMemberAlarmValue[]=
    {
     cAtConcateMemberAlarmTypeCrc,
     cAtConcateMemberAlarmTypeLom,
     cAtConcateMemberAlarmTypeGidM,
     cAtConcateMemberAlarmTypeSqm,
     cAtConcateMemberAlarmTypeMnd,
     cAtConcateMemberAlarmTypeTsf,
     cAtConcateMemberAlarmTypeTsd,
     cAtConcateMemberAlarmTypeMsu,
     cAtConcateMemberAlarmTypeSqnc,
     cAtConcateMemberAlarmTypeLoa,
     cAtConcateMemberAlarmTypeOom,
     cAtConcateMemberAlarmTypeSinkStateChange,
     cAtConcateMemberAlarmTypeSourceStateChange
    };

static const char *cAtConcateMemberAlarmUpperStr[] =
    {
     "CRC",
     "LOM",
     "GIDM",
     "SQM",
     "MND",
     "TSF",
     "TSD",
     "MSU",
     "SQNC",
     "LOA",
     "OOM",
     "SK-STATE",
     "SO-STATE"
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 ColorForSourceState(eAtLcasMemberSourceState state)
    {
    switch (state)
        {
        case cAtLcasMemberSourceStateIdle  : return cColorCodeIdle;
        case cAtLcasMemberSourceStateAdd   : return cColorCodeAdd;
        case cAtLcasMemberSourceStateDnu   : return cColorCodeDnu;
        case cAtLcasMemberSourceStateNorm  : return cColorCodeNormal;
        case cAtLcasMemberSourceStateEos   : return cColorCodeEos;
        case cAtLcasMemberSourceStateRmv   : return cColorCodeRmv;
        case cAtLcasMemberSourceStateFixed : return cColorCodeFixed;
        case cAtLcasMemberSourceStateInvl  : return cColorCodeInvalid;
        default                            : return cColorCodeInvalid;
        }
    }

static uint8 ColorForSinkState(eAtLcasMemberSinkState state)
    {
    switch (state)
        {
        case cAtLcasMemberSinkStateIdle : return cColorCodeIdle;
        case cAtLcasMemberSinkStateFail : return cSevCritical;
        case cAtLcasMemberSinkStateOk   : return cSevInfo;
        case cAtLcasMemberSinkStateFixed: return cColorCodeFixed;
        case cAtLcasMemberSinkStateInvl : return cColorCodeInvalid;
        default                         : return cColorCodeInvalid;
        }
    }

static uint8 ColorForCtrl(eAtLcasMemberCtrl ctrl)
    {
    switch (ctrl)
        {
        case cAtLcasMemberCtrlFixed: return cColorCodeFixed;
        case cAtLcasMemberCtrlAdd  : return cColorCodeAdd;
        case cAtLcasMemberCtrlNorm : return cColorCodeNormal;
        case cAtLcasMemberCtrlEos  : return cColorCodeEos;
        case cAtLcasMemberCtrlIdle : return cColorCodeIdle;
        case cAtLcasMemberCtrlDnu  : return cColorCodeDnu;
        case cAtLcasMemberCtrlInvl : return cColorCodeInvalid;
        default:
            return cSevCritical;
        }
    }

static void PutAlarmToTable(tTab *table, uint32 row, uint32 colum, AtChannel channel, uint32 alarm, uint32 alarmType)
    {
    if (AtChannelAlarmIsSupported(channel, alarmType))
        CliAtModuleConcatePutAlarm2Cell(table, row, colum, alarm, alarmType);
    else
        StrToCell(table, row, colum, sAtNotApplicable);
    }

static AtConcateMember *CliConcateMembersFromString(char *vcgIdsString, char *memberIdsString,
                                                    uint32 *numMembers,
                                                    uint32 (*NumMembersGet)(AtConcateGroup),
                                                    AtConcateMember (*MemberGetByIndex)(AtConcateGroup, uint32),
                                                    AtConcateMember (*MemberGetByChannel)(AtConcateGroup, AtChannel))
    {
    uint32 bufferSize;
    AtConcateMember *memberList = (AtConcateMember*)CliSharedObjectListGet(&bufferSize);
    AtList vcgList = CliAtVcgVcgsFromString(vcgIdsString);
    AtChannel *channelList;
    uint32 group_i;
    uint32 numberOfMembers = 0;
    AtConcateMember member;

    if (AtListLengthGet(vcgList) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: all of input VCG IDs may be invalid or all of them have not been created yet\r\n");
        *numMembers = 0;
        return NULL;
        }

    if (memberIdsString == NULL)
        {
        uint32 member_i;

        for (group_i = 0; group_i < AtListLengthGet(vcgList); group_i++)
            {
            AtConcateGroup group = (AtConcateGroup)AtListObjectGet(vcgList, group_i);
            for (member_i = 0; member_i < NumMembersGet(group); member_i++)
                {
                member = MemberGetByIndex(group, member_i);
                if (member)
                    memberList[numberOfMembers++] = member;
                }
            }

        *numMembers = numberOfMembers;
        return memberList;
        }

    /* Get member list with member string */
    channelList = CliSharedChannelListGet(&bufferSize);
    bufferSize = CliChannelListFromString(memberIdsString, channelList, bufferSize);
    if (bufferSize == 0)
        {
        AtPrintc(cSevWarning, "WARNING: all of input member IDs may be invalid or all of them have not been provisioned yet\r\n");
        *numMembers = 0;
        return NULL;
        }

    for (group_i = 0; group_i < AtListLengthGet(vcgList); group_i++)
        {
        AtConcateGroup group = (AtConcateGroup)AtListObjectGet(vcgList, group_i);
        uint32 channel_i;

        for (channel_i = 0; channel_i < bufferSize; channel_i++)
            {
            member = MemberGetByChannel(group, channelList[channel_i]);
            if (member)
                memberList[numberOfMembers++] = member;
            }
        }

    *numMembers = numberOfMembers;
    return memberList;
    }

static AtConcateMember *CliSinkConcateMembersFromString(char *vcgIdsString, char *memberIdsString, uint32 *numMembers)
    {
    return CliConcateMembersFromString(vcgIdsString, memberIdsString, numMembers,
                                       AtConcateGroupNumSinkMembersGet,
                                       AtConcateGroupSinkMemberGetByIndex,
                                       AtConcateGroupSinkMemberGetByChannel);
    }

static AtConcateMember *CliSourceConcateMembersFromString(char *vcgIdsString, char *memberIdsString, uint32 *numMembers)
    {
    return CliConcateMembersFromString(vcgIdsString, memberIdsString, numMembers,
                                       AtConcateGroupNumSourceMembersGet,
                                       AtConcateGroupSourceMemberGetByIndex,
                                       AtConcateGroupSourceMemberGetByChannel);
    }

static eBool VcgMemberHelper(void)
    {
    AtPrintc(cSevWarning, "WARNING: There are no any members for all of input VCGs.\r\n");
    return cAtTrue;
    }

static eBool AlarmShow(char argc, char **argv, CliConcateMembersGet MembersGet)
    {
    tTab *table;
    const char *heading[] ={"VcgId", "Member", "CRC", "LOM", "GIDM", "SQM", "MND", "TSF", "TSD", "MSU", "SQNC", "LOA", "OOM"};
    uint32 member_i;
    uint32 numMembers;
    AtConcateMember *memberList;
    char *memberIdsString = (argc > 1) ? argv[1] : NULL;

    memberList = MembersGet(argv[0], memberIdsString, &numMembers);
    if (numMembers == 0)
        return VcgMemberHelper();

    /* Create table */
    table = TableAlloc(numMembers, mCount(heading), heading);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (member_i = 0; member_i < numMembers; member_i = AtCliNextIdWithSleep(member_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtConcateMember member = memberList[member_i];
        AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
        uint8 column = 0;
        uint32 alarm = AtChannelDefectGet((AtChannel)member);

        /* Common information */
        StrToCell(table, member_i, column++, (char *)CliChannelIdStringGet((AtChannel)group));
        StrToCell(table, member_i, column++, (char *)CliChannelIdStringGet(AtConcateMemberChannelGet(member)));

        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeCrc);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeLom);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeGidM);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeSqm);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeMnd);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeTsf);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeTsd);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeMsu);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeSqnc);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeLoa);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeOom);
        }

    TablePrint(table);
    TableFree(table);

    return cAtTrue;
    }

static eBool InterruptShow(char argc, char **argv, CliConcateMembersGet MembersGet)
    {
    tTab *table;
    const char *heading[] ={"VcgId", "Member", "CRC", "LOM", "GIDM", "SQM", "MND",
                            "TSF", "TSD", "MSU", "SQNC", "LOA", "OOM", "SourceStateChange", "SinkStateChange"};
    uint32 member_i;
    uint32 numMembers;
    AtConcateMember *memberList;
    eAtHistoryReadingMode readingMode = CliHistoryReadingModeGet(argv[1]);
    char *memberIdsString = (argc > 2) ? argv[2] : NULL;
    uint32 (*InterruptGet)(AtChannel self);

    memberList = MembersGet(argv[0], memberIdsString, &numMembers);
    if (numMembers == 0)
        return VcgMemberHelper();

    if (readingMode == cAtHistoryReadingModeReadOnly)
        InterruptGet = AtChannelDefectInterruptGet;
    else if (readingMode == cAtHistoryReadingModeReadToClear)
        InterruptGet = AtChannelDefectInterruptClear;
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Create table */
    table = TableAlloc(numMembers, mCount(heading), heading);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (member_i = 0; member_i < numMembers; member_i = AtCliNextIdWithSleep(member_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtConcateMember member = memberList[member_i];
        AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
        uint32 alarm = InterruptGet((AtChannel)member);
        uint8 column = 0;

        /* Common information */
        StrToCell(table, member_i, column++, (char *)CliChannelIdStringGet((AtChannel)group));
        StrToCell(table, member_i, column++, (char *)CliChannelIdStringGet(AtConcateMemberChannelGet(member)));

        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeCrc);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeLom);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeGidM);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeSqm);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeMnd);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeTsf);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeTsd);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeMsu);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeSqnc);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeLoa);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeOom);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeSourceStateChange);
        PutAlarmToTable(table, member_i, column++, (AtChannel)member, alarm, cAtConcateMemberAlarmTypeSinkStateChange);
        }

    TablePrint(table);
    TableFree(table);

    return cAtTrue;
    }

static eBool InteruptMaskSet(char argc, char **argv,
                             const char *alarmsTypeStr[],
                             const uint32 *alarmsTypeVal,
                             uint32 numAlarms,
                             AtConcateMember *(*MembersGet)(char *, char *, uint32 *))
    {
    AtConcateMember *memberList;
    uint32 numMembers, member_i;
    uint32 intrMaskVal;
    eBool convertSuccess;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
    char *memberIdsString = NULL;

    if (argc > 3)
        memberIdsString = argv[3];

    /* Get list of path */
    memberList = MembersGet(argv[0], memberIdsString, &numMembers);
    if (numMembers == 0)
        return VcgMemberHelper();

    /* Get interrupt mask value from input string */
    intrMaskVal = CliMaskFromString(argv[1], alarmsTypeStr, alarmsTypeVal, numAlarms);
    if (intrMaskVal == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (member_i = 0; member_i < numMembers; member_i = AtCliNextIdWithSleep(member_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel channel;

        channel = (AtChannel)memberList[member_i];
        ret = AtChannelInterruptMaskSet(channel, intrMaskVal, enable ? intrMaskVal : 0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set interrupt mask for member %s, ret = %s\r\n",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool IsSourceInterruptMask(uint32 alarms)
    {
    if (alarms & cAtConcateMemberAlarmTypeSourceStateChange)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSinkInterruptMask(uint32 alarms)
    {
    static const uint32 allSinkAlarms = cAtConcateMemberAlarmTypeAll & ~cAtConcateMemberAlarmTypeSourceStateChange;

    if (alarms & allSinkAlarms)
        return cAtTrue;

    return cAtFalse;
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    AtConcateMember member = (AtConcateMember)channel;
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [VCAT/LCAS] Defect changed at member %s",
             AtOsalDateTimeInUsGet(), CliChannelIdStringGet(AtConcateMemberChannelGet(member)));
    for (alarm_i = 0; alarm_i < mCount(cAtConcateMemberAlarmValue); alarm_i++)
        {
        uint32 alarmType = cAtConcateMemberAlarmValue[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtConcateMemberAlarmUpperStr[alarm_i]);

        if (changedAlarms & alarmType & cAtConcateMemberAlarmTypeSinkStateChange)
            AtPrintc(cSevNormal, "%s",
                     CliAtLcasMemberSinkState2String(AtConcateMemberSinkStateGet(member)));

        else if (changedAlarms & alarmType & cAtConcateMemberAlarmTypeSourceStateChange)
            AtPrintc(cSevNormal, "%s",
                     CliAtLcasMemberSourceState2String(AtConcateMemberSourceStateGet(member)));
        else
            {
            if (currentStatus & alarmType)
                AtPrintc(cSevCritical, "set");
            else
                AtPrintc(cSevInfo, "clear");
            }
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtConcateMemberObserver observer = userData;
    AtConcateMember member = (AtConcateMember)channel;

    AtChannelObserverDefectsUpdate((AtChannelObserver)observer, changedAlarms, currentStatus);

    if (changedAlarms & cAtConcateMemberAlarmTypeSinkStateChange)
        AtConcateMemberObserverSinkStatePush(observer, AtConcateMemberSinkStateGet(member));
    if (changedAlarms & cAtConcateMemberAlarmTypeSourceStateChange)
        AtConcateMemberObserverSourceStatePush(observer, AtConcateMemberSourceStateGet(member));
    }

const char *CliAtLcasMemberSourceState2String(eAtLcasMemberSourceState state)
    {
    static const char  *cCmdLcasMemberSourceStateStr[] = {
                                                   "FIXED",
                                                   "ADD",
                                                   "NORMAL",
                                                   "EOS",
                                                   "IDLE",
                                                   "DNU",
                                                   "RMV",
                                                   "INVL",
                                                   };
    static uint32 cCmdLcasMemberSourceStateVal[] = {
                                                    cAtLcasMemberSourceStateFixed,
                                                    cAtLcasMemberSourceStateAdd,
                                                    cAtLcasMemberSourceStateNorm,
                                                    cAtLcasMemberSourceStateEos,
                                                    cAtLcasMemberSourceStateIdle,
                                                    cAtLcasMemberSourceStateDnu,
                                                    cAtLcasMemberSourceStateRmv,
                                                    cAtLcasMemberSourceStateInvl,
                                                   };
    return CliEnumToString(state, cCmdLcasMemberSourceStateStr, cCmdLcasMemberSourceStateVal, mCount(cCmdLcasMemberSourceStateVal), NULL);
    }

const char *CliAtLcasMemberCtrl2String(eAtLcasMemberCtrl ctrl)
    {
    static const char  *cAtLcasMemberCtrlStr[] = {
                                           "FIXED",
                                           "ADD",
                                           "NORMAL",
                                           "EOS",
                                           "IDLE",
                                           "DNU",
                                           };

    static uint32 cAtLcasMemberCtrlVal[] = {
                                            cAtLcasMemberCtrlFixed,
                                            cAtLcasMemberCtrlAdd,
                                            cAtLcasMemberCtrlNorm,
                                            cAtLcasMemberCtrlEos,
                                            cAtLcasMemberCtrlIdle,
                                            cAtLcasMemberCtrlDnu
                                            };
    return CliEnumToString(ctrl, cAtLcasMemberCtrlStr, cAtLcasMemberCtrlVal, mCount(cAtLcasMemberCtrlVal), NULL);
    }

const char *CliAtLcasMemberSinkState2String(eAtLcasMemberSinkState state)
    {
    static const char  *cAtLcasMemberSinkStateStr[] = {
                                                "OK",
                                                "FAIL",
                                                "IDLE",
                                                "FIXED",
                                                "INVL",
                                                };
    static uint32 cAtLcasMemberSinkStateVal[]= {
                                                cAtLcasMemberSinkStateOk,
                                                cAtLcasMemberSinkStateFail,
                                                cAtLcasMemberSinkStateIdle,
                                                cAtLcasMemberSinkStateFixed,
                                                cAtLcasMemberSinkStateInvl,
                                                };

    return CliEnumToString(state, cAtLcasMemberSinkStateStr, cAtLcasMemberSinkStateVal, mCount(cAtLcasMemberSinkStateVal), NULL);
    }

static const char * CliConcateMemberAlarm2String(uint32 alarms, eAtConcateGroupSide side)
    {
    uint16 i;
    static char alarmString[64];

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cAtConcateMemberAlarmValue); i++)
            {
            if ((side == cAtConcateGroupSideSource) && !IsSourceInterruptMask(cAtConcateMemberAlarmValue[i]))
                continue;

            if ((side == cAtConcateGroupSideSink) && !IsSinkInterruptMask(cAtConcateMemberAlarmValue[i]))
                continue;

            if (alarms & (cAtConcateMemberAlarmValue[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, cAtConcateMemberAlarmStr[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

eBool CmdVcgSourceMemberShow(char argc, char **argv)
    {
    tTab *table;
    const char *heading[] ={"VcgId", "Member",
                            "SourceSequence", "SourceMst", "SourceCtrl", "SourceState", "SourceIntrMask"};
    uint32 row_i;
    uint32 numMembers;
    AtConcateMember *memberList;
    uint32 sequence;
    char *memberIdString = (argc > 1) ? argv[1] : NULL;

    memberList = CliSourceConcateMembersFromString(argv[0], memberIdString, &numMembers);
    if (numMembers == 0)
        {
        AtPrintc(cSevWarning, "WARNING: There are no members for all of input VCGs. ID maybe not correct\r\n");
        return cAtTrue;
        }

    /* Create table */
    table = TableAlloc(numMembers, mCount(heading), heading);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (row_i = 0; row_i < numMembers; row_i = AtCliNextIdWithSleep(row_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtConcateMember member = memberList[row_i];
        AtConcateGroup vcg = AtConcateMemberConcateGroupGet(member);
        const char *stringValue;
        eAtLcasMemberCtrl ctrl;
        uint8 column = 0;
        uint32 mask;

        /* Common information */
        StrToCell(table, row_i, column++, (char *)CliChannelIdStringGet((AtChannel)vcg));
        StrToCell(table, row_i, column++, (char *)CliChannelIdStringGet(AtConcateMemberSourceChannelGet(member)));

        if (CliAtVcgIsLcasVcg(vcg))
            {
            eAtLcasMemberSourceState sourceState;
            uint32 mst;

            /* Source sequence */
            sequence = AtConcateMemberSourceSequenceGet(member);
            StrToCell(table, row_i, column++, CliNumber2String(sequence, "%d")); /* Let use standard SQ. */

            /* Source MST */
            mst = AtConcateMemberSourceMstGet(member);
            ColorStrToCell(table, row_i, column++, mst ? "FAIL" : "OK", mst ? cSevCritical : cSevInfo);

            /* Source CTRL */
            ctrl = AtConcateMemberSourceControlGet(member);
            stringValue = CliAtLcasMemberCtrl2String(ctrl);
            ColorStrToCell(table, row_i, column++, stringValue ? stringValue : "Error", ColorForCtrl(ctrl));

            /* Source state */
            sourceState = AtConcateMemberSourceStateGet(member);
            stringValue = CliAtLcasMemberSourceState2String(sourceState);
            ColorStrToCell(table, row_i, column++, stringValue ? stringValue : "Error", ColorForSourceState(sourceState));
            }

        /* VCAT VCG */
        else if (CliAtVcgIsVcatVcg(vcg))
            {
            /* Source sequence */
            sequence = AtConcateMemberSourceSequenceGet(member);
            StrToCell(table, row_i, column++, CliNumber2String(sequence, "%d"));

            /* Source MST */
            StrToCell(table, row_i, column++, sAtNotApplicable);

            /* Source CTRL */
            ctrl = AtConcateMemberSourceControlGet(member);
            stringValue = CliAtLcasMemberCtrl2String(ctrl);
            ColorStrToCell(table, row_i, column++, stringValue ? stringValue : "Error", ColorForCtrl(ctrl));

            /* Source state */
            StrToCell(table, row_i, column++, sAtNotApplicable);
            }

        /* CCAT VCG */
        else
            {
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            }

        /* Interrupt mask */
        mask = AtChannelInterruptMaskGet((AtChannel)member);
        StrToCell(table, row_i, column++, CliConcateMemberAlarm2String(mask, cAtConcateGroupSideSource));
        }

    TablePrint(table);
    TableFree(table);

    AtPrintc(cSevInfo,   "* Note: \r\n");
    AtPrintc(cSevNormal, "  - SourceMst is Receive MST\r\n");

    return cAtTrue;
    }

eBool CmdVcgSinkMemberShow(char argc, char **argv)
    {
    tTab *table;
    const char *heading[] ={"VcgId", "Member",
                            "SinkSequence", "ExpSequence", "SinkMst", "SinkCtrl", "SinkState", "SinkIntrMask"};
    uint32 row_i;
    uint32 numMembers;
    AtConcateMember *memberList;
    uint32 sequence;
    char *memberIdString = (argc > 1) ? argv[1] : NULL;

    memberList = CliSinkConcateMembersFromString(argv[0], memberIdString, &numMembers);
    if (numMembers == 0)
        {
        AtPrintc(cSevWarning, "WARNING: There are no members for all of input VCGs. ID maybe not correct\r\n");
        return cAtTrue;
        }

    /* Create table */
    table = TableAlloc(numMembers, mCount(heading), heading);
    if (table == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (row_i = 0; row_i < numMembers; row_i = AtCliNextIdWithSleep(row_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtConcateMember member = memberList[row_i];
        AtConcateGroup vcg = AtConcateMemberConcateGroupGet(member);
        eAtLcasMemberCtrl ctrl;
        const char *stringValue;
        uint8 column = 0;
        uint32 mask;

        /* Common information */
        StrToCell(table, row_i, column++, (char *)CliChannelIdStringGet((AtChannel)vcg));
        StrToCell(table, row_i, column++, (char *)CliChannelIdStringGet(AtConcateMemberSinkChannelGet(member)));

        if (CliAtVcgIsLcasVcg(vcg))
            {
            eAtLcasMemberSinkState sinkState;
            uint32 mst;

            /* Sink sequence */
            sequence = AtConcateMemberSinkSequenceGet(member);
            StrToCell(table, row_i, column++, CliNumber2String(sequence, "%d")); /* Let use standard SQ. */

            /* Expected Sink SQ*/
            StrToCell(table, row_i, column++, sAtNotApplicable);

            /* Sink MST */
            mst = AtConcateMemberSinkMstGet(member);
            ColorStrToCell(table, row_i, column++, mst ? "FAIL" : "OK", mst ? cSevCritical : cSevInfo);

            /* Sink CTRL */
            ctrl = AtConcateMemberSinkControlGet(member);
            stringValue = CliAtLcasMemberCtrl2String(ctrl);
            ColorStrToCell(table, row_i, column++, stringValue ? stringValue : "Error", ColorForCtrl(ctrl));

            /* Sink state */
            sinkState = AtConcateMemberSinkStateGet(member);
            stringValue = CliAtLcasMemberSinkState2String(sinkState);
            ColorStrToCell(table, row_i, column++, stringValue ? stringValue : "Error", ColorForSinkState(sinkState));
            }

        /* VCAT VCG */
        else if (CliAtVcgIsVcatVcg(vcg))
            {
            /* Sink sequence */
            sequence = AtConcateMemberSinkSequenceGet(member);
            StrToCell(table, row_i, column++, CliNumber2String(sequence, "%d"));

            /* Sink expected sequence */
            sequence = AtConcateMemberSinkExpectedSequenceGet(member);
            StrToCell(table, row_i, column++, CliNumber2String(sequence, "%d"));

            /* Sink MST */
            StrToCell(table, row_i, column++, sAtNotApplicable);

            /* Sink CTRL */
            ctrl = AtConcateMemberSinkControlGet(member);
            stringValue = CliAtLcasMemberCtrl2String(ctrl);
            ColorStrToCell(table, row_i, column++, stringValue ? stringValue : "Error", ColorForCtrl(ctrl));

            /* Sink state */
            StrToCell(table, row_i, column++, sAtNotApplicable);
            }

        /* CCAT VCG */
        else
            {
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            StrToCell(table, row_i, column++, sAtNotApplicable);
            }

        /* Interrupt mask */
        mask = AtChannelInterruptMaskGet((AtChannel)member);
        StrToCell(table, row_i, column++, CliConcateMemberAlarm2String(mask, cAtConcateGroupSideSink));
        }

    TablePrint(table);
    TableFree(table);

    AtPrintc(cSevInfo,   "* Note: \r\n");
    AtPrintc(cSevNormal, "  - SinkMst is Transmit MST\r\n");

    return cAtTrue;
    }

eBool CmdVcgMemberAlarmShow(char argc, char **argv)
    {
    return AlarmShow(argc, argv, CliSinkConcateMembersFromString);
    }

eBool CmdVcgMemberInterruptShow(char argc, char **argv)
    {
    /* TODO: this implement shall be not suitable for asymmetric provision. */
    return InterruptShow(argc, argv, CliSinkConcateMembersFromString);
    }

eBool CmdVcgMemberInterruptMaskSet(char argc, char **argv)
    {
    return InteruptMaskSet(argc, argv,
                           cAtConcateMemberAlarmStr,
                           cAtConcateMemberAlarmValue,
                           mCount(cAtConcateMemberAlarmValue),
                           CliSinkConcateMembersFromString);
    }

eBool CmdVcgMemberAlarmCapture(char argc, char **argv)
    {
    eBool convertSuccess;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    static tAtChannelEventListener listener;
    uint32 member_i;
    uint32 numMembers;
    AtConcateMember *memberList;
    char *memberIdsString = (argc > 2) ? argv[2] : NULL;
    AtUnused(argc);

    /* Get list of members */
    memberList = CliSinkConcateMembersFromString(argv[0], memberIdsString, &numMembers);
    if (numMembers == 0)
        return VcgMemberHelper();

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        listener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else
        listener.AlarmChangeState = AlarmChangeState;

    for (member_i = 0; member_i < numMembers; member_i = AtCliNextIdWithSleep(member_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel member;

        member = (AtChannel)memberList[member_i];

        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(member, &listener, CliAtVcgMemberObserverGet(member));
            else
                ret = AtChannelEventListenerAdd(member, &listener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(member, &listener);

            if (AutotestIsEnabled())
                CliAtVcgMemberObserverDelete(member);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(member),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdVcgMemberAlarmCaptureShow(char argc, char **argv)
    {
    uint32 numMembers, i;
    const char *pHeading[] ={"VcgId", "Member", "CRC", "LOM", "GIDM", "SQM", "MND",
                             "TSF", "TSD", "MSU", "SQNC", "LOA", "OOM", "SourceStateChange", "SinkStateChange"};
    eAtHistoryReadingMode readMode;
    AtConcateMember *memberList;
    eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    char *memberIdsString = (argc > 2) ? argv[2] : NULL;
    AtUnused(argc);

    memberList = CliSinkConcateMembersFromString(argv[0], memberIdsString, &numMembers);
    if (numMembers == 0)
        return VcgMemberHelper();

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    if (argc > 3)
        silent = CliHistoryReadingShouldSilent(readMode, argv[3]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numMembers, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numMembers; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtConcateMember member = memberList[i];
        AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
        AtChannelObserver observer = CliAtVcgMemberObserverGet((AtChannel)member);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)group));
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet(AtConcateMemberChannelGet(member)));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeCrc);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeLom);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeGidM);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeSqm);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeMnd);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeTsf);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeTsd);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeMsu);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeSqnc);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeLoa);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtConcateMemberAlarmTypeOom);

        /* Print member states recorded. */
        StrToCell(tabPtr, i, column++, AtConcateMemberObserverSourceStateString((AtConcateMemberObserver)observer));
        StrToCell(tabPtr, i, column++, AtConcateMemberObserverSinkStateString((AtConcateMemberObserver)observer));
        if (readMode == cAtHistoryReadingModeReadToClear)
            {
            AtConcateMemberObserverSourceStateFlush((AtConcateMemberObserver)observer);
            AtConcateMemberObserverSinkStateFlush((AtConcateMemberObserver)observer);
            }

        /* Delete observer */
        CliAtVcgMemberObserverDelete((AtChannel)member);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }


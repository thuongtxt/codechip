/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : CliAtModuleMap.h
 * 
 * Created Date: Jun 4, 2014
 *
 * Description : MAP CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULECONCATE_H_
#define _CLIATMODULECONCATE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleConcate.h"
#include "AtConcateGroup.h"
#include "AtVcg.h"
#include "AtConcateMember.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtModuleConcateRet (*MemberAddRemoveFunc)(AtVcg, AtConcateMember);
typedef AtConcateMember (*MemberGetByChannelFunc)(AtConcateGroup self, AtChannel channel);
typedef eAtModuleConcateRet (*SequenceSetFunc)(AtConcateMember self, uint32 sinkSequence);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void CliAtModuleConcatePutAlarm2Cell(tTab* table, uint32 row, uint32 column, uint32 alarm, uint32 alarmType);
AtList CliAtVcgVcgsFromString(char *vcgIdsString);
eBool CliAtVcgIsLcasVcg(AtConcateGroup group);
eBool CliAtVcgIsVcatVcg(AtConcateGroup group);
eBool CliAtVcgLcasEnable(char argc, char **argv, eBool enable);
const char *CliAtConcateGroupType2String(eAtConcateGroupType concateType);
eAtModuleConcateRet CliAtVcgLcasSourceSinkMemberAdd(AtVcg vcg, AtConcateMember member);
eAtModuleConcateRet CliAtVcgLcasSourceSinkMemberRemove(AtVcg vcg, AtConcateMember member);

eBool CliAtConcateMemberProvision(char argc, char **argv,
                              AtConcateMember (*ProvisionFunc)(AtConcateGroup self, AtChannel channel),
                              eAtModuleConcateRet (*DeProvisionFunc)(AtConcateGroup self, AtChannel channel));

eBool CliAtVcgLcasMemberAddRemove(char *vcgString, char *memberIdsString,
                                  MemberAddRemoveFunc memberAddRemoveFunc,
                                  MemberGetByChannelFunc memberGetByChannel);

const char *CliAtLcasMemberSourceState2String(eAtLcasMemberSourceState state);
const char *CliAtLcasMemberCtrl2String(eAtLcasMemberCtrl ctrl);
const char *CliAtLcasMemberSinkState2String(eAtLcasMemberSinkState state);

#endif /* _CLIATMODULECONCATE_H_ */


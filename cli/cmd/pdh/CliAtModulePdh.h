/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : CliAtModulePdh.h
 * 
 * Created Date: Nov 7, 2012
 *
 * Description : PDH CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULEPDH_H_
#define _CLIATMODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtPdhNxDs0.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtPdhLoopbackModeInvalid 0xFF

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtModulePdhRet (*EnableFunc)(AtPdhChannel, eBool);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char *CliPdhValidLoopbackModes(void);
uint32 CliPdhLoopbackModeFromString(const char *loopbackStr);
const char *CliPdhLoopbackModeString(eAtPdhLoopbackMode loopbackMode);
char *CliBitmap2String(uint32 bitmap);
char *CliNxDs0Bitmap2String(uint32 sigBitmap);
eBool CliAtPdhChannelEnable(char argc, char **argv, EnableFunc func, eBool enable);

const char **CliAtPdhDe1AlarmTypeStr(uint32 *numAlarms);
const uint32 *CliAtPdhDe1AlarmTypeVal(uint32 *numAlarms);
const char **CliAtPdhDe3AlarmTypeStr(uint32 *numAlarms);
const uint32 *CliAtPdhDe3AlarmTypeVal(uint32 *numAlarms);

#endif /* _CLIATMODULEPDH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : atclipdh_de1.c
 *
 * Created Date: Dec 1, 2011
 *
 * Description : DS1/E1 CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtPdhChannel.h"
#include "AtPdhDe1.h"
#include "AtSdhVc.h"
#include "AtPw.h"
#include "AtPdhDe1Prm.h"
#include "CliAtModulePdh.h"
#include "AtChannelHierarchyDisplayer.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"
#include "../common/CliAtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtPdhDataLinkCounterNumbs 9
#define cAtPdhDe1Event     (cAtPdhDs1AlarmBomChange|cAtPdhDs1AlarmInbandLoopCodeChange|\
                            cAtPdhE1AlarmSsmChange|cAtPdhDs1AlarmPrmLBBitChange|\
                            cAtPdhDe1AlarmClockStateChange)


typedef eAtRet (*AttributeSetFunc)(AtPdhDe1 self, uint32 value);
#define mAttributeSetFunc(func) ((AttributeSetFunc)(func))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_pBuf[200];

static const eBool cAtAppBoolVal[]  = {
                                     cAtTrue,
                                     cAtFalse
                                     };

static const char *cAtAppBoolStr[]  = {
                                 "en",
                                 "dis"
                                 };

static const char *cAtPdhDe1FrameModeStr[] = {
                                      "unknown",
                                      "ds1_unframed",
                                      "ds1_sf",
                                      "ds1_esf",
                                      "e1_unframed",
                                      "e1_basic",
                                      "e1_crc"
                                        };

static const eAtPdhDe1FrameType cAtPdhDe1FrameModeVal[]  = {
                                                   cAtPdhDe1FrameUnknown,
                                                   cAtPdhDs1J1UnFrm      , /**< DS1/J1 Unframe */
                                                   cAtPdhDs1FrmSf        , /**< DS1 SF (D4) */
                                                   cAtPdhDs1FrmEsf       , /**< DS1 ESF */
                                                   cAtPdhE1UnFrm         , /**< E1 unframe */
                                                   cAtPdhE1Frm           , /**< E1 basic frame, FAS/NFAS framing */
                                                   cAtPdhE1MFCrc           /**< E1 Multi-Frame with CRC4 */
                                                    };

static const char *cAtPdhDe1AlarmTypeStr[] =
    {
     "los",
     "lof",
     "ais",
     "rai",
     "lomf",
     "ber-sf",
     "ber-sd",
     "siglof",
     "sigrai",
     "ebit",
     "bom-change",
     "inband-loopcode-change",
     "prm-lbbit-change",
     "ssm-change",
     "ber-tca",
     "clock-state-change"
    };

static const uint32 cAtPdhDe1AlarmTypeVal[] =
    {
     cAtPdhDe1AlarmLos,
     cAtPdhDe1AlarmLof,
     cAtPdhDe1AlarmAis,
     cAtPdhDe1AlarmRai,
     cAtPdhDe1AlarmLomf,
     cAtPdhDe1AlarmSfBer,
     cAtPdhDe1AlarmSdBer,
     cAtPdhDe1AlarmSigLof,
     cAtPdhDe1AlarmSigRai,
     cAtPdhDe1AlarmEbit,
     cAtPdhDs1AlarmBomChange,
     cAtPdhDs1AlarmInbandLoopCodeChange,
     cAtPdhDs1AlarmPrmLBBitChange,
     cAtPdhE1AlarmSsmChange,
     cAtPdhDe1AlarmBerTca,
     cAtPdhDe1AlarmClockStateChange
    };

static const char *cAtPdhDe1AlarmTypeUpperStr[] =
    {
     "LOS",
     "LOF",
     "AIS",
     "RAI",
     "LOMF",
     "BER-SF",
     "BER-SD",
     "SigLOF",
     "SigRAI",
     "E-bit",
     "BOM",
     "InbandLPC",
     "PRM-LBbit",
     "SSM",
     "BER-TCA",
     "ACR/DCR"
    };

static const char *cAtPdhDe1CounterTypeStr[] = {"bpv", "crc", "fe", "rei", "tx-cs", "rx-cs"};
static const eAtPdhDe1CounterType cAtPdhDe1CounterTypeVal[] = {
                                                         cAtPdhDe1CounterBpvExz,
                                                         cAtPdhDe1CounterCrc,
                                                         cAtPdhDe1CounterFe,
                                                         cAtPdhDe1CounterRei,
                                                         cAtPdhDe1CounterTxCs,
                                                         cAtPdhDe1CounterRxCs
                                                        };
static const char *cAtPdhDe1LoopcodeStr[] =
    {
    "unknown",
    "line-active",
    "line-deactive",
    "facility1-active",
    "facility1-deactive",
    "facility2-active",
    "facility2-deactive",
    "inband-disable"
    };

static const uint32 cAtPdhDe1LoopcodeVal[] =
    {
     cAtPdhDe1LoopcodeInvalid,
     cAtPdhDe1LoopcodeLineActivate,
     cAtPdhDe1LoopcodeLineDeactivate,
     cAtPdhDe1LoopcodeFacility1Activate,
     cAtPdhDe1LoopcodeFacility1Deactivate,
     cAtPdhDe1LoopcodeFacility2Activate,
     cAtPdhDe1LoopcodeFacility2Deactivate,
     cAtPdhDe1LoopcodeInbandDisable
    };

static const char *cAtPdhDe1PrmStandardStr[] =
    {
    "unknown", "ansi", "at&t"
    };

static const char *cAtPdhDe1PrmStandardXmlStr[] =
    {
    "unknown", "ansi", "at&amp;t"
    };

static const uint32 cAtPdhDe1PrmStandardVal[] =
    {
    cAtPdhDe1PrmStandardUnknown,    /**< Unknown standard */
    cAtPdhDe1PrmStandardAnsi,       /**< ANSI standard */
    cAtPdhDe1PrmStandardAtt         /**< AT&T standard */
    };

static const uint32 cAtPdhDe1SaVal[] =
    {
    0,
    cAtPdhE1Sa4,
    cAtPdhE1Sa5,
    cAtPdhE1Sa6,
    cAtPdhE1Sa7,
    cAtPdhE1Sa8
    };

static const char *cAtPdhDe1SaStr[] =
    {
    "none", "sa-4", "sa-5", "sa-6", "sa-7", "sa-8"
    };

static const uint32 cAtPdhDe1TxBomModeVal[] =
    {
    cAtPdhBomSentModeInvalid,
    cAtPdhBomSentModeOneShot,
    cAtPdhBomSentModeContinuous
    };

static const char *cAtPdhDe1TxBomModeStr[] =
    {
    "invalid", "oneshot", "continuous"
    };

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 AtChannelHwClockStateGet(AtChannel self);

/*--------------------------- Implementation ---------------------------------*/
static const char *RxLoopCodeStr(eAtPdhDe1Loopcode rxCode)
    {
    const char *codeStr = CliEnumToString(rxCode, cAtPdhDe1LoopcodeStr, cAtPdhDe1LoopcodeVal, mCount(cAtPdhDe1LoopcodeVal), NULL);
    return codeStr ? codeStr : sAtUnknown;
    }

static const char * TimingModeStr(uint32 mode)
    {
    switch (mode)
        {
        case cAtTimingModeAcr: return "ACR";
        case cAtTimingModeDcr: return "DCR";
        default:               return "Clock";
        }
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [PDH] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtPdhDe1AlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtPdhDe1AlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtPdhDe1AlarmClockStateChange)
            AtPrintc(cSevNormal, " * [%s:", TimingModeStr(AtChannelTimingModeGet(channel)));
        else
            AtPrintc(cSevNormal, " * [%s:", cAtPdhDe1AlarmTypeUpperStr[alarm_i]);

        if (changedAlarms & alarmType & cAtPdhDe1Event)
            {
            switch (alarmType)
                {
                case cAtPdhDs1AlarmBomChange:
                    AtPrintc(cSevNormal, "0x%x", AtPdhDe1RxBomGet((AtPdhDe1)channel));
                    break;
                case cAtPdhE1AlarmSsmChange:
                    AtPrintc(cSevNormal, "0x%x", AtPdhChannelRxSsmGet((AtPdhChannel)channel));
                    break;
                case cAtPdhDs1AlarmPrmLBBitChange:
                    AtPrintc(cSevNormal, "%u", AtPdhDe1PrmRxLBBitGet((AtPdhDe1)channel));
                    break;
                case cAtPdhDs1AlarmInbandLoopCodeChange:
                    AtPrintc(cSevNormal, "%s", RxLoopCodeStr(AtPdhDe1RxLoopcodeGet((AtPdhDe1)channel)));
                    break;
                case cAtPdhDe1AlarmClockStateChange:
                    {
                    AtPrintc(cSevNormal, "SW: %s, HW: %d",
                             CliClockStateString(AtChannelClockStateGet(channel)),
                             AtChannelHwClockStateGet(channel));
                    }
                    break;
                default:
                    AtPrintc(cSevInfo, "detect");
                }
            }
        else
            {
            if (currentStatus & alarmType)
                AtPrintc(cSevCritical, "set");
            else
                AtPrintc(cSevInfo, "clear");
            }
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cAtPdhDe1AlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtPdhDe1AlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtPdhDe1AlarmClockStateChange)
            logPtr += AtSprintf(logPtr, " * [%s:", TimingModeStr(AtChannelTimingModeGet(channel)));
        else
            logPtr += AtSprintf(logPtr, " * [%s:", cAtPdhDe1AlarmTypeUpperStr[alarm_i]);

        if (changedAlarms & alarmType & cAtPdhDe1Event)
            {
            switch (alarmType)
                {
                case cAtPdhDs1AlarmBomChange:
                    logPtr += AtSprintf(logPtr, "0x%x", AtPdhDe1RxBomGet((AtPdhDe1)channel));
                    break;
                case cAtPdhE1AlarmSsmChange:
                    logPtr += AtSprintf(logPtr, "0x%x", AtPdhChannelRxSsmGet((AtPdhChannel)channel));
                    break;
                case cAtPdhDs1AlarmPrmLBBitChange:
                    logPtr += AtSprintf(logPtr, "%u", AtPdhDe1PrmRxLBBitGet((AtPdhDe1)channel));
                    break;
                case cAtPdhDs1AlarmInbandLoopCodeChange:
                    logPtr += AtSprintf(logPtr, "%s", RxLoopCodeStr(AtPdhDe1RxLoopcodeGet((AtPdhDe1)channel)));
                    break;
                case cAtPdhDe1AlarmClockStateChange:
                    {
                    logPtr += AtSprintf(logPtr, "SW: %s, HW: %d",
                                        CliClockStateString(AtChannelClockStateGet(channel)),
                                        AtChannelHwClockStateGet(channel));
                    }
                    break;
                default:
                    logPtr += AtSprintf(logPtr, "detect");
                }
            }
        else
            {
            if (currentStatus & alarmType)
                logPtr += AtSprintf(logPtr, "set");
            else
                logPtr += AtSprintf(logPtr, "clear");
            }
        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

static void DefectLogging(AtChannel channel, uint32 currentDefect, eBool enable)
    {
    uint8 alarm_i;

    for (alarm_i = 0; alarm_i < mCount(cAtPdhDe1AlarmTypeUpperStr); alarm_i++)
        {
        eAtPdhDe1AlarmType alarmType = cAtPdhDe1AlarmTypeVal[alarm_i];
        if (currentDefect & alarmType)
            {
            AtPrintc(cSevNormal, "\r\n%s [PDH] Defect [%s] %s at channel %s\r\n",
                     AtOsalDateTimeInUsGet(), cAtPdhDe1AlarmTypeUpperStr[alarm_i], enable ? "forced" : "unforced", CliChannelIdStringGet(channel));
            return;
            }
        }
    }

static void EventLogging(AtChannel channel, const char *actionString, const char *valueString)
    {
    AtPrintc(cSevNormal, "\r\n%s [PDH] %s [%s] at channel %s\r\n",
             AtOsalDateTimeInUsGet(), actionString, valueString, CliChannelIdStringGet(channel));
    }

/* To get DS1/E1 ID list from string */
static eBool De1Enable(char argc, char **argv, eBool enable)
    {
    /* Declare variables */
    eAtRet  ret;
    uint32 numChannels, i;
    AtChannel *channelList;
    uint8 idx;
    AtUnused(argc);

    ret = cAtOk;
    idx = 0;

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[idx++], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtChannelEnable(channelList[i], enable);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable");
        }

    return cAtTrue;
    }

static eBool Enable(char argc, char **argv, EnableFunc func, eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = func((AtPdhChannel)channelList[i], enable);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable this function!");
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool Uint8AttributeSet(char argc, char **argv, eAtModulePdhRet (*uint8AttributeSetFunc)(AtPdhChannel self, uint8 value))
    {
    /* Declare variables */
    eAtRet  ret = cAtOk;
    uint32 numChannels, i;
    AtChannel *channelList;
    uint8 idx = 0;
    uint8 value;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[idx++], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get data */
    value = (uint8)AtStrToDw(argv[idx++]);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = uint8AttributeSetFunc((AtPdhChannel)channelList[i], value);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set value");
        }

    return cAtTrue;
    }

static uint32 CliPdhIntrMaskFromString(char *pIntrStr)
    {
    return CliMaskFromString(pIntrStr, cAtPdhDe1AlarmTypeStr, cAtPdhDe1AlarmTypeVal, mCount(cAtPdhDe1AlarmTypeVal));
    }

static const char *CliPdhDe1AlarmStrFromMask(uint32 alarmMask)
    {
    return CliAlarmMaskToString(cAtPdhDe1AlarmTypeStr, cAtPdhDe1AlarmTypeVal, mCount(cAtPdhDe1AlarmTypeVal), alarmMask);
    }

static eBool DataLinkCountersShow(char *pDe1Str, uint32 (*CounterGetFunc)(AtPdhChannel, uint32), eBool silent)
    {
    /* Declare variables */
    uint32 numChannels, i, counterValue;
    AtChannel *channelList;
    uint8 column, counter_i;
    tTab *tabPtr = NULL;
    const char *pHeading[] =
        {
        "DE1 ID",
        "Tx_Pkts",
        "Tx_Bytes",
        "Tx_Abort_Pkts",
        "Rx_Pkts",
        "Rx_Bytes",
        "Rx_Abort_Pkts",
        "Rx_FCS_Pkts",
        "Rx_Drop_Pkts",
        "Rx_Length_violation_Pkts"
        };

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(pDe1Str, channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical,
                 "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    if (!silent)
        {
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        column = 0;
        StrToCell(tabPtr, i, column++, (char*)CliChannelIdStringGet((AtChannel) channelList[i]));

        for (counter_i = 0; counter_i < cAtPdhDataLinkCounterNumbs; counter_i++)
            {
            counterValue = CounterGetFunc((AtPdhChannel) channelList[i],
                                          counter_i);
            CliCounterPrintToCell(tabPtr,
                                  i,
                                  column++,
                                  counterValue,
                                  cAtCounterTypeError,
                                  cAtTrue);
            }
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static void DataLinkMessageReceived(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint32 byte_i, length;
    static uint8 m_dataBuffer[1024];

    AtUnused(currentStatus);

    if ((changedAlarms & cAtPdhDs1AlarmLapdChange) == 0)
        return;

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "NewMsg is received at %s\r\n", CliChannelIdStringGet(channel));
    length = AtPdhChannelDataLinkReceive((AtPdhChannel)channel, m_dataBuffer, sizeof(m_dataBuffer));
    for (byte_i = 0; byte_i < length; byte_i++)
        {
        AtPrintc(cSevInfo, " %02x", m_dataBuffer[byte_i]);
        if (((byte_i + 1) % 16) == 0)
            AtPrintc(cSevInfo, "\r\n");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static eBool DataLinkNotification(char *pDe1Str, eBool enable)
    {
    uint32 numberOfDe1, i;
    AtChannel *de1s;
    static tAtChannelEventListener de1DataLinkListener;
    eBool success = cAtTrue;

    /* Get the shared buffer to hold all of objects, then call the parse function */
    de1s = CliSharedChannelListGet(&numberOfDe1);
    numberOfDe1 = De1ListFromString(pDe1Str, de1s, numberOfDe1);
    if (!numberOfDe1)
        {
        AtPrintc(cSevCritical,
                 "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    AtOsalMemInit(&de1DataLinkListener, 0, sizeof(de1DataLinkListener));
    de1DataLinkListener.AlarmChangeState = DataLinkMessageReceived;

    /* Register Interrupt listener */
    for (i = 0; i < numberOfDe1; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;

        /* Register/deregister alarm listener */
        if (enable)
            ret = AtChannelEventListenerAdd(de1s[i], &de1DataLinkListener);
        else
            ret = AtChannelEventListenerRemove(de1s[i], &de1DataLinkListener);

        /* Let user know if error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener on %s, ret = %s\r\n",
                     enable ? "register" : "deregister",
                     CliChannelIdStringGet(de1s[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool LoopCodeEnableSet(char argc, char **argv, eBool enable, eAtModulePdhRet (*enableFunc)(AtPdhDe1, eBool))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical,
                 "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = enableFunc((AtPdhDe1) channelList[i], enable);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret,
                                     channelList[i],
                                     "Can not enable/disable\r\n");
        }

    return cAtTrue;
    }

static char *SaBitMaskString(uint32 mask)
    {
    static char saBitMaskString[16];
    eBool convertSuccess;

    mAtEnumToStr(cAtPdhDe1SaStr, cAtPdhDe1SaVal, mask, saBitMaskString, convertSuccess);
    if (!convertSuccess)
        return NULL;

    return saBitMaskString;
    }

static eBool PrmStandardSet(char argc, char **argv,
                            eAtModulePdhRet (*AttributeSet)(AtPdhDe1 self, eAtPdhDe1PrmStandard standard))
    {
    eAtRet  ret;
    eBool   blResult;
    uint32 numChannels, i;
    AtChannel *channelList;
    eAtPdhDe1PrmStandard standard = cAtPdhDe1PrmStandardUnknown;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get PRM standard */
    mAtStrToEnum(cAtPdhDe1PrmStandardStr, cAtPdhDe1PrmStandardVal, argv[1], standard, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected:  ansi or at&t \r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       ret = AttributeSet((AtPdhDe1)channelList[i], standard);
       if (ret != cAtOk)
           mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set PRM standard");
       }

    return cAtTrue;
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtChannelLogEnable(channelList[i], enable);
        if (ret != cAtOk)
           {
           AtPrintc(cSevCritical,
                    "ERROR: Cannot enable logging for DE1 %s, ret = %s\r\n",
                    CliChannelIdStringGet(channelList[i]),
                    AtRet2String(ret));
           success = cAtFalse;
           }
        }

    return success;
    }

static eBool PrmEnable(char argc, char **argv, eAtModulePdhRet (*Atttribute)(AtPdhDe1, eBool), eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = Atttribute((AtPdhDe1)channelList[i], enable);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable Prm\r\n");
            success = cAtFalse;
            }
        }

    return success;
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelObserver observer = userData;

    AtChannelObserverDefectsUpdate(observer, changedAlarms, currentStatus);

    if (changedAlarms & cAtPdhDe1AlarmClockStateChange)
        AtChannelObserverClockStatePush(observer, AtChannelClockStateGet(channel));
    }

static eBool AttributeSet(char argc, char **argv, uint32 value, AttributeSetFunc setFunc)
    {
    uint32 numChannels, channel_i;
    AtChannel *channelList;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = setFunc((AtPdhDe1)channelList[channel_i], value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                "ERROR: Can not configure channel %s, ret = %s\r\n",
                AtChannelIdString(channelList[channel_i]),
                AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool AlarmAffectingEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32      numChannels, i;
    AtChannel   *channelList;
    uint32 alarmValue = 0;
    AtUnused(argc);

    /* Get list of paths */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Alarm types */
    alarmValue = CliPdhIntrMaskFromString(argv[1]);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        ret |= AtPdhChannelAlarmAffectingEnable((AtPdhChannel)channelList[i], alarmValue, enable);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static const char *PrmStandard2String(uint32 value)
    {
    if (AutotestIsEnabled())
        return CliEnumToString(value, cAtPdhDe1PrmStandardXmlStr, cAtPdhDe1PrmStandardVal, mCount(cAtPdhDe1PrmStandardVal), NULL);
    else
        return CliEnumToString(value, cAtPdhDe1PrmStandardStr, cAtPdhDe1PrmStandardVal, mCount(cAtPdhDe1PrmStandardVal), NULL);
    }

const char **CliAtPdhDe1AlarmTypeStr(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPdhDe1AlarmTypeStr);
    return cAtPdhDe1AlarmTypeStr;
    }

const uint32 *CliAtPdhDe1AlarmTypeVal(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPdhDe1AlarmTypeVal);
    return cAtPdhDe1AlarmTypeVal;
    }

eBool CmdPdhDe1FrameModeSet(char argc, char **argv)
    {
    eAtPdhDe1FrameType frameMode = cAtPdhDe1FrameUnknown;
    eBool convertSuccess = cAtFalse;

    mAtStrToEnum(cAtPdhDe1FrameModeStr, cAtPdhDe1FrameModeVal, argv[1], frameMode, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown framing mode %s. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cAtPdhDe1FrameModeStr, mCount(cAtPdhDe1FrameModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return AttributeSet(argc, argv, frameMode, mAttributeSetFunc(AtPdhChannelFrameTypeSet));
    }

/* Enable/Disable and get signaling */
static eBool CliPdhDe1SignalingSet(char argc, char **argv, eAtModulePdhRet (*fSignalingEnable)(AtPdhDe1, eBool))
    {
    eAtRet ret = cAtOk;
    uint32 numChannels, i, sigBitmap, numItems;
    AtChannel *channelList;
    eBool enable = cAtFalse, result;
    uint8 idx = 0;
    AtIdParser idParser;

	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[idx++], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get Ds0 list */
    idParser = AtIdParserNew(argv[idx++], NULL, NULL);
    numItems = AtIdParserNumNumbers(idParser);
    if (numItems == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS0 list. Example: 1 or 1-16 or 1-15,17-31\r\n");
        AtObjectDelete((AtObject)idParser);
        return cAtFalse;
        }

    sigBitmap = 0;
    for(i = 0; i < numItems; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 slotId = AtIdParserNextNumber(idParser);
        if (slotId > 31)
            AtPrintc(cSevWarning, "WARNING: timeslot ID %u is invalid and it is ignored\r\n", slotId);
        sigBitmap |= (cBit0 << slotId);
        }

    /* Get signaling mode */
    mAtStrToEnum(cAtAppBoolStr, cAtAppBoolVal, argv[idx], enable, result);
    idx++;
    if(!result)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter. Expected: en/dis\r\n");
        AtObjectDelete((AtObject)idParser);
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        ret = AtPdhDe1SignalingDs0MaskSet((AtPdhDe1)channelList[i], sigBitmap);
        ret |= fSignalingEnable((AtPdhDe1)channelList[i], enable);
        if (ret != cAtOk)
           mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set signaling");
        }

    AtObjectDelete((AtObject)idParser);

    return cAtTrue;
    }

eBool CmdPdhDe1SignalingSet(char argc, char **argv)
    {
    return CliPdhDe1SignalingSet(argc, argv, AtPdhDe1SignalingEnable);
    }

eBool CmdPdhDe1TxSignalingSet(char argc, char **argv)
    {
    return CliPdhDe1SignalingSet(argc, argv, AtPdhDe1TxSignalingEnable);
    }

eBool CmdPdhDe1RxSignalingSet(char argc, char **argv)
    {
    return CliPdhDe1SignalingSet(argc, argv, AtPdhDe1RxSignalingEnable);
    }

eBool CmdPdhDe1SignalingPatternSet(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 numChannels = 0, i = 0, sigBitmap = 0, numItems = 0;
    AtChannel *channelList;
    AtIdParser idParser;
    uint8 pattern;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get Ds0 list */
    idParser = AtIdParserNew(argv[1], NULL, NULL);
    numItems = AtIdParserNumNumbers(idParser);
    if (numItems == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS0 list. Example: 1 or 1-16 or 1-15,17-31\r\n");
        AtObjectDelete((AtObject)idParser);
        return cAtFalse;
        }

    for(i = 0; i < numItems; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 slotId = AtIdParserNextNumber(idParser);
        if (slotId > 31)
            AtPrintc(cSevWarning, "WARNING: timeslot ID %u is invalid and it is ignored\r\n", slotId);
        sigBitmap |= (cBit0 << slotId);
        }

    /* Get signaling pattern */
    pattern = (uint8)AtStrtoul(argv[2], NULL, 16);

    for (i = 0; i < numChannels; i++)
        {
        uint32 ts = 0;
        AtPdhDe1 de1 = (AtPdhDe1)channelList[i];
        for (ts = 0; ts < 32; ts++)
            {
            if ((cBit0 << ts) & sigBitmap)
                {
                ret = AtPdhDe1SignalingPatternSet(de1, ts, pattern);
                if (ret != cAtOk)
                    AtPrintc(cSevWarning, "WARNING: Channel %s: TS#%d Can not set signaling pattern. ret = %s\n",               \
                                 CliChannelIdStringGet((AtChannel)de1), ts, AtRet2String(ret));
                }
            }
        }

    AtObjectDelete((AtObject)idParser);

    return cAtTrue;
    }

/* Show signaling information of specific Time Slots of DS1/E1 */
eBool  CmdPdhDe1SignalingShow(char argc, char **argv)
    {
    uint32 numChannels = 0, i = 0, sigBitmap = 0, numItems = 0, column = 0, row = 0;
    AtChannel *channelList;
    AtIdParser idParser;
    uint8 pattern;
    tTab    *tabPtr;
    const char *pHeading[] = {"DE1ID.TS", "sigEnable", "sigPattern"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get Ds0 list */
    idParser = AtIdParserNew(argv[1], NULL, NULL);
    numItems = AtIdParserNumNumbers(idParser);
    if (numItems == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS0 list. Example: 1 or 1-16 or 1-15,17-31\r\n");
        AtObjectDelete((AtObject)idParser);
        return cAtFalse;
        }

    for(i = 0; i < numItems; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 slotId = AtIdParserNextNumber(idParser);
        if (slotId > 31)
            AtPrintc(cSevWarning, "WARNING: timeslot ID %u is invalid and it is ignored\r\n", slotId);
        sigBitmap |= (cBit0 << slotId);
        }

    /* Create table with titles */
    tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        AtObjectDelete((AtObject)idParser);
        return cAtFalse;
        }

    row = 0;
    for (i = 0; i < numChannels; i++)
        {
        uint32 ts = 0;
        AtPdhDe1 de1 = (AtPdhDe1)channelList[i];
        eBool supported = AtPdhDe1SignalingIsSupported(de1);
        eBool sigEnabled = AtPdhDe1SignalingIsEnabled(de1);
        uint32 ds0SignalingMask = AtPdhDe1SignalingDs0MaskGet(de1);
        char cellStr[128];
        for (ts = 0; ts < 32; ts++)
            {
            uint32 tsMask = cBit0 << ts;
            if (tsMask & sigBitmap)
                {
                TableAddRow(tabPtr, 1);
                column = 0;
                /*ID*/
                AtSprintf(cellStr, "%s.%d", CliChannelIdStringGet((AtChannel)de1), ts);
                StrToCell(tabPtr, row, column++, cellStr);
                /*Enable*/
                AtSprintf(cellStr, "%s", (sigEnabled && (tsMask & ds0SignalingMask)) ? "en" : "dis");
                StrToCell(tabPtr, row, column++, cellStr);
                /*Pattern*/
                pattern = AtPdhDe1SignalingPatternGet(de1, ts);
                if (supported)
                    AtSprintf(cellStr, "0x%x", pattern);
                else
                    AtSprintf(cellStr, "%s", "NA");
                StrToCell(tabPtr, row, column++, cellStr);

                row++;
                }
            }
        }

    AtObjectDelete((AtObject)idParser);
    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* SET interrupt mask */
eBool CmdPdhDe1InterruptMaskSet(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet  ret;
    uint32 numChannels, intrMaskVal, i;
    AtChannel   *channelList;
    eBool enable = cAtFalse, result;
    uint8  idx;
	AtUnused(argc);

    ret = cAtOk;
    idx = 0;

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[idx++], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get and Convert interrupt maks from string */
    intrMaskVal = CliPdhIntrMaskFromString(argv[idx++]);

    /* Get enable mode */
    mAtStrToEnum(cAtAppBoolStr, cAtAppBoolVal, argv[idx], enable, result);
    idx++;
    if(!result)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter\r\n"
                                 "Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* SET Enabel/Disable */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtChannelInterruptMaskSet(channelList[i], intrMaskVal, (enable) ? intrMaskVal : 0x0);
        if (ret != cAtOk)
           mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set interrupt mask");
        }

    return cAtTrue;
    }

eBool CmdPdhDe1LoopbackSet(char argc, char **argv)
    {
    uint32 loopMode;

    loopMode = CliPdhLoopbackModeFromString(argv[1]);
    if (loopMode == cAtPdhLoopbackModeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid loopback mode. Expected: %s\r\n", CliPdhValidLoopbackModes());
        return cAtFalse;
        }

    return AttributeSet(argc, argv, loopMode, mAttributeSetFunc(AtChannelLoopbackSet));
    }

/* Set timing for DS1/E1 channels */
eBool CmdPdhDe1TimingSet(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, i;
    AtChannel *channelList, timingSrc;
    eAtTimingMode timingType;
    uint32 *pwIds = NULL;
    uint32 numPws = 0;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get timing mode */
    timingType = CliTimingModeFromString(argv[1]);
    if(timingType == cAtTimingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid timing mode parameter. Expected: %s\r\n", CliValidTimingModes());
        return cAtFalse;
        }

    /* Get timing source */
    timingSrc = NULL;
    if (timingType == cAtTimingModeSlave)
        {
        uint32 numMaster;
        numMaster = De1ListFromString(argv[2], &timingSrc, 1);
        if (numMaster != 1)
            {
            AtPrintc(cSevCritical, "ERROR: Timing source is not correct or too many timing sources\r\n");
            return cAtFalse;
            }
        }

    /* PW may be required in case of ACR/DCR */
    if ((timingType == cAtTimingModeAcr) || ((timingType == cAtTimingModeDcr)))
        {
        pwIds  = CliSharedIdBufferGet(&numPws);
        numPws = CliIdListFromString(argv[2], pwIds, numPws);
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = cAtOk;

        if (timingType == cAtTimingModeSlave)
            {
            ret = AtChannelTimingSet(channelList[i], timingType, timingSrc);
            }
        else
            {
            AtChannel timingSource = NULL;
            AtPw pw;
            AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
            eBool validTimingSource = cAtTrue;

            if (i < numPws)
                {
                timingSource = (AtChannel)AtModulePwGetPw(pwModule, (uint16)(pwIds[i] - 1));
                pw = (AtPw)timingSource;
                if ((AtPwTypeGet(pw) == cAtPwTypeSAToP) && (channelList[i] != AtPwBoundCircuitGet(pw)))
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: Cannot use %s as timing source of %s. The %s is currently bound to %s.\r\n"
                             "Input xxx to let internal implementation determine timing source.",
                             CliChannelIdStringGet(timingSource),
                             CliChannelIdStringGet(channelList[i]),
                             CliChannelIdStringGet(timingSource),
                             CliChannelIdStringGet(AtPwBoundCircuitGet(pw)));
                    success = cAtFalse;
                    validTimingSource = cAtFalse;
                    }
                }

            if (validTimingSource)
                ret = AtChannelTimingSet(channelList[i], timingType, timingSource);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set timing for %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

/* Force alarm or get alarm forcing status */
eBool CmdPdhDe1AlarmForce(char argc, char **argv)
    {
    uint32 numChannels, alarmMask, i;
    AtChannel *channelList;
    eBool convertSuccess;
    eAtRet (*Force)(AtChannel, uint32);
    eAtDirection direction;
    eBool success = cAtTrue;
    eBool enable = cAtFalse;
	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get Direction */
    direction = CliDirectionFromStr(argv[1]);
    if(direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
        return cAtFalse;
        }

    /* Get alarm type */
    alarmMask = CliPdhIntrMaskFromString(argv[2]);

    /* Get enable mode */
    mAtStrToBool(argv[3], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter. Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* Determine correct function to force */
    if (enable)
        Force = (direction == cAtDirectionRx) ? AtChannelRxAlarmForce : AtChannelTxAlarmForce;
    else
        Force = (direction == cAtDirectionRx) ? AtChannelRxAlarmUnForce : AtChannelTxAlarmUnForce;

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = Force((AtChannel)channelList[i], alarmMask);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not force alarms");
            success = cAtFalse;
            }

        if (CliTimeLoggingIsEnabled())
            DefectLogging((AtChannel)channelList[i], alarmMask, enable);
        }

    return success;
    }

/* Read or clear counters */
eBool CmdPdhDe1CounterGet(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, i;
    AtChannel *channelList;
    uint16 column;
    tTab *tabPtr = NULL;
    uint8 counter_i;
    eBool silent = cAtFalse;
    uint32 counterValue;
    eAtHistoryReadingMode readingMode;
    uint8 pdhDe1CounterType[] = {cAtPdhDe1CounterBpvExz, cAtPdhDe1CounterCrc,
                                 cAtPdhDe1CounterFe, cAtPdhDe1CounterRei, cAtPdhDe1CounterTxCs, cAtPdhDe1CounterRxCs};
    const char* pHeading[] = {"DE1 ID", "BPV-EXZ", "CRC", "F-Bit", "REI", "TX-CS", "RX-CS"};
    uint32 (*CounterRead)(AtChannel self, uint16 counterType);
	AtUnused(argc);

	if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    CounterRead = (readingMode == cAtHistoryReadingModeReadToClear) ? AtChannelCounterClear : AtChannelCounterGet;

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        column = 0;
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        for (counter_i = 0; counter_i < mCount(pdhDe1CounterType); counter_i++)
            {
            counterValue = CounterRead((AtChannel)channelList[i], pdhDe1CounterType[counter_i]);
            CliCounterPrintToCell(tabPtr, i, column++, counterValue, cAtCounterTypeError, cAtTrue);
            }
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* Show current alarm states of DS1/E1 */
eBool  CmdPdhDe1AlarmShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32  numChannels, i, column, alarmBitmap;
    AtChannel *channelList;
    tTab    *tabPtr;
    const char *pHeading[] = {"DE1 ID", "LOS", "LOF", "AIS", "RAI", "LOMF", "SigLOF", "SigRAI", "Ebit", "BER-SD", "BER-SF", "BER-TCA"};
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
        column = 0;
       StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

       /* Get alarm state */
       alarmBitmap = AtChannelAlarmGet(channelList[i]);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLos) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmLos) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLof) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmAis) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmAis) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmRai) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmRai) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLomf) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmLomf) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSigLof) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSigLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSigRai) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSigRai) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmEbit)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmEbit)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSdBer)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSdBer)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSfBer)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSfBer)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmBerTca)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmBerTca)   ? cSevCritical : cSevInfo);
       }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe1ListenedAlarmShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32  numChannels, i, column, alarmBitmap;
    AtChannel *channelList;
    tTab    *tabPtr;
    const char *pHeading[] = {"DE1 ID", "Count", "Matched", "LOS", "LOF", "AIS", "RAI", "LOMF", "SigLOF", "SigRAI", "Ebit", "BER-SD", "BER-SF", "BER-TCA"};
    uint32 (*ListenedDefectGet)(AtChannel self, uint32 * listenedCount);
    eAtHistoryReadingMode readingMode;
    uint32 counter;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    ListenedDefectGet = AtChannelListenedDefectGet;
    if (argc > 1)
        {
        /* Get reading action mode */
        readingMode = CliHistoryReadingModeGet(argv[1]);
        if (readingMode == cAtHistoryReadingModeUnknown)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
            return cAtFalse;
            }

        if (readingMode == cAtHistoryReadingModeReadToClear)
            ListenedDefectGet = AtChannelListenedDefectClear;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (i = 0; i < numChannels; i++)
       {
        column = 0;
       StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

       /* Get alarm state */
       alarmBitmap = ListenedDefectGet(channelList[i], &counter);

       StrToCell(tabPtr, i, column++, CliNumber2String(counter, "%u"));
       StrToCell(tabPtr, i, column++, "TBD");

       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLos) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmLos) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLof) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmAis) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmAis) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmRai) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmRai) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLomf) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmLomf) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSigLof) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmSigLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSigRai) ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmSigRai) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmEbit)   ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmEbit)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSdBer)   ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmSdBer)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSfBer)   ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmSfBer)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmBerTca)   ? "Set" : "Clear", (alarmBitmap & cAtPdhDe1AlarmBerTca)   ? cSevCritical : cSevInfo);
       }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* Show interrupt states of DS1/E1 */
eBool  CmdPdhDe1InterruptShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32  numChannels, i, column, alarmBitmap;
    AtChannel *channelList;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    const char *pHeading[] = {"DE1 ID", "LOS", "LOF", "AIS", "RAI", "LOMF", "SigLOF",
                              "SigRAI", "Ebit", "BER-SD", "BER-SF", "InbandLoopCodeChange",
                              "BOMChange", "RpmLBbitChange", "SSMChange", "BER-TCA", "ClockStateChange"};
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Create table with titles */
    if (!silent)
        {
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }
        }

    /* Get data */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
        column = 0;
       StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

       /* Get alarm state */
       if (readingMode == cAtHistoryReadingModeReadToClear)
           alarmBitmap = AtChannelAlarmInterruptClear(channelList[i]);
       else
           alarmBitmap = AtChannelAlarmInterruptGet(channelList[i]);

       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLos) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmLos) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLof) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmAis) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmAis) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmRai) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmRai) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmLomf) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmLomf) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSigLof) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSigLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSigRai) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSigRai) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmEbit)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmEbit)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSdBer)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSdBer)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmSfBer)   ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmSfBer)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDs1AlarmInbandLoopCodeChange)   ? "set" : "clear", (alarmBitmap & cAtPdhDs1AlarmInbandLoopCodeChange)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDs1AlarmBomChange)   ? "set" : "clear", (alarmBitmap & cAtPdhDs1AlarmBomChange)   ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDs1AlarmPrmLBBitChange) ? "set" : "clear", (alarmBitmap & cAtPdhDs1AlarmPrmLBBitChange) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhE1AlarmSsmChange) ? "set" : "clear", (alarmBitmap & cAtPdhE1AlarmSsmChange) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmBerTca) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmBerTca) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1AlarmClockStateChange) ? "set" : "clear", (alarmBitmap & cAtPdhDe1AlarmClockStateChange) ? cSevCritical : cSevInfo);
       }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1Show(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, i, intrMaskVal, sigBitmap, alarmMask;
    AtChannel *channelList;
    eAtPdhDe1FrameType frameMode;
    eAtPdhLoopbackMode loopbackMode;
    uint8 colIdx;
    eAtTimingMode timingType;
    eAtLedState ledState;
    eBool enable;
    eBool result = cAtFalse;
    tTab *tabPtr;
    uint8 idleCode;
    const char *pHeading[] = {"DE1 ID", "FramingMode", "TimingMode", "TimingSource", "clockState", "HwClockState",
                              "Signaling", "DS0List", "LoopbackMode", "Interrupt",
                              "ForcedTxAlarms", "ForcedRxAlarms", "Led State",
                              "Bound", "Enable", "autoRai", "autoAis", "Idle", "IdleCode", "LosDetection"};
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char *timingModeString;
        AtChannel timingSrc;
        AtChannel boundChannel = NULL;

        /* Get ID */
        colIdx = 0;
        StrToCell(tabPtr, i, colIdx++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        /* Frame mode */
        frameMode = AtPdhChannelFrameTypeGet((AtPdhChannel)channelList[i]);
        mAtEnumToStr(cAtPdhDe1FrameModeStr, cAtPdhDe1FrameModeVal, frameMode, m_pBuf, result);
        ColorStrToCell(tabPtr, i, colIdx++, result ? m_pBuf : "Error", result ? cSevInfo : cSevCritical);

        /* Timing mode and Timing source */
        timingType = AtChannelTimingModeGet(channelList[i]);
        timingModeString = CliTimingModeString(timingType);
        ColorStrToCell(tabPtr, i, colIdx++, timingModeString ? timingModeString : "Error", timingModeString ? cSevInfo : cSevCritical);

        /* Timing source */
        timingSrc = AtChannelTimingSourceGet((AtChannel)channelList[i]);
        StrToCell(tabPtr, i, colIdx++, timingSrc ? CliChannelIdStringGet(timingSrc) : "none");

        /* Clock state */
        StrToCell(tabPtr, i, colIdx++, CliClockStateString(AtChannelClockStateGet(channelList[i])));
        AtSprintf(m_pBuf, "%d", AtChannelHwClockStateGet(channelList[i]));
        StrToCell(tabPtr, i, colIdx++, m_pBuf);

        /* Signaling */
        enable = AtPdhDe1SignalingIsEnabled((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, colIdx++, (enable) ? "en" : "dis", (enable) ? cSevMajor : cSevInfo);

        sigBitmap = AtPdhDe1SignalingDs0MaskGet((AtPdhDe1)channelList[i]);
        AtSprintf(m_pBuf, "%s", (sigBitmap) ? (CliNxDs0Bitmap2String(sigBitmap)) : "None");
        ColorStrToCell(tabPtr, i, colIdx++, m_pBuf, (enable) ? cSevMajor : cSevInfo);

        /* Loopback mode */
        loopbackMode = AtChannelLoopbackGet((AtChannel)channelList[i]);
        StrToCell(tabPtr, i, colIdx++, CliPdhLoopbackModeString(loopbackMode));

        /* Interrupt mask */
        intrMaskVal = AtChannelInterruptMaskGet(channelList[i]);
        AtSprintf(m_pBuf, "%s", CliPdhDe1AlarmStrFromMask(intrMaskVal));
        ColorStrToCell(tabPtr, i, colIdx++, (intrMaskVal) ? m_pBuf : "None", (intrMaskVal) ? cSevCritical : cSevInfo);

        /* Forced Alarm */
        alarmMask = AtChannelTxForcedAlarmGet((AtChannel)channelList[i]);
        AtStrcpy(m_pBuf, CliPdhDe1AlarmStrFromMask(alarmMask));
        ColorStrToCell(tabPtr, i, colIdx++, (alarmMask) ? m_pBuf : "None", (alarmMask) ? cSevCritical : cSevInfo);

        alarmMask = AtChannelRxForcedAlarmGet((AtChannel)channelList[i]);
        AtStrcpy(m_pBuf, CliPdhDe1AlarmStrFromMask(alarmMask));
        ColorStrToCell(tabPtr, i, colIdx++, (alarmMask) ? m_pBuf : "None", (alarmMask) ? cSevCritical : cSevInfo);

        /* Led State */
        ledState = AtPdhChannelLedStateGet((AtPdhChannel)channelList[i]);
        ColorStrToCell(tabPtr, i, colIdx++, CliLedStateStr(ledState), cSevInfo);

        /* Bound encap channel */
        boundChannel = (AtChannel)AtChannelBoundChannelGet(channelList[i]);
        if (boundChannel)
            AtSprintf(m_pBuf, "%s", CliChannelIdStringGet(boundChannel));
        ColorStrToCell(tabPtr, i, colIdx++, (boundChannel) ? m_pBuf : "None", boundChannel ? cSevInfo : cSevNormal);

        enable = AtChannelIsEnabled(channelList[i]);
        mAtBoolToStr(enable, m_pBuf, result);
        ColorStrToCell(tabPtr, i, colIdx++, result ? m_pBuf : "Error", enable ? cSevInfo : cSevCritical);

        /* Auto RAI */
        enable = AtPdhChannelAutoRaiIsEnabled((AtPdhChannel)channelList[i]);
        ColorStrToCell(tabPtr, i, colIdx++, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);

        /* Auto AIS */
        enable = AtPdhDe1AutoTxAisIsEnabled((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, colIdx++, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);

        /* IDLE */
        enable = AtPdhChannelIdleIsEnabled((AtPdhChannel)channelList[i]);
        ColorStrToCell(tabPtr, i, colIdx++,  CliBoolToString(enable), cSevInfo);

        /* IDLE CODE */
        idleCode = AtPdhChannelIdleCodeGet((AtPdhChannel)channelList[i]);
        StrToCell(tabPtr, i, colIdx++,  CliNumber2String(idleCode, "0x%x"));

        /* LOS detection */
        enable = AtPdhChannelLosDetectionIsEnabled((AtPdhChannel)channelList[i]);
        ColorStrToCell(tabPtr, i, colIdx++,  CliBoolToString(enable), enable ? cSevInfo : cSevNormal);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1Enable(char argc, char **argv)
    {
    return De1Enable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe1Disable(char argc, char **argv)
    {
    return De1Enable(argc, argv, cAtFalse);
    }

/* SET DS1/E1 Led State */
eBool CmdPdhDe1LedStateSet(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet cliRet = cAtOk;
    uint32 numChannels, i;
    AtChannel *channelList;
    eAtLedState ledState;
    uint8 idx = 0;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[idx++], channelList, numChannels);

    /* Get LED state */
    ledState = CliLedStateFromStr(argv[idx]);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       cliRet = AtPdhChannelLedStateSet((AtPdhChannel)channelList[i], ledState);
       if (cliRet != cAtOk)
           AtPrintc(cSevCritical,
                    "ERROR: Cannot change LED state of %s to %s, ret = %s\r\n",
                    CliChannelIdStringGet((AtChannel)channelList[i]),
                    CliLedStateStr(ledState),
                    AtRet2String(cliRet));
       }

    return cAtTrue;
    }

eBool CmdDe1MapHierarchyShow(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    AtChannelHierarchyDisplayer displayer;
    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);

    displayer = AtPdhDe1HierarchyDisplayerGet(cAtChannelHierarchyModeAlarm, 0);
    for (i = 0; i < numChannels; i++)
        {
        if (channelList[i] == NULL)
            continue;

        AtChannelHierarchyShow(displayer, channelList[i], "");
        }

    return cAtTrue;
    }

eBool CmdDe1Debug(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrintc(cSevInfo, "===========================================\r\n");
        AtPrintc(cSevInfo, "= %s\r\n", CliChannelIdStringGet(channelList[i]));
        AtPrintc(cSevInfo, "===========================================\r\n");
        AtChannelDebug(channelList[i]);
        AtPrintc(cSevInfo, "\r\n");
        }

    return cAtTrue;
    }

eBool CmdPdhDe1ErrorForce(char argc, char** argv)
    {
    uint32 numChannels, i;
    eAtPdhDe1CounterType errorType = 0;
    AtChannel *channelList;
    eBool convertSuccess;
    eAtRet (*Force)(AtChannel, uint32);
    eBool success = cAtTrue;
    eBool force = cAtFalse;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get error type */
    mAtStrToEnum(cAtPdhDe1CounterTypeStr, cAtPdhDe1CounterTypeVal, argv[1], errorType, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid error type. Expect: bpv/crc/fe/rei\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], force, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling, expected: en/dis\r\n");
        return cAtFalse;
        }

    /* Determine correct function to force */
    Force = force ? AtChannelTxErrorForce : AtChannelTxErrorUnForce;

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = Force((AtChannel)channelList[i], errorType);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not force error");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe1AlarmCapture(char argc, char **argv)
    {
    uint32 numberOfDe1, i;
    AtChannel *de1s;
    static tAtChannelEventListener intrListener;
    eBool convertSuccess;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    de1s = CliSharedChannelListGet(&numberOfDe1);
    numberOfDe1 = De1ListFromString(argv[0], de1s, numberOfDe1);
    if (!numberOfDe1)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    if (AutotestIsEnabled())
        intrListener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        intrListener.AlarmChangeState = AlarmChangeLogging;
    else
        intrListener.AlarmChangeState = AlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    /* Register Interrupt listener */
    for (i = 0; i < numberOfDe1; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = cAtOk;

        /* Register/deregister alarm listener */
        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(de1s[i], &intrListener, CliAtPdhDe1ObserverGet(de1s[i]));
            else
                ret = AtChannelEventListenerAdd(de1s[i], &intrListener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(de1s[i], &intrListener);

            if (AutotestIsEnabled())
                CliAtPdhDe1ObserverDelete(de1s[i]);
            }

        /* Let user know if error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener on %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(de1s[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe1AutoRaiEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelAutoRaiEnable, cAtTrue);
    }

eBool CmdPdhDe1AutoRaiDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelAutoRaiEnable, cAtFalse);
    }

eBool CmdPdhDe1IdleCodeSet(char argc, char **argv)
    {
    return Uint8AttributeSet(argc, argv, AtPdhChannelIdleCodeSet);
    }

eBool CmdPdhDe1IdleEnable(char argc, char **argv)
    {
    return Enable(argc, argv, AtPdhChannelIdleEnable, cAtTrue);
    }

eBool CmdPdhDe1IdleDisable(char argc, char **argv)
    {
    return Enable(argc, argv, AtPdhChannelIdleEnable, cAtFalse);
    }

eBool CmdPdhDe1DataLinkEnable(char argc, char **argv)
    {
    return Enable(argc, argv, AtPdhChannelDataLinkEnable, cAtTrue);
    }

eBool CmdPdhDe1DataLinkDisable(char argc, char **argv)
    {
    return Enable(argc, argv, AtPdhChannelDataLinkEnable, cAtFalse);
    }

eBool CmdPdhDe1DataLinkSabitsMaskSet(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet ret;
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 saBitsMask;
    eBool  success;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical,
                 "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get Sa-BIts Mask Value from Strings */
    saBitsMask = AtStrToDw(argv[1]);

    /* Apply */
    success = cAtTrue;
    ret = cAtOk;
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhDe1DataLinkSaBitsMaskSet((AtPdhDe1) channelList[i], (uint8) saBitsMask);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            mWarnNotReturnCliIfNotOk(ret,
                                     channelList[i],
                                     "Can not set sa-bits mask");
            }
        }

    return success;
    }

eBool CmdPdhDe1DataLinkNotificationEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return DataLinkNotification(argv[0], cAtTrue);
    }

eBool CmdPdhDe1DataLinkNotificationDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return DataLinkNotification(argv[0], cAtFalse);
    }

eBool CmdPdhDe1DataLinkMsgSend(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet ret;
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool sucess = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical,
                 "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhChannelDataLinkSend((AtPdhChannel) channelList[i],
                                         (uint8*) argv[1],
                                         AtStrlen(argv[1]));
        if (ret != cAtOk)
            {
            sucess = cAtFalse;
            mWarnNotReturnCliIfNotOk(ret,
                                     channelList[i],
                                     "Can not send DLK message");
            }
        }
    return sucess;
    }

eBool CmdPdhDe1DataLinkShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool enable;
    uint8 saBitsMask;
    tTab *tabPtr;
    const char *pHeading[] =
        {
        "DE1 ID", "Enable", "Sa-Bit", "FCS Bit Order"
        };


    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical,
                 "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 collum = 0;
        const char *stringValue;
        StrToCell(tabPtr,
                  i,
                  collum++,
                  (char *) CliChannelIdStringGet((AtChannel) channelList[i]));
        enable = AtPdhChannelDataLinkIsEnabled((AtPdhChannel) channelList[i]);
        ColorStrToCell(tabPtr, i, collum++, CliBoolToString(enable), enable ? cSevInfo : cSevNormal);

        if (enable)
            {
            if (AtPdhDe1SaBitsIsApplicable((AtPdhDe1)channelList[i]))
                {
                saBitsMask = AtPdhDe1DataLinkSaBitsMaskGet((AtPdhDe1) channelList[i]);
                StrToCell(tabPtr, i, collum++, CliNumber2String((uint32) saBitsMask, "0x%x"));
                }
            else
                {
                ColorStrToCell(tabPtr, i, collum++, sAtNotApplicable, cSevNormal);
                }

            stringValue = CliBitOrderModeString(AtPdhChannelDataLinkFcsBitOrderGet((AtPdhChannel)channelList[i]));
            ColorStrToCell(tabPtr, i, collum, stringValue ? stringValue : "Error", stringValue ? cSevInfo : cSevCritical);
            }
        else
            {
            ColorStrToCell(tabPtr, i, collum++, sAtNotApplicable, cSevNormal);
            ColorStrToCell(tabPtr, i, collum++, sAtNotApplicable, cSevNormal);
            }
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1DataLinkCountersShow(char argc, char **argv)
    {
	eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    uint32 (*CounterGet)(AtPdhChannel self, uint32 counterType) = AtPdhChannelDataLinkCounterGet;

    AtUnused(argc);

    if (argc < 2)
       {
       AtPrintc(cSevCritical, "ERROR: Not enough parameter");
       return cAtFalse;
       }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtPdhChannelDataLinkCounterClear;

    return DataLinkCountersShow(argv[0], CounterGet, silent);
    }

eBool CmdPdhDe1LoopcodeEnable(char argc, char **argv)
    {
    return LoopCodeEnableSet(argc, argv, cAtTrue, AtPdhDe1LoopcodeEnable);
    }

eBool CmdPdhDe1LoopcodeDisable(char argc, char **argv)
    {
    return LoopCodeEnableSet(argc, argv, cAtFalse, AtPdhDe1LoopcodeEnable);
    }

eBool CmdPdhDe1TxLoopcode(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet ret;
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 loopcode;
    eBool sucess;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get loop code */
    loopcode = CliStringToEnum(argv[1], cAtPdhDe1LoopcodeStr, cAtPdhDe1LoopcodeVal, mCount(cAtPdhDe1LoopcodeVal), &sucess);
    if (sucess == cAtFalse)
        {
        CliExpectedValuesPrint(cSevCritical, cAtPdhDe1LoopcodeStr, mCount(cAtPdhDe1LoopcodeVal));
        AtPrintc(cSevCritical, "\r\n");
        return sucess;
        }

    /* Apply */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhDe1TxLoopcodeSet((AtPdhDe1) channelList[i], loopcode);
        if (ret != cAtOk)
            {
            sucess = cAtFalse;
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set loop code");
            }

        if (CliTimeLoggingIsEnabled())
            EventLogging((AtChannel)channelList[i], "Force loop-code", argv[1]);
        }

    return sucess;
    }

eBool CmdPdhDe1LoopCodeShow(char argc, char **argv)
    {
    uint32  numChannels, i;
    AtChannel *channelList;
    tTab    *tabPtr;
    const char *pHeading[] = {"DE1 ID", "TxLoopCode", "RxLoopCode"};
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 column = 0;
        uint32 loopcode;
        const char *rxstring;
        const char *txstring;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

        loopcode = AtPdhDe1TxLoopcodeGet((AtPdhDe1)channelList[i]);
        txstring = CliEnumToString(loopcode, cAtPdhDe1LoopcodeStr, cAtPdhDe1LoopcodeVal, mCount(cAtPdhDe1LoopcodeVal), NULL);
        StrToCell(tabPtr, i, column++, txstring);

        loopcode = AtPdhDe1RxLoopcodeGet((AtPdhDe1)channelList[i]);
        rxstring = CliEnumToString(loopcode, cAtPdhDe1LoopcodeStr, cAtPdhDe1LoopcodeVal, mCount(cAtPdhDe1LoopcodeVal), NULL);
        StrToCell(tabPtr, i, column++, rxstring);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe1BomShow(char argc, char **argv)
    {
    uint32  numChannels, i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE1 ID", "TxBom", "RxBom", "RxCurrentBom", "TxCfgSentTimes", "Max TxCfgSentTimes"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 value;
        uint32 nTimes;
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

        value = AtPdhDe1TxBomGet((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(value, "0x%x"), cSevInfo);

        value = AtPdhDe1RxBomGet((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(value, "0x%x"), cSevInfo);

        value = AtPdhDe1RxCurrentBomGet((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(value, "0x%x"), cSevInfo);

        nTimes = AtPdhDe1TxBomSentCfgNTimes((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(nTimes, "%d"), cSevInfo);

        nTimes = AtPdhDe1TxBomSentMaxNTimes((AtPdhDe1)channelList[i]);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(nTimes, "%d"), cSevInfo);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1TxBom(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool sucess = cAtTrue;
    uint8 value;
    eAtPdhBomSentMode sentMode = 0;
    eBool shouldUseSentMode = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough params \n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get message */
    value = (uint8)AtStrtoul(argv[1], NULL, 16);

    if (argc == 3)
        {
        shouldUseSentMode = cAtTrue;
        sentMode = CliStringToEnum(argv[2], cAtPdhDe1TxBomModeStr, cAtPdhDe1TxBomModeVal, sizeof(cAtPdhDe1TxBomModeVal), &sucess);
        if (!sucess)
            {
            AtPrintc(cSevCritical, "Invalid sent mode \n");
            return sucess;
            }
        }

    /* Apply */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = cAtOk;
        if (!shouldUseSentMode)
            ret = AtPdhDe1TxBomSet((AtPdhDe1) channelList[i], value);
        else
            ret = AtPdhDe1TxBomWithModeSet((AtPdhDe1) channelList[i], value, sentMode);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Send BOM fail on %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
            sucess = cAtFalse;
            }
        }

    return sucess;
    }

eBool CmdPdhDe1TxBomNtimes(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, i, nTimes;
    AtChannel *channelList;
    eBool sucess = cAtTrue;
    uint8 value;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get message */
    value = (uint8)AtStrtoul(argv[1], NULL, 16);
    nTimes = (uint32)AtStrtoul(argv[2], NULL, 10);

    /* Apply */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtPdhDe1TxBomNTimesSend((AtPdhDe1) channelList[i], value, nTimes);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Send BOM fail on %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
            sucess = cAtFalse;
            }
        }

    return sucess;
    }

eBool CmdPdhDe1LogShow(char argc, char** argv)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        AtChannelLogShow(channelList[i]);

    return cAtTrue;
    }

eBool CmdPdhDe1LogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe1LogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

eBool CmdPdhDe1PrmStandardSet(char argc, char **argv)
    {
    return PrmStandardSet(argc, argv, AtPdhDe1PrmStandardSet);
    }

eBool  CmdPdhDe1PrmShow(char argc, char **argv)
    {
    uint32  numChannels, channel_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE1 ID", "txStandard", "rxStandard", "enabled", "txCr", "expCr", "txLb", "rxLb"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 value;
        uint8 column = 0;
        const char *string;

        StrToCell(tabPtr, channel_i, column++, CliChannelIdStringGet((AtChannel)channelList[channel_i]));

        value = AtPdhDe1PrmTxStandardGet((AtPdhDe1)channelList[channel_i]);
        string = PrmStandard2String(value);
        ColorStrToCell(tabPtr, channel_i, column++, string ? string : "Error", string ? cSevInfo : cSevCritical);

        value = AtPdhDe1PrmRxStandardGet((AtPdhDe1)channelList[channel_i]);
        string = PrmStandard2String(value);
        ColorStrToCell(tabPtr, channel_i, column++, string ? string : "Error", string ? cSevInfo : cSevCritical);

        ColorStrToCell(tabPtr, channel_i, column++, CliBoolToString(AtPdhDe1PrmIsEnabled((AtPdhDe1)channelList[channel_i])), cSevInfo);
        ColorStrToCell(tabPtr, channel_i, column++, CliNumber2String(AtPdhDe1PrmTxCRBitGet((AtPdhDe1)channelList[channel_i]), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, channel_i, column++, CliNumber2String(AtPdhDe1PrmExpectedCRBitGet((AtPdhDe1)channelList[channel_i]), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, channel_i, column++, CliNumber2String(AtPdhDe1PrmTxLBBitGet((AtPdhDe1)channelList[channel_i]), "%u"), cSevInfo);
        ColorStrToCell(tabPtr, channel_i, column++, CliNumber2String(AtPdhDe1PrmRxLBBitGet((AtPdhDe1)channelList[channel_i]), "%u"), cSevInfo);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1TxSsm(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet  ret = cAtOk;
    uint32 numChannels, i;
    AtChannel *channelList;
    uint8 value;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get data */
    value = (uint8)AtStrtoul(argv[1], NULL, 16);
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhChannelTxSsmSet((AtPdhChannel)channelList[i], value);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set value");
        }

    return cAtTrue;
    }

eBool CmdPdhDe1TxSlipBufferEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelTxSlipBufferEnable, cAtTrue);
    }

eBool CmdPdhDe1TxSlipBufferDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelTxSlipBufferEnable, cAtFalse);
    }

eBool CmdPdhDe1SlipBufferShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, de1_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE1 ID", "txEnabled", "rxEnabled"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (de1_i = 0; de1_i < numChannels; de1_i = AtCliNextIdWithSleep(de1_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eBool enable;
        uint8 column = 0;

        StrToCell(tabPtr, de1_i, column++, CliChannelIdStringGet((AtChannel)channelList[de1_i]));

        /* enable */
        enable = AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)channelList[de1_i]);
        ColorStrToCell(tabPtr, de1_i, column++,  CliBoolToString(enable), enable ? cSevInfo : cSevNormal);

        enable = AtPdhChannelRxSlipBufferIsEnabled((AtPdhChannel)channelList[de1_i]);
        ColorStrToCell(tabPtr, de1_i, column++,  CliBoolToString(enable), enable ? cSevInfo : cSevNormal);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1SsmEnable(char argc, char **argv)
    {
    return Enable(argc, argv, AtPdhChannelSsmEnable, cAtTrue);
    }

eBool CmdPdhDe1SsmDisable(char argc, char **argv)
    {
    return Enable(argc, argv, AtPdhChannelSsmEnable, cAtFalse);
    }

eBool CmdPdhDe1SsmSaMaskSet(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet  ret = cAtOk;
    uint32 numChannels, channel_i;
    AtChannel *channelList;
    uint32 value;
    eBool result;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    mAtStrToEnum(cAtPdhDe1SaStr, cAtPdhDe1SaVal, argv[1], value, result);
    if(!result)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter. Expected: en/dis\r\n");
        return cAtFalse;
        }

    result = cAtTrue;
    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhDe1SsmSaBitsMaskSet((AtPdhDe1)channelList[channel_i], (uint8)value);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[channel_i], "Can not set value");
            result = cAtFalse;
            }
        }

    return result;
    }

eBool CmdPdhDe1SsmShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, de1_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE1 ID", "enabled", "saMask", "txSsm", "rxSsm" };

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (de1_i = 0; de1_i < numChannels; de1_i = AtCliNextIdWithSleep(de1_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPdhChannel channel = (AtPdhChannel)channelList[de1_i];
        eBool enable;
        uint8 value;
        uint8 colIdx = 0;

        StrToCell(tabPtr, de1_i, colIdx++, CliChannelIdStringGet((AtChannel)channel));

        /* enable */
        enable = AtPdhChannelSsmIsEnabled(channel);
        ColorStrToCell(tabPtr, de1_i, colIdx++,  CliBoolToString(enable), enable ? cSevInfo : cSevNormal);

        /* SaBitMask */
        if (AtPdhChannelFrameTypeGet(channel) == cAtPdhE1MFCrc)
            {
            value = AtPdhDe1SsmSaBitsMaskGet((AtPdhDe1)channel);
            StrToCell(tabPtr, de1_i, colIdx++, SaBitMaskString(value));
            }
        else
            StrToCell(tabPtr, de1_i, colIdx++, sAtNotApplicable);

        /* Tx message */
        value = AtPdhChannelTxSsmGet(channel);
        StrToCell(tabPtr, de1_i, colIdx++,  CliNumber2String(value, "0x%x"));

        /* Rx message */
        value = AtPdhChannelRxSsmGet(channel);
        StrToCell(tabPtr, de1_i, colIdx,  CliNumber2String(value, "0x%x"));
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPdhDe1PrmTxStandardSet(char argc, char **argv)
    {
    return PrmStandardSet(argc, argv, AtPdhDe1PrmTxStandardSet);
    }

eBool CmdPdhDe1PrmStandardReceivedSet(char argc, char **argv)
    {
    return PrmStandardSet(argc, argv, AtPdhDe1PrmRxStandardSet);
    }

eBool CmdAtPdhDe1PrmEnable(char argc, char **argv)
    {
    return PrmEnable(argc, argv, AtPdhDe1PrmEnable, cAtTrue);
    }

eBool CmdAtPdhDe1PrmDisable(char argc, char **argv)
    {
    return PrmEnable(argc, argv, AtPdhDe1PrmEnable, cAtFalse);
    }

eBool CmdAtPdhDe1PrmTxCRBitSet(char argc, char **argv)
    {
    return Uint8AttributeSet(argc, argv, (eAtModulePdhRet (*)(AtPdhChannel, uint8))AtPdhDe1PrmTxCRBitSet);
    }

eBool CmdAtPdhDe1PrmTxMessageLBBitSet(char argc, char **argv)
    {
    return Uint8AttributeSet(argc, argv, (eAtModulePdhRet (*)(AtPdhChannel, uint8))AtPdhDe1PrmTxLBBitSet);
    }

eBool CmdAtPdhDe1PrmCRBitExpectSet(char argc, char **argv)
    {
    return Uint8AttributeSet(argc, argv, (eAtModulePdhRet (*)(AtPdhChannel, uint8))AtPdhDe1PrmExpectedCRBitSet);
    }

eBool CmdAtPdhDe1AlarmCaptureShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    const char *pHeading[] = {"DE1 ID", "LOS", "LOF", "AIS", "RAI", "LOMF", "BER-SF", "BER-SD", "SigLOF",
                              "SigRAI", "Ebit", "BOMChange", "InbandLoopCodeChange", "RpmLBbitChange",
                              "SSMChange", "BER-TCA", "ClockStateChange"};

    eAtHistoryReadingMode readMode;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channelList[i];
        AtChannelObserver observer = CliAtPdhDe1ObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmLos);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmLof);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmAis);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmRai);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmLomf);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmSfBer);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmSdBer);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmSigLof);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmSigRai);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmEbit);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs1AlarmBomChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs1AlarmInbandLoopCodeChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs1AlarmPrmLBBitChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhE1AlarmSsmChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe1AlarmBerTca);

        /* Print clock states recorded. */
        StrToCell(tabPtr, i, column++, AtChannelObserverClockStateString(observer));
        if (readMode == cAtHistoryReadingModeReadToClear)
            AtchannelObserverClockStateFlush(observer);

        /* Delete observer */
        CliAtPdhDe1ObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1LosDetectionEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelLosDetectionEnable, cAtTrue);
    }

eBool CmdPdhDe1LosDetectionDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelLosDetectionEnable, cAtFalse);
    }

eBool CmdPdhDe1InterruptClear(char argc, char **argv)
    {
    uint32 numChannels, defects, channel_i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Defect to clear */
    defects = CliPdhIntrMaskFromString(argv[1]);

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        AtChannelSpecificDefectInterruptClear(channelList[channel_i], defects);

    return cAtTrue;
    }

eBool CmdPdhDe1DataLinkFcsBitOrderSet(char argc, char **argv)
    {
    eAtBitOrder bitOrder = CliBitOrderFromString(argv[1]);

    if (bitOrder == cAtBitOrderUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid bit order: %s\r\n", CliValidBitOrderModes());
        return cAtFalse;
        }

    return AttributeSet(argc, argv, bitOrder, mAttributeSetFunc(AtPdhChannelDataLinkFcsBitOrderSet));
    }

eBool CmdAtPdhDe1AutoTxAisEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhDe1AutoTxAisEnable, cAtTrue);
    }

eBool CmdAtPdhDe1AutoTxAisDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhDe1AutoTxAisEnable, cAtFalse);
    }

eBool CmdPdhDe1AlarmAffectingEnable(char argc, char **argv)
    {
    return AlarmAffectingEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe1AlarmAffectingDisable(char argc, char **argv)
    {
    return AlarmAffectingEnable(argc, argv, cAtFalse);
    }

eBool CmdPdhDe1AlarmAffectGet(char argc, char **argv)
    {
    uint32 numChannels, i, j;
    tTab *tabPtr;
    char buff[16];
    AtChannel *channelList;
    eBool  isEnable;
    const char *pHeading[] = {"DE1 ID", "LOS", "AIS", "LOF", "RAI", "LOMF"};
    const uint32 cCmdPdhDe1AlarmValues[] = {cAtPdhDe1AlarmLos,
                                            cAtPdhDe1AlarmAis,
                                            cAtPdhDe1AlarmLof,
                                            cAtPdhDe1AlarmRai,
                                            cAtPdhDe1AlarmLomf};
    AtUnused(argc);


    /* Get list of path */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        /* Print alarm status */
        for (j = 0; j < mCount(cCmdPdhDe1AlarmValues); j++)
            {
            isEnable = AtPdhChannelAlarmAffectingIsEnabled((AtPdhChannel)channelList[i], cCmdPdhDe1AlarmValues[j]);
            AtSprintf(buff, "%s", (isEnable ? "en" : "dis"));
            ColorStrToCell(tabPtr, i, j+1, buff, isEnable ? cSevNormal : cSevCritical);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1RxSlipBufferEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelRxSlipBufferEnable, cAtTrue);
    }

eBool CmdPdhDe1RxSlipBufferDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelRxSlipBufferEnable, cAtFalse);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : CliAtPdhErrorGenerator.h
 * 
 * Created Date: Mar 24, 2016
 *
 * Description : PDH error generator CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATPDHERRORGENERATOR_H_
#define _CLIATPDHERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePdh.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef AtErrorGenerator (*tErrorGeneratorGet)(AtPdhChannel channel);
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannel De3ChannelFromString(char* argv);
AtChannel De1ChannelFromString(char* argv);
eBool CliPdhErrorGeneratorErrorTypeSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);
eBool CliPdhErrorGeneratorModeSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);
eBool CliPdhErrorGeneratorErrorNumSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);
eBool CliPdhErrorGeneratorErrorRateSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);
eBool CliPdhErrorGeneratorStart(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);
eBool CliPdhErrorGeneratorStop(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);
eBool CliPdhErrorGeneratorShow(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATPDHERRORGENERATOR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhIdParser.c
 *
 * Created Date: Dec 7, 2013
 *
 * Description : PDH ID parser
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhSerialLine.h"
#include "AtPdhDe3.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumLevels 5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe3 Ec1Stm0VcDe3(uint32 ecId)
    {
    AtSdhLine line = (AtSdhLine)AtModuleSdhLineGet(CliModuleSdh(), (uint8)(ecId));
    AtSdhVc vc3 = AtSdhLineVc3Get(line, 0, 0);
    return (AtPdhDe3)AtSdhChannelMapChannelGet((AtSdhChannel)vc3);
    }

static AtPdhDe3 CliLiuDe3(uint32 id)
    {
    return AtModulePdhDe3Get(CliModulePdh(), id);
    }

static AtPdhSerialLine CliSerLineWithSdhId(uint32 id)
    {
    uint32 startEc1LineId = AtModuleSdhStartEc1LineIdGet(CliModuleSdh());
    if (id < startEc1LineId)
        return NULL;

    return (AtPdhSerialLine)AtModulePdhDe3SerialLineGet(CliModulePdh(), (uint32)(id - startEc1LineId));
    }

static AtPdhDe3 LiuOrEc1De3FromId(uint32 id)
    {
    AtPdhSerialLine serialLine = CliSerLineWithSdhId(id);
    if (serialLine && (AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeEc1))
        return Ec1Stm0VcDe3(id);

    return CliLiuDe3(id);
    }

static AtIdParser ThreeLevelIdParserCreate(char *pStrIdList)
    {
    char maxFormat[16];
    char minFormat[16];
    AtIdParser idParser;
    AtModuleSdh moduleSdh = CliModuleSdh();

    /* Try creating parser with EC1 ID */
    AtSprintf(maxFormat, "%u.7.4", CliAtModuleSdhMaxEc1LineIdGet(moduleSdh));
    AtSprintf(minFormat, "%u.1.1", CliAtModuleSdhStartEc1LineIdGet(moduleSdh));

    idParser = AtIdParserNew(pStrIdList, minFormat, maxFormat);
    if (idParser)
        return idParser;

    /* Try creating parser with LIU ID */
    AtSprintf(maxFormat, "%u.7.4", AtModulePdhNumberOfDe3sGet(CliModulePdh()));
    AtSprintf(minFormat, "1.1.1");
    return AtIdParserNew(pStrIdList, minFormat, maxFormat);
    }

static AtPdhDe3 Stm0De3FromId(uint32 id)
	{
	AtSdhLine line = (AtSdhLine)AtModuleSdhLineGet(CliModuleSdh(), (uint8)(id));
	if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
		return Ec1Stm0VcDe3(id);
	return NULL;
	}

static uint32 ThreeLevelDe1ListFromDotString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 i, numChannels;
    uint8 numLevel = 3;

    AtIdParser idParser = ThreeLevelIdParserCreate(pStrIdList);
    if (idParser == NULL)
        return 0;

    numChannels = 0;
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / numLevel); i++)
        {
        uint32 levels[cMaxNumLevels];
        AtPdhDe3 de3;

        CliIdBuild(idParser, levels, numLevel);
        de3 = LiuOrEc1De3FromId(CliId2DriverId(levels[0]));

        if (de3 == NULL)
        	de3 = Stm0De3FromId(CliId2DriverId(levels[0]));

        if (de3)
            channels[numChannels] = (AtChannel)AtPdhDe3De1Get(de3, (uint8)CliId2DriverId(levels[1]), (uint8)CliId2DriverId(levels[2]));
        else
            {
            AtSdhLine ec1Line = AtModuleSdhLineGet(CliModuleSdh(), (uint8)CliId2DriverId(levels[0]));
			AtSdhVc vc1x = AtSdhLineVc1xGet(ec1Line, 0, 0, (uint8)CliId2DriverId(levels[1]), (uint8)CliId2DriverId(levels[2]));

			if (vc1x == NULL)
                continue;

			channels[numChannels] = AtSdhChannelMapChannelGet((AtSdhChannel)vc1x);
            }

        if (channels[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);
    return numChannels;
    }

static AtIdParser TwoLevelIdParserCreate(char *pStrIdList)
    {
    char maxFormat[16];
    char minFormat[16];
    AtIdParser idParser;
    AtModuleSdh moduleSdh = CliModuleSdh();

    /* Try creating parser with EC1 ID */
    AtSprintf(maxFormat, "%u.7", CliAtModuleSdhMaxEc1LineIdGet(moduleSdh));
    AtSprintf(minFormat, "%u.1", CliAtModuleSdhStartEc1LineIdGet(moduleSdh));

    idParser = AtIdParserNew(pStrIdList, minFormat, maxFormat);
    if (idParser)
        return idParser;

    /* Try creating parser with LIU ID */
    AtSprintf(maxFormat, "%u.7", AtModulePdhNumberOfDe3sGet(CliModulePdh()));
    AtSprintf(minFormat, "1.1");
    return AtIdParserNew(pStrIdList, minFormat, maxFormat);
    }

static uint32 TwoLevelsDe2ListFromDotString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 i, numChannels;
    uint8 numLevel = 2;

    AtIdParser idParser = TwoLevelIdParserCreate(pStrIdList);
    if (idParser == NULL)
        return 0;

    numChannels = 0;
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / numLevel); i++)
        {
        uint32 levels[cMaxNumLevels];
        AtPdhDe3 de3;

        CliIdBuild(idParser, levels, numLevel);
        de3 = LiuOrEc1De3FromId(CliId2DriverId(levels[0]));

        if (de3 == NULL)
        	de3 = Stm0De3FromId(CliId2DriverId(levels[0]));

        if (de3 == NULL)
            continue;

        channels[numChannels] = (AtChannel)AtPdhDe3De2Get(de3, (uint8)CliId2DriverId(levels[1]));
        if (channels[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);
    return numChannels;
    }

static uint32 De1ListFromDotString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    if (CliAtNumIdLevel(pStrIdList) == 3)
        return ThreeLevelDe1ListFromDotString(pStrIdList, channels, bufferSize);

    return AtSdhDe1ListFromString(pStrIdList, channels, bufferSize);
    }

static uint32 De2ListFromDotString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    if (CliAtNumIdLevel(pStrIdList) == 2)
        return TwoLevelsDe2ListFromDotString(pStrIdList, channels, bufferSize);

    return AtSdhDe2ListFromString(pStrIdList, channels, bufferSize);
    }

uint32 De1ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numChannels, channel_i, numValidChannels;

    if (pStrIdList == NULL)
        return 0;

    /* If there is a '.' in ID list, these DS1s/E1s are mapped to VC-1x */
    if (AtStrchr(pStrIdList, '.'))
        return De1ListFromDotString(pStrIdList, channels, bufferSize);

    /* It is just a list of flat ID */
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(pStrIdList, idBuffer, numChannels);
    numValidChannels = 0;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        channels[numValidChannels] = (AtChannel)AtModulePdhDe1Get(CliModulePdh(), idBuffer[channel_i] - 1);
        if (channels[numValidChannels] == NULL)
            continue;
            
        numValidChannels++;
        if (numValidChannels > bufferSize)
            break;
        }

    return numValidChannels;
    }

uint32 CliDe2ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    if (pStrIdList == NULL)
        return 0;

    /* If there is a '.' in ID list, these DS2s/E2s are mapped to DS3/E3 */
    if (AtStrchr(pStrIdList, '.'))
        return De2ListFromDotString(pStrIdList, channels, bufferSize);

    return 0;
    }

static uint32 VcDe3ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32    numVc3;
    AtChannel mappedDe3;
    uint32    numChannels, i;

    numVc3 = AtSdhVc3ListFromString(pStrIdList, channels, bufferSize);
    if (numVc3 == 0)
        return 0;

    /* Return all mapped DS3/E3 */
    numChannels = 0;
    for (i = 0; i < numVc3; i++)
        {
        if (AtSdhChannelMapTypeGet((AtSdhChannel)channels[i]) != cAtSdhVcMapTypeVc3MapDe3)
            continue;

        mappedDe3 = AtSdhChannelMapChannelGet((AtSdhChannel)channels[i]);
        if (mappedDe3 == NULL)
            continue;

        /* Put this DS3/E3 to the list */
        channels[numChannels] = mappedDe3;
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

static uint32 LiuOrEc1De3ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 numIds, i;
    uint32 *idBuffer = CliSharedIdBufferGet(&numIds);
    uint32 numChannels = 0;

    numIds = CliIdListFromString(pStrIdList, idBuffer, numIds);
    for (i = 0; i < numIds; i++)
        {
        uint8 id = (uint8)(idBuffer[i] - 1);
        AtChannel channel = (AtChannel)LiuOrEc1De3FromId(id);
        if (channel == NULL)
        	channel = (AtChannel)Stm0De3FromId(id);

        if (channel == NULL)
            continue;

        channels[numChannels] = channel;
        numChannels = numChannels + 1;

        if (numChannels == bufferSize)
            break;
        }

    return numChannels;
    }

uint32 CliDe3ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    if (pStrIdList == NULL)
        return 0;

    /* If there is a '.' in ID list, these DS3s/E3s are mapped to VC3 */
    if (AtStrchr(pStrIdList, '.'))
        return VcDe3ListFromString(pStrIdList, channels, bufferSize);

    return LiuOrEc1De3ListFromString(pStrIdList, channels, bufferSize);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhMdlController.c
 *
 * Created Date: Jun 14, 2015
 *
 * Description : MDL CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhDe3.h"
#include "AtPdhMdlController.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
#define cMdlDataElementCommonMax  4

/* Function Pointers */
typedef AtPdhMdlController (*MdlControllerGet)(AtPdhDe3);
typedef uint16 (*DataElementGet)(AtPdhMdlController , eAtPdhMdlDataElement, uint8 *, uint16);
typedef uint32 (*CounterFuncGet)(AtPdhMdlController, uint32);

/*--------------------------- Macros -----------------------------------------*/
/* This function is to warn setting or getting a ID channel is not OK, it will not return CLI */
#define mCliDisplayIfNotOk(ret, channel, message)\
            AtPrintc(cSevWarning, \
                     "WARNING: Channel ID %s: %s. ret = %s\n",\
                     AtChannelIdString((AtChannel)channel), message, AtRet2String(ret))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static char m_buffer[64];
static char m_rawBuffer[256];

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *SharedBufferGet(void)
    {
    return m_buffer;
    }

static void SharedBufferInit(void)
    {
    AtOsalMemInit(m_buffer, 0, sizeof (m_buffer));
    }

static char *RawDataGet(char * buffer, uint16 length)
    {
    uint16 byte_i;
    char buf[8];
    AtOsalMemInit(m_rawBuffer, 0, sizeof (m_rawBuffer));

    AtSprintf(buf, "0x");
    AtStrcat(m_rawBuffer, buf);
    for (byte_i = 0; byte_i < length; byte_i++)
        {
        AtSprintf(buf, "%02x,", buffer[byte_i]);
        AtStrcat(m_rawBuffer, buf);

        if ((buffer[byte_i] == 0) && (byte_i != (length - 1)))
            {
            AtSprintf(buf, "...");
            AtStrcat(m_rawBuffer, buf);
            break;
            }
        }

    return m_rawBuffer;
    }

static AtPdhMdlController *MdlControllersListFromString(char *pDe3StrIds,
                                                 MdlControllerGet ControllerGetFunc,
                                                 uint32 *numControllers)
    {
    uint32              numberDe3s, de3_i, numberMdls, controller_i;
    AtPdhMdlController  *mdlList = (AtPdhMdlController*)CliSharedObjectListGet(&numberMdls);
    AtChannel           *de3List = CliSharedChannelListGet(&numberDe3s);
    
    *numControllers = 0;

    if ((mdlList == NULL)|| (de3List == NULL))
        {
        AtPrintc(cSevCritical, "ERROR: Share Buffer have problem!!! \n");
        return NULL;
        }

    numberDe3s = CliDe3ListFromString(pDe3StrIds, de3List, numberDe3s);
    if (numberDe3s == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        *numControllers = 0;
        return NULL;
        }

    controller_i = 0;
    for (de3_i = 0; de3_i < numberDe3s; de3_i++)
        {
        AtPdhMdlController mdlController = ControllerGetFunc((AtPdhDe3)de3List[de3_i]);
        if (mdlController == NULL)
            continue;
        mdlList[controller_i++] = mdlController;
        if (controller_i >= numberMdls)
            break;
        }

    *numControllers = controller_i;
    return mdlList;
    }

static eBool Enable(char *pDe3StrIds, MdlControllerGet ControllerGetFunc, eBool enable)
    {
    eBool  success;
    uint32 numControllers;
    uint32 controller_i;

    AtPdhMdlController* mdlControllerList = MdlControllersListFromString(pDe3StrIds, ControllerGetFunc, &numControllers);
    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, " Can not get MDL controller on this DS3-id = %s\r\n", pDe3StrIds);
        return cAtFalse;
        }

    success = cAtTrue;
    for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtPdhMdlControllerEnable(mdlControllerList[controller_i], enable);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            mCliDisplayIfNotOk(ret, AtPdhMdlControllerDe3Get(mdlControllerList[controller_i]), "Can not enable/disable MDL controller\r\n");
            }
        }
    return success;
    }

static eBool TxStringSet(char **argv,
                         MdlControllerGet ControllerGetFunc,
                         eAtPdhMdlDataElement element)
    {
    eBool  success;
    uint32 numControllers;
    uint32 controller_i;

    AtPdhMdlController* mdlControllerList = MdlControllersListFromString(argv[0], ControllerGetFunc, &numControllers);
    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, " Can not get MDL controller on this DS3-id = %s\r\n", argv[0]);
        return cAtFalse;
        }

    success = cAtTrue;
    for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtPdhMdlControllerTxDataElementSet(mdlControllerList[controller_i],
                                                        element,
                                                        (const uint8 *) argv[1],
                                                        (uint16) AtStrlen(argv[1]));
        if (ret != cAtOk)
            {
            success = cAtFalse;
            mCliDisplayIfNotOk(ret, AtPdhMdlControllerDe3Get(mdlControllerList[controller_i]), "Can not change data-element for Tx MDL controller\r\n");
            }
        }

    return success;
    }

static eBool Transmit(char *pDe3StrIds, MdlControllerGet ControllerGetFunc)
    {
    eBool  success;
    uint32 numControllers;
    uint32 controller_i;

    AtPdhMdlController* mdlControllerList = MdlControllersListFromString(pDe3StrIds, ControllerGetFunc, &numControllers);
    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, " Can not get MDL controller on this DS3-id = %s\r\n", pDe3StrIds);
        return cAtFalse;
        }

    success = cAtTrue;
    for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtPdhMdlControllerSend(mdlControllerList[controller_i]);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            mCliDisplayIfNotOk(ret, AtPdhMdlControllerDe3Get(mdlControllerList[controller_i]), "Can not transmit MDL controller\r\n");
            }
        }
    return success;
    }

static eBool Receive(char *pDe3StrIds, MdlControllerGet ControllerGetFunc)
    {
    uint32 numControllers;
    uint32 controller_i;
    AtPdhMdlController* mdlControllerList = MdlControllersListFromString(pDe3StrIds, ControllerGetFunc, &numControllers);

    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Can not get MDL controller on this DS3-id = %s\r\n", pDe3StrIds);
        return cAtFalse;
        }

    for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eBool messageReceived = AtPdhMdlControllerMessageReceived(mdlControllerList[controller_i]);
        if (messageReceived)
            AtPrintc(cSevInfo, "* Detect new message on %s\r\n",  (char *) CliChannelIdStringGet((AtChannel) AtPdhMdlControllerDe3Get(mdlControllerList[controller_i])));
        }

    return cAtTrue;
    }

static uint32 LastDataElementGet(AtPdhMdlController controller)
    {
    uint32 mdlMsgType = AtPdhMdlControllerTypeGet(controller);
    switch(mdlMsgType)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAtPdhMdlDataElementPfi;
        case cAtPdhMdlControllerTypeIdleSignal : return cAtPdhMdlDataElementIdleSignalPortNumber;
        case cAtPdhMdlControllerTypeTestSignal : return cAtPdhMdlDataElementTestSignalGeneratorNumber;
        default: return cBit31_0;
        }
    }

static eBool TableShow( tTab *tabPtr,
                        eBool isTx,
                        AtPdhMdlController* mdlControllerList,
                        uint32 numControllers,
                        eBool showRawData)
    {
    uint32 controller_i, element_i;
    eBool success = cAtTrue;
    eAtPdhMdlDataElement elements[cMdlDataElementCommonMax + 1];
    uint16 sizes[cMdlDataElementCommonMax + 1];
    DataElementGet DataElementGetFunc = isTx ? AtPdhMdlControllerTxDataElementGet : AtPdhMdlControllerRxDataElementGet;

    /* Create Elements */
    for (element_i = 0; element_i < cMdlDataElementCommonMax; element_i++)
        elements[element_i] = element_i;

    elements[cMdlDataElementCommonMax] = LastDataElementGet(mdlControllerList[0]);
    element_i = 0;
    sizes[element_i++] = cMdlEICStringSize;
    sizes[element_i++] = cMdlLICStringSize;
    sizes[element_i++] = cMdlFICStringSize;
    sizes[element_i++] = cMdlUINTStringSize;
    sizes[element_i++] = cMdlPortPfiGenStringSize;

    /* Update Table Information */
    for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint16 index;
        uint16 length;
        uint32 colIdx = 0;
        AtPdhMdlController controller = mdlControllerList[controller_i];
        eBool enable = AtPdhMdlControllerIsEnabled(controller);
        char* shareBuffer = SharedBufferGet();

        StrToCell(tabPtr, controller_i,  colIdx++, (char *) CliChannelIdStringGet((AtChannel) AtPdhMdlControllerDe3Get(controller)));
        ColorStrToCell(tabPtr, controller_i, colIdx++, enable ? "Enable" : "Disable", enable ? cSevInfo : cSevCritical);

        /* Update Content for Element */
        for (element_i = 0; element_i < cMdlDataElementCommonMax + 1; element_i++)
            {
            SharedBufferInit();
            length = DataElementGetFunc(controller, elements[element_i], (uint8*)shareBuffer, sizes[element_i]);
            if (element_i == cMdlDataElementCommonMax)
                {
                for (index = 0; index < 3 ; index ++)
                    {
                    if (((elements[cMdlDataElementCommonMax] == cAtPdhMdlDataElementPfi) && (index == 0))||
                        ((elements[cMdlDataElementCommonMax] == cAtPdhMdlDataElementIdleSignalPortNumber)  && (index == 1))||
                        ((elements[cMdlDataElementCommonMax] == cAtPdhMdlDataElementTestSignalGeneratorNumber)  && (index == 2)))
                        {
                        if (showRawData)
                            StrToCell(tabPtr, controller_i, colIdx++, (length != 0) ? RawDataGet(shareBuffer, length): "N.A");
                        else
                            StrToCell(tabPtr, controller_i, colIdx++, (length != 0) ? shareBuffer: "");
                        }
                    else
                        {
                        StrToCell(tabPtr, controller_i, colIdx++, "N/A");
                        }
                    }
                }
            else
                {
                if (showRawData)
                    StrToCell(tabPtr, controller_i, colIdx++, (length != 0) ? RawDataGet(shareBuffer, length): "N.A");
                else
                    StrToCell(tabPtr, controller_i, colIdx++, (length != 0) ? shareBuffer: "");
                }
            }
        }

    return success;
    }

static eBool Show(char argc, char **argv, MdlControllerGet ControllerGetFunc, eBool isTx)
    {
    eBool success;
    uint32 numControllers;
    AtPdhMdlController* mdlControllerList;
    tTab *tabPtr;
    const char *heading[] = { "ID", "Enable", "EIC", "LIC", "FIC", "UNIT", "PFI", "PORT", "GEN"};
    eBool showRawData = cAtFalse;

    /* Get MDL controller */
    mdlControllerList = MdlControllersListFromString(argv[0], ControllerGetFunc, &numControllers);
    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, " Can not get MDL controller on this DS3-id = %s\r\n", argv[0]);
        return cAtFalse;
        }

    if (argc > 1)
        showRawData = (AtStrcmp(argv[1], "raw") == 0) ? cAtTrue : cAtFalse;

    /* Allocate Table */
    tabPtr = TableAlloc(numControllers, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    success = TableShow(tabPtr, isTx, mdlControllerList, numControllers, showRawData);

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

static eBool CountersShow(char argc, char **argv, MdlControllerGet ControllerGetFunc)
    {
    uint32 controller_i;
    uint32 numControllers;
    uint32 readingMode;
    AtPdhMdlController* mdlControllerList;
    tTab *tabPtr;
    const char *heading[] = { "ID", "Tx-Pkts", "Tx-Bytes", "Rx-Pkts", "Rx-Bytes", "Rx-Drop-Ptks"};
    CounterFuncGet CounterRead;

    AtUnused(argc);

    /* Get MDL controller */
    mdlControllerList = MdlControllersListFromString(argv[0], ControllerGetFunc, &numControllers);
    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, " Can not get MDL controller on this DS3-id = %s\r\n", argv[0]);
        return cAtFalse;
        }

    /* Get Reading Mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if(readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    CounterRead = (readingMode == cAtHistoryReadingModeReadToClear) ? AtPdhMdlControllerCountersClear : AtPdhMdlControllerCountersGet;
    /* Allocate Table */
    tabPtr = TableAlloc(numControllers, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Update Table Information */
      for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
          {
          uint32 colIdx = 0;
          uint32 counterValue = 0;
          AtPdhMdlController controller = mdlControllerList[controller_i];
          eBool enable = AtPdhMdlControllerIsEnabled(controller);
          StrToCell(tabPtr, controller_i,  colIdx++, (char *) CliChannelIdStringGet((AtChannel) AtPdhMdlControllerDe3Get(controller)));
          if (enable)
              counterValue = CounterRead(controller, cAtPdhMdlControllerCounterTypeTxPkts);
          CliCounterPrintToCell(tabPtr, controller_i, colIdx++, counterValue, cAtCounterTypeGood, enable);
          if (enable)
              counterValue = CounterRead(controller, cAtPdhMdlControllerCounterTypeTxBytes);
          CliCounterPrintToCell(tabPtr, controller_i, colIdx++, counterValue, cAtCounterTypeGood, enable);
          if (enable)
              counterValue = CounterRead(controller, cAtPdhMdlControllerCounterTypeRxPkts);
          CliCounterPrintToCell(tabPtr, controller_i, colIdx++, counterValue, cAtCounterTypeGood, enable);
          if (enable)
              counterValue = CounterRead(controller, cAtPdhMdlControllerCounterTypeRxBytes);
          CliCounterPrintToCell(tabPtr, controller_i, colIdx++, counterValue, cAtCounterTypeGood, enable);
          if (enable)
              counterValue = CounterRead(controller, cAtPdhMdlControllerCounterTypeRxDropPkts);
          CliCounterPrintToCell(tabPtr, controller_i, colIdx++, counterValue, cAtCounterTypeError, enable);
          }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool Debug(char *pDe3StrIds, MdlControllerGet ControllerGetFunc)
    {
    uint32 numControllers, controller_i;
    AtPdhMdlController* mdlControllerList;

    mdlControllerList = MdlControllersListFromString(pDe3StrIds, ControllerGetFunc, &numControllers);
    if (numControllers == 0)
        {
        AtPrintc(cSevCritical, " Can not get MDL controller on this DS3-id = %s\r\n", pDe3StrIds);
        return cAtFalse;
        }

    for (controller_i = 0; controller_i < numControllers; controller_i = AtCliNextIdWithSleep(controller_i, AtCliLimitNumRestTdmChannelGet()))
        AtPdhMdlControllerDebug(mdlControllerList[controller_i]);

    return cAtTrue;
    }

eBool CmdAtPdhDe3MdlPathMessageEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return Enable(argv[0], AtPdhDe3PathMessageMdlController, cAtTrue);
    }

eBool CmdAtPdhDe3MdlPathMessageDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return Enable(argv[0], AtPdhDe3PathMessageMdlController, cAtFalse);
    }

eBool CmdAtPdhDe3MdlPathMessageEicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3PathMessageMdlController, cAtPdhMdlDataElementEic);
    }

eBool CmdAtPdhDe3MdlPathMessageLicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3PathMessageMdlController, cAtPdhMdlDataElementLic);
    }

eBool CmdAtPdhDe3MdlPathMessageFicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3PathMessageMdlController, cAtPdhMdlDataElementFic);
    }

eBool CmdAtPdhDe3MdlPathMessageUnitSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3PathMessageMdlController, cAtPdhMdlDataElementUnit);
    }

eBool CmdAtPdhDe3MdlPathMessagePfiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3PathMessageMdlController, cAtPdhMdlDataElementPfi);
    }

eBool CmdAtPdhDe3MdlPathMessageTxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3PathMessageMdlController, cAtTrue);
    }

eBool CmdAtPdhDe3MdlPathMessageRxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3PathMessageMdlController, cAtFalse);
    }

eBool CmdAtPdhDe3MdlMessageRxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3RxCurrentMdlControllerGet, cAtFalse);
    }

eBool CmdAtPdhDe3MdlPathMessageDebug(char argc, char **argv)
    {
    AtUnused(argc);
    return Debug(argv[0], AtPdhDe3PathMessageMdlController);
    }

eBool CmdAtPdhDe3MdlPathMessageTransmit(char argc, char **argv)
    {
    AtUnused(argc);
    return Transmit(argv[0], AtPdhDe3PathMessageMdlController);
    }

eBool CmdAtPdhDe3MdlPathMessageReceive(char argc, char **argv)
    {
    AtUnused(argc);
    return Receive(argv[0], AtPdhDe3PathMessageMdlController);
    }

eBool CmdAtPdhDe3MdlPathMessageCountersShow(char argc, char **argv)
    {
    return CountersShow(argc, argv, AtPdhDe3PathMessageMdlController);
    }

eBool CmdAtPdhDe3MdlTestSignalEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return Enable(argv[0], AtPdhDe3TestSignalMdlController, cAtTrue);
    }

eBool CmdAtPdhDe3MdlTestSignalDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return Enable(argv[0], AtPdhDe3TestSignalMdlController, cAtFalse);
    }

eBool CmdAtPdhDe3MdlTestSignalEicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3TestSignalMdlController, cAtPdhMdlDataElementEic);
    }

eBool CmdAtPdhDe3MdlTestSignalLicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3TestSignalMdlController, cAtPdhMdlDataElementLic);
    }

eBool CmdAtPdhDe3MdlTestSignalFicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3TestSignalMdlController, cAtPdhMdlDataElementFic);
    }

eBool CmdAtPdhDe3MdlTestSignalUnitSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3TestSignalMdlController, cAtPdhMdlDataElementUnit);
    }

eBool CmdAtPdhDe3MdlTestSignalGeneratorNumberSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv,AtPdhDe3TestSignalMdlController, cAtPdhMdlDataElementTestSignalGeneratorNumber);
    }

eBool CmdAtPdhDe3MdlTestSignalTxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3TestSignalMdlController, cAtTrue);
    }

eBool CmdAtPdhDe3MdlTestSignalRxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3TestSignalMdlController, cAtFalse);
    }

eBool CmdAtPdhDe3MdlTestSignalDebug(char argc, char **argv)
    {
    AtUnused(argc);
    return Debug(argv[0], AtPdhDe3TestSignalMdlController);
    }

eBool CmdAtPdhDe3MdlTestSignalTransmit(char argc, char **argv)
    {
    AtUnused(argc);
    return Transmit(argv[0], AtPdhDe3TestSignalMdlController);
    }

eBool CmdAtPdhDe3MdlTestSignalReceive(char argc, char **argv)
    {
    AtUnused(argc);
    return Receive(argv[0], AtPdhDe3TestSignalMdlController);
    }

eBool CmdAtPdhDe3MdlTestSignalCountersShow(char argc, char **argv)
    {
    return CountersShow(argc, argv, AtPdhDe3TestSignalMdlController);
    }

eBool CmdAtPdhDe3MdlIdleSignalEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return Enable(argv[0], AtPdhDe3IdleSignalMdlController, cAtTrue);
    }

eBool CmdAtPdhDe3MdlIdleSignalDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return Enable(argv[0], AtPdhDe3IdleSignalMdlController, cAtFalse);
    }

eBool CmdAtPdhDe3MdlIdleSignalEicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv, AtPdhDe3IdleSignalMdlController, cAtPdhMdlDataElementEic);
    }

eBool CmdAtPdhDe3MdlIdleSignalLicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv, AtPdhDe3IdleSignalMdlController, cAtPdhMdlDataElementLic);
    }

eBool CmdAtPdhDe3MdlIdleSignalFicSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv, AtPdhDe3IdleSignalMdlController, cAtPdhMdlDataElementFic);
    }

eBool CmdAtPdhDe3MdlIdleSignalUnitSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv, AtPdhDe3IdleSignalMdlController, cAtPdhMdlDataElementUnit);
    }

eBool CmdAtPdhDe3MdlIdleSignalPortSet(char argc, char **argv)
    {
    AtUnused(argc);
    return TxStringSet(argv, AtPdhDe3IdleSignalMdlController, cAtPdhMdlDataElementIdleSignalPortNumber);
    }

eBool CmdAtPdhDe3MdlIdleSignalTxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3IdleSignalMdlController, cAtTrue);
    }

eBool CmdAtPdhDe3MdlIdleSignalRxShow(char argc, char **argv)
    {
    return Show(argc, argv, AtPdhDe3IdleSignalMdlController, cAtFalse);
    }

eBool CmdAtPdhDe3MdlIdleSignalDebug(char argc, char **argv)
    {
    AtUnused(argc);
    return Debug(argv[0], AtPdhDe3IdleSignalMdlController);
    }

eBool CmdAtPdhDe3MdlIdleSignalTransmit(char argc, char **argv)
    {
    AtUnused(argc);
    return Transmit(argv[0], AtPdhDe3IdleSignalMdlController);
    }

eBool CmdAtPdhDe3MdlIdleSignalReceive(char argc, char **argv)
    {
    AtUnused(argc);
    return Receive(argv[0], AtPdhDe3IdleSignalMdlController);
    }

eBool CmdAtPdhDe3MdlIdleSignalCountersShow(char argc, char **argv)
    {
    return CountersShow(argc, argv, AtPdhDe3IdleSignalMdlController);
    }

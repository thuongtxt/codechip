/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhDe1ErrorGenerator.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : DS1/E1 error generator CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPdhErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel De1sFromString(char* argv)
    {
    uint32 numPdhChannels;
    AtChannel *pdhChannels;

    pdhChannels = CliSharedChannelListGet(&numPdhChannels);
    numPdhChannels = De1ListFromString(argv, pdhChannels, numPdhChannels);
    if (numPdhChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No PDH channel is parsed\r\n");
        return NULL;
        }

    if (numPdhChannels > 1)
        {
        AtPrintc(cSevCritical, "ERROR: Only support a single PDH channel\r\n");
        return NULL;
        }

    return pdhChannels[0];
    }

eBool CmdPdhDe1TxErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorTypeSet(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1TxErrorGeneratorModeSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorModeSet(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1TxErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorNumSet(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1TxErrorGeneratorErrorRateSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorRateSet(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1TxErrorGeneratorStart(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStart(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1TxErrorGeneratorStop(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStop(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1TxErrorGeneratorShow(char argc, char **argv)
    {
    return CliPdhErrorGeneratorShow(argc, argv, De1sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorTypeSet(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorModeSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorModeSet(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorNumSet(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorErrorRateSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorRateSet(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorStart(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStart(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorStop(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStop(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe1RxErrorGeneratorShow(char argc, char **argv)
    {
    return CliPdhErrorGeneratorShow(argc, argv, De1sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

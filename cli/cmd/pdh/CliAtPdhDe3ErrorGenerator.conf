# @group CliAtPdhDe3ErrorGenerator "Error generator"
# @ingroup CliAtPdhDe3 "DS3/E3"

6 pdh de3 tx error generator type CmdPdhDe3TxErrorGeneratorErrorTypeSet 2 de3Id=1 type
/*
    Syntax     : pdh de3 tx error generator type <de3Id> <fbit-include/fbit-exclude/fbit-only>
    Parameter  : de3Id - DE3 identifier
                 type - Error type is generated such as full bit stream, payload or fbit only
    Description: Do one shot force error at TX direction
    Example    : pdh de3 tx error generator type 1 fbit-include
*/

6 pdh de3 tx error generator errornum CmdPdhDe3TxErrorGeneratorErrorNumSet 2 de3Id=1 numErrors=1
/*
    Syntax     : pdh de3 tx error generator errornum <de3Id> <numErrors>
    Parameter  : de3Id     - DE3 identifier
                 numErrors - Number of errors to generate in one-shot
    Description: Set number of errors to force one-shot at TX direction
    Example    : pdh de3 tx error generator errornum 1 3
*/

6 pdh de3 tx error generator start CmdPdhDe3TxErrorGeneratorStart 1 de3Id=1
/*
    Syntax     : pdh de3 tx error generator start <de3Id>
    Parameter  : de3Id - DE3 identifier
    Description: Start error generator at TX direction
    Example    : pdh de3 tx error generator start 1
*/

6 pdh de3 tx error generator stop CmdPdhDe3TxErrorGeneratorStop 1 de3Id=1
/*
    Syntax     : pdh de3 tx error generator stop <de3Id>
    Parameter  : de3Id - DE3 identifier
    Description: Stop error generator at TX direction
    Example    : pdh de3 tx error generator stop 1
*/

6 show pdh de3 tx error generator CmdPdhDe3TxErrorGeneratorShow 1 de3Id=1
/*
    Syntax     : show pdh de3 tx error generator <de3Id>
    Parameter  : de3Id - DE3 identifier
    Description: Show error generating information at TX direction
    Example    : show pdh de3 tx error generator 1
*/

6 pdh de3 rx error generator type CmdPdhDe3RxErrorGeneratorErrorTypeSet 2 de3Id=1 type
/*
    Syntax     : pdh de3 rx error generator type <de3Id> <fbit-include/fbit-exclude/fbit-only>
    Parameter  : de3Id - DE3 identifier
                 type - Error type is generated such as full bit stream, payload or fbit only
    Description: Do one shot force error at RX direction
    Example    : pdh de3 rx error generator type 1 fbit-include
*/

6 pdh de3 rx error generator errornum CmdPdhDe3RxErrorGeneratorErrorNumSet 2 de3Id=1 numErrors=1
/*
    Syntax     : pdh de3 rx error generator errornum <de3Id> <numErrors>
    Parameter  : de3Id     - DE3 identifier
                 numErrors - Number of errors to generate in one-shot
    Description: Set number of errors to force one-shot at RX direction
    Example    : pdh de3 rx error generator errornum 1 3
*/

6 pdh de3 rx error generator start CmdPdhDe3RxErrorGeneratorStart 1 de3Id=1
/*
    Syntax     : pdh de3 rx error generator start <de3Id>
    Parameter  : de3Id - DE3 identifier
    Description: Start error generator at RX direction
    Example    : pdh de3 rx error generator start 1
*/

6 pdh de3 rx error generator stop CmdPdhDe3RxErrorGeneratorStop 1 de3Id=1
/*
    Syntax     : pdh de3 rx error generator stop <de3Id>
    Parameter  : de3Id - DE3 identifier
    Description: Stop error generator at RX direction
    Example    : pdh de3 rx error generator stop 1
*/

6 show pdh de3 rx error generator CmdPdhDe3RxErrorGeneratorShow 1 de3Id=1
/*
    Syntax     : show pdh de3 rx error generator <de3Id>
    Parameter  : de3Id - DE3 identifier
    Description: Show error generating information at RX direction
    Example    : show pdh de3 rx error generator 1
*/

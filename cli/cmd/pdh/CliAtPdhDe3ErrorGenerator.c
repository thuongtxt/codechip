/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhDe3ErrorGenerator.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : DS3/E3 error generator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPdhErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel De3sFromString(char* argv)
    {
    uint32 numPdhChannels;
    AtChannel *pdhChannels;
    pdhChannels = CliSharedChannelListGet(&numPdhChannels);
    numPdhChannels = CliDe3ListFromString(argv, pdhChannels, numPdhChannels);
    if (numPdhChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No PDH channel is parsed\r\n");
        return NULL;
        }
    if (numPdhChannels > 1)
        {
        AtPrintc(cSevCritical, "ERROR: Only support a single PDH channel\r\n");
        return NULL;
        }
    return pdhChannels[0];
    }

eBool CmdPdhDe3TxErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorTypeSet(argc, argv, De3sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe3TxErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorNumSet(argc, argv, De3sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe3TxErrorGeneratorStart(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStart(argc, argv, De3sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe3TxErrorGeneratorStop(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStop(argc, argv, De3sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe3TxErrorGeneratorShow(char argc, char **argv)
    {
    return CliPdhErrorGeneratorShow(argc, argv, De3sFromString, AtPdhChannelTxErrorGeneratorGet);
    }

eBool CmdPdhDe3RxErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorTypeSet(argc, argv, De3sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe3RxErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    return CliPdhErrorGeneratorErrorNumSet(argc, argv, De3sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe3RxErrorGeneratorStart(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStart(argc, argv, De3sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe3RxErrorGeneratorStop(char argc, char **argv)
    {
    return CliPdhErrorGeneratorStop(argc, argv, De3sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

eBool CmdPdhDe3RxErrorGeneratorShow(char argc, char **argv)
    {
    return CliPdhErrorGeneratorShow(argc, argv, De3sFromString, AtPdhChannelRxErrorGeneratorGet);
    }

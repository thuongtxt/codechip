/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhDe3.c
 *
 * Created Date: Feb 4, 2013
 *
 * Description : DE3 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "AtCli.h"
#include "AtPdhChannel.h"
#include "AtPdhDe2.h"
#include "AtModulePdh.h"
#include "AtDevice.h"
#include "CliAtModulePdh.h"
#include "../sdh/CliAtModuleSdh.h"
#include "AtSdhVc.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
static const char *sCliPdhAlms[] =
    {
    "lof",
    "ais",
    "rai"
    };

static uint32 const cCliPdhDe2AlmMasks[] =
    {
    cAtPdhDe2AlarmLof,
    cAtPdhDe2AlarmAis,
    cAtPdhDe2AlarmRai
    };

#define mAttributeSetFunc(func) ((De2AttributeSetFunc)(func))
/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*De2AttributeSetFunc)(AtPdhDe2 self, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

static char m_buffer[200];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *SharedBuffer(void)
    {
    return m_buffer;
    }

static uint32 CliPdhDe2IntrMaskFromString(char *pIntrStr)
    {
    return CliMaskFromString(pIntrStr, sCliPdhAlms, cCliPdhDe2AlmMasks, mCount(cCliPdhDe2AlmMasks));
    }

static char *CliPdhDe2AlarmStrFromMask(uint32 alarmMask)
    {
    uint8       i;
    static char allAlarms[64];
    static char alarm[16];

    AtOsalMemInit(allAlarms, 0, sizeof(allAlarms));
    AtOsalMemInit(alarm, 0, sizeof(alarm));

    for (i = 0; i < mCount(sCliPdhAlms); i++)
        {
        if ((alarmMask & cCliPdhDe2AlmMasks[i]) == 0)
            continue;

        AtSprintf(alarm, "|%s", sCliPdhAlms[i]);
        AtStrcat(allAlarms, alarm);
        }

    return &allAlarms[1];
    }
eBool CmdPdhDe2AlarmForce(char argc, char **argv)
    {
    eAtRet      ret = cAtOk;
    uint32      numChannels, alarmMask, i;
    AtChannel   *channelList;
    eBool       enable = cAtFalse;
    eBool       blResult = cAtFalse;
    eAtRet      (*Func)(AtChannel, uint32);
    eAtDirection direction;
	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS2/E2 list, expected: 1.1.1.1 or 1.1.1.1-1.1.1.3, ...\n");
        return cAtFalse;
        }

    direction = CliDirectionFromStr(argv[1]);
    if(direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expected %s\r\n", CliValidDirections());
        return cAtFalse;
        }

    alarmMask = CliPdhDe2IntrMaskFromString(argv[2]);
    mAtStrToBool(argv[3], enable, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling operation. Expected: en/dis\r\n");
        return cAtFalse;
        }

    if (enable)
        Func = (direction == cAtDirectionRx) ? AtChannelRxAlarmForce : AtChannelTxAlarmForce;
    else
        Func = (direction == cAtDirectionRx) ? AtChannelRxAlarmUnForce : AtChannelTxAlarmUnForce;

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = Func((AtChannel)channelList[i], alarmMask);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Can not force alarms for %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdPdhDe2InterruptMaskSet(char argc, char **argv)
    {
    eAtRet      ret = cAtOk;
    eBool       blResult = cAtFalse;
    uint32      numChannels, i;
    AtChannel   *channelList;
    uint32      intrMaskVal;
    eBool       enable = cAtFalse;
	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS2/E2 list, expected: 1.1.1.1 or 1.1.1.1-1.1.1.3, ...\n");
        return cAtFalse;
        }

    intrMaskVal = CliPdhDe2IntrMaskFromString(argv[1]);

    mAtStrToBool(argv[2], enable, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling operation. Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       ret = AtChannelInterruptMaskSet(channelList[i], intrMaskVal, (enable) ? intrMaskVal : 0);
       if (ret != cAtOk)
           AtPrintc(cSevCritical,
                    "ERROR: Can not set interrupt mask of %s, ret = %s\r\n",
                    CliChannelIdStringGet(channelList[i]),
                    AtRet2String(ret));
       }

    return cAtTrue;
    }

eBool CmdPdhDe2Show(char argc, char **argv)
    {
    uint32              numChannels, i, intrMaskVal, alarmMask;
    AtChannel           *channelList;
    tTab                *tabPtr;
    const char          *pHeading[] = {"DE2 ID", "Interrupt","ForcedTxAlarms", "ForcedRxAlarms"};
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS2/E2 list, expected: 1.1.1.1 or 1.1.1.1-1.1.1.3, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSprintf(SharedBuffer(), "%s", CliChannelIdStringGet((AtChannel)channelList[i]));
        StrToCell(tabPtr, i, 0, SharedBuffer());

        intrMaskVal = AtChannelInterruptMaskGet(channelList[i]);
		AtSprintf(SharedBuffer(), "%s", CliPdhDe2AlarmStrFromMask(intrMaskVal));
		ColorStrToCell(tabPtr, i, 1, (intrMaskVal) ? SharedBuffer() : "None", (intrMaskVal) ? cSevCritical : cSevInfo);

		alarmMask = AtChannelTxForcedAlarmGet((AtChannel)channelList[i]);
		AtStrcpy(SharedBuffer(), CliPdhDe2AlarmStrFromMask(alarmMask));
		ColorStrToCell(tabPtr, i, 2, (alarmMask) ? SharedBuffer() : "None", (alarmMask) ? cSevCritical : cSevInfo);

		alarmMask = AtChannelRxForcedAlarmGet((AtChannel)channelList[i]);
		AtStrcpy(SharedBuffer(), CliPdhDe2AlarmStrFromMask(alarmMask));
		ColorStrToCell(tabPtr, i, 3, (alarmMask) ? SharedBuffer() : "None", (alarmMask) ? cSevCritical : cSevInfo);

		}

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe2CountersShow(char argc, char **argv)
    {
    eBool                 silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    uint32                numChannels = 0, row_i = 0;
    uint16                column = 0;
    AtChannel             *channelList;
    tTab                  *tabPtr;
    uint8                 counterTypes[] = {cAtPdhDe2CounterParity, cAtPdhDe2CounterFbe};
    const char            *pHeading[] = {"DE2 ID", "Parity","FBE"};
    uint32                (*CounterRead)(AtChannel self, uint16 counterType);
    AtUnused(argc);

    if (argc < 2)
       {
       AtPrintc(cSevCritical, "ERROR: Not enough parameter");
       return cAtFalse;
       }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    CounterRead = (readingMode == cAtHistoryReadingModeReadToClear) ? AtChannelCounterClear : AtChannelCounterGet;

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS2/E2 list, expected: 1.1.1.1 or 1.1.1.1-1.1.1.3, ...\n");
        return cAtFalse;
        }

    if (!silent)
        {
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Fail to create table\r\n");
            return cAtFalse;
            }
        }

    for (row_i = 0; row_i < numChannels; row_i = AtCliNextIdWithSleep(row_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 counter_i = 0;

        column = 0;
        StrToCell(tabPtr, row_i, column++, CliChannelIdStringGet((AtChannel)channelList[row_i]));

        for (counter_i = 0; counter_i < mCount(counterTypes); counter_i++)
            {
            uint32 counterValue = CounterRead((AtChannel)channelList[row_i], counterTypes[counter_i]);
            CliCounterPrintToCell(tabPtr, row_i, column++, counterValue, cAtCounterTypeError, cAtTrue);
            }

        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe2AlarmShow(char argc, char **argv)
    {
    uint32      numChannels, i, alarmBitmap;
    AtChannel   *channelList;
    tTab        *tabPtr;
    const char  *pHeading[] = {"DE2 ID", "LOS", "LOF", "AIS", "RAI"};
	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)channelList[i]));

       alarmBitmap = AtChannelAlarmGet(channelList[i]);
       ColorStrToCell(tabPtr, i, 1, (alarmBitmap & cAtPdhDe2AlarmLos) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmLos) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, 2, (alarmBitmap & cAtPdhDe2AlarmLof) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, 3, (alarmBitmap & cAtPdhDe2AlarmAis) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmAis) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, 4, (alarmBitmap & cAtPdhDe2AlarmRai) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmRai) ? cSevCritical : cSevInfo);
       }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe2InterruptShow(char argc, char **argv)
    {
    uint32      numChannels, i, alarmBitmap;
    AtChannel   *channelList;
    tTab        *tabPtr;
    eAtHistoryReadingMode readingMode;
    const char  *pHeading[] = {"DE2 ID", "LOS", "LOF", "AIS", "RAI"};
	AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)channelList[i]));

       if (readingMode == cAtHistoryReadingModeReadToClear)
           alarmBitmap = AtChannelAlarmInterruptClear(channelList[i]);
       else
           alarmBitmap = AtChannelAlarmInterruptGet(channelList[i]);

       ColorStrToCell(tabPtr, i, 1, (alarmBitmap & cAtPdhDe2AlarmLos) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmLos) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, 2, (alarmBitmap & cAtPdhDe2AlarmLof) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmLof) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, 3, (alarmBitmap & cAtPdhDe2AlarmAis) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmAis) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, 4, (alarmBitmap & cAtPdhDe2AlarmRai) ? "set" : "clear", (alarmBitmap & cAtPdhDe2AlarmRai) ? cSevCritical : cSevInfo);
       }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe2LogShow(char argc, char** argv)
    {
    uint32      numChannels, i;
    AtChannel   *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        AtChannelLogShow(channelList[i]);

    return cAtTrue;
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint32      numChannels, i;
    AtChannel   *channelList;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe2ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtChannelLogEnable(channelList[i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable logging for DE2 %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe2LogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe2LogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

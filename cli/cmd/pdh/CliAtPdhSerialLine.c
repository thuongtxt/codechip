/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhSerialLine.c
 *
 * Created Date: April 9, 2015
 *
 * Description : DS3/E3 Serial Line CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhSerialLine.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
static const char *cAtPdhSerialLineModeStr[] =
    {
    "ec1", "ds3", "e3", "unknown"
    };

static uint32 const cAtPdhSerialLineModeVal[] =
    {
    cAtPdhDe3SerialLineModeEc1,
    cAtPdhDe3SerialLineModeDs3,
    cAtPdhDe3SerialLineModeE3,
    cAtPdhDe3SerialLineModeUnknown
    };

static const char * cLoopbackModeStr[]={"release", "local", "remote"};
static const eAtLoopbackMode cLoopbackModeVal[]={cAtLoopbackModeRelease,
                                                 cAtLoopbackModeLocal,
                                                 cAtLoopbackModeRemote };

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeSetFunc)(AtPdhSerialLine self, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SerialLineListFromString(char *pStrIdList,
                                       AtChannel *channels,
                                       uint32 bufferSize)
    {
    uint32 *idBuffer, numChannels, i, numValidChannels = 0;

    if (pStrIdList == NULL)
        return 0;

    /* It is just a list of flat ID */
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(pStrIdList, idBuffer, numChannels);
    if (numChannels > bufferSize)
        numChannels = bufferSize;
    for (i = 0; i < numChannels; i++)
        {
        channels[numValidChannels] = (AtChannel) AtModulePdhDe3SerialLineGet(CliModulePdh(), idBuffer[i] - 1);
        if (channels[numValidChannels] == NULL)
            continue;

        numValidChannels++;
        }

    return numValidChannels;
    }

static eBool AttributeSet(char *idStrList, uint32 value, AttributeSetFunc attributeSetFunc)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = SerialLineListFromString(idStrList, channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS3/E3 Serial line list, expected: 1 or 1-48, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = attributeSetFunc((AtPdhSerialLine)channelList[i], value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure serial line %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)channelList[i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhSerialLineModeSet(char argc, char **argv)
    {
    uint32 mode = cAtPdhDe3SerialLineModeUnknown;
    eBool blResult = cAtFalse;
    AtUnused(argc);

    mAtStrToEnum(cAtPdhSerialLineModeStr,
                 cAtPdhSerialLineModeVal,
                 argv[1],
                 mode,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Error: wrong input mode, expected : \r\n");
        CliExpectedValuesPrint(cSevCritical, cAtPdhSerialLineModeStr, mCount(cAtPdhSerialLineModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], mode, AtPdhSerialLineModeSet);
    }

eBool CmdPdhSerialLineModeShow(char argc, char **argv)
    {
    uint32 mode;
    char buff[16];
    eBool convertSuccess;
    uint32 numChannels, i;
    AtChannel *channelList;
    tTab *tabPtr;
    eAtLoopbackMode loopbackMode;
    const char *pHeading[] =  { "Serial ID", "Mode", "Loopback"};


    AtUnused(argc);
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = SerialLineListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS3/E3 Serial line list, expected: 1 or 1-48, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channelList[i]));

        mode = AtPdhSerialLineModeGet((AtPdhSerialLine) channelList[i]);
        mAtEnumToStr(cAtPdhSerialLineModeStr, cAtPdhSerialLineModeVal, mode, buff, convertSuccess);
        ColorStrToCell(tabPtr, i, column++, convertSuccess ? buff : "Error", convertSuccess ? ((mode == cAtPdhDe3SerialLineModeUnknown) ? cSevWarning : cSevNormal) : cSevCritical);

        loopbackMode = AtChannelLoopbackGet(channelList[i]);
        StrToCell(tabPtr, i, column++, CliLoopbackModeString(loopbackMode));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhSerialLineLoopbackSet(char argc, char **argv)
    {
    eAtLoopbackMode loopbackMode = cAtLoopbackModeRelease;
    eBool convertSuccess;
    AtUnused(argc);

    mAtStrToEnum(cLoopbackModeStr,
                 cLoopbackModeVal,
                 argv[1],
                 loopbackMode,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <loopbackMode> , expected release|local|remote\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], loopbackMode, (AttributeSetFunc)AtChannelLoopbackSet);
    }

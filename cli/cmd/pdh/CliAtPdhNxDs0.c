/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : NxDS0
 *
 * File        : CliPdhNxDs0.c
 *
 * Created Date: Nov 29, 2012
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePdh.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NxDs0Show(AtList allNxDs0s)
    {
    tTab *tabPtr;
    uint32 row_i;
    uint8 abcd1;
    const char* pHeading[] = {"NxDS0", "Bound", "CAS IDLE"};
    char cellString[128];

    /* Create table with titles */
    tabPtr = TableAlloc(AtListLengthGet(allNxDs0s), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    row_i = 0;
    while (AtListLengthGet(allNxDs0s) > 0)
        {
        AtChannel nxDs0 = (AtChannel)AtListObjectRemoveAtIndex(allNxDs0s, 0);
        AtChannel boundChannel;

        /* Call function get mask of nxDS0 and convert to string */
        StrToCell(tabPtr, row_i, 0, (char *)CliChannelIdStringGet(nxDs0));

        /* Bound encap channel */
        boundChannel = (AtChannel)AtChannelBoundEncapChannelGet(nxDs0);
        if (boundChannel == NULL)
            boundChannel = (AtChannel)AtChannelBoundPwGet((AtChannel)nxDs0);

        StrToCell(tabPtr, row_i, 1, boundChannel ? CliChannelIdStringGet(boundChannel) : "None");
        abcd1 = AtPdhNxDs0CasIdleCodeGet((AtPdhNxDS0)nxDs0);
        AtSprintf(cellString, "0x%x", abcd1);
        StrToCell(tabPtr, row_i, 2, cellString);

        row_i = AtCliNextIdWithSleep(row_i, AtCliLimitNumRestTdmChannelGet());
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhNxDs0Create(char argc, char **argv)
    {
    uint32 numChannels, i;
    uint32 numDs0, bitmap, *ds0List;
    AtChannel *channelList;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get DE1 ID */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);

    /* Get DS0 list */
    ds0List = CliSharedIdBufferGet(&numDs0);
    numDs0  = CliIdListFromString(argv[1], ds0List, numDs0);

    /* Create bitmap from DS0 list */
    bitmap = 0;
    for (i = 0; i < numDs0; i++)
        bitmap |= cBit0 << ds0List[i];

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        if (AtPdhDe1NxDs0Create((AtPdhDe1)channelList[i], bitmap) == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Create NxDs0 fail on %s.\r\n",
                     CliChannelIdStringGet(channelList[i]));
            success = cAtFalse;
            }
        }

    if (!success)
        {
        AtPrintc(cSevInfo,   "INFO: NxDS0 is created fail for some reasons:\r\n");
        AtPrintc(cSevNormal, "- Timeslot 0 is used for framing\r\n"
                             "- Timeslot 16 is used for signaling (if signaling is enabled)\r\n"
                             "- Timeslots may be in used by other NxDS0 group\r\n"
                             "- Timeslots do not exist (in case of unframe)\r\n");
        }

    return success;
    }

eBool CmdPdhNxDs0Delete(char argc, char **argv)
    {
    uint32 numChannels, i;
    uint32 numDs0, bitmap, *ds0List;
    AtChannel *channelList;
    AtPdhNxDS0 nxDs0;
    eAtRet ret;
	AtUnused(argc);

    /* Get DE1 ID */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);

    /* Get DS0 list */
    ds0List = CliSharedIdBufferGet(&numDs0);
    numDs0 = CliIdListFromString(argv[1], ds0List, numDs0);

    /* Create bitmap from DS0 list */
    bitmap = 0;
    for (i = 0; i < numDs0; i++)
        bitmap |= cBit0 << ds0List[i];

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Check if DS0 has already been created */
        nxDs0 = AtPdhDe1NxDs0Get((AtPdhDe1)channelList[i], bitmap);
        if (nxDs0 == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: NxDS0 0x%08x of %s has not been created\r\n",
                     bitmap,
                     CliChannelIdStringGet(channelList[i]));
            continue;
            }

        /* Delete it */
        ret = AtPdhDe1NxDs0Delete((AtPdhDe1)channelList[i], nxDs0);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Delete NxDS0 0x%08x of %s fail with ret = %s\r\n",
                     bitmap,
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdPdhNxDs0Show(char argc, char** argv)
    {
    uint32 numDe1s, i;
    AtChannel *de1s;
    AtList allNxDs0s = AtListCreate(0);
	AtUnused(argc);

    /* Get list of all DS1s/E1s */
    de1s    = CliSharedChannelListGet(&numDe1s);
    numDe1s = De1ListFromString(argv[0], de1s, numDe1s);

    /* Get all of NxDs0 */
    for (i = 0; i < numDe1s; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtIterator iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)de1s[i]);
        AtPdhNxDS0 nxDs0;

        while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(iterator)) != NULL)
            AtListObjectAdd(allNxDs0s, (AtObject)nxDs0);
        AtObjectDelete((AtObject)iterator);
        }

    /* Show them */
    if (AtListLengthGet(allNxDs0s) == 0)
        AtPrintc(cSevWarning, "WARNING: No NxDS0s to display, ID may be wrong\r\n");
    else
        NxDs0Show(allNxDs0s);

    AtObjectDelete((AtObject)allNxDs0s);

    return cAtTrue;
    }

eBool CmdPdhNxDs0CasIdleCodeSet(char argc, char **argv)
    {
    uint32 numChannels, i;
    uint32 numDs0, bitmap, *ds0List;
    AtChannel *channelList;
    AtPdhNxDS0 nxDs0;
    eAtRet ret;
    uint8 abcd1;
    AtUnused(argc);

    /* Get DE1 ID */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);

    /* Get DS0 list */
    ds0List = CliSharedIdBufferGet(&numDs0);
    numDs0 = CliIdListFromString(argv[1], ds0List, numDs0);
    abcd1 = (uint8)AtStrtoul(argv[2], NULL, 16);

    /* Create bitmap from DS0 list */
    bitmap = 0;
    for (i = 0; i < numDs0; i++)
        bitmap |= cBit0 << ds0List[i];

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Check if DS0 has already been created */
        nxDs0 = AtPdhDe1NxDs0Get((AtPdhDe1)channelList[i], bitmap);
        if (nxDs0 == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: NxDS0 0x%08x of %s has not been created\r\n",
                     bitmap,
                     CliChannelIdStringGet(channelList[i]));
            continue;
            }

        /* Delete it */
        ret = AtPdhNxDs0CasIdleCodeSet(nxDs0, abcd1);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: CAS idle code NxDS0 0x%08x of %s fail with ret = %s\r\n",
                     bitmap,
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
        }

    return cAtTrue;
    }

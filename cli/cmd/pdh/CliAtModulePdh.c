/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : atclipdh_de3.c
 *
 * Created Date: Dec 1, 2011
 *
 * Description : DS3/E3 CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePdh.h"
#include "AtDevice.h"
#include "AtCli.h"
#include "CliAtModulePdh.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../clock/CliAtModuleClock.h"
#include "AtAttPdhManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mTsEboolVal(i, bitmap)     ((bitmap>>i) & cBit0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtPdhChannelLoopbackStr[] = {
                                           "release",
                                           "payloadloopin",
                                           "payloadloopout",
                                           "lineloopin",
                                           "lineloopout"
                                            };

static const uint32 cAtPdhChannelLoopbackVal[] = {cAtPdhLoopbackModeRelease,
                                                  cAtPdhLoopbackModeLocalPayload,
                                                  cAtPdhLoopbackModeRemotePayload,
                                                  cAtPdhLoopbackModeLocalLine,
                                                  cAtPdhLoopbackModeRemoteLine};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtRet ret = AtModuleInterruptEnable((AtModule)modulePdh, enable);
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool PrmCrCompareEnable(char argc, char **argv, eBool enable)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtRet ret = AtModulePdhPrmCrCompareEnable(modulePdh, enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s compare CR bit, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

uint32 CliPdhLoopbackModeFromString(const char *loopbackStr)
    {
    uint32 loopbackMode = cAtPdhLoopbackModeRelease;
    eBool result;

    mAtStrToEnum(cAtPdhChannelLoopbackStr, cAtPdhChannelLoopbackVal, loopbackStr, loopbackMode, result);
    if (!result)
        return cAtPdhLoopbackModeInvalid;

    return loopbackMode;
    }

const char *CliPdhValidLoopbackModes(void)
    {
    return "payloadloopin, payloadloopout, lineloopin, lineloopout";
    }

const char *CliPdhLoopbackModeString(eAtPdhLoopbackMode loopbackMode)
    {
    const char *loopbackModeString = CliEnumToString(loopbackMode,
                                                     cAtPdhChannelLoopbackStr,
                                                     cAtPdhChannelLoopbackVal,
                                                     mCount(cAtPdhChannelLoopbackVal),
                                                     NULL);
    return loopbackModeString ? loopbackModeString : "unknown";
    }

char *CliBitmap2String(uint32 bitmap)
    {
    static char buf1[64];
    static char buf2[64];
    uint8 i, count;

    count = 0;
    AtOsalMemInit(buf1, 0, sizeof(buf1));
    AtOsalMemInit(buf2, 0, sizeof(buf2));

    if (bitmap == cBit31_0)
        {
        AtSprintf(buf1, "0-31");
        return buf1;
        }

    for (i = 0; i < 32; i++)
        {
        if (mTsEboolVal(i, bitmap) - mTsEboolVal((i - 1), bitmap) == 1) /* change 0->1*/
            {
            if (mTsEboolVal(i, bitmap) - mTsEboolVal((i + 1), bitmap) == 1) /* but change 1->0 also */
                {
                /* Print to m_pBuf: "%u", i */
                AtSprintf(buf1, "%u,", i);
                AtStrcat(buf2, buf1);
                }
            else
                {
                /* Print to m_pBuf: "%u", i */
                AtSprintf(buf1, "%u", i);
                AtStrcat(buf2, buf1);
                count++;
                }
            }
        else if (mTsEboolVal(i, bitmap) == 1 &&
                 mTsEboolVal(i, bitmap) == mTsEboolVal((i - 1), bitmap)
                && mTsEboolVal(i, bitmap) == mTsEboolVal((i + 1), bitmap))  /* const 1 */
            {
            count++;
            }
        else if (mTsEboolVal(i, bitmap) - mTsEboolVal((i + 1), bitmap) == 1)  /* just change from 1 -> 0*/
            {
            if (count > 0)
                {
                /* Strcat buf1 to m_pBuf:*/
                AtSprintf(buf1, "-%u,", i);
                AtStrcat(buf2, buf1);
                count = 0;
                }
            }
        else continue;
        }

    /* Cut "," last symbol in string */
    AtOsalMemInit(buf1, 0, sizeof(buf1));
    AtStrncpy(buf1, buf2, (AtStrlen(buf2)) ? (AtStrlen(buf2) - 1) : AtStrlen(buf2));
    return buf1;
    }

char *CliNxDs0Bitmap2String(uint32 sigBitmap)
    {
    return CliBitmap2String(sigBitmap);
    }

eBool CmdPdhModuleInit(char argc, char **argv)
    {
    eAtRet  ret;
    uint32 i;
    AtModulePdh pdhModule;
    AtChannel de1Channel;
	AtUnused(argv);
	AtUnused(argc);

    pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);

    /* Init global PDH module */
    ret = AtModuleInit((AtModule)pdhModule);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Initialize module PDH fail.\r\n");
        return cAtFalse;
        }

    /* Init all channels of PDH module */
    for (i = 0; i < AtModulePdhNumberOfDe1sGet(pdhModule); i++)
        {
        de1Channel = (AtChannel)AtModulePdhDe1Get(pdhModule, i);
        ret = AtChannelInit(de1Channel);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "Initialize channel DS1/E1 #%u fail. Ret = %s\r\n",
                     AtChannelIdGet(de1Channel) + 1, AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdPdhModuleOutClkSet(char argc, char **argv)
    {
    uint32 i;
    uint32 numPdhChannels;
    AtChannel *pdhChannels;
    eAtRet ret = cAtOk;
    uint32 *refOutIds, numRefOutIds;
    uint32 *tempIds;
	AtUnused(argc);

    /* Get ID buffers */
    refOutIds   = CliSharedIdBufferGet(&numRefOutIds);
    pdhChannels = CliSharedChannelListGet(&numPdhChannels);

    /* Get ID of clock signal */
    numRefOutIds = CliIdListFromString(argv[0], refOutIds, numRefOutIds);
    if (numRefOutIds == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid list of output clock IDs, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    /* Backup result ID list, if not, other parsing function will destroy */
    tempIds = refOutIds;
    refOutIds = AtOsalMemAlloc(sizeof(uint32) * numRefOutIds);
    if (refOutIds == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory\r\n");
        return cAtFalse;
        }
    AtOsalMemCpy(refOutIds, tempIds, sizeof(uint32) * numRefOutIds);

    /* Clock source is disconnected by inputing "none" */
    if (AtStrcmp(argv[1], "none") == 0)
        pdhChannels = NULL;
    else
        {
        /* Get list of pdh channel */
        numPdhChannels = De1ListFromString(argv[1], pdhChannels, numPdhChannels);
        if (numPdhChannels == 0)
            {
            AtPrintc(cSevCritical, "ERROR: No PDH channel is parsed\r\n");
            AtOsalMemFree(refOutIds);
            return cAtFalse;
            }

        /* Make sure that two lists have the same number of items */
        if (numPdhChannels != numRefOutIds)
            {
            AtPrintc(cSevCritical, "ERROR: Number of PDH channels and output clock IDs must be the same\r\n");
            AtOsalMemFree(refOutIds);
            return cAtFalse;
            }
        }

    /* Configure */
    for (i = 0; i < numRefOutIds; i++)
        {
        AtModulePdh pdhModule;
        AtPdhDe1 de1Channel;

        pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);

        /* Get corresponding objects */
        if (pdhChannels == NULL)
            de1Channel = NULL;
        else
            de1Channel = (AtPdhDe1)pdhChannels[i];

        /* Apply */
        ret = AtModulePdhOutputClockSourceSet(pdhModule, (uint8)(refOutIds[i] - 1), (AtPdhChannel)de1Channel);
        if(ret != cAtOk)
            AtPrintc(cSevCritical,
                     "ERROR: Output clock of %s to clock #%u fail, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)de1Channel),
                     refOutIds[i],
                     AtRet2String(ret));
        }

    AtOsalMemFree(refOutIds);

    return cAtTrue;
    }

eBool CmdPdhModuleOutClkGet(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 numIds, i;
    const char *heading[] = {"Output clock ID", "Source"};
    AtList extractors = AtListCreate(0);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    numIds = AtListLengthGet(extractors);
    if (numIds == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct extractor lists, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Create table */
    tabPtr = TableAlloc(numIds, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numIds; i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        uint32 extractorId = AtClockExtractorIdGet(extractor);
        StrToCell(tabPtr, i, 0, CliNumber2String(extractorId + 1, "%u"));
        StrToCell(tabPtr, i, 1, (char *)CliChannelIdStringGet((AtChannel)AtModulePdhOutputClockSourceGet(pdhModule, (uint8)extractorId)));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)extractors);

    return cAtTrue;
    }

eBool CmdPdhInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModulePdhPrmCrCompareEnable(char argc, char **argv)
    {
    return PrmCrCompareEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModulePdhPrmCrCompareDisable(char argc, char **argv)
    {
    return PrmCrCompareEnable(argc, argv, cAtFalse);
    }

#if 0
static eBool CliForcePeriodSet(char argc, char **argv,eAtRet (*ForcePeriodSet)(AtAttPdhManager self, uint32 numSeconds))
    {
    if (argc!=1)
        return cAtFalse;
    else
        {
        AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
        AtAttPdhManager mngr = AtModulePdhAttManager(pdhModule);
        eAtRet ret = ForcePeriodSet(mngr, AtStrToDw(argv[0]));

        if (ret != cAtOk)
            return cAtFalse;
        }
    return cAtTrue;
    }

static eBool CliForcePerfrm(char argc, char **argv,eAtRet (*ForcePerframe)(AtAttPdhManager self))
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    AtAttPdhManager mngr = AtModulePdhAttManager(pdhModule);
    eAtRet ret = ForcePerframe(mngr);


    AtUnused(argc);
    AtUnused(argv);
    if (ret != cAtOk)
        return cAtFalse;

    return cAtTrue;
    }

eBool CmdAtModulePdhDe1ForcePeriodSet(char argc, char **argv)
    {
    return CliForcePeriodSet(argc, argv, AtAttPdhManagerDe1ForcePeriodSet);
    }

eBool CmdAtModulePdhDe3ForcePeriodSet(char argc, char **argv)
    {
    return CliForcePeriodSet(argc, argv, AtAttPdhManagerDe3ForcePeriodSet);
    }

eBool CmdAtModulePdhDe3ForcePerfrmEnable(char argc, char **argv)
    {
    return CliForcePerfrm(argc, argv, AtAttPdhManagerDe3ForcePerframeEnable);
    }

eBool CmdAtModulePdhDe3ForcePerfrmDisable(char argc, char **argv)
    {
    return CliForcePerfrm(argc, argv, AtAttPdhManagerDe3ForcePerframeDisable);
    }
#endif

eBool CmdAtModulePdhGet(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(4, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)pdhModule);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));

    row = row + 1;
    enabled = AtModulePdhPrmCrCompareIsEnabled(pdhModule);
    StrToCell(tabPtr, row, 0, "prmCRBitCompared");
    ColorStrToCell(tabPtr, row, 1, CliBoolToString(enabled), enabled ? cSevInfo : cSevNormal);

    row = row + 1;
    StrToCell(tabPtr, row, 0, "numDe1s");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModulePdhNumberOfDe1sGet(pdhModule), "%u"));

    row = row + 1;
    StrToCell(tabPtr, row, 0, "numDe3s");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModulePdhNumberOfDe3sGet(pdhModule), "%u"));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtModulePdhDebug(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cAtModulePdh));
    return cAtTrue;
    }


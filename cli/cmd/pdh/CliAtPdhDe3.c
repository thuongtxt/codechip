/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhDe3.c
 *
 * Created Date: Feb 4, 2013
 *
 * Description : DE3 CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "AtCli.h"
#include "AtPdhChannel.h"
#include "AtPdhDe3.h"
#include "AtModulePdh.h"
#include "AtDevice.h"
#include "CliAtModulePdh.h"
#include "../sdh/CliAtModuleSdh.h"
#include "AtSdhVc.h"
#include "AtChannelHierarchyDisplayer.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"
#include "../common/CliAtChannelObserver.h"
int AtCliExecute(const char* str);

/*--------------------------- Define -----------------------------------------*/
#define cAtPdhDs3Event (cAtPdhDs3AlarmAicChange|cAtPdhDs3AlarmFeacChange|\
                        cAtPdhE3AlarmPldTypeChange|cAtPdhE3AlarmTmChange|\
                        cAtPdhDe3AlarmSsmChange|cAtPdhDs3AlarmMdlChange|\
                        cAtPdhDe3AlarmClockStateChange)

/*--------------------------- Macros -----------------------------------------*/
static const char *cAtPdhDe3AlarmTypeStr[] =
    {
    "los",
    "lof",
    "ais",
    "rai",
    "ber-sf",
    "ber-sd",
    "ber-tca",
    "idle",
    "aic-change",
    "feac-change",
    "tim",
    "pldtype-change",
    "tm-change",
    "ssm-change",
    "mdl-change",
    "clock-state-change"
    };

static const uint32 cAtPdhDe3AlarmTypeVal[] =
    {
    cAtPdhDe3AlarmLos,
    cAtPdhDe3AlarmLof,
    cAtPdhDe3AlarmAis,
    cAtPdhDe3AlarmRai,
    cAtPdhDe3AlarmSfBer,
    cAtPdhDe3AlarmSdBer,
    cAtPdhDe3AlarmBerTca,
    cAtPdhDs3AlarmIdle,
    cAtPdhDs3AlarmAicChange,
    cAtPdhDs3AlarmFeacChange,
    cAtPdhE3AlarmTim,
    cAtPdhE3AlarmPldTypeChange,
    cAtPdhE3AlarmTmChange,
    cAtPdhDe3AlarmSsmChange,
    cAtPdhDs3AlarmMdlChange,
    cAtPdhDe3AlarmClockStateChange
    };

static const char *cAtPdhDe3AlarmTypeUpperStr[] =
    {
     "LOS",
     "LOF",
     "AIS",
     "RAI",
     "BER-SF",
     "BER-SD",
     "BER-TCA",
     "IDLE",
     "AIC",
     "FEAC",
     "TIM",
     "PSL",
     "TM",
     "SSM",
     "MDL",
     "ACR/DCR"
    };

static const char *cAtPdhMdlStandardStr[] =
    {
    "unknown", "ansi", "at&t"
    };

static const uint32 cAtPdhMdlStandardVal[] =
    {
    cAtPdhMdlStandardUnknown,    /**< Unknown standard */
    cAtPdhMdlStandardAnsi,       /**< ANSI standard */
    cAtPdhMdlStandardAtt         /**< AT&T standard */
    };

static const char *cAtPdhDe3DataLinkOptionStr[] =
    {
    "unknown", "dl", "udl"
    };

static const uint32 cAtPdhDe3DataLinkOptionVal[] =
    {
     cAtPdhDe3DataLinkOptionUnknown,
     cAtPdhDe3DataLinkOptionDlBits,
     cAtPdhDe3DataLinkOptionUdlBits
    };

static const char *cAtPdhDe3StuffModeStr[] =
    {
    "fine_stuff",
    "full_stuff"
    };

static const uint32 cAtPdhDe3StuffModeVal[] =
    {
     cAtPdhDe3StuffModeFineStuff,
     cAtPdhDe3StuffModeFullStuff
    };

static const uint32 cAtPdhDe3TxFeacModeVal[] =
    {
    cAtPdhBomSentModeInvalid,
    cAtPdhBomSentModeOneShot,
    cAtPdhBomSentModeContinuous
    };

static const char *cAtPdhDe3TxFeacModeStr[] =
    {
    "invalid", "oneshot", "continuous"
    };

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*De3AttributeSetFunc)(AtPdhDe3 self, uint32 value);
#define mAttributeSetFunc(func) ((De3AttributeSetFunc)(func))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtPdhDe3FrameModeStr[] = {
                                      "ds3_unframed",
                                      "ds3_cbit_unchannelize",
                                      "ds3_cbit_28ds1",
                                      "ds3_cbit_21e1",
                                      "ds3_m13_28ds1",
                                      "ds3_m13_21e1",
                                      "e3_unframed",
                                      "e3_g832",
                                      "e3_g751",
                                      "e3_g751_16e1s"
                                      };

static const eAtPdhDe3FrameType cAtPdhDe3FrameModeVal[]  = {
                                                     cAtPdhDs3Unfrm,
                                                     cAtPdhDs3FrmCbitUnChn,
                                                     cAtPdhDs3FrmCbitChnl28Ds1s,
                                                     cAtPdhDs3FrmCbitChnl21E1s,
                                                     cAtPdhDs3FrmM13Chnl28Ds1s,
                                                     cAtPdhDs3FrmM13Chnl21E1s,
                                                     cAtPdhE3Unfrm,
                                                     cAtPdhE3Frmg832,
                                                     cAtPdhE3FrmG751,
                                                     cAtPdhE3FrmG751Chnl16E1s
                                                    };

static char m_buffer[200];

static const char *cAtPdhDe3FeacTypeStr[] = {
                                      "unknown",
                                      "alarm",
                                      "loop_active",
                                      "loop_deactive",
                                      "history_alarm",
                                      "history_loop_active",
                                      "history_loop_deactive"
                                      };

static const uint32 cAtPdhDe3FeacTypeVal[]  = {cAtPdhDe3FeacTypeUnknown,
                                               cAtPdhDe3FeacTypeAlarm,
                                               cAtPdhDe3FeacTypeLoopActive,
                                               cAtPdhDe3FeacTypeLoopDeactive,
                                               cAtPdhDe3FeacTypeHistoryAlarm,
                                               cAtPdhDe3FeacTypeHistoryLoopActive,
                                               cAtPdhDe3FeacTypeHistoryLoopDeactive};

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 AtChannelHwClockStateGet(AtChannel self);

/*--------------------------- Implementation ---------------------------------*/
static char *SharedBuffer(void)
    {
    return m_buffer;
    }

static eBool ChannelEnable(char argc, char **argv, eBool enable)
    {
    eAtRet      ret = cAtOk;
    uint32      numChannels, i;
    AtChannel   *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtChannelEnable(channelList[i], enable);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable");
        }

    return cAtTrue;
    }

static eBool Enable(char argc, char **argv, EnableFunc func, eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS3/E3 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = func((AtPdhChannel)channelList[i], enable);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable this function!");
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool AttributeSet(char argc, char **argv, uint32 value, De3AttributeSetFunc setFunc)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       eAtRet ret = setFunc((AtPdhDe3)channelList[i], value);
       if (ret != cAtOk)
           {
           AtPrintc(cSevCritical,
                    "ERROR: Can not configure channel %s, ret = %s\r\n",
                    AtChannelIdString(channelList[i]),
                    AtRet2String(ret));
           success = cAtFalse;
           }
       }

    return success;
    }

static eBool IdleEnable(char argc, char **argv, eBool enable)
    {
    eAtRet      ret = cAtOk;
    uint32      numChannels, i;
    AtChannel   *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhDe3TxIdleEnable((AtPdhDe3)channelList[i], enable);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable");
        }

    return cAtTrue;
    }

static uint32 CliPdhDe3IntrMaskFromString(char *pIntrStr)
    {
    return CliMaskFromString(pIntrStr, cAtPdhDe3AlarmTypeStr, cAtPdhDe3AlarmTypeVal, mCount(cAtPdhDe3AlarmTypeVal));
    }

static char *CliPdhDe3AlarmStrFromMask(uint32 alarmMask)
    {
    uint8       i;
    static char allAlarms[128];
    static char alarm[16];

    AtOsalMemInit(allAlarms, 0, sizeof(allAlarms));
    AtOsalMemInit(alarm, 0, sizeof(alarm));

    for (i = 0; i < mCount(cAtPdhDe3AlarmTypeStr); i++)
        {
        if ((alarmMask & cAtPdhDe3AlarmTypeVal[i]) == 0)
            continue;

        AtSprintf(alarm, "|%s", cAtPdhDe3AlarmTypeStr[i]);
        AtStrcat(allAlarms, alarm);
        }

    return &allAlarms[1];
    }

static void FeaccodeReceive(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    eAtPdhDe3FeacType feacType;
    uint8 feacCode;
    char feacTypeStr[128];
    eBool convertSuccess;

    AtUnused(currentStatus);

    if ((changedAlarms & cAtPdhDs3AlarmFeacChange) == 0)
        return;

    if (AtPdhDe3RxFeacGet((AtPdhDe3)channel, &feacType, &feacCode) != cAtOk)
        {
        AtPrintc(cSevNormal,
                 "\r\n%s: Get Rx Feac failed\r\n",
                 CliChannelIdStringGet(channel));
        return;

        }

    AtOsalMemInit(feacTypeStr, sizeof(feacTypeStr), 0);
    mAtEnumToStr(cAtPdhDe3FeacTypeStr, cAtPdhDe3FeacTypeVal, feacType, feacTypeStr, convertSuccess);
    if (convertSuccess)
        {
        AtPrintc(cSevNormal,
                 "\r\n%s: detect control = %s, new loopcode = %d\r\n",
                 CliChannelIdStringGet(channel), feacTypeStr, feacCode);
        }
    else
        {
        AtPrintc(cSevNormal,
                 "\r\n%s: detect control = %d, new loopcode = %d\r\n",
                 CliChannelIdStringGet(channel), feacType, feacCode);
        }
    }

static eBool FeacNotification(char *pDe3Str, eBool enable)
    {
    uint32 numberOfDe3, i;
    AtChannel *de3s;
    static tAtChannelEventListener de3FeacListener;
    eBool success = cAtTrue;

    /* Get the shared buffer to hold all of objects, then call the parse function */
    de3s = CliSharedChannelListGet(&numberOfDe3);
    numberOfDe3 = CliDe3ListFromString(pDe3Str, de3s, numberOfDe3);
    if (!numberOfDe3)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    AtOsalMemInit(&de3FeacListener, 0, sizeof(de3FeacListener));
    de3FeacListener.AlarmChangeState = FeaccodeReceive;

    /* Register Interrupt listener */
    for (i = 0; i < numberOfDe3; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;

        /* Register/deregister alarm listener */
        if (enable)
            ret = AtChannelEventListenerAdd(de3s[i], &de3FeacListener);
        else
            ret = AtChannelEventListenerRemove(de3s[i], &de3FeacListener);

        /* Let user know if error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener on %s, ret = %s\r\n",
                     enable ? "register" : "deregister",
                     CliChannelIdStringGet(de3s[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool FeacIsApplicable(AtPdhChannel de3)
    {
    uint32 frameMode = AtPdhChannelFrameTypeGet(de3);

    if ((frameMode == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (frameMode == cAtPdhDs3FrmCbitChnl21E1s)  ||
        (frameMode == cAtPdhDs3FrmCbitUnChn))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 FeacToCell(tTab *tabPtr, uint32 row, uint32 startColumn, AtPdhDe3 de3, eAtModulePdhRet (*FeacGet)(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code))
    {
    eAtPdhDe3FeacType feacType;
    uint8 feacCode;

    if (FeacGet(de3, &feacType, &feacCode) == cAtOk)
        {
        const char *string = CliEnumToString(feacType, cAtPdhDe3FeacTypeStr, cAtPdhDe3FeacTypeVal, mCount(cAtPdhDe3FeacTypeVal), NULL);
        ColorStrToCell(tabPtr, row, startColumn++,  string ? string : "Error", string ? cSevNormal : cSevCritical);
        StrToCell(tabPtr, row, startColumn++, CliNumber2String(feacCode, "0x%x"));
        }
    else
        {
        ColorStrToCell(tabPtr, row, startColumn++, "Error", cSevCritical);
        ColorStrToCell(tabPtr, row, startColumn++, "Error", cSevCritical);
        }

    /* Return next column */
    return (uint8)startColumn;
    }

static void AlarmToCell(tTab *tabPtr, uint32 i, uint32 column, uint32 alarm, uint32 alarmType)
    {
    eBool set = (alarm & alarmType) ? cAtTrue : cAtFalse;
    ColorStrToCell(tabPtr, i, column, set ? "set" : "clear", set ? cSevCritical : cSevInfo);
    }

static eBool SsmEnable(char argc, char** argv, eBool enable)
    {
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhChannelSsmEnable((AtPdhChannel)channelList[de3_i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable SSM for DE3 %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[de3_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelObserver observer = userData;

    AtChannelObserverDefectsUpdate(observer, changedAlarms, currentStatus);

    if (changedAlarms & cAtPdhDe3AlarmClockStateChange)
        AtChannelObserverClockStatePush(observer, AtChannelClockStateGet(channel));
    }

static eBool BitOrderSet(char argc, char **argv, eAtModulePdhRet (*udlBitOrderSetFunc)(AtPdhDe3 self, eAtBitOrder bitOrder))
    {
    eAtBitOrder bitOrder = CliBitOrderFromString(argv[1]);

    if (bitOrder == cAtBitOrderUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid bit order: %s\r\n", CliValidBitOrderModes());
        return cAtFalse;
        }

    return AttributeSet(argc, argv, bitOrder, mAttributeSetFunc(udlBitOrderSetFunc));
    }

static eAtModulePdhRet TxRxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    eAtRet ret = cAtOk;

    ret |= AtPdhDe3TxUdlBitOrderSet(self, bitOrder);
    ret |= AtPdhDe3RxUdlBitOrderSet(self, bitOrder);

    return ret;
    }

static eBool AlarmAffectingEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32      numChannels, i;
    AtChannel   *channelList;
    uint32 alarmValue = 0;
    AtUnused(argc);

    /* Get list of paths */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Alarm types */
    alarmValue = CliPdhDe3IntrMaskFromString(argv[1]);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        ret |= AtPdhChannelAlarmAffectingEnable((AtPdhChannel)channelList[i], alarmValue, enable);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdPdhDe3FramingModeSet(char argc, char **argv)
    {
    eAtPdhDe3FrameType  frameMode = cAtPdhDe3FrameUnknown;
    eBool convertSuccess = cAtFalse;

    mAtStrToEnum(cAtPdhDe3FrameModeStr, cAtPdhDe3FrameModeVal, argv[1], frameMode, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown framing mode %s. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cAtPdhDe3FrameModeStr, mCount(cAtPdhDe3FrameModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return AttributeSet(argc, argv, frameMode, mAttributeSetFunc(AtPdhChannelFrameTypeSet));
    }

eBool CmdPdhDe3TimingModeSet(char argc, char **argv)
    {
    eAtRet          ret = cAtOk;
    uint32          numChannels, i;
    AtChannel       *channelList, timingSrc = NULL;
    eAtTimingMode   timingMode;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    timingMode = CliTimingModeFromString(argv[1]);
    if (timingMode == cAtTimingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid timing mode parameter. Expected: %s\r\n", CliValidTimingModes());
        return cAtFalse;
        }

    if (timingMode == cAtTimingModeSlave)
        {
        uint32 numMaster;
        numMaster = CliDe3ListFromString(argv[2], &timingSrc, 1);
        if (numMaster != 1)
            {
            AtPrintc(cSevCritical, "ERROR: Timing source is not correct\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       ret = AtChannelTimingSet(channelList[i], timingMode, timingSrc);
       if (ret != cAtOk)
           AtPrintc(cSevCritical, "ERROR: Can not set timing mode for %s, ret = %s\r\n",
                    CliChannelIdStringGet(channelList[i]),
                    AtRet2String(ret));
       }

    return cAtTrue;
    }

static void DefectLogging(AtChannel channel, uint32 currentDefect, eBool enable)
    {
    uint8 alarm_i;

    for (alarm_i = 0; alarm_i < mCount(cAtPdhDe3AlarmTypeUpperStr); alarm_i++)
        {
        eAtPdhDe3AlarmType alarmType = cAtPdhDe3AlarmTypeVal[alarm_i];
        if (currentDefect & alarmType)
            {
            AtPrintc(cSevNormal, "\r\n%s [PDH] Defect [%s] %s at channel %s\r\n",
                     AtOsalDateTimeInUsGet(), cAtPdhDe3AlarmTypeUpperStr[alarm_i], enable ? "forced" : "unforced", CliChannelIdStringGet(channel));
            return;
            }
        }
    }

eBool CmdPdhDe3AlarmForce(char argc, char **argv)
    {
    eAtRet      ret = cAtOk;
    uint32      numChannels, alarmMask, i;
    AtChannel   *channelList;
    eBool       enable = cAtFalse;
    eBool       blResult = cAtFalse;
    eAtRet      (*Func)(AtChannel, uint32);
    eAtDirection direction;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    direction = CliDirectionFromStr(argv[1]);
    if(direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expected %s\r\n", CliValidDirections());
        return cAtFalse;
        }

    alarmMask = CliPdhDe3IntrMaskFromString(argv[2]);
    mAtStrToBool(argv[3], enable, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling operation. Expected: en/dis\r\n");
        return cAtFalse;
        }

    if (enable)
        Func = (direction == cAtDirectionRx) ? AtChannelRxAlarmForce : AtChannelTxAlarmForce;
    else
        Func = (direction == cAtDirectionRx) ? AtChannelRxAlarmUnForce : AtChannelTxAlarmUnForce;

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = Func((AtChannel)channelList[i], alarmMask);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Can not force alarms for %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
        if (CliTimeLoggingIsEnabled())
            DefectLogging((AtChannel)channelList[i], alarmMask, enable);
        }

    return cAtTrue;
    }

eBool CmdPdhDe3LoopbackSet(char argc, char **argv)
    {
    uint32 loopMode;

    loopMode = CliPdhLoopbackModeFromString(argv[1]);
    if (loopMode == cAtPdhLoopbackModeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid loopback mode. Expected: %s\r\n", CliPdhValidLoopbackModes());
        return cAtFalse;
        }

    return AttributeSet(argc, argv, loopMode, mAttributeSetFunc(AtChannelLoopbackSet));
    }

eBool CmdPdhDe3InterruptMaskSet(char argc, char **argv)
    {
    eAtRet      ret = cAtOk;
    eBool       blResult = cAtFalse;
    uint32      numChannels, i;
    AtChannel   *channelList;
    uint32      intrMaskVal;
    eBool       enable = cAtFalse;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    intrMaskVal = CliPdhDe3IntrMaskFromString(argv[1]);

    mAtStrToBool(argv[2], enable, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling operation. Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       ret = AtChannelInterruptMaskSet(channelList[i], intrMaskVal, (enable) ? intrMaskVal : 0);
       if (ret != cAtOk)
           AtPrintc(cSevCritical,
                    "ERROR: Can not set interrupt mask of %s, ret = %s\r\n",
                    CliChannelIdStringGet(channelList[i]),
                    AtRet2String(ret));
       }

    return cAtTrue;
    }

eBool CmdPdhDe3Show(char argc, char **argv)
    {
    uint32              numChannels, i, intrMaskVal, alarmMask;
    AtChannel           *channelList;
    AtPdhDe3            pdhDe3;
    eAtPdhLoopbackMode  loopbackMode;
    eAtPdhDe3FrameType  frameMode;
    eAtTimingMode       timingType;
    eBool               blResult = cAtFalse;
    eBool               enable;
    tTab                *tabPtr;
    const char          *pHeading[] = {"DE3 ID", "Framingmode", "TimingMode", "TimingSource", "clockState","hwClockState",
                                       "Idle", "Loopback", "Interrupt","ForcedTxAlarms",
                                       "ForcedRxAlarms", "Bound", "Enable", "AutoRai", "LosDetection"};
    char* shareBuffer = SharedBuffer();
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to display or Invalid DS3/E3 list, expected: 1 or 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char *timingModeString;
        AtChannel timingSrc, boundChannel;
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        frameMode = AtPdhChannelFrameTypeGet((AtPdhChannel)channelList[i]);
        mAtEnumToStr(cAtPdhDe3FrameModeStr, cAtPdhDe3FrameModeVal, frameMode, shareBuffer, blResult);
        ColorStrToCell(tabPtr, i, column++, blResult ? SharedBuffer() : "Error", blResult ? cSevInfo : cSevCritical);

        timingType = AtChannelTimingModeGet(channelList[i]);
        timingModeString = CliTimingModeString(timingType);
        ColorStrToCell(tabPtr, i, column++, timingModeString ? timingModeString : "Error", timingModeString ? cSevInfo : cSevCritical);

        timingSrc = AtChannelTimingSourceGet(channelList[i]);
        StrToCell(tabPtr, i, column++, timingSrc ? CliChannelIdStringGet(timingSrc) : "none");
        StrToCell(tabPtr, i, column++, CliClockStateString(AtChannelClockStateGet(channelList[i])));
        AtSprintf(SharedBuffer(), "%d", AtChannelHwClockStateGet(channelList[i]));
        StrToCell(tabPtr, i, column++, SharedBuffer());

        pdhDe3 = (AtPdhDe3)channelList[i];
        mAtBoolToStr(AtPdhDe3TxIdleIsEnabled(pdhDe3), SharedBuffer(), blResult);
        ColorStrToCell(tabPtr, i, column++, SharedBuffer(), cSevInfo);

        loopbackMode = AtChannelLoopbackGet((AtChannel)channelList[i]);
        StrToCell(tabPtr, i, column++, CliPdhLoopbackModeString(loopbackMode));

        intrMaskVal = AtChannelInterruptMaskGet(channelList[i]);
        AtSprintf(SharedBuffer(), "%s", CliPdhDe3AlarmStrFromMask(intrMaskVal));
        ColorStrToCell(tabPtr, i, column++, (intrMaskVal) ? SharedBuffer() : "None", (intrMaskVal) ? cSevCritical : cSevInfo);

        alarmMask = AtChannelTxForcedAlarmGet((AtChannel)channelList[i]);
        AtStrcpy(SharedBuffer(), CliPdhDe3AlarmStrFromMask(alarmMask));
        ColorStrToCell(tabPtr, i, column++, (alarmMask) ? SharedBuffer() : "None", (alarmMask) ? cSevCritical : cSevInfo);

        alarmMask = AtChannelRxForcedAlarmGet((AtChannel)channelList[i]);
        AtStrcpy(SharedBuffer(), CliPdhDe3AlarmStrFromMask(alarmMask));
        ColorStrToCell(tabPtr, i, column++, (alarmMask) ? SharedBuffer() : "None", (alarmMask) ? cSevCritical : cSevInfo);

        boundChannel = AtChannelBoundChannelGet(channelList[i]);
        if (boundChannel)
            ColorStrToCell(tabPtr, i, column++, CliChannelIdStringGet(boundChannel), cSevInfo);
        else
            ColorStrToCell(tabPtr, i, column++, "None", cSevNormal);

        enable = AtChannelIsEnabled(channelList[i]);
        mAtBoolToStr(enable, SharedBuffer(), blResult);
        ColorStrToCell(tabPtr, i, column++, blResult ? SharedBuffer() : "Error", enable ? cSevInfo : cSevCritical);

        enable = AtPdhChannelAutoRaiIsEnabled((AtPdhChannel)channelList[i]);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);

        /* LOS detection */
        enable = AtPdhChannelLosDetectionIsEnabled((AtPdhChannel)channelList[i]);
        ColorStrToCell(tabPtr, i, column++,  CliBoolToString(enable), enable ? cSevInfo : cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe3AlarmShow(char argc, char **argv)
    {
    uint32      numChannels, i, alarms;
    AtChannel   *channelList;
    tTab        *tabPtr;
    const char *pHeading[] = {"DE3 ID", "LOS", "LOF", "AIS", "RAI", "AIC", "IDLE", "FEAC change", "TM change", "PT change", "SSM change", "BER-SD", "BER-SF", "BER-TCA"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

        alarms = AtChannelAlarmGet(channelList[i]);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmLos);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmLof);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmAis);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmRai);

        if (AtPdhDe3IsE3((AtPdhDe3)channelList[i]))
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhE3AlarmTmChange);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhE3AlarmPldTypeChange);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDe3AlarmSsmChange);
            }
        else
            {
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDs3AlarmAicChange);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDs3AlarmIdle);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDs3AlarmFeacChange);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }

        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmSdBer);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmSfBer);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmBerTca);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe3ListenedAlarmShow(char argc, char **argv)
    {
    uint32      numChannels, i, alarms;
    AtChannel   *channelList;
    tTab        *tabPtr;
    const char *pHeading[] = {"DE3 ID", "Count", "Matched", "LOS", "LOF", "AIS", "RAI", "AIC", "IDLE", "FEAC change", "TM change", "PT change", "SSM change", "BER-SD", "BER-SF", "BER-TCA"};
    uint32 (*ListenedDefectGet)(AtChannel self, uint32 * listenedCount);
    eAtHistoryReadingMode readingMode;
    uint32 counter;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    ListenedDefectGet = AtChannelListenedDefectGet;
    if (argc > 1)
        {
        /* Get reading action mode */
        readingMode = CliHistoryReadingModeGet(argv[1]);
        if (readingMode == cAtHistoryReadingModeUnknown)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
            return cAtFalse;
            }

        if (readingMode == cAtHistoryReadingModeReadToClear)
            ListenedDefectGet = AtChannelListenedDefectClear;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

        alarms = ListenedDefectGet(channelList[i], &counter);
        StrToCell(tabPtr, i, column++, CliNumber2String(counter, "%u"));
        StrToCell(tabPtr, i, column++, "TBD");

        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmLos);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmLof);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmAis);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmRai);

        if (AtPdhDe3IsE3((AtPdhDe3)channelList[i]))
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhE3AlarmTmChange);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhE3AlarmPldTypeChange);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDe3AlarmSsmChange);
            }
        else
            {
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDs3AlarmAicChange);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDs3AlarmIdle);
            AlarmToCell(tabPtr, i, column++, alarms, cAtPdhDs3AlarmFeacChange);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }

        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmSdBer);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmSfBer);
        AlarmToCell(tabPtr, i, column++, alarms ,cAtPdhDe3AlarmBerTca);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool  CmdPdhDe3InterruptShow(char argc, char **argv)
    {
    uint32      numChannels, i, alarmBitmap;
    AtChannel   *channelList;
    tTab        *tabPtr = NULL;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    const char  *pHeading[] = {"DE3 ID", "LOS", "LOF", "AIS", "RAI", "AIC", "IDLE",
                               "FEAC change", "TM change", "PT change", "SSM change",
                               "MDL change", "BER-SD", "BER-SF", "BER-TCA", "ClockStateChange"};
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);
    
    if (!silent)
        {
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channelList[i];
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        if (readingMode == cAtHistoryReadingModeReadToClear)
            alarmBitmap = AtChannelAlarmInterruptClear(channel);
        else
            alarmBitmap = AtChannelAlarmInterruptGet(channel);

        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmLos);
        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmLof);
        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmAis);
        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmRai);

        if (AtPdhDe3IsE3((AtPdhDe3)channel))
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            AlarmToCell(tabPtr, i, column++, alarmBitmap ,cAtPdhE3AlarmTmChange);
            AlarmToCell(tabPtr, i, column++, alarmBitmap ,cAtPdhE3AlarmPldTypeChange);
            AlarmToCell(tabPtr, i, column++, alarmBitmap ,cAtPdhDe3AlarmSsmChange);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }
        else
            {
            AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDs3AlarmAicChange);
            AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDs3AlarmIdle);
            AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDs3AlarmFeacChange);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            AlarmToCell(tabPtr, i, column++, alarmBitmap,cAtPdhDe3AlarmSsmChange);
            AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDs3AlarmMdlChange);
            }

        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmSdBer);
        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmSfBer);
        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmBerTca);
        AlarmToCell(tabPtr, i, column++, alarmBitmap, cAtPdhDe3AlarmClockStateChange);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe3CounterGet(char argc, char **argv)
    {
    uint32      numChannels, row_i, counter_i;
    AtChannel   *channelList;
    uint16      column;
    tTab        *tabPtr = NULL;
    uint32      counterValue;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    uint8       counterTypes[] = {cAtPdhDe3CounterFBit, cAtPdhDe3CounterRei,
                                  cAtPdhDe3CounterPBit, cAtPdhDe3CounterCPBit, cAtPdhDe3CounterBpvExz, cAtPdhDe3CounterTxCs};
    const char* pHeading[] = {"DE3 ID", "F-Bit", "REI", "P-Bit", "CP-Bit", "BPV-EXZ", "TX-CS"};
    uint32 (*CounterRead)(AtChannel self, uint16 counterType);
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to display, ID list may be not correct\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if(readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    CounterRead = (readingMode == cAtHistoryReadingModeReadToClear) ? AtChannelCounterClear : AtChannelCounterGet;

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);
    
    if (!silent)
        {
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }
        }

    for (row_i = 0; row_i < numChannels; row_i = AtCliNextIdWithSleep(row_i, AtCliLimitNumRestTdmChannelGet()))
        {
        column = 0;
        StrToCell(tabPtr, row_i, column++, CliChannelIdStringGet((AtChannel)channelList[row_i]));

        for (counter_i = 0; counter_i < mCount(counterTypes); counter_i++)
            {
            counterValue = CounterRead((AtChannel)channelList[row_i], counterTypes[counter_i]);
            CliCounterPrintToCell(tabPtr, row_i, column++, counterValue, cAtCounterTypeError, cAtTrue);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe3FeacShow(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE3 ID", "TxFEACControl", "TxFEACCode", "TxSentMode", "RxFEACControl", "RxFEACCode"};

    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPdhDe3 de3 = (AtPdhDe3)channelList[i];

        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)channelList[i]));

        if (FeacIsApplicable((AtPdhChannel)de3))
            {
            eAtPdhBomSentMode sentMode = AtPdhDe3TxFeacModeGet(de3);
            const char *sentModeStr = CliEnumToString(sentMode, cAtPdhDe3TxFeacModeStr, cAtPdhDe3TxFeacModeVal, mCount(cAtPdhDe3TxFeacModeVal), NULL);
            FeacToCell(tabPtr, i, 1, de3, AtPdhDe3TxFeacGet);
            StrToCell(tabPtr, i, 3, sentModeStr);
            FeacToCell(tabPtr, i, 4, de3, AtPdhDe3RxFeacGet);
            }
        else
            {
            StrToCell(tabPtr, i, 1, sAtNotApplicable);
            StrToCell(tabPtr, i, 2, sAtNotApplicable);
            StrToCell(tabPtr, i, 3, sAtNotApplicable);
            StrToCell(tabPtr, i, 4, sAtNotApplicable);
            StrToCell(tabPtr, i, 5, sAtNotApplicable);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe3PayloadTypeSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtStrToDw(argv[1]), mAttributeSetFunc(AtPdhDe3TxPldTypeSet));
    }

eBool CmdPdhDe3TmEnable(char argc, char **argv)
    {
    return AttributeSet(argc, argv, cAtTrue, mAttributeSetFunc(AtPdhDe3TmEnable));
    }

eBool CmdPdhDe3TmDisable(char argc, char **argv)
    {
    return AttributeSet(argc, argv, cAtFalse, mAttributeSetFunc(AtPdhDe3TmEnable));
    }

eBool CmdPdhDe3TmCodeSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtStrToDw(argv[1]), mAttributeSetFunc(AtPdhDe3TxTmCodeSet));
    }

eBool CmdPdhDe3TransmitNbit(char argc, char **argv)
    {
    uint32      numChannels, i;
    AtChannel   *channelList;
    uint8       nBitValue;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    nBitValue = (uint8)AtStrToDw(argv[1]);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       eAtRet ret = AtPdhChannelNationalBitsSet((AtPdhChannel)channelList[i], nBitValue);
       if (ret != cAtOk)
           {
           AtPrintc(cSevCritical,
                    "ERROR: Can not set national bit of %s, ret = %s\r\n",
                    CliChannelIdStringGet(channelList[i]),
                    AtRet2String(ret));
           success = cAtFalse;
           }
       }

    return success;
    }

eBool CmdPdhDe3Enable(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe3Disable(char argc, char **argv)
    {
    return ChannelEnable(argc, argv, cAtFalse);
    }

eBool CmdPdhDe3OverHeadShow(char argc, char **argv)
    {
    uint32              numChannels, i;
    AtChannel           *channelList;
    uint8               nbits;
    tTab                *tabPtr;
    const char          *pHeading[] = {"DE3 ID", "TxTM", "RxTM", "TxPldType", "RxPldType", "TxNbit", "RxNbit",
                                       "TxFEACControl", "TxFEACCode", "RxFEACControl", "RxFEACCode", "RxAIC"};
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 column = 0;
        AtPdhDe3 de3 = (AtPdhDe3)channelList[i];
        eAtPdhDe3FrameType  frameMode = AtPdhChannelFrameTypeGet((AtPdhChannel)channelList[i]);
        eBool isE3G832 = (frameMode == cAtPdhE3Frmg832) ? cAtTrue : cAtFalse;
        eBool ignoreTm = cAtFalse;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

        ignoreTm = isE3G832 && AtPdhDe3TmIsEnabled(de3);
        AtSprintf(SharedBuffer(), "%d", AtPdhDe3TxTmCodeGet(de3));
        ColorStrToCell(tabPtr, i, column++, ignoreTm ? SharedBuffer() : "xxx", cSevInfo);
        AtSprintf(SharedBuffer(), "%d", AtPdhDe3RxTmCodeGet(de3));
        ColorStrToCell(tabPtr, i, column++, ignoreTm ? SharedBuffer() : "xxx", cSevInfo);

        /* Payload Type */
        if (isE3G832)
            {
            AtSprintf(SharedBuffer(), "%d", AtPdhDe3TxPldTypeGet(de3));
            ColorStrToCell(tabPtr, i, column++, SharedBuffer(), cSevInfo);
            AtSprintf(SharedBuffer(), "%d", AtPdhDe3RxPldTypeGet(de3));
            ColorStrToCell(tabPtr, i, column++, SharedBuffer(), cSevInfo);
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }

        /* National bits */
        if (AtPdhChannelNationalBitsSupported((AtPdhChannel)de3))
            {
            nbits = AtPdhChannelNationalBitsGet((AtPdhChannel)channelList[i]);
            AtSprintf(SharedBuffer(), "%d", nbits);
            StrToCell(tabPtr, i, column++, SharedBuffer());

            nbits = AtPdhChannelRxNationalBitsGet((AtPdhChannel)channelList[i]);
            AtSprintf(SharedBuffer(), "%d", nbits);
            StrToCell(tabPtr, i, column++, SharedBuffer());
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }

        /* FEAC */
        if (AtPdhDe3IsDs3CbitFrameType(frameMode))
            {
            column = FeacToCell(tabPtr, i, column++, de3, AtPdhDe3TxFeacGet);
            column = FeacToCell(tabPtr, i, column++, de3, AtPdhDe3RxFeacGet);
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }

        /* AIC */
        if (AtPdhDe3IsDs3CbitFrameType(frameMode))
            {
            AtSprintf(SharedBuffer(), "%d", AtPdhDe3RxAicGet(de3));
            StrToCell(tabPtr, i, column++, SharedBuffer());
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe3IdleEnable(char argc, char **argv)
    {
    return IdleEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe3IdleDisable(char argc, char **argv)
    {
    return IdleEnable(argc, argv, cAtFalse);
    }

eBool CmdPdhDe3TransmitFeacSet(char argc, char **argv)
    {
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    eAtPdhDe3FeacType feacType;
    uint8 feacCode;
    eAtPdhBomSentMode sentMode = 0;
    eBool usedSentMode = cAtFalse;

    if (argc < 3)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough params\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Get feac code */
    mAtStrToEnum(cAtPdhDe3FeacTypeStr, cAtPdhDe3FeacTypeVal, argv[1], feacType, convertSuccess);
    feacCode = (uint8)AtStrToDw(argv[2]);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown FEAC type %s. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cAtPdhDe3FeacTypeStr, mCount(cAtPdhDe3FeacTypeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    if (argc == 4)
        {
        usedSentMode = cAtTrue;
        sentMode = CliStringToEnum(argv[3], cAtPdhDe3TxFeacModeStr, cAtPdhDe3TxFeacModeVal, sizeof(cAtPdhDe3TxFeacModeVal), &success);
        if (!success)
            {
            AtPrintc(cSevCritical, "Invalid sent mode \n");
            return success;
            }
        }

    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = cAtOk;

        if (!usedSentMode)
            ret = AtPdhDe3TxFeacSet((AtPdhDe3)channelList[de3_i], feacType, feacCode);
        else
            ret = AtPdhDe3TxFeacWithModeSet((AtPdhDe3)channelList[de3_i], feacType, feacCode, sentMode);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Can not change FEAC code/control of %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[de3_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe3FeacNotificationEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return FeacNotification(argv[0], cAtTrue);
    }

eBool CmdPdhDe3FeacNotificationDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return FeacNotification(argv[0], cAtFalse);
    }

eBool CmdPdhDe3LogShow(char argc, char** argv)
    {
    uint32 numChannels, de3_i;
    AtChannel *channelList;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        AtChannelLogShow(channelList[de3_i]);

    return cAtTrue;
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtChannelLogEnable(channelList[de3_i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable logging for DE3 %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[de3_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe3LogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe3LogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

static const char * TimingModeStr(uint32 mode)
    {
    switch (mode)
        {
        case cAtTimingModeAcr: return "ACR";
        case cAtTimingModeDcr: return "DCR";
        default:               return "Clock";
        }
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [PDH] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtPdhDe3AlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtPdhDe3AlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtPdhDe3AlarmClockStateChange)
            AtPrintc(cSevNormal, " * [%s:", TimingModeStr(AtChannelTimingModeGet(channel)));
        else
            AtPrintc(cSevNormal, " * [%s:", cAtPdhDe3AlarmTypeUpperStr[alarm_i]);
        if (changedAlarms & alarmType & cAtPdhDs3Event)
            {
            char str[128];
            switch (alarmType)
                {
                case cAtPdhDs3AlarmFeacChange:
                    {
                    eAtPdhDe3FeacType feacType = cAtPdhDe3FeacTypeUnknown;
                    uint8 feacCode;
                    AtPdhDe3RxFeacGet((AtPdhDe3)channel, &feacType, &feacCode);
                    AtPrintc(cSevNormal, "%s-%d", cAtPdhDe3FeacTypeStr[feacType], feacCode);
                    }break;
                case cAtPdhDe3AlarmSsmChange:
                    AtPrintc(cSevNormal, "0x%x", AtPdhChannelRxSsmGet((AtPdhChannel)channel));
                    break;
                case cAtPdhDs3AlarmMdlChange:
                    AtSprintf(str, "show pdh de3 mdl rx %s", AtChannelIdString(channel));
                    AtPrintf("\n");
                    AtCliExecute(str);
                    break;
                case cAtPdhDe3AlarmClockStateChange:
                    {
                    AtPrintc(cSevNormal, "SW: %s, HW: %d",
                             CliClockStateString(AtChannelClockStateGet(channel)),
                             AtChannelHwClockStateGet(channel));
                    }
                    break;
                default:
                    AtPrintc(cSevInfo, "detect");
                }
            }
        else
            {
            if (currentStatus & alarmType)
                AtPrintc(cSevCritical, "set");
            else
                AtPrintc(cSevInfo, "clear");
            }
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cAtPdhDe3AlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtPdhDe3AlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtPdhDe3AlarmClockStateChange)
            logPtr += AtSprintf(logPtr, " * [%s:", TimingModeStr(AtChannelTimingModeGet(channel)));
        else
            logPtr += AtSprintf(logPtr, " * [%s:", cAtPdhDe3AlarmTypeUpperStr[alarm_i]);
        if (changedAlarms & alarmType & cAtPdhDs3Event)
            {
            switch (alarmType)
                {
                case cAtPdhDs3AlarmFeacChange:
                    {
                    eAtPdhDe3FeacType feacType = cAtPdhDe3FeacTypeUnknown;
                    uint8 feacCode;
                    AtPdhDe3RxFeacGet((AtPdhDe3)channel, &feacType, &feacCode);
                    logPtr += AtSprintf(logPtr, "%s-%d", cAtPdhDe3FeacTypeStr[feacType], feacCode);
                    }break;
                case cAtPdhDe3AlarmSsmChange:
                    logPtr += AtSprintf(logPtr, "0x%x", AtPdhChannelRxSsmGet((AtPdhChannel)channel));
                    break;
                case cAtPdhDe3AlarmClockStateChange:
                    {
                    logPtr += AtSprintf(logPtr, "SW: %s, HW: %d",
                                        CliClockStateString(AtChannelClockStateGet(channel)),
                                        AtChannelHwClockStateGet(channel));
                    }
                    break;
                default:
                    logPtr += AtSprintf(logPtr, "detect");
                }
            }
        else
            {
            if (currentStatus & alarmType)
                logPtr += AtSprintf(logPtr, "set");
            else
                logPtr += AtSprintf(logPtr, "clear");
            }
        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

const char **CliAtPdhDe3AlarmTypeStr(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPdhDe3AlarmTypeStr);
    return cAtPdhDe3AlarmTypeStr;
    }

const uint32 *CliAtPdhDe3AlarmTypeVal(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPdhDe3AlarmTypeVal);
    return cAtPdhDe3AlarmTypeVal;
    }

eBool CmdPdhDe3AlarmCapture(char argc, char **argv)
    {
    uint32 numberOfDe3, i;
    AtChannel *de3s;
    static tAtChannelEventListener intrListener;
    eBool convertSuccess;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    de3s = CliSharedChannelListGet(&numberOfDe3);
    numberOfDe3 = CliDe3ListFromString(argv[0], de3s, numberOfDe3);
    if (!numberOfDe3)
        {
        AtPrintc(cSevCritical, "Invalid DS3/E3 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    if (AutotestIsEnabled())
        intrListener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        intrListener.AlarmChangeState = AlarmChangeLogging;
    else
        intrListener.AlarmChangeState = AlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    /* Register Interrupt listener */
    for (i = 0; i < numberOfDe3; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = cAtOk;

        /* Register/deregister alarm listener */
        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(de3s[i], &intrListener, CliAtPdhDe3ObserverGet(de3s[i]));
            else
                ret = AtChannelEventListenerAdd(de3s[i], &intrListener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(de3s[i], &intrListener);

            if (AutotestIsEnabled())
                CliAtPdhDe3ObserverDelete(de3s[i]);
            }

        /* Let user know if error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener on %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(de3s[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe3MdlStandardSet(char argc, char **argv)
    {
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    eAtPdhMdlStandard standard;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Get standard */
    mAtStrToEnum(cAtPdhMdlStandardStr, cAtPdhMdlStandardVal, argv[1], standard, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: invalid MDL Standard, expected: ansi or at&t\n");
        return cAtFalse;
        }

    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtPdhDe3MdlStandardSet((AtPdhDe3)channelList[de3_i], standard);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Can not change MDL Standard of %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[de3_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe3MdlShow(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE3 ID", "MDL Standard"};

    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eBool result;

        uint32 value = AtPdhDe3MdlStandardGet((AtPdhDe3)channelList[channel_i]);
        StrToCell(tabPtr, channel_i, 0, CliChannelIdStringGet((AtChannel)channelList[channel_i]));

        mAtEnumToStr(cAtPdhMdlStandardStr, cAtPdhMdlStandardVal, value, m_buffer, result);
        ColorStrToCell(tabPtr, channel_i, 1, result ? m_buffer : "Error", result ? cSevInfo : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDe3MapHierarchyShow(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    AtChannelHierarchyDisplayer displayer;
    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);

    displayer = AtPdhChannelHierarchyDisplayerGet(cAtChannelHierarchyModeAlarm, 0);
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        if (channelList[i] == NULL)
            continue;

        AtChannelHierarchyShow(displayer, channelList[i], "");
        }

    return cAtTrue;
    }

eBool CmdPdhDe3SsmEnable(char argc, char** argv)
    {
    return SsmEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe3SsmDisable(char argc, char** argv)
    {
    return SsmEnable(argc, argv, cAtFalse);
    }

eBool CmdPdhDe3SsmSend(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet  ret = cAtOk;
    uint32 numChannels, i;
    AtChannel *channelList;
    uint8 value;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Get data */
    value = (uint8)AtStrtoul(argv[1], NULL, 16);
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtPdhChannelTxSsmSet((AtPdhChannel)channelList[i], value);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not set value");
        }

    return cAtTrue;
    }

eBool CmdPdhDe3SsmShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE3 ID", "Enable", "Tx-SSM", "Rx-SSM", "Tx-BitOrder", "Rx-BitOrder"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 value;
        eBool enable;
        uint8 colIdx = 0;
        const char *stringVal;

        StrToCell(tabPtr, de3_i, colIdx++, CliChannelIdStringGet((AtChannel)channelList[de3_i]));

        /* enable */
        enable = AtPdhChannelSsmIsEnabled((AtPdhChannel)channelList[de3_i]);
        StrToCell(tabPtr, de3_i, colIdx++,  CliBoolToString(enable));

        /* message */
        value = AtPdhChannelTxSsmGet((AtPdhChannel)channelList[de3_i]);
        StrToCell(tabPtr, de3_i, colIdx++,  CliNumber2String(value, "0x%x"));

        /* message */
        value = AtPdhChannelRxSsmGet((AtPdhChannel)channelList[de3_i]);
        StrToCell(tabPtr, de3_i, colIdx++,  CliNumber2String(value, "0x%x"));

        /* Bit ordering */
        stringVal = CliBitOrderModeString(AtPdhDe3TxUdlBitOrderGet((AtPdhDe3)channelList[de3_i]));
        StrToCell(tabPtr, de3_i, colIdx++, stringVal ? stringVal : "Error");
        stringVal = CliBitOrderModeString(AtPdhDe3RxUdlBitOrderGet((AtPdhDe3)channelList[de3_i]));
        StrToCell(tabPtr, de3_i, colIdx++, stringVal ? stringVal : "Error");
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtPdhChannelEnable(char argc, char **argv, EnableFunc func, eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = func((AtPdhChannel)channelList[i], enable);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, channelList[i], "Can not enable/disable this function!");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe3DataLinkEnable(char argc, char **argv)
    {
    return CliAtPdhChannelEnable(argc, argv, AtPdhChannelDataLinkEnable, cAtTrue);
    }

eBool CmdPdhDe3DataLinkDisable(char argc, char **argv)
    {
    return CliAtPdhChannelEnable(argc, argv, AtPdhChannelDataLinkEnable, cAtFalse);
    }

eBool CmdPdhDe3TxSlipBufferEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelTxSlipBufferEnable, cAtTrue);
    }

eBool CmdPdhDe3TxSlipBufferDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelTxSlipBufferEnable, cAtFalse);
    }

eBool CmdPdhDe3TxSlipBufferShow(char argc, char **argv)
    {
    /* Declare variables */
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE3 ID", "txEnabled"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (de3_i = 0; de3_i < numChannels; de3_i = AtCliNextIdWithSleep(de3_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eBool enable;
        uint8 column = 0;

        StrToCell(tabPtr, de3_i, column++, CliChannelIdStringGet((AtChannel)channelList[de3_i]));

        /* enable */
        enable = AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)channelList[de3_i]);
        ColorStrToCell(tabPtr, de3_i, column++,  CliBoolToString(enable), enable ? cSevInfo : cSevNormal);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPdhDe3AlarmCaptureShow(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    const char  *pHeading[] = {"DE3 ID", "LOS", "LOF", "AIS", "RAI", "BER-SF", "BER-SD", "BER-TCA", "IDLE", "AIC",
                               "FEAC change", "TIM", "PT change", "TM change", "SSM change",
                               "MDL change", "ClockStateChange"};
    eAtHistoryReadingMode readMode;
    eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channelList[i];
        AtChannelObserver observer = CliAtPdhDe3ObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmLos);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmLof);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmAis);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmRai);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmSfBer);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmSdBer);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmBerTca);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs3AlarmIdle);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs3AlarmAicChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs3AlarmFeacChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhE3AlarmTim);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhE3AlarmPldTypeChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhE3AlarmTmChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDe3AlarmSsmChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPdhDs3AlarmMdlChange);

        /* Print clock states recorded. */
        StrToCell(tabPtr, i, column++, AtChannelObserverClockStateString(observer));
        if (readMode == cAtHistoryReadingModeReadToClear)
            AtchannelObserverClockStateFlush(observer);

        /* Delete observer */
        CliAtPdhDe3ObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDe3Debug(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrintc(cSevInfo, "===========================================\r\n");
        AtPrintc(cSevInfo, "= %s\r\n", CliChannelIdStringGet(channelList[i]));
        AtPrintc(cSevInfo, "===========================================\r\n");
        AtChannelDebug(channelList[i]);
        AtPrintc(cSevInfo, "\r\n");
        }

    return cAtTrue;
    }

eBool CmdPdhDe3AutoRaiEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelAutoRaiEnable, cAtTrue);
    }

eBool CmdPdhDe3AutoRaiDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelAutoRaiEnable, cAtFalse);
    }

eBool CmdPdhDe3TxUdlBitOrderSet(char argc, char **argv)
    {
    return BitOrderSet(argc, argv, AtPdhDe3TxUdlBitOrderSet);
    }

eBool CmdPdhDe3RxUdlBitOrderSet(char argc, char **argv)
    {
    return BitOrderSet(argc, argv, AtPdhDe3RxUdlBitOrderSet);
    }

eBool CmdPdhDe3UdlBitOrderSet(char argc, char **argv)
    {
    return BitOrderSet(argc, argv, TxRxUdlBitOrderSet);
    }

eBool CmdPdhDe3LosDetectionEnable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelLosDetectionEnable, cAtTrue);
    }

eBool CmdPdhDe3LosDetectionDisable(char argc, char **argv)
    {
    return Enable(argc, argv, (EnableFunc)AtPdhChannelLosDetectionEnable, cAtFalse);
    }

eBool CmdPdhDe3DataLinkFcsBitOrderSet(char argc, char **argv)
    {
    eAtBitOrder bitOrder = CliBitOrderFromString(argv[1]);

    if (bitOrder == cAtBitOrderUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid bit order: %s\r\n", CliValidBitOrderModes());
        return cAtFalse;
        }

    return AttributeSet(argc, argv, bitOrder, mAttributeSetFunc(AtPdhChannelDataLinkFcsBitOrderSet));
    }

eBool CmdPdhDe3DataLinkOption(char argc, char **argv)
    {
    uint32 numChannels, de3_i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    eAtPdhDe3DataLinkOption option;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Get option */
    mAtStrToEnum(cAtPdhDe3DataLinkOptionStr, cAtPdhDe3DataLinkOptionVal, argv[1], option, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: invalid Data-Link Option, expected: dl or udl\r\n");
        return cAtFalse;
        }

    for (de3_i = 0; de3_i < numChannels; de3_i++)
        {
        eAtRet ret = AtPdhDe3DataLinkOptionSet((AtPdhDe3)channelList[de3_i], option);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Can not change Data-Link option of %s, ret = %s\r\n",
                     CliChannelIdStringGet(channelList[de3_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe3DataLinkShow(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *pHeading[] = {"DE3 ID", "Enable", "Option", "FCS Bit Order"};

    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        uint32 value;
        uint32 collum = 0;
        eBool enable = AtPdhChannelDataLinkIsEnabled((AtPdhChannel)channelList[channel_i]);
        const char *stringValue;

        StrToCell(tabPtr, channel_i, collum++, CliChannelIdStringGet((AtChannel)channelList[channel_i]));
        StrToCell(tabPtr, channel_i, collum++, CliBoolToString(enable));
        if (enable)
            {
            value  = AtPdhDe3DataLinkOptionGet((AtPdhDe3)channelList[channel_i]);
            stringValue = CliEnumToString(value, cAtPdhDe3DataLinkOptionStr, cAtPdhDe3DataLinkOptionVal, mCount(cAtPdhDe3DataLinkOptionVal), NULL);
            ColorStrToCell(tabPtr, channel_i, collum++, stringValue ? stringValue : "Error", stringValue ? cSevInfo : cSevCritical);

            stringValue = CliBitOrderModeString(AtPdhChannelDataLinkFcsBitOrderGet((AtPdhChannel)channelList[channel_i]));
            ColorStrToCell(tabPtr, channel_i, collum, stringValue ? stringValue : "Error", stringValue ? cSevInfo : cSevCritical);
            }
        else
            {
            ColorStrToCell(tabPtr, channel_i, collum++, sAtNotApplicable, cSevNormal);
            ColorStrToCell(tabPtr, channel_i, collum, sAtNotApplicable, cSevNormal);
            }
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe3InterruptClear(char argc, char **argv)
    {
    uint32 numChannels, defects, channel_i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Defect to clear */
    defects = CliPdhDe3IntrMaskFromString(argv[1]);

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        AtChannelSpecificDefectInterruptClear(channelList[channel_i], defects);

    return cAtTrue;
    }

eBool CmdPdhDe3AlarmAffectingEnable(char argc, char **argv)
    {
    return AlarmAffectingEnable(argc, argv, cAtTrue);
    }

eBool CmdPdhDe3AlarmAffectingDisable(char argc, char **argv)
    {
    return AlarmAffectingEnable(argc, argv, cAtFalse);
    }

eBool CmdPdhDe3StuffModeSet(char argc, char **argv)
    {
    eAtPdhDe3StuffMode  stuffMode = cAtPdhDe3StuffModeFineStuff;
    eBool convertSuccess = cAtFalse;

    mAtStrToEnum(cAtPdhDe3StuffModeStr, cAtPdhDe3StuffModeVal, argv[1], stuffMode, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown stuff mode %s. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cAtPdhDe3StuffModeStr, mCount(cAtPdhDe3StuffModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return AttributeSet(argc, argv, stuffMode, mAttributeSetFunc(AtPdhDe3StuffModeSet));
    }

eBool CmdPdhDe3AlarmAffectGet(char argc, char **argv)
    {
    uint32 numChannels, i, j;
    tTab *tabPtr;
    char buff[16];
    AtChannel *channelList;
    eBool  isEnable;
    const char *pHeading[] = {"DE3 ID", "LOS", "AIS", "LOF", "RAI", "IDLE"};
    const uint32 cCmdPdhDe3AlarmValues[] = {cAtPdhDe3AlarmLos,
                                            cAtPdhDe3AlarmAis,
                                            cAtPdhDe3AlarmLof,
                                            cAtPdhDe3AlarmRai,
                                            cAtPdhDs3AlarmIdle};
    AtUnused(argc);

    /* Get list of path */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliDe3ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        /* Print alarm status */
        for (j = 0; j < mCount(cCmdPdhDe3AlarmValues); j++)
            {
            isEnable = AtPdhChannelAlarmAffectingIsEnabled((AtPdhChannel)channelList[i], cCmdPdhDe3AlarmValues[j]);
            AtSprintf(buff, "%s", (isEnable ? "en" : "dis"));
            ColorStrToCell(tabPtr, i, j+1, buff, isEnable ? cSevNormal : cSevCritical);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtPdhErrorGenerator.c
 *
 * Created Date: Mar 24, 2016
 *
 * Description : PDH Error generator CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "AtPdhChannel.h"
#include "AtDevice.h"
#include "AtCli.h"
#include "CliAtModulePdh.h"
#include "AtCliModule.h"
#include "CliAtPdhErrorGenerator.h"
#include "../diag/CliAtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CliPdhErrorGeneratorErrorTypeSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    eAtRet apiReturn;
    AtChannel channel;
    uint32 errorType;
    AtErrorGenerator errorGenerator;
    AtUnused(argc);

    channel = channelGet(argv[0]);
    if (channel == NULL)
        {
        return cAtFalse;
        }
    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    errorType = CliPdhErrorGeneratorErrorTypeFromString(argv[1]);
    if(errorType == cAtPdhErrorGeneratorErrorTypeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid mode. Expected: %s\r\n", CliValidPdhErrorGeneratorErrorType());
        return cAtFalse;
        }

    apiReturn = AtErrorGeneratorErrorTypeSet(errorGenerator, errorType);
    if (apiReturn != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change error generating type on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(apiReturn));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPdhErrorGeneratorModeSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    eAtRet apiReturn;
    AtChannel channel;
    uint32 errorMode;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);

    channel = channelGet(argv[0]);
    if (channel == NULL)
        return cAtFalse;

    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    errorMode = CliErrorGeneratorModeFromString(argv[1]);
    if(errorMode == cAtErrorGeneratorModeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid mode. Expected: %s\r\n", CliValidErrorGeneratorMode());
        return cAtFalse;
        }

    apiReturn = AtErrorGeneratorModeSet(errorGenerator, errorMode);
    if (apiReturn != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change error generating mode on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(apiReturn));

        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPdhErrorGeneratorErrorNumSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    eAtRet apiReturn;
    AtChannel channel;
    uint32 errorNum;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channel = channelGet(argv[0]);
    if (channel == NULL)
        {
        return cAtFalse;
        }
    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    errorNum = AtStrToDw(argv[1]);

    apiReturn = AtErrorGeneratorErrorNumSet(errorGenerator, errorNum);
    if (apiReturn != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change number of generated errors on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(apiReturn));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPdhErrorGeneratorErrorRateSet(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    eAtRet apiReturn;
    AtChannel channel;
    uint32 errorRate;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channel = channelGet(argv[0]);
    if (channel == NULL)
        {
        return cAtFalse;
        }
    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    errorRate = CliBerRateFromString(argv[1]);
    if(errorRate == cAtBerRateUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid ber rate. Expected: %s\r\n", CliValidBerRates());
        return cAtFalse;
        }

    apiReturn = AtErrorGeneratorErrorRateSet(errorGenerator, errorRate);
    if (apiReturn != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change error generating rate on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(apiReturn));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPdhErrorGeneratorStart(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    eAtRet apiReturn;
    AtChannel channel;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channel = channelGet(argv[0]);
    if (channel == NULL)
        {
        return cAtFalse;
        }
    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    apiReturn = AtErrorGeneratorStart(errorGenerator);
    if (apiReturn != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Start generating error on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(apiReturn));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPdhErrorGeneratorStop(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    eAtRet apiReturn;
    AtChannel channel;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channel = channelGet(argv[0]);
    if (channel == NULL)
        {
        return cAtFalse;
        }
    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    apiReturn = AtErrorGeneratorStop(errorGenerator);
    if (apiReturn != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Stop generating error on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(apiReturn));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPdhErrorGeneratorShow(char argc, char **argv, ChannelFromStringFunction channelGet, tErrorGeneratorGet generatorGet)
    {
    uint32 i, numChannels = 1;
    AtChannel channel;
    uint32 errorMode, errorType, errorNum, errorRate;
    eBool isStarted;
    AtErrorGenerator errorGenerator;
    tTab                *tabPtr;
    const char          *pHeading[] = {"Channel ID", "gen-mode", "error-type", "is-started", "error-num", "error-rate"};

    AtUnused(argc);
    channel = channelGet(argv[0]);
    if (channel == NULL)
        {
        return cAtFalse;
        }
    errorGenerator = generatorGet((AtPdhChannel)channel);
    if (errorGenerator == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No error generator is available\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        char sharedString[64];
        const char *str;

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channel));

        errorMode = AtErrorGeneratorModeGet(errorGenerator);
        str = CliErrorGeneratorModeString(errorMode);
        StrToCell(tabPtr, i, column++, str != NULL ? str : "invalid");

        errorType = AtErrorGeneratorErrorTypeGet(errorGenerator);
        str = CliPdhErrorGeneratorErrorTypeString(errorType);
        StrToCell(tabPtr, i, column++, str != NULL ? str : "invalid");

        isStarted = AtErrorGeneratorIsStarted(errorGenerator);
        StrToCell(tabPtr, i, column++, isStarted ? "en" : "dis");

        errorNum = AtErrorGeneratorErrorNumGet(errorGenerator);
        AtSprintf(sharedString, "%d", errorNum);
        StrToCell(tabPtr, i, column++, sharedString);

        errorRate = AtErrorGeneratorErrorRateGet(errorGenerator);
        str = CliBerRateString(errorRate);
        StrToCell(tabPtr, i, column++, str != NULL ? str : "invalid");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

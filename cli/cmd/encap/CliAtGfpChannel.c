/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliAtGfpChannel.c
 *
 * Created Date: Jun 9, 2014
 *
 * Description : GFP CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "CliAtModuleEncap.h"
#include "../concate/CliAtModuleConcate.h"
#include "AtGfpChannel.h"
#include "AtConcateGroup.h"
#include "../common/CliAtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumGfpsPerTable 10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtGfpFrmModeStr[] = {"gfpf", "gfpt"};
static uint32 cAtGfpFrmModeVal[] = {cAtGfpFrameModeGfpF, cAtGfpFrameModeGfpT};

static const char *cCmdGfpChannelAlarmTypeStr[] = {"lfd", "csf"};
static uint32 cCmdGfpChannelAlarmTypeVal[] = {cAtGfpChannelAlarmLfd, cAtGfpChannelAlarmCsf};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool AlarmShow(char argc, char **argv, uint32 (*AlarmGetFunc)(AtChannel self))
    {
    uint32 numChannels, channel_i;
    AtGfpChannel *channels;
    tTab *tabPtr;
    uint32 alarmStat;
    const char *heading[] = {"ID", "LFD", "CSF"};

    AtUnused(argc);

    /* Get channels */
    channels = (AtGfpChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapGfpChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        uint32 column;
        StrToCell(tabPtr, channel_i, 0, CliNumber2String(AtChannelIdGet((AtChannel)channels[channel_i]) + 1, "%d"));
        alarmStat = AlarmGetFunc((AtChannel) channels[channel_i]);
        for (column = 0; column < mCount(cCmdGfpChannelAlarmTypeVal); column++)
            {
            eBool raise = (alarmStat & cCmdGfpChannelAlarmTypeVal[column]) ? cAtTrue : cAtFalse;
            ColorStrToCell(tabPtr, channel_i, column + 1, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static uint32 CliEncapGfpAlarmMaskFromString(char *alarmStr)
    {
    return CliMaskFromString(alarmStr, cCmdGfpChannelAlarmTypeStr, cCmdGfpChannelAlarmTypeVal, mCount(cCmdGfpChannelAlarmTypeVal));
    }

static const char * CliEncapGfpAlarm2String(uint32 alarms)
    {
    uint16 i;
    static char alarmString[64];

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cCmdGfpChannelAlarmTypeVal); i++)
            {
            if (alarms & (cCmdGfpChannelAlarmTypeVal[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, cCmdGfpChannelAlarmTypeStr[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

static char** BuildHeading(AtChannel *channels, uint32 currentGfpIndex, uint32 numGfpsPerTable)
    {
    AtChannel channel;
    uint32 i;
    char   **headings;

    headings = AtOsalMemAlloc((numGfpsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(8);
    AtSprintf(headings[0], "GFP");

    for (i = 0; i < numGfpsPerTable; i++)
        {
        channel = (AtChannel)channels[currentGfpIndex + i];
        headings[i + 1] = AtOsalMemAlloc(8);
        AtSprintf(headings[i + 1], "%u", AtChannelIdGet(channel) + 1);
        }

    return headings;
    }

static void DestroyHeadings(char **heading, uint32 numColumns)
    {
    uint32 i;

    if (heading == NULL)
        return;

    for (i = 0; i < numColumns; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static AtChannel *CliEncapGfpChannelsFromString(char *gfpIdListString, uint32 *numChannels)
    {
    uint32 bufferSize;
    AtGfpChannel* channels = (AtGfpChannel *)((void**)CliSharedChannelListGet(&bufferSize));
    *numChannels = CliAtModuleEncapGfpChannelsFromString(gfpIdListString, channels, bufferSize);
    return (AtChannel*)channels;
    }

eBool CliAtGfpChannelIntegerAttributeSet(char *channelIdString, uint32 value, AtGfpChannelIntegerAttributeSetFunc attributeSet)
    {
    uint32 numChannels, channel_i;
    AtGfpChannel *channels;
    eBool success = cAtTrue;

    /* Get channels */
    channels = (AtGfpChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapGfpChannelsFromString(channelIdString, channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = attributeSet(channels[channel_i], value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "ERROR: Configure channel %u fail, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)channels[channel_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtGfpChannelTxPtiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxPtiSet);
    }

eBool CmdAtGfpChannelExpectedPtiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelExpectedPtiSet);
    }

eBool CmdAtGfpChannelTxUpiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxUpiSet);
    }

eBool CmdAtGfpChannelExpectedUpiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelExpectedUpiSet);
    }

eBool CmdAtGfpChannelTxExiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxExiSet);
    }

eBool CmdAtGfpChannelExpectedExiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelExpectedExiSet);
    }

eBool CmdAtGfpChannelTxPfiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxPfiSet);
    }

eBool CmdEncapGfpShow(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtGfpChannel *channels;
    tTab *tabPtr;
    const char *heading[] = {"ID",
                             "txPti", "expPti", "txExi",
                             "expExi", "txUpi", "expUpi",
                             "txPfi",
                             "fcsMonitor", "ptiMonitor", "upiMonitor", "exiMonitor",
                             "frameMode", "txFcs", "payloadScramble", "coreHeaderScramble",
                             "txCsf", "txCsfUpi", "rxCsfUpi",
                             "interruptMask"};

    AtUnused(argc);

    /* Get channels */
    channels = (AtGfpChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapGfpChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Make table */
    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        const char *stringValue;
        uint32 intrMaskVal;

        uint16 column = 0;
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtChannelIdGet((AtChannel)channels[channel_i]) + 1, "%d"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelTxPtiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelExpectedPtiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelTxExiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelExpectedExiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelTxUpiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelExpectedUpiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelTxPfiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelFcsMonitorIsEnabled(channels[channel_i])));
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelPtiMonitorIsEnabled(channels[channel_i])));
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelUpiMonitorIsEnabled(channels[channel_i])));
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelExiMonitorIsEnabled(channels[channel_i])));

        /* Frame mode */
        stringValue = CliEnumToString(AtGfpChannelFrameModeGet(channels[channel_i]),
                                      cAtGfpFrmModeStr,
                                      cAtGfpFrmModeVal,
                                      mCount(cAtGfpFrmModeVal),
                                      NULL);
        ColorStrToCell(tabPtr, channel_i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        /* FCS enable */
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelTxFcsIsEnabled(channels[channel_i])));

        /* Scramble */
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelPayloadScrambleIsEnabled(channels[channel_i])));
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelCoreHeaderScrambleIsEnabled(channels[channel_i])));

        /* CSF */
        StrToCell(tabPtr, channel_i, column++, CliBoolToString(AtGfpChannelTxCsfIsEnabled(channels[channel_i])));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelTxCsfUpiGet(channels[channel_i]), "0x%x"));
        StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtGfpChannelRxCsfUpiGet(channels[channel_i]), "0x%x"));

        /* Print interrupt mask values */
        intrMaskVal = AtChannelInterruptMaskGet((AtChannel)channels[channel_i]);
        StrToCell(tabPtr, channel_i, column++, CliEncapGfpAlarm2String(intrMaskVal));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEncapGfpFcsMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelFcsMonitorEnable);
    }

eBool CmdEncapGfpFcsMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelFcsMonitorEnable);
    }

eBool CmdEncapGfpPtiMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelPtiMonitorEnable);
    }

eBool CmdEncapGfpPtiMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelPtiMonitorEnable);
    }

eBool CmdEncapGfpUpiMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelUpiMonitorEnable);
    }

eBool CmdEncapGfpUpiMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelUpiMonitorEnable);
    }

eBool CmdEncapGfpExiMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelExiMonitorEnable);
    }

eBool CmdEncapGfpExiMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelExiMonitorEnable);
    }

eBool CmdEncapGfpFrameModeSet(char argc, char **argv)
    {
    eBool success;
    eAtGfpFrameMode frmMode = CliStringToEnum(argv[1], cAtGfpFrmModeStr, cAtGfpFrmModeVal, mCount(cAtGfpFrmModeVal), &success);

    AtUnused(argc);

    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid frame mode. Expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtGfpFrmModeStr, mCount(cAtGfpFrmModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return CliAtGfpChannelIntegerAttributeSet(argv[0], frmMode, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelFrameModeSet);
    }

eBool CmdEncapGfpTxFcsEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxFcsEnable);
    }

eBool CmdEncapGfpTxFcsDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxFcsEnable);
    }

eBool CmdEncapGfpShowAlarm(char argc, char **argv)
    {
    return AlarmShow(argc, argv, AtChannelDefectGet);
    }

eBool CmdEncapGfpShowInterrupt(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode = CliHistoryReadingModeGet(argv[1]);

    if (readingMode == cAtHistoryReadingModeReadOnly)
        return AlarmShow(argc, argv, AtChannelDefectInterruptGet);
    if (readingMode == cAtHistoryReadingModeReadToClear)
        return AlarmShow(argc, argv, AtChannelDefectInterruptClear);

    AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected: %s\r\n", CliValidHistoryReadingModes());
    return cAtFalse;
    }

eBool CmdEncapGfpCounter(char argc, char **argv)
    {
    AtChannel *channels;
    uint32 numChannels;
    uint32 (*CountersGet)(AtChannel, uint16) = NULL;
    eAtHistoryReadingMode readingMode;
    uint32 numTables, table_i, remainGfps;
    const char *counterNames[] = {
                            "txFrame",
                            "txByte",
                            "txGoodFrm",
                            "txIdle",
                            "txCsf",
                            "txCmf",
                            "rxFrame",
                            "rxByte",
                            "rxGoodFrm",
                            "rxIdleFrm",
                            "rxCsf",
                            "rxCmf",
                            "rxCHecCorrErr",
                            "rxCHecUnCorrErr",
                            "rxTHecCorrErr",
                            "rxTHecUnCorrErr",
                            "rxEHecErr",
                            "rxFcsErr",
                            "rxUpm",
                            "rxExm",
                            "rxDrops"
                            };

    AtUnused(argc);

    /* Get channels */
    channels = CliEncapGfpChannelsFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No GFP, they have not been created yet or"
                              " invalid GFP list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    remainGfps = numChannels;

    /* Calculate number of tables need to show counters of all input Gfps */
    numTables = numChannels / cNumGfpsPerTable;
    if ((numChannels % cNumGfpsPerTable) > 0)
        numTables = numTables + 1;

    /* Reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeReadOnly)
        CountersGet = AtChannelCounterGet;
    else
        CountersGet = AtChannelCounterClear;

    for (table_i = 0; table_i < numTables; table_i = AtCliNextIdWithSleep(table_i, 1))
        {
        uint32 channel_i;
        tTab *tabPtr = NULL;
        char **heading = NULL;
        uint32 numGfpsPerTable;
        uint32 currentIndex = table_i * cNumGfpsPerTable;
        uint8 counter_i = 0;

        numGfpsPerTable = remainGfps;
        if (numGfpsPerTable > cNumGfpsPerTable)
            numGfpsPerTable = cNumGfpsPerTable;

        heading = BuildHeading(channels, currentIndex, numGfpsPerTable);
        tabPtr = TableAlloc(mCount(counterNames), numGfpsPerTable + 1, (void *)heading);
        if (!tabPtr)
            {
            DestroyHeadings(heading, numGfpsPerTable + 1);
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }

        /* Build column of counter names */
        for (counter_i = 0; counter_i < mCount(counterNames); counter_i++)
            StrToCell(tabPtr, counter_i, 0, counterNames[counter_i]);

        /* Make table content */
        for (channel_i = 0; channel_i < numGfpsPerTable; channel_i++)
            {
            AtChannel channel = channels[currentIndex + channel_i];
            uint32 column = channel_i + 1U;
            counter_i = 0;

            /* Print counter */
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeTxFrames, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeTxBytes, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeTxGoodFrames, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeTxIdleFrames, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeTxCsf, cAtCounterTypeNeutral, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeTxCmf, cAtCounterTypeNeutral, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxFrames, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxBytes, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxGoodFrames, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxIdleFrames, cAtCounterTypeGood, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxCsf, cAtCounterTypeNeutral, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxCmf, cAtCounterTypeNeutral, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxCHecCorrected, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxCHecError, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxTHecCorrected, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxTHecError, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxEHecError, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxFcsError, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxUpm, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxExm, cAtCounterTypeError, tabPtr, counter_i++, column);
            CliChannelCounterPrintToCell(channel, CountersGet, cAtGfpChannelCounterTypeRxDrops, cAtCounterTypeError, tabPtr, counter_i++, column);
            }

        /* Show it */
        TablePrint(tabPtr);
        TableFree(tabPtr);
        DestroyHeadings(heading, numGfpsPerTable + 1);

        /* For next GFPs */
        remainGfps = remainGfps - numGfpsPerTable;
        }

    return cAtTrue;
    }

eBool CmdEncapGfpPayloadScrambleEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelPayloadScrambleEnable);
    }

eBool CmdEncapGfpPayloadScrambleDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelPayloadScrambleEnable);
    }

eBool CmdEncapGfpCoreHeaderScrambleEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelCoreHeaderScrambleEnable);
    }

eBool CmdEncapGfpCoreHeaderScrambleDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelCoreHeaderScrambleEnable);
    }

eBool CmdAtGfpChannelDebug(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtGfpChannel *channels;

    /* Get channels */
    AtUnused(argc);
    channels = (AtGfpChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapGfpChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        AtChannelDebug((AtChannel)channels[channel_i]);

    return cAtTrue;
    }

eBool CmdEncapGfpTxCsfEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtTrue, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxCsfEnable);
    }

eBool CmdEncapGfpTxCsfDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], cAtFalse, (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxCsfEnable);
    }

eBool CmdEncapGfpTxCsfUpiSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliAtGfpChannelIntegerAttributeSet(argv[0], AtStrToDw(argv[1]), (AtGfpChannelIntegerAttributeSetFunc)AtGfpChannelTxCsfUpiSet);
    }

eBool CmdEncapGfpInterruptMask(char argc, char **argv)
    {
    AtChannel *channels;
    uint32 numChannels, i;
    uint32 intrMaskVal;
    eBool convertSuccess;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of path */
    channels = CliEncapGfpChannelsFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No GFP, they have not been created yet or invalid GFP list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = CliEncapGfpAlarmMaskFromString(argv[1])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel channel;

        channel = (AtChannel)channels[i];
        ret = AtChannelInterruptMaskSet(channel, intrMaskVal, enable ? intrMaskVal : 0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set interrupt mask for channel %s, ret = %s\r\n",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cCmdGfpChannelAlarmTypeStr); alarm_i++)
        {
        uint32 alarmType = cCmdGfpChannelAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        logPtr += AtSprintf(logPtr, " * [%s:", cCmdGfpChannelAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            logPtr += AtSprintf(logPtr, "set");
        else
            logPtr += AtSprintf(logPtr, "clear");
        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [GFP] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cCmdGfpChannelAlarmTypeStr); alarm_i++)
        {
        uint32 alarmType = cCmdGfpChannelAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cCmdGfpChannelAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelObserver observer = userData;
    AtUnused(channel);
    AtChannelObserverDefectsUpdate(observer, changedAlarms, currentStatus);
    }

eBool CmdEncapGfpAlarmCapture(char argc, char **argv)
    {
    AtChannel *channels;
    uint32 numChannels, i;
    static tAtChannelEventListener listener;
    eBool convertSuccess;
    eBool captureEnabled = cAtFalse;
    eBool success = cAtTrue;

    AtUnused(argc);
    if (argc < 2)
        {
        AtPrintc(cSevNormal, "Error: Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Get list of path */
    channels = CliEncapGfpChannelsFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No GFP, they have not been created yet or invalid GFP list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        listener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        listener.AlarmChangeState = AlarmChangeLogging;
    else
        listener.AlarmChangeState = AlarmChangeState;

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel channel;

        channel = (AtChannel)channels[i];

        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(channel, &listener, CliAtGfpChannelObserverGet(channel));
            else
                ret = AtChannelEventListenerAdd(channel, &listener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(channel, &listener);

            if (AutotestIsEnabled())
                CliAtGfpChannelObserverDelete(channel);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEncapGfpAlarmCaptureGet(char argc, char **argv)
    {
    const char *pHeading[] = {"ID", "LFD", "CSF"};
    uint32 numChannels, i;
    eAtHistoryReadingMode readMode;
    AtChannel *channels;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevNormal, "Error: Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Get list of path */
    channels = CliEncapGfpChannelsFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No GFP, they have not been created yet or invalid GFP list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[i];
        AtChannelObserver observer = CliAtGfpChannelObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtGfpChannelAlarmLfd);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtGfpChannelAlarmCsf);

        /* Delete observer */
        CliAtGfpChannelObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

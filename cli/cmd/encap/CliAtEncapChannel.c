/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : CliAtEncapChannel.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : Encap channel CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cCmdEncapTypeCreateStr[] =
    {
    "cisco", "ppp", "framerelay", "atm", "gfp"
    };
static const eAtEncapTypeCreate cCmdEncapTypeCreateVal[] =
    {
    cAtHdlcFrmCiscoHdlcCreate,
    cAtHdlcFrmPppCreate,
    cAtHdlcFrmFrCreate,
    cAtEncapAtmCreate,
    cAtEncapGfpCreate
    };

static const char *cCmdEncapTypeStr[] =
    {
    "hdlc", "atm", "gfp"
    };

static uint32 cCmdEncapTypeVal[] =
    {
    cAtEncapHdlc, cAtEncapAtm, cAtEncapGfp
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Enable(char argc, char **argv, eBool enable)
    {
    eBool success = cAtTrue;
    uint32 numChannels, channel_i;
    AtEncapChannel *channels;

    AtUnused(argc);

    /* Get channels */
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channel, the ID may be wrong or channels have not been created\r\n");
        return cAtTrue;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtChannelEnable((AtChannel)channels[channel_i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: %s encapsulation channel %s fail with ret = %s\r\n",
                     enable ? "Enable" : "Disable",
                     CliChannelIdStringGet((AtChannel) channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEncapChannelCreate(char argc, char **argv)
    {
    eAtEncapTypeCreate encType = cAtHdlcFrmPppCreate;
    uint32 *idBuffer, bufferSize, numberOfIds, i;
    eBool blResult, cliSuccess = cAtTrue;
    AtEncapChannel encapChannel;
    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numberOfIds = CliIdListFromString(argv[0], idBuffer, bufferSize);
    if (numberOfIds == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtFalse;
        }

    mAtStrToEnum(cCmdEncapTypeCreateStr,
                 cCmdEncapTypeCreateVal,
                 argv[1],
                 encType,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <encapType>, expected cisco|ppp|framerelay|atm|laps\r\n");
        return cAtFalse;
        }

    /* SET */
    for (i = 0; i < numberOfIds; i++)
        {
        uint16 channelId = (uint16)(idBuffer[i] - 1);
        encapChannel = NULL;
        switch (encType)
            {
            case cAtHdlcFrmPppCreate:
                encapChannel = (AtEncapChannel)AtModuleEncapHdlcPppChannelCreate(CliAtEncapModule(), channelId);
                break;

            case cAtHdlcFrmFrCreate:
                encapChannel = (AtEncapChannel)AtModuleEncapFrChannelCreate(CliAtEncapModule(), channelId);
                break;

            case cAtHdlcFrmCiscoHdlcCreate:
                encapChannel = (AtEncapChannel)AtModuleEncapCiscoHdlcChannelCreate(CliAtEncapModule(), channelId);
                break;

            case cAtEncapAtmCreate:
                encapChannel = (AtEncapChannel)AtModuleEncapAtmTcCreate(CliAtEncapModule(), channelId);
                break;

            case cAtEncapGfpCreate:
                encapChannel = (AtEncapChannel)AtModuleEncapGfpChannelCreate(CliAtEncapModule(), channelId);
                break;

            default:
                AtPrintc(cSevCritical, "ERROR: Encapsulation type is not support. Expected cisco|ppp|framerelay|atm|gfp|laps\r\n");
                return cAtFalse;
            }

        /* Check if channel is created */
        if (encapChannel == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: The channel %u exist or invalid id\r\n", i+1);
            cliSuccess = cAtFalse;
            }
        }

    return cliSuccess;
    }

eBool CmdEncapChannelEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdEncapChannelDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

/* Delete encapsulation channel*/
eBool CmdEncapChannelDelete(char argc, char **argv)
    {
    uint32 *idBuffer, bufferSize, numberOfIds, i;
    eAtRet ret;
    AtUnused(argc);
    ret = cAtOk;

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numberOfIds = CliIdListFromString(argv[0], idBuffer, bufferSize);
    if (numberOfIds == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtFalse;
        }

    for (i = 0; i < numberOfIds; i++)
        {
        ret = AtModuleEncapChannelDelete(CliAtEncapModule(), (uint16)(idBuffer[i] - 1));
        if (ret != cAtOk)
            AtPrintc(cSevWarning,
                     "Input encap channel ID #%u is invalid, ret code = %s\r\n",
                     idBuffer[i],
                     AtRet2String(ret));
        }

    return cAtTrue;
    }

/* Bind physical*/
eBool CmdEncapBindPhy(char argc, char **argv)
    {
    uint32 numEncapChannels, numPhysicals, channel_i;
    AtEncapChannel *encapChannelList;
    AtChannel *physicalList;
    eBool success;

    AtUnused(argc);
    /* Get the shared buffer and divide it to two parts to store
     * encap channels and physical channels */
    encapChannelList = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numEncapChannels));
    physicalList = (AtChannel *)((void**)CliSharedObjectListGet(&numPhysicals));

    /* Get ID of encap channels */
    numEncapChannels = CliAtEncapChannelsFromString(argv[0], encapChannelList, numEncapChannels);
    if (numEncapChannels == 0)
        return cAtFalse;

    /* Get shared channel buffer and parse ID of binded channel */
    if (!AtStrcmp(argv[1], "none"))
        {
        numPhysicals = numEncapChannels;
        physicalList = NULL;
        }
    else
        numPhysicals = CliChannelListFromString(argv[1], physicalList, numPhysicals);

    /* Check to ensure encap channels are binded 1 to 1 */
    if (numEncapChannels != numPhysicals)
        {
        AtPrintc(cSevCritical, "ERROR: The number of Encap Channels and Physicals Binding are different\r\n");
        return cAtFalse;
        }

    success = cAtTrue;
    for (channel_i = 0; channel_i < numEncapChannels; channel_i++)
        {
        AtChannel physical = (physicalList == NULL) ? NULL : physicalList[channel_i];
        eAtRet ret = AtEncapChannelPhyBind(encapChannelList[channel_i], physical);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            AtPrintc(cSevWarning, "Can't bind encap channel %u to physical %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)encapChannelList[channel_i]) + 1,
                     AtChannelIdGet(physical) + 1,
                     AtRet2String(ret));
            }
        }

    return success;
    }

/* SET scramble */
eBool CmdEncapChannelScramble(char argc, char **argv)
    {
    eBool enable;
    eBool success;
    uint32 numChannels, channel_i;
    AtEncapChannel *channels;
    AtUnused(argc);

    /* Get channels */
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    mAtStrToBool(argv[1], enable, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <enable> , expected en|dis\r\n");
        return cAtFalse;
        }

    success = cAtTrue;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtEncapChannelScrambleEnable(channels[channel_i],  enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Encapsulation channel %u cannot be enabled/disabled, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)channels[channel_i]) + 1,
                     AtRet2String(ret));

            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEncapChannelDebug(char argc, char **argv)
    {
    AtEncapChannel *channels;
    uint32 channel_i;
    uint32 numChannels;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get channels */
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
         return cAtFalse;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        const char *encapTypeString = NULL;
        AtEncapChannel encap = channels[channel_i];
        eBool convertSuccess = cAtTrue;
        uint32 encapType = AtEncapChannelEncapTypeGet(encap);

        encapTypeString = CliEnumToString(encapType,
                cCmdEncapTypeStr,
                cCmdEncapTypeVal,
                mCount(cCmdEncapTypeVal),
                NULL);

        if (encapTypeString)
            AtPrintc(cSevInfo, "Encap Type: %s \r\n", encapTypeString);

        if (!convertSuccess)
            success = cAtFalse;

        AtChannelDebug((AtChannel)encap);
        }

    return success;
    }

eBool CmdEncapChannelShow(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtEncapChannel *channels;
    tTab *tabPtr;
    const char *heading[] ={"ID", "BoundPhysical", "EncapType", "Scramble", "EthFlow"};

    AtUnused(argc);
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    tabPtr = TableAlloc(numChannels, 5, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        const char *string;
        AtChannel physicalChannel;
        eAtEncapType encapType;
        uint8 colum = 0;
        AtEthFlow ethFlow;

        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)channels[channel_i]) + 1, "%u"));

        /* Print Physical Binding*/
        physicalChannel = AtEncapChannelBoundPhyGet(channels[channel_i]);
        StrToCell(tabPtr, channel_i, colum++, (char *)CliChannelIdStringGet((AtChannel)physicalChannel));

        /* Print Encapsulation type*/
        encapType = AtEncapChannelEncapTypeGet(channels[channel_i]);
        string = CliEnumToString(encapType, cCmdEncapTypeStr, cCmdEncapTypeVal, mCount(cCmdEncapTypeVal), NULL);
        StrToCell(tabPtr, channel_i, colum++, string ? string : "Error");

        /* Print en/dis scramble */
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelScrambleIsEnabled((AtHdlcChannel)channels[channel_i])));

        /* Print eth flow */
        ethFlow = AtEncapChannelBoundFlowGet(channels[channel_i]);
        StrToCell(tabPtr, channel_i, colum++, (char *)CliChannelIdStringGet((AtChannel)ethFlow));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtEncapFlowBind(char argc, char **argv)
    {
    uint32 numChannels;
    AtEncapChannel *channels;
    uint32 bufferSize, channel_i;
    uint32 *flowIdBuffer = NULL;
    uint32 numFlows;
    eBool isBind;
    eBool success;

    AtUnused(argc);

    /* Get channels */
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
         return cAtFalse;

    /* Flow is different from NULL, bind HDLC link. Unbind if flow is NULL */
    numFlows = 0;
    flowIdBuffer =  CliSharedIdBufferGet(&bufferSize);
    if (AtStrcmp(argv[1], "none"))
        {
        /* And use remain ID buffer for flow IDs */
        numFlows = CliIdListFromString(argv[1], flowIdBuffer, bufferSize);
        if (numChannels != numFlows)
            {
            AtPrintc(cSevCritical, "ERROR: The number of GFP channel and Ethernet flow are different\r\n");
            return cAtFalse;
            }
        }

    /*
     * Flow id buffer is NULL, unbind, otherwise, bind
     * */
    isBind = (numFlows != 0) ? cAtTrue : cAtFalse;

    /* Handle each object by ID */
    success = cAtTrue;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret;
        AtEthFlow flow = isBind ? (AtEthFlow)AtModuleEthFlowGet(CliAtEthModule(), (uint16)(flowIdBuffer[channel_i] - 1)) : NULL;
        AtEncapChannel encap = channels[channel_i];
        if (isBind && (flow == NULL))
            {
            AtPrintc(cSevCritical, "ERROR: Flow %u does not exist, cannot bind it to link %u\r\n", flowIdBuffer[channel_i], AtChannelIdGet((AtChannel ) encap) + 1);
            success = cAtFalse;
            continue;
            }

        ret = AtEncapChannelFlowBind(encap, flow);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            if (isBind)
                AtPrintc(cSevWarning, "Cannot bind flow %u to link %u, Return code = %s\n", flowIdBuffer[channel_i], AtChannelIdGet((AtChannel ) encap) + 1, AtRet2String(ret));
            else
                AtPrintc(cSevWarning, "Cannot unbind flow from link %u, Return code = %s\n", AtChannelIdGet((AtChannel ) encap) + 1, AtRet2String(ret));
            }
        }

    return success;
    }

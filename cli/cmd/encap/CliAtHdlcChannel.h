/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : CliAtHdlcChannel.h
 * 
 * Created Date: Jul 21, 2017
 *
 * Description : HDLC channel common CLI functions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATHDLCCHANNEL_H_
#define _CLIATHDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtHdlcChannelFlagSet(AtHdlcChannel *channels, uint32 numChannels, char *flagString, char *numFlagString);
eBool CliAtHdlcChannelMtuSet(AtHdlcChannel *channels, uint32 numChannels, char *mtuString);
eBool CliAtHdlcChannelFcsModeSet(AtHdlcChannel *channels, uint32 numChannels, char *fcsModeString);
const char *CliAtHdlcStuffMode2String(eAtHdlcStuffMode stuffMode);
eBool CliAtHdlcChannelStuffModeSet(AtHdlcChannel *channels, uint32 numChannels, char *stuffString);
eBool CliAtHdlcChannelFcsCalculationMode(AtHdlcChannel *channels, uint32 numChannels, const char *modeString);
const char *CliAtHdlcFcsCalculationMode2Str(eAtHdlcFcsCalculationMode mode);
uint32 CliHdlcFcsModeFromString(const char *fcsModeStr);
const char *CliHdlcFcsModeToString(uint32 fcsMode);
eBool CliHdlcChannelCounterShow(AtHdlcChannel *channels, uint32 numChannels, char argc, char **argv);
const char *CliHdlcChannelAlarm2String(uint32 alarms);
eBool CliAtHdlcChannelIntrMaskSet(char *intrMaskString, char *enableString, AtHdlcChannel *channels, uint32 numChannels);
eBool CliAtHdlcChannelInterruptShow(char argc, char **argv, AtHdlcChannel *channels, uint32 numChannels);
eBool CliAtHdlcChannelAlarmShow(AtHdlcChannel *channels, uint32 numChannels);
void CliAtHdlcChannelAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATHDLCCHANNEL_H_ */


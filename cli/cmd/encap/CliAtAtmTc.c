/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : CliAtAtmTc.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : ATM TC CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAtmTc.h"
#include "AtCli.h"
#include "CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cCmdAtmNetworkInterfaceTypeStr[] =
    {
    "uni", "nni"
    };
static const uint32 cCmdAtmNetworkInterfaceTypeVal[] =
    {
    cAtAtmNetworkInterfaceTypeUni, cAtAtmNetworkInterfaceTypeNni
    };

static const char *cCmdAtmCellMappingModeStr[] =
    {
    "directly", "plcp"
    };
static const uint32 cCmdAtmCellMappingModeVal[] =
    {
    cAtAtmCellMappingModeDirectly, cAtAtmCellMappingModePlcp
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdEncapAtmTcNetwokIntf(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtAtmTc *channels;
    eAtAtmNetworkInterfaceType infType = cAtAtmNetworkInterfaceTypeInvalid;
    eBool blResult;

    AtUnused(argc);

    /* Get channels */
    channels = (AtAtmTc *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapAtmChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtTrue;
        }

    mAtStrToEnum(cCmdAtmNetworkInterfaceTypeStr,
                 cCmdAtmNetworkInterfaceTypeVal,
                 argv[1],
                 infType,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <networkinf> , expected uni|nni");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtAtmTcNetworkInterfaceTypeSet(channels[channel_i], infType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "channel %u cannot be change network interface type, ret = %s\r\n",
                     AtChannelIdGet((AtChannel) channels[channel_i]) + 1,
                     AtRet2String(ret));

            blResult = cAtFalse;
            }
        }

    return blResult;
    }

/* Configure ATM TC*/
eBool CmdEncapAtmTcCellMapMode(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtAtmTc *channels;
    eAtAtmCellMappingMode mapMode = cAtAtmCellMappingModeInvalid;
    eBool blResult;

    AtUnused(argc);

    /* Get channels */
    channels = (AtAtmTc *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapAtmChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtTrue;
        }

    mAtStrToEnum(cCmdAtmCellMappingModeStr,
                 cCmdAtmCellMappingModeVal,
                 argv[1],
                 mapMode,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <cellMapMode> , expected directly|plcp\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtAtmTcCellMappingModeSet(channels[channel_i], mapMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not set mapping mode of ATM TC %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel) channels[channel_i]) + 1,
                     AtRet2String(ret));

            blResult = cAtFalse;
            }
        }

    return blResult;
    }

/*Show ATM TC configuration*/
eBool CmdEncapAtmTc(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtAtmTc *channels;
    tTab *tabPtr;
    const char *heading[] =
        {
        "ID", "InterfaceType", "CellMapMode"
        };
    AtUnused(argc);

    /* Get channels */
    channels = (AtAtmTc *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapAtmChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, 3, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        const char* string;

        StrToCell(tabPtr, channel_i, 0, CliNumber2String(AtChannelIdGet((AtChannel )channels[channel_i]) + 1,  "%u"));
        string = CliEnumToString(AtAtmTcNetworkInterfaceTypeGet(channels[channel_i]),
                     cCmdAtmNetworkInterfaceTypeStr,
                     cCmdAtmNetworkInterfaceTypeVal,
                     mCount(cCmdAtmNetworkInterfaceTypeVal),
                     NULL);
        StrToCell(tabPtr, channel_i, 1, string ? string : "Error");

        string = CliEnumToString(AtAtmTcCellMappingModeGet(channels[channel_i]),
                     cCmdAtmCellMappingModeStr,
                     cCmdAtmCellMappingModeVal,
                     mCount(cCmdAtmCellMappingModeVal),
                     NULL);
        StrToCell(tabPtr, channel_i, 2, string ? string : "Error");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*Show ATM TC counters*/
eBool CmdEncapAtmTcCounter(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtAtmTc *channels;
    tAtAtmTcCounters atmCounters;
    eBool sucess = cAtTrue;
    tTab *tabPtr;
    const char *heading[] = {
                                "ID",
                                "rxGoodCell",
                                "rxIdleCell",
                                "rxHecErrCell",
                                "rxHecErrCorrectedCell",
                                "rxFebe",
                                "rxB1",
                                "txGoodCell",
                                "txIdleCell"
                                };
    AtUnused(argc);

    /* Get channels */
    channels = (AtAtmTc *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapAtmChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtTrue;
        }

    /* Handle each object by ID */
    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, 9, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret;
        eBool isValidCounter;
        uint32 colum = 0;
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel )channels[channel_i]) + 1,  "%u"));
        if (CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadToClear)
            ret = AtChannelAllCountersClear((AtChannel)channels[channel_i], (void*) &atmCounters);
        else
            ret = AtChannelAllCountersGet((AtChannel)channels[channel_i], (void*) &atmCounters);

        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "ATM TC channel %s cannot get counters, ret = %s\n",
                     AtChannelIdString((AtChannel) channels[channel_i]),
                     AtRet2String(ret));
            sucess = cAtFalse;
            }

        /* Print to cell */
        isValidCounter = (ret == cAtOk) ? cAtTrue : cAtFalse;
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.rxGoodCell, cAtCounterTypeGood, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.rxIdleCell, cAtCounterTypeNeutral, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.rxHecErrCell, cAtCounterTypeError, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.rxHecErrCorrectedCell, cAtCounterTypeError, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.rxFebe, cAtCounterTypeError, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.rxB1, cAtCounterTypeError, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.txGoodCell, cAtCounterTypeGood, isValidCounter);
        CliCounterPrintToCell(tabPtr, channel_i, colum++, atmCounters.txIdleCell, cAtCounterTypeGood, isValidCounter);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    if (sucess == cAtFalse)
        AtPrintc(cSevWarning,
                "WARNING: Cell with Error value reason because the channel does not exist or invalid id\r\n");

    return sucess;
    }

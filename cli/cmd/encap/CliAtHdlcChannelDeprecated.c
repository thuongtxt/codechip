/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliAtHdlcChannelDeprecated.c
 *
 * Created Date: Oct 13, 2017
 *
 * Description : Cli encapsulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#include "AtEncapChannel.h"
#include "AtCli.h"
#include "CliAtModuleEncap.h"
#include "AtHdlcChannelDeprecated.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeSetFunc)(AtHdlcChannel self, eBool bypass);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool BoolAttributeSet(char argc, char **argv, AttributeSetFunc setFunc)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    eBool boolValue;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channel, the ID may be wrong or channels have not been created\r\n");
        return cAtTrue;
        }

    /* Enabling */
    mAtStrToBool(argv[1], boolValue, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    /* Apply */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = setFunc(channels[channel_i], boolValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel) channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelAddressControlErrorBypass(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, (AttributeSetFunc)AtHdlcChannelAddressControlErrorBypass);
    }

eBool CmdAtHdlcChannelFcsErrorBypass(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, (AttributeSetFunc)AtHdlcChannelFcsErrorBypass);
    }

eBool CmdAtHdlcChannelAddressControlInsert(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    uint8 address, control;
    eBool enabled = cAtFalse;
    eBool success = cAtFalse;

    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Enabling */
    mAtStrToBool(argv[1], enabled, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    /* Address/control */
    address = (uint8)AtStrtoul(argv[2], NULL, 16);
    control = (uint8)AtStrtoul(argv[3], NULL, 16);

    /* Apply */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret;
        ret  = AtHdlcChannelTxAddressSet(channels[channel_i], &address);
        ret |= AtHdlcChannelTxControlSet(channels[channel_i], &control);
        ret |= AtHdlcChannelAddressInsertionEnable(channels[channel_i], enabled);
        ret |= AtHdlcChannelControlInsertionEnable(channels[channel_i], enabled);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning, "Cannot configure channel %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel) channels[channel_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelAddressControlCompare(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    uint8 expectedAddress, expectedControl;
    eBool enabled = cAtFalse;
    eBool success = cAtFalse;


    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Enabling */
    mAtStrToBool(argv[1], enabled, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    /* Address/control */
    expectedAddress = (uint8)AtStrtoul(argv[2], NULL, 16);
    expectedControl = (uint8)AtStrtoul(argv[3], NULL, 16);

    /* Apply */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret;
        ret  = AtHdlcChannelExpectedAddressSet(channels[channel_i], &expectedAddress);
        ret |= AtHdlcChannelExpectedControlSet(channels[channel_i], &expectedControl);
        ret |= AtHdlcChannelAddressMonitorEnable(channels[channel_i], enabled);
        ret |= AtHdlcChannelControlMonitorEnable(channels[channel_i], enabled);

        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Cannot configure channel %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel) channels[channel_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

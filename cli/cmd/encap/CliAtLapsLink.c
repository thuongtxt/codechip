/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : CliAtLapsLink.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : LAPS link CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleEncap.h"
#include "AtLapsLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*LapsAttributeSetFunc)(AtLapsLink self, uint16 value);
typedef eAtRet (*LapsBoolAttributeSetFunc)(AtLapsLink self, eBool boolValue);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool LapsAttributeSet(char argc, char **argv, LapsAttributeSetFunc setFunc)
    {
    uint32 numChannels, i;
    AtLapsLink *links;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get channels */
    links = (AtLapsLink *)CliHdlcLinkListGet(argv[0], &numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Apply */
    for (i = 0; i < numChannels; i++)
        {
        eAtRet ret = setFunc(links[i], (uint16)AtStrToDw(argv[1]));
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)links[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }


static eBool LapsBoolAttributeSet(char argc, char **argv, LapsBoolAttributeSetFunc setFunc, eBool boolVal)
    {
    uint32 numChannels, i;
    AtLapsLink *links;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get channels */
    links = (AtLapsLink *)CliHdlcLinkListGet(argv[0], &numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Apply */
    for (i = 0; i < numChannels; i++)
        {
        eAtRet ret = setFunc(links[i], boolVal);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)links[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtLapsLinkTxSapiSet(char argc, char **argv)
    {
    return LapsAttributeSet(argc, argv, AtLapsLinkTxSapiSet);
    }

eBool CmdAtLapsLinkExpectedSapiSet(char argc, char **argv)
    {
    return LapsAttributeSet(argc, argv, AtLapsLinkExpectedSapiSet);
    }

eBool CmdAtLapsLinkSapiMonitorEnable(char argc, char **argv)
    {
    return LapsBoolAttributeSet(argc, argv, AtLapsLinkSapiMonitorEnable, cAtTrue);
    }

eBool CmdAtLapsLinkSapiMonitorDisable(char argc, char **argv)
    {
    return LapsBoolAttributeSet(argc, argv, AtLapsLinkSapiMonitorEnable, cAtFalse);
    }

eBool CmdAtLapsLinkShow(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtLapsLink *links;
    eBool success = cAtTrue;
    tTab *tabPtr;
    const char *heading[] = {"LAPS", "Tx-SAPI", "Exp-SAPI", "MonitorEnable"};

    /* Get channels */
    AtUnused(argc);
    links = (AtLapsLink *)CliHdlcLinkListGet(argv[0], &numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Create table */
    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numChannels; i++)
        {
        uint8 colum = 0;
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)links[i]) + 1, "%u"));
        StrToCell(tabPtr, i, colum++, CliNumber2String((uint32)AtLapsLinkTxSapiGet(links[i]), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliNumber2String((uint32)AtLapsLinkExpectedSapiGet(links[i]), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliBoolToString(AtLapsLinkSapiMonitorIsEnabled(links[i])));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CmdAtLapsLinkCountersShow(char argc, char **argv)
    {
    uint32 (*CountersGet)(AtChannel, uint16) = NULL;
    uint32 numChannels, i;
    AtLapsLink *links;
    tTab *tabPtr;
    eAtHistoryReadingMode readingMode;
    eBool success = cAtTrue;
    const char *heading[] = {"LAPS", " SAPI-MisMatch-Counters"};

    /* Get channels */
    AtUnused(argc);
    links = (AtLapsLink *)CliHdlcLinkListGet(argv[0], &numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeReadOnly)
        CountersGet = AtChannelCounterGet;
    else
        CountersGet = AtChannelCounterClear;

    /* Create table */
    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numChannels; i++)
        {
        uint8 colum = 0;
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)links[i]) + 1, "%u"));
        CliChannelCounterPrintToCell((AtChannel) links[i],
        		                     CountersGet,
                                     cAtLapsLinkCounterTypeRxSapiError,
                                     cAtCounterTypeError,
                                     tabPtr,
                                     i,
                                     colum++);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

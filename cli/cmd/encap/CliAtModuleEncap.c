/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : atcliencap.c
 *
 * Created Date: Oct 5, 2012
 *
 * Description : Cli encapsulation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdEncapShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"MaxChannel", "NextFreeChannel", "IdlePattern"};
	AtUnused(argv);
	AtUnused(argc);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    StrToCell(tabPtr, 0, 0, CliNumber2String(AtModuleEncapMaxChannelsGet(CliAtEncapModule()), "%u"));
    StrToCell(tabPtr, 0, 1, CliNumber2String(AtModuleEncapFreeChannelGet(CliAtEncapModule()), "%u"));
    StrToCell(tabPtr, 0, 2, CliNumber2String(AtModuleEncapIdlePatternGet(CliAtEncapModule()), "0x%hx"));

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtModuleEncapIdlePatternSet(char argc, char** argv)
    {
    uint8 pattern = (uint8)AtStrToDw(argv[0]);
	AtUnused(argc);

    if(AtModuleEncapIdlePatternSet(CliAtEncapModule(), pattern) != cAtOk)
        return cAtFalse;

    return cAtTrue;
    }

eBool CmdAtModuleEncapDebugLoopback(char argc, char **argv)
    {
    eAtLoopbackMode loopMode = CliLoopbackModeFromString(argv[0]);
    eAtRet ret = AtModuleDebugLoopbackSet((AtModule)CliAtEncapModule(), (uint8)loopMode);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevWarning, "Can not set loopback mode for encap module, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModuleEncapDebug(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebug((AtModule)CliAtEncapModule());
    return cAtTrue;
    }

AtModuleEncap CliAtEncapModule(void)
    {
    return (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliAtEncapModule(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModuleEncapInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleEncapInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleEncapShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleEncap moduleEncap = CliAtEncapModule();
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)moduleEncap);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

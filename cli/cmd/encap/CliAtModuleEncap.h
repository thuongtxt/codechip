/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtModuleEncapcli.h
 *
 * Created Date: Oct 13, 2012
 *
 * Description : ENCAP CLI helper functions
 *
 * Notes       : TODO Notes
 *----------------------------------------------------------------------------*/

#ifndef ATMODULEENCAPCLI_H_
#define ATMODULEENCAPCLI_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "atclib.h"
#include "AtHdlcLink.h"
#include "AtHdlcChannel.h"
#include "AtModuleEncap.h"
#include "AtCli.h"
#include "AtDevice.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief Use for create Channel encapsulation by specified type
 */
typedef enum eAtEncapTypeCreate
    {
    cAtHdlcFrmCiscoHdlcCreate,/**< Cisco HDLC */
    cAtHdlcFrmPppCreate,      /**< PPP */
    cAtHdlcFrmFrCreate,       /**< Frame relay */
    cAtEncapAtmCreate,        /**< ATM */
    cAtEncapGfpCreate        /**< GFP */
    } eAtEncapTypeCreate;

typedef eAtModuleEncapRet (*AtGfpChannelIntegerAttributeSetFunc)(AtGfpChannel self, uint32 value);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
AtEthFlow *CliHdlcLinkOamFlowListGet(char *pStrEncapChannelIdList, uint32 *numFlows);
AtHdlcLink *CliHdlcLinkListGet(char *pStrHdlcIdList, uint32 *numLinks);

eBool CliAtGfpChannelIntegerAttributeSet(char *channelIdString, uint32 value, AtGfpChannelIntegerAttributeSetFunc attributeSet);
uint32 CliAtEncapChannelsFromString(char *idListString, AtEncapChannel *channels, uint32 channelBufferSize);
uint32 CliAtModuleEncapHdlcChannelsFromString(char *idListString, AtHdlcChannel *channels, uint32 channelBufferSize);
uint32 CliAtModuleEncapGfpChannelsFromString(char *idListString, AtGfpChannel *channels, uint32 channelBufferSize);
uint32 CliAtModuleEncapAtmChannelsFromString(char *idListString, AtAtmTc *channels, uint32 channelBufferSize);
uint32 CliAtModuleEncapFrVirtualCircuitFromNonPrefixString(char *pStrIdList, AtChannel *channels, uint32 channelBufferSize);
uint32 CliAtModuleEncapMfrVirtualCircuitFromNonPrefixString(char *pStrIdList, AtChannel *channels, uint32 channelBufferSize);

#endif /* ATMODULEENCAPCLI_H_ */

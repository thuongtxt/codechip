/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : CliAtHdlcLink.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : HDLC Link CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleEncap.h"
#include "../eth/CliAtModuleEth.h"
#include "AtPppLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*LinkEnableFunc)(AtChannel self, eBool enable);
typedef eAtRet (*FlowMacSetFunc)(AtEthFlow, uint8 *);
typedef eAtModuleEthRet (*EthVlanManipulateFunc)(AtEthFlow self, const tAtEthVlanDesc * desc);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cCmdPppLinkPhaseStr[] = {"dead", "establish", "network", "networkactive"};
static const uint32 cCmdPppLinkPhaseVal[] = {cAtPppLinkPhaseDead, cAtPppLinkPhaseEstablish,
                                             cAtPppLinkPhaseEnterNetwork, cAtPppLinkPhaseNetworkActive};

static const char *cCmdHdlcLinkOamModeStr[] =
    {
    "topsn", "tocpu", "discard"
    };
static const uint32 cCmdHdlcLinkOamModeVal[] =
    {
    cAtHdlcLinkOamModeToPsn, cAtHdlcLinkOamModeToCpu, cAtHdlcLinkOamModeDiscard
    };

static const char *cHdlcLinkPduStr[] = { "ipv4", "ipv6", "ethernet", "any"};
static const uint32 cHdlcLinkPduVal[] = { cAtHdlcPduTypeIPv4, cAtHdlcPduTypeIPv6,
                                          cAtHdlcPduTypeEthernet, cAtHdlcPduTypeAny};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool LinkTrafficEnable(char argc, char **argv, LinkEnableFunc LinkEnable, eBool enable)
    {
    uint32 numberOfLinks;
    uint32 link_i;
    AtHdlcLink *links;
    eBool success = cAtTrue;

    AtUnused(argc);
    links = CliHdlcLinkListGet(argv[0], &numberOfLinks);
    if (numberOfLinks == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No links to %s. Please check input ID\r\n", enable ? "enable" : "disable");
        return cAtTrue;
        }

    /* Handle each object by ID */
    for (link_i = 0; link_i < numberOfLinks; link_i++)
        {
        eAtRet ret = LinkEnable((AtChannel)links[link_i], enable);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            AtPrintc(cSevCritical,
                     "ERROR %s link %s fail with ret = %s\n",
                     enable ? "Enable" : "Disable",
                     CliChannelIdStringGet((AtChannel)links[link_i]),
                     AtRet2String(ret));
            }
        }

    return success;
    }

static eBool OamEgMacSet(char argc, char**argv, FlowMacSetFunc macSetFunc)
    {
    AtEthFlow *flows;
    uint32 flow_i;
    uint32 numFlows;
    uint8 mac[cAtMacAddressLen];
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    /* Get all flows */
    flows = CliHdlcLinkOamFlowListGet(argv[0], &numFlows);
    if ((numFlows == 0) || (flows == NULL))
        {
        AtPrintc(cSevWarning, "No flow\r\n");
        return cAtTrue;
        }

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "Invalid MAC address\r\n");
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        ret |= macSetFunc(flows[flow_i], mac);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

static eBool HelperOamVlanManipulate(char **argv, EthVlanManipulateFunc vlanManipulateFunc)
    {
    AtEthFlow *flows;
    uint32 flow_i;
    uint32 numFlows;
    tAtEthVlanDesc desc;
    eAtRet ret = cAtOk;

    /* Initialize structure */
    AtOsalMemInit(&desc, 0x0, sizeof(desc));

    /* Get all flows */
    flows = CliHdlcLinkOamFlowListGet(argv[0], &numFlows);
    if ((numFlows == 0) || (flows == NULL))
        {
        AtPrintc(cSevWarning, "No flow\r\n");
        return cAtTrue;
        }

    /* Get Port Id */
    desc.ethPortId = (uint8)CliId2DriverId(AtStrToDw(argv[1]));

    /* Get CVLAN and SVLAN Description */
    if (!CliEthFlowVlanGet(argv[2], argv[3], &desc))
        {
        AtPrintc(cSevCritical, "Invalid VLAN, expected %s\r\n", CliExpectedVlanFormat());
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        ret |= vlanManipulateFunc(flows[flow_i], &desc);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEncapHdlcLinkShow(char argc, char **argv)
    {
    uint32 numberOfLinks;
    uint32 link_i;
    AtHdlcLink *links;
    tTab *tabPtr;
    const char *string;
    const char *heading[] = {"LinkID", "OAM", "EthFlowId", "TrafficTx",
                             "TrafficRx", "LinkPhase", "Insert Lan-Fcs", "PFC", "Enabled", "PDU", "Bundle"};

    AtUnused(argc);
    /* Get the shared ID buffer */
    links = CliHdlcLinkListGet(argv[0], &numberOfLinks);
    if (numberOfLinks == 0)
        {
        AtPrintc(cSevCritical,
                 "ERROR: The number of Encap Links are zero!\r\n");
        return cAtFalse;
        }

    /* Handle each object by ID */
    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLinks, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (link_i = 0; link_i < numberOfLinks; link_i++)
        {
        uint32 colum = 0;
        AtHdlcChannel hdlcChannel;
        AtHdlcBundle bundle;
        eBool enabled;
        AtEthFlow flow;
        eBool success;

        StrToCell(tabPtr, link_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)links[link_i]) + 1, "%u"));
        string = CliEnumToString(AtHdlcLinkOamPacketModeGet(links[link_i]),
                         cCmdHdlcLinkOamModeStr,
                         cCmdHdlcLinkOamModeVal,
                         mCount(cCmdHdlcLinkOamModeVal),
                         NULL);
        StrToCell(tabPtr, link_i, colum++, string ? string : "Error");

        flow = AtHdlcLinkBoundFlowGet(links[link_i]);
        if (flow)
            StrToCell(tabPtr, link_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)flow) + 1, "%u"));
        else
            StrToCell(tabPtr, link_i, colum++, "xxx");

        StrToCell(tabPtr, link_i, colum++, CliBoolToString(AtHdlcLinkTxTrafficIsEnabled(links[link_i])));
        StrToCell(tabPtr, link_i, colum++, CliBoolToString(AtHdlcLinkRxTrafficIsEnabled(links[link_i])));

        hdlcChannel = AtHdlcLinkHdlcChannelGet(links[link_i]);
        if (AtHdlcChannelFrameTypeGet(hdlcChannel) == cAtHdlcFrmPpp)
            {
            /* Link phase */
            string = CliEnumToString(AtPppLinkPhaseGet((AtPppLink)links[link_i]),
                         cCmdPppLinkPhaseStr,
                         cCmdPppLinkPhaseVal,
                         mCount(cCmdPppLinkPhaseVal),
                         NULL);
            StrToCell(tabPtr, link_i, colum++, string ? string : "Error");

            /* LAN FCS */
            enabled = AtPppLinkBcpLanFcsInsertionIsEnabled((AtPppLink)links[link_i]);
            ColorStrToCell(tabPtr, link_i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

            /* Compress */
            enabled = AtPppLinkTxProtocolFieldIsCompressed((AtPppLink)links[link_i]);
            ColorStrToCell(tabPtr, link_i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);
            }
        else
            {
            /* Link phase */
            StrToCell(tabPtr, link_i, colum++, sAtNotApplicable);

            /* Compress */
            StrToCell(tabPtr, link_i, colum++, sAtNotApplicable);

            /* LAN FCS */
            StrToCell(tabPtr, link_i, colum++, sAtNotApplicable);
            }

        /* Enable */
        enabled = AtChannelIsEnabled((AtChannel)links[link_i]);
        ColorStrToCell(tabPtr, link_i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        string = CliEnumToString(AtHdlcLinkPduTypeGet(links[link_i]),
                                cHdlcLinkPduStr,
                                cHdlcLinkPduVal,
                                sizeof(cHdlcLinkPduVal),
                                &success);
        StrToCell(tabPtr, link_i, colum++, string ? string : "Error");

        /* Bundle that this link belongs to */
        bundle = AtHdlcLinkBundleGet(links[link_i]);
        if (bundle)
            StrToCell(tabPtr, link_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)bundle) + 1, "%u"));
        else
            StrToCell(tabPtr, link_i, colum++, "none");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEncapLkOAM(char argc, char **argv)
    {
    uint32 numberOfLinks, link_i;
    AtHdlcLink *links;
    eAtHdlcLinkOamMode eOmaMd = cAtHdlcLinkOamModeInvalid;
    eBool succcess;

    /* Get channels */
    AtUnused(argc);
    links = CliHdlcLinkListGet(argv[0], &numberOfLinks);
    if (numberOfLinks == 0)
        return cAtFalse;

    mAtStrToEnum(cCmdHdlcLinkOamModeStr,
                 cCmdHdlcLinkOamModeVal,
                 argv[1],
                 eOmaMd,
                 succcess);
    if (!succcess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <oamMode> , expected topsn|tocpu\r\n");
        return cAtFalse;
        }

    succcess = cAtTrue;
    for (link_i = 0; link_i < numberOfLinks; link_i++)
        {
        eAtRet ret = AtHdlcLinkOamPacketModeSet(links[link_i], eOmaMd);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Cannot change OAM mode on link %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)links[link_i]) + 1,
                     AtRet2String(ret));
            succcess = cAtFalse;
            }
        }

    return succcess;
    }

/* Flow binding*/
eBool CmdEncapHdlcLkFlowBind(char argc, char **argv)
    {
    uint32 bufferSize;
    uint32 numberOfLinks;
    uint32 numberOfFlowIds;
    uint32 link_i;
    uint32 *flowIdBuffer = NULL;
    AtHdlcLink *links;
    eBool isBind = cAtFalse;
    eBool success = cAtTrue;

    /* Get channels */
    AtUnused(argc);
    links = CliHdlcLinkListGet(argv[0], &numberOfLinks);
    if ((numberOfLinks == 0)|| (links == NULL))
         return cAtFalse;

    /* Flow is different from NULL, bind HDLC link. Unbind if flow is NULL */
    if (AtStrcmp(argv[1], "none"))
        {
        /* And use remain ID buffer for flow IDs */
        flowIdBuffer = CliSharedIdBufferGet(&bufferSize);
        numberOfFlowIds = CliIdListFromString(argv[1], flowIdBuffer, bufferSize);
        if (numberOfLinks != numberOfFlowIds)
            {
            AtPrintc(cSevCritical, "ERROR: The number of Encap Links and Ethernet Flows are different\r\n");
            return cAtFalse;
            }
        }

    /* Flow id buffer is NULL, unbind, otherwise, bind */
    isBind = (flowIdBuffer) ? cAtTrue : cAtFalse;
    for (link_i = 0; link_i < numberOfLinks; link_i++)
        {
        eAtRet ret;
        AtEthFlow flow = flowIdBuffer ? (AtEthFlow)AtModuleEthFlowGet(CliAtEthModule(), (uint16)(flowIdBuffer[link_i] - 1)) : NULL;
        if (isBind && (flow == NULL))
            {
            AtPrintc(cSevCritical,
                     "ERROR: Flow %u does not exist, cannot bind it to link %s\r\n",
                     flowIdBuffer[link_i],
                     CliChannelIdStringGet((AtChannel) links[link_i]));
            success = cAtFalse;
            continue;
            }

        ret = AtHdlcLinkFlowBind(links[link_i], flow);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            if (isBind)
                AtPrintc(cSevWarning,
                        "Cannot bind flow %u to link %u, Return code = %s\n",
                        flowIdBuffer[link_i],
                        AtChannelIdGet((AtChannel)links[link_i]) + 1,
                        AtRet2String(ret));
            else
                AtPrintc(cSevWarning,
                         "Cannot unbind flow from link %u, Return code = %s\n",
                         AtChannelIdGet((AtChannel) links[link_i]) + 1,
                         AtRet2String(ret));
            }
        }

    return success;
    }

eBool CmdEncapHdlcLkTrafficTxEn(char argc, char **argv)
    {
    return LinkTrafficEnable(argc, argv, AtChannelTxEnable, cAtTrue);
    }

eBool CmdEncapHdlcLkTrafficTxDis(char argc, char **argv)
    {
    return LinkTrafficEnable(argc, argv, AtChannelTxEnable, cAtFalse);
    }

eBool CmdEncapHdlcLkTrafficRxEn(char argc, char **argv)
    {
    return LinkTrafficEnable(argc, argv, AtChannelRxEnable, cAtTrue);
    }

eBool CmdEncapHdlcLkTrafficRxDis(char argc, char **argv)
    {
    return LinkTrafficEnable(argc, argv, AtChannelRxEnable, cAtFalse);
    }

eBool CmdHdlcLinkOamEgDestMacSet(char argc, char **argv)
    {
    return OamEgMacSet(argc, argv, (FlowMacSetFunc)AtEthFlowEgressDestMacSet);
    }

eBool CmdHdlcLinkOamEgSrcMacSet(char argc, char**argv)
    {
    return OamEgMacSet(argc, argv, (FlowMacSetFunc)AtEthFlowEgressSourceMacSet);
    }

eBool CmdHdlcLinkOamEgVlanSet(char argc, char **argv)
	{	
	AtUnused(argc);
    return HelperOamVlanManipulate(argv, AtEthFlowEgressVlanSet);
	}
	
eBool CmdHdlcLinkOamEgVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return HelperOamVlanManipulate(argv, AtEthFlowEgressVlanAdd);
    }

eBool CmdHdlcLinkOamEgVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return HelperOamVlanManipulate(argv, AtEthFlowEgressVlanRemove);
    }

eBool CmdHdlcLinkOamIgVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return HelperOamVlanManipulate(argv, AtEthFlowIngressVlanAdd);
    }

eBool CmdHdlcLinkOamIgVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return HelperOamVlanManipulate(argv, AtEthFlowIngressVlanRemove);
    }

eBool CmdHdlcLinkOamFlowGet(char argc, char**argv)
    {
    AtEthFlow *flows;
    uint32 numFlows;
    AtUnused(argc);

    /* Get all flows */
    flows = CliHdlcLinkOamFlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "No flow\r\n");
        return cAtTrue;
        }

    return CliEthFlowsTableShow(flows, numFlows);
    }

eBool CmdAtEncapHdlcLinkPduTypeSet(char argc, char **argv)
    {
    eBool success;
    AtHdlcLink *links;
    uint32 numberOfLinks, link_i;
    uint16 pduType = cAtHdlcPduTypeAny;

    /* Get channels */
    AtUnused(argc);
    links = CliHdlcLinkListGet(argv[0], &numberOfLinks);
    if (numberOfLinks == 0)
        {
        AtPrintc(cSevWarning, "ERROR: no link, the ID may be wrong or link have not been created\r\n");
        return cAtFalse;
        }

    pduType = (uint16)CliStringToEnum(argv[1], cHdlcLinkPduStr, cHdlcLinkPduVal, sizeof(cHdlcLinkPduVal), &success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <pduType> , expected ethernet|ipv4|ipv6|any\r\n");
        return cAtFalse;
        }

    success = cAtTrue;
    for (link_i = 0; link_i < numberOfLinks; link_i++)
        {
        eAtRet ret = AtHdlcLinkPduTypeSet(links[link_i], pduType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Cannot changed PDU type on link %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)links[link_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEncapHdlcLinkDebug(char argc, char** argv)
    {
    AtHdlcLink *links;
    uint32 numberOfLinks, link_i;

    /* Get channels */
    AtUnused(argc);
    links = CliHdlcLinkListGet(argv[0], &numberOfLinks);
    if (numberOfLinks == 0)
        {
        AtPrintc(cSevWarning, "ERROR: no link, the ID may be wrong or link have not been created\r\n");
        return cAtFalse;
        }

    for (link_i = 0; link_i < numberOfLinks; link_i++)
        AtChannelDebug((AtChannel)links[link_i]);

    return cAtTrue;
    }

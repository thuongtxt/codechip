/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : CliAtResequenceEngine.c
 *
 * Created Date: May 29, 2017
 *
 * Description : Cli encapsulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../driver/include/man/AtDevice.h"
#include "../../../driver/include/man/AtModule.h"
#include "../../../driver/src/implement/default/man/ThaDevice.h"
#include "AtCli.h"
#include "CliAtModuleEncap.h"
#include "showtable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtResequenceManager ResequenceManager(void)
    {
    return ThaDeviceResequenceManagerGet((ThaDevice)CliDevice());
    }

static eBool ResequenceEngineEnable(char argc, char **argv, eBool enable)
    {
    uint32 i;
    uint32 *engineIdList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* ID list */
    engineIdList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIdList, bufferSize);

    if (numberOfEngine == 0)
        return cAtFalse;

    /* Get controllers */
    for (i = 0; i < numberOfEngine; i++)
        {
        uint32 engineId = engineIdList[i] - 1;
        AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(),engineId);
        eAtRet ret = AtResequenceEngineEnable(resequenceEngine, enable);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: re-sequence engine can not be %s, ret = %s\r\n", (enable) ? "enabled" : "disabled", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

static eBool ResequenceEngineQueueEnable(char argc, char **argv, eBool enable)
    {
    uint32 i;
    uint32 *engineList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* queueId list */
    engineList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineList, bufferSize);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Engine Id parameter invalid, expect 1,2..\r\n");
        return cAtFalse;
        }

    /* Get controllers */
    for (i = 0; i < numberOfEngine; i++)
        {
        uint32 engineId = engineList[i] - 1;
        AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(), engineId);
        eAtRet ret = AtResequenceEngineOutputQueueEnable(resequenceEngine, enable);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: re-sequence engine can not be %s, ret = %s\r\n", (enable) ? "enabled" : "disabled", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtEncapResequenceEngineEnable(char argc, char **argv)
    {
    return ResequenceEngineEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEncapResequenceEngineDisable(char argc, char **argv)
    {
    return ResequenceEngineEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEncapResequenceEngineQueue(char argc, char **argv)
    {
    uint32 i;
    uint32 queueId;
    uint32 *engineIdList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* ID list */
    engineIdList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIdList, bufferSize);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Engine Id parameter invalid, expect 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    queueId = AtStrToDw(argv[1]);
    for (i = 0; i < numberOfEngine; i++)
        {
        uint32 engineId = engineIdList[i] - 1;
        AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(),engineId);
        eAtRet ret = AtResequenceEngineOutputQueueSet(resequenceEngine, queueId);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: re-sequence engine not set = %s\r\n", AtRet2String(ret));
            return cAtFalse;
            }
       }

    return cAtTrue;
    }

eBool CmdAtEncapResequenceEngineQueueEnable(char argc, char **argv)
    {
    return ResequenceEngineQueueEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEncapResequenceEngineQueueDisable(char argc, char **argv)
    {
    return ResequenceEngineQueueEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEncapResequenceEngineTimeoutSet(char argc, char **argv)
    {
    uint32 i;
    uint32 maxDelay;
    uint32 *engineIdList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* ID list */
    engineIdList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIdList, bufferSize);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Engine Id parameter invalid, expect 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    maxDelay = AtStrToDw(argv[1]);
    for (i = 0; i < numberOfEngine; i++)
        {
        uint32 engineId = engineIdList[i] - 1;
        AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(), engineId);
        eAtRet ret = AtResequenceEngineTimeoutSet(resequenceEngine, maxDelay);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: re-sequence not set time out ret = %s\r\n", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtEncapResequenceEngineMsruSet(char argc, char **argv)
    {
    uint32 i;
    uint32 msru;
    uint32 *engineIdList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* ID list */
    engineIdList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIdList, bufferSize);
    if (numberOfEngine == 0)
       {
       AtPrintc(cSevCritical, "ERROR: Engine Id parameter invalid, expect 1,2..\r\n");
       return cAtFalse;
       }

    /* Call API */
    msru = AtStrToDw(argv[1]);
    for (i = 0; i < numberOfEngine; i++)
       {
       uint32 engineId = engineIdList[i] - 1;
       AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(),engineId);
       eAtRet ret = AtResequenceEngineMsruSet(resequenceEngine, msru);
       if (ret != cAtOk)
           {
           AtPrintc(cSevCritical, "ERROR: re-sequence not set msru ret = %s\r\n", AtRet2String(ret));
           return cAtFalse;
           }
       }

    return cAtTrue;
    }

eBool CmdAtEncapResequenceEngineMrruSet(char argc, char **argv)
    {
    uint32 i;
    uint32 mrru;
    uint32 *engineIdList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* ID list */
    engineIdList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIdList, bufferSize);
    if (numberOfEngine == 0)
       {
       AtPrintc(cSevWarning, "No Engine to display\r\n");
       return cAtTrue;
       }

    /* Call API */
    mrru = AtStrToDw(argv[1]);
    for (i = 0; i < numberOfEngine; i++)
       {
       uint32 engineId = engineIdList[i] - 1;
		AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(),engineId);
       eAtRet ret = AtResequenceEngineMrruSet(resequenceEngine, mrru);
       if (ret != cAtOk)
           {
           AtPrintc(cSevCritical, "ERROR: re-sequence not set mrru ret = %s\r\n", AtRet2String(ret));
           return cAtFalse;
           }
       }

    return cAtTrue;
    }

eBool CmdAtShowEncapResequenceEngine(char argc, char **argv)
    {
    char   buff[256];
    const char *pHeading[] = {"Engine ID", "Enable", "Output Queue", "Queue Enable", "Hdlc Bundle", "Flow", "Time out", "Msru", "Mrru"};
    tTab   *tabPtr;
    uint32 i;
    uint32 *engineIdList;
    uint32 bufferSize, numberOfEngine;
    AtUnused(argc);

    /* parse Engine Id */
    engineIdList = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIdList, bufferSize);

    /* No Engine Id to show */
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevWarning, "No Engine to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numberOfEngine, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfEngine; i++)
        {
        uint32 column = 0;
        uint32 engineId = engineIdList[i] - 1;
        AtResequenceEngine resequenceEngine = AtResequenceManagerEngineGet(ResequenceManager(), engineId);
        eBool isEnabled = AtResequenceEngineIsEnabled(resequenceEngine);
        eBool queueIsEnabled = AtResequenceEngineOutputQueueIsEnabled(resequenceEngine);
        AtHdlcBundle bundle = AtResequenceEngineHdlcBundleGet(resequenceEngine);
        AtEthFlow flow = AtResequenceEngineEthFlowGet(resequenceEngine);

        /* Print Bundle Id */
        AtSprintf(buff, "%u", AtResequenceEngineIdGet(resequenceEngine) + 1);
        StrToCell(tabPtr, i, column++, buff);

        ColorStrToCell(tabPtr, i, column++, CliBoolToString(isEnabled), isEnabled ? cSevInfo : cSevCritical);

        /* Print Queue */
        AtSprintf(buff, "%u", AtResequenceEngineOutputQueueGet(resequenceEngine));
        StrToCell(tabPtr, i, column++, buff);

        ColorStrToCell(tabPtr, i, column++, CliBoolToString(queueIsEnabled), queueIsEnabled ? cSevInfo : cSevCritical);
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)bundle));
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)flow));

        /* Print time out */
        AtSprintf(buff, "%u", AtResequenceEngineTimeoutGet(resequenceEngine));
        StrToCell(tabPtr, i, column++, buff);

        /* Print Msru */
        AtSprintf(buff, "%u", AtResequenceEngineMsruGet(resequenceEngine));
        StrToCell(tabPtr, i, column++, buff);

        /* Print Mrru */
        AtSprintf(buff, "%u", AtResequenceEngineMrruGet(resequenceEngine));
        StrToCell(tabPtr, i, column++, buff);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

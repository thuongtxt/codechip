/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtModuleEncapId.c
 *
 * Created Date: Oct 11, 2012
 *
 * Description : CLI Encapsulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleEncap.h"
#include "AtEncapChannel.h"
#include "../concate/CliAtModuleConcate.h"
#include "../sdh/CliAtSdhLineDcc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* EncapTypeToString(uint32 encapType)
    {
    switch (encapType)
        {
        case cAtEncapHdlc: return "HDLC";
        case cAtEncapGfp: return "GFP";
        default: return "UNKNOW";
        }
    }

static uint32 VcgEncapsFromString(char *idListString,
                                  eAtEncapType encapType,
                                  eBool checkEncapType,
                                  AtEncapChannel *channels,
                                  uint32 channelBufferSize)
    {
    uint32 *idBuffer, numberOfIds, index;
    uint32 numChannels;
    AtEncapChannel encapChannel;
    AtConcateGroup group;

    if (idListString == NULL)
        return 0;

    /* Parse flat IDs */
    idBuffer    = CliSharedIdBufferGet(&numberOfIds);
    numberOfIds = CliIdListFromString(idListString, idBuffer, numberOfIds);
    if (numberOfIds == 0)
        return numberOfIds;

    /* Get HDLC channels */
    numChannels = 0;
    for (index = 0; index < numberOfIds; index++)
        {
        /* Enough */
        if (numChannels == channelBufferSize)
            return numChannels;

        group = AtModuleConcateGroupGet(CliAtConcateModule(), idBuffer[index] - 1);
        encapChannel = AtConcateGroupEncapChannelGet(group);

        if ((group == NULL)|| (encapChannel == NULL))
            {
            AtPrintc(cSevWarning,
                     "WARNING: Encapsulation channel vcg.%d has not been created or it is not %s. It is ignored now.\r\n",
                     idBuffer[index],
                     EncapTypeToString(encapType));
            continue;
            }

        if (checkEncapType &&((AtConcateGroupEncapTypeGet(group) != encapType)|| (AtConcateGroupEncapTypeGet(group) != encapType)))
            {
            AtPrintc(cSevWarning,
                     "WARNING: Encapsulation channel vcg.%d has not been created or it is not %s. It is ignored now.\r\n",
                     idBuffer[index],
                     EncapTypeToString(encapType));
            continue;
            }

        channels[numChannels] = encapChannel;
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

static uint32 EncapChannelsFromString(char *idListString,
                                      eAtEncapType encapType,
                                      eBool checkEncapType,
                                      AtEncapChannel *channels,
                                      uint32 channelBufferSize)
    {
    uint32 *idBuffer, numberOfIds, index;
    uint32 numChannels;
    AtEncapChannel encapChannel;

    if (idListString == NULL)
        return 0;

    /* Parse flat IDs */
    idBuffer    = CliSharedIdBufferGet(&numberOfIds);
    numberOfIds = CliIdListFromString(idListString, idBuffer, numberOfIds);
    if (numberOfIds == 0)
        return numberOfIds;

    /* Get HDLC channels */
    numChannels = 0;
    for (index = 0; index < numberOfIds; index++)
        {
        /* Enough */
        if (numChannels == channelBufferSize)
            return numChannels;

        encapChannel = AtModuleEncapChannelGet(CliAtEncapModule(), (uint16)(idBuffer[index] - 1));
        if ((encapChannel == NULL) || (checkEncapType && (AtEncapChannelEncapTypeGet(encapChannel) != encapType)))
            continue;

        channels[numChannels] = encapChannel;
        numChannels = numChannels + 1;
        }

    return numChannels;
    }

static uint32 VcgEncapChannelsFromString(char *idListString, eAtEncapType channelType, eBool needCheckEncapType, AtEncapChannel *channels, uint32 channelBufferSize)
    {
    char *idType, *idString;
    char* pDupString = IdTypeAndIdListGet(idListString, &idType, &idString);
    uint32 numChannels;

    if ((idType == NULL) || (idString == NULL))
        return 0;

    numChannels = VcgEncapsFromString(idString, channelType, needCheckEncapType, channels, channelBufferSize);

    /* Free memory */
    AtOsalMemFree(pDupString);

    return numChannels;
    }

static uint32 DccEncapChannelsFromString(char *idListString, AtEncapChannel *channels, uint32 channelBufferSize)
    {
    char *idType, *idString;
    char* pDupString = IdTypeAndIdListGet(idListString, &idType, &idString);
    AtList hdlcList = CliAtDccHdlcListFromString(idString);
    uint32 i, numHdlc = AtListLengthGet(hdlcList);

    for (i = 0; i < numHdlc; i++)
        {
        if (i == channelBufferSize)
            break;

        channels[i] = (AtEncapChannel)AtListObjectGet(hdlcList, i);
        }

    AtOsalMemFree(pDupString);
    AtObjectDelete((AtObject)hdlcList);
    return i;
    }

static uint32 CliEncapChannelsFromString(char *idListString, eAtEncapType channelType, eBool needCheckEncapType, AtEncapChannel *channels, uint32 channelBufferSize)
    {
    if (idListString == NULL)
        return 0;

    if (AtStrncmp(idListString, sAtChannelTypeVcg, AtStrlen(sAtChannelTypeVcg)) == 0)
        return VcgEncapChannelsFromString(idListString, channelType, needCheckEncapType, channels, channelBufferSize);

    if (AtStrncmp(idListString, sAtChannelTypeDcc, AtStrlen(sAtChannelTypeDcc)) == 0)
        return DccEncapChannelsFromString(idListString, channels, channelBufferSize);

    return EncapChannelsFromString(idListString,  channelType, needCheckEncapType, channels, channelBufferSize);
    }

uint32 CliAtModuleEncapHdlcChannelsFromString(char *idListString, AtHdlcChannel *channels, uint32 channelBufferSize)
    {
    eBool hasCheckEncapType = cAtTrue;
    return CliEncapChannelsFromString(idListString, cAtEncapHdlc, hasCheckEncapType, (AtEncapChannel*)channels, channelBufferSize);
    }

uint32 CliAtModuleEncapGfpChannelsFromString(char *idListString, AtGfpChannel *channels, uint32 channelBufferSize)
    {
    eBool hasCheckEncapType = cAtTrue;
    return CliEncapChannelsFromString(idListString, cAtEncapGfp, hasCheckEncapType, (AtEncapChannel*)channels, channelBufferSize);
    }

uint32 CliAtModuleEncapAtmChannelsFromString(char *idListString, AtAtmTc *channels, uint32 channelBufferSize)
    {
    eBool hasCheckEncapType = cAtTrue;
    return CliEncapChannelsFromString(idListString, cAtEncapAtm, hasCheckEncapType, (AtEncapChannel*)channels, channelBufferSize);
    }

uint32 CliAtEncapChannelsFromString(char *idListString, AtEncapChannel *channelBuffer, uint32 channelBufferSize)
    {
    eBool needCheckEncapType = cAtFalse;
    return CliEncapChannelsFromString(idListString, cAtEncapUnknown, needCheckEncapType, channelBuffer, channelBufferSize);
    }

AtEthFlow *CliHdlcLinkOamFlowListGet(char *pStrEncapChannelIdList, uint32 *numFlows)
    {
    AtHdlcChannel *channelBuffer;
    AtEthFlow *allFlows;
    uint32 numHdlcChannels;
    uint32 hdlc_channel_i;
    uint32 numFlowIds;
    uint32 numValidFlows = 0;

    /* Get the shared ID buffer */
    channelBuffer = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numHdlcChannels));
    numHdlcChannels = CliAtModuleEncapHdlcChannelsFromString(pStrEncapChannelIdList, channelBuffer, numHdlcChannels);
    if (numHdlcChannels == 0)
        {
        AtPrintc(cSevWarning, "ERROR: Invalid parameter <channelList> , expected 1-%d or vcg.1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()),
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return NULL;
        }

    allFlows = (AtEthFlow *)((void**)CliSharedObjectListGet(&numFlowIds));
    for (hdlc_channel_i = 0; hdlc_channel_i < numHdlcChannels; hdlc_channel_i++)
        {
        AtHdlcLink link = AtHdlcChannelHdlcLinkGet(channelBuffer[hdlc_channel_i]);
        AtEthFlow flow = AtHdlcLinkOamFlowGet(link);
        if (flow != NULL)
            {
            allFlows[numValidFlows]= flow;
            numValidFlows++;

            /* If store full share buffer */
            if (numValidFlows == numFlowIds)
                break;
            }
        else
            AtPrintc(cSevWarning, "Flow does not exist on channel %s\r\n", CliChannelIdStringGet((AtChannel) channelBuffer[hdlc_channel_i]));
        }

    *numFlows = numValidFlows;

    return allFlows;
    }

AtHdlcLink *CliHdlcLinkListGet(char *pStrHdlcIdList, uint32 *numLinks)
    {
    uint32 numHdlcChannels, hdlc_channel_i;
    AtHdlcChannel *channelBuffer;
    uint32 numLinkIds;
    AtHdlcLink *allLinks;
    uint32 numValidLinks = 0;

    *numLinks = 0;

    /* Get the shared ID buffer */
    channelBuffer = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numHdlcChannels));
    numHdlcChannels = CliAtModuleEncapHdlcChannelsFromString(pStrHdlcIdList, channelBuffer, numHdlcChannels);
    if (numHdlcChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <channelList> , expected 1-%d or vcg.1-%d\r\n",
                 AtModuleEncapMaxChannelsGet(CliAtEncapModule()),
                 AtModuleConcateMaxGroupGet(CliAtConcateModule()));
        return NULL;
        }

    allLinks = (AtHdlcLink *)((void**)CliSharedObjectListGet(&numLinkIds));
    for (hdlc_channel_i = 0; hdlc_channel_i < numHdlcChannels; hdlc_channel_i++)
        {
        AtHdlcLink link = AtHdlcChannelHdlcLinkGet(channelBuffer[hdlc_channel_i]);
        if (link != NULL)
            {
            allLinks[numValidLinks]= link;
            numValidLinks++;

            /* If store full share buffer */
              if (numValidLinks == numLinkIds)
                  break;
            }
        else
            AtPrintc(cSevWarning,
                     "Link does not exist on channel %s\r\n",
                     CliChannelIdStringGet((AtChannel) channelBuffer[hdlc_channel_i]));
        }

    *numLinks = numValidLinks;

    return allLinks;
    }

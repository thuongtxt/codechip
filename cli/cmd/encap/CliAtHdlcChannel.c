/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliAtHdlcChannel.c
 *
 * Created Date: Mar 21, 2016
 *
 * Description : Cli encapsulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleEncap.h"
#include "AtHdlcChannel.h"
#include "showtable.h"
#include "AtModuleEth.h"
#include "AtAtmTc.h"
#include "AtEncapChannel.h"
#include "AtPppLink.h"
#include "AtCliModule.h"
#include "CliAtHdlcChannel.h"
#include "../eth/CliAtModuleEth.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterType, counterKind, counterName)                     \
        CliCounterTypePrintToCell(tabPtr, counter_i++, channel_i + 1UL, allCounters.counterType, counterKind, counterName); \

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeSetFunc)(AtHdlcChannel self, uint32 value);
typedef eAtRet (*AttributeAddrCtrlSetFunc)(AtHdlcChannel self, uint32 value1, uint8* value2);
typedef eAtModuleEncapRet (*BoolAttributeSetFunc)(AtHdlcChannel self, eBool value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cCmdEncapHdlcFrameTypeStr[] =
    {
    "unknown", "cisco", "ppp", "framerelay", "laps", "lapd"
    };

static const uint32 cCmdEncapHdlcFrameTypeVal[] =
    {
    cAtHdlcFrmUnknown,
    cAtHdlcFrmCiscoHdlc,
    cAtHdlcFrmPpp,
    cAtHdlcFrmFr,
    cAtHdlcFrmLaps,
    cAtHdlcFrmLapd
    };

static const char *cCmdHdlcFcsModeStr[] =
    {
    "nofcs", "fcs16", "fcs32"
    };
static const uint32 cCmdHdlcFcsModeVal[] =
    {
    cAtHdlcFcsModeNoFcs, cAtHdlcFcsModeFcs16, cAtHdlcFcsModeFcs32
    };

static const char *cCmdHdlcStuffModeStr[] =
    {
    "bit", "byte"
    };
static const uint32 cCmdHdlcStuffModeVal[] =
    {
    cAtHdlcStuffBit, cAtHdlcStuffByte
    };

static const char *cCmdHdlcFcsCalculationModeStr[] =
    {
    "lsb", "msb"
    };

static const uint32 cCmdHdlcFcsCalculationModeVal[] =
    {
     cAtHdlcFcsCalculationModeLsb, cAtHdlcFcsCalculationModeMsb
    };

static const char *cAtHdlcChannelAlarmTypeStr[] =
    {
    "fcs-error",
    "undersize",
    "oversize",
    "decap-buffer-full",
    "encap-buffer-full"
    };

static const uint32 cAtHdlcChannelAlarmTypeVal[] =
    {
     cAtHdlcChannelAlarmTypeFcsError,
     cAtHdlcChannelAlarmTypeUndersize,
     cAtHdlcChannelAlarmTypeOversize,
     cAtHdlcChannelAlarmTypeDecapBufferFull,
     cAtHdlcChannelAlarmTypeEncapBufferFull
    };

static const char *cAtEncapHdlcDropConditionStr[] = {
                                        "none",
                                        "fcs-error",
                                        "address-control-error"
                                        };

static const uint32 cAtEncapHdlcDropConditionValue[] = {
                                        cAtHdlcChannelDropConditionNone,
                                        cAtHdlcChannelDropConditionPktFcsError,
                                        cAtHdlcChannelDropConditionPktAddrCtrlError
                                        };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DisplayNoChannels(void)
    {
    AtPrintc(cSevWarning, "WARNING: no channels to configure\r\n");
    return cAtTrue;
    }

static uint32* CliListOfValuesFromString(char *valueListString, uint32 *numValue)
    {
    uint32      *idBuf;
    uint32      bufferSize;

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    *numValue = CliIdListFromString(valueListString, idBuf, bufferSize);

    return idBuf;
    }

static eBool BoolFunctionSet(char argc, char **argv, BoolAttributeSetFunc setFunc, eBool boolValue)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No HDLC channels, check flat ID list or encapsulation type\r\n");
        return cAtTrue;
        }

    /* Apply */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = setFunc(channels[channel_i], boolValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool BoolAttributeSet(char argc, char **argv, AttributeSetFunc setFunc)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    eBool boolValue;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channel, the ID may be wrong or channels have not been created\r\n");
        return cAtTrue;
        }

    /* Enabling */
    mAtStrToBool(argv[1], boolValue, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    /* Apply */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = setFunc(channels[channel_i], boolValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel) channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static char** BuildHeadingForChannels(AtHdlcChannel *channelList, uint32 currentChannelIndex, uint32 numChannelsPerTable)
    {
    AtHdlcChannel channel;
    uint32 channel_i;
    char **headings;

    headings = AtOsalMemAlloc((numChannelsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(8);
    AtSprintf(headings[0], "Channel");

    for (channel_i = 0; channel_i < numChannelsPerTable; channel_i++)
        {
        channel = channelList[currentChannelIndex + channel_i];
        headings[channel_i + 1] = AtOsalMemAlloc(16);
        AtSnprintf(headings[channel_i + 1], 16, "%s", AtChannelIdString((AtChannel)channel));
        }

    return headings;
    }

static void DestroyHeadings(char **heading, uint32 numColumns)
    {
    uint32 i;

    for (i = 0; i < numColumns; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

eBool CliHdlcChannelCounterShow(AtHdlcChannel *channels, uint32 numChannels, char argc, char **argv)
    {
    static const uint8 cNumChannelsPerTable = 10;
    uint32 numTables, table_i;
    uint32  remainChannel = numChannels;
    tAtHdlcChannelCounters allCounters;
    eAtRet (*AllCountersGet)(AtChannel self, void* allCounters) = AtChannelAllCountersGet;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    eBool silent = cAtFalse;

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Calculate number of tables need to show counters of all input Channels */
    numTables = numChannels / cNumChannelsPerTable;
    if ((numChannels % cNumChannelsPerTable) > 0)
        numTables = numTables + 1;

    if (readingMode == cAtHistoryReadingModeReadToClear)
        AllCountersGet = AtChannelAllCountersClear;

    for (table_i = 0; table_i < numTables; table_i++)
        {
        tTab *tabPtr = NULL;
        uint32 numChannelsPerTable;
        uint16 channel_i;
        char **heading;
        uint8 counter_i = 0;
        uint32 currentChannelIndex = table_i * cNumChannelsPerTable;

        numChannelsPerTable = remainChannel;
        if (numChannelsPerTable > cNumChannelsPerTable)
            numChannelsPerTable = cNumChannelsPerTable;

        /* Create table */
        heading = BuildHeadingForChannels(channels, currentChannelIndex, numChannelsPerTable);

        if (!silent)
            {
            tabPtr = TableAlloc(0, numChannelsPerTable + 1, (void *)heading);
            if (!tabPtr)
                {
                AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
                return cAtFalse;
                }
            }

        /* Build table */
        for (channel_i = 0; channel_i < numChannelsPerTable; channel_i++)
            {
            AtChannel channel = (AtChannel)channels[currentChannelIndex + channel_i];
            if (AllCountersGet(channel, &allCounters) != cAtOk)
                {
                AtPrintc(cSevWarning, "ERROR: Cannot get counters of channel %s\r\n", AtChannelIdString(channel));
                continue;
                }

            counter_i = 0;
            mPutCounter(txPkt,                cAtCounterTypeGood, "txPacket");
            mPutCounter(txByte,               cAtCounterTypeGood, "txByte");
            mPutCounter(txGoodPkt,            cAtCounterTypeGood, "txGoodPacket");
            mPutCounter(txGoodByte,           cAtCounterTypeGood, "txGoodByte");
            mPutCounter(txIdleByte,           cAtCounterTypeGood, "txIdleByte");
            mPutCounter(txFragment,           cAtCounterTypeGood, "txFragment");
            mPutCounter(txPlain,              cAtCounterTypeGood, "txPlain");
            mPutCounter(txOam,                cAtCounterTypeGood, "txOam");
            mPutCounter(txMTUPkts,            cAtCounterTypeGood, "txMTUPacket");
            mPutCounter(txAbortPkt,           cAtCounterTypeError,"txAbortPacket");

            mPutCounter(rxPkt,                cAtCounterTypeGood, "rxPacket");
            mPutCounter(rxByte,               cAtCounterTypeGood, "rxByte");
            mPutCounter(rxGoodPkt,            cAtCounterTypeGood, "rxGoodPacket");
            mPutCounter(rxGoodByte,           cAtCounterTypeGood, "rxGoodByte");
            mPutCounter(rxFragment,           cAtCounterTypeGood, "rxFragment");
            mPutCounter(rxPlain,              cAtCounterTypeGood, "rxPlain");
            mPutCounter(rxOam,                cAtCounterTypeGood, "rxOam");
            mPutCounter(rxIdleByte,           cAtCounterTypeGood, "rxIdleByte");
            mPutCounter(rxMRUPkts,            cAtCounterTypeGood, "rxMRUPacket");
            mPutCounter(rxAbortPkt,           cAtCounterTypeError,"rxAbortPacket");
            mPutCounter(rxFcsErrPkt,          cAtCounterTypeError,"rxFcsErrorPacket");
            mPutCounter(rxAddrCtrlErrPkt,     cAtCounterTypeError,"rxAddCtrlErrorPacket");
            mPutCounter(rxDiscardPkt,         cAtCounterTypeError,"rxDiscardPacket");
            mPutCounter(rxMinLenVioPackets,   cAtCounterTypeError,"rxMinLenViolationPacket");
            mPutCounter(rxMinLenVioBytes,     cAtCounterTypeError,"rxMinLenViolationByte");
            mPutCounter(rxMaxLenVioPackets,   cAtCounterTypeError,"rxMaxLenViolationPacket");
            mPutCounter(rxMaxLenVioBytes,     cAtCounterTypeError,"rxMaxLenViolationByte");
            }

        TablePrint(tabPtr);
        TableFree(tabPtr);
        DestroyHeadings(heading, numChannelsPerTable + 1);

        /* Next table */
        remainChannel = remainChannel - numChannelsPerTable;
        }

    return cAtTrue;
    }

uint32 CliHdlcFcsModeFromString(const char *fcsModeStr)
    {
    eBool valid;
    eAtHdlcFcsMode fcsMode = cAtHdlcFcsModeFcs16;

    fcsMode = CliStringToEnum(fcsModeStr, cCmdHdlcFcsModeStr, cCmdHdlcFcsModeVal, mCount(cCmdHdlcFcsModeVal), &valid);
    if (valid)
        return fcsMode;

    AtPrintc(cSevCritical, "ERROR: Invalid parameter FCS mode. Expected: ");
    CliExpectedValuesPrint(cSevCritical, cCmdHdlcFcsModeStr, mCount(cCmdHdlcFcsModeVal));
    AtPrintc(cSevCritical, "\r\n");
    return cAtHdlcFcsModeUnknown;
    }

const char *CliHdlcFcsModeToString(uint32 fcsMode)
    {
    return CliEnumToString(fcsMode, cCmdHdlcFcsModeStr, cCmdHdlcFcsModeVal, mCount(cCmdHdlcFcsModeVal), NULL);
    }

eBool CliAtHdlcChannelFlagSet(AtHdlcChannel *channels, uint32 numChannels, char *flagString, char *numFlagString)
    {
    uint32 channel_i;
    uint8 flagValue;
    eBool success = cAtTrue;
    uint32 numFlags = 1;

    if (numChannels == 0)
        return DisplayNoChannels();

    flagValue = (uint8)AtStrtoul(flagString, NULL, 16);
    if (numFlagString)
        numFlags = AtStrToDw(numFlagString);

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelFlagSet(channels[channel_i], (uint8)flagValue);

        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not set flag for HDLC channel %s, ret = %s\n",
                     CliChannelIdStringGet((AtChannel)channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }

        if (numFlagString)
            {
            ret = AtHdlcChannelNumFlagsSet(channels[channel_i], (uint8)numFlags);
            if (ret != cAtOk)
                {
                AtPrintc(cSevWarning,
                         "Can not configure number of flags for HDLC channel %s, ret = %s\n",
                         CliChannelIdStringGet((AtChannel)channels[channel_i]),
                         AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    return success;
    }

eBool CliAtHdlcChannelMtuSet(AtHdlcChannel *channels, uint32 numChannels, char *mtuString)
    {
    uint32 channel_i;
    uint32 mtuVal;
    eBool success = cAtTrue;

    /* Get channels */
    if (numChannels == 0)
        return DisplayNoChannels();

    mtuVal = (uint32)AtStrToDw(mtuString);
    success = cAtTrue;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelMtuSet(channels[channel_i], mtuVal);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not set MTU for HDLC channel %s, ret = %s\n",
                     CliChannelIdStringGet((AtChannel)channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtHdlcChannelFcsModeSet(AtHdlcChannel *channels, uint32 numChannels, char *fcsModeString)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    eAtHdlcFcsMode fcsMode = cAtHdlcFcsModeFcs16;

    /* Get channels */
    if (numChannels == 0)
        return DisplayNoChannels();

    fcsMode = CliHdlcFcsModeFromString(fcsModeString);
    if (fcsMode == cAtHdlcFcsModeUnknown)
        return cAtFalse;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelFcsModeSet(channels[channel_i], fcsMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not change fcsMode for HDLC channel %s, ret = %s\n",
                     CliChannelIdStringGet((AtChannel)channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtHdlcChannelStuffModeSet(AtHdlcChannel *channels, uint32 numChannels, char *stuffString)
    {
    uint32 channel_i;
    eAtHdlcStuffMode stuffMode = cAtHdlcStuffBit;
    eBool success;

    if (numChannels == 0)
        return DisplayNoChannels();

    mAtStrToEnum(cCmdHdlcStuffModeStr,
                 cCmdHdlcStuffModeVal,
                 stuffString,
                 stuffMode,
                 success);
    if (!success)
        {
        AtPrintc(cSevWarning, "Invalid parameter <stuffingMode> , expected bit|byte\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelStuffModeSet(channels[channel_i], stuffMode);

        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not set stuff mode for HDLC channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

const char *CliAtHdlcStuffMode2String(eAtHdlcStuffMode stuffMode)
    {
    return CliEnumToString(stuffMode,
                           cCmdHdlcStuffModeStr,
                           cCmdHdlcStuffModeVal,
                           mCount(cCmdHdlcStuffModeVal),
                           NULL);
    }

eBool CmdAtHdlcChannelAddrInsertEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressInsertionEnable, cAtTrue);
    }

eBool CmdAtHdlcChannelAddrInsertDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressInsertionEnable, cAtFalse);
    }

eBool CmdAtHdlcChannelCtrlInsertEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelControlInsertionEnable, cAtTrue);
    }

eBool CmdAtHdlcChannelCtrlInsertDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelControlInsertionEnable, cAtFalse);
    }

eBool CmdAtHdlcChannelAddrTransmit(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtHdlcChannel *channels;
    uint32 numValues;
    uint32 *valuesList;
    eBool success = cAtTrue;
    uint8 address;
    eAtRet ret;
    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No HDLC channels, check flat ID list or encapsulation type\r\n");
        return cAtFalse;
        }

    /* Get List of values */
    valuesList = CliListOfValuesFromString(argv[1], &numValues);
    if ((numValues != 1) && (numValues == numChannels))
        {
        AtPrintc(cSevNormal, "Number of input values is not acceptable, must be 1 or %u value\r\n", numChannels);
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        address = (uint8)((numValues == 1) ? valuesList[0] : valuesList[i]);
        ret = AtHdlcChannelTxAddressSet(channels[i], &address);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)channels[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelCtrlTransmit(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtHdlcChannel *channels;
    uint32 numValues;
    uint32 *valuesList;
    eBool success = cAtTrue;
    uint8 control;
    eAtRet ret;
    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No HDLC channels, check flat ID list or encapsulation type\r\n");
        return cAtFalse;
        }

    /* Get List of values */
    valuesList = CliListOfValuesFromString(argv[1], &numValues);
    if ((numValues != 1) && (numValues == numChannels))
        {
        AtPrintc(cSevNormal, "Number of input values is not acceptable, must be 1 or %u value\r\n", numChannels);
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        control = (uint8)((numValues == 1) ? valuesList[0] : valuesList[i]);
        ret = AtHdlcChannelTxControlSet(channels[i], &control);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)channels[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelAddrCompareEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressMonitorEnable, cAtTrue);
    }

eBool CmdAtHdlcChannelAddrCompareDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressMonitorEnable, cAtFalse);
    }

eBool CmdAtHdlcChannelCtrlCompareEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelControlMonitorEnable, cAtTrue);
    }

eBool CmdAtHdlcChannelCtrlCompareDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelControlMonitorEnable, cAtFalse);
    }

eBool CmdAtHdlcChannelAddressExpect(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtHdlcChannel *channels;
    uint32 numValues;
    uint32 *valuesList;
    eBool success = cAtTrue;
    uint8 expectedAddress;
    eAtRet ret;
    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No HDLC channels, check flat ID list or encapsulation type\r\n");
        return cAtFalse;
        }

    /* Get List of values */
    valuesList = CliListOfValuesFromString(argv[1], &numValues);
    if ((numValues != 1) && (numValues == numChannels))
        {
        AtPrintc(cSevNormal, "Number of input values is not acceptable, must be 1 or %u value\r\n", numChannels);
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        expectedAddress = (uint8)((numValues == 1) ? valuesList[0] : valuesList[i]);
        ret = AtHdlcChannelExpectedAddressSet(channels[i], &expectedAddress);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)channels[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelControlExpect(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtHdlcChannel *channels;
    uint32 numValues;
    uint32 *valuesList;
    eBool success = cAtTrue;
    uint8 expectedControl;
    eAtRet ret;
    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No HDLC channels, check flat ID list or encapsulation type\r\n");
        return cAtFalse;
        }

    /* Get List of values */
    valuesList = CliListOfValuesFromString(argv[1], &numValues);
    if ((numValues != 1) && (numValues == numChannels))
        {
        AtPrintc(cSevNormal, "Number of input values is not acceptable, must be 1 or %u value\r\n", numChannels);
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        expectedControl = (uint8)((numValues == 1) ? valuesList[0] : valuesList[i]);
        ret = AtHdlcChannelExpectedControlSet(channels[i], &expectedControl);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)channels[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelAddrBypassEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressControlBypass, cAtTrue);
    }

eBool CmdAtHdlcChannelAddrBypassDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressControlBypass, cAtFalse);
    }

eBool CmdAtHdlcChannelCtrlBypassEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressControlBypass, cAtTrue);
    }

eBool CmdAtHdlcChannelCtrlBypassDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressControlBypass, cAtFalse);
    }

eBool CmdAtHdlcChannelFrameTypeSet(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    eAtHdlcFrameType frameType = cAtHdlcFrmUnknown;
    eBool success;
    eAtRet ret;
    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], (AtEncapChannel*)channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    /* Frame type */
    mAtStrToEnum(cCmdEncapHdlcFrameTypeStr,
                 cCmdEncapHdlcFrameTypeVal,
                 argv[1],
                 frameType,
                 success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <encapType>, expected cisco|ppp|framerelay|atm\r\n");
        return cAtFalse;
        }

    /* Apply all channels */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        ret = AtHdlcChannelFrameTypeSet(channels[channel_i], frameType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not set frame type for HDLC channel %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel) channels[channel_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEncapHdlcShow(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    tTab *tabPtr;
    const char *heading[] = {"ID", "physical", "scramble", "flag", "fcs", "stuffing",
                             "mtu", "FrameType", "fcsErrorByPass", "txACFC", "rxACFC", "idlePattern", "addressSize", "fcsCalculationMode"};

    /* Get channels */
    AtUnused(argc);
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtChannel physicalChannel;
        const char *string;
        uint32 colum = 0;

        /* Print ID */
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)channels[channel_i]) + 1, "%u"));

        /* Print Physical Binding*/
        physicalChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)channels[channel_i]);
        StrToCell(tabPtr, channel_i, colum++, (physicalChannel != NULL) ? (char *)CliChannelIdStringGet(physicalChannel) : "None");

        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelScrambleIsEnabled(channels[channel_i])));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtHdlcChannelFlagGet(channels[channel_i]), "%#x"));

        /* Print FCS mode */
        string = CliHdlcFcsModeToString(AtHdlcChannelFcsModeGet(channels[channel_i]));
        StrToCell(tabPtr, channel_i, colum++, string ? string : "Error");

        /* Print stuff mode */
        string = CliEnumToString(AtHdlcChannelStuffModeGet(channels[channel_i]),
                     cCmdHdlcStuffModeStr,
                     cCmdHdlcStuffModeVal,
                     mCount(cCmdHdlcStuffModeVal),
                     NULL);
        StrToCell(tabPtr, channel_i, colum++, string ? string : "Error");

        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtHdlcChannelMtuGet(channels[channel_i]), "%u"));

        /* Print Encapsulation Frame type*/
        string = CliEnumToString(AtHdlcChannelFrameTypeGet(channels[channel_i]),
                     cCmdEncapHdlcFrameTypeStr,
                     cCmdEncapHdlcFrameTypeVal,
                     mCount(cCmdEncapHdlcFrameTypeVal),
                     NULL);
        StrToCell(tabPtr, channel_i, colum++, string ? string : "Error");

        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelFcsErrorIsBypassed(channels[channel_i])));

        /* Compress */
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelTxAddressControlIsCompressed(channels[channel_i])));
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelRxAddressControlIsCompressed(channels[channel_i])));

        /* Idle */
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtHdlcChannelIdlePatternGet(channels[channel_i]), "%#x"));

        /* Address Size */
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtHdlcChannelAddressSizeGet(channels[channel_i]), "%u"));

        /* Print Value Calculation FCS*/
        string = CliEnumToString(AtHdlcChannelFcsCalculationModeGet(channels[channel_i]),
                     cCmdHdlcFcsCalculationModeStr,
                     cCmdHdlcFcsCalculationModeVal,
                     mCount(cCmdHdlcFcsCalculationModeVal),
                     NULL);
        StrToCell(tabPtr, channel_i, colum++, string ? string : "Error");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEncapHdlcCounter(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of link */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtFalse;
        }

    return CliHdlcChannelCounterShow(channels, numChannels, argc, argv);
    }

eBool CmdEncapHdlcFcsMode(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels;

    AtUnused(argc);
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);

    return CliAtHdlcChannelFcsModeSet(channels, numChannels, argv[1]);
    }

eBool CmdEncapHdlcStuffMode(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels;

    /* Get channels */
    AtUnused(argc);
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);

    return CliAtHdlcChannelStuffModeSet(channels, numChannels, argv[1]);
    }

eBool CmdEncapHdlcMtu(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels;

    AtUnused(argc);
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);

    return CliAtHdlcChannelMtuSet(channels, numChannels, argv[1]);
    }

eBool CmdAtHdlcChannelAddrCtrlTxCompressEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelTxAddressControlCompress, cAtTrue);
    }

eBool CmdAtHdlcChannelAddrCtrlTxCompressDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelTxAddressControlCompress, cAtFalse);
    }

eBool CmdAtHdlcChannelAddrCtrlRxCompressEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelRxAddressControlCompress, cAtTrue);
    }

eBool CmdAtHdlcChannelAddrCtrlRxCompressDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelRxAddressControlCompress, cAtFalse);
    }

eBool CmdAtHdlcChannelAddrCtrlCompressEnable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressControlCompress, cAtTrue);
    }

eBool CmdAtHdlcChannelAddrCtrlCompressDisable(char argc, char **argv)
    {
    return BoolFunctionSet(argc, argv, AtHdlcChannelAddressControlCompress, cAtFalse);
    }

eBool CmdAtHdlcChannelAddressControlBypass(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, (AttributeSetFunc)(AtHdlcChannelAddressControlBypass));
    }

eBool CmdAtHdlcChannelShowAddressControl(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    eBool enabled;
    uint8 address, control;
    tTab *tabPtr;

    const char *heading[] =
            {"HDLC", "insert", "txAddr", "txCtrl", "compare", "expAddr", "expCtrl", "bypass", "bypassError", "compress"};


    AtUnused(argc);
    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
         return cAtFalse;

    /* Create table */
    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtHdlcChannel hdlcChannel = channels[channel_i];
        uint8 colum = 0;

        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)channels[channel_i]) + 1, "%u"));

        /* Address/control insertion */
        enabled  = AtHdlcChannelAddressInsertionIsEnabled(hdlcChannel);
        AtHdlcChannelTxAddressGet(hdlcChannel, &address);
        enabled |= AtHdlcChannelAddressInsertionIsEnabled(hdlcChannel);
        AtHdlcChannelTxControlGet(hdlcChannel, &control);
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(enabled));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String((uint32)address, "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(control, "0x%02x"));

        /* Address/control comparison */
        enabled  = AtHdlcChannelAddressMonitorIsEnabled(hdlcChannel);
        AtHdlcChannelExpectedAddressGet(hdlcChannel, &address);
        enabled |= AtHdlcChannelAddressMonitorIsEnabled(hdlcChannel);
        AtHdlcChannelExpectedControlGet(hdlcChannel, &control);
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(enabled));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(address, "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String((uint32)control, "0x%02x"));
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelAddressControlIsBypassed(hdlcChannel)));

        /* Error handling */
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelAddressControlErrorIsBypassed(hdlcChannel)));
        StrToCell(tabPtr, channel_i, colum++, CliBoolToString(AtHdlcChannelAddressControlIsCompressed(hdlcChannel)));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEncapHdlcFlag(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels;

    AtUnused(argc);

    /* Get channels */
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    return CliAtHdlcChannelFlagSet(channels, numChannels, argv[1], NULL);
    }

eBool CmdAtHdlcChannelIdlePatternSet(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    uint8 idlePattern;
    eBool success = cAtTrue;

    /* Get channels */
    AtUnused(argc);
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    idlePattern = (uint8)AtStrtoul(argv[1], NULL, 16);
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelIdlePatternSet(channels[channel_i], idlePattern);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not change Idle pattern for HDLC channel %u, ret = %s\n",
                     AtChannelIdGet((AtChannel)channels[channel_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtHdlcChannelAddressSizeSet(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    uint8 addressSize;
    eBool success = cAtTrue;

    /* Get channels */
    AtUnused(argc);
    channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        return cAtFalse;

    addressSize = (uint8)AtStrToDw(argv[1]);
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelAddressSizeSet(channels[channel_i], addressSize);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not change Address size for HDLC channel %u, ret = %s\n",
                     AtChannelIdGet((AtChannel)channels[channel_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEncapHdlcFcsCalculationSet(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = (AtHdlcChannel *)((void**)CliSharedChannelListGet(&numChannels));
    AtUnused(argc);
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channels, numChannels);
    return CliAtHdlcChannelFcsCalculationMode(channels, numChannels, argv[1]);
    }

eBool CliAtHdlcChannelIntrMaskSet(char *intrMaskString, char *enableString, AtHdlcChannel *channels, uint32 numChannels)
    {
    uint32 intrMaskVal;
    eBool enable = cAtFalse;
    uint32 i;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;

    /* Get interrupt mask value from input string */
    intrMaskVal = CliMaskFromString(intrMaskString,
                                    cAtHdlcChannelAlarmTypeStr,
                                    cAtHdlcChannelAlarmTypeVal,
                                    mCount(cAtHdlcChannelAlarmTypeVal));
    if (intrMaskVal == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(enableString, enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel hdlcChannel = (AtChannel)channels[i];
        ret = AtChannelInterruptMaskSet(hdlcChannel, intrMaskVal, enable ? intrMaskVal : 0x0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure interrupt mask for %s, ret = %s\r\n",
                     CliChannelIdStringGet(hdlcChannel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtHdlcChannelInterruptShow(char argc, char **argv, AtHdlcChannel *channels, uint32 numChannels)
    {
    uint32 channel_i;
    uint32 alarmStat, alarm_i;
    tTab *tabPtr = NULL;
    uint32 (*AlarmInterruptGet)(AtChannel self) = AtChannelDefectInterruptGet;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    const char *heading[] = { "ID", " ", " ", " ", " ", " "};

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    /* Silent */
    if (argc >= 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
        /* Build heading */
        AtAssert(mCount(heading) == (mCount(cAtHdlcChannelAlarmTypeStr) + 1));
        for (alarm_i = 0; alarm_i < mCount(cAtHdlcChannelAlarmTypeStr); alarm_i++)
            heading[alarm_i + 1] = cAtHdlcChannelAlarmTypeStr[alarm_i];

        /* Create table */
        tabPtr = TableAlloc(numChannels, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    if (readingMode == cAtHistoryReadingModeReadToClear)
        AlarmInterruptGet = AtChannelDefectInterruptClear;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        uint32 column = 0;
        AtChannel hdlcChannel = (AtChannel)channels[channel_i];
        StrToCell(tabPtr, channel_i, column++, AtChannelIdString(hdlcChannel));

        /* Print alarm status */
        alarmStat = AlarmInterruptGet(hdlcChannel);
        for (alarm_i = 0; alarm_i < mCount(cAtHdlcChannelAlarmTypeVal); alarm_i++)
            {
            if (alarmStat & cAtHdlcChannelAlarmTypeVal[alarm_i])
                ColorStrToCell(tabPtr, channel_i, alarm_i + 1, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, channel_i, alarm_i + 1, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtHdlcChannelAlarmShow(AtHdlcChannel *channels, uint32 numChannels)
    {
    uint32 channel_i;
    uint32 alarmStat, alarm_i;
    tTab *tabPtr = NULL;
    const char *heading[] = { "ID", " ", " ", " ", " ", " "};

    /* Build heading */
    AtAssert(mCount(heading) == (mCount(cAtHdlcChannelAlarmTypeStr) + 1));
    for (alarm_i = 0; alarm_i < mCount(cAtHdlcChannelAlarmTypeStr); alarm_i++)
        heading[alarm_i + 1] = cAtHdlcChannelAlarmTypeStr[alarm_i];

    /* Create table */
    tabPtr = TableAlloc(numChannels, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        uint32 column = 0;
        AtChannel hdlcChannel = (AtChannel)channels[channel_i];
        StrToCell(tabPtr, channel_i, column++, AtChannelIdString(hdlcChannel));

        /* Print alarm status */
        alarmStat = AtChannelDefectGet(hdlcChannel);
        for (alarm_i = 0; alarm_i < mCount(cAtHdlcChannelAlarmTypeVal); alarm_i++)
            {
            if (alarmStat & cAtHdlcChannelAlarmTypeVal[alarm_i])
                ColorStrToCell(tabPtr, channel_i, alarm_i + 1, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, channel_i, alarm_i + 1, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

const char *CliHdlcChannelAlarm2String(uint32 alarms)
    {
    uint16 alarm_i;
    static char alarmString[128];

    AtOsalMemInit((char*) alarmString, 0, sizeof(alarmString));

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        {
        AtSprintf(alarmString, "none");
        return alarmString;
        }

    /* There are alarms */
    for (alarm_i = 0; alarm_i < mCount(cAtHdlcChannelAlarmTypeVal); alarm_i++)
        {
        if (alarms & (cAtHdlcChannelAlarmTypeVal[alarm_i]))
            AtSprintf(alarmString, "%s%s|", alarmString, cAtHdlcChannelAlarmTypeStr[alarm_i]);
        }

    if (AtStrlen(alarmString) == 0)
        return "None";

    /* Remove the last '|' */
    alarmString[AtStrlen(alarmString) - 1] = '\0';

    return alarmString;
    }

eBool CliAtHdlcChannelFcsCalculationMode(AtHdlcChannel *channels, uint32 numChannels, const char *modeString)
    {
    uint32 channel_i;
    eBool success;
    eAtHdlcFcsCalculationMode fcsMode;

    fcsMode = CliStringToEnum(modeString,
                              cCmdHdlcFcsCalculationModeStr,
                              cCmdHdlcFcsCalculationModeVal,
                              mCount(cCmdHdlcFcsCalculationModeVal), &success);
    if (!success)
        {
        AtPrintc(cSevWarning, "Invalid parameter <fcsMode> , expected lsb|msb\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelFcsCalculationModeSet(channels[channel_i], fcsMode);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevWarning,
                 "Set FCS mode for HDLC channel %s fail with ret = %s\n",
                 CliChannelIdStringGet((AtChannel)channels[channel_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

const char *CliAtHdlcFcsCalculationMode2Str(eAtHdlcFcsCalculationMode mode)
    {
    return CliEnumToString(mode,
                           cCmdHdlcFcsCalculationModeStr,
                           cCmdHdlcFcsCalculationModeVal,
                           mCount(cCmdHdlcFcsCalculationModeVal), NULL);
    }

void CliAtHdlcChannelAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [ENCAP] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtHdlcChannelAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtHdlcChannelAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtHdlcChannelAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

eBool CmdAtEncapHdlcDropPacketConditionMaskSet(char argc, char **argv)
    {
    uint32 dropConditionMask, channel_i, numChannels;
    AtEncapChannel *channels;
    eBool boolValue;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get channels */
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channel, the ID may be wrong or channels have not been created\r\n");
        return cAtTrue;
        }

    /* Get interrupt mask value from input string */
    dropConditionMask = CliMaskFromString(argv[1], cAtEncapHdlcDropConditionStr, cAtEncapHdlcDropConditionValue, mCount(cAtEncapHdlcDropConditionValue));
    if (dropConditionMask == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], boolValue, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* Apply */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtEncapChannelDropPacketConditionMaskSet(channels[channel_i], boolValue, boolValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel) channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEncapHdlcDropPacketConditionMaskGet(char argc, char **argv)
    {
    uint32 dropConditionMask, channel_i, numChannels;
    AtEncapChannel *channels;
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Encap channel", "Drop-Packet-Conditions"};

    AtUnused(argc);

    /* Get channels */
    channels = (AtEncapChannel *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliAtEncapChannelsFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channel, the ID may be wrong or channels have not been created\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        uint8 colum = 0;
        dropConditionMask = AtEncapChannelDropPacketConditionMaskGet((AtEncapChannel)channels[channel_i]);
        StrToCell(tabPtr, channel_i, colum++, CliNumber2String(AtChannelIdGet((AtChannel)channels[channel_i]) + 1, "%u"));
        StrToCell(tabPtr, channel_i, colum++, CliAlarmMaskToString(cAtEncapHdlcDropConditionStr,
                                                          cAtEncapHdlcDropConditionValue,
                                                          mCount(cAtEncapHdlcDropConditionValue),
                                                          dropConditionMask));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

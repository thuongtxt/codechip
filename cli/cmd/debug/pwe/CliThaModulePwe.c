/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : CliThaModulePwe.c
 *
 * Created Date: Apr 17, 2017
 *
 * Description : PWE debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../util/CliAtDebugger.h"
#include "../../../../driver/src/generic/man/AtModuleInternal.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/default/pwe/ThaModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe PweModule(void)
    {
    return (ThaModulePwe)AtDeviceModuleGet(CliDevice(), cThaModulePwe);
    }

static void PutFreeCacheInfo(tTab *tabPtr, uint32 row, uint32 colum, uint32 free, uint32 used)
    {
    eAtSevLevel color = cSevWarning;
    eBool supported = cAtTrue;

    if (free == 0)
        color = cSevCritical;
    if (used == 0)
        color = cSevInfo;

    if ((free == cInvalidUint32) || (used == cInvalidUint32))
        {
        color = cSevNormal;
        supported = cAtFalse;
        }

    ColorStrToCell(tabPtr, row, colum, supported ? CliNumber2String(free, "%u") : sAtNotSupported, color);
    }

static void PutUsedCacheInfo(tTab *tabPtr, uint32 row, uint32 colum, uint32 free, uint32 used)
    {
    eAtSevLevel color = cSevWarning;
    eBool supported = cAtTrue;

    if (used == 0)
        color = cSevInfo;
    if (free == 0)
        color = cSevCritical;

    if ((free == cInvalidUint32) || (used == cInvalidUint32))
        {
        color = cSevNormal;
        supported = cAtFalse;
        }

    ColorStrToCell(tabPtr, row, colum, supported ? CliNumber2String(used, "%u") : sAtNotSupported, color);
    }

static AtModule Module(void)
    {
    return AtDeviceModuleGet(CliDevice(), cThaModulePwe);
    }

eBool CmdThaModulePweCacheUsageShow(char argc, char **argv)
    {
    tThaPweCache cache;
    tTab *tabPtr;
    const char *heading[] ={"ResourceName", "free", "used"};
    static uint32 cNumAttributes = 6;
    uint32 row = 0;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(cNumAttributes, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    ThaModulePweCacheUsageGet(PweModule(), &cache);

    StrToCell(tabPtr, row, 0, "512ByteCache");
    PutFreeCacheInfo(tabPtr, row, 1, cache.free512ByteCache, cache.used512ByteCache);
    PutUsedCacheInfo(tabPtr, row, 2, cache.free512ByteCache, cache.used512ByteCache);
    row = row + 1;

    StrToCell(tabPtr, row, 0, "256ByteCache");
    PutFreeCacheInfo(tabPtr, row, 1, cache.free256ByteCache, cache.used256ByteCache);
    PutUsedCacheInfo(tabPtr, row, 2, cache.free256ByteCache, cache.used256ByteCache);
    row = row + 1;

    StrToCell(tabPtr, row, 0, "128ByteCache");
    PutFreeCacheInfo(tabPtr, row, 1, cache.free128ByteCache, cache.used128ByteCache);
    PutUsedCacheInfo(tabPtr, row, 2, cache.free128ByteCache, cache.used128ByteCache);
    row = row + 1;

    StrToCell(tabPtr, row, 0, "64ByteCache");
    PutFreeCacheInfo(tabPtr, row, 1, cache.free64ByteCache, cache.used64ByteCache);
    PutUsedCacheInfo(tabPtr, row, 2, cache.free64ByteCache, cache.used64ByteCache);
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numHiBlock");
    PutFreeCacheInfo(tabPtr, row, 1, cache.freeNumHiBlock, cache.usedNumHiBlock);
    PutUsedCacheInfo(tabPtr, row, 2, cache.freeNumHiBlock, cache.usedNumHiBlock);
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numLoBlock");
    PutFreeCacheInfo(tabPtr, row, 1, cache.freeNumLoBlock, cache.usedNumLoBlock);
    PutUsedCacheInfo(tabPtr, row, 2, cache.freeNumLoBlock, cache.usedNumLoBlock);
    row = row + 1;

    AtAssert(row == cNumAttributes); /* This will help when new rows are added */

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdThaModulePweReactivate(char argc, char **argv)
    {
    AtModule pweModule = (AtModule)PweModule();
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = AtModuleReactivate(pweModule);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Reactivate PWE module fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdPweModuleDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleDebug(Module());
    return cAtTrue;
    }

eBool CmdThaModulePweDebuggerShow(char argc, char **argv)
    {
    AtDebugger debugger = AtModuleDebuggerCreate(Module());
    eBool success = CliAtDebuggerShow(debugger);
    AtUnused(argc);
    AtUnused(argv);
    AtObjectDelete((AtObject)debugger);
    return success;
    }

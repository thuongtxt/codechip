/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : CliAtAttPdhManager.c
 *
 * Created Date: Aug 24, 2017
 *
 * Description : AttPdhManager CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtAttController.h"
#include "../att/CliAtAttController.h"
#include "AtAttPdhManager.h"
#include "../../driver/src/implement/default/att/ThaAttPdhManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAttPdhManagerForceLogicVal[] = {
                                      0,
                                      1
                                    };
static const char *cAttPdhManagerForceLogicStr[] = {
                                     "att",
                                     "dut"
};
static const eAttPdhForceAlarmDurationUnit cAttPdhForceAlarmDurationUnitVal[] = {
                                    cAttPdhForceAlarmDurationUnitMillisecond,
                                    cAttPdhForceAlarmDurationUnitSecond
                                    };
static const char *cAttPdhForceAlarmDurationUnitStr[] = {
                                     "ms",
                                     "second"
};
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAttPdhForceAlarmDurationUnitSet(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    ThaAttPdhManager attManager = (ThaAttPdhManager)AtModulePdhAttManager(modulePdh);
    eAttPdhForceAlarmDurationUnit unit;
    eAtRet ret;

    AtUnused(argc);
    unit = CliStringToEnum(argv[0], cAttPdhForceAlarmDurationUnitStr, cAttPdhForceAlarmDurationUnitVal, mCount(cAttPdhForceAlarmDurationUnitVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Force Alarm Duration Unit is failed: %s, expect ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAttPdhForceAlarmDurationUnitStr, mCount(cAttPdhForceAlarmDurationUnitStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaAttPdhManagerAlarmDurationUnitSet(attManager, unit);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select Force Alarm Duration Unit fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttPdhCheckForceRateShowSet(char argc, char** argv)
    {
    eBool enable;
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    ThaAttPdhManager attManager = (ThaAttPdhManager)AtModulePdhAttManager(modulePdh);
    eAtRet ret;
    AtUnused(argc);

    enable = CliBoolFromString(argv[0]);
    ret = ThaAttPdhManagerCheckForceRateShowSet(attManager, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: enable/disable check rate fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttPdhDebugForceLogicSet(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 logicForcing;
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    ThaAttPdhManager attManager = (ThaAttPdhManager)AtModulePdhAttManager(modulePdh);
    eAtRet ret;
    AtUnused(argc);

    logicForcing = CliStringToEnum(argv[0], cAttPdhManagerForceLogicStr, cAttPdhManagerForceLogicVal, mCount(cAttPdhManagerForceLogicVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Logic forcing is failed: %s, expect ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAttPdhManagerForceLogicStr, mCount(cAttPdhManagerForceLogicStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaAttPdhManagerDebugForcingLogicSet(attManager, (logicForcing ? cAtTrue : cAtFalse));
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select logic fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool CliForcePeriodSet(char argc, char **argv,eAtRet (*ForcePeriodSet)(AtAttPdhManager self, uint32 numSeconds))
    {
    if (argc!=1)
        return cAtFalse;
    else
        {
        AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
        AtAttPdhManager mngr = AtModulePdhAttManager(pdhModule);
        eAtRet ret = ForcePeriodSet(mngr, AtStrToDw(argv[0]));

        if (ret != cAtOk)
            return cAtFalse;
        }
    return cAtTrue;
    }

static eBool CliForcePerfrm(char argc, char **argv,eAtRet (*ForcePerframe)(AtAttPdhManager self))
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    AtAttPdhManager mngr = AtModulePdhAttManager(pdhModule);
    eAtRet ret = ForcePerframe(mngr);


    AtUnused(argc);
    AtUnused(argv);
    if (ret != cAtOk)
        return cAtFalse;

    return cAtTrue;
    }

eBool CmdAtModulePdhDe1ForcePeriodSet(char argc, char **argv)
    {
    return CliForcePeriodSet(argc, argv, AtAttPdhManagerDe1ForcePeriodSet);
    }

eBool CmdAtModulePdhDe3ForcePeriodSet(char argc, char **argv)
    {
    return CliForcePeriodSet(argc, argv, AtAttPdhManagerDe3ForcePeriodSet);
    }

eBool CmdAtModulePdhDe3ForcePerfrmEnable(char argc, char **argv)
    {
    return CliForcePerfrm(argc, argv, AtAttPdhManagerDe3ForcePerframeEnable);
    }

eBool CmdAtModulePdhDe3ForcePerfrmDisable(char argc, char **argv)
    {
    return CliForcePerfrm(argc, argv, AtAttPdhManagerDe3ForcePerframeDisable);
    }

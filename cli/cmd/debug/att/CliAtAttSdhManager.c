/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : CliAtAttSdhManager.c
 *
 * Created Date: Aug 24, 2017
 *
 * Description : AttSdhManager CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtAttController.h"
#include "AtAttSdhManager.h"
#include "../../driver/src/implement/default/att/ThaAttSdhManager.h"
#include <stdlib.h>

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAttSdhManagerForceLogicVal[] = {
                                      0,
                                      1
                                    };
static const char *cAttSdhManagerForceLogicStr[] = {
                                     "att",
                                     "dut"
};

static const eAttSdhForceAlarmDurationUnit cAttSdhForceAlarmDurationUnitVal[] = {
                                    cAttSdhForceAlarmDurationUnitMillisecond,
                                    cAttSdhForceAlarmDurationUnitSecond
                                    };
static const char *cAttSdhForceAlarmDurationUnitStr[] = {
                                     "ms",
                                     "second"
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAttSdhForceAlarmDurationUnitSet(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    ThaAttSdhManager attManager = (ThaAttSdhManager)AtModuleSdhAttManager(moduleSdh);
    eAttSdhForceAlarmDurationUnit unit;
    eAtRet ret;

    AtUnused(argc);
    unit = CliStringToEnum(argv[0], cAttSdhForceAlarmDurationUnitStr, cAttSdhForceAlarmDurationUnitVal, mCount(cAttSdhForceAlarmDurationUnitVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Force Alarm Duration Unit is failed: %s, expect ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAttSdhForceAlarmDurationUnitStr, mCount(cAttSdhForceAlarmDurationUnitStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaAttSdhManagerAlarmDurationUnitSet(attManager, unit);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select Force Alarm Duration Unit  fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhDebugForceLogicSet(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 logicForcing;
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    ThaAttSdhManager attManager = (ThaAttSdhManager)AtModuleSdhAttManager(moduleSdh);
    eAtRet ret;
    AtUnused(argc);

    logicForcing = CliStringToEnum(argv[0], cAttSdhManagerForceLogicStr, cAttSdhManagerForceLogicVal, mCount(cAttSdhManagerForceLogicVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Logic forcing is failed: %s, expect ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAttSdhManagerForceLogicStr, mCount(cAttSdhManagerForceLogicStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaAttSdhManagerDebugForcingLogicSet(attManager, (logicForcing ? cAtTrue : cAtFalse));
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select logic fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static ThaAttSdhManager AttSdhManager(void)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    ThaAttSdhManager attManager = (ThaAttSdhManager)AtModuleSdhAttManager(moduleSdh);
    return attManager;
    }

eBool CmdAttSdhDebugHoPointerLooptimeEnable(char argc, char** argv)
    {
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret = cAtOk;

    AtUnused(argc);
    AtUnused(argv);
    ret = ThaAttSdhManagerDebugHoPointerLooptimeEnable(attManager, cAtTrue);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select logic fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdAttSdhDebugHoPointerLooptimeDisable(char argc, char** argv)
    {
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret = cAtOk;

    AtUnused(argc);
    AtUnused(argv);
    ret = ThaAttSdhManagerDebugHoPointerLooptimeEnable(attManager, cAtFalse);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select logic fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdAttSdhDebugLoPointerLooptimeEnable(char argc, char** argv)
    {
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret = cAtOk;

    AtUnused(argc);
    AtUnused(argv);
    ret = ThaAttSdhManagerDebugLoPointerLooptimeEnable(attManager, cAtTrue);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select logic fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdAttSdhDebugLoPointerLooptimeDisable(char argc, char** argv)
    {
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret = cAtOk;

    AtUnused(argc);
    AtUnused(argv);
    ret = ThaAttSdhManagerDebugLoPointerLooptimeEnable(attManager, cAtFalse);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select logic fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdAttSdhCheckForceRateShowSet(char argc, char** argv)
    {
    eBool enable;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    enable = CliBoolFromString(argv[0]);
    ret = ThaAttSdhManagerCheckForceRateShowSet(attManager, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: enable/disable check rate fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhCheckForceRateEnSet(char argc, char** argv)
    {
    eBool enable;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    enable = CliBoolFromString(argv[0]);
    ret = ThaAttSdhManagerCheckForceRateEnSet(attManager, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: enable/disable check rate fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhDebugSohColRemoveSet(char argc, char** argv)
    {
    uint8 col;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    col = (uint8)AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerSohColRemoveSet(attManager, col);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set SOH col remove fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhForcePointerEnable(char argc, char** argv)
    {
    eBool enable;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    enable = CliBoolFromString(argv[0]);
    ret    = AtAttSdhManagerForcePointerAdjEnable((AtAttSdhManager)attManager, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: enable/disable check rate fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783DebugSet(char argc, char** argv)
    {
    eBool enable;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    enable = CliBoolFromString(argv[0]);
    ret = ThaAttSdhManagerG783DebugSet(attManager, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: enable/disable check rate fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T0Set(char argc, char** argv)
    {
    uint32 T0;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T0 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T0Set(attManager, T0);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T0 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T1Set(char argc, char** argv)
    {
    uint32 T1;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T1 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T1Set(attManager, T1);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T1 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T2Set(char argc, char** argv)
    {
    uint32 T2;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T2 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T2Set(attManager, T2);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T2 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T3Set(char argc, char** argv)
    {
    uint32 T3 = 0;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret = cAtOk;
    AtUnused(argc);

    T3 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T3Set(attManager, T3);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T3 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T3Note2Set(char argc, char** argv)
    {
    uint32 T3Note2 = 0;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret = cAtOk;
    AtUnused(argc);

    T3Note2 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T3Note2Set(attManager, T3Note2);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T3Note2 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T4Set(char argc, char** argv)
    {
    uint32 T4;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T4 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T4Set(attManager, T4);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T4 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }


eBool CmdAttSdhG783T4Note5Set(char argc, char** argv)
    {
    float T4Note5;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T4Note5 = strtof(argv[0], NULL);
    ret = ThaAttSdhManagerG783T4Note5Set(attManager, T4Note5);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T4Note5 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T5Set(char argc, char** argv)
    {
    uint32 T5;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T5 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T5Set(attManager, T5);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T5 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhG783T5Note6Set(char argc, char** argv)
    {
    uint32 T5;
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);

    T5 = AtStrToDw(argv[0]);
    ret = ThaAttSdhManagerG783T5Note6Set(attManager, T5);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: set T5Note6 with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAttSdhForcePointerShow(char argc, char** argv)
    {
    ThaAttSdhManager attManager = AttSdhManager();
    tTab             *tabPtr = NULL;
    uint32           i=0,T;
    float            F;
    const char *pHeading[] = {"Name", "Value"};

    /* Get the shared buffer to hold all of objects, then call the parse function */
    AtUnused(argc);
    AtUnused(argv);
    tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T0Get(attManager);
    StrToCell(tabPtr, i, 0, "T0");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T1Get(attManager);
    StrToCell(tabPtr, i, 0, "T1");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T2Get(attManager);
    StrToCell(tabPtr, i, 0, "T2");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T3Get(attManager);
    StrToCell(tabPtr, i, 0, "T3");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T3Note2Get(attManager);
    StrToCell(tabPtr, i, 0, "T3Note2");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T4Get(attManager);
    StrToCell(tabPtr, i, 0, "T4");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    F = ThaAttSdhManagerG783T4Note5Get(attManager);
    StrToCell(tabPtr, i, 0, "T4Note5");
    StrToCell(tabPtr, i, 1, CliNumber2String(F, "%.3f"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T5Get(attManager);
    StrToCell(tabPtr, i, 0, "T5");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerG783T5Note6Get(attManager);
    StrToCell(tabPtr, i, 0, "T5Note6");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerForcePointerNumChannelGet(attManager);
    StrToCell(tabPtr, i, 0, "NumChannel");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TableAddRow(tabPtr, 1);
    T = ThaAttSdhManagerForcePointerExecuteTimeMsGet(attManager);
    StrToCell(tabPtr, i, 0, "ForcePointerExecuteTime(ms)");
    StrToCell(tabPtr, i, 1, CliNumber2String(T, "%d"));
    i++;

    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

eBool CmdAttShowMapMatch(char argc, char** argv)
    {
    AtHal hal = NULL;
    uint32 regValue=0, maskValue, shiftValue, address, value_en, value_id;
    uint32 id, startAddress, stopAddress;
    if(argc < 3)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }
    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    if (hal == NULL)
       {
       AtPrintc(cSevCritical, "ERROR: No HAL\r\n");
       return cAtFalse;
       }
    StrToDw(argv[0], 'd', &id);
    StrToDw(argv[1], 'h', &startAddress);
    StrToDw(argv[2], 'h', &stopAddress);

    if (stopAddress<startAddress)
        return cAtFalse;
    for (address=startAddress; address<stopAddress; address++)
        {
        regValue = AtHalRead(hal, address);
        maskValue = cBit10;
        shiftValue = 10;
        mFieldGet(regValue, maskValue, shiftValue, uint32, &value_en);

        maskValue = cBit9_0;
        shiftValue = 0;
        mFieldGet(regValue, maskValue, shiftValue, uint32, &value_id);
        if (value_id==id && value_en)
            {
            AtPrintf("0x%06X : 0x%08X\r\n", address, regValue);
            }
        }

    return cAtTrue;
    }

eBool CmdAttSdhForcePointerSourceMultiplexingResync(char argc, char** argv)
    {
    ThaAttSdhManager attManager = AttSdhManager();
    eAtRet ret;
    AtUnused(argc);
    AtUnused(argv);
    ret = ThaAttSdhManagerForcePointerSourceMultiplexingResync(attManager);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: enable/disable check rate fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }



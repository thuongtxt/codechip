/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliThaPktAnalyzerDebug.c
 *
 * Created Date: Dec 19, 2012
 *
 * Description : Packet analyzer CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePktAnalyzer.h"
#include "../../pw/CliAtModulePw.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/pktanalyzer/Tha60210012InternalPktAnalyzer.h"
#include "../../../../driver/src/implement/codechip/Tha60290011/pktanalyzer/Tha60290011ModulePktAnalyzer.h"
#include "../../../../driver/src/implement/default/pw/debugger/ThaPwDebug.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutHexValToCell(value, row, col)   \
    AtSprintf(buf, "0x%X", value);          \
    ColorStrToCell(tabPtr, row, col, buf, value ? cSevInfo : cSevNormal);\

#define mPutDecValToCell(value, row, col)   \
    AtSprintf(buf, "%d", value);          \
    ColorStrToCell(tabPtr, row, col, buf, value ? cSevInfo : cSevNormal);\

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtPktAnalyzer Analyzer(void)
    {
    AtModulePktAnalyzer pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(CliDevice(), cAtModulePktAnalyzer);
    return Tha60210012InternalPacketAnalyzerGet(pktAnalyzerModule);
    }

static ThaPktAnalyzer ClsAnalyzer(void)
    {
    AtModulePktAnalyzer pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(CliDevice(), cAtModulePktAnalyzer);
    return Tha60290011ClsPacketAnalyzerGet(pktAnalyzerModule);
    }

static eBool Analyze(char argc, char **argv, eThaPktAnalyzerPktDumpMode direction)
    {
    uint32 serviceId = AtStrToDw(argv[0]) - 1;
    AtPktAnalyzer analyzer = Analyzer();

    AtUnused(argc);

    if (analyzer == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Packet analyzer does not exist\r\n");
        return cAtFalse;
        }

    /* Set dump direction and service ID */
    Tha60210012InternalPktAnalyzerDirectionSet(analyzer, direction);
    Tha60210012InternalPktAnalyzerServiceIdSet(analyzer, serviceId);

    /* Give time for HW getting packet */
    AtOsalSleep(1);
    AtPktAnalyzerAnalyzeRxPackets(analyzer);

    return cAtTrue;
    }

static void PacketCla2CdrPerErrorPwShow(tTab *tabPtr, AtPw pw, uint32 pwIndex, uint16 pktNum)
    {
    uint16 pkt_i;
    char *pwIdString = CliChannelIdStringGet((AtChannel)pw);

    for (pkt_i = 0; pkt_i < pktNum; pkt_i++)
        {
        uint32 row_i = (pwIndex * pktNum) + pkt_i;
        uint32 col_i = 0;

        StrToCell(tabPtr, row_i, col_i++, pwIdString);
        StrToCell(tabPtr, row_i, col_i++, CliNumber2String(pkt_i + 1, "%u"));

        ColorStrToCell(tabPtr, row_i, col_i++, "error", cSevCritical);
        ColorStrToCell(tabPtr, row_i, col_i++, "error", cSevCritical);

        StrToCell(tabPtr, row_i, col_i++, "error");
        StrToCell(tabPtr, row_i, col_i++, "error");
        StrToCell(tabPtr, row_i, col_i++, "error");
        StrToCell(tabPtr, row_i, col_i++, "error");
        }
    }

static eBool TxInternalPktAnalyze(char argc, char **argv, eThaPktAnalyzerPktDumpMode direction)
    {
    AtPktAnalyzer analyzer = Analyzer();

    if (analyzer == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Packet analyzer does not exist\r\n");
        return cAtFalse;
        }

    if (argc == 1)
        {
        uint32 bufferSize, numItem, *idList;

        idList = CliSharedIdBufferGet(&bufferSize);
        numItem = CliIdListFromString(argv[0], idList, bufferSize);
        if ((Tha60210012InternalPktAnalyzerTxStartEntrySet(analyzer, (uint16)idList[0]) != cAtOk) ||
            (Tha60210012InternalPktAnalyzerTxLastEntrySet(analyzer, (uint16)idList[numItem - 1]) != cAtOk))
            return cAtFalse;
        }

    /* Analyze packet */
    Tha60210012InternalPktAnalyzerDirectionSet(analyzer, direction);
    Tha60210012InternalTxPktAnalyzer(analyzer);

    return cAtTrue;
    }

eBool CmdPktAnalyzerPacketCla2Pda(char argc, char **argv)
    {
    return Analyze(argc, argv, cThaPktAnalyzerDumpClaToPda);
    }

eBool CmdPktAnalyzerPacketPda2Map(char argc, char **argv)
    {
    return Analyze(argc, argv, cThaPktAnalyzerDumpPdaToMap);
    }

eBool CmdPktAnalyzerPacketCla2Cdr(char argc, char **argv)
    {
    const uint32 cMaxRows = 4096;
    const char *pHeading[] = {"PwId", "PacketId", "HoEn", "PktErr", "RtpTimeStamp", "RtpTimeStampOffset", "RtpSequenceNumber", "CWSequenceNumber"};
    uint32 numPws, pw_i;
    AtPw *pwList;
    tTab *tabPtr;
    uint32 col_i, row_i;
    uint16 pkt_i;
    uint16 pktNum = 256;
    uint32 numRows;

    /* Get PW list */
    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get number of packets to dump for PW */
    if (argc > 1)
        pktNum = (uint16)AtStrToDw(argv[1]);

    if (pktNum > 256)
        pktNum = 256;

    /* Choose number of rows to show and limit it */
    numRows = numPws * pktNum;
    if (numRows >= cMaxRows)
        numRows = cMaxRows;

    /* Create table with titles */
    tabPtr = TableAlloc(numRows, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = pwList[pw_i];
        char *pwIdString = CliChannelIdStringGet((AtChannel)pw);
        eAtRet ret;

        /* Flush data */
        ThaPwPktAnalyzerCla2CdrDataFlush(pw);

        /* Trigger for HW to start caching packets */
        ret = ThaPwPktAnalyzerCla2CdrTrigger(pw);
        if (ret != cAtOk)
            {
            PacketCla2CdrPerErrorPwShow(tabPtr, pw, pw_i, pktNum);
            continue;
            }

        /* Sleep to wait packets */
        AtOsalSleep(2);

        for (pkt_i = 0; pkt_i < pktNum; pkt_i++)
            {
            eBool eboolValue;
            uint32 offset;
            row_i = (pw_i * pktNum) + pkt_i;
            col_i = 0;

            StrToCell(tabPtr, row_i, col_i++, pwIdString);
            StrToCell(tabPtr, row_i, col_i++, CliNumber2String(pkt_i + 1, "%u"));

            eboolValue = ThaPwPktAnalyzerCla2CdrHoIsEnabled(pw, pkt_i);
            ColorStrToCell(tabPtr, row_i, col_i++, eboolValue ? "en" : "dis", eboolValue ? cSevInfo : cSevNormal);

            eboolValue = ThaPwPktAnalyzerCla2CdrPacketIsError(pw, pkt_i);
            ColorStrToCell(tabPtr, row_i, col_i++, eboolValue ? "set" : "clear", eboolValue ? cSevCritical : cSevInfo);

            StrToCell(tabPtr, row_i, col_i++, CliNumber2String(ThaPwPktAnalyzerCla2CdrRtpTimeStampGet(pw, pkt_i), "%u"));

            offset = (pkt_i == 0) ? 0 : ThaPwPktAnalyzerCla2CdrRtpTimeStampOffset(pw, (uint16)(pkt_i - 1), pkt_i);
            StrToCell(tabPtr, row_i, col_i++, CliNumber2String(offset, "%u"));

            StrToCell(tabPtr, row_i, col_i++, CliNumber2String(ThaPwPktAnalyzerCla2CdrRtpSequenceNumberGet(pw, pkt_i), "%u"));
            StrToCell(tabPtr, row_i, col_i++, CliNumber2String(ThaPwPktAnalyzerCla2CdrControlWordSequenceNumberGet(pw, pkt_i), "%u"));
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPktAnalyzerPacketTxFromPla(char argc, char **argv)
    {
    return TxInternalPktAnalyze(argc, argv, cThaPktAnalyzerDumpPlaOut);
    }

eBool CmdPktAnalyzerPacketRxAtPwe(char argc, char **argv)
    {
    return TxInternalPktAnalyze(argc, argv, cThaPktAnalyzerDumpPweRx);
    }

eBool CmdPktAnalyzerPacketTxPlaHeader(char argc, char **argv)
    {
    AtPktAnalyzer analyzer = Analyzer();
    static char buf[32];
    tThaInternalPlaPacketHeader header;
    uint16 startEntry, lastEntry, entry_i, row_i, col_i;
    tTab *tabPtr;
    const char *heading[] = {"ID", "FlowId", "FcsByte", "PadNum", "FcsEn", "HdrLen", "EoP", "ForwardType",
    		                 "NetworkProtoId", "PsnAddr", "PktService", "PsnLen", "PktLen"};
    uint32 num_row;

    if (analyzer == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Packet analyzer does not exist\r\n");
        return cAtFalse;
        }

    if (argc == 1)
        {
        uint32 bufferSize, numItem, *idList;

        idList = CliSharedIdBufferGet(&bufferSize);
        numItem = CliIdListFromString(argv[0], idList, bufferSize);
        if ((Tha60210012InternalPktAnalyzerTxStartEntrySet(analyzer, (uint16)idList[0]) != cAtOk) ||
            (Tha60210012InternalPktAnalyzerTxLastEntrySet(analyzer, (uint16)idList[numItem - 1]) != cAtOk))
            return cAtFalse;
        }

    /* Analyze packet */
    startEntry = Tha60210012InternalPktAnalyzerTxStartEntryGet(analyzer);
    lastEntry = Tha60210012InternalPktAnalyzerTxLastEntryGet(analyzer);
    num_row = (uint32)(lastEntry - startEntry + 1);

    tabPtr = TableAlloc(num_row, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Getting header */
    AtOsalMemInit(&header, 0, sizeof(tThaInternalPlaPacketHeader));
    row_i = 0;
    for (entry_i = startEntry; entry_i <= lastEntry; entry_i ++)
        {
        col_i = 0;
		Tha60210012InternalPktAnalyzerDirectionSet(analyzer, cThaPktAnalyzerDumpPlaTxHeader);
		Tha60210012InternalTxPlaPktHeaderAnalyze(analyzer, entry_i, &header);

        /* Put to table */
        AtSprintf(buf, "%d", entry_i);
		StrToCell(tabPtr, row_i, col_i++, buf);

		mPutDecValToCell(header.ethFlowId, row_i, col_i++);
		mPutDecValToCell(header.numFcsByte, row_i, col_i++);
		mPutDecValToCell(header.numPadding, row_i, col_i++);
		mPutDecValToCell(header.fcsEn, row_i, col_i++);
		mPutDecValToCell(header.headerLen, row_i, col_i++);
		mPutDecValToCell(header.eop, row_i, col_i++);
		mPutDecValToCell(header.forwardType, row_i, col_i++);
		mPutHexValToCell(header.networkProtoId, row_i, col_i++);
		mPutHexValToCell(header.psnAddress, row_i, col_i++);
		mPutHexValToCell(header.pktService, row_i, col_i++);
		mPutDecValToCell(header.psnLen, row_i, col_i++);
		mPutDecValToCell(header.pktLen, row_i++, col_i++);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool ClsPktAnalyze(char argc, char **argv, uint8 type)
    {
    ThaPktAnalyzer analyzer = ClsAnalyzer();
    AtUnused(argc);
    AtUnused(argv);

    if (analyzer == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Packet analyzer does not exist\r\n");
        return cAtFalse;
        }

    /* Analyze packet */
    Tha60290011ClsPktAnalyzerPacketTypeSet(analyzer, type);
    Tha60290011ClsPktAnalyzer(analyzer);

    return cAtTrue;
    }

eBool CmdPktAnalyzerPacketRxPweHeader(char argc, char **argv)
    {
    AtPktAnalyzer analyzer = Analyzer();
    static char buf[32];
    tThaInternalPlaPacketHeader header;
    uint16 startEntry, lastEntry, entry_i, row_i, col_i;
    tTab *tabPtr;
    const char *heading[] = {"ID", "FlowId", "EoP", "PktLen", "NetworkProtoId", "PsnAddr",
    		                 "PsnLen", "PktService", "ForwardType", "FragmentFcn"};
    uint32 num_row;

    if (analyzer == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Packet analyzer does not exist\r\n");
        return cAtFalse;
        }

    if (argc == 1)
        {
        uint32 bufferSize, numItem, *idList;

        idList = CliSharedIdBufferGet(&bufferSize);
        numItem = CliIdListFromString(argv[0], idList, bufferSize);
        if ((Tha60210012InternalPktAnalyzerTxStartEntrySet(analyzer, (uint16)idList[0]) != cAtOk) ||
            (Tha60210012InternalPktAnalyzerTxLastEntrySet(analyzer, (uint16)idList[numItem - 1]) != cAtOk))
            return cAtFalse;
        }

    /* Analyze packet */
    startEntry = Tha60210012InternalPktAnalyzerTxStartEntryGet(analyzer);
    lastEntry = Tha60210012InternalPktAnalyzerTxLastEntryGet(analyzer);
    num_row = (uint32)(lastEntry - startEntry + 1);

    tabPtr = TableAlloc(num_row, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Getting header */
    AtOsalMemInit(&header, 0, sizeof(tThaInternalPlaPacketHeader));
    row_i = 0;
    for (entry_i = startEntry; entry_i <= lastEntry; entry_i ++)
        {
        col_i = 0;
		Tha60210012InternalPktAnalyzerDirectionSet(analyzer, cThaPktAnalyzerDumpPweRxHeader);
		Tha60210012InternalRxPwePktHeaderAnalyze(analyzer, entry_i, &header);

        /* Put to table */
        AtSprintf(buf, "%d", entry_i);
		StrToCell(tabPtr, row_i, col_i++, buf);

		mPutDecValToCell(header.ethFlowId, row_i, col_i++);
		mPutDecValToCell(header.eop, row_i, col_i++);
		mPutDecValToCell(header.pktLen, row_i, col_i++);
		mPutHexValToCell(header.networkProtoId, row_i, col_i++);
		mPutHexValToCell(header.psnAddress, row_i, col_i++);
		mPutDecValToCell(header.psnLen, row_i, col_i++);
		mPutHexValToCell(header.pktService, row_i, col_i++);
		mPutDecValToCell(header.forwardType, row_i, col_i++);
		mPutDecValToCell(header.fragmentFcn, row_i++, col_i++);
		}

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPktAnalyzerPacketRxCls(char argc, char **argv)
    {
    return ClsPktAnalyze(argc, argv, cClsPktAnalyzerRx);
    }

eBool CmdPktAnalyzerPacketTxClsDs1(char argc, char **argv)
    {
    return ClsPktAnalyze(argc, argv, cClsPktAnalyzerDs1Tx);
    }

eBool CmdPktAnalyzerPacketTxClsDs3(char argc, char **argv)
    {
    return ClsPktAnalyze(argc, argv, cClsPktAnalyzerDs3Tx);
    }

eBool CmdPktAnalyzerPacketTxClsControl(char argc, char **argv)
    {
    return ClsPktAnalyze(argc, argv, cClsPktAnalyzerControlTx);
    }

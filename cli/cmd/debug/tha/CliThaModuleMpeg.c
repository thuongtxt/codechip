/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPEG
 *
 * File        : CliThaModuleMpeg.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/mpeg/Tha60210012ModuleMpeg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*MpegQueueAttributeSetFunc)(Tha60210012ModuleMpeg self, uint32 channelId, uint32 value);
typedef eAtRet (*QueueThresholdFunction)(Tha60210012ModuleMpeg self, uint32 channelId, uint32 internalThreshold, uint32 externalThreshold);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cThaQueuePriorityStr[] = {"highest", "medium", "lowest"};

static const uint32 cThaMpegQueuePriorityValue[] = {cThaMpegQueuePriorityHighest,
                                                    cThaMpegQueuePriorityMedium,
                                                    cThaMpegQueuePriorityLowest};

static const char* cMpegQueueCounterString[] = {
                                  "Total good packet enqueue",
                                  "CLA Declare Drop",
                                  "MPEG enable for PDA read",
                                  "PDA request read MPEG",
                                  "MPEG return valid to PDA",
                                  "MPEG packet valid to PDA",
                                  "PDA discard full",
                                  "QMXFF request Write DDR",
                                  "QMXFF request Read DDR",
                                  "DDR return ACK to MPEG",
                                  "DDR return Valid Write to MPEG",
                                  "DDR return Valid Read to MPEG"
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210012ModuleMpeg MpegModule(void)
    {
    return (Tha60210012ModuleMpeg)AtDeviceModuleGet(CliDevice(), cThaModuleMpeg);
    }

static eBool LinkQueueThresholdSet(char argc, char** argv, eAtRet (*LinkQueueFunc)(Tha60210012ModuleMpeg self,
                                                                                   uint32 linkId,
                                                                                   uint32 queueId,
                                                                                   uint32 internalThreshold,
                                                                                   uint32 externalThreshold))
    {
    eAtRet ret;
    uint32 numLink, link_i;
    uint32 *pListLinks, bufferSize;
    uint32 interThrshold, exterThrshold;
    eBool success = cAtTrue;
    uint32 queueId;
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    queueId = AtStrToDw(argv[1]) - 1;
    interThrshold = AtStrToDw(argv[2]);
    exterThrshold = AtStrToDw(argv[3]);
    for (link_i = 0; link_i < numLink; link_i++)
        {
        ret = LinkQueueFunc(MpegModule(), pListLinks[link_i] - 1, queueId, interThrshold, exterThrshold);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for link: %d, ret=%s", pListLinks[link_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool MpegQueueThresholdSet(char argc, char** argv, QueueThresholdFunction attributeFunc)
    {
    eAtRet ret;
    uint32 numChannel, channel_i;
    uint32 *pListChannels, bufferSize;
    uint32 interThrshold, exterThrshold;
    eBool success = cAtTrue;
    AtUnused(argc);

    pListChannels = CliSharedIdBufferGet(&bufferSize);
    numChannel = CliIdListFromString(argv[0], pListChannels, bufferSize);
    if (numChannel == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Channel list may be invalid or no created channel\r\n");
        return cAtFalse;
        }

    interThrshold = AtStrToDw(argv[1]);
    exterThrshold = AtStrToDw(argv[2]);
    for (channel_i = 0; channel_i < numChannel; channel_i++)
        {
        ret = attributeFunc(MpegModule(), pListChannels[channel_i] - 1, interThrshold, exterThrshold);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for channels: %d, ret=%s", pListChannels[channel_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool PppLinkQueueEnable(char argc, char** argv, eBool enable, eAtRet (*QueueEnableFunc)(Tha60210012ModuleMpeg self,
                                                                                                uint32 linkId,
                                                                                                uint32 queueId,
                                                                                                eBool enable))
    {
    eAtRet ret;
    uint32 queueId;
    uint32 numChannel, channel_i;
    uint32 *pListChannels, bufferSize;
    eBool success = cAtTrue;
    AtUnused(argc);

    pListChannels = CliSharedIdBufferGet(&bufferSize);
    numChannel = CliIdListFromString(argv[0], pListChannels, bufferSize);
    if (numChannel == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    queueId = AtStrToDw(argv[1]) - 1;
    for (channel_i = 0; channel_i < numChannel; channel_i++)
        {
        ret = QueueEnableFunc(MpegModule(), pListChannels[channel_i] - 1, queueId, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for Link: %d, ret=%s", pListChannels[channel_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool MpegQueueAttributeSet(char argc, char** argv, MpegQueueAttributeSetFunc func, uint32 value)
    {
    eAtRet ret;
    uint32 numId, id_i;
    uint32 *pListId, bufferSize;
    eBool success = cAtTrue;
    Tha60210012ModuleMpeg mpegModule = (Tha60210012ModuleMpeg)AtDeviceModuleGet(CliDevice(), cThaModuleMpeg);
    AtUnused(argc);

    if (mpegModule == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: MPEG module is not supported\n");
        return cAtFalse;
        }

    pListId = CliSharedIdBufferGet(&bufferSize);
    numId = CliIdListFromString(argv[0], pListId, bufferSize);
    if (numId == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID list may be invalid\r\n");
        return cAtFalse;
        }

    for (id_i = 0; id_i < numId; id_i++)
        {
        ret = func(mpegModule, pListId[id_i] - 1, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure priority for ID: %u, ret = %s", pListId[id_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdMpBundleQueueMinThresholdSet(char argc, char** argv)
    {
    return MpegQueueThresholdSet(argc, argv, Tha60210012ModuleMpegBundleQueueMinThresholdSet);
    }

eBool CmdMpBundleQueueMaxThresholdSet(char argc, char** argv)
    {
    return MpegQueueThresholdSet(argc, argv, Tha60210012ModuleMpegBundleQueueMaxThresholdSet);
    }

eBool CmdMpBundleQueuePrioritySet(char argc, char** argv)
    {
    eThaMpegQueuePriority priority;
    eBool convertSuccess;

    priority = CliStringToEnum(argv[1], cThaQueuePriorityStr, cThaMpegQueuePriorityValue, mCount(cThaMpegQueuePriorityValue), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid priority value, expected: ");
        return cAtFalse;
        }

    return MpegQueueAttributeSet(argc, argv, Tha60210012ModuleMpegQueueBundlePrioritySet, priority);
    }

eBool CmdMpBundleQueueQuantumSet(char argc, char** argv)
    {
    return MpegQueueAttributeSet(argc, argv, Tha60210012ModuleMpegBundleQueueQuantumSet, AtStrToDw(argv[1]));
    }

eBool CmdMpBundleQueueShow(char argc, char** argv)
    {
    tTab *tabPtr;
    uint32 bundle_i, numberBundle;
    uint32 *pBundleList, bufferSize;
    eBool convertSuccess;
    const char *heading[] = {"BundleId",
                             "Enable",
                             "MaxIntThreshold(pkts)",
                             "MaxExtThreshold(pkts)",
                             "MinIntThreshold(pkts)",
                             "MinExtThreshold(pkts)",
                             "Priority",
                             "QuantumInByte"};
    AtUnused(argc);

    pBundleList  = CliSharedIdBufferGet(&bufferSize);
    numberBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numberBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid bundle list or no created bundle\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numberBundle, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot alloc table\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (bundle_i = 0; bundle_i < numberBundle; bundle_i++)
        {
        const char *string;
        uint32 value;
        uint8 row = 0;

        StrToCell(tabPtr, bundle_i, row++, CliNumber2String(pBundleList[bundle_i], "%u"));
        string = CliBoolToString(Tha60210012ModuleMpegBundleQueueIsEnabled(MpegModule(), pBundleList[bundle_i] - 1));
        StrToCell(tabPtr, bundle_i, row++, string);

        value = Tha60210012ModuleMpegBundleQueueMaxInternalThresholdGet(MpegModule(), pBundleList[bundle_i] - 1);
        StrToCell(tabPtr, bundle_i, row++, CliNumber2String(value, "%u"));

        value = Tha60210012ModuleMpegBundleQueueMaxExternalThresholdGet(MpegModule(), pBundleList[bundle_i] - 1) * 128;
        StrToCell(tabPtr, bundle_i, row++, CliNumber2String(value, "%u"));

        value = Tha60210012ModuleMpegBundleQueueMinInternalThresholdGet(MpegModule(), pBundleList[bundle_i] - 1);
        StrToCell(tabPtr, bundle_i, row++, CliNumber2String(value, "%u"));

        value = Tha60210012ModuleMpegBundleQueueMinExternalThresholdGet(MpegModule(), pBundleList[bundle_i] - 1) * 128;
        StrToCell(tabPtr, bundle_i, row++, CliNumber2String(value, "%u"));

        string = CliEnumToString(Tha60210012ModuleMpegBundleQueuePriorityGet(MpegModule(), pBundleList[bundle_i] - 1) ,
                                 cThaQueuePriorityStr,
                                 cThaMpegQueuePriorityValue,
                                 mCount(cThaMpegQueuePriorityValue),
                                 &convertSuccess);
        ColorStrToCell(tabPtr, bundle_i, row++, convertSuccess ? string : "unknown", convertSuccess ? cSevInfo : cSevWarning);

        value = Tha60210012ModuleMpegBundleQueueQuantumGet(MpegModule(), pBundleList[bundle_i] - 1);
        StrToCell(tabPtr, bundle_i, row++, CliNumber2String(value, "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdVcgQueueMinThresholdSet(char argc, char** argv)
    {
    return MpegQueueThresholdSet(argc, argv, Tha60210012ModuleMpegVcgQueueMinThresholdSet);
    }

eBool CmdVcgQueueMaxThresholdSet(char argc, char** argv)
    {
    return MpegQueueThresholdSet(argc, argv, Tha60210012ModuleMpegVcgQueueMaxThresholdSet);
    }

eBool CmdVcgQueuePrioritySet(char argc, char** argv)
    {
    eBool convertSuccess;
    eThaMpegQueuePriority priority;

    priority = CliStringToEnum(argv[1], cThaQueuePriorityStr, cThaMpegQueuePriorityValue, mCount(cThaMpegQueuePriorityValue), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid priority value, expected: ");
        return cAtFalse;
        }

    return MpegQueueAttributeSet(argc, argv, Tha60210012ModuleMpegQueueVcgPrioritySet, priority);
    }

eBool CmdVcgQueueQuantumSet(char argc, char** argv)
    {
    return MpegQueueAttributeSet(argc, argv, Tha60210012ModuleMpegVcgQueueQuantumSet, AtStrToDw(argv[1]));
    }

eBool CmdVcgQueueShow(char argc, char** argv)
    {
    uint32 vcg_i, numVcg;
    uint32 *pListVcgs, bufferSize;
    eBool convertSuccess;
    tTab *tabPtr;
    const char *heading[] = {"VCG ID",
                             "Enable",
                             "MaxIntThreshold(pkts)",
                             "MaxExtThreshold(pkts)",
                             "MinIntThreshold(pkts)",
                             "MinExtlThreshold(pkts)",
                             "Priority",
                             "QuantumInByte"};
    AtUnused(argc);

    pListVcgs = CliSharedIdBufferGet(&bufferSize);
    numVcg = CliIdListFromString(argv[0], pListVcgs, bufferSize);
    if (numVcg == 0)
        {
        AtPrintc(cSevCritical, "ERROR: VCG list may be invalid or no created vcg\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numVcg, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot alloc table\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (vcg_i = 0; vcg_i < numVcg; vcg_i++)
        {
        const char *string;
        uint32 value;
        uint8 column = 0;

        StrToCell(tabPtr, vcg_i, column++, CliNumber2String(pListVcgs[vcg_i], "%u"));

        string = CliBoolToString(Tha60210012ModuleMpegVcgQueueIsEnabled(MpegModule(), pListVcgs[vcg_i] - 1));
        StrToCell(tabPtr, vcg_i, column++, string);

        value = Tha60210012ModuleMpegVcgQueueMaxInternalThresholdGet(MpegModule(), pListVcgs[vcg_i] - 1);
        StrToCell(tabPtr, vcg_i, column++, CliNumber2String(value, "%u"));

        value = Tha60210012ModuleMpegVcgQueueMaxExternalThresholdGet(MpegModule(), pListVcgs[vcg_i] - 1) * 128;
        StrToCell(tabPtr, vcg_i, column++, CliNumber2String(value, "%u"));

        value = Tha60210012ModuleMpegVcgQueueMinInternalThresholdGet(MpegModule(), pListVcgs[vcg_i] - 1);
        StrToCell(tabPtr, vcg_i, column++, CliNumber2String(value, "%u"));

        value = Tha60210012ModuleMpegVcgQueueMinExternalThresholdGet(MpegModule(), pListVcgs[vcg_i] - 1) * 128;
        StrToCell(tabPtr, vcg_i, column++, CliNumber2String(value, "%u"));

        string = CliEnumToString(Tha60210012ModuleMpegVcgQueuePriorityGet(MpegModule(), pListVcgs[vcg_i] - 1) ,
                                 cThaQueuePriorityStr,
                                 cThaMpegQueuePriorityValue,
                                 mCount(cThaMpegQueuePriorityValue),
                                 &convertSuccess);
        ColorStrToCell(tabPtr, vcg_i, column++, convertSuccess ? string : "unknown", convertSuccess ? cSevInfo : cSevWarning);

        value = Tha60210012ModuleMpegVcgQueueQuantumGet(MpegModule(), pListVcgs[vcg_i] - 1);
        StrToCell(tabPtr, vcg_i, column++, CliNumber2String(value, "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdLinkQueueMinThresholdSet(char argc, char **argv)
    {
    return LinkQueueThresholdSet(argc, argv, Tha60210012ModuleMpegLinkQueueMinThresholdSet);
    }

eBool CmdLinkQueueMaxThresholdSet(char argc, char **argv)
    {
    return LinkQueueThresholdSet(argc, argv, Tha60210012ModuleMpegLinkQueueMaxThresholdSet);
    }

eBool CmdLinkQueuePrioritySet(char argc, char** argv)
    {
    eAtRet ret;
    uint32 numLink, link_i;
    uint32 *pListLinks, bufferSize;
    eBool success = cAtTrue;
    eBool convertSuccess;
    uint32 queueId;
    eThaMpegQueuePriority priority;
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    queueId = AtStrToDw(argv[1]) - 1;
    priority = CliStringToEnum(argv[2], cThaQueuePriorityStr, cThaMpegQueuePriorityValue, mCount(cThaMpegQueuePriorityValue), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid priority value, expected: ");
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLink; link_i++)
        {
        ret = Tha60210012ModuleMpegLinkQueuePrioritySet(MpegModule(), pListLinks[link_i] - 1, queueId, priority);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for link: %d, ret=%s", pListLinks[link_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdLinkQueueQuantumSet(char argc, char** argv)
    {
    eAtRet ret;
    uint32 numLink, link_i;
    uint32 *pListLinks, bufferSize;
    uint32 quantum;
    eBool success = cAtTrue;
    uint32 queueId;
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    queueId = AtStrToDw(argv[1]) - 1;
    quantum = AtStrToDw(argv[2]);
    for (link_i = 0; link_i < numLink; link_i++)
        {
        ret = Tha60210012ModuleMpegLinkQueueQuantumSet(MpegModule(), pListLinks[link_i] - 1, queueId, quantum);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for link: %d, ret=%s", pListLinks[link_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdLinkQueueShow(char argc, char** argv)
    {
    uint32 link_i, numLink, row_i;
    uint32 numQueue, queue_i;
    uint32 *pListLinks, bufferSize;
    uint32 *pQueuesList;
    eBool convertSuccess;
    tTab *tabPtr;
    const char *heading[] = {"Link ID",
                             "Queue Id",
                             "Enable",
                             "MaxIntThreshold(pkts)",
                             "MaxExtThreshold(pkts)",
                             "MinIntThreshold(pkts)",
                             "MinExtThreshold(pkts)",
                             "Priority",
                             "QuantumInByte"};
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize / 2);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    pQueuesList = &(pListLinks[bufferSize / 2]);
    numQueue = CliIdListFromString(argv[1], pQueuesList, bufferSize / 2);
    if (numQueue == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Queue ID list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numLink * numQueue, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot alloc table\r\n");
        return cAtFalse;
        }

    /* Call API */
    row_i = 0;
    for (link_i = 0; link_i < numLink; link_i++)
        {
        for (queue_i = 0; queue_i < numQueue; queue_i++)
            {
            const char *string;
            uint32 value;
            uint8 column = 0;
            uint32 queueId = pQueuesList[queue_i] - 1;

            StrToCell(tabPtr, row_i, column++, CliNumber2String(pListLinks[link_i], "%u"));
            StrToCell(tabPtr, row_i, column++, CliNumber2String(pQueuesList[queue_i], "%u"));

            string = CliBoolToString(Tha60210012ModuleMpegLinkQueueIsEnabled(MpegModule(), pListLinks[link_i] - 1, queueId));
            StrToCell(tabPtr, row_i, column++, string);

            value = Tha60210012ModuleMpegLinkQueueMaxInternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId);
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            value = Tha60210012ModuleMpegLinkQueueMaxExternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId) * 128;
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            value = Tha60210012ModuleMpegLinkQueueMinInternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId);
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            value = Tha60210012ModuleMpegLinkQueueMinExternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId) * 128;
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            string = CliEnumToString(Tha60210012ModuleMpegLinkQueuePriorityGet(MpegModule(), pListLinks[link_i] - 1, queueId),
                                     cThaQueuePriorityStr,
                                     cThaMpegQueuePriorityValue,
                                     mCount(cThaMpegQueuePriorityValue),
                                     &convertSuccess);
            ColorStrToCell(tabPtr, row_i, column++, convertSuccess ? string : "unknown", convertSuccess ? cSevInfo : cSevWarning);

            value = Tha60210012ModuleMpegLinkQueueQuantumGet(MpegModule(), pListLinks[link_i] - 1, queueId);
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));
            row_i = row_i + 1;
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdMpBundleQueueEnable(char argc, char **argv)
    {
    return MpegQueueAttributeSet(argc, argv, (MpegQueueAttributeSetFunc)Tha60210012ModuleMpegBundleQueueEnable, cAtTrue);
    }

eBool CmdMpBundleQueueDisable(char argc, char **argv)
    {
    return MpegQueueAttributeSet(argc, argv, (MpegQueueAttributeSetFunc)Tha60210012ModuleMpegBundleQueueEnable, cAtFalse);
    }

eBool CmdVcgQueueEnable(char argc, char **argv)
    {
    return MpegQueueAttributeSet(argc, argv, (MpegQueueAttributeSetFunc)Tha60210012ModuleMpegVcgQueueEnable, cAtTrue);
    }

eBool CmdVcgQueueDisable(char argc, char **argv)
    {
    return MpegQueueAttributeSet(argc, argv, (MpegQueueAttributeSetFunc)Tha60210012ModuleMpegVcgQueueEnable, cAtFalse);
    }

eBool CmdLinkQueueEnable(char argc, char **argv)
    {
    return PppLinkQueueEnable(argc, argv, cAtTrue, Tha60210012ModuleMpegLinkQueueEnable);
    }

eBool CmdLinkQueueDisable(char argc, char **argv)
    {
    return PppLinkQueueEnable(argc, argv, cAtFalse, Tha60210012ModuleMpegLinkQueueEnable);
    }

eBool CmdMpegQueueCounterShow(char argc, char** argv)
    {
    tTab *tabPtr;
    eAtHistoryReadingMode readingMode;
    uint32 numOfRows, row_i, counterVal;
    const char *heading[] = {"Counters", "Values"};
    AtUnused(argc);

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    numOfRows = mCount(cMpegQueueCounterString);
    tabPtr = TableAlloc(numOfRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    for (row_i = 0; row_i < numOfRows; row_i++)
        ColorStrToCell(tabPtr, row_i, 0, cMpegQueueCounterString[row_i], ((row_i == 1) || (row_i == 6)) ? cSevDebug : cSevNormal);

    for (row_i = 0; row_i < numOfRows; row_i++)
        {
        counterVal = Tha60210012ModuleMpegDebugCounterGet(MpegModule(), row_i, (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse);

        if ((row_i == 1) || (row_i == 6))
            ColorStrToCell(tabPtr, row_i, 1, CliNumber2String(counterVal, "%u"), (counterVal > 0) ? cSevCritical : cSevInfo);
        ColorStrToCell(tabPtr, row_i, 1, CliNumber2String(counterVal, "%u"), cSevInfo);
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdMpegModuleDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cThaModuleMpeg));
    return cAtTrue;
    }

eBool CmdEthPortQueueMinThresholdSet(char argc, char **argv)
    {
    return LinkQueueThresholdSet(argc, argv, Tha60210012ModuleMpegEthBypassQueueMinThresholdSet);
    }

eBool CmdEthPortQueueMaxThresholdSet(char argc, char **argv)
    {
    return LinkQueueThresholdSet(argc, argv, Tha60210012ModuleMpegEthBypassQueueMaxThresholdSet);
    }

eBool CmdEthPortQueuePrioritySet(char argc, char** argv)
    {
    eAtRet ret;
    uint32 numLink, link_i;
    uint32 *pListLinks, bufferSize;
    eBool success = cAtTrue;
    eBool convertSuccess;
    uint32 queueId;
    eThaMpegQueuePriority priority;
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    queueId = AtStrToDw(argv[1]) - 1;
    priority = CliStringToEnum(argv[2], cThaQueuePriorityStr, cThaMpegQueuePriorityValue, mCount(cThaMpegQueuePriorityValue), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid priority value, expected: ");
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLink; link_i++)
        {
        ret = Tha60210012ModuleMpegEthBypassQueuePrioritySet(MpegModule(), pListLinks[link_i] - 1, queueId, priority);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for link: %d, ret=%s", pListLinks[link_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEthPortQueueQuantumSet(char argc, char** argv)
    {
    eAtRet ret;
    uint32 numLink, link_i;
    uint32 *pListLinks, bufferSize;
    uint32 quantum;
    eBool success = cAtTrue;
    uint32 queueId;
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    queueId = AtStrToDw(argv[1]) - 1;
    quantum = AtStrToDw(argv[2]);
    for (link_i = 0; link_i < numLink; link_i++)
        {
        ret = Tha60210012ModuleMpegEthByPassQueueQuantumSet(MpegModule(), pListLinks[link_i] - 1, queueId, quantum);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure threshold for link: %d, ret=%s", pListLinks[link_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEthPortQueueShow(char argc, char** argv)
    {
    uint32 link_i, numLink, row_i;
    uint32 numQueue, queue_i;
    uint32 *pListLinks, bufferSize;
    uint32 *pQueuesList;
    eBool convertSuccess;
    tTab *tabPtr;
    const char *heading[] = {"Port ID",
                             "Queue Id",
                             "Enable",
                             "MaxIntThreshold(pkts)",
                             "MaxExtThreshold(pkts)",
                             "MinIntThreshold(pkts)",
                             "MinExtThreshold(pkts)",
                             "Priority",
                             "Quantum(bytes)"};
    AtUnused(argc);

    pListLinks = CliSharedIdBufferGet(&bufferSize);
    numLink = CliIdListFromString(argv[0], pListLinks, bufferSize / 2);
    if (numLink == 0)
        {
        AtPrintc(cSevCritical, "ERROR: PPP Link list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    pQueuesList = &(pListLinks[bufferSize / 2]);
    numQueue = CliIdListFromString(argv[1], pQueuesList, bufferSize / 2);
    if (numQueue == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Queue ID list may be invalid or no created link\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numLink * numQueue, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot alloc table\r\n");
        return cAtFalse;
        }

    /* Call API */
    row_i = 0;
    for (link_i = 0; link_i < numLink; link_i++)
        {
        for (queue_i = 0; queue_i < numQueue; queue_i++)
            {
            const char *string;
            uint32 value;
            uint8 column = 0;
            uint32 queueId = pQueuesList[queue_i] - 1;

            StrToCell(tabPtr, row_i, column++, CliNumber2String(pListLinks[link_i], "%u"));
            StrToCell(tabPtr, row_i, column++, CliNumber2String(pQueuesList[queue_i], "%u"));

            string = CliBoolToString(Tha60210012ModuleMpegEthBypassQueueIsEnabled(MpegModule(), pListLinks[link_i] - 1, queueId));
            StrToCell(tabPtr, row_i, column++, string);

            value = Tha60210012ModuleMpegEthBypassQueueMaxInternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId);
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            value = Tha60210012ModuleMpegEthBypassQueueMaxExternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId) * 128;
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            value = Tha60210012ModuleMpegEthBypassQueueMinInternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId);
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            value = Tha60210012ModuleMpegEthBypassQueueMinExternalThresholdGet(MpegModule(), pListLinks[link_i] - 1, queueId) * 128;
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));

            string = CliEnumToString(Tha60210012ModuleMpegEthBypassQueuePriorityGet(MpegModule(), pListLinks[link_i] - 1, queueId),
                                     cThaQueuePriorityStr,
                                     cThaMpegQueuePriorityValue,
                                     mCount(cThaMpegQueuePriorityValue),
                                     &convertSuccess);
            ColorStrToCell(tabPtr, row_i, column++, convertSuccess ? string : "unknown", convertSuccess ? cSevInfo : cSevWarning);

            value = Tha60210012ModuleMpegEthBypassQueueQuantumGet(MpegModule(), pListLinks[link_i] - 1, queueId);
            StrToCell(tabPtr, row_i, column++, CliNumber2String(value, "%u"));
            row_i = row_i + 1;
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEthPortQueueEnable(char argc, char **argv)
    {
    return PppLinkQueueEnable(argc, argv, cAtTrue, Tha60210012ModuleMpegEthBypassQueueEnable);
    }

eBool CmdEthPortQueueDisable(char argc, char **argv)
    {
    return PppLinkQueueEnable(argc, argv, cAtFalse, Tha60210012ModuleMpegEthBypassQueueEnable);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaModuleSdh.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/default/sdh/ThaSdhVc.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtModuleVal[] = {
                                      cAtModuleSdh,
                                      cAtModuleSur,
									  cThaModulePmc
                                    };
static const char *cAtModuleStr[] = {
                                     "sdh",
                                     "sur",
									 "pmc"
};
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SdhPathIsVc(AtSdhChannel channel)
    {
    eAtSdhChannelType type = AtSdhChannelTypeGet(channel);
    if ((type == cAtSdhChannelTypeVc12)   ||
        (type == cAtSdhChannelTypeVc11)   ||
        (type == cAtSdhChannelTypeVc4)    ||
        (type == cAtSdhChannelTypeVc3)    ||
        (type == cAtSdhChannelTypeVc4_4c) ||
        (type == cAtSdhChannelTypeVc4_16c))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SdhPathCdrDebug(char argc, char** argv, void (*DebugFunc)(ThaCdrController))
    {
    AtChannel *channel;
    uint32 numChannels, i;
    AtChannel aChannel;

    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        aChannel = channel[i];
        if ((aChannel == NULL) || !SdhPathIsVc((AtSdhChannel)aChannel))
            continue;

        DebugFunc(ThaSdhVcCdrControllerGet((AtSdhVc)aChannel));
        }

    return cAtTrue;
    }

eBool CmdSdhPathCdrDebug(char argc, char** argv)
    {
    return SdhPathCdrDebug(argc, argv, ThaCdrControllerDebug);
    }

eBool CmdThaModuleSdhDebugCountersModule(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 moduleId;
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    eAtRet ret;

    if (argc == 0)
        {
        const char *moduleName;

        moduleId = ThaModuleSdhDebugCountersModuleGet(sdhModule);
        moduleName = CliEnumToString(moduleId, cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), NULL);
        AtPrintc(cSevInfo, "Counter module: %s\r\n", moduleName);
        return cAtTrue;
        }

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModuleSdhDebugCountersModuleSet(sdhModule, moduleId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

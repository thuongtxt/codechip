/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliThaPdhDebug.c
 *
 * Created Date: Nov 11, 2013
 *
 * Description : CLIs debug for THALASSA PDH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtPdhChannel.h"
#include "AtPdhDe1.h"
#include "AtSdhVc.h"
#include "../../../../driver/src/implement/default/pdh/ThaModulePdhReg.h"
#include "../../../../driver/src/implement/default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PdhDe1AutoAisIsEnabled(AtPdhDe1 de1)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + ThaPdhDe1DefaultOffset((ThaPdhDe1)de1);
    uint32 regVal  = mChannelHwRead(de1, address, cAtModulePdh);
    return (regVal & cThaTxDE1AutoAisMask) ? cAtTrue : cAtFalse;
    }

eBool CmdPdhDe1AutoAisRdiGet(char argc, char **argv)
    {
    /* Declare variables */
    uint32  numChannels, i;
    tTab   *tabPtr;
    char buff[64];
    AtChannel *channelList;
    const char *pHeading[] = {"DE1 ID", "AIS", "RAI"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        AtPdhDe1 de1 = (AtPdhDe1)channelList[i];
        uint8 column;

        /* Get ID */
        column = 0;
        AtSprintf(buff, "%s", CliChannelIdStringGet((AtChannel)channelList[i]));
        StrToCell(tabPtr, i, column++, buff);

        AtSprintf(buff, "%s", (PdhDe1AutoAisIsEnabled(de1) ? "en" : "dis"));
        StrToCell(tabPtr, i, column++, buff);

        AtSprintf(buff, "%s", (AtPdhChannelAutoRaiIsEnabled((AtPdhChannel)de1) ? "en" : "dis"));
        StrToCell(tabPtr, i, column++, buff);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : CliThaModuleConcate.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtDebug.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/concate/Tha60210012ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cThaConcateDumpModeVal[] = { 0, 1 };
static const char *cThaConcateDumpModeStr[] = { "full", "frame" };

static const uint32 cThaConcateSelBusVal[] = { 0, 1, 2, 3 };
static const char *cThaConcateSelBusStr[] = { "pdhfifoin", "pdhfifoout", "sinkpdh", "encap" };

static const uint32 cThaConcateDdrDumpSelVal[] = { 0, 1};
static const char *cThaConcateDdrDumpSelStr[] = { "vcat2pla", "pla2vcat"};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdModuleModuleConcateDumpData(char argc, char **argv)
    {
    eBool convertSuccess;
    uint8 dumpMode, selBus;

    AtUnused(argc);

    dumpMode = (uint8)CliStringToEnum(argv[0], cThaConcateDumpModeStr, cThaConcateDumpModeVal, mCount(cThaConcateDumpModeVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown dump mode %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cThaConcateDumpModeStr, mCount(cThaConcateDumpModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    selBus = (uint8)CliStringToEnum(argv[1], cThaConcateSelBusStr, cThaConcateSelBusVal, mCount(cThaConcateSelBusVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown bus  %s, expect: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cThaConcateSelBusStr, mCount(cThaConcateSelBusVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    Tha60210012ModuleConcateDataDump(CliAtConcateModule(), dumpMode, selBus);
    return cAtTrue;
    }

eBool CmdConcateModuleModuleDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    AtModuleDebug((AtModule) CliAtConcateModule());
    return cAtTrue;
    }

eBool CmdModuleModuleConcateDdrDump(char argc, char **argv)
    {
    eBool convertSuccess;
    uint32 lineId;
    uint8 selBus;

    AtUnused(argc);
    lineId = (uint32)AtStrtoul(argv[0], NULL, 10);
    selBus = (uint8)CliStringToEnum(argv[1], cThaConcateDdrDumpSelStr, cThaConcateDdrDumpSelVal, mCount(cThaConcateDdrDumpSelVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown dump select %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cThaConcateDdrDumpSelStr, mCount(cThaConcateDdrDumpSelVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    Tha60210012ModuleConcateDdrDump(CliAtConcateModule(), lineId, selBus);
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module     : SUR
 *
 * File        : CliThaSurDebug.c
 *
 * Created Date: Sep 16, 2015
 *
 * Description: Debug CLI implement for Surveillance module.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../sur/CliAtModuleSur.h"
#include "../../../../driver/src/implement/default/sur/hard/ThaModuleHardSurReg.h"
#include "../../../../driver/src/implement/default/sur/hard/ThaModuleHardSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 eAtModuleSurCounterTypeVal[] = {cThaModuleSurCounterStsParams,
                                                    cThaModuleSurCounterVtParams,
                                                    cThaModuleSurCounterDe3Params,
                                                    cThaModuleSurCounterDe1Params,
                                                    cThaModuleSurCounterPwParams,
                                                    cThaModuleSurCounterPwStatistic};
static const char* eAtModuleSurCounterTypeStr[] = {"sts", "vt", "de3", "de1", "pw", "pw-counters"};

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

eBool CmdSurDdrRead(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint8 page;
    uint32 counterType, address;
    uint32* value;
    eBool result = cAtTrue;
    eAtHistoryReadingMode readingMode;
    uint32 holdId;
    uint32 maxHold = ThaModuleHardSurNumberDdrHoldRegisters((ThaModuleHardSur)CliModuleSur());
    AtUnused(argc);

    /* Get page number */
    page = (uint8)AtStrToDw(argv[0]);

    /* Get counter type. */
    mAtStrToEnum(eAtModuleSurCounterTypeStr, eAtModuleSurCounterTypeVal, argv[1], counterType, result);
    if (!result)
        return cAtFalse;

    /* Get address in HEX. */
    address = AtStrtoul(argv[2], NULL, 16);

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[3]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    value = AtOsalMemAlloc(sizeof(uint32) * maxHold);
    if (!value)
        {
        return cAtFalse;
        }

    ret = ThaModuleHardSurDdrRead2Clear((ThaModuleHardSur)CliModuleSur(), page, (uint8)counterType, address, value, (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Read fail with error = %s\r\n", AtRet2String(ret));
        AtOsalMemFree(value);
        return cAtFalse;
        }

    AtPrintc(cSevNormal, "SUR DDR Read at page-%d type-%d address-0x%x\r\nValue: ", page, counterType, address);
    for (holdId = 0; holdId < maxHold; holdId++)
        {
        AtPrintc(cSevNormal, "0x%x.", value[holdId]);
        }
    AtPrintc(cSevNormal, "\r\n");

    AtOsalMemFree(value);
    return cAtTrue;
    }

eBool CmdAtModuleSurActivate(char argc, char **argv)
    {
    eAtRet ret = AtModuleActivate((AtModule)CliModuleSur());
    AtUnused(argc);
    AtUnused(argv);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot activate Surveillance module, error: %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModuleSurDeActivate(char argc, char **argv)
    {
    eAtRet ret = AtModuleDeactivate((AtModule)CliModuleSur());
    AtUnused(argc);
    AtUnused(argv);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot deactivate Surveillance module, error: %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliThaModulePmc.c
 *
 * Created Date: Dec 15, 2016
 *
 * Description : Debug CLI for PMC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/codechip/Tha60290011/pmc/Tha60290011ModulePmc.h"
#include "../../../../driver/src/implement/codechip/Tha60290081/pmc/Tha60290081ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#define mPutCounterToCell(row, col, value)									\
	{																		\
	AtSprintf(buf, "%u", value);										    \
	StrToCell(tabPtr, row, col, buf);									    \
	}
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char* cPmcGlobalCounterString[] = {"MAC -> CLA(port0)",
                                                "MAC -> CLA(port1)",
                                                "CLA -> PDA(port0)",
                                                "CLA -> PDA(port1)",
                                                "PDA -> ENC#0",
                                                "PDA -> ENC#1",
                                                "PDA -> ENC#2",
                                                "PDA -> ENC#3",
                                                "PLA -> PKA#0",
                                                "PLA -> PKA#1",
                                                "PKA#0 -> PW#0",
                                                "PKA#1 -> PW#1",
                                                "PW#0 -> ETH",
                                                "PW#1 -> ETH",
                                                "PDA -> ETH_PASS_1G",
                                                "PDA -> ETH_PASS_10G"};

static const char* cPmcGlobalCounterIdString[] = {"vld_counters",
                                                  "sop_counters",
                                                  "eop_counters",
                                                  "err_counters",
                                                  "byte_counters"};

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char* cThaModuelPmcTickModeStr[]    = {"auto", "manual"};
static const uint32 cThaModuelPmcTickModeValue[] = {cThaModulePmcTickModeAuto, cThaModulePmcTickModeManual};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePmc PmcModule(void)
    {
    return (ThaModulePmc)AtDeviceModuleGet(CliDevice(), cThaModulePmc);
    }

eBool CmdDebugModulePmcClaCounter(char argc, char **argv)
    {
	uint8 i, column;
    tTab *tabPtr;
    eBool r2c;
    static char buf[32];
    const char *heading[] = {"sliceId", "rxPkt", "de1Bytes", "de3Bytes", "ctrlPkt",
                             "de1GoodPkt", "de3GoodPkt", "ctrlGoodPkt",
                             "DAErrPkt", "SAErrPkt", "errEthTypePkt", "errLenPkt",
                             "errSeqPkt", "errLenFieldPkt", "FcsErr"};
    AtUnused(argc);

    /* Create table with titles */
    tabPtr = TableAlloc(2, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }
    r2c = ((CliHistoryReadingModeGet(argv[0]) == cAtHistoryReadingModeReadOnly) ? cAtFalse : cAtTrue);

    /* Put counters */
    for (i = 0; i < 2; i++)
		{
		column = 0;
		mPutCounterToCell(i, column++, i);
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaRxPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxDe1BytesGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxDe3BytesGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxControlPacketBytesGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxDe1GoodPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxDe3GoodPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxControlGoodPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxDAErrPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxSAErrPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxEthTypeErrPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxLenErrPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxSequenceErrPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxLenFieldErrPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcClaTxFcsErrPacketsGet(PmcModule(), i, r2c));
		}

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDebugModulePmcEthPdhMuxCounter(char argc, char **argv)
    {
	uint8 i, column;
    tTab *tabPtr;
    eBool r2c;
    static char buf[32];
    const char *heading[] = {" ", "DE1 bus", "DE3 bus", "Control bus"};
    AtUnused(argc);

    /* Create table with titles */
    tabPtr = TableAlloc(2, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }
    r2c = ((CliHistoryReadingModeGet(argv[0]) == cAtHistoryReadingModeReadOnly) ? cAtFalse : cAtTrue);

    StrToCell(tabPtr, 0, 0, "Packets");
    StrToCell(tabPtr, 1, 0, "Bytes");

    /* Put counters */
    column = 1;
    for (i = 0; i < 3; i++)
		{
		mPutCounterToCell(0, column, Tha60290011ModulePmcPdhMuxPacketsGet(PmcModule(), i, r2c));
		mPutCounterToCell(1, column++, Tha60290011ModulePmcPdhMuxBytesGet(PmcModule(), i, r2c));
		}

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDebugModulePmcPdhMuxCounter(char argc, char **argv)
    {
	uint8 i, column;
    tTab *tabPtr;
    eBool r2c;
    static char buf[32];
    const char *heading[] = {"ChannelId", "DE1 bit", "DE3 bit"};
    uint32 bufferSize, numItem, *idList;
    AtUnused(argc);

	idList = CliSharedIdBufferGet(&bufferSize);
	numItem = CliIdListFromString(argv[0], idList, bufferSize);

	r2c = ((CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadOnly) ? cAtFalse : cAtTrue);

    /* Create table with titles */
    tabPtr = TableAlloc(numItem, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Put counters */
    for (i = 0; i < numItem; i++)
		{
    	column = 0;
    	mPutCounterToCell(i, column++, idList[i]);
		mPutCounterToCell(i, column++, Tha60290011ModulePmcPdhDe1BitGet(PmcModule(), idList[i], r2c));
		mPutCounterToCell(i, column++, Tha60290011ModulePmcPdhDe3BitGet(PmcModule(), idList[i], r2c));
		}

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdThaModulePmcDebugTickModeSet(char argc, char** argv)
    {
    eBool success;
    eThaModulePmcTickMode mode;
    eAtRet ret;

    AtUnused(argc);

    mode = CliStringToEnum(argv[0], cThaModuelPmcTickModeStr, cThaModuelPmcTickModeValue, mCount(cThaModuelPmcTickModeStr), &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: PMC Tick mode %s may be invalid or not supported. Expected: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cThaModuelPmcTickModeStr, mCount(cThaModuelPmcTickModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModulePmcTickModeSet(PmcModule(), mode);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdThaModulePmcDebugTickSet(char argc, char** argv)
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = ThaModulePmcTick(PmcModule());
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdThaModulePmcShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"Attribute", "Value"};
    static const uint32 cNumRows = 1;
    uint32 rowId = 0;
    const char *string;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(cNumRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    string = CliEnumToString(ThaModulePmcTickModeGet(PmcModule()),
                             cThaModuelPmcTickModeStr,
                             cThaModuelPmcTickModeValue,
                             mCount(cThaModuelPmcTickModeValue),
                             NULL);

    StrToCell(tabPtr, rowId, 0, "TickMode");
    StrToCell(tabPtr, rowId, 1, string ? string : "error");
    rowId = rowId + 1;

    AtAssert(rowId == cNumRows); /* Will help when new rows are manually added */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPmcGlobalCounterGet(char argc, char** argv)
    {
    tTab *tabPtr;
    uint8 column = 0;
    eAtHistoryReadingMode readingMode;
    uint32 numOfRows, row_i, counterVal;
    uint32 numCounterIds, cntId;
    AtModule pmcModule = (AtModule)AtDeviceModuleGet(CliDevice(), cThaModulePwPmc);
    const char *heading[] = {"Counters", "vld_counters", "sop_counters", "eop_counters", "err_counters", "byte_counters"};
    AtUnused(argc);

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    numCounterIds = mCount(cPmcGlobalCounterIdString);
    numOfRows = mCount(cPmcGlobalCounterString);
    tabPtr = TableAlloc(numOfRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    for (row_i = 0; row_i < numOfRows; row_i++)
        ColorStrToCell(tabPtr, row_i, 0, cPmcGlobalCounterString[row_i], cSevNormal);

    for (row_i = 0; row_i < numOfRows; row_i++)
        {
        column = 1;
        for (cntId = 0; cntId < numCounterIds; cntId++)
            {
            counterVal = Tha60290081ModulePmcGlobalCounterGet(pmcModule, row_i, cntId, readingMode);

            ColorStrToCell(tabPtr, row_i, column, CliNumber2String(counterVal, "%u"), cSevInfo);
            column = (uint8)(column + 1);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

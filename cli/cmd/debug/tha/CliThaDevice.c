/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Management
 *
 * File        : CliThaDevice.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/eth/Tha60210012ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cpmc_glbcnt_pen_base 0x00100

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaDeviceDebugPacketCounterType
    {
    cThaDeviceDebugPacketCounterInv,
    cThaDeviceDebugPacketCounterSop,
    cThaDeviceDebugPacketCounterEop,
    cThaDeviceDebugPacketCounterErr,
    cThaDeviceDebugPacketCounterNob
    }eThaDeviceDebugPacketCounterType;

typedef enum eThaDeviceDebugBlockCounterType
    {
    cThaDeviceDebugBlockCounterTypeInputCla,
    cThaDeviceDebugBlockCounterTypeOutputCla,
    cThaDeviceDebugBlockCounterTypeInputFramming,
    cThaDeviceDebugBlockCounterTypeOutputFramming,
    cThaDeviceDebugBlockCounterTypeInputPda,
    cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc0,
    cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc1,
    cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc2,
    cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc3,
    cThaDeviceDebugBlockCounterTypeOutputPwe,
    cThaDeviceDebugBlockCounterTypeOutputMuxToOOBFC,
    cThaDeviceDebugBlockCounterTypeOutputMuxToPwe,
    cThaDeviceDebugBlockCounterTypeOutputMuxToXfi
    }eThaDeviceDebugBlockCounterType;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(uint32 blockType, uint32 counterType, eBool r2c)
    {
    return ((blockType * 16UL) + (uint32)(r2c * 8) + counterType);
    }

static uint32 CounterGet(AtHal hal, uint32 blockType, uint32 counterType, eBool r2c)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    uint32 baseaddress = Tha60210012ModuleEthPMCInternalBaseAddress(ethModule);
    uint32 address = (baseaddress + cpmc_glbcnt_pen_base + CounterOffset(blockType, counterType, r2c));
    uint32 counter = AtHalRead(hal, address);

    return counter;
    }

eBool CmdMapModuleDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cThaModuleMap));
    return cAtTrue;
    }

eBool CmdClaModuleDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cThaModuleCla));
    return cAtTrue;
    }

eBool CmdDeviceCounterDebug(char argc, char **argv)
    {
    uint32 numRows, i;
    tTab *tabPtr;
    AtHal hal;
    eBool r2c;
    eAtHistoryReadingMode readMode;
    const char *blockTypeStr[] =
        {
        "InputCLA",
        "OutputCLA",
        "InputFramming",
        "OutputFramming",
        "InputPDA",
        "OutputPDA_To_Lo-ENC#0",
        "OutputPDA_To_Lo-ENC#1",
        "OutputPDA_To_Lo-ENC#2",
        "OutputPDA_To_Lo-ENC#3",
        "OutputPWE",
        "OutputMUX_To_OOBFC",
        "OutputMUX_To_PWE",
        "OutputMUX_To_XFI"
        };

    const uint32 blockTypeVal[] =
        {
         cThaDeviceDebugBlockCounterTypeInputCla,
         cThaDeviceDebugBlockCounterTypeOutputCla,
         cThaDeviceDebugBlockCounterTypeInputFramming,
         cThaDeviceDebugBlockCounterTypeOutputFramming,
         cThaDeviceDebugBlockCounterTypeInputPda,
         cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc0,
         cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc1,
         cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc2,
         cThaDeviceDebugBlockCounterTypeOutputPdaToLoEnc3,
         cThaDeviceDebugBlockCounterTypeOutputPwe,
         cThaDeviceDebugBlockCounterTypeOutputMuxToOOBFC,
         cThaDeviceDebugBlockCounterTypeOutputMuxToPwe,
         cThaDeviceDebugBlockCounterTypeOutputMuxToXfi
        };

    const char *heading[] =
        {
         "Block Type", "Packet Valid", "Packet Sop", "Packet Eop", "Packet Err", "Packet Nob",
        };


    AtUnused(argc);
    readMode = CliHistoryReadingModeGet(argv[0]);
    if (readMode == cAtHistoryReadingModeUnknown)
        return cAtFalse;

    /* Create table with titles */
    numRows = mCount(blockTypeStr);
    tabPtr = TableAlloc(numRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    r2c =  (CliHistoryReadingModeGet(argv[0])  == cAtHistoryReadingModeReadToClear) ? cAtTrue: cAtFalse;

    /* Make table content */
    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    for (i = 0; i < numRows; i++)
        {
        uint32 counterValue;
        uint32 colum = 0;
        StrToCell(tabPtr, i, colum++, blockTypeStr[i]);

        counterValue = CounterGet(hal, blockTypeVal[i], cThaDeviceDebugPacketCounterInv, r2c);
        CliCounterPrintToCell(tabPtr, i, colum++, counterValue, cAtCounterTypeGood, cAtTrue);

        counterValue = CounterGet(hal, blockTypeVal[i], cThaDeviceDebugPacketCounterSop, r2c);
        CliCounterPrintToCell(tabPtr, i, colum++, counterValue, cAtCounterTypeGood, cAtTrue);

        counterValue = CounterGet(hal, blockTypeVal[i], cThaDeviceDebugPacketCounterEop, r2c);
        CliCounterPrintToCell(tabPtr, i, colum++, counterValue, cAtCounterTypeGood, cAtTrue);

        counterValue = CounterGet(hal, blockTypeVal[i], cThaDeviceDebugPacketCounterErr, r2c);
        CliCounterPrintToCell(tabPtr, i, colum++, counterValue, cAtCounterTypeError, cAtTrue);

        counterValue = CounterGet(hal, blockTypeVal[i], cThaDeviceDebugPacketCounterNob, r2c);
        CliCounterPrintToCell(tabPtr, i, colum++, counterValue, cAtCounterTypeGood, cAtTrue);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

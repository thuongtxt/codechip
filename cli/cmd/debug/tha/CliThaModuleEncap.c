/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliThaModuleEncap.c
 *
 * Created Date: Jun 6, 2016
 *
 * Description : To debug ENC module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "../../encap/CliAtModuleEncap.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/encap/Tha60210012PhysicalHdlcChannel.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/encap/Tha60210012ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtModuleVal[] = {
                                      cAtModuleEncap,
                                      cAtModuleSur
                                     };

static const char *cAtModuleStr[] = {
                                     "encap",
                                     "sur"
                                    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdThaModuleEncapDebugCounterModule(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 moduleId;
    ThaModuleEncap module = (ThaModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    eAtRet ret;

    if (argc == 0)
        {
        const char *moduleName;

        moduleId = ThaModuleEncapDebugCountersModuleGet(module);
        moduleName = CliEnumToString(moduleId, cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), NULL);
        AtPrintc(cSevInfo, "Counter module: %s\r\n", moduleName);
        return cAtTrue;
        }

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModuleEncapDebugCountersModuleSet(module, moduleId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliThaModulePda.c
 *
 * Created Date: Jul 14, 2017
 *
 * Description : Debug module pda clis
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../util/CliAtDebugger.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule Module(void)
    {
    return AtDeviceModuleGet(CliDevice(), cThaModulePda);
    }

eBool CmdPdaModuleDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleDebug(Module());
    return cAtTrue;
    }

eBool CmdThaModulePdaDebuggerShow(char argc, char **argv)
    {
    AtDebugger debugger = AtModuleDebuggerCreate(Module());
    eBool success = CliAtDebuggerShow(debugger);
    AtUnused(argc);
    AtUnused(argv);
    AtObjectDelete((AtObject)debugger);
    return success;
    }


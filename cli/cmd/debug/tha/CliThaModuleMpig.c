/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPIG
 *
 * File        : CliThaModuleMpig.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/mpig/Tha60210012ModuleMpig.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*MpigQueueAttributeSetFunc)(Tha60210012ModuleMpig self, uint32 queueId, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char* cQueueCounterString[] = {
                                  "Enqueue Counters (packets)",
                                  "Total good packet enqueue",
                                  "Internal Packet bypass to Internal FiFo directly",
                                  "Internal Packet bypass to Internal FiFo from Tail FiFo",
                                  "External Packet to Internal FiFo",
                                  "Discard because Queue Disable CFG",
                                  "Discard because Input(PLA, write cache) declare drop",
                                  "Discard because Internal FiFo full",
                                  "Discard because External FiFo full",
                                  "Enqueue Counters (bytes)",
                                  "Total good byte enqueue",
                                  "Internal byte bypass to Internal FiFo directly",
                                  "Internal byte bypass to Internal FiFo from Tail FiFo",
                                  "External byte to Internal FiFo",
                                  "Discard because Queue Disable CFG",
                                  "Discard because Input(CLA, write cache) declare drop",
                                  "Discard because Internal FiFo full",
                                  "Discard because External FiFo full",
                                  "Dequeue Counters",
                                  "Total good packet dequeue (packets)",
                                  "Total good end of packet dequeue (packets)",
                                  "Total good byte dequeue (bytes)"
};

static const char * cThaQueuePriorityStr[] = {"highest", "medium", "lowest"};

static const uint32 cTha60210012MpigQueuePriority[] = {cThaMpigQueuePriorityHighest,
                                                       cThaMpigQueuePriorityMedium,
                                                       cThaMpigQueuePriorityLowest};

static const char* cMpigGlobalCounterString[] = {
                                                 "MPIG enqueue Good Packet",
                                                 "MPIG enqueue Drop Packet",
                                                 "MPIG dequeue Good Packet",
                                                 "MPIG request Write DDR",
                                                 "MPIG request Read DDR",
                                                 "MPIG Valid Write DDR",
                                                 "MPIG Valid Read DDR",
                                                 "MPIG ACK DDR",
                                                 "QMXFF Write get Valid",
                                                 "QMXFF Read get Valid(=3xMPIG request Read DDR)",
                                                 "QMXFF Read get first Valid",
                                                 "QMXFF Read get end Valid",
                                                 "Total Packet to Head internal FiFo",
                                                 "Packet Directly to head FiFo",
                                                 "Packet from tail FiFo to head FiFo",
                                                 "Total Discard"
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool MpigQueueAttributeSet(char argc, char** argv, MpigQueueAttributeSetFunc func, uint32 value)
    {
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numQueue, queue_i;
    eAtRet ret = cAtError;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argc);

    if (mpigModule == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: MPIG module is not supported\n");
        return cAtFalse;
        }

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numQueue = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "ERROR: Invalid parameter <queueId> , expected 1-%u\r\n", 8);
        return cAtFalse;
        }

    for (queue_i = 0; queue_i < numQueue; queue_i++)
        {
        ret = func(mpigModule, idBuf[queue_i] - 1, value);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "Queue %u cannot be set, ret = %s\r\n", idBuf[queue_i], AtRet2String(ret));
        }

    return cAtTrue;
    }

static void CounterPrint(tTab *tabPtr, uint32 numOfRows, uint32 queueId, tTha60210012MpigQueueCounter counters)
    {
    uint32 i;
    uint32 cntIdx;

    uint32 gapIdx0, gapIdx1, gapIdx2;

    gapIdx0 = 0;
    gapIdx1 = cTha60210012MpigPktEnqueueCounterNum + (gapIdx0 + 1);
    gapIdx2 = cTha60210012MpigByteEnqueueCountersNum + (gapIdx1 + 1);

    cntIdx = 0;
    for (i = 0; i < numOfRows; i++)
        {
        if ((i == gapIdx0) || (i == gapIdx1) || (i == gapIdx2))
            {
            cntIdx = 0;
            continue;
            }

        if (i < gapIdx1)
            ColorStrToCell(tabPtr,
                           i,
                           queueId + 1,
                           CliNumber2String(counters.pktEnqueueCnt[cntIdx], "%u"),
                           ((cntIdx > 3) && (counters.pktEnqueueCnt[cntIdx] > 0)) ? cSevCritical : cSevInfo);
        else if (i < gapIdx2)
            ColorStrToCell(tabPtr,
                           i,
                           queueId + 1,
                           CliNumber2String(counters.byteEnqueueCnt[cntIdx], "%u"),
                           ((cntIdx > 3) && (counters.byteEnqueueCnt[cntIdx] > 0)) ? cSevCritical : cSevInfo);
        else
            ColorStrToCell(tabPtr, i, queueId + 1, CliNumber2String(counters.byteDequeueCnt[cntIdx], "%u"), cSevInfo);


        cntIdx++;
        }
    }

eBool CmdMpigQueueThresholdSet(char argc, char** argv)
    {
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numQueue, queue_i;
    uint32 internalThreshold = 0;
    uint32 externalThreshold = 0;
    eAtRet ret = cAtError;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    static uint8 portId = 0;
    AtUnused(argc);

    if (mpigModule == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: MPIG module is not supported\n");
        return cAtFalse;
        }

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numQueue = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "ERROR: Invalid parameter <queueId> , expected 1-%u\r\n", 8);
        return cAtFalse;
        }

    internalThreshold = (uint32)AtStrToDw(argv[1]);
    externalThreshold = (uint32)AtStrToDw(argv[2]);

    for (queue_i = 0; queue_i < numQueue; queue_i++)
        {
        ret = Tha60210012ModuleMpigQueueMinThresholdSet(mpigModule, portId, idBuf[queue_i] - 1, internalThreshold, externalThreshold);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "Queue %u cannot be set, ret = %s\r\n", idBuf[queue_i], AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdMpigQueueEnable(char argc, char** argv)
    {
    return MpigQueueAttributeSet(argc, argv, (MpigQueueAttributeSetFunc)Tha60210012ModuleMpigQueueEnable, cAtTrue);
    }

eBool CmdMpigQueueDisable(char argc, char** argv)
    {
    return MpigQueueAttributeSet(argc, argv, (MpigQueueAttributeSetFunc)Tha60210012ModuleMpigQueueEnable, cAtFalse);
    }

eBool CmdMpigHybridQueueingEnable(char argc, char** argv)
    {
    return MpigQueueAttributeSet(argc, argv, (MpigQueueAttributeSetFunc)Tha60210012ModuleMpigHybridQueueingEnable, cAtTrue);
    }

eBool CmdMpigHybridQueueingDisable(char argc, char** argv)
    {
    return MpigQueueAttributeSet(argc, argv, (MpigQueueAttributeSetFunc)Tha60210012ModuleMpigHybridQueueingEnable, cAtFalse);
    }

eBool CmdMpigQueueQuantumSet(char argc, char** argv)
    {
    return MpigQueueAttributeSet(argc, argv, (MpigQueueAttributeSetFunc)Tha60210012ModuleMpigQueueQuantumSet, (uint32)AtStrToDw(argv[1]));
    }

eBool CmdMpigQueuePrioritySet(char argc, char** argv)
    {
    eThaMpigQueuePriority priority = cThaMpigQueuePriorityUnknown;
    eBool convertSucess = 0;

    priority = CliStringToEnum(argv[1], cThaQueuePriorityStr, cTha60210012MpigQueuePriority, mCount(cTha60210012MpigQueuePriority), &convertSucess);
    return MpigQueueAttributeSet(argc, argv, (MpigQueueAttributeSetFunc)Tha60210012ModuleMpigQueuePrioritySet, priority);
    }

eBool CmdMpigAllQueueConfigShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"Queue", "Enable", "Hybrib", "MinIntThreshold(pkts)", "MinExtlThreshold(pkts)", "QuantumInByte", "Priority"};
    uint32 queueId;
    uint32 internalThreshold;
    uint32 externalThreshold;
    eBool enable;
    eBool hybrib;
    uint32 quantumInByte;
    eThaMpigQueuePriority priority;
    const char *string;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    static uint8 portId = 0;
    AtUnused(argc);
    AtUnused(argv);

    if (mpigModule == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: MPIG module is not supported\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(8, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for(queueId = 0; queueId < 8; queueId++)
        {
        StrToCell(tabPtr, queueId, 0, CliNumber2String(queueId + 1, "%u"));

        enable = Tha60210012ModuleMpigQueueIsEnabled(mpigModule, portId, queueId);
        ColorStrToCell(tabPtr, queueId, 1, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);

        hybrib = Tha60210012ModuleMpigHybribQueueingIsEnabled(mpigModule, portId, queueId);
        ColorStrToCell(tabPtr, queueId, 2, CliBoolToString(hybrib), hybrib ? cSevInfo : cSevCritical);

        internalThreshold = Tha60210012ModuleMpigQueueMinInternalThresholdGet(mpigModule, portId, queueId);
        ColorStrToCell(tabPtr, queueId, 3, CliNumber2String(internalThreshold, "%u"), cSevInfo);

        externalThreshold = Tha60210012ModuleMpigQueueMinExternalThresholdGet(mpigModule, portId, queueId) * 32;
        ColorStrToCell(tabPtr, queueId, 4, CliNumber2String(externalThreshold, "%u"), cSevInfo);

        quantumInByte = Tha60210012ModuleMpigQueueQuantumGet(mpigModule, portId, queueId);
        ColorStrToCell(tabPtr, queueId, 5, CliNumber2String(quantumInByte, "%u"), cSevInfo);

        priority = Tha60210012ModuleMpigQueuePriorityGet(mpigModule, portId, queueId);
        string = CliEnumToString(priority, cThaQueuePriorityStr, cTha60210012MpigQueuePriority, mCount(cTha60210012MpigQueuePriority), NULL);
        ColorStrToCell(tabPtr, queueId, 6, string ? string : "Unknown", string ? cSevInfo : cSevWarning);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdMpigQueueCounterShow(char argc, char** argv)
    {
    tTab *tabPtr;
    char **heading;
    void *pHeading;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numQueue, queue_i;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    eAtHistoryReadingMode readingMode;
    tTha60210012MpigQueueCounter counters;
    eAtRet ret;
    uint32 i;
    uint32 numOfRows;
    uint32 gapIdx0, gapIdx1, gapIdx2;

    gapIdx0 = 0;
    gapIdx1 = cTha60210012MpigPktEnqueueCounterNum + (gapIdx0 + 1);
    gapIdx2 = cTha60210012MpigByteEnqueueCountersNum + (gapIdx1 + 1);
    AtUnused(argc);

    if (mpigModule == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: MPIG module is not supported\n");
        return cAtFalse;
        }

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numQueue = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "ERROR: Invalid parameter <queueId> , expected 1-%u\r\n", 8);
        return cAtFalse;
        }

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Create table with titles */
    heading = AtOsalMemAlloc((numQueue + 1) * sizeof(char*));
    for (i = 0; i < (numQueue + 1); i++)
        {
        heading[i] = AtOsalMemAlloc(20 * sizeof(char));
        if (i == 0)
            AtSprintf(heading[i], "Counters");
        else
            AtSprintf(heading[i], "Queue-%d", idBuf[i - 1]);
        }

    numOfRows = mCount(cQueueCounterString);
    pHeading = heading;
    tabPtr = TableAlloc(numOfRows, numQueue + 1, (const char**)pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Print Descriptions for counters */
    for (i = 0; i < numOfRows; i++)
        ColorStrToCell(tabPtr, i, 0, cQueueCounterString[i], ((i == gapIdx0) || (i == gapIdx1) || (i == gapIdx2)) ? cSevDebug : cSevNormal);

    for (queue_i = 0; queue_i < numQueue; queue_i++)
        {
        ret = Tha60210012ModuleMpigQueueCounterDebug(mpigModule, idBuf[queue_i] - 1, &counters, (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse);
        if (ret != cAtOk)
            continue;

        CounterPrint(tabPtr, numOfRows, queue_i, counters);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    for (i = 0; i < (numQueue + 1); i++)
        AtOsalMemFree(heading[i]);

    AtOsalMemFree(heading);
    return cAtTrue;
    }

eBool CmdMpigGlobalCounterShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"Type", "Counter"};
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    eAtHistoryReadingMode readingMode;
    tTha60210012MpigGlobalCounter counters;
    eAtRet ret;
    uint32 i;
    uint32 numOfRows;

    AtUnused(argc);

    if (mpigModule == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: MPIG module is not supported\n");
        return cAtFalse;
        }

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Create table with titles */
    numOfRows = mCount(cMpigGlobalCounterString);
    tabPtr = TableAlloc(numOfRows, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Print Descriptions for counters */
    for (i = 0; i < numOfRows; i++)
        {
        /* Print name of counters */
        ColorStrToCell(tabPtr, i, 0, cMpigGlobalCounterString[i], cSevNormal);

        ret = Tha60210012ModuleMpigGlobalCounterDebug(mpigModule, &counters, (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse);
        if (ret != cAtOk)
            continue;
        ColorStrToCell(tabPtr, i, 1, CliNumber2String(counters.ddrAccessQueueCnt[i], "%u"), cSevInfo);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdMpigDebug(char argc, char **argv)
    {
    AtModule mpigModule = AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argv);
    AtUnused(argc);
    AtModuleDebug(mpigModule);
    return cAtTrue;
    }

eBool CmdMpigDumpInforDebugEnable(char argc, char **argv)
    {
    eAtRet ret;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argv);
    AtUnused(argc);
    ret = Tha60210012ModuleMpigDumpInfoDebugEnable(mpigModule, 1);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigDumpInforDebugDisable(char argc, char **argv)
    {
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    eAtRet ret = Tha60210012ModuleMpigDumpInfoDebugEnable(mpigModule, 0);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigDumpInforDebug(char argc, char **argv)
    {
    eAtRet ret;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argv);
    AtUnused(argc);
    ret = Tha60210012ModuleMpigDumpInfoDebug(mpigModule);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigTransmitOneshotEnable(char argc, char **argv)
    {
    eAtRet ret;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argv);
    AtUnused(argc);
    ret = Tha60210012ModuleMpigTransmitOneshotEnable(mpigModule, 1);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigTransmitOneshotDisable(char argc, char **argv)
    {
    eAtRet ret;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argv);
    AtUnused(argc);
    ret = Tha60210012ModuleMpigTransmitOneshotEnable(mpigModule, 0);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigTransmitOneshotNumPktSet(char argc, char **argv)
    {
    eAtRet ret;
    uint32 numPkt;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argc);
    /* Get Number of packets */
    numPkt = (uint32)AtStrToDw(argv[0]);

    ret = Tha60210012ModuleMpigTransmitOneshotNumPktSet(mpigModule, numPkt);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigTransmitOneshotNumPktStoreBeforeTransmitSet(char argc, char **argv)
    {
    eAtRet ret;
    uint32 numPkt;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argc);
    /* Get Number of packets */
    numPkt = (uint32)AtStrToDw(argv[0]);

    ret = Tha60210012ModuleMpigTransmitOneshotNumPktStoreBeforeTransmitSet(mpigModule, numPkt);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));
    return cAtTrue;
    }

eBool CmdMpigMonitorPacketSet(char argc, char **argv)
    {
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numChannel;
    eAtRet ret;
    uint32 channelIdx;
    uint32 pktLen;
    Tha60210012ModuleMpig mpigModule = (Tha60210012ModuleMpig)AtDeviceModuleGet(CliDevice(), cThaModuleMpig);
    AtUnused(argc);
    /* Get Channel ID */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numChannel = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "ERROR: Invalid parameter <ChannelId>\r\n");
        return cAtFalse;
        }

    /* Get Packet length */
    pktLen = (uint32)AtStrToDw(argv[1]);

    for(channelIdx = 0; channelIdx < numChannel; channelIdx++)
        {
        ret = Tha60210012ModuleMpigMonitorPacketSet(mpigModule, (uint16)(idBuf[channelIdx] - 1), (uint16)pktLen);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "Channel-%u cannot be set, ret = %s\r\n", idBuf[channelIdx], AtRet2String(ret));
        }

    return cAtTrue;
    }

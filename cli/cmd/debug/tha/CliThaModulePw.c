/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliThaModulePw.c
 *
 * Created Date: May 18, 2016
 *
 * Description : PW debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhNxDs0.h"
#include "../pw/CliAtModulePw.h"
#include "../../../../driver/src/implement/default/pdh/ThaPdhDe1.h"
#include "../../../../driver/src/implement/default/pw/ThaModulePw.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/codechip/Tha60210061/pda/Tha60210061ModulePda.h"
#include "../../../../driver/src/implement/default/pwe/ThaModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtModuleVal[] = {
                                      cAtModuleSdh,
                                      cAtModulePdh,
                                      cAtModulePpp,
                                      cAtModuleFr,
                                      cAtModuleEncap,
                                      cAtModuleEth,
                                      cAtModulePw,
                                      cAtModulePrbs,
                                      cAtModuleIma,
                                      cAtModuleAtm,
                                      cAtModuleRam,
                                      cAtModuleBer,
                                      cAtModulePktAnalyzer,
                                      cAtModuleClock,
                                      cAtModuleXc,
                                      cAtModuleAps,
                                      cAtModuleSur,
									  cThaModulePmc
                                    };
static const char *cAtModuleStr[] = {
                                     "sdh",
                                     "pdh",
                                     "ppp",
                                     "fr",
                                     "encap",
                                     "eth",
                                     "pw",
                                     "prbs",
                                     "ima",
                                     "atm",
                                     "ram",
                                     "ber",
                                     "pktanalyzer",
                                     "clock",
                                     "xc",
                                     "aps",
                                     "sur",
									 "pmc"
};

/*--------------------------- Forward declarations ---------------------------*/
extern void ThaOcnVtPohInsertEnable(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool enable);
extern eAtRet ThaSdhChannel2HwMasterStsId(AtSdhChannel channel, eAtModule module, uint8 *slice, uint8 *sts);
extern eAtRet ThaPdhChannelHwIdGet(AtPdhChannel channel, eAtModule phyModule,  uint8* sliceId, uint8 *hwIdInSlice);
extern void ThaModulePwePwDebug(AtPw pw);

/*--------------------------- Implementation ---------------------------------*/
static uint8 SliceOfPw(uint32 pwId)
    {
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtPw pw = AtModulePwGetPw(pwModule, (uint16)(pwId - 1));
    AtSdhChannel sdhChannel = NULL;
    AtPdhChannel pdhChannel = NULL;
    uint8 slice, sts;

    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);

    else if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
        pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);

    else if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        pdhChannel = (AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)AtPwBoundCircuitGet(pw));

    if (sdhChannel)
        ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleSdh, &slice, &sts);

    else if (pdhChannel)
        ThaPdhChannelHwIdGet(pdhChannel, cAtModulePdh, &slice, &sts);

    else
        return 0;

    return (uint8)(slice + 1);
    }

static uint32 PwPrbsMonAddress(uint32 slice)
    {
    uint32 productCode = AtDeviceProductCodeGet(CliDevice());
    if ((productCode & cBit23_0) == (0x60210031 & cBit23_0))
        return (0x247800UL + (uint32)(slice - 1)*0x8000UL);
    else
        return (0x246800UL + (uint32)(slice - 1)*0x1000UL);
    }

static uint32 PwPrbsGenAddress(uint32 slice)
    {
    uint32 productCode = AtDeviceProductCodeGet(CliDevice());
    if ((productCode & cBit23_0) == (0x60210031 & cBit23_0))
        return (0x247000UL + (uint32)(slice - 1)*0x8000UL);
    else
        return (0x246400UL + (uint32)(slice - 1)*0x1000UL);
    }

eBool CmdPwPrbsCheck(char argc, char **argv)
    {
    uint32 numPws, bufferSize;
    uint32 pw_i;
    uint8 slice;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    eBool success = cAtTrue;
    eBool prbsIsRunning;

    /* Get the shared ID buffer */
    uint32* idBuf = CliSharedIdBufferGet(&bufferSize);
    AtUnused(argc);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        uint32 firstTimeVal, secondTimeVal;
        uint32 address;
        uint8 checkTime;

        slice = SliceOfPw(idBuf[pw_i]);
        if (slice == 0)
            continue;

        address = PwPrbsMonAddress(slice) + idBuf[pw_i] - 1;

        /* Check if prbs is running */
        prbsIsRunning = cAtFalse;
        firstTimeVal = AtHalRead(hal, address);
        for (checkTime = 0; checkTime < 10; checkTime++)
            {
            AtOsalUSleep(100);
            secondTimeVal = AtHalRead(hal, address);

            if (firstTimeVal != secondTimeVal)
                {
                prbsIsRunning = cAtTrue;
                break;
                }
            }

        if (!prbsIsRunning)
            {
            AtPrintc(cSevWarning, "\r\nPrbs for pw %u is stop, address 0x%08x\r\n", idBuf[pw_i], address);
            success = cAtFalse;
            continue;
            }

        if (((firstTimeVal & cBit17_16) >> 16) != 1)
            {
            AtPrintc(cSevCritical, "\r\nPrbs for pw %u is fail: read 0x%08x = 0x%08x\r\n", idBuf[pw_i], address, firstTimeVal);
            success = cAtFalse;
            continue;
            }

        /* OK indicate */
        AtPrintc(cSevNormal, ".");
        AtFileFlush(AtStdStandardOutput(AtStdSharedStdGet()));
        }

    if (success)
        AtPrintc(cSevInfo, "\r\nPrbs for all pws are good\r\n");

    return cAtTrue;
    }

static uint32 PwPrbsAlarm(uint32 pwId)
    {
    uint8 slice;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    eBool prbsIsRunning;

    /* Get the shared ID buffer */
    uint32 firstTimeVal, secondTimeVal;
    uint32 address;
    uint8 checkTime;

    slice = SliceOfPw(pwId + 1);
    if (slice == 0)
        return cAtPrbsEngineAlarmTypeError;

    address = PwPrbsMonAddress(slice) + pwId;

    /* Check if prbs is running */
    prbsIsRunning = cAtFalse;
    firstTimeVal = AtHalRead(hal, address);
    for (checkTime = 0; checkTime < 10; checkTime++)
        {
        AtOsalUSleep(100);
        secondTimeVal = AtHalRead(hal, address);

        if (firstTimeVal != secondTimeVal)
            {
            prbsIsRunning = cAtTrue;
            break;
            }
        }

    if (!prbsIsRunning)
        {
        return cAtPrbsEngineAlarmTypeError;
        }

    if (((firstTimeVal & cBit17_16) >> 16) != 1)
        {
        return cAtPrbsEngineAlarmTypeLossSync;
        }
    else
        return 0;
    }

static eBool PwPrbsEnable(char argc, char **argv, eBool enable)
    {
    uint32 numPws, bufferSize;
    uint32 pw_i;
    uint8 slice;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    AtModulePw pwModule;

    /* Get the shared ID buffer */
    uint32* idBuf = CliSharedIdBufferGet(&bufferSize);
    AtUnused(argc);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[pw_i] - 1));
        uint32 address;

        slice = SliceOfPw(idBuf[pw_i]);
        if (slice == 0)
            continue;

        address = PwPrbsGenAddress(slice) + idBuf[pw_i] - 1;
        AtHalWrite(hal, address, (enable) ? 1 : 0);

        if(AtPwTypeGet(pw) == cAtPwTypeCEP)
            {
            AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
            ThaOcnVtPohInsertEnable(sdhChannel,
                                    AtSdhChannelSts1Get(sdhChannel),
                                    AtSdhChannelTug2Get(sdhChannel),
                                    AtSdhChannelTu1xGet(sdhChannel),
                                    enable);
            }
        }

    return cAtTrue;
    }

static eBool PwPrbsIsEnabled(uint32 pwId)
    {
    uint8 slice;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    uint32 address, regVal;

    slice = SliceOfPw((uint32)(pwId+1));
    if (slice == 0)
        return cAtFalse;

    address = PwPrbsGenAddress(slice) + pwId;
    regVal = AtHalRead(hal, address);

    return regVal&cBit0 ? cAtTrue:cAtFalse;
    }

static eBool CliThaDebugPwPrbsShowHelper(uint32 engineIds[], uint32 numEngines)
    {
    tTab *tabPtr;
    uint32 engine_i;
    const char *pHeading[] = {"channel",
                              "engineId",
                              "txMode", "rxMode", "txFixPattern", "rxFixPattern",
                              "invert", "forceError", "txEnabled", "rxEnabled",
                              "txBitOrder", "rxBitOrder", "alarm", "sticky", "genSide", "monSide"};
    eBool boolVal;

    if (numEngines == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numEngines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot create table\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < numEngines; engine_i++)
        {
        uint32 engineId = engineIds[engine_i] - 1;
        uint32 alarm = PwPrbsAlarm(engineId);
        uint32 sticky = alarm;
        eBool isEnabled = PwPrbsIsEnabled(engineId);
        uint8 column = 0;


        StrToCell(tabPtr, engine_i, column++, CliNumber2String(engineId + 1, "%d"));
        StrToCell(tabPtr, engine_i, column++, CliNumber2String(engineId + 1, "%d"));

        ColorStrToCell(tabPtr, engine_i, column++, "NA", cSevCritical);

        ColorStrToCell(tabPtr, engine_i, column++, "NA", cSevCritical);

        StrToCell(tabPtr, engine_i, column++, "NA");
        StrToCell(tabPtr, engine_i, column++, "NA");
        StrToCell(tabPtr, engine_i, column++, "NA");

        boolVal = cAtFalse;
        ColorStrToCell(tabPtr, engine_i, column++, CliBoolToString(boolVal), cSevNormal);

        boolVal = isEnabled;
        ColorStrToCell(tabPtr, engine_i, column++, CliBoolToString(boolVal), boolVal ? cSevInfo : cSevCritical);

        boolVal = isEnabled;
        ColorStrToCell(tabPtr, engine_i, column++, CliBoolToString(boolVal), boolVal ? cSevInfo : cSevCritical);

        ColorStrToCell(tabPtr, engine_i, column++, "NA", cSevCritical);

        ColorStrToCell(tabPtr, engine_i, column++, "NA", cSevCritical);

        ColorStrToCell(tabPtr, engine_i, column++, alarm==0  ? "none":(alarm&cAtPrbsEngineAlarmTypeError?"error":"lossSync"), alarm ? cSevCritical : cSevInfo);
        ColorStrToCell(tabPtr, engine_i, column++, sticky==0  ? "none":(sticky&cAtPrbsEngineAlarmTypeError?"error":"lossSync"), sticky ? cSevCritical : cSevInfo);

        ColorStrToCell(tabPtr, engine_i, column++, "psn", cSevInfo);
        ColorStrToCell(tabPtr, engine_i, column++, "tdm", cSevInfo);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static ThaModulePw PwModule(void)
    {
    return (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    }

static eBool SubPortVlanInsertionEnable(char argc, char** argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    ThaModulePwSubPortVlanInsertionEnable(PwModule(), enabled);
    return cAtTrue;
    }

static eBool SubPortVlanCheckingEnable(char argc, char** argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    ThaModulePwSubPortVlanCheckingEnable(PwModule(), enabled);
    return cAtTrue;
    }

static eBool HitlessHeaderChangeEnable(char argc, char** argv, eBool enabled)
    {
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtUnused(argc);
    AtUnused(argv);

    ThaModulePwHitlessHeaderChangeEnable(pwModule, enabled);
    return cAtTrue;
    }

eBool CmdPwPrbsEnable(char argc, char **argv)
    {
    return PwPrbsEnable(argc, argv, cAtTrue);
    }

eBool CmdPwPrbsDisable(char argc, char **argv)
    {
    return PwPrbsEnable(argc, argv, cAtFalse);
    }

eBool CmdPwDebugHeader(char argc, char** argv)
    {
    uint32        i;
    uint32        numPws;
    AtPw*         pwList;

    AtUnused(argc);
    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw = pwList[i];
        ThaModulePwePwDebug(pw);
        }

    return cAtTrue;
    }

eBool CmdPwPrbsShow(char argc, char **argv)
    {
    uint32 bufferSize, numPws;
    uint32* idBuf = CliSharedIdBufferGet(&bufferSize);
    AtUnused(argc);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }
    return CliThaDebugPwPrbsShowHelper(idBuf, numPws);
    }

eBool CmdPwPrbsDebug(char argc, char** argv)
    {
    uint16        i;
    uint16        column;
    uint32        numPws;
    tTab          *tabPtr = NULL;
    const char    *heading[] = {"PW", "PlaNotSync", "PlaError", "PweNotSync", "PweError", "ClaNotSync", "ClaError", "PdaNotSync", "PdaError"};
    AtPw          *pwList;
    ThaModulePw   modulePw = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw   pw;
        uint32 alarms = 0;
        eBool  alarmRaise;
        static const eThaPwPrbsError alarmTypes[] = {cThaPwPrbsPlaNotSync,
                                                     cThaPwPrbsPlaError,
                                                     cThaPwPrbsPweNotSync,
                                                     cThaPwPrbsPweError,
                                                     cThaPwPrbsClaNotSync,
                                                     cThaPwPrbsClaError,
                                                     cThaPwPrbsPdaNotSync,
                                                     cThaPwPrbsPdaError,
                                                     cThaPwPrbsEncNotSync,
                                                     cThaPwPrbsEncError};
        uint8 alarm_i;

        pw = pwList[i];
        alarms = ThaPwDebuggerPwPrbsCheck(ThaModulePwPwDebuggerGet(modulePw), pw);
        if (alarms == cInvalidUint32)
            {
            AtPrintc(cSevCritical, "ERROR: this feature is not supported for input circuit\r\n");
            return cAtFalse;
            }

        /* ID */
        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        /* Alarm */
        for (alarm_i = 0; alarm_i < mCount(alarmTypes); alarm_i++)
            {
            alarmRaise = (alarms & alarmTypes[alarm_i]) ? cAtTrue : cAtFalse;
            ColorStrToCell(tabPtr, i, column++, alarmRaise ? "set" : "clear", alarmRaise ? cSevCritical : cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPwSpeedDebug(char argc, char** argv)
    {
    uint16        i;
    uint32        numPws;
    AtPw         *pwList;
    ThaModulePw   modulePw = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        ThaPwDebuggerPwSpeedShow(ThaModulePwPwDebuggerGet(modulePw), pwList[i]);

    return cAtTrue;
    }

eBool CmdThaModulePwDebugCounterModule(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;

    uint32 moduleId;
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    eAtRet ret;

    if (argc == 0)
        {
        const char *moduleName;

        moduleId = ThaModulePwDebugCountersModuleGet(pwModule);
        moduleName = CliEnumToString(moduleId, cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), NULL);
        AtPrintc(cSevInfo, "Counter module: %s\r\n", moduleName);
        return cAtTrue;
        }

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModulePwDebugCountersModuleSet(pwModule, moduleId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdThaModulePwDebugAlarmModule(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 moduleId;
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    eAtRet ret;

    if (argc == 0)
        {
        const char *moduleName;

        moduleId = ThaModulePwDebugAlarmModuleGet(pwModule);
        moduleName = CliEnumToString(moduleId, cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), NULL);
        AtPrintc(cSevInfo, "Alarm module: %s\r\n", moduleName);
        return cAtTrue;
        }

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModulePwDebugAlarmModuleSet(pwModule, moduleId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdPwResourcesLimitDisable(char argc, char** argv)
    {
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtUnused(argc);
    AtUnused(argv);
    ThaModulePwResourcesLimitationDisable(pwModule, cAtTrue);
    return cAtTrue;
    }

eBool CmdPwResourcesLimitEnable(char argc, char** argv)
    {
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtUnused(argc);
    AtUnused(argv);
    ThaModulePwResourcesLimitationDisable(pwModule, cAtFalse);
    return cAtTrue;
    }

eBool CmdThaModulePwSubPortVlanInsertionEnable(char argc, char** argv)
    {
    return SubPortVlanInsertionEnable(argc, argv, cAtTrue);
    }

eBool CmdThaModulePwSubPortVlanInsertionDisable(char argc, char** argv)
    {
    return SubPortVlanInsertionEnable(argc, argv, cAtFalse);
    }

eBool CmdThaModulePwSubPortVlanCheckingEnable(char argc, char** argv)
    {
    return SubPortVlanCheckingEnable(argc, argv, cAtTrue);
    }

eBool CmdThaModulePwSubPortVlanCheckingDisable(char argc, char** argv)
    {
    return SubPortVlanCheckingEnable(argc, argv, cAtFalse);
    }

eBool CmdThaModulePwHitlessHeaderChangeEnable(char argc, char** argv)
    {
    return HitlessHeaderChangeEnable(argc, argv, cAtTrue);
    }

eBool CmdThaModulePwHitlessHeaderChangeDisable(char argc, char** argv)
    {
    return HitlessHeaderChangeEnable(argc, argv, cAtFalse);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliThaRegister.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Register debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/default/concate/binder/ThaVcgBinder.h"
#include "../../pw/CliAtModulePw.h"
#include "../../encap/CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char* cThaPhysicalModuleStr[] = {"ocn", "poh", "xc", "cdr", "map", "pdh", "pwe", "pda", "cla", "eth", "concate", "encap", "mpeg", "mpig"};
static const uint32 cThaPhysicalModuleId[] = {cThaModuleOcn, cThaModulePoh, cAtModuleXc, cThaModuleCdr,
                                              cThaModuleMap, cAtModulePdh, cThaModulePwe, cThaModulePda,
                                              cThaModuleCla, cAtModuleEth , cAtModuleConcate, cAtModuleEncap,
                                              cThaModuleMpeg, cThaModuleMpig};

/*--------------------------- Forward declarations ---------------------------*/
extern void ThaModuleOcnSdhChannelRegsShow(AtSdhChannel sdhChannel);
extern void ThaModulePohSdhChannelRegShow(AtSdhChannel sdhChannel);
extern void ThaModuleEncapEncapChannelRegsShow(AtEncapChannel channel);
extern void ThaModuleMpegEncapChannelRegsShow(AtEncapChannel channel);
extern void ThaModuleEncapHdlcBundleRegsShow(AtHdlcBundle bundle);
extern void ThaModuleMpegHdlcBundleRegsShow(AtHdlcBundle bundle);
extern void ThaModulePdaEthFlowRegsShow(AtEthFlow flow);
extern void ThaModulePweEthFlowRegsShow(AtEthFlow flow);
extern void ThaModuleXcSdhChannelRegsShow(AtSdhChannel sdhChannel);
extern void ThaModuleCdrSdhChannelRegsShow(AtSdhChannel sdhChannel);
extern void ThaModuleCdrPdhChannelRegsShow(AtPdhChannel pdhChannel);
extern void ThaModuleAbstractMapSdhChannelRegsShow(AtSdhChannel sdhChannel);
extern void ThaModuleAbstractMapPdhChannelRegsShow(AtPdhChannel pdhChannel);
extern void ThaModulePdhSdhChannelRegsShow(AtSdhChannel sdhChannel);
extern void ThaModulePdhPdhChannelRegsShow(AtPdhChannel pdhChannel);
extern void ThaModulePwePwRegsShow(AtPw pw);
extern void ThaModulePdaPwRegsShow(AtPw pw);
extern void ThaModuleClaPwRegsShow(AtPw pw);
extern void ThaModuleClaEthPortRegShow(AtEthPort port);
extern void ThaModuleEthPortRegShow(AtEthPort port);
extern void ThaModuleMpigEthFlowRegsShow(AtEthFlow flow);

/*--------------------------- Implementation ---------------------------------*/
static uint32 PhysicalModuleIdFromString(const char *physicalModuleString)
    {
    uint32 moduleId = cThaModuleEnd;
    eBool convertSuccess;

    mAtStrToEnum(cThaPhysicalModuleStr, cThaPhysicalModuleId, physicalModuleString, moduleId, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <phyModule>, expected eth, cla, pda,...\r\n");
        return cAtFalse;
        }

    return moduleId;
    }

static eBool PdhChannelRegsDebug(uint32 (*ChannelListFromString)(char *idList, AtChannel *channelList, uint32 bufferSize), char argc, char **argv)
    {
    AtChannel *channel;
    uint32 numChannels, i, moduleId;
    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channel, numChannels);
    moduleId = PhysicalModuleIdFromString(argv[1]);

    for (i = 0; i < numChannels; i++)
        {
        if (channel[i] == NULL)
            continue;

        switch (moduleId)
            {
            case cThaModuleCdr:
                ThaModuleCdrPdhChannelRegsShow((AtPdhChannel)channel[i]);
                break;

            case cThaModuleMap:
                ThaModuleAbstractMapPdhChannelRegsShow((AtPdhChannel)channel[i]);
                break;

            case cAtModulePdh:
                ThaModulePdhPdhChannelRegsShow((AtPdhChannel)channel[i]);
                break;

            case cAtModuleConcate:
                ThaModuleConcatePdhChannelRegsShow((AtPdhChannel)channel[i]);
                break;

            default:
                AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

    return cAtTrue;
    }

eBool CmdEthPortRegsDebug(char argc, char **argv)
    {
    AtEthPort port;
    uint8 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPorts;
    uint32 moduleId;
    AtModuleEth ethModule;
    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return cAtFalse;
        }

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    moduleId = PhysicalModuleIdFromString(argv[1]);

    for (i = 0; i < numPorts; i++)
        {
        port = AtModuleEthPortGet(ethModule, (uint8)(idBuf[i] - 1));
        switch (moduleId)
            {
            case cAtModuleEth:
                ThaModuleEthPortRegShow(port);
                break;

            case cThaModuleCla:
                ThaModuleClaEthPortRegShow(port);
                break;

            default:
                AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

    return cAtTrue;
    }

eBool CmdPdhChannelRegsDebug(char argc, char **argv)
    {
    return PdhChannelRegsDebug(CliChannelListFromString, argc, argv);
    }

eBool CmdPwRegsDebug(char argc, char **argv)
    {
    AtPw *pwList;
    uint16 i;
    uint32 numPws;
    uint32 moduleId;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    moduleId = PhysicalModuleIdFromString(argv[1]);
    for (i = 0; i < numPws; i++)
        {
        switch (moduleId)
            {
            case cThaModulePwe:
                ThaModulePwePwRegsShow(pwList[i]);
                break;

            case cThaModulePda:
                ThaModulePdaPwRegsShow(pwList[i]);
                break;

            case cThaModuleCla:
                ThaModuleClaPwRegsShow(pwList[i]);
                break;

            default:
                AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

    return success;
    }

eBool CmdSdhChannelRegsDebug(char argc, char **argv)
    {
    AtChannel *channel;
    uint32 numChannels, i, moduleId;
    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);
    moduleId = PhysicalModuleIdFromString(argv[1]);

    for (i = 0; i < numChannels; i++)
        {
        if (channel[i] == NULL)
            continue;

        switch (moduleId)
            {
            case cThaModuleOcn:
                ThaModuleOcnSdhChannelRegsShow((AtSdhChannel)channel[i]);
                break;

            case cThaModulePoh:
                ThaModulePohSdhChannelRegShow((AtSdhChannel)channel[i]);
                break;

            case cAtModuleXc:
                ThaModuleXcSdhChannelRegsShow((AtSdhChannel)channel[i]);
                break;

            case cThaModuleCdr:
                ThaModuleCdrSdhChannelRegsShow((AtSdhChannel)channel[i]);
                break;

            case cThaModuleMap:
                ThaModuleAbstractMapSdhChannelRegsShow((AtSdhChannel)channel[i]);
                break;

            case cAtModulePdh:
                ThaModulePdhSdhChannelRegsShow((AtSdhChannel)channel[i]);
                break;

            case cAtModuleConcate:
                ThaModuleConcateSdhChannelRegsShow((AtSdhChannel)channel[i]);
                break;

            default:
                AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

    return cAtTrue;
    }

eBool CmdPdhDe1RegsDebug(char argc, char **argv)
    {
    return PdhChannelRegsDebug(De1ListFromString, argc, argv);
    }

eBool CmdPdhDe3RegsDebug(char argc, char **argv)
    {
    return PdhChannelRegsDebug(CliDe3ListFromString, argc, argv);
    }

eBool CmdAtDebugEncapChannelRegs(char argc, char **argv)
    {
    AtEncapChannel *channelList;
    uint16 i;
    uint32 numChannel, bufferSize, moduleId;
    AtUnused(argc);

    /* Get Channel ID */
    channelList = (AtEncapChannel *)CliSharedChannelListGet(&bufferSize);
    if ((numChannel = CliAtEncapChannelsFromString(argv[0], channelList, bufferSize)) == 0)
      {
      AtPrintc(cSevWarning, "ERROR: Invalid parameter <ChannelId>\r\n");
      return cAtFalse;
      }

    moduleId = PhysicalModuleIdFromString(argv[1]);
    for (i = 0; i < numChannel; i++)
        {
        switch(moduleId)
            {
            case cAtModuleEncap:
                ThaModuleEncapEncapChannelRegsShow(channelList[i]);
                break;

            case cThaModuleMpeg:
                ThaModuleMpegEncapChannelRegsShow(channelList[i]);
                break;

            default:
                AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

    return cAtTrue;
    }

eBool CmdAtDebugPppBundleRegs(char argc, char **argv)
    {
    uint32 *pBundleList;
    uint32 numBundle, bufferSize;
    uint32 i, moduleId;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of bundles */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No protection line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Debug them */
    moduleId = PhysicalModuleIdFromString(argv[1]);
    for (i = 0; i < numBundle; i++)
        {
        AtHdlcBundle bundle = (AtHdlcBundle)CliMpBundleGet(pBundleList[i]);
        switch(moduleId)
            {
            case cAtModuleEncap:
                ThaModuleEncapHdlcBundleRegsShow(bundle);
                break;

            case cThaModuleMpeg:
                ThaModuleMpegHdlcBundleRegsShow(bundle);
                break;

            default:
                AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

    return success;
    }

eBool CmdAtDebugEthFlowRegs(char argc, char **argv)
    {
    uint32 numFlows, i;
    AtEthFlow *flowList;
    uint32 moduleId;
    AtUnused(argc);

    flowList = CliEthFlowListGet(argv[0], &numFlows);
    if (flowList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No FLOW, they have not been created yet or invalid FLOW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Debug them */
    moduleId = PhysicalModuleIdFromString(argv[1]);
    for (i = 0; i < numFlows; i++)
        {
        switch(moduleId)
            {
            case cThaModuleCla:
               ThaModuleClaEthFlowRegsShow(flowList[i]);
               break;

            case cThaModulePda:
                ThaModulePdaEthFlowRegsShow(flowList[i]);
                break;
                
            case cThaModulePwe:
                ThaModulePweEthFlowRegsShow(flowList[i]);
                break;

            case cThaModuleMpig:
                ThaModuleMpigEthFlowRegsShow(flowList[i]);
                break;

            default:
               AtPrintc(cSevWarning, "Invalid physical module\r\n");
            }
        }

  	return cAtTrue;
    }

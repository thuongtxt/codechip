/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BMT module
 *
 * File        : CliThaModuleBmt.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/
#define cBlockSizeInDwords 8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern AtModule ThaDeviceBmtModule(AtDevice device);
extern uint8 ThaModuleBmtDdrRead(AtModule self, uint32 address, uint32 *values);
extern uint8 ThaModuleBmtDdrWrite(AtModule self, uint32 address, const uint32 *values);

/*--------------------------- Implementation ---------------------------------*/
static AtModule BmtModule(void)
    {
    return ThaDeviceBmtModule(CliDevice());
    }

eBool CmdBmtDdrRead(char argc, char **argv)
    {
    uint32 values[cBlockSizeInDwords];
    uint32 addr;
    uint8 numDwords;
    int32 i;
    AtUnused(argc);

    /* Read */
    StrToDw(argv[0], 'h', &addr);
    numDwords = ThaModuleBmtDdrRead(BmtModule(), addr, values);
    if (numDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Read BMT DDR fail\r\n");
        return cAtFalse;
        }

    /* Show data */
    for (i = numDwords; i > 0; i--)
        {
        if (i == numDwords)
            AtPrintc(cSevNormal, "%08x", values[i-1]);
        else
            AtPrintc(cSevNormal, ".%08x", values[i-1]);
        }
    AtPrintc(cSevNormal, "\r\n");

    return cAtTrue;
    }

eBool CmdBmtDdrWrite(char argc, char **argv)
    {
    uint32 addr;
    uint32 values[cBlockSizeInDwords + 1];
    uint32 numDwords;
    AtUnused(argc);

    AtOsalMemInit(values, 0, sizeof(values));

    /* Get address */
    StrToDw(argv[0], 'h', &addr);

    /* Get data */
    if (AtStrToArrayDw(argv[1], values, &numDwords) == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: data is invalid\r\n");
        return cAtFalse;
        }

    /* Check if data is valid */
    if (numDwords < cBlockSizeInDwords)
        {
        AtPrintc(cSevCritical, "ERROR: Need %d dwords\r\n", cBlockSizeInDwords);
        return cAtFalse;
        }

    /* Write */
    numDwords = ThaModuleBmtDdrWrite(BmtModule(), addr, values);
    if (numDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Write BMT DDR fail\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

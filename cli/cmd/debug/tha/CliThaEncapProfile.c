/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliThaEncapProfile.c
 *
 * Created Date: Jun 16, 2016
 *
 * Description : ENCAP profiles debug
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "../../driver/src/implement/default/encap/dynamic/ThaModuleDynamicEncap.h"
#include "../../driver/src/implement/default/encap/profile/ThaProfileManager.h"
#include "../../driver/src/implement/default/encap/profile/profilepool/ThaProfilePool.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char* cNetworkTypeStr[]    = {"ipv4", "ipv6", "uni_mpls", "mul_mpls", "osi_network", "appletalk",
                                           "novel_ipx", "xerox_nsidp", "cisco_system", "cisco_discovery_protocol", "net_bios_framing",
                                           "ipv6_hdr_compress", "8021D_packet", "ibm_bdpu", "dec_lan_brigde", "bcp", "all"};
static const uint32 cNetworkTypeValue[] = {cThaProfileNetworkTypeIpv4, cThaProfileNetworkTypeIpv6, cThaProfileNetworkTypeUniMpls,
                                           cThaProfileNetworkTypeMultiMpls, cThaProfileNetworkTypeOSINetwork, cThaProfileNetworkTypeAppleTalk,
                                           cThaProfileNetworkTypeNovellIPX, cThaProfileNetworkTypeXeroxNSIDP, cThaProfileNetworkTypeCiscoSystem,
                                           cThaProfileNetworkTypeCiscoDiscoveryProtocol, cThaProfileNetworkTypeNetbiosFraming,
                                           cThaProfileNetworkTypeIPv6HeaderCompression, cThaProfileNetworkType8021DPacket,
                                           cThaProfileNetworkTypeIBMBDPU, cThaProfileNetworkTypeDecLanBrigde,
                                           cThaProfileNetworkTypeBcp, cThaProfileNetworkTypeAll};

static const char* cOamTypeStr[]    = {"lcp", "ncp", "q933", "cisco_lmi", "clnp", "esis", "isis", "lipcm", "all"};
static const uint32 cOamTypeValue[] = {cThaProfileOamTypeLCP, cThaProfileOamTypeNCP, cThaProfileOamTypeQ933,
                                       cThaProfileOamTypeCiscoLMI, cThaProfileOamTypeCLNP, cThaProfileOamTypeESIS,
                                       cThaProfileOamTypeISIS, cThaProfileOamTypeLIPCM, cThaProfileOamTypeAll};

static const char* cFcnTypeStr[]    = {"fecn", "becn", "de", "cbit", "all"};
static const uint32 cFcnTypeValue[] = {cThaProfileFcnTypeFECN, cThaProfileFcnTypeBECN, cThaProfileFcnTypeDE,
                                       cThaProfileFcnTypeC, cThaProfileFcnTypeAll};

static const char* cErrorTypeStr[]    = {"fcs_err", "decap_abort", "address_err", "control_err", "protocol_err", "multilink_err", "mac_err", "lookup_err", "all"};
static const uint32 cErrorTypeValue[] = {cThaProfileErrorTypeFcs,
                                         cThaProfileErrorTypeDecAbort,
                                         cThaProfileErrorTypeAddress,
                                         cThaProfileErrorTypeControl,
                                         cThaProfileErrorTypeProtocol,
                                         cThaProfileErrorTypeMultilink,
                                         cThaProfileErrorTypeMac,
                                         cThaProfileErrorTypeLookup,
                                         cThaProfileErrorTypeAll};

static const char* cBcpTypeStr[]    = {"fbit", "zbit", "bbit", "pbit", "pads_field", "all"};
static const uint32 cBcpTypeValue[] = {cThaProfileBcpTypeFbit, cThaProfileBcpTypeZbit, cThaProfileBcpTypeBbit,
                                       cThaProfileBcpTypePbit, cThaProfileBcpTypePADsField, cThaProfileBcpTypeAll};

static const char* cRulesTypeStr[]    = {"drop", "oam", "data", "corrupt"};
static const uint32 cRulesTypeValue[] = {cThaProfileRuleDrop, cThaProfileRuleControl, cThaProfileRuleData, cThaProfileRuleCorrupt};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaProfilePool Pool(void)
    {
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    ThaProfileManager manager = ThaModuleEncapProfileManagerGet(encapModule);
    return ThaProfileManagerProfilePoolGet(manager);
    }

static AtList NetworkProfileListGet(void)
    {
    return ThaProfilePoolNetworkProfileListGet(Pool());
    }

static AtList OamProfileListGet(void)
    {
    return ThaProfilePoolOamProfileListGet(Pool());
    }

static AtList ErrorProfileListGet(void)
    {
    return ThaProfilePoolErrorProfileListGet(Pool());
    }

static AtList BcpProfileListGet(void)
    {
    return ThaProfilePoolBcpProfileListGet(Pool());
    }

static AtList FcnProfileListGet(void)
    {
    return ThaProfilePoolFcnProfileListGet(Pool());
    }

static const char *Rule2String(uint8 rule)
    {
    if (rule == cThaProfileRuleDrop)    return "drop";
    if (rule == cThaProfileRuleControl) return "oam";
    if (rule == cThaProfileRuleData)    return "data";
    if (rule == cThaProfileRuleCorrupt) return "corrupt";

    return "invalid";
    }

static char** HeadingAllocate(uint32 numProfilesPerTable)
    {
    uint32 profileIndex;
    char   **headings;

    headings = AtOsalMemAlloc((numProfilesPerTable + 1) * sizeof(char*));
    if (headings == NULL)
        return NULL;

    for (profileIndex = 0; profileIndex < numProfilesPerTable + 1; profileIndex++)
        {
        headings[profileIndex] = AtOsalMemAlloc(20 * sizeof(char));
        if (profileIndex == 0)
            AtSprintf(headings[profileIndex], "Profile type");
        else
            AtSprintf(headings[profileIndex], "None");
        }

    return headings;
    }

static uint8 Rule2Color(eThaProfileRule rule)
    {
    if (rule == cThaProfileRuleDrop)    return cSevCritical;
    if (rule == cThaProfileRuleControl) return cSevWarning;
    if (rule == cThaProfileRuleData)    return cSevInfo;
    if (rule == cThaProfileRuleCorrupt) return cSevDebug;

    return cSevNormal;
    }

static eBool ProfilesShowByIdList(AtList (*ProfileListGet)(void), uint32 *idBuffer, uint32 numberProfile)
    {
    tTab *tabPtr;
    AtList listProfile;
    uint8 typeIndex, numOfRows;
    uint32 numTables, table_i;
    const char **profileStr;
    uint16* allTypes;
    static const uint8 cNumProfilePerTable = 16;

    listProfile = ProfileListGet();
    numTables = numberProfile / cNumProfilePerTable;
    if ((numberProfile % cNumProfilePerTable) > 0)
        numTables = numTables + 1;

    for (table_i = 0; table_i < numTables; table_i++)
        {
        uint32 numProfilePerTable;
        uint32 profile_i;
        char **heading;
        uint32 currentProfileIndex = table_i * cNumProfilePerTable;
        ThaProfile profile;

        numProfilePerTable = numberProfile;
        if (numProfilePerTable > cNumProfilePerTable)
            numProfilePerTable = cNumProfilePerTable;

        /* Create table */
        heading = HeadingAllocate(numProfilePerTable);

        /* Print descriptions for profile type */
        profile = (ThaProfile)AtListObjectGet(listProfile, currentProfileIndex);
        profileStr = ThaProfileAllTypesStringGet(profile, &numOfRows);

        tabPtr = TableAlloc(numOfRows, numProfilePerTable + 1, (void*)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }

        for (typeIndex = 0; typeIndex < numOfRows; typeIndex++)
            StrToCell(tabPtr, typeIndex, 0, profileStr[typeIndex]);

        for (profile_i = 0; profile_i < numProfilePerTable; profile_i++)
            {
            uint32 profileId = idBuffer[currentProfileIndex + profile_i] - 1;
            char headerTmp[20];

            profile = (ThaProfile)AtListObjectGet(listProfile, profileId);
            if (profile == NULL)
                continue;

            AtSnprintf(headerTmp, sizeof(headerTmp), "Profile %u", ThaProfileProfileIdGet(profile) + 1);
            TableColumnHeaderSet(tabPtr, profile_i + 1, headerTmp);

            allTypes = ThaProfileAllTypesGet(profile, NULL);
            for (typeIndex = 0; typeIndex < numOfRows; typeIndex++)
                {
                eThaProfileRule rule = ThaProfileRuleOfTypeGet(profile, allTypes[typeIndex]);
                ColorStrToCell(tabPtr, typeIndex, profile_i + 1, Rule2String(rule), Rule2Color(rule));
                }
            }

        TablePrint(tabPtr);
        TableFree(tabPtr);

        for (typeIndex = 0; typeIndex < numProfilePerTable + 1; typeIndex++)
            AtOsalMemFree(heading[typeIndex]);
        AtOsalMemFree(heading);

        /* For next table */
        numberProfile = numberProfile - numProfilePerTable;
        }

    return cAtTrue;
    }

static eBool AllProfilesShow(AtList (*ProfileListGet)(void))
    {
    tTab *tabPtr;
    AtList listProfile;
    uint8 typeIndex, numOfRows;
    uint32 numTables, table_i;
    uint32  remainProfile;
    const char **profileStr;
    uint16* allTypes;
    static const uint8 cNumProfilePerTable = 16;

    listProfile = ProfileListGet();
    remainProfile = AtListLengthGet(listProfile);
    numTables = remainProfile / cNumProfilePerTable;
    if ((remainProfile % cNumProfilePerTable) > 0)
        numTables = numTables + 1;

    for (table_i = 0; table_i < numTables; table_i++)
        {
        uint32 numProfilePerTable;
        uint32 profileId;
        char **heading;
        uint32 currentProfileIndex = table_i * cNumProfilePerTable;
        ThaProfile profile;

        numProfilePerTable = remainProfile;
        if (numProfilePerTable > cNumProfilePerTable)
            numProfilePerTable = cNumProfilePerTable;

        /* Create table */
        heading = HeadingAllocate(numProfilePerTable);

        /* Print descriptions for profile type */
        profile = (ThaProfile)AtListObjectGet(listProfile, currentProfileIndex);
        profileStr = ThaProfileAllTypesStringGet(profile, &numOfRows);

        tabPtr = TableAlloc(numOfRows, numProfilePerTable + 1, (void *)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }

        for (typeIndex = 0; typeIndex < numOfRows; typeIndex++)
            StrToCell(tabPtr, typeIndex, 0, profileStr[typeIndex]);

        for (profileId = 0; profileId < numProfilePerTable; profileId++)
            {
            char headerTmp[20];
            profile = (ThaProfile)AtListObjectGet(listProfile, currentProfileIndex + profileId);
            if (profile == NULL)
                continue;

            AtSnprintf(headerTmp, sizeof(headerTmp), "Profile %u", ThaProfileProfileIdGet(profile) + 1);
            TableColumnHeaderSet(tabPtr, profileId + 1, headerTmp);

            allTypes = ThaProfileAllTypesGet(profile, NULL);
            for (typeIndex = 0; typeIndex < numOfRows; typeIndex++)
                {
                eThaProfileRule rule = ThaProfileRuleOfTypeGet(profile, allTypes[typeIndex]);
                ColorStrToCell(tabPtr, typeIndex, profileId + 1, Rule2String(rule), Rule2Color(rule));
                }
            }

        TablePrint(tabPtr);
        TableFree(tabPtr);

        for (typeIndex = 0; typeIndex < numProfilePerTable + 1; typeIndex++)
            AtOsalMemFree(heading[typeIndex]);
        AtOsalMemFree(heading);

        /* For next table */
        remainProfile = remainProfile - numProfilePerTable;
        }

    return cAtTrue;
    }

static eBool ProfilesShow(char argc, char **argv, AtList (*ProfileListGet)(void))
    {
    if (argc >= 1)
        {
        uint32 *idBuffer, numberProfile;

        /* Parse flat IDs */
        idBuffer = CliSharedIdBufferGet(&numberProfile);
        numberProfile = CliIdListFromString(argv[0], idBuffer, numberProfile);
        if (numberProfile == 0)
            return cAtFalse;

        return ProfilesShowByIdList(ProfileListGet, idBuffer, numberProfile);
        }

    return AllProfilesShow(ProfileListGet);
    }

static uint32 NetworkTypeFromStringGet(char *typeStr)
    {
    return CliMaskFromString(typeStr, cNetworkTypeStr, cNetworkTypeValue, mCount(cNetworkTypeValue));
    }

static uint32 OamTypeFromStringGet(char *typeStr)
    {
    return CliMaskFromString(typeStr, cOamTypeStr, cOamTypeValue, mCount(cOamTypeValue));
    }

static uint32 ErrorTypeFromStringGet(char *typeStr)
    {
    return CliMaskFromString(typeStr, cErrorTypeStr, cErrorTypeValue, mCount(cErrorTypeValue));
    }

static uint32 BcpTypeFromStringGet(char *typeStr)
    {
    return CliMaskFromString(typeStr, cBcpTypeStr, cBcpTypeValue, mCount(cBcpTypeValue));
    }

static uint32 FcnTypeFromStringGet(char *typeStr)
    {
    return CliMaskFromString(typeStr, cFcnTypeStr, cFcnTypeValue, mCount(cFcnTypeValue));
    }

static eAtRet ProfileAllTypesRuleSet(ThaProfile profile, eThaProfileRule rule)
    {
    eAtRet ret = cAtOk;
    uint8 numRules, rules_i;
    uint16 *allProfileType = ThaProfileAllTypesGet(profile, &numRules);

    for (rules_i = 0; rules_i < numRules; rules_i++)
        ret |= ThaProfileTypesRuleSet(profile, allProfileType[rules_i], rule);

    return ret;
    }

static eBool ProfileRuleSet(char argc, char **argv,
                            uint32 (*TypeFromStringFunc)(char *typeStr),
                            ThaProfile (*ProfileGet)(ThaProfilePool self, uint32 profileId))
    {
    eAtRet ret = cAtOk;
    eThaProfileRule rule;
    uint32 *idBuffer, numberProfile, profileId;
    eBool convertSuccess = cAtTrue;
    uint32 profileType;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    ThaProfileManager manager = ThaModuleEncapProfileManagerGet(encapModule);
    ThaProfilePool pool = ThaProfileManagerProfilePoolGet(manager);

    AtUnused(argc);

    /* Parse flat IDs */
    idBuffer = CliSharedIdBufferGet(&numberProfile);
    numberProfile = CliIdListFromString(argv[0], idBuffer, numberProfile);
    if (numberProfile == 0)
        return cAtFalse;

    profileType = TypeFromStringFunc(argv[1]);
    rule = CliStringToEnum(argv[2], cRulesTypeStr, cRulesTypeValue, mCount(cRulesTypeValue), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: profile rule is invalid. Ex. drop/data/control/corrupt\r\n");
        return cAtFalse;
        }

    for (profileId = 0; profileId < numberProfile; profileId++)
        {
        ThaProfile profile = ProfileGet(pool, idBuffer[profileId] - 1);

        if (AtStrcmp(argv[1], "all") == 0)
            {
            ret |= ProfileAllTypesRuleSet(profile, rule);
            continue;
            }

        ret |= ThaProfileTypesRuleSet(profile, profileType, rule);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdThaEncapNetworkProfileGet(char argc, char **argv)
    {
    return ProfilesShow(argc, argv, NetworkProfileListGet);
    }

eBool CmdThaEncapOamProfileGet(char argc, char **argv)
    {
    return ProfilesShow(argc, argv, OamProfileListGet);
    }

eBool CmdThaEncapErrorProfileGet(char argc, char **argv)
    {
    return ProfilesShow(argc, argv, ErrorProfileListGet);
    }

eBool CmdThaEncapBcpProfileGet(char argc, char **argv)
    {
    return ProfilesShow(argc, argv, BcpProfileListGet);
    }

eBool CmdThaEncapFcnProfileGet(char argc, char **argv)
    {
    return ProfilesShow(argc, argv, FcnProfileListGet);
    }

eBool CmdThaEncapNetworkProfileSet(char argc, char **argv)
    {
    return ProfileRuleSet(argc, argv, NetworkTypeFromStringGet, ThaProfilePoolNetworkProfileGet);
    }

eBool CmdThaEncapOamProfileSet(char argc, char **argv)
    {
    return ProfileRuleSet(argc, argv, OamTypeFromStringGet, ThaProfilePoolOamProfileGet);
    }

eBool CmdThaEncapFcnProfileSet(char argc, char **argv)
    {
    return ProfileRuleSet(argc, argv, FcnTypeFromStringGet, ThaProfilePoolFcnProfileGet);
    }

eBool CmdThaEncapErrorProfileSet(char argc, char **argv)
    {
    return ProfileRuleSet(argc, argv, ErrorTypeFromStringGet, ThaProfilePoolErrorProfileGet);
    }

eBool CmdThaEncapBcpProfileSet(char argc, char **argv)
    {
    return ProfileRuleSet(argc, argv, BcpTypeFromStringGet, ThaProfilePoolBcpProfileGet);
    }

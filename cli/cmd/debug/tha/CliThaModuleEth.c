/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : CliThaModuleEth.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/eth/Tha60210012ModuleEth.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/pwe/Tha60210012ModulePwe.h"
#include "../../../../driver/src/implement/default/man/ThaDeviceInternal.h"
#include "../../../../driver/src/generic/pw/AtPwInternal.h"
#include "../../../../driver/src/generic/eth/AtEthFlowInternal.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/pwe/Tha60210012ModulePwe.h"
#include "../../../../driver/src/implement/codechip/Tha60210061/cla/Tha60210061ModuleCla.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../pw/CliAtModulePw.h"
#include "../../../../driver/src/implement/codechip/Tha60290011/eth/Tha60290011ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char* cPtchInsertModeStr[]    = {"none", "1-byte", "2-bytes"};
static const uint32 cPtchInsertModeValue[] = {cAtPtchModeUnknown, cAtPtchModeOneByte, cAtPtchModeTwoBytes};
static const uint32 cAtModuleVal[] = {
                                      cAtModuleEth,
									  cThaModulePmc
                                    };
static const char *cAtModuleStr[] = {
                                     "eth",
									 "pmc"
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static ThaModulePwe PweModule(void)
    {
    return (ThaModulePwe)AtDeviceModuleGet(CliDevice(), cThaModulePwe);
    }

eBool CmdPtchLookupEnable(char argc, char** argv)
    {
    eAtRet ret;
    AtModuleEth eth;

    AtUnused(argc);
    AtUnused(argv);

    eth = EthModule();
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "Eth Module is NULL\r\n");
        return cAtFalse;
        }
    ret = Tha60210012ModuleEthPtchLookupEnable(eth, cAtTrue);
    if(ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));

    return cAtTrue;
    }

eBool CmdPtchLookupDisable(char argc, char** argv)
    {
    eAtRet ret;
    AtModuleEth eth;

    AtUnused(argc);
    AtUnused(argv);

    eth = EthModule();
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "Eth Module is NULL\r\n");
        return cAtFalse;
        }
    ret = Tha60210012ModuleEthPtchLookupEnable(eth, cAtFalse);
    if(ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot be set, ret = %s\r\n", AtRet2String(ret));

    return cAtTrue;
    }

eBool CmdAtPwPtchInsertionMode(char argc, char **argv)
    {
    uint32 numPws, pw_i;
    AtPw *pwList;
    eBool success;
    eAtPtchMode insertMode;
    eAtRet ret;

    AtUnused(argc);

    /* Get list of Pws */
    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Get PTCH insertion mode */
    insertMode = CliStringToEnum(argv[1], cPtchInsertModeStr, cPtchInsertModeValue, mCount(cPtchInsertModeStr), &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: PTCH insertion mode %s may be invalid or not supported. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cPtchInsertModeStr, mCount(cPtchInsertModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    if (Tha60210012ModulePwePtchEnableOnModuleIsSupported(PweModule()))
        {
        AtPrintc(cSevWarning, "Warning: PTCH can not be controlled per PW anymore, use CLIs \"eth ptchservice ...\" instead");
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        ret = AtPwPtchInsertionModeSet(pwList[pw_i], insertMode);
        ret |= AtPwPtchServiceEnable(pwList[pw_i], cAtTrue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Set insertion mode for %s fail with ret = %s\r\n", CliChannelIdStringGet((AtChannel)pwList[pw_i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPwPtchInsertionShow(char argc, char **argv)
    {
    const char *pHeading[] = {"PW", "PTCH Insertion"};
    uint32 numPws, pw_i;
    AtPw *pwList;
    tTab *tabPtr;

    AtUnused(argc);

    /* Get list of Pws */
    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPws, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        eAtPtchMode insertMode = AtPwPtchInsertionModeGet(pwList[pw_i]);
        const char *insertModeString = CliEnumToString(insertMode, cPtchInsertModeStr, cPtchInsertModeValue, mCount(cPtchInsertModeStr), NULL);

        StrToCell(tabPtr, pw_i, 0, CliChannelIdStringGet((AtChannel)pwList[pw_i]));
        ColorStrToCell(tabPtr, pw_i, 1, insertModeString ? insertModeString : "Error", (insertMode == cAtPtchModeUnknown) ? cSevCritical : cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdModuleEthIngressChannelIdSet(char argc, char **argv)
    {
    AtModuleEth eth;
    eAtRet ret;
    uint8 channelId;

    AtUnused(argc);

    /* Get Channel ID */
    channelId = (uint8)AtStrToDw(argv[1]);

    eth = EthModule();
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Eth Module is NULL\r\n");
        return cAtFalse;
        }
    ret = Tha60210012ModuleEthIngressChannelIdSet(eth, channelId);
    if(ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot set  ingress channel ID, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdAtExauiCounterShow(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode;
    AtModuleEth eth;
    eAtRet ret;
    AtUnused(argc);

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    eth = EthModule();
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Eth Module is NULL\r\n");
        return cAtFalse;
        }
    ret = Tha60210012ModuleEthExauiGlobalCountersDebug(eth, (readingMode == cAtHistoryReadingModeReadToClear)? cAtTrue:cAtFalse);
    if(ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot show debugging counters, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdAtEthFlowPtchInsertionMode(char argc, char **argv)
    {
    uint32 numFlows, flow_i;
    AtEthFlow *flowList;
    eAtPtchMode insertMode;
    eBool success;
    eAtRet ret;

    AtUnused(argc);

    /* Get list of Pws */
    flowList = CliEthFlowListGet(argv[0], &numFlows);
    if (flowList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No FLOW, they have not been created yet or invalid FLOW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Get PTCH insertion mode */
    insertMode = CliStringToEnum(argv[1], cPtchInsertModeStr, cPtchInsertModeValue, mCount(cPtchInsertModeStr), &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: PTCH insertion mode %s may be invalid or not supported. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cPtchInsertModeStr, mCount(cPtchInsertModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    if (Tha60210012ModulePwePtchEnableOnModuleIsSupported(PweModule()))
        {
        AtPrintc(cSevWarning, "Warning: PTCH can not be controlled per flow anymore, use CLIs \"eth ptchservice ...\" instead");
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        ret = AtEthFlowPtchInsertionModeSet(flowList[flow_i], insertMode);
        ret |= AtEthFlowPtchServiceEnable(flowList[flow_i], cAtTrue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Set insertion mode for %s fail with ret = %s\r\n", CliChannelIdStringGet((AtChannel)flowList[flow_i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEthFlowPtchInsertionShow(char argc, char **argv)
    {
    const char *pHeading[] =
        {
        "PW", "PTCH Insertion"
        };
    uint32 numFlows, flow_i;
    AtEthFlow *flowList;
    tTab *tabPtr;
    AtUnused(argc);

    /* Get list of Pws */
    flowList = CliEthFlowListGet(argv[0], &numFlows);
    if (flowList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No FLOW, they have not been created yet or invalid FLOW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numFlows, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        eAtPtchMode insertMode = AtEthFlowPtchInsertionModeGet(flowList[flow_i]);
        const char *insertModeString = CliEnumToString(insertMode, cPtchInsertModeStr, cPtchInsertModeValue, mCount(cPtchInsertModeStr), NULL);

        StrToCell(tabPtr, flow_i, 0, CliChannelIdStringGet((AtChannel)flowList[flow_i]));
        ColorStrToCell(tabPtr, flow_i, 1, insertModeString ? insertModeString : "Error", (insertMode == cAtPtchModeUnknown) ? cSevCritical : cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEthFlowDebugHeader(char argc, char **argv)
    {
    uint32 numFlows, flow_i;
    AtEthFlow *flowList;
    ThaModulePwe pweModule;
    AtUnused(argc);

    /* Get list of Pws */
    flowList = CliEthFlowListGet(argv[0], &numFlows);
    if (flowList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No FLOW, they have not been created yet or invalid FLOW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pweModule = (ThaModulePwe)AtDeviceModuleGet(CliDevice(), cThaModulePwe);
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        Tha60210012ModulePweEthFlowHeaderShow(pweModule, flowList[flow_i]);

    return cAtTrue;
    }

eBool CmdThaModuleEthDebugCountersModule(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 moduleId;
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    if (argc == 0)
        {
        const char *moduleName;

        moduleId = ThaModuleEthDebugCountersModuleGet(ethModule);
        moduleName = CliEnumToString(moduleId, cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), NULL);
        AtPrintc(cSevInfo, "Counter module: %s\r\n", moduleName);
        return cAtTrue;
        }

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModuleEthDebugCountersModuleSet(ethModule, moduleId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdThaModuleEthDebugFsmTxEnable(char argc, char** argv)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = ThaModuleEthTxFsmPinEnable(ethModule, cAtTrue);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdThaModuleEthDebugFsmTxDisable(char argc, char** argv)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = ThaModuleEthTxFsmPinEnable(ethModule, cAtFalse);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdThaModuleEthDebugFsmRxEnable(char argc, char** argv)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = ThaModuleEthRxFsmPinEnable(ethModule, cAtTrue);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdThaModuleEthDebugFsmRxDisable(char argc, char** argv)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = ThaModuleEthRxFsmPinEnable(ethModule, cAtFalse);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdThaModuleEthDebugFsmShow(char argc, char** argv)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eBool txFsmPinEn, txFsmPinStatus, rxFsmPinEn, rxFsmPinStatus;
    const char *pHeading[] = { "TxFsmPinCfg", "TxFsmPinStatus", "RxFsmPinCfg", "RxFsmPinStatus"};
    tTab *tabPtr;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    txFsmPinEn = ThaModuleEthTxFsmPinIsEnabled(ethModule);
    txFsmPinStatus = ThaModuleEthTxFsmPinStatusGet(ethModule);
    rxFsmPinEn = ThaModuleEthRxFsmPinIsEnabled(ethModule);
    rxFsmPinStatus = ThaModuleEthRxFsmPinStatusGet(ethModule);

    StrToCell(tabPtr, 0, 0, txFsmPinEn ? "en" : "dis");
    StrToCell(tabPtr, 0, 1, txFsmPinStatus ? "en" : "dis");
    StrToCell(tabPtr, 0, 2, rxFsmPinEn ? "en" : "dis");
    StrToCell(tabPtr, 0, 3, rxFsmPinStatus ? "en" : "dis");

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDebugEthernetCounterPerServiceGet(char argc, char **argv)
    {
    uint32 port_i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *ports;
    tTab *tabPtr;
    eAtHistoryReadingMode readingMode;
    char **heading;
    AtUnused(argc);

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    heading = (void*)AtOsalMemAlloc((numPorts + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (port_i = 1; port_i <= numPorts; port_i++)
        {
        heading[port_i] = (char*)AtOsalMemAlloc(3);
        if (heading[port_i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            AtStringArrayFree((char**)heading, (uint8)(port_i));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[port_i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (port_i = 1; port_i <= numPorts; port_i++)
        AtSprintf((void*)heading[port_i], "%s", AtChannelIdString(ports[port_i - 1]));

    tabPtr = TableAlloc(0, numPorts + 1UL, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);

    /* Make table content */
    for (port_i = 1; port_i <= numPorts; port_i++)
        {
        uint16 rowId = 0;
        AtEthPort port = (AtEthPort) ports[port_i - 1];
        uint32 values;

        values = Tha60210061ModuleClaTotalPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "Total Packets");

        values = Tha60210061ModuleClaDiscardPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeError, "Discard Packets");

        values = Tha60210061ModuleClaCESPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "CES Packets");

        values = Tha60210061ModuleClaCEPPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "CEP Packets");

        values = Tha60210061ModuleClaEOPPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "EOP Packets");

        values = Tha60210061ModuleClaHDLCPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "HDLC Packets");

        values = Tha60210061ModuleClacHDLCPacketCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "cHDLC Packets");

        values = Tha60210061ModuleClaPppBridgePktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "PPP Bridge Packets");

        values = Tha60210061ModuleClaPppOamPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "PPP oam Packets");

        values = Tha60210061ModuleClaPppDataPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "PPP data Packets");

        values = Tha60210061ModuleClaMlpppDataPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "MLPPP data Packets");

        values = Tha60210061ModuleClaMlpppOamPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "MLPPP oam Packets");

        values = Tha60210061ModuleClaFrDataPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "FR data Packets");

        values = Tha60210061ModuleClaFrOamPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "FR oam Packets");

        values = Tha60210061ModuleClaMFrOamPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "MFR oam Packets");

        values = Tha60210061ModuleClaEthPassThroughPktCountPerService(port, readingMode);
        CliCounterTypePrintToCell(tabPtr, rowId++, port_i, values, cAtCounterTypeGood, "Eth passthrough Packets");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtStringArrayFree((char**)heading, (uint8)(numPorts + 1));

    return cAtTrue;
    }

eBool CmdThaModuleEthDebugDccEnable(char argc, char** argv)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = Tha60290011ModuleEthDccEnable(ethModule, cAtTrue);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdThaModuleEthDebugDccDisable(char argc, char** argv)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = Tha60290011ModuleEthDccEnable(ethModule, cAtFalse);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

eBool CmdThaModuleEthDebugDccShow(char argc, char** argv)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eBool dccMode;
    const char *pHeading[] = {"Dcc Mode"};
    tTab *tabPtr;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    dccMode = Tha60290011ModuleEthDccIsEnabled(ethModule);

    StrToCell(tabPtr, 0, 0, dccMode ? "en" : "dis");

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

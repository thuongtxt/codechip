/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliThaModulePdh.c
 *
 * Created Date: May 18, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhChannel.h"
#include "AtPdhNxDs0.h"
#include "../../../../driver/src/implement/default/pdh/ThaPdhDe1.h"
#include "../../../../driver/src/implement/default/pdh/ThaPdhDe3.h"
#include "../../../../driver/src/implement/default/sdh/ThaSdhVc.h"
#include "../../../../driver/src/implement/default/pdh/ThaModulePdh.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtModuleVal[] = {
                                      cAtModulePdh,
                                      cAtModuleSur,
									  cThaModulePmc
                                    };
static const char *cAtModuleStr[] = {
                                     "pdh",
                                     "sur",
									 "pmc"
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaCdrController PdhPathControllerGet(AtPdhChannel channel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(channel);
    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return ThaPdhDe1CdrControllerGet((ThaPdhDe1)channel);

    if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
        return ThaPdhDe3CdrControllerGet((ThaPdhDe3)channel);

    if (channelType == cAtPdhChannelTypeNxDs0)
        return ThaPdhDe1CdrControllerGet((ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)channel));

    return NULL;
    }

static eBool PdhPathCdrDebug(char argc, char** argv, uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                             void (*DebugFunc)(ThaCdrController))
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    ThaCdrController controller;

    AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        if (channelList[i] == NULL)
            continue;

        if ((controller = PdhPathControllerGet((AtPdhChannel)channelList[i])) == NULL)
            continue;

        DebugFunc(controller);
        }

    return cAtTrue;
    }

eBool CmdPdhDe1CdrDebug(char argc, char** argv)
    {
    return PdhPathCdrDebug(argc, argv, De1ListFromString, ThaCdrControllerDebug);
    }

eBool CmdPdhDe3CdrDebug(char argc, char** argv)
    {
    return PdhPathCdrDebug(argc, argv, CliDe3ListFromString, ThaCdrControllerDebug);
    }

eBool CmdPwHeaderCdrDebug(char argc, char** argv)
    {
    AtPw        pw;
    uint16      i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws;
    AtModulePw  pwModule;
    AtChannel   circuit;
    eAtPwType   pwType;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i++)
        {
        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        circuit = AtPwBoundCircuitGet(pw);
        if (circuit == NULL)
            {
            AtPrintc(cSevWarning, "No debug information for PW %u because it has not been bound circuit\r\n", idBuf[i]);
            continue;
            }

        pwType = AtPwTypeGet(pw);
        if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
            ThaCdrControllerPwHeaderDebug(PdhPathControllerGet((AtPdhChannel)circuit), pw);

        else if (pwType == cAtPwTypeCEP)
            ThaCdrControllerPwHeaderDebug(ThaSdhVcCdrControllerGet((AtSdhVc)circuit), pw);
        }

    return cAtTrue;
    }

eBool CmdThaModulePdhDebugCountersModule(char argc, char** argv)
    {
    eBool convertSuccess = cAtFalse;
    uint32 moduleId;
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    eAtRet ret;

    if (argc == 0)
        {
        const char *moduleName;

        moduleId = ThaModulePdhDebugCountersModuleGet(pdhModule);
        moduleName = CliEnumToString(moduleId, cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), NULL);
        AtPrintc(cSevInfo, "Counter module: %s\r\n", moduleName);
        return cAtTrue;
        }

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = ThaModulePdhDebugCountersModuleSet(pdhModule, moduleId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: select module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

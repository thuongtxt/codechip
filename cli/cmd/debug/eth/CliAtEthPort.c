/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliAtEthPort.c
 *
 * Created Date: Dec 13, 2015
 *
 * Description : ETH Port debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../eth/CliAtModuleEth.h"
#include "../physical/CliAtSerdesController.h"
#include "../../../../driver/src/generic/physical/AtSerdesControllerInternal.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/eth/Tha60210012EXaui.h"
#include "../../../../driver/src/implement/codechip/Tha60210061/eth/Tha60210061ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eTha60210012Source SourceFromStr(const char *sourceStr)
    {
    if (AtStrcmp(sourceStr, "xfi") == 0)    return cTha60210012SourceXFI;
    if (AtStrcmp(sourceStr, "exaui") == 0)  return cTha60210012SourceEXaui;

    return cTha60210012SourceInvalid;
    }

static const char *Source2String(eTha60210012Source source)
    {
    if (source == cTha60210012SourceXFI)   return "xfi";
    if (source == cTha60210012SourceEXaui) return "exaui";
    return "N/A";
    }

static const char *Alarm2String(uint32 alarm)
    {
    if ((alarm & cAtPrbsEngineAlarmTypeError)) return "Error";
    return "None";
    }

static eBool HelperEXauiBoolSet(char argc, char **argv,eBool enable, eAtRet (*BoolFuncAttribute)(AtEthPort self, eBool enable))
    {
    AtChannel *ports;
    uint32 port_i;
    uint32 bufferSize;
    uint32 numPorts;
    eBool success = cAtTrue;
    AtUnused(argc);

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);

    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = BoolFuncAttribute((AtEthPort)ports[port_i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set attribute of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)ports[port_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool HelperEXauiValueSet(char argc, char **argv, uint32 value, eAtRet (*ValueFuncAttribute)(AtEthPort self, uint32 value))
    {
    AtChannel *ports;
    uint32 port_i;
    uint32 bufferSize;
    uint32 numPorts;
    eBool success = cAtTrue;
    AtUnused(argc);

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);

    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = ValueFuncAttribute((AtEthPort)ports[port_i], value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set attribute of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)ports[port_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEthPortSerdesForceAlarm(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success;
    uint32 alarmType;
    eBool enable;
    eAtDirection direction;
    eAtRet (*AlarmForce)(AtSerdesController self, uint32 alarmType);
    eBool blResult = cAtTrue;
    uint32 i;
    AtUnused(argc);

    /* Direction */
    direction = CliDirectionFromStr(argv[1]);
    if (direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    alarmType = CliSerdesControllerAlarmMaskFromString(argv[2]);

    /* Enable/disable */
    mAtStrToBool(argv[3], enable, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: expect en/dis, not %s\r\n", argv[3]);
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    /* Determine function to use */
    if (direction == cAtDirectionRx)
        AlarmForce = enable ? AtSerdesControllerRxAlarmForce : AtSerdesControllerRxAlarmUnforce;
    else
        {
        AtObjectDelete((AtObject)controllers);
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(controllers); i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AlarmForce(controller, alarmType);
        if (ret != cAtOk)
            {
            AtChannel port = AtSerdesControllerPhysicalPortGet(controller);
            AtPrintc(cSevCritical,
                     "ERROR: Cannot force %s alarm for SERDES of %s, ret = %s\r\n",
                     argv[2],
                     CliChannelIdStringGet(port),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesForcedAlarmShow(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eAtDirection direction;
    eBool success = cAtTrue;
    eBool silent = cAtFalse;
    AtUnused(argc);

    /* Direction */
    direction = CliDirectionFromStr(argv[1]);
    if (direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    if (direction == cAtDirectionRx)
        {
        success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerRxForcedAlarmGet, silent);
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesForcableAlarmShow(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eAtDirection direction;
    eBool success = cAtTrue;
    eBool silent = cAtFalse;
    AtUnused(argc);

    /* Direction */
    direction = CliDirectionFromStr(argv[1]);
    if (direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    if (direction == cAtDirectionRx)
        {
        success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerRxForcableAlarmsGet, silent);
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtPrbsEngineEXauiPortMonitorSourceSelect(char argc, char **argv)
    {
    eTha60210012Source source;
    eBool success = cAtTrue;
    AtChannel *ports;
    uint32 port_i;
    uint32 numPorts, bufferSize;

    AtUnused(argc);
    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Direction */
    source = SourceFromStr(argv[1]);
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = Tha60210012EthEXauiPrbsMonitorSourceSelect((AtEthPort)ports[port_i], source);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot Select source generate of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)ports[port_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPrbsEngineEXauiSingleGenerate(char argc, char **argv)
    {
    uint32 numPacket;
    eBool success = cAtTrue;
    AtChannel *ports;
    uint32 port_i;
    uint32 numPorts, bufferSize;

    AtUnused(argc);
    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    numPacket = AtStrToDw(argv[1]);
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = Tha60210012EthEXauiPrbsSingleGenerate((AtEthPort)ports[port_i], numPacket);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot single generate packet of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)ports[port_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPrbsEngineEXauiContinuosGenerate(char argc, char **argv)
    {
    AtChannel *ports;
    uint32 bufferSize, port_i;
    uint32 numPorts;
    eBool success = cAtTrue;
    AtUnused(argc);

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = Tha60210012EthEXauiPrbsContinousGenerate((AtEthPort)ports[port_i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot continuous generate packet of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)ports[port_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPrbsEngineEXauiEnable(char argc, char **argv)
    {
    return HelperEXauiBoolSet(argc, argv, cAtTrue, Tha60210012EthEXauiPrbsEnable);
    }

eBool CmdAtPrbsEngineEXauiDisable(char argc, char **argv)
    {
    return HelperEXauiBoolSet(argc, argv, cAtFalse, Tha60210012EthEXauiPrbsEnable);
    }

eBool CmdAtPrbsEngineEXauiForceError(char argc, char **argv)
    {
    return HelperEXauiBoolSet(argc, argv, cAtTrue, Tha60210012EthEXauiPrbsForceError);
    }

eBool CmdAtPrbsEngineEXauiUnForceError(char argc, char **argv)
    {
    return HelperEXauiBoolSet(argc, argv, cAtFalse, Tha60210012EthEXauiPrbsForceError);
    }

eBool CmdAtPrbsEngineEXauiLenModeSet(char argc, char **argv)
    {
    static const char* cPrbsLenModeStr[] = {"fix", "increase"};
    static const eTha60210012EthExauiPrbsLenMode cPrbsLenModeVal[] = {cTha60210012EthEXauiPrbsLenModeFix, cTha60210012EthEXauiPrbsLenModeIncrease};
    uint32 lenMode;
    eBool success;
    lenMode = CliStringToEnum(argv[1], cPrbsLenModeStr, cPrbsLenModeVal, mCount(cPrbsLenModeStr), &success);
    if(!success)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot get lenght mode\r\n");
        return cAtFalse;
        }

    return HelperEXauiValueSet(argc, argv, lenMode, Tha60210012EthExauiPrbsLenModeSet);
    }

eBool CmdAtPrbsEngineEXauiMaxLenSet(char argc, char **argv)
    {
    uint32 maxLen = AtStrToDw(argv[1]);
    return HelperEXauiValueSet(argc, argv, maxLen, Tha60210012EthExauiPrbsMaxLenSet);
    }

eBool CmdAtPrbsEngineEXauiMinLenSet(char argc, char **argv)
    {
    uint32 minLen = AtStrToDw(argv[1]);
    return HelperEXauiValueSet(argc, argv, minLen, Tha60210012EthExauiPrbsMinLenSet);
    }

eBool CmdAtPrbsEngineEXauiBandwidthSet(char argc, char **argv)
    {
    eBool success;
    static const char* cPrbsBandwitchStr[] = {"1.2", "2.4", "3.6", "4.8", "6", "7.2", "8.4", "9.6"};
    static const uint32 cPrbsBandwitchVal[] = {0, 1, 2, 3, 4, 5, 6, 7};

    uint32 bandwidth = CliStringToEnum(argv[1], cPrbsBandwitchStr, cPrbsBandwitchVal,mCount(cPrbsBandwitchStr), &success);
    if(!success)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot set bandwidth\r\n");
        return cAtFalse;
        }

    return HelperEXauiValueSet(argc, argv, bandwidth, Tha60210012EthExauiPrbsBandwidthSet);
    }

eBool CmdAtPrbsEngineEXauiConfigGet(char argc, char **argv)
    {
    AtChannel *ports;
    uint32 bufferSize, port_i;
    uint32 numPorts;
    uint8 i;
    tTab *tabPtr;
    char **heading;
    void *pHeading;
    const char *infoStr[] = {"Enable", "Force", "NumPktGen", "GenerateMode",
                             "SideMonitoring", "Error", "PktLenMode", "MaxPacketLen", "MinPackeLen", "BandWidth"};
    AtUnused(argc);

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create heading */
    heading = AtOsalMemAlloc((uint32)(numPorts + 1) * sizeof(char*));
    for (i = 0; i < (numPorts + 1); i++)
        {
        heading[i] = AtOsalMemAlloc(20 * sizeof(char));
        if (i == 0)
            AtSprintf(heading[i], "eXAUI ID");
        else
            AtSprintf(heading[i], "Port-%u", AtChannelIdGet(ports[i - 1]) + 1);
        }

    /* Create table with titles */
    pHeading = heading;
    tabPtr = TableAlloc(mCount(infoStr), (uint32)(numPorts + 1), (const char **)pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for(i = 0; i < mCount(infoStr); i++)
        {
        StrToCell(tabPtr, i, 0, infoStr[i]);
        }

    for (port_i = 0; port_i < numPorts; port_i ++)
        {
        uint32 numPacketGenerate;
        eTha60210012EthExauiPrbsLenMode lenMode;
        uint32 colum = (uint32)(port_i + 1);
        uint32 row = 0;
        AtEthPort ethPort = (AtEthPort)ports[port_i];
        StrToCell(tabPtr, row++, colum, CliBoolToString(Tha60210012EthEXauiPrbsIsEnabled(ethPort)));
        StrToCell(tabPtr, row++, colum, CliBoolToString(Tha60210012EthEXauiPrbsIsForceError(ethPort)));

        numPacketGenerate = Tha60210012EthEXauiPrbsSingleGenerateGet(ethPort);
        StrToCell(tabPtr, row++, colum, CliNumber2String(numPacketGenerate, "%u"));
        StrToCell(tabPtr, row++, colum, (numPacketGenerate != 0xFFFF) ? "single" : "continuous");
        StrToCell(tabPtr, row++, colum, Source2String(Tha60210012EthEXauiPrbsMonitorSourceSelectGet(ethPort)));
        StrToCell(tabPtr, row++, colum, Alarm2String(Tha60210012EthEXauiPrbsErrorGet(ethPort)));
        lenMode = Tha60210012EthExauiPrbsLenModeGet(ethPort);
        StrToCell(tabPtr, row++, colum, (lenMode == cTha60210012EthEXauiPrbsLenModeFix)? "Fixd_length":"Increased_Length");
        StrToCell(tabPtr, row++, colum, CliNumber2String(Tha60210012EthExauiPrbsMaxLenGet(ethPort), "%u"));
        StrToCell(tabPtr, row++, colum, CliNumber2String(Tha60210012EthExauiPrbsMinLenGet(ethPort), "%u"));
        StrToCell(tabPtr, row++, colum, CliNumber2String((1.2 * (1 + Tha60210012EthExauiPrbsBandwidthGet(ethPort))), "%2.1f"));
        }


    TablePrint(tabPtr);
    TableFree(tabPtr);
    for (i = 0; i < (numPorts + 1); i++)
        AtOsalMemFree(heading[i]);

    AtOsalMemFree(heading);

    return cAtTrue;
    }

eBool CmdAtPrbsEngineEXauiCounterGet(char argc, char **argv)
    {
    AtChannel *ports;
    uint32 port_i, bufferSize;
    uint32 numPorts;
    tTab *tabPtr;
    uint32 (*counterFunc)(AtEthPort self, uint32 counterType) = Tha60210012EthEXauiPrbsCounterGet;
    const char *heading[] = {"EXaui ID", "RxPackets", "RxBytes"};

    AtUnused(argc);
    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    if (CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadToClear)
        counterFunc = Tha60210012EthEXauiPrbsCounterClear;

    for (port_i = 0; port_i < numPorts; port_i ++)
        {
        uint8 colum = 0;
        StrToCell(tabPtr, port_i, colum++, CliNumber2String(AtChannelIdGet(ports[port_i]) + 1, "%u"));
        StrToCell(tabPtr, port_i, colum++, CliNumber2String(counterFunc((AtEthPort)ports[port_i], cTha60210012EthEXauiPrbsCounterRxPackets), "%u"));
        StrToCell(tabPtr, port_i, colum++, CliNumber2String(counterFunc((AtEthPort)ports[port_i], cTha60210012EthEXauiPrbsCounterRxBytes), "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }


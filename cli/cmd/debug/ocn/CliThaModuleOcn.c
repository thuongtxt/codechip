/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : CliThaModuleOcn.c
 *
 * Created Date: Oct 20, 2016
 *
 * Description : OCN module debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/default/ocn/ThaModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(void)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(CliDevice(), cThaModuleOcn);
    }

eBool CmdThaModuleOcnDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleDebug((AtModule)OcnModule());
    return cAtTrue;
    }

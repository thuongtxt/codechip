/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : CliAtEyeScan.c
 *
 * Created Date: Jul 29, 2017
 *
 * Description : Eyescan debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtTokenizer.h"
#include "../../physical/CliAtSerdesController.h"
#include "../../physical/CliAtEyeScan.h"
#include "../../../../driver/src/generic/physical/AtEyeScanControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DefaultSettingEnable(char argc, char **argv, eBool enabled)
    {
    AtList controllers = CliAtSerdesEyeScanControllersFromString(argv[0]);
    uint32 controller_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no controllers, ID may be wrong\r\n");
        return cAtTrue;
        }

    for (controller_i = 0; controller_i < AtListLengthGet(controllers);controller_i++)
        {
        AtEyeScanController controller = (AtEyeScanController)AtListObjectGet(controllers, controller_i);
        eAtRet ret = AtEyeScanControllerDefaultSettingEnable(controller, enabled);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Enable default setting fail on %s with ret = %s\r\n",
                 AtObjectToString((AtObject)controller),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool LaneEyescanEnable(char argc, char **argv, eBool enabled)
    {
    AtList lanes = CliAtEyeScanLanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneAttributeSet(lanes, enabled, (AtEyeScanLaneAttributeSetFunc)AtEyeScanLaneEyeScanEnable);
    AtObjectDelete((AtObject)lanes);
    AtUnused(argc);
    return success;
    }

static eBool LaneApply(char argc, char **argv, eAtRet (*ApplyFunction)(AtEyeScanLane self))
    {
    AtList lanes = CliAtEyeScanLanesFromIdString(argv[0]);
    eBool success = cAtTrue;
    uint32 lane_i;

    AtUnused(argc);

    if (AtListLengthGet(lanes) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no lanes to control, the ID may be wrong\r\n");
        return cAtTrue;
        }

    for (lane_i = 0; lane_i < AtListLengthGet(lanes); lane_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(lanes, lane_i);
        eAtRet ret = ApplyFunction(lane);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Configure lane %s fail with ret = %s\r\n",
                     AtObjectToString((AtObject)lane), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lanes);

    return success;
    }

static eAtRet LanePixelScan(AtList lanes, int32 horzOffset, int32 vertOffset)
    {
    uint32 lane_i;
    eAtRet ret = cAtOk;

    for (lane_i = 0; lane_i < AtListLengthGet(lanes); lane_i++)
        {
        AtEyeScanLane lane = (AtEyeScanLane)AtListObjectGet(lanes, lane_i);
        ret |= AtEyeScanLaneDebugPixelScan(lane, horzOffset, vertOffset);
        }

    return ret;
    }

eBool CmdAtEyeScanLanePixelScan(char argc, char **argv)
    {
    AtList lanes = CliAtEyeScanLanesFromIdString(argv[0]);
    AtTokenizer pixelTokenizer;
    eBool success = cAtTrue;

    AtUnused(argc);

    if (AtListLengthGet(lanes) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no lanes to control, the ID may be wrong\r\n");
        return cAtTrue;
        }

    pixelTokenizer = AtTokenizerNew(argv[1], ",");
    if (AtTokenizerNumStrings(pixelTokenizer) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No pixels to scan. Pixel input example: 1.2,-1.-2\r\n");
        AtObjectDelete((AtObject)lanes);
        return cAtTrue;
        }

    while (AtTokenizerHasNextString(pixelTokenizer))
        {
        char *pixelString = AtTokenizerNextString(pixelTokenizer);
        AtTokenizer pointParser = AtTokenizerNew(pixelString, ".");
        int32 horzOffset, vertOffset;
        eAtRet ret;

        if (AtTokenizerNumStrings(pointParser) != 2)
            {
            AtPrintc(cSevWarning,
                     "Point %s is invalid, format should be x.y\r\n",
                     pixelString);
            AtObjectDelete((AtObject)pointParser);
            }

        horzOffset = AtAtoi(AtTokenizerNextString(pointParser));
        vertOffset = AtAtoi(AtTokenizerNextString(pointParser));
        ret = LanePixelScan(lanes, horzOffset, vertOffset);
        if (ret != cAtOk)
            success = cAtFalse;

        AtObjectDelete((AtObject)pointParser);
        }

    AtObjectDelete((AtObject)pixelTokenizer);
    AtObjectDelete((AtObject)lanes);

    return success;
    }

eBool CmdAtEyeScanLaneEyescanEnable(char argc, char **argv)
    {
    return LaneEyescanEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEyeScanLaneEyescanDisable(char argc, char **argv)
    {
    return LaneEyescanEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEyeScanControllerDefaultSettingEnable(char argc, char **argv)
    {
    return DefaultSettingEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEyeScanControllerDefaultSettingDisable(char argc, char **argv)
    {
    return DefaultSettingEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEyeScanLaneInit(char argc, char **argv)
    {
    return LaneApply(argc, argv, AtEyeScanLaneInit);
    }

eBool CmdAtEyeScanLaneSetup(char argc, char **argv)
    {
    return LaneApply(argc, argv, AtEyeScanLaneSetup);
    }

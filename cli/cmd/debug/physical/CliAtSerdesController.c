/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes
 *
 * File        : CliAtSerdesController.c
 *
 * Created Date: Aug 30, 2016
 *
 * Description : Debug serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../physical/CliAtSerdesController.h"
#include "../../../../driver/src/generic/physical/AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtSerdesLayerStr[]  = {"pcs", "pma"};
static const uint32 cAtSerdesLayerVal[] = {cAtSerdesPhysicalLayerPcs, cAtSerdesPhysicalLayerPma};

static const char *cAtSerdesControllerEyescanLogicStr[] = {"auto_select", "stm", "xaui", "gty", "gtye3", "gthe3"};
static const uint32 cAtSerdesControllerEyescanLogicVal[] = {cAtSerdesControllerEyescanAutoSelect,
                                                            cAtSerdesControllerEyescanStm,
                                                            cAtSerdesControllerEyescanXaui,
                                                            cAtSerdesControllerEyescanGty,
                                                            cAtSerdesControllerEyescanGtyE3,
                                                            cAtSerdesControllerEyescanGthE3
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SerdesLayerFromString(const char *serdesTypeString)
    {
    uint8 i;

    for (i = 0; i < mCount(cAtSerdesLayerStr); i++)
        {
        if (AtStrcmp(serdesTypeString, cAtSerdesLayerStr[i]) == 0)
            return cAtSerdesLayerVal[i];
        }

    return cAtSerdesPhysicalLayerPma;
    }

eBool CmdAtSerdesControllerLoopbackDebug(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    uint32 controller_i;
    eBool success = cAtTrue;
    eAtLoopbackMode loopbackMode = CliLoopbackModeFromString(argv[1]);
    uint32 layer = cAtSerdesPhysicalLayerPcs;

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES to loopback\r\n");
        return cAtTrue;
        }

    if (argc == 3)
        layer = SerdesLayerFromString(argv[2]);

    for (controller_i = 0; controller_i < AtListLengthGet(controllers); controller_i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, controller_i);
        eAtRet ret = AtSerdesControllerLayerLoopbackSet(controller, layer, loopbackMode);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Cannot loopback SERDES %u, ret = %s\r\n",
                 AtSerdesControllerIdGet(controller),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtControllerSerdesDebug(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eBool success = CliAtSerdesControllerDebug(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerForcedAlarmShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eAtDirection direction;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Direction */
    direction = CliDirectionFromStr(argv[1]);
    if (direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }
    if (direction == cAtDirectionRx)
        {
        success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerRxForcedAlarmGet, cAtFalse);
        }
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerForcableAlarmShow(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    eAtDirection direction;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Direction */
    direction = CliDirectionFromStr(argv[1]);
    if (direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }
    if (direction == cAtDirectionRx)
        {
        success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerRxForcableAlarmsGet, cAtFalse);
        }
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSerdesControllerEyescanLogicSet(char argc, char **argv)
    {
    AtList controllers = CliAtSerdesControllersList(argv[0]);
    uint32 serdes_i;
    eBool success = cAtTrue;
    eAtSerdesControllerEyescanLogic logic = cAtSerdesControllerEyescanAutoSelect;

    AtUnused(argc);

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevInfo, "No SERDES\r\n");
        return cAtTrue;
        }

    logic = CliStringToEnum(argv[1],
                            cAtSerdesControllerEyescanLogicStr,
                            cAtSerdesControllerEyescanLogicVal,
                            mCount(cAtSerdesControllerEyescanLogicVal),
                            &success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: please input correct logic. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtSerdesControllerEyescanLogicStr, mCount(cAtSerdesControllerEyescanLogicVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (serdes_i = 0; serdes_i < AtListLengthGet(controllers); serdes_i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, serdes_i);
        AtSerdesControllerEyescanLogicSet(controller, logic);
        }

    AtObjectDelete((AtObject)controllers);
    return success;
    }

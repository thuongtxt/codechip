/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtIpCore.c
 *
 * Created Date: Jul 29, 2016
 *
 * Description : IP core debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../driver/src/generic/man/AtIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtIpCoreInterruptShow(char argc, char **argv)
    {
    AtDevice device = CliDevice();
    uint32 numCores = AtDeviceNumIpCoresGet(device);
    uint32 numIds;
    uint32 *coreIdList = CliSharedIdBufferGet(&numIds);
    uint32 core_i;
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"IpCore", "Enable", "Total count", "Contiguous count", "Max proc. (us)", "Last proc. (us)"};
    eBool enabled;
    eBool read2Clear = cAtFalse;

    numCores = CliIdListFromString(argv[0], coreIdList, numIds);
    if (numCores == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid IPcore ID list. Expected: 1, 2-3...\r\n");
        return cAtFalse;
        }

    if (argc > 1)
        {
        if (CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadToClear)
            read2Clear = cAtTrue;
        }

    tabPtr = TableAlloc(numCores, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }


    for (core_i = 0; core_i < numCores; core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(device, (uint8)(coreIdList[core_i] - 1));
        uint32 column = 0;

        if (core == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: ignore not existing core %d\r\n", coreIdList[core_i]);
            continue;
            }

        StrToCell(tabPtr, row, column++, CliNumber2String(coreIdList[core_i], "%u"));

        enabled = AtIpCoreInterruptIsEnabled(core);
        StrToCell(tabPtr, row, column++, CliBoolToString(enabled));

        StrToCell(tabPtr, row, column++, CliNumber2String(AtIpCoreInterruptCountRead2Clear(core, read2Clear), "%u"));
        StrToCell(tabPtr, row, column++, CliNumber2String(AtIpCoreInterruptContiguousCountRead2Clear(core, read2Clear), "%u"));
        StrToCell(tabPtr, row, column++, CliNumber2String(AtIpCoreInterruptMaxProcessingTimeInUsRead2Clear(core, read2Clear), "%u"));
        StrToCell(tabPtr, row, column++, CliNumber2String(AtIpCoreInterruptLastProcessingTimeInUsRead2Clear(core, read2Clear), "%u"));

        row++;
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

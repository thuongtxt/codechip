/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Driver
 *
 * File        : CliAtDriver.c
 *
 * Created Date: May 19, 2017
 *
 * Description : Driver debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtDriverHaRestoreAndKeepDatabase(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    while (AtDriverRestoreAndKeepDatabase(AtDriverSharedDriverGet(), cAtTrue) > 0);
    return cAtTrue;
    }

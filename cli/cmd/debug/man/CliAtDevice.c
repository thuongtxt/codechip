/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDevice.c
 *
 * Created Date: Sep 29, 2016
 *
 * Description : Device debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/generic/man/AtDeviceInternal.h"
#include "../util/CliAtDebugger.h"
#include "../../../../driver/src/implement/codechip/Tha60290061/man/Tha60290061Device.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool TestbenchEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDeviceTestbenchEnable(CliDevice(), enabled);
    return cAtTrue;
    }

eBool CmdAtDevicePlatformSet(char argc, char **argv)
    {
    uint32 productCode;

    AtUnused(argc);
    StrToDw(argv[0], 'h', &productCode);
    AtDevicePlatformSet(CliDevice(), productCode);

    return cAtTrue;
    }

eBool CmdAtDeviceTestbenchDisable(char argc, char **argv)
    {
    return TestbenchEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDeviceTestbenchEnable(char argc, char **argv)
    {
    return TestbenchEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDeviceDebugTableShow(char argc, char **argv)
    {
    AtDebugger debugger = AtDeviceDebuggerCreate(CliDevice());
    AtUnused(argc);
    AtUnused(argv);

    AtDeviceDebug(CliDevice());
    CliAtDebuggerShow(debugger);
    AtDeviceDebuggerDelete(CliDevice());

    return cAtTrue;
    }

eBool CmdAtDeviceDdrOffloadAuditEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    Tha60290061DeviceAuditEnable((Tha60290061Device)CliRealDevice(), cAtTrue);
    return cAtTrue;
    }

eBool CmdAtDeviceDdrOffloadAuditDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    Tha60290061DeviceAuditEnable((Tha60290061Device)CliRealDevice(), cAtFalse);
    return cAtTrue;
    }

eBool CmdAtDeviceDdrOffloadAudit(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtFile outPutFile = NULL;
    uint32 subDeviceId = 0;
    eTha60290061AuditMode auditMode = cTha60290061AuditCompare;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough params\r\n");
        return cAtFalse;
        }

    if (argv[0] == 0)
        {
        AtPrintc(cSevCritical, "Invalid subdeviceId\r\n");
        return cAtFalse;
        }

    if (!Tha60290061DeviceAuditIsEnabled((Tha60290061Device)CliRealDevice()))
        {
        AtPrintc(cSevCritical, "Device audit is not enabled\r\n");
        return cAtFalse;
        }

    StrToDw(argv[0], 'd', &subDeviceId);
    subDeviceId = (uint32)(subDeviceId - 1);
    outPutFile = AtStdFileOpen(AtStdSharedStdGet(), argv[1], cAtFileOpenModeWrite | cAtFileOpenModeTruncate);

    if ((argc == 3) && (AtStrcmp(argv[2], "none") == 0))
        auditMode = cTha60290061AuditNoCompare;

    ret = Tha60290061DeviceDdrOffloadDbAudit(CliRealDevice(), subDeviceId, outPutFile, auditMode);
    AtStdFileClose(outPutFile);

    if (ret == cAtOk)
        return cAtTrue;
    return cAtFalse;
    }

eBool CmdAtDeviceDdrOffloadAuditInit(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 subDeviceId = 0;
    AtUnused(argc);

    if (!Tha60290061DeviceAuditIsEnabled((Tha60290061Device)CliRealDevice()))
        {
        AtPrintc(cSevCritical, "Device audit is not enabled\r\n");
        return cAtFalse;
        }

    StrToDw(argv[0], 'd', &subDeviceId);
    subDeviceId = (uint32)(subDeviceId - 1);
    if (AtDeviceNumSubDeviceGet(CliRealDevice()) <= subDeviceId)
        {
        AtPrintc(cSevCritical, "sub Device audit is out of range\r\n");
        return cAtFalse;
        }

    ret = Tha60290061DeviceDdrOffloadDbAuditInit(CliRealDevice(), subDeviceId);

    if (ret == cAtOk)
        return cAtTrue;
    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : FIXME Module Name
 *
 * File        : CliAtModule.c
 *
 * Created Date: Jul 20, 2018
 *
 * Description : FIXME Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../../../driver/src/generic/man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char* cAtModuleStr[] = {"sur", "ptp"};
static const uint32 cAtModuleVal[] = {
                                      cAtModuleSur,
                                      cAtModulePtp
                                     };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool ModuleActivate(char argc, char** argv, eAtRet (*activateFunc)(AtModule))
    {
    eBool convertSuccess = cAtFalse;
    uint32 moduleId;
    eAtRet ret;
    AtUnused(argc);

    moduleId = CliStringToEnum(argv[0], cAtModuleStr, cAtModuleVal, mCount(cAtModuleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown module %s, expect: ", argv[0]);
        CliExpectedValuesPrint(cSevCritical, cAtModuleStr, mCount(cAtModuleStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = activateFunc(AtDeviceModuleGet(CliDevice(), moduleId));
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Activate module fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModuleActivate(char argc, char** argv)
    {
    return ModuleActivate(argc, argv, AtModuleActivate);
    }

eBool CmdAtModuleDeactivate(char argc, char** argv)
    {
    return ModuleActivate(argc, argv, AtModuleDeactivate);
    }

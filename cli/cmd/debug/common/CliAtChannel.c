/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : CliAtChannel.c
 *
 * Created Date: May 1, 2017
 *
 * Description : Channel debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtTokenizer.h"
#include "../../../../driver/src/implement/default/man/ThaDevice.h"
#include "../../../../driver/src/implement/default/ocn/ThaModuleOcn.h"
#include "../../../../driver/src/implement/default/pdh/ThaModulePdh.h"
#include "../../../../driver/src/implement/default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleLookup(const char *moduleName)
    {
    AtIterator moduleIterator = ThaDeviceModuleIteratorCreate(CliDevice());
    AtModule module, lookupModule = NULL;

    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        {
        if (AtStrcmp(AtModuleTypeString(module), moduleName) == 0)
            {
            lookupModule = module;
            break;
            }
        }

    AtObjectDelete((AtObject)moduleIterator);

    return lookupModule;
    }

static uint32 AllModules(AtModule *modulesBuffer, uint32 bufferSize)
    {
    AtIterator moduleIterator = ThaDeviceModuleIteratorCreate(CliDevice());
    AtModule module;
    uint32 numModules = 0;

    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        {
        modulesBuffer[numModules] = module;
        numModules = numModules + 1;
        if (numModules >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)moduleIterator);

    return numModules;
    }

static uint32 ModulesFromString(char *moduleNames, AtModule *modulesBuffer, uint32 bufferSize)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(moduleNames, ",");
    uint32 numModules = 0;

    while (AtTokenizerHasNextString(tokenizer))
        {
        char *moduleName = AtTokenizerNextString(tokenizer);
        AtModule module = ModuleLookup(moduleName);
        if (module == NULL)
            continue;

        modulesBuffer[numModules] = module;
        numModules = numModules + 1;
        if (numModules >= bufferSize)
            return numModules;
        }

    return numModules;
    }

static eAtRet SdhChannelHwIdGet(AtChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    return ThaSdhChannelHwStsGet((AtSdhChannel)channel, phyModule, swSts, sliceId, hwStsInSlice);
    }

static eAtRet PdhChannelHwIdGet(AtChannel channel, eAtModule phyModule, uint8 unused, uint8* sliceId, uint8 *hwStsInSlice)
    {
    AtUnused(unused);
    return ThaPdhChannelHwIdGet((AtPdhChannel)channel, phyModule, sliceId, hwStsInSlice);
    }

static uint8 SdhChannelNumSts(AtChannel channel)
    {
    return AtSdhChannelNumSts((AtSdhChannel)channel);
    }

static uint8 PdhChannelNumSts(AtChannel channel)
    {
    AtUnused(channel);
    return 1;
    }

static uint8 SdhChannelStartSts1(AtChannel channel)
    {
    return AtSdhChannelSts1Get((AtSdhChannel)channel);
    }

static uint8 PdhChannelStartSts1(AtChannel channel)
    {
    AtUnused(channel);
    return cInvalidUint8;
    }

static eBool ChannelHwIdShow(char argc, char **argv,
                             uint8 (*NumSts)(AtChannel channel),
                             uint8 (*StartSts1)(AtChannel channel),
                             eAtRet (*HwIdGet)(AtChannel channel, eAtModule phyModule, uint8 sts1Id, uint8* sliceId, uint8 *hwStsInSlice))
    {
    uint32 column;
    uint32 numChannels, row_i;
    uint32 module_i;
    tTab *tabPtr;
    AtChannel *channels = CliSharedChannelListGet(&numChannels);
    const char *pHeading[] = {"Channel",
                              "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                              "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    uint32 numModules;
    AtModule *modules = (AtModule *)CliSharedObjectListGet(&numModules);

    if (argc <= 1)
        numModules = AllModules(modules, numModules);
    else
        numModules = ModulesFromString(argv[1], modules, numModules);

    if (numModules == 0)
        {
        AtPrintc(cSevCritical, "WARNING: no modules to display, the input module names may be wrong\r\n");
        return cAtTrue;
        }

    /* Get list of channels */
    numChannels = CliChannelListFromString(argv[0], channels, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channels to process, the input ID may be wrong\r\n");
        return cAtTrue;
        }

    /* See if table can hold all of modules */
    numModules = mMin(numModules, mCount(pHeading) - 1);

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Build header with module names retrieved from driver */
    for (module_i = 0; module_i < numModules; module_i++)
        {
        AtModule module = modules[module_i];
        TableColumnHeaderSet(tabPtr, module_i + 1, AtModuleTypeString(module));
        }

    /* Make table content */
    for (row_i = 0; row_i < numChannels; row_i++)
        {
        AtChannel channel = channels[row_i];

        column = 0;
        StrToCell(tabPtr, row_i, column++, CliChannelIdStringGet(channel));

        for (module_i = 0; module_i < numModules; module_i++)
            {
            uint8 sts_i;
            uint32 stringBufferSize, remainBufferSize;
            char *string;
            eAtRet ret = cAtOk;
            eAtSevLevel color = cSevNormal;
            AtModule module = modules[module_i];
            uint8 numSts = NumSts(channel);
            uint8 startSts = StartSts1(channel);

            /* Have an empty string */
            string = CliSharedCharBufferGet(&stringBufferSize);
            remainBufferSize = stringBufferSize;
            string[0] = '\0';

            for (sts_i = 0; sts_i < numSts; sts_i++)
                {
                uint8 hwSlice, hwSts;
                char hwIdString[16];
                uint8 sts1 = (uint8)(startSts + sts_i);

                ret = HwIdGet(channel, AtModuleTypeGet(module), sts1, &hwSlice, &hwSts);
                if (ret != cAtOk)
                    {
                    AtSprintf(string, "%s", AtRet2String(ret));
                    color = cSevCritical;
                    break;
                    }

                if (sts_i > 0)
                    {
                    AtStrncat(string, "|", remainBufferSize);
                    remainBufferSize = remainBufferSize - 1;
                    }

                AtSnprintf(hwIdString, sizeof(hwIdString), "%d,%d", hwSlice, hwSts);
                AtStrncat(string, hwIdString, remainBufferSize);
                remainBufferSize = remainBufferSize - AtStrlen(hwIdString);
                }

            ColorStrToCell(tabPtr, row_i, column++, string, color);
            }
        }

    SubTablePrint(tabPtr, 0, numChannels - 1, 0, numModules);
    TableFree(tabPtr);

    AtPrintc(cSevInfo, "* NOTE: ID is list of slice,sts separated by \'|\'\r\n");

    return cAtTrue;
    }

static eBool AuLookup(char *pStrIdList, eAtModule moduleId)
    {
    tTab *tabPtr;
    AtIdParser idParser = AtIdParserNew(pStrIdList, "0.0", "15.47"); /* May be more ? */
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    const char *pHeading[] = {"HwId", "SwId"};
    uint32 rowId = 0;
    uint32 charBufferSize;
    char *charBuffer = CliSharedCharBufferGet(&charBufferSize);

    if (idParser == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: %s hardware ID may be wrong\r\n", pStrIdList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(AtIdParserNumNumbers(idParser) / AtIdParserNumIdLevel(idParser), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (rowId = 0; rowId < (AtIdParserNumNumbers(idParser) / AtIdParserNumIdLevel(idParser)); rowId++)
        {
        uint32 sliceId = AtIdParserNextNumber(idParser);
        uint32 stsId = AtIdParserNextNumber(idParser);
        AtSdhPath path = ThaModuleSdhAuPathFromHwIdGet(sdhModule, moduleId, (uint8)sliceId, (uint8)stsId);
        uint32 column = 0;

        AtSnprintf(charBuffer, charBufferSize, "%d.%d", sliceId, stsId);
        StrToCell(tabPtr, rowId, column++, charBuffer);
        StrToCell(tabPtr, rowId, column++, CliChannelIdStringGet((AtChannel)path));
        }

    AtObjectDelete((AtObject)idParser);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool TuLookup(char *pStrIdList, eAtModule moduleId)
    {
    tTab *tabPtr;
    AtIdParser idParser = AtIdParserNew(pStrIdList, "0.0.0.0", "15.47.6.3"); /* May be more ? */
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    const char *pHeading[] = {"HwId", "SwId"};
    uint32 rowId = 0;
    uint32 charBufferSize;
    char *charBuffer = CliSharedCharBufferGet(&charBufferSize);

    if (idParser == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: %s hardware ID may be wrong\r\n", pStrIdList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(AtIdParserNumNumbers(idParser) / AtIdParserNumIdLevel(idParser), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (rowId = 0; rowId < (AtIdParserNumNumbers(idParser) / AtIdParserNumIdLevel(idParser)); rowId++)
        {
        uint32 sliceId = AtIdParserNextNumber(idParser);
        uint32 stsId = AtIdParserNextNumber(idParser);
        uint32 vtgId = AtIdParserNextNumber(idParser);
        uint32 vtId = AtIdParserNextNumber(idParser);
        AtSdhPath path = ThaModuleSdhTuPathFromHwIdGet(sdhModule, moduleId, (uint8)sliceId, (uint8)stsId, (uint8)vtgId, (uint8)vtId);
        uint32 column = 0;

        AtSnprintf(charBuffer, charBufferSize, "%d.%d.%d.%d", sliceId, stsId, vtgId, vtId);
        StrToCell(tabPtr, rowId, column++, charBuffer);
        StrToCell(tabPtr, rowId, column++, CliChannelIdStringGet((AtChannel)path));
        }

    AtObjectDelete((AtObject)idParser);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhChannelHwIdShow(char argc, char **argv)
    {
    return ChannelHwIdShow(argc, argv, SdhChannelNumSts, SdhChannelStartSts1, SdhChannelHwIdGet);
    }

eBool CmdAtPdhChannelHwIdShow(char argc, char **argv)
    {
    return ChannelHwIdShow(argc, argv, PdhChannelNumSts, PdhChannelStartSts1, PdhChannelHwIdGet);
    }

eBool CmdAtSdhChannelSwIdLookup(char argc, char **argv)
    {
    char *pDupString;
    char *idType, *idString;
    uint32 numModules;
    AtModule *modules = (AtModule *)CliSharedObjectListGet(&numModules);

    AtUnused(argc);

    numModules = ModulesFromString(argv[1], modules, numModules);
    if (numModules != 1)
        {
        AtPrintc(cSevCritical, "ERROR: Module must be input\r\n");
        return cAtFalse;
        }

    /* Get ID type */
    pDupString = IdTypeAndIdListGet(argv[0], &idType, &idString);
    if ((idType == NULL) || (idString == NULL))
        {
        AtOsalMemFree(pDupString);
        return cAtFalse;
        }

    if (AtStrcmp(idType, "au") == 0)
        AuLookup(idString, AtModuleTypeGet(modules[0]));
    else if (AtStrcmp(idType, "tu") == 0)
        TuLookup(idString, AtModuleTypeGet(modules[0]));
    else
        {
        AtPrintc(cSevCritical, "ERROR: Only au/tu are supported now\r\n");
        return cAtFalse;
        }

    /* Free memory */
    AtOsalMemFree(pDupString);

    return cAtTrue;
    }

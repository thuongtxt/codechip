/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliAtPdhDe1Debug.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : Debug CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhDe1.h"
#include "AtTokenizer.h"
#include "AtPdhDe1Prm.h"
#include "../../textui/CliAtTextUI.h"
#include "../../../include/AtCliModule.h"
#include "../../../../driver/src/generic/pdh/AtPdhDe1Internal.h"
#include "../../../../driver/src/generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../../driver/src/implement/default/pdh/debugger/ThaPdhDebugger.h"
#include "../../../../driver/src/implement/default/pdh/ThaModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mRetCodeCheck(ret, ApiName)                                            \
    if ((ret) != cAtOk)                                                        \
        {                                                                      \
        AtPrintc(cSevCritical, "ERROR: %s fail with ret = %s\r\n",             \
                 (ApiName),                                                    \
                 AtRet2String((ret)));                                         \
        return cAtFalse;                                                       \
        }

#define mRetObjectCheck(debugObject)                                           \
    if ((debugObject) == NULL)                                                 \
        {                                                                      \
        AtPrintc(cSevCritical, "ERROR:  can not get the PRM Debugger Object. It may not support!\r\n");\
        return cAtFalse;                                                       \
        }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtPrmPayLoadBitStr[] = {"G1", "G2", "G3", "G4", "G5", "G6",
                                            "SE", "FE", "LV","SL", "LB"};

static const uint32 cAtPrmPayLoadBit[]  = {cThaPdhDebuggerPrmPayloadBitG1,
                                           cThaPdhDebuggerPrmPayloadBitG2,
                                           cThaPdhDebuggerPrmPayloadBitG3,
                                           cThaPdhDebuggerPrmPayloadBitG4,
                                           cThaPdhDebuggerPrmPayloadBitG5,
                                           cThaPdhDebuggerPrmPayloadBitG6,
                                           cThaPdhDebuggerPrmPayloadBitSE,
                                           cThaPdhDebuggerPrmPayloadBitFE,
                                           cThaPdhDebuggerPrmPayloadBitLV,
                                           cThaPdhDebuggerPrmPayloadBitSL,
                                           cThaPdhDebuggerPrmPayloadBitLB};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDebugger Debugger(void)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    return ThaModulePdhDebugger(pdhModule);
    }

static eBool De1PrmDebugMonitorEnable(eBool enable)
    {
	eAtRet ret;
	ThaPdhDebugger debug = Debugger();
	mRetObjectCheck(debug)
	ret = ThaPdhDebuggerPrmMonitorEnable(debug, enable);
	mRetCodeCheck(ret, "ThaPdhDebuggerPrmMonitorEnable")
    return cAtTrue;
    }

static const char* DebugPrmMode2String(void)
    {
    if (ThaPdhDebuggerPrmIsForcedFullPrmPayload(Debugger()))
        return "10bit";
    if (ThaPdhDebuggerPrmIsForced8BitPrmPayload(Debugger()))
        return "8bit";

    return "none";
    }

static uint32 AllPrmPayloadBitGetFromString(char* string, uint8 *numBytes)
    {
    char* payloadBitStr;
    eBool convertSuccess;
    uint32 payloadBitMap = 0;
    uint32 numItems = 0;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer((void *)string, "|");
    eThaPdhDebuggerPrmPayloadBit prmPayloadBit;

    while((AtTokenizerHasNextString(tokenizer)) && (numItems < 324))
        {
        payloadBitStr = AtTokenizerNextString(tokenizer);
        prmPayloadBit = CliStringToEnum(payloadBitStr, cAtPrmPayLoadBitStr, cAtPrmPayLoadBit, mCount(cAtPrmPayLoadBit), &convertSuccess);
        if (!convertSuccess)
            continue;

        payloadBitMap |= prmPayloadBit;
        numItems++;
        }

    *numBytes = (uint8)numItems;
    return payloadBitMap;
    }

static char* AllPrmPayloadBitToString(uint32 bitMap, uint32 numBits)
    {
    uint8 i;
    static char string[128];
    eBool convertSuccess;

    if (numBits == 0)
        {
        AtSprintf(string, "none");
        return string;
        }

    AtOsalMemInit(string, 0, sizeof(string));
    for (i = 0; i < 32; i++)
        {
        char temp[5];

        if ((bitMap & (cBit0 << i)) == 0)
            continue;

        AtOsalMemInit(temp, 0, sizeof(temp));
        mAtEnumToStr(cAtPrmPayLoadBitStr, cAtPrmPayLoadBit, (cBit0 << i), temp, convertSuccess);
        if (convertSuccess)
            {
            AtStrcat(string, temp);
            AtStrcat(string, ",");
            }
        }

    string[AtStrlen(string) - 1] = '\0';
    return string;
    }

static eBool PrmDebugForcePaload(char argc, char **argv, eBool forced)
    {
    eAtRet ret;
    uint32 payloadBitMap;
    uint8 numPayloadBit;
	ThaPdhDebugger debug = Debugger();

    AtUnused(argc);
	mRetObjectCheck(debug)
    payloadBitMap = AllPrmPayloadBitGetFromString(argv[0], &numPayloadBit);
    if (numPayloadBit == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRM Payload bit list\n");
        return cAtFalse;
        }

    ret = ThaPdhDebuggerPrmForcePayLoad(debug, payloadBitMap, forced);
	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForcePayLoad")
    return cAtTrue;
    }

static AtPdhPrmController CapturePrmController(char *idString)
    {
    uint32 numChannels;
    AtChannel *channelList;
    AtPdhDe1 de1;
    AtPdhPrmController prmController;

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(idString, channelList, numChannels);
    if (numChannels != 1)
        {
        AtPrintc(cSevCritical, "ERROR: Only one DE1 can be input to capture PRM messages at time\r\n");
        return NULL;
        }

    de1 = (AtPdhDe1)channelList[0];
    prmController = AtPdhDe1PrmControllerGet(de1);
    if (prmController == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: %s does not have any associated PRM controller.\r\n", CliChannelIdStringGet((AtChannel)de1));
        return NULL;
        }

    return prmController;
    }

static eBool PrmCaptureStart(char argc, char **argv, eBool started)
    {
    AtPdhPrmController prmController;
    eAtRet ret;

    AtUnused(argc);

    prmController = CapturePrmController(argv[0]);
    if (prmController == NULL)
        return cAtFalse;

    if (started)
        ret = AtPdhPrmControllerCaptureStart(prmController);
    else
        ret = AtPdhPrmControllerCaptureStop(prmController);

    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: %s capturing fail with ret = %s\r\n", started ? "Start" : "Stop", AtRet2String(ret));
    return cAtFalse;
    }

static eBool FastSearchEnable(char argc, char** argv, eBool enable)
    {
    eAtRet  ret = cAtOk;
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;

    AtUnused(argc);
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = ThaPdhDe1FastFrameSearchEnable((ThaPdhDe1)channelList[i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning, "Can not %s fast search on %s, ret = %s\r\n",
                     enable ? "enable" : "disable",
                     CliChannelIdStringGet(channelList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe1PrmDebugChannelSelect(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels, i;
    eAtRet ret = cAtOk;
	ThaPdhDebugger debug = Debugger();

    AtUnused(argc);
	mRetObjectCheck(debug)

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        if (channelList[i] == NULL)
            continue;

        ret = ThaPdhDebuggerPrmChannelSelect(debug, (AtPdhDe1)channelList[i]);
    	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForcePayLoad")
        }

    return cAtTrue;
    }

eBool CmdPdhDe1PrmDebugMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return De1PrmDebugMonitorEnable(cAtTrue);
    }

eBool CmdPdhDe1PrmDebugMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return De1PrmDebugMonitorEnable(cAtFalse);
    }

eBool CmdPdhDe1PrmDebugForceMode(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
	ThaPdhDebugger debug = Debugger();

    AtUnused(argc);
	mRetObjectCheck(debug)

    if (AtStrcmp(argv[0], "8bit") == 0)
        {
    	ret = ThaPdhDebuggerPrmForce8BitPrmPayload(debug, cAtTrue);
    	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForce8BitPrmPayload")
        }
    else if (AtStrcmp(argv[0], "10bit") == 0)
        {
    	ret = ThaPdhDebuggerPrmForcedFullPrmPayload(debug, cAtTrue);
    	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForcedFullPrmPayload")
        }
    else if (AtStrcmp(argv[0], "dis") == 0)
        {
        ret = ThaPdhDebuggerPrmForce8BitPrmPayload(debug, cAtFalse);
    	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForce8BitPrmPayload")
        ret = ThaPdhDebuggerPrmForcedFullPrmPayload(debug, cAtFalse);
    	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForcedFullPrmPayload")
        }
    else
        {
        AtPrintc(cSevCritical, "ERROR: Input force mode fails, expected 8bit/10bit/dis \r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdPdhDe1PrmDebugForcePaload(char argc, char **argv)
    {
    return PrmDebugForcePaload(argc, argv, cAtTrue);
    }

eBool CmdPdhDe1PrmDebugUnForcePaload(char argc, char **argv)
    {
    return PrmDebugForcePaload(argc, argv, cAtFalse);
    }

eBool CmdPdhDe1PrmDebugForceNmNi(char argc, char **argv)
    {
    uint8 value;
    eAtRet ret;
	ThaPdhDebugger debug = Debugger();

    AtUnused(argc);
	mRetObjectCheck(debug)

    value = (uint8)AtStrToDw(argv[0]);
    ret = ThaPdhDebuggerPrmForceNmNi(debug, cThaPdhDebuggerPrmPayloadBitNmNi, value);
	mRetCodeCheck(ret, "ThaPdhDebuggerPrmForceNmNi")
    return cAtTrue;
    }

eBool CmdPdhDe1PrmDebugGet(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 values, numPayloadBits;
    const char* pHeading[] = {"DE1 ID", "Enable", "Mode", "PayloadBitForced", "NmNiBit"};
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
	ThaPdhDebugger debug = Debugger();

    AtUnused(argc);
    AtUnused(argv);
	mRetObjectCheck(debug)

    tabPtr = TableAlloc(1, 5, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    values = ThaPdhDebuggerPrmChannelSelectGet(debug);
    StrToCell(tabPtr, 0, 0, (values == cBit31_0) ? "N/A" : CliChannelIdStringGet((AtChannel)AtModulePdhDe1Get(pdhModule, values)));
    StrToCell(tabPtr, 0, 1, CliBoolToString(ThaPdhDebuggerPrmMonitorIsEnabled(debug)));
    StrToCell(tabPtr, 0, 2, DebugPrmMode2String());
    numPayloadBits = ThaPdhDebuggerPrmIsForcedPayLoad(debug, &values);
    StrToCell(tabPtr, 0, 3, AllPrmPayloadBitToString(values, numPayloadBits));
    StrToCell(tabPtr, 0, 4, CliNumber2String(ThaPdhDebuggerPrmUnForceNmNi(debug), "%u"));

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1PrmInterruptGet(char argc, char **argv)
    {
    uint32  numChannels, i, column, alarmBitmap;
    AtChannel *channelList;
    tTab *tabPtr = NULL;
    eAtHistoryReadingMode readingMode;
    const char *pHeading[] = {"DE1 ID","DiscardMessage", "MissMessage"};
    eBool silent = cAtFalse;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Fail to create table\r\n");
            return cAtFalse;
            }
        }

    /* Get data */
    for (i = 0; i < numChannels; i++)
       {
        column = 0;
       StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

       /* Get alarm state */
       if (readingMode == cAtHistoryReadingModeReadToClear)
           alarmBitmap = AtPdhDe1PrmAlarmInterruptClear((AtPdhDe1)channelList[i]);
       else
           alarmBitmap = AtPdhDe1PrmAlarmInterruptGet((AtPdhDe1)channelList[i]);

       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1PrmAlarmDiscardMessage) ? "Set" : "Clear",
                      (alarmBitmap & cAtPdhDe1PrmAlarmDiscardMessage) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtPdhDe1PrmAlarmMissMessage) ? "Set" : "Clear",
                      (alarmBitmap & cAtPdhDe1PrmAlarmMissMessage) ? cSevCritical : cSevInfo);
       }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAPdhDe1PrmCounterGet(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint16 column;
    tTab *tabPtr = NULL;
    uint8 counter_i;
    uint32 counterValue;
    eAtHistoryReadingMode readingMode;
    uint8 pdhDe1CounterType[] = {cAtPdhDe1PrmCounterTxMessage, cAtPdhDe1PrmCounterTxByteMessage,
                                 cAtPdhDe1PrmCounterRxGoodMessage, cAtPdhDe1PrmCounterRxByteMessage,
                                 cAtPdhDe1PrmCounterRxDiscardMessage, cAtPdhDe1PrmCounterRxMissMessage};
    const char* pHeading[] = {"DE1 ID", "TxMessage", "TxByteMessage", "RxGoodMessage", "RxByteMessage", "RxDiscardMessage", "RxMissMessage"};
    uint32 (*CounterRead)(AtPdhDe1 self, uint16 counterType);
    eBool silent = cAtFalse;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if(readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    CounterRead = (readingMode == cAtHistoryReadingModeReadToClear) ? AtPdhDe1PrmCounterClear : AtPdhDe1PrmCounterGet;

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    if (!silent)
        {
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Fail to create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i++)
        {
        column = 0;
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        for (counter_i = 0; counter_i < mCount(pdhDe1CounterType); counter_i++)
            {
            counterValue = CounterRead((AtPdhDe1)channelList[i], pdhDe1CounterType[counter_i]);
            CliCounterPrintToCell(tabPtr, i, column++, counterValue, cAtCounterTypeError, cAtTrue);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPdhDe1LiuLoopbackSet(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    ThaPdhDebugger debugger = Debugger();
    eAtLoopbackMode loopbackMode = cAtLoopbackModeRelease;
    eBool success = cAtTrue;
    AtUnused(argc);

    if (debugger == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Debugger is not support!\r\n");
        return cAtFalse;                                                       \
        }

    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to configure\r\n");
        return cAtTrue;
        }

    loopbackMode = CliLoopbackModeFromString(argv[1]);
    for (i = 0; i < numChannels; i++)
        {
        eAtRet ret = ThaPdhDebuggerDe1LiuLoopbackSet(debugger, (AtPdhDe1)channelList[i], loopbackMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure channel %s, ret = %s\r\n",
                     AtChannelIdString(channelList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPdhDe1LiuLoopbackGet(char argc, char **argv)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    ThaPdhDebugger debugger = Debugger();
    tTab *tabPtr;
    const char *pHeading[] = {"DE1 ID", "LoopbackMode"};
    AtUnused(argc);

    if (debugger == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Debugger is not support!\r\n");
        return cAtFalse;                                                       \
        }

    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to show\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 loopbackMode;

        /* Get ID */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        /* Loopback mode */
        loopbackMode = ThaPdhDebuggerDe1LiuLoopbackGet(debugger, (AtPdhDe1)channelList[i]);
        StrToCell(tabPtr, i, 1, CliLoopbackModeString(loopbackMode));
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPdhDe1PrmControllerCaptureRxMessages(char argc, char **argv)
    {
    AtPdhPrmController prmController;
    AtList messages;

    AtUnused(argc);

    prmController = CapturePrmController(argv[0]);
    if (prmController == NULL)
        return cAtFalse;

    if (!AtPdhPrmControllerCaptureIsStarted(prmController))
        {
        AtPrintc(cSevWarning, "WARNING: Capturing has not been started yet\r\n");
        return cAtTrue;
        }

    messages = AtPdhPrmControllerCaptureRxMessages(prmController);
    AtPktAnalyzerAllPacketsDisplay(messages, cAtPktAnalyzerDisplayModeHumanReadable);

    return cAtTrue;
    }

eBool CmdAtPdhDe1PrmControllerCaptureStart(char argc, char **argv)
    {
    return PrmCaptureStart(argc, argv, cAtTrue);
    }

eBool CmdAtPdhDe1PrmControllerCaptureStop(char argc, char **argv)
    {
    return PrmCaptureStart(argc, argv, cAtFalse);
    }

eBool CmdAtPdhDe1FastSearchEnable(char argc, char **argv)
    {
    return FastSearchEnable(argc, argv, cAtTrue);
    }

eBool CmdAtPdhDe1FastSearchDisable(char argc, char **argv)
    {
    return FastSearchEnable(argc, argv, cAtFalse);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : CliAtModulePdhDebug.c
 *
 * Created Date: Sep 26, 2016 
 *
 * Description : Implementation of Debug Module PDH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhChannel.h"
#include "../../../driver/src/generic/pdh/AtModulePdhInternal.h"
#include "../../../driver/src/generic/pdh/AtPdhChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef AtPdhChannel (*PdhChannelGetFunc)(AtModulePdh module, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtModulePdhLiuFrequencyDe1SetDebug(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 de1Id;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    AtPdhDe1 de1;
    AtUnused(argc);

    /* Get ID buffers */
    de1Id = CliId2DriverId(AtStrToDw(argv[0]));
    de1 = AtModulePdhDe1Get(pdhModule, de1Id);
    if (de1 == NULL)
        {
        AtPrintc(cSevCritical, "Channel is not exist or invalid DS1/E1 Id, expected: 1 or 2,4, ...\n");
        return cAtFalse;
        }

    /* Apply */
    ret = AtModulePdhLiuFrequencyMonitorSet(pdhModule, (AtPdhChannel)de1);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Set Liu Frequency Monitor for Liu %d fail, ret = %s\r\n",
                 de1Id,
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePdhLiuFrequencyDe3SetDebug(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 de3Id;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    AtPdhDe3 de3;
    AtUnused(argc);

    /* Get ID buffers */
    de3Id = CliId2DriverId(AtStrToDw(argv[0]));
    de3 = AtModulePdhDe3Get(pdhModule, de3Id);
    if (de3 == NULL)
        {
        AtPrintc(cSevCritical, "Channel is not exist or invalid DS3/E3 Id, expected: 1 or 2,4, ...\n");
        return cAtFalse;
        }

    /* Apply */
    ret = AtModulePdhLiuFrequencyMonitorSet(pdhModule, (AtPdhChannel)de3);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Set Liu Frequency Monitor for Liu %d fail, ret = %s\r\n",
                 de3Id,
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePdhLiuFrequencyDebug(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint8 colIdx;
    tTab *tabPtr;
    tAtModulePdhLiuMonitor clockMonitor;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    const char *pHeading[] = {"Id", "TxFreq", "RxFreq"};
    AtOsalMemInit(&clockMonitor, 0, sizeof(clockMonitor));
    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    ret = AtModulePdhLiuFrequencyMonitorGet(pdhModule, &clockMonitor);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Fail to get frequency\r\n");
        return cAtFalse;
        }

    if (clockMonitor.channel == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Pdh channel is Null\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    colIdx = 0;
    StrToCell(tabPtr, 0, colIdx++, (char *)CliChannelIdStringGet((AtChannel)clockMonitor.channel));
    StrToCell(tabPtr, 0, colIdx++, CliNumber2String(clockMonitor.txClockRate, "%u"));
    StrToCell(tabPtr, 0, colIdx++, CliNumber2String(clockMonitor.rxClockRate, "%u"));

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

static eBool LiuRxClockDebug(PdhChannelGetFunc PdhChannelGet)
    {
    const uint32 cNumChannels = 48;
    uint32 i;
    uint8 colIdx;
    tTab *tabPtr;
    eBool isLOC;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    const char *pHeading[] = {"Channel", "RxLOC"};

    /* Create table with titles */
    tabPtr = TableAlloc(cNumChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    /* Get data */
    for (i = 0; i < cNumChannels; i++)
        {
        AtPdhChannel pdhChannel = PdhChannelGet(pdhModule, i);
        if (pdhChannel == NULL)
            continue;

        /* Get ID */
        colIdx = 0;
        StrToCell(tabPtr, i, colIdx++, CliNumber2String(i + 1, "%u"));

        isLOC = AtPdhChannelLiuIsLoc(pdhChannel);
        ColorStrToCell(tabPtr, i, colIdx++, isLOC ? "yes" : "no", isLOC ? cSevCritical : cSevNormal);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

eBool CmdAtModulePdhLiuRxClockDe1Debug(char argc, char **argv)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    uint32 numDe1 = AtModulePdhNumberOfDe1sGet(pdhModule);
    AtUnused(argc);
    AtUnused(argv);

    if (numDe1 == 0)
        {
        AtPrintc(cSevCritical, "Pdh De1 channel is not exist\r\n");
        return cAtFalse;
        }

    return LiuRxClockDebug((PdhChannelGetFunc)AtModulePdhDe1Get);
    }

eBool CmdAtModulePdhLiuRxClockDe3Debug(char argc, char **argv)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), (eAtModule)cAtModulePdh);
    uint32 numDe3 = AtModulePdhNumberOfDe3sGet(pdhModule);
    AtUnused(argc);
    AtUnused(argv);

    if (numDe3 == 0)
        {
        AtPrintc(cSevCritical, "Pdh De3 channel is not exist\r\n");
        return cAtFalse;
        }

    return LiuRxClockDebug((PdhChannelGetFunc)AtModulePdhDe3Get);
    }

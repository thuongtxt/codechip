# @group None "None"
4 debug pdh de1 prm CmdPdhDe1PrmDebugChannelSelect 1 deIdList=0
/*
    Syntax     : debug pdh de1 prm <deIdList>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Monitor PRM on channel that selected
    Example    : debug pdh de1 prm 1
*/

6 debug pdh de1 prm monitor enable CmdPdhDe1PrmDebugMonitorEnable 0
/*
    Syntax     : debug pdh de1 prm monitor enable
    Parameter  : none
    Description: Enable monitor PRM 
    Example    : debug pdh de1 prm monitor enable
*/

6 debug pdh de1 prm monitor disable CmdPdhDe1PrmDebugMonitorDisable 0
/*
    Syntax     : debug pdh de1 prm monitor disable
    Parameter  : none
    Description: Disable monitor PRM 
    Example    : debug pdh de1 prm monitor disable
*/

6 debug pdh de1 prm force mode CmdPdhDe1PrmDebugForceMode 1 mode=dis
/*
    Syntax     : debug pdh de1 prm force mode <8bit/10bit/dis>
    Parameter  : mode - Mode to monitor
                        + 8bit
                        + 10bit
                        + dis
    Description: Enable/Disable force full or part of PRM payload
    Example    : debug pdh de1 prm force mode dis
*/

5 debug pdh de1 prm force CmdPdhDe1PrmDebugForcePaload 1 prmPayloadBits=G1
/*
    Syntax     : debug pdh de1 prm force <G1|G2|G3|G4|G5|G6|SE|FE|LV|SL|LB>
    Parameter  : prmPayloadBits - List of PRM payload bit:
                                  G1 G2 G3 G4 G5 G6
                                  SE FE LV SL LB
    Description: Forces value for PRM payload 
    Example    : debug pdh de1 prm force G1
*/

5 debug pdh de1 prm unforce CmdPdhDe1PrmDebugUnForcePaload 1 prmPayloadBits=G1
/*
    Syntax     : debug pdh de1 prm unforce <G1|G2|G3|G4|G5|G6|SE|FE|LV|SL|LB>
    Parameter  : prmPayloadBits - List of PRM payload bit:
                                  G1 G2 G3 G4 G5 G6
                                  SE FE LV SL LB
    Description: Un-Forced value for PRM payload 
    Example    : debug pdh de1 prm unforce G1
*/

6 debug pdh de1 prm force nmni CmdPdhDe1PrmDebugForceNmNi 1 values=0
 /*
    Syntax     : debug pdh de1 prm force nmni 0
    Parameter  : values   - Value forced for NM/NI bit
    Description: Forced value for PRM NMNI payload bit
    Example    : debug pdh de1 prm force nmni 0
*/

5 show pdh de1 debug prm CmdPdhDe1PrmDebugGet 0
/*
    Syntax     : debug show pdh de1 prm
    Parameter  : none
    Description: Get prm de1 debug
    Example    : debug show pdh de1 prm
*/

6 show pdh de1 debug prm counters CmdAPdhDe1PrmCounterGet -1 de1List=1 counterMod=r2c silent 
/*
    Syntax     : show pdh de1 debug prm counters <de1List> <r2c/ro> [silent]
    Parameter  : de1List    - Input DS1/E1 identifier. Format of one ID item is as following
                              + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                              + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                                The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 counterMod - Reading action mode:
                              + r2c: Read to Clear
                              + ro: Read only
                 silent     - If "silent" is specified, there would be no 
                              output with "r2c" mode
    Description: Get counters PRM on E1 Channels.
    Example    : show pdh de1 debug prm counters 1 r2c
*/

6 show pdh de1 debug prm interrupt CmdPdhDe1PrmInterruptGet -1 de1List=1 action=r2c silent
/*
    Syntax     : show pdh de1 debug prm interrupt <de1List> <r2c/ro> [silent]
    Parameter  : de1List  - Input DS1/E1 identifier. Format of one ID item is as following
                            + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                            + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                              The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 action   - Reading action mode:
                             + r2c: Read to Clear
                             + ro: Read only
                silent    - If "silent" is specified, there would be no 
                            output with "r2c" mode
    Description: Get history alarm PRM on E1 Channels.
    Example    : show pdh de1 debug prm interrupt 1 r2c
*/

5 debug pdh de1 liu loopback CmdPdhDe1LiuLoopbackSet 2 de1List=1 loopbackMode=release
/*
    Syntax     : debug pdh de1 liu loopback <de1List> <release/local/remote>
    Parameter  : de1List      - Input DS1/E1 identifier. Format of one ID item is as following
                               + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                 loopbackMode - Line loopback
                               + release
                               + local
                               + remote
    Description: Loopback DE1 at LIU interface
    Example    : debug pdh de1 liu loopback 1 local
*/

6 debug show pdh de1 liu loopback CmdPdhDe1LiuLoopbackGet 1 de1List=1
/*
    Syntax     : debug show pdh de1 liu loopback <de1List>
    Parameter  : de1List      - Input DS1/E1 identifier. Format of one ID item is as following
                               + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
    Description: Show loopback configuration of DE1 at LIU interface
    Example    : debug show pdh de1 liu loopback 1-10
*/

4 pdh de1 prm capture CmdAtPdhDe1PrmControllerCaptureRxMessages 1 de1=1
/*
    Syntax     : pdh de1 prm capture <de1>
    Parameter  : de1 - Input DS1/E1 identifier. Format of one ID item is as following
                       + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                       + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                         The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                       Only one DS1/E1 can be input at a time.
    Description: Capture PRM messages
    Example    : pdh de1 prm capture 1
*/

5 pdh de1 prm capture start CmdAtPdhDe1PrmControllerCaptureStart 1 de1=1
/*
    Syntax     : pdh de1 prm capture start <de1>
    Parameter  : de1 - Input DS1/E1 identifier. Format of one ID item is as following
                       + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                       + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                         The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                       Only one DS1/E1 can be input at a time.
    Description: Start capturing PRM messages
    Example    : pdh de1 prm capture start 1
*/

5 pdh de1 prm capture stop CmdAtPdhDe1PrmControllerCaptureStop 1 de1=1
/*
    Syntax     : pdh de1 prm capture stop <de1>
    Parameter  : de1 - Input DS1/E1 identifier. Format of one ID item is as following
                       + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                       + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                         The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                       Only one DS1/E1 can be input at a time.
    Description: Stop capturing PRM messages
    Example    : pdh de1 prm capture stop 1
*/

5 debug pdh de1 fastsearch enable CmdAtPdhDe1FastSearchEnable 1 de1=1
/*
    Syntax     : debug pdh de1 fastsearch enable <de1>
    Parameter  : de1 - Input DS1/E1 identifier. Format of one ID item is as following
                       + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                       + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                         The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Enable fast search engine 
    Example    : debug pdh de1 fastsearch enable 1
                 debug pdh de1 fastsearch enable 1.1.1.1.1
*/

5 debug pdh de1 fastsearch disable CmdAtPdhDe1FastSearchDisable 1 de1=1
/*
    Syntax     : debug pdh de1 fastsearch disable <de1>
    Parameter  : de1 - Input DS1/E1 identifier. Format of one ID item is as following
                       + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                       + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                         The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Disable fast search engine 
    Example    : debug pdh de1 fastsearch disable 1
                 debug pdh de1 fastsearch disable 1.1.1.1.1
*/

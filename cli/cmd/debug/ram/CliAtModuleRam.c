/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : CliAtModuleRam.c
 *
 * Created Date: Dec 16, 2016
 *
 * Description : Debug CLIs for RAM
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../ram/CliAtModuleRam.h"
#include "../../../../driver/src/generic/ram/AtModuleRamInternal.h"
#include "../../../../driver/src/generic/ram/AtRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleRam RamModule(void)
    {
    return (AtModuleRam)AtDeviceModuleGet(CliDevice(), cAtModuleRam);
    }

static eBool DdrErrorLatchingEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleRamDdrErrorLatchingEnable(RamModule(), enabled);
    return cAtTrue;
    }

static eBool QdrErrorLatchingEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtModuleRamQdrErrorLatchingEnable(RamModule(), enabled);
    return cAtTrue;
    }

static eBool HwAssistAcceptanceTest(char *ramString, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, ram_i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);

    /* Get RAMs */
    numRams = CliIdListFromString(ramString, ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            continue;

        AtRamHwAssistAcceptanceTest(ram);
        }

    AtPrintc(cSevInfo, "HW assist tool is basically working\r\n");
    return cAtTrue;
    }

eBool CmdAtModuleRamDdrErrorLatchingEnable(char argc, char **argv)
    {
    return DdrErrorLatchingEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleRamDdrErrorLatchingDisable(char argc, char **argv)
    {
    return DdrErrorLatchingEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleRamQdrErrorLatchingEnable(char argc, char **argv)
    {
    return QdrErrorLatchingEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleRamQdrErrorLatchingDisable(char argc, char **argv)
    {
    return QdrErrorLatchingEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDdrHwAssistAcceptanceTest(char argc, char **argv)
    {
    AtUnused(argc);
    return HwAssistAcceptanceTest(argv[0], AtModuleRamDdrGet);
    }

eBool CmdAtQdrHwAssistAcceptanceTest(char argc, char **argv)
    {
    AtUnused(argc);
    return HwAssistAcceptanceTest(argv[0], AtModuleRamQdrGet);
    }

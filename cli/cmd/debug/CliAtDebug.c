/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliAtDebug.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : CLIs for hardware debugging purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "commacro.h"
#include "../../../driver/src/generic/man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtModuleEthRet AtEthPortDicEnable(AtEthPort self, eBool enable);

/*--------------------------- Implementation ---------------------------------*/
static eBool DicEnable(char argc, char **argv, eBool dicEnable)
    {
    uint32 bufferSize;
    uint32 numPorts, port_i;
    AtChannel *ports;
    eAtRet ret = cAtOk;
	AtUnused(argc);

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv[0], ports, bufferSize);

    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        ret |= AtEthPortDicEnable((AtEthPort)ports[port_i], dicEnable);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdDdrCounterFlush(char argc, char **argv)
    {
    AtHal      hal;
	AtUnused(argv);
	AtUnused(argc);

    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    if (hal == NULL)
        {
        AtPrintc(cSevCritical, "Null Pointer");
        return cAtFalse;
        }

    AtHalWrite(hal, 0x00C00020, 0x10000);
    AtOsalSleep(1);
    AtHalWrite(hal, 0x00C00020, 0x0);

    return cAtTrue;
    }

eBool CmdAtDebugDicEnable(char argc, char **argv)
    {
    return DicEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDebugDicDisable(char argc, char **argv)
    {
    return DicEnable(argc, argv, cAtFalse);
    }
eBool CmdAtSleep(char argc, char **argv)
    {
    tAtOsalCurTime  startTime, curTime;
    uint32 timeToSleep = AtStrToDw(argv[0]);
	AtUnused(argc);

    AtOsalCurTimeGet(&startTime);
    AtOsalSleep(timeToSleep);
    AtOsalCurTimeGet(&curTime);

    AtPrintc(cSevInfo, "Elapse time = %u(ms)\r\n", mTimeIntervalInMsGet(startTime, curTime));

    return cAtTrue;
    }

eBool CmdAtUSleep(char argc, char **argv)
    {
    tAtOsalCurTime  startTime, curTime;
    uint32 timeToSleep = AtStrToDw(argv[0]);
	AtUnused(argc);

    AtOsalCurTimeGet(&startTime);
    AtOsalUSleep(timeToSleep);
    AtOsalCurTimeGet(&curTime);

    AtPrintc(cSevWarning, "Elapse time = %u(ms)\r\n", mTimeIntervalInMsGet(startTime, curTime));

    return cAtTrue;
    }

eBool CmdAtDebugLoggingTimeEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return CliTimeLoggingEnable(cAtTrue);
    }

eBool CmdAtDebugLoggingTimeDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return CliTimeLoggingEnable(cAtFalse);
    }

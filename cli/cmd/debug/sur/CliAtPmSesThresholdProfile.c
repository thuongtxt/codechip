/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : CliAtPmSesThresholdProfile.c
 *
 * Created Date: Aug 31, 2017
 *
 * Description : Debug CLI implement for Surveillance SES threshold.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../sur/CliAtModuleSur.h"
#include "../sur/CliAtSurEngine.h"
#include "../../../../driver/src/generic/sur/AtModuleSurInternal.h"
#include "../../../../driver/src/generic/sur/AtPmSesThresholdProfile.h"
#include "../../../../driver/src/generic/sur/AtSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*SesThresholdValueSet)(AtPmSesThresholdProfile, uint32);
typedef eAtRet (*SesThresholdProfileSet)(AtSurEngine, AtPmSesThresholdProfile);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ProfilesFromString(char *pStrIdList, AtPmSesThresholdProfile *profiles, uint32 bufferSize)
    {
    uint32 *idBuffer, numIds, profile_i;
    uint32 numValidProfiles = 0;
    AtModuleSur moduleSur = CliModuleSur();

    if (pStrIdList == NULL)
        return 0;

    /* It is just a list of flat ID */
    idBuffer = CliSharedIdBufferGet(&numIds);
    numIds = CliIdListFromString(pStrIdList, idBuffer, numIds);
    if (numIds > bufferSize)
        numIds = bufferSize;

    /* Just have valid profile */
    for (profile_i = 0; profile_i < numIds; profile_i++)
        {
        AtPmSesThresholdProfile profile = AtModuleSurSesThresholdProfileGet(moduleSur, idBuffer[profile_i] - 1);
        if (profile)
            {
            profiles[numValidProfiles] = profile;
            numValidProfiles = numValidProfiles + 1;
            }
        else
            AtPrintc(cSevWarning,  "Profile %u does not exist\r\n", idBuffer[profile_i]);
        }

    if (numValidProfiles == 0)
        {
        AtPrintc(cSevWarning,
                 "WARNING: No Profile is found with input %s, expected: 1 or 1-%u, ...\r\n",
                 pStrIdList,
                 AtModuleSurNumSesThresholdProfiles(moduleSur));
        }

    return numValidProfiles;
    }

static eBool ValueSet(char argc, char **argv, SesThresholdValueSet thresholdSet)
    {
    uint32 i;
    AtPmSesThresholdProfile *profiles;
    uint32 numProfiles;
    uint32 threshold;
    eBool success = cAtTrue;

    /* Get the shared ID buffer */
    AtUnused(argc);
    profiles = (AtPmSesThresholdProfile*) CliSharedObjectListGet(&numProfiles);
    numProfiles = ProfilesFromString(argv[0], profiles, numProfiles);
    if (numProfiles == 0)
        return cAtFalse;

    threshold = AtStrtoul(argv[1], NULL, 10);
    for (i = 0; i < numProfiles; i++)
        {
        AtPmSesThresholdProfile profile = profiles[i];
        eAtRet ret = thresholdSet(profile, threshold);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "SES Threshold for Profile %u cannot be set new threshold %u, ret = %s\r\n",
                     AtPmSesThresholdProfileIdGet(profile) + 1,
                     threshold,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static AtChannel *ChannelsParse(char *idString, ChannelsIdParseFunc idParseFunc, uint32 *numChannels)
    {
    AtChannel* channels = CliSharedChannelListGet(numChannels);

    *numChannels = idParseFunc(idString, channels, *numChannels);
    if (*numChannels == 0)
        {
        AtPrintc(cSevWarning, "No engine\r\n");
        return NULL;
        }

    return channels;
    }

static eBool ProfileSet(char argc, char **argv, ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    uint32 numProfiles;
    AtPmSesThresholdProfile *profiles = (AtPmSesThresholdProfile*)CliSharedObjectListGet(&numProfiles);

    AtUnused(argc);

    /* Get the shared ID buffer */
    numProfiles = ProfilesFromString(argv[1], profiles, numProfiles);
    if (numProfiles != 1)
        return cAtFalse;

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[channel_i];
        AtSurEngine engine = AtChannelSurEngineGet(channel);
        eAtRet ret = AtSurEngineSesThresholdProfileSet(engine, profiles[0]);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Set %s failed on channel %s, ret = %s\r\n",
                     CliChannelIdStringGet(channel),
                     AtObjectToString((AtObject)profiles[0]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool Show(char argc, char **argv, ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    const char *pHeading[] = {"engineId", "profileId"};
    tTab *tabPtr = NULL;

    AtUnused(argc);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "No channel for show with input %s\r\n", argv[0]);
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 column = 0;
        AtSurEngine engine = AtChannelSurEngineGet(channels[channel_i]);
        AtPmSesThresholdProfile profile = AtSurEngineSesThresholdProfileGet(engine);

        /* Put channel ID */
        StrToCell(tabPtr, channel_i, column++, (char *)CliChannelIdStringGet(channels[channel_i]));
        if (profile)
            StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtPmSesThresholdProfileIdGet(profile) + 1, "%d"));
        else
            StrToCell(tabPtr, channel_i, column++, "None");
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

eBool CmdPdhDe3PmSesThresholdProfileSet(char argc, char **argv)
    {
    return ProfileSet(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe1PmSesThresholdProfileSet(char argc, char **argv)
    {
    return ProfileSet(argc, argv, De1ListFromString);
    }

eBool CmdSdhLinePmSesThresholdProfileSet(char argc, char **argv)
    {
    return ProfileSet(argc, argv, CliAtSdhLinesFromString);
    }

eBool CmdSdhPathPmSesThresholdProfileSet(char argc, char **argv)
    {
    return ProfileSet(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdPwPmSesThresholdProfileSet(char argc, char **argv)
    {
    return ProfileSet(argc, argv, CliTdmPwsFromString);
    }

eBool CmdPdhDe3PmSesThresholdProfileShow(char argc, char **argv)
    {
    return Show(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe1PmSesThresholdProfileShow(char argc, char **argv)
    {
    return Show(argc, argv, De1ListFromString);
    }

eBool CmdSdhLinePmSesThresholdProfileShow(char argc, char **argv)
    {
    return Show(argc, argv, CliAtSdhLinesFromString);
    }

eBool CmdSdhPathPmSesThresholdProfileShow(char argc, char **argv)
    {
    return Show(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdPwPmSesThresholdProfileShow(char argc, char **argv)
    {
    return Show(argc, argv, CliTdmPwsFromString);
    }

eBool CmdSurThresholdProfileLineSesSSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfileSdhSectionThresholdSet);
    }

eBool CmdSurThresholdProfileLineSesLSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfileSdhLineThresholdSet);
    }

eBool CmdSurThresholdProfilePathSesPSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfileSdhHoPathThresholdSet);
    }

eBool CmdSurThresholdProfilePathSesVSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfileSdhLoPathThresholdSet);
    }

eBool CmdSurThresholdProfileDe3SesLSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfilePdhDe3LineThresholdSet);
    }

eBool CmdSurThresholdProfileDe3SesPSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfilePdhDe3PathThresholdSet);
    }

eBool CmdSurThresholdProfileDe1SesLSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfilePdhDe1LineThresholdSet);
    }

eBool CmdSurThresholdProfileDe1SesPSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfilePdhDe1PathThresholdSet);
    }

eBool CmdSurThresholdProfilePwSesSet(char argc, char **argv)
    {
    return ValueSet(argc, argv, AtPmSesThresholdProfilePwThresholdSet);
    }

eBool CmdSurSesThresholdProfileShow(char argc, char **argv)
    {
    uint32 numProfiles;
    AtPmSesThresholdProfile *profiles = (AtPmSesThresholdProfile*)CliSharedObjectListGet(&numProfiles);
    uint32 profile_i;
    tTab *tabPtr;
    const char *pHeading[] = {"ProfileId", "SDH SES-S", "SDH SES-L", "SDH SES-P", "SDH SES-V",
                              "PDH DE3 SES-L", "PDH DE3 SES-P", "PDH DE1 SES-L", "PDH DE1 SES-P", "PW SES"};
    AtUnused(argc);

    /* Get the shared ID buffer */
    numProfiles = ProfilesFromString(argv[0], profiles, numProfiles);
    if (numProfiles == 0)
        return cAtFalse;

    /* Create table with titles */
    tabPtr = TableAlloc(numProfiles, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (profile_i = 0; profile_i < numProfiles; profile_i = AtCliNextIdWithSleep(profile_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 threshold;
        AtPmSesThresholdProfile profile = profiles[profile_i];
        uint32 col_i = 0;

        /* Put channel ID */
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(AtPmSesThresholdProfileIdGet(profile) + 1, "%u"));

        /* Show threshold */
        threshold = AtPmSesThresholdProfileSdhSectionThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfileSdhLineThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfileSdhHoPathThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfileSdhLoPathThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfilePdhDe3LineThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfilePdhDe3PathThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfilePdhDe1LineThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfilePdhDe1PathThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));

        threshold = AtPmSesThresholdProfilePwThresholdGet(profile);
        StrToCell(tabPtr, profile_i, col_i++, CliNumber2String(threshold, "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

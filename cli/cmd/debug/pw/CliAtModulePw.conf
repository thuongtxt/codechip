# @group None "None"

4 debug pw timeout jitter_buffer_empty CmdThaModulePwJitterEmptyTimeoutSet 1 timeoutMs
/*
    Syntax     : debug pw timeout jitter_buffer_empty <timeoutMs>
    Parameter  : timeoutMs - Timeout to wait for jitter buffer be empty
    Description: Set timeout to wait for jitter buffer be empty. Note, set to 0
                 to use default timeout.
    Example    : debug pw timeout jitter_buffer_empty 100
*/

4 debug show pw internalcounters CmdThaDebugPwInternalCounterGet 2 pwList=1 readingMode=r2c
/*
    Syntax     : debug show pw internalcounters <pwList> <readingMode>
    Parameter  : pwList - List of PW IDs
                           + <Id>:   Flat Id for TDM PW (SAToP, CEP, CESoP)
                           + hdlc.<hdlcId>:   HDLC PW
                           + fr.<FrLinkId>:   FR Link PW
                           + mfr.<MfrBundleId>:   MFR Bundle PW
                           + fr.<FrLinkId>.<DlciId>:  FR Link Virtual Circuit PW
                           + mfr.<MfrBundleId>.<DlciId>:   MFR Bundle Virtual Circuit PW      
                 readingMode    - Reading Mode
                           + r2c: read to clear
                           + ro : read only
    Description: Show debug counters for PW
    Example    : debug show pw internalcounters 1 r2c
*/

4 debug show pw counterpools CmdDebugPwInternalMemoryPoolGet 0
/*
    Syntax     : debug show pw counterpools
    Parameter  : None
    Description: Show memory pool of internal counters
    Example    : debug show pw counterpools
*/

4 debug pw counterpool set CmdDebugPwInternalCounterPoolSet -1 pwList=1-32 poolList=0-31
/*
    Syntax     : debug pw counterpool set
    Parameter  : pwList - List of PW IDs
                           + <Id>:   Flat Id for TDM PW (SAToP, CEP, CESoP)
                           + hdlc.<hdlcId>:   HDLC PW
                           + fr.<FrLinkId>:   FR Link PW
                           + mfr.<MfrBundleId>:   MFR Bundle PW
                           + fr.<FrLinkId>.<DlciId>:  FR Link Virtual Circuit PW
                           + mfr.<MfrBundleId>.<DlciId>:   MFR Bundle Virtual Circuit PW
                 poolList - List of memory pool ID [0-31]
    Description: Allocate PW memory pool of internal counters
    Example    : debug pw counterpool set 1-32
                 debug pw counterpool set 1 5 
*/

4 debug pw counterpool clear CmdDebugPwInternalCounterPoolClear 1 pwList=1-32 
/*
    Syntax     : debug pw counterpool clear
    Parameter  : pwList - List of PW IDs
                           + <Id>:   Flat Id for TDM PW (SAToP, CEP, CESoP)
                           + hdlc.<hdlcId>:   HDLC PW
                           + fr.<FrLinkId>:   FR Link PW
                           + mfr.<MfrBundleId>:   MFR Bundle PW
                           + fr.<FrLinkId>.<DlciId>:  FR Link Virtual Circuit PW
                           + mfr.<MfrBundleId>.<DlciId>:   MFR Bundle Virtual Circuit PW
    Description: Deallocate PW memory pool of internal counters
    Example    : debug pw counterpool clear 1 
*/

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPw.c
 *
 * Created Date: Dec 31, 2016
 *
 * Description : PW debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtCli.h"
#include "AtTokenizer.h"
#include "../../pw/CliAtModulePw.h"
#include "../../../../driver/src/implement/default/pw/adapters/ThaPwAdapter.h"
#include "../../../../driver/src/implement/default/pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *PwHeaderString(AtPw pw, uint32 *headerLengthInBytes)
    {
    ThaPwAdapter pwAdaper = ThaPwAdapterGet(pw);
    ThaPwHeaderController headerController = ThaPwAdapterHeaderController(pwAdaper);
    uint8 headerLen = 0, byte_i;
    uint8 *headerBuffer;
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);

    /* Read header from HW */
    headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(headerController, &headerLen);
    if ((headerBuffer == NULL) || (headerLen == 0))
        return NULL;

    /* Build string */
    buffer[0] = '\0';
    for (byte_i = 0; byte_i < headerLen; byte_i++)
        {
        char *byteString = NULL;

        if (byte_i == 0)
            byteString = AtCliNumber2String("%02X", headerBuffer[byte_i]);
        else
            byteString = AtCliNumber2String(".%02X", headerBuffer[byte_i]);

        AtStrncat(buffer, byteString, bufferSize);
        bufferSize = bufferSize - AtStrlen(byteString);
        }

    *headerLengthInBytes = headerLen;

    return buffer;
    }

eBool CmdAtPwHwHeaderShow(char argc, char **argv)
    {
    AtList pws = CliPwListFromString(argv[0]);
    uint32 numPws = AtListLengthGet(pws), pw_i;
    tTab *tabPtr;
    const char *heading[] = {"PW", "length", "header"};

    AtUnused(argc);

    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pws);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pws, pw_i);
        uint32 column = 0;
        uint32 headerLength = 0;
        char *headerString = PwHeaderString(pw, &headerLength);

        StrToCell(tabPtr, pw_i, column++, CliChannelIdStringGet((AtChannel)pw));
        StrToCell(tabPtr, pw_i, column++, CliNumber2String(headerLength, "%d"));
        StrToCell(tabPtr, pw_i, column++, headerString);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pws);

    return cAtTrue;
    }

eBool CmdAtPwHwHeaderSet(char argc, char **argv)
    {
    AtList pws = CliPwListFromString(argv[0]);
    uint32 numPws = AtListLengthGet(pws), pw_i;
    AtTokenizer headerParser = AtTokenizerSharedTokenizer(argv[1], ",");
    uint32 headerBufferSize;
    uint8 *headerBuffer = (uint8 *)CliSharedCharBufferGet(&headerBufferSize);
    eBool success = cAtTrue;

    AtUnused(argc);

    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    if (numPws != AtTokenizerNumStrings(headerParser))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of PWs (%d) and headers (%d) do not match\r\n",
                 numPws,
                 AtTokenizerNumStrings(headerParser));
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pws, pw_i);
        char *headerString;
        ThaPwAdapter pwAdaper = ThaPwAdapterGet(pw);
        ThaPwHeaderController headerController = ThaPwAdapterHeaderController(pwAdaper);
        eAtRet ret;

        /* Parse header string to bytes and set it */
        headerString = AtTokenizerNextString(headerParser);
        headerBufferSize = AtString2Bytes(headerString, headerBuffer, headerBufferSize, 16);
        ret = ThaPwHeaderControllerHwRawHeaderSet(headerController, headerBuffer, (uint8)headerBufferSize);
        if (ret == cAtOk)
            continue;

        /* Failure handling */
        AtPrintc(cSevCritical, "ERROR: Configure raw header on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)pw),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)pws);

    return success;
    }

eBool CmdAtPwBufferOverrunForce(char argc, char **argv)
    {
    uint32 numPws, pw_i;
    AtPw *pwList;
    eBool blResult = cAtTrue;
    eBool force = cAtFalse;

    AtUnused(argc);
    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Enable/disable */
    mAtStrToBool(argv[1], force, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: expect en/dis, not %s\r\n", argv[1]);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = (AtPw)pwList[pw_i];
        ThaPwAdapter pwAdapter = ThaPwAdapterGet(pw);
        eAtRet ret;

        if (!force)
            ret = ThaPwAdapterBufferOverrunForce(pwAdapter, force);
        else
            ret = AtChannelTxTrafficEnable(AtPwBoundCircuitGet(pw), cAtFalse);

        if (ret == cAtOk)
            continue;

        /* Failure handling */
        AtPrintc(cSevCritical, "ERROR: Force/unforce buffer overrun on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)pw),
                 AtRet2String(ret));
        blResult = cAtFalse;
        }

    return blResult;
    }


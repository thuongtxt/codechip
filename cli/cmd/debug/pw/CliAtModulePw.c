/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliAtModulePw.c
 *
 * Created Date: Oct 23, 2015
 *
 * Description : PW debug
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "../../cli/cmd/pw/CliAtModulePw.h"
#include "../../../../driver/src/implement/default/pw/ThaModulePw.h"
#include "../../../../driver/src/implement/codechip/Tha60210012/pw/Tha60210012ModulePw.h"
#include "../../../driver/src/implement/default/man/ThaDevice.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounterValueToCell(value, color)                                   \
    do                                                                         \
        {                                                                      \
        AtSprintf(buf, "%u", value);                                         \
        ColorStrToCell(tabPtr, counter_i++, pw_i + 1UL, buf, value ? color : cSevNormal);           \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdThaModulePwJitterEmptyTimeoutSet(char argc, char **argv)
    {
    uint32 timeoutMs = AtStrToDw(argv[0]);
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtUnused(argc);
    ThaModulePwJitterEmptyTimeoutSet(pwModule, timeoutMs);
    return cAtTrue;
    }

static char** BuildHeadingForPws(AtPw *pwList, uint32 currentPwIndex, uint32 numPwsPerTable)
    {
    AtPw   pw;
    uint32 i;
    char   **headings;

    headings = AtOsalMemAlloc((numPwsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(8);
    AtSprintf(headings[0], "PW");

    for (i = 0; i < numPwsPerTable; i++)
        {
        pw = pwList[currentPwIndex + i];
        headings[i + 1] = AtOsalMemAlloc(8);
        AtSprintf(headings[i + 1], "%u", AtChannelIdGet((AtChannel)pw) + 1);
        }

    return headings;
    }

static void DestroyHeadings(char **heading, uint32 numColumns)
    {
    uint32 i;

    for (i = 0; i < numColumns; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static eBool CounterShow(AtPw *pwList, uint32 numPws, eAtHistoryReadingMode readingMode)
    {
    const char *counterNames[] = {/* Common counters */
                                  "PoolId", "TxPackets", "TxPayloadBytes",
                                  "TxPacketFragment", "TxLbitPackets", "TxRbitPackets",
                                  "TxMbitPackets", "TxPbitPackets",
                                  "RxPackets", "RxPayloadBytes", "RxLofsPacket",
                                  "RxLbitPackets", "RxNbitPackets", "RxPbitPackets",
                                  "RxRbitPackets", "RxLatePackets", "RxEarlyPackets",
                                  "RxLostPackets", "RxJitBufOverrunEvents", "RxJitBufUnderrunEvents",
                                  "RxMalformedPackets", "RxStrayPackets",
                                  };
    uint32 numTables, table_i;
    uint32  remainPws = numPws;
    const uint32 cNumPwsPerTable = 11;

    /* Calculate number of tables need to show counters of all input PWs */
    numTables = numPws / cNumPwsPerTable;
    if ((numPws % cNumPwsPerTable) > 0)
        numTables = numTables + 1;

    for (table_i = 0; table_i < numTables; table_i = AtCliNextIdWithSleep(table_i, 1))
        {
        tTab *tabPtr;
        uint32 numPwsPerTable;
        uint16 pw_i;
        char **heading;
        uint8 counter_i = 0;
        static char buf[32];
        uint32 currentPwIndex = table_i * cNumPwsPerTable;

        numPwsPerTable = remainPws;
        if (numPwsPerTable > cNumPwsPerTable)
            numPwsPerTable = cNumPwsPerTable;

        /* Create table */
        heading = BuildHeadingForPws(pwList, currentPwIndex, numPwsPerTable);
        tabPtr = TableAlloc(mCount(counterNames), numPwsPerTable + 1, (void *)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }

        /* Build column of counter names */
        for (counter_i = 0; counter_i < mCount(counterNames); counter_i++)
            StrToCell(tabPtr, counter_i, 0, counterNames[counter_i]);

        /* Put counters of current PWs */
        for (pw_i = 0; pw_i < numPwsPerTable; pw_i++)
            {
            AtPw pw;
            uint32 poolId;
            tThaDebugPwInternalCnt counters;
            AtOsalMemInit(&counters, 0, sizeof(tThaDebugPwInternalCnt));

            /* Get PW */
            pw = pwList[currentPwIndex + pw_i];

            /* Read counters */
            Tha60210012DebugPwInternalCounterGet(pw, &counters, readingMode);
            poolId = Tha60210012DebugPwInternalCounterPoolIdGet(pw);
            counter_i = 0;

            /* Show counters */
            AtSprintf(buf, "%d", (poolId != cThaInternalCounterPoolInvalid) ? poolId : 0);
            ColorStrToCell(tabPtr, counter_i++, pw_i + 1UL, buf, (poolId != cThaInternalCounterPoolInvalid) ? cSevInfo : cSevNormal);

            mPutCounterValueToCell(counters.txPackets, cSevInfo);
            mPutCounterValueToCell(counters.txPayloadBytes, cSevInfo);
            mPutCounterValueToCell(counters.txPacketFragments, cSevInfo);
            mPutCounterValueToCell(counters.txLbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.txRbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.txMbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.txPbitPackets, cSevCritical);

            mPutCounterValueToCell(counters.rxPackets, cSevInfo);
            mPutCounterValueToCell(counters.rxPayloadBytes, cSevInfo);
            mPutCounterValueToCell(counters.rxLofsPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxLbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxNbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxPbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxRbitPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxLatePackets, cSevCritical);
            mPutCounterValueToCell(counters.rxEarlyPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxLostPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxOverrunPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxUnderrunPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxMalformPackets, cSevCritical);
            mPutCounterValueToCell(counters.rxStrayPackets, cSevCritical);
            }

        /* Show it */
        TablePrint(tabPtr);
        TableFree(tabPtr);
        DestroyHeadings(heading, numPwsPerTable + 1);

        /* For next PWs */
        remainPws = remainPws - numPwsPerTable;
        }

    return cAtTrue;
    }

eBool CmdThaDebugPwInternalCounterGet(char argc, char **argv)
    {
    AtPw *pwList;
    eAtHistoryReadingMode readingMode;
    uint32 numPws;
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode, expect: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    return CounterShow(pwList, numPws, readingMode);
    }

eBool CmdDebugPwInternalMemoryPoolGet(char argc, char **argv)
    {
    uint32 poolList[cThaInternalCounterMaxPoolNum];
    static char buf[16];
    uint16 row_i;
    tTab *tabPtr;
    const char *heading[] = {"PoolID", "PwId"};
    ThaModulePw pwModule;

    AtUnused(argc);
    AtUnused(argv);
    AtOsalMemInit(poolList, cBit31_0, sizeof(poolList));

    tabPtr = TableAlloc(cThaInternalCounterMaxPoolNum, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    Tha60210012DebugPwInternalMemoryPoolGet(pwModule, poolList);

    for (row_i = 0; row_i < cThaInternalCounterMaxPoolNum; row_i ++)
        {
        AtSprintf(buf, "%d", row_i);
        StrToCell(tabPtr, row_i, 0, buf);

        if (poolList[row_i] == cBit31_0)
            StrToCell(tabPtr, row_i, 1, "None");
        else
            {
            AtSprintf(buf, "%d", poolList[row_i] + 1);
            StrToCell(tabPtr, row_i, 1, buf);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool CounterPoolSet(char argc, char **argv, eBool enable)
    {
    uint32 bufferSize, numPw, numPool, *pwList, poolIdList[32], pw_i;
    ThaModulePw pwModule;
    AtPw pw;
    eAtRet ret = cAtOk;

    if ((argc == 0) || (argc > 2))
        return cAtFalse;

    /* Get PW list */
    pwList = CliSharedIdBufferGet(&bufferSize);
    numPw = CliIdListFromString(argv[0], pwList, bufferSize);
    
    /* Get counter pool list */
    if (enable)
        {
        if (numPw > cThaInternalCounterMaxPoolNum)
            {
            AtPrintc(cSevWarning, "Max pool internal counter is %u\n", cThaInternalCounterMaxPoolNum);
            return cAtFalse;
            }

        if (argc == 2)
            {
            numPool = CliIdListFromString(argv[1], poolIdList, cThaInternalCounterMaxPoolNum);
            if ((numPw != numPool))
                {
                AtPrintc(cSevWarning, "numPW = %d, numPool = %d\n", numPw, numPool);
                return cAtFalse;
                }
            }
        else
            AtOsalMemInit(poolIdList, cThaInternalCounterPoolInvalid, sizeof(uint32) * cThaInternalCounterMaxPoolNum);
        }

    pwModule = (ThaModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (pw_i = 0; pw_i < numPw; pw_i ++)
        {
        pw = AtModulePwGetPw((AtModulePw)pwModule, pwList[pw_i] - 1);
        if (pw)
            ret |= Tha60210012DebugPwInternalCounterPoolIdSet(pw, poolIdList[pw_i], enable);
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdDebugPwInternalCounterPoolSet(char argc, char **argv)
    {
    return CounterPoolSet(argc, argv, cAtTrue);
    }

eBool CmdDebugPwInternalCounterPoolClear(char argc, char **argv)
    {
    return CounterPoolSet(argc, argv, cAtFalse);
    }

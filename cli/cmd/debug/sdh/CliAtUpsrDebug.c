/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : CliAtUpsrIntrDebug.c
 *
 * Created Date: Aug 28, 2015
 *
 * Description : Sample code for UPSR interrupt handle.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../driver/src/implement/codechip/Tha60210011/poh/Tha60210011PohReg.h"
#include "../../../../driver/src/generic/common/AtChannelInternal.h"
#include "../sdh/CliAtModuleSdh.h"
#include "AtCliModule.h"
#include "AtSdhPath.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"

#include "AtCiscoUpsr.h"

/*--------------------------- Define -----------------------------------------*/
#define cUpsrChannelBitMask(ch)  (cBit0 << (ch))

#define mUpsrTableCheck()                                                      \
    do                                                                         \
        {                                                                      \
        if (UpsrTableGet(CliDevice()) == NULL)                                 \
            {                                                                  \
            AtPrintc(cSevCritical, "ERROR: This feature has not been supported on this product\r\n"); \
            return cAtFalse;                                                   \
            }                                                                  \
        }while(0)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool UpsrInterruptIsEnabled = cAtFalse;

static const char *cAtUpsrAlarmStr[] = {"aps-sd", "aps-sf"};
static const char *cAtUpsrAlarmUpperStr[] = {"APS-SD", "APS-SF"};
static const uint32 cAtUpsrAlarmVal[] = {cAtCiscoUpsrAlarmSd, cAtCiscoUpsrAlarmSf};
static const char * cAtUpsrGroupLabelSetStr[] = { "invalid",
                                                  "primary",
                                                  "backup"
                                                };
static const eAtCiscoPwGroupLabelSet cAtUpsrGroupLabelSetVal[]  = {cAtCiscoPwGroupLabelSetUnknown,
                                                            cAtCiscoPwGroupLabelSetPrimary,
                                                            cAtCiscoPwGroupLabelSetBackup
                                                            };

static AtCiscoUpsr m_ciscoUpsr = NULL;

static AtList m_ciscoUpsrEvent = NULL;
static AtOsalMutex m_mutexUpsrEvent = NULL;
static tAtTextUIListerner* m_textUIListener = NULL;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
void AtDeviceUpsrInterruptProcess(AtDevice device);
extern void AtCiscoUpsrHistoryTableFlush(void);

/*--------------------------- Implementation ---------------------------------*/
static uint32 DeviceUpsrBaseAddress(AtDevice device)
    {
    return (uint32)(AtHalBaseAddressGet(AtDeviceIpCoreHalGet(device, 0)) + (0x2F0000 << 2));
    }

static uint32 DeviceMetaBaseAddress(AtDevice device)
    {
    return (uint32)AtHalBaseAddressGet(AtDeviceIpCoreHalGet(device, 0));
    }

static void TextUIWillStop(AtTextUI self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtCiscoUpsrDelete(m_ciscoUpsr);
    m_ciscoUpsr = NULL;
    }

static tAtTextUIListerner *Listener(void)
    {
    static tAtTextUIListerner m_listener;
    static tAtTextUIListerner *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.WillStop = TextUIWillStop;
    pListener = &m_listener;

    return pListener;
    }

static AtHal SimulationHal(void)
    {
    return AtDeviceIpCoreHalGet(CliDevice(), 0);
    }

static at_uint32 SimulationLocalAddress(at_uint32 address)
    {
    return address / 4;
    }

static at_uint32 SimulationDirectReadHandler(at_uint32 address)
    {
    return AtHalRead(SimulationHal(), SimulationLocalAddress(address));
    }

static void SimulationDirectWriteHandler(at_uint32 address, at_uint32 value)
    {
    AtHalWrite(SimulationHal(), SimulationLocalAddress(address), value);
    }

static at_uint32 SimulationDeviceInfoDirectReadHandler(at_uint32 address)
    {
    if (address == 0)
        {
        at_uint32 productCode = AtDeviceProductCodeGet(CliDevice());
        return (productCode & (~cBit31_28)) | (0x6 << 28);
        }

    return AtHalRead(SimulationHal(), address);
    }

static void SimulationSetup(void)
    {
    AtCiscoUpsrDirectReadHandlerSet(SimulationDirectReadHandler);
    AtCiscoUpsrDirectWriteHandlerSet(SimulationDirectWriteHandler);
    AtCiscoUpsrDeviceMetaDirectReadHandlerSet(SimulationDeviceInfoDirectReadHandler);
    }

static AtCiscoUpsr UpsrTableGet(AtDevice device)
    {
    if (m_ciscoUpsr)
        return m_ciscoUpsr;

    if (AtDeviceIsSimulated(device))
        SimulationSetup();

    m_ciscoUpsr = AtCiscoUpsrCreate(DeviceMetaBaseAddress(device), NULL, NULL);
    if (m_ciscoUpsr == NULL)
        return NULL;

    AtCiscoUpsrBaseAddressSet(m_ciscoUpsr, DeviceUpsrBaseAddress(device));

    AtTextUIListenerAdd(AtCliSharedTextUI(), Listener(), NULL);

    return m_ciscoUpsr;
    }

static eBool IsHoVc(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeVc4_16c ||
        channelType == cAtSdhChannelTypeVc4_4c ||
        channelType == cAtSdhChannelTypeVc4 ||
        channelType == cAtSdhChannelTypeVc4_nc)
        return cAtTrue;

    if (channelType == cAtSdhChannelTypeVc3)
    	return (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)path)) == cAtSdhChannelTypeAu3) ? cAtTrue: cAtFalse;

    return cAtFalse;
    }

static eBool IsLoVc(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeVc12 ||
        channelType == cAtSdhChannelTypeVc11)
        return cAtTrue;

    if (channelType == cAtSdhChannelTypeVc3)
    	return (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)path)) == cAtSdhChannelTypeTu3) ? cAtTrue: cAtFalse;

    return cAtFalse;
    }

static eBool IsAu(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeAu4_16c ||
        channelType == cAtSdhChannelTypeAu4_4c ||
        channelType == cAtSdhChannelTypeAu4 ||
        channelType == cAtSdhChannelTypeAu3 ||
        channelType == cAtSdhChannelTypeAu4_nc)
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsTu(AtSdhPath path)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (channelType == cAtSdhChannelTypeTu3  ||
        channelType == cAtSdhChannelTypeTu12 ||
        channelType == cAtSdhChannelTypeTu11)
        return cAtTrue;
    return cAtFalse;
    }

static eBool PathIsValid(AtSdhPath path)
	{
    return IsHoVc(path) || IsLoVc(path) || IsAu(path) || IsTu(path);
	}

static uint32 UpsrAlarmMaskFromString(char *alarmStr)
    {
    return CliMaskFromString(alarmStr, cAtUpsrAlarmStr, cAtUpsrAlarmVal, mCount(cAtUpsrAlarmVal));
    }

static AtSdhPath SdhAuOfStm4FromStsGet(AtSdhLine line, uint8 tfi5HwSts)
    {
    AtSdhChannel aug4 = (AtSdhChannel)AtSdhLineAug4Get(line, 0);

    if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
        return (AtSdhPath)AtSdhChannelSubChannelGet(aug4, 0);
    else
        {
        if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4Map4xAug1s)
            {
            uint8 aug1Id = (uint8)(tfi5HwSts / 3);
            AtSdhChannel aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, aug1Id);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
                return (AtSdhPath)AtSdhChannelSubChannelGet(aug1, 0);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
                return (AtSdhPath)AtSdhChannelSubChannelGet(aug1, (uint8)(tfi5HwSts % 3));
            }
        }

    return NULL;
    }

static AtSdhPath SdhAuOfStm1FromStsGet(AtSdhLine line, uint8 tfi5HwSts)
    {
    AtSdhChannel aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, 0);

    if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
        return (AtSdhPath)AtSdhChannelSubChannelGet(aug1, 0);

    return (AtSdhPath)AtSdhChannelSubChannelGet(aug1, tfi5HwSts); /* Aug1 to 3xVc3s */
    }

static AtSdhPath SdhAuOfStm16FromStsGet(AtSdhLine line, uint8 tfi5HwSts)
    {
    AtSdhChannel aug16;

    aug16 = (AtSdhChannel)AtSdhLineAug16Get(line, 0);
    if (AtSdhChannelMapTypeGet(aug16) == cAtSdhAugMapTypeAug16MapVc4_16c)
        return (AtSdhPath)AtSdhChannelSubChannelGet(aug16, 0);
    else
        {
        uint8 aug4Id = (uint8)(tfi5HwSts / 12);
        AtSdhChannel aug4 = (AtSdhChannel)AtSdhLineAug4Get(line, aug4Id);
        if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
            return (AtSdhPath)AtSdhChannelSubChannelGet(aug4, 0);

        else if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4Map4xAug1s)
            {
            uint8 aug1Id = (uint8)(tfi5HwSts / 3);
            AtSdhChannel aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, aug1Id);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
                return (AtSdhPath)AtSdhChannelSubChannelGet(aug1, 0);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
                return (AtSdhPath)AtSdhChannelSubChannelGet(aug1, (uint8)(tfi5HwSts % 3));
            }
        }

    return NULL;
    }

static AtSdhPath AtModuleSdhAuGet(AtModuleSdh sdhModule, uint32 stsIndex)
    {
    uint8 lineId = (uint8)(stsIndex / 48);
    uint8 stsId = (uint8)(stsIndex % 48);
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);

    if (!line)
        return NULL;

    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm16)
        return SdhAuOfStm16FromStsGet(line, stsId);
        
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm4)
        return SdhAuOfStm4FromStsGet(line, stsId);
        
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm1)
        return SdhAuOfStm1FromStsGet(line, stsId);

    return NULL;
    }

static AtSdhPath AtModuleSdhTuGet(AtModuleSdh sdhModule, uint32 stsIndex, uint32 vtIndex)
    {
    AtSdhChannel au = (AtSdhChannel)AtModuleSdhAuGet(sdhModule, stsIndex);

    if (AtSdhChannelTypeGet(au) == cAtSdhChannelTypeAu4)
        {
        AtSdhChannel vc4 = AtSdhChannelSubChannelGet(au, 0);
        if (AtSdhChannelMapTypeGet(vc4) == cAtSdhVcMapTypeVc4Map3xTug3s)
            {
            uint8 tug3Id = (uint8)(stsIndex % 3);
            AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc4, tug3Id);

            if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
                return (AtSdhPath)AtSdhChannelSubChannelGet(tug3, 0);

            if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3Map7xTug2s)
                {
                AtSdhChannel tug2 = AtSdhChannelSubChannelGet(tug3, (uint8)(vtIndex / 4));
                return (AtSdhPath)AtSdhChannelSubChannelGet(tug2,  (uint8)(vtIndex % 4));
                }
            }
        }

    else if (AtSdhChannelTypeGet(au) == cAtSdhChannelTypeAu3)
        {
        AtSdhChannel vc3 = AtSdhChannelSubChannelGet(au, 0);

        if (AtSdhChannelMapTypeGet(vc3) == cAtSdhVcMapTypeVc3Map7xTug2s)
            {
            AtSdhChannel tug2 = AtSdhChannelSubChannelGet(vc3, (uint8)(vtIndex / 4));
            return (AtSdhPath)AtSdhChannelSubChannelGet(tug2, (uint8)(vtIndex % 4));
            }
        }

    return NULL;
    }

static uint32 AuIndexGet(AtSdhPath sdhAu)
    {
    uint8 lineId = AtSdhChannelLineGet((AtSdhChannel)sdhAu);
    uint8 stsId = AtSdhChannelSts1Get((AtSdhChannel)sdhAu);
    return (uint32)(lineId * 48 + stsId);
    }

static uint32 TuIndexGet(AtSdhPath sdhTu)
    {
    uint8 lineId = AtSdhChannelLineGet((AtSdhChannel)sdhTu);
    uint8 stsId = AtSdhChannelSts1Get((AtSdhChannel)sdhTu);
    uint32 vtIndex = (uint32)((lineId * 48 + stsId) * 28);
    vtIndex += AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)sdhTu)) * 4UL + AtChannelIdGet((AtChannel)sdhTu);
    return vtIndex;
    }

static uint32 HoVcIndexGet(AtSdhPath sdhVc)
    {
	AtSdhPath sdhAu = (AtSdhPath)AtSdhChannelParentChannelGet((AtSdhChannel)sdhVc);
	return AuIndexGet(sdhAu);
    }

static uint32 LoVcIndexGet(AtSdhPath sdhVc)
    {
	AtSdhPath sdhTu = (AtSdhPath)AtSdhChannelParentChannelGet((AtSdhChannel)sdhVc);
	return TuIndexGet(sdhTu);
    }

static uint32 ChannelRow(AtSdhPath path)
    {
    if (IsAu(path))
        return AuIndexGet(path) / 32;

    if (IsHoVc(path))
        return HoVcIndexGet(path) / 32;

    if (IsTu(path))
        return TuIndexGet(path) / 28;

    if (IsLoVc(path))
        return LoVcIndexGet(path) / 28;

    return cBit31_0;
    }

static uint32 ChannelBit(AtSdhPath path)
    {
    if (IsAu(path))
        return cBit0 << (AuIndexGet(path) % 32);

    if (IsHoVc(path))
        return cBit0 << (HoVcIndexGet(path) % 32);

    if (IsTu(path))
        return cBit0 << (TuIndexGet(path) % 28);

    if (IsLoVc(path))
        return cBit0 << (LoVcIndexGet(path) % 28);

    return 0;
    }

static eAtRet UpsrInterruptMaskSet(AtSdhPath path, uint32 defectMask, uint32 enableMask)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)path);
    uint32 rowIndex = ChannelRow(path);
    uint32 bitMask = ChannelBit(path);
    uint32 regVal;
    uint32 (*MaskRead)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32) = NULL;
    void (*MaskWrite)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32, uint32) = NULL;

    if (!PathIsValid(path))
        return cAtErrorInvlParm;

    if (AtDeviceIsSimulated(device))
        return cAtOk;

    if (IsAu(path) || IsHoVc(path))
        {
        MaskRead = AtCiscoUpsrStsMaskRead;
        MaskWrite = AtCiscoUpsrStsMaskWrite;
        }

    if (IsTu(path) || IsLoVc(path))
        {
        MaskRead = AtCiscoUpsrVtMaskRead;
        MaskWrite = AtCiscoUpsrVtMaskWrite;
        }

    if ((MaskRead == NULL) || (MaskWrite == NULL))
        return cAtErrorInvlParm;

    if (defectMask & cAtCiscoUpsrAlarmSf)
        {
        regVal = MaskRead(UpsrTableGet(device), cAtCiscoUpsrAlarmSf, rowIndex);
        regVal = (enableMask & cAtCiscoUpsrAlarmSf) ? (regVal | bitMask) : (regVal & ~bitMask);
        MaskWrite(UpsrTableGet(device), cAtCiscoUpsrAlarmSf, rowIndex, regVal);
        }

    if (defectMask & cAtCiscoUpsrAlarmSd)
        {
        regVal = MaskRead(UpsrTableGet(device), cAtCiscoUpsrAlarmSd, rowIndex);
        regVal = (enableMask & cAtCiscoUpsrAlarmSd) ? (regVal | bitMask) : (regVal & ~bitMask);
        MaskWrite(UpsrTableGet(device), cAtCiscoUpsrAlarmSd, rowIndex, regVal);
        }

    return cAtOk;
    }

static uint32 UpsrInterruptMaskGet(AtSdhPath path)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)path);
    uint32 rowIndex = ChannelRow(path);
    uint32 bitMask = ChannelBit(path);
    uint32 regVal;
    uint32 (*MaskRead)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32) = NULL;
    uint32 defectMask = 0;

    if (!PathIsValid(path))
        return 0;

    if (AtDeviceIsSimulated(device))
        return 0;

    if (IsAu(path) || IsHoVc(path))
        MaskRead = AtCiscoUpsrStsMaskRead;

    if (IsTu(path) || IsLoVc(path))
        MaskRead = AtCiscoUpsrVtMaskRead;

    /* Impossible, but to make static analysis tool be happy */
    if (MaskRead == NULL)
        return 0x0;

    regVal = MaskRead(UpsrTableGet(device), cAtCiscoUpsrAlarmSf, rowIndex);
    if (regVal & bitMask)
        defectMask |= cAtCiscoUpsrAlarmSf;

    regVal = MaskRead(UpsrTableGet(device), cAtCiscoUpsrAlarmSd, rowIndex);
    if (regVal & bitMask)
        defectMask |= cAtCiscoUpsrAlarmSd;

    return defectMask;
    }

static uint32 UpsrDefectGet(AtSdhPath path)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)path);
    uint32 rowIndex = ChannelRow(path);
    uint32 bitMask = ChannelBit(path);
    uint32 regVal;
    uint32 defectStat = 0;
    uint32 (*StatusRead)(AtCiscoUpsr, eAtCiscoUpsrAlarm, uint32) = NULL;

    if (AtDeviceIsSimulated(device))
        return 0;

    if (!PathIsValid(path))
        {
        AtPrintc(cSevWarning, "WARNING: UPSR is not supported on %s\r\n", CliChannelIdStringGet((AtChannel)path));
        return 0;
        }

    if (IsAu(path) || IsHoVc(path))
        StatusRead = AtCiscoUpsrStsStatusRead;

    if (IsTu(path) || IsLoVc(path))
        StatusRead = AtCiscoUpsrVtStatusRead;

    /* Impossible, but to make static analysis tool be happy */
    if (StatusRead == NULL)
        return 0x0;

    regVal = StatusRead(UpsrTableGet(device), cAtCiscoUpsrAlarmSf, rowIndex);
    if (regVal & bitMask)
        defectStat |= cAtCiscoUpsrAlarmSf;

    regVal = StatusRead(UpsrTableGet(device), cAtCiscoUpsrAlarmSd, rowIndex);
    if (regVal & bitMask)
        defectStat |= cAtCiscoUpsrAlarmSd;

    return defectStat;
    }

static eBool UpsrInterruptEnable(char argc, char **argv, eBool enable)
    {
    UpsrInterruptIsEnabled = enable;
    AtUnused(argc);
    AtUnused(argv);
    return cAtTrue;
    }

static void StsUpsrNotify(uint32 baseAddress, void *data, uint32 stsId, eAtCiscoUpsrAlarm changedAlarms, eAtCiscoUpsrAlarm currentStatus)
    {
    AtChannel au = (AtChannel)AtModuleSdhAuGet(CliModuleSdh(), stsId);
    uint8 alarm_i;
    AtUnused(baseAddress);
    AtUnused(data);

    AtPrintc(cSevNormal, "\r\n%s [UPSR] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(au));
    for (alarm_i = 0; alarm_i < mCount(cAtUpsrAlarmUpperStr); alarm_i++)
        {
        uint32 alarmType = cAtUpsrAlarmVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtUpsrAlarmUpperStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void VtUpsrNotify(uint32 baseAddress, void *data, uint32 stsId, uint32 vtId, eAtCiscoUpsrAlarm changedAlarms, eAtCiscoUpsrAlarm currentStatus)
    {
    AtChannel tu = (AtChannel)AtModuleSdhTuGet(CliModuleSdh(), stsId, vtId);
    uint8 alarm_i;
    AtUnused(baseAddress);
    AtUnused(data);

    AtPrintc(cSevNormal, "\r\n%s [UPSR] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(tu));
    for (alarm_i = 0; alarm_i < mCount(cAtUpsrAlarmUpperStr); alarm_i++)
        {
        uint32 alarmType = cAtUpsrAlarmVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtUpsrAlarmUpperStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void UpsrEventsCleanup(AtTextUI self, void *userData)
    {
    tAtCapturedEvent *capturedEvent;

    AtUnused(self);
    AtUnused(userData);
    if (m_ciscoUpsrEvent == NULL)
        return;

    while ((capturedEvent = (tAtCapturedEvent *)AtListObjectRemoveAtIndex(m_ciscoUpsrEvent, 0)) != NULL)
        AtOsalMemFree(capturedEvent);

    AtObjectDelete((AtObject)m_ciscoUpsrEvent);
    m_ciscoUpsrEvent = NULL;
    AtOsalMutexDestroy(m_mutexUpsrEvent);
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;

    if (m_textUIListener)
        return m_textUIListener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.WillStop = UpsrEventsCleanup;
    m_listener.DidStop  = NULL;
    m_textUIListener = &m_listener;

    return m_textUIListener;
    }

static void UpsrEventCreate(void)
    {
    m_ciscoUpsrEvent = AtListCreate(0);
    m_mutexUpsrEvent = AtOsalMutexCreate();
    }

static void AutotestStsUpsrEventState(at_uint32 baseAddress,
                                      void *data,
                                      at_uint32 stsId,
                                      eAtCiscoUpsrAlarm changedAlarms,
                                      eAtCiscoUpsrAlarm currentStatus)
    {
    AtChannel au;

    AtUnused(data);
    AtUnused(baseAddress);
    if (m_ciscoUpsrEvent == NULL)
        UpsrEventCreate();

    if (m_textUIListener == NULL)
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);

    au = (AtChannel)AtModuleSdhAuGet(CliModuleSdh(), stsId);
    AtOsalMutexLock(m_mutexUpsrEvent);
    CliPushLastEventToListEvent(au, changedAlarms, currentStatus, m_ciscoUpsrEvent, 0);
    AtOsalMutexUnLock(m_mutexUpsrEvent);
    }

static void AutotestVtUpsrEventState(at_uint32 baseAddress,
                                     void *data,
                                     at_uint32 stsId,
                                     at_uint32 vtId,
                                     eAtCiscoUpsrAlarm changedAlarms,
                                     eAtCiscoUpsrAlarm currentStatus)
    {
    AtChannel tu = (AtChannel)AtModuleSdhTuGet(CliModuleSdh(), stsId, vtId);

    AtUnused(data);
    AtUnused(baseAddress);
    if (m_ciscoUpsrEvent == NULL)
        UpsrEventCreate();

    if (m_textUIListener == NULL)
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);

    tu = (AtChannel)AtModuleSdhTuGet(CliModuleSdh(), stsId, vtId);
    AtOsalMutexLock(m_mutexUpsrEvent);
    CliPushLastEventToListEvent(tu, changedAlarms, currentStatus, m_ciscoUpsrEvent, 0);
    AtOsalMutexUnLock(m_mutexUpsrEvent);
    }

static eBool UpsrNotificationEnable(char argc, char **argv, eBool enable)
    {
    static tAtCiscoUpsrListener listener;

    AtUnused(argc);
    AtUnused(argv);
    mUpsrTableCheck();

    if (AutotestIsEnabled())
        {
        listener.StsInterruptChanged = AutotestStsUpsrEventState;
        listener.VtInterruptChanged  = AutotestVtUpsrEventState;
        }
    else
        {
        listener.StsInterruptChanged = StsUpsrNotify;
        listener.VtInterruptChanged  = VtUpsrNotify;
        }

    if (enable)
        AtCiscoUpsrListenerAdd(UpsrTableGet(CliDevice()), NULL, &listener);
    else
        AtCiscoUpsrListenerRemove(UpsrTableGet(CliDevice()), NULL, &listener);

    return cAtTrue;
    }

void AtDeviceUpsrInterruptProcess(AtDevice device)
    {
    if (UpsrInterruptIsEnabled && !AtDeviceIsSimulated(device))
        {
        AtCiscoUpsrInterruptProcess(UpsrTableGet(device));
        }
    }

eBool CmdDebugUpsrIntrMask(char argc, char **argv)
    {
    AtSdhPath *paths;
    uint32 numberPaths, i;
    uint32 intrMaskVal;
    eBool convertSuccess;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;

    AtUnused(argc);
    mUpsrTableCheck();

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = UpsrAlarmMaskFromString(argv[1])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i++)
        {
        eAtRet ret;
        AtSdhPath path = (AtSdhPath)paths[i];

        ret = UpsrInterruptMaskSet(path, intrMaskVal, enable ? intrMaskVal : 0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set SD/SF interrupt mask for path %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)path),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdDebugUpsrShow(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 intrMaskVal;
    eAtRet ret = cAtOk;
    AtSdhPath *paths;
    tTab *tabPtr;
    const char *pHeading[] = {"PathId", "SF mask", "SD mask", "SF defect", "SD defect"};

    AtUnused(argc);
    mUpsrTableCheck();

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        ret = cAtErrorInvlParm;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i++)
        {
        AtSdhPath thisPath = (AtSdhPath)paths[i];
        uint8 column = 0;

        /* Print list pathsId */
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)thisPath));

        /* SF/SD mask */
        intrMaskVal = UpsrInterruptMaskGet(thisPath);
        if (intrMaskVal & cAtCiscoUpsrAlarmSf)
            ColorStrToCell(tabPtr, i, column++, "set", cSevCritical);
        else
            ColorStrToCell(tabPtr, i, column++, "clear", cSevInfo);

        if (intrMaskVal & cAtCiscoUpsrAlarmSd)
            ColorStrToCell(tabPtr, i, column++, "set", cSevCritical);
        else
            ColorStrToCell(tabPtr, i, column++, "clear", cSevInfo);

        /* SF/SD alarm */
        intrMaskVal = UpsrDefectGet(thisPath);
        if (intrMaskVal & cAtCiscoUpsrAlarmSf)
            ColorStrToCell(tabPtr, i, column++, "set", cSevCritical);
        else
            ColorStrToCell(tabPtr, i, column++, "clear", cSevInfo);

        if (intrMaskVal & cAtCiscoUpsrAlarmSd)
            ColorStrToCell(tabPtr, i, column++, "set", cSevCritical);
        else
            ColorStrToCell(tabPtr, i, column++, "clear", cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdDebugUpsrInterruptEnable(char argc, char **argv)
    {
    return UpsrInterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdDebugUpsrInterruptDisable(char argc, char **argv)
    {
    return UpsrInterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdDebugUpsrNotificationEnable(char argc, char **argv)
    {
    return UpsrNotificationEnable(argc, argv, cAtTrue);
    }

eBool CmdDebugUpsrNotificationDisable(char argc, char **argv)
    {
    return UpsrNotificationEnable(argc, argv, cAtFalse);
    }

eBool CmdDebugUpsrInterruptRestore(char argc, char **argv)
    {
    AtDevice device = CliDevice();
    mUpsrTableCheck();
    AtUnused(argc);
    AtUnused(argv);
    if (!AtDeviceIsSimulated(device))
        {
        AtCiscoUpsrHistoryTableFlush();
        AtCiscoUpsrInterruptRestore(UpsrTableGet(device));
        }
    return cAtTrue;
    }

eBool CmdDebugUpsrNotificationGet(char argc, char **argv)
    {
    const char *pHeading[] = {"Channel", "APS-SD", "APS-SF"};
    eAtHistoryReadingMode readMode;
    AtList filteredEvent;
    AtChannel *channelList;
    uint32 numChannels;
    eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    tAtCliUpsrInterruptStatistic statistic;

    mUpsrTableCheck();

    AtUnused(argc);
    /* Get list of path */
    channelList = (AtChannel *)CliSdhPathFromArgumentGet(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);
    AtOsalMutexLock(m_mutexUpsrEvent);
    filteredEvent = CliCapturedEventFilterByChannels(channelList, numChannels, m_ciscoUpsrEvent, readMode);
    AtOsalMutexUnLock(m_mutexUpsrEvent);
    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(AtListLengthGet(filteredEvent), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    AtOsalMutexLock(m_mutexUpsrEvent);
    CliPutEventsToTableAndDoStatistics(tabPtr, filteredEvent,
                                       cAtUpsrAlarmVal, mCount(cAtUpsrAlarmVal), 0, readMode,
                                       &statistic);
    AtOsalMutexUnLock(m_mutexUpsrEvent);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtPrintc(cSevNormal, "Statistics: Number of Set: %d\r\n", statistic.setNums);
    AtPrintc(cSevNormal, "Statistics: Number of Clear: %d\r\n", statistic.clearNums);
    AtPrintc(cSevNormal, "Statistics: Number of Unchanged: %d\r\n", statistic.unchangedNums);

    return cAtTrue;
    }

static eBool DebugApsUpsrGroupEnable(char argc, char **argv, eBool enable)
    {
    uint32 *idBuf;
    uint32 bufferSize, numGroupIds;
    uint32 groupId;
    AtUnused(argc);

    mUpsrTableCheck();

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numGroupIds = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    for (groupId = 0; groupId < numGroupIds; groupId++)
        AtCiscoApsGroupEnable(UpsrTableGet(CliDevice()), idBuf[groupId] - 1, enable);

    return cAtTrue;
    }

eBool CmdDebugApsUpsrGroupEnable(char argc, char **argv)
    {
    return DebugApsUpsrGroupEnable(argc, argv, cAtTrue);
    }

eBool CmdDebugApsUpsrGroupDisable(char argc, char **argv)
    {
    return DebugApsUpsrGroupEnable(argc, argv, cAtFalse);
    }

eBool CmdDebugApsUpsrGroupGet(char argc, char **argv)
    {
    uint32 *idBuf;
    uint32 bufferSize, numGroupIds;
    uint32 groupId;
    tTab *tabPtr;
    const char *pHeading[] = {"GroupId", "Enable"};

    mUpsrTableCheck();

    AtUnused(argc);

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numGroupIds = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numGroupIds, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (groupId = 0; groupId < numGroupIds; groupId++)
        {
        uint8 column = 0;
        uint32 enable = AtCiscoApsGroupIsEnabled(UpsrTableGet(CliDevice()), idBuf[groupId] - 1);

        StrToCell(tabPtr, groupId, column++, (char *)CliNumber2String(idBuf[groupId], "%u"));
        ColorStrToCell(tabPtr, groupId, column++, enable ? "en" : "dis", cSevInfo);
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool HsUpsrGroupLabelSetSelect(char argc, char **argv,
                                       void (*LabelSetSelect)(AtCiscoUpsr,
                                                              at_uint32,
                                                              eAtCiscoPwGroupLabelSet))
    {
    uint32 *idBuf;
    uint32 bufferSize, numGroupIds, groupId;
    eBool convertSuccess = cAtFalse;
    eAtCiscoPwGroupLabelSet labelSet = cAtCiscoPwGroupLabelSetUnknown;
    AtUnused(argc);

    mUpsrTableCheck();

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numGroupIds = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    mAtStrToEnum(cAtUpsrGroupLabelSetStr, cAtUpsrGroupLabelSetVal, argv[1], labelSet, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Group Label Set \r\n");
        return cAtFalse;
        }

    for (groupId = 0; groupId < numGroupIds; groupId++)
        LabelSetSelect(UpsrTableGet(CliDevice()), idBuf[groupId] - 1, labelSet);

    return cAtTrue;
    }

eBool CmdDebugHsUpsrGroupTxLabelSetSelect(char argc, char **argv)
    {
    return HsUpsrGroupLabelSetSelect(argc, argv, AtCiscoHsGroupTxLabelSetSelect);
    }

eBool CmdDebugHsUpsrGroupRxLabelSetSelect(char argc, char **argv)
    {
    return HsUpsrGroupLabelSetSelect(argc, argv, AtCiscoHsGroupRxLabelSetSelect);
    }

eBool CmdDebugHsUpsrGroupGet(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 *idBuf;
    uint32 bufferSize, numGroupIds, groupId;
    eBool convertSuccess = cAtFalse;
    eAtCiscoPwGroupLabelSet label;
    const char *pHeading[] = {"GroupId", "Tx label", "Rx label"};

    AtUnused(argc);
    mUpsrTableCheck();

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numGroupIds = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numGroupIds, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (groupId = 0; groupId < numGroupIds; groupId++)
        {
        uint8 column = 0;
        const char *labelSetStr;

        StrToCell(tabPtr, groupId, column++, (char *)CliNumber2String(idBuf[groupId], "%u"));

        label = AtCiscoHsGroupTxSelectedLabelGet(UpsrTableGet(CliDevice()), idBuf[groupId] - 1);
        labelSetStr = CliEnumToString(label,
                                      cAtUpsrGroupLabelSetStr,
                                      cAtUpsrGroupLabelSetVal,
                                      mCount(cAtUpsrGroupLabelSetVal),
                                      &convertSuccess);
        ColorStrToCell(tabPtr, groupId, column++, labelSetStr, cSevNormal);

        label = AtCiscoHsGroupRxSelectedLabelGet(UpsrTableGet(CliDevice()), idBuf[groupId] - 1);
        labelSetStr = CliEnumToString(label,
                                      cAtUpsrGroupLabelSetStr,
                                      cAtUpsrGroupLabelSetVal,
                                      mCount(cAtUpsrGroupLabelSetVal),
                                      &convertSuccess);
        ColorStrToCell(tabPtr, groupId, column++, labelSetStr, cSevNormal);
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDebugUpsrInformation(char argc, char **argv)
    {
    AtCiscoUpsr upsrTable = UpsrTableGet(CliDevice());

    mUpsrTableCheck();

    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevNormal, "Product code:      0x%x\r\n", AtCiscoUpsrProductCodeGet(upsrTable));
    AtPrintc(cSevNormal, "FPGA version:      0x%x\r\n", AtCiscoUpsrFpgaVersion(upsrTable));
    AtPrintc(cSevNormal, "FPGA build-number: 0x%x\r\n", AtCiscoUpsrFpgaBuiltNumber(upsrTable));

    return cAtTrue;
    }

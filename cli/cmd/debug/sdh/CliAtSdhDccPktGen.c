/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtSdhDccPktGen.c
 *
 * Created Date: Apr 6, 2017
 *
 * Description : DCC PKT GEN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../../../cmd/sdh/CliAtSdhLineDcc.h"
#include "../../../../driver/src/implement/codechip/Tha60290021/encap/Tha6029DccHdlcPktGenerator.h"

/*--------------------------- Define -----------------------------------------*/
typedef eAtRet (*AttributeSetFunc)(AtDevice self, uint32 val);
typedef eAtRet (*ChannelAttributeSetFunc)(AtHdlcChannel self, uint32 val);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char * cAtGeneratorModeStr[] =
    {
    "none", "oneshot", "continuous"
    };

static const uint32 cAtGeneratorModeValue[] =
    {
    cAtGeneratorModeInvalid, cAtGeneratorModeOneshot, cAtGeneratorModeContinuous
    };

static const char * cAtPktErrorTypeStr[] =
    {
    "none", "length", "fcs", "payload"
    };

static const uint32 cAtPktErrorTypeValue[] =
    {
     cAtPktErrorTypeNone, cAtPktErrorTypeLength, cAtPktErrorTypeFcs, cAtPktErrorTypePayload
    };

static const char * cAtLengthModeStr[] =
    {
    "unknown", "fix", "increase", "random"
    };

static const uint32 cAtLengthModeValue[] =
    {
     cAtLengthModeUnknown, cAtLengthModeFix, cAtLengthModeIncrease, cAtLengthModeRandom
    };

static const char * cAtPktLoopbackTypeStr[] =
    {
    "none", "dcc-sgmii", "dcc-hdlc", "kbyte-sgmii", "kbyte-sdh", "dcc-buffer"
    };

static const uint32 cAtPktLoopbackTypeValue[] =
    {
    cAtPktLoopbackTypeNone,
    cAtPktLoopbackTypeDccSgmiiLoop,
    cAtPktLoopbackTypeDccHdlcLoop,
    cAtPktLoopbackTypeKbyteSgmiiLoop,
    cAtPktLoopbackTypeKbyteSdhLoop,
    cAtPktLoopbackTypeDccBufferLoop
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PktLoopbackMaskFromString(char *alarmStr)
    {
    return CliMaskFromString(alarmStr,
                             cAtPktLoopbackTypeStr,
                             cAtPktLoopbackTypeValue,
                             mCount(cAtPktLoopbackTypeValue));
    }

static const char * PktLoopback2String(uint32 alarms)
    {
    uint16 i;
    static char loopbackString[128];

    /* Initialize string */
    loopbackString[0] = '\0';
    if (alarms == 0)
        AtSprintf(loopbackString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cAtPktLoopbackTypeValue); i++)
            {
            if (alarms & (cAtPktLoopbackTypeValue[i]))
                AtSprintf(loopbackString, "%s%s|", loopbackString, cAtPktLoopbackTypeStr[i]);
            }

        if (AtStrlen(loopbackString) == 0)
            return "None";

        /* Remove the last '|' */
        loopbackString[AtStrlen(loopbackString) - 1] = '\0';
        }

    return loopbackString;
    }

static eAtGeneratorMode GeneratorModeFromString(const char *string)
    {
    eAtGeneratorMode mode;
    eBool convertSuccess;

    mAtStrToEnum(cAtGeneratorModeStr,
                 cAtGeneratorModeValue,
                 string,
                 mode,
                 convertSuccess);
    return convertSuccess ? mode : cAtGeneratorModeInvalid;
    }

static const char *GeneratorModeToString(eAtGeneratorMode mode)
    {
    return CliEnumToString(mode,
                           cAtGeneratorModeStr,
                           cAtGeneratorModeValue,
                           mCount(cAtGeneratorModeValue),
                           NULL);
    }

static eAtPktErrorType PktErrorTypeFromString(const char *string)
    {
    eAtPktErrorType mode;
    eBool convertSuccess;

    mAtStrToEnum(cAtPktErrorTypeStr,
                 cAtPktErrorTypeValue,
                 string,
                 mode,
                 convertSuccess);
    return convertSuccess ? mode : cAtPktErrorTypeNone;
    }

static const char *PktErrorTypeToString(uint32 mode)
    {
    return CliEnumToString(mode,
                           cAtPktErrorTypeStr,
                           cAtPktErrorTypeValue,
                           mCount(cAtPktErrorTypeValue),
                           NULL);
    }

static eAtLengthMode LengthModeFromString(const char *string)
    {
    eAtLengthMode mode;
    eBool convertSuccess;

    mAtStrToEnum(cAtLengthModeStr,
                 cAtLengthModeValue,
                 string,
                 mode,
                 convertSuccess);
    return convertSuccess ? mode : cAtLengthModeUnknown;
    }

static const char *LengthModeToString(uint32 mode)
    {
    return CliEnumToString(mode,
                           cAtLengthModeStr,
                           cAtLengthModeValue,
                           mCount(cAtLengthModeValue),
                           NULL);
    }

static eBool Enable(char argc,
                    char **argv,
                    eBool enabled,
                    eAtRet (*EnableFunc)(AtDevice self, eBool enabled),
                    const char* description)
    {
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);
    AtUnused(argv);

    ret = EnableFunc(CliDevice(), enabled);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot %s DCC-HDLC-%s-function, ret = %s\r\n",
                 enabled ? "enable" : "disable",
                 description,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool AttributeSet(char argc,
                          char **argv,
                          uint32 value,
                          AttributeSetFunc func)
    {
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);
    AtUnused(argv);

    ret = func(CliDevice(), value);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot set value on DCC-HDLC-Debug-function, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool ChannelAttributeSet(char argc,
                                 char **argv,
                                 uint32 value,
                                 ChannelAttributeSetFunc func)
    {
    uint32 i;
    AtList hdlcList = CliAtDccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    eBool success = cAtTrue;

    AtUnused(argc);
    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) hdlcList);
        return cAtFalse;
        }

    for (i = 0; i < numberHdlc; i++)
        {
        AtHdlcChannel hdlc = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        eAtRet ret = func(hdlc, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can set value = %s, ret = %s\r\n",
                     argv[0],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject) hdlcList);
    return success;
    }

eBool CmdAtDccHdlcPktGenEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, Tha6029DccHdlcPktGenEnable, "generator");
    }

eBool CmdAtDccHdlcPktGenDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, Tha6029DccHdlcPktGenEnable, "generator");
    }

eBool CmdAtDccHdlcPktMonEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, Tha6029DccHdlcPktMonEnable, "monitor");
    }

eBool CmdAtDccHdlcPktMonDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, Tha6029DccHdlcPktMonEnable, "monitor");
    }

eBool CmdAtDccHdlcPktInit(char argc, char **argv)
    {
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);
    AtUnused(argv);

    ret = Tha6029DccHdlcPktGenInit(CliDevice());
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot init DCC-HDLC-Debug-function, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtDccHdlcPktGenTimerIntervalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtStrToDw(argv[0]), Tha6029DccHdlcPktGenTimerIntervalSet);
    }

eBool CmdAtDccHdlcPktMonTimerIntervalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtStrToDw(argv[0]), Tha6029DccHdlcPktMonTimerIntervalSet);
    }

eBool CmdAtDccHdlcPktGenMaxLengthSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtStrToDw(argv[0]), Tha6029DccHdlcPktGenMaxLengthSet);
    }

eBool CmdAtDccHdlcPktGenMinLengthSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtStrToDw(argv[0]), Tha6029DccHdlcPktGenMinLengthSet);
    }

eBool CmdAtDccHdlcPktGenModeSet(char argc, char **argv)
    {
    uint32 mode = GeneratorModeFromString(argv[0]);
    return AttributeSet(argc, argv, mode, (AttributeSetFunc)Tha6029DccHdlcPktGenModeSet);
    }

eBool CmdAtDccHdlcPktGenErrorForce(char argc, char **argv)
    {
    uint32 errorMask = PktErrorTypeFromString(argv[0]);
    return AttributeSet(argc, argv, errorMask, Tha6029DccHdlcPktGenErrorForce);
    }

eBool CmdAtDccHdlcPktGenErrorUnForce(char argc, char **argv)
    {
    uint32 errorMask = PktErrorTypeFromString(argv[0]);
    return AttributeSet(argc, argv, errorMask, Tha6029DccHdlcPktGenErrorUnForce);
    }

eBool CmdAtDccHdlcPktShow(char argc, char **argv)
    {
    eBool enabled;
    uint32 value;
    const char *strValue;
    uint32 column = 0;
    tTab *tabPtr;
    const char *pHeading[] = {"Generator", "Monitor", "Gen-Interval", "Mon-Interval", "mode", "error-mask", "min-length", "max-length", "loopback"};

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    enabled = Tha6029DccHdlcPktGenIsEnabled(CliDevice());
    ColorStrToCell(tabPtr, 0, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

    enabled = Tha6029DccHdlcPktMonIsEnabled(CliDevice());
    ColorStrToCell(tabPtr, 0, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);


    value = Tha6029DccHdlcPktGenTimerIntervalGet(CliDevice());
    StrToCell(tabPtr, 0, column++, CliNumber2String(value, "0x%08x"));

    value = Tha6029DccHdlcPktMonTimerIntervalGet(CliDevice());
    StrToCell(tabPtr, 0, column++, CliNumber2String(value, "0x%08x"));

    value = Tha6029DccHdlcPktGenModeGet(CliDevice());
    strValue = GeneratorModeToString(value);
    StrToCell(tabPtr, 0, column++, strValue ? strValue : "Error");


    value = Tha6029DccHdlcPktGenForcedErrorGet(CliDevice());
    strValue = PktErrorTypeToString(value);
    StrToCell(tabPtr, 0, column++, strValue ? strValue : "Error");

    value = Tha6029DccHdlcPktGenMinLengthGet(CliDevice());
    StrToCell(tabPtr, 0, column++, CliNumber2String(value, "%u"));

    value = Tha6029DccHdlcPktGenMaxLengthGet(CliDevice());
    StrToCell(tabPtr, 0, column++, CliNumber2String(value, "%u"));

    value = Tha6029DccHdlcSoELoopbackTypeGet(CliDevice());
    StrToCell(tabPtr, 0, column++, PktLoopback2String(value));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDccHdlcPktGenChannelEnable(char argc, char **argv)
    {
    return ChannelAttributeSet(argc, argv, cAtTrue, (ChannelAttributeSetFunc)Tha6029DccHdlcPktGenChannelEnable);
    }

eBool CmdAtDccHdlcPktGenChannelDisable(char argc, char **argv)
    {
    return ChannelAttributeSet(argc, argv, cAtFalse, (ChannelAttributeSetFunc)Tha6029DccHdlcPktGenChannelEnable);
    }

eBool CmdAtDccHdlcPktGenChannelHeaderVlanIdSet(char argc, char **argv)
    {
    return ChannelAttributeSet(argc, argv, AtStrToDw(argv[1]), (ChannelAttributeSetFunc)Tha6029DccHdlcPktGenChannelHeaderVlanIdSet);
    }

eBool CmdAtDccHdlcPktGenChannelHeaderDestMacLsbSet(char argc, char **argv)
    {
    return ChannelAttributeSet(argc, argv, AtStrToDw(argv[1]), (ChannelAttributeSetFunc)Tha6029DccHdlcPktGenChannelHeaderDestMacLsbSet);
    }

eBool CmdAtDccHdlcPktGenChannelLengthModeSet(char argc, char **argv)
    {
    uint32 mode = LengthModeFromString(argv[1]);
    return ChannelAttributeSet(argc, argv, mode, (ChannelAttributeSetFunc)Tha6029DccHdlcPktGenChannelLengthModeSet);
    }

eBool CmdAtDccHdlcPktGenGenChannelPayloadPatternSet(char argc, char **argv)
    {
    uint32 mode = CliPrbsModeStringToEnum(argv[1]);
    return ChannelAttributeSet(argc, argv, mode, (ChannelAttributeSetFunc)Tha6029DccHdlcPktGenChannelPayloadPatternSet);
    }

eBool CmdAtDccHdlcPktGenChannelFixPatternSet(char argc, char **argv)
    {
    return ChannelAttributeSet(argc, argv, AtStrToDw(argv[1]), Tha6029DccHdlcPktGenChannelFixPatternSet);
    }

eBool CmdAtDccHdlcPktGenChannelNumPktSet(char argc, char **argv)
    {
    return ChannelAttributeSet(argc, argv, AtStrToDw(argv[1]), Tha6029DccHdlcPktGenChannelNumPktSet);
    }

eBool CmdAtDccHdlcPktGenChannelShow(char argc, char **argv)
    {
    uint32 i;
    tTab *tabPtr;
    AtList hdlcList = CliAtDccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    const char *heading[] =  {"DCC", "enable", "Vlan", "Dest_Mac[7:0]", "LengthMode", " PatternMode", "FixedPattern", "NumPkts"};

    /* Get PW List */
    AtUnused(argc);
    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) hdlcList);
        return cAtFalse;
        }

    tabPtr = TableAlloc(numberHdlc, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject) hdlcList);
        return cAtFalse;
        }

    for (i = 0; i < numberHdlc; i++)
        {
        uint32 value;
        eBool enabled;
        const char *strValue;
        uint32 colum = 0;
        AtHdlcChannel hdlc = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        StrToCell(tabPtr, i, colum++, AtChannelIdString((AtChannel) hdlc));

        enabled = Tha6029DccHdlcPktGenChannelIsEnabled(hdlc);
        ColorStrToCell(tabPtr, i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        value = Tha6029DccHdlcPktGenChannelHeaderVlanIdGet(hdlc);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "0x%04x"));

        value = Tha6029DccHdlcPktGenChannelHeaderDestMacLsbGet(hdlc);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "0x%02x"));

        value = Tha6029DccHdlcPktGenChannelLengthModeGet(hdlc);
        strValue = LengthModeToString(value);
        StrToCell(tabPtr, i, colum++, strValue ? strValue : "Error");

        value = Tha6029DccHdlcPktGenChannelPayloadPatternGet(hdlc);
        strValue = CliPrbsModeToString(value);
        StrToCell(tabPtr, i, colum++, strValue ? strValue : "Error");

        value = Tha6029DccHdlcPktGenChannelFixPatternGet(hdlc);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "0x%02x"));

        value = Tha6029DccHdlcPktGenChannelNumPktGet(hdlc);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));
        }

    AtObjectDelete((AtObject)hdlcList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDccHdlcPktGenChannelCountersShow(char argc, char **argv)
    {
    uint32 i;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;

    AtList hdlcList = CliAtDccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    const char *heading[] =  {"DCC", "rxGoodPkts", "RxFcs", "RxAbort", "RxPktErrData", "RxPktErrSeq", "ChannelIdError"};

    /* Get PW List */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    if (argc >= 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject) hdlcList);
        return cAtFalse;
        }

    if (!silent)
        {
        tabPtr = TableAlloc(numberHdlc, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            AtObjectDelete((AtObject) hdlcList);
            return cAtFalse;
            }
        }

    for (i = 0; i < numberHdlc; i++)
        {
        uint32 value;
        uint32 colum = 0;
        AtHdlcChannel hdlc = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        StrToCell(tabPtr, i, colum++, AtChannelIdString((AtChannel) hdlc));

        value = Tha6029DccHdlcPktGenChannelCounterGet(hdlc, cAtHdlcChannelCounterTypeRxGoodPackets, readingMode);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));

        value = Tha6029DccHdlcPktGenChannelCounterGet(hdlc, cAtHdlcChannelCounterTypeRxFcsErrors, readingMode);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));

        value = Tha6029DccHdlcPktGenChannelCounterGet(hdlc, cAtHdlcChannelCounterTypeRxAborts, readingMode);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));

        value = Tha6029DccHdlcPktGenChannelCounterGet(hdlc, cAtHdlcChannelCounterTypeRxPacketsDataError, readingMode);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));

        value = Tha6029DccHdlcPktGenChannelCounterGet(hdlc, cAtHdlcChannelCounterTypeRxPacketsSeqError, readingMode);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));

        value = Tha6029DccHdlcPktGenChannelCounterGet(hdlc, cAtHdlcChannelCounterTypeRxChannelIdError, readingMode);
        StrToCell(tabPtr, i, colum++, CliNumber2String(value, "%u"));
        }

    AtObjectDelete((AtObject)hdlcList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDebugPktLoopbackSet(char argc, char **argv)
    {
    eAtRet ret;
    uint32 loopback;
    eBool convertSuccess;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get interrupt mask value from input string */
    if ((loopback = PktLoopbackMaskFromString(argv[0])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid state: ");
        CliExpectedValuesPrint(cSevCritical, cAtPktLoopbackTypeStr, mCount(cAtPktLoopbackTypeValue));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    ret = Tha6029DccHdlcSoELoopbackTypeSet(CliDevice(), loopback, enable);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Set Tha6029DccHdlcSoELoopbackTypeSet, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

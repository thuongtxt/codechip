/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtSdhPath.c
 *
 * Created Date: Mar 27, 2016
 *
 * Description : Debug CLIs for SDH Path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../../../../driver/src/generic/sdh/AtSdhPathInternal.h"
#include "../../../../driver/src/implement/codechip/Tha60210061/sdh/Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PohEnable(char argc, char **argv, eBool enabled, eAtRet (*EnableFunc)(AtSdhPath self, eBool enabled))
    {
    uint32 numberPaths, i;
    AtSdhPath *sdhVcs;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of paths */
    sdhVcs = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "No channels, check hierarchy\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i++)
        {
        eAtRet ret = EnableFunc(sdhVcs[i], enabled);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot %s POH on %s, ret = %s\r\n",
                     enabled ? "enable" : "disable",
                     CliChannelIdStringGet((AtChannel)sdhVcs[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtSdhPathPohMonitorEnable(char argc, char **argv)
    {
    return PohEnable(argc, argv, cAtTrue, AtSdhPathPohMonitorEnable);
    }

eBool CmdAtSdhPathPohMonitorDisable(char argc, char **argv)
    {
    return PohEnable(argc, argv, cAtFalse, AtSdhPathPohMonitorEnable);
    }

eBool CmdAtSdhPathPohInsertionEnable(char argc, char **argv)
    {
    return PohEnable(argc, argv, cAtTrue, AtSdhPathPohInsertionEnable);
    }

eBool CmdAtSdhPathPohInsertionDisable(char argc, char **argv)
    {
    return PohEnable(argc, argv, cAtFalse, AtSdhPathPohInsertionEnable);
    }

eBool CmdAtSdhPathPohShow(char argc, char **argv)
    {
    uint32 numberPaths, i;
    AtSdhPath *paths;
    tTab *tabPtr;
    const char *pHeading[] = {"PathId", "monitor", "insertion"};

    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i++)
        {
        AtChannel thisPath = (AtChannel)paths[i];
        eBool enabled;
        uint32 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(thisPath));

        enabled = AtSdhPathPohMonitorIsEnabled(paths[i]);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        enabled = AtSdhPathPohInsertionIsEnabled(paths[i]);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathCounterVirtualFifoGet(char argc, char **argv)
    {
    tTab *tabPtr = NULL;
    AtSdhPath *paths;
    uint32 numberPaths, i;
    eAtHistoryReadingMode readMode;
    const char *pHeading[] = {"PathId", "POS DET", "NEG DET", "POS GEN", "NEG GEN"};
    tAtSdhPathCounters allCounters;
    uint32 (*AllCountersGetFunc)(AtSdhPath, tAtSdhPathCounters*) = Tha60210061ModuleSdhEc1PathVirtualFifoCounterGet;
    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    readMode = CliHistoryReadingModeGet(argv[1]);
    if (readMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "Invalid reading mode\r\n");
        return cAtFalse;
        }

    if (readMode == cAtHistoryReadingModeReadToClear)
        AllCountersGetFunc = Tha60210061ModuleSdhEc1PathVirtualFifoCounterClear;

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)paths[i]));
        AllCountersGetFunc(paths[i], &allCounters);

        ColorStrToCell(tabPtr, i, column++, CliNumber2String(allCounters.rxPPJC, "%u"), (allCounters.rxPPJC == 0) ? cSevNormal : cSevCritical);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(allCounters.rxNPJC, "%u"), (allCounters.rxNPJC == 0) ? cSevNormal : cSevCritical);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(allCounters.txPPJC, "%u"), (allCounters.txPPJC == 0) ? cSevNormal : cSevCritical);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(allCounters.txNPJC, "%u"), (allCounters.txNPJC == 0) ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

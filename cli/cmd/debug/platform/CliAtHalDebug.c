/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : CliAtHalDebug.c
 *
 * Created Date: Sep 18, 2015
 *
 * Description : HAL debug command
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "AtCli.h"
#include "../../../../driver/src/generic/man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtFile m_halLogFile = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DidReadRegister(AtHal hal, void *listener, uint32 address, uint32 value)
    {
    AtUnused(listener);

    if (m_halLogFile == NULL)
        return;

    AtFilePrintf(m_halLogFile, "Read  0x%08x, value 0x%08x (FPGA at %s)\r\n",
                 address, value, AtHalToString(hal));
    AtFileFlush(m_halLogFile);
    }

static void DidWriteRegister(AtHal hal, void *listener, uint32 address, uint32 value)
    {
    AtUnused(listener);

    if (m_halLogFile == NULL)
        return;

    AtFilePrintf(m_halLogFile, "Write 0x%08x, value 0x%08x (FPGA at %s)\r\n",
                 address, value, AtHalToString(hal));
    AtFileFlush(m_halLogFile);
    }

static eBool LogOpen(const char *fileName)
    {
    m_halLogFile = AtStdFileOpen(AtStdSharedStdGet(), fileName, cAtFileOpenModeWrite | cAtFileOpenModeTruncate);
    return m_halLogFile ? cAtTrue : cAtFalse;
    }

static void LogClose(void)
    {
    AtStdFileClose(m_halLogFile);
    m_halLogFile = NULL;
    }

static void HalCleanup(void)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    uint8 numDevices = 0, device_i;
    AtDevice *devices = AtDriverAddedDevicesGet(driver, &numDevices);

    for (device_i = 0; device_i < numDevices; device_i++)
        {
        AtDevice device = devices[device_i];
        uint8 core_i;

        for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
            {
            AtHal hal = AtDeviceIpCoreHalGet(device, core_i);
            AtHal cachedHalSim = AtHalCachedHalSimGet(hal);
            AtHalDelete(cachedHalSim);
            AtHalCachedHalSimSet(hal, NULL);
            }
        }
    }

static void TextUIWillStop(AtTextUI self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    LogClose();
    HalCleanup();
    }

static tAtHalListener *HalListener(void)
    {
    static tAtHalListener listener;
    static tAtHalListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidReadRegister  = DidReadRegister;
    listener.DidWriteRegister = DidWriteRegister;
    pListener = &listener;

    return pListener;
    }

static tAtTextUIListerner *TextUIListener(void)
    {
    static tAtTextUIListerner listener;
    static tAtTextUIListerner *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.WillStop = TextUIWillStop;
    pListener = &listener;

    return pListener;
    }

static eBool ReadWriteAccessEnable(char argc, char **argv, eBool enabled)
    {
    AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), 0);
    AtUnused(argc);
    AtUnused(argv);
    AtHalReadOperationEnable(hal, enabled);
    AtHalWriteOperationEnable(hal, enabled);
    AtDeviceSimulate(CliDevice(), enabled ? cAtFalse : cAtTrue);
    return cAtTrue;
    }

static eBool LogEnable(char argc, char **argv, eBool enabled)
    {
    AtDevice device = CliDevice();
    uint8 core_i;

    AtUnused(argc);
    AtUnused(argv);

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(device, core_i);
        if (enabled)
            AtHalListenerAdd(hal, NULL, HalListener());
        else
            AtHalListenerRemove(hal, NULL, HalListener());
        }

    if (enabled)
        {
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);
        }
    else
        {
        LogClose();
        AtTextUIListenerRemove(AtCliSharedTextUI(), TextUIListener());
        }

    return cAtTrue;
    }

static uint32 RealProductCode(uint32 productCode)
    {
    uint32 familyMask  = cBit31_28;
    uint32 familyShift = 28;
    uint8 family = (uint8)mRegField(productCode, family);
    if (family == 0xF)
        family = 0x6;
    mRegFieldSet(productCode, family, family);
    return productCode;
    }

static eBool CachingEnable(char argc, char **argv, eBool enabled)
    {
    AtDevice device = CliDevice();
    uint32 productCode = RealProductCode(AtDeviceProductCodeGet(device));
    uint8 core_i;

    AtUnused(argc);
    AtUnused(argv);

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
        {
        AtHal halSim;
        AtHal hal = AtDeviceIpCoreHalGet(device, core_i);
        AtHalCacheEnable(hal, enabled);

        if (!enabled)
            continue;

        halSim = AtHalCachedHalSimGet(hal);
        if (halSim)
            continue;

        halSim = AtHalSimCreateForProductCode(productCode, cAtHalSimDefaultMemorySizeInMbyte);
        AtHalCachedHalSimSet(hal, halSim);
        }

    if (enabled)
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);

    return cAtTrue;
    }

eBool CmdAtHalReadWriteAccessEnable(char argc, char **argv)
    {
    return ReadWriteAccessEnable(argc, argv, cAtTrue);
    }

eBool CmdAtHalReadWriteAccessDisable(char argc, char **argv)
    {
    return ReadWriteAccessEnable(argc, argv, cAtFalse);
    }

eBool CmdAtHalLogFileSet(char argc, char **argv)
    {
    AtUnused(argc);

    LogClose();

    if (AtStrcmp(argv[0], "none") == 0)
        return cAtTrue;

    if (LogOpen(argv[0]))
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: cannot open file %s\r\n", argv[0]);
    return cAtFalse;
    }

eBool CmdAtHalLogEnable(char argc, char **argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtHalLogDisable(char argc, char **argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

eBool CmdAtHalCachingEnable(char argc, char **argv)
    {
    return CachingEnable(argc, argv, cAtTrue);
    }

eBool CmdAtHalCachingDisable(char argc, char **argv)
    {
    return CachingEnable(argc, argv, cAtFalse);
    }

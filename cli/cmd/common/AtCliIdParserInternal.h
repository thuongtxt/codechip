/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtCliIdParserInternal.h
 * 
 * Created Date: Apr 6, 2015
 *
 * Description : CLI ID parser
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLIIDPARSERINTERNAL_H_
#define _ATCLIIDPARSERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtObject.h"
#include "AtCliIdParser.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCliIdParserMethods
    {
    uint32 (*MpBundleListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize);
    uint32 (*PppLinkListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize);
    uint32 (*EncapChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize, uint32 encapType);
    }tAtCliIdParserMethods;

typedef struct tAtCliIdParser
    {
    const tAtCliIdParserMethods *methods;
    }tAtCliIdParser;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCliIdParser AtCliIdParserObjectInit(AtCliIdParser self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLIIDPARSERINTERNAL_H_ */


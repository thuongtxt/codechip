/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtChannelObserverInternal.h
 * 
 * Created Date: Aug 26, 2019
 *
 * Description : Channel observer representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELOBSERVERINTERNAL_H_
#define _ATCHANNELOBSERVERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtChannel.h"
#include "AtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelObserver
    {
    tAtObject super;

    /* Private data */
    AtChannel observedChannel;
    void *userData;

    eBool deleting; /* Request for deletion */
    uint32 refs;
    uint32 changedDefects;
    uint32 currentDefects;
    uint32 changedFailures;
    uint32 currentFailures;
    uint32 changedParams;

    uint8 clockStates[32];
    uint8 clockIndex;  /* Index to array of clock states */
    }tAtChannelObserver;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelObserver AtChannelObserverObjectInit(AtChannelObserver self, AtChannel observedChannel, void *userData);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELOBSERVERINTERNAL_H_ */


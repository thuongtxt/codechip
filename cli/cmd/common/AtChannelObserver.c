/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtChannelObserver.c
 *
 * Created Date: Apr 22, 2019
 *
 * Description : Channel observer implemetation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtCli.h"
#include "AtChannelObserverInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAlarmsUpdate(oldEvents, oldStatus, newEvents, newStatus)              \
    do {                                                                       \
        uint32 saveStatus = oldStatus;                                         \
        oldEvents |= newEvents;                                                \
        oldStatus = (~newEvents & saveStatus) | (newEvents & newStatus);       \
    } while (0)

#define mEventReadAndClear(events)                                             \
    do {                                                                       \
        uint32 value = events;                                                 \
        events = events ^ value;                                               \
        return value;                                                          \
    } while (0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void OverrideAtObject(AtChannelObserver self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtChannelObserver self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtChannelObserver);
    }

AtChannelObserver AtChannelObserverObjectInit(AtChannelObserver self, AtChannel observedChannel, void *userData)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override((AtChannelObserver)self);
    m_methodsInit = 1;

    self->observedChannel = observedChannel;
    self->userData = userData;

    return self;
    }

AtChannelObserver AtChannelObserverNew(AtChannel observedChannel, void *userData)
    {
    /* Allocate memory */
    AtChannelObserver newObserver = AtOsalMemAlloc(ObjectSize());
    if (newObserver == NULL)
        return NULL;

    /* Construct it */
    return AtChannelObserverObjectInit(newObserver, observedChannel, userData);
    }

AtChannel AtChannelObserverChannelGet(AtChannelObserver self)
    {
    return (self) ? self->observedChannel : NULL;
    }

void *AtChannelObserverUserDataGet(AtChannelObserver self)
    {
    return (self) ? self->userData : NULL;
    }

void AtChannelObserverDelete(AtChannelObserver self)
    {
    if (self == NULL)
        return;

    if (self->refs > 0)
        {
        self->deleting = cAtTrue;
        return;
        }

    AtObjectDelete((AtObject)self);
    }

eBool AtChannelObserverIsDeleting(AtChannelObserver self)
    {
    if (self)
        return self->deleting;
    return cAtFalse;
    }

void AtChannelObserverDefectsUpdate(AtChannelObserver self, uint32 changedAlarms, uint32 currentStatus)
    {
    if (self)
        mAlarmsUpdate(self->changedDefects, self->currentDefects, changedAlarms, currentStatus);
    }

void AtChannelObserverFailuresUpdate(AtChannelObserver self, uint32 changedFailures, uint32 currentStatus)
    {
    if (self)
        mAlarmsUpdate(self->changedFailures, self->currentFailures, changedFailures, currentStatus);
    }

void AtChannelObserverTcaUpdate(AtChannelObserver self, uint32 changedParams)
    {
    if (self)
        self->changedParams |= changedParams;
    }

void AtChannelObserverClockStatePush(AtChannelObserver self, uint8 changedState)
    {
    if (self == NULL)
        return;

    if (self->clockIndex < 32)
        {
        self->clockStates[self->clockIndex] = changedState;
        self->clockIndex++;
        }
    }

uint32 AtChannelObserverDefectGet(AtChannelObserver self)
    {
    return (self) ? self->currentDefects : 0;
    }

uint32 AtChannelObserverDefectHistoryGet(AtChannelObserver self)
    {
    return (self) ? self->changedDefects : 0;
    }

uint32 AtChannelObserverDefectHistoryClear(AtChannelObserver self)
    {
    if (self)
        mEventReadAndClear(self->changedDefects);
    return 0;
    }

uint32 AtChannelObserverFailureGet(AtChannelObserver self)
    {
    return (self) ? self->currentFailures : 0;
    }

uint32 AtChannelObserverFailureHistoryGet(AtChannelObserver self)
    {
    return (self) ? self->changedFailures : 0;
    }

uint32 AtChannelObserverFailureHistoryClear(AtChannelObserver self)
    {
    if (self)
        mEventReadAndClear(self->changedFailures);
    return 0;
    }

uint32 AtChannelObserverTcaHistoryGet(AtChannelObserver self)
    {
    return (self) ? self->changedParams : 0;
    }

uint32 AtChannelObserverTcaHistoryClear(AtChannelObserver self)
    {
    if (self)
        mEventReadAndClear(self->changedParams);
    return 0;
    }

const char *AtChannelObserverClockStateString(AtChannelObserver self)
    {
    static char stateString[256];
    static char subString[32];
    uint32 sizeString = 0;
    uint8 i;

    if (self == NULL)
        return "null";

    if (self->clockIndex == 0)
        return "NotChanged";

    if (self->clockIndex >= 32)
        return "out-of-memory";

    AtOsalMemInit(stateString, 0, sizeof(stateString));
    AtOsalMemInit(subString, 0, sizeof(subString));

    for (i = 0; i < self->clockIndex; i++)
        {
        sizeString += AtSnprintf(subString, 16, "->%s", CliClockStateString(self->clockStates[i]));
        if (sizeString > sizeof(stateString))
            {
            AtSprintf(stateString, "out-of-memory");
            return stateString;
            }

        AtStrncat(stateString, subString, sizeString);
        }

    return &stateString[2];
    }

void AtchannelObserverClockStateFlush(AtChannelObserver self)
    {
    if (self)
        {
        AtOsalMemInit(self->clockStates, 0, sizeof(self->clockStates));
        self->clockIndex = 0;
        }
    }

AtChannelObserver AtChannelObserverGet(AtChannelObserver self)
    {
    if (self == NULL)
        return 0;

    self->refs++;
    return self;
    }

uint32 AtChannelObserverPut(AtChannelObserver self)
    {
    if (self == NULL)
        return 0;

    if (self->refs)
        self->refs--;
    return self->refs;
    }

/*------------------------------------------------------------------------------     
 *                                                                                  
 * COPYRIGHT (C) 2008 Arrive Technologies Inc.                                      
 *                                                                                  
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information                     
 * is prohibited except by express written agreement with Arrive Technologies.      
 *                                                                                  
 * Module      : Common Command                                       
 *                                                                                  
 * File        : basiccmd.c                                      
 *                                                                                  
 * Created Date: 28-May-08                                                          
 *                                                                                  
 * Description : 
 *                                                                                  
 * Notes       : None                                                               
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "attypes.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local variables --------------------------------*/ 

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/ 

/*--------------------------- Forward declarations ---------------------------*/
static eBool ColorEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtStdColorEnable(AtStdSharedStdGet(), enabled);
    return cAtTrue;
    }

eBool CmdScriptRun(char argc, char **argv)
    {
    eBool success = AtTextUIScriptRun(AtCliSharedTextUI(), argv[0]);

    AtUnused(argc);

    if (!success)
        AtPrintc(cSevCritical, "ERROR: Run script %s fail\r\n", argv[0]);

    return success;
    }

eBool CmdHistoryShow(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtTextUIHistoryShow(AtCliSharedTextUI());
    return cAtTrue;
    }

eBool CmdHistoryClear(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtTextUIHistoryClear(AtCliSharedTextUI());
    return cAtTrue;
    }

eBool CmdAtTextUIScriptStart(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtTextUIScriptStart(AtCliSharedTextUI());
    return cAtTrue;
    }

eBool CmdAtTextUIScriptCancel(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtTextUIScriptCancel(AtCliSharedTextUI());
    return cAtTrue;
    }

eBool CmdAtTextUIScriptFinish(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtTextUIScriptFinish(AtCliSharedTextUI());
    return cAtTrue;
    }

eBool CmdAutoTestEnable(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AutotestEnable(cAtTrue);
    AtCliModeClr(cAtCliModeAll);
    AtCliModeSet(cAtCliModeAutotest);

    return cAtTrue;
    }

eBool CmdAutoTestDisable(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AutotestEnable(cAtFalse);
    AtCliModeClr(cAtCliModeAll);

    return cAtTrue;
    }

eBool CmdCliList(char argc, char **argv)
    {
    AtTextUICmdShow(AtCliSharedTextUI(), argv, (uint32)argc);
    return cAtTrue;
    }

eBool CmdHelpShow(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtTextUIHelp(AtCliSharedTextUI());
    return cAtTrue;
    }

eBool CmdColorEnable(char argc, char **argv)
    {
    return ColorEnable(argc, argv, cAtTrue);
    }

eBool CmdColorDisable(char argc, char **argv)
    {
    return ColorEnable(argc, argv, cAtFalse);
    }

eBool CmdColorShow(char argc, char **argv)
    {
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    enabled = AtStdColorIsEnabled(AtStdSharedStdGet());
    AtPrintc(cSevNormal, "Color is ");
    AtPrintc(enabled ? cSevInfo : cSevCritical, "%s\r\n", enabled ? "enabled" : "disabled");

    return cAtTrue;
    }

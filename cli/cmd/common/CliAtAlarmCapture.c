/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Alarm captures
 *
 * File        : CliAtAlarmCapture.c
 *
 * Created Date: Dec 20, 2015
 *
 * Description : Alarm captures
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tClockStateEvent
    {
    AtChannel channel;
    uint8 state;
    }tClockStateEvent;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtList m_ChannelEvents = NULL;
static AtList m_ClockStateEvents = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtCapturedEvent *DummyEventCreate(AtChannel channel)
    {
    tAtCapturedEvent* dummyEvent = AtOsalMemAlloc(sizeof(tAtCapturedEvent));
    AtOsalMemInit(dummyEvent, 0, sizeof(tAtCapturedEvent));

    dummyEvent->channel = channel;
    return dummyEvent;
    }

static tAtCapturedEvent *EventClone(tAtCapturedEvent* event)
    {
    tAtCapturedEvent* cloned = AtOsalMemAlloc(sizeof(tAtCapturedEvent));
    AtOsalMemCpy(cloned, event, sizeof(tAtCapturedEvent));

    return cloned;
    }

static void AlarmEventCleanup(void)
    {
    tAtCapturedEvent *capturedEvent;

    if (m_ChannelEvents == NULL)
        return;

    while ((capturedEvent = (tAtCapturedEvent *)AtListObjectRemoveAtIndex(m_ChannelEvents, 0)) != NULL)
        AtOsalMemFree(capturedEvent);

    AtObjectDelete((AtObject)m_ChannelEvents);
    m_ChannelEvents = NULL;
    }

static void ClockStateEventCleanup(void)
    {
    tClockStateEvent *stateEvent;

    if (m_ClockStateEvents == NULL)
        return;

    while ((stateEvent = (tClockStateEvent *)AtListObjectRemoveAtIndex(m_ClockStateEvents, 0)) != NULL)
        AtOsalMemFree(stateEvent);

    AtObjectDelete((AtObject)m_ClockStateEvents);
    m_ClockStateEvents = NULL;
    }

static void CliChannelEventsCleanup(AtTextUI self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AlarmEventCleanup();
    ClockStateEventCleanup();
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;

    m_listener.WillStop = CliChannelEventsCleanup;
    m_listener.DidStop  = NULL;
    return &m_listener;
    }

static void AllClockStateEventCleanupPerChannel(AtChannel currentChannel)
    {
    uint32 state_i;

    for (state_i = 0; state_i < AtListLengthGet(m_ClockStateEvents); state_i++)
        {
        tClockStateEvent *stateEvent = (tClockStateEvent *)AtListObjectGet(m_ClockStateEvents, state_i);

        if (stateEvent->channel != currentChannel)
            continue;

        AtListObjectRemoveAtIndex(m_ClockStateEvents, state_i);
        AtOsalMemFree(stateEvent);
        }
    }

AtList CliCapturedEventFilterByChannels(AtChannel* channelList, uint32 numChannels, AtList capturedEvents, eAtHistoryReadingMode readMode)
    {
    uint32 channel_i;
    eBool channelHasEvent;

    if (m_ChannelEvents == NULL)
        {
        m_ChannelEvents    = AtListCreate(0);
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);
        }

    if (m_ChannelEvents == NULL)
        return NULL;

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        tAtCapturedEvent *capturedEvent;
        AtIterator iteratorEvent = AtListIteratorCreate(capturedEvents);

        channelHasEvent = cAtFalse;
        while ((capturedEvent = (tAtCapturedEvent *)AtIteratorNext(iteratorEvent)) != NULL)
            {
            if (capturedEvent->channel == channelList[channel_i])
                {
                AtListObjectAdd(m_ChannelEvents, (AtObject)EventClone(capturedEvent));
                channelHasEvent = cAtTrue;
                if (readMode == cAtHistoryReadingModeReadToClear)
                    {
                    AtIteratorRemove(iteratorEvent);
                    AtOsalMemFree(capturedEvent);
                    continue;
                    }
                }
            }

        if (!channelHasEvent) /* Dummy event when there is no event on channel */
            AtListObjectAdd(m_ChannelEvents, (AtObject)DummyEventCreate(channelList[channel_i]));

        AtObjectDelete((AtObject)iteratorEvent);
        }

    return m_ChannelEvents;
    }

static const char *AllClockState2String(tAtCapturedEvent *anEvent)
    {
    uint32 event_i;
    static char stateString[256];
    static char subString[32];
    uint32 sizeString = 0;

    if (AtListLengthGet(m_ClockStateEvents) == 0)
        return "NotChanged";

    AtOsalMemInit(stateString, 0, sizeof(stateString));
    AtOsalMemInit(subString, 0, sizeof(subString));
    for (event_i = 0; event_i < AtListLengthGet(m_ClockStateEvents); event_i++)
        {
        tClockStateEvent *clockEvent = (tClockStateEvent *)AtListObjectGet(m_ClockStateEvents, event_i);

        if (clockEvent->channel == anEvent->channel)
            {
            sizeString += AtSnprintf(subString, 16, "->%s", CliClockStateString(clockEvent->state));
            if (sizeString > sizeof(stateString))
                {
                AtSprintf(stateString, "out-of-memory");
                return stateString;
                }

            AtStrncat(stateString, subString, sizeString);
            }
        }

    return &stateString[2];
    }

/*
 * Put event to table, assume number of table's row is equal to events list length
 */
void CliPutEventsToTableAndDoStatistics(tTab *tabPtr,
                         AtList events,
                         const uint32 *alarms,
                         uint32 numAlarm,
                         uint32 clockStateMask,
                         eAtHistoryReadingMode readMode,
                         tAtCliUpsrInterruptStatistic *statistic)
    {
    uint32 row_i, alarm_i;
    tAtCapturedEvent *anEvent;
    eBool isSet =  cAtFalse, isClear = cAtFalse, isNoChanged =  cAtFalse;

    if (statistic)
        AtOsalMemInit(statistic, 0, sizeof(tAtCliUpsrInterruptStatistic));

    row_i = 0;
    while ((anEvent = (tAtCapturedEvent*)AtListObjectRemoveAtIndex(events, 0)) != NULL)
        {
        StrToCell(tabPtr, row_i, 0, (char *)CliChannelIdStringGet((AtChannel)anEvent->channel));
        for (alarm_i = 0; alarm_i < numAlarm; alarm_i++)
            {
            if (anEvent->changedAlarms & alarms[alarm_i] & clockStateMask)
                ColorStrToCell(tabPtr, row_i, alarm_i + 1, AllClockState2String(anEvent), cSevNormal);
            else
                CliCapturedAlarmToCellAndDoStatistics(tabPtr, row_i, alarm_i + 1, anEvent->changedAlarms, anEvent->currentStatus, alarms[alarm_i],
                                                      &isSet, &isClear, &isNoChanged);
            }

        if (readMode == cAtHistoryReadingModeReadToClear)
            AllClockStateEventCleanupPerChannel(anEvent->channel);
        AtOsalMemFree(anEvent);
        row_i++;
        if (statistic)
            {
            statistic->setNums += (isSet == cAtTrue) ? 1 : 0;
            statistic->clearNums += (isClear == cAtTrue) ? 1 : 0;
            statistic->unchangedNums += (isNoChanged == cAtTrue) ? 1 : 0;
            }
        }
    }

void CliPutEventsToTable(tTab *tabPtr,
                         AtList events,
                         const uint32 *alarms,
                         uint32 numAlarm,
                         uint32 clockStateMask,
                         eAtHistoryReadingMode readMode)
    {
    CliPutEventsToTableAndDoStatistics(tabPtr, events, alarms, numAlarm, clockStateMask, readMode, NULL);
    }

static void PushAllClockStateToListEvent(tAtCapturedEvent *oldEvent)
    {
    tClockStateEvent *stateEvent = AtOsalMemAlloc(sizeof(tClockStateEvent));

    if (stateEvent == NULL)
          return;

    if (m_ClockStateEvents == NULL)
        {
        m_ClockStateEvents = AtListCreate(0);
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);
        }

    stateEvent->state = AtChannelClockStateGet(oldEvent->channel);
    stateEvent->channel = oldEvent->channel;
    AtListObjectAdd(m_ClockStateEvents, (AtObject)stateEvent);
    }

void CliPushLastEventToListEvent(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, AtList listCapturedEvents, uint32 clockStateMask)
    {
    tAtCapturedEvent *oldEvent, *currentEvent;
    AtIterator iteratorEvent = AtListIteratorCreate(listCapturedEvents);

    while ((oldEvent = (tAtCapturedEvent *)AtIteratorNext(iteratorEvent)) != NULL)
        {
        if (oldEvent->channel == channel)
            {
            AtIteratorRemove(iteratorEvent);
            AtOsalMemFree(oldEvent);
            }
        }

    currentEvent = AtOsalMemAlloc(sizeof(tAtCapturedEvent));
    if (currentEvent == NULL)
          return;

    currentEvent->channel = channel;
    currentEvent->changedAlarms = changedAlarms;
    currentEvent->currentStatus = currentStatus;
    AtListObjectAdd(listCapturedEvents, (AtObject)currentEvent);
    if (currentEvent->changedAlarms & clockStateMask)
        PushAllClockStateToListEvent(currentEvent);
    AtObjectDelete((AtObject)iteratorEvent);
    }

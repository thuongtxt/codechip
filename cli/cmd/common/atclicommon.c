/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATCLICOMMON
 *
 * File        : atclicommon.c
 *
 * Created Date: 28-Nov-11
 *
 * Description : This file contain all common ID and Type of CLI layer
 *
 * Notes       : None
 *
 ----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCommon.h"
#include "atclib.h"
#include "AtCliModule.h"
#include "AtDriver.h"
#include "AtTokenizer.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE
#define vlanStringMin "0.0.0"
#define vlanStringMax "7.1.4095"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtHistoryReadingModeStr[] =
    {
    "ro", "r2c"
    };

static const eAtHistoryReadingMode cAtHistoryReadingModeVal[] =
    {
    cAtHistoryReadingModeReadOnly, cAtHistoryReadingModeReadToClear
    };

static const char * cAtLedStateStr[] = {
                                      "off",
                                      "on",
                                      "blink"
                                      };

static const eAtLedState cAtLedStateVal[]  = {
                                             cAtLedStateOff,
                                             cAtLedStateOn,
                                             cAtLedStateBlink
                                             };

static const char * cAtBerRateStr[] ={"1e-2", "1e-3", "1e-4", "1e-5", "1e-6", "1e-7", "1e-8", "1e-9", "1e-10"};
static const eAtBerRate cAtBerRateValue[]={cAtBerRate1E2, cAtBerRate1E3, cAtBerRate1E4, cAtBerRate1E5,
                                           cAtBerRate1E6, cAtBerRate1E7, cAtBerRate1E8,
                                           cAtBerRate1E9, cAtBerRate1E10};

static const char *cAtTimingModeStr[] =
    {
    "system", "loop", "sdh_sys", "sdh_line", "prc", "ext#1", "ext#2", "slave", "acr", "dcr","free_running", "none", sAtNotApplicable
    };

static const eAtTimingMode cAtTimingModeVal[] =
    {
    cAtTimingModeSys,
    cAtTimingModeLoop,
    cAtTimingModeSdhSys,
    cAtTimingModeSdhLineRef,
	cAtTimingModePrc,
    cAtTimingModeExt1Ref,
    cAtTimingModeExt2Ref,
    cAtTimingModeSlave,
    cAtTimingModeAcr,
    cAtTimingModeDcr,
    cAtTimingModeFreeRunning,
    cAtTimingModeUnknown,
    cAtTimingModeNotApplicable
    };

static const char *cAtClockStateStr[] = {"unknown", sAtNotApplicable, "init", "hold-over", "learning", "locked"};
static const eAtClockState cAtClockStateVal[] = {
                                           cAtClockStateUnknown,
                                           cAtClockStateNotApplicable,
                                           cAtClockStateInit,
                                           cAtClockStateHoldOver,
                                           cAtClockStateLearning,
                                           cAtClockStateLocked
                                          };

static const char * cAtLoopbackModeStr[] = {"release", "local", "remote"};
static const eAtLoopbackMode cAtLoopbackModeVal[]={cAtLoopbackModeRelease, cAtLoopbackModeLocal, cAtLoopbackModeRemote };

static const char *cAtBitOrderStr[]  = {"msb", "lsb", sAtNotApplicable};
static const uint32 cAtBitOrderVal[] = {cAtBitOrderMsb, cAtBitOrderLsb, cAtBitOrderNotApplicable};

static eBool m_TimeLoggingEnable = cAtFalse;
static eBool m_interruptLogEnable = cAtFalse;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *VlanTag2Str(const tAtEthVlanTag *vlanTag)
    {
    static char vlanTagBuf[16];
    AtSprintf(vlanTagBuf, "%d.%d.%d", vlanTag->priority, vlanTag->cfi, vlanTag->vlanId);

    return vlanTagBuf;
    }

static eAtSevLevel ColorOfCounterType(eAtCounterType counterType, uint64 counterValue)
    {
    if (counterType == cAtCounterTypeGood)
        {
        if (counterValue == cInvalidValue)
            return cSevWarning;
        return (counterValue > 0) ? cSevInfo : cSevCritical;
        }

    if (counterType == cAtCounterTypeError)
        return (counterValue > 0) ? cSevCritical : cSevNormal;

    if (counterType == cAtCounterTypeNeutral)
        {
        if (counterValue == cInvalidValue)
            return cSevWarning;
        return (counterValue > 0) ? cSevInfo : cSevNormal;
        }

    return cSevNormal;
    }

static void PrintCounter32Bit(char *buf, eBool isValidCounter, uint32 counterValue)
    {
    if (isValidCounter)
        {
        if (counterValue == cInvalidValue)
            AtSprintf(buf, "0x%x", counterValue);
        else
            AtSprintf(buf, "%u", counterValue);
        }
    else
        AtSprintf(buf, "Error");
    }

static void PrintCounter64Bit(char *buf, eBool isValidCounter, uint64 counterValue)
    {
    if (isValidCounter)
        {
        if (counterValue == cInvalidValue)
            AtSprintf(buf, "0x%llx", counterValue);
        else
            AtSprintf(buf, "%llu", counterValue);
        }
    else
        AtSprintf(buf, "Error");
    }

static void CliLongCounterPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           uint64 counterValue,
                           eAtCounterType counterType,
                           eBool isValidCounter,
                           eBool isLongCounter)
    {
    static char buf[50];
    eAtSevLevel color;

    if (tabPtr == NULL)
        return;

    /* Not applicable counter */
    if (counterType == cAtCounterTypeNA)
        {
        ColorStrToCell(tabPtr, row, column, sAtNotApplicable, cSevNormal);
        return;
        }

    if (counterType == cAtCounterTypeNotSupport)
        {
        ColorStrToCell(tabPtr, row, column, sAtNotSupported, cSevNormal);
        return;
        }

    /* Put to cell with correct color */
    color = isValidCounter ? ColorOfCounterType(counterType, counterValue) : cSevCritical;
    if (isLongCounter)
        PrintCounter64Bit(buf, isValidCounter, counterValue);
    else
        PrintCounter32Bit(buf, isValidCounter, (uint32)counterValue);

    ColorStrToCell(tabPtr, row, column, buf, color);
    }

eAtHistoryReadingMode CliHistoryReadingModeGet(char *readingModeString)
    {
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    eBool convertSuccess;

    if (readingModeString == NULL)
        return cAtHistoryReadingModeUnknown;

    mAtStrToEnum(cAtHistoryReadingModeStr,
                 cAtHistoryReadingModeVal,
                 readingModeString,
                 readingMode,
                 convertSuccess);

    return convertSuccess ? readingMode : cAtHistoryReadingModeUnknown;
    }

const char *CliValidHistoryReadingModes(void)
    {
    return "ro, r2c";
    }

eBool CliIsSilentIndication(char *verboseModeStr)
    {
    if (verboseModeStr == NULL)
        return cAtFalse;

    return (AtStrcmp(verboseModeStr, "silent") == 0) ? cAtTrue : cAtFalse;
    }

eBool CliHistoryReadingShouldSilent(eAtHistoryReadingMode readingMode, char *verboseModeStr)
    {
    if (readingMode != cAtHistoryReadingModeReadToClear)
        return cAtFalse;

    return CliIsSilentIndication(verboseModeStr);
    }

eAtLedState CliLedStateFromStr(const char *ledStateStr)
    {
    eAtLedState ledState = cAtLedStateOff;
    eBool isValid;

    mAtStrToEnum(cAtLedStateStr, cAtLedStateVal, ledStateStr, ledState, isValid);
    return isValid ? ledState : cAtLedStateOff;
    }

const char *CliLedStateStr(eAtLedState ledState)
    {
    static char buf[8];
    eBool isValid;

    mAtEnumToStr(cAtLedStateStr, cAtLedStateVal, ledState, buf, isValid);
    return isValid ? buf : NULL;
    }

eAtDirection CliDirectionFromStr(const char *directionStr)
    {
    if (AtStrcmp(directionStr, "rx") == 0)  return cAtDirectionRx;
    if (AtStrcmp(directionStr, "tx") == 0)  return cAtDirectionTx;
    if (AtStrcmp(directionStr, "all") == 0) return cAtDirectionAll;

    return cAtDirectionInvalid;
    }

const char *CliValidDirections(void)
    {
    return "rx, tx, all";
    }

eAtBerRate CliBerRateFromString(const char *berRateString)
    {
    eAtBerRate berRate = cAtBerRateUnknown;
    eBool convertSuccess;

    mAtStrToEnum(cAtBerRateStr,
                 cAtBerRateValue,
                 berRateString,
                 berRate,
                 convertSuccess);
    return convertSuccess ? berRate : cAtBerRateUnknown;
    }

char *CliBerRateString(eAtBerRate berRate)
    {
    static char berRateString[16];
    eBool convertSuccess;

    mAtEnumToStr(cAtBerRateStr, cAtBerRateValue, berRate, berRateString, convertSuccess);

    return convertSuccess ? berRateString : NULL;
    }

const char *CliValidBerRates(void)
    {
    return "1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9";
    }

eAtTimingMode CliTimingModeFromString(const char *timingModeString)
    {
    eAtTimingMode timingMode = cAtTimingModeUnknown;
    eBool convertSuccess;

    mAtStrToEnum(cAtTimingModeStr, cAtTimingModeVal, timingModeString, timingMode, convertSuccess);
    if (!convertSuccess)
        return cAtTimingModeUnknown;

    return timingMode;
    }

const char *CliValidTimingModes(void)
    {
    return "system/loop/sdh_sys/sdh_line/prc/ext#1/ext#2/slave/acr/dcr/epar";
    }

char *CliTimingModeString(eAtTimingMode timingMode)
    {
    static char timingModeString[16];
    eBool convertSuccess;

    mAtEnumToStr(cAtTimingModeStr, cAtTimingModeVal, timingMode, timingModeString, convertSuccess);
    if (!convertSuccess)
        return NULL;

    return timingModeString;
    }

const char *CliClockStateString(eAtClockState clockState)
    {
    uint8 i;

    for (i = 0; i < mCount(cAtClockStateVal); i++)
        {
        if (cAtClockStateVal[i] == clockState)
            return cAtClockStateStr[i];
        }

    return NULL;
    }

void CliCounterPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           uint32 counterValue,
                           eAtCounterType counterType,
                           eBool isValidCounter)
    {
    CliLongCounterPrintToCell(tabPtr, row, column, (uint64)counterValue, counterType, isValidCounter, cAtFalse);
    }

void CliCounter64BitPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           uint64 counterValue,
                           eAtCounterType counterType,
                           eBool isValidCounter)
    {
    CliLongCounterPrintToCell(tabPtr, row, column, (uint64)counterValue, counterType, isValidCounter, cAtTrue);
    }

eAtLoopbackMode CliLoopbackModeFromString(const char *loopbackModeString)
    {
    uint8 i;

    for (i = 0; i < mCount(cAtLoopbackModeStr); i++)
        {
        if (AtStrcmp(loopbackModeString, cAtLoopbackModeStr[i]) == 0)
            return cAtLoopbackModeVal[i];
        }

    return cAtLoopbackModeRelease;
    }

const char *CliLoopbackModeString(eAtLoopbackMode loopbackMode)
    {
    uint8 i;

    for (i = 0; i < mCount(cAtLoopbackModeVal); i++)
        {
        if (loopbackMode == cAtLoopbackModeVal[i])
            return cAtLoopbackModeStr[i];
        }

    return "unknown";
    }

const char *CliValidLoopbackModes(void)
    {
    return "release/local/remote";
    }

uint32 CliStringToEnum(const char *aString, const char *enumStrings[], const uint32 *enumValues, uint32 size, eBool *success)
    {
    uint32 i;

    if ((aString == NULL) || (enumStrings == NULL) || (enumValues == NULL))
        {
        if (success)
            *success = cAtFalse;
        return cInvalidValue;
        }

    for (i = 0; i < size; i++)
        {
        if (AtStrcmp(aString, enumStrings[i]) == 0)
            {
            if (success)
                *success = cAtTrue;
            return enumValues[i];
            }
        }

    if (success)
        *success = cAtFalse;
    return cInvalidValue;
    }

eBool CliBoolFromString(const char *pString)
    {
    eBool value, convertSuccess;
    mAtStrToBool(pString, value, convertSuccess);
    return (eBool)(convertSuccess ? value : cAtFalse);
    }

const char *CliBoolToString(eBool value)
    {
    return value ? "en" : "dis";
    }

const char *CliEnumToString(uint32 value, const char *enumStrings[], const uint32 *enumValues, uint32 size, eBool *success)
    {
    uint32 i;

    for (i = 0; i < size; i++)
        {
        if (value == enumValues[i])
            {
            if (success)
                *success = cAtTrue;
            return enumStrings[i];
            }
        }

    if (success)
        *success = cAtFalse;

    return NULL;
    }

void CliExpectedValuesPrint(eAtSevLevel color, const char *enumStrings[], uint32 size)
    {
    uint32 i;

    for (i = 0; i < size; i++)
        {
        if (i == 0)
            AtPrintc(color, "%s", enumStrings[i]);
        else
            AtPrintc(color, "/%s", enumStrings[i]);
        }
    }

char *AtCliNumber2String(const char *format, ...)
    {
    static char buffer[64];

    va_list args;
    AtStd std = AtStdSharedStdGet();

    if (std == NULL)
        return 0;

    va_start(args, format);
    AtStdSprintf(std, buffer, format, args);
    va_end(args);

    buffer[15] = '\0';
    return buffer;
    }

uint32 CliMaskFromString(char *maskString, const char **predefinedStrings, const uint32 *predefinedValues, uint32 numValues)
    {
    uint32 mask = 0;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(maskString, "|");

    while (AtTokenizerHasNextString(tokenizer))
        {
        char *token = AtTokenizerNextString(tokenizer);
        eBool convertSuccess;
        uint32 value;

        value = CliStringToEnum(token, predefinedStrings, predefinedValues, numValues, &convertSuccess);
        if (convertSuccess)
            mask |= value;
        }

    return mask;
    }

const char* CliAlarmMaskToString(const char **predefinedStrings, const uint32 *predefinedValues, uint32 numValues, uint32 alarmMask)
    {
    static char alarmString[256];
    static char subString[32];
    const char **alarmTypeStr = predefinedStrings;
    const uint32 *alarmTypeVal = predefinedValues;

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarmMask == 0)
        {
        AtSprintf(alarmString, "None");
        return alarmString;
        }

    /* There are alarms */
    else
        {
        uint32 i;
        uint32 sizeString = 0;
        for (i = 0; i < numValues; i++)
            {
            if (alarmMask & (alarmTypeVal[i]))
                {
                sizeString += AtSnprintf(subString, sizeof(subString), "|%s", alarmTypeStr[i]);
                if (sizeString > sizeof(alarmString))
                    {
                    AtSprintf(alarmString, "out-of-memory");
                    return alarmString;
                    }
                AtStrncat(alarmString, subString, sizeString);
                }
            }
        }

    return &alarmString[1];
    }

char *CliVlans2Str(const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    static char vlansStrBuf[32];
    AtOsalMemInit(vlansStrBuf, 0, sizeof(vlansStrBuf));

    /* No VLAN */
    if (cVlan == NULL)
        {
        AtStrcat(vlansStrBuf, "none");
        return vlansStrBuf;
        }

    /* One or two VLANs */
    AtStrcat(vlansStrBuf, VlanTag2Str(cVlan));
    if (sVlan != NULL)
        {
        AtStrcat(vlansStrBuf, ",");
        AtStrcat(vlansStrBuf, VlanTag2Str(sVlan));
        }

    return vlansStrBuf;
    }

eBool CliEthFlowVlanGet(char *pStrCvlan, char *pStrSvlan, tAtEthVlanDesc *desc)
    {
    uint32 vlanBuf[10];
    uint32 numItem;
    char minFormat[] = "0.0.0";
    char maxFormat[] = "7.1.4095";

    if (pStrCvlan != NULL)
        {
        if (!AtOsalMemCmp(pStrCvlan, "none", 4))
            return cAtTrue;

        IdComplexNumParse((void*)pStrCvlan,
                          minFormat,
                          maxFormat,
                          10,
                          vlanBuf,
                          &numItem);
        if (numItem == 3)
            {
            AtEthVlanTagConstruct((uint8)vlanBuf[0], (uint8)vlanBuf[1], (uint16)vlanBuf[2], &desc->vlans[0]);
            desc->numberOfVlans++;
            }
        else
            return cAtFalse;
        }

    if (pStrSvlan != NULL)
        {
        if (!AtOsalMemCmp(pStrSvlan, "none", 4))
            return cAtTrue;

        IdComplexNumParse((void*)pStrSvlan,
                          minFormat,
                          maxFormat,
                          10,
                          vlanBuf,
                          &numItem);
        if (numItem == 3)
            {
            AtEthVlanTagConstruct((uint8)vlanBuf[0], (uint8)vlanBuf[1], (uint16)vlanBuf[2], &desc->vlans[1]);
            desc->numberOfVlans++;
            }
        else
            return cAtFalse;
        }

    return cAtTrue;
    }

const char *CliExpectedVlanFormat(void)
    {
    return "priority.cfi.vlanId";
    }

uint32 CliId2DriverId(uint32 cliId)
    {
    return (uint32)(cliId - 1);
    }

void CliIdBuild(AtIdParser parser, uint32 *ids, uint8 numLevels)
    {
    uint32 level_i;

    for (level_i = 0; level_i < numLevels; level_i++)
        ids[level_i] = AtIdParserNextNumber(parser);
    }

AtModuleSdh CliModuleSdh(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

AtModulePdh CliModulePdh(void)
    {
    return (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);
    }

char *CliTimeInUsToString(uint32 timeInUs, char *buffer, uint32 bufferSize)
    {
    static char m_buffer[32];
    uint32 millisecond, second, minute, hour, day;

    if (buffer == NULL)
        {
        buffer = m_buffer;
        bufferSize = sizeof(m_buffer);
        AtOsalMemInit(buffer, 0, bufferSize);
        }

    /* Just us */
    millisecond = timeInUs / 1000;
    if (millisecond == 0)
        {
        AtSnprintf(buffer, bufferSize, "%u(us)", timeInUs);
        return buffer;
        }

    /* Not enough one second, just display ms and us */
    second = millisecond / 1000;
    if (second == 0)
        {
        AtSnprintf(buffer, bufferSize, "%u.%u(ms)", millisecond, (timeInUs % 1000));
        return buffer;
        }

    /* Not enough one minute, just display seconds and milliseconds */
    minute = second / 60;
    if (minute == 0)
        {
        AtSnprintf(buffer, bufferSize, "%u.%u(s)", second, (millisecond % 1000));
        return buffer;
        }

    /* Not enough 1 hour, just display minutes and seconds */
    hour = minute / 60;
    if (hour == 0)
        {
        AtSnprintf(buffer, bufferSize, "%u(m), %u(s)", minute, (second % 60));
        return buffer;
        }

    /* Not enough one day, just display hours and minutes */
    day = hour / 24;
    if (day == 0)
        {
        AtSnprintf(buffer, bufferSize, "%u(h), %u(m)", hour, (minute % 60));
        return buffer;
        }

    /* Display days and hours */
    AtSnprintf(buffer, bufferSize, "%u(d), %u(h)", day, hour % 24);
    return buffer;
    }

AtChannel AtChannelBoundChannelGet(AtChannel channel)
    {
    AtChannel boundChannel = (AtChannel)AtChannelBoundEncapChannelGet(channel);

    if (boundChannel == NULL)
        boundChannel = (AtChannel)AtChannelBoundPwGet(channel);

    return boundChannel;
    }

eBool CliTimeLoggingEnable(eBool enable)
    {
    m_TimeLoggingEnable = enable;
    return cAtTrue;
    }

eBool CliTimeLoggingIsEnabled(void)
    {
    return m_TimeLoggingEnable;
    }

void CliCapturedAlarmToCell(tTab* pTab, uint32 row, uint32 column, uint32 changedAlarms, uint32 currentStatus, uint32 alarmToDisplay)
    {
    if (changedAlarms & alarmToDisplay)
        ColorStrToCell(pTab, row, column, (currentStatus & alarmToDisplay) ? "Set" : "Clear", (currentStatus & alarmToDisplay) ? cSevCritical : cSevInfo);
    else
        StrToCell(pTab, row, column, "NotChanged");
    }

void CliCapturedAlarmToCellAndDoStatistics(tTab* pTab, uint32 row, uint32 column, uint32 changedAlarms, uint32 currentStatus, uint32 alarmToDisplay,
                            eBool *isSet, eBool *isClear, eBool *isNoChanged)
    {
    *isSet = cAtFalse;
    *isClear = cAtFalse;
    *isNoChanged = cAtFalse;
    if (changedAlarms & alarmToDisplay)
        {
        *isSet = (currentStatus & alarmToDisplay) ? cAtTrue : cAtFalse;
        *isClear = (currentStatus & alarmToDisplay) ? cAtFalse : cAtTrue;
        ColorStrToCell(pTab, row, column, (currentStatus & alarmToDisplay) ? "Set" : "Clear", (currentStatus & alarmToDisplay) ? cSevCritical : cSevInfo);
        }
    else
        {
        StrToCell(pTab, row, column, "NotChanged");
        *isNoChanged = cAtTrue;
        }
    }

eAtBitOrder CliBitOrderFromString(const char *bitOrderString)
    {
    eBool success = cAtFalse;
    eAtBitOrder bitOrder = CliStringToEnum(bitOrderString, cAtBitOrderStr, cAtBitOrderVal, mCount(cAtBitOrderVal), &success);
    return success ? bitOrder : cAtBitOrderUnknown;
    }

const char *CliBitOrderModeString(eAtBitOrder bitOrder)
    {
    return CliEnumToString(bitOrder, cAtBitOrderStr, cAtBitOrderVal, mCount(cAtBitOrderVal), NULL);
    }

const char *CliValidBitOrderModes(void)
    {
    return "msb, lsb";
    }

eBool CliInterruptLogEnable(eBool enable)
    {
    AtLogger logger = AtDriverLoggerGet(AtDriverSharedDriverGet());
    AtLoggerLevelEnable(logger, cAtLogLevelNormal, cAtTrue);

    m_interruptLogEnable = enable;
    return cAtTrue;
    }

eBool CliInterruptLogIsEnabled(void)
    {
    return m_interruptLogEnable;
    }

void CliChannelCounterPrintToCell(AtChannel self,
                                 uint32 (*CountersGet)(AtChannel, uint16),
                                 uint32 counterType,
                                 eAtCounterType counterShowType,
                                 tTab *tabPtr,
                                 uint32 row,
                                 uint32 column)
    {
    CliCounterPrintToCell(tabPtr,
                          row,
                          column,
                          CountersGet(self, (uint16)counterType),
                          AtChannelCounterIsSupported(self, (uint16)counterType) ? counterShowType : cAtCounterTypeNA,
                          cAtTrue);
     }

void CliChannelAlarmPrintToCell(AtChannel self, uint32 alarm, uint32 alarmType, tTab *table, uint32 row, uint32 column)
    {
    if (AtChannelAlarmIsSupported(self, alarmType))
        {
        uint32 raise = alarm & alarmType;
        ColorStrToCell(table, row, column++, raise ? "set" : "clear", raise ? cSevCritical : cSevInfo);
        }
    else
        ColorStrToCell(table, row, column, sAtNotApplicable, cSevNormal);
    }

void CliCounterTypePrintToCell(tTab *tabPtr,
                               uint32 row,
                               uint32 column,
                               uint32 counterValue,
                               eAtCounterType counterType,
                               const char *counterName)
    {
    uint32 numRows, numColumns;

    /* Need to add new rows */
    TableSizeGet(tabPtr, &numRows, &numColumns);
    if (row >= numRows)
        TableAddRow(tabPtr, 1);

    /* Print header row and value */
    StrToCell(tabPtr, row, 0, counterName);
    CliCounterPrintToCell(tabPtr, row++, column, counterValue, counterType, cAtTrue);
    }

tAtVlan *CliVlanFromString(char *vlanString, tAtVlan *vlan)
    {
    uint32 value;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(vlanString, ".");

    if (AtTokenizerNumStrings(tokenizer) != 4)
        return NULL;

    /* TPID */
    value = AtStrToDw(AtTokenizerNextString(tokenizer));
    if (value > cBit15_0)
        return NULL;
    vlan->tpid = (uint16)value;

    /* Priority */
    value = AtStrToDw(AtTokenizerNextString(tokenizer));
    if (value > 7)
        return NULL;
    vlan->priority = (uint8)value;

    /* CFI */
    value = AtStrToDw(AtTokenizerNextString(tokenizer));
    if (value > 1)
        return NULL;
    vlan->cfi = (uint8)value;

    /* VLAN ID */
    value = AtStrToDw(AtTokenizerNextString(tokenizer));
    if (value > cBit11_0)
        return NULL;
    vlan->vlanId = (uint16)value;

    return vlan;
    }

char *CliVlan2Str(const tAtVlan *vlanTag)
    {
    static char vlanTagBuf[16];
    AtSprintf(vlanTagBuf, "0x%.4x.%d.%d.%d", vlanTag->tpid, vlanTag->priority, vlanTag->cfi, vlanTag->vlanId);
    return vlanTagBuf;
    }

const char *CliFullVlanExpectedFormat(void)
    {
    return "tpid.priority.cfi.vlanId";
    }

char *CliDwordArrayToString(const uint32 *data, uint32 numDwords)
    {
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);
    uint32 dword_i;

    if (numDwords == 0)
        return NULL;

    buffer[0] = '\0';
    for (dword_i = 0; dword_i < numDwords; dword_i++)
        {
        char *dwordString = CliNumber2String(data[dword_i], "%08x");
        uint32 length = AtStrlen(dwordString);

        if (dword_i == 0)
            {
            AtStrncat(buffer, dwordString, bufferSize);
            bufferSize = bufferSize - length;
            }
        else
            {
            AtStrncat(buffer, ".", bufferSize);
            bufferSize = bufferSize - 1;
            AtStrncat(buffer, dwordString, bufferSize);
            bufferSize = bufferSize - length;
            }
        }

    return buffer;
    }

uint32 CliListToArray(AtList objects, AtObject *objectsBuffer, uint32 bufferSize)
    {
    uint32 numObjects = AtListLengthGet(objects);
    uint32 object_i;

    /* Prevent overflow */
    if (numObjects >= bufferSize)
        numObjects = bufferSize;

    for (object_i = 0; object_i < numObjects; object_i++)
        objectsBuffer[object_i] = AtListObjectGet(objects, object_i);

    return numObjects;
    }

const char *AtSevLevel2Str(eAtSevLevel level)
    {
    static const char *cAtSevLevelStr[] = {
                                           "critical",
                                           "major",
                                           "warning",
                                           "neutral",
                                           "debug",
                                           "info",
                                           "normal"
    };
    static uint32 cAtSevLevelVal[] = {cSevCritical,
                                      cSevMajor   ,
                                      cSevWarning ,
                                      cSevNeutral ,
                                      cSevDebug   ,
                                      cSevInfo    ,
                                      cSevNormal};
    return CliEnumToString(level, cAtSevLevelStr, cAtSevLevelVal, mCount(cAtSevLevelVal), NULL);
    }

eBool CliIpv4StringToArray(const char *pIPv4AddrStr, uint8 *pIpv4Addr)
    {
    uint32 numBytes = AtString2Bytes(pIPv4AddrStr, pIpv4Addr, cAtIpv4AddressLen, 10);
    return (numBytes == cAtIpv4AddressLen) ? cAtTrue : cAtFalse;
    }

char *CliIpV4ArrayToString(const uint8 *pIPv4Addr)
    {
    return AtBytes2String(pIPv4Addr, cAtIpv4AddressLen, 10);
    }

static void StringArrayFree(char **pStringArray, uint32 numItem)
    {
    uint32 itemIndex;

    for (itemIndex = 0; itemIndex < numItem; itemIndex++)
        AtOsalMemFree(pStringArray[itemIndex]);
    AtOsalMemFree(pStringArray);
    }

static eBool StringNumberIsValid(const char *pHexString)
    {
    uint32 i;

    if (AtStrlen(pHexString) > 4)
        return cAtFalse;

    for (i = 0; i < AtStrlen(pHexString); i++)
        {
        if (!AtIsHexDigit(pHexString[i]))
            return cAtFalse;
        }

    return cAtTrue;
    }

static uint32 IpV6String2Bytes(const char *pByteString, uint8 *pBytes)
    {
    eBool isValid = cAtTrue;
    char **pItemList;
    uint32 numItem = 0;
    uint32 itemIndex;
    uint8 item_i;
    char *doubleColonPtr;
    char *pStr;
    char *dupStr;

    /* Cannot parse a NULL string */
    if (pByteString == NULL)
        return cAtFalse;

    dupStr = AtStrdup(pByteString);
    pStr = dupStr;

    doubleColonPtr = AtStrstr(dupStr, "::");
    if (doubleColonPtr)
        {
        /* Check if more double colon in address string, it should be an incorrect
         * IPv6 address. */
        if (AtStrstr(doubleColonPtr + 1, "::") != NULL)
            {
            AtOsalMemFree(dupStr);
            return 0;
            }
        }

    /* Have all of word tokens */
    pItemList = StrTokenGet(dupStr, ":", &numItem);

    /* Without double colon, it must have enough 8 tokens. Otherwise, must be less than 8 */
    if (((doubleColonPtr == NULL) && (numItem != 8)) ||
        ((doubleColonPtr != NULL) && (numItem >= 8)))
        {
        StringArrayFree(pItemList, numItem);
        AtOsalMemFree(dupStr);
        return 0;
        }

    /* Convert all of them to bytes */
    itemIndex = 0;
    for (item_i = 0; item_i < 16; item_i = (uint8)(item_i + 2))
        {
        uint16 value;
        char *tokenPtr = NULL;

        if (itemIndex < numItem)
            {
            eBool nextItem = cAtTrue;

            /* Remaining address string is leaded by double colon (::) */
            if (AtStrstr(pStr, "::") == pStr)
                {
                uint8 remainItems = (uint8)(numItem - itemIndex);
                if (item_i < (16 - remainItems * 2))
                    nextItem = cAtFalse;
                }

            if (nextItem)
                {
                tokenPtr = AtStrstr(pStr, pItemList[itemIndex]);

                /* Get pointer to next item */
                pStr += AtStrlen(pItemList[itemIndex]);
                if (AtStrstr(pStr, "::") > pStr)
                    pStr++; /* Single colon here */
                }
            }

        /* Make sure that character is valid */
        if (!StringNumberIsValid(pItemList[itemIndex]))
            {
            isValid = cAtFalse;
            break;
            }

        /* No double colon */
        if (doubleColonPtr == NULL)
            {
            value = (uint16)AtStrtoul(tokenPtr, NULL, 16);
            itemIndex++;
            }

        /* Ended by a double colon */
        else if (tokenPtr == NULL)
            value = 0;

        else
            {
            /* Tokens trailing by double colon, ff80::xxxx:xxxx */
            if ((tokenPtr < doubleColonPtr))
                {
                value = (uint16)AtStrtoul(tokenPtr, NULL, 16);
                itemIndex++;
                }

            /* Tokens leading by double colon, xxxx:xxxx::1234:5678 */
            else
                {
                uint8 remainItems = (uint8)(numItem - itemIndex);
                if (item_i >= (16 - remainItems * 2))
                    {
                    value = (uint16)AtStrtoul(tokenPtr, NULL, 16);
                    itemIndex++;
                    }
                else
                    value = 0;
                }
            }

        pBytes[item_i]     = (uint8)((value & cBit15_8) >> 8);
        pBytes[item_i + 1] = (uint8)(value & cBit7_0);
        }

    /* Release resource */
    StringArrayFree(pItemList, numItem);
    AtOsalMemFree(dupStr);

    return isValid ? 16 : 0;
    }

eBool CliIpV6StringToArray(const char *pIPv6AddrStr, uint8 *pIpv6Addr)
    {
    uint32 numBytes = IpV6String2Bytes(pIPv6AddrStr, pIpv6Addr);
    return (numBytes == cAtIpv6AddressLen) ? cAtTrue : cAtFalse;
    }

static eBool IpV6ArrayIsAllZeros(const uint8 *pIPv6Addr)
    {
    uint8 byteId;
    for (byteId = 0; byteId < cAtIpv6AddressLen; byteId++)
        if (pIPv6Addr[byteId] != 0)
            return cAtFalse;

    return cAtTrue;
    }

static void IpV6ArrayToWords(const uint8 *pIPv6Array, uint16 *ipV6WordArray)
    {
    uint32 i;
    uint8 word_i = 0;

    for (i = 0; i < cAtIpv6AddressLen; i += 2U)
        {
        ipV6WordArray[word_i] = (uint16)((uint16)(pIPv6Array[i] << 8) & cBit15_8);
        ipV6WordArray[word_i] = (uint16)(ipV6WordArray[word_i] | pIPv6Array[i + 1]);
        word_i++;
        }
    }

char *CliIpV6ArrayToString(const uint8 *pIPv6Addr)
    {
    static char cAllZerosOmit[] = "::";
    static eBool lastWordIsAllzeros = cAtFalse;
    static char buf[50];
    uint16 ipv6Words[8];
    uint32 i;

    AtOsalMemInit((void*)buf, 0, sizeof(buf));

    if (IpV6ArrayIsAllZeros(pIPv6Addr))
        return cAllZerosOmit;

    IpV6ArrayToWords(pIPv6Addr, ipv6Words);
    for (i = 0; i < 8; i++)
        {
        char cBuf[10] = {0};

        if (ipv6Words[i] == 0)
            {
            if (!lastWordIsAllzeros)
                {
                AtSprintf(cBuf, "%s", cAllZerosOmit);
                lastWordIsAllzeros = cAtTrue;
                }
            }
        else
            {
            /* If last word is all-zeros, do not add ':' */
            if (lastWordIsAllzeros)
                {
                AtSprintf(cBuf, "%x", ipv6Words[i]);
                lastWordIsAllzeros = cAtFalse;
                }
            else
                AtSprintf(cBuf, "%s%x", (i) ? ":" : "", ipv6Words[i]);
            }

        AtStrcat(buf, cBuf);
        }

    return buf;
    }

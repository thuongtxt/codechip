/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtChannelObserver.h
 * 
 * Created Date: Jun 7, 2019
 *
 * Description : Channel observer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELOBSERVER_H_
#define _ATCHANNELOBSERVER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelObserver *AtChannelObserver;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelObserver AtChannelObserverNew(AtChannel observedChannel, void *userData);

AtChannel AtChannelObserverChannelGet(AtChannelObserver self);
void *AtChannelObserverUserDataGet(AtChannelObserver self);

void AtChannelObserverDelete(AtChannelObserver self);
eBool AtChannelObserverIsDeleting(AtChannelObserver self);

/* Data handling */
void AtChannelObserverDefectsUpdate(AtChannelObserver self, uint32 changedAlarms, uint32 currentStatus);
void AtChannelObserverFailuresUpdate(AtChannelObserver self, uint32 changedFailures, uint32 currentStatus);
void AtChannelObserverTcaUpdate(AtChannelObserver self, uint32 changedParams);
void AtChannelObserverClockStatePush(AtChannelObserver self, uint8 changedState);

/* Observed defects */
uint32 AtChannelObserverDefectGet(AtChannelObserver self);
uint32 AtChannelObserverDefectHistoryGet(AtChannelObserver self);
uint32 AtChannelObserverDefectHistoryClear(AtChannelObserver self);

/* Observed failures */
uint32 AtChannelObserverFailureGet(AtChannelObserver self);
uint32 AtChannelObserverFailureHistoryGet(AtChannelObserver self);
uint32 AtChannelObserverFailureHistoryClear(AtChannelObserver self);

/* Observed TCA */
uint32 AtChannelObserverTcaHistoryGet(AtChannelObserver self);
uint32 AtChannelObserverTcaHistoryClear(AtChannelObserver self);

/* Observed clock states */
const char *AtChannelObserverClockStateString(AtChannelObserver self);
void AtchannelObserverClockStateFlush(AtChannelObserver self);

/* Reference tracking for safe deletion */
AtChannelObserver AtChannelObserverGet(AtChannelObserver self);
uint32 AtChannelObserverPut(AtChannelObserver self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELOBSERVER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : CliAtChannel.c
 *
 * Created Date: Apr 22, 2019
 *
 * Description : CLI AtChannel interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "AtOsal.h"
#include "AtDictionary.h"
#include "AtIterator.h"
#include "AtCliModule.h"
#include "CliAtChannelObserver.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cTableHash4         2
#define cTableHash16        4
#define cTableHash128       7

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtTable *AtTable;

typedef struct tAtTableMethods
    {
    AtChannelObserver (*EntryCreate)(AtTable self, AtChannel channel);
    }tAtTableMethods;

typedef struct tAtTable
    {
    AtDictionary dict;
    AtOsalMutex lock;

    const tAtTableMethods *methods;
    }tAtTable;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtTextUIListerner   *m_textUIListener = NULL;

/* All tables */
static tAtTable  *m_pdhDe3Table = NULL;
static tAtTable  *m_pdhDe1Table = NULL;
static tAtTable  *m_sdhPathTable = NULL;
static tAtTable  *m_sdhLineTable = NULL;
static tAtTable  *m_pwTable = NULL;
static tAtTable  *m_gfpChannelTable = NULL;
static tAtTable  *m_vcgMemberTable = NULL;

static AtChannelObserver DefaultEntryCreate(AtTable self, AtChannel channel);
static AtChannelObserver VcgMemberEntryCreate(AtTable self, AtChannel channel);

static tAtTableMethods defaultTableMethods = {
        .EntryCreate = DefaultEntryCreate,
        };

static tAtTableMethods vcgMemberTableMethods = {
        .EntryCreate = VcgMemberEntryCreate,
        };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TableDelete(AtTextUI self, void *userData)
    {
    AtTable table = userData;
    AtUnused(self);
    AtDictionaryDeleteWithObjectHandler(table->dict, AtObjectDelete);
    table->dict = NULL;
    AtOsalMutexDestroy(table->lock);
    table->lock = NULL;
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;

    if (m_textUIListener)
        return m_textUIListener;

    AtOsalMemInit(&m_listener, 0, sizeof(tAtTextUIListerner));
    m_listener.WillStop = TableDelete;

    m_textUIListener = &m_listener;
    return m_textUIListener;
    }

static AtTable TableInit(AtTable table, uint32 order)
    {
    AtOsalMemInit(table, 0, sizeof(tAtTable));

    table->dict = AtDefaultDictionaryNew(order);
    table->lock = AtOsalMutexCreate();
    AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), table);

    return table;
    }

static AtChannelObserver TableCreateEntry(AtTable self, AtChannel channel)
    {
    if (self)
        return self->methods->EntryCreate(self, channel);
    return NULL;
    }

static AtChannelObserver DefaultEntryCreate(AtTable self, AtChannel channel)
    {
    return AtChannelObserverNew(channel, self);
    }

static AtChannelObserver VcgMemberEntryCreate(AtTable self, AtChannel channel)
    {
    return (AtChannelObserver)AtConcateMemberObserverNew(channel, self);
    }

static AtTable DefaultTableInit(AtTable table, uint32 order)
    {
    table = TableInit(table, order);
    if (table)
        table->methods = &defaultTableMethods;
    return table;
    }

static AtTable CliPdhDe3Table(void)
    {
    static tAtTable m_table;

    if (m_pdhDe3Table)
        return m_pdhDe3Table;

    m_pdhDe3Table = DefaultTableInit(&m_table, cTableHash16);
    return m_pdhDe3Table;
    }

static AtTable CliPdhDe1Table(void)
    {
    static tAtTable m_table;

    if (m_pdhDe1Table)
        return m_pdhDe1Table;

    m_pdhDe1Table = DefaultTableInit(&m_table, cTableHash128);
    return m_pdhDe1Table;
    }

static AtTable CliSdhPathTable(void)
    {
    static tAtTable m_table;

    if (m_sdhPathTable)
        return m_sdhPathTable;

    m_sdhPathTable = DefaultTableInit(&m_table, cTableHash128);
    return m_sdhPathTable;
    }

static AtTable CliSdhLineTable(void)
    {
    static tAtTable m_table;

    if (m_sdhLineTable)
        return m_sdhLineTable;

    m_sdhLineTable = DefaultTableInit(&m_table, cTableHash4);
    return m_sdhLineTable;
    }

static AtTable CliPwTable(void)
    {
    static tAtTable m_table;

    if (m_pwTable)
        return m_pwTable;

    m_pwTable = DefaultTableInit(&m_table, cTableHash128);
    return m_pwTable;
    }

static AtTable CliGfpChannelTable(void)
    {
    static tAtTable m_table;

    if (m_gfpChannelTable)
        return m_gfpChannelTable;

    m_gfpChannelTable = DefaultTableInit(&m_table, cTableHash16);
    return m_gfpChannelTable;
    }

static AtTable CliVcgMemberTable(void)
    {
    static tAtTable m_table;

    if (m_vcgMemberTable)
        return m_vcgMemberTable;

    m_vcgMemberTable = TableInit(&m_table, cTableHash128);
    if (m_vcgMemberTable)
        m_vcgMemberTable->methods = &vcgMemberTableMethods;
    return m_vcgMemberTable;
    }

static eAtRet TableObserverAdd(AtTable table, AtChannel channel, AtChannelObserver observer)
    {
    eAtRet ret;
    AtOsalMutexLock(table->lock);
    ret = AtDictionaryObjectAdd(table->dict, (const void*)channel, (AtObject)observer);
    AtOsalMutexUnLock(table->lock);
    return ret;
    }

static eAtRet TableObserverRemove(AtTable table, AtChannel channel)
    {
    eAtRet ret;
    AtOsalMutexLock(table->lock);
    ret = AtDictionaryObjectRemove(table->dict, (const void*)channel);
    AtOsalMutexUnLock(table->lock);
    return ret;
    }

static AtChannelObserver TableObserverGet(AtTable table, AtChannel channel)
    {
    AtObject ret;
    AtOsalMutexLock(table->lock);
    ret = AtDictionaryObjectGet(table->dict, (const void*)channel);
    AtOsalMutexUnLock(table->lock);
    return (AtChannelObserver)ret;
    }

static void TableObserverDelete(AtTable table, AtChannel channel)
    {
    AtChannelObserver observer = TableObserverGet(table, channel);
    if (observer == NULL)
        return;

    /* Only remove observer from the table when all references are dropped. */
    if (AtChannelObserverPut(observer) == 0)
        TableObserverRemove(table, channel);

    AtChannelObserverDelete(observer);
    }

/* Called when channel deleted */
static void WillDeleteObserver(AtChannel channel, void *userData)
    {
    AtTable table = userData;
    TableObserverDelete(table, channel);
    }

static tAtChannelEventListener *ChannelListener(void)
    {
    static tAtChannelEventListener listener;
    static tAtChannelEventListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.WillDelete = WillDeleteObserver;
    pListener = &listener;
    return pListener;
    }

static AtChannelObserver ObserverCreate(AtTable table, AtChannel channel)
    {
    eAtRet ret;
    AtChannelObserver observer;

    observer = TableCreateEntry(table, channel);
    if (observer == NULL)
        return NULL;

    ret = TableObserverAdd(table, channel, observer);
    if (ret != cAtOk)
        {
        AtObjectDelete((AtObject)observer);
        return NULL;
        }

    /* Register for future deletion when the associated channel is deleted. */
    ret = AtChannelEventListenerAddWithUserData(channel, ChannelListener(), table);
    if (ret != cAtOk)
        {
        TableObserverRemove(table, channel);
        AtObjectDelete((AtObject)observer);

        AtPrintc(cSevCritical, "ERROR: Cannot register \"WillDelete\" for channel %s\r\n",
                 CliChannelIdStringGet(channel));

        return NULL;
        }

    return observer;
    }

static AtChannelObserver ObserverGet(AtTable table, AtChannel channel)
    {
    AtChannelObserver observer = TableObserverGet(table, channel);
    if (observer)
        return observer;

    observer = ObserverCreate(table, channel);
    return observer;
    }

/* Called in CLI context */
static void ObserverDelete(AtTable table, AtChannel channel)
    {
    AtChannelObserver observer = TableObserverGet(table, channel);
    if (observer == NULL)
        return;

    /* If request deleting from user mapping change, just delete it */
    if ((AtChannelObserverPut(observer) == 0) && AtChannelObserverIsDeleting(observer))
        {
        TableObserverRemove(table, channel);
        AtObjectDelete((AtObject)observer);
        }
    }

/* CLI interfaces */

AtChannelObserver CliAtPdhDe3Observer(AtChannel de3)
    {
    return ObserverGet(CliPdhDe3Table(), de3);
    }

AtChannelObserver CliAtPdhDe3ObserverGet(AtChannel de3)
    {
    return AtChannelObserverGet(CliAtPdhDe3Observer(de3));
    }

AtChannelObserver CliAtPdhDe1Observer(AtChannel de1)
    {
    return ObserverGet(CliPdhDe1Table(), de1);
    }

AtChannelObserver CliAtPdhDe1ObserverGet(AtChannel de1)
    {
    return AtChannelObserverGet(CliAtPdhDe1Observer(de1));
    }

AtChannelObserver CliAtSdhPathObserver(AtChannel path)
    {
    return ObserverGet(CliSdhPathTable(), path);
    }

AtChannelObserver CliAtSdhPathObserverGet(AtChannel path)
    {
    return AtChannelObserverGet(CliAtSdhPathObserver(path));
    }

AtChannelObserver CliAtSdhLineObserver(AtChannel line)
    {
    return ObserverGet(CliSdhLineTable(), line);
    }

AtChannelObserver CliAtSdhLineObserverGet(AtChannel line)
    {
    return AtChannelObserverGet(CliAtSdhLineObserver(line));
    }

AtChannelObserver CliAtPwObserver(AtChannel pw)
    {
    return ObserverGet(CliPwTable(), pw);
    }

AtChannelObserver CliAtPwObserverGet(AtChannel pw)
    {
    return AtChannelObserverGet(CliAtPwObserver(pw));
    }

AtChannelObserver CliAtGfpChannelObserver(AtChannel channel)
    {
    return ObserverGet(CliGfpChannelTable(), channel);
    }

AtChannelObserver CliAtGfpChannelObserverGet(AtChannel channel)
    {
    return AtChannelObserverGet(CliAtGfpChannelObserver(channel));
    }

AtChannelObserver CliAtVcgMemberObserver(AtChannel member)
    {
    return ObserverGet(CliVcgMemberTable(), member);
    }

AtChannelObserver CliAtVcgMemberObserverGet(AtChannel member)
    {
    return AtChannelObserverGet(CliAtVcgMemberObserver(member));
    }

void CliAtPdhDe3ObserverDelete(AtChannel de3)
    {
    ObserverDelete(CliPdhDe3Table(), de3);
    }

void CliAtPdhDe1ObserverDelete(AtChannel de1)
    {
    ObserverDelete(CliPdhDe1Table(), de1);
    }

void CliAtSdhPathObserverDelete(AtChannel path)
    {
    ObserverDelete(CliSdhPathTable(), path);
    }

void CliAtSdhLineObserverDelete(AtChannel line)
    {
    ObserverDelete(CliSdhLineTable(), line);
    }

void CliAtPwObserverDelete(AtChannel pw)
    {
    ObserverDelete(CliPwTable(), pw);
    }

void CliAtGfpChannelObserverDelete(AtChannel channel)
    {
    ObserverDelete(CliGfpChannelTable(), channel);
    }

void CliAtVcgMemberObserverDelete(AtChannel member)
    {
    ObserverDelete(CliVcgMemberTable(), member);
    }

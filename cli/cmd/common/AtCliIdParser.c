
/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ID parser
 *
 * File        : AtIdParser.c
 *
 * Created Date: Oct 16, 2012
 *
 * Description : ID parser
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"

#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtModuleEth.h"
#include "AtEncapChannel.h"
#include "AtHdlcChannel.h"
#include "AtModuleSdh.h"
#include "AtPdhDe1.h"
#include "AtTokenizer.h"
#include "AtEncapBundle.h"
#include "../sdh/CliAtModuleSdh.h"
#include "AtCliIdParserInternal.h"
#include "../sdh/CliAtSdhLineDcc.h"

/*--------------------------- Define -----------------------------------------*/
#define cCliSharedChannelBufferSize (24 * 1024)
#define cCliSharedObjectBufferSize  (24 * 1024)
#define cCliSharedCharBufferSize    1024
#define sIdTypeBlankChar  " "
#define cFormatORSeparate "|"
#define cSeparateChar     ","

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtCliIdParserMethods m_methods;

static uint32 m_sharedIdBuffer[cCliSharedChannelBufferSize];
static AtChannel m_sharedChannelBuffer[cCliSharedChannelBufferSize];
static AtObject m_sharedObjectBuffer[cCliSharedObjectBufferSize];
static char m_sharedCharBuffer[cCliSharedCharBufferSize];

/* Global ID parser */
static AtCliIdParser m_sharedIdParser = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Get ID type and ID list.
 *
 * @param [in] pStrIdList String of ID List
 * @param [out] pIdType Point to ID type string
 * @param [out] pIdList Point to ID list string
 *
 * @return Duplicated string of pStrIdList on success and the caller must free
 *         this memory after usage */
char* IdTypeAndIdListGet(const char *pStrIdList, char **pIdType, char **pIdList)
    {
    char *pDupStrIdList;

    /* Get ID type and ID list */
    pDupStrIdList = AtStrdup(pStrIdList);
    *pIdType = AtStrtok(pDupStrIdList, sIdTypeSeparateChar);
    *pIdList = AtStrtok(NULL, sIdTypeBlankChar);
    if ((*pIdType == NULL) || (*pIdList == NULL))
        {
        AtOsalMemFree(pDupStrIdList);
        return NULL;
        }

    return pDupStrIdList;
    }

/*
 * Special case with NxDs0, there are two cases of nxDs0: FlatDe1Id.Ds0List and Tu1xId.Ds0List
 */
static char* NxDs0De1IdAndDs0IdListGet(const char *pStrIdList, char **pIdDe1, char **pIdDs0List)
    {
    char *pDupStrIdList = NULL;
    char* beginDs0List;

    if (pStrIdList == NULL)
        return NULL;

    /* Get from last "." */
    beginDs0List = AtStrrchr(pStrIdList, '.');
    if ((beginDs0List == NULL) || (AtStrlen(beginDs0List) < 2))
        return NULL;

    /* Ignore "." */
    beginDs0List++;

    /* Get ID type and ID list */
    pDupStrIdList = AtOsalMemAlloc(AtStrlen(pStrIdList) - AtStrlen(beginDs0List));
    if (pDupStrIdList == NULL)
        return NULL;

    AtStrncpy(pDupStrIdList, pStrIdList, (AtStrlen(pStrIdList) - AtStrlen(beginDs0List)) - 1);
    pDupStrIdList[(AtStrlen(pStrIdList) - AtStrlen(beginDs0List)) - 1] = '\0';

    if (pIdDs0List)
        *pIdDs0List = (char*)beginDs0List;

    if (pIdDe1)
        *pIdDe1 = pDupStrIdList;

    return pDupStrIdList;
    }

uint32 CliNxDs0ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    char *pDupString, *de1IdString = NULL, *ds0ListString = NULL;
    uint32 nxDs0Bitmap, numDe1s, de1_i, numNxDs0s;
    AtChannel* pdhDe1;
    AtIdParser idParser;

    pdhDe1 = AtOsalMemAlloc(sizeof(AtChannel) * bufferSize);
    if (pdhDe1 == NULL)
        return 0;

    pDupString = NxDs0De1IdAndDs0IdListGet(pStrIdList, &de1IdString, &ds0ListString);

    /* Get DE1 */
    if ((numDe1s = De1ListFromString(de1IdString, pdhDe1, bufferSize)) == 0)
        {
        AtOsalMemFree(pDupString);
        AtOsalMemFree(pdhDe1);
        return 0;
        }

    /* Get DS0 list */
    if (ds0ListString == NULL)
        {
        AtOsalMemFree(pDupString);
        AtOsalMemFree(pdhDe1);
        return 0;
        }

    /* Create bitmap */
    idParser = AtIdParserNew(ds0ListString, NULL, NULL);
    nxDs0Bitmap = 0;

    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 slotId = AtIdParserNextNumber(idParser);

        if (slotId > 32)
            {
            AtPrintc(cSevWarning, "WARNING: timeslot %u is out of range (0..31), it is ignored\r\n", slotId);
            continue;
            }

        nxDs0Bitmap |= (cBit0 << slotId);
        }

    numNxDs0s = 0;
    for (de1_i = 0; de1_i < numDe1s; de1_i++)
        {
        AtChannel channel = (AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)pdhDe1[de1_i], nxDs0Bitmap);
        if (channel)
            {
            channels[numNxDs0s] = channel;
            numNxDs0s++;
            }
        }

    /* Clean up */
    AtObjectDelete((AtObject)idParser);
    AtOsalMemFree(pDupString);
    AtOsalMemFree(pdhDe1);

    return numNxDs0s;
    }

static uint32 EthFlowListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numItems, i;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);

    /* Return AtEthFlow objects */
    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        channels[i] = (AtChannel)AtModuleEthFlowGet(ethModule, (uint16)(idBuffer[i] - 1));

    return numItems;
    }

static uint32 VcgListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numItems, i;
    AtModuleConcate concateModule = (AtModuleConcate)AtDeviceModuleGet(CliDevice(), cAtModuleConcate);
    uint32 numValidVcgs = 0;

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);

    /* Return AtEthPort objects */
    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        {
        channels[numValidVcgs] = (AtChannel)AtModuleConcateGroupGet(concateModule, (uint8)(idBuffer[i] - 1));
        if (channels[numValidVcgs])
            numValidVcgs = numValidVcgs + 1;
        }

    return numValidVcgs;
    }

static uint32 EncapChannelListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize, uint32 encapType)
    {
    AtUnused(pStrIdList);
    AtUnused(channels);
    AtUnused(bufferSize);
    AtUnused(encapType);
    return 0;
    }

static uint32 MpBundleListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    AtUnused(pStrIdList);
    AtUnused(channels);
    AtUnused(bufferSize);
    return 0;
    }

static uint32 PppLinkListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    AtUnused(pStrIdList);
    AtUnused(channels);
    AtUnused(bufferSize);
    return 0;
    }

static uint32 FrChannelListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numItems, i, numChannels = 0;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    AtEncapChannel encapChannel;

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);

    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        {
        encapChannel = AtModuleEncapChannelGet(encapModule, (uint16)(idBuffer[i] - 1));
        if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
            continue;

        if (AtHdlcChannelFrameTypeGet((AtHdlcChannel)encapChannel) != cAtHdlcFrmFr)
            continue;

        /* Just get valid channels */
        channels[numChannels] = (AtChannel)encapChannel;
        if (channels[numChannels])
            numChannels = numChannels + 1;
        }

    return numChannels;
    }

static uint32 MfrBundleListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numItems, i, numChannels = 0;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    AtHdlcBundle encapBundle;

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);

    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        {
        encapBundle = AtModuleEncapBundleGet(encapModule, (uint16)(idBuffer[i] - 1));
        if (AtEncapBundleTypeGet((AtEncapBundle)encapBundle) != cAtEncapBundleTypeMfr)
            continue;

        /* Just get valid channels */
        channels[numChannels] = (AtChannel)encapBundle;
        if (channels[numChannels])
            numChannels = numChannels + 1;
        }

    return numChannels;
    }

static void MethodsInit(AtCliIdParser self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MpBundleListFromString);
        mMethodOverride(m_methods, PppLinkListFromString);
        mMethodOverride(m_methods, EncapChannelListFromString);
        }

    mMethodsSet(self, &m_methods);
    }

static AtEthPort PortGet(AtModuleEth ethModule, uint32 portId)
    {
    return AtModuleEthPortGet(ethModule, (uint8)portId);
    }

static uint32 SubPortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 i, numChannels = 0;
    char maxFormat[16];
    char minFormat[16];
    AtIdParser idParser;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    AtSprintf(maxFormat, "%u.4", AtModuleEthMaxPortsGet(ethModule));
    AtSprintf(minFormat, "1.1");

    idParser = AtIdParserNew(pStrIdList, minFormat, maxFormat);

    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        AtEthPort port = PortGet(ethModule, CliId2DriverId(AtIdParserNextNumber(idParser)));
        channel[numChannels] = (AtChannel)AtEthPortSubPortGet(port, CliId2DriverId(AtIdParserNextNumber(idParser)));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);
    return numChannels;
    }

static uint32 PortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 numPorts, i;
    uint32 *idBuf;
    uint32 numChannels = 0;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(pStrIdList, idBuf, bufferSize)) == 0)
        return numChannels;

    for (i = 0; i < numPorts; i++)
        {
        channel[numChannels] = (AtChannel)PortGet(ethModule, idBuf[i] - 1);
        if (channel[numChannels])
            numChannels = numChannels + 1;

       if (numChannels >= bufferSize)
           break;
        }

    return numChannels;
    }

static uint32 DccChannelsFromString(char *idListString, AtChannel *channels, uint32 channelBufferSize)
    {
    AtList hdlcList;
    uint32 i, numHdlc;

    hdlcList = CliAtDccHdlcListFromString(idListString);
    numHdlc = AtListLengthGet(hdlcList);
    for (i = 0; i < numHdlc; i++)
        {
        if (i == channelBufferSize)
            break;

        channels[i] = (AtChannel)AtListObjectGet(hdlcList, i);
        }

    AtObjectDelete((AtObject)hdlcList);
    return i;
    }

AtList AtCliIdStrListSeparateByComma(char *pStrIdList)
    {
    char *strId;
    AtList listId = AtListCreate(0);

    if (pStrIdList == NULL)
       return listId;

    strId = AtStrtok(pStrIdList, cSeparateChar);
    if (strId != NULL)
        AtListObjectAdd(listId, (AtObject)(void *)strId);

    while (strId != NULL)
        {
        strId = AtStrtok(NULL, cSeparateChar);
        if (strId != NULL)
            AtListObjectAdd(listId, (AtObject)(void *)strId);
        }

    return listId;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCliIdParser);
    }

/*
 * Get the shared buffer used to store all numbers
 *
 * @param [out] bufferSize Buffer size
 *
 * @return The shared buffer
 */
uint32 *CliSharedIdBufferGet(uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = cCliSharedChannelBufferSize;

    return m_sharedIdBuffer;
    }

/*
 * To get list of ID from string
 *
 * @param idString String of ID List
 * @param [out] idList List of ID
 * @param [in] bufferSize Buffer size (just to prevent out of range)
 *
 * @return Number of IDs
 */
uint32 CliIdListFromString(char *idString, uint32 *idList, uint32 bufferSize)
    {
    uint32 numItems = 0, i;
    AtIdParser idParser = AtIdParserNew(idString, NULL, NULL);

    if (AtIdParserNumIdLevel(idParser) != 1)
        {
        AtObjectDelete((AtObject)idParser);
        return 0;
        }

    numItems = AtIdParserNumNumbers(idParser);
    if (numItems > bufferSize)
        {
        AtPrintc(cSevWarning, "ID buffer is not enough, only %u channels will be processed\r\n", bufferSize);
        numItems = bufferSize;
        }

    for (i = 0; i < numItems; i++)
        idList[i] = AtIdParserNextNumber(idParser);

    AtObjectDelete((AtObject)idParser);

    return numItems;
    }

/*
 * To parse string of channels to list of AtChannel(s).
 *
 * @param idList String of channel list
 * @param [out] channelList Buffer to store all of channels
 * @param bufferSize Buffer size
 *
 * @return Number of AtChannel(s)
 */
uint32 CliChannelListFromString(char *idList, AtChannel *channelList, uint32 bufferSize)
    {
    char *pDupString;
    char *idType, *idString;
    uint32 numChannels = 0;
    AtCliIdParser parser = AtCliIdParserSharedIdParserGet();

    /* Get ID type */
    pDupString = IdTypeAndIdListGet(idList, &idType, &idString);

    if ((idType == NULL) || (idString == NULL))
        return 0;

    /* DE1 */
    if (AtStrcmp(idType, sAtChannelTypePdhDe1) == 0)
        numChannels = De1ListFromString(idString, channelList, bufferSize);

    /* DE3 */
    else if (AtStrcmp(idType, sAtChannelTypePdhDe3) == 0)
        numChannels = CliDe3ListFromString(idString, channelList, bufferSize);

    /* NxDs0 */
    else if (AtStrcmp(idType, sAtChannelTypePdhNxDs0) == 0)
        numChannels = CliNxDs0ListFromString(idString, channelList, bufferSize);

    /* Ethernet flow */
    else if (AtStrcmp(idType, sAtChannelTypeEthFlow) == 0)
        numChannels = EthFlowListFromString(idString, channelList, bufferSize);

    /* Ethernet port */
    else if (AtStrcmp(idType, sAtChannelTypeEthPort) == 0)
        numChannels = CliEthPortListFromString(idString, channelList, bufferSize);

    /* HDLC, ATM-TC */
    else if (AtStrcmp(idType, sAtChannelTypeEncapHdlc)== 0)
        numChannels = mMethodsGet(parser)->EncapChannelListFromString(idString, channelList, bufferSize, cAtEncapHdlc);
    else if (AtStrcmp(idType, sAtChannelTypeEncapAtmTc)== 0)
        numChannels = mMethodsGet(parser)->EncapChannelListFromString(idString, channelList, bufferSize, cAtEncapAtm);

    /* PPP Bundle */
    else if (AtStrcmp(idType, sAtChannelTypePppBundle)== 0)
        numChannels = mMethodsGet(parser)->MpBundleListFromString(idString, channelList, bufferSize);

    /* PPP link */
    else if (AtStrcmp(idType, sAtChannelTypePppLink)== 0)
        numChannels = mMethodsGet(parser)->PppLinkListFromString(idString, channelList, bufferSize);

    /* VCG */
    else if (AtStrcmp(idType, sAtChannelTypeVcg)== 0)
        numChannels = VcgListFromString(idString, channelList, bufferSize);

    /* Frame Relay */
    else if (AtStrcmp(idType, sAtChannelTypeFrameRelay)== 0)
        numChannels = FrChannelListFromString(idString, channelList, bufferSize);

    /* Multi-Link Frame Relay */
    else if (AtStrcmp(idType, sAtChannelTypeMultiLinkFrameRelay)== 0)
        numChannels = MfrBundleListFromString(idString, channelList, bufferSize);

    /* DCC channel */
    else if (AtStrncmp(idType, sAtChannelTypeDcc, AtStrlen(sAtChannelTypeDcc)) == 0)
        numChannels = DccChannelsFromString(idString, channelList, bufferSize);

    else if (AtStrncmp(idType, sAtChannelTypeHdlcDcc, AtStrlen(sAtChannelTypeHdlcDcc)) == 0)
        numChannels = DccChannelsFromString(idString, channelList, bufferSize);

    /* For other cases, id list string may be of SDH channels */
    else
        numChannels = CliAtModuleSdhChanelsFromString(idList, channelList, bufferSize);

    /* Free memory */
    AtOsalMemFree(pDupString);

    return numChannels;
    }

AtList CliChannelsFromString(AtList channels, char *idList)
    {
    uint32 numberChannels, i;
    uint32 bufferSize;
    AtChannel *channelArray = CliSharedChannelListGet(&bufferSize);

    numberChannels = CliChannelListFromString(idList, channelArray, bufferSize);
    if (numberChannels == 0)
        return 0;

    for (i = 0; i < numberChannels; i++)
        AtListObjectAdd(channels, (AtObject)channelArray[i]);

    return channels;
    }

/*
 * Get the shared buffer used to store all of AtChannel(s)
 *
 * @param [out] bufferSize Buffer size
 *
 * @return Shared buffer
 */
AtChannel *CliSharedChannelListGet(uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = cCliSharedChannelBufferSize;

    return m_sharedChannelBuffer;
    }

/*
 * To get ID string of a channel including type and ID
 *
 * @param channel Channel
 *
 * @return Channel ID string
 */
char *CliChannelIdStringGet(AtChannel channel)
    {
    static char idString[128];

    if (channel == NULL)
        {
        AtSprintf(idString, "none");
        return idString;
        }

    AtSprintf(idString, "%s.%s", AtChannelTypeString(channel), AtChannelIdString(channel));

    return idString;
    }

AtCliIdParser AtCliIdParserObjectInit(AtCliIdParser self)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtCliIdParser AtCliIdParserSharedIdParserGet(void)
    {
    if (m_sharedIdParser == NULL)
        m_sharedIdParser = AtCliIdParserDefault();
    return m_sharedIdParser;
    }

void AtCliIdParserSharedIdParserSet(AtCliIdParser parser)
    {
    m_sharedIdParser = parser;
    }

AtCliIdParser AtCliIdParserTiny(void)
    {
    static tAtCliIdParser m_parser;
    static AtCliIdParser m_defaultParser = NULL;

    if (m_defaultParser == NULL)
        m_defaultParser = AtCliIdParserObjectInit((AtCliIdParser)&m_parser);
    return m_defaultParser;
    }

/*
 * Get the shared buffer used to store all of AtChannel(s)
 *
 * @param [out] bufferSize Buffer size
 *
 * @return Shared buffer
 */
AtObject *CliSharedObjectListGet(uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = cCliSharedObjectBufferSize;

    return m_sharedObjectBuffer;
    }

uint32 CliEthPortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    AtList portIdStrings = AtCliIdStrListSeparateByComma(pStrIdList);
    AtIterator iterator = AtListIteratorGet(portIdStrings);
    uint32 numLevel;
    char *strId;
    uint32 numPorts = 0;

    AtIteratorRestart(iterator);
    while ((strId = (char*)AtIteratorNext(iterator)) != NULL)
        {
        numLevel = CliAtNumIdLevel(strId);
        if (numLevel == 2)
            numPorts += SubPortListFromString(strId, &channel[numPorts], bufferSize - numPorts);
        else
            numPorts += PortListFromString(strId, &channel[numPorts], bufferSize - numPorts);
        }

    AtObjectDelete((AtObject)portIdStrings);
    return numPorts;
    }

char *CliSharedCharBufferGet(uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = sizeof(m_sharedCharBuffer);
    return m_sharedCharBuffer;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtChannelTable.h
 * 
 * Created Date: Jun 10, 2019
 *
 * Description : CLI AtChannel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATCHANNEL_H_
#define _CLIATCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannelObserver.h"
#include "AtConcateMemberObserver.h"
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Get observer from channel */
AtChannelObserver CliAtPdhDe3Observer(AtChannel de3);
AtChannelObserver CliAtPdhDe1Observer(AtChannel de1);
AtChannelObserver CliAtSdhPathObserver(AtChannel path);
AtChannelObserver CliAtSdhLineObserver(AtChannel line);
AtChannelObserver CliAtPwObserver(AtChannel pw);
AtChannelObserver CliAtGfpChannelObserver(AtChannel channel);
AtChannelObserver CliAtVcgMemberObserver(AtChannel member);

/* Get observer from channel and keep track */
AtChannelObserver CliAtPdhDe3ObserverGet(AtChannel de3);
AtChannelObserver CliAtPdhDe1ObserverGet(AtChannel de1);
AtChannelObserver CliAtSdhPathObserverGet(AtChannel path);
AtChannelObserver CliAtSdhLineObserverGet(AtChannel line);
AtChannelObserver CliAtPwObserverGet(AtChannel pw);
AtChannelObserver CliAtGfpChannelObserverGet(AtChannel channel);
AtChannelObserver CliAtVcgMemberObserverGet(AtChannel member);

/* Observer deletion */
void CliAtPdhDe3ObserverDelete(AtChannel de3);
void CliAtPdhDe1ObserverDelete(AtChannel de1);
void CliAtSdhPathObserverDelete(AtChannel path);
void CliAtSdhLineObserverDelete(AtChannel line);
void CliAtPwObserverDelete(AtChannel pw);
void CliAtGfpChannelObserverDelete(AtChannel channel);
void CliAtVcgMemberObserverDelete(AtChannel member);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATCHANNEL_H_ */


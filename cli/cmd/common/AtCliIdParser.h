/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtCliIdParser.h
 * 
 * Created Date: Apr 6, 2015
 *
 * Description : CLI ID parser
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLIIDPARSER_H_
#define _ATCLIIDPARSER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCliIdParser * AtCliIdParser;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCliIdParser AtCliIdParserSharedIdParserGet(void);
void AtCliIdParserSharedIdParserSet(AtCliIdParser parser);

/* Concrete parser */
AtCliIdParser AtCliIdParserTiny(void);
AtCliIdParser AtCliIdParserDefault(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLIIDPARSER_H_ */


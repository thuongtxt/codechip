/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FIXME module name
 * 
 * File        : AtConcateMemberObserver.h
 * 
 * Created Date: Aug 26, 2019
 *
 * Description : FIXME Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONCATEMEMBEROBSERVER_H_
#define _ATCONCATEMEMBEROBSERVER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtConcateMemberObserver *AtConcateMemberObserver;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelObserver AtConcateMemberObserverNew(AtChannel observedChannel, void *userData);

const char *AtConcateMemberObserverSourceStateString(AtConcateMemberObserver self);
const char *AtConcateMemberObserverSinkStateString(AtConcateMemberObserver self);

void AtConcateMemberObserverSourceStatePush(AtConcateMemberObserver self, uint8 changedState);
void AtConcateMemberObserverSinkStatePush(AtConcateMemberObserver self, uint8 changedState);
void AtConcateMemberObserverSourceStateFlush(AtConcateMemberObserver self);
void AtConcateMemberObserverSinkStateFlush(AtConcateMemberObserver self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCONCATEMEMBEROBSERVER_H_ */


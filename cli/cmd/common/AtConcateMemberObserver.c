/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtConcateMemberObserver.c
 *
 * Created Date: Aug 26, 2019
 *
 * Description : VCAT/LCAS member observer implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../concate/CliAtModuleConcate.h"
#include "AtChannelObserverInternal.h"
#include "AtConcateMemberObserver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtConcateMemberObserver
    {
    tAtChannelObserver super;

    /* Private data */
    uint8 sourceStates[32];
    uint8 sourceIndex;  /* Index to array of source states */
    uint8 sinkStates[32];
    uint8 sinkIndex;  /* Index to array of sink states */
    }tAtConcateMemberObserver;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Override(AtChannelObserver self)
    {
    AtUnused(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtConcateMemberObserver);
    }

static AtChannelObserver ObjectInit(AtChannelObserver self, AtChannel observedChannel, void *userData)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelObserverObjectInit(self, observedChannel, userData) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelObserver AtConcateMemberObserverNew(AtChannel observedChannel, void *userData)
    {
    /* Allocate memory */
    AtChannelObserver newObserver = AtOsalMemAlloc(ObjectSize());
    if (newObserver == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObserver, observedChannel, userData);
    }

const char *AtConcateMemberObserverSourceStateString(AtConcateMemberObserver self)
    {
    static char stateString[256];
    static char subString[32];
    uint32 sizeString = 0;
    uint8 i;

    if (self == NULL)
        return "null";

    if (self->sourceIndex == 0)
        return "NotChanged";

    if (self->sourceIndex >= 32)
        return "out-of-memory";

    AtOsalMemInit(stateString, 0, sizeof(stateString));
    AtOsalMemInit(subString, 0, sizeof(subString));

    for (i = 0; i < self->sourceIndex; i++)
        {
        sizeString += AtSnprintf(subString, 16, "->%s", CliAtLcasMemberSourceState2String(self->sourceStates[i]));
        if (sizeString > sizeof(stateString))
            {
            AtSprintf(stateString, "out-of-memory");
            return stateString;
            }

        AtStrncat(stateString, subString, sizeString);
        }

    return &stateString[2];
    }

void AtConcateMemberObserverSourceStatePush(AtConcateMemberObserver self, uint8 changedState)
    {
    if (self == NULL)
        return;

    if (self->sourceIndex < 32)
        {
        self->sourceStates[self->sourceIndex] = changedState;
        self->sourceIndex++;
        }
    }

void AtConcateMemberObserverSourceStateFlush(AtConcateMemberObserver self)
    {
    if (self)
        {
        AtOsalMemInit(self->sourceStates, 0, sizeof(self->sourceStates));
        self->sourceIndex = 0;
        }
    }

const char *AtConcateMemberObserverSinkStateString(AtConcateMemberObserver self)
    {
    static char stateString[256];
    static char subString[32];
    uint32 sizeString = 0;
    uint8 i;

    if (self == NULL)
        return "null";

    if (self->sinkIndex == 0)
        return "NotChanged";

    if (self->sinkIndex >= 32)
        return "out-of-memory";

    AtOsalMemInit(stateString, 0, sizeof(stateString));
    AtOsalMemInit(subString, 0, sizeof(subString));

    for (i = 0; i < self->sinkIndex; i++)
        {
        sizeString += AtSnprintf(subString, 16, "->%s", CliAtLcasMemberSinkState2String(self->sinkStates[i]));
        if (sizeString > sizeof(stateString))
            {
            AtSprintf(stateString, "out-of-memory");
            return stateString;
            }

        AtStrncat(stateString, subString, sizeString);
        }

    return &stateString[2];
    }

void AtConcateMemberObserverSinkStatePush(AtConcateMemberObserver self, uint8 changedState)
    {
    if (self == NULL)
        return;

    if (self->sinkIndex < 32)
        {
        self->sinkStates[self->sinkIndex] = changedState;
        self->sinkIndex++;
        }
    }

void AtConcateMemberObserverSinkStateFlush(AtConcateMemberObserver self)
    {
    if (self)
        {
        AtOsalMemInit(self->sinkStates, 0, sizeof(self->sinkStates));
        self->sinkIndex = 0;
        }
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtCliIdParserDefault.c
 *
 * Created Date: Apr 6, 2015
 *
 * Description : Default CLI ID parser
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtCli.h"
#include "AtCliIdParserInternal.h"
#include "AtModuleEncap.h"
#include "AtHdlcChannel.h"
#include "AtModulePpp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtCliIdParserDefault
    {
    tAtCliIdParser super;
    }tAtCliIdParserDefault;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtCliIdParserMethods m_AtCliIdParserOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EncapChannelListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize, uint32 encapType)
    {
    uint32 *idBuffer, numItems, i, numEncapChannels;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    AtEncapChannel encapChannel;

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);
    numEncapChannels = 0;

    /* Return AtEncapChannel objects */
    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        {
        encapChannel = AtModuleEncapChannelGet(encapModule, (uint16)(idBuffer[i] - 1));
        if (AtEncapChannelEncapTypeGet(encapChannel) != encapType)
            continue;

        /* Save */
        channels[numEncapChannels] = (AtChannel)encapChannel;
        numEncapChannels = numEncapChannels + 1;
        }

    return numEncapChannels;
    }

static uint32 MpBundleListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numItems, i;
    AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(CliDevice(), cAtModulePpp);

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);

    /* Return AtMpBundle objects */
    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        channels[i] = (AtChannel)AtModulePppMpBundleGet(pppModule, idBuffer[i] - 1);

    return numItems;
    }

static uint32 PppLinkListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numItems, i, numChannels = 0;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    AtEncapChannel encapChannel;

    /* Parse ID */
    idBuffer = CliSharedIdBufferGet(&numItems);
    numItems = CliIdListFromString(pStrIdList, idBuffer, numItems);

    /* Return AtMpBundle objects */
    if (numItems > bufferSize)
        numItems = bufferSize;
    for (i = 0; i < numItems; i++)
        {
        encapChannel = AtModuleEncapChannelGet(encapModule, (uint16)(idBuffer[i] - 1));
        if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
            continue;

        if (AtHdlcChannelFrameTypeGet((AtHdlcChannel)encapChannel) != cAtHdlcFrmPpp)
            continue;

        /* Just get valid channels */
        channels[numChannels] = (AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);
        if (channels[numChannels])
            numChannels = numChannels + 1;
        }

    return numChannels;
    }

static void OverrideAtCliIdParser(AtCliIdParser self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtCliIdParserOverride, mMethodsGet(self), sizeof(m_AtCliIdParserOverride));

        mMethodOverride(m_AtCliIdParserOverride, MpBundleListFromString);
        mMethodOverride(m_AtCliIdParserOverride, PppLinkListFromString);
        mMethodOverride(m_AtCliIdParserOverride, EncapChannelListFromString);
        }

    mMethodsSet(self, &m_AtCliIdParserOverride);
    }

static void Override(AtCliIdParser self)
    {
    OverrideAtCliIdParser(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCliIdParserDefault);
    }

static AtCliIdParser ObjectInit(AtCliIdParser self)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtCliIdParserObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtCliIdParser AtCliIdParserDefault(void)
    {
    static tAtCliIdParserDefault m_parser;
    static AtCliIdParser m_defaultParser = NULL;

    if (m_defaultParser == NULL)
        m_defaultParser = ObjectInit((AtCliIdParser)&m_parser);
    return m_defaultParser;
    }


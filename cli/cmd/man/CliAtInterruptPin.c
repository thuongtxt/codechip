/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtInterruptPin.c
 *
 * Created Date: Oct 1, 2016
 *
 * Description : Interrupt PIN CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtInterruptPin.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeSetFunc)(AtInterruptPin self, uint32 mode);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtTriggerModeVal[] = {
                                          cAtTriggerModeEdge,
                                          cAtTriggerModeLevelHigh,
                                          cAtTriggerModeLevelLow
                                          };

static const char *cAtTriggerModeStr[] = {
                                         "edge",
                                         "level-high",
                                         "level-low"
                                         };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtInterruptPin *PinsFromString(char *idString, uint32 *numPins)
    {
    uint32 numObjects = 0, numValidPins = 0;
    AtInterruptPin *pins = (AtInterruptPin *)CliSharedObjectListGet(&numObjects);
    char maxFormat[8];
    AtIdParser parser;
    AtDevice device = CliDevice();

    AtSnprintf(maxFormat, sizeof(maxFormat), "%u", AtDeviceNumInterruptPins(device));
    parser = AtIdParserNew(idString, "1", maxFormat);

    while (AtIdParserHasNextNumber(parser))
        {
        uint32 pinId = AtIdParserNextNumber(parser);
        AtInterruptPin pin = AtDeviceInterruptPinGet(device, pinId - 1);
        if (pin == NULL)
            continue;

        if (numValidPins >= numObjects)
            break;

        pins[numValidPins] = pin;
        numValidPins = numValidPins + 1;
        }

    AtObjectDelete((AtObject)parser);

    *numPins = numValidPins;
    return pins;
    }

static void ShowPinIdError(void)
    {
    AtPrintc(cSevCritical, "ERROR: No input PINs, please input correct ID. Expect: 1,2...\r\n");
    }

static eBool AttributeSet(char *idString, uint32 value, AttributeSetFunc func)
    {
    uint32 numPins = 0, pin_i;
    AtInterruptPin *pins;
    eBool success = cAtTrue;

    /* Get list of PINs */
    pins = PinsFromString(idString, &numPins);
    if (numPins == 0)
        {
        ShowPinIdError();
        return cAtFalse;
        }

    for (pin_i = 0; pin_i < numPins; pin_i++)
        {
        AtInterruptPin pin = pins[pin_i];
        eAtRet ret = func(pin, value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Configure PIN %u fail with ret = %s\r\n",
                 AtInterruptPinIdGet(pin) + 1,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool Trigger(char argc, char **argv, eBool triggered)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], triggered, (AttributeSetFunc)AtInterruptPinTrigger);
    }

eBool CmdAtInterruptPinTriggerModeSet(char argc, char **argv)
    {
    eBool success = cAtTrue;
    eAtTriggerMode mode = CliStringToEnum(argv[1], cAtTriggerModeStr, cAtTriggerModeVal, mCount(cAtTriggerModeVal), &success);
    if (success)
        return AttributeSet(argv[0], mode, (AttributeSetFunc)AtInterruptPinTriggerModeSet);

    AtPrintc(cSevCritical, "ERROR: Please input valid trigger mode. Expected: ");
    CliExpectedValuesPrint(cSevCritical, cAtTriggerModeStr, mCount(cAtTriggerModeVal));
    AtPrintc(cSevNormal, "\r\n");

    AtUnused(argc);

    return cAtFalse;
    }

eBool CmdAtInterruptPinTriggerStart(char argc, char **argv)
    {
    return Trigger(argc, argv, cAtTrue);
    }

eBool CmdAtInterruptPinTriggerStop(char argc, char **argv)
    {
    return Trigger(argc, argv, cAtFalse);
    }

eBool CmdAtInterruptPinShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"PIN", "triggerMode", "triggered", "asserted"};
    uint32 numPins = 0, pin_i;
    AtInterruptPin *pins = PinsFromString(argv[0], &numPins);

    if (numPins == 0)
        {
        ShowPinIdError();
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPins, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (pin_i = 0; pin_i < numPins; pin_i++)
        {
        AtInterruptPin pin = pins[pin_i];
        uint32 column = 0;
        const char *stringValue;
        eBool boolValue;

        /* PIN ID */
        StrToCell(tabPtr, pin_i, column++, CliNumber2String(AtInterruptPinIdGet(pin) + 1, "%u"));

        /* Trigger mode */
        stringValue = CliEnumToString(AtInterruptPinTriggerModeGet(pin),
                                      cAtTriggerModeStr, cAtTriggerModeVal, mCount(cAtTriggerModeVal),
                                      NULL);
        StrToCell(tabPtr, pin_i, column++, stringValue ? stringValue : "Error");

        /* Trigger */
        boolValue = AtInterruptPinIsTriggered(pin);
        ColorStrToCell(tabPtr, pin_i, column++, boolValue ? "start" : "stop", boolValue ? cSevInfo : cSevNormal);

        /* Trigger */
        boolValue = AtInterruptPinIsAsserted(pin);
        ColorStrToCell(tabPtr, pin_i, column++, boolValue ? "set" : "clear", boolValue ? cSevCritical : cSevInfo);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtUnused(argc);

    return cAtTrue;
    }

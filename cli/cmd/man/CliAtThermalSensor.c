/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtThermalSensor.c
 *
 * Created Date: Apr 5, 2016
 *
 * Description : Thermal sensor CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "../physical/CliAtSensor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtThermalSensorAlarmStr[] = {"overheat"};
static const uint32 cAtThermalSensorAlarmVal[] = {cAtThermalSensorAlarmOverheat};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSensor Sensor(void)
    {
    return (AtSensor)AtDeviceThermalSensorGet(CliDevice());
    }

static int32 CelciusValueFromString(char* string)
    {
    if (string == NULL)
        return 0x0;
    return AtAtoi(string);
    }

static eBool AlarmThresholdSet(char argc, char** argv,
                               eAtRet (*ThresholdSetFunc)(AtThermalSensor self, int32 celsius))
    {
    int32 celsius = CelciusValueFromString(argv[0]);
    eAtRet ret = cAtOk;

    AtUnused(argc);

    ret = ThresholdSetFunc((AtThermalSensor)Sensor(), celsius);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Can not set alarm threshold for Thermal sensor. ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static void AlarmChangeStateNotify(AtSensor sensor,
                                   uint32 changedAlarms,
                                   uint32 currentAlarms,
                                   void *listener)
    {
    CliAtSensorAlarmChangeNotify(sensor,
                                 listener,
                                 changedAlarms,
                                 currentAlarms,
                                 cAtThermalSensorAlarmStr,
                                 cAtThermalSensorAlarmVal,
                                 mCount(cAtThermalSensorAlarmVal));
    }

static tAtSensorEventListener *Listener(void)
    {
    static tAtSensorEventListener listener;
    static tAtSensorEventListener *pListener = NULL;
    
    if (pListener)
    	return pListener;
    	
    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.AlarmChangeStateNotify = AlarmChangeStateNotify;
    pListener = &listener;
    
    return pListener;
    }

eBool CmdAtDeviceThermalSensorAlarmSetThresholdSet(char argc, char** argv)
    {
    return AlarmThresholdSet(argc, argv, AtThermalSensorAlarmSetThresholdSet);
    }

eBool CmdAtDeviceThermalSensorAlarmClearThresholdSet(char argc, char** argv)
    {
    return AlarmThresholdSet(argc, argv, AtThermalSensorAlarmClearThresholdSet);
    }

eBool CmdAtDeviceThermalSensorShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"set threshold", "clear threshold", "interruptMask", "current temp", "min recorded temp", "max recorded temp"};
    AtThermalSensor sensor = (AtThermalSensor)Sensor();
    uint8 column = 0;

    AtUnused(argv);
    AtUnused(argc);

    if (sensor == NULL)
        {
        AtPrintc(cSevInfo, "No thermal sensor to show.\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorAlarmSetThresholdGet(sensor),      "%d"));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorAlarmClearThresholdGet(sensor),    "%d"));
    StrToCell(tabPtr, 0, column++, CliAtSensorAlarmMaskStringGet((AtSensor)sensor, cAtThermalSensorAlarmStr, cAtThermalSensorAlarmVal, mCount(cAtThermalSensorAlarmVal)));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorCurrentTemperatureGet(sensor),     "%d"));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorMinRecordedTemperatureGet(sensor), "%d"));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorMaxRecordedTemperatureGet(sensor), "%d"));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceThermalSensorAlarmsShow(char argc, char** argv)
    {
    return CliAtSensorAlarmShow(argc, argv,
                                cAtThermalSensorAlarmStr,
                                cAtThermalSensorAlarmVal,
                                mCount(cAtThermalSensorAlarmVal),
                                Sensor);
    }

eBool CmdAtDeviceThermalSensorInterruptShow(char argc, char** argv)
    {
    return CliAtSensorInterruptShow(argc, argv,
                                    cAtThermalSensorAlarmStr,
                                    cAtThermalSensorAlarmVal,
                                    mCount(cAtThermalSensorAlarmVal),
                                    Sensor);
    }

eBool CmdAtDeviceThermalSensorInterruptMask(char argc, char** argv)
    {
    return CliAtSensorInterruptMaskSet(argc, argv,
                                       cAtThermalSensorAlarmStr,
                                       cAtThermalSensorAlarmVal,
                                       mCount(cAtThermalSensorAlarmVal),
                                       Sensor);
    }

eBool CmdAtDeviceThermalSensorNotificationEnable(char argc, char **argv)
    {
    return CliAtSensorNotificationEnable(argc, argv, cAtTrue, Listener(), Sensor);
    }

eBool CmdAtDeviceThermalSensorNotificationDisable(char argc, char **argv)
    {
    return CliAtSensorNotificationEnable(argc, argv, cAtFalse, Listener(), Sensor);
    }

eBool CmdAtDeviceThermalSensorConstrainShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"setThresholdMin", "setThresholdMax", "clearThresholdMin", "clearThresholdMax"};
    AtThermalSensor sensor = (AtThermalSensor)Sensor();
    uint8 column = 0;

    AtUnused(argv);
    AtUnused(argc);

    if (sensor == NULL)
        {
        AtPrintc(cSevInfo, "No thermal sensor to show.\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorAlarmSetThresholdMin(sensor), "%d"));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorAlarmSetThresholdMax(sensor), "%d"));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorAlarmClearThresholdMin(sensor), "%d"));
    StrToCell(tabPtr, 0, column++, CliNumber2String(AtThermalSensorAlarmClearThresholdMax(sensor), "%d"));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceThermalSensorInit(char argc, char** argv)
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = AtSensorInit((AtSensor)Sensor());
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Initialize Sensor fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

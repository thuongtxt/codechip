/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Driver
 *
 * File        : CliAtDriver.c
 *
 * Created Date: Dec 27, 2012
 *
 * Description : Driver CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"
#include "CliAtDriver.h"
#include "CliAtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidSubDeviceIndex cBit7_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_activeDeviceIndex = 0;
static uint8 m_activeSubDeviceIndex = cInvalidSubDeviceIndex;

static const char * cAtDriverRoleStr[] = {"active", "standby"};
static const uint32 cAtDriverRoleVal[]  = {cAtDriverRoleActive, cAtDriverRoleStandby};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 DeviceIndex(AtDevice device)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    uint8 i;

    if (addedDevices == NULL)
        return 0xFF;

    for (i = 0; i < numAddedDevices; i++)
        {
        if (addedDevices[i] == device)
            return i;
        }

    return 0xFF;
    }

/*
 * Select device by index.
 *
 * @param deviceIndex Device index in driver
 *
 * @return AtDevice object if device index is valid, otherwise, NULL is returned
 */
static AtDevice CliDeviceSelect(uint8 deviceIndex)
    {
    /* Get all added devices */
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);

    if ((addedDevices == NULL) || (deviceIndex >= numAddedDevices))
        return NULL;

    m_activeDeviceIndex = deviceIndex;
    m_activeSubDeviceIndex = cInvalidSubDeviceIndex;
    return addedDevices[deviceIndex];
    }

static AtDevice CliSubDeviceSelect(uint8 subdeviceIndex)
    {
    AtDevice masterDevice = CliDevice();
    uint8 numSubDevices = 0;
    AtDevice *subDevices = NULL;

    if (masterDevice == NULL)
        return NULL;

    subDevices = AtDeviceAllSubDevicesGet(masterDevice, &numSubDevices);
    if (subDevices == NULL)
        return masterDevice;

    if  ((numSubDevices == 0) || (subdeviceIndex >= numSubDevices))
        {
        m_activeSubDeviceIndex = cInvalidSubDeviceIndex;
        return masterDevice;
        }

    m_activeSubDeviceIndex = subdeviceIndex;
    return subDevices[subdeviceIndex];
    }

static eBool DebugEnable(char argc, char **argv, eBool enable)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDriverDebugEnable(enable);
    return cAtTrue;
    }

static eAtSevLevel DeviceRoleColor(eAtDeviceRole deviceRole)
    {
    switch (deviceRole)
        {
        case cAtDeviceRoleActive : return cSevInfo;
        case cAtDeviceRoleAuto   : return cSevInfo;
        case cAtDeviceRoleStandby: return cSevWarning;
        case cAtDeviceRoleUnknown:
        default:
            return cSevCritical;
        }
    }

/*
 * Get current selected device
 * @return AtDevice object if there is selected device, otherwise, NULL is returned
 */
AtDevice CliDevice(void)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    AtDevice retDev = NULL;

    if ((addedDevices == NULL) || (m_activeDeviceIndex >= numAddedDevices))
        return NULL;

    retDev = addedDevices[m_activeDeviceIndex];
    if (m_activeSubDeviceIndex == cInvalidSubDeviceIndex)
        return retDev;

    return AtDeviceSubDeviceGet(retDev, m_activeSubDeviceIndex);
    }

AtDevice CliRealDevice(void)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    AtDevice retDev = NULL;

    if ((addedDevices == NULL) || (m_activeDeviceIndex >= numAddedDevices))
        return NULL;

    retDev = addedDevices[m_activeDeviceIndex];
    return retDev;
    }

uint8 DeviceIdFromCmdParamId(uint8 cmdParamId)
    {
    return (uint8)(cmdParamId - 1);
    }

eBool SubDeviceIdFromCmdParamIdIsValid(uint8 cmdParamId)
    {
    uint8 numSubDevice = AtDeviceNumSubDeviceGet(CliRealDevice());
    if ((cmdParamId == 0) || (cmdParamId > numSubDevice))
        return cAtFalse;
    return cAtTrue;
    }

/*
 * Set device by device object
 *
 * @param device Device object to be selected
 *
 * @return The input device is returned if it exists in driver. Otherwise, NULL
 * object is returned
 */
AtDevice CliDeviceSet(AtDevice device)
    {
    return CliDeviceSelect(DeviceIndex(device));
    }

eBool CmdAtDeviceSelect(char argc, char **argv)
    {
    uint8 deviceId = (uint8)(AtStrtoul(argv[0], NULL, 10) - 1);
    AtDevice device;
    uint8 numAddedDevices;
	AtUnused(argc);

    /* User does not want to select any device */
    if (AtStrcmp(argv[0], "none") == 0)
        {
        m_activeDeviceIndex = cBit7_0;
        return cAtTrue;
        }

    AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    if (numAddedDevices == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No added devices\r\n");
        return cAtFalse;
        }

    /* Check if this device can be selected */
    device = CliDeviceSelect(deviceId);
    if (device == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Out of range device ID, expected range: [1, %d]\r\n", numAddedDevices + 1);
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtSubDeviceSelect(char argc, char **argv)
    {
    uint8 Id = 0, numSubDevice;
    uint8 deviceId;
    AtDevice subDevice, realDevice;
    AtUnused(argc);

    /* User does not want to select any device */
    if (AtStrcmp(argv[0], "none") == 0)
        {
        m_activeSubDeviceIndex = cInvalidSubDeviceIndex;
        return cAtTrue;
        }

    Id = (uint8)AtStrtoul(argv[0], NULL, 10);
    realDevice = CliRealDevice();
    numSubDevice = AtDeviceNumSubDeviceGet(realDevice);
    if (Id == 0 || Id > numSubDevice)
        {
        AtPrintc(cSevCritical, "ERROR: Sub device ID is from 1 to %d\r\n", numSubDevice);
        return cAtFalse;
        }
    deviceId = DeviceIdFromCmdParamId(Id);

    m_activeSubDeviceIndex = cInvalidSubDeviceIndex;
    /* Check if this device can be selected */
    subDevice = CliSubDeviceSelect(deviceId);
    if (subDevice == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Sub device was NULL, please do 'device select' first\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

static char* DeviceIdToString(AtDevice device, uint8 deviceIndex, AtDevice subDevice, uint8 subDeviceIndex)
    {
    static char buf[64];

    AtOsalMemInit(buf, 0, sizeof(buf));
    if (device == subDevice)
        {
        if (deviceIndex == m_activeDeviceIndex && cInvalidSubDeviceIndex == m_activeSubDeviceIndex)
            AtSprintf(buf, "%d(*)", deviceIndex + 1);
        else
            AtSprintf(buf, "%d", deviceIndex + 1);
        }
    else
        {
        if (deviceIndex == m_activeDeviceIndex && subDeviceIndex == m_activeSubDeviceIndex)
            AtSprintf(buf, "%d.%d(*)", deviceIndex + 1, subDeviceIndex + 1);
        else
            AtSprintf(buf, "%d.%d", deviceIndex + 1, subDeviceIndex + 1);
        }

    return buf;
    }

static eBool DeviceInfoToAtable(tTab *tabPtr, uint32 row_i, AtDevice device, uint8 deviceIndex, AtDevice subDevice, uint8 subDeviceIndex)
    {
    char buf[64];
    uint8 column = 0;
    eBool deviceSelected = cAtFalse;
    AtHal hal = AtDeviceIpCoreHalGet(subDevice, 0);
    uint8 numCores = AtDeviceNumIpCoresGet(subDevice);
    eAtDeviceRole deviceRole;
    const char *stringValue;
    char *deviceIdStr = DeviceIdToString(device, deviceIndex, subDevice, subDeviceIndex);

    column = 0;
    /* Device ID */
    if (AtStrstr(deviceIdStr, "*"))
        deviceSelected = cAtTrue;

    StrToCell(tabPtr, row_i, column++, deviceIdStr);

    /* Code */
    AtSprintf(buf, "0x%08x", AtDeviceProductCodeGet(subDevice));
    StrToCell(tabPtr, row_i, column++, buf);

    /* Version */
    StrToCell(tabPtr, row_i, column++, AtDeviceVersion(subDevice));

    /* Base address */
    if (numCores <= 1)
        AtSprintf(buf, "%s", hal ? AtHalToString(hal) : "No HAL");
    StrToCell(tabPtr, row_i, column++, buf);

    /* Role */
    if (AtDeviceHasRole(subDevice))
        {
        char roleBuffer[16] = {0};

        deviceRole = AtDeviceRoleGet(subDevice);
        stringValue = CliAtDeviceRole2String(deviceRole);
        if (stringValue)
            AtSprintf(roleBuffer, "%s", stringValue);

        if (deviceRole == cAtDeviceRoleAuto)
            {
            deviceRole = AtDeviceAutoRoleGet(subDevice);
            stringValue = CliAtDeviceRole2String(deviceRole);

            if (stringValue)
                AtSprintf(roleBuffer, "%s (auto)", stringValue);
            else /* Fallback to device auto role */
                deviceRole = cAtDeviceRoleAuto;
            }

        ColorStrToCell(tabPtr, row_i, column++, AtStrlen(roleBuffer) ? roleBuffer : "Error", DeviceRoleColor(deviceRole));
        }
    else
        StrToCell(tabPtr, row_i, column++, sAtNotSupported);

    return deviceSelected;
    }

eBool CmdAtDriverShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"Device", "Product code", "Version", "HAL", "Role"};
    uint8 numAddedDevices;
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice *devices = AtDriverAddedDevicesGet(driver, &numAddedDevices);
    uint8 i;
    eBool deviceSelected = cAtFalse;
    eAtDriverRole role = AtDriverRoleGet(driver);
    const char *stringValue = CliAtDriverRoleString(driver);
    uint32 row = 0;

	AtUnused(argv);
	AtUnused(argc);

    AtPrintc((role == cAtDriverRoleActive) ? cSevInfo : cSevWarning,
            "Driver Version  : %s.b%03d (%s)",
            AtDriverVersion(driver),
            AtDriverBuiltNumber(driver),
            stringValue);

    /* Show version detail if any */
    stringValue = AtDriverVersionDescription(driver);
    if (stringValue)
        AtPrintc(cSevInfo, " (%s)", stringValue);
    AtPrintc(cSevNormal, "\r\n");

    if ((devices == NULL) || (numAddedDevices == 0))
        {
        AtPrintc(cSevInfo, "No added device\r\n");
        return cAtTrue;
        }

    /* Create device table */
    tabPtr = TableAlloc(0, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    row = 0;
    for (i = 0; i < numAddedDevices; i++)
        {
        uint8 j, numSubDevices;
        AtDevice device, subDevice;
        AtDevice *subDevices = NULL;

        device = devices[i];
        subDevices = AtDeviceAllSubDevicesGet(device, &numSubDevices);

        subDevice = device;
        TableAddRow(tabPtr, 1);
        deviceSelected |= DeviceInfoToAtable(tabPtr, row++, device, i, subDevice, 0);

        if (numSubDevices == 0)
            continue;

        for (j = 0; j < numSubDevices; j++)
            {
            subDevice = subDevices[j];
            TableAddRow(tabPtr, 1);
            deviceSelected |= DeviceInfoToAtable(tabPtr, row++, device, i, subDevice, j);
            }
        }

    /* Show it */
    AtPrintc(cSevInfo, "Hardware Version: \r\n");
    TablePrint(tabPtr);
    TableFree(tabPtr);

    if (!deviceSelected)
        AtPrintc(cSevWarning, "WARNING: No selected device, all of CLIs will report error\r\n");

    return cAtTrue;
    }

eBool CmdAtDriverSerialize(char argc, char **argv)
    {
    AtCoder newCoder = AtDefaultCoderNew();
    AtFile file = NULL;
    AtStd std = AtStdSharedStdGet();

    if (argc > 0)
        file = AtStdFileOpen(std, argv[0], cAtFileOpenModeWrite | cAtFileOpenModeTruncate);

    AtDefaultCoderOutputFileSet((AtDefaultCoder)newCoder, file);
    AtCoderEncodeObject(newCoder, (AtObject)AtDriverSharedDriverGet(), NULL);
    AtCoderDelete(newCoder);

    AtStdFileClose(file);
    return cAtTrue;
    }

eBool CmdAtDriverDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDriverDebug(AtDriverSharedDriverGet());
    return cAtTrue;
    }

eBool CmdAtDriverDebugEnable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDriverDebugDisable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDriverRoleSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtFalse;
    eAtDriverRole role = CliStringToEnum(argv[0], cAtDriverRoleStr, cAtDriverRoleVal, mCount(cAtDriverRoleVal), &convertSuccess);
    eAtRet ret = cAtOk;

    AtUnused(argc);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid roles: ");
        CliExpectedValuesPrint(cSevCritical, cAtDriverRoleStr, mCount(cAtDriverRoleVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtDriverRoleSet(AtDriverSharedDriverGet(), role);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Set role fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

const char *CliAtDriverRoleString(AtDriver driver)
    {
    return CliEnumToString(AtDriverRoleGet(driver), cAtDriverRoleStr, cAtDriverRoleVal, mCount(cAtDriverRoleVal), NULL);
    }

eBool CmdAtDriverRestore(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    while (AtDriverRestore(AtDriverSharedDriverGet()) > 0);
    return cAtTrue;
    }

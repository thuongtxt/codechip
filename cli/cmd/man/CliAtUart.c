/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Man
 *
 * File        : CliAtUart.c
 *
 * Created Date: Apr 25, 2016
 *
 * Description : CliAtUart implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtUart.h"
#include "AtTextUI.h"
#include "AtCliModule.h"
#include "AtSemController.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtUartMaxDecodeBufferLenInBytes 1024

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char *m_sharedBuffer = NULL;
static char *m_sharedDecodeBuffer = NULL;
static char m_decodeLine[cAtUartMaxDecodeBufferLenInBytes];

static const char *cAtSemCounterTypesStringVals[] = {"correctable",
                                                     "uncorrectable"};
static eAtSemControllerCounterType cAtSemCounterTypeVals[] = {cAtSemControllerCounterTypeCorrectable,
                                                              cAtSemControllerCounterTypeUncorrectable};

/*--------------------------- Forward declarations ---------------------------*/
extern eBool AtDeviceIsSimulated(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
static void TextUIWillStop(AtTextUI self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);

    if (m_sharedBuffer)
        AtOsalMemFree(m_sharedBuffer);

    if (m_sharedDecodeBuffer)
        AtOsalMemFree(m_sharedDecodeBuffer);

    m_sharedBuffer = NULL;
    m_sharedDecodeBuffer = NULL;
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner* listener = NULL;
    static tAtTextUIListerner m_listener;

    if (listener)
        return listener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.WillStop = TextUIWillStop;
    m_listener.DidStop = NULL;
    listener = &m_listener;

    return listener;
    }

static char *SharedDecodeBuffer(void)
    {
    if (m_sharedDecodeBuffer)
        {
        AtOsalMemInit(m_sharedDecodeBuffer, 0, cAtUartMaxDecodeBufferLenInBytes);
        return m_sharedDecodeBuffer;
        }

    m_sharedDecodeBuffer = (char *)AtOsalMemAlloc(cAtUartMaxDecodeBufferLenInBytes);
    AtOsalMemInit(m_sharedDecodeBuffer, 0, cAtUartMaxDecodeBufferLenInBytes);
    AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);

    return m_sharedDecodeBuffer;
    }

static uint32 CliUartCommandGet(char argc, char** argv, char *commandString)
    {
    uint32 i;
    uint32 commandLen = 0;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameters.\r\n");
        return cAtFalse;
        }

    for (i = 1; i < (uint32)argc; i++)
        {
        if (i != 1)
            AtSprintf(&commandString[commandLen], "%s", " ");
        commandLen = AtStrlen(commandString);

        AtSprintf(&commandString[commandLen], "%s", argv[i]);
        commandLen = AtStrlen(commandString);
        }

    return commandLen;
    }

static const char* SemState2String(char *uartLine)
    {
    if ((uartLine[3] == '0') && (uartLine[4] == '0')) return "Idle";
    if ((uartLine[3] == '0') && (uartLine[4] == '1')) return "Initialization";
    if ((uartLine[3] == '0') && (uartLine[4] == '2')) return "Observation";
    if ((uartLine[3] == '0') && (uartLine[4] == '4')) return "Correction";
    if ((uartLine[3] == '2') && (uartLine[4] == '0')) return "Detect-only";
    if ((uartLine[3] == '4') && (uartLine[4] == '0')) return "Diagnostic Scan";
    if ((uartLine[3] == '0') && (uartLine[4] == '8')) return "Classification";
    if ((uartLine[3] == '1') && (uartLine[4] == '0')) return "Injection";
    if ((uartLine[3] == '1') && (uartLine[4] == 'F')) return "Fatal Error";

    return "Unknown";
    }

static char* UartStateChangeDecode(char *uartLine, char* buffer)
    {
    /* Format: SC {2-digit hex value} */
    AtSprintf(buffer, "Change state to %s", SemState2String(uartLine));
    return buffer;
    }

static const char* SemFlag2String(char *uartLine)
    {
    if ((uartLine[3] == '0') && (uartLine[4] == '0')) return "Correctable, Non-Essential";
    if ((uartLine[3] == '2') && (uartLine[4] == '0')) return "Uncorrectable, Non-Essential";
    if ((uartLine[3] == '4') && (uartLine[4] == '0')) return "Correctable, Essential";
    if ((uartLine[3] == '6') && (uartLine[4] == '0')) return "Uncorrectable, Essential";

    return "Unknown";
    }

static char* FlagChangeDecode(char *uartLine, char* buffer)
    {
    /* Format: FC {2-digit hex value} */
    AtSprintf(buffer, "Change flag to %s", SemFlag2String(uartLine));
    return buffer;
    }

static const char* DecodePerLine(char *uartLine, uint32 lineLen)
    {
    char *buffer = SharedDecodeBuffer();

    /* Add end of string symbol */
    uartLine[lineLen] = '\0';
    buffer[0] = '\0';

    if (AtStrncmp(uartLine, "SC", 2) == 0)
        return UartStateChangeDecode(uartLine, buffer);

    if (AtStrncmp(uartLine, "FC", 2) == 0)
        return FlagChangeDecode(uartLine, buffer);

    if (AtStrncmp(uartLine, "SN", 2) == 0)      return "SLR Number";
    if (AtStrncmp(uartLine, "RI", 2) == 0)      return "Reserved Information";
    if (AtStrncmp(uartLine, "TS", 2) == 0)      return "Timestamp";
    if (AtStrncmp(uartLine, "TB", 2) == 0)      return "Table Base";
    if (AtStrncmp(uartLine, "CB", 2) == 0)      return "Classification Base";
    if (AtStrncmp(uartLine, "CL", 2) == 0)      return "Classification Level";
    if (AtStrncmp(uartLine, "MF", 2) == 0)      return "Maximum Linear Frame Count";
    if (AtStrncmp(uartLine, "PA", 2) == 0)      return "PFA of Detected Error";
    if (AtStrncmp(uartLine, "LA", 2) == 0)      return "LFA of Detected Error";

    if (AtStrncmp(uartLine, "AUX", 3) == 0)     return "AUX Error Detected";
    if (AtStrncmp(uartLine, "CRC", 3) == 0)     return "CRC Error Detected";
    if (AtStrncmp(uartLine, "ECC", 3) == 0)     return "ECC Error Detected";

    return NULL;
    }

static void BufferPrint(char *buffer, uint32 bufferSize)
    {
    uint32 i = 0;
    uint32 index_i;

    if (bufferSize == 0)
        return;

    AtPrintc(cSevNormal, "Received data as below: (length = %u)\r\n", bufferSize);
    index_i = 0;
    while (i < bufferSize)
        {
        if (buffer[i] == '\r')  /* line ending */
            {
            /* Print previous decode line and break line ending */
            const char *lineDecode = DecodePerLine(m_decodeLine, AtStrlen(m_decodeLine));
            if (lineDecode != NULL)
                AtPrintc(cSevNormal, " ; %s", DecodePerLine(m_decodeLine, AtStrlen(m_decodeLine)));

            /* Break line (Some platforms need \n to print correctly) */
            AtPrintc(cSevInfo, "%c%c", buffer[i], '\n');
            index_i = 0;
            }
        else
            {
        	if (index_i == 0)
        		AtOsalMemInit(m_decodeLine, 0, sizeof(m_decodeLine));

        	/* Print UART code and store it to decode line */
            AtPrintc(cSevInfo, "%c", buffer[i]);
            if (index_i < cAtUartMaxDecodeBufferLenInBytes) /* Protect Buffer */
            	m_decodeLine[index_i++] = buffer[i];
            else
                AtPrintc(cSevCritical, "line message too long, over 128 bytes! \r\n");
            }

        i++;
        }

    AtPrintc(cSevInfo, "%c%c", '\r', '\n');
    }

static eBool UartCommandSend(AtUart uart, const char* buffer, uint32 bufferSize)
    {
    eAtRet ret;

    /* Send command to IP */
    AtPrintc(cSevNormal, "Processing UART command for UART #%u: \"%s\"\r\n",  (uint32)(AtUartSuperLogicRegionIdGet(uart) + 1), buffer);
    ret = AtUartTransmit(uart, buffer, bufferSize);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Send UART command fail, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliAtUartCommand(AtUart uart, char argc, char **argv,
                       eBool needSend, eBool needReceive,
                       char *command, uint32 length)
    {
    char *buffer;
    char uartCommand[16];
    eBool success = cAtTrue;
    uint32 txFifoLen = length;
    uint32 rxFifoLen = 0;

    if (needSend && (command == NULL))
        {
        AtOsalMemInit(uartCommand, 0, sizeof (uartCommand));
        txFifoLen = CliUartCommandGet(argc, argv, uartCommand);
        command = uartCommand;
        }

    /* Send command to IP */
    if (needSend && (UartCommandSend(uart, command, txFifoLen) != cAtTrue))
        success = cAtFalse;

    if (!needReceive)
        return success;

    /* After send, hardware will received at least 1024 * 8* 77Mhz ~ ,
     * software wait to get Fifo hardware
     * */
    AtOsalUSleep(1000);

    /* Receive data returned */
    buffer = CliUartSharedBuffer();
    rxFifoLen = AtUartReceive(uart, buffer, cAtUartMaxBufferLenInBytes);
    BufferPrint(buffer, rxFifoLen);

    return success;
    }

eBool CliAtDeviceUartCommand(AtList uarts, char argc, char **argv, eBool needSend, eBool needReceive)
    {
    uint8 uart_i;
    char uartCommand[16];
    eBool success = cAtTrue;
    uint32 txFifoLen = 0;

    if (AtListLengthGet(uarts) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: UART list is NULL\r\n");
        return cAtFalse;
        }

    if (needSend)
    	{
        AtOsalMemInit(uartCommand, 0, sizeof (uartCommand));
    	txFifoLen = CliUartCommandGet(argc, argv, uartCommand);
    	}

    for (uart_i = 0; uart_i < AtListLengthGet(uarts); uart_i++)
        {
        AtUart uart = (AtUart)AtListObjectGet(uarts, uart_i);

        if (!CliAtUartCommand(uart, argc, argv, needSend, needReceive, uartCommand, txFifoLen))
            success = cAtFalse;
        }

    return success;
    }

eBool CliAtDeviceUartDebug(AtList uarts)
    {
    uint8 uart_i;

    if (AtListLengthGet(uarts) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: UART list is NULL\r\n");
        return cAtFalse;
        }

    for (uart_i = 0; uart_i < AtListLengthGet(uarts); uart_i++)
        AtUartDebug((AtUart)AtListObjectGet(uarts, uart_i));

    return cAtTrue;
    }

static AtSemController SemController(AtUart uart)
    {
    return AtDeviceSemControllerGetByIndex(AtUartDeviceGet(uart), AtUartSuperLogicRegionIdGet(uart));
    }

static eBool ValidateResultShow(AtList uarts, eAtRet *result)
    {
    tTab *tabPtr;
    uint8 i;
    const char *pHeading[] = {"SemId", "Result", "Corrected counter", "Uncorrected counter"};
    uint32 numUarts = AtListLengthGet(uarts);

    if (numUarts == 0)
        {
        AtPrintc(cSevInfo, "No UART interface\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc((uint32)numUarts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numUarts; i++)
        {
        uint32 column = 0;
        AtUart uart = (AtUart)AtListObjectGet(uarts, i);
        AtSemController semController = SemController(uart);

        StrToCell(tabPtr, i, column++, CliNumber2String(AtSemControllerIdGet(semController) + 1, "%u"));
        ColorStrToCell(tabPtr, i, column++, AtRet2String(result[i]), (result[i] == cAtOk) ? cSevInfo : cSevCritical);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(AtSemControllerCounterGet(semController, cAtSemControllerCounterTypeCorrectable), "%d"), cSevNormal);
        ColorStrToCell(tabPtr, i, column++, CliNumber2String(AtSemControllerCounterGet(semController, cAtSemControllerCounterTypeUncorrectable), "%d"), cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtUartValidate(AtList uarts, char argc, char** argv)
    {
    uint8 uart_i;
    uint32 numUarts = AtListLengthGet(uarts);
    eAtRet *validateResult;
    eBool success = cAtTrue;

    AtUnused(argc);
    AtUnused(argv);

    if (numUarts == 0)
        {
        AtPrintc(cSevCritical, "ERROR: UART list is NULL\r\n");
        return cAtFalse;
        }

    validateResult = (eAtRet *)AtOsalMemAlloc(numUarts * sizeof(eAtRet));
    if (validateResult == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Allocate memory fail\r\n");
        return cAtFalse;
        }

    for (uart_i = 0; uart_i < numUarts; uart_i++)
        {
        AtUart uart = (AtUart)AtListObjectGet(uarts, uart_i);
        AtSemController semController = SemController(uart);

        validateResult[uart_i] = AtSemControllerValidateByUart(semController);
        if (validateResult[uart_i] != cAtOk)
        	{
            AtPrintc(cSevCritical, "ERROR: UART SEM#%d validate fail with ret code = %s\r\n", uart_i + 1, AtRet2String(validateResult[uart_i]));
            success = cAtFalse;
        	}
        }

    if (ValidateResultShow(uarts, validateResult) != cAtTrue)
        success = cAtFalse;

    AtOsalMemFree(validateResult);
    return success;
    }

static void UartErrorReportPrint(uint32 semIdx, const char *report, uint32 length)
    {
    uint32 i;
    AtPrintc(cSevInfo, "Sem#%u UART report:\r\n", semIdx + 1);
    for (i = 0; i < length; i++)
        {
        if (report[i] == '\r')
            AtPrintc(cSevInfo, "\r\n");
        else
            AtPrintc(cSevInfo, "%c", report[i]);
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static const char* ReportStringMake(uint32 *length)
    {
    static const char *uartDryRunReport[] ={
        "O",
        "SC 02",
        "O>",
        "SC 04",
        "SED OK",
        "PA 0000121E",
        "LA 00000065",/*"LA 000004F8"*/
        "WD 00 BT 01",/*"WD 05 BT 0E"*/
        "COR",
        "WD 00 BT 01",/*"WD 05 BT 0E"*/
        "END",
        "FC 40",
        "SC 08",
        "FC 40",
        "SC 02",
        "O>",
        "SC 04",
        "DED",
        "PA 00001223",
        "LA 00000065",/*"LA 000004FD"*/
        "COR",
        "END",
        "FC 60",
        "SC 08",
        "FC 60",
        "SC 00",
        "I>",
        ""};
    uint32 i = 0, j = 0, pos = 0;
    char *position = NULL;
    char *str = CliUartSharedBuffer();
    *length = cAtUartMaxBufferLenInBytes;
    AtOsalMemInit(str, 0, *length);
    position = str;
    while (AtStrcmp(uartDryRunReport[i], "") != 0)
        {
        for (j = 0; j < AtStrlen(uartDryRunReport[i]); j++)
            {
            position[pos] = uartDryRunReport[i][j];
            pos++;
            }
        position[pos] = '\r';
        pos++;
        i++;
        }
    *length = pos;
    return (const char*)str;
    }

eBool CliAtUartErrorForce(AtList uarts, char argc, char** argv)
    {
    uint8 uart_i;
    uint32 numUarts = AtListLengthGet(uarts);
    eAtRet *validateResult;
    eBool success = cAtTrue;
    uint32 errorType;

    AtUnused(argc);

    mAtStrToEnum(cAtSemCounterTypesStringVals, cAtSemCounterTypeVals, argv[1], errorType, success);
    if (success != cAtTrue)
        {
        AtPrintc(cSevCritical, "ERROR: SEM error type is invalid\r\n");
        return cAtFalse;
        }

    if (numUarts == 0)
        {
        AtPrintc(cSevCritical, "ERROR: UART list is NULL\r\n");
        return cAtFalse;
        }

    validateResult = (eAtRet *)AtOsalMemAlloc(numUarts * sizeof(eAtRet));
    if (validateResult == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Allocate memory fail\r\n");
        return cAtFalse;
        }

    for (uart_i = 0; uart_i < numUarts; uart_i++)
        {
        AtUart uart = (AtUart)AtListObjectGet(uarts, uart_i);
        AtSemController semController = SemController(uart);
        const char* uartReport;
        uint32 reportSize = 0, semId = AtSemControllerIdGet(semController);

        validateResult[uart_i] = AtSemControllerForceErrorByUart(semController, errorType);
        if (AtDeviceIsSimulated(CliDevice()))
            {
            validateResult[uart_i] = cAtOk;
            uartReport = ReportStringMake(&reportSize);
            UartErrorReportPrint(semId, uartReport, reportSize);
            CliAtUartEssentialErrorInReportCheck(semController, uartReport, reportSize);
            }
        else
            {
            if (validateResult[uart_i] != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: UART SEM#%d force error fail with ret code = %s\r\n", uart_i + 1, AtRet2String(validateResult[uart_i]));
                success = cAtFalse;
                }
            }
        }

    if (ValidateResultShow(uarts, validateResult) != cAtTrue)
        success = cAtFalse;

    AtOsalMemFree(validateResult);
    return success;
    }

eBool CliAtUartAsyncErrorForce(AtList uarts, char argc, char** argv)
    {
    uint8 uart_i;
    uint32 numUarts = AtListLengthGet(uarts);
    eAtRet *validateResult;
    eBool success = cAtTrue;
    uint32 errorType;

    AtUnused(argc);

    mAtStrToEnum(cAtSemCounterTypesStringVals, cAtSemCounterTypeVals, argv[1], errorType, success);
    if (success != cAtTrue)
        {
        AtPrintc(cSevCritical, "ERROR: SEM error type is invalid\r\n");
        return cAtFalse;
        }

    if (numUarts == 0)
        {
        AtPrintc(cSevCritical, "ERROR: UART list is NULL\r\n");
        return cAtFalse;
        }

    validateResult = (eAtRet *)AtOsalMemAlloc(numUarts * sizeof(eAtRet));
    if (validateResult == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Allocate memory fail\r\n");
        return cAtFalse;
        }

    for (uart_i = 0; uart_i < numUarts; uart_i++)
        {
        AtUart uart = (AtUart)AtListObjectGet(uarts, uart_i);
        AtSemController semController = SemController(uart);
        const char* uartReport;
        uint32 reportSize = 0, semId = AtSemControllerIdGet(semController);

        if (AtDeviceIsSimulated(CliDevice()))
            {
            validateResult[uart_i] = cAtOk;
            uartReport = ReportStringMake(&reportSize);
            UartErrorReportPrint(semId, uartReport, reportSize);
            CliAtUartEssentialErrorInReportCheck(semController, uartReport, reportSize);
            }
        else
            {
            validateResult[uart_i] = AtSemControllerAsyncForceErrorByUart(semController, errorType);
            while(AtDeviceAsyncRetValIsInState(validateResult[uart_i]))
                {
                validateResult[uart_i] = AtSemControllerAsyncForceErrorByUart(semController, errorType);
                }
            if (validateResult[uart_i] != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: UART SEM#%d force error fail with ret code = %s\r\n", uart_i + 1, AtRet2String(validateResult[uart_i]));
                success = cAtFalse;
                }
            }
        }

    if (ValidateResultShow(uarts, validateResult) != cAtTrue)
        success = cAtFalse;

    AtOsalMemFree(validateResult);
    return success;
    }

void CliAtDeviceUartInterruptHandler(AtSemController self, uint32 interruptStatus, uint32 currenAlarm)
    {
    char *buffer = CliUartSharedBuffer();
    uint32 bufferLength = cAtUartMaxBufferLenInBytes, uartReportLength;
    AtUart uart = AtSemControllerUartGet(self);

    if (interruptStatus & currenAlarm & cAtSemControllerAlarmErrorCorrectable)
        {
        uartReportLength = AtUartReceive(uart, buffer, bufferLength);
        UartErrorReportPrint(AtSemControllerIdGet(self), buffer, uartReportLength);
        CliAtUartEssentialErrorInReportCheck(self, buffer, uartReportLength);
        }

    if (interruptStatus & currenAlarm & cAtSemControllerAlarmErrorNoncorrectable)
        {
        uartReportLength = AtUartReceive(uart, buffer, bufferLength);
        UartErrorReportPrint(AtSemControllerIdGet(self), buffer, uartReportLength);
        }
    }

char *CliUartSharedBuffer(void)
    {
    if (m_sharedBuffer)
        {
        AtOsalMemInit(m_sharedBuffer, 0, cAtUartMaxBufferLenInBytes);
        return m_sharedBuffer;
        }

    m_sharedBuffer = (char *)AtOsalMemAlloc(cAtUartMaxBufferLenInBytes);
    AtOsalMemInit(m_sharedBuffer, 0, cAtUartMaxBufferLenInBytes);
    AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);

    return m_sharedBuffer;
    }

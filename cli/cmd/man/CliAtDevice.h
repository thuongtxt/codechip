/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : CliAtDevice.h
 * 
 * Created Date: Sep 23, 2016
 *
 * Description : Device CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATDEVICE_H_
#define _CLIATDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char *CliAtDeviceRole2String(eAtDeviceRole role);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATDEVICE_H_ */


# @group CliAtInterruptPin "Interrupt PINs"
# @ingroup CliAtDevice "Device"

5 device interrupt pin trigger mode CmdAtInterruptPinTriggerModeSet 2 pinIds triggerMode
/*
    Syntax     : device interrupt pin trigger mode <pinIds> <edge/level-high/level-low>
    Parameter  : pinIds      - List of PIN IDs. Example: 1,2
                 triggerMode - [edge/level-high/level-low] Trigger mode
    Description: Set trigger mode
    Example    : device interrupt pin trigger mode 1-2 level-high
*/

5 device interrupt pin trigger start CmdAtInterruptPinTriggerStart 1 pinIds
/*
    Syntax     : device interrupt pin trigger start <pinIds>
    Parameter  : pinIds      - List of PIN IDs. Example: 1,2
    Description: Start triggering interrupt PINs
    Example    : device interrupt pin trigger start 1-2
*/

5 device interrupt pin trigger stop CmdAtInterruptPinTriggerStop 1 pinIds
/*
    Syntax     : device interrupt pin trigger stop <pinIds>
    Parameter  : pinIds      - List of PIN IDs. Example: 1,2
    Description: Stop triggering interrupt PINs
    Example    : device interrupt pin trigger stop 1-2
*/

4 show device interrupt pin CmdAtInterruptPinShow 1 pinIds
/*
    Syntax     : show device interrupt pin <pinIds>
    Parameter  : pinIds - List of PIN IDs. Example: 1,2
    Description: Show interrupt PIN information
    Example    : show device interrupt pin 1-2
*/

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDriverLogger.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : CLIs to control driver logger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cLogLevelStr[] = {"critical", "warning", "info", "normal", "debug"};
static const uint32 cLogLevelValue[]= {cAtLogLevelCritical, cAtLogLevelWarning, cAtLogLevelInfo, cAtLogLevelNormal, cAtLogLevelDebug};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtLogger Logger(void)
    {
    return AtDriverLoggerGet(AtDriverSharedDriverGet());
    }

static eBool LoggerLevelEnable(char argc, char **argv, eBool enable)
    {
    uint32 levelMask = CliMaskFromString(argv[0], cLogLevelStr, cLogLevelValue, mCount(cLogLevelValue));
    AtUnused(argc);

    AtLoggerLevelEnable(Logger(), levelMask, enable);
    return cAtTrue;
    }

static void DidLogMessage(AtLogger self, eAtLogLevel level, const char *message, void *userData)
    {
    AtUnused(self);
    AtPrintf("%s %s %s", AtObjectToString((AtObject)userData), AtLoggerLevel2String(level), message);
    }

static void WillRemoveListener(AtLogger self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtPrintc(cSevInfo, "Listener will be removed\r\n");
    }

static tAtLoggerListener *Listener(void)
    {
    static tAtLoggerListener m_listener;
    static tAtLoggerListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.DidLogMessage = DidLogMessage;
    m_listener.WillRemoveListener = WillRemoveListener;
    pListener = &m_listener;

    return pListener;
    }

static eBool Listen(char argc, char **argv, eBool listen)
    {
    AtLogger logger = Logger();
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    if (listen)
        ret = AtLoggerListenerAdd(logger, Listener(), AtDriverSharedDriverGet());
    else
        ret = AtLoggerListenerRemove(logger, Listener());

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot %s listening, ret = %s\r\n",
                 listen ? "start" : "stop",
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ApiLogEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDriverApiLogEnable(enabled);
    return cAtTrue;
    }

static eBool AllApiLogEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDriverAllApiLogEnable(enabled);
    return cAtTrue;
    }

static eBool MultiDevPrefixApiLogEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDriverMultiDevPrefixApiLogEnable(enabled);
    return cAtTrue;
    }

eBool CmdAtDriverLoggerVerboseEnable(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtLoggerVerbose(Logger(), cAtTrue);
    return cAtTrue;
    }

eBool CmdAtDriverLoggerVerboseDisable(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtLoggerVerbose(Logger(), cAtFalse);
    return cAtTrue;
    }

eBool CmdAtDriverLoggerShow(char argc, char **argv)
    {
    uint32 numMessages = AtLoggerNumMessages(Logger());
    eBool enable = AtLoggerIsEnabled(Logger());
    eAtLogLevel level = cAtLogLevelCritical;
    eBool convertResult;

    AtPrintc(cSevNormal, "* Logging is: ");
    AtPrintc(enable ? cSevInfo : cSevCritical, "%s\r\n", enable ? "Enabled" : "Disabled");
    AtPrintc(cSevNormal, "* Messages: %s", (numMessages == 0) ? "There is no message\r\n" : "\r\n");

    enable = AtDriverApiLogIsEnabled();
    AtPrintc(cSevNormal, "* API logging is: ");
    AtPrintc(enable ? cSevInfo : cSevCritical, "%s\r\n", enable ? "Enabled" : "Disabled");

    enable = AtDriverAllApiLogIsEnabled();
    AtPrintc(cSevNormal, "* All API logging is: ");
    AtPrintc(enable ? cSevInfo : cSevCritical, "%s\r\n", enable ? "Enabled" : "Disabled");

    enable = AtDriverMultiDevPrefixApiLogIsEnabled();
    AtPrintc(cSevNormal, "* mdev_x_/sdev_x.y_ prefix in the API logging is: ");
    AtPrintc(enable ? cSevInfo : cSevCritical, "%s\r\n", enable ? "Enabled" : "Disabled");

    if (argc > 0)
        {
        mAtStrToEnum(cLogLevelStr, cLogLevelValue, argv[0], level, convertResult);
        if (convertResult)
            {
            AtLoggerShowByLevel(Logger(), level);
            return cAtTrue;
            }
        }

    AtLoggerShow(Logger());
    return cAtTrue;
    }

eBool CmdAtDriverLoggerFlush(char argc, char **argv)
    {
    eBool verbose = cAtTrue;

    AtUnused(argv);

    if ((argc > 0) && (AtStrcmp(argv[0], "silence") == 0))
        verbose = cAtFalse;

    if (verbose)
        AtLoggerShow(Logger());

    AtLoggerFlush(Logger());

    return cAtTrue;
    }

eBool CmdAtDriverLoggerTest(char argc, char **argv)
    {
    uint32 numberMsg = 1025;
    uint32 msg_i;
    AtUnused(argc);
    AtUnused(argv);

    for (msg_i = 0; msg_i < numberMsg; msg_i++)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "Msg %d: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                                                    "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
                                                                    "cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
                                                                    "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"
                                                                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
                                                                    "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff\r\n", msg_i);
        }

    return cAtTrue;
    }

eBool CmdAtDriverLoggerEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtLoggerEnable(Logger(), cAtTrue);
    return cAtTrue;
    }

eBool CmdAtDriverLoggerDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtLoggerEnable(Logger(), cAtFalse);
    return cAtTrue;
    }

eBool CmdAtDriverLoggerLevelEnable(char argc, char **argv)
    {
    return LoggerLevelEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDriverLoggerLevelDisable(char argc, char **argv)
    {
    return LoggerLevelEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDriverLoggerExport(char argc, char **argv)
    {
    AtFile file = AtStdFileOpen(AtStdSharedStdGet(), argv[0], cAtFileOpenModeWrite | cAtFileOpenModeTruncate);
    AtLogger logger = AtDriverLoggerGet(AtDriverSharedDriverGet());
    AtUnused(argc);
    AtLoggerFileExport(logger, file);
    AtFileClose(file);
    return cAtTrue;
    }

eBool CmdAtDriverLoggerListenStart(char argc, char **argv)
    {
    return Listen(argc, argv, cAtTrue);
    }

eBool CmdAtDriverLoggerListenStop(char argc, char **argv)
    {
    return Listen(argc, argv, cAtFalse);
    }

eBool CmdAtDriverApiLogEnable(char argc, char **argv)
    {
    return ApiLogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDriverApiLogDisable(char argc, char **argv)
    {
    return ApiLogEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDriverAllApiLogEnable(char argc, char **argv)
    {
    return AllApiLogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDriverAllApiLogDisable(char argc, char **argv)
    {
    return AllApiLogEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDriverMultiDevPrefixApiLogEnable(char argc, char **argv)
    {
    return MultiDevPrefixApiLogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDriverMultiDevPrefixApiLogDisable(char argc, char **argv)
    {
    return MultiDevPrefixApiLogEnable(argc, argv, cAtFalse);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDeviceAccessLog.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : Access logging
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDebug.h"
#include "AtDevice.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tRegisterRange
    {
    uint32 start;
    uint32 stop;
    }tRegisterRange;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Access logging */
static eBool m_readLog = cAtFalse;
static eBool m_writeLog = cAtFalse;
static AtFile m_accessLogFile = NULL;
static char m_accessLogMessage[128];
static AtList m_ignoreRanges = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AllRangesDelete(void)
    {
    while (AtListLengthGet(m_ignoreRanges))
        AtOsalMemFree(AtListObjectRemoveAtIndex(m_ignoreRanges, 0));
    AtObjectDelete((AtObject)m_ignoreRanges);
    m_ignoreRanges = NULL;
    }

static AtList IgnoreRanges(void)
    {
    if (m_ignoreRanges == NULL)
        m_ignoreRanges = AtListCreate(0);
    return m_ignoreRanges;
    }

static void RangeAdd(uint32 startAddress, uint32 stopAddress)
    {
    tRegisterRange *newRange = AtOsalMemAlloc(sizeof(tRegisterRange));
    newRange->start = startAddress;
    newRange->stop = stopAddress;
    AtListObjectAdd(IgnoreRanges(), (AtObject)newRange);
    }

static eBool ShouldLogRegister(uint32 address)
    {
    AtIterator iterator;
    tRegisterRange *range;

    if (m_ignoreRanges == NULL)
        return cAtTrue;

    iterator = AtListIteratorCreate(m_ignoreRanges);
    while ((range = (tRegisterRange *)AtIteratorNext(iterator)) != NULL)
        {
        if ((address >= range->start) && (address <= range->stop))
            break;
        }
    AtObjectDelete((AtObject)iterator);

    /* At least one ignore range is found */
    return range ? cAtFalse : cAtTrue;
    }

static void LogMessage(const char *message, eBool log)
    {
    if (!log)
        return;

    if (m_accessLogFile)
        {
        AtFileWrite(m_accessLogFile, message, AtStrlen(message), 1);
        AtFileFlush(m_accessLogFile);
        }
    else
        AtPrintf("%s", message);
    }

static eBool ShouldLogRead(uint32 address)
    {
    if (m_readLog && ShouldLogRegister(address))
        return cAtTrue;
    return cAtFalse;
    }

static eBool ShouldLogWrite(uint32 address)
    {
    if (m_writeLog && ShouldLogRegister(address))
        return cAtTrue;
    return cAtFalse;
    }

static void DidReadRegister(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtUnused(coreId);

    if (!ShouldLogRead(address))
        return;

    AtSprintf(m_accessLogMessage, "Short read  0x%08x: 0x%08x\r\n", address, value);
    LogMessage(m_accessLogMessage, m_readLog);
    }

static void DidWriteRegister(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtUnused(coreId);

    if (!ShouldLogWrite(address))
        return;

    AtSprintf(m_accessLogMessage, "Short write 0x%08x: 0x%08x\r\n", address, value);
    LogMessage(m_accessLogMessage, m_writeLog);
    }

static void DidLongReadOnCore(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtUnused(coreId);

    if (!ShouldLogRead(address))
        return;

    AtSprintf(m_accessLogMessage, "Long read   0x%08x: %s\r\n", address, AtDebugLongRegValue2String(dataBuffer, numDwords));
    LogMessage(m_accessLogMessage, m_readLog);
    }

static void DidLongWriteOnCore(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    AtUnused(coreId);

    if (!ShouldLogWrite(address))
        return;

    AtSprintf(m_accessLogMessage, "Long write  0x%08x: %s\r\n", address, AtDebugLongRegValue2String(dataBuffer, numDwords));
    LogMessage(m_accessLogMessage, m_writeLog);
    }

static void WillRemoveListener(AtDevice self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);

    AtFileClose(m_accessLogFile);
    m_accessLogFile = NULL;
    AllRangesDelete();
    }

static tAtDeviceListener *AccessListener(void)
    {
	static tAtDeviceListener listener;
	static tAtDeviceListener *pListener = NULL;
	
	if (pListener)
	    return pListener;

	listener.DidReadRegister    = DidReadRegister;
    listener.DidWriteRegister   = DidWriteRegister;
    listener.DidLongReadOnCore  = DidLongReadOnCore;
    listener.DidLongWriteOnCore = DidLongWriteOnCore;
    listener.WillRemoveListener = WillRemoveListener;
    pListener = &listener;
	
    return pListener;
    }

eBool CmdAtDeviceReadWriteLog(char argc, char **argv)
    {
    char *logFile  = NULL;

    if (argc == 0)
        {
        AtPrintc(cSevNormal, "* Read  log: %s\r\n", m_readLog ? "enable" : "disable");
        AtPrintc(cSevNormal, "* Write log: %s\r\n", m_writeLog ? "enable" : "disable");
        AtPrintc(cSevNormal, "* Log file : %s\r\n", m_accessLogFile ? AtFilePath(m_accessLogFile) : "None");
        return cAtTrue;
        }

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: please specify read/write enabling\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        logFile = argv[2];

    m_readLog  = CliBoolFromString(argv[0]);
    m_writeLog = CliBoolFromString(argv[1]);

    /* Need to close previous log file */
    AtFileClose(m_accessLogFile);
    m_accessLogFile = NULL;

    /* And open the new one */
    if (logFile)
        m_accessLogFile = AtStdFileOpen(AtStdSharedStdGet(), logFile, cAtFileOpenModeAppend);

    AtDeviceListenerAdd(CliDevice(), AccessListener(), NULL);
    return cAtTrue;
    }

eBool CmdAtDeviceReadWriteLogIgnoreRange(char argc, char **argv)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(argv[0], "-");
    uint32 start, stop;

    AtUnused(argc);

    StrToDw(AtTokenizerNextString(tokenizer), 'h', &start);
    StrToDw(AtTokenizerNextString(tokenizer), 'h', &stop);
    RangeAdd(start, stop);
    return cAtTrue;
    }

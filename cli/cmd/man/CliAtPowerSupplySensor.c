/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : CliAtPowerSupplySensor.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : CliAtPowerSupplySensor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "../physical/CliAtSensor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPowerSupplySensorAlarmStr[] = {"int", "aux", "bram", "psintlp", "psintfp", "psaux", "user0", "user1", "user2", "user3"};
static const uint32 cAtPowerSupplySensorAlarmVal[] = {cAtPowerSupplySensorAlarmOfVccInt,
                                                      cAtPowerSupplySensorAlarmOfVccAux,
                                                      cAtPowerSupplySensorAlarmOfVccBram,
                                                      cAtPowerSupplySensorAlarmOfVccPsintlp,
                                                      cAtPowerSupplySensorAlarmOfVccPsintfp,
                                                      cAtPowerSupplySensorAlarmOfVccPsaux,
                                                      cAtPowerSupplySensorAlarmOfVccUser0,
                                                      cAtPowerSupplySensorAlarmOfVccUser1,
                                                      cAtPowerSupplySensorAlarmOfVccUser2,
                                                      cAtPowerSupplySensorAlarmOfVccUser3,
                                                      };

static const char *cAtPowerSupplySensorVoltageStr[] = {"int", "aux", "bram", "psintlp", "psintfp", "psaux", "user0", "user1", "user2", "user3"};
static const uint32 cAtPowerSupplySensorVoltageVal[] = {cAtPowerSupplyVoltageInt,
                                                        cAtPowerSupplyVoltageAux,
                                                        cAtPowerSupplyVoltageBram,
                                                        cAtPowerSupplyVoltagePsintlp,
                                                        cAtPowerSupplyVoltagePsintfp,
                                                        cAtPowerSupplyVoltagePsaux,
                                                        cAtPowerSupplyVoltageUser0,
                                                        cAtPowerSupplyVoltageUser1,
                                                        cAtPowerSupplyVoltageUser2,
                                                        cAtPowerSupplyVoltageUser3
                                                        };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSensor Sensor(void)
    {
    return (AtSensor)AtDevicePowerSupplySensorGet(CliDevice());
    }

static uint32 VoltageValueFromString(char* string)
    {
    if (string == NULL)
        return 0x0;
    return AtStrToDw(string);
    }

static eBool AlarmThresholdSet(char argc, char** argv,
                               eAtRet (*ThresholdSetFunc)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltageValue))
    {
    eBool success;
    uint32 voltageValue;
    eAtRet ret = cAtOk;
    eAtPowerSupplyVoltage voltageType = CliStringToEnum(argv[0],
                                                        cAtPowerSupplySensorVoltageStr,
                                                        cAtPowerSupplySensorVoltageVal,
                                                        mCount(cAtPowerSupplySensorVoltageVal),
                                                        &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid voltage type: ");
        CliExpectedValuesPrint(cSevCritical, cAtPowerSupplySensorVoltageStr, mCount(cAtPowerSupplySensorVoltageVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    AtUnused(argc);
    voltageValue = VoltageValueFromString(argv[1]);
    ret = ThresholdSetFunc((AtPowerSupplySensor)Sensor(), voltageType, voltageValue);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical,
             "ERROR: Can not set alarm threshold for PowerSupply sensor, ret = %s\r\n",
             AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtPowerSupplySensorAlarmUpperThresholdSet(char argc, char** argv)
    {
    return AlarmThresholdSet(argc, argv, AtPowerSupplySensorAlarmUpperThresholdSet);
    }

eBool CmdAtPowerSupplySensorAlarmLowerThresholdSet(char argc, char** argv)
    {
    return AlarmThresholdSet(argc, argv, AtPowerSupplySensorAlarmLowerThresholdSet);
    }

eBool CmdAtDevicePowerSupplySensorShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"VoltageType", "UpperThreshold(mV)", "LowerThreshold(mV)", "CurrentVoltage(mV)", "MinRecordedVoltage(mV)", "MaxRecordedVoltage(mV)"};
    AtPowerSupplySensor sensor = (AtPowerSupplySensor)Sensor();
    uint8 column = 0;
    uint8 row_i;

    AtUnused(argv);
    AtUnused(argc);

    if (sensor == NULL)
        {
        AtPrintc(cSevInfo, "No sensor to show.\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(mCount(cAtPowerSupplySensorVoltageVal), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (row_i = 0; row_i < mCount(cAtPowerSupplySensorVoltageVal); row_i++)
        {
        column = 0;
        StrToCell(tabPtr, row_i, column++, cAtPowerSupplySensorVoltageStr[row_i]);
        StrToCell(tabPtr, row_i, column++, CliNumber2String(AtPowerSupplySensorAlarmUpperThresholdGet(sensor, cAtPowerSupplySensorVoltageVal[row_i]), "%d"));
        StrToCell(tabPtr, row_i, column++, CliNumber2String(AtPowerSupplySensorAlarmLowerThresholdGet(sensor, cAtPowerSupplySensorVoltageVal[row_i]), "%d"));
        StrToCell(tabPtr, row_i, column++, CliNumber2String(AtPowerSupplySensorVoltageGet(sensor, cAtPowerSupplySensorVoltageVal[row_i]), "%d"));
        StrToCell(tabPtr, row_i, column++, CliNumber2String(AtPowerSupplySensorMinRecordedVoltageGet(sensor, cAtPowerSupplySensorVoltageVal[row_i]), "%d"));
        StrToCell(tabPtr, row_i, column++, CliNumber2String(AtPowerSupplySensorMaxRecordedVoltageGet(sensor, cAtPowerSupplySensorVoltageVal[row_i]), "%d"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDevicePowerSupplySensorInterruptShow(char argc, char** argv)
    {
    return CliAtSensorInterruptShow(argc, argv,
                                    cAtPowerSupplySensorAlarmStr,
                                    cAtPowerSupplySensorAlarmVal,
                                    mCount(cAtPowerSupplySensorAlarmVal),
                                    Sensor);
    }

eBool CmdAtDevicePowerSupplySensorAlarmsShow(char argc, char** argv)
    {
    return CliAtSensorAlarmShow(argc, argv,
                                cAtPowerSupplySensorAlarmStr,
                                cAtPowerSupplySensorAlarmVal,
                                mCount(cAtPowerSupplySensorAlarmVal),
                                Sensor);
    }

eBool CmdAtPowerSupplySensorInit(char argc, char** argv)
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = AtSensorInit((AtSensor)Sensor());
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Initialize Sensor fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

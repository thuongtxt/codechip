/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDevice.c
 *
 * Created Date: Nov 11, 2012
 *
 * Description : AtDevice CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "CliAtDevice.h"
#include "../physical/CliAtSensor.h"
#include "../util/CliAtDebugger.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtDeviceRoleStr[] = {
                                        "active",
                                        "standby",
                                        "auto"
                                        };

static const uint32 cAtDeviceRoleVal[]  = {cAtDeviceRoleActive,
                                           cAtDeviceRoleStandby,
                                           cAtDeviceRoleAuto
                                           };

static const char * cAtDeviceAlarmTypeStr[] = {
                                          "protection"
                                          };

static const char * cAtDeviceAlarmPrettyTypeStr[] = {
                                          "active"
                                          };

static const uint32 cAtDeviceAlarmTypeVal[]  = {
                                            cAtDeviceAlarmProtectionSwitched
                                           };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InterruptEnable(char argc, char **argv, eBool enable, eAtRet (*Func)(AtIpCore self, eBool interruptEnable))
    {
    AtIterator coreIterator = AtDeviceCoreIteratorCreate(CliDevice());
    AtIpCore core;
    eAtRet ret = cAtOk;
    AtUnused(argv);
    AtUnused(argc);

    while ((core = (AtIpCore)AtIteratorNext(coreIterator)) != NULL)
        ret |= Func(core, enable);

    AtObjectDelete((AtObject)coreIterator);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool InterruptLogEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);
    return CliInterruptLogEnable(enabled);
    }

static eAtRet AsyncProcessUntilDone(AtDevice device, eAtRet (*Function)(AtDevice))
    {
    eAtRet ret;
    const uint32 timeoutMs = 5 * 60 * 1000; /* 5 minutes, not this slow, but... */
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    uint32 relaxTimeInUs = 0, midleRelax = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapseTime <= timeoutMs)
        {
        /* Only retry when having again error code */
        ret = Function(device);
        if (!AtDeviceAsyncRetValIsInState(ret))
            {
            AtPrintc(cSevInfo, "Total fixed Delay Time(us) = %d against total elapsed time = %d (including fixed delay) \r\n", relaxTimeInUs, elapseTime*1000);
            return ret;
            }

        if (ret == cAtRetNeedDelay)
            {
            midleRelax = AtDeviceAsyncDelayTimeUsGet(CliDevice());
            relaxTimeInUs += midleRelax;
            AtOsalUSleep(midleRelax);
            }

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Timeout */
    AtPrintc(cSevCritical, "ERROR: Device async process take too much time and has not finished yet\r\n");
    return cAtErrorDevFail;
    }

static eBool AsyncProcess(char argc, char **argv, eAtRet (*Function)(AtDevice self))
    {
    eAtRet ret;
    eBool block = cAtFalse;

    /* Check if "block" is input to wait till async initialize is done */
    if (argc > 0)
        {
        if (AtStrcmp(argv[0], "block") != 0)
            {
            AtPrintc(cSevCritical, "ERROR: expect \"block\"\r\n");
            return cAtFalse;
            }

        block = cAtTrue;
        }

    /* Async initializing */
    ret = (block) ? AsyncProcessUntilDone(CliDevice(), Function) : Function(CliDevice());

    /* Done, no error happens */
    if (ret == cAtOk)
        {
        AtPrintc(cSevInfo, "Device async process is completed\r\n");
        return cAtTrue;
        }

    /* Operator may want to try again or error actually happens */
    if (ret == cAtErrorAgain)
        {
        AtPrintc(cSevWarning, "Device async process has not been completed, try again\r\n");
        return cAtTrue;
        }

    /* Or a fixed delay should be made */
    if (ret == cAtRetNeedDelay)
        {
        AtPrintc(cSevWarning, "Device async process has not been completed, try again after %d (us)\r\n", AtDeviceAsyncDelayTimeUsGet(CliDevice()));
        return cAtTrue;
        }

    AtPrintc(cSevCritical, "Device async process is fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

static eBool AlwaysUseAsyncInit(void)
    {
    return cAtFalse;
    }

const char *CliAtDeviceRole2String(eAtDeviceRole role)
    {
    return CliEnumToString(role, cAtDeviceRoleStr, cAtDeviceRoleVal, mCount(cAtDeviceRoleVal), NULL);
    }

static uint32 CliDeviceIntrMaskFromString(char *pIntrStr)
    {
    return CliMaskFromString(pIntrStr, cAtDeviceAlarmTypeStr, cAtDeviceAlarmTypeVal, mCount(cAtDeviceAlarmTypeVal));
    }

static const char *Alarm2String(uint32 alarms, char *pAlarmString)
    {
    uint16 i;

    /* Initialize string */
    pAlarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(pAlarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cAtDeviceAlarmTypeVal); i++)
            {
            if (alarms & (cAtDeviceAlarmTypeVal[i]))
                AtSprintf(pAlarmString, "%s%s|", pAlarmString, cAtDeviceAlarmTypeStr[i]);
            }

        if (AtStrlen(pAlarmString) == 0)
            return "None";

        /* Remove the last '|' */
        pAlarmString[AtStrlen(pAlarmString) - 1] = '\0';
        }

    return pAlarmString;
    }

static void AlarmChangeState(AtDevice self, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    uint8 alarm_i;
    AtUnused(userData);

    AtPrintc(cSevNormal, "\r\n%s [Device] alarm changed at device %s", AtOsalDateTimeInUsGet(), AtObjectToString((AtObject)self));
    for (alarm_i = 0; alarm_i < mCount(cAtDeviceAlarmPrettyTypeStr); alarm_i++)
        {
        eAtDeviceAlarmType alarmType = cAtDeviceAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtDeviceAlarmPrettyTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static void AlarmChangeLogging(AtDevice self, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;
    AtUnused(userData);

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cAtDeviceAlarmPrettyTypeStr); alarm_i++)
        {
        eAtDeviceAlarmType alarmType = cAtDeviceAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        logPtr += AtSprintf(logPtr, " * [%s:", cAtDeviceAlarmPrettyTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            logPtr += AtSprintf(logPtr, "set");
        else
            logPtr += AtSprintf(logPtr, "clear");
        logPtr += AtSprintf(logPtr, "]");
        }
    CliDeviceLog(self, logBuffer);
    }

static eBool DevAlarmDefectShow(char argc, char **argv, uint32 (*AlarmGet)(AtDevice self), eBool silent)
    {
    uint16        column;
    tTab          *tabPtr = NULL;
    const char    *heading[] = {"Device", "protection"};
    static char   idString[64];
    uint32 alarms = 0;
    static const eAtDeviceAlarmType alarmTypes[] = {cAtDeviceAlarmProtectionSwitched};
    eBool  alarmRaise;
    uint8 alarm_i;

    AtUnused(argc);
    AtUnused(argv);

    if (!silent)
        {
        tabPtr = TableAlloc(1, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    alarms = AlarmGet((AtDevice)CliDevice());
    /* ID */
    column = 0;
    AtSprintf(idString, "%s", AtObjectToString((AtObject)CliDevice()));
    StrToCell(tabPtr, 0, column++, idString);

    /* Alarm */
    for (alarm_i = 0; alarm_i < mCount(alarmTypes); alarm_i++)
        {
        if (!AtDeviceAlarmIsSupported((AtDevice)CliDevice(), alarmTypes[alarm_i]))
            {
            ColorStrToCell(tabPtr, 0, column++, sAtNotApplicable, cSevNormal);
            continue;
            }

        alarmRaise = (alarms & alarmTypes[alarm_i]) ? cAtTrue : cAtFalse;
        ColorStrToCell(tabPtr, 0, column++, alarmRaise ? "set" : "clear", alarmRaise ? cSevCritical : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool HistoryShow(char argc, char **argv,
                         uint32 (*HistoryClearFunc)(AtDevice self),
                         uint32 (*HistoryGetFunc)(AtDevice self))
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 1 ) ? argv[1] : NULL);
    if (readingMode == cAtHistoryReadingModeReadToClear)
        return DevAlarmDefectShow(argc, argv, HistoryClearFunc, silent);
    else
        return DevAlarmDefectShow(argc, argv, HistoryGetFunc, silent);
    }

static eBool AlarmInterruptShow(char argc, char **argv)
    {
    return HistoryShow(argc, argv, AtDeviceAlarmInterruptClear, AtDeviceAlarmInterruptGet);
    }

eBool CmdAtDeviceInit(char argc, char **argv)
    {
    eAtRet ret;

    if (AlwaysUseAsyncInit())
        ret = AsyncProcessUntilDone(CliDevice(), AtDeviceAsyncInit);
    else
        ret = AtDeviceInit(CliDevice());

	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot initialize device, ret = %s\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtDeviceAsyncInit(char argc, char **argv)
    {
    return AsyncProcess(argc, argv, AtDeviceAsyncInit);
    }

eBool CmdAtDeviceShowReadWrite(char argc, char **argv)
    {
    eBool parseResult;
    eBool showWrite = cAtFalse;
    eBool showRead = cAtFalse;
    AtIterator coreIterate;
    AtIpCore core;
	AtUnused(argc);

    /* Get read option */
    mAtStrToBool(argv[0], showRead, parseResult);
    if (!parseResult)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    /* Get write option */
    mAtStrToBool(argv[1], showWrite, parseResult);
    if (!parseResult)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    /* Iterate and configure HAL of all cores */
    coreIterate = AtDeviceCoreIteratorCreate(CliDevice());
    while ((core = (AtIpCore)AtIteratorNext(coreIterate)) != NULL)
        {
        AtHalShowRead(AtIpCoreHalGet(core), showRead);
        AtHalShowWrite(AtIpCoreHalGet(core), showWrite);
        }

    AtObjectDelete((AtObject)coreIterate);

    return cAtTrue;
    }

eBool CmdAtDeviceAllServicesDestroy(char argc, char **argv)
    {
    eAtRet ret = AtDeviceAllServicesDestroy(CliDevice());
    eBool success = (ret == cAtOk) ? cAtTrue : cAtFalse;
    AtUnused(argc);
    AtUnused(argv);

    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Destroy services fail with ret = %s\r\n", AtRet2String(ret));

    return success;
    }

eBool CmdAtDeviceInterruptProcess(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtDeviceInterruptProcess(CliDevice());
    return cAtTrue;
    }

eBool CmdAtDeviceInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue, AtIpCoreInterruptEnable);
    }

eBool CmdAtDeviceInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse, AtIpCoreInterruptEnable);
    }

eBool CmdAtDeviceSemInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue, AtIpCoreSemInterruptEnable);
    }

eBool CmdAtDeviceSemInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse, AtIpCoreSemInterruptEnable);
    }

eBool CmdAtDeviceSysmonInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue, AtIpCoreSysmonInterruptEnable);
    }

eBool CmdAtDeviceSysmonInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse, AtIpCoreSysmonInterruptEnable);
    }

eBool CmdAtDeviceMemoryTest(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    if (AtDeviceMemoryTest(CliDevice()) != cAtOk)
        {
        AtPrintc(cSevCritical, "Memory testing FAIL\r\n");
        return cAtFalse;
        }

    AtPrintc(cSevInfo, "Memory testing PASS\r\n");
    return cAtTrue;
    }

eBool CmdAtDeviceShowModules(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"module", "capacity"};
    uint8 row_i;
    AtModule module;
    AtIterator moduleIterator = AtDeviceModuleIteratorCreate(CliDevice());
	AtUnused(argv);
	AtUnused(argc);

	/* Plus one row for SEM */
    tabPtr = TableAlloc(AtIteratorCount(moduleIterator) + 1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)moduleIterator);
        return cAtFalse;
        }

    row_i = 0;
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        {
        StrToCell(tabPtr, row_i, 0, AtModuleTypeString(module));
        StrToCell(tabPtr, row_i, 1, AtModuleCapacityDescription(module));
        row_i++;
        }

    StrToCell(tabPtr, row_i, 0, "sem");
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtDeviceNumSemControllersGet(CliDevice()), "controllers: %u"));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)moduleIterator);

    return cAtTrue;
    }

eBool CmdAtDeviceDebug(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtDeviceDebug(CliDevice());
    return cAtTrue;
    }

eBool CmdAtDeviceStatusClear(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtDeviceStatusClear(CliDevice());
    return cAtTrue;
    }

eBool CmdAtDeviceWarmRestoreStart(char argc, char **argv)
    {
    eAtRet ret = AtDeviceWarmRestoreStart(CliDevice());
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot start warm restore, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtDeviceWarmRestoreStop(char argc, char **argv)
    {
    eAtRet ret = AtDeviceWarmRestoreStop(CliDevice());
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot stop warm restore, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtDeviceWarmRestore(char argc, char **argv)
    {
    eAtRet ret = AtDeviceWarmRestore(CliDevice());

    AtUnused(argc);
    AtUnused(argv);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot warm restore, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtDeviceSSKeyCheck(char argc, char **argv)
    {
    eAtRet ret = AtDeviceSSKeyCheck(CliDevice());
	AtUnused(argv);
	AtUnused(argc);

    if (ret == cAtOk)
        {
        AtPrintc(cSevInfo, "SSKey works!\r\n");
        return cAtTrue;
        }

    AtPrintc(cSevCritical, "SSKey does not work, ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtDeviceDiagnosticModeEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDeviceDiagnosticModeEnable(CliDevice(), cAtTrue);
    return cAtTrue;
    }

eBool CmdAtDeviceDiagnosticModeDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDeviceDiagnosticModeEnable(CliDevice(), cAtFalse);
    return cAtTrue;
    }

eBool CmdAtDeviceDiagnosticCheckEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDeviceDiagnosticCheckEnable(CliDevice(), cAtTrue);
    return cAtTrue;
    }

eBool CmdAtDeviceDiagnosticCheckDisabled(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDeviceDiagnosticCheckEnable(CliDevice(), cAtFalse);
    return cAtTrue;
    }

eBool CmdAtDeviceDiagnosticShow(char argc, char **argv)
    {
    eBool enabled;
    AtUnused(argc);
    AtUnused(argv);

    enabled = AtDeviceDiagnosticModeIsEnabled(CliDevice());
    AtPrintc(cSevNormal, "* Diagnostic mode: ");
    AtPrintc(enabled ? cSevInfo : cSevNormal, "%s\r\n", enabled ? "enabled" : "disabled");

    enabled = AtDeviceDiagnosticCheckIsEnabled(CliDevice());
    AtPrintc(cSevNormal, "* Diagnostic check: ");
    AtPrintc(enabled ? cSevInfo : cSevNormal, "%s\r\n", enabled ? "enabled" : "disabled");

    return cAtTrue;
    }

eBool CmdAtDeviceInterruptRestore(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtDeviceInterruptRestore(CliDevice());
    return cAtTrue;
    }

eBool CmdAtDeviceReset(char argc, char **argv)
    {
    eAtRet ret;
    AtUnused(argv);
    AtUnused(argc);

    ret = AtDeviceReset(CliDevice());
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Reset device fail, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtDeviceInterruptShow(char argc, char **argv)
    {
    AtIterator coreIterator = NULL;
    AtIpCore core;
    uint32 numIpCore = AtDeviceNumIpCoresGet(CliDevice());
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"IpCore", "Interrupt", "InterruptMask"};
    eBool enabled;
    uint32 intrMaskVal;
    uint32 column = 0;
    char alarmString[128];

    AtUnused(argv);
    AtUnused(argc);

    if (argc > 0)
        return AlarmInterruptShow(argc, argv);

    coreIterator = AtDeviceCoreIteratorCreate(CliDevice());
    tabPtr = TableAlloc(numIpCore, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    while ((core = (AtIpCore)AtIteratorNext(coreIterator)) != NULL)
        {
        column = 0;

        StrToCell(tabPtr, row, column++, CliNumber2String(AtIpCoreIdGet(core) + 1, "%u"));

        enabled = AtIpCoreInterruptIsEnabled(core);
        StrToCell(tabPtr, row, column, CliBoolToString(enabled));
        }

    /* Interrupt mask */
    intrMaskVal = AtDeviceInterruptMaskGet(CliDevice());
    column++;
    StrToCell(tabPtr, row, column, Alarm2String(intrMaskVal, alarmString));

    AtObjectDelete((AtObject)coreIterator);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceInterruptLogEnable(char argc, char **argv)
    {
    return InterruptLogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDeviceInterruptLogDisable(char argc, char **argv)
    {
    return InterruptLogEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDeviceAllChannelsListenDefectClear(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtDeviceAllChannelsListenedDefectClear(CliDevice());
    return cAtTrue;
    }

eBool CmdAtDeviceRoleSet(char argc, char** argv)
    {
    eAtDeviceRole role;
    eBool convertSuccess = cAtTrue;
    eAtRet ret;

    AtUnused(argc);

    role = CliStringToEnum(argv[0], cAtDeviceRoleStr, cAtDeviceRoleVal, mCount(cAtDeviceRoleVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Device role parameter. Expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtDeviceRoleStr, mCount(cAtDeviceRoleVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtDeviceRoleSet(CliDevice(), role);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Set device role is failed, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtDeviceAsyncReset(char argc, char **argv)
    {
    return AsyncProcess(argc, argv, AtDeviceAsyncReset);
    }

eBool CmdAtDeviceProblemsQuery(char argc, char **argv)
    {
    AtDebugger debugger = AtDebuggerNew();
    AtQuerier querier = AtDeviceQuerierGet(CliDevice());

    AtUnused(argc);
    AtUnused(argv);

    AtQuerierProblemsQuery(querier, (AtObject)CliDevice(), debugger);

    if (AtDebuggerNumEntries(debugger) == 0)
        AtPrintc(cSevInfo, "No problems are found\r\n");
    else
        CliAtDebuggerShow(debugger);

    AtObjectDelete((AtObject)debugger);

    return cAtTrue;
    }

eBool CmdAtDeviceShowSubDevice(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"ID", "Description"};
    uint8 row_i, dev_i;
    AtDevice subDev;
    AtDevice masterDev = CliDevice();
    uint8 numSubDev = AtDeviceNumSubDeviceGet(masterDev);
    char cellStr[64];
    AtUnused(argv);
    AtUnused(argc);

    /* Plus one row for SEM */
    tabPtr = TableAlloc(numSubDev, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    row_i = 0;
    for (dev_i = 0; dev_i < numSubDev; dev_i++)
        {
        subDev = AtDeviceSubDeviceGet(masterDev, dev_i);
        AtSprintf(cellStr, "%d", (dev_i + 1));
        StrToCell(tabPtr, row_i, 0, cellStr);
        StrToCell(tabPtr, row_i, 1, AtObjectToString((AtObject)subDev));
        row_i++;
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdDeviceInterruptMaskSet(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet  ret;
    uint32 intrMaskVal;
    eBool enable = cAtFalse, result;
    AtUnused(argc);

    ret = cAtOk;

    /* Get and Convert interrupt mask from string */
    intrMaskVal = CliDeviceIntrMaskFromString(argv[0]);

    /* Get enable mode */
    mAtStrToBool(argv[1], enable, result)
    if(!result)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter\r\n"
                                 "Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* SET Enabel/Disable */
    ret = AtDeviceInterruptMaskSet(CliDevice(), intrMaskVal, (enable) ? intrMaskVal : 0x0);
    if (ret != cAtOk)
       AtPrintc(cSevCritical, "Can not set interrupt mask for %s, ret: %s\r\n", AtObjectToString((AtObject)CliDevice()), AtRet2String(ret));

    return cAtTrue;
    }

eBool CmdDeviceAlarmCapture(char argc, char **argv)
    {
    eAtRet ret;
    eBool convertSuccess;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    static tAtDeviceEventListener devListener;
    AtUnused(argc);

    /* Enabling */
    mAtStrToBool(argv[0], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&devListener, 0, sizeof(devListener));
    if (AutotestIsEnabled())
        devListener.AlarmChangeStateWithUserData = AlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        devListener.AlarmChangeStateWithUserData = AlarmChangeLogging;
    else
        devListener.AlarmChangeStateWithUserData = AlarmChangeState;

    if (captureEnabled)
        ret = AtDeviceEventListenerAddWithUserData(CliDevice(), &devListener, NULL);
    else
        ret = AtDeviceEventListenerRemove(CliDevice(), &devListener);

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                 captureEnabled ? "register" : "deregister",
                 AtObjectToString((AtObject)CliDevice()),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdDeviceAlarmShow(char argc, char **argv)
    {
    return DevAlarmDefectShow(argc, argv, AtDeviceAlarmGet, cAtFalse);
    }


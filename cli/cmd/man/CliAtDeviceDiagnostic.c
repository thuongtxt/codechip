/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtDeviceDiagnostic.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : Device diagnostic CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "CliAtUart.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtDeviceDiagUartTransmit(char argc, char **argv)
    {
    AtUart uart = AtDeviceDiagnosticUartGet(CliDevice());
    return CliAtUartCommand(uart, argc, argv, cAtTrue, cAtFalse, argv[0], AtStrlen(argv[0]));
    }

eBool CmdAtDeviceDiagUartReceive(char argc, char **argv)
    {
    AtUart uart = AtDeviceDiagnosticUartGet(CliDevice());
    return CliAtUartCommand(uart, argc, argv, cAtFalse, cAtTrue, NULL, 0);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtUartEbd.c
 *
 * Created Date: Sep 14, 2016
 *
 * Description : To handle EBD files
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "CliAtUart.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void CliAtUartEssentialErrorInReportCheck(AtSemController sem, const char *report, uint32 length)
    {
    eBool isEssential = AtSemControllerErrorIsEssential(sem, report, length);
    AtPrintc(cSevInfo, "SEM#%u: The error is %s\r\n", AtSemControllerIdGet(sem) + 1, isEssential ? "essential" : "non-essential");
    }

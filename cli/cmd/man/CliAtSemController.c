/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtSemController.c
 *
 * Created Date: Apr 5, 2016
 *
 * Description : SEM controller CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtTokenizer.h"
#include "CliAtUart.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtSemControllerAlarmStr[] =
        {
         "active",
         "idle",
         "initialization",
         "observation",
         "injection",
         "classification",
         "correction",
         "fatal-error",
         "essential",
         "correctable",
         "uncorrectable",
         "heartbeat",
         "heartbeatslr1",
         "heartbeatslr2",
         "activeslr1",
         "activeslr2"
        };
static const uint32 cAtSemControllerAlarmVal[] =
        {
         cAtSemControllerAlarmActive,
         cAtSemControllerAlarmIdle,
         cAtSemControllerAlarmInitialization,
         cAtSemControllerAlarmObservation,
         cAtSemControllerAlarmInjection,
         cAtSemControllerAlarmClassification,
         cAtSemControllerAlarmCorrection,
         cAtSemControllerAlarmFatalError,
         cAtSemControllerAlarmErrorEssential,
         cAtSemControllerAlarmErrorCorrectable,
         cAtSemControllerAlarmErrorNoncorrectable,
         cAtSemControllerAlarmHeartbeat,
         cAtSemControllerAlarmHeartbeatSlr1,
         cAtSemControllerAlarmHeartbeatSlr2,
         cAtSemControllerAlarmActiveSlr1,
         cAtSemControllerAlarmActiveSlr2
        };
static const char * cAtSemControllerAlarmUpperStr[] =
        {
         "Active",
         "Idle",
         "Initialization",
         "Observation",
         "Injection",
         "Classification",
         "Correction",
         "Fatal-Error",
         "ErrorEssential",
         "Error-Correctable",
         "Error-Uncorrectable",
         "Heartbeat",
         "HeartbeatSlr1",
         "HeartbeatSlr2",
         "ActiveSlr1",
         "ActiveSlr2"
        };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char * Alarm2StateString(uint32 alarms)
    {
    static char alarmString[256];

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "Unknown");

    /* There are alarms */
    else
        {
        if (alarms & cAtSemControllerAlarmIdle)
            AtSprintf(alarmString, "%s%s|", alarmString, "Idle");
        if (alarms & cAtSemControllerAlarmInitialization)
            AtSprintf(alarmString, "%s%s|", alarmString, "Initialization");
        if (alarms & cAtSemControllerAlarmObservation)
            AtSprintf(alarmString, "%s%s|", alarmString, "Observation");
        if (alarms & cAtSemControllerAlarmInjection)
            AtSprintf(alarmString, "%s%s|", alarmString, "Injection");
        if (alarms & cAtSemControllerAlarmClassification)
            AtSprintf(alarmString, "%s%s|", alarmString, "Classification");
        if (alarms & cAtSemControllerAlarmCorrection)
            AtSprintf(alarmString, "%s%s|", alarmString, "Correction");

        if (AtStrlen(alarmString) == 0)
            return "Unknown";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

static const char * Alarms2String(uint32 alarms, const char **arrayOfAlarmString)
    {
    uint16 i;
    static char alarmString[256];
    uint32 numAlarms = mCount(cAtSemControllerAlarmVal);

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < numAlarms; i++)
            {
            if (alarms & (cAtSemControllerAlarmVal[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, arrayOfAlarmString[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

static const char * AlarmMask2String(uint32 alarms)
    {
    return Alarms2String(alarms, cAtSemControllerAlarmStr);
    }

static uint32 AlarmString2Mask(char * alarmStr)
    {
    uint32 alarmMask = 0;
    AtTokenizer typeParser = AtTokenizerSharedTokenizer(alarmStr, "|");
    uint32 numTypes = mCount(cAtSemControllerAlarmVal);

    /* Parse masks */
    while (AtTokenizerHasNextString(typeParser))
        {
        char *alarmTypeStr = AtTokenizerNextString(typeParser);
        eBool convertSuccess = cAtFalse;
        uint32 alarm = CliStringToEnum(alarmTypeStr, cAtSemControllerAlarmStr, cAtSemControllerAlarmVal, numTypes, &convertSuccess);

        if (!convertSuccess)
            {
            AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", alarmTypeStr);
            CliExpectedValuesPrint(cSevCritical, cAtSemControllerAlarmStr, numTypes);
            AtPrintc(cSevCritical, "\r\n");
            return cAtFalse;
            }

        alarmMask = alarmMask | alarm;
        }

    return alarmMask;
    }

static AtSemController* ControllersGet(char* pSemIdStrList, uint32 *numSemControllers)
    {
    uint32 numIds;
    uint32 *semIdList = CliSharedIdBufferGet(&numIds);
    uint32 numSemIds;
    uint32 sem_i;
    AtSemController *semControllerList = (AtSemController*)CliSharedObjectListGet(&numSemIds);

    *numSemControllers = 0;
    numSemIds = CliIdListFromString(pSemIdStrList, semIdList, numIds);

    for (sem_i = 0; sem_i < numSemIds; sem_i++)
        {
        semControllerList[*numSemControllers] = AtDeviceSemControllerGetByIndex(CliDevice(), (uint8)CliId2DriverId(semIdList[sem_i]));
        if (semControllerList[*numSemControllers])
            (*numSemControllers)++;
        }

    return semControllerList;
    }

static eBool AlarmShow(AtSemController *semControllers, uint32 numSemControllers, uint32 (*AlarmGet)(AtSemController semController))
    {
    tTab *tabPtr;
    uint8 column = 0;
    uint8 row = 0;
    uint32 numAlarms = mCount(cAtSemControllerAlarmVal);

    tabPtr = TableAlloc(numSemControllers, numAlarms, cAtSemControllerAlarmUpperStr);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (row = 0; row < numSemControllers; row++)
        {
        uint32 alarms = AlarmGet(semControllers[row]);

        for (column = 0; column < numAlarms; column++)
            {
            if (alarms & cAtSemControllerAlarmVal[column])
                ColorStrToCell(tabPtr, row, column, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, row, column, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static void SemAlarmChangeStateNotify(AtSemController controller,
                                      uint32 changedAlarms,
                                      uint32 currentAlarms,
                                      void *listener)
    {
    uint32 alarm_i;
    AtUart uart = AtSemControllerUartGet(controller);
    AtUnused(listener);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "%s [PHY] Alarms changed at %s",
             AtOsalDateTimeInUsGet(),
             AtObjectToString((AtObject)controller));

    for (alarm_i = 0; alarm_i < mCount(cAtSemControllerAlarmVal); alarm_i++)
        {
        uint32 alarmType = cAtSemControllerAlarmVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtSemControllerAlarmUpperStr[alarm_i]);
        if (currentAlarms & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }

    AtPrintc(cSevInfo, "\r\n");
    if (uart != NULL)
        CliAtDeviceUartInterruptHandler(controller, changedAlarms, currentAlarms);
    }

static tAtSemControllerEventListener *Listener(void)
	{
	static tAtSemControllerEventListener listener;
	static tAtSemControllerEventListener *pListener = NULL;
		
	if (pListener)
		return pListener;
	
	AtOsalMemInit(&listener, 0, sizeof(listener));
	listener.AlarmChangeStateNotify = SemAlarmChangeStateNotify;
	
	pListener = &listener;
	return pListener;
	}

static eBool NotificationEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret;
    eBool success = cAtTrue;
    uint32 numSemControllers;
    AtSemController *semControllersList = ControllersGet(argv[0], &numSemControllers);
    tAtSemControllerEventListener *listener = Listener();
    uint8 sem_i;

    AtUnused(argc);
    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        if (enable)
            ret = AtSemControllerEventListenerAdd(semControllersList[sem_i], listener, NULL);
        else
            ret = AtSemControllerEventListenerRemove(semControllersList[sem_i], listener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Listener %s fail on %s, ret = %s\r\n",
                     enable ? "add" : "remove",
                     AtObjectToString((AtObject)semControllersList[sem_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static uint8 SemStatusColor(eAtSemControllerStatus semStatus)
    {
    AtUnused(semStatus);
    return cSevNormal;
    }

static const char *SemStatusString(eAtSemControllerAlarm semStatus)
    {
    return Alarm2StateString(semStatus);
    }

static uint8 SemAlarmColor(eAtSemControllerAlarm semAlarm)
    {
    AtUnused(semAlarm);
    return cSevNormal;
    }

static const char *SemAlarmString(eAtSemControllerAlarm semAlarm)
    {
    return Alarms2String(semAlarm, cAtSemControllerAlarmUpperStr);
    }

static AtList UartsGet(char *idString)
    {
    uint32 numObjects, i;
    AtSemController *semControllersList = ControllersGet(idString, &numObjects);
    AtList uartList = AtListCreate(0);

    for (i = 0; i < numObjects; i++)
        {
        AtUart uart = AtSemControllerUartGet(semControllersList[i]);
        if (uart)
            AtListObjectAdd(uartList, (AtObject)uart);
        }

    return uartList;
    }

static char* SemControllerEbdFilePathsToString(AtSemController self)
    {
    uint8 slr = 0, numSlr = AtSemControllerNumSlr(self);
    uint32 strLen = 0;
    char *catString = NULL;

    for (slr = 0; slr < numSlr; slr++)
        strLen += AtSemControllerEbdFilePathGet(self, slr) != NULL ? AtStrlen(AtSemControllerEbdFilePathGet(self, slr)) : 0;

    if (strLen == 0)
        return NULL;

    catString = AtOsalMemAlloc(strLen + (uint32)(3*numSlr));
    strLen = 0;
    for (slr = 0; slr < numSlr; slr++)
        {
        const char *filePath = AtSemControllerEbdFilePathGet(self, slr);
        AtSprintf(&catString[strLen], "%s", filePath);

        if (slr == (numSlr - 1))
            break;

        strLen += AtStrlen(filePath);
        AtSprintf(&catString[strLen], "%s", "\r\n");
        strLen += 2;
        }

    return catString;
    }

eBool CmdAtDeviceSemShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint8 sem_i;
    eBool boolVal;
    eAtSemControllerAlarm semAlarm;
    uint32 numSemControllers;
    AtSemController *semControllersList;
    const char *pHeading[] = {"SemId", "Active", "Status", "Alarm", "Error count", "InterruptMask"};

    AtUnused(argc);

    /* Get SEM ID list */
    semControllersList = ControllersGet(argv[0], &numSemControllers);
    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc((uint32)numSemControllers, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        AtSemController semController = semControllersList[sem_i];
        uint8 column = 0;

        /* SEM ID */
        StrToCell(tabPtr, sem_i, column++, CliNumber2String(sem_i + 1, "%u"));

        /* TODO: This table should show InterruptMask column only.
         * Other should be removed when OCN card has been upgraded to new SEM controller. */

        /* Status and alarm */
        semAlarm = AtSemControllerAlarmGet(semController);
        if (semAlarm == 0)
            AtPrintc(cSevCritical, "SEM IP is not active\r\n");
        boolVal = (semAlarm & cAtSemControllerAlarmActive) ? cAtTrue : cAtFalse;
        ColorStrToCell(tabPtr, sem_i, column++, boolVal ? "Yes" : "No", boolVal ? cSevInfo : cSevCritical);
        ColorStrToCell(tabPtr, sem_i, column++, SemStatusString(semAlarm), SemStatusColor(semAlarm));
        ColorStrToCell(tabPtr, sem_i, column++, SemAlarmString(semAlarm), SemAlarmColor(semAlarm));

        /* Counter */
        ColorStrToCell(tabPtr, sem_i, column++, CliNumber2String(AtSemControllerCorrectedErrorCounterGet(semController), "%u"), cSevNormal);

        semAlarm = AtSemControllerInterruptMaskGet(semController);
        StrToCell(tabPtr, sem_i, column++, AlarmMask2String(semAlarm));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceSemInfoShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint8 sem_i;
    eAtSemControllerAlarm semAlarm;
    uint32 numSemControllers;
    AtSemController *semControllersList;
    const char *pHeading[] = {"SemId", "Device Name", "Sem State", "Frame Size", "MaxLfa", "Number of SLR", "EBD files"};

    AtUnused(argc);

    /* Get SEM ID list */
    semControllersList = ControllersGet(argv[0], &numSemControllers);
    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc((uint32)numSemControllers, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        AtSemController semController = semControllersList[sem_i];
        uint32 column = 0;
        char *ebdFilePaths = NULL;

        /* SEM ID */
        StrToCell(tabPtr, sem_i, column++, CliNumber2String(sem_i + 1, "%u"));

        /* Device Name */
        StrToCell(tabPtr, sem_i, column++, AtSemControllerDeviceDescription(semController));

        /* Sem State */
        semAlarm = AtSemControllerAlarmGet(semController);
        ColorStrToCell(tabPtr, sem_i, column++, SemStatusString(semAlarm), SemStatusColor(semAlarm));

        /* Frame size */
        ColorStrToCell(tabPtr, sem_i, column++, CliNumber2String(AtSemControllerFrameSize(semController), "%u"), cSevNormal);

        /* Max Frame */
        ColorStrToCell(tabPtr, sem_i, column++, CliNumber2String(AtSemControllerMaxLfa(semController), "0x%x"), cSevNormal);

        /* Number of SLR */
        ColorStrToCell(tabPtr, sem_i, column++, CliNumber2String(AtSemControllerNumSlr(semController), "%u"), cSevNormal);

        /* EBD file paths */
        ebdFilePaths = SemControllerEbdFilePathsToString(semController);
        ColorStrToCell(tabPtr, sem_i, column++, ebdFilePaths, cSevNormal);
        AtOsalMemFree(ebdFilePaths);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceSemValidate(char argc, char **argv)
    {
    tTab *tabPtr;
    uint8 sem_i;
    const char *pHeading[] = {"SemId", "Result", "Corrected counter"};
    uint32 numSemControllers;
    AtSemController *semControllersList;
    AtUnused(argc);

    /* Get SEM ID list */
    semControllersList = ControllersGet(argv[0], &numSemControllers);
    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc((uint32)numSemControllers, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        char * buffer = CliUartSharedBuffer();
        AtSemController semControler = semControllersList[sem_i];
        AtUart uart = AtSemControllerUartGet(semControler);
        uint32 column = 0;
        eAtRet semValidate;

        StrToCell(tabPtr, sem_i, column++, CliNumber2String(AtSemControllerIdGet(semControler) + 1, "%u"));

        semValidate = AtSemControllerValidate(semControler);
        ColorStrToCell(tabPtr, sem_i, column++, AtRet2String(semValidate), (semValidate == cAtOk) ? cSevInfo : cSevCritical);

        /*  To release buffer hw then can validate many times with out clear Rx Buffer */
        if (uart)
            {
        	AtOsalMemInit(buffer, 0, cAtUartMaxBufferLenInBytes);
        	AtUartReceive(uart, buffer, cAtUartMaxBufferLenInBytes);
            }

        ColorStrToCell(tabPtr, sem_i, column++, CliNumber2String(AtSemControllerCorrectedErrorCounterGet(semControler), "%d"), cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceSemAlarmShow(char argc, char** argv)
    {
    uint32 numSemControllers;
    AtSemController *semControllersList = ControllersGet(argv[0], &numSemControllers);
    eBool result;
    AtUnused(argc);

    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    result = AlarmShow(semControllersList, numSemControllers, AtSemControllerAlarmGet);
    AtPrintc(cSevInfo, "\r\nNote: Active and Observation are usually set in normal operation\r\n");

    return result;
    }

eBool CmdAtDeviceSemInterruptShow(char argc, char** argv)
    {
    uint32 numSemControllers;
    AtSemController *semControllersList = ControllersGet(argv[0], &numSemControllers);
    uint32 (*InterruptGet)(AtSemController semController);
    AtUnused(argc);

    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    /* Print alarm status */
    if (CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadToClear)
        InterruptGet = AtSemControllerAlarmHistoryClear;
    else
        InterruptGet = AtSemControllerAlarmHistoryGet;

    return AlarmShow(semControllersList, numSemControllers, InterruptGet);
    }

eBool CmdAtDeviceSemCountersShow(char argc, char** argv)
    {
    uint32 numSemControllers;
    AtSemController *semControllersList = ControllersGet(argv[0], &numSemControllers);
    tTab *tabPtr;
    uint8 column = 0, row = 0;
    uint32 (*CounterGet)(AtSemController self, uint32 counterType);
    const char *pHeading[] = {"Essential", "Correctable", "Uncorrectable"};
    uint32 counterType[] = {cAtSemControllerCounterTypeEssential,
                            cAtSemControllerCounterTypeCorrectable,
                            cAtSemControllerCounterTypeUncorrectable};
    AtUnused(argc);

    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numSemControllers, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Print alarm status */
    if (CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadToClear)
        CounterGet = AtSemControllerCounterClear;
    else
        CounterGet = AtSemControllerCounterGet;

    for (row = 0; row < numSemControllers; row++)
        {
        for (column = 0; column < mCount(counterType); column++)
            {
            uint32 counter = CounterGet(semControllersList[row], counterType[column]);
            ColorStrToCell(tabPtr, row, column, CliNumber2String(counter, "%u"), counter ? cSevCritical : cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtDeviceSemInterruptMask(char argc, char** argv)
    {
    eAtRet ret;
    uint32 numSemControllers;
    AtSemController *semControllersList = ControllersGet(argv[0], &numSemControllers);
    uint32 alarmMask = AlarmString2Mask(argv[1]);
    eBool enabled = CliBoolFromString(argv[2]);
    uint8 sem_i;
    AtUnused(argc);

    if (numSemControllers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SEM controller list ID. Expected [1-3]\r\n");
        return cAtFalse;
        }

    for (sem_i = 0; sem_i < numSemControllers; sem_i++)
        {
        ret = AtSemControllerInterruptMaskSet(semControllersList[sem_i], alarmMask, (enabled) ? alarmMask : 0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot set interrupt mask for SEM, ret = %s\n", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtDeviceSemNotificationEnable(char argc, char **argv)
    {
    return NotificationEnable(argc, argv, cAtTrue);
    }

eBool CmdAtDeviceSemNotificationDisable(char argc, char **argv)
    {
    return NotificationEnable(argc, argv, cAtFalse);
    }

eBool CmdAtDeviceSemUartCommand(char argc, char **argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtDeviceUartCommand(uarts, argc, argv, cAtTrue, cAtTrue);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

eBool CmdAtDeviceSemUartCommandSend(char argc, char** argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtDeviceUartCommand(uarts, argc, argv, cAtTrue, cAtFalse);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

eBool CmdAtDeviceSemUartCommandReceive(char argc, char** argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtDeviceUartCommand(uarts, argc, argv, cAtFalse, cAtTrue);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

eBool CmdAtDeviceSemUartValidate(char argc, char** argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtUartValidate(uarts, argc, argv);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

eBool CmdAtDeviceSemUartErrorForce(char argc, char** argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtUartErrorForce(uarts, argc, argv);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

eBool CmdAtDeviceSemUartAsyncErrorForce(char argc, char** argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtUartAsyncErrorForce(uarts, argc, argv);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

eBool CmdAtDeviceSemUartEbdFilePathSet(char argc, char** argv)
    {
    uint32 semId = AtStrToDw(argv[0]) - 1;
    AtSemController sem = AtDeviceSemControllerGetByIndex(CliDevice(), (uint8)semId);
    uint8 slr, numSlr = 0;

    if (sem == NULL)
        {
        AtPrintc(cSevCritical, "SEM Controller#%d is not exist\r\n", semId);
        return cAtFalse;
        }

    numSlr = AtSemControllerNumSlr(sem);
    if ((uint32)(argc - 1) != numSlr)
        {
        AtPrintc(cSevCritical, "Number of EBD files (%d) is not equal to number of SLR (%d)\r\n", argc - 1, numSlr);
        return cAtFalse;
        }

    for (slr = 0; slr < numSlr; slr++)
        {
        eAtRet ret = AtSemControllerEbdFilePathSet(sem, slr, argv[1 + slr]);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "Set EBD file path at SLR#%d fail with ret = %s\r\n", slr, AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtDeviceSemUartDebug(char argc, char** argv)
    {
    AtList uarts = UartsGet(argv[0]);
    eBool success = CliAtDeviceUartDebug(uarts);
    AtUnused(argc);
    AtObjectDelete((AtObject)uarts);
    return success;
    }

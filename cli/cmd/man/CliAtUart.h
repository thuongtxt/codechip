/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Man
 * 
 * File        : CliAtUart.h
 * 
 * Created Date: Apr 25, 2016
 *
 * Description : CliAtUart declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATUART_H_
#define _CLIATUART_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtUart.h"
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#define cAtUartMaxBufferLenInBytes  2048

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
char *CliUartSharedBuffer(void);
void CliAtDeviceUartInterruptHandler(AtSemController self, uint32 interruptStatus, uint32 currenAlarm);

eBool CliAtUartCommand(AtUart uart, char argc, char **argv,
                       eBool needSend, eBool needReceive,
                       char *command, uint32 length);
eBool CliAtDeviceUartCommand(AtList uarts, char argc, char **argv, eBool needSend, eBool needReceive);
eBool CliAtDeviceUartDebug(AtList uarts);
eBool CliAtUartValidate(AtList uarts, char argc, char** argv);
eBool CliAtUartErrorForce(AtList uarts, char argc, char** argv);
eBool CliAtUartAsyncErrorForce(AtList uarts, char argc, char** argv);
eBool CliAtDeviceUartReceive(AtList uarts);

eBool CliAtUartEbdFilePathSet(uint32 semId, const char* filePath);
void CliAtUartEssentialErrorInReportCheck(AtSemController sem, const char *report, uint32 length);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATUART_H_ */


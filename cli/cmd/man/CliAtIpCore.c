/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : CliAtIpCore.c
 *
 * Created Date: Feb 24, 2016
 *
 * Description : IP core CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtIpCore.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtIpCoreDebug(char argc, char **argv)
    {
    AtDevice device = CliDevice();
    uint32 numCores = AtDeviceNumIpCoresGet(device);
    uint32 numIds;
    uint32 *coreIdList = CliSharedIdBufferGet(&numIds);
    uint32 core_i;

    if (argc > 0)
        numCores = CliIdListFromString(argv[0], coreIdList, numIds);

    /* Do not need to display core ID if device has only one core */
    if (numCores == 1)
        {
        AtIpCoreDebug(AtDeviceIpCoreGet(device, 0));
        return cAtTrue;
        }

    for (core_i = 0; core_i < numCores; core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(device, (uint8)(coreIdList[core_i] - 1));
        if (core == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: ignore not existing core %d\r\n", coreIdList[core_i]);
            continue;
            }

        AtPrintc(cSevInfo, "* %s:\r\n", AtObjectToString((AtObject)core));
        AtIpCoreDebug(core);
        }

    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtRegisterHashTableInternal.h
 * 
 * Created Date: Oct 6, 2015
 *
 * Description : Register hash table internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_REGCHECK_HASHTABLE_CLIATREGISTERHASHTABLEINTERNAL_H_
#define _CLI_CMD_REGCHECK_HASHTABLE_CLIATREGISTERHASHTABLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "CliAtRegisterHashTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tRegisterHashTable
    {
    RegisterEntry* regBucket;
    uint32 maxCollisionPerHash;
    uint32 size;
    }tRegisterHashTable;

typedef struct tRegisterHashTableIterator
    {
    uint32 index;
    RegisterEntry currentEntry;
    RegisterHashTable hashTable;
    }tRegisterHashTableIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_REGCHECK_HASHTABLE_CLIATREGISTERHASHTABLEINTERNAL_H_ */


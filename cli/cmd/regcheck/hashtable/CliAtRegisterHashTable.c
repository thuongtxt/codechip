/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtRegisterHashTable.c
 *
 * Created Date: Sep 30, 2015
 *
 * Description : Register hash table
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../regcheck/CliAtRegisterCheck.h"
#include "CliAtRegisterHashTableInternal.h"
#include "../entry/CliAtRegisterEntryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
RegisterHashTableIterator RegisterHashTableIteratorCreate(RegisterHashTable hashTable)
    {
    RegisterHashTableIterator iterator;
    if (hashTable == NULL)
        return NULL;

    iterator = AtOsalMemAlloc(sizeof(tRegisterHashTableIterator));
    AtOsalMemInit(iterator, 0, sizeof(tRegisterHashTableIterator));
    iterator->hashTable = hashTable;
    return iterator;
    }

void RegisterHashTableIteratorDelete(RegisterHashTableIterator iterator)
    {
    if (iterator == NULL)
        return;

    AtOsalMemFree(iterator);
    }

RegisterEntry RegisterHashTableIteratorNext(RegisterHashTableIterator iterator)
    {
    if (iterator == NULL)
        return NULL;

    if (iterator->currentEntry)
        iterator->currentEntry = iterator->currentEntry->next;

    while (iterator->currentEntry == NULL)
        {
        if (iterator->index == iterator->hashTable->size - 1)
            return NULL;

        iterator->currentEntry = iterator->hashTable->regBucket[iterator->index];
        iterator->index = iterator->index + 1;
        }

    return iterator->currentEntry;
    }

RegisterHashTableIterator RegisterHashTableIteratorRestart(RegisterHashTableIterator iterator)
    {
    if (iterator)
        {
        iterator->index = 0;
        iterator->currentEntry = NULL;
        }

    return iterator;
    }

RegisterHashTable RegisterHashTableCreate(uint32 size, uint32 maxCollision)
    {
    RegisterHashTable regHashTable = AtOsalMemAlloc(sizeof(tRegisterHashTable));
    AtOsalMemInit(regHashTable, 0, sizeof(tRegisterHashTable));

    regHashTable->regBucket = AtOsalMemAlloc(size * sizeof(RegisterEntry));
    AtOsalMemInit(regHashTable->regBucket, 0 , size * sizeof(RegisterEntry));
    regHashTable->maxCollisionPerHash = maxCollision;
    regHashTable->size = size;

    return regHashTable;
    }

static void RegisterFlush(RegisterHashTable hashTable)
    {
    RegisterEntry entry = NULL;
    RegisterEntry prev = NULL;
    uint32 i;

    if (hashTable->regBucket == NULL)
        return;

    for (i = 0; i < hashTable->size; i++)
        {
        entry = hashTable->regBucket[i];
        while (entry)
            {
            prev = entry;
            entry = entry->next;
            AtOsalMemFree(prev);
            }

        hashTable->regBucket[i] = NULL;
        }
    }

void RegisterHashTableDelete(RegisterHashTable hashTable)
    {
    if (hashTable->regBucket)
        {
        RegisterFlush(hashTable);

        /* Delete bucket */
        AtOsalMemFree(hashTable->regBucket);
        hashTable->regBucket = NULL;
        }

    AtOsalMemFree(hashTable);
    }

uint32 RegisterHashTableIndexOfAddress(RegisterHashTable hashTable, uint32 address)
    {
    return (uint32) (address % hashTable->size);
    }

RegisterEntry RegisterHashTableRegisterGet(RegisterHashTable hashTable, uint32 address)
    {
    uint32 index = 0;
    RegisterEntry entry;

    /* Calculate hash value and buffer index */
    index = RegisterHashTableIndexOfAddress(hashTable, address);
    if (hashTable->regBucket[index] == NULL)
        return NULL;

    entry = hashTable->regBucket[index];
    while (entry)
        {
        /* Update value */
        if (entry->address == address)
            return entry;

        entry = entry->next;
        }

    return NULL;
    }

eBool RegisterHashTableRegisterAdd(RegisterHashTable hashTable, RegisterEntry regEntry)
    {
    uint32 index = 0;

    /* Calculate hash value and buffer index */
    index = RegisterHashTableIndexOfAddress(hashTable, regEntry->address);
    if (hashTable->regBucket[index] == NULL)
        hashTable->regBucket[index] = regEntry;

    else
        {
        uint32 count = 0;
        RegisterEntry prev = NULL;
        RegisterEntry entry = hashTable->regBucket[index];
        while (entry)
            {
            /* Update value */
            if (entry->address == regEntry->address)
                return cAtFalse;

            prev = entry;
            entry = entry->next;
            count++; /* Next collision */
            }

        /* Do not insert if max collision is exceeded */
        if ((hashTable->maxCollisionPerHash != 0)  && (count >= hashTable->maxCollisionPerHash))
            return cAtFalse;

        prev->next = regEntry;
        }

    return cAtTrue;
    }

void RegisterHashTableRegisterRemove(RegisterHashTable hashTable, uint32 address)
    {
    uint32 index = 0;
    eBool isFound = cAtFalse;
    RegisterEntry prev = NULL;
    RegisterEntry entry;

    /* Calculate hash value and buffer index */
    index = RegisterHashTableIndexOfAddress(hashTable, address);
    if (hashTable->regBucket[index] == NULL)
        return;

    entry = hashTable->regBucket[index];
    while (entry)
        {
        if (entry->address == address)
            {
            isFound = cAtTrue;
            break;
            }

        prev  = entry;
        entry = entry->next;
        }

    if (isFound == cAtFalse)
        return;

    /* Remove First */
    if (entry == hashTable->regBucket[index])
        {
        hashTable->regBucket[index] = entry->next;
        AtOsalMemFree(entry);
        return;
        }

    /* Remove Last */
    if (entry->next == NULL)
        {
        AtOsalMemFree(entry);
        prev->next = NULL;
        return;
        }

    /* Remove Midle, adjust ref next */
    if ((prev->next) && (entry->next))
        {
        prev->next = entry->next;
        AtOsalMemFree(entry);
        return ;
        }
    }

void RegisterHashTableRegisterShow(RegisterHashTable hashTable)
    {
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);
    RegisterEntry entry;

    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        RegisterEntryShow(entry);

    RegisterHashTableIteratorDelete(iterator);
    }

void RegisterHashTableRegisterFlush(RegisterHashTable hashTable)
    {
    RegisterFlush(hashTable);
    }


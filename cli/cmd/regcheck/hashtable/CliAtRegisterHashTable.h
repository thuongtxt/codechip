/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtRegisterHashTable.h
 * 
 * Created Date: Oct 6, 2015
 *
 * Description : Hash table public utility
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_REGCHECK_HASHTABLE_CLIATREGISTERHASHTABLE_H_
#define _CLI_CMD_REGCHECK_HASHTABLE_CLIATREGISTERHASHTABLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../entry/CliAtRegisterEntry.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tRegisterHashTable  * RegisterHashTable;
typedef struct tRegisterHashTableIterator * RegisterHashTableIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Hash table */
RegisterHashTable RegisterHashTableCreate(uint32 size, uint32 maxCollision);
void RegisterHashTableDelete(RegisterHashTable hashTable);
uint32 RegisterHashTableIndexOfAddress(RegisterHashTable hashTable, uint32 address);
RegisterEntry RegisterHashTableRegisterGet(RegisterHashTable hashTable, uint32 address);
void RegisterHashTableRegisterRemove(RegisterHashTable hashTable, uint32 address);
eBool RegisterHashTableRegisterAdd(RegisterHashTable hashTable, RegisterEntry regEntry);
void RegisterHashTableRegisterShow(RegisterHashTable hashTable);
void RegisterHashTableRegisterFlush(RegisterHashTable hashTable);

/* Hash table iterator */
RegisterHashTableIterator RegisterHashTableIteratorCreate(RegisterHashTable hashTable);
void RegisterHashTableIteratorDelete(RegisterHashTableIterator iterator);
RegisterEntry RegisterHashTableIteratorNext(RegisterHashTableIterator iterator);
RegisterHashTableIterator RegisterHashTableIteratorRestart(RegisterHashTableIterator iterator);

#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_REGCHECK_HASHTABLE_CLIATREGISTERHASHTABLE_H_ */


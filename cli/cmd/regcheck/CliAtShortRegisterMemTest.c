/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtShortRegisterMemTest.c
 *
 * Created Date: Oct 1, 2015
 *
 * Description : Short register memory test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtRegisterCheck.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int DataBusTestOnReg(AtHal hal, ShortRegisterEntry entry)
    {
    uint32 pattern = cBit0;
    uint32 readValue;
    uint32 mask = ShortRegisterEntryMaskGet(entry);
    uint32 address = RegisterEntryAddressGet((RegisterEntry)entry);
    uint8 bitPosition;

    /* Perform a walking 1's test at the given address. */
    for (bitPosition = 0; bitPosition < 32; bitPosition++)
        {
        pattern <<= bitPosition;
        if ((pattern & mask) == 0)
            continue;

        /* Write the test pattern. */
        AtHalWrite(hal, address, pattern);

        /* Read it back (immediately is okay for this test). */
        readValue = AtHalRead(hal, address);
        if (readValue != pattern)
            return -1;
        }

    return (0);
    }

static int DataBusTest(RegistersDb regDb, AtHal hal)
    {
    RegisterEntry entry = NULL;
    RegisterHashTable hashTable = regDb->shortRegs;
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);
    int ret;

    if (iterator == NULL)
        return -1;

    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        ret = DataBusTestOnReg(hal, (ShortRegisterEntry)entry);
        if (ret != 0)
            return ret;
        }

    RegisterHashTableIteratorDelete(iterator);
    return 0;
    }

static uint32 TestPattern(void)
    {
    return 0xAAAAAAAA;
    }

static uint32 TestAntiPattern(void)
    {
    return 0x55555555;
    }

static void FillPattern(RegisterHashTableIterator iterator, AtHal hal, uint32 pattern)
    {
    RegisterEntry entry;
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        AtHalWrite(hal, RegisterEntryAddressGet(entry), pattern);
        }
    }

static int AddressBusTest(RegistersDb regDb, AtHal hal)
    {
    uint32 pattern     = TestPattern();
    uint32 antipattern = TestAntiPattern();
    uint32 readValue, mask;
    uint32 testAddress, address;

    RegisterEntry testEntry, entry;
    RegisterHashTable hashTable = regDb->shortRegs;
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);
    RegisterHashTableIterator iterator2 = RegisterHashTableIteratorCreate(hashTable);

    if (RegisterHashTableIteratorNext(iterator) == NULL)
        {
        RegisterHashTableIteratorDelete(iterator);
        RegisterHashTableIteratorDelete(iterator2);
        return -1;
        }

    /* Write default pattern */
    RegisterHashTableIteratorRestart(iterator);
    FillPattern(iterator, hal, pattern);

    /* Check for address bits stuck high. */
    RegisterHashTableIteratorRestart(iterator);
    testEntry = RegisterHashTableIteratorNext(iterator);
    testAddress = RegisterEntryAddressGet(testEntry);
    AtHalWrite(hal, testAddress, antipattern);
    RegisterHashTableIteratorRestart(iterator);
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        if (testEntry != entry)
            {
            address = RegisterEntryAddressGet(entry);
            readValue = AtHalRead(hal, address);
            mask = ShortRegisterEntryMaskGet((ShortRegisterEntry)entry);

            if ((readValue & mask) != (pattern & mask))
                return -1;
            }
        }

    AtHalWrite(hal, testAddress, pattern);

    RegisterHashTableIteratorRestart(iterator);
    while ((testEntry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        testAddress = RegisterEntryAddressGet(testEntry);
        AtHalWrite(hal, testAddress, antipattern);

        while ((entry = RegisterHashTableIteratorNext(iterator2)) != NULL)
            {
            if (testEntry != entry)
                {
                address = RegisterEntryAddressGet(entry);
                readValue = AtHalRead(hal, address);
                mask = ShortRegisterEntryMaskGet((ShortRegisterEntry)entry);
                if ((readValue & mask) != (pattern & mask))
                    return -1;
                }
            }

        AtHalWrite(hal, testAddress, pattern);
        }

    RegisterHashTableIteratorDelete(iterator);
    RegisterHashTableIteratorDelete(iterator2);
    return 0;
    }

static int DeviceTest(RegistersDb regDb, AtHal hal)
    {
    uint32 pattern = 1;
    uint32 antipattern;
    uint32 readValue;
    uint32 mask, address;
    RegisterEntry entry;
    RegisterHashTable hashTable = regDb->shortRegs;
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);

    /* Fill memory with a known pattern. */
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        AtHalWrite(hal, RegisterEntryAddressGet(entry), pattern);
        pattern++;
        }

    /* Check each location and invert it for the second pass. */
    pattern = 1;
    RegisterHashTableIteratorRestart(iterator);
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        address = RegisterEntryAddressGet(entry);
        readValue = AtHalRead(hal, address);
        mask = ShortRegisterEntryMaskGet((ShortRegisterEntry)entry);
        if ((readValue & mask) != (pattern & mask))
            return -1;

        antipattern = (~pattern) & mask;
        AtHalWrite(hal, address, antipattern);
        pattern++;
        }

    /* Check each location for the inverted pattern and zero it. */
    pattern = 1;
    RegisterHashTableIteratorRestart(iterator);
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        address = RegisterEntryAddressGet(entry);
        mask = ShortRegisterEntryMaskGet((ShortRegisterEntry)entry);
        antipattern = (~pattern) & mask;
        readValue = AtHalRead(hal, address);

        if ((readValue & mask) != (antipattern & mask))
            return -1;

        pattern++;
        }

    RegisterHashTableIteratorDelete(iterator);
    return 0;
    }

eBool ShortRegisterMemTest(RegistersDb regDb)
    {
    int ret = 0;
    AtHal hal;

    if (regDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot memtest, register checking may be stopped\r\n");
        return cAtFalse;
        }

    hal  = AtDeviceIpCoreHalGet(regDb->device, 0);
    ret |= DataBusTest(regDb, hal);
    ret |= AddressBusTest(regDb, hal);
    ret |= DeviceTest(regDb, hal);

    return (ret < 0) ? cAtFalse : cAtTrue;
    }

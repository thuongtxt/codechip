/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtRegisterCheck.h
 * 
 * Created Date: Sep 30, 2015
 *
 * Description : Register definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_REGISTER_CLIATREGISTERCHECK_H_
#define _CLI_CMD_REGISTER_CLIATREGISTERCHECK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "hashtable/CliAtRegisterHashTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cRegisterHashTableSize (cBit18_0 + 1)
#define cRegisterMaxCollisionPerHash 10

#define cValueIsValid          cBit0
#define cVerbose               cBit1
#define cAutoAddLong           cBit2
#define cAutoAddShort          cBit3
#define cLogError              cBit4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tRegistersDb * RegistersDb;
typedef struct tRegistersDb
    {
    RegisterHashTable shortRegs;
    RegisterHashTable longRegs;

    AtDevice device;
    uint8 operation;
    AtLogger logger;
    } tRegistersDb;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliRegisterCheckDestroy(void);
eBool CliRegisterCheckStart(void);

/* Memory test */
eBool ShortRegisterMemTest(RegistersDb regDb);
eBool LongRegisterMemTest(RegistersDb regDb);

/* Utils */
eBool LongRegisterValueChanged(LongRegisterEntry reg, const uint32 *dataBuffer, uint16 length);
const char* LongRegValsCompareString(const uint32 *dataBuffer1, const uint32 *dataBuffer2, uint16 length);

#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_REGISTER_CLIATREGISTERCHECK_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Register
 *
 * File        : CliAtRegisterCheck.c
 *
 * Created Date: Sep 17, 2015
 *
 * Description : N/A
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtRegisterCheck.h"
#include "entry/CliAtRegisterEntry.h"
#include "../../../driver/src/generic/man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtRegisterWriteEventListener registerListener;
static tAtHalListener halListener;
static RegistersDb m_RegisterDb = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Log(RegistersDb self, eAtLogLevel level, const char *format, ...)
    {
    va_list args;

    if (self == NULL)
        return;

    va_start(args, format);
    AtLoggerLog(self->logger, level, format, args);
    va_end(args);
    }

static RegistersDb RegistersDbCreate(AtDevice device)
    {
    RegistersDb self  = (RegistersDb ) AtOsalMemAlloc(sizeof(tRegistersDb));
    if (self == NULL)
        return NULL;

    AtOsalMemInit(self, 0, sizeof(tRegistersDb));
    self->shortRegs = RegisterHashTableCreate(cRegisterHashTableSize, cRegisterMaxCollisionPerHash);
    self->longRegs  = RegisterHashTableCreate(cRegisterHashTableSize, cRegisterMaxCollisionPerHash);
    self->device    = device;
    self->operation |= cLogError;

    return self;
    }

static eBool LogError(RegistersDb self)
    {
    return (self->operation & cLogError) ? cAtTrue : cAtFalse;
    }

static void ShortRegErrorLog(RegistersDb self, ShortRegisterEntry shortReg, uint32 realVal)
    {
    if (LogError(self))
        Log(self, cAtLogLevelCritical, "%s", ShortRegisterErrorString(shortReg, realVal));
    else
        AtPrintc(cSevCritical, "%s", ShortRegisterErrorString(shortReg, realVal));
    }

static void ShortReRead(ShortRegisterEntry entry)
    {
    uint32 address = RegisterEntryAddressGet((RegisterEntry)entry);
    AtHal hal      = AtDeviceIpCoreHalGet(m_RegisterDb->device, 0);
    AtHalRead(hal, address);
    }

static eBool ShortRegisterEntryAdd(RegistersDb self, uint32 address, uint32 writableMask)
    {
    RegisterEntry newEntry = (RegisterEntry)ShortRegisterEntryNew(address, writableMask);
    if (!RegisterHashTableRegisterAdd(self->shortRegs, newEntry))
        {
        AtOsalMemFree(newEntry);
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ShortRegisterEntryAutoAdd(RegistersDb self, uint32 address, uint32 value, uint32 writableMask)
    {
    RegisterEntry newEntry = ShortRegisterEntryNew(address, writableMask);
    ShortRegisterEntryValueUpdate((ShortRegisterEntry)newEntry, value);

    if (!RegisterHashTableRegisterAdd(self->shortRegs, newEntry))
        {
        AtOsalMemFree(newEntry);
        return cAtFalse;
        }

    return cAtTrue;
    }

static  void ShortRegisterEntryRemove(RegistersDb  self, uint32 address)
    {
    RegisterHashTableRegisterRemove(self->shortRegs, address);
    }

static ShortRegisterEntry ShortRegisterGet(RegistersDb self, uint32 address)
    {
    return (ShortRegisterEntry)RegisterHashTableRegisterGet(self->shortRegs, address);
    }

static ShortRegisterEntry ShortRegisterValueUpdate(RegistersDb self, uint32 address, uint32 value)
    {
    ShortRegisterEntry reg = ShortRegisterGet(self, address);
    return ShortRegisterEntryValueUpdate(reg, value);
    }

static eBool IsAutoAddLong(RegistersDb self)
    {
    return (self->operation & cAutoAddLong) ? cAtTrue : cAtFalse;
    }

static eBool IsAutoAddShort(RegistersDb self)
    {
    return (self->operation & cAutoAddShort) ? cAtTrue : cAtFalse;
    }

static void DidWriteRegister(AtHal self, void *listener, uint32 address, uint32 value)
    {
    ShortRegisterEntry reg;

    AtUnused(self);
    AtUnused(listener);
    if (m_RegisterDb == NULL)
        return;

    reg = ShortRegisterValueUpdate(m_RegisterDb, address, value);
    if (reg)
        {
        ShortReRead(reg);
        return;
        }

    if (IsAutoAddShort(m_RegisterDb))
        ShortRegisterEntryAutoAdd(m_RegisterDb, address, value, cBit31_0);
    }

static void DidReadRegister(AtHal self, void *listener, uint32 address, uint32 value)
    {
    ShortRegisterEntry reg;
    AtUnused(self);
    AtUnused(listener);

    if (m_RegisterDb == NULL)
        return;

    reg = ShortRegisterGet(m_RegisterDb, address);
    if (reg == NULL)
        return;

    if (ShortRegisterEntryValueChanged(reg, value))
        ShortRegErrorLog(m_RegisterDb, reg, value);
    }

static eBool LongRegisterEntryAdd(RegistersDb self, uint32 address, uint32* writableMask)
    {
    RegisterEntry newEntry = LongRegisterEntryNew(address, writableMask);
    if (!RegisterHashTableRegisterAdd(self->longRegs, newEntry))
        {
        AtOsalMemFree(newEntry);
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool LongRegisterEntryAutoAdd(RegistersDb self, uint32 address, const uint32 *dataBuffer, uint16 length, uint32* writableMask)
    {
    RegisterEntry newEntry = LongRegisterEntryNew(address, writableMask);
    LongRegisterEntryValueUpdate((LongRegisterEntry)newEntry, dataBuffer, (uint8)length);

    if (newEntry == NULL)
        return cAtFalse;

    if (!RegisterHashTableRegisterAdd(self->longRegs, newEntry))
        {
        AtOsalMemFree(newEntry);
        return cAtFalse;
        }

    return cAtTrue;
    }

static void LongRegisterEntryRemove(RegistersDb  self, uint32 address)
    {
    RegisterHashTableRegisterRemove(self->longRegs, address);
    }

static LongRegisterEntry LongRegisterGet(RegistersDb self, uint32 address)
    {
    return (LongRegisterEntry)RegisterHashTableRegisterGet(self->longRegs, address);
    }

static LongRegisterEntry LongRegisterValueUpdate(RegistersDb self, uint32 address, const uint32* value, uint16 numDw)
    {
    LongRegisterEntry longEntry = LongRegisterGet(self, address);
    return LongRegisterEntryValueUpdate(longEntry, value, numDw);
    }

static void LongRegErrorLog(RegistersDb self, LongRegisterEntry reg, const uint32 *dataBuffer, uint16 length)
    {
    if (LogError(self))
        Log(self, cAtLogLevelCritical, "%s", LongRegisterErrorString(reg, dataBuffer, length));
    else
        AtPrintc(cSevCritical, "%s", LongRegisterErrorString(reg, dataBuffer, length));
    }

static void LongRead(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId)
    {
    LongRegisterEntry reg;
    AtUnused(self);
    AtUnused(coreId);
    AtUnused(moduleId);

    if (m_RegisterDb == NULL)
        return;

    reg = LongRegisterGet(m_RegisterDb, address);
    if (reg == NULL)
        return;

    if (LongRegisterEntryValueChanged(reg, dataBuffer, length))
        LongRegErrorLog(m_RegisterDb, reg, dataBuffer, length);
    }

static void LongReRead(LongRegisterEntry entry)
    {
    uint32 address = RegisterEntryAddressGet((RegisterEntry)entry);
    uint32 dataBuffer[cLongRegMaxSize];

    AtDeviceLongReadOnCore(m_RegisterDb->device, address, dataBuffer, cLongRegMaxSize, 0);
    }

static uint32* LongFullMask(uint32 numHoldReg)
    {
    uint16 dw_i, numDw;
    static uint32* pMask = NULL;
    static uint32 masks[cLongRegMaxSize];

    if (pMask)
        return pMask;

    numDw = (uint16)((numHoldReg < cLongRegMaxSize) ? numHoldReg : cLongRegMaxSize);
    for (dw_i = 0; dw_i < numDw; dw_i++)
        masks[dw_i] = cBit31_0;

    pMask = masks;
    return pMask;
    }

static void LongWrite(AtDevice device, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId)
    {
    LongRegisterEntry reg;
    AtUnused(coreId);

    if (m_RegisterDb == NULL)
        return;

    reg = LongRegisterValueUpdate(m_RegisterDb, address, dataBuffer, length);
    if (reg)
        {
        LongReRead(reg);
        return;
        }

    if (IsAutoAddLong(m_RegisterDb))
        {
        uint16 numHoldReg;
        AtModuleHoldRegistersGet(AtDeviceModuleGet(device, moduleId), &numHoldReg);
        LongRegisterEntryAutoAdd(m_RegisterDb, address, dataBuffer, length, LongFullMask(numHoldReg));
        }
    }

static AtHal Hal(void)
    {
    return AtDeviceIpCoreHalGet(CliDevice(), 0);
    }

static eBool CreateLogger(RegistersDb db)
    {
    static const uint32 cMaxNumMessages   = 1024;
    static const uint32 cMaxMessageLength = 512;

    AtLogger logger = AtDefaultLoggerNew(cMaxNumMessages, cMaxMessageLength);
    if (logger == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create logger\r\n");
        return cAtFalse;
        }

    db->logger = logger;
    return cAtTrue;
    }

eBool CliRegisterCheckStart(void)
    {
    /* Create database */
    if (m_RegisterDb)
        {
        AtPrintc(cSevInfo, "Register checking has been started.\r\n");
        return cAtTrue;
        }

    m_RegisterDb = RegistersDbCreate(CliDevice());
    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot start register checking because can not initialize database.\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&halListener, 0, sizeof(halListener));
    halListener.DidWriteRegister = DidWriteRegister;
    halListener.DidReadRegister  = DidReadRegister;
    AtHalListenerAdd(Hal(), NULL, &halListener);

    AtOsalMemInit(&registerListener, 0, sizeof(registerListener));
    registerListener.LongWrite  = LongWrite;
    registerListener.LongRead   = LongRead;

    if (AtDeviceRegisterEventListenerAdd(CliDevice(), &registerListener) != cAtOk)
        {
        AtPrintc(cSevCritical, " Can not add register checking listener \r\n");
        CliRegisterCheckDestroy();
        return cAtFalse;
        }

    if (CreateLogger(m_RegisterDb) != cAtTrue)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ShortRegScan(void)
    {
    RegisterEntry entry = NULL;
    RegisterHashTable hashTable;
    RegisterHashTableIterator iterator;

    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot scan, register checking may be stopped\r\n");
        return cAtFalse;
        }

    hashTable = m_RegisterDb->shortRegs;
    iterator = RegisterHashTableIteratorCreate(hashTable);
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        ShortReRead((ShortRegisterEntry)entry);

    RegisterHashTableIteratorDelete(iterator);
    return cAtTrue;
    }

static eBool LongRegScan(void)
    {
    RegisterEntry entry = NULL;
    RegisterHashTable hashTable;
    RegisterHashTableIterator iterator;

    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot scan, register checking may be stopped\r\n");
        return cAtFalse;
        }

    hashTable = m_RegisterDb->longRegs;
    iterator = RegisterHashTableIteratorCreate(hashTable);
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        LongReRead((LongRegisterEntry)entry);

    RegisterHashTableIteratorDelete(iterator);
    return cAtTrue;
    }

static void RegistersDbDelete(void)
    {
    RegisterHashTableDelete(m_RegisterDb->shortRegs);
    m_RegisterDb->shortRegs = NULL;
    RegisterHashTableDelete(m_RegisterDb->longRegs);
    m_RegisterDb->longRegs  = NULL;
    AtObjectDelete((AtObject)m_RegisterDb->logger);
    AtOsalMemFree(m_RegisterDb);
    m_RegisterDb = NULL;
    }

static eBool VerboseModeEnable(eBool enable)
    {
    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot set verbose mode, register checking may be stopped\r\n");
        return cAtFalse;
        }

    if (enable)
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation | cVerbose);
    else
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation & ~cVerbose);

    return cAtTrue;
    }

static eBool AutoAddLongModeEnable(eBool enable)
    {
    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot set auto mode, register checking may be stopped\r\n");
        return cAtFalse;
        }

    if (enable)
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation | cAutoAddLong);
    else
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation & ~cAutoAddLong);

    return cAtTrue;
    }

static eBool AutoAddShortModeEnable(eBool enable)
    {
    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Cannot set auto mode, register checking may be stopped\r\n");
        return cAtFalse;
        }

    if (enable)
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation | cAutoAddShort);
    else
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation & ~cAutoAddShort);

    return cAtTrue;
    }

static void LogErrorEnable(eBool enable)
    {
    if (enable)
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation | cLogError);
    else
        m_RegisterDb->operation = (uint8)(m_RegisterDb->operation & ~cLogError);
    }

eBool CliRegisterCheckDestroy(void)
    {
    if (m_RegisterDb == NULL)
        return cAtTrue;

    AtHalListenerRemove(Hal(), NULL, &halListener);
    AtDeviceRegisterEventListenerRemove(CliDevice());
    RegistersDbDelete();

    return cAtTrue;
    }

eBool CmdRegisterCheckStart(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return CliRegisterCheckStart();
    }

eBool CmdRegisterCheckStop(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return CliRegisterCheckDestroy();
    }

eBool CmdRegisterCheckAdd(char argc, char **argv)
    {
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 writableMask[cLongRegMaxSize], numMask;
    uint32 addressTemp;
    uint32 counter_i;
    eBool isLongReg = cAtFalse;
    eBool ret;

    AtOsalMemInit(writableMask, 0, sizeof(writableMask));
    numMask = 1;

    if (m_RegisterDb == NULL)
       {
       AtPrintc(cSevCritical, "Cannot add, register checking may be stopped\r\n");
       return cAtFalse;
       }

    if (argc > 1)
        {
        if (AtStrcmp(argv[1], "fullshort") == 0)
            {
            writableMask[0] = cBit31_0;
            numMask = 1;
            }

        else if (AtStrcmp(argv[1], "fulllong") == 0)
            {
            AtOsalMemInit(writableMask, cBit7_0, sizeof(writableMask));
            numMask = cLongRegMaxSize;
            }

        else if (!AtStrToArrayDw(argv[1], writableMask, &numMask))
            {
            AtPrintc(cSevCritical, "Write table mask maybe wrong\r\n");
            return cAtFalse;
            }
        }

    if (argc > 2)
        numbers = AtStrtoul(argv[2], NULL, 10);

    if (argc > 3)
        step = AtStrtoul(argv[3], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    isLongReg = (numMask > 1) ? cAtTrue : cAtFalse;
    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        if (isLongReg)
            ret = LongRegisterEntryAdd(m_RegisterDb, addressTemp, writableMask);
        else
            ret = ShortRegisterEntryAdd(m_RegisterDb, addressTemp, writableMask[0]);

        if (!ret)
            {
            AtPrintc(cSevCritical, "Can not add  address 0x%08x to checking-list\r\n", addressTemp);
            return cAtFalse;
            }
        }

    return ret;
    }

eBool CmdRegisterCheckRemove(char argc, char **argv)
    {
    uint32 numbers = 1;
    uint32 step = 1;
    uint32 address = AtStrtoul(argv[0], NULL, 16);
    uint32 addressTemp;
    uint32 counter_i;

    if (m_RegisterDb == NULL)
       {
       AtPrintc(cSevCritical, "Cannot remove, register checking may be stopped\r\n");
       return cAtFalse;
       }

    if (argc > 1)
        numbers = AtStrtoul(argv[1], NULL, 10);

    if (argc > 2)
        step = AtStrtoul(argv[2], NULL, 10);

    if (numbers == 0)
        {
        AtPrintc(cSevCritical, " numbers must be not 0!! \r\n");
        return cAtFalse;
        }

    if (step == 0)
        {
        AtPrintc(cSevCritical, " step must be not 0!! \r\n");
        return cAtFalse;
        }

    for (counter_i = 0; counter_i < numbers; counter_i++)
        {
        addressTemp = address + (counter_i * step);
        ShortRegisterEntryRemove(m_RegisterDb, addressTemp);
        LongRegisterEntryRemove(m_RegisterDb, addressTemp);
        }

    return cAtTrue;
    }

eBool CmdRegisterCheckScan(char argc, char **argv)
    {
    eBool ret;

    AtUnused(argc);
    AtUnused(argv);
    ret = ShortRegScan();
    ret |= LongRegScan();

    return ret;
    }

eBool CmdRegisterCheckVerboseEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return VerboseModeEnable(cAtTrue);
    }

eBool CmdRegisterCheckVerboseDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return VerboseModeEnable(cAtFalse);
    }

eBool CmdRegisterCheckAutoModeEnable(char argc, char **argv)
    {
    if (argc > 0)
        {
        if (AtStrcmp(argv[0], "long") == 0)
            return AutoAddLongModeEnable(cAtTrue);

        if (AtStrcmp(argv[0], "short") == 0)
            return AutoAddShortModeEnable(cAtTrue);

        else
            {
            AtPrintc(cSevCritical, "Invalid register type, expect <long|short> \r\n");
            return cAtFalse;
            }
        }

    AutoAddLongModeEnable(cAtTrue);
    AutoAddShortModeEnable(cAtTrue);
    return cAtTrue;
    }

eBool CmdRegisterCheckAutoModeDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AutoAddLongModeEnable(cAtFalse);
    AutoAddShortModeEnable(cAtFalse);
    return cAtTrue;
    }

eBool CmdRegisterCheckMemTest(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    LogErrorEnable(cAtFalse);
    ShortRegisterMemTest(m_RegisterDb);
    LongRegisterMemTest(m_RegisterDb);
    LogErrorEnable(cAtTrue);

    return cAtTrue;
    }

eBool CmdRegisterCheckShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Register checking is stopped\r\n");
        return cAtTrue;
        }

    RegisterHashTableRegisterShow(m_RegisterDb->shortRegs);
    RegisterHashTableRegisterShow(m_RegisterDb->longRegs);
    return cAtTrue;
    }

eBool CmdRegisterCheckFlush(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Register checking is stopped, nothing to flush\r\n");
        return cAtTrue;
        }

    RegisterHashTableRegisterFlush(m_RegisterDb->shortRegs);
    RegisterHashTableRegisterFlush(m_RegisterDb->longRegs);

    return cAtTrue;
    }

eBool CmdRegisterCheckShowLogger(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Register checking is stopped, nothing to show\r\n");
        return cAtTrue;
        }

    AtLoggerShow(m_RegisterDb->logger);
    return cAtTrue;
    }

eBool CmdRegisterCheckLoggerFlush(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (m_RegisterDb == NULL)
        {
        AtPrintc(cSevCritical, "Register checking is stopped, nothing to show\r\n");
        return cAtTrue;
        }

    AtLoggerFlush(m_RegisterDb->logger);
    return cAtTrue;
    }


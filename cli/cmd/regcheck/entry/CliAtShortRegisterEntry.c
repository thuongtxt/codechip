/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtShortRegisterEntry.c
 *
 * Created Date: Oct 6, 2015
 *
 * Description : Register Entry implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "CliAtRegisterEntryInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ShortRegisterEntry)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tRegisterEntryMethods m_RegisterEntryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Show(RegisterEntry self)
    {
    AtPrintc(cSevNormal, "Address 0x%08X, Mask = 0x%08X, Value = 0x%08X\r\n", self->address, mThis(self)->mask, mThis(self)->value);
    }

static void OverrideRegisterEntry(RegisterEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_RegisterEntryOverride, self->methods, sizeof(m_RegisterEntryOverride));
        m_RegisterEntryOverride.Show = Show;
        }

    self->methods = &m_RegisterEntryOverride;
    }

static RegisterEntry ObjectInit(RegisterEntry self, uint32 address, uint32 writableMask)
    {
    AtOsalMemInit(self, 0, sizeof(tShortRegisterEntry));
    RegisterEntryObjectInit(self, address);

    /* Override */
    OverrideRegisterEntry(self);
    mThis(self)->mask = writableMask;

    return self;
    }

RegisterEntry ShortRegisterEntryNew(uint32 address, uint32 writableMask)
    {
    RegisterEntry entry = AtOsalMemAlloc(sizeof(tShortRegisterEntry));
    if (entry == NULL)
        return NULL;

    return ObjectInit(entry, address, writableMask);
    }

ShortRegisterEntry ShortRegisterEntryValueUpdate(ShortRegisterEntry self, uint32 value)
    {
    if (self == NULL)
        return NULL;

    self->value = (value & self->mask);
    ((RegisterEntry)self)->valueIsValid = 1;

    return self;
    }

eBool ShortRegisterEntryValueChanged(ShortRegisterEntry self, uint32 newValue)
    {
    if (!((RegisterEntry)self)->valueIsValid)
        return cAtFalse;

    return (self->value != newValue) ? cAtTrue : cAtFalse;
    }

const char* ShortRegisterErrorString(ShortRegisterEntry self, uint32 newValue)
    {
    static char errorBuffer[256];
    AtSprintf(errorBuffer, "Address 0x%08x wrote 0x%08x vs read 0x%08x (mask: 0x%08x)\r\n",
              RegisterEntryAddressGet((RegisterEntry)self), self->value, newValue, self->mask);

    errorBuffer[253] = '\r';
    errorBuffer[254] = '\n';
    errorBuffer[255] = '\0';

    return errorBuffer;
    }

uint32 ShortRegisterEntryMaskGet(ShortRegisterEntry self)
    {
    if (self)
        return self->mask;

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtLongRegisterEntry.c
 *
 * Created Date: Oct 6, 2015
 *
 * Description : Long register entry implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtRegisterEntryInternal.h"
#include "AtDebug.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((LongRegisterEntry)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tRegisterEntryMethods m_RegisterEntryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* DataBuffer2String(const uint32 *dataBuffer, uint16 length)
    {
    return AtDebugLongRegValue2String(dataBuffer, (uint16)((length > cLongRegMaxSize) ? cLongRegMaxSize : length));
    }

static void Show(RegisterEntry self)
    {
    AtPrintc(cSevNormal, "Address 0x%08X, ", self->address);
    AtPrintc(cSevNormal, "Mask = %s, ", DataBuffer2String(mThis(self)->mask, cLongRegMaxSize));
    AtPrintc(cSevNormal, "Value = %s\r\n", DataBuffer2String(mThis(self)->value, cLongRegMaxSize));
    }

static void OverrideRegisterEntry(RegisterEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_RegisterEntryOverride, self->methods, sizeof(m_RegisterEntryOverride));
        m_RegisterEntryOverride.Show = Show;
        }

    self->methods = &m_RegisterEntryOverride;
    }

static const char* LongRegValsCompareString(const uint32 *dataBuffer1, const uint32 *dataBuffer2, uint16 length)
    {
    static char longRegValueCompareStr[256];
    AtOsalMemInit(longRegValueCompareStr, 0, sizeof(longRegValueCompareStr));
    AtSprintf(longRegValueCompareStr, "wrote ");
    AtStrcat(longRegValueCompareStr, DataBuffer2String(dataBuffer1, length));
    AtStrcat(longRegValueCompareStr, " vs read ");
    AtStrcat(longRegValueCompareStr, DataBuffer2String(dataBuffer2, length));
    longRegValueCompareStr[255] = '\0';
    return longRegValueCompareStr;
    }

static RegisterEntry ObjectInit(RegisterEntry self, uint32 address, uint32* writableMask)
    {
    uint8 dw_i;
    AtOsalMemInit(self, 0, sizeof(tLongRegisterEntry));
    RegisterEntryObjectInit(self, address);

    /* Override */
    OverrideRegisterEntry(self);
    for (dw_i = 0; dw_i < cLongRegMaxSize; dw_i++)
        mThis(self)->mask[dw_i] = writableMask[dw_i];

    return self;
    }

RegisterEntry LongRegisterEntryNew(uint32 address, uint32* writableMask)
    {
    RegisterEntry entry = AtOsalMemAlloc(sizeof(tLongRegisterEntry));
    if (entry == NULL)
        return NULL;

    return ObjectInit(entry, address, writableMask);
    }

LongRegisterEntry LongRegisterEntryValueUpdate(LongRegisterEntry self, const uint32* value, uint16 numDword)
    {
    uint8 dw_i;

    if (self == NULL)
        return NULL;

    numDword = (uint16)((numDword < cLongRegMaxSize) ? numDword : cLongRegMaxSize);
    for (dw_i = 0; dw_i < numDword; dw_i++)
        self->value[dw_i] = (value[dw_i] & self->mask[dw_i]);

    ((RegisterEntry)self)->valueIsValid = 1;
    return self;
    }

eBool LongRegisterEntryValueChanged(LongRegisterEntry self, const uint32 *dataBuffer, uint16 length)
    {
    uint16 dw_i;
    uint16 numCheckDw;;

    if (!((RegisterEntry)self)->valueIsValid)
        return cAtFalse;

    numCheckDw = (uint16)((length < cLongRegMaxSize) ? length : cLongRegMaxSize);
    for (dw_i = 0; dw_i < numCheckDw; dw_i++)
        {
        if (self->value[dw_i] != (dataBuffer[dw_i] & self->mask[dw_i]))
            return cAtTrue;
        }

    return cAtFalse;
    }

const char* LongRegisterErrorString(LongRegisterEntry self, const uint32* newValue, uint16 length)
    {
    static char errorBuffer[256];
    AtSprintf(errorBuffer, "Address 0x%08x %s\r\n", RegisterEntryAddressGet((RegisterEntry)self), LongRegValsCompareString(self->value, newValue, length));

    errorBuffer[253] = '\r';
    errorBuffer[254] = '\n';
    errorBuffer[255] = '\0';

    return errorBuffer;
    }

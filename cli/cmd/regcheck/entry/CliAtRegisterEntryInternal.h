/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtRegisterEntryInternal.h
 * 
 * Created Date: Oct 6, 2015
 *
 * Description : Register entry internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_REGCHECK_ENTRY_CLIATREGISTERENTRYINTERNAL_H_
#define _CLI_CMD_REGCHECK_ENTRY_CLIATREGISTERENTRYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "CliAtRegisterEntry.h"
#include "AtStd.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tRegisterEntryMethods
    {
    void (*Show)(RegisterEntry self);
    }tRegisterEntryMethods;

typedef struct tRegisterEntry
    {
    const tRegisterEntryMethods * methods;

    uint32 address;
    uint8 valueIsValid;
    void * next;
    }tRegisterEntry;

typedef struct tShortRegisterEntry
    {
    tRegisterEntry super;
    uint32 mask;
    uint32 value;
    }tShortRegisterEntry;

typedef struct tLongRegisterEntry
    {
    tRegisterEntry super;
    uint32 mask[cLongRegMaxSize];
    uint32 value[cLongRegMaxSize];
    }tLongRegisterEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
RegisterEntry RegisterEntryObjectInit(RegisterEntry self, uint32 address);


#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_REGCHECK_ENTRY_CLIATREGISTERENTRYINTERNAL_H_ */


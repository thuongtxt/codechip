/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : CliAtRegisterEntry.h
 * 
 * Created Date: Oct 6, 2015
 *
 * Description : Register entry utility
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_REGCHECK_ENTRY_CLIATREGISTERENTRY_H_
#define _CLI_CMD_REGCHECK_ENTRY_CLIATREGISTERENTRY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cLongRegMaxSize 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tRegisterEntry      * RegisterEntry;
typedef struct tShortRegisterEntry * ShortRegisterEntry;
typedef struct tLongRegisterEntry  * LongRegisterEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
RegisterEntry ShortRegisterEntryNew(uint32 address, uint32 writableMask);
ShortRegisterEntry ShortRegisterEntryValueUpdate(ShortRegisterEntry self, uint32 value);
eBool ShortRegisterEntryValueChanged(ShortRegisterEntry self, uint32 newValue);
const char* ShortRegisterErrorString(ShortRegisterEntry self, uint32 newValue);
uint32 ShortRegisterEntryMaskGet(ShortRegisterEntry self);

RegisterEntry LongRegisterEntryNew(uint32 address, uint32* writableMask);
LongRegisterEntry LongRegisterEntryValueUpdate(LongRegisterEntry self, const uint32* value, uint16 numDword);
eBool LongRegisterEntryValueChanged(LongRegisterEntry self, const uint32 *dataBuffer, uint16 length);
const char* LongRegisterErrorString(LongRegisterEntry self, const uint32* newValue, uint16 length);

/* Common */
uint32 RegisterEntryAddressGet(RegisterEntry self);
void RegisterEntryShow(RegisterEntry self);

#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_REGCHECK_ENTRY_CLIATREGISTERENTRY_H_ */


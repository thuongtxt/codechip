/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtRegisterEntry.c
 *
 * Created Date: Oct 6, 2015
 *
 * Description : Register entry implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtRegisterEntryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tRegisterEntryMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Show(RegisterEntry self)
    {
    AtUnused(self);
    }

/* Initialize implementation structure */
static void MethodsInit(RegisterEntry self)
    {
    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        m_methods.Show = Show;
        }

    self->methods = &m_methods;
    }

RegisterEntry RegisterEntryObjectInit(RegisterEntry self, uint32 address)
    {
    if (self == NULL)
        return NULL;

    /* Setup class */
    AtOsalMemInit(self, 0, sizeof(tRegisterEntry));
    MethodsInit(self);
    m_methodsInit = 1;
    self->address = address;

    return self;
    }

void RegisterEntryShow(RegisterEntry self)
    {
    if (self)
        self->methods->Show(self);
    }

uint32 RegisterEntryAddressGet(RegisterEntry self)
    {
    if (self)
        return self->address;

    return 0;
    }

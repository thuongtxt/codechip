/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtLongRegisterMemTest.c
 *
 * Created Date: Oct 1, 2015
 *
 * Description : Long registers memory test
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtRegisterCheck.h"
#include "entry/CliAtRegisterEntry.h"
#include "hashtable/CliAtRegisterHashTable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint16 m_IncrPatternDwIndex = 0;
static uint32 m_IncrPattern[cLongRegMaxSize];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static void PatternReset(uint32* pattern)
    {
    uint8 dw_i;
    pattern[0] = 1;

    for (dw_i = 1; dw_i < cLongRegMaxSize; dw_i++)
        pattern[dw_i] = 0;
    }

static uint32* Walking1Pattern(void)
    {
    static uint8 bitPosition = 0;
    static uint32 walking1Pattern[cLongRegMaxSize];

    if (bitPosition == 0)
        {
        PatternReset(walking1Pattern);
        bitPosition++;
        return walking1Pattern;
        }

    if (bitPosition == cLongRegMaxSize * 32) /* Last bit */
        {
        bitPosition = 0;
        return NULL;
        }

    if ((bitPosition >= 32) && (bitPosition % 32) == 0)
        {
        walking1Pattern[bitPosition / 32 - 1] = 0;
        walking1Pattern[bitPosition / 32] = 1;
        }
    else
        walking1Pattern[bitPosition / 32] = walking1Pattern[bitPosition / 32] << 1;

    bitPosition++;
    return walking1Pattern;
    }

static int DataBusTestOnReg(LongRegisterEntry entry, AtDevice device)
    {
    uint32 *pattern;
    uint32 address = RegisterEntryAddressGet((RegisterEntry)entry);
    uint32 readValue[cLongRegMaxSize];

    while ((pattern = Walking1Pattern()) != NULL)
        {
        AtDeviceLongWriteOnCore(device, address, pattern, cLongRegMaxSize, 0);

        /* Read it back (immediately is okay for this test). */
        AtDeviceLongReadOnCore(device, address, readValue, cLongRegMaxSize, 0);
        }

    return (0);
    }

static int DataBusTest(RegistersDb self)
    {
    RegisterEntry entry = NULL;
    RegisterHashTable hashTable = self->longRegs;
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);
    int ret;

    if (iterator == NULL)
        return -1;

    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        ret = DataBusTestOnReg((LongRegisterEntry)entry, self->device);
        if (ret != 0)
            return ret;
        }

    RegisterHashTableIteratorDelete(iterator);
    return 0;
    }

static uint32* TestPattern(void)
    {
    static uint32 testPattern[cLongRegMaxSize];
    static uint32* pTestPattern = NULL;

    if (pTestPattern == NULL)
        {
        uint8 dw_i;
        for (dw_i = 0; dw_i < cLongRegMaxSize; dw_i++)
            testPattern[dw_i] = 0xAAAAAAAA;
        pTestPattern = testPattern;
        }

    return pTestPattern;
    }

static uint32* TestAntiPattern(uint32 *pattern)
    {
    uint8 dw_i;
    static uint32 testAntiPattern[cLongRegMaxSize];
    for (dw_i = 0; dw_i < cLongRegMaxSize; dw_i++)
        testAntiPattern[dw_i] = ~pattern[dw_i];

    return testAntiPattern;
    }

static void FillPattern(RegisterHashTableIterator iterator, AtDevice device, uint32* pattern)
    {
    RegisterEntry entry;
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        AtDeviceLongWriteOnCore(device, RegisterEntryAddressGet(entry), pattern, cLongRegMaxSize, 0);
    }

static int AddressBusTest(RegistersDb self, AtDevice device)
    {
    uint32* pattern     = TestPattern();
    uint32* antipattern = TestAntiPattern(pattern);
    uint32 readValue[cLongRegMaxSize];
    uint32 testAddress;

    RegisterEntry testEntry, entry;
    RegisterHashTable hashTable = self->longRegs;
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);
    RegisterHashTableIterator iterator2 = RegisterHashTableIteratorCreate(hashTable);

    if (RegisterHashTableIteratorNext(iterator) == NULL) /* Empty */
        {
        RegisterHashTableIteratorDelete(iterator);
        RegisterHashTableIteratorDelete(iterator2);
        return -1;
        }

    /* Write default pattern */
    RegisterHashTableIteratorRestart(iterator);
    FillPattern(iterator, device, pattern);

    /* Check for address bits stuck high. */
    RegisterHashTableIteratorRestart(iterator);
    testEntry = RegisterHashTableIteratorNext(iterator);
    testAddress = RegisterEntryAddressGet(testEntry);
    AtDeviceLongWriteOnCore(device, testAddress, antipattern, cLongRegMaxSize, 0);
    RegisterHashTableIteratorRestart(iterator);
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        if (testEntry != entry)
            AtDeviceLongReadOnCore(device, RegisterEntryAddressGet(entry), readValue, cLongRegMaxSize, 0);
        }

    AtDeviceLongWriteOnCore(device, testAddress, pattern, cLongRegMaxSize, 0);
    RegisterHashTableIteratorRestart(iterator);
    while ((testEntry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        testAddress = RegisterEntryAddressGet(testEntry);
        AtDeviceLongWriteOnCore(device, testAddress, antipattern, cLongRegMaxSize, 0);
        while ((entry = RegisterHashTableIteratorNext(iterator2)) != NULL)
            {
            if (testEntry != entry)
                AtDeviceLongReadOnCore(device, RegisterEntryAddressGet(entry), readValue, cLongRegMaxSize, 0);
            }

        AtDeviceLongWriteOnCore(device, testAddress, pattern, cLongRegMaxSize, 0);
        }

    RegisterHashTableIteratorDelete(iterator);
    RegisterHashTableIteratorDelete(iterator2);
    return 0;
    }

static uint32* Increase1Pattern(void)
    {
    if (m_IncrPatternDwIndex == 0)
        {
        PatternReset(m_IncrPattern);
        return m_IncrPattern;
        }

    if (m_IncrPatternDwIndex == cLongRegMaxSize)
        {
        m_IncrPatternDwIndex = 0;
        return NULL;
        }

    if (m_IncrPattern[m_IncrPatternDwIndex] == cBit31_0)
        {
        m_IncrPattern[m_IncrPatternDwIndex] = 0;
        m_IncrPattern[m_IncrPatternDwIndex + 1] = 1;
        m_IncrPatternDwIndex = (uint16)(m_IncrPatternDwIndex + 1);
        }
    else
        m_IncrPattern[m_IncrPatternDwIndex] += 1;

    return m_IncrPattern;
    }

static void Increase1Reset(void)
    {
    m_IncrPatternDwIndex = 0;
    }

static int DeviceTest(RegistersDb self, AtDevice device)
    {
    uint32 readValue[cLongRegMaxSize];
    RegisterEntry entry;
    RegisterHashTable hashTable = self->longRegs;
    RegisterHashTableIterator iterator = RegisterHashTableIteratorCreate(hashTable);
    uint32 address;

    /* Fill memory with a known pattern. */
    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        AtDeviceLongWriteOnCore(device, RegisterEntryAddressGet(entry), Increase1Pattern(), cLongRegMaxSize, 0);

    /* Check each location and invert it for the second pass. */
    RegisterHashTableIteratorRestart(iterator);
    Increase1Reset();

    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        {
        address = RegisterEntryAddressGet(entry);
        AtDeviceLongReadOnCore(device, address, readValue, cLongRegMaxSize, 0);
        AtDeviceLongWriteOnCore(device, address, TestAntiPattern(Increase1Pattern()), cLongRegMaxSize, 0);
        }

    /* Check each location for the inverted pattern and zero it. */
    Increase1Reset();
    RegisterHashTableIteratorRestart(iterator);

    while ((entry = RegisterHashTableIteratorNext(iterator)) != NULL)
        AtDeviceLongReadOnCore(device, RegisterEntryAddressGet(entry), readValue, cLongRegMaxSize, 0);

    RegisterHashTableIteratorDelete(iterator);
    return 0;
    }

eBool LongRegisterMemTest(RegistersDb self)
    {
    int ret = 0;

    if (self == NULL)
        {
        AtPrintc(cSevCritical, "Cannot memtest, register checking may be stopped\r\n");
        return cAtFalse;
        }

    ret |= DataBusTest(self);
    ret |= AddressBusTest(self, self->device);
    ret |= DeviceTest(self, self->device);

    return (ret < 0) ? cAtFalse : cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : CliAtBerMeasureTimeEngine.h
 * 
 * Created Date: Dec 15, 2017
 *
 * Description : CLI to control SDH path BER monitoring
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATBERMEASURETIMEENGINE_H_
#define _CLIATBERMEASURETIMEENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModuleBer.h"
#include "AtSdhPath.h"
#include "AtBerMeasureTimeEngine.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtRet (*MeasureTimeEngineAttributeFunc)(AtBerMeasureTimeEngine, uint32);
typedef eAtRet (*MeasureTimeEngineNoneAttributeFunc)(AtBerMeasureTimeEngine);
typedef AtBerMeasureTimeEngine (*MeasureTimeEngineFuncCreate)(AtModuleBer, AtBerController);
typedef AtChannel* (*ChannelFromArgumentGetFunc)(char*, uint32 *);
typedef AtBerController (*BerControllerGetFunc)(AtChannel);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliBerMeasureTimeEngineCreate(char argc,
                                    char **argv,
                                    ChannelFromArgumentGetFunc channelGetFunc,
                                    BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEngineDelete(char argc,
                                    char **argv,
                                    ChannelFromArgumentGetFunc channelGetFunc,
                                    BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEngineModeSet(char argc,
                                     char **argv,
                                     ChannelFromArgumentGetFunc channelGetFunc,
                                     BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEnginePollingStart(char argc,
                                          char **argv,
                                          ChannelFromArgumentGetFunc channelGetFunc,
                                          BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEnginePollingStop(char argc,
                                         char **argv,
                                         ChannelFromArgumentGetFunc channelGetFunc,
                                         BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEngineInputErrorConnect(char argc,
                                               char **argv,
                                               ChannelFromArgumentGetFunc channelGetFunc,
                                               BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEngineInputErrorDisconnect(char argc,
                                                  char **argv,
                                                  ChannelFromArgumentGetFunc channelGetFunc,
                                                  BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEngineEstimatedTimesClear(char argc,
                                                 char **argv,
                                                 ChannelFromArgumentGetFunc channelGetFunc,
                                                 BerControllerGetFunc controllerGetFunc);

eBool CliBerMeasureTimeEngineShow(char argc,
                                  char **argv,
                                  ChannelFromArgumentGetFunc channelGetFunc,
                                  BerControllerGetFunc controllerGetFunc);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATBERMEASURETIMEENGINE_H_ */


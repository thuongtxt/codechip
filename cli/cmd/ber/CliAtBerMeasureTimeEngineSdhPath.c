/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerControllerSdhPath.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLI to control SDH path BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtBerMeasureTimeEngine.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtSdhPathBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                         (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                         (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                          (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                               (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                              (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                                    (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                                       (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                                      (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

eBool CmdAtSdhPathBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliSdhPathFromArgumentGet,
                                       (BerControllerGetFunc) AtSdhChannelBerControllerGet);
    }

# @group CliAtBerMeasureTimeEngineSdhLine "SDH Line BER"
# @ingroup CliAtModuleBer "BER module"

6 sdh line rs ber measure create CmdAtSdhLineRsBerMeasureTimeEngineCreate 2 lineId=1 engineType=soft
/*
    Syntax     : sdh line rs ber measure create <lineId> <engineType>
    Parameter  : lineId         - SDH/SONET Line Id
                 engineType     - Engine Type
                                  + soft   : soft-engine 
                                  + hard   : hard-engine
    Description: Create measure engine base on soft/hard type.
    Example    : sdh line rs ber measure create 1 soft
*/

6 sdh line rs ber measure delete CmdAtSdhLineRsBerMeasureTimeEngineDelete 1 lineId=1
/*
    Syntax     : sdh line rs ber measure delete <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Delete measure engine.
    Example    : sdh line rs ber measure delete 1
*/

7 sdh line rs ber measure polling start CmdAtSdhLineRsBerMeasureTimeEnginePollingStart 1 lineId=1
/*
    Syntax     : sdh line rs ber measure polling start <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Start polling on this BER measure engine.
    Example    : sdh line rs ber measure polling start 1
*/

7 sdh line rs ber measure polling stop CmdAtSdhLineRsBerMeasureTimeEnginePollingStop 1 lineId=1
/*
    Syntax     : sdh line rs ber measure polling stop <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Stop polling on this BER measure engine.
    Example    : sdh line rs ber measure polling stop 1
*/

7 sdh line rs ber measure inputerror connect CmdAtSdhLineRsBerMeasureTimeEngineInputErrorConnect 1 lineId=1
/*
    Syntax     : sdh line rs ber measure inputerror connect <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Connect input error for measure detection time.
    Example    : sdh line rs ber measure inputerror connect 1
*/

7 sdh line rs ber measure inputerror disconnect CmdAtSdhLineRsBerMeasureTimeEngineInputErrorDisconnect 1 lineId=1
/*
    Syntax     : sdh line rs ber measure inputerror disconnect <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Dis-connect input error for measure clearing time.
    Example    : sdh line rs ber measure inputerror disconnect 1
*/

6 sdh line rs ber measure clear CmdAtSdhLineRsBerMeasureTimeEngineEstimatedTimesClear 1 lineId=1
/*
    Syntax     : sdh line rs ber measure clear <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Clear all measured time data
    Example    : sdh line rs ber measure clear 1
*/

6 show sdh line rs ber measure CmdAtSdhLineRsBerMeasureTimeEngineShow 1 lineId=1
/*
    Syntax     : show sdh line rs ber measure <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Show all measured time data.
    Example    : show sdh line rs ber measure 1
*/

6 sdh line rs ber measure mode CmdAtSdhLineRsBerMeasureTimeEngineModeSet 2 lineId=1 mode=
/*
    Syntax     : sdh line rs ber measure mode <lineId> <mode>
    Parameter  : lineId         - SDH/SONET Line Id
                 mode           - Measure mode
                                  + manual   : applicable for only hard-engine 
                                  + oneshot  : applicable for  hard-engine/soft-engine
                                  + continous_none_stop  : applicable for  hard-engine/soft-engine
                                  + continous_auto_stop  : applicable for  hard-engine/soft-engine
    Description: Set measuring mode.
    Example    : sdh line rs ber measure mode 1 manual
*/


6 sdh line ms ber measure create CmdAtSdhLineMsBerMeasureTimeEngineCreate 2 lineId=1 engineType=soft
/*
    Syntax     : sdh line ms ber measure create <lineId> <engineType>
    Parameter  : lineId         - SDH/SONET Line Id
                 engineType     - Engine Type
                                  + soft   : soft-engine 
                                  + hard   : hard-engine
    Description: Create measure engine base on soft/hard type.
    Example    : sdh line ms ber measure create 1 soft
*/

6 sdh line ms ber measure delete CmdAtSdhLineMsBerMeasureTimeEngineDelete 1 lineId=1
/*
    Syntax     : sdh line ms ber measure delete <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Delete measure engine.
    Example    : sdh line ms ber measure delete 1
*/

7 sdh line ms ber measure polling start CmdAtSdhLineMsBerMeasureTimeEnginePollingStart 1 lineId=1
/*
    Syntax     : sdh line ms ber measure polling start <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Start polling on this BER measure engine.
    Example    : sdh line ms ber measure polling start 1
*/

7 sdh line ms ber measure polling stop CmdAtSdhLineMsBerMeasureTimeEnginePollingStop 1 lineId=1
/*
    Syntax     : sdh line ms ber measure polling stop <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Stop polling on this BER measure engine.
    Example    : sdh line ms ber measure polling stop 1
*/

7 sdh line ms ber measure inputerror connect CmdAtSdhLineMsBerMeasureTimeEngineInputErrorConnect 1 lineId=1
/*
    Syntax     : sdh line ms ber measure inputerror connect <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Connect input error for measure detection time.
    Example    : sdh line ms ber measure inputerror connect 1
*/

7 sdh line ms ber measure inputerror disconnect CmdAtSdhLineMsBerMeasureTimeEngineInputErrorDisconnect 1 lineId=1
/*
    Syntax     : sdh line ms ber measure inputerror disconnect <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Dis-connect input error for measure clearing time.
    Example    : sdh line ms ber measure inputerror disconnect 1
*/

6 sdh line ms ber measure clear CmdAtSdhLineMsBerMeasureTimeEngineEstimatedTimesClear 1 lineId=1
/*
    Syntax     : sdh line ms ber measure clear <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Clear all measured time data
    Example    : sdh line ms ber measure clear 1
*/

6 show sdh line ms ber measure CmdAtSdhLineMsBerMeasureTimeEngineShow 1 lineId=1
/*
    Syntax     : show sdh line ms ber measure <lineId>
    Parameter  : lineId         - SDH/SONET Line Id
                 
    Description: Show all measured time data.
    Example    : show sdh line ms ber measure 1
*/

6 sdh line ms ber measure mode CmdAtSdhLineMsBerMeasureTimeEngineModeSet 2 lineId=1 mode=
/*
    Syntax     : sdh line ms ber measure mode <lineId> <mode>
    Parameter  : lineId         - SDH/SONET Line Id
                 mode           - Measure mode
                                  + manual   : applicable for only hard-engine 
                                  + oneshot  : applicable for  hard-engine/soft-engine
                                  + continous_none_stop  : applicable for  hard-engine/soft-engine
                                  + continous_auto_stop  : applicable for  hard-engine/soft-engine
    Description: Set measuring mode.
    Example    : sdh line ms ber measure mode 1 manual
*/

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerMeasureTimeEngineSdhLine.c
 *
 * Created Date: Dec 15, 2017
 *
 * Description : CLI to control SDH path BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtBerMeasureTimeEngine.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhLine* CliSdhLineFromArgumentGet(char* arg, uint32 *numberPaths)
    {
    uint32 bufferSize;

    AtSdhLine* lines = (AtSdhLine *)((void**)CliSharedChannelListGet(&bufferSize));
    *numberPaths = CliSdhLineArrayFromString(arg, (AtChannel *)((void**)lines), bufferSize);

    return lines;
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                         (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                         (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                          (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                               (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                              (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                                    (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                                       (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                                      (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineRsBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                       (BerControllerGetFunc) AtSdhLineRsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                         (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                         (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                          (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                               (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                              (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                                    (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                                       (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                                      (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }

eBool CmdAtSdhLineMsBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliSdhLineFromArgumentGet,
                                       (BerControllerGetFunc) AtSdhLineMsBerControllerGet);
    }


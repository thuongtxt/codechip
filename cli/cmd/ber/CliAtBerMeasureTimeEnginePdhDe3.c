/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerMeasureTimeEngineDe3.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLI to control De3 BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtBerMeasureTimeEngine.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe3* CliDe3FromArgumentGet(char* arg, uint32 *numberDe3s)
    {
    uint32 bufferSize;

    AtPdhDe3* De3s = (AtPdhDe3 *)((void**)CliSharedChannelListGet(&bufferSize));
    *numberDe3s = CliDe3ListFromString(arg, (AtChannel *)((void**)De3s), bufferSize);

    return De3s;
    }

eBool CmdAtDe3PathBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                          (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                               (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                              (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                                    (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                                       (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                                      (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3PathBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                       (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                          (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                               (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                              (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                                    (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                                       (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                                      (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe3LineBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliDe3FromArgumentGet,
                                       (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtModuleBer.c
 *
 * Created Date: Jan 10, 2014
 *
 * Description : BER module CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleBer BerModule(void)
    {
    return (AtModuleBer)AtDeviceModuleGet(CliDevice(), cAtModuleBer);
    }

eBool CmdAtModuleBerDebugEnable(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebugEnable((AtModule)BerModule(), cAtTrue);
    return cAtTrue;
    }

eBool CmdAtModuleBerDebugDisable(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebugEnable((AtModule)BerModule(), cAtFalse);
    return cAtTrue;
    }

eBool CmdAtModuleBerDebug(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebug((AtModule)BerModule());
    return cAtTrue;
    }

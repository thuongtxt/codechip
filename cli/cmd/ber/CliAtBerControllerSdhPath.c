/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerControllerSdhPath.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLI to control SDH path BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtSdhPath.h"
#include "AtCliModule.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Enable(char argc, char**argv, eBool enable)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    eBool success = cAtTrue;
    AtChannel *paths;
	AtUnused(argc);

    /* Get list of path */
    paths = CliSharedChannelListGet(&bufferSize);
    numberPaths = CliChannelListFromString(argv[0], paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to force, ID list may be wrong\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtBerController berController = AtSdhChannelBerControllerGet((AtSdhChannel)paths[i]);
        eAtRet ret = AtBerControllerEnable(berController, enable);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s %s fail with ret = %s\r\n",
                 enable ? "Enable" : "Disable",
                 CliChannelIdStringGet((AtChannel)paths[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool ThresholdSet(char argc, char**argv, eAtModuleBerRet (*func)(AtSdhChannel, eAtBerRate))
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    AtChannel *paths;
    eAtBerRate threshold;
    eBool success = cAtTrue;

	AtUnused(argc);

    /* Get list of path */
    paths = CliSharedChannelListGet(&bufferSize);
    numberPaths = CliChannelListFromString(argv[0], paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to force, ID list may be wrong\r\n");
        return cAtFalse;
        }

    /* Get threshold */
    threshold = CliBerRateFromString(argv[1]);
    if(threshold == cAtBerRateUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid ber rate. Expected: %s\r\n", CliValidBerRates());
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = func((AtSdhChannel)paths[i], threshold);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set SD threshold %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)paths[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eAtModuleBerRet BerSdThresSet(AtSdhChannel path, eAtBerRate berRate)
    {
    AtBerController berController = AtSdhChannelBerControllerGet(path);
    return AtBerControllerSdThresholdSet(berController, berRate);
    }

static eAtModuleBerRet BerSfThresSet(AtSdhChannel path, eAtBerRate berRate)
    {
    AtBerController berController = AtSdhChannelBerControllerGet(path);
    return AtBerControllerSfThresholdSet(berController, berRate);
    }

static eAtModuleBerRet BerTcaThresSet(AtSdhChannel path, eAtBerRate berRate)
    {
    AtBerController berController = AtSdhChannelBerControllerGet(path);
    return AtBerControllerTcaThresholdSet(berController, berRate);
    }

eBool CmdSdhPathBerMonitorEnable(char argc, char**argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdSdhPathBerMonitorDisable(char argc, char**argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdSdhPathBerSdThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, BerSdThresSet);
    }

eBool CmdSdhPathBerSfThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, BerSfThresSet);
    }

eBool CmdSdhPathBerTcaThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, BerTcaThresSet);
    }

eBool CmdSdhPathBerGet(char argc, char**argv)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    tTab *tabPtr;
    AtChannel *paths;
    const char *pHeading[] = {"PathId", "Enable", "SD threshold", "SF threshold", "TCA threshold", "Current BER"};
    eBool berEn;
	AtUnused(argc);

    /* Get list of path */
    paths = CliSharedChannelListGet(&bufferSize);
    numberPaths = CliChannelListFromString(argv[0], paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to force, ID list may be wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char *berString;
        AtBerController berController;

        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)paths[i]));

        /* Print BER information */
        berController = AtSdhChannelBerControllerGet((AtSdhChannel)paths[i]);
        berEn = AtBerControllerIsEnabled(berController);
        ColorStrToCell(tabPtr, i, 1, CliBoolToString(berEn), berEn ? cSevInfo : cSevCritical);

        /* If BER is disable, nothing to show next */
        if (berEn == cAtFalse)
            {
            StrToCell(tabPtr, i, 2, "xxx");
            StrToCell(tabPtr, i, 3, "xxx");
            StrToCell(tabPtr, i, 4, "xxx");
            StrToCell(tabPtr, i, 5, "xxx");
            continue;
            }

        /* SD */
        berString = CliBerRateString(AtBerControllerSdThresholdGet(berController));
        StrToCell(tabPtr, i, 2, berString ? berString : "Invalid");

        /* SF */
        berString = CliBerRateString(AtBerControllerSfThresholdGet(berController));
        StrToCell(tabPtr, i, 3, berString ? berString : "Invalid");

        /* TCA */
        berString = CliBerRateString(AtBerControllerTcaThresholdGet(berController));
        StrToCell(tabPtr, i, 4, berString ? berString : "Invalid");

        /* Current BER */
        berString = CliBerRateString(AtBerControllerCurBerGet(berController));
        StrToCell(tabPtr, i, 5, berString ? berString : "Invalid");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdSdhPathBerDebug(char argc, char**argv)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    AtChannel *paths;
    eBool debugOn = cAtFalse;
    eBool convertSuccess = cAtFalse;
	AtUnused(argc);

    /* Get list of path */
    paths = CliSharedChannelListGet(&bufferSize);
    numberPaths = CliChannelListFromString(argv[0], paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to force, ID list may be wrong\r\n");
        return cAtFalse;
        }

    mAtStrToBool(argv[1], debugOn, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtBerController berController;
        berController = AtSdhChannelBerControllerGet((AtSdhChannel)paths[i]);

        if (AtBerControllerIsEnabled(berController))
            AtBerControllerDebugOn(berController, debugOn);
        }

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerControllerPdhDe1.c
 *
 * Created Date: Aug 3, 2015
 *
 * Description : DS1/E1 BER monitoring CLI implement.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhDe1.h"
#include "AtCliModule.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
typedef AtBerController (*ControllerGet)(AtPdhChannel);
typedef eAtModuleBerRet (*BerThresSet)(AtBerController, eAtBerRate);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Enable(char argc, char**argv, ControllerGet ControllerGetFunc, eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;

    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "WARNING: no channels are input. The input ID may be wrong.\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
    	eAtRet ret;
        AtBerController berController = ControllerGetFunc((AtPdhChannel)channelList[i]);

   	    if (berController == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: %s can not get BER-Controller. It may not support or not applicable! \r\n",
                     CliChannelIdStringGet((AtChannel)channelList[i]));
            continue;
            }
        
        ret = AtBerControllerEnable(berController, enable);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s %s fail with ret = %s\r\n",
                 enable ? "Enable" : "Disable",
                 CliChannelIdStringGet(channelList[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool ThresholdSet(char argc, char**argv, ControllerGet ControllerGetFunc, BerThresSet ThresSetFunc)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eAtBerRate threshold;
    eBool success = cAtTrue;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "WARNING: no channels are input. The input ID may be wrong.\r\n");
        return cAtFalse;
        }

    /* Get threshold */
    threshold = CliBerRateFromString(argv[1]);
    if(threshold == cAtBerRateUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid ber rate. Expected: %s\r\n", CliValidBerRates());
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
    	eAtRet ret;
        AtBerController berController = ControllerGetFunc((AtPdhChannel)channelList[i]);
   	    if (berController == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: %s can not get BER-Controller. It may not support or not applicable! \r\n",
                     CliChannelIdStringGet((AtChannel)channelList[i]));
            continue;
            }

   	    ret = ThresSetFunc(berController, threshold);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set threshold %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(channelList[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool Show(char argc, char**argv, ControllerGet ControllerGetFunc)
    {
    uint32 numChannels, i;
    tTab *tabPtr;
    AtChannel *channelList;
    const char *pHeading[] = {"DE1 ID", "Enable", "SD threshold", "SF threshold", "TCA threshold", "Current BER"};
    eBool berEn;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "WARNING: no channels are input. The input ID may be wrong.\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char *berString;
        AtBerController berController;

        /* Print list channelListId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)channelList[i]));

        /* Print BER information */
        berController = ControllerGetFunc((AtPdhChannel)channelList[i]);
        
        /* If BER is disable, nothing to show next */
        if (berController == NULL)
            {
            StrToCell(tabPtr, i, 1, sAtNotSupported);
            StrToCell(tabPtr, i, 2, sAtNotSupported);
            StrToCell(tabPtr, i, 3, sAtNotSupported);
            StrToCell(tabPtr, i, 4, sAtNotSupported);
            StrToCell(tabPtr, i, 5, sAtNotSupported);
            continue;
            }

        berEn = AtBerControllerIsEnabled(berController);
        ColorStrToCell(tabPtr, i, 1, CliBoolToString(berEn), berEn ? cSevInfo : cSevCritical);

        /* If BER is disable, nothing to show next */
        if (berEn == cAtFalse)
            {
            StrToCell(tabPtr, i, 2, "xxx");
            StrToCell(tabPtr, i, 3, "xxx");
            StrToCell(tabPtr, i, 4, "xxx");
            StrToCell(tabPtr, i, 5, "xxx");
            continue;
            }

        /* SD */
        berString = CliBerRateString(AtBerControllerSdThresholdGet(berController));
        StrToCell(tabPtr, i, 2, berString ? berString : "Invalid");

        /* SF */
        berString = CliBerRateString(AtBerControllerSfThresholdGet(berController));
        StrToCell(tabPtr, i, 3, berString ? berString : "Invalid");

        /* TCA */
        berString = CliBerRateString(AtBerControllerTcaThresholdGet(berController));
        StrToCell(tabPtr, i, 4, berString ? berString : "Invalid");

        /* Current BER */
        berString = CliBerRateString(AtBerControllerCurBerGet(berController));
        StrToCell(tabPtr, i, 5, berString ? berString : "Invalid");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool Debug(char argc, char**argv, ControllerGet ControllerGetFunc)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool debugOn = cAtFalse;
    eBool convertSuccess = cAtFalse;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "WARNING: no channels are input. The input ID may be wrong.\r\n");
        return cAtFalse;
        }

    mAtStrToBool(argv[1], debugOn, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtBerController berController;
        berController = ControllerGetFunc((AtPdhChannel)channelList[i]);

   	    if (berController == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: %s can not get BER-Controller. It may not support or not applicable! \r\n",
                     CliChannelIdStringGet((AtChannel)channelList[i]));
            continue;
            }

        if (AtBerControllerIsEnabled(berController))
            AtBerControllerDebugOn(berController, debugOn);
        }

    return cAtTrue;
    }

eBool CmdPdhDe1PathBerMonitorEnable(char argc, char**argv)
    {
    return Enable(argc, argv, AtPdhChannelPathBerControllerGet, cAtTrue);
    }

eBool CmdPdhDe1PathBerMonitorDisable(char argc, char**argv)
    {
    return Enable(argc, argv, AtPdhChannelPathBerControllerGet, cAtFalse);
    }

eBool CmdPdhDe1PathBerSdThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtPdhChannelPathBerControllerGet, AtBerControllerSdThresholdSet);
    }

eBool CmdPdhDe1PathBerSfThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtPdhChannelPathBerControllerGet, AtBerControllerSfThresholdSet);
    }

eBool CmdPdhDe1PathBerTcaThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtPdhChannelPathBerControllerGet, AtBerControllerTcaThresholdSet);
    }

eBool CmdPdhDe1PathBerGet(char argc, char**argv)
    {
    return Show(argc, argv, AtPdhChannelPathBerControllerGet);
    }

eBool CmdPdhDe1PathBerDebug(char argc, char**argv)
    {
    return Debug(argc, argv, AtPdhChannelPathBerControllerGet);
    }

eBool CmdPdhDe1LineBerMonitorEnable(char argc, char**argv)
    {
    return Enable(argc, argv, AtPdhChannelLineBerControllerGet, cAtTrue);
    }

eBool CmdPdhDe1LineBerMonitorDisable(char argc, char**argv)
    {
    return Enable(argc, argv, AtPdhChannelLineBerControllerGet, cAtFalse);
    }

eBool CmdPdhDe1LineBerSdThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtPdhChannelLineBerControllerGet, AtBerControllerSdThresholdSet);
    }

eBool CmdPdhDe1LineBerSfThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtPdhChannelLineBerControllerGet, AtBerControllerSfThresholdSet);
    }

eBool CmdPdhDe1LineBerTcaThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtPdhChannelLineBerControllerGet, AtBerControllerTcaThresholdSet);
    }

eBool CmdPdhDe1LineBerGet(char argc, char**argv)
    {
    return Show(argc, argv, AtPdhChannelLineBerControllerGet);
    }

eBool CmdPdhDe1LineBerDebug(char argc, char**argv)
    {
    return Debug(argc, argv, AtPdhChannelLineBerControllerGet);
    }

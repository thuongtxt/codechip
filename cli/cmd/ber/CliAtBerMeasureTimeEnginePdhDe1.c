/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerMeasureTimeEngineDe1.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLI to control De1 BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtBerMeasureTimeEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe1* CliDe1FromArgumentGet(char* arg, uint32 *numberDe1s)
    {
    uint32 bufferSize;

    AtPdhDe1* de1s = (AtPdhDe1 *)((void**)CliSharedChannelListGet(&bufferSize));
    *numberDe1s = De1ListFromString(arg, (AtChannel *)((void**)de1s), bufferSize);

    return de1s;
    }

eBool CmdAtDe1PathBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                          (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                               (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                              (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                                    (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                                       (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                                      (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1PathBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                       (BerControllerGetFunc) AtPdhChannelPathBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineCreate(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineCreate(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineDelete(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineDelete(argc,
                                         argv,
                                         (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                         (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineModeSet(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineModeSet(argc,
                                          argv,
                                          (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                          (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEnginePollingStart(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStart(argc,
                                               argv,
                                               (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                               (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEnginePollingStop(char argc, char **argv)
    {
    return CliBerMeasureTimeEnginePollingStop(argc,
                                              argv,
                                              (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                              (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineInputErrorConnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorConnect(argc,
                                                    argv,
                                                    (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                                    (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineInputErrorDisconnect(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineInputErrorDisconnect(argc,
                                                       argv,
                                                       (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                                       (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineEstimatedTimesClear(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineEstimatedTimesClear(argc,
                                                      argv,
                                                      (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                                      (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }

eBool CmdAtDe1LineBerMeasureTimeEngineShow(char argc, char **argv)
    {
    return CliBerMeasureTimeEngineShow(argc,
                                       argv,
                                       (ChannelFromArgumentGetFunc) CliDe1FromArgumentGet,
                                       (BerControllerGetFunc) AtPdhChannelLineBerControllerGet);
    }



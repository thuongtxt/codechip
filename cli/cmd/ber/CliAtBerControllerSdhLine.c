/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerControllerSdhLine.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLI to control BER monitoring for SDH Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtSdhLine.h"
#include "AtCliModule.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static AtSdhLine LineGet(AtModuleSdh sdhModule, uint32 lineId)
    {
    return AtModuleSdhLineGet(sdhModule, (uint8)lineId);
    }

static eBool Enable(char argc, char**argv, eBool enable, AtBerController (*BerControllerGet)(AtSdhLine))
    {
    uint32 numberOfLines, i;
    uint32 *sdhLineIds;
    eBool success = cAtTrue;

	AtUnused(argc);

    /* Get list of line */
    sdhLineIds = CliSharedIdBufferGet(&numberOfLines);
    numberOfLines = CliIdListFromString(argv[0], sdhLineIds, numberOfLines);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Line ID list, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = LineGet(SdhModule(), sdhLineIds[i] - 1);
        AtBerController berController;
        eAtRet ret;

        if (line == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: ignore line %d which does not exist\r\n", sdhLineIds[i]);
            continue;
            }

        berController = BerControllerGet(line);
        ret = AtBerControllerEnable(berController, enable);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s %s fail with ret = %s\r\n",
                 enable ? "Enable" : "Disable",
                 CliChannelIdStringGet((AtChannel)line),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool ThresholdSet(char argc, char**argv,
                             eAtModuleBerRet (*thresholdSetFunc)(AtBerController controller, eAtBerRate),
                             AtBerController (*controllerGetFunc)(AtSdhLine line))
    {
    eAtBerRate threshold;

    /* Get list of line */
    uint32 numberOfLines, i;
    uint32 *sdhLineIds;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of line */
    sdhLineIds = CliSharedIdBufferGet(&numberOfLines);
    numberOfLines = CliIdListFromString(argv[0], sdhLineIds, numberOfLines);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Line ID list, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    /* Get threshold */
    threshold = CliBerRateFromString(argv[1]);
    if(threshold == cAtBerRateUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid ber rate. Expected: %s\r\n", CliValidBerRates());
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = LineGet(SdhModule(), sdhLineIds[i] - 1);
        AtBerController controller = controllerGetFunc(line);
        eAtRet ret = thresholdSetFunc(controller, threshold);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set SD threshold %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)line),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool Show(char argc, char**argv, AtBerController (*BerControllerGet)(AtSdhLine))
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Line", "Enable", "SD threshold", "SF threshold", "TCA threshold", "Current BER"};
    eBool berEn;

    /* Get list of line */
    uint32 numberOfLines, i;
    uint32 *sdhLineIds;
    AtSdhLine line;
	AtUnused(argc);

    /* Get list of line */
    sdhLineIds = CliSharedIdBufferGet(&numberOfLines);
    numberOfLines = CliIdListFromString(argv[0], sdhLineIds, numberOfLines);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Line ID list, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char *berString;
        AtBerController berController;

        line = LineGet(SdhModule(), sdhLineIds[i] - 1);

        /* Print list linesId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)line));

        /* Print BER information */
        berController = BerControllerGet(line);
        berEn = AtBerControllerIsEnabled(berController);
        ColorStrToCell(tabPtr, i, 1, CliBoolToString(berEn), berEn ? cSevInfo : cSevCritical);

        /* If BER is disable, nothing to show next */
        if (berEn == cAtFalse)
            {
            StrToCell(tabPtr, i, 2, "xxx");
            StrToCell(tabPtr, i, 3, "xxx");
            StrToCell(tabPtr, i, 4, "xxx");
            StrToCell(tabPtr, i, 5, "xxx");
            continue;
            }

        /* SD */
        berString = CliBerRateString(AtBerControllerSdThresholdGet(berController));
        StrToCell(tabPtr, i, 2, berString ? berString : "Invalid");

        /* SF */
        berString = CliBerRateString(AtBerControllerSfThresholdGet(berController));
        StrToCell(tabPtr, i, 3, berString ? berString : "Invalid");

        /* TCA */
        berString = CliBerRateString(AtBerControllerTcaThresholdGet(berController));
        StrToCell(tabPtr, i, 4, berString ? berString : "Invalid");

        /* Current BER */
        berString = CliBerRateString(AtBerControllerCurBerGet(berController));
        StrToCell(tabPtr, i, 5, berString ? berString : "Invalid");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool Debug(char argc, char**argv, AtBerController (*controllerGetFunc)(AtSdhLine line))
    {
    /* Get list of line */
    uint32 numberOfLines, i;
    uint32 *sdhLineIds;
    eBool debugOn = cAtFalse;
    eBool convertSuccess = cAtFalse;
	AtUnused(argc);

    /* Get list of line */
    sdhLineIds = CliSharedIdBufferGet(&numberOfLines);
    numberOfLines = CliIdListFromString(argv[0], sdhLineIds, numberOfLines);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Line ID list, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    mAtStrToBool(argv[1], debugOn, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = LineGet(SdhModule(), sdhLineIds[i] - 1);
        AtBerController berController = controllerGetFunc(line);

        if (AtBerControllerIsEnabled(berController))
            AtBerControllerDebugOn(berController, debugOn);
        }

    return cAtTrue;
    }

eBool CmdSdhLineRsBerMonitorDisable(char argc, char**argv)
    {
    return Enable(argc, argv, cAtFalse, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineRsBerMonitorEnable(char argc, char**argv)
    {
    return Enable(argc, argv, cAtTrue, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineRsBerSdThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtBerControllerSdThresholdSet, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineRsBerSfThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtBerControllerSfThresholdSet, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineRsBerTcaThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtBerControllerTcaThresholdSet, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineRsBerGet(char argc, char**argv)
    {
    return Show(argc, argv, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineRsBerDebug(char argc, char**argv)
    {
    return Debug(argc, argv, AtSdhLineRsBerControllerGet);
    }

eBool CmdSdhLineMsBerMonitorEnable(char argc, char**argv)
    {
    return Enable(argc, argv, cAtTrue, AtSdhLineMsBerControllerGet);
    }

eBool CmdSdhLineMsBerMonitorDisable(char argc, char**argv)
    {
    return Enable(argc, argv, cAtFalse, AtSdhLineMsBerControllerGet);
    }

eBool CmdSdhLineMsBerSdThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtBerControllerSdThresholdSet, AtSdhLineMsBerControllerGet);
    }

eBool CmdSdhLineMsBerSfThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtBerControllerSfThresholdSet, AtSdhLineMsBerControllerGet);
    }

eBool CmdSdhLineMsBerTcaThres(char argc, char**argv)
    {
    return ThresholdSet(argc, argv, AtBerControllerTcaThresholdSet, AtSdhLineMsBerControllerGet);
    }

eBool CmdSdhLineMsBerGet(char argc, char**argv)
    {
    return Show(argc, argv, AtSdhLineMsBerControllerGet);
    }

eBool CmdSdhLineMsBerDebug(char argc, char**argv)
    {
    return Debug(argc, argv, AtSdhLineMsBerControllerGet);
    }

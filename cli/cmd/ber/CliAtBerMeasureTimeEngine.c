/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : CliAtBerMeasureTimeEngineCommon.c
 *
 * Created Date: Dec 15, 2017
 *
 * Description : CLI to control SDH path BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtBerMeasureTimeEngine.h"

/*--------------------------- Define -----------------------------------------*/
#define mTimeShow(timeInMs)\
        ColorStrToCell(tabPtr,\
                       row,\
                       col++,\
                       CliNumber2String(timeInMs, "%10u"),\
                       (timeInMs == 0) ? cSevWarning: cSevInfo)
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtBerMeasureEngineTypeStr[] =
    {
    "soft", "hard"
    };

static const eBool cAtBerMeasureEngineTypeVal[] =
    {
    cAtFalse, cAtTrue
    };

static const char * cAtBerMeasureModeStr[] =
    {
    "unknown",
    "manual",
    "continous_none_stop",
    "continous_auto_stop",
    "one_shot"
    };

static const eAtBerMeasureMode cAtBerMeasureModeVal[] =
    {
    cAtBerMeasureModeUnKnown,
    cAtBerMeasureModeManaual,
    cAtBerMeasureModeContinousNoneStop,
    cAtBerMeasureModeContinousAutoStop,
    cAtBerMeasureModeOneShot
    };

static const char * cAtBerMeasureStateStr[] =
    {
    "unknown",
    "wait_error_start",
    "wait_sf_detection",
    "wait_error_stop",
    "wait_sf_clearing",
    "wait_sd_clearing",
    "wait_tca_clearing",
    "one_shot_done",
    "stop_in_continous",
    "wait_to_latch_in_manual",
    "latched_in_manual"
    };

static const eAtBerMeasureState cAtBerMeasureStateVal[] =
    {
     cAtBerMeasureStateUnKnown,
     cAtBerMeasureStateWaitErrorStart,
     cAtBerMeasureStateWaitSfDetectionTime,
     cAtBerMeasureStateWaitErrorStop,
     cAtBerMeasureStateWaitSfClearingTime,
     cAtBerMeasureStateWaitSdClearingTime,
     cAtBerMeasureStateWaitTcaClearingTime,
     cAtBerMeasureStateOneShotDone,
     cAtBerMeasureStateStopInContinous,
     cAtBerMeasureStateWaitToLachInManual,
     cAtBerMeasureStateLatchedInManual
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *StateString(eAtBerMeasureState state)
    {
    static char retString[16];
    eBool convertSuccess;

    mAtEnumToStr(cAtBerMeasureStateStr, cAtBerMeasureStateVal, state, retString, convertSuccess);

    return convertSuccess ? retString : NULL;
    }

static char *ModeString(eAtBerMeasureMode mode)
    {
    static char retString[16];
    eBool convertSuccess;

    mAtEnumToStr(cAtBerMeasureModeStr, cAtBerMeasureModeVal, mode, retString, convertSuccess);

    return convertSuccess ? retString : NULL;
    }


static AtModuleBer BerModule(void)
    {
    return (AtModuleBer)AtDeviceModuleGet(CliDevice(), cAtModuleBer);
    }

static AtChannel ChannelGet(char *channelIdStr, ChannelFromArgumentGetFunc channelGetFunc)
    {
    uint32 numberChannels;
    AtChannel *channels;
    channels = channelGetFunc(channelIdStr, &numberChannels);
    if (numberChannels == 0)
        {
        AtPrintc(cSevCritical, "Error: ID is wrong\r\n");
        return NULL;
        }

    if (numberChannels != 1)
        {
        AtPrintc(cSevCritical, "Error: Only support one Channel\r\n");
        return NULL;
        }

    return channels[0];
    }

static eBool MeasureTimeEngineCreate(char argc,
                                     char **argv,
                                     ChannelFromArgumentGetFunc channelGetFunc,
                                     BerControllerGetFunc controllerGetFunc,
                                     MeasureTimeEngineFuncCreate FuncCreate)
    {
    AtBerController controller;
    AtBerMeasureTimeEngine engine;
    AtChannel channel;

    AtUnused(argc);

    /* Get Channel */
    channel = ChannelGet(argv[0], channelGetFunc);
    if (channel == NULL)
        return cAtFalse;

    controller = controllerGetFunc(channel);
    engine = FuncCreate(BerModule(), controller);
    if (engine == NULL)
        {
        AtPrintc(cSevCritical, "Engine may not support or already created!\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliBerMeasureTimeEngineCreate(char argc,
                                    char **argv,
                                    ChannelFromArgumentGetFunc channelGetFunc,
                                    BerControllerGetFunc controllerGetFunc)
    {
    eBool convertSuccess = cAtTrue;
    eBool IsHardEngine = cAtFalse;

    mAtStrToEnum(cAtBerMeasureEngineTypeStr, cAtBerMeasureEngineTypeVal, argv[1], IsHardEngine, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Engine Type, expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtBerMeasureEngineTypeStr, mCount(cAtBerMeasureEngineTypeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    if (IsHardEngine)
        return MeasureTimeEngineCreate(argc, argv, channelGetFunc, controllerGetFunc, AtModuleBerMeasureTimeHardEngineCreate);

    return MeasureTimeEngineCreate(argc, argv, channelGetFunc, controllerGetFunc, AtModuleBerMeasureTimeSoftEngineCreate);
    }

static AtBerMeasureTimeEngine CliBerMeasureTimeEngineGet(char *channelIdStr,
                                                         ChannelFromArgumentGetFunc channelGetFunc,
                                                         BerControllerGetFunc controllerGetFunc)
    {
    AtBerController controller;
    AtBerMeasureTimeEngine engine;
    AtChannel channel;

    /* Get Channel */
    channel = ChannelGet(channelIdStr, channelGetFunc);
    if (channel == NULL)
        return cAtFalse;

    controller = controllerGetFunc(channel);
    engine = AtModuleBerMeasureTimeEngineGet(BerModule());
    if (!engine)
       {
       AtPrintc(cSevCritical, "Engine is not created!\r\n");
       return NULL;
       }

    if (AtBerMeasureTimeEngineControllerGet(engine) != controller)
       {
       AtPrintc(cSevCritical, "Engine is associate with this channel!\r\n");
       return NULL;
       }

    return engine;
    }

eBool CliBerMeasureTimeEngineDelete(char argc,
                                    char **argv,
                                    ChannelFromArgumentGetFunc channelGetFunc,
                                    BerControllerGetFunc controllerGetFunc)
    {
    eAtRet ret;
    AtBerMeasureTimeEngine engine = CliBerMeasureTimeEngineGet(argv[0], channelGetFunc, controllerGetFunc);

    AtUnused(argc);
    if (engine == NULL)
        return cAtFalse;

    ret = AtModuleBerMeasureTimeEngineDelete(BerModule(), engine);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Failed with ret = %s \r\n", AtDriverRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool MeasureTimeEngineAttributeSet(char argc,
                                           char **argv,
                                           uint32 value,
                                           ChannelFromArgumentGetFunc channelGetFunc,
                                           BerControllerGetFunc controllerGetFunc,
                                           MeasureTimeEngineAttributeFunc func)
    {
    AtBerMeasureTimeEngine engine = CliBerMeasureTimeEngineGet(argv[0], channelGetFunc, controllerGetFunc);
    eAtRet ret = cAtOk;
    AtUnused(argc);

    if (engine == NULL)
        return cAtFalse;

    ret = func(engine, value);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Failed with ret = %s \r\n", AtDriverRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

static eBool MeasureTimeEngineNoneAttributeSet(char argc,
                                               char **argv,
                                               ChannelFromArgumentGetFunc channelGetFunc,
                                               BerControllerGetFunc controllerGetFunc,
                                               MeasureTimeEngineNoneAttributeFunc func)
    {
    AtBerMeasureTimeEngine engine = CliBerMeasureTimeEngineGet(argv[0], channelGetFunc, controllerGetFunc);
    eAtRet ret = cAtOk;

    AtUnused(argc);

    if (engine == NULL)
        return cAtFalse;

    ret = func(engine);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Failed with ret = %s \r\n", AtDriverRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliBerMeasureTimeEngineModeSet(char argc,
                                     char **argv,
                                     ChannelFromArgumentGetFunc channelGetFunc,
                                     BerControllerGetFunc controllerGetFunc)
    {
    eBool convertSuccess = cAtTrue;
    eAtBerMeasureMode mode = cAtBerMeasureModeUnKnown;

    /* Get BER-measure mode */
    mAtStrToEnum(cAtBerMeasureModeStr, cAtBerMeasureModeVal, argv[1], mode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid mode, expected: ");
        CliExpectedValuesPrint(cSevCritical, cAtBerMeasureModeStr, mCount(cAtBerMeasureModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return MeasureTimeEngineAttributeSet(argc,
                                         argv,
                                         mode,
                                         channelGetFunc,
                                         controllerGetFunc,
                                         AtBerMeasureTimeEngineModeSet);
    }

eBool CliBerMeasureTimeEnginePollingStart(char argc,
                                          char **argv,
                                          ChannelFromArgumentGetFunc channelGetFunc,
                                          BerControllerGetFunc controllerGetFunc)
    {
    return MeasureTimeEngineNoneAttributeSet(argc,
                                             argv,
                                             channelGetFunc,
                                             controllerGetFunc,
                                             AtBerMeasureTimeEnginePollingStart);
    }

eBool CliBerMeasureTimeEnginePollingStop(char argc,
                                         char **argv,
                                         ChannelFromArgumentGetFunc channelGetFunc,
                                         BerControllerGetFunc controllerGetFunc)
    {
    return MeasureTimeEngineNoneAttributeSet(argc,
                                             argv,
                                             channelGetFunc,
                                             controllerGetFunc,
                                             AtBerMeasureTimeEnginePollingStop);
    }

eBool CliBerMeasureTimeEngineInputErrorConnect(char argc,
                                               char **argv,
                                               ChannelFromArgumentGetFunc channelGetFunc,
                                               BerControllerGetFunc controllerGetFunc)
    {
    return MeasureTimeEngineNoneAttributeSet(argc,
                                             argv,
                                             channelGetFunc,
                                             controllerGetFunc,
                                             AtBerMeasureTimeEngineInputErrorConnect);
    }

eBool CliBerMeasureTimeEngineInputErrorDisconnect(char argc,
                                                  char **argv,
                                                  ChannelFromArgumentGetFunc channelGetFunc,
                                                  BerControllerGetFunc controllerGetFunc)
    {
    return MeasureTimeEngineNoneAttributeSet(argc,
                                             argv,
                                             channelGetFunc,
                                             controllerGetFunc,
                                             AtBerMeasureTimeEngineInputErrorDisConnect);
    }

eBool CliBerMeasureTimeEngineEstimatedTimesClear(char argc,
                                                 char **argv,
                                                 ChannelFromArgumentGetFunc channelGetFunc,
                                                 BerControllerGetFunc controllerGetFunc)
    {
    return MeasureTimeEngineNoneAttributeSet(argc,
                                             argv,
                                             channelGetFunc,
                                             controllerGetFunc,
                                             AtBerMeasureTimeEngineEstimatedTimesClear);
    }

static eBool MeasureTimeEngineShow(char *channelIdStr, AtBerMeasureTimeEngine engine, eBool displayStatisticResult)
    {
    tTab *tabPtr;
    eAtRet ret;
    eAtBerRate sfThres,sdThres, tcaThres;
    AtBerController controller;
    tAtBerMeasureEstimatedTime estimateTime;

    uint32 row;
    uint32 numRows = cAtBerRate1E9 - cAtBerRate1E2;
    const char *pHeading[] = {"BER",   "DetectionTime", "DetectionTimeReq",  "ClearingTime", "ClearingTimeReq"};

    AtPrintc(cSevInfo, "\r\n * Measuring in %s\r\n", channelIdStr);

    ret = AtBerMeasureTimeEngineEstimatedTimesGet(engine, &estimateTime);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "AtBerMeasureTimeEngineEstimatedTimesGet is failed with ret = %s \r\n", AtDriverRet2String(ret));
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numRows, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    controller = AtBerMeasureTimeEngineControllerGet(engine);
    sfThres = AtBerControllerSfThresholdGet(controller);
    sdThres = AtBerControllerSdThresholdGet(controller);
    tcaThres = AtBerControllerTcaThresholdGet(controller);
    for (row = 0; row < numRows; row++)
        {
        uint32 timeInMs;
        uint32 col = 0;
        eAtBerRate rate = row + cAtBerRate1E3;
        StrToCell(tabPtr, row, col++, CliBerRateString(rate));

        if (rate == sfThres)
            {
            timeInMs = estimateTime.sfDetection.estimateTime;
            mTimeShow(timeInMs);
            timeInMs = AtBerControllerExpectedDetectionTimeInMs(controller, rate);
            mTimeShow(timeInMs);
            timeInMs = estimateTime.sfClearing.estimateTime;
            mTimeShow(timeInMs);
            timeInMs = AtBerControllerExpectedClearingTimeInMs(controller, rate);
            mTimeShow(timeInMs);
            }
        else if (rate == sdThres)
            {
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            timeInMs = estimateTime.sdClearing.estimateTime;
            mTimeShow(timeInMs);
            timeInMs = AtBerControllerExpectedClearingTimeInMs(controller, rate);
            mTimeShow(timeInMs);
            }
        else if (rate == tcaThres)
            {
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            timeInMs = estimateTime.tcaClearing.estimateTime;
            mTimeShow(timeInMs);
            timeInMs = AtBerControllerExpectedClearingTimeInMs(controller, rate);
            mTimeShow(timeInMs);
            }
        else
            {
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            StrToCell(tabPtr, row, col++, sAtNotApplicable);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    if (displayStatisticResult)
        {
        AtPrintc(cSevInfo, "\r\n * Statistics:\r\n");
        AtPrintc(cSevInfo, "       - Number of estimation %u\r\n", estimateTime.numberOfEstimation);
        AtPrintc(cSevInfo,
                 "       - Detection time [%5s] take in range [%10u, %10u][ms]\r\n",
                 CliBerRateString(sfThres),
                 estimateTime.sfDetection.estimateTimeMin,
                 estimateTime.sfDetection.estimateTimeMax);
        AtPrintc(cSevInfo,
                 "       - Clearing time  [%5s] take in range [%10u, %10u][ms]\r\n",
                 CliBerRateString(sfThres),
                 estimateTime.sfClearing.estimateTimeMin,
                 estimateTime.sfClearing.estimateTimeMax);
        if (sdThres != sfThres)
            {
            AtPrintc(cSevInfo,
                     "       - Clearing time  [%5s] take in range [%10u, %10u][ms]\r\n",
                     CliBerRateString(sdThres),
                     estimateTime.sdClearing.estimateTimeMin,
                     estimateTime.sdClearing.estimateTimeMax);
            }
        if (tcaThres != sdThres)
            {
            AtPrintc(cSevInfo,
                     "       - Clearing time  [%5s] take in range [%10u, %10u][ms]\r\n",
                     CliBerRateString(tcaThres),
                     estimateTime.tcaClearing.estimateTimeMin,
                     estimateTime.tcaClearing.estimateTimeMax);
            }
        }

    AtPrintc(cSevInfo, "\r\n * NOTE:\r\n");
    AtPrintc(cSevInfo, "       - unit in mili-second.\r\n");
    AtPrintc(cSevInfo, "       - DetectionTime must be smaller than DetectionTimeReq.\r\n");
    AtPrintc(cSevInfo, "       - ClearingTime  must be smaller than ClearingTimeReq.\r\n");
    AtPrintc(cSevInfo, "       - state = %s \r\n", StateString(AtBerMeasureTimeEngineStateGet(engine)));
    AtPrintc(cSevInfo, "       - mode  = %s \r\n", ModeString(AtBerMeasureTimeEngineModeGet(engine)));
    return cAtTrue;
    }

eBool CliBerMeasureTimeEngineShow(char argc,
                                  char **argv,
                                  ChannelFromArgumentGetFunc channelGetFunc,
                                  BerControllerGetFunc controllerGetFunc)
    {
    eBool displayStatisticResult;
    AtBerMeasureTimeEngine engine = CliBerMeasureTimeEngineGet(argv[0], channelGetFunc, controllerGetFunc);
    eAtBerMeasureMode mode = AtBerMeasureTimeEngineModeGet(engine);
    AtUnused(argc);

    if (engine == NULL)
        return cAtFalse;

    if (mode == cAtBerMeasureModeUnKnown)
        {
        AtPrintc(cSevCritical, "ERROR: Mode measure is unknow!\r\n");
        return cAtFalse;
        }

    displayStatisticResult = cAtTrue;
    if (mode == cAtBerMeasureModeManaual)
        {
        AtBerController controller = AtBerMeasureTimeEngineControllerGet(engine);
        if (AtBerControllerIsSf(controller))
            {
            AtPrintc(cSevCritical, "ERROR: Hardware in SF alarm, not ready to show all result!\r\n");
            return cAtFalse;
            }
        if (AtBerControllerIsSd(controller))
            {
            AtPrintc(cSevCritical, "ERROR: Hardware in SD alarm, not ready to show all result!\r\n");
            return cAtFalse;
            }
        if (AtBerControllerIsTca(controller))
            {
            AtPrintc(cSevCritical, "ERROR: Hardware in TCA alarm, not ready to show all result!\r\n");
            return cAtFalse;
            }

        displayStatisticResult = cAtFalse;
        }

    return MeasureTimeEngineShow(argv[0], engine, displayStatisticResult);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : CliAtPtchService.c
 *
 * Created Date: May 7, 2016
 *
 * Description : PTCH service CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleEth.h"
#include "AtPtchService.h"
#include "AtCisco.h"
#include "CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeSetFunction)(AtPtchService self, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPtchServiceStr[] = {
                                    "cem",
                                    "imsg_eop",
                                    "exaui0",
                                    "exaui1",
                                    "gebypass"
};

static const eAtPtchServiceType cAtPtchServiceValue[] = {
                                                  cAtPtchServiceTypeCEM,
                                                  cAtPtchServiceTypeIMSG_EoP,
                                                  cAtPtchServiceTypeEXAUI_0,
                                                  cAtPtchServiceTypeEXAUI_1,
                                                  cAtPtchServiceTypeGeBypass
};

static const char * cAtPtchServiceCountersStr[] = {
                                    "TxBytes",
                                    "TxFrames",
                                    "TxOversizeFrames",
                                    "RxBytes",
                                    "RxFrames",
                                    "RxOversizeFrames",
                                    "RxErrorFrames",
                                    "RxGoodFrames",
                                    "RxDropFrames"
};
static const eAtPtchServiceCounterType cAtPtchServiceCountersVal[] = {
                                            cAtPtchServiceCounterTypeTxBytes,
                                            cAtPtchServiceCounterTypeTxFrames,
                                            cAtPtchServiceCounterTypeTxOversizeFrames,
                                            cAtPtchServiceCounterTypeRxBytes,
                                            cAtPtchServiceCounterTypeRxFrames,
                                            cAtPtchServiceCounterTypeRxOversizeFrames,
                                            cAtPtchServiceCounterTypeRxErrorFrames,
                                            cAtPtchServiceCounterTypeRxGoodFrames,
                                            cAtPtchServiceCounterTypeRxDropFrames
};

static const char *cPtchInsertModeStr[]    = {"1-byte", "2-bytes"};
static const uint32 cPtchInsertModeValue[] = {cAtPtchModeOneByte, cAtPtchModeTwoBytes};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static AtPtchService PtchService(const char *serviceTypeString)
    {
    eBool convertSuccess = cAtFalse;
    eAtPtchServiceType serviceType = CliStringToEnum(serviceTypeString, cAtPtchServiceStr, cAtPtchServiceValue, mCount(cAtPtchServiceValue), &convertSuccess);
    AtModuleEth eth = EthModule();
    AtPtchService ptch;

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: PTCH service %s may be invalid or not supported. Expected: ", serviceTypeString);
        CliExpectedValuesPrint(cSevCritical, cAtPtchServiceStr, mCount(cAtPtchServiceStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ptch = AtModuleEthPtchServiceGet(eth, serviceType);
    if (ptch == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: PTCH service %s may be not supported\r\n", serviceTypeString);
        return cAtFalse;
        }

    return AtModuleEthPtchServiceGet(eth, serviceType);
    }

static eBool AttributeSet(char argc, char** argv, AttributeSetFunction setFunction)
    {
    AtPtchService ptch = PtchService(argv[0]);
    uint32 offset = (uint32)AtStrToDw(argv[1]);
    eAtRet ret;

    if (ptch == NULL)
        return cAtFalse;

    AtUnused(argc);

    ret = setFunction(ptch, offset);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Configure offset fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool PtchSet(char argc, char** argv, eAtRet (*PtchSetFunction)(AtPtchService self, uint16 ptchId))
    {
    AtPtchService ptch = PtchService(argv[0]);
    uint16 ptchId = (uint16)AtStrToDw(argv[1]);
    eAtRet ret;

    if (ptch == NULL)
        return cAtFalse;

    AtUnused(argc);

    ret = PtchSetFunction(ptch, ptchId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Configure PTCH fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eAtSevLevel PtchCounterColorSet(eAtPtchServiceCounterType counterType, uint32 counter)
    {
    eAtSevLevel color;
    color = (((counterType==cAtPtchServiceCounterTypeTxOversizeFrames)||
             (counterType==cAtPtchServiceCounterTypeRxOversizeFrames)||
             (counterType==cAtPtchServiceCounterTypeRxErrorFrames)||
             (counterType==cAtPtchServiceCounterTypeRxDropFrames))&&(counter > 0))? cSevCritical: cSevInfo;
    return color;
    }

static eBool Enable(char argc, char** argv, eBool enabled)
    {
    AtPtchService ptch = PtchService(argv[0]);
    eAtRet ret;

    AtUnused(argc);

    if (ptch == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown PTCH service %s\r\n", argv[0]);
        return cAtFalse;
        }

    ret = AtPtchServiceEnable(ptch, enabled);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: %s PTCH service %s fail with ret = %s\r\n", enabled ? "Enable" : "Disable", argv[0], AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliAtPtchServiceIngressModeSet(AtPtchService (*PtchServiceGet)(const char *), char argc, char **argv)
    {
    eAtPtchMode insertMode;
    AtPtchService ptch;
    eBool success;
    eAtRet ret;

    AtUnused(argc);
    ptch = PtchServiceGet(argv[0]);
    if (ptch == NULL)
        return cAtFalse;

    /* Get PTCH insertion mode */
    insertMode = CliStringToEnum(argv[1], cPtchInsertModeStr, cPtchInsertModeValue, mCount(cPtchInsertModeStr), &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: PTCH insertion mode %s may be invalid or not supported. Expected: ", argv[1]);
        CliExpectedValuesPrint(cSevCritical, cPtchInsertModeStr, mCount(cPtchInsertModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtPtchServiceIngressModeSet(ptch, insertMode);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Configure PTCH fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtPtchServiceIngressOffsetSet(char argc, char** argv)
    {
    return AttributeSet(argc, argv, AtPtchServiceIngressOffsetSet);
    }

eBool CmdAtPtchServiceEgressOffsetSet(char argc, char** argv)
    {
    return AttributeSet(argc, argv, AtPtchServiceEgressOffsetSet);
    }

eBool CmdAtPtchServiceIngressPtchIdSet(char argc, char** argv)
    {
    return PtchSet(argc, argv, AtPtchServiceIngressPtchSet);
    }

eBool CmdAtPtchServiceEgressPtchIdSet(char argc, char** argv)
    {
    return PtchSet(argc, argv, AtPtchServiceEgressPtchSet);
    }

eBool CmdAtPtchServiceShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"Service", "Ingress_Offset", "Ingress_Ptch", "Egress_Offset", "Egress_Ptch", "Mode", "Enabled"};
    uint32 service_i;
    AtPtchService ptch;
    AtModuleEth eth;
    uint32 offset;
    uint16 ptchId;
    eBool convertSuccess;
    eAtPtchServiceType service;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    tabPtr = TableAlloc(mCount(cAtPtchServiceStr), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    eth = EthModule();
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Eth Module is NULL\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (service_i = 0; service_i < mCount(cAtPtchServiceStr); service_i++)
        {
        eBool enabled;
        uint8 column = 0;
        const char *string;

        service = service_i + 1;

        /* Print type of service */
        StrToCell(tabPtr, service_i, column++, CliEnumToString(service, cAtPtchServiceStr, cAtPtchServiceValue, mCount(cAtPtchServiceValue), &convertSuccess));

        ptch = AtModuleEthPtchServiceGet(eth, service);
        if (ptch == NULL)
            {
            for (; column < mCount(heading); column++)
                StrToCell(tabPtr, service_i, column, "N/A");
            continue;
            }

        /* Print Ingress Offset */
        offset = AtPtchServiceIngressOffsetGet(ptch);
        if (service != cAtPtchServiceTypeEXAUI_0)
            ColorStrToCell(tabPtr, service_i, column++, "N/A", cSevNormal);
        else
            ColorStrToCell(tabPtr, service_i, column++, CliNumber2String(offset, "0x%08x"), cSevInfo);

        /* Print Ingress PTCH */
        ptchId = AtPtchServiceIngressPtchGet(ptch);
        if (service == cAtPtchServiceTypeEXAUI_0)
            ColorStrToCell(tabPtr, service_i, column++, "N/A", cSevNormal);
        else
            ColorStrToCell(tabPtr, service_i, column++, CliNumber2String(ptchId, "0x%04x"), cSevInfo);

        /* Print Egress Offset */
        offset = AtPtchServiceEgressOffsetGet(ptch);
        if (service != cAtPtchServiceTypeEXAUI_0)
            ColorStrToCell(tabPtr, service_i, column++, "N/A", cSevNormal);
        else
            ColorStrToCell(tabPtr, service_i, column++, CliNumber2String(offset, "0x%08x"), cSevInfo);

        /* Print Egress PTCH */
        ptchId = AtPtchServiceEgressPtchGet(ptch);
        ColorStrToCell(tabPtr, service_i, column++, CliNumber2String(ptchId, "0x%02x"), cSevInfo);

        string = CliEnumToString(AtPtchServiceIngressModeGet(ptch),
                                 cPtchInsertModeStr,
                                 cPtchInsertModeValue,
                                 mCount(cPtchInsertModeStr),
                                 &convertSuccess);
        ColorStrToCell(tabPtr, service_i, column++, convertSuccess ? string : "Unknown", cSevInfo);

        /* Enabling */
        enabled = AtPtchServiceIsEnabled(ptch);
        ColorStrToCell(tabPtr, service_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPtchServiceCounterShow(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *heading[] = {"Counter", "CEM", "IMSG/EOP", "EXAUI_0", "EXAUI_1"};
    AtPtchService ptch;
    AtModuleEth eth;
    uint32 counter, idx, ser_i, column;
    eAtHistoryReadingMode readingMode;
    eBool convertSuccess;
    uint8 color;

    /* Get reading mode. */
    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(mCount(cAtPtchServiceCountersStr), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    eth = EthModule();
    if(eth == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Eth Module is NULL\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for(idx = 0; idx <= mCount(cAtPtchServiceCountersStr); idx++)
        {
        column = 0;
        /* Print type of counters */
        StrToCell(tabPtr, idx, column, CliEnumToString(idx, cAtPtchServiceCountersStr, cAtPtchServiceCountersVal, mCount(cAtPtchServiceCountersStr), &convertSuccess));
        column++;

        for(ser_i = 1; ser_i <= mCount(cAtPtchServiceStr); ser_i++)
            {
            ptch = AtModuleEthPtchServiceGet(eth, ser_i);
            if(ptch == NULL)
                continue;
            counter = (readingMode == cAtHistoryReadingModeReadOnly)? AtPtchServiceCounterGet(ptch, idx) : AtPtchServiceCounterClear(ptch, idx);
            color = PtchCounterColorSet(idx, counter);
            ColorStrToCell(tabPtr, idx, column, CliNumber2String(counter, "%u"), color);
            column++;
            }

        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtUnused(argc);
    return cAtTrue;
    }

eBool CmdAtPtchServiceEnable(char argc, char** argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPtchServiceDisable(char argc, char** argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPtchServiceIngressModeSet(char argc, char **argv)
    {
    return CliAtPtchServiceIngressModeSet(PtchService, argc, argv);
    }

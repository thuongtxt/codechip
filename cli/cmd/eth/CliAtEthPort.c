/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtEthPort.c
 *
 * Created Date: Oct 10, 2012
 *
 * Description : CLI of ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleEth.h"
#include "AtEthFlowControl.h"
#include "AtNumber.h"
#include "AtCliModule.h"
#include "AtClockMonitor.h"
#include "../physical/CliAtSerdesController.h"
#include "../textui/CliAtTextUI.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../clock/CliAtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterType, counterKind, counterName)                     \
    do                                                                         \
        {                                                                      \
        if (AtChannelCounterIsSupported(port, counterType))                    \
            CliCounterTypePrintToCell(tabPtr, rowId++, i, CounterGet(port, counterType), counterKind, counterName); \
        else                                                                   \
            CliCounterTypePrintToCell(tabPtr, rowId++, i, 0, cAtCounterTypeNotSupport, counterName); \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*ApsSwBrFunc)(AtEthPort self, AtEthPort toPort);
typedef eAtRet (*EthPortApsReleaseFunc)(AtEthPort self);
typedef eAtRet (*SerdesSetFunc)(AtEthPort self, AtSerdesController serdes);
typedef eAtRet (*EnableFunc)(AtChannel self, eBool enabled);

typedef struct tCliEthPortDefect
    {
    uint32 defect;
    const char *name;
    }tCliEthPortDefect;
typedef struct tCliEthPortDefect  * CliEthPortDefect;

typedef enum ePortType
    {
    cPortTypeUnknown,
    cPortTypePort,
    cPortTypeSubPort
    }ePortType;

/*--------------------------- Global variables -------------------------------*/
static AtList m_capturedEvents = NULL;
static tAtTextUIListerner* m_EventAlarmListener = NULL;

/*--------------------------- Local variables --------------------------------*/
static const char * cAtBoolStr[] = {
                            "en",
                            "dis"
                            };

static const eBool cAtBoolVal[]  = {
                             cAtTrue,
                             cAtFalse
                             };

static const char * cAtEthPortSpeedMdStr[] = {
                                     "10m",
                                     "100m",
                                     "1000m",
                                     "2500m",
                                     "10g",
                                     "40g",
                                     sAtNotApplicable
                                     };

static const uint32 cAtEthPortSpeedMdVal[]  = {
                                              cAtEthPortSpeed10M,
                                              cAtEthPortSpeed100M,
                                              cAtEthPortSpeed1000M,
                                              cAtEthPortSpeed2500M,
                                              cAtEthPortSpeed10G,
                                              cAtEthPortSpeed40G,
                                              cAtEthPortSpeedNA
                                              };

static const char * cAtEthPortDuplexMdStr[] = {
                                     "half-duplex",
                                     "full-duplex",
                                     "auto-detect"
                                     };

static const uint32 cAtEthPortDuplexMdVal[]  = {cAtEthPortWorkingModeHalfDuplex,
                                                cAtEthPortWorkingModeFullDuplex,
                                                cAtEthPortWorkingModeAutoDetect
                                                };

static const char * cAtEthPortInterfaceMdStr[] = {
                                          "1000basex",
                                          "2500basex",
                                          "sgmii",
                                          "qsgmii",
                                          "rgmii",
                                          "gmii",
                                          "mii",
                                          "xgmii",
                                          "xaui",
                                          "base-r",
                                          "base-kr",
                                          "base-fx",
                                          sAtNotApplicable
                                         };

static const eAtEthPortInterface cAtEthPortInterfaceMdVal[]  = {
                                                         cAtEthPortInterface1000BaseX,
                                                         cAtEthPortInterface2500BaseX,
                                                         cAtEthPortInterfaceSgmii,
                                                         cAtEthPortInterfaceQSgmii,
                                                         cAtEthPortInterfaceRgmii,
                                                         cAtEthPortInterfaceGmii,
                                                         cAtEthPortInterfaceMii,
                                                         cAtEthPortInterfaceXGMii,
                                                         cAtEthPortInterfaceXAUI,
                                                         cAtEthPortInterfaceBaseR,
                                                         cAtEthPortInterfaceBaseKR,
                                                         cAtEthPortInterfaceBaseFx,
                                                         cAtEthPortInterfaceNA
                                                       };

static const char * cAtEthPortLedStateStr[] = {
                                      "off",
                                      "on",
                                      "blink"
                                     };

static const eAtLedState cAtEthPortLedStateVal[]  = {
                                              cAtLedStateOff,
                                              cAtLedStateOn,
                                              cAtLedStateBlink
                                             };

static const char *cAtEthPortLoopbackStr[] = {
                                       "release",
                                       "local",
                                       "remote"
                                        };

static const eAtLoopbackMode cAtEthPortLoopbackVal[] = {
                                                        cAtLoopbackModeRelease,
                                                        cAtLoopbackModeLocal,
                                                        cAtLoopbackModeRemote,
                                                        };

static const char *cAtEthPortAlarmtypeStr[] = {
                                       "up",
                                       "down",
                                       "los",
                                       "tx-fault",
                                       "tx-underflow-error",
                                       "local-fault",
                                       "remote-fault",
                                       "rx-internal-fault",
                                       "rx-received-local-fault",
                                       "rx-aligned-error",
                                       "rx-mis-aligned",
                                       "rx-truncated",
                                       "rx-hi-ber",
                                       "excessive-error-ratio",
                                       "loss-of-clock",
                                       "frequency-out-of-range",
                                       "loss-of-data-sync",
                                       "loss-of-frame",
                                       "auto-neg-parallel-detection-fault",
                                       "auto-neg-state-change",
                                       "rx-framing-error",
                                       "auto-neg-fec-inc-cant-correct",
                                       "auto-neg-fec-inc-correct",
                                       "auto-neg-fec-lock-error"
                                        };

static const uint32 cAtEthPortAlarmtypeValue[] = {
                                    cAtEthPortAlarmLinkUp,
                                    cAtEthPortAlarmLinkDown,
                                    cAtEthPortAlarmLos,
                                    cAtEthPortAlarmTxFault,
                                    cAtEthPortAlarmTxUnderflowError,
                                    cAtEthPortAlarmLocalFault,
                                    cAtEthPortAlarmRemoteFault,
                                    cAtEthPortAlarmRxInternalFault,
                                    cAtEthPortAlarmRxReceivedLocalFault,
                                    cAtEthPortAlarmRxAlignedError,
                                    cAtEthPortAlarmRxMisAligned,
                                    cAtEthPortAlarmRxTruncated,
                                    cAtEthPortAlarmRxHiBer,
                                    cAtEthPortAlarmExcessiveErrorRatio,
                                    cAtEthPortAlarmLossofClock,
                                    cAtEthPortAlarmFrequencyOutofRange,
                                    cAtEthPortAlarmLossofDataSync,
                                    cAtEthPortAlarmLossofFrame,
                                    cAtEthPortAlarmAutoNegParallelDetectionFault,
                                    cAtEthPortAlarmAutoNegStateChange,
                                    cAtEthPortAlarmRxFramingError,
                                    cAtEthPortAlarmAutoNegFecIncCantCorrect,
                                    cAtEthPortAlarmAutoNegFecIncCorrect,
                                    cAtEthPortAlarmAutoNegFecLockError
                                    };

static const uint32 cAtEthSubPortAlarmTypeVal[] = {cAtEthSubPortAlarmRxFramingError,
                                                   cAtEthSubPortAlarmRxSyncedError,
                                                   cAtEthSubPortAlarmRxMfLenError,
                                                   cAtEthSubPortAlarmRxMfRepeatError,
                                                   cAtEthSubPortAlarmRxMfError,
                                                   cAtEthSubPortAlarmRxBipError,
                                                   cAtEthSubPortAlarmAutoNegFecIncCantCorrect,
                                                   cAtEthSubPortAlarmAutoNegFecIncCorrect,
                                                   cAtEthSubPortAlarmAutoNegFecLockError
                                                   };

static const char *cAtEthSubPortAlarmTypeStr[] = {"rx-framing-error",
                                                  "rx-synced-error",
                                                  "rx-mf-len-error",
                                                  "rx-mf-repeat-error",
                                                  "rx-mf-error",
                                                  "rx-bip-error",
                                                  "auto-neg-fec-inc-cant-correct",
                                                  "auto-neg-fec-inc-correct",
                                                  "auto-neg-fec-lock-error"};

static const char *cAtEthPortDropConditionStr[] = {
                                                   "none",
                                                   "pcs-error",
                                                   "fcs-error",
                                                   "undersized",
                                                   "oversized",
                                                   "pause-frame",
                                                   "loop-da"
                                                    };

static const uint32 cAtEthPortDropConditionValue[] = {
                                                      cAtEthPortDropConditionNone,
                                                      cAtEthPortDropConditionPcsError,
                                                      cAtEthPortDropConditionPktFcsError,
                                                      cAtEthPortDropConditionPktUnderSize,
                                                      cAtEthPortDropConditionPktOverSize,
                                                      cAtEthPortDropConditionPktPauseFrame,
                                                      cAtEthPortDropConditionPktLoopDa
                                                    };

/*--------------------------- Forward declarations ---------------------------*/
static const char *RemoteFault2String(eAtEthPortRemoteFault remoteFault);

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortGet(AtModuleEth ethModule, uint32 portId)
    {
    return AtModuleEthPortGet(ethModule, (uint8)portId);
    }

static eBool EthPortIdIsValid(AtModuleEth ethModule, uint32 portId)
    {
    return (portId < AtModuleEthMaxPortsGet(ethModule)) ? cAtTrue : cAtFalse;
    }

static eBool Enable(char argc, char **argv, eBool enable, EnableFunc enableFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get the shared ID buffer */
	channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtChannel port = channelList[i];
        eAtRet ret;

        ret = enableFunc(port, enable);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s %s fail with ret = %s\r\n",
                 enable ? "Enable" : "Disable",
                 CliChannelIdStringGet((AtChannel)port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool SerdesControllerEnable(char argc, char **argv, eBool enable)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerEnable(controllers, enable);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

static eBool PktSizeSet(char argc, char **argv, eAtModuleEthRet (*PacketSizeSet)(AtEthPort self, uint32 packetSize))
    {
    uint32 bufferSize;
    uint8 numPorts, i;
    uint32 numberPacketSize;
    AtChannel *channelList;
    eBool success = cAtTrue;
	AtUnused(argc);

	/* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }
        
    /* Get maximum number packet */
    numberPacketSize = (uint32)AtStrToDw(argv[1]);

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = PacketSizeSet((AtEthPort)channelList[i], numberPacketSize);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot limit packet size on port %u, ret = %s\r\n",
                     AtChannelIdGet(channelList[i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool ApsSwBr(uint32 fromPortId, uint32 toPortId, ApsSwBrFunc swBrFunc)
    {
    eAtRet ret;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    AtEthPort   fromPort  = PortGet(ethModule, fromPortId - 1);
    AtEthPort   toPort    = PortGet(ethModule, toPortId - 1);

    ret = swBrFunc(fromPort, toPort);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot switch/bridge from port %d to port %d, ret = %s\r\n",
                 fromPortId,
                 toPortId,
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ApsRelease(char *portListString, EthPortApsReleaseFunc apsReleaseFunc)
    {
    uint32 bufferSize, numPorts, i;
    uint32 *portIds;
    AtEthPort ethPort;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    eBool success = cAtTrue;

    /* Get list of ports */
    portIds  = CliSharedIdBufferGet(&bufferSize);
    numPorts = CliIdListFromString(portListString, portIds, bufferSize);
    if (portIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No port, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Apply configure for all input ports */
    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret;

        ethPort = PortGet(ethModule, portIds[i] - 1);
        ret = apsReleaseFunc(ethPort);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot release switch/bridge on port %u, ret = %s\r\n",
                     portIds[i],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool HigigEnable(char argc, char **argv, eBool enable)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eAtRet ret;
	AtUnused(argc);

	/* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        ret = AtEthPortHiGigEnable((AtEthPort)channelList[i], enable);
        if (ret != cAtOk)
            AtPrintc(cSevWarning, "Ethernet port flow control %u cannot be enable, ret = %d\r\n", AtChannelIdGet(channelList[i]) + 1, ret);
        }

    return cAtTrue;
    }

static eBool SerdesSet(uint32 portId, uint32 serdesId, SerdesSetFunc setFunc)
    {
    eAtRet ret;
    AtEthPort port;
    AtSerdesController serdes;
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Get Port Object */
    port = PortGet(ethModule, portId - 1);
    if (port == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Port ID, expect start from 1\r\n");
        return cAtFalse;
        }

    /* Get SERDES Object */
    if (serdesId == cInvalidUint32)
        serdes = NULL;
    else
        {
        serdes = AtModuleEthSerdesController(ethModule, serdesId - 1);
        if (serdes == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid SERDES ID, expect start from 1\r\n");
            return cAtFalse;
            }
        }

    ret = setFunc(port, serdes);
    if (ret == cAtOk)
        return cAtTrue;

    if (serdesId == cInvalidUint32)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Configure SERDES \"none\" on port %u fail; ret = %s\r\n",
                 portId,
                 AtRet2String(ret));
        }
    else
        {
        AtPrintc(cSevCritical,
                 "ERROR: Configure SERDES %u on port %u fail; ret = %s\r\n",
                 serdesId,
                 portId,
                 AtRet2String(ret));
        }

    return cAtFalse;
    }

static eBool SerdesBridgeRelease(uint32 portId)
    {
    eAtRet ret;
    AtEthPort port = PortGet((AtModuleEth) AtDeviceModuleGet(CliDevice(), cAtModuleEth), portId - 1);
    if (port == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Port ID, expect start from 1\r\n");
        return cAtFalse;
        }

    ret = AtEthPortTxSerdesBridge(port, NULL);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot release bridge on port %d, ret = %s\r\n",
                 portId,
                 AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool PowerDown(char argc, char **argv, eBool powerDown)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerPowerDown(controllers, powerDown);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

static eBool Ipv6AddrStr2Byte(const char *pIPv6AddrStr, uint8 *pIpv6Addr)
    {
    uint32 numBytes = AtString2Bytes(pIPv6AddrStr, pIpv6Addr, cAtIpv6AddressLen, 10);
    return (numBytes == cAtIpv6AddressLen) ? cAtTrue : cAtFalse;
    }

static void HeadingFree(char** heading, uint8 numItem)
    {
    uint8 i;
    for (i = 0; i < numItem; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    AtChannel *channelList;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        ret = AtChannelLogEnable(channelList[i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable logging for port %u, ret = %s\r\n",
                     AtChannelIdGet(channelList[i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static const char *LinkStatus2String(eAtEthPortLinkStatus linkStatus)
    {
    static uint32 cAtEthPortLinkStatusVal[] = {cAtEthPortLinkStatusDown,
                                               cAtEthPortLinkStatusUp,
                                               cAtEthPortLinkStatusUnknown,
                                               cAtEthPortLinkStatusNotSupported};
    static const char *cAtEthPortLinkStatusStr[] = {
                                                   "down",
                                                   "up",
                                                   "unknown",
                                                   sAtNotSupported
                                                   };

    return CliEnumToString(linkStatus, cAtEthPortLinkStatusStr, cAtEthPortLinkStatusVal, mCount(cAtEthPortLinkStatusVal), NULL);
    }

static eAtSevLevel LinkStatusColor(eAtEthPortLinkStatus linkStatus)
    {
    switch (linkStatus)
        {
        case cAtEthPortLinkStatusDown        : return cSevCritical;
        case cAtEthPortLinkStatusUp          : return cSevInfo;
        case cAtEthPortLinkStatusNotSupported: return cSevNormal;
        case cAtEthPortLinkStatusUnknown:
        default:
            return cSevWarning;
        }
    }


static const char *AutoNegStateString(eAtEthPortAutoNegState state)
    {
    static const uint32 cAtEthPortAutoNegStateVal[] = {
                                                       cAtEthPortAutoNegNotSupported,
                                                       cAtEthPortAutoNegEnable,
                                                       cAtEthPortAutoNegRestart,
                                                       cAtEthPortAutoNegAbilityDetect,
                                                       cAtEthPortAutoNegAckDetect,
                                                       cAtEthPortAutoNegNextPageWait,
                                                       cAtEthPortAutoNegCompleteAck,
                                                       cAtEthPortAutoNegIdleDetect,
                                                       cAtEthPortAutoNegLinkOk,
                                                       cAtEthPortAutoNegDisableLinkOk,
                                                       cAtEthPortAutoNegInvalid
                                                      };

    static const char *cAtEthPortAutoNegStateStr[] = {
                                                      sAtNotSupported,
                                                      "enable",
                                                      "restart",
                                                      "ability-detect",
                                                      "ack-detect",
                                                      "next-page-wait",
                                                      "complete-ack",
                                                      "idle-detect",
                                                      "link-ok",
                                                      "disable-link-ok",
                                                      "invalid"
                                                      };
    return CliEnumToString(state, cAtEthPortAutoNegStateStr, cAtEthPortAutoNegStateVal, mCount(cAtEthPortAutoNegStateVal), NULL);
    }

static eAtModuleEthRet AutoNegRestartTrigger(AtEthPort self, eBool unused)
    {
    eAtRet ret = cAtOk;

    AtUnused(unused);

    ret |= AtEthPortAutoNegRestartOn(self, cAtTrue);
    ret |= AtEthPortAutoNegRestartOn(self, cAtFalse);

    return ret;
    }

static eBool AutoNegRestartWithHandler(char argc, char **argv,
                                       eAtModuleEthRet (*AutoNegRestartHandler)(AtEthPort, eBool),
                                       eBool restart)
    {
    eBool success = cAtTrue;
    uint32 *idBuf;
    uint32 bufferSize, numPorts, port_i;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = PortGet(ethModule, idBuf[port_i] - 1);
        eAtRet ret;

        ret = AutoNegRestartHandler(port, restart);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s autoneg fail on %s with ret = %s\r\n",
                 restart ? "Restart" : "Stop restarting",
                 CliChannelIdStringGet((AtChannel)port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static const char *Ret2String(eAtRet ret)
    {
    switch ((uint32)ret)
        {
        case cAtErrorNotApplicable:  return sAtNotApplicable;
        case cAtErrorModeNotSupport: return sAtNotSupported;
        case cAtErrorNotImplemented: return sAtNotSupported;
        default:
            return AtRet2String(ret);
        }
    }

static tCliEthPortCounter *AllPossibleCounters(uint32 *numPossibleCounters)
    {
    static tCliEthPortCounter counters[] =
        {
        {cAtEthPortCounterTxPackets,               cAtCounterTypeGood,    "txPackets"},
        {cAtEthPortCounterTxBytes,                 cAtCounterTypeGood,    "txBytes"},
        {cAtEthPortCounterTxGoodPackets,           cAtCounterTypeGood,    "txGoodPackets"},
        {cAtEthPortCounterTxGoodBytes,             cAtCounterTypeGood,    "txGoodBytes"},
        {cAtEthPortCounterTxOamPackets,            cAtCounterTypeNeutral, "txOamPackets"},
        {cAtEthPortCounterTxPeriodOamPackets,      cAtCounterTypeNeutral, "txPeriodOamPackets"},
        {cAtEthPortCounterTxPwPackets,             cAtCounterTypeNeutral, "txPwPackets"},
        {cAtEthPortCounterTxPacketsLen64,          cAtCounterTypeNeutral, "txPacketsLen64"},
        {cAtEthPortCounterTxPacketsLen0_64,        cAtCounterTypeNeutral, "txPacketsLen0_64"},
        {cAtEthPortCounterTxPacketsLen65_127,      cAtCounterTypeNeutral, "txPacketsLen65_127"},
        {cAtEthPortCounterTxPacketsLen128_255,     cAtCounterTypeNeutral, "txPacketsLen128_255"},
        {cAtEthPortCounterTxPacketsLen256_511,     cAtCounterTypeNeutral, "txPacketsLen256_511"},
        {cAtEthPortCounterTxPacketsLen512_1024,    cAtCounterTypeNeutral, "txPacketsLen512_1024"},
        {cAtEthPortCounterTxPacketsLen1025_1528,   cAtCounterTypeNeutral, "txPacketsLen1025_1528"},
        {cAtEthPortCounterTxPacketsLen1529_2047,   cAtCounterTypeNeutral, "txPacketsLen1529_2047"},
        {cAtEthPortCounterTxPacketsJumbo,          cAtCounterTypeNeutral, "txPacketsJumbo"},
        {cAtEthPortCounterTxTopPackets,            cAtCounterTypeNeutral, "txTopPackets"},
        {cAtEthPortCounterTxPausePackets,          cAtCounterTypeNeutral, "txPauFramePackets"},
        {cAtEthPortCounterTxMultCastPackets,       cAtCounterTypeNeutral, "txMultCastPackets"},
        {cAtEthPortCounterTxUniCastPackets,        cAtCounterTypeNeutral, "txUniCastPackets"},
        {cAtEthPortCounterTxBrdCastPackets,        cAtCounterTypeNeutral, "txBrdCastPackets"},
        {cAtEthPortCounterTxOversizePackets,       cAtCounterTypeError,   "txOversizePackets"},
        {cAtEthPortCounterTxUndersizePackets,      cAtCounterTypeError,   "txUndersizePackets"},
        {cAtEthPortCounterTxDiscardedPackets,      cAtCounterTypeError,   "txDiscardedPackets"},
        {cAtEthPortCounterTxOverFlowDroppedPackets,cAtCounterTypeError,   "txOverFlowDroppedPackets"},
        {cAtEthPortCounterRxPackets,               cAtCounterTypeGood,    "rxPackets"},
        {cAtEthPortCounterRxBytes,                 cAtCounterTypeGood,    "rxBytes"},
        {cAtEthPortCounterRxGoodPackets,           cAtCounterTypeGood,    "rxGoodPackets"},
        {cAtEthPortCounterRxGoodBytes,             cAtCounterTypeGood,    "rxGoodBytes"},
        {cAtEthPortCounterRxErrEthHdrPackets,      cAtCounterTypeError,   "rxErrEthHdrPackets"},
        {cAtEthPortCounterRxErrBusPackets,         cAtCounterTypeError,   "rxErrBusPackets"},
        {cAtEthPortCounterRxErrFcsPackets,         cAtCounterTypeError,   "rxErrFcsPackets"},
        {cAtEthPortCounterRxOversizePackets,       cAtCounterTypeError,   "rxOversizePackets"},
        {cAtEthPortCounterRxUndersizePackets,      cAtCounterTypeError,   "rxUndersizePackets"},
        {cAtEthPortCounterRxPacketsLen64,          cAtCounterTypeNeutral, "rxPacketsLen64"},
        {cAtEthPortCounterRxPacketsLen0_64,        cAtCounterTypeNeutral, "rxPacketsLen0_64"},
        {cAtEthPortCounterRxPacketsLen65_127,      cAtCounterTypeNeutral, "rxPacketsLen65_127"},
        {cAtEthPortCounterRxPacketsLen128_255,     cAtCounterTypeNeutral, "rxPacketsLen128_255"},
        {cAtEthPortCounterRxPacketsLen256_511,     cAtCounterTypeNeutral, "rxPacketsLen256_511"},
        {cAtEthPortCounterRxPacketsLen512_1024,    cAtCounterTypeNeutral, "rxPacketsLen512_1024"},
        {cAtEthPortCounterRxPacketsLen1025_1528,   cAtCounterTypeNeutral, "rxPacketsLen1025_1528"},
        {cAtEthPortCounterRxPacketsLen1529_2047,   cAtCounterTypeNeutral, "rxPacketsLen1529_2047"},
        {cAtEthPortCounterRxPacketsJumbo,          cAtCounterTypeNeutral, "rxPacketsJumbo"},
        {cAtEthPortCounterRxPwUnsupportedPackets,  cAtCounterTypeError,   "rxPwUnsupportedPackets"},
        {cAtEthPortCounterRxErrPwLabelPackets,     cAtCounterTypeError,   "rxErrPwLabelPackets"},
        {cAtEthPortCounterRxDiscardedPackets,      cAtCounterTypeError,   "rxDiscardedPackets"},
        {cAtEthPortCounterRxPausePackets,          cAtCounterTypeNeutral, "rxPausePackets"},
        {cAtEthPortCounterRxErrPausePackets,       cAtCounterTypeError,   "rxErrPausePackets"},
        {cAtEthPortCounterRxBrdCastPackets,        cAtCounterTypeNeutral, "rxBrdCastPackets"},
        {cAtEthPortCounterRxMultCastPackets,       cAtCounterTypeNeutral, "rxMultCastPackets"},
        {cAtEthPortCounterRxUniCastPackets,        cAtCounterTypeNeutral, "rxUniCastPackets"},
        {cAtEthPortCounterRxArpPackets,            cAtCounterTypeNeutral, "rxArpPackets"},
        {cAtEthPortCounterRxOamPackets,            cAtCounterTypeNeutral, "rxOamPackets"},
        {cAtEthPortCounterRxEfmOamPackets,         cAtCounterTypeNeutral, "rxEfmOamPackets"},
        {cAtEthPortCounterRxErrEfmOamPackets,      cAtCounterTypeError,   "rxErrEfmOamPackets"},
        {cAtEthPortCounterRxEthOamType1Packets,    cAtCounterTypeNeutral, "rxEthOamType1Packets"},
        {cAtEthPortCounterRxEthOamType2Packets,    cAtCounterTypeNeutral, "rxEthOamType2Packets"},
        {cAtEthPortCounterRxIpv4Packets,           cAtCounterTypeNeutral, "rxIpv4Packets"},
        {cAtEthPortCounterRxErrIpv4Packets,        cAtCounterTypeError,   "rxErrIpv4Packets"},
        {cAtEthPortCounterRxIcmpIpv4Packets,       cAtCounterTypeNeutral, "rxIcmpIpv4Packets"},
        {cAtEthPortCounterRxIpv6Packets,           cAtCounterTypeNeutral, "rxIpv6Packets"},
        {cAtEthPortCounterRxErrIpv6Packets,        cAtCounterTypeError,   "rxErrIpv6Packets"},
        {cAtEthPortCounterRxIcmpIpv6Packets,       cAtCounterTypeNeutral, "rxIcmpIpv6Packets"},
        {cAtEthPortCounterRxMefPackets,            cAtCounterTypeNeutral, "rxMefPackets"},
        {cAtEthPortCounterRxErrMefPackets,         cAtCounterTypeError,   "rxErrMefPackets"},
        {cAtEthPortCounterRxMplsPackets,           cAtCounterTypeNeutral, "rxMplsPackets"},
        {cAtEthPortCounterRxErrMplsPackets,        cAtCounterTypeError,   "rxErrMplsPackets"},
        {cAtEthPortCounterRxMplsErrOuterLblPackets,cAtCounterTypeError,   "rxMplsErrOuterLblPackets"},
        {cAtEthPortCounterRxMplsDataPackets,       cAtCounterTypeNeutral, "rxMplsDataPackets"},
        {cAtEthPortCounterRxLdpIpv4Packets,        cAtCounterTypeNeutral, "rxLdpIpv4Packets"},
        {cAtEthPortCounterRxLdpIpv6Packets,        cAtCounterTypeNeutral, "rxLdpIpv6Packets"},
        {cAtEthPortCounterRxMplsIpv4Packets,       cAtCounterTypeNeutral, "rxMplsIpv4Packets"},
        {cAtEthPortCounterRxMplsIpv6Packets,       cAtCounterTypeNeutral, "rxMplsIpv6Packets"},
        {cAtEthPortCounterRxErrL2tpv3Packets,      cAtCounterTypeError,   "rxErrL2tpv3Packets"},
        {cAtEthPortCounterRxL2tpv3Packets,         cAtCounterTypeNeutral, "rxL2tpv3Packets"},
        {cAtEthPortCounterRxL2tpv3Ipv4Packets,     cAtCounterTypeNeutral, "rxL2tpv3Ipv4Packets"},
        {cAtEthPortCounterRxL2tpv3Ipv6Packets,     cAtCounterTypeNeutral, "rxL2tpv3Ipv6Packets"},
        {cAtEthPortCounterRxUdpPackets,            cAtCounterTypeNeutral, "rxUdpPackets"},
        {cAtEthPortCounterRxErrUdpPackets,         cAtCounterTypeError,   "rxErrUdpPackets"},
        {cAtEthPortCounterRxUdpIpv4Packets,        cAtCounterTypeNeutral, "rxUdpIpv4Packets"},
        {cAtEthPortCounterRxUdpIpv6Packets,        cAtCounterTypeNeutral, "rxUdpIpv6Packets"},
        {cAtEthPortCounterRxErrPsnPackets,         cAtCounterTypeError,   "rxErrPsnPackets"},
        {cAtEthPortCounterRxPacketsSendToCpu,      cAtCounterTypeNeutral, "rxPacketsSendToCpu"},
        {cAtEthPortCounterRxPacketsSendToPw,       cAtCounterTypeNeutral, "rxPacketsSendToPw"},
        {cAtEthPortCounterRxTopPackets,            cAtCounterTypeNeutral, "rxTopPackets"},
        {cAtEthPortCounterRxPacketDaMis,           cAtCounterTypeNeutral, "rxPacketDaMis"},
        {cAtEthPortCounterRxBip8Errors,            cAtCounterTypeError,   "rxBip8Errors"},
        {cAtEthPortCounterRxOverFlowDroppedPackets,cAtCounterTypeError,   "rxOverFlowDroppedPackets"},
        {cAtEthPortCounterRxFragmentPackets,       cAtCounterTypeError,   "rxFragmentPackets"},
        {cAtEthPortCounterRxJabberPackets,         cAtCounterTypeError,   "rxJabberPackets"},
        {cAtEthPortCounterRxLoopDaPackets,         cAtCounterTypeError,   "rxLoopDaPackets"},
        {cAtEthPortCounterRxPcsErrorPackets,       cAtCounterTypeError,   "rxPcsErrPackets"},
        {cAtEthPortCounterRxPcsInvalidCount,       cAtCounterTypeError,   "rxPcsInvalidCount"},

        /* XILINX MAC */
        {cAtEthPortCounterTxPacketsLen512_1023,    cAtCounterTypeNeutral, "txPacketsLen512_1023"},
        {cAtEthPortCounterTxPacketsLen1024_1518,   cAtCounterTypeNeutral, "txPacketsLen1024_1518"},
        {cAtEthPortCounterTxPacketsLen1519_1522,   cAtCounterTypeNeutral, "txPacketsLen1519_1522"},
        {cAtEthPortCounterTxPacketsLen1523_1548,   cAtCounterTypeNeutral, "txPacketsLen1523_1548"},
        {cAtEthPortCounterTxPacketsLen1549_2047,   cAtCounterTypeNeutral, "txPacketsLen1549_2047"},
        {cAtEthPortCounterTxPacketsLen2048_4095,   cAtCounterTypeNeutral, "txPacketsLen2048_4095"},
        {cAtEthPortCounterTxPacketsLen4096_8191,   cAtCounterTypeNeutral, "txPacketsLen4096_8191"},
        {cAtEthPortCounterTxPacketsLen8192_9215,   cAtCounterTypeNeutral, "txPacketsLen8192_9215"},
        {cAtEthPortCounterTxPacketsLarge,          cAtCounterTypeNeutral, "txPacketsLarge"},
        {cAtEthPortCounterTxPacketsSmall,          cAtCounterTypeNeutral, "txPacketsSmall"},
        {cAtEthPortCounterTxErrFcsPackets,         cAtCounterTypeError,   "txErrFcsPackets"},
        {cAtEthPortCounterTxErrPackets,            cAtCounterTypeError,   "txErrPackets"},
        {cAtEthPortCounterTxVlanPackets,           cAtCounterTypeNeutral, "txVlanPackets"},
        {cAtEthPortCounterTxUserPausePackets,      cAtCounterTypeNeutral, "txUserPausePackets"},
        {cAtEthPortCounterRxPacketsLen512_1023,    cAtCounterTypeNeutral, "rxPacketsLen512_1023"},
        {cAtEthPortCounterRxPacketsLen1024_1518,   cAtCounterTypeNeutral, "rxPacketsLen1024_1518"},
        {cAtEthPortCounterRxPacketsLen1519_1522,   cAtCounterTypeNeutral, "rxPacketsLen1519_1522"},
        {cAtEthPortCounterRxPacketsLen1523_1548,   cAtCounterTypeNeutral, "rxPacketsLen1523_1548"},
        {cAtEthPortCounterRxPacketsLen1549_2047,   cAtCounterTypeNeutral, "rxPacketsLen1549_2047"},
        {cAtEthPortCounterRxPacketsLen2048_4095,   cAtCounterTypeNeutral, "rxPacketsLen2048_4095"},
        {cAtEthPortCounterRxPacketsLen4096_8191,   cAtCounterTypeNeutral, "rxPacketsLen4096_8191"},
        {cAtEthPortCounterRxPacketsLen8192_9215,   cAtCounterTypeNeutral, "rxPacketsLen8192_9215"},
        {cAtEthPortCounterRxPacketsLarge,          cAtCounterTypeNeutral, "rxPacketsLarge"},
        {cAtEthPortCounterRxPacketsSmall,          cAtCounterTypeNeutral, "rxPacketsSmall"},
        {cAtEthPortCounterRxErrPackets,            cAtCounterTypeError,   "rxErrPackets"},
        {cAtEthPortCounterRxVlanPackets,           cAtCounterTypeNeutral, "rxVlanPackets"},
        {cAtEthPortCounterRxUserPausePackets,      cAtCounterTypeNeutral, "rxUserPausePackets"},
        {cAtEthPortCounterRxPacketsTooLong,        cAtCounterTypeNeutral, "rxPacketsTooLong"},
        {cAtEthPortCounterRxStompedFcsPackets,     cAtCounterTypeNeutral, "rxStompedFcsPackets"},
        {cAtEthPortCounterRxInRangeErrPackets,     cAtCounterTypeError,   "rxInRangeErrPackets"},
        {cAtEthPortCounterRxTruncatedPackets,      cAtCounterTypeError,   "rxTruncatedPackets"},
        {cAtEthPortCounterRxFecIncCorrectCount,    cAtCounterTypeNeutral, "rxFecIncCorrectCount"},
        {cAtEthPortCounterRxFecIncCantCorrectCount,cAtCounterTypeError,   "rxFecIncCantCorrectCount"},
        {cAtEthPortCounterRxFecLockErrorCount,     cAtCounterTypeError,   "rxFecLockErrorCount"},
        };

    if (numPossibleCounters)
        *numPossibleCounters = mCount(counters);

    return counters;
    }

static eBool CounterIsSupported(AtChannel *ports, uint32 numPorts, CliEthPortCounter counterInfo)
    {
    uint32 port_i;

    /* Not to affect auto testing system, just simply put all counters */
    if (AutotestIsEnabled())
        return cAtTrue;

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel port = ports[port_i];
        if (AtChannelCounterIsSupported(port, counterInfo->type))
            return cAtTrue;
        }

    return cAtFalse;
    }

static CliEthPortCounter *AllSupportedCountersDetect(AtChannel *ports, uint32 numPorts, uint32 *numSupportedCounters)
    {
    uint32 numPossibleCounters = 0, possibleCounter_i;
    tCliEthPortCounter *allPosisbleCounters = AllPossibleCounters(&numPossibleCounters);
    uint32 memorySize = sizeof(CliEthPortCounter) * numPossibleCounters;
    CliEthPortCounter *supportedCounters;

    supportedCounters = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(supportedCounters, 0, memorySize);
    if (supportedCounters == NULL)
        return NULL;
    *numSupportedCounters = 0;

    for (possibleCounter_i = 0; possibleCounter_i < numPossibleCounters; possibleCounter_i++)
        {
        CliEthPortCounter counterInfo = &(allPosisbleCounters[possibleCounter_i]);

        if (CounterIsSupported(ports, numPorts, counterInfo))
            {
            supportedCounters[*numSupportedCounters] = counterInfo;
            *numSupportedCounters = *numSupportedCounters + 1;
            }
        }

    return supportedCounters;
    }

static eBool PortCounterGet(AtEthPort *ports, uint32 numPorts, char argc, char **argv)
    {
    uint32 i;
    tTab *tabPtr = NULL;
    char **heading;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    uint32 (*CounterGet)(AtChannel self, uint16 counterType) = AtChannelCounterGet;
    uint32 numSupportedCounters = 0;
    CliEthPortCounter *allSupportedCounters = NULL;
    AtUnused(argc);

    /* Get counter mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtChannelCounterClear;

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPorts + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (i = 1; i <= numPorts; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(3);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPorts; i++)
        AtSprintf((void*)heading[i], "%s", AtChannelIdString((AtChannel)ports[i- 1]));

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
        tabPtr = TableAlloc(0, numPorts + 1UL, (void *)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    allSupportedCounters = AllSupportedCountersDetect((AtChannel *)(void *)ports, numPorts, &numSupportedCounters);
    for (i = 1; i <= numPorts; i++)
        {
        uint32 counter_i;
        uint16 rowId = 0;
        AtChannel port = (AtChannel)ports[i - 1];

        AtChannelAllCountersLatchAndClear(port, (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse);

        for (counter_i = 0; counter_i < numSupportedCounters; counter_i++)
            {
            CliEthPortCounter counterInfo = allSupportedCounters[counter_i];
            mPutCounter(counterInfo->type, counterInfo->kind, counterInfo->name);
            }
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    for (i = 0; i <= numPorts; i++)
        AtOsalMemFree((void*)heading[i]);
    AtOsalMemFree(heading);

    AtOsalMemFree(allSupportedCounters);

    return cAtTrue;
    }

static eBool AutoNegEnable(char argc, char **argv, eBool enabled)
    {
    uint32 *idBuf;
    uint8 numPorts = 0;
    AtEthPort *ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    eBool success = CliAtEthPortAutoNegEnable(ports, numPorts, enabled);
    AtUnused(argc);
    AtOsalMemFree(ports);
    return success;
    }

static void CapturedEventsCleanup(AtTextUI self, void *userData)
    {
    tAtCapturedEvent *capturedEvent;

    AtUnused(self);
    AtUnused(userData);
    if (m_capturedEvents == NULL)
        return;

    while ((capturedEvent = (tAtCapturedEvent *)AtListObjectRemoveAtIndex(m_capturedEvents, 0)) != NULL)
        AtOsalMemFree(capturedEvent);

    AtObjectDelete((AtObject)m_capturedEvents);
    m_capturedEvents = NULL;
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;

    m_listener.WillStop = CapturedEventsCleanup;
    m_listener.DidStop  = NULL;
    m_EventAlarmListener = &m_listener;
    return m_EventAlarmListener;
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    if (m_capturedEvents == NULL)
        m_capturedEvents = AtListCreate(0);

    if (m_EventAlarmListener == NULL)
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), NULL);

    CliPushLastEventToListEvent(channel, changedAlarms, currentStatus, m_capturedEvents, 0);
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cAtEthPortAlarmtypeStr); alarm_i++)
        {
        uint32 alarmType = cAtEthPortAlarmtypeValue[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        logPtr += AtSprintf(logPtr, " * [%s:", cAtEthPortAlarmtypeStr[alarm_i]);
        if (currentStatus & alarmType)
            logPtr += AtSprintf(logPtr, "set");
        else
            logPtr += AtSprintf(logPtr, "clear");

        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

static void PortAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [ETH] Defect changed at %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtEthPortAlarmtypeStr); alarm_i++)
        {
        uint32 alarmType = cAtEthPortAlarmtypeValue[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtEthPortAlarmAutoNegStateChange)
            {
            AtPrintc(cSevNormal, " * [AN:");
            AtPrintc(cSevNormal, "%s", AutoNegStateString(AtEthPortAutoNegStateGet((AtEthPort)channel)));
            }

        else if (changedAlarms & alarmType & cAtEthPortAlarmRxRemoteFault)
            {
            AtPrintc(cSevNormal, " * [RF:");

            /* Need to display remote fault code if applicable */
            if (currentStatus & alarmType)
                {
                eAtEthPortRemoteFault faultCode = AtEthPortRemoteFaultGet((AtEthPort)channel);

                if (faultCode == cAtEthPortRemoteFaultNotSupported)
                    AtPrintc(cSevCritical, "set");

                else
                    AtPrintc(cSevCritical, "set - %s", RemoteFault2String(faultCode));
                }

            /* When clear, just display indication */
            else
                AtPrintc(cSevInfo, "clear");
            }
        else
            {
            AtPrintc(cSevNormal, " * [%s:", cAtEthPortAlarmtypeStr[alarm_i]);
            if (currentStatus & alarmType)
                AtPrintc(cSevCritical, "set");
            else
                AtPrintc(cSevInfo, "clear");
            }

        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void SubPortAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [ETH] Defect changed at %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtEthSubPortAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtEthSubPortAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtEthSubPortAlarmTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");

        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static tCliEthPortDefect *AllPossiblePortDefects(uint32 *numPossibleDefects)
    {
    static tCliEthPortDefect defects[] =
        {
        {cAtEthPortAlarmLinkUp                       , "link-up"},
        {cAtEthPortAlarmLinkDown                     , "link-down"},
        {cAtEthPortAlarmLos                          , "los"},
        {cAtEthPortAlarmTxFault                      , "tx-fault"},
        {cAtEthPortAlarmTxUnderflowError             , "tx-underflow-error"},
        {cAtEthPortAlarmLocalFault                   , "local-fault"},
        {cAtEthPortAlarmRemoteFault                  , "remote-fault"},
        {cAtEthPortAlarmRxInternalFault              , "rx-internal-fault"},
        {cAtEthPortAlarmRxReceivedLocalFault         , "rx-received-local-fault"},
        {cAtEthPortAlarmRxAlignedError               , "rx-aligned-error"},
        {cAtEthPortAlarmRxMisAligned                 , "rx-mis-aligned"},
        {cAtEthPortAlarmRxTruncated                  , "rx-truncated"},
        {cAtEthPortAlarmRxHiBer                      , "rx-hi-ber"},
        {cAtEthPortAlarmExcessiveErrorRatio          , "excessive-error-ratio"},
        {cAtEthPortAlarmLossofClock                  , "loss-of-clock"},
        {cAtEthPortAlarmFrequencyOutofRange          , "frequency-out-of-range"},
        {cAtEthPortAlarmLossofDataSync               , "loss-of-data-sync"},
        {cAtEthPortAlarmLossofFrame                  , "loss-of-frame"},
        {cAtEthPortAlarmAutoNegParallelDetectionFault, "auto-neg-parallel-detection-fault"},
        {cAtEthPortAlarmAutoNegStateChange           , "auto-neg-state-change"},
        {cAtEthPortAlarmFcsError                     , "fcs-error"},
        {cAtEthPortAlarmOversized                    , "oversized"}
        };

    if (numPossibleDefects)
        *numPossibleDefects = mCount(defects);

    return defects;
    }

static tCliEthPortDefect *AllPossibleSubPortDefects(uint32 *numPossibleDefects)
    {
    static tCliEthPortDefect defects[] =
        {
        {cAtEthSubPortAlarmRxFramingError          , "rx-framing-error"},
        {cAtEthSubPortAlarmRxSyncedError           , "rx-synced-error"},
        {cAtEthSubPortAlarmRxMfLenError            , "rx-mf-len-error"},
        {cAtEthSubPortAlarmRxMfRepeatError         , "rx-mf-repeat-error"},
        {cAtEthSubPortAlarmRxMfError               , "rx-mf-error"},
        {cAtEthSubPortAlarmRxBipError              , "rx-bip-error"},
        {cAtEthSubPortAlarmAutoNegFecIncCantCorrect, "auto-neg-fec-inc-cant-correct"},
        {cAtEthSubPortAlarmAutoNegFecIncCorrect    , "auto-neg-fec-inc-correct"},
        {cAtEthSubPortAlarmAutoNegFecLockError     , "auto-neg-fec-lock-error"}
        };

    if (numPossibleDefects)
        *numPossibleDefects = mCount(defects);

    return defects;
    }

static eBool DefectIsSupported(AtChannel *ports, uint32 numPorts, CliEthPortDefect defectInfo)
    {
    uint32 port_i;

    /* Not to affect auto testing system, just simply put all counters */
    if (AutotestIsEnabled())
        return cAtTrue;

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel port = ports[port_i];
        if (AtChannelAlarmIsSupported(port, defectInfo->defect))
            return cAtTrue;
        }

    return cAtFalse;
    }

static ePortType PortType(AtChannel *ports, uint32 numPorts)
    {
    ePortType allPortType = cPortTypeUnknown;
    uint32 port_i;

    if (numPorts == 0)
        return allPortType;

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = (AtEthPort)ports[port_i];
        uint32 portType = AtEthPortParentPortGet(port) ? cPortTypeSubPort : cPortTypePort;

        if (allPortType == cPortTypeUnknown)
            allPortType = portType;

        if (portType != allPortType)
            return cPortTypeUnknown;
        }

    return allPortType;
    }

static eBool AllSupportedDefectsCanDetect(AtChannel *ports, uint32 numPorts)
    {
    if (PortType(ports, numPorts) == cPortTypeUnknown)
        return cAtFalse;

    return cAtTrue;
    }

static CliEthPortDefect *AllSupportedDefectsDetect(AtChannel *ports, uint32 numPorts, uint32 *numSupportedDefects)
    {
    uint32 numPossibleDefects = 0, possibleDefect_i;
    tCliEthPortDefect *allPosisbleDefects = NULL;
    uint32 memorySize;
    CliEthPortDefect *supportedDefects;

    *numSupportedDefects = 0;
    if (!AllSupportedDefectsCanDetect(ports, numPorts))
        return NULL;

    /* Port and subport have different defects */
    if (AtEthPortParentPortGet((AtEthPort)ports[0]))
        allPosisbleDefects = AllPossibleSubPortDefects(&numPossibleDefects);
    else
        allPosisbleDefects = AllPossiblePortDefects(&numPossibleDefects);

    /* Find all of supported defects */
    memorySize = sizeof(CliEthPortDefect) * numPossibleDefects;
    supportedDefects = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(supportedDefects, 0, memorySize);
    for (possibleDefect_i = 0; possibleDefect_i < numPossibleDefects; possibleDefect_i++)
        {
        CliEthPortDefect defectInfo = &(allPosisbleDefects[possibleDefect_i]);

        if (DefectIsSupported(ports, numPorts, defectInfo))
            {
            supportedDefects[*numSupportedDefects] = defectInfo;
            *numSupportedDefects = *numSupportedDefects + 1;
            }
        }

    return supportedDefects;
    }

static eAtSevLevel ColorForBitVal(uint32 bitVal)
    {
    return bitVal ? cSevCritical : cSevInfo;
    }

static const char *StringForBitVal(uint32 bitVal)
    {
    return bitVal ? "set" : "clear";
    }

static eBool PortDefectShowWithSilent(AtChannel *ports, uint32 numPorts,
                                      uint32 (*DefectGet)(AtChannel self),
                                      eAtSevLevel (*ColorForBitValFunc)(uint32 bitVal),
                                      const char *(*StringForBitValFunc)(uint32 bitVal),
                                      eBool silent)
    {
    uint32 i;
    tTab *tabPtr = NULL;
    char **heading;
    uint32 numSupportedDefects = 0;
    CliEthPortDefect *allSupportedDefects = NULL;
    uint32 defect_i, row;

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPorts + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (i = 1; i <= numPorts; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(3);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPorts; i++)
        AtSprintf((void*)heading[i], "%s", AtChannelIdString((AtChannel)ports[i- 1]));

    /* Have list of possible defects that input channels can support */
    allSupportedDefects = AllSupportedDefectsDetect((AtChannel *)(void *)ports, numPorts, &numSupportedDefects);

    if (!silent)
        {
        uint32 numRows = numSupportedDefects ? (numSupportedDefects + 1) : 0; /* Plus 1 for port description */

        tabPtr = TableAlloc(numRows , numPorts + 1UL, (void *)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    /* Note, it is tempted to move this code block before creating table, doing
     * so will have CLI unit-test fail because the table database is of the
     * previous run */
    if (numSupportedDefects == 0)
        {
        AtPrintc(cSevInfo, "None of defects are supported on input ports\r\n");
        TableFree(tabPtr);
        HeadingFree((char**)heading, (uint8)(numPorts + 1));
        AtOsalMemFree(allSupportedDefects);
        return cAtTrue;
        }

    /* Print defect names */
    row = 0;
    StrToCell(tabPtr, row++, 0, "Description");
    for (defect_i = 0; defect_i < numSupportedDefects; defect_i++)
        {
        CliEthPortDefect defectInfo = allSupportedDefects[defect_i];
        StrToCell(tabPtr, row++, 0, defectInfo->name);
        }

    /* Print all defect status */
    for (i = 1; i <= numPorts; i++)
        {
        AtChannel port = (AtChannel)ports[i - 1];
        uint32 defects = DefectGet(port);

        /* Port description */
        row = 0;
        StrToCell(tabPtr, row++, i, AtObjectToString((AtObject)port));

        /* All of its defects */
        for (defect_i = 0; defect_i < numSupportedDefects; defect_i++)
            {
            CliEthPortDefect defectInfo = allSupportedDefects[defect_i];
            eBool supported = AtChannelAlarmIsSupported(port, defectInfo->defect);

            if (supported)
                {
                uint32 set =  defects & defectInfo->defect;
                eAtSevLevel color = ColorForBitValFunc(set);
                const char *string = StringForBitValFunc(set);
                ColorStrToCell(tabPtr, row++, i, string, color);
                }
            else
                StrToCell(tabPtr, row++, i, sAtNotSupported);
            }
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    HeadingFree((char**)heading, (uint8)(numPorts + 1));
    AtOsalMemFree(allSupportedDefects);

    return cAtTrue;
    }

static const char *InterruptMaskStringForBitVal(uint32 bitVal)
    {
    return bitVal ? "en" : "dis";
    }

static const char *DuplexModeString(eAtEthPortDuplexMode modes)
    {
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);
    uint32 mode_i;

    if (modes == cAtEthPortWorkingModeNone)
        return "none";

    if (modes == cAtEthPortWorkingModeNA)
        return sAtNotApplicable;

    if (modes == cAtEthPortWorkingModeUnknown)
        return NULL;

    buffer[0] = '\0';

    for (mode_i = 0; mode_i < mCount(cAtEthPortDuplexMdVal); mode_i++)
        {
        eAtEthPortDuplexMode mode = cAtEthPortDuplexMdVal[mode_i];
        const char *strVal;

        if ((modes & mode) == 0)
            continue;

        strVal = CliEnumToString(mode, cAtEthPortDuplexMdStr, cAtEthPortDuplexMdVal, mCount(cAtEthPortDuplexMdVal), NULL);
        if (strVal)
            AtStrcat(buffer, strVal);
        AtStrcat(buffer, "|");
        }

    /* Remove the last "|" */
    buffer[AtStrlen(buffer) - 1] = '\0';

    if (AtStrlen(buffer) == 0)
        return NULL;

    return buffer;
    }

static eAtSevLevel DuplexModeColor(eAtEthPortDuplexMode modes)
    {
    switch (modes)
        {
        case cAtEthPortWorkingModeUnknown   : return cSevCritical;
        case cAtEthPortWorkingModeNone      : return cSevCritical;
        case cAtEthPortWorkingModeHalfDuplex: return cSevInfo;
        case cAtEthPortWorkingModeFullDuplex: return cSevInfo;
        case cAtEthPortWorkingModeAutoDetect: return cSevNormal;
        case cAtEthPortWorkingModeNA        : return cSevNormal;
        default:
            return cSevNormal;
        }
    }

static const char *RemoteFault2String(eAtEthPortRemoteFault remoteFault)
    {
    static const char *cAtEthPortRemoteFaultStr[] = {sAtNotSupported,
                                                     "no-error",
                                                     "offline",
                                                     "link-failure",
                                                     "auto-neg-error"
    };

    static uint32 cAtEthPortRemoteFaultVal[] = {cAtEthPortRemoteFaultNotSupported,
                                                cAtEthPortRemoteFaultNoError,
                                                cAtEthPortRemoteFaultOffline,
                                                cAtEthPortRemoteFaultLinkFailure,
                                                cAtEthPortRemoteFaultAutoNegError
    };

    return CliEnumToString(remoteFault,
                           cAtEthPortRemoteFaultStr,
                           cAtEthPortRemoteFaultVal,
                           mCount(cAtEthPortRemoteFaultVal),
                           NULL);
    }

static eAtSevLevel RemoteFaultColor(eAtEthPortRemoteFault remoteFault)
    {
    switch (remoteFault)
        {
        case cAtEthPortRemoteFaultNotSupported: return cSevNormal;
        case cAtEthPortRemoteFaultNoError     : return cSevInfo;

        case cAtEthPortRemoteFaultUnknown     : return cSevCritical;
        case cAtEthPortRemoteFaultOffline     : return cSevCritical;
        case cAtEthPortRemoteFaultLinkFailure : return cSevCritical;
        case cAtEthPortRemoteFaultAutoNegError: return cSevCritical;

        default:
            return cSevCritical;
        }
    }

static eAtModuleEthRet FecEnable(AtEthPort self, eBool enabled)
    {
    eAtRet ret = cAtOk;

    ret |= AtEthPortTxFecEnable(self, enabled);
    ret |= AtEthPortRxFecEnable(self, enabled);

    return ret;
    }

static const char *FecEnablingGet(AtEthPort port)
    {
    eBool rxEnabled = AtEthPortRxFecIsEnabled(port);
    eBool txEnabled = AtEthPortTxFecIsEnabled(port);

    if (mBoolToBin(rxEnabled) == mBoolToBin(txEnabled))
        return CliBoolToString(rxEnabled);

    if (rxEnabled)
        return "rx";

    if (txEnabled)
        return "tx";

    /* Impossible */
    return NULL;
    }

static AtList ClockMonitorGet(char *idString)
    {
    uint32 numPorts = 0;
    AtList clockMonitors = AtListCreate(0);
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    uint32 port_i;

    numPorts = CliEthPortListFromString(idString, ports, numPorts);
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = (AtEthPort)ports[port_i];
        AtClockMonitor monitor = AtEthPortClockMonitorGet(port);

        if (monitor)
            AtListObjectAdd(clockMonitors, (AtObject)monitor);
        }

    return clockMonitors;
    }

static eBool IpgSet(char argc, char **argv, eAtModuleEthRet (*IpgSetFunc)(AtEthPort self, uint8 ipg))
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i, ipg;
    AtChannel *channelList;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    ipg = (uint8)AtStrToDw(argv[1]);

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = IpgSetFunc((AtEthPort)channelList[i], ipg);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Change IPG on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(channelList[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool VlanSet(char argc, char **argv, eAtRet (*CVlanSetFunc)(AtEthPort self, const tAtVlan *vlan))
    {
    uint32 *idBuf;
    uint8 numPorts, port_i;
    AtEthPort *ports;
    tAtVlan vlan, *pVlan = NULL;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Port List */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if ((ports == NULL) || (numPorts == 0))
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        AtOsalMemFree(ports);
        return cAtTrue;
        }

    /* Parse VLAN */
    if (AtStrcmp(argv[1], "none") != 0)
        {
        pVlan = CliVlanFromString(argv[1], &vlan);
        if (pVlan == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid VLAN, expected %s\r\n", CliFullVlanExpectedFormat());
            AtOsalMemFree(ports);
            return cAtFalse;
            }
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        eAtRet ret = CVlanSetFunc(ports[port_i], pVlan);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Configure CVLAN fail with ret = %s\r\n", AtRet2String(ret));
        success = cAtFalse;
        }

    AtOsalMemFree(ports);

    return success;
    }

AtEthPort *CliEthPortListGet(char *pStrPortIdList, uint32 **idBuf, uint8 *numValidPorts)
    {
    uint32 bufferSize;
    uint32 port_i, portId;
    uint32 validPort_i = 0;
    uint32 numInputPorts;
    AtEthPort port;
    AtEthPort *allPorts;
    AtModuleEth ethModule;

    /* Get the shared ID buffer */
    *idBuf = CliSharedIdBufferGet(&bufferSize);
    if (numValidPorts)
        *numValidPorts = 0;

    if ((numInputPorts = CliIdListFromString((void*)pStrPortIdList, *idBuf, bufferSize)) == 0)
        return NULL;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Need to count number of valid ports before allocating memory */
    for (port_i = 0; port_i < numInputPorts; port_i++)
        {
        portId = (*idBuf)[port_i] - 1;
        if (!EthPortIdIsValid(ethModule, portId))
            continue;

        if (PortGet(ethModule, portId))
            validPort_i = validPort_i + 1;
        }

    /* No port */
    if (validPort_i == 0)
        return NULL;

    /* Allocate memory then put valid ports to array */
    allPorts = AtOsalMemAlloc(validPort_i * sizeof(AtEthPort));
    if (allPorts == NULL)
    	return NULL;

    validPort_i = 0;
    for (port_i = 0; port_i < numInputPorts; port_i++)
        {
        portId = (*idBuf)[port_i] - 1;
        if (!EthPortIdIsValid(ethModule, portId))
            continue;

        port = PortGet(ethModule, portId);
        if (port != NULL)
            {
            allPorts[validPort_i]= port;
            validPort_i++;
            }
        }
        
    if (numValidPorts)
        *numValidPorts = (uint8)validPort_i;

    return allPorts;
    }

char *CliEthPortMacAddressByte2Str(const uint8 *pMacAddr)
    {
    return AtBytes2String(pMacAddr, cAtMacAddressLen, 16);
    }

AtList CliAtEthPortSerdesControllers(char *pPortListString)
    {
    uint32 *portIdList;
    AtList controllers;
    uint32 bufferSize, numberOfSerdes;
    uint32 i;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    if (AtStrchr(pPortListString, '.'))
        return NULL;

    /* ID list of ports */
    portIdList     = CliSharedIdBufferGet(&bufferSize);
    numberOfSerdes = CliIdListFromString(pPortListString, portIdList, bufferSize);
    if (numberOfSerdes == 0)
        return NULL;

    /* Get controllers */
    controllers = AtListCreate(0);
    for (i = 0; i < numberOfSerdes; i++)
        {
        uint32 portId = portIdList[i] - 1;
        AtSerdesController controler = AtModuleEthSerdesController(ethModule, portId);
        if (controler)
            AtListObjectAdd(controllers, (AtObject)controler);
        }

    /* No controller */
    if (AtListLengthGet(controllers) == 0)
        {
        AtObjectDelete((AtObject)controllers);
        return NULL;
        }

    return controllers;
    }

eBool CliAtEthPortAutoNegEnable(AtEthPort *ports, uint32 numPorts, eBool enabled)
    {
    eBool success = cAtTrue;
    uint32 port_i;

    if (numPorts == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no ports to configure, ID may be wrong\r\n");
        return cAtTrue;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = ports[port_i];
        eAtRet ret;

        ret = AtEthPortAutoNegEnable(port, enabled);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s autoneg on port %s fail with ret = %s\r\n",
                 enabled ? "Enable" : "Disable",
                 CliChannelIdStringGet((AtChannel)port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdEthPortSpeedSet(char argc, char **argv)
    {
    eAtEthPortSpeed speedMode = cAtEthPortSpeedUnknown;
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    eBool blResult;
    AtChannel *channelList;
    eAtRet ret = cAtOk;
	AtUnused(argc);

	/* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get speed mode from string */
    mAtStrToEnum(cAtEthPortSpeedMdStr,
                 cAtEthPortSpeedMdVal,
                 argv[1],
                 speedMode,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid Speed Mode\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortSpeedSet((AtEthPort)channelList[i], speedMode);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortDuplexSet(char argc, char **argv)
    {
    eAtEthPortDuplexMode duplexMode = cAtEthPortWorkingModeAutoDetect;
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    eBool blResult;
    AtChannel *channelList;
    eAtRet ret = cAtOk;
	AtUnused(argc);

	/* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get duplex mode from string */
    mAtStrToEnum(cAtEthPortDuplexMdStr,
                 cAtEthPortDuplexMdVal,
                 argv[1],
                 duplexMode,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid Working Mode\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortDuplexModeSet((AtEthPort)channelList[i], duplexMode);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortTxIpgSet(char argc, char **argv)
    {
    return IpgSet(argc, argv, AtEthPortTxIpgSet);
    }

eBool CmdEthPortRxIpgSet(char argc, char **argv)
    {
    return IpgSet(argc, argv, AtEthPortRxIpgSet);
    }

eBool CmdEthPortPauFrmIntvlSet(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    uint32 pauFrmIntvl;
    AtChannel *channelList;
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    pauFrmIntvl = (uint32)AtStrToDw(argv[1]);

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortPauseFrameIntervalSet((AtEthPort)channelList[i], pauFrmIntvl);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortPauFrmExpTimeSet(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    uint32 pauFrmExpTime;
    AtChannel *channelList;
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Get the shared ID buffer */
   channelList = CliSharedChannelListGet(&bufferSize);
   if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
       {
       AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
       return cAtFalse;
       }

    /* Get Ethernet Module */
    pauFrmExpTime = (uint32)AtStrToDw(argv[1]);

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortPauseFrameExpireTimeSet((AtEthPort)channelList[i], pauFrmExpTime);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortFlowCtrl(char argc, char **argv)
    {
    uint32 bufferSize, i;
    uint8 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
    eBool enable;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Enabling */
    enable = CliBoolFromString(argv[1]);

    /* Apply all ports */
    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = AtEthPortFlowControlEnable((AtEthPort)channelList[i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s flow control on port %u, ret = %s\r\n",
                     enable ? "enable" : "disable", AtChannelIdGet(channelList[i]) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEthPortAutoNegEnable(char argc, char **argv)
    {
    return AutoNegEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEthPortAutoNegDisable(char argc, char **argv)
    {
    return AutoNegEnable(argc, argv, cAtFalse);
    }

eBool CmdEthPortCfgGet(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts, i;
    uint8 row = 0;
    eBool blResult = cAtFalse;
    AtChannel *channelList;
    eAtLoopbackMode loopbackMode;
    tAtEthPortAllConfig configs;
    tTab *tabPtr;
    char **heading;
    const char *colStr[] = {"Description", "Speed", "Auto-Neg", "Pause Frame Interval Time",
                            "Pause Frame Expire Time", "Tx IPG", "Rx IPG", "Flow Control",
                            "Working", "Interface", "Mac Check", "Led State","Source MAC",
                            "Dest MAC", "IPv4", "IPv6", "Loopback", "MinPacketSize","MaxPacketSize",
                            "HiGig", "In-used Bandwidth", "Running Bandwidth", "Enabled", "Link",
                            "DisparityForced", "FEC", "K30.7Forced", "LinkTraining"};
    char buf[100];
    AtUnused(argc);

    /* Initialize structure */
    AtOsalMemInit(&configs, 0x0, sizeof(tAtEthPortAllConfig));
    AtOsalMemInit(buf, 0x0, sizeof(buf));

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc(sizeof(char*) * (numPorts + 1UL));
    if (heading == NULL)
        return cAtFalse;

    for (i = 0; i <= numPorts; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(sizeof(char) * 32);
        if (heading[i] == NULL)
            {
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }
        }

    /* Create table with titles */
    AtSprintf((char *)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPorts; i++)
        AtSprintf((char *)heading[i], "%s", AtChannelIdString(channelList[i - 1]));

    tabPtr = TableAlloc(mCount(colStr), numPorts + 1UL, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtStringArrayFree(heading, numPorts + 1UL);
        return cAtFalse;
        }

    for (i = 0; i < mCount(colStr); i++)
        StrToCell(tabPtr, row++, 0, colStr[i]);

    /* Make table content */
    for (i = 1; i <= numPorts; i++)
        {
        AtEthPort port = (AtEthPort)channelList[i - 1];
        eAtRet ret = AtChannelAllConfigGet((AtChannel)port, &configs);
        eBool enabled;
        eAtEthPortLinkStatus linkStatus;

        row = 0;

        /* Description */
        StrToCell(tabPtr, row++, i, AtObjectToString((AtObject)port));

        /* Print Ethernet port speed */
        if (configs.speed == cAtEthPortSpeedUnknown)
            ColorStrToCell(tabPtr, row++, i, "Error", cSevCritical);
        else
            {
            mAtEnumToStr(cAtEthPortSpeedMdStr,
                         cAtEthPortSpeedMdVal,
                         configs.speed,
                         buf,
                         blResult);
            StrToCell(tabPtr, row++, i, blResult ? buf : "Error");
            }

        /* Convert enum to string */
        mAtEnumToStr(cAtBoolStr,
                     cAtBoolVal,
                     configs.autoNegEn,
                     buf,
                     blResult);

        /* Print Ethernet port AutoNeg Configuration */
        StrToCell(tabPtr, row++, i, blResult ? buf : "Error");

        /* Print Ethernet port Pause Frame Interval */
        AtSprintf(buf, "%d", configs.pauFrmInterval);
        StrToCell(tabPtr, row++, i, buf);

        /* Print Ethernet port Pause Frame Expire */
        AtSprintf(buf, "%d", configs.pauFrmExpTime);
        StrToCell(tabPtr, row++, i, buf);

        /* Print Ethernet port Tx IPG */
        if (AtEthPortTxIpgIsConfigurable(port))
            {
            AtSprintf(buf, "%d", configs.numTxIpg);
            StrToCell(tabPtr, row++, i, buf);
            }
        else
            StrToCell(tabPtr, row++, i, sAtNotSupported);

        /* Print Ethernet port Rx IPG */
        if (AtEthPortRxIpgIsConfigurable(port))
            {
            AtSprintf(buf, "%d", configs.numRxIpg);
            StrToCell(tabPtr, row++, i, buf);
            }
        else
            StrToCell(tabPtr, row++, i, sAtNotSupported);

        /* Convert enum to string */
        mAtEnumToStr(cAtBoolStr,
                     cAtBoolVal,
                     configs.flowCtrlEn,
                     buf,
                     blResult);

        /* Print Ethernet port Flow Control Enable Status */
        StrToCell(tabPtr, row++, i, buf);

        /* Convert enum to string */
        mAtEnumToStr(cAtEthPortDuplexMdStr,
                     cAtEthPortDuplexMdVal,
                     configs.workingMd,
                     buf,
                     blResult);

        /* Print Ethernet port Working Mode */
        StrToCell(tabPtr, row++, i, buf);

        /* Print Ethernet port Interface */
        if (configs.interface == cAtEthPortInterfaceUnknown)
            ColorStrToCell(tabPtr, row++, i, "Error", cSevCritical);
        else
            {
            /* Convert enum to string */
            mAtEnumToStr(cAtEthPortInterfaceMdStr,
                         cAtEthPortInterfaceMdVal,
                         configs.interface,
                         buf,
                         blResult);
            StrToCell(tabPtr, row++, i, buf);
            }

        /* Convert enum to string */
        mAtEnumToStr(cAtBoolStr,
                     cAtBoolVal,
                     configs.macChkEn,
                     buf,
                     blResult);

        /* Print Ethernet port Mac Check */
        StrToCell(tabPtr, row++, i, buf);

        /* Convert enum to string */
        mAtEnumToStr(cAtEthPortLedStateStr,
                     cAtEthPortLedStateVal,
                     configs.ledState,
                     buf,
                     blResult);

        /* Print Ethernet port Led State */
        StrToCell(tabPtr, row++, i, buf);

        /* Source MAC */
        ret = AtEthPortSourceMacAddressGet(port, configs.srcMacAddr);
        if (ret == cAtOk)
            StrToCell(tabPtr, row++, i, (void*)CliEthPortMacAddressByte2Str(configs.srcMacAddr));
        else
            StrToCell(tabPtr, row++, i, Ret2String(ret));

        /* Dest MAC */
        ret = AtEthPortDestMacAddressGet(port, configs.destMacAddr);
        if (ret == cAtOk)
            StrToCell(tabPtr, row++, i, (void*)CliEthPortMacAddressByte2Str(configs.destMacAddr));
        else
            StrToCell(tabPtr, row++, i, Ret2String(ret));

        /* IPv4 */
        ret = AtEthPortIpV4AddressGet(port, configs.srcIpv4Addr);
        if (ret == cAtOk)
            StrToCell(tabPtr, row++, i, CliAtModuleEthIPv4AddrByte2Str(configs.srcIpv4Addr));
        else
            StrToCell(tabPtr, row++, i, Ret2String(ret));

        /* IPv6 */
        ret = AtEthPortIpV6AddressGet(port, configs.srcIpv6Addr);
        if (ret == cAtOk)
            StrToCell(tabPtr, row++, i, CliAtModuleEthIPv6AddrByte2Str(configs.srcIpv6Addr));
        else
            StrToCell(tabPtr, row++, i, Ret2String(ret));
        
        /* Print loopback */
        loopbackMode = AtChannelLoopbackGet((AtChannel)port);
        mAtEnumToStr(cAtEthPortLoopbackStr, cAtEthPortLoopbackVal, loopbackMode, buf, blResult);
        StrToCell(tabPtr, row++, i, buf);

        /* Print maximum packet size */
        AtSprintf(buf, "%u", AtEthPortMinPacketSizeGet(port));
        StrToCell(tabPtr, row++, i, buf);

        /* Print minimum packet size */
        AtSprintf(buf, "%u", AtEthPortMaxPacketSizeGet(port));
        StrToCell(tabPtr, row++, i, buf);

        /* Print HiGig enabling */
        enabled = AtEthPortHiGigIsEnabled(port);
        mAtBoolToStr(enabled, buf, blResult);
        StrToCell(tabPtr, row++, i, blResult ? buf : "Error");

        /* Current in-used bandwidth */
        AtSprintf(buf, "%s Kbps", AtNumberDigitGroupingString(AtEthPortProvisionedBandwidthInKbpsGet(port)));
        StrToCell(tabPtr, row++, i, buf);
        AtSprintf(buf, "%s Kbps", AtNumberDigitGroupingString(AtEthPortRunningBandwidthInKbpsGet(port)));
        StrToCell(tabPtr, row++, i, buf);

        /* Enabling */
        enabled = AtChannelIsEnabled((AtChannel)port);
        ColorStrToCell(tabPtr, row++, i, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        /* Link status */
        linkStatus = AtEthPortLinkStatus(port);
        ColorStrToCell(tabPtr, row++, i, LinkStatus2String(linkStatus), LinkStatusColor(linkStatus));

        /* Disparity */
        if (AtEthPortDisparityCanForce(port))
            {
            enabled = AtEthPortDisparityIsForced(port);
            ColorStrToCell(tabPtr, row++, i, CliBoolToString(enabled), enabled ? cSevCritical : cSevInfo);
            }
        else
            StrToCell(tabPtr, row++, i, sAtNotSupported);

        if (AtEthPortFecIsSupported(port))
            StrToCell(tabPtr, row++, i, FecEnablingGet(port));
        else
            StrToCell(tabPtr, row++, i, sAtNotSupported);

        /* K30.7 */
        if (AtEthPortK30_7CanForce(port))
            {
            enabled = AtEthPortK30_7IsForced(port);
            ColorStrToCell(tabPtr, row++, i, CliBoolToString(enabled), enabled ? cSevCritical : cSevInfo);
            }
        else
            StrToCell(tabPtr, row++, i, sAtNotSupported);

        /* Link Training */
        if (AtEthPortLinkTrainingIsSupported(port))
            {
            enabled = AtEthPortLinkTrainingIsEnabled(port);
            ColorStrToCell(tabPtr, row++, i, CliBoolToString(enabled), enabled ? cSevInfo : cSevNormal);
            }
        else
            StrToCell(tabPtr, row++, i, sAtNotSupported);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    AtStringArrayFree(heading, numPorts + 1UL);

    return cAtTrue;
    }

eBool CmdEthPortCounterGet(char argc, char **argv)
    {
    uint32 numPorts;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    eBool success;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (ports == NULL)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    success = PortCounterGet((AtEthPort *)ports, numPorts, argc, argv);

    return success;
    }

eBool CmdEthPortAlarmGet(char argc, char **argv)
    {
    uint32 numPorts = 0;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);

    AtUnused(argc);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }

    return PortDefectShowWithSilent(ports, numPorts,
                                    AtChannelDefectGet,
                                    ColorForBitVal,
                                    StringForBitVal,
                                    cAtFalse);
    }

eBool CmdEthPortIntrGet(char argc, char **argv)
    {
    uint32 numPorts = 0;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    eBool success;
    eAtHistoryReadingMode readingMode;
    uint32 (*DefectGet)(AtChannel self) = AtChannelDefectInterruptGet;
    eBool silent = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }

    /* Reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Get counter mode */
    if (readingMode == cAtHistoryReadingModeReadToClear)
        DefectGet = AtChannelDefectInterruptClear;

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    success = PortDefectShowWithSilent(ports, numPorts,
                                       DefectGet,
                                       ColorForBitVal,
                                       StringForBitVal,
                                       silent);

    return success;
    }

eBool CmdEthPortSrcMac(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    AtChannel *channelList;
    uint8 mac[cAtMacAddressLen];
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "Invalid Source MAC\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = AtEthPortSourceMacAddressSet((AtEthPort)channelList[i], mac);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set SMAC fail on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(channelList[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdEthPortDestMac(char argc, char **argv)
    {
    uint32 *idBuf;
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    AtModuleEth ethModule;
    AtEthPort port;
    uint8 mac[cAtMacAddressLen];
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = (uint8)CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "Invalid Destination MAC\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        port = PortGet(ethModule, idBuf[i] - 1);
        if (port != NULL)
            ret |= AtEthPortDestMacAddressSet(port, mac);
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortInterfaceSet(char argc, char **argv)
    {
    eAtEthPortInterface intfMode = cAtEthPortInterfaceUnknown;
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    eBool blResult;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get Interface mode from string */
    mAtStrToEnum(cAtEthPortInterfaceMdStr,
                 cAtEthPortInterfaceMdVal,
                 argv[1],
                 intfMode,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid Interface Mode\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = AtEthPortInterfaceSet((AtEthPort)channelList[i], intfMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Change interface on port %u fail, ret = %s\r\n",
                     AtChannelIdGet(channelList[i]) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEthPortMacCheck(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    AtChannel *channelList;
    eBool enable = cAtFalse;
    eBool convertSuccess, success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Convert String to Enum */
    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[1],
                 enable,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "Invalid mode\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = AtEthPortMacCheckingEnable((AtEthPort)channelList[i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s MAC checking for port %u, ret = %s\r\n",
                     enable ? "enable" : "disable", AtChannelIdGet(channelList[i]) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEthPortLedState(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    eBool blResult;
    eAtLedState ledState = cAtLedStateOff;
    AtChannel *channelList;
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get Led State from string */
    mAtStrToEnum(cAtEthPortLedStateStr,
                 cAtEthPortLedStateVal,
                 argv[1],
                 ledState,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid Led State Mode\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortLedStateSet((AtEthPort)channelList[i], ledState);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

/*------------------------------------------------------------------------------------------------------------*/
eBool CmdEthPortLoopbackSet(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    AtChannel *channelList;
    eAtLoopbackMode loopMode = cAtLoopbackModeRelease;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get loopback mode */
    mAtStrToEnum(cAtEthPortLoopbackStr, cAtEthPortLoopbackVal, argv[1], loopMode, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Loop back mode. Expected: release, local, remote\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret;
        ret  = AtChannelLoopbackSet(channelList[i], (uint8)loopMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot loopback on port %u, ret = %s\r\n",
                     AtChannelIdGet(channelList[i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEthPortEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtChannelEnable);
    }

eBool CmdEthPortDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtChannelEnable);
    }

eBool CmdAtEthPortSerdesControllerInit(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerInit(controllers);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesControllerEnable(char argc, char **argv)
    {
    return SerdesControllerEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEthPortSerdesControllerDisable(char argc, char **argv)
    {
    return SerdesControllerEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEthPortSerdesPhyParamSet(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerPhyParamSet(controllers, argv[1], argv[2]);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesShow(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllersShow(controllers);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtEthPortSerdesAlarmShow(char argc, char **argv)
    {
    eBool silent = cAtFalse;
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerAlarmGet, silent);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtEthPortSerdesAlarmHistoryShow(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success;
    eAtHistoryReadingMode readingMode;
    uint32 (*AlarmGet)(AtSerdesController);
    eBool silent = cAtFalse;
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (readingMode == cAtHistoryReadingModeReadOnly)
        AlarmGet = AtSerdesControllerAlarmHistoryGet;
    else
        AlarmGet = AtSerdesControllerAlarmHistoryClear;

    success = CliAtSerdesControllersAlarmShow(controllers, AlarmGet, silent);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtEthPortSerdesInterruptMask(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success;
    uint32 intrMask = CliSerdesControllerAlarmMaskFromString(argv[1]);
    eBool enable;
    AtUnused(argc);

    mAtStrToBool(argv[2], enable, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    success = CliAtSerdesControllerInterruptMaskSet(controllers, intrMask, enable);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtEthPortSerdesNotficationEnable(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerNotificationEnable(controllers, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesNotficationDisable(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerNotificationEnable(controllers, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesDebug(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerDebug(controllers);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesParamShow(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllersParamShow(controllers, (argc < 2) ? NULL : argv[1]);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtEthPortSerdesLoopback(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerLoopback(controllers, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortMaxPktSize(char argc, char **argv)
    {
    return PktSizeSet(argc, argv, AtEthPortMaxPacketSizeSet);
    }

eBool CmdAtEthPortMinPktSize(char argc, char **argv)
    {
    return PktSizeSet(argc, argv, AtEthPortMinPacketSizeSet);
    }

eBool CmdAtEthPortDebug(char argc, char **argv)
    {
    AtChannel *channelList;
    uint8 numPorts;
    uint32 wIndex;
    uint32 bufferSize;
	AtUnused(argc);

	/* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    for (wIndex = 0; wIndex < numPorts; wIndex++)
        AtChannelDebug(channelList[wIndex]);

    return cAtTrue;
    }

eBool CmdAtEthPortSwitch(char argc, char **argv)
    {
    uint32 fromPortId = AtStrtoul(argv[0], NULL, 10);
    uint32 toPortId   = AtStrtoul(argv[1], NULL, 10);
	AtUnused(argc);
    return ApsSwBr(fromPortId, toPortId, (ApsSwBrFunc)AtEthPortSwitch);
    }

eBool CmdAtEthPortSwitchRelease(char argc, char **argv)
    {
    AtUnused(argc);
    return ApsRelease(argv[0], (EthPortApsReleaseFunc)AtEthPortSwitchRelease);
    }

eBool CmdAtEthPortBridge(char argc, char **argv)
    {
    uint32 fromPortId = AtStrtoul(argv[0], NULL, 10);
    uint32 toPortId   = AtStrtoul(argv[1], NULL, 10);
	AtUnused(argc);
    return ApsSwBr(fromPortId, toPortId, (ApsSwBrFunc)AtEthPortBridge);
    }

eBool CmdAtEthPortBridgeRelease(char argc, char **argv)
    {
    AtUnused(argc);
    return ApsRelease(argv[0], (EthPortApsReleaseFunc)AtEthPortBridgeRelease);
    }

eBool CmdAtEthPortApsGet(char argc, char **argv)
    {
    tTab *tabPtr;
    char buf[16];
    const char *pHeading[] = {"Port Id", "switchedPort", "bridgedPort"};
    uint32 numPorts;
    AtChannel *channelList;
    uint32 bufferSize, i;
	AtUnused(argc);

	/* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, 3, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numPorts; i++)
        {
        AtEthPort swBrPort;

        /* The port itself */
        StrToCell(tabPtr, i, 0, CliChannelIdStringGet(channelList[i]));

        /* Switch port */
        swBrPort = AtEthPortSwitchedPortGet((AtEthPort)channelList[i]);
        if (swBrPort == NULL)
            StrToCell(tabPtr, i, 1, "none");
        else
            {
            AtSprintf(buf, "%u", AtChannelIdGet((AtChannel)swBrPort) + 1);
            StrToCell(tabPtr, i, 1, buf);
            }

        /* Bridged port */
        swBrPort = AtEthPortBridgedPortGet((AtEthPort)channelList[i]);
        if (swBrPort == NULL)
            StrToCell(tabPtr, i, 2, "none");
        else
            {
            AtSprintf(buf, "%u", AtChannelIdGet((AtChannel)swBrPort) + 1);
            StrToCell(tabPtr, i, 2, buf);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdEthPortHigigDisable(char argc, char **argv)
    {
    return HigigEnable(argc, argv, cAtFalse);
    }

eBool CmdEthPortHigigEnable(char argc, char **argv)
    {
    return HigigEnable(argc, argv, cAtTrue);
    }

eBool CmdEthPortIpv4Set(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    uint8 ipv4Buf[cAtIpv4AddressLen];
    AtChannel *channelList;
    eAtRet ret = cAtOk;
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(ipv4Buf, 0x0, cAtIpv4AddressLen);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get IPv4 */
    if (!CliAtModuleEthIpv4AddrStr2Byte(argv[1], ipv4Buf))
        {
        AtPrintc(cSevCritical, "ERROR: IPv4 address must have %d bytes separated by '.'\r\n", cAtIpv4AddressLen);
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortIpV4AddressSet((AtEthPort)channelList[i], (uint8*)ipv4Buf);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortIpv6Set(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    uint8 ipv6Buf[cAtIpv6AddressLen];
    AtChannel *channelList;
    eAtRet ret = cAtOk;
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(ipv6Buf, 0x0, cAtIpv6AddressLen);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get IPv6 */
    if (!Ipv6AddrStr2Byte(argv[1], ipv6Buf))
        {
        AtPrintc(cSevCritical, "ERROR: IPv6 address must have %d bytes separated by '.'\r\n", cAtIpv6AddressLen);
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        ret |= AtEthPortIpV6AddressSet((AtEthPort)channelList[i], (uint8*)ipv6Buf);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthPortLogShow(char argc, char** argv)
    {
    uint32 bufferSize;
    uint8 numPorts;
    uint8 i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        AtChannelLogShow(channelList[i]);

    return cAtTrue;
    }

eBool CmdEthPortLogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdEthPortLogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

eBool CmdEthPortSerdesSelect(char argc, char **argv)
    {
    uint32 portId = AtStrtoul(argv[0], NULL, 10);
    uint32 serdesId = AtStrtoul(argv[1], NULL, 10);
    AtUnused(argc);

    if (AtStrcmp(argv[1], "none") == 0)
        serdesId = cInvalidUint32;

    return SerdesSet(portId, serdesId, (SerdesSetFunc)AtEthPortRxSerdesSelect);
    }

eBool CmdEthPortTxSerdesSet(char argc, char **argv)
    {
    uint32 portId = AtStrtoul(argv[0], NULL, 10);
    uint32 serdesId = AtStrtoul(argv[1], NULL, 10);
    AtUnused(argc);

    return SerdesSet(portId, serdesId, (SerdesSetFunc)AtEthPortTxSerdesSet);
    }

eBool CmdEthPortSerdesBridge(char argc, char **argv)
    {
    uint32 serdesId;
    uint32 portId = AtStrtoul(argv[0], NULL, 10);
    char *bridgeSerdes = argv[1];

    AtUnused(argc);

    if (AtStrcmp(bridgeSerdes, "none")   == 0)
        return SerdesBridgeRelease(portId);

    serdesId = AtStrtoul(bridgeSerdes, NULL, 10);
    return SerdesSet(portId, serdesId, (SerdesSetFunc)AtEthPortTxSerdesBridge);
    }

eBool CmdAtEthPortSerdesApsGet(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Port", "SelectedSerdes", "BridgedSerdes", "TransmitSerdes"};
    AtChannel *channelList;
    uint32 numPorts, bufferSize, i;

    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numPorts; i++)
        {
        AtSerdesController serdes;
        uint8 column = 0;
        AtEthPort port = (AtEthPort)channelList[i];

        /* The port itself */
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)port));

        /* Switch port */
        serdes = AtEthPortRxSelectedSerdes(port);
        if (serdes == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            StrToCell(tabPtr, i, column++, CliNumber2String(AtSerdesControllerIdGet(serdes) + 1, "%u"));

        /* Bridged port */
        serdes = AtEthPortTxBridgedSerdes(port);
        if (serdes == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            StrToCell(tabPtr, i, column++, CliNumber2String(AtSerdesControllerIdGet(serdes) + 1, "%u"));

        /* Transmit port */
        serdes = AtEthPortTxSerdesGet(port);
        if (serdes == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            StrToCell(tabPtr, i, column++, CliNumber2String(AtSerdesControllerIdGet(serdes) + 1, "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtEthPortSerdesControllerReset(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesControllerRxReset(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerRxReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesControllerTxReset(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerTxReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesControllerEqualizerModeSet(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerEqualizerModeSet(controllers, argc, argv);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesControllerPowerUp(char argc, char **argv)
    {
    return PowerDown(argc, argv, cAtFalse);
    }

eBool CmdAtEthPortSerdesControllerPowerDown(char argc, char **argv)
    {
    return PowerDown(argc, argv, cAtTrue);
    }

eBool CmdAtEthportInterruptMask(char argc, char **argv)
    {
    uint32 numPorts = 0;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    uint32 intrMask, port_i;
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
    eBool enable = cAtFalse;
    ePortType portType;

    AtUnused(argc);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }

    portType = PortType(ports, numPorts);
    if (portType == cPortTypeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Subports and ports cannot be input on the same command\r\n");
        return cAtFalse;
        }

    /* Get interrupt mask value from input string */
    if (portType == cPortTypePort)
        intrMask = CliMaskFromString(argv[1], cAtEthPortAlarmtypeStr, cAtEthPortAlarmtypeValue, mCount(cAtEthPortAlarmtypeValue));
    else
        intrMask = CliMaskFromString(argv[1], cAtEthSubPortAlarmTypeStr, cAtEthSubPortAlarmTypeVal, mCount(cAtEthSubPortAlarmTypeVal));
    if (intrMask == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel port = ports[port_i];
        eAtRet ret = AtChannelInterruptMaskSet(port, intrMask, enable ? intrMask : 0x0);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevWarning,
                 "ERROR: Cannot configure interrupt mask for port %s, ret = %s\r\n",
                 CliChannelIdStringGet(port), AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }
    
eBool CmdAtEthportAlarmCapture(char argc, char **argv)
    {
    uint32 numPorts, i;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    static tAtChannelEventListener listener;
    ePortType portType;

    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevNormal, "Error: Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }

    portType = PortType(ports, numPorts);
    if (portType == cPortTypeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Subports and ports cannot be input on the same command\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        listener.AlarmChangeState = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        listener.AlarmChangeState = AlarmChangeLogging ;
    else
        listener.AlarmChangeState = (portType == cPortTypeSubPort) ? SubPortAlarmChangeState : PortAlarmChangeState;

    for (i = 0; i < numPorts; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel port = ports[i];

        if (captureEnabled)
            ret = AtChannelEventListenerAdd(port, &listener);
        else
            ret = AtChannelEventListenerRemove(port, &listener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(port),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtEthportAlarmCaptureGet(char argc, char **argv)
    {
    AtList filteredEvent;
    eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    uint32 *idBuf;
    uint8 numPorts;
    eAtHistoryReadingMode readMode;
    AtEthPort *ports;
    const char *pHeading[] = {"Port Id", "LINK-UP", "LINK-DOWN", "LINK-LOS", "LINK-SYNC", "REA-LIGN", "AUTO-NEGSTATECHANGE"};

    if (argc < 2)
        {
        AtPrintc(cSevNormal, "Error: Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Get the shared ID buffer */
    ports = CliEthPortListGet(argv[0], &idBuf, &numPorts);
    if (ports == NULL)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);
    filteredEvent = CliCapturedEventFilterByChannels((AtChannel*)ports, numPorts, m_capturedEvents, readMode);

    if (argc >= 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(AtListLengthGet(filteredEvent), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    CliPutEventsToTable(tabPtr, filteredEvent, cAtEthPortAlarmtypeValue, mCount(cAtEthPortAlarmtypeValue), 0, readMode);
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(ports);

    return cAtTrue;
    }

eBool CmdAtEthAutoNegShow(char argc, char **argv)
    {
    uint32 *idBuf;
    uint32 bufferSize, numPorts, port_i;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Port Id", "Description", "enabled", "restart", "completed", "state", "speed", "duplex", "link", "remote-fault"};

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = PortGet(ethModule, idBuf[port_i] - 1);
        uint32 column = 0;
        eBool boolVal;
        eAtEthPortAutoNegState state;
        eAtEthPortSpeed speed;
        eAtEthPortDuplexMode duplex;
        eAtEthPortLinkStatus linkStatus;
        eAtEthPortRemoteFault remoteFault;
        const char *stringVal;

        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtChannelIdGet((AtChannel)port) + 1, "%u"));
        StrToCell(tabPtr, port_i, column++, AtObjectToString((AtObject)port));

        if (!AtEthPortAutoNegIsSupported(port))
            {
            for (; column < mCount(pHeading); column++)
                StrToCell(tabPtr, port_i, column, sAtNotSupported);
            continue;
            }

        boolVal = AtEthPortAutoNegIsEnabled(port);
        ColorStrToCell(tabPtr, port_i, column++, CliBoolToString(boolVal), boolVal ? cSevInfo : cSevCritical);

        /* If autoneg is disabled, other information will be invalid, just hide them */
        if (!AtEthPortAutoNegIsEnabled(port))
            {
            for (; column < mCount(pHeading); column++)
                StrToCell(tabPtr, port_i, column, sAtNotCare);
            continue;
            }

        boolVal = AtEthPortAutoNegRestartIsOn(port);
        ColorStrToCell(tabPtr, port_i, column++, boolVal ? "on" : "off", boolVal ? cSevWarning : cSevNormal);

        boolVal = AtEthPortAutoNegIsComplete(port);
        ColorStrToCell(tabPtr, port_i, column++, boolVal ? "yes" : "no", boolVal ? cSevInfo : cSevWarning);

        state = AtEthPortAutoNegStateGet(port);
        stringVal = AutoNegStateString(state);
        ColorStrToCell(tabPtr, port_i, column++, stringVal, (state == cAtEthPortAutoNegInvalid) ? cSevCritical : cSevNormal);

        speed = AtEthPortAutoNegSpeedGet(port);
        stringVal = CliEnumToString(speed, cAtEthPortSpeedMdStr, cAtEthPortSpeedMdVal, mCount(cAtEthPortSpeedMdVal), NULL);
        ColorStrToCell(tabPtr, port_i, column++, stringVal ? stringVal : "Error", stringVal ? cSevNormal : cSevCritical);

        duplex = AtEthPortAutoNegDuplexModeGet(port);
        stringVal = DuplexModeString(duplex);
        ColorStrToCell(tabPtr, port_i, column++, stringVal ? stringVal : "Error", DuplexModeColor(duplex));

        linkStatus = AtEthPortLinkStatus(port);
        stringVal = LinkStatus2String(linkStatus);
        ColorStrToCell(tabPtr, port_i, column++, stringVal, LinkStatusColor(linkStatus));

        remoteFault = AtEthPortRemoteFaultGet(port);
        stringVal = RemoteFault2String(remoteFault);
        ColorStrToCell(tabPtr, port_i, column++, stringVal ? stringVal : "Error", RemoteFaultColor(remoteFault));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtEthportDropPacketConditionMaskSet(char argc, char **argv)
    {
    uint32 *idBuf;
    uint32 bufferSize, numPorts, dropConditionMask, port_i;
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
    eBool enable = cAtFalse;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevNormal, "Error: Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return cAtFalse;
        }

    /* Get interrupt mask value from input string */
    dropConditionMask = CliMaskFromString(argv[1], cAtEthPortDropConditionStr, cAtEthPortDropConditionValue, mCount(cAtEthPortDropConditionValue));
    if (dropConditionMask == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = PortGet(ethModule, idBuf[port_i] - 1);
        eAtRet ret = AtEthPortDropPacketConditionMaskSet(port, dropConditionMask, enable ? dropConditionMask : 0x0);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevWarning, "ERROR: Cannot configure drop condition mask for port %u, ret = %s\r\n", idBuf[port_i], AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtEthportDropPacketConditionMaskGet(char argc, char **argv)
    {
    uint32 *idBuf;
    char buf[16];
    uint32 bufferSize, numPorts, dropConditionMask, port_i;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Port Id", "Drop-Packet-Conditions"};

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtEthPort port = PortGet(ethModule, idBuf[port_i] - 1);
        dropConditionMask = AtEthPortDropPacketConditionMaskGet(port);
        AtSprintf(buf, "%2u", AtChannelIdGet((AtChannel)port) + 1);
        StrToCell(tabPtr, port_i, 0, buf);
        StrToCell(tabPtr, port_i, 1, CliAlarmMaskToString(cAtEthPortDropConditionStr,
        											      cAtEthPortDropConditionValue,
                                                          mCount(cAtEthPortDropConditionValue),
														  dropConditionMask));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

eBool CmdAtEthPortAutoNegRestartOn(char argc, char **argv)
    {
    return AutoNegRestartWithHandler(argc, argv, AtEthPortAutoNegRestartOn, cAtTrue);
    }

eBool CmdAtEthPortAutoNegRestartOff(char argc, char **argv)
    {
    return AutoNegRestartWithHandler(argc, argv, AtEthPortAutoNegRestartOn, cAtFalse);
    }

eBool CmdAtEthPortAutoNegRestart(char argc, char **argv)
    {
    return AutoNegRestartWithHandler(argc, argv, AutoNegRestartTrigger, cAtTrue /* Not matter */);
    }

eBool CmdAtEthPortInterruptMaskGet(char argc, char **argv)
    {
    uint32 numPorts = 0;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);

    AtUnused(argc);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }

    return PortDefectShowWithSilent(ports, numPorts,
                                    AtChannelInterruptMaskGet,
                                    ColorForBitVal,
                                    InterruptMaskStringForBitVal,
                                    cAtFalse);
    }

eBool CmdAtEthPortDisparityForce(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, (EnableFunc)AtEthPortDisparityForce);
    }

eBool CmdAtEthPortDisparityUnForce(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, (EnableFunc)AtEthPortDisparityForce);
    }

eBool CmdAtEthPortRxFecEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, (EnableFunc)AtEthPortRxFecEnable);
    }

eBool CmdAtEthPortRxFecDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, (EnableFunc)AtEthPortRxFecEnable);
    }

eBool CmdAtEthPortTxFecEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, (EnableFunc)AtEthPortTxFecEnable);
    }

eBool CmdAtEthPortTxFecDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, (EnableFunc)AtEthPortTxFecEnable);
    }

eBool CmdAtEthPortFecEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, (EnableFunc)FecEnable);
    }

eBool CmdAtEthPortFecDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, (EnableFunc)FecEnable);
    }

eBool CmdAtEthPortAlarmForce(char argc, char **argv)
    {
    eAtDirection direction;
    uint32 numPorts = 0;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    uint32 forcedAlarm, port_i;
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
    ePortType portType;
    eBool forced = cAtFalse;
    eAtRet (*AlarmForce)(AtChannel self, uint32 alarmType);

    AtUnused(argc);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }
    portType = PortType(ports, numPorts);
    if (portType == cPortTypeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Subports and ports cannot be input on the same command\r\n");
        return cAtFalse;
        }

    /* Get alarm type from input string */
    if (portType == cPortTypePort)
        forcedAlarm = CliMaskFromString(argv[1], cAtEthPortAlarmtypeStr, cAtEthPortAlarmtypeValue, mCount(cAtEthPortAlarmtypeValue));
    else
        forcedAlarm = CliMaskFromString(argv[1], cAtEthSubPortAlarmTypeStr, cAtEthSubPortAlarmTypeVal, mCount(cAtEthSubPortAlarmTypeVal));
    if (forcedAlarm == 0)
        {
        AtPrintc(cSevInfo, "No alarms to force\r\n");
        return cAtTrue;
        }

    /* Direction */
    direction = CliDirectionFromStr(argv[2]);
    if(direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        return cAtFalse;
        }

    /* Enable/disable */
    mAtStrToBool(argv[3], forced, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable force. Expect: en/dis\r\n");
        return cAtFalse;
        }

    /* Determine API for forcing */
    if (direction == cAtDirectionTx)
        AlarmForce = forced ? AtChannelTxAlarmForce : AtChannelTxAlarmUnForce;
    else
        AlarmForce = forced ? AtChannelRxAlarmForce : AtChannelRxAlarmUnForce;

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel port = ports[port_i];
        eAtRet ret = AlarmForce(port, forcedAlarm);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevWarning,
                 "ERROR: %s alarm on %s fail with ret = %s\r\n",
                 forced ? "Forcing" : "Un-forcing",
                 CliChannelIdStringGet(port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtEthPortForcedAlarmShow(char argc, char **argv)
    {
    uint32 numPorts = 0, port_i;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Port Id", "Description", "ForcedTxAlarms", "ForcedRxAlarms"};

    AtUnused(argc);

    /* Get Port List */
    numPorts = CliEthPortListFromString(argv[0], ports, numPorts);
    if (numPorts == 0)
        {
        AtPrintc(cSevInfo, "WARNING: No port to display, the ID list may be wrong, expect: 1,2-3,..\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtChannel port = ports[port_i];
        uint32 column = 0;
        const char **alarmTypeStr = cAtEthPortAlarmtypeStr;
        const uint32 *alarmTypeVal = cAtEthPortAlarmtypeValue;
        uint32 alarmCount = mCount(cAtEthPortAlarmtypeValue);
        uint32 alarms;
        const char *stringValue;

        /* Subport */
        if (AtEthPortParentPortGet((AtEthPort)port))
            {
            alarmTypeStr = cAtEthSubPortAlarmTypeStr;
            alarmTypeVal = cAtEthSubPortAlarmTypeVal;
            alarmCount = mCount(cAtEthSubPortAlarmTypeVal);
            }

        /* Port ID and descriptions */
        StrToCell(tabPtr, port_i, column++, CliChannelIdStringGet(port));
        StrToCell(tabPtr, port_i, column++, AtObjectToString((AtObject)port));

        /* TX forced alarms */
        alarms = AtChannelTxForcedAlarmGet(port);
        stringValue = CliAlarmMaskToString(alarmTypeStr,
                                           alarmTypeVal,
                                           alarmCount,
                                           alarms);
        ColorStrToCell(tabPtr, port_i, column++, stringValue, alarms ? cSevCritical : cSevNormal);

        /* RX forced alarms */
        alarms = AtChannelRxForcedAlarmGet(port);
        stringValue = CliAlarmMaskToString(alarmTypeStr,
                                           alarmTypeVal,
                                           alarmCount,
                                           alarms);
        ColorStrToCell(tabPtr, port_i, column++, stringValue, alarms ? cSevCritical : cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtEthPortK30_7Force(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, (EnableFunc)AtEthPortK30_7Force);
    }

eBool CmdAtEthPortK30_7UnForce(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, (EnableFunc)AtEthPortK30_7Force);
    }

eBool CmdAtEthPortClockMonitorThresholdSet(char argc, char **argv)
    {
    AtList monitors = ClockMonitorGet(argv[0]);
    eBool success = CliAtClockMonitorThresholdSet(monitors, argv[1], argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)monitors);
    return success;
    }

eBool CmdAtEthPortClockMonitorGet(char argc, char **argv)
    {
    AtList monitors = ClockMonitorGet(argv[0]);
    eBool success = CliAtClockMonitorGet(monitors);
    AtUnused(argc);
    AtObjectDelete((AtObject)monitors);
    return success;
    }

eBool CmdAtEthPortVlanTransmitSet(char argc, char **argv)
    {
    return VlanSet(argc, argv, AtEthPortTxCVlanSet);
    }

eBool CmdAtEthPortVlanExpectedSet(char argc, char **argv)
    {
    return VlanSet(argc, argv, AtEthPortExpectedCVlanSet);
    }

eBool CmdAtEthPortVlanGet(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts, i;
    uint8 row = 0;
    AtChannel *channelList;
    tAtVlan vlan;
    tTab *tabPtr;
    char **heading;
    const char *colStr[] = {"Description", "TX C-VLAN", "Expected C-VLAN"};
    char buf[100];
    AtUnused(argc);

    /* Initialize structure */
    AtOsalMemInit(buf, 0x0, sizeof(buf));

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc(sizeof(char*) * (numPorts + 1UL));
    if (heading == NULL)
        return cAtFalse;

    for (i = 0; i <= numPorts; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(sizeof(char) * 32);
        if (heading[i] == NULL)
            {
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }
        }

    /* Create table with titles */
    AtSprintf((char *)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPorts; i++)
        AtSprintf((char *)heading[i], "%s", AtChannelIdString(channelList[i - 1]));

    tabPtr = TableAlloc(mCount(colStr), numPorts + 1UL, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtStringArrayFree(heading, numPorts + 1UL);
        return cAtFalse;
        }

    for (i = 0; i < mCount(colStr); i++)
        StrToCell(tabPtr, row++, 0, colStr[i]);

    /* Make table content */
    for (i = 1; i <= numPorts; i++)
        {
        AtEthPort port = (AtEthPort)channelList[i - 1];
        tAtVlan *pVlan = NULL;

        AtOsalMemInit(&vlan, 0x0, sizeof(tAtVlan));
        row = 0;

        /* Description */
        StrToCell(tabPtr, row++, i, AtObjectToString((AtObject)port));

        /* Print transmit C-VLAN */
        pVlan = AtEthPortTxCVlanGet(port, &vlan);
        StrToCell(tabPtr, row++, i, pVlan ? CliVlan2Str(pVlan) : "None");

        /* Print expected C-VLAN */
        pVlan = AtEthPortExpectedCVlanGet(port, &vlan);
        StrToCell(tabPtr, row++, i, pVlan ? CliVlan2Str(pVlan) : "None");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    AtStringArrayFree(heading, numPorts + 1UL);

    return cAtTrue;
    }

eBool CmdAtEthPortLinkTrainingEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, (EnableFunc)AtEthPortLinkTrainingEnable);
    }

eBool CmdAtEthPortLinkTrainingDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, (EnableFunc)AtEthPortLinkTrainingEnable);
    }

eBool CmdAtEthPortLinkTrainingRestart(char argc, char **argv)
    {
    uint32 bufferSize, i;
    uint8 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = AtEthPortLinkTrainingRestart((AtEthPort)channelList[i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot restart link training on port %u, ret = %s\r\n",
                     AtChannelIdGet(channelList[i]) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }


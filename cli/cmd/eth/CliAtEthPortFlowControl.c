/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : CliAtEthPortFlowControl.c
 *
 * Created Date: Feb 18, 2014
 *
 * Description : ETH Flow control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtEthFlowControl.h"
#include "CliAtModuleEth.h"
#include "CliAtEthFlowControl.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList FlowControls(char *idListStr)
    {
    uint8 port_i;
    uint32 *portList;
    AtList ethFlowControls;
    uint32 bufferSize, numberOfPorts;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    portList      = CliSharedIdBufferGet(&bufferSize);
    numberOfPorts = CliIdListFromString(idListStr, portList, bufferSize);
    if (numberOfPorts == 0)
        return NULL;

    ethFlowControls = AtListCreate(0);
    for (port_i = 0; port_i < numberOfPorts; port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(ethModule, (uint8)(portList[port_i] - 1));
        AtEthFlowControl ethFlowControl = AtEthPortFlowControlGet(port);
        if (ethFlowControl)
            AtListObjectAdd(ethFlowControls, (AtObject)ethFlowControl);
        }

    if (AtListLengthGet(ethFlowControls) == 0)
        {
        AtObjectDelete((AtObject)ethFlowControls);
        return NULL;
        }

    return ethFlowControls;
    }

eBool CmdEthPortFlowControlGet(char argc, char **argv)
    {
    eAtRet ret;
    AtList ethFlowControls = FlowControls(argv[0]);
    const char *pHeading[] = {"PortId", "Enable", "pauseFrameDMAC", "pauseFrameSMAC", "pauseFrameEthType", "pauseFramePeriod", "pauseFrameQuanta", "highThreshold", "lowThreshold"};
    tTab *tabPtr;
    uint32 port_i;
    static uint8 mac[cAtMacAddressLen];
    AtUnused(argc);

    if (AtListLengthGet(ethFlowControls) == 0)
        {
        AtPrintc(cSevWarning, "No Flow to display\r\n");
        AtObjectDelete((AtObject)ethFlowControls);
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(ethFlowControls), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)ethFlowControls);
        return cAtFalse;
        }

    for (port_i = 0; port_i < AtListLengthGet(ethFlowControls); port_i++)
        {
        AtEthFlowControl ethFlowControl = (AtEthFlowControl)AtListObjectGet(ethFlowControls, port_i);

        StrToCell(tabPtr, port_i, 0, (char *)CliChannelIdStringGet(AtEthFlowControlChannelGet(ethFlowControl)));
        StrToCell(tabPtr, port_i, 1, CliBoolToString(AtEthFlowControlIsEnabled(ethFlowControl)));

        /* Get Dest MAC */
        ret = AtEthFlowControlDestMacGet(ethFlowControl, mac);
        if(ret != cAtOk)
            ColorStrToCell(tabPtr, port_i, 2, "Error", cSevCritical);
        else
            StrToCell(tabPtr, port_i, 2, (void*)CliEthPortMacAddressByte2Str(mac));

        /* Get Source MAC */
        ret = AtEthFlowControlSourceMacGet(ethFlowControl, mac);
        if(ret != cAtOk)
            ColorStrToCell(tabPtr, port_i, 3, "Error", cSevCritical);
        else
            StrToCell(tabPtr, port_i, 3, (void*)CliEthPortMacAddressByte2Str(mac));

        StrToCell(tabPtr, port_i, 4, CliNumber2String(AtEthFlowControlEthTypeGet(ethFlowControl), "0x%x"));
        StrToCell(tabPtr, port_i, 5, CliNumber2String(AtEthFlowControlPauseFramePeriodGet(ethFlowControl), "0x%x"));
        StrToCell(tabPtr, port_i, 6, CliNumber2String(AtEthFlowControlPauseFrameQuantaGet(ethFlowControl), "0x%x"));
        StrToCell(tabPtr, port_i, 7, CliNumber2String(AtEthFlowControlHighThresholdGet(ethFlowControl), "0x%x"));
        StrToCell(tabPtr, port_i, 8, CliNumber2String(AtEthFlowControlLowThresholdGet(ethFlowControl), "0x%x"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)ethFlowControls);

    return cAtTrue;
    }

eBool CmdEthPortFlowCtrlCounterGet(char argc, char **argv)
    {
    tTab *tabPtr = NULL;
    uint8 port_i;
    eAtHistoryReadingMode readingMode;
    static char buf[8];
    const char *pHeading[] = {"PortId", "txPauseFrame", "RxPauseFrame"};
    eBool silent = cAtFalse;
    AtList ethFlowControls = FlowControls(argv[0]);
	AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    if (AtListLengthGet(ethFlowControls) == 0)
        {
        AtPrintc(cSevWarning, "No Flow to display\r\n");
        AtObjectDelete((AtObject)ethFlowControls);
        return cAtTrue;
        }

    /* Get counter mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
  	  	silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);
    
    if (!silent)
        {
        tabPtr = TableAlloc(AtListLengthGet(ethFlowControls), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            AtObjectDelete((AtObject)ethFlowControls);
            return cAtFalse;
            }
        }

    for (port_i = 0; port_i < AtListLengthGet(ethFlowControls); port_i++)
        {
        AtEthFlowControl ethFlowControl = (AtEthFlowControl)AtListObjectGet(ethFlowControls, port_i);
        uint16 txPktPauseFrame, rxPktPauseFrame;

        if (readingMode == cAtHistoryReadingModeReadToClear)
            {
            txPktPauseFrame = AtEthFlowControlTxPauseFrameCounterClear(ethFlowControl);
            rxPktPauseFrame = AtEthFlowControlRxPauseFrameCounterClear(ethFlowControl);
            }
        else
            {
            txPktPauseFrame = AtEthFlowControlRxPauseFrameCounterGet(ethFlowControl);
            rxPktPauseFrame = AtEthFlowControlRxPauseFrameCounterGet(ethFlowControl);
            }

        StrToCell(tabPtr, port_i, 0, (char *)CliChannelIdStringGet(AtEthFlowControlChannelGet(ethFlowControl)));

        AtSprintf(buf, "0x%x", txPktPauseFrame);
        StrToCell(tabPtr, port_i, 1, buf);

        AtSprintf(buf, "0x%x", rxPktPauseFrame);
        StrToCell(tabPtr, port_i, 2, buf);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)ethFlowControls);

    return cAtTrue;
    }

eBool CmdEthPortFlowControlPauseFrameDestMacSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlDestMacSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthPortFlowControlPauseFrameSrcMacSet(char argc, char** argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlSrcMacSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthPortFlowControlPauseFrameEthTypeSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlEthTypeSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthPortFlowControlHighThresholdSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlHighThresholdSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthPortFlowControlLowThresholdSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlLowThresholdSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthPortFlowControlPauseFramePeriodSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlPauseFramePeriodSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthPortFlowControlPauseFrameQuantaSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlPauseFrameQuantaSet(ethFlowControls, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

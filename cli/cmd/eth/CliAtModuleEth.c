/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : CliModuleEth.c
 *
 * Created Date: Feb 1, 2013
 *
 * Description : Ethernet module CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtModuleEth CliAtEthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static eBool DefaultTpIdSet(char argc, char **argv, eAtModuleEthRet (*TpIdSet)(AtModuleEth, uint16))
    {
    eAtRet ret = TpIdSet(CliAtEthModule(), (uint16)AtStrToDw(argv[0]));

    AtUnused(argc);

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change default TPID fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliAtEthModule(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdModuleEthISISMacSet(char argc, char **argv)
    {
    uint8 mac[cAtMacAddressLen];
    eAtRet ret;
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    if (!CliAtModuleEthMacAddrStr2Byte(argv[0], mac))
        {
        AtPrintc(cSevCritical, "Invalid MAC address\r\n");
        return cAtFalse;
        }

    ret = AtModuleEthISISMacSet(CliAtEthModule(), mac);
    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdModuleEthISISMacGet(char argc, char **argv)
    {
    uint8 mac[cAtMacAddressLen];
    eAtRet ret;
	AtUnused(argv);
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    ret = AtModuleEthISISMacGet(CliAtEthModule(), mac);
    if (ret != cAtOk)
    	return cAtFalse;
    	
    AtPrintc(cSevInfo, "%s\r\n", CliEthPortMacAddressByte2Str(mac));

    return cAtTrue;
    }

eBool CliAtModuleEthMacAddrStr2Byte(const char *pMacAddrStr, uint8 *pMacAddr)
    {
    uint32 numBytes = AtString2Bytes(pMacAddrStr, pMacAddr, cAtMacAddressLen, 16);
    return (numBytes == cAtMacAddressLen) ? cAtTrue : cAtFalse;
    }

eBool CliAtModuleEthIpv4AddrStr2Byte(const char *pIPv4AddrStr, uint8 *pIpv4Addr)
    {
    uint32 numBytes = AtString2Bytes(pIPv4AddrStr, pIpv4Addr, cAtIpv4AddressLen, 10);
    return (numBytes == cAtIpv4AddressLen) ? cAtTrue : cAtFalse;
    }

const char *CliAtModuleEthIPv4AddrByte2Str(const uint8 *pIPv4Addr)
    {
    return AtBytes2String(pIPv4Addr, cAtIpv4AddressLen, 10);
    }

const char *CliAtModuleEthIPv6AddrByte2Str(const uint8 *pIPv6Addr)
    {
    return AtBytes2String(pIPv6Addr, cAtIpv6AddressLen, 10);
    }

AtList CliEthFlowListFromString(char *pStrFlowIdList, uint32 * numFlows)
    {
    uint32 bufferSize;
    uint32 i;
    uint32 numIdsInList;
    AtModuleEth ethModule;
    uint32 *idBuffer;
    AtList flowList;

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    if ((numIdsInList = CliIdListFromString((void*)pStrFlowIdList, idBuffer, bufferSize)) == 0)
        return NULL;

    /* Get Ethernet Module */
    flowList = AtListCreate(0);
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    for (i = 0; i < numIdsInList; i++)
        {
        AtEthFlow flow = AtModuleEthFlowGet(ethModule,(uint16)(CliId2DriverId(idBuffer[i])));
        if (flow != NULL)
            AtListObjectAdd(flowList, (AtObject)flow);
        }

    *numFlows = AtListLengthGet(flowList);
    if (numFlows == 0)
        {
        AtObjectDelete((AtObject)flowList);
        return NULL;
        }

    return flowList;
    }

AtEthFlow *CliEthFlowsFromString(char *pStrFlowIdList, uint32 * numFlows)
    {
    uint32 bufferSize;
    uint32 i;
    uint32 numIdsInList;
    AtModuleEth ethModule;
    uint32 *idBuffer;
    AtEthFlow *flowList;
    uint32 flow_i = 0;

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    if ((numIdsInList = CliIdListFromString((void*)pStrFlowIdList, idBuffer, bufferSize)) == 0)
        return NULL;

    /* Get Ethernet Module */
    flowList = (AtEthFlow *)CliSharedChannelListGet(&bufferSize);
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    for (i = 0; i < numIdsInList; i++)
        {
        AtEthFlow flow = AtModuleEthFlowGet(ethModule,(uint16)(CliId2DriverId(idBuffer[i])));
        if (flow != NULL)
            {
            flowList[flow_i] = flow;
            flow_i++;
            }
        }

    *numFlows = flow_i;
    return flowList;
    }

eBool CmdAtModuleEthDebug(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtModuleDebug((AtModule)CliAtEthModule());
    return cAtTrue;
    }

eBool CmdAtModuleEthDefaultCVlanTpIdSet(char argc, char **argv)
    {
    return DefaultTpIdSet(argc, argv, AtModuleEthDefaultCVlanTpIdSet);
    }

eBool CmdAtModuleEthDefaultSVlanTpIdSet(char argc, char **argv)
    {
    return DefaultTpIdSet(argc, argv, AtModuleEthDefaultSVlanTpIdSet);
    }

eBool CmdAtModuleEthShow(char argc, char **argv)
    {
    tTab *pTab;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    uint32 row_i = 0;
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    /* Initialize table */
    pTab = TableAlloc(6, mCount(heading), heading);
    if (pTab == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    StrToCell(pTab, row_i, 0, "numPorts");
    StrToCell(pTab, row_i, 1, CliNumber2String(AtModuleEthMaxPortsGet(ethModule), "%u"));
    row_i = row_i + 1;

    StrToCell(pTab, row_i, 0, "numFlows");
    StrToCell(pTab, row_i, 1, CliNumber2String(AtModuleEthMaxFlowsGet(ethModule), "%u"));
    row_i = row_i + 1;

    StrToCell(pTab, row_i, 0, "nextFreeFlow");
    if (AtModuleEthMaxFlowsGet(ethModule) == 0)
        StrToCell(pTab, row_i, 1, "N/A");
    else
        StrToCell(pTab, row_i, 1, CliNumber2String(AtModuleEthFreeFlowIdGet(ethModule) + 1, "%u"));
    row_i = row_i + 1;

    StrToCell(pTab, row_i, 0, "defaultCVlanTpId");
    StrToCell(pTab, row_i, 1, CliNumber2String(AtModuleEthDefaultCVlanTpIdGet(ethModule), "0x%04x"));
    row_i = row_i + 1;

    StrToCell(pTab, row_i, 0, "defaultSVlanTpId");
    StrToCell(pTab, row_i, 1, CliNumber2String(AtModuleEthDefaultSVlanTpIdGet(ethModule), "0x%04x"));
    row_i = row_i + 1;

    enabled = AtModuleInterruptIsEnabled((AtModule)ethModule);
    StrToCell(pTab, row_i, 0, "Interrupt");
    StrToCell(pTab, row_i, 1, CliBoolToString(enabled));

    /* Show table */
    TablePrint(pTab);
    TableFree(pTab);

    return cAtTrue;
    }

eBool CmdEthInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdEthInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleEthInit(char argc, char **argv)
    {
    eAtRet ret = AtModuleInit((AtModule)CliAtEthModule());
    AtUnused(argc);
    AtUnused(argv);
    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : atcliethflow.c
 *
 * Created Date: Oct 10, 2012
 *
 * Description : Ethernet flow CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleEth.h"
#include "AtCli.h"
#include "AtOsal.h"
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtModuleEth.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"
#include "AtEthPort.h"
#include "AtEthFlow.h"
#include "AtMpBundle.h"
#include "CliAtModuleEth.h"
#include "AtCliModule.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtMacAddressStrLen (cAtMacAddressLen * 3)

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterType, counterKind, counterName)                     \
    CliCounterTypePrintToCell(tabPtr, counter_i++, flow_i + 1UL, CounterGet((AtChannel)flow, counterType), counterKind, counterName); \

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtModuleEthRet (*MacSetFunc)(AtEthFlow, uint8 *);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthFlow FlowGet(AtModuleEth ethModule, uint32 flowId)
    {
    return AtModuleEthFlowGet(ethModule, (uint16)flowId);
    }

static eBool EthFlowEnable(char argc, char **argv, eBool enable)
    {
    AtEthFlow flow;
    uint32 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numFlows;
    AtModuleEth ethModule;
    eAtRet ret;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid flow ID list. Expected: 1 or 1-16, ...\r\n");
        return cAtFalse;
        }

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    for (i = 0; i < numFlows; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        flow = FlowGet(ethModule, idBuf[i] - 1);
        ret = AtChannelEnable((AtChannel)flow, enable);
        if (ret != cAtOk)
            AtPrintc(cSevWarning, "Flow %u cannot be enabled/disabled, ret = %d\r\n", idBuf[i], ret);
        }

    return cAtTrue;
    }

static eBool EthFlowMacSet(char argc, char **argv, MacSetFunc macSetFunc)
    {
    AtEthFlow flow;
    uint16 i;
    uint32 *idBuf;
    uint32 numFlows;
    uint8 mac[cAtMacAddressLen];
    AtModuleEth ethModule;
    uint32 bufferSize;
    eAtRet ret = cAtOk;
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "Invalid MAC address\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numFlows; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        flow = FlowGet(ethModule, idBuf[i] - 1);
        if (flow != NULL)
            ret |= macSetFunc(flow, mac);
        else
            AtPrintc(cSevCritical, "Flow %u does not exist\r\n", idBuf[i]);
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

static char *VlanTag2Str(const tAtEthVlanTag *vlanTag)
    {
    static char vlanTagBuf[16];
    AtSprintf(vlanTagBuf, "%d.%d.%d", vlanTag->priority, vlanTag->cfi, vlanTag->vlanId);

    return vlanTagBuf;
    }

static char* TraffficDescr2Str(const tAtEthVlanDesc *trafficDescr)
    {
    static char vlansStrBuf[36];

    if ((trafficDescr == NULL) || (trafficDescr->numberOfVlans == 0))
        {
        AtSprintf(vlansStrBuf, "None");
        return vlansStrBuf;
        }

    /* If two VLANs exist, show the S-VLAN first */
    AtSprintf(vlansStrBuf, "port.%d,", trafficDescr->ethPortId + 1);
    if (trafficDescr->numberOfVlans == 2)
        {
        AtStrcat(vlansStrBuf, VlanTag2Str(&(trafficDescr->vlans[1])));
        AtStrcat(vlansStrBuf, ",");
        }

    /* Only C-VLAN exist */
    AtStrcat(vlansStrBuf, VlanTag2Str(&(trafficDescr->vlans[0])));

    return vlansStrBuf;
    }

/* Outside need to free returned string after using */
static char* VlanTrafficsDescriptionBuild(AtEthFlow flow,
                                          uint32 numTraffics,
                                          tAtEthVlanDesc *(*VlanAtIndex)(AtEthFlow, uint32, tAtEthVlanDesc *))
    {
    uint16 trafDescr_i;
    uint32 bufferSize = numTraffics * 40 + 8;
    char* vlanStrBuffer = AtOsalMemAlloc(bufferSize);
    AtOsalMemInit(vlanStrBuffer, 0, bufferSize);

    if (numTraffics == 0)
        {
        AtSprintf(vlanStrBuffer, "None");
        return vlanStrBuffer;
        }

    for (trafDescr_i = 0; trafDescr_i < numTraffics; trafDescr_i++)
        {
        tAtEthVlanDesc trafficDescr;
        static char buf[40];

        AtOsalMemInit(buf, 0, sizeof(buf));
        VlanAtIndex(flow, trafDescr_i, &trafficDescr);
        AtSnprintf(buf, sizeof(buf) - 1, (trafDescr_i == 0) ? "%s" : " - %s", TraffficDescr2Str(&trafficDescr));
        AtStrcat(vlanStrBuffer, buf);
        }

    return vlanStrBuffer;
    }

static char** BuildHeadingForFlows(AtEthFlow *flowList, uint32 currentFlowIndex, uint32 numFlowPerTable)
    {
    AtEthFlow flow;
    uint32 flow_i;
    char **headings;

    headings = AtOsalMemAlloc((numFlowPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(8);
    AtSprintf(headings[0], "Flow");

    for (flow_i = 0; flow_i < numFlowPerTable; flow_i++)
        {
        flow = flowList[currentFlowIndex + flow_i];
        headings[flow_i + 1] = AtOsalMemAlloc(8);
        AtSprintf(headings[flow_i + 1], "%u", AtChannelIdGet((AtChannel)flow) + 1);
        }

    return headings;
    }

static void DestroyHeadings(char **heading, uint32 numColumns)
    {
    uint32 i;

    if (heading == NULL)
        return;

    for (i = 0; i < numColumns; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static eBool CounterShow(AtEthFlow *flowList, uint32 numFlows, char argc, char **argv)
    {
    static const uint8 cNumFlowPerTable = 10;
    uint32 numTables, table_i;
    uint32  remainFlow = numFlows;
    uint32 (*CounterGet)(AtChannel self, uint16 counterType) = AtChannelCounterGet;
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Calculate number of tables need to show counters of all input Flows */
    numTables = numFlows / cNumFlowPerTable;
    if ((numFlows % cNumFlowPerTable) > 0)
        numTables = numTables + 1;

    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtChannelCounterClear;

    for (table_i = 0; table_i < numTables; table_i++)
        {
        tTab *tabPtr = NULL;
        uint16 flow_i;
        char **heading = NULL;
        uint8 counter_i = 0;
        uint32 numFlowPerTable;
        uint32 currentFlowIndex = table_i * cNumFlowPerTable;

        numFlowPerTable = remainFlow;
        if (numFlowPerTable > cNumFlowPerTable)
            numFlowPerTable = cNumFlowPerTable;

        /* Create table */
        if (!silent)
            {
            heading = BuildHeadingForFlows(flowList, currentFlowIndex, numFlowPerTable);
            tabPtr = TableAlloc(0, numFlowPerTable + 1, (void *)heading);
            if (!tabPtr)
                {
                AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
                return cAtFalse;
                }
            }

        /* Build table */
        for (flow_i = 0; flow_i < numFlowPerTable; flow_i++)
            {
            AtEthFlow flow = (AtEthFlow)flowList[currentFlowIndex + flow_i];

            counter_i = 0;
            mPutCounter(cAtEthFlowCounterTxPackets,                cAtCounterTypeGood, "txPacket");
            mPutCounter(cAtEthFlowCounterTxGoodPackets,            cAtCounterTypeGood, "txGoodPacket");
            mPutCounter(cAtEthFlowCounterTxBytes,                  cAtCounterTypeGood, "txByte");
            mPutCounter(cAtEthFlowCounterTxGoodBytes,              cAtCounterTypeGood, "txGoodByte");
            mPutCounter(cAtEthFlowCounterTxFragmentPackets,        cAtCounterTypeGood, "txFragment");
            mPutCounter(cAtEthFlowCounterTxGoodFragmentPackets,    cAtCounterTypeGood, "txGoodFragment");
            mPutCounter(cAtEthFlowCounterTxBECNPackets,            cAtCounterTypeGood, "txBECNPacket");
            mPutCounter(cAtEthFlowCounterTxFECNPackets,            cAtCounterTypeGood, "txFECNPacket");
            mPutCounter(cAtEthFlowCounterTxDEPackets,              cAtCounterTypeGood, "txDEPacket");
            mPutCounter(cAtEthFlowCounterTxNullFragmentPackets,    cAtCounterTypeError,"txNullFragment");
            mPutCounter(cAtEthFlowCounterTxDiscardPackets,         cAtCounterTypeError,"txDiscardPacket");
            mPutCounter(cAtEthFlowCounterTxDiscardBytes,           cAtCounterTypeError,"txDiscardByte");
            mPutCounter(cAtEthFlowCounterTxDiscardFragmentPackets, cAtCounterTypeError,"txDiscardFragment");
            mPutCounter(cAtEthFlowCounterTxTimeOuts,               cAtCounterTypeError,"txTimeOutPacket");
            mPutCounter(cAtEthFlowCounterTxWindowViolationPackets, cAtCounterTypeError,"txWindowViolation");
            mPutCounter(cAtEthFlowCounterTxMRRUExceedPackets,      cAtCounterTypeError,"txMrruExceed");

            mPutCounter(cAtEthFlowCounterRxPackets,                cAtCounterTypeGood, "rxPacket");
            mPutCounter(cAtEthFlowCounterRxGoodPackets,            cAtCounterTypeGood, "rxGoodPacket");
            mPutCounter(cAtEthFlowCounterRxBytes,                  cAtCounterTypeGood, "rxByte");
            mPutCounter(cAtEthFlowCounterRxGoodBytes,              cAtCounterTypeGood, "rxGoodByte");
            mPutCounter(cAtEthFlowCounterRxFragmentPackets,        cAtCounterTypeGood, "rxFragment");
            mPutCounter(cAtEthFlowCounterRxGoodFragmentPackets,    cAtCounterTypeGood, "rxGoodFragment");
            mPutCounter(cAtEthFlowCounterRxBECNPackets,            cAtCounterTypeGood, "rxBECNPacket");
            mPutCounter(cAtEthFlowCounterRxFECNPackets,            cAtCounterTypeGood, "rxFECNPacket");
            mPutCounter(cAtEthFlowCounterRxDEPackets,              cAtCounterTypeGood, "rxDEPacket");
            mPutCounter(cAtEthFlowCounterRxNullFragmentPackets,    cAtCounterTypeError, "rxNullFragment");
            mPutCounter(cAtEthFlowCounterRxDiscardPackets,         cAtCounterTypeError, "rxDiscardPacket");
            mPutCounter(cAtEthFlowCounterRxDiscardBytes,           cAtCounterTypeError, "rxDiscardByte");
            }

        TablePrint(tabPtr);
        TableFree(tabPtr);
        DestroyHeadings(heading, numFlowPerTable + 1);

        /* Next table */
        remainFlow = remainFlow - numFlowPerTable;
        }

    return cAtTrue;
    }

AtEthFlow *CliEthFlowListGet(char *pStrFlowIdList, uint32 *numFlows)
    {
    uint32 bufferSize;
    uint32 i;
    uint32 numValidFlows = 0;
    uint32 numIdsInList, numChannels;
    AtEthFlow flow;
    AtEthFlow *allFlows;
    AtModuleEth ethModule;
    uint32 *idBuffer;

    /* Init first to return when parsing fail */
    *numFlows = 0;

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    if ((numIdsInList = CliIdListFromString((void*)pStrFlowIdList, idBuffer, bufferSize)) == 0)
        return NULL;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Use shared buffer instead of alloc to avoid forgetting free */
    allFlows = (AtEthFlow *)((void**)CliSharedChannelListGet(&numChannels));

    for (i = 0; i < numIdsInList; i++)
        {
        flow = FlowGet(ethModule, idBuffer[i] - 1);
        if (flow != NULL)
            {
            allFlows[numValidFlows]= flow;
            numValidFlows++;

            /* If store full shared buffer */
            if (numValidFlows == numChannels)
                break;
            }
        }
    *numFlows = numValidFlows;

    return allFlows;
    }

eBool CmdEthFlowCreate(char argc, char **argv)
    {
    AtEthFlow flow;
    uint32 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numFlows;
    AtModuleEth ethModule;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        return cAtFalse;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    for (i = 0; i < numFlows; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        /* Create flow */
        flow = AtModuleEthFlowCreate(ethModule, (uint16)(idBuf[i] - 1));

        /* Check if it is created */
        if (flow == NULL)
            AtPrintc(cSevCritical, "ERROR: Flow %u cannot be created\r\n", idBuf[i]);
        }

    return cAtTrue;
    }

eBool CmdEthFlowEnable(char argc, char **argv)
    {
    return EthFlowEnable(argc, argv, cAtTrue);
    }

eBool CmdEthFlowDisable(char argc, char **argv)
    {
    return EthFlowEnable(argc, argv, cAtFalse);
    }

eBool CmdEthFlowDelete(char argc, char **argv)
    {
    uint32 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numFlows;
    AtModuleEth ethModule;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid flow ID list. Expected: 1 or 1-16, ...\r\n");
        return cAtFalse;
        }

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    for (i = 0; i < numFlows; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        if (AtModuleEthFlowDelete(ethModule, (uint16)(idBuf[i] - 1)) != cAtOk)
            AtPrintc(cSevWarning, "Flow %u cannot be deleted\r\n", idBuf[i]);
        }

    return cAtTrue;
    }

eBool CmdEthFlowEgDestMacSet(char argc, char **argv)
    {
    return EthFlowMacSet(argc, argv, AtEthFlowEgressDestMacSet);
    }

eBool CmdEthFlowEgSrcMacSet(char argc, char** argv)
    {
    return EthFlowMacSet(argc, argv, AtEthFlowEgressSourceMacSet);
    }

eBool CmdEthFlowEgVlanSet(char argc, char **argv)
    {
    AtEthFlow flow;
    uint16 i;
    uint32 *idBuf;
    uint32 numFlows;
    tAtEthVlanDesc desc;
    AtModuleEth ethModule;
    eAtRet ret = cAtOk;
	AtUnused(argc);

    /* Initialize structure */
    AtOsalMemInit(&desc, 0x0, sizeof(desc));

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&numFlows);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, numFlows)) == 0)
        return cAtFalse;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Get Port Id */
    desc.ethPortId = (uint8)(AtStrToDw(argv[1]) - 1);

    /* Get CVLAN and SVLAN Description */
    if (!CliEthFlowVlanGet(argv[2], argv[3], &desc))
        {
        AtPrintc(cSevCritical, "Invalid VLAN, expected %s\r\n", CliExpectedVlanFormat());
        return cAtFalse;
        }

    for (i = 0; i < numFlows; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        flow = FlowGet(ethModule, idBuf[i] - 1);
        if (flow != NULL)
            ret |= AtEthFlowEgressVlanSet(flow, &desc);
        else
            {
            AtPrintc(cSevCritical, "Flow %u does not exist\r\n", idBuf[i]);
            ret = cAtError;
            }
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

static void VlanDescriptionBufferMemoryFree(char *vlanDescriptionBuffer)
    {
    if (vlanDescriptionBuffer != NULL)
        AtOsalMemFree(vlanDescriptionBuffer);
    }

static eBool EthFlowVlanModify(char argc, char **argv, eAtModuleEthRet (*VlanModify)(AtEthFlow self, const tAtEthVlanDesc * desc))
    {
    uint16 i;
    uint32 *idBuf;
    uint32 numFlows;
    tAtEthVlanDesc desc;
    AtModuleEth ethModule;
    uint32 bufferSize;
    eBool success = cAtTrue;
    AtUnused(argc);

    AtOsalMemInit(&desc, 0x0, sizeof(desc));

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of flow may be invalid\r\n");
        return cAtFalse;
        }

    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    desc.ethPortId = (uint8)(AtAtoi(argv[1]) - 1);

    /* Get CVLAN and SVLAN Description */
    if (!CliEthFlowVlanGet(argv[2], argv[3], &desc))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid VLAN, expected %s\r\n", CliExpectedVlanFormat());
        return cAtFalse;
        }

    for (i = 0; i < numFlows; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtEthFlow flow = FlowGet(ethModule, idBuf[i] - 1);
        eAtRet ret = VlanModify(flow, &desc);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Add/Remove VLAN for flow %u fail with ret = %s\r\n",
                     idBuf[i],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliEthFlowsTableShow(AtEthFlow *flows, uint32 numFlows)
    {
    AtMpBundle mpBundle;
    AtHdlcLink hdlcLink;
    AtEncapChannel encapChannel;
    AtPw pw;
    uint16 flow_i;
    tTab *tabPtr;
    const char *heading[] = {"Flow ID", "SMAC", "DMAC", "Enable", "EG-VLANs",
                             "IG-VLANs", "Bind"};
    static char buf[16];
    static uint8 mac[cAtMacAddressStrLen];
    eAtRet ret = cAtOk;
    eBool convertSuccess;

    if (flows == NULL)
        return cAtFalse;

    /* Create table with titles */
    tabPtr = TableAlloc(numFlows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (flow_i = 0; flow_i < numFlows; flow_i = (uint16)AtCliNextIdWithSleep(flow_i, AtCliLimitNumRestPwGet()))
        {
        char *vlanDescriptionBuffer = NULL;
        uint32 numTraffics;
        AtEthFlow flow = flows[flow_i];
        eBool canControlMac = AtEthFlowEgressMacIsProgrammable(flow);

        /* Print ID */
        AtSprintf(buf, "%u", AtChannelIdGet((AtChannel)flow) + 1);
        StrToCell(tabPtr, flow_i, 0, buf);

        if (canControlMac)
            {
            /* Get source MAC */
            ret = AtEthFlowEgressSourceMacGet(flow, mac);
            if(ret != cAtOk)
                ColorStrToCell(tabPtr, flow_i, 1, "Error", cSevCritical);
            else
                StrToCell(tabPtr, flow_i, 1, CliEthPortMacAddressByte2Str(mac));

            /* Get source MAC */
            ret = AtEthFlowEgressDestMacGet(flow, mac);
            if(ret != cAtOk)
                ColorStrToCell(tabPtr, flow_i, 2, "Error", cSevCritical);
            else
                StrToCell(tabPtr, flow_i, 2, CliEthPortMacAddressByte2Str(mac));
            }
        else
            {
            ColorStrToCell(tabPtr, flow_i, 1, "N/A", cSevInfo);
            ColorStrToCell(tabPtr, flow_i, 2, "N/A", cSevInfo);
            }

        /* Enable */
        mAtBoolToStr(AtChannelIsEnabled((AtChannel)flow), buf, convertSuccess);
        StrToCell(tabPtr, flow_i, 3, convertSuccess ? buf : "Error");

        /* EG-VLANs */
        numTraffics = AtEthFlowEgressNumVlansGet(flow);
        vlanDescriptionBuffer = VlanTrafficsDescriptionBuild(flow, numTraffics, AtEthFlowEgressVlanAtIndex);
        StrToCell(tabPtr, flow_i, 4, vlanDescriptionBuffer);
        VlanDescriptionBufferMemoryFree(vlanDescriptionBuffer);

        /* Get IG VLANs */
        numTraffics = AtEthFlowIngressNumVlansGet(flow);
        vlanDescriptionBuffer = VlanTrafficsDescriptionBuild(flow, numTraffics, AtEthFlowIngressVlanAtIndex);
        StrToCell(tabPtr, flow_i, 5, vlanDescriptionBuffer);
        VlanDescriptionBufferMemoryFree(vlanDescriptionBuffer);

        /* Try to show channel that this flow is bound with */
        hdlcLink = AtEthFlowHdlcLinkGet(flow);
        mpBundle = AtEthFlowMpBundleGet(flow);
        encapChannel = AtEthFlowBoundEncapChannelGet(flow);
        pw = AtEthFlowPwGet(flow);
        if (encapChannel)
            AtSprintf(buf, "%s", CliChannelIdStringGet((AtChannel)encapChannel));
        else if (hdlcLink)
            AtSprintf(buf, "%s", CliChannelIdStringGet((AtChannel)hdlcLink));
        else if (mpBundle)
            {
            uint8 classId = AtMpBundleFlowClassNumberGet(mpBundle, flow);
            AtSprintf(buf, "%s.%d", CliChannelIdStringGet((AtChannel)mpBundle), classId);
            }
        else if (pw)
            AtSprintf(buf, "%s", CliChannelIdStringGet((AtChannel)pw));
        else
            AtSprintf(buf, "%s", "None");

        StrToCell(tabPtr, flow_i, 6, buf);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtPrintc(cSevInfo,   "* Note: \r\n");
    AtPrintc(cSevNormal, "  - Double VLANs: PortId,SVLAN,CVLAN\r\n");
    AtPrintc(cSevNormal, "  - Single VLAN : PortId,CVLAN\r\n");

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthFlowGet(char argc, char **argv)
    {
    AtEthFlow *flows;
    uint32 numFlows = 0;
	AtUnused(argc);

    /* Get Flow List */
    flows = CliEthFlowListGet(argv[0], &numFlows);

    /* Check if there is any flow need to be displayed */
    if (numFlows == 0)
        {
        AtPrintc(cSevNormal, "No flow to display\r\n");
        return cAtTrue;
        }

    return CliEthFlowsTableShow(flows, numFlows);
    }

eBool CmdEthFlowEgVlanAdd(char argc, char **argv)
    {
    return EthFlowVlanModify(argc, argv, AtEthFlowEgressVlanAdd);
    }

eBool CmdEthFlowIgVlanAdd(char argc, char **argv)
    {
    return EthFlowVlanModify(argc, argv, AtEthFlowIngressVlanAdd);
    }

eBool CmdEthFlowIgVlanRemove(char argc, char **argv)
    {
    return EthFlowVlanModify(argc, argv, AtEthFlowIngressVlanRemove);
    }

eBool CmdEthFlowEgVlanRemove(char argc, char **argv)
    {
    return EthFlowVlanModify(argc, argv, AtEthFlowEgressVlanRemove);
    }

eBool CmdEthFlowCounterGet(char argc, char **argv)
    {
    AtEthFlow *flows;
    uint32 numFlows;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of Flows */
    flows = CliEthFlowListGet(argv[0], &numFlows);

    /* Check if there is any flow need to be displayed */
    if ((numFlows == 0) || (flows == NULL))
        {
        AtPrintc(cSevNormal, "No flow to display\r\n");
        return cAtTrue;
        }

    return CounterShow(flows, numFlows, argc, argv);
    }

eBool CmdAtEthFlowDebug(char argc, char **argv)
    {
    AtEthFlow *flows;
    uint32   numFlows;
    uint32   wIndex;
	AtUnused(argc);

    flows = CliEthFlowListGet(argv[0], &numFlows);
    if ((flows == NULL) || (numFlows == 0))
        {
        AtPrintc(cSevWarning, "No flow to show debug information, list of flows may be invalid\r\n");
        return cAtTrue;
        }

    for (wIndex = 0; wIndex < numFlows; wIndex = AtCliNextIdWithSleep(wIndex, AtCliLimitNumRestPwGet()))
        AtChannelDebug((AtChannel)flows[wIndex]);

    return cAtTrue;
    }

static eBool VlanTypeSet(char argc, char** argv, eAtModuleEthRet (*VlanTpIdSet)(AtEthFlow, uint16))
    {
    uint16 i;
    uint32 *idBuf;
    uint32 numFlows;
    AtModuleEth ethModule;
    uint32 bufferSize;
    uint16 vlanType;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numFlows = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of flow may be invalid\r\n");
        return cAtFalse;
        }

    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    /* Get CVLAN and SVLAN Description */
    vlanType = (uint16)AtStrToDw(argv[1]);

    for (i = 0; i < numFlows; i++)
        {
        AtEthFlow flow = FlowGet(ethModule, idBuf[i] - 1);
        eAtRet ret = VlanTpIdSet(flow, vlanType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Change VLAN TPID for flow %u fail with ret = %s\r\n", idBuf[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdEthFlowEgressCVlanType(char argc, char** argv)
    {
    return VlanTypeSet(argc, argv, AtEthFlowEgressCVlanTpIdSet);
    }

eBool CmdEthFlowEgressSVlanType(char argc, char** argv)
    {
    return VlanTypeSet(argc, argv, AtEthFlowEgressSVlanTpIdSet);
    }

eBool CmdEthFlowVlanTypeGet(char argc, char** argv)
    {
    AtEthFlow *flows;
    uint32 numFlows = 0;
    uint32 flow_i;
    tTab *tabPtr;
    const char *heading[] = {"Flow ID", "Egress SVLAN Type", "Egress CVLAN Type"};

    AtUnused(argc);

    /* Get Flow List */
    flows = CliEthFlowListGet(argv[0], &numFlows);

    /* Check if there is any flow need to be displayed */
    if (numFlows == 0)
        {
        AtPrintc(cSevNormal, "No flow to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numFlows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        AtEthFlow flow = flows[flow_i];

        /* Print ID */
        StrToCell(tabPtr, flow_i, 0, (char *)CliChannelIdStringGet((AtChannel)flow));

        /* SVLAN Type */
        StrToCell(tabPtr, flow_i, 1, CliNumber2String(AtEthFlowEgressSVlanTpIdGet(flow), "0x%X"));

        /* CVLAN Type */
        StrToCell(tabPtr, flow_i, 2, CliNumber2String(AtEthFlowEgressCVlanTpIdGet(flow), "0x%X"));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

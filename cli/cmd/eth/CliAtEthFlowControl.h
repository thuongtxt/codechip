/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : CliAtEthFlowControl.h
 * 
 * Created Date: Oct 22, 2013
 *
 * Description : Ethernet flow control common CLI implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATETHFLOWCONTROL_H_
#define _CLIATETHFLOWCONTROL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "AtEthFlowControl.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CmdEthFlowControlDestMacSet(AtList ethFlowControls, const char *paramString);
eBool CmdEthFlowControlSrcMacSet(AtList ethFlowControls, const char *paramString);
eBool CmdEthFlowControlEthTypeSet(AtList ethFlowControls, const char *paramString);
eBool CmdEthFlowControlHighThresholdSet(AtList ethFlowControls, const char *paramString);
eBool CmdEthFlowControlLowThresholdSet(AtList ethFlowControls, const char *paramString);
eBool CmdEthFlowControlPauseFramePeriodSet(AtList ethFlowControls, const char *paramString);
eBool CmdEthFlowControlPauseFrameQuantaSet(AtList ethFlowControls, const char *paramString);

#endif /* _CLIATETHFLOWCONTROL_H_ */


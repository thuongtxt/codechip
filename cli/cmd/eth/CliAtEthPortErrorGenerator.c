/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : CliAtEthPortErrorGenerator.c
 *
 * Created Date: Mar 28, 2016
 *
 * Description : Ethernet port error generator CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#include "AtDevice.h"
#include "AtCli.h"
#include "AtCliModule.h"
#include "AtEthPort.h"
#include "../diag/CliAtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtErrorGenerator (*GeneratorGetFunction)(AtEthPort port);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel* PortsFromString(char* argv, uint32 *numChannels)
    {
    AtChannel *ports;
    uint32 numPorts, bufferSize;

    ports = CliSharedChannelListGet(&bufferSize);
    numPorts = CliEthPortListFromString(argv, ports, bufferSize);
    if (numPorts == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No ETH port is parsed\r\n");
        return NULL;
        }

    if (numChannels)
        *numChannels = numPorts;

    return ports;
    }

static eBool ErrorTypeSet(char argc, char **argv, GeneratorGetFunction generatorGet)
    {
    eAtRet apiReturn;
    eBool ret = cAtTrue;
    AtChannel *channels;
    uint32 errorType, i, numChannels;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channels = PortsFromString(argv[0], &numChannels);
    if (channels == NULL)
        return cAtFalse;

    errorType = CliEthPortErrorGeneratorErrorTypeFromString(argv[1]);
    if(errorType == cAtEthPortErrorGeneratorErrorTypeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid mode. Expected: %s\r\n", CliValidEthPortErrorGeneratorErrorType());
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        errorGenerator = generatorGet((AtEthPort)channels[i]);
        if (errorGenerator == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: No error generator is available at %s\r\n", AtObjectToString((AtObject)channels[i]));
            ret = cAtFalse;
            break;
            }

        apiReturn = AtErrorGeneratorErrorTypeSet(errorGenerator, errorType);
        if (apiReturn != cAtOk)
            {
            ret = cAtFalse;
            break;
            }
        }

    return ret;
    }

static eBool ErrorNumSet(char argc, char **argv, GeneratorGetFunction generatorGet)
    {
    eAtRet apiReturn;
    eBool ret = cAtTrue;
    AtChannel *channels;
    uint32 errorNum, i, numChannels;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channels = PortsFromString(argv[0], &numChannels);
    if (channels == NULL)
        return cAtFalse;

    errorNum = AtStrToDw(argv[1]);

    for (i = 0; i < numChannels; i++)
       {
       errorGenerator = generatorGet((AtEthPort)channels[i]);
       if (errorGenerator == NULL)
           {
           AtPrintc(cSevCritical, "ERROR: No error generator is available at %s\r\n", AtObjectToString((AtObject)channels[i]));
           ret = cAtFalse;
           break;
           }

       apiReturn = AtErrorGeneratorErrorNumSet(errorGenerator, errorNum);
       if (apiReturn != cAtOk)
           {
           ret = cAtFalse;
           break;
           }
       }

    return ret;
    }

static eBool Start(char argc, char **argv, GeneratorGetFunction generatorGet)
    {
    eAtRet apiReturn;
    AtChannel *channels;
    eBool ret = cAtTrue;
    uint32 i, numChannels;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channels = PortsFromString(argv[0], &numChannels);
    if (channels == NULL)
        return cAtFalse;

    for (i = 0; i < numChannels; i++)
       {
       errorGenerator = generatorGet((AtEthPort)channels[i]);
       if (errorGenerator == NULL)
           {
           AtPrintc(cSevCritical, "ERROR: No error generator is available at %s\r\n", AtObjectToString((AtObject)channels[i]));
           ret = cAtFalse;
           break;
           }

       apiReturn = AtErrorGeneratorStart(errorGenerator);
       if (apiReturn != cAtOk)
           {
           ret = cAtFalse;
           break;
           }
       }

    return ret;
    }

static eBool Stop(char argc, char **argv, GeneratorGetFunction generatorGet)
    {
    eAtRet apiReturn;
    eBool ret = cAtTrue;
    AtChannel *channels;
    uint32 i, numChannels;
    AtErrorGenerator errorGenerator;

    AtUnused(argc);
    channels = PortsFromString(argv[0], &numChannels);
    if (channels == NULL)
        return cAtFalse;

    for (i = 0; i < numChannels; i++)
       {
       errorGenerator = generatorGet((AtEthPort)channels[i]);
       if (errorGenerator == NULL)
           {
           AtPrintc(cSevCritical, "ERROR: No error generator is available at %s\r\n", AtObjectToString((AtObject)channels[i]));
           ret = cAtFalse;
           break;
           }

       apiReturn = AtErrorGeneratorStop(errorGenerator);
       if (apiReturn != cAtOk)
           {
           ret = cAtFalse;
           break;
           }
       }

    return ret;
    }

static eBool Show(char argc, char **argv, GeneratorGetFunction generatorGet)
    {
    uint32 i, numChannels = 1;
    AtChannel *channels;
    uint32 errorType, errorMode, errorNum;
    eBool isStarted, ret = cAtTrue;
    AtErrorGenerator errorGenerator;
    tTab                *tabPtr;
    const char          *pHeading[] = {"Channel ID", "gen-mode", "error-type", "is-started", "error-num"};

    AtUnused(argc);

    channels = PortsFromString(argv[0], &numChannels);
    if (channels == NULL)
        return cAtFalse;

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        char sharedString[64];
        const char *str;
        errorGenerator = generatorGet((AtEthPort)channels[i]);
        if (errorGenerator == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: No error generator is available at %s\r\n", AtObjectToString((AtObject)channels[i]));
            ret = cAtFalse;
            continue;
            }

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)channels[i]));

        errorMode = AtErrorGeneratorModeGet(errorGenerator);
        str = CliErrorGeneratorModeString(errorMode);
        StrToCell(tabPtr, i, column++, str != NULL ? str : "invalid");

        errorType = AtErrorGeneratorErrorTypeGet(errorGenerator);
        str = CliEthPortErrorGeneratorErrorTypeString(errorType);
        StrToCell(tabPtr, i, column++, str != NULL ? str : "invalid");

        isStarted = AtErrorGeneratorIsStarted(errorGenerator);
        StrToCell(tabPtr, i, column++, isStarted ? "en" : "dis");

        errorNum = AtErrorGeneratorErrorNumGet(errorGenerator);
        AtSprintf(sharedString, "%d", errorNum);
        StrToCell(tabPtr, i, column++, sharedString);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return ret;
    }

eBool CmdEthPortTxErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    return ErrorTypeSet(argc, argv, AtEthPortTxErrorGeneratorGet);
    }

eBool CmdEthPortTxErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    return ErrorNumSet(argc, argv, AtEthPortTxErrorGeneratorGet);
    }

eBool CmdEthPortTxErrorGeneratorStart(char argc, char **argv)
    {
    return Start(argc, argv, AtEthPortTxErrorGeneratorGet);
    }

eBool CmdEthPortTxErrorGeneratorStop(char argc, char **argv)
    {
    return Stop(argc, argv, AtEthPortTxErrorGeneratorGet);
    }

eBool CmdEthPortTxErrorGeneratorShow(char argc, char **argv)
    {
    return Show(argc, argv, AtEthPortTxErrorGeneratorGet);
    }

eBool CmdEthPortRxErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    return ErrorTypeSet(argc, argv, AtEthPortRxErrorGeneratorGet);
    }

eBool CmdEthPortRxErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    return ErrorNumSet(argc, argv, AtEthPortRxErrorGeneratorGet);
    }

eBool CmdEthPortRxErrorGeneratorStart(char argc, char **argv)
    {
    return Start(argc, argv, AtEthPortRxErrorGeneratorGet);
    }

eBool CmdEthPortRxErrorGeneratorStop(char argc, char **argv)
    {
    return Stop(argc, argv, AtEthPortRxErrorGeneratorGet);
    }

eBool CmdEthPortRxErrorGeneratorShow(char argc, char **argv)
    {
    return Show(argc, argv, AtEthPortRxErrorGeneratorGet);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : CliAtModuleEth.h
 * 
 * Created Date: Nov 6, 2013
 *
 * Description : Ethernet module CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULEETH_H_
#define _CLIATMODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtOsal.h"
#include "AtCommon.h"
#include "AtClasses.h"

#include "AtDevice.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtEthFlow.h"
#include "AtPtchService.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tCliEthPortCounter
    {
    eAtEthPortCounterType type;
    eAtCounterType kind;
    const char *name;
    }tCliEthPortCounter;

typedef struct tCliEthPortCounter * CliEthPortCounter;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtList CliAtEthPortSerdesControllers(char *pPortListString);
char *CliEthPortMacAddressByte2Str(const uint8 *pMacAddr);
eBool CliAtModuleEthMacAddrStr2Byte(const char *pMacAddrStr, uint8 *pMacAddr);
eBool CliAtModuleEthIpv4AddrStr2Byte(const char *pIPv4AddrStr, uint8 *pIpv4Addr);
const char *CliAtModuleEthIPv4AddrByte2Str(const uint8 *pIPv4Addr);
const char *CliAtModuleEthIPv6AddrByte2Str(const uint8 *pIPv6Addr);
AtList CliEthFlowListFromString(char *pStrFlowIdList, uint32 * numFlows);
AtEthFlow *CliEthFlowsFromString(char *pStrFlowIdList, uint32 * numFlows);
eBool CliEthFlowVlanGet(char *pStrCvlan, char *pStrSvlan, tAtEthVlanDesc *desc);
eBool CliEthFlowsTableShow(AtEthFlow *flows, uint32 numFlows);
eBool CliAtEthPortAutoNegEnable(AtEthPort *ports, uint32 numPorts, eBool enabled);
eBool CliAtPtchServiceIngressModeSet(AtPtchService (*PtchService)(const char *), char argc, char **argv);

#endif /* _CLIATMODULEETH_H_ */


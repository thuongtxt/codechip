/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : CliAtEthFlowControl.c
 *
 * Created Date: Oct 5, 2013
 *
 * Description : Ethernet flow control common CLI implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtEthFlow.h"
#include "CliAtEthFlowControl.h"
#include "CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtModuleEthRet (*EthFlowControlMacSet)(AtEthFlowControl, uint8 *);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FlowControlMacSet(AtList flowControls, const char *paramString, EthFlowControlMacSet setFunc)
    {
    eAtRet ret = cAtOk;
    uint8  mac[cAtMacAddressLen];
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(flowControls) == 0)
        {
        AtPrintc(cSevInfo, "No flow control to configure\r\n");
        return cAtTrue;
        }

    AtOsalMemInit(mac, 0x0, cAtMacAddressLen);

    if (!CliAtModuleEthMacAddrStr2Byte(paramString, mac))
        {
        AtPrintc(cSevCritical, "Invalid MAC address\r\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(flowControls); i++)
        {
        AtEthFlowControl flowControl = (AtEthFlowControl)AtListObjectGet(flowControls, i);
        ret = setFunc(flowControl, mac);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set MAC flow control for channel %s, ret = %s\r\n",
                     CliChannelIdStringGet(AtEthFlowControlChannelGet(flowControl)),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool AttributeSet(AtList ethFlowControls, const char *paramString, eAtModuleEthRet (*AttributeSetFunc)(AtEthFlowControl self, uint32 value))
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 value;

    if (AtListLengthGet(ethFlowControls) == 0)
        {
        AtPrintc(cSevInfo, "No Flow to configure\r\n");
        return cAtTrue;
        }

    value = AtStrToDw(paramString);

    for (i = 0; i < AtListLengthGet(ethFlowControls); i++)
        {
        AtEthFlowControl ethFlowControl = (AtEthFlowControl)AtListObjectGet(ethFlowControls, i);
        ret |= AttributeSetFunc(ethFlowControl, value);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, AtEthFlowControlChannelGet(ethFlowControl), "Can not set value");
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdEthFlowControlDestMacSet(AtList ethFlowControls, const char *paramString)
    {
    return FlowControlMacSet(ethFlowControls, paramString, AtEthFlowControlDestMacSet);
    }

eBool CmdEthFlowControlSrcMacSet(AtList ethFlowControls, const char *paramString)
    {
    return FlowControlMacSet(ethFlowControls, paramString, AtEthFlowControlSourceMacSet);
    }

eBool CmdEthFlowControlEthTypeSet(AtList ethFlowControls, const char *paramString)
    {
    return AttributeSet(ethFlowControls, paramString, AtEthFlowControlEthTypeSet);
    }

eBool CmdEthFlowControlHighThresholdSet(AtList ethFlowControls, const char *paramString)
    {
    return AttributeSet(ethFlowControls, paramString, AtEthFlowControlHighThresholdSet);
    }

eBool CmdEthFlowControlLowThresholdSet(AtList ethFlowControls, const char *paramString)
    {
    return AttributeSet(ethFlowControls, paramString, AtEthFlowControlLowThresholdSet);
    }

eBool CmdEthFlowControlPauseFramePeriodSet(AtList ethFlowControls, const char *paramString)
    {
    return AttributeSet(ethFlowControls, paramString, AtEthFlowControlPauseFramePeriodSet);
    }

eBool CmdEthFlowControlPauseFrameQuantaSet(AtList ethFlowControls, const char *paramString)
    {
    return AttributeSet(ethFlowControls, paramString, AtEthFlowControlPauseFrameQuantaSet);
    }

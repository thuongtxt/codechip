/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : CliAtEthFlowFlowControl.c
 *
 * Created Date: Dec 6, 2013
 *
 * Description : Flow control CLI of ETH Flow
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtEthPort.h"
#include "AtEthFlow.h"
#include "CliAtEthFlowControl.h"
#include "CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList FlowControls(char *pFlowListString)
    {
    uint32 *flowIdList;
    AtList ethFlowControls;
    uint32 bufferSize, numberOfFlows;
    uint32 i;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    flowIdList    = CliSharedIdBufferGet(&bufferSize);
    numberOfFlows = CliIdListFromString(pFlowListString, flowIdList, bufferSize);
    if (numberOfFlows == 0)
        return NULL;

    ethFlowControls = AtListCreate(0);
    for (i = 0; i < numberOfFlows; i++)
        {
        uint32 flowId = flowIdList[i] - 1;
        AtEthFlow flow = AtModuleEthFlowGet(ethModule, (uint16)flowId);
        AtEthFlowControl ethFlowControl = AtEthFlowFlowControlGet(flow);
        if (ethFlowControl)
            AtListObjectAdd(ethFlowControls, (AtObject)ethFlowControl);
        }

    if (AtListLengthGet(ethFlowControls) == 0)
        {
        AtObjectDelete((AtObject)ethFlowControls);
        return NULL;
        }

    return ethFlowControls;
    }

eBool CmdEthFlowFlowControlDestMacSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlDestMacSet(ethFlowControls, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthFlowFlowControlSrcMacSet(char argc, char** argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlSrcMacSet(ethFlowControls, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthFlowFlowControlEthTypeSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlEthTypeSet(ethFlowControls, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthFlowFlowControlHighThresholdSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlHighThresholdSet(ethFlowControls, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthFlowFlowControlLowThresholdSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlLowThresholdSet(ethFlowControls, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthFlowFlowControlPauseFramePeriodSet(char argc, char **argv)
    {
    AtList ethFlowControls = FlowControls(argv[0]);
    eBool success = CmdEthFlowControlPauseFramePeriodSet(ethFlowControls, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)ethFlowControls);
    return success;
    }

eBool CmdEthFlowFlowControlGet(char argc, char **argv)
    {
    eAtRet ret;
    AtList ethFlowControls = FlowControls(argv[0]);
    const char *pHeading[] = {"Flow ID", "DMAC", "SMAC", "ethType", "highThreshold", "lowThreshold", "pausePeriod"};
    tTab *tabPtr;
    uint32 flow_i;
    static uint8 mac[cAtMacAddressLen];
    static char buf[16];
    AtUnused(argc);

    if (AtListLengthGet(ethFlowControls) == 0)
        {
        AtPrintc(cSevWarning, "No Flow to display\r\n");
        AtObjectDelete((AtObject)ethFlowControls);
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(ethFlowControls), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)ethFlowControls);
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < AtListLengthGet(ethFlowControls); flow_i++)
        {
        AtEthFlowControl ethFlowControl = (AtEthFlowControl)AtListObjectGet(ethFlowControls, flow_i);

        StrToCell(tabPtr, flow_i, 0, (char *)CliChannelIdStringGet(AtEthFlowControlChannelGet(ethFlowControl)));

        /* Get Dest MAC */
        ret = AtEthFlowControlDestMacGet(ethFlowControl, mac);
        if(ret != cAtOk)
            ColorStrToCell(tabPtr, flow_i, 1, "Error", cSevCritical);
        else
            StrToCell(tabPtr, flow_i, 1, (void*)CliEthPortMacAddressByte2Str(mac));

        /* Get Source MAC */
        ret = AtEthFlowControlSourceMacGet(ethFlowControl, mac);
        if(ret != cAtOk)
            ColorStrToCell(tabPtr, flow_i, 2, "Error", cSevCritical);
        else
            StrToCell(tabPtr, flow_i, 2, (void*)CliEthPortMacAddressByte2Str(mac));

        AtSprintf(buf, "0x%x", AtEthFlowControlEthTypeGet(ethFlowControl));
        StrToCell(tabPtr, flow_i, 3, buf);

        AtSprintf(buf, "0x%x", AtEthFlowControlHighThresholdGet(ethFlowControl));
        StrToCell(tabPtr, flow_i, 4, buf);

        AtSprintf(buf, "0x%x", AtEthFlowControlLowThresholdGet(ethFlowControl));
        StrToCell(tabPtr, flow_i, 5, buf);

        AtSprintf(buf, "0x%x", AtEthFlowControlPauseFramePeriodGet(ethFlowControl));
        StrToCell(tabPtr, flow_i, 6, buf);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)ethFlowControls);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : CliAtEthPortEyeScan.c
 *
 * Created Date: Dec 25, 2014
 *
 * Description : ETH Port eye scan
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleEth.h"
#include "AtTokenizer.h"
#include "../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ShowLaneFormatError(void)
    {
    AtPrintc(cSevCritical, "ERROR: Invalid lane ID, expect format: <portId>.<laneId>\r\n");
    }

static AtModuleEth EthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    }

static AtList LanesFromIdString(char *lanesString)
    {
    AtIdParser idParser;
    char idMaxformat[8];
    AtModuleEth ethModule = EthModule();
    AtList lanes;

    /* Create ID parser */
    AtSprintf(idMaxformat, "%d.%d", AtModuleEthMaxPortsGet(ethModule), AtEyeScanControllerMaxNumLanes());
    idParser = AtIdParserNew(lanesString, "1.1", idMaxformat);
    if (idParser == NULL)
        {
        ShowLaneFormatError();
        return NULL;
        }

    /* Have all valid lanes */
    lanes = AtListCreate(0);
    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 portId  = AtIdParserNextNumber(idParser);
        uint32 laneId  = AtIdParserNextNumber(idParser);
        AtEthPort port = AtModuleEthPortGet(ethModule, (uint8)(portId - 1));
        AtEyeScanController controller = AtEthPortEyeScanControllerGet(port);
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(controller, (uint8)(laneId - 1));
        if (lane)
            AtListObjectAdd(lanes, (AtObject)lane);
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: Ignore invalid lane %d.%d which does not exist\r\n",
                     portId, laneId);
            }
        }

    /* Free resources */
    AtObjectDelete((AtObject)idParser);
    if (AtListLengthGet(lanes) == 0)
        {
        AtObjectDelete((AtObject)lanes);
        lanes = NULL;
        AtPrintc(cSevCritical, "ERROR: No lanes are parsed. The ID may be wrong, expected: <portId>.<laneId>\r\n");
        }

    return lanes;
    }

static AtEyeScanLane LaneFromIdString(char *laneString)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(laneString, ".");
    uint32 portId, laneId;
    AtEthPort port;
    AtEyeScanLane lane;

    if (AtTokenizerNumStrings(tokenizer) != 2)
        return NULL;

    portId = AtStrToDw(AtTokenizerNextString(tokenizer)) - 1;
    laneId = AtStrToDw(AtTokenizerNextString(tokenizer)) - 1;
    port   = AtModuleEthPortGet(EthModule(), (uint8)portId);
    lane   = AtEyeScanControllerLaneGet(AtEthPortEyeScanControllerGet(port), (uint8)laneId);
    if (lane == NULL)
        ShowLaneFormatError();

    return lane;
    }

static eBool AttributeSet(char argc, char **argv,
                          eBool (*attributeSetFunc)(AtList lanes, char *valueString))
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool result = attributeSetFunc(lanes, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return result;
    }

static eBool EyeScanEnable(char argc, char **argv, eBool enable)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneEnable(lanes, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }

static eBool DebugEnable(char argc, char **argv, eBool enable)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneDebugEnable(lanes, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }

AtList CliAtEthPortSerdesEyeScanControllerFromIdString(char *idString)
    {
    uint8 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPorts;
    AtModuleEth ethModule;
    AtList controllers = NULL;

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(idString, idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list. Expected: 1 or 1-2, ...\r\n");
        return NULL;
        }

    controllers = AtListCreate(0);
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    for (i = 0; i < numPorts; i++)
        {
        AtEthPort port = AtModuleEthPortGet(ethModule, (uint8)(idBuf[i] - 1));
        AtSerdesController serdesController = AtEthPortSerdesController(port);
        AtEyeScanController controller = AtSerdesControllerEyeScanControllerGet(serdesController);
        if (controller)
            AtListObjectAdd(controllers, (AtObject)controller);
        else
            AtPrintc(cSevWarning, "WARNING: port %d does not have eye scan controllers\r\n", idBuf[i]);
        }

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no eye scan controller, ID may be wrong or ports do not have any controllers\r\n");
        AtObjectDelete((AtObject)controllers);
        controllers = NULL;
        }

    return controllers;
    }

eBool CmdAtEthPortSerdesEyeScan(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesEyeScanControllerFromIdString(argv[0]);
    eBool result = CliAtEyeScanControllerScan(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return result;
    }

eBool CmdAtEthPortSerdesEyeScanLaneDrpRead(char argc, char **argv)
    {
    return CliAtEyeScanLaneDrpRead(LaneFromIdString(argv[0]), argc, argv);
    }

eBool CmdAtEthPortSerdesEyeScanLaneDrpWrite(char argc, char **argv)
    {
    return CliAtEyeScanLaneDrpWrite(LaneFromIdString(argv[0]), argc, argv);
    }

eBool CmdAtEthPortSerdesEyeScanLaneStepSizeHorizonalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneStepSizeHorizonalSet);
    }

eBool CmdAtEthPortSerdesEyeScanLaneStepSizeVerticalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneStepSizeVerticalSet);
    }

eBool CmdAtEthPortSerdesEyeScanLaneMaxPrescaleSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneMaxPrescaleSet);
    }

eBool CmdAtEthPortSerdesEyeScanLaneShow(char argc, char **argv)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool result = CliAtEyeScanLaneShow(lanes);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return result;
    }

eBool CmdAtEthPortSerdesEyeScanLaneEnable(char argc, char **argv)
    {
    return EyeScanEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEthPortSerdesEyeScanLaneDisable(char argc, char **argv)
    {
    return EyeScanEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEthPortSerdesEyeScanControllerEqualizerSet(char argc, char **argv)
    {
    AtList controllers = CliAtEthPortSerdesEyeScanControllerFromIdString(argv[0]);
    eBool success = CliAtEyeScanControllerEqualizerSet(controllers, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtEthPortSerdesEyeScanLaneDebugEnable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtTrue);
    }

eBool CmdAtEthPortSerdesEyeScanLaneDebugDisable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtFalse);
    }

eBool CmdAtEthPortSerdesEyeScanLaneDebug(char argc, char **argv)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    AtUnused(argc);
    CliAtEyeScanLaneDebug(lanes);
    AtObjectDelete((AtObject)lanes);
    return cAtTrue;
    }


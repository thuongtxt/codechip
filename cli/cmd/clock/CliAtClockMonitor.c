/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : CliAtClockMonitor.c
 *
 * Created Date: Aug 7, 2017
 *
 * Description : Clock monitor CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleClock.h"
#include "AtCli.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtClockMonitorThresholdStr[] = {"loss-of-clock",
                                                    "frequency-out-of-range"};

static const uint32 cAtClockMonitorThresholdVal[] = {
                                                     cAtClockMonitorThresholdLossOfClock,
                                                     cAtClockMonitorThresholdFrequencyOutOfRange
};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CliAtClockMonitorThresholdSet(AtList clockMonitors, char *names, char *values)
    {
    AtTokenizer nameTokenizer, valueTokenizer;
    char *thresholdName;
    uint32 thresholdValue = 0;
    eBool success = cAtTrue;

    if (AtListLengthGet(clockMonitors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no clock monitor to configure\r\n");
        return cAtFalse;
        }

    nameTokenizer = AtTokenizerNew(names, "|");
    if (AtTokenizerNumStrings(nameTokenizer) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no thresholds to be configured\r\n");
        AtObjectDelete((AtObject)nameTokenizer);
        return cAtTrue;
        }

    valueTokenizer = AtTokenizerNew(values, ",");
    if (AtTokenizerNumStrings(valueTokenizer) == 0)
        {
        AtPrintc(cSevCritical, "WARNING: no input values\r\n");
        AtObjectDelete((AtObject)nameTokenizer);
        AtObjectDelete((AtObject)valueTokenizer);
        return cAtFalse;
        }

    while ((thresholdName = AtTokenizerNextString(nameTokenizer)) != NULL)
        {
        eBool convertSuccess = cAtTrue;
        eAtClockMonitorThreshold thresholdType;
        uint32 monitor_i, numClockMonitor;

        thresholdType = CliStringToEnum(thresholdName,
                                        cAtClockMonitorThresholdStr, cAtClockMonitorThresholdVal, mCount(cAtClockMonitorThresholdVal),
                                        &convertSuccess);
        if (!convertSuccess)
            {
            AtPrintc(cSevCritical, "ERROR: Please input valid threshold type. Expect: ");
            CliExpectedValuesPrint(cSevCritical, cAtClockMonitorThresholdStr, mCount(cAtClockMonitorThresholdStr));
            AtPrintc(cSevCritical, "\r\n");
            success = cAtFalse;
            continue;
            }

        /* If values are not fully input, just use the last input value */
        if (AtTokenizerHasNextString(valueTokenizer))
            thresholdValue = AtStrToDw(AtTokenizerNextString(valueTokenizer));

        /* Now apply for all of input monitors */
        numClockMonitor = AtListLengthGet(clockMonitors);
        for (monitor_i = 0; monitor_i < numClockMonitor; monitor_i++)
            {
            AtClockMonitor monitor = (AtClockMonitor)AtListObjectGet(clockMonitors, monitor_i);
            eAtRet ret;

            if (!AtClockMonitorThresholdIsSupported(monitor, thresholdType))
                {
                AtPrintc(cSevWarning, "WARNING: %s does not support changing threshold %s\r\n",
                         AtObjectToString((AtObject)monitor),
                         thresholdName);
                continue;
                }

            ret = AtClockMonitorThresholdSet(monitor, thresholdType, thresholdValue);
            if (ret == cAtOk)
                continue;

            AtPrintc(cSevCritical, "ERROR: Changing threshold %s on %s fail with ret = %s\r\n",
                     thresholdName,
                     AtObjectToString((AtObject)monitor),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)nameTokenizer);
    AtObjectDelete((AtObject)valueTokenizer);

    return success;
    }

eBool CliAtClockMonitorGet(AtList clockMonitors)
    {
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Channel", "current(ppm)", "loss-of-clock-threshold(ppm)", "frequency-out-of-range-threshold(ppm)"};
    uint32 numClockMonitor, monitor_i;
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);

    numClockMonitor = AtListLengthGet(clockMonitors);
    tabPtr = TableAlloc(numClockMonitor, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (monitor_i = 0; monitor_i < numClockMonitor; monitor_i++)
        {
        AtClockMonitor monitor = (AtClockMonitor)AtListObjectGet(clockMonitors, monitor_i);
        uint32 column = 0;
        AtChannel channel;

        channel = (AtChannel)AtClockMonitorMonitoredObjectGet(monitor);
        StrToCell(tabPtr, monitor_i, column++, CliChannelIdStringGet((AtChannel)channel));

        StrToCell(tabPtr, monitor_i, column++, CliNumber2String(AtClockMonitorCurrentPpmGet(monitor), "%u"));

        AtSnprintf(buffer, bufferSize, "%u/%u(max)",
                   AtClockMonitorThresholdGet(monitor, cAtClockMonitorThresholdLossOfClock),
                   AtClockMonitorThresholdMax(monitor, cAtClockMonitorThresholdLossOfClock));
        StrToCell(tabPtr, monitor_i, column++, buffer);

        AtSnprintf(buffer, bufferSize, "%u/%u(max)",
                   AtClockMonitorThresholdGet(monitor, cAtClockMonitorThresholdFrequencyOutOfRange),
                   AtClockMonitorThresholdMax(monitor, cAtClockMonitorThresholdFrequencyOutOfRange));
        StrToCell(tabPtr, monitor_i, column++, buffer);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

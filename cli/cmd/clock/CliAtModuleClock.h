/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : CliAtClockExtractor.h
 * 
 * Created Date: Feb 17, 2015
 *
 * Description : Clock extractor CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATCLOCKEXTRACTOR_H_
#define _CLIATCLOCKEXTRACTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtClockMonitor.h"
#include "AtClockExtractor.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtList CliAtModuleClockExtractorsFromString(AtList extractorList, char *extractorIdListString);
eBool CliAtClockMonitorThresholdSet(AtList clockMonitors, char *names, char *values);
eBool CliAtClockMonitorGet(AtList clockMonitors);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATCLOCKEXTRACTOR_H_ */


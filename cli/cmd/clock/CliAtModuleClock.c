/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : CliAtModuleClock.c
 *
 * Created Date: Nov 22, 2013
 *
 * Description : Clock module CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "CliAtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleClock ClockModule(void)
    {
    return (AtModuleClock)AtDeviceModuleGet(CliDevice(), cAtModuleClock);
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)ClockModule(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

AtList CliAtModuleClockExtractorsFromString(AtList extractorList, char *extractorIdListString)
    {
    uint32 *extractorIds;
    uint32 bufferSize, numberOfExtractors, i;

    extractorIds = CliSharedIdBufferGet(&bufferSize);
    numberOfExtractors = CliIdListFromString(extractorIdListString, extractorIds, bufferSize);
    if (numberOfExtractors == 0)
        return NULL;

    for (i = 0; i < numberOfExtractors; i++)
        {
        AtClockExtractor extractor;

        if (i > cBit7_0)
            continue;

        extractor = AtModuleClockExtractorGet(ClockModule(), (uint8)(extractorIds[i] - 1));
        if (extractor)
            AtListObjectAdd(extractorList, (AtObject)extractor);
        }

    return extractorList;
    }

eBool CmdAtModuleClockDebug(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebug((AtModule)ClockModule());

    return cAtTrue;
    }

eBool CmdAtModuleClockCheck(char argc, char **argv)
    {
    AtModuleClock clockModule = ClockModule();
    uint32 ret = AtModuleClockAllClockCheck(clockModule);
	AtUnused(argv);
	AtUnused(argc);

    if (ret == 0)
        {
        if (AtModuleClockAllClockCheckIsSupported(clockModule))
            AtPrintc(cSevInfo, "All clocks are good\r\n");
        else
            AtPrintc(cSevWarning, "Clock checking has not been supported. "
                                  "The CLI: \"debug device\" may display clock information\r\n");
        }
    else
        {
        AtPrintc(cSevCritical, "Checking all clocks fail with ret = 0x%0X:\r\n", ret);
        AtPrintc(cSevCritical, "SONET/SDH SERDES PLL clocks: %s\r\n", ret & cAtModuleClockAlarmSdhSerdesPllFail ? "FAILED" : "OK");
        }

    return cAtTrue;
    }

eBool CmdModuleClockInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdModuleClockInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleClockShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleClock clockModule = ClockModule();
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)clockModule);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

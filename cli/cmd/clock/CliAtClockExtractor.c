/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : CliAtClockExtractor.c
 *
 * Created Date: Sep 3, 2013
 *
 * Description : Clock extractor CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "CliAtModuleClock.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../physical/CliAtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtClockExtractorSquelchingOptionVal[] =
    {
     cAtClockExtractorSquelchingLos,
     cAtClockExtractorSquelchingLof,
     cAtClockExtractorSquelchingAis,
     cAtClockExtractorSquelchingEthLf,
     cAtClockExtractorSquelchingEthRf,
     cAtClockExtractorSquelchingEthLossync,
     cAtClockExtractorSquelchingEthEer,
     cAtClockExtractorSquelchingEthLinkDown,
     cAtClockExtractorSquelchingNone
    };

static const char *cAtClockExtractorSquelchingOptionStr[] =
    {
     "los",
     "lof",
     "ais",
     "eth-lf",
     "eth-rf",
     "eth-lossync",
     "eth-eer",
     "eth-linkdown",
     "none"
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ClockSquelchingOptionMaskFromString(char *pIntrStr)
    {
    return CliMaskFromString(pIntrStr, cAtClockExtractorSquelchingOptionStr, cAtClockExtractorSquelchingOptionVal, mCount(cAtClockExtractorSquelchingOptionVal));
    }

static const char *ClockSquelchingOptionStrFromMask(uint32 alarmMask)
    {
    return CliAlarmMaskToString(cAtClockExtractorSquelchingOptionStr, cAtClockExtractorSquelchingOptionVal, mCount(cAtClockExtractorSquelchingOptionVal), alarmMask);
    }

static const char *ClockSourceTypeGet(AtClockExtractor extractor)
    {
    if (AtClockExtractorPdhDe1LiuClockIsExtracted(extractor))
        return "liu";
    if (AtClockExtractorPdhDe3LiuClockIsExtracted(extractor))
        return "liu";
    if (AtClockExtractorPdhDe1Get(extractor))
        return "de1";
    if (AtClockExtractorPdhDe3Get(extractor))
        return "de3";
    if (AtClockExtractorSystemClockIsExtracted(extractor))
        return "system";
    if (AtClockExtractorSdhSystemClockIsExtracted(extractor))
        return "sdh_sys";
    if (AtClockExtractorSdhLineGet(extractor))
        return "sdh_line";
    if (AtClockExtractorExternalIdIsValid(extractor, AtClockExtractorExternalClockGet(extractor)))
        return "ext";
    if (AtClockExtractorSerdesGet(extractor))
        return "serdes";

    return "none";
    }

static const char *ClockSourceGet(AtClockExtractor extractor)
    {
    static char buf[16];

    if (AtClockExtractorExternalIdIsValid(extractor, AtClockExtractorExternalClockGet(extractor)))
        {
        AtSprintf(buf, "%d", AtClockExtractorExternalClockGet(extractor) + 1);
        return buf;
        }

    if (AtClockExtractorPdhDe1LiuClockIsExtracted(extractor) || AtClockExtractorPdhDe1Get(extractor))
        return (char *)CliChannelIdStringGet((AtChannel)AtClockExtractorPdhDe1Get(extractor));

    if (AtClockExtractorPdhDe3LiuClockIsExtracted(extractor) || AtClockExtractorPdhDe3Get(extractor))
        return (char *)CliChannelIdStringGet((AtChannel)AtClockExtractorPdhDe3Get(extractor));

    if (AtClockExtractorSystemClockIsExtracted(extractor))
        return sAtNotApplicable;

    if (AtClockExtractorSdhSystemClockIsExtracted(extractor))
        return sAtNotApplicable;

    if (AtClockExtractorSdhLineGet(extractor))
        {
        AtSdhLine sdhLine = AtClockExtractorSdhLineGet(extractor);
        AtSprintf(buf, "%u", AtChannelIdGet((AtChannel)sdhLine) + 1);
        return buf;
        }

    if (AtClockExtractorSerdesGet(extractor))
        {
        AtSerdesController serdes = AtClockExtractorSerdesGet(extractor);
        return AtObjectToString((AtObject)serdes);
        }

    return "none";
    }

static eBool CmdAtClockExtractorNoParamProcess(char argc, char **argv, eAtModuleClockRet (*NoParamFunction)(AtClockExtractor self))
    {
    AtList extractors = AtListCreate(0);
    uint32 i;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    if (AtListLengthGet(extractors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct extractor lists, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Configure them */
    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        eAtRet ret = NoParamFunction(extractor);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure clock extractor %d, ret = %s\r\n",
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)extractors);

    return success;
    }

static eBool PdhDe1ClockExtract(char argc, char **argv, eAtModuleClockRet (*De1ClockExtract)(AtClockExtractor self, AtPdhDe1 de1))
    {
    AtList extractors = AtListCreate(0);
    eBool success = cAtTrue;
    AtChannel *de1s;
    uint32 numDe1s, i;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);

    /* Get DS1/E1 list */
    de1s = CliSharedChannelListGet(&numDe1s);
    numDe1s = De1ListFromString(argv[1], de1s, numDe1s);

    /* Make sure that they are valid */
    if ((AtListLengthGet(extractors) == 0) || (numDe1s == 0))
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct list of extractors and DS1/E1s\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    if (AtListLengthGet(extractors) != numDe1s)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of clock extractors (%d) and clock sources (%d) must be equal\r\n",
                 AtListLengthGet(extractors), numDe1s);
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        eAtRet ret = cAtOk;

        ret = De1ClockExtract(extractor, (AtPdhDe1)de1s[i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot extract clock from %s for extractor %d, ret = %s\r\n",
                     CliChannelIdStringGet(de1s[i]),
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    /* Cleanup */
    AtObjectDelete((AtObject)extractors);

    return success;
    }

static eBool PdhDe3ClockExtract(char argc, char **argv, eAtModuleClockRet (*De3ClockExtract)(AtClockExtractor self, AtPdhDe3 de3))
    {
    AtList extractors = AtListCreate(0);
    eBool success = cAtTrue;
    AtChannel *de3s;
    uint32 numDe3s, i;
    AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);

    /* Get DS3/E3 list */
    de3s = CliSharedChannelListGet(&numDe3s);
    numDe3s = CliDe3ListFromString(argv[1], de3s, numDe3s);

    /* Make sure that they are valid */
    if ((AtListLengthGet(extractors) == 0) || (numDe3s == 0))
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct list of extractors and DS3/E3s\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    if (AtListLengthGet(extractors) != numDe3s)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of clock extractors (%d) and clock sources (%d) must be equal\r\n",
                 AtListLengthGet(extractors), numDe3s);
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        eAtRet ret = cAtOk;

        ret = De3ClockExtract(extractor, (AtPdhDe3)de3s[i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot extract clock from %s for extractor %d, ret = %s\r\n",
                     CliChannelIdStringGet(de3s[i]),
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    /* Cleanup */
    AtObjectDelete((AtObject)extractors);

    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList extractors = AtListCreate(0);
    uint32 i;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    if (AtListLengthGet(extractors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct extractor lists, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        eAtRet ret = AtClockExtractorEnable(extractor, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s clock extractor %d, ret = %s\r\n",
                     enable ? "enable" : "disable",
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)extractors);

    return success;
    }

eBool CmdAtClockExtractorSystemClockExtract(char argc, char **argv)
    {
    return CmdAtClockExtractorNoParamProcess(argc, argv, AtClockExtractorSystemClockExtract);
    }

eBool CmdAtClockExtractorSdhSystemClockExtract(char argc, char **argv)
    {
    return CmdAtClockExtractorNoParamProcess(argc, argv, AtClockExtractorSdhSystemClockExtract);
    }

eBool CmdAtClockExtractorSdhLineClockExtract(char argc, char **argv)
    {
    AtList extractors = AtListCreate(0);
    AtList lines      = AtListCreate(0);
    uint32 i;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    CliSdhLineListFromString(lines, argv[1]);
    if ((AtListLengthGet(extractors) == 0) || (AtListLengthGet(lines) == 0))
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct list of extractors and lines, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        AtObjectDelete((AtObject)lines);
        return cAtFalse;
        }

    if (AtListLengthGet(extractors) != AtListLengthGet(lines))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of clock extractors (%u) and clock sources (%u) must be equal\r\n",
                 AtListLengthGet(extractors),
                 AtListLengthGet(lines));
        AtObjectDelete((AtObject)extractors);
        AtObjectDelete((AtObject)lines);

        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        AtSdhLine sdhLine = NULL;
        eAtRet ret = cAtOk;

        sdhLine = (AtSdhLine)AtListObjectGet(lines, i);
        ret = AtClockExtractorSdhLineClockExtract(extractor, sdhLine);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot extract clock from line %u for extractor %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)sdhLine),
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    /* Cleanup */
    AtObjectDelete((AtObject)extractors);
    AtObjectDelete((AtObject)lines);

    return success;
    }

eBool CmdAtClockExtractorExternalClockExtract(char argc, char **argv)
    {
    AtList extractors = AtListCreate(0);
    uint32 i;
    eBool success = cAtTrue;
    uint32 numExternalClocks;
    uint32 *externalClockIds;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    if (AtListLengthGet(extractors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct list of extractors, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Get external clock IDs */
    externalClockIds = CliSharedIdBufferGet(&numExternalClocks);
    numExternalClocks = CliIdListFromString(argv[1], externalClockIds, numExternalClocks);
    if (numExternalClocks != AtListLengthGet(extractors))
        {
        AtPrintc(cSevCritical,
                 "ERROR: number of extractors (%u) must be equal to number external clocks (%u)\r\n",
                 AtListLengthGet(extractors), numExternalClocks);
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        eAtRet ret = cAtOk;

        ret = AtClockExtractorExternalClockExtract(extractor, (uint8)(externalClockIds[i] - 1));
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot extract clock from ext#%u for extractor %u, ret = %s\r\n",
                     externalClockIds[i],
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    /* Cleanup */
    AtObjectDelete((AtObject)extractors);

    return success;
    }

eBool CmdAtClockExtractorPdhDe1ClockExtract(char argc, char **argv)
    {
    return PdhDe1ClockExtract(argc, argv, AtClockExtractorPdhDe1ClockExtract);
    }

eBool CmdAtClockExtractorPdhDe1LiuClockExtract(char argc, char **argv)
    {
    return PdhDe1ClockExtract(argc, argv, AtClockExtractorPdhDe1LiuClockExtract);
    }

eBool CmdAtClockExtractorEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtClockExtractorDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtClockExtractorShow(char argc, char **argv)
    {
    AtList extractors = AtListCreate(0);
    tTab *tabPtr;
    char buf[16];
    const char *heading[] ={"ID", "enable", "sourceType", "source", "squelching"};
    uint32 i;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    if (AtListLengthGet(extractors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct extractor lists, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(AtListLengthGet(extractors), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        eBool enable = AtClockExtractorIsEnabled(extractor);
        uint32 squelchingOptions;
        eBool squelchingIsSupported;
        uint32 column = 0;

        AtSprintf(buf, "%d", AtClockExtractorIdGet(extractor) + 1);
        StrToCell(tabPtr, i, column++, buf);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);
        StrToCell(tabPtr, i, column++, ClockSourceTypeGet(extractor));
        StrToCell(tabPtr, i, column++, ClockSourceGet(extractor));

        squelchingIsSupported = AtClockExtractorSquelchingIsSupported(extractor);
        squelchingOptions = AtClockExtractorSquelchingOptionGet(extractor);
        StrToCell(tabPtr, i, column++, squelchingIsSupported ? ClockSquelchingOptionStrFromMask(squelchingOptions) : sAtNotSupported);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)extractors);

    return cAtTrue;
    }

eBool CmdAtClockExtractorDebug(char argc, char **argv)
    {
    AtList extractors = AtListCreate(0);
    uint32 i;
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    if (AtListLengthGet(extractors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct extractor lists, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(extractors); i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        AtClockExtractorDebug(extractor);
        }

    AtObjectDelete((AtObject)extractors);

    return cAtTrue;
    }

eBool CmdAtClockExtractorPdhDe3ClockExtract(char argc, char **argv)
    {
    return PdhDe3ClockExtract(argc, argv, AtClockExtractorPdhDe3ClockExtract);
    }

eBool CmdAtClockExtractorPdhDe3LiuClockExtract(char argc, char **argv)
    {
    return PdhDe3ClockExtract(argc, argv, AtClockExtractorPdhDe3LiuClockExtract);
    }

eBool CmdAtClockExtractorSerdesClockExtract(char argc, char** argv)
    {
    AtList extractors = AtListCreate(0);
    AtList controllers;
    eBool success = cAtTrue;
    uint32 i, numExtractors, numSerdes;
    AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);

    /* Get serdes controller list */
    controllers = CliAtSerdesControllersList(argv[1]);

    /* Make sure that they are valid */
    numExtractors = AtListLengthGet(extractors);
    numSerdes = AtListLengthGet(controllers);
    if ((numExtractors == 0) || (numSerdes == 0))
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct list of extractors and Serdes\r\n");
        AtObjectDelete((AtObject)extractors);
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    if (numExtractors != numSerdes)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of clock extractors (%d) and clock sources (%d) must be equal\r\n",
                 numExtractors, numSerdes);
        AtObjectDelete((AtObject)extractors);
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    /* Apply */
    for (i = 0; i < numExtractors; i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        AtSerdesController serdes = (AtSerdesController)AtListObjectGet(controllers, i);
        eAtRet ret = AtClockExtractorSerdesClockExtract(extractor, serdes);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot extract clock from serdes %d for extractor %d, ret = %s\r\n",
                     AtSerdesControllerIdGet(serdes) + 1,
                     AtClockExtractorIdGet(extractor) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    /* Cleanup */
    AtObjectDelete((AtObject)extractors);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtClockExtractorSquelchingOptionSet(char argc, char **argv)
    {
    AtList extractors = AtListCreate(0);
    uint32 extractor_i, options;
    eBool success = cAtTrue;
    
    AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    if (AtListLengthGet(extractors) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct list of extractors, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Get external clock IDs */
    options = ClockSquelchingOptionMaskFromString(argv[1]);

    /* Apply */
    for (extractor_i = 0; extractor_i < AtListLengthGet(extractors); extractor_i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, extractor_i);
        eAtRet ret = cAtOk;

        ret = AtClockExtractorSquelchingOptionSet(extractor, options);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Set option for the clock extractor#%u got error, ret = %s\r\n",
                 AtClockExtractorIdGet(extractor) + 1,
                 AtRet2String(ret));
        success = cAtFalse;
        }

    /* Cleanup */
    AtObjectDelete((AtObject)extractors);

    return success;
    }
        

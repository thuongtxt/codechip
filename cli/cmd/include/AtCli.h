/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtCli.h
 * 
 * Created Date: Oct 16, 2012
 *
 * Description : CLI common header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLICOMMON_H_
#define _ATCLICOMMON_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "atidparse.h"
#include "commacro.h"
#include "showtable.h"

#include "AtIdParser.h"
#include "AtList.h"
#include "AtCommon.h"
#include "AtChannel.h"
#include "AtModuleEth.h"
#include "AtModulePpp.h"
#include "AtEthVlanTag.h"
#include "AtLogger.h"
#include "AtPdhChannel.h"
#include "AtEthPort.h"
#include "../sdh/CliAtModuleSdh.h"

/* To have all CLI prototypes */
#include "../../atsdkcli.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define sAtNotApplicable         "N/A"
#define sAtNotSupported          "N/S"
#define sAtUnknown               "Unknown"
#define sAtNotCare               "xxx"

/* SDH channel types */
#define sAtChannelTypeSdhLine    "line"

#define sAtChannelTypeSdhAug64   "aug64"
#define sAtChannelTypeSdhAug16   "aug16"
#define sAtChannelTypeSdhAug4    "aug4"
#define sAtChannelTypeSdhAug1    "aug1"
#define sAtChannelTypeSdhAug     "aug"

#define sAtChannelTypeSdhAu4_16nc  "au4_16nc"
#define sAtChannelTypeSdhAu4_nc  "au4_nc"
#define sAtChannelTypeSdhAu4_64c "au4_64c"
#define sAtChannelTypeSdhAu4_16c "au4_16c"
#define sAtChannelTypeSdhAu4_4c  "au4_4c"
#define sAtChannelTypeSdhAu4     "au4"
#define sAtChannelTypeSdhAu3     "au3"
#define sAtChannelTypeSdhAu      "au"

#define sAtChannelTypeSdhTug3    "tug3"
#define sAtChannelTypeSdhTug2    "tug2"
#define sAtChannelTypeSdhTug     "tug"

#define sAtChannelTypeSdhVc4_16nc  "vc4_16nc"
#define sAtChannelTypeSdhVc4_nc  "vc4_nc"
#define sAtChannelTypeSdhVc4_64c "vc4_64c"
#define sAtChannelTypeSdhVc4_16c "vc4_16c"
#define sAtChannelTypeSdhVc4_4c  "vc4_4c"
#define sAtChannelTypeSdhVc4     "vc4"
#define sAtChannelTypeSdhVc3     "vc3"
#define sAtChannelTypeSdhVc1x    "vc1x"
#define sAtChannelTypeSdhVc      "vc"

#define sAtChannelTypeSdhTu3     "tu3"
#define sAtChannelTypeSdhTu1x    "tu1x"
#define sAtChannelTypeSdhTu      "tu"

/* PDH channel types */
#define sAtChannelTypePdhNxDs0 "nxds0"
#define sAtChannelTypePdhDe1 "de1"
#define sAtChannelTypePdhDe3 "de3"

/* Ethernet channel type */
#define sAtChannelTypeEthPort "eport"
#define sAtChannelTypeEthFlow "flow"

/* Encapsulation channel type */
#define sAtChannelTypeEncapHdlc "hdlc"
#define sAtChannelTypeEncapAtmTc "atmtc"

/* PPP channel type */
#define sAtChannelTypePppBundle "mlppp"
#define sAtChannelTypePppLink "ppp"

/* VCG type */
#define sAtChannelTypeVcg "vcg"

/* Frame relay type */
#define sAtChannelTypeFrameRelay "fr"
#define sAtChannelTypeMultiLinkFrameRelay "mfr"

/* DCC channels */
#define sAtChannelTypeDcc "dcc"
#define sAtChannelTypeHdlcDcc "hdlc_dcc"

#define sIdTypeSeparateChar "."
#define cIdTypeStringMaxLength 16

/*--------------------------- Macros -----------------------------------------*/
#define mCliEnum2Str(ptypeName, intVal, buffer)                               \
    clish_ptype_select__find_enum_name(clish_shell_find_ptype(context->shell, ptypeName), intVal, buffer, mCount(buffer))

/* This function is to warn setting or getting a ID channel is not OK, it will not return CLI */
#define mWarnNotReturnCliIfNotOk(ret, channel, message)                        \
    AtPrintc(cSevWarning, "WARNING: Channel %s: %s. ret = %s\n",               \
             CliChannelIdStringGet((AtChannel)channel), message, AtRet2String(ret))
#define mCliCheckLinesValid(numberOfLines)                                     \
    if (numberOfLines == 0)                                                    \
          {                                                                    \
          AtPrintc(cSevCritical, "ERROR: Invalid parameter <lines> , expected 1-8\r\n");  \
          return cAtFalse;                                                     \
          }

#define mTimeIntervalInUsGet(prevTime, curTime) \
    (((((curTime).sec - (prevTime).sec) * 1000000) + ((curTime).usec)) - ((prevTime).usec))

/* NOTE: although a macro should be prefixed with 'm', but this should be an
 * exception to avoid conflicting when merging main branches. */
#define CliNumber2String(number, format) AtCliNumber2String(format, number)

#define CliChannelLog(self_, messageNoNewLine)                                 \
    do                                                                         \
        {                                                                      \
        AtChannelLog((AtChannel)self_, cAtLogLevelNormal, AtSourceLocation, "%s: %s\r\n", AtFunction, messageNoNewLine); \
        }while(0)

#define CliDeviceLog(self_, messageNoNewLine)                                 \
    do                                                                         \
        {                                                                      \
        AtDeviceLog((AtDevice)self_, cAtLogLevelNormal, AtSourceLocation, "%s: %s\r\n", AtFunction, messageNoNewLine); \
        }while(0)
/*--------------------------- Typedefs ---------------------------------------*/
/*
 * Counter reading mode
 */
typedef enum eAtHistoryReadingMode
    {
    cAtHistoryReadingModeReadOnly    = 0,
    cAtHistoryReadingModeReadToClear = 1,
    cAtHistoryReadingModeUnknown
    }eAtHistoryReadingMode;

typedef enum eAtDirection
    {
    cAtDirectionTx,
    cAtDirectionRx,
    cAtDirectionAll,
    cAtDirectionInvalid
    }eAtDirection;

typedef enum eAtCounterType
    {
    cAtCounterTypeGood,
    cAtCounterTypeError,
    cAtCounterTypeNeutral,
    cAtCounterTypeNA,
    cAtCounterTypeNotSupport
    }eAtCounterType;

typedef struct tAtCapturedEvent
    {
    AtChannel channel;
    uint32 changedAlarms;
    uint32 currentStatus;
    }tAtCapturedEvent;

typedef struct tAtCliUpsrInterruptStatistic
    {
    uint32 setNums;
    uint32 clearNums;
    uint32 unchangedNums;
    }tAtCliUpsrInterruptStatistic;

typedef AtChannel (*ChannelFromStringFunction)(char* argv);
typedef AtChannel (*ChannelListFromStringFunction)(char* argv);

/*--------------------------- Forward declarations ---------------------------*/
extern void AtChannelLog(AtChannel self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...);
extern void AtDeviceLog(AtDevice self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...);

/*--------------------------- Entries ----------------------------------------*/
AtDevice CliDevice(void);
AtDevice CliRealDevice(void);/*real device regardless to current subdevice */
uint8 DeviceIdFromCmdParamId(uint8 cmdParamId);
eBool SubDeviceIdFromCmdParamIdIsValid(uint8 cmdParamId);
AtModuleEth CliAtEthModule(void);
AtModuleEncap CliAtEncapModule(void);
AtModuleConcate CliAtConcateModule(void);

uint32 *CliSharedIdBufferGet(uint32 *bufferSize);
uint32 CliIdListFromString(char *idString, uint32 *idList, uint32 bufferSize);
uint32 CliChannelListFromString(char *idList, AtChannel *channelList, uint32 bufferSize);
AtChannel *CliSharedChannelListGet(uint32 *bufferSize);
char *CliSharedCharBufferGet(uint32 *bufferSize);
char *CliChannelIdStringGet(AtChannel channel);
uint32 CliNxDs0ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);

const char *CliExpectedVlanFormat(void);
char *CliVlans2Str(const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
tAtVlan *CliVlanFromString(char *vlanString, tAtVlan *vlan);
char *CliVlan2Str(const tAtVlan *vlanTag);
const char *CliFullVlanExpectedFormat(void);
eBool CliEthFlowVlanGet(char *pStrCvlan, char *pStrSvlan, tAtEthVlanDesc *desc);
uint32 CliId2DriverId(uint32 cliId);
void CliIdBuild(AtIdParser parser, uint32 *ids, uint8 numLevels);
AtModuleSdh CliModuleSdh(void);
AtModulePdh CliModulePdh(void);

char** StrTokenGet(const char *pStr, const char *pDelim, uint32 *tokenCount);
char* IdTypeAndIdListGet(const char *pStrIdList, char **pIdType, char **pIdList);
AtHdlcLink HdlcLink(uint32 linkId);
AtMpBundle CliMpBundleGet(uint32 bundleId);
uint32 De1ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);
uint32 CliDe3ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);
uint32 CliDe2ListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);
AtEthPort *CliEthPortListGet(char *pStrPortIdList, uint32 **idBuf, uint8 *numPorts);
AtEthFlow *CliEthFlowListGet(char *pStrFlowIdList, uint32 *numFlows);
uint32 CliBundleIdListFromString(char *IdString, uint32 *Idlist, uint32 bufferSize);
eBool AtIsHexChar(char c);

/* BER */
eAtBerRate CliBerRateFromString(const char *berRateString);
char *CliBerRateString(eAtBerRate berRate);
const char *CliValidBerRates(void);

/* History reading mode */
eAtHistoryReadingMode CliHistoryReadingModeGet(char *readingModeString);
const char *CliValidHistoryReadingModes(void);
eBool CliHistoryReadingShouldSilent(eAtHistoryReadingMode readingMode, char *verboseModeStr);
eBool CliIsSilentIndication(char *verboseModeStr);

/* LED state */
eAtLedState CliLedStateFromStr(const char *ledStateStr);
const char *CliLedStateStr(eAtLedState ledState);

/* Direction */
eAtDirection CliDirectionFromStr(const char *directionStr);
const char *CliValidDirections(void);

/* Timing mode */
eAtTimingMode CliTimingModeFromString(const char *timingModeString);
char *CliTimingModeString(eAtTimingMode timingMode);
const char *CliValidTimingModes(void);
const char *CliClockStateString(eAtClockState clockState);

/* Loopback mode */
eAtLoopbackMode CliLoopbackModeFromString(const char *loopbackModeString);
const char *CliLoopbackModeString(eAtLoopbackMode loopbackMode);
const char *CliValidLoopbackModes(void);

/* Get color of counter type */
void CliCounterPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           uint32 counterValue,
                           eAtCounterType counterType,
                           eBool isValidCounter);

void CliCounter64BitPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           uint64 counterValue,
                           eAtCounterType counterType,
                           eBool isValidCounter);

void CliChannelCounterPrintToCell(AtChannel self,
                                 uint32 (*CountersGet)(AtChannel, uint16),
                                 uint32 counterType,
                                 eAtCounterType counterShowType,
                                 tTab *tabPtr,
                                 uint32 row,
                                 uint32 column);

void CliChannelAlarmPrintToCell(AtChannel self,
                                uint32 alarm,
                                uint32 alarmType,
                                tTab *table,
                                uint32 row,
                                uint32 column);

AtList CliChannelsFromString(AtList channels, char *idList);

/* Convert between enum and string */
eBool CliBoolFromString(const char *pString);
const char *CliBoolToString(eBool value);
uint32 CliStringToEnum(const char *aString, const char *enumStrings[], const uint32 *enumValues, uint32 size, eBool *success);
const char *CliEnumToString(uint32 value, const char *enumStrings[], const uint32 *enumValues, uint32 size, eBool *success);
void CliExpectedValuesPrint(eAtSevLevel color, const char *enumStrings[], uint32 size);

char *AtCliNumber2String(const char *format, ...);
char *CliTimeInUsToString(uint32 timeInUs, char *buffer, uint32 bufferSize);
const char *AtSevLevel2Str(eAtSevLevel level);

/* Parse mask */
uint32 CliMaskFromString(char *maskString, const char **predefinedStrings, const uint32 *predefinedValues, uint32 numValues);
const char* CliAlarmMaskToString(const char **predefinedStrings, const uint32 *predefinedValues, uint32 numValues, uint32 alarmMask);

void AtCliRegisterPrettyPrint(uint32 value);
AtObject *CliSharedObjectListGet(uint32 *bufferSize);
char *CliSharedCharBufferGet(uint32 *bufferSize);

AtChannel AtChannelBoundChannelGet(AtChannel channel);

eBool CliTimeLoggingEnable(eBool enable);
eBool CliTimeLoggingIsEnabled(void);

void CliCapturedAlarmToCell(tTab* pTab, uint32 row, uint32 column, uint32 changedAlarms, uint32 currentStatus, uint32 alarmToDisplay);
void CliCapturedAlarmToCellAndDoStatistics(tTab* pTab, uint32 row, uint32 column, uint32 changedAlarms, uint32 currentStatus, uint32 alarmToDisplay,
                            eBool *isSet, eBool *isClear, eBool *isNoChanged);

/* Bit ordering */
eAtBitOrder CliBitOrderFromString(const char *bitOrderString);
const char *CliBitOrderModeString(eAtBitOrder bitOrder);
const char *CliValidBitOrderModes(void);

/* Helper for interrupt capturing */
AtList CliCapturedEventFilterByChannels(AtChannel* channelList, uint32 numChannels, AtList capturedEvents, eAtHistoryReadingMode readMode);
void CliPutEventsToTable(tTab *tabPtr, AtList events, const uint32 *alarms, uint32 numAlarm, uint32 clockStateMask, eAtHistoryReadingMode readMode);
void CliPutEventsToTableAndDoStatistics(tTab *tabPtr,
                         AtList events,
                         const uint32 *alarms,
                         uint32 numAlarm,
                         uint32 clockStateMask,
                         eAtHistoryReadingMode readMode,
                         tAtCliUpsrInterruptStatistic *statistic);
void CliPushLastEventToListEvent(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, AtList listCapturedEvents, uint32 clockStateMask);

/* Interrupt captured to log file. */
eBool CliInterruptLogEnable(eBool enable);
eBool CliInterruptLogIsEnabled(void);

void CliCounterTypePrintToCell(tTab *tabPtr, uint32 row, uint32 column, uint32 counterValue, eAtCounterType counterType, const char *counterName);

/* For PRBS mode */
uint32 CliPrbsModeStringToEnum(const char *modeString);
const char *CliPrbsModeToString(eAtPrbsMode mode);
char *CliPrbsAlarmToString(uint32 alarm);

char *CliDwordArrayToString(const uint32 *data, uint32 numDwords);
uint32 CliEthPortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize);
AtList AtCliIdStrListSeparateByComma(char *pStrIdList);
uint32 CliListToArray(AtList objects, AtObject *objectsBuffer, uint32 bufferSize);

/* IPv4/IPv6*/
eBool CliIpv4StringToArray(const char *pIPv4AddrStr, uint8 *pIpv4Addr);
char *CliIpV4ArrayToString(const uint8 *pIPv4Addr);
eBool CliIpV6StringToArray(const char *pIPv6AddrStr, uint8 *pIpv6Addr);
char *CliIpV6ArrayToString(const uint8 *pIPv6Addr);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLICOMMON_H_ */


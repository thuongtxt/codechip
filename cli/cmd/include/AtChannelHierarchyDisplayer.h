/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtChannelHierarchyDisplayer.h
 * 
 * Created Date: Jun 4, 2013
 *
 * Description : Hierarchy displayer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELHIERARCHYDISPLAYER_H_
#define _ATCHANNELHIERARCHYDISPLAYER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtObject.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtChannelHierarchyBranchStypeNormal "_"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtChannelHierarchyMode
    {
    cAtChannelHierarchyModeAlarm,
    cAtChannelHierarchyModeHistoryRO,
    cAtChannelHierarchyModeHistoryR2C
    }eAtChannelHierarchyMode;

typedef struct tAtChannelHierarchyDisplayer * AtChannelHierarchyDisplayer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, const char* prefix);

/* Concrete displayer */
AtChannelHierarchyDisplayer AtChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtSdhChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel);
AtChannelHierarchyDisplayer AtPdhDe1HierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel);
AtChannelHierarchyDisplayer AtPdhChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel);
AtChannelHierarchyDisplayer AtMpBundleHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtEthFlowHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtEthVlanDescHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtHdlcLinkHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtPwHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtSonetChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel);
AtChannelHierarchyDisplayer AtHdlcChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtSdhDccHierarchyDisplayerGet(eAtChannelHierarchyMode mode);
AtChannelHierarchyDisplayer AtHdlcChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode);

/* Helper */
eBool ChannelHierarchyRecursiveShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength, eBool isFirstSub, eBool isLastSub);
AtChannelHierarchyDisplayer AtChannelHierarchyDisplayerModeSet(AtChannelHierarchyDisplayer self, eAtChannelHierarchyMode mode);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELHIERARCHYDISPLAYER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : CliFr.h
 * 
 * Created Date: Jun 28, 2016
 *
 * Description : FR/MLFR header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_AT_FR_H_
#define _CLI_AT_FR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "AtHdlcChannel.h"
#include "AtEncapChannel.h"
#include "AtHdlcLink.h"
#include "AtFrLink.h"
#include "AtFrVirtualCircuit.h"
#include "AtMfrBundle.h"
#include "AtEncapBundle.h"
#include "../encap/CliAtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cDlciMaxId      (4095)
#define cTwoLevelsIdVcMinFormatStr "1.0"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef AtEthFlow * (*EthFlowsListGetFunc)(char *pStrVirtualCircuitsIdList, uint32 *numFlows);
typedef eAtModuleEthRet (*EthVlanManipulateFunc)(AtEthFlow self, const tAtEthVlanDesc * desc);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 CliFrLinkIdListFromString(char *idList, AtFrLink *links, uint32 bufferSize);
uint32 CliMfrBundleIdListFromString(char *idList, AtMfrBundle *bundles, uint32 bufferSize);
eBool CliEthOamVlanModify(char **argv, EthFlowsListGetFunc listIdGetFunc, EthVlanManipulateFunc vlanManipulateFunc);
char *CliFrVirtualCircuitIdStringGet(AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _CLI_AT_FR_H_ */


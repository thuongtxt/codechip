/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliAtFrLink.c
 *
 * Created Date: Apr 27, 2016
 *
 * Description : Frame Relay Link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtHdlcChannel.h"
#include "showtable.h"
#include "../eth/CliAtModuleEth.h"
#include "../encap/CliAtModuleEncap.h"
#include "CliAtFr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char * cAtEncapTypeStr[] = {"snap",
                                         "nlpid",
                                         "cisco"};

static const eAtFrLinkEncapType cAtEncapTypeVal[] = {cAtFrLinkEncapTypeIetfWithSNAP,
                                                     cAtFrLinkEncapTypeIetfWithoutSNAP,
                                                     cAtFrLinkEncapTypeCisco};

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *TwoLevelsIdFrVcMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u", AtModuleEncapMaxChannelsGet(CliAtEncapModule()), cDlciMaxId);

    return maxFormat;
    }

static AtFrLink FrLinkFromIdString(char* idString)
    {
    uint32 frLinkId = AtStrToDw(idString);
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(CliAtEncapModule(), (uint16)(CliId2DriverId(frLinkId)));
    if (hdlcChannel == NULL)
        {
        AtPrintc(cSevCritical, "Channel = %u, maybe not created\r\n", frLinkId);
        return NULL;
        }

    if (AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmFr)
        {
        AtPrintc(cSevCritical,"Channel %u invalid encapsulation type\r\n", frLinkId);
        return NULL;
        }

    return (AtFrLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;
    }

static AtEthFlow *CliFrVirtualCircuitOamFlowListGet(char *pStrVirtualCircuitsIdList, uint32 *numFlows)
    {
    AtFrVirtualCircuit *vcsBuffer;
    AtEthFlow *allFlows;
    uint32 numVcs;
    uint32 vcIdx;
    uint32 numFlowIds;
    uint32 numValidFlows = 0;

    /* Get the shared ID buffer */
    vcsBuffer = (AtFrVirtualCircuit *)((void**)CliSharedChannelListGet(&numVcs));
    numVcs = CliAtModuleEncapFrVirtualCircuitFromNonPrefixString(pStrVirtualCircuitsIdList, (AtChannel *) vcsBuffer, numVcs);
    if (numVcs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: FR Virtual Circuit %s does not exist or invalid Id, expect format <id>.<dlci>\r\n", pStrVirtualCircuitsIdList);
        return NULL;
        }

    allFlows = (AtEthFlow *)((void**)CliSharedObjectListGet(&numFlowIds));
    for (vcIdx = 0; vcIdx < numVcs; vcIdx++)
        {
        AtEthFlow flow = AtFrVirtualCircuitOamFlowGet(vcsBuffer[vcIdx]);
        if (flow != NULL)
            {
            allFlows[numValidFlows]= flow;
            numValidFlows++;

            /* If store full share buffer */
            if (numValidFlows == numFlowIds)
                break;
            }
        else
            AtPrintc(cSevCritical, "ERROR: Flow does not exist on channel %s\r\n", CliFrVirtualCircuitIdStringGet((AtChannel) vcsBuffer[vcIdx]));
        }

    *numFlows = numValidFlows;
    return allFlows;
    }

static eBool Q922AddressFieldSet(char argc, char **argv, eAtModuleFrRet (*BitFieldSetFunc)(AtFrLink self, uint8 values))
    {
    AtFrLink *links;
    uint32 numLinks, link_i;
    eAtModuleFrRet ret;
    uint8 values;
    eBool success = cAtTrue;

    AtUnused(argc);
    links = (AtFrLink *) ((void**) CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[0], links, numLinks);
    if (numLinks == 0)
       {
       AtPrintc(cSevCritical, "ERROR: Link %s does not exist or invalid Id, expected 1,2...\r\n", argv[0]);
       return cAtFalse;
       }

    values = (uint8)AtStrToDw(argv[1]);
    for (link_i = 0; link_i < numLinks; link_i++)
        {
        ret = BitFieldSetFunc(links[link_i], values);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Set value for Q922 address field returns fail with ret=%s\r\n", AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

char *CliFrVirtualCircuitIdStringGet(AtChannel channel)
    {
    static char idString[128];

    if (channel == NULL)
        {
        AtSprintf(idString, "none");
        return idString;
        }

    AtSprintf(idString, "%s.%u", AtChannelTypeString(channel), AtChannelIdGet(channel));

    return idString;
    }

uint32 CliAtModuleEncapFrVirtualCircuitFromNonPrefixString(char *pStrIdList, AtChannel *channelList, uint32 bufferSize)
    {
    uint32 i, numChannels = 0;
    AtIdParser idParser;

    /* Create ID parser */
    idParser = AtIdParserNew(pStrIdList, cTwoLevelsIdVcMinFormatStr, TwoLevelsIdFrVcMaxFormat());

    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        AtHdlcChannel hdlcChannel;
        AtFrLink link;
        AtFrVirtualCircuit vc;
        uint32 firstPosId = AtIdParserNextNumber(idParser);
        uint32 secondPosId = AtIdParserNextNumber(idParser);

        hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(CliAtEncapModule(), (uint16)(CliId2DriverId(firstPosId)));
        if (hdlcChannel == NULL)
            continue;

        if (AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmFr)
            {
            AtPrintc(cSevWarning, "Channel %u invalid encapsulation type\r\n", firstPosId);
            continue;
            }

        link = (AtFrLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;
        if (link == NULL)
            {
            AtPrintc(cSevWarning, "Link is not exits on Channel %u\r\n", firstPosId);
            continue;
            }

        vc = AtFrLinkVirtualCircuitGet(link, secondPosId);
        if (vc == NULL)
            {
            AtPrintc(cSevWarning,
                     "VirtualCircuit with dcli = %u on channel %s is not exits!\r\n",
                     secondPosId,
                     AtChannelTypeString((AtChannel) link));

            continue;
            }

        channelList[numChannels++] = (AtChannel) vc;
        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }


uint32 CliFrLinkIdListFromString(char *idList, AtFrLink *links, uint32 bufferSize)
    {
    uint32 idBufferSize;
    uint32 link_i;
    uint32 numValidLink = 0;
    uint32 *idBuffer = CliSharedIdBufferGet(&idBufferSize);
    uint32 numberLinks = CliIdListFromString(idList, idBuffer, idBufferSize);

    for (link_i = 0; link_i < numberLinks; link_i++)
        {
        AtHdlcChannel  hdlcChannel;
        AtFrLink link;

        hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(CliAtEncapModule(), (uint16)(CliId2DriverId(idBuffer[link_i])));
        if (hdlcChannel == NULL)
            {
            AtPrintc(cSevWarning, "Channel = %u, maybe not created\r\n", idBuffer[link_i]);
            continue;
            }

        if (AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmFr)
            {
            AtPrintc(cSevWarning, "Channel %u invalid encapsulation type\r\n", idBuffer[link_i]);
            continue;
            }

        link = (AtFrLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;
        if (link == NULL)
            {
            AtPrintc(cSevWarning,"Link is not exits on Channel %u\r\n", idBuffer[link_i]);
            continue;
            }

        if (numValidLink >= bufferSize)
            break;

        links[numValidLink++] = link;
        }

    return numValidLink;
    }

eBool CmdAtFrLinkVirtualCircuitCreate(char argc, char **argv)
    {
    uint32 *idBuffer, bufferSize;
    uint32 dlci_i, numDlci;
    eBool cliSuccess = cAtTrue;
    AtFrLink link;
    AtUnused(argc);

    link = FrLinkFromIdString(argv[0]);
    if (link == NULL)
        {
        AtPrintc(cSevCritical,"link is not exits\r\n");
        return cAtFalse;
        }

	idBuffer = CliSharedIdBufferGet(&bufferSize);
    numDlci = CliIdListFromString(argv[1], idBuffer, bufferSize);
    if (numDlci == 0)
        {
        AtPrintc(cSevCritical, "ERROR: DLCI ID list may be invalid, expected 1,2...\r\n");
        return cAtFalse;
        }

    for (dlci_i = 0; dlci_i < numDlci; dlci_i++)
        {
        AtFrVirtualCircuit virtualCircuit = AtFrLinkVirtualCircuitCreate(link, idBuffer[dlci_i]);
        if (virtualCircuit == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Virtual circuit %u exist or invalid id\r\n", idBuffer[dlci_i]);
            cliSuccess = cAtFalse;
            }
        }

    return cliSuccess;
    }

eBool CmdAtFrLinkVirtualCircuitDelete(char argc, char **argv)
    {
    uint32 *idBuffer, bufferSize;
    uint32 numDlci, dlci_i;
    eBool cliSuccess = cAtTrue;
    eAtModuleFrRet ret;
    AtFrLink link;
    AtUnused(argc);

    link = FrLinkFromIdString(argv[0]);
    if (link == NULL)
        {
        AtPrintc(cSevCritical,"link is not exits\r\n");
        return cAtFalse;
        }

    /* Get the shared ID buffer */        
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numDlci = CliIdListFromString(argv[1], idBuffer, bufferSize);
    if (numDlci == 0)
        {
        AtPrintc(cSevCritical, "ERROR: DLCI ID list may be invalid, expected 1,2...\r\n");
        return cAtFalse;
        }

    for (dlci_i = 0; dlci_i < numDlci; dlci_i++)
        {
        ret = AtFrLinkVirtualCircuitDelete(link, idBuffer[dlci_i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Delete Virtual-Circuit (DLCI = %d) of %s, ret = %s fail\r\n",
                     idBuffer[dlci_i],
                     CliChannelIdStringGet((AtChannel) link),
                     AtRet2String(ret));

            cliSuccess = cAtFalse;
            }
        }

    return cliSuccess;
    }

eBool CmdAtVirtualCircuitFlowBind(char argc, char **argv)
    {
    uint32 numVcs, vc_i;
    AtChannel *virtualCircuitList;
    eBool cliSuccess = cAtTrue;
    AtList flowsList;
    uint32 numFlows;
    AtUnused(argc);

    virtualCircuitList = (AtChannel *) ((void**) CliSharedChannelListGet(&numVcs));
    numVcs = CliAtModuleEncapFrVirtualCircuitFromNonPrefixString(argv[0], virtualCircuitList, numVcs);
    if (numVcs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: FR Virtual Circuit %s does not exist or invalid id, expect format <id>.<dlci>\r\n", argv[0]);
        return cAtFalse;
        }

    /* Get shared channel buffer and parse ID of binded channel */
    if (!AtStrcmp(argv[1], "none"))
        {
        numFlows = numVcs;
        flowsList = NULL;
        }
    else
        flowsList = CliEthFlowListFromString(argv[1], &numFlows);

    if (numVcs != numFlows)
        {
        AtPrintc(cSevCritical, "ERROR: The number of Virtual Circuits and Flows Binding are different\r\n");
        AtObjectDelete((AtObject)flowsList);
        return cAtFalse;
        }

    for (vc_i = 0; vc_i < numVcs; vc_i++)
        {
        AtEthFlow flow = (flowsList == NULL) ? NULL : (AtEthFlow)AtListObjectGet(flowsList, vc_i);
        eAtModuleFrRet ret = AtFrVirtualCircuitFlowBind((AtFrVirtualCircuit) virtualCircuitList[vc_i], flow);
        if (ret != cAtOk)
            {
            cliSuccess = cAtFalse;
            if (flow)
                {
                AtPrintc(cSevWarning, "Can't bind Virtual Circuit %s to physical %s, ret = %s\r\n",
                         CliFrVirtualCircuitIdStringGet((AtChannel)virtualCircuitList[vc_i]),
                         CliChannelIdStringGet((AtChannel) flow),
                         AtRet2String(ret));
                }
            else
                {
                AtPrintc(cSevCritical, "Can't unbind Virtual Circuit %s, ret = %s\r\n",
                         CliFrVirtualCircuitIdStringGet((AtChannel)virtualCircuitList[vc_i]),
                         AtRet2String(ret));
                }
            }
        }
    AtObjectDelete((AtObject)flowsList);

    return cliSuccess;
    }

eBool CmdAtFrLinkShow(char argc, char **argv)
    {
    uint32 numLinks, link_i;
    AtFrLink *links;
    tTab *tabPtr = NULL;
    eAtFrLinkEncapType encapType;
    const char *heading[] = {"ID", "EncapType", "C/R", "D/C", "FECN", "BECN", "DE", "DL-CORE"};

    AtUnused(argc);

    links = (AtFrLink *) ((void**) CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[0], links, numLinks);
    if (numLinks == 0)
       {
       AtPrintc(cSevCritical, "ERROR: Link(s) %s does not exist, invalid encapsulation type or invalid id, expected 1,2...\r\n", argv[0]);
       return cAtFalse;
       }

    /* Create table with titles */
    tabPtr = TableAlloc(numLinks, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        uint32 column = 0;
        const char * stringValue;
        StrToCell(tabPtr, link_i, column++, (char *) CliChannelIdStringGet((AtChannel)links[link_i]));
        encapType = AtFrLinkEncapTypeGet(links[link_i]);
        stringValue = CliEnumToString(encapType,
                                      cAtEncapTypeStr,
                                      cAtEncapTypeVal,
                                      mCount(cAtEncapTypeVal),
                                      NULL);
        ColorStrToCell(tabPtr, link_i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        StrToCell(tabPtr, link_i, column++, CliNumber2String(AtFrLinkTxQ922AddressCRGet(links[link_i]), "%u"));
        StrToCell(tabPtr, link_i, column++, CliNumber2String(AtFrLinkTxQ922AddressDCGet(links[link_i]), "%u"));
        StrToCell(tabPtr, link_i, column++, CliNumber2String(AtFrLinkTxQ922AddressFECNGet(links[link_i]), "%u"));
        StrToCell(tabPtr, link_i, column++, CliNumber2String(AtFrLinkTxQ922AddressBECNGet(links[link_i]), "%u"));
        StrToCell(tabPtr, link_i, column++, CliNumber2String(AtFrLinkTxQ922AddressDEGet(links[link_i]), "%u"));
        StrToCell(tabPtr, link_i, column++, CliNumber2String(AtFrLinkTxQ922AddressDlCoreGet(links[link_i]), "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtFrLinkShowVirtualCircuit(char argc, char **argv)
    {
    uint32 numLinks, link_i;
    AtFrLink *links;
    tTab *tabPtr;
    const char *heading[] = { "ID", "DLCI", "BoundFlow"};
    uint32 column = 0;
    uint32 row = 0;

    AtUnused(argc);

    links = (AtFrLink *)((void**)CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[0], links, numLinks);
    if (numLinks == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Link %s does not exist or invalid Id, expected 1,2...\r\n", argv[0]);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(0, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        AtIterator iterator;
        AtFrVirtualCircuit dlciCircuit;

        if (AtFrLinkNumberVirtualCircuitGet(links[link_i]) == 0)
            {
            column = 0;
            TableAddRow(tabPtr, 1);
            StrToCell(tabPtr, row, column++, (char *) CliChannelIdStringGet((AtChannel) links[link_i]));
            StrToCell(tabPtr, row, column++, "none");
            StrToCell(tabPtr, row++, column++, "none");
            }
        else
            {
            iterator = AtFrLinkVirtualCircuitIteratorGet(links[link_i]);
            while ((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(iterator)))
                {
                AtEthFlow flow = AtFrVirtualCircuitBoundFlowGet(dlciCircuit);
                column = 0;
                TableAddRow(tabPtr, 1);
                StrToCell(tabPtr, row, column++, CliChannelIdStringGet((AtChannel)links[link_i]));
                StrToCell(tabPtr, row, column++, CliNumber2String(AtChannelIdGet((AtChannel)dlciCircuit), "%u"));
                StrToCell(tabPtr, row++, column++, CliChannelIdStringGet((AtChannel)flow));
                }
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtFrLinkCounters(char argc, char **argv)
    {
    uint32 numLinks, link_i;
    AtFrLink *links;
    eAtHistoryReadingMode readingMode;
    tTab *tabPtr;
    uint32 (*CountersGet)(AtChannel, uint16) = NULL;
    const char *heading[] = {"ID", "TxPkts", "TxBytes", "RxPkts", "RxBytes"};
    AtUnused(argc);

    links = (AtFrLink *) ((void**) CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[0], links, numLinks);
    if (numLinks == 0)
       {
       AtPrintc(cSevCritical, "ERROR: Link %s does not exist or invalid id, expected 1,2...\r\n", argv[0]);
       return cAtFalse;
       }

    /* Create table with titles */
    tabPtr = TableAlloc(numLinks, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeReadOnly)
        CountersGet = AtChannelCounterGet;
    else
        CountersGet = AtChannelCounterClear;

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        uint32 column = 0;
        StrToCell(tabPtr,  link_i, column++, (char *) CliChannelIdStringGet((AtChannel) links[link_i]));

        CliChannelCounterPrintToCell((AtChannel)links[link_i], CountersGet, cAtFrVirtualCircuitCounterTypeTxGoodPkts, cAtCounterTypeGood, tabPtr, link_i, column++);
        CliChannelCounterPrintToCell((AtChannel)links[link_i], CountersGet, cAtFrVirtualCircuitCounterTypeTxGoodBytes, cAtCounterTypeGood, tabPtr, link_i, column++);

        CliChannelCounterPrintToCell((AtChannel)links[link_i], CountersGet, cAtFrVirtualCircuitCounterTypeRxGoodPkts, cAtCounterTypeGood, tabPtr, link_i, column++);
        CliChannelCounterPrintToCell((AtChannel)links[link_i], CountersGet, cAtFrVirtualCircuitCounterTypeRxGoodBytes, cAtCounterTypeGood, tabPtr, link_i, column++);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtVirtualCircuitCounters(char argc, char **argv)
    {
    uint32 numVcs, vc_i, bufferSize;
    AtChannel *virtualCircuitList;
    eAtHistoryReadingMode readingMode;
    tTab *tabPtr;
    uint32 (*CountersGet)(AtChannel, uint16) = NULL;
    const char *heading[] = {"ID", "TxGoodPkts", "TxBytePkts", "RxGoodPkts", "RxBytePkts"};

    AtUnused(argc);

    virtualCircuitList = (AtChannel *) ((void**) CliSharedChannelListGet(&bufferSize));
    numVcs = CliAtModuleEncapFrVirtualCircuitFromNonPrefixString(argv[0], virtualCircuitList, bufferSize);
    if (numVcs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: FR Virtual Circuit %s does not exist or invalid Id, expect format <id>.<dlci>\r\n", argv[0]);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numVcs, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeReadOnly)
        CountersGet = AtChannelCounterGet;
    else
        CountersGet = AtChannelCounterClear;

    for (vc_i = 0; vc_i < numVcs; vc_i++)
        {
        uint32 column = 0;
        StrToCell(tabPtr,  vc_i, column++, (char *) CliChannelIdStringGet((AtChannel) virtualCircuitList[vc_i]));

        CliChannelCounterPrintToCell((AtChannel)virtualCircuitList[vc_i], CountersGet, cAtFrVirtualCircuitCounterTypeTxGoodPkts, cAtCounterTypeGood, tabPtr, vc_i, column++);
        CliChannelCounterPrintToCell((AtChannel)virtualCircuitList[vc_i], CountersGet, cAtFrVirtualCircuitCounterTypeTxGoodBytes, cAtCounterTypeGood, tabPtr, vc_i, column++);

        CliChannelCounterPrintToCell((AtChannel)virtualCircuitList[vc_i], CountersGet, cAtFrVirtualCircuitCounterTypeRxGoodPkts, cAtCounterTypeGood, tabPtr, vc_i, column++);
        CliChannelCounterPrintToCell((AtChannel)virtualCircuitList[vc_i], CountersGet, cAtFrVirtualCircuitCounterTypeRxGoodBytes, cAtCounterTypeGood, tabPtr, vc_i, column++);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtFrVirtualCircuitOamEgressVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliFrVirtualCircuitOamFlowListGet, AtEthFlowEgressVlanAdd);
    }

eBool CmdAtFrVirtualCircuitOamEgressVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliFrVirtualCircuitOamFlowListGet, AtEthFlowEgressVlanRemove);
    }

eBool CmdAtFrVirtualCircuitOamIngressVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliFrVirtualCircuitOamFlowListGet, AtEthFlowIngressVlanAdd);
    }

eBool CmdAtFrVirtualCircuitOamIngressVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliFrVirtualCircuitOamFlowListGet, AtEthFlowIngressVlanAdd);
    }

eBool CmdAtFrVirtualCircuitOamShow(char argc, char **argv)
    {
    AtEthFlow *flows;
    uint32 numFlows;
    AtUnused(argc);

    /* Get all flows */
    flows = CliFrVirtualCircuitOamFlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevCritical, "No flow\r\n");
        return cAtTrue;
        }

    return CliEthFlowsTableShow(flows, numFlows);
    }

eBool CmdAtFrLinkEncapTypeSet(char argc, char **argv)
    {
    AtFrLink *links;
    uint32 numLinks, link_i;
    eAtFrLinkEncapType encapType;
    eAtModuleFrRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    links = (AtFrLink *) ((void**) CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[0], links, numLinks);
    if (numLinks == 0)
       {
       AtPrintc(cSevCritical, "ERROR: Link %s does not exist or invalid Id, expected 1,2...\r\n", argv[0]);
       return cAtFalse;
       }

    encapType = CliStringToEnum(argv[1], cAtEncapTypeStr, cAtEncapTypeVal, mCount(cAtEncapTypeVal), &success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid encap Type\r\n");
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        ret = AtFrLinkEncapTypeSet(links[link_i], encapType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Encap type set is return failed with ret=%s\r\n", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtFrLinkQ922AddressCRSet(char argc, char **argv)
    {
    if (AtStrToDw(argv[1]) > 1)
        return cAtFalse;

    return Q922AddressFieldSet(argc, argv, AtFrLinkTxQ922AddressCRSet);
    }

eBool CmdAtFrLinkQ922AddressDCSet(char argc, char **argv)
    {
    if (AtStrToDw(argv[1]) > 1)
        return cAtFalse;

    return Q922AddressFieldSet(argc, argv, AtFrLinkTxQ922AddressDCSet);
    }

eBool CmdAtFrLinkQ922AddressFECNSet(char argc, char **argv)
    {
    return Q922AddressFieldSet(argc, argv, AtFrLinkTxQ922AddressFECNSet);
    }

eBool CmdAtFrLinkQ922AddressBECNSet(char argc, char **argv)
    {
    return Q922AddressFieldSet(argc, argv, AtFrLinkTxQ922AddressBECNSet);
    }

eBool CmdAtFrLinkQ922AddressDESet(char argc, char **argv)
    {
    return Q922AddressFieldSet(argc, argv, AtFrLinkTxQ922AddressDESet);
    }

eBool CmdAtFrLinkQ922AddressDLCoreSet(char argc, char **argv)
    {
    return Q922AddressFieldSet(argc, argv, AtFrLinkTxQ922AddressDlCoreSet);
    }

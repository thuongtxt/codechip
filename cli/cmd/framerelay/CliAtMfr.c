/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : CliAtMFR.c
 *
 * Created Date: June 27, 2016
 *
 * Description : Frame Relay Bundle CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "showtable.h"
#include "AtModuleEth.h"
#include "AtModuleEncap.h"
#include "../eth/CliAtModuleEth.h"
#include "CliAtFr.h"
#include "AtMfrBundle.h"

/*--------------------------- Define -----------------------------------------*/
#define mBundleGetErrorPrint()\
AtPrintc(cSevCritical, "ERROR: Bundle(s) %s does not exist, invalid encasulation type or invalid id expected 1-%d\r\n", argv[0],\
AtModuleEncapMaxBundlesGet(CliAtEncapModule()))\

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char * cCmdFragmentSizeStr[] =
    {
    "none", "64bytes", "128bytes", "256bytes", "512bytes"
    };

static const uint32 cCmdFragmentSizeVal[] =
    {
    cAtNoFragment,
    cAtFragmentSize64,
    cAtFragmentSize128,
    cAtFragmentSize256,
    cAtFragmentSize512
    };

static const char * cAtFragmentFormatStr[] = {"end_to_end",
                                              "uni_nni"};

static const eAtFrLinkEncapType cAtFragmentFormatVal[] = {cAtMfrBundleFragmentFormatEndToEnd,
                                                          cAtMfrBundleFragmentFormatUNINNI};

static const char * cAtSchedulingModeStr[] = {"random", "fair", "strict", "smart", "deficit"};
static const eAtHdlcBundleSchedulingMode cAtSchedulingModeVal[]={cAtHdlcBundleSchedulingModeRandom,
                                                                 cAtHdlcBundleSchedulingModeFairRR,
                                                                 cAtHdlcBundleSchedulingModeStrictRR,
                                                                 cAtHdlcBundleSchedulingModeSmartRR,
                                                                 cAtHdlcBundleSchedulingModeDWRR};

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *TwoLevelsIdMfrVcMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u", AtModuleEncapMaxBundlesGet(CliAtEncapModule()), cDlciMaxId);

    return maxFormat;
    }

uint32 CliAtModuleEncapMfrVirtualCircuitFromNonPrefixString(char *pStrIdList, AtChannel *channelList, uint32 bufferSize)
    {
    uint32 i;
    uint32 numChannels = 0;
    AtIdParser idParser = AtIdParserNew(pStrIdList, cTwoLevelsIdVcMinFormatStr, TwoLevelsIdMfrVcMaxFormat());

    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        AtFrVirtualCircuit mfrVc;
        uint32 firstPosId = AtIdParserNextNumber(idParser);
        uint32 secondPosId = AtIdParserNextNumber(idParser);

        AtMfrBundle bundle = (AtMfrBundle)AtModuleEncapBundleGet(CliAtEncapModule(), CliId2DriverId(firstPosId));
        if (bundle == NULL)
            continue;

        if (AtEncapBundleTypeGet((AtEncapBundle)bundle) != cAtEncapBundleTypeMfr)
            continue;

        mfrVc = AtMfrBundleVirtualCircuitGet(bundle, secondPosId);
        if (mfrVc != NULL)
            {
            channelList[numChannels++] = (AtChannel) mfrVc;
            if (numChannels >= bufferSize)
                break;
            }
        }

    AtObjectDelete((AtObject)idParser);
    return numChannels;
    }

static AtEthFlow *CliBundleOamFlowListGet(char *pStrBundlesIdList, uint32 *numFlows)
    {
    AtMfrBundle *bundlesBuffer;
    AtEthFlow *allFlows;
    uint32 numBundles;
    uint32 bundlesIdx;
    uint32 numFlowIds;
    uint32 numValidFlows = 0;

    /* Get the shared ID buffer */
    bundlesBuffer = (AtMfrBundle *)((void**)CliSharedChannelListGet(&numBundles));
    numBundles = CliMfrBundleIdListFromString(pStrBundlesIdList, bundlesBuffer, numBundles);
    if (numBundles == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle(s) %s does not exist or invalid bundle id, expected 1-%d or vcg.1-%d\r\n",
                 pStrBundlesIdList,
                 AtModuleEncapMaxBundlesGet(CliAtEncapModule()),
                 AtModuleEncapMaxBundlesGet(CliAtEncapModule()));
        return NULL;
        }

    allFlows = (AtEthFlow *)((void**)CliSharedObjectListGet(&numFlowIds));
    for (bundlesIdx = 0; bundlesIdx < numBundles; bundlesIdx++)
        {
        AtEthFlow flow = AtHdlcBundleOamFlowGet((AtHdlcBundle)bundlesBuffer[bundlesIdx]);
        if (flow != NULL)
            {
            allFlows[numValidFlows]= flow;
            numValidFlows++;

            /* If store full share buffer */
            if (numValidFlows == numFlowIds)
                break;
            }
        else
            AtPrintc(cSevCritical, "ERROR: Flow does not exist on channel %s\r\n", CliChannelIdStringGet((AtChannel) bundlesBuffer[bundlesIdx]));
        }

    *numFlows = numValidFlows;

    return allFlows;
    }

static AtEthFlow *CliBundleVirtualCircuitOamFlowListGet(char *pStrVirtualCircuitsIdList, uint32 *numFlows)
    {
    AtFrVirtualCircuit *vcsBuffer;
    AtEthFlow *allFlows;
    uint32 numVcs;
    uint32 vcIdx;
    uint32 numFlowIds;
    uint32 numValidFlows = 0;

    /* Get the shared ID buffer */
    vcsBuffer = (AtFrVirtualCircuit *)((void**)CliSharedChannelListGet(&numVcs));
    numVcs = CliAtModuleEncapMfrVirtualCircuitFromNonPrefixString(pStrVirtualCircuitsIdList, (AtChannel *)vcsBuffer, numVcs);
    if (numVcs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: MFR Virtual Circuit(s) %s does not exist or invalid id, expect format <id>.<dlci>\r\n", pStrVirtualCircuitsIdList);
        return NULL;
        }

    allFlows = (AtEthFlow *)((void**)CliSharedObjectListGet(&numFlowIds));
    for (vcIdx = 0; vcIdx < numVcs; vcIdx++)
        {
        AtEthFlow flow = AtFrVirtualCircuitOamFlowGet(vcsBuffer[vcIdx]);
        if (flow != NULL)
            {
            allFlows[numValidFlows] = flow;
            numValidFlows++;

            /* If store full share buffer */
            if (numValidFlows == numFlowIds)
                break;
            }
        else
            AtPrintc(cSevCritical, "Flow does not exist on channel %s\r\n", CliChannelIdStringGet((AtChannel)vcsBuffer[vcIdx]));
        }

    *numFlows = numValidFlows;

    return allFlows;
    }

uint32 CliMfrBundleIdListFromString(char *idList, AtMfrBundle *bundles, uint32 bufferSize)
    {
    uint32 idBufferSize;
    uint32 bundle_i;
    uint32 numValidBundle = 0;
    uint32 *idBuffer = CliSharedIdBufferGet(&idBufferSize);
    uint32 numberBundles = CliIdListFromString(idList, idBuffer, idBufferSize);

    for (bundle_i = 0; bundle_i < numberBundles; bundle_i++)
        {
        AtHdlcBundle bundle = AtModuleEncapBundleGet(CliAtEncapModule(), CliId2DriverId(idBuffer[bundle_i]));
        if (bundle == NULL)
            {
            AtPrintc(cSevWarning, "Bundle = %u does not exist!\r\n", idBuffer[bundle_i]);
            continue;
            }

        if (AtEncapBundleTypeGet((AtEncapBundle) bundle) != cAtEncapBundleTypeMfr)
            {
            AtPrintc(cSevWarning, "Bundle = %u is not MFR bundle\r\n", idBuffer[bundle_i]);
            continue;
            }

        if (numValidBundle >= bufferSize)
            break;

        bundles[numValidBundle++] = (AtMfrBundle)bundle;
        }

    return numValidBundle;
    }

static AtHdlcBundle OneBundleFromString(char* idString)
    {
    AtMfrBundle *bundles;
    uint32 numBundles;
    uint32 bufferSize;

    /* Get the shared ID buffer */
    bundles = (AtMfrBundle *)((void**)CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(idString, bundles, bufferSize);
    if (numBundles == 0)
        return NULL;

    if (numBundles > 1)
        AtPrintc(cSevWarning, "WARNING: Only accept one bundle, only first bundle will be processed\r\n");

    return (AtHdlcBundle)bundles[0];
    }

eBool CmdAtMfrLinkVirtualCircuitCreate(char argc, char **argv)
    {
    uint32 *idBuffer, bufferSize;
    uint32 dlci_i, numDlci;
    eBool cliSuccess = cAtTrue;
    AtHdlcBundle bundle;
    AtFrVirtualCircuit virtualCircuit;
    AtUnused(argc);

    bundle = OneBundleFromString(argv[0]);
    if (bundle == NULL)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numDlci = CliIdListFromString(argv[1], idBuffer, bufferSize);
    if (numDlci == 0)
        {
        AtPrintc(cSevCritical, "ERROR: DLCI ID list may be invalid, expected 1,2...\r\n");
        return cAtFalse;
        }

    for (dlci_i = 0; dlci_i < numDlci; dlci_i++)
        {
        virtualCircuit = AtMfrBundleVirtualCircuitCreate((AtMfrBundle)bundle, idBuffer[dlci_i]);
        if (virtualCircuit == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: The channel %u exist or invalid id\r\n", idBuffer[dlci_i]);
            cliSuccess = cAtFalse;
            }
        }

    return cliSuccess;
    }

eBool CmdAtMfrLinkVirtualCircuitDelete(char argc, char **argv)
    {
    uint32 *idBuffer, bufferSize;
    uint32 dlci_i, numDlci;
    eBool cliSuccess = cAtTrue;
    eAtRet ret;
    AtHdlcBundle bundle;
    AtUnused(argc);

    bundle = OneBundleFromString(argv[0]);
    if (bundle == NULL)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numDlci = CliIdListFromString(argv[1], idBuffer, bufferSize);
    if (numDlci == 0)
        {
        AtPrintc(cSevCritical, "ERROR: DLCI ID list may be invalid, expected 1,2...\r\n");
        return cAtFalse;
        }

    for (dlci_i = 0; dlci_i < numDlci; dlci_i++)
        {
        ret = AtMfrBundleVirtualCircuitDelete((AtMfrBundle)bundle, idBuffer[dlci_i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: %s delete Virtual-Circuit DLCI %d, ret = %s \r\n",
                     CliChannelIdStringGet((AtChannel) bundle),
                     idBuffer[dlci_i],
                     AtRet2String(ret));
            cliSuccess = cAtFalse;
            }
        }

    return cliSuccess;
    }

eBool CmdAtMfrBundleCreate(char argc, char **argv)
    {
    uint32 *idBuffer, bufferSize, numberChannels, channel_i;
    eBool cliSuccess = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuffer = CliSharedIdBufferGet(&bufferSize);
    numberChannels = CliIdListFromString(argv[0], idBuffer, bufferSize);
    if (numberChannels == 0)
       {
       AtPrintc(cSevCritical, "ERROR: Invalid Id, expected 1-%d\r\n", AtModuleEncapMaxBundlesGet(CliAtEncapModule()));
       return cAtFalse;
       }

    for (channel_i = 0; channel_i < numberChannels; channel_i++)
        {
        AtMfrBundle bundle = AtModuleEncapMfrBundleCreate(CliAtEncapModule(), CliId2DriverId(idBuffer[channel_i]));

        if (bundle == NULL)
            {
            cliSuccess = cAtFalse;
            AtPrintc(cSevCritical, "Bundle = %u, maybe not created. It may be exits!\r\n", idBuffer[channel_i]);
            }
        }

    return cliSuccess;
    }

eBool CmdAtMfrBundleDelete(char argc, char **argv)
    {
    uint32 bufferSize, numBundles, bundleIdx;
    eAtRet ret;
    eBool cliSuccess = cAtTrue;
    AtMfrBundle *bundlesList;
    AtUnused(argc);

    /* Get the shared ID buffer */
    bundlesList = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundlesList, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    for (bundleIdx = 0; bundleIdx < numBundles; bundleIdx++)
        {
        ret = AtModuleEncapBundleDelete(CliAtEncapModule(), AtChannelIdGet((AtChannel)bundlesList[bundleIdx]));
        if (ret != cAtOk)
            {
            cliSuccess = cAtFalse;
            AtPrintc(cSevCritical,
                     "ERROR: Can not delete %s, ret = %s \r\n",
                     CliChannelIdStringGet((AtChannel)bundlesList[bundleIdx]),
                     AtRet2String(ret));
            }
        }

    return cliSuccess;
    }

eBool CmdAtMfrBundleLinkAdd(char argc, char **argv)
    {
    uint32 bundleId;
    uint32 numLinks, link_i;
    AtFrLink *links;
    AtHdlcBundle bundle;
    eBool cliSuccess = cAtTrue;

    AtUnused(argc);

    /* Get Input */
    bundleId = AtStrToDw(argv[0]);
    links = (AtFrLink *) ((void**) CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[1], links, numLinks);
    if (numLinks == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Link(s) %s does not exist, invalid encapsulation type or invalid id expected 1-%d\r\n", argv[1], AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtFalse;
        }

    bundle = AtModuleEncapBundleGet(CliAtEncapModule(), CliId2DriverId(bundleId));
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not existed! \r\n", bundleId);
        return cAtFalse;
        }

    if (AtEncapBundleTypeGet((AtEncapBundle) bundle) != cAtEncapBundleTypeMfr)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not MFR bundle\r\n", bundleId);
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        eAtRet ret = AtHdlcBundleLinkAdd(bundle, (AtHdlcLink) links[link_i]);

        if (ret != cAtOk)
            {
            cliSuccess = cAtFalse;
            AtPrintc(cSevCritical,
                     "ERROR: Can not add link %s to Bundle = %u, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel) links[link_i]),
                     bundleId,
                     AtRet2String(ret));
            }
        }

    return cliSuccess;
    }

eBool CmdAtMfrBundleLinkRemove(char argc, char **argv)
    {
    uint32 bundleId;
    uint32 numLinks, link_i;
    AtFrLink *links;
    AtHdlcBundle bundle;
    eBool cliSuccess = cAtTrue;

    AtUnused(argc);

    /* Get Input */
    bundleId = AtStrToDw(argv[0]);
    links = (AtFrLink *) ((void**) CliSharedChannelListGet(&numLinks));
    numLinks = CliFrLinkIdListFromString(argv[1], links, numLinks);
    if (numLinks == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Link(s) %s does not exist, invalid encapsulation type or invalid id expected 1-%d\r\n", argv[1], AtModuleEncapMaxChannelsGet(CliAtEncapModule()));
        return cAtFalse;
        }

    bundle = AtModuleEncapBundleGet(CliAtEncapModule(), CliId2DriverId(bundleId));
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not existed! \r\n", bundleId);
        return cAtFalse;
        }

    if (AtEncapBundleTypeGet((AtEncapBundle) bundle) != cAtEncapBundleTypeMfr)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not MFR bundle\r\n", bundleId);
        return cAtFalse;
        }

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        eAtRet ret = AtHdlcBundleLinkRemove(bundle, (AtHdlcLink) links[link_i]);

        if (ret != cAtOk)
            {
            cliSuccess = cAtFalse;
            AtPrintc(cSevCritical,
                     "ERROR: Can not remove link %s to Bundle = %u, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel) links[link_i]),
                     bundleId,
                     AtRet2String(ret));
            }
        }

    return cliSuccess;
    }

eBool CmdAtMfrBundleShow(char argc, char **argv)
    {
    AtMfrBundle *bundles;
    uint32 bufferSize, bundle_i;
    uint32 numBundles;
    tTab *tabPtr = NULL;
    const char *heading[] = {"ID", "Fragment size", "Fragment format", "Scheduling mode"};

    AtUnused(argc);

    /* Get the shared ID buffer */
    bundles = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundles, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numBundles, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        uint32 column = 0;
        const char * stringValue;
        uint32 fragmentSize = AtHdlcBundleFragmentSizeGet((AtHdlcBundle) bundles[bundle_i]);
        eAtMfrBundleFragmentFormat fragmentFormat;
        eAtHdlcBundleSchedulingMode schedulingMode;

        StrToCell(tabPtr,  bundle_i, column++, (char *) CliChannelIdStringGet((AtChannel)bundles[bundle_i]));
        stringValue = CliEnumToString(fragmentSize,
                                      cCmdFragmentSizeStr,
                                      cCmdFragmentSizeVal,
                                      mCount(cCmdFragmentSizeVal),
                                      NULL);
        ColorStrToCell(tabPtr, bundle_i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        fragmentFormat = AtMfrBundleFragmentFormatGet(bundles[bundle_i]);
        stringValue = CliEnumToString(fragmentFormat,
                                      cAtFragmentFormatStr,
                                      cAtFragmentFormatVal,
                                      mCount(cAtFragmentFormatVal),
                                      NULL);
        ColorStrToCell(tabPtr, bundle_i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        schedulingMode = AtHdlcBundleSchedulingModeGet((AtHdlcBundle)bundles[bundle_i]);
        stringValue = CliEnumToString(schedulingMode,
                                      cAtSchedulingModeStr,
                                      cAtSchedulingModeVal,
                                      mCount(cAtSchedulingModeStr),
                                      NULL);
        ColorStrToCell(tabPtr, bundle_i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtMfrBundleCounters(char argc, char **argv)
    {
    AtMfrBundle *bundles;
    uint32 bufferSize, bundle_i;
    uint32 numBundles;
    eAtHistoryReadingMode readingMode;
    tTab *tabPtr;
    uint32 (*CountersGet)(AtChannel, uint16) = NULL;
    const char *heading[] = {"ID", "TxPkts", "TxBytes", "TxFragmentPkts","TxDiscardPkts", "RxPkts", "RxBytes", "RxFragmentPkts","RxDiscardPkts"};

    AtUnused(argc);

    /* Get the shared ID buffer */
    bundles = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundles, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numBundles, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeReadOnly)
        CountersGet = AtChannelCounterGet;
    else
        CountersGet = AtChannelCounterClear;


    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        uint32 column = 0;
        StrToCell(tabPtr,  bundle_i, column++, (char *) CliChannelIdStringGet((AtChannel) bundles[bundle_i]));

        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeTxGoodPkts, cAtCounterTypeGood, tabPtr, bundle_i, column++);
        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeTxGoodBytes, cAtCounterTypeGood, tabPtr, bundle_i, column++);
        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeTxFragments, cAtCounterTypeGood, tabPtr, bundle_i, column++);
        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeTxDiscardPkts, cAtCounterTypeError, tabPtr, bundle_i, column++);

        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeRxGoodPkts, cAtCounterTypeGood, tabPtr, bundle_i, column++);
        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeRxGoodBytes, cAtCounterTypeGood, tabPtr, bundle_i, column++);
        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeRxFragments, cAtCounterTypeGood, tabPtr, bundle_i, column++);
        CliChannelCounterPrintToCell((AtChannel)bundles[bundle_i], CountersGet, cAtMfrBundleCounterTypeRxDiscardPkts, cAtCounterTypeError, tabPtr, bundle_i, column++);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtMfrVirtualCircuitFlowBind(char argc, char **argv)
    {
    uint32 numVcs, vc_i;
    AtChannel *virtualCircuitList;
    eBool cliSuccess = cAtTrue;
    AtList flowsList;
    uint32 numFlows;
    AtUnused(argc);

    virtualCircuitList = (AtChannel *) ((void**) CliSharedChannelListGet(&numVcs));
    numVcs = CliAtModuleEncapMfrVirtualCircuitFromNonPrefixString(argv[0], virtualCircuitList, numVcs);
    if (numVcs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Virtual circuit(s) %s does not exist or invalid channel Id, expected <id>.<dlci>\r\n", argv[0]);
        return cAtFalse;
        }

    /* Get shared channel buffer and parse ID of binded channel */
    if (!AtStrcmp(argv[1], "none"))
        {
        numFlows = numVcs;
        flowsList = NULL;
        }
    else
        flowsList = CliEthFlowListFromString(argv[1], &numFlows);

    if (numVcs != numFlows)
        {
        AtPrintc(cSevCritical, "ERROR: The number of Virtual Circuits and Physicals Binding are different\r\n");
        AtObjectDelete((AtObject)flowsList);
        return cAtFalse;
        }

    for (vc_i = 0; vc_i < numVcs; vc_i++)
        {
        AtEthFlow flow = (flowsList == NULL) ? NULL : (AtEthFlow)AtListObjectGet(flowsList, vc_i);
        eAtModuleFrRet ret = AtFrVirtualCircuitFlowBind((AtFrVirtualCircuit) virtualCircuitList[vc_i], flow);

        if (ret != cAtOk)
            {
            cliSuccess = cAtFalse;
            if (flow)
                {
                AtPrintc(cSevWarning, "Can't bind Virtual Circuit %s to physical %s, ret = %s\r\n",
                         CliFrVirtualCircuitIdStringGet((AtChannel) virtualCircuitList[vc_i]),
                         CliChannelIdStringGet((AtChannel) flow),
                         AtRet2String(ret));
                }
            else
                {
                AtPrintc(cSevWarning, "Can't unbind Virtual Circuit %s, ret = %s\r\n",
                         CliFrVirtualCircuitIdStringGet((AtChannel) virtualCircuitList[vc_i]),
                         AtRet2String(ret));
                }
            }
        }
    AtObjectDelete((AtObject)flowsList);

    return cliSuccess;
    }

eBool CmdMfrBundleShowLink(char argc, char **argv)
    {
    uint32 bundleId, numBundles, numRows;
    AtMfrBundle *bundlesList;
    uint32 bufferSize;
    uint32 rowId = 0;
    tTab   *tabPtr;
    char   buff[16];
    const char *heading[] = {"ID", "LinkId"};
    AtUnused(argc);

    /* Get the shared ID buffer */
    bundlesList = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundlesList, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    /* Calculate number of links of all input bundles */
    numRows = 0;
    for (bundleId = 0; bundleId < numBundles; bundleId++)
        numRows = numRows + AtHdlcBundleNumberOfLinksGet((AtHdlcBundle)bundlesList[bundleId]);

    /* No links to show */
    if (numRows == 0)
        {
        AtPrintc(cSevInfo, "No link to display\r\n");
        return cAtTrue;
        }

    /* Make table */
    tabPtr = TableAlloc(numRows, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Show table */
    for (bundleId = 0; bundleId < numBundles; bundleId++)
        {
        AtIterator  linkIterator;
        AtHdlcLink  link;

        /* Ignore bundle that has no link */
        if (AtHdlcBundleNumberOfLinksGet((AtHdlcBundle)bundlesList[bundleId]) == 0)
            continue;

        linkIterator = AtHdlcBundleLinkIteratorCreate((AtHdlcBundle)bundlesList[bundleId]);
        while ((link = (AtHdlcLink)AtIteratorNext(linkIterator)) != NULL)
            {
            /* Print Bundle Id */
            StrToCell(tabPtr, rowId, 0, (char *) CliChannelIdStringGet((AtChannel) bundlesList[bundleId]));

            /* And this link */
            AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)link) + 1);
            StrToCell(tabPtr, rowId, 1, buff);

            /* Next row */
            rowId = rowId + 1;
            }

        AtObjectDelete((AtObject)linkIterator);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdMfrBundleShowVirtualCircuit(char argc, char **argv)
    {
    uint32 numBundles, bundle_i;
    AtMfrBundle * bundlesList;
    uint32 bufferSize;
    tTab *tabPtr;
    const char *heading[] = {"ID", "DLCI", "BoundFlow"};
    uint32 column = 0;
    uint32 row = 0;

    AtUnused(argc);

    /* Get the shared ID buffer */
    bundlesList = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundlesList, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return 0;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(0, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        if (AtMfrBundleNumberVirtualCircuitGet(bundlesList[bundle_i]) == 0)
            {
            column = 0;
            TableAddRow(tabPtr, 1);
            StrToCell(tabPtr, row, column++, (char *) CliChannelIdStringGet((AtChannel) bundlesList[bundle_i]));
            StrToCell(tabPtr, row, column++, "none");
            StrToCell(tabPtr, row++, column++, "none");
            }
        else
            {
            AtIterator iterator = AtMfrBundleVirtualCircuitIteratorGet(bundlesList[bundle_i]);
            AtFrVirtualCircuit dlciCircuit;
            while((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(iterator)))
                {
                AtEthFlow flow = AtFrVirtualCircuitBoundFlowGet(dlciCircuit);
                column = 0;
                TableAddRow(tabPtr, 1);
                StrToCell(tabPtr, row, column++, (char *) CliChannelIdStringGet((AtChannel) bundlesList[bundle_i]));
                StrToCell(tabPtr, row, column++, CliNumber2String(AtChannelIdGet((AtChannel)dlciCircuit), "%u"));
                StrToCell(tabPtr, row++, column++, (char *) CliChannelIdStringGet((AtChannel) flow));
                }
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliEthOamVlanModify(char **argv, EthFlowsListGetFunc listIdGetFunc, EthVlanManipulateFunc vlanManipulateFunc)
    {
    AtEthFlow *flows;
    uint32 flowIdx, numFlows;
    eAtRet ret = cAtOk;
    tAtEthVlanDesc desc;

    /* Initialize structure */
    AtOsalMemInit(&desc, 0x0, sizeof(desc));

    flows = listIdGetFunc(argv[0], &numFlows);
    if ((numFlows == 0) || (flows == NULL))
        {
        AtPrintc(cSevCritical, "ERROR: No flow\r\n");
        return cAtTrue;
        }

    /* Get Port Id */
    desc.ethPortId = (uint8)CliId2DriverId(AtStrToDw(argv[1]));

    /* Get CVLAN and SVLAN Description */
    if (!CliEthFlowVlanGet(argv[2], argv[3], &desc))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid VLAN, expected %s\r\n", CliExpectedVlanFormat());
        return cAtFalse;
        }

    for (flowIdx = 0; flowIdx < numFlows; flowIdx++)
        {
        ret |= vlanManipulateFunc(flows[flowIdx], &desc);
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdAtMfrVirtualCircuitOamEgressVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleVirtualCircuitOamFlowListGet, AtEthFlowEgressVlanAdd);
    }

eBool CmdAtMfrVirtualCircuitOamEgressVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleVirtualCircuitOamFlowListGet, AtEthFlowEgressVlanRemove);
    }

eBool CmdAtMfrVirtualCircuitOamIngressVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleVirtualCircuitOamFlowListGet, AtEthFlowIngressVlanAdd);
    }

eBool CmdAtMfrVirtualCircuitOamIngressVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleVirtualCircuitOamFlowListGet, AtEthFlowIngressVlanRemove);
    }

eBool CmdAtMfrVirtualCircuitOamShow(char argc, char **argv)
    {
    AtEthFlow *flows;
    uint32 numFlows;
    AtUnused(argc);

    /* Get all flows */
    flows = CliBundleVirtualCircuitOamFlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "No flow\r\n");
        return cAtTrue;
        }

    return CliEthFlowsTableShow(flows, numFlows);
    }

eBool CmdAtMfrOamEgressVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleOamFlowListGet, AtEthFlowEgressVlanAdd);
    }

eBool CmdAtMfrOamEgressVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleOamFlowListGet, AtEthFlowEgressVlanRemove);
    }

eBool CmdAtMfrOamIngressVlanAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleOamFlowListGet, AtEthFlowIngressVlanAdd);
    }

eBool CmdAtMfrOamIngressVlanRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return CliEthOamVlanModify(argv, CliBundleOamFlowListGet, AtEthFlowIngressVlanRemove);
    }

eBool CmdAtMfrOamShow(char argc, char**argv)
    {
    AtEthFlow *flows;
    uint32 numFlows;
    AtUnused(argc);

    /* Get all flows */
    flows = CliBundleOamFlowListGet(argv[0], &numFlows);
    if (numFlows == 0)
        {
        AtPrintc(cSevWarning, "No flow\r\n");
        return cAtTrue;
        }

    return CliEthFlowsTableShow(flows, numFlows);
    }

eBool CmdAtMfrBundleFragmentFormatSet(char argc, char**argv)
    {
    AtMfrBundle *bundles;
    uint32 bufferSize, bundle_i;
    uint32 numBundles;
    eAtMfrBundleFragmentFormat format;
    eAtModuleFrRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    bundles = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundles, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    format = CliStringToEnum(argv[1], cAtFragmentFormatStr, cAtFragmentFormatVal, mCount(cAtFragmentFormatVal), &success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid fragment format\r\n");
        return cAtFalse;
        }

    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        ret = AtMfrBundleFragmentFormatSet(bundles[bundle_i], format);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Fragment format set return failed with ret=%s\r\n", AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdMfrBundleSchedulingModeSet(char argc, char **argv)
    {
    AtMfrBundle *bundles;
    uint32 numBundles, bundle_i;
    uint32 bufferSize;
    eAtHdlcBundleSchedulingMode schedulingMode = cAtHdlcBundleSchedulingModeRandom;
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    bundles = (AtMfrBundle *) ((void**) CliSharedChannelListGet(&bufferSize));
    numBundles = CliMfrBundleIdListFromString(argv[0], bundles, bufferSize);
    if (numBundles == 0)
        {
        mBundleGetErrorPrint();
        return cAtFalse;
        }

    /* Get fragment distribution mode */
    mAtStrToEnum(cAtSchedulingModeStr,
                 cAtSchedulingModeVal,
                 argv[1],
                 schedulingMode,
                 convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Scheduling mode is invalid, expected: random/fair/strict/smart\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        eAtRet ret = AtHdlcBundleSchedulingModeSet((AtHdlcBundle)bundles[bundle_i], schedulingMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot change scheduling mode for bundle %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)bundles[bundle_i]) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC Module
 *
 * File        : CliAtCrossConnect.c
 *
 * Created Date: Oct 23, 2014
 *
 * Description : CLIs of XC channels
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "AtCli.h"
#include "AtDevice.h"
#include "AtCrossConnect.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtCrossConnect (*CrossConnectGet)(AtModuleXc self);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsTwoWayConnect(const char *optionString)
    {
    if (AtStrcmp(optionString, "two-way") == 0)
        return cAtTrue;

    return cAtFalse;
    }

static eBool NeedShowAllConnections(char *optionString)
    {
    if (AtStrcmp(optionString, "full") == 0)
        return cAtTrue;

    return cAtFalse;
    }

static AtModuleXc XcModule(void)
    {
    return (AtModuleXc)AtDeviceModuleGet(CliDevice(), cAtModuleXc);
    }

static eBool Connect(char argc, char **argv,
                     CrossConnectGet xcGetFunc,
                     uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel *wholeChannelBuffer = CliSharedChannelListGet(&numChannels);
    uint32 numSources  = numChannels / 2;
    uint32 numDests    = numChannels / 2;
    AtChannel *sources = wholeChannelBuffer;
    AtChannel *dests   = &(wholeChannelBuffer[numSources]);
    AtCrossConnect crossConnect;
    eBool twoWayConnect = cAtFalse;
    uint16 i;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get source */
    numSources = ChannelListFromString(argv[0], sources, numSources);
    if (!numSources)
        {
        AtPrintc(cSevCritical, "No source to connect.\r\n");
        return cAtFalse;
        }

    /* Get Destination */
    numDests = ChannelListFromString(argv[1], dests, numDests);
    if (!numDests)
        {
        AtPrintc(cSevCritical, "No destination to connect.\r\n");
        return cAtFalse;
        }

    /* Check input source & destination */
    if ((numSources > 1) && (numSources != numDests))
        {
        AtPrintc(cSevCritical, "ERROR: Number of sources (%d) must be equal to destinations (%d)\r\n", numSources, numDests);
        return cAtFalse;
        }

    /* Check if two-way connections need be made */
    if (argc > 2)
        twoWayConnect = IsTwoWayConnect(argv[2]);

    crossConnect = xcGetFunc(XcModule());
    for (i = 0; i < numDests; i++)
        {
        eAtRet ret = cAtOk;
        AtChannel source = (numSources == 1) ? sources[0] : sources[i];

        /* Connect */
        ret |= AtCrossConnectChannelConnect(crossConnect, source, dests[i]);
        if (twoWayConnect)
            ret |= AtCrossConnectChannelConnect(crossConnect, dests[i], source);

        /* Let user know if error */
        if (ret != cAtOk)
            {
            /* Need to separate two calling because of internal shared buffer */
            AtPrintc(cSevCritical, "ERROR: Connect %s ", CliChannelIdStringGet(source));
            AtPrintc(cSevCritical, "to %s fail with ret = %s\r\n", CliChannelIdStringGet(dests[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool Disconnect(char argc, char **argv,
                        CrossConnectGet xcGetFunc,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel *wholeChannelBuffer = CliSharedChannelListGet(&numChannels);
    uint32 numSources  = numChannels / 2;
    uint32 numDests    = numChannels / 2;
    AtChannel *sources = wholeChannelBuffer;
    AtChannel *dests   = &(wholeChannelBuffer[numSources]);
    AtCrossConnect crossConnect = xcGetFunc(XcModule());
    eBool twoWayConnect = cAtFalse;
    uint32 source_i, dest_i;
    char *destChannelsString = NULL;

    /* Get source */
    numSources = ChannelListFromString(argv[0], sources, numSources);
    if (!numSources)
        {
        AtPrintc(cSevCritical, "No source to connect.\r\n");
        return cAtFalse;
        }

    /* If user doesn't input destination list (one-way disconnect) */
    if (argc == 1)
        {
        AtChannel channel = sources[0];
        numDests = AtCrossConnectNumDestChannelsGet(crossConnect, channel);
        for (dest_i = 0; dest_i < numDests; dest_i++)
            dests[dest_i] = AtCrossConnectDestChannelGetByIndex(crossConnect, channel, dest_i);
        }

    /* Check if two-way disconnect or destination list is input */
    else if (argc == 2)
        {
        twoWayConnect = IsTwoWayConnect(argv[1]);
        if (twoWayConnect)
            {
            AtChannel channel = sources[0];
            numDests = AtCrossConnectNumDestChannelsGet(crossConnect, channel);
            for (dest_i = 0; dest_i < numDests; dest_i++)
                dests[dest_i] = AtCrossConnectDestChannelGetByIndex(crossConnect, channel, dest_i);
            }
        else
            destChannelsString = argv[1];
        }

    /* User input both "two-way" option & destination */
    else
        {
        destChannelsString = argv[1];
        twoWayConnect = IsTwoWayConnect(argv[2]);
        if (!twoWayConnect)
            {
            AtPrintc(cSevCritical, "The input mode \"%s\" is invalid\r\n", argv[2]);
            return cAtFalse;
            }
        }

    /* Get destination */
    if (destChannelsString)
        {
        numDests = ChannelListFromString(destChannelsString, dests, numDests);
        if (numDests == 0)
            {
            AtPrintc(cSevCritical, "Invalid destination list or No destination connecting to this source.\r\n");
            return cAtFalse;
            }
        }

    /* In case of many source & destination & number of source & destination is not same. */
    if ((numSources > 1) && (numSources != numDests))
        {
        for (source_i = 0; source_i < numSources; source_i++)
            {
            for (dest_i = 0; dest_i < AtCrossConnectNumDestChannelsGet(crossConnect, sources[source_i]); dest_i++)
                {
                AtChannel source = sources[source_i];
                AtChannel dest = AtCrossConnectDestChannelGetByIndex(crossConnect, source, dest_i);
                eAtRet ret = AtCrossConnectChannelDisconnect(crossConnect, source, dest);
                if (twoWayConnect)
                    ret |= AtCrossConnectChannelDisconnect(crossConnect, dest, source);

                if (ret != cAtOk)
                    {
                    /* Need to separate two calling because of internal shared buffer */
                    AtPrintc(cSevCritical, "ERROR: Disconnect connection from %s ", CliChannelIdStringGet(source));
                    AtPrintc(cSevCritical, "to %s fail with ret = %s\r\n", CliChannelIdStringGet(dest), AtRet2String(ret));
                    success = cAtFalse;
                    }
                }
            }
        }
    else
        {
        for (dest_i = 0; dest_i < numDests; dest_i++)
            {
            AtChannel source = (numSources == 1)? sources[0] : sources[dest_i];
            AtChannel dest   = dests[dest_i];
            eAtRet ret = AtCrossConnectChannelDisconnect(crossConnect, source, dest);
            if (twoWayConnect)
                ret |= AtCrossConnectChannelDisconnect(crossConnect, dest, source);

            /* Let user know if error */
            if (ret != cAtOk)
                {
                /* Need to separate two calling because of internal shared buffer */
                AtPrintc(cSevCritical, "ERROR: Disconnect connection from %s ", CliChannelIdStringGet(source));
                AtPrintc(cSevCritical, "to %s fail with ret = %s\r\n", CliChannelIdStringGet(dest), AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    return success;
    }

static eBool ShowNoneDestination(AtChannel *channels, uint16 channel_i)
    {
    tTab *tabPtr;
    const char* pHeading[] = {"Source", "Destinations"};

    tabPtr = TableAlloc(1, 2, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, 0, 0, (char *)CliChannelIdStringGet(channels[channel_i]));
    StrToCell(tabPtr, 0, 1, "none");

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool ShowDestinations(AtCrossConnect crossConnect, AtChannel channel)
    {
    uint32 row, numRows;
    uint32 dest_i;
    static char description[128];
    tTab *tabPtr;
    const char* pHeading[] = {"Source", "Destinations"};
    uint32 numDests = AtCrossConnectNumDestChannelsGet(crossConnect, channel);
    static uint32 cNumConnectionsPerRow = 8;

    /* Calculate number of rows to create table */
    numRows = numDests / cNumConnectionsPerRow;
    if ((numDests % cNumConnectionsPerRow) != 0)
        numRows = numRows + 1;

    /* Create table */
    tabPtr = TableAlloc(numRows, 2, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    row = 0;
    AtOsalMemInit(description, 0, sizeof(description));

    for (dest_i = 0; dest_i < numDests; dest_i++)
        {
        AtChannel dest = AtCrossConnectDestChannelGetByIndex(crossConnect, channel, dest_i);

        /* Just need to display ID string for the first row */
        if (dest_i == 0)
            ColorStrToCell(tabPtr, dest_i, 0, (char *)CliChannelIdStringGet(channel), cSevInfo);
        else
            ColorStrToCell(tabPtr, dest_i, 0, " ... ", cSevInfo);

        /* Update descriptions */
        AtStrcat(description, ",");
        AtStrcat(description, CliChannelIdStringGet(dest));

        /* Enough data to display on one row, display it */
        if ((((dest_i + 1) % cNumConnectionsPerRow) == 0)|| (dest_i == (numDests - 1)))
            {
            ColorStrToCell(tabPtr, row++, 1, &description[1], cSevMajor); /* Ignore the first ',' */
            AtOsalMemInit(description, 0, sizeof(description));
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool ShowAllDestinations(AtChannel *channels,
                                 uint32 numChannels,
                                 AtCrossConnect crossConnect)
    {
    uint16 channel_i;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eBool success = cAtTrue;

        if (AtCrossConnectNumDestChannelsGet(crossConnect, channels[channel_i]) == 0)
            success = ShowNoneDestination(channels, channel_i);
        else
            success = ShowDestinations(crossConnect, channels[channel_i]);

        if (success == cAtFalse)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool DestinationsShow(char argc, char **argv,
                              CrossConnectGet xcGetFunc,
                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    uint32 numChannels;
    AtChannel *channels;
    uint16 channel_i;
    tTab *tabPtr;
    const char* pHeading[] = {"Source", "Destinations"};
    AtCrossConnect crossConnect = xcGetFunc(XcModule());
    eBool showFull = cAtFalse;

    /* Check if Show full XC */
    if (argc > 1)
        showFull = NeedShowAllConnections(argv[1]);

    /* Get source list */
    channels = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channels, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "No source to connect.\r\n");
        return cAtFalse;
        }

    if (showFull)
        return ShowAllDestinations(channels, numChannels, crossConnect);

    tabPtr = TableAlloc(numChannels, 2, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        static char connections[128];
        uint16 dest_i;
        uint32 numDests = AtCrossConnectNumDestChannelsGet(crossConnect, channels[channel_i]);

        StrToCell(tabPtr, channel_i, 0, (char *)CliChannelIdStringGet(channels[channel_i]));

        /* No destination */
        if (numDests == 0)
            {
            StrToCell(tabPtr, channel_i, 1, "none");
            continue;
            }

        /* Build a string containing all of connections */
        AtOsalMemInit(connections, 0, sizeof(connections));
        for (dest_i = 0; dest_i < numDests; dest_i++)
            {
            AtChannel dest = AtCrossConnectDestChannelGetByIndex(crossConnect, channels[channel_i], dest_i);
            char *channelIdString = CliChannelIdStringGet(dest);
            uint32 newLength = AtStrlen(connections) + AtStrlen(channelIdString);
            uint32 remainBytes = sizeof(connections) - AtStrlen(connections) - 1; /* -1 for trailing */

            /* Not enough buffer to show full connections */
            if (newLength >= sizeof(connections))
                {
                AtStrncat(connections, ",...", remainBytes);
                break;
                }

            AtSnprintf(connections, remainBytes, ",%s", channelIdString);
            }

        StrToCell(tabPtr, channel_i, 1, &connections[1]); /* Cut the first `,` */
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool SourceShow(char argc, char **argv,
                        CrossConnectGet xcGetFunc,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *dests, uint32 bufferSize))
    {
    uint32 numChannels;
    AtChannel *dests;
    AtCrossConnect crossConnect = xcGetFunc(XcModule());
    uint16 dest_i;
    tTab *tabPtr;
    const char* pHeading[] = {"Destination", "Source"};
	AtUnused(argc);

    /* Get channels */
    dests = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], dests, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "No destination to show.\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numChannels, 2, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    for (dest_i = 0; dest_i < numChannels; dest_i++)
        {
        AtChannel source = AtCrossConnectSourceChannelGet(crossConnect, dests[dest_i]);

        StrToCell(tabPtr, dest_i, 0, (char *)CliChannelIdStringGet(dests[dest_i]));
        StrToCell(tabPtr, dest_i, 1, (char *)CliChannelIdStringGet(source));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdXcDe1Connect(char argc, char **argv)
    {
    return Connect(argc, argv, AtModuleXcDe1CrossConnectGet, De1ListFromString);
    }

eBool CmdXcDe1Disconnect(char argc, char **argv)
    {
    return Disconnect(argc, argv, AtModuleXcDe1CrossConnectGet, De1ListFromString);
    }

eBool CmdXcDe3Connect(char argc, char **argv)
    {
    return Connect(argc, argv, AtModuleXcDe3CrossConnectGet, CliDe3ListFromString);
    }

eBool CmdXcDe3Disconnect(char argc, char **argv)
    {
    return Disconnect(argc, argv, AtModuleXcDe3CrossConnectGet, CliDe3ListFromString);
    }

eBool CmdXcNxDs0Connect(char argc, char **argv)
    {
    return Connect(argc, argv, (CrossConnectGet)AtModuleXcDs0CrossConnectGet, CliNxDs0ListFromString);
    }

eBool CmdXcNxDs0Disconnect(char argc, char **argv)
    {
    return Disconnect(argc, argv, (CrossConnectGet)AtModuleXcDs0CrossConnectGet, CliNxDs0ListFromString);
    }

eBool CmdXcNxDs0DestShow(char argc, char **argv)
    {
    return DestinationsShow(argc, argv, (CrossConnectGet)AtModuleXcDs0CrossConnectGet, CliNxDs0ListFromString);
    }

eBool CmdXcDe1DestShow(char argc, char **argv)
    {
    return DestinationsShow(argc, argv, AtModuleXcDe1CrossConnectGet, De1ListFromString);
    }

eBool CmdXcDe3DestShow(char argc, char **argv)
    {
    return DestinationsShow(argc, argv, AtModuleXcDe3CrossConnectGet, CliDe3ListFromString);
    }

eBool CmdXcNxDs0SourceShow(char argc, char **argv)
    {
    return SourceShow(argc, argv, (CrossConnectGet)AtModuleXcDs0CrossConnectGet, CliNxDs0ListFromString);
    }

eBool CmdXcDe1SourceShow(char argc, char **argv)
    {
    return SourceShow(argc, argv, AtModuleXcDe1CrossConnectGet, De1ListFromString);
    }

eBool CmdXcDe3SourceShow(char argc, char **argv)
    {
    return SourceShow(argc, argv, AtModuleXcDe3CrossConnectGet, CliDe3ListFromString);
    }

eBool CmdXcDs0Connect(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    /* TODO: to be provided */
    return cAtFalse;
    }

eBool CmdXcDs0Disconnect(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    /* TODO: to be provided */
    return cAtFalse;
    }

eBool CmdXcDs0DestShow(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    /* TODO: to be provided */
    return cAtFalse;
    }

eBool CmdXcDs0SourceShow(char argc, char **argv)
    {
    return SourceShow(argc, argv, (CrossConnectGet)AtModuleXcDs0CrossConnectGet, CliNxDs0ListFromString);
    }

eBool CmdXcVcConnect(char argc, char **argv)
    {
    return Connect(argc, argv, AtModuleXcVcCrossConnectGet, CliAtModuleSdhChanelsFromString);
    }

eBool CmdXcVcDisconnect(char argc, char **argv)
    {
    return Disconnect(argc, argv, AtModuleXcVcCrossConnectGet, CliAtModuleSdhChanelsFromString);
    }

eBool CmdXcVcDestShow(char argc, char **argv)
    {
    return DestinationsShow(argc, argv, AtModuleXcVcCrossConnectGet, CliAtModuleSdhChanelsFromString);
    }

eBool CmdXcVcSourceShow(char argc, char **argv)
    {
    return SourceShow(argc, argv, AtModuleXcVcCrossConnectGet, CliAtModuleSdhChanelsFromString);
    }

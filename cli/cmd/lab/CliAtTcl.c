/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LAB
 *
 * File        : CliAtTcl.c
 *
 * Created Date: Mar 3, 2017
 *
 * Description : Work with TCL interpreter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern int AtSkdTclEvalFile(const char *filename);

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtTclScriptExecute(char argc, char **argv)
    {
    char *filename = argv[0];
    AtTextUI textUI = AtCliSharedTextUI();
    eBool historyEnabled = AtTextUIHistoryIsEnabled(textUI);
    int ret;

    AtUnused(argc);

    AtTextUIHistoryEnable(textUI, cAtFalse);
    AtTextUIVerbose(textUI, AtTextUIScriptIsVerbosed(textUI));
    ret = AtSkdTclEvalFile(filename);
    AtTextUIVerbose(textUI, cAtFalse);
    AtTextUIHistoryEnable(textUI, historyEnabled);

    return (ret == 0) ? cAtTrue : cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAttSdhLine.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : SDH line ATT CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include <stdlib.h>             /* system()  */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtAttSdhLineRefClkStr[] =
    {
    "free-run",
    "reference"
    };

static const uint32 cAtAttSdhLineRefClkVal[] =
    {
     0,
     1
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtAttSdhRefClkSet(char argc, char **argv)
    {
    uint32 clkRef =  CliStringToEnum(argv[0], cAtAttSdhLineRefClkStr, cAtAttSdhLineRefClkVal, mCount(cAtAttSdhLineRefClkVal), NULL);
    AtUnused(argc);

    /* Reference */
    if (clkRef)
        {
        if (system("idtconfig /home/ATVN_Eng/nmloc/init.d/CE32K7_INLK/CE32_INLK_83M66_25M_19M44_CDR_19M44.tcs") < 0)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot set reference clock\r\n");
            return cAtFalse;
            }
        }

    /* Free */
    else
        {
        if (system("sh /ffs/etc/init.d/CE32_INLK_83M66_25M_19M44.sh") < 0)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot set free-run clock\r\n");
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtAttSdhRefClkShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (system("idt8t49diag -i 19") < 0)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot set reference clock\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

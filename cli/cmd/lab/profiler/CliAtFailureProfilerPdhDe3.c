/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : CliAtFailureProfilerPdhDe3.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : DS3/E3 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "CliAtFailureProfiler.h"
#include "../pdh/CliAtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel *De3sFromString(char *idString, uint32 *numChannels)
    {
    AtChannel *channelList = CliSharedChannelListGet(numChannels);
    *numChannels = CliDe3ListFromString(idString, channelList, *numChannels);
    return channelList;
    }

static eBool Start(char argc, char **argv, eBool started)
    {
    uint32 numChannels;
    AtChannel *channelList = De3sFromString(argv[0], &numChannels);

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerFailureProfilersStart(channelList, numChannels, started);
    }

static eBool Inject(char argc, char **argv, CliAtFailureProfilerInjectHandler injectHandler)
    {
    uint32 numChannels;
    AtChannel *channels = De3sFromString(argv[0], &numChannels);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPdhDe3AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPdhDe3AlarmTypeVal(&numAlarms);

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerInject(channels, numChannels, argv[1], argv[2], alarmTypeStr, alarmTypeVal, numAlarms, injectHandler);
    }

eBool CmdAtFailureProfilerPdhDe3Show(char argc, char **argv)
    {
    uint32 numChannels;
    AtChannel *channelList;
    eAtHistoryReadingMode readMode = CliHistoryReadingModeGet(argv[1]);
    eBool silent = cAtFalse;
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPdhDe3AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPdhDe3AlarmTypeVal(&numAlarms);

    AtUnused(argc);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = De3sFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerEventsShow(channelList, numChannels, silent, readMode,
                                          alarmTypeStr, alarmTypeVal, numAlarms,
                                          alarmTypeStr, alarmTypeVal, numAlarms);
    }

eBool CmdAtFailureProfilerPdhDe3Start(char argc, char **argv)
    {
    return Start(argc, argv, cAtTrue);
    }

eBool CmdAtFailureProfilerPdhDe3Stop(char argc, char **argv)
    {
    return Start(argc, argv, cAtFalse);
    }

eBool CmdAtFailureProfilerPdhDe3DefectInject(char argc, char **argv)
    {
    return Inject(argc, argv, AtFailureProfilerDefectInject);
    }

eBool CmdAtFailureProfilerPdhDe3FailureInject(char argc, char **argv)
    {
    return Inject(argc, argv, AtFailureProfilerFailureInject);
    }

eBool CmdAtFailureProfilerPdhDe3CurrentDefectsShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtChannel *channels = De3sFromString(argv[0], &numChannels);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPdhDe3AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPdhDe3AlarmTypeVal(&numAlarms);

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS3/E3 list, expected: 1.1.1 or 1.1.1-1.1.3, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerCurrentDefectsShow(channels, numChannels, alarmTypeStr, alarmTypeVal, numAlarms);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : CliAtFailureProfilerPw.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : PW CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPw.h"
#include "CliAtFailureProfiler.h"
#include "../pw/CliAtModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Start(char argc, char **argv, eBool started)
    {
    uint32 numPws;
    AtChannel *pws = (AtChannel *)CliPwArrayFromStringGet(argv[0], &numPws);

    AtUnused(argc);

    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, hdlc.1-2, ppp.2-3, fr.1.2-1.4 ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerFailureProfilersStart(pws, numPws, started);
    }

static eBool Inject(char argc, char **argv, CliAtFailureProfilerInjectHandler injectHandler)
    {
    uint32 numPws;
    AtChannel *pws = (AtChannel *)CliPwArrayFromStringGet(argv[0], &numPws);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPwAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPwAlarmTypeVal(&numAlarms);

    AtUnused(argc);

    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, hdlc.1-2, ppp.2-3, fr.1.2-1.4 ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerInject(pws, numPws, argv[1], argv[2], alarmTypeStr, alarmTypeVal, numAlarms, injectHandler);
    }

eBool CmdAtFailureProfilerPwShow(char argc, char **argv)
    {
    AtChannel *channel;
    uint32 numPws;
    eAtHistoryReadingMode readMode = CliHistoryReadingModeGet(argv[1]);
    eBool silent = cAtFalse;
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPwAlarmTypeStrForPwProfile(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPwAlarmTypeValForPwProfile(&numAlarms);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    channel = (AtChannel *)CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, hdlc.1-2, ppp.2-3, fr.1.2-1.4 ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerEventsShow(channel, numPws, silent, readMode,
                                          alarmTypeStr, alarmTypeVal, numAlarms,
                                          alarmTypeStr, alarmTypeVal, numAlarms);
    }

eBool CmdAtFailureProfilerPwStart(char argc, char **argv)
    {
    return Start(argc, argv, cAtTrue);
    }

eBool CmdAtFailureProfilerPwStop(char argc, char **argv)
    {
    return Start(argc, argv, cAtFalse);
    }

eBool CmdAtFailureProfilerPwDefectInject(char argc, char **argv)
    {
    return Inject(argc, argv, AtFailureProfilerDefectInject);
    }

eBool CmdAtFailureProfilerPwFailureInject(char argc, char **argv)
    {
    return Inject(argc, argv, AtFailureProfilerFailureInject);
    }

eBool CmdAtFailureProfilerPwCurrentDefectsShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtChannel *channels = (AtChannel *)CliPwArrayFromStringGet(argv[0], &numChannels);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPwAlarmTypeStrForPwProfile(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPwAlarmTypeValForPwProfile(&numAlarms);

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, hdlc.1-2, ppp.2-3, fr.1.2-1.4 ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerCurrentDefectsShow(channels, numChannels, alarmTypeStr, alarmTypeVal, numAlarms);
    }

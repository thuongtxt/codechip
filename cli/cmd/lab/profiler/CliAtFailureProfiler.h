/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Profiler
 * 
 * File        : CliAtFailureProfiler.h
 * 
 * Created Date: Aug 5, 2017
 *
 * Description : Common functions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATFAILUREPROFILER_H_
#define _CLIATFAILUREPROFILER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtFailureProfiler.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtRet (*CliAtFailureProfilerInjectHandler)(AtFailureProfiler self, uint32 defects, uint32 currentStatus);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtFailureProfilerEventsShow(AtChannel *channels, uint32 numChannel,
                                     eBool silent, eAtHistoryReadingMode readMode,
                                     const char **defectTypeStr, const uint32 *defectTypeVal, uint32 numDefects,
                                     const char **failureTypeStr, const uint32 *failureTypeVal, uint32 numFailures);
eBool CliAtFailureProfilerInject(AtChannel *channels, uint32 numChannels, char *alarmString, char *injectStatus,
                                 const char **alarmTypeStrings, const uint32 *alarmTypeVal, uint32 numAlarms,
                                 CliAtFailureProfilerInjectHandler injectHandler);
eBool CliAtFailureProfilerFailureProfilersStart(AtChannel *channel, uint32 numChannel, eBool started);
eBool CliAtFailureProfilerCurrentDefectsShow(AtChannel *channels, uint32 numChannel,
                                             const char **alarmTypeStr, const uint32 *alarmTypeVal, uint32 numAlarms);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATFAILUREPROFILER_H_ */


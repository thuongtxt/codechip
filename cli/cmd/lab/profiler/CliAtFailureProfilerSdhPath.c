/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : CliAtFailureProfilerSdhPath.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : SDH path CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtFailureProfiler.h"
#include "AtSdhPath.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../sur/CliAtModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Start(char argc, char **argv, eBool started)
    {
    uint32 numberPaths;
    AtChannel *paths = (AtChannel *)CliSdhPathFromArgumentGet(argv[0], &numberPaths);

    AtUnused(argc);

    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerFailureProfilersStart(paths, numberPaths, started);
    }

static eBool Inject(char argc, char **argv,
                    CliAtFailureProfilerInjectHandler injectHandler,
                    uint32 numAlarms,
                    const char **alarmTypeStr,
                    const uint32 *alarmTypeVal)
    {
    uint32 numPaths;
    AtChannel *paths = (AtChannel *)CliSdhPathFromArgumentGet(argv[0], &numPaths);

    AtUnused(argc);

    if (numPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerInject(paths, numPaths, argv[1], argv[2], alarmTypeStr, alarmTypeVal, numAlarms, injectHandler);
    }

eBool CmdAtFailureProfilerSdhPathShow(char argc, char **argv)
    {
    AtChannel *paths;
    uint32 numberPaths;
    eAtHistoryReadingMode readMode = CliHistoryReadingModeGet(argv[1]);
    eBool silent = cAtFalse;
    uint32 numDefects;
    const char **defectTypeStr = CliAtSdhPathAlarmTypeStr(&numDefects);
    const uint32 *defectTypeVal = CliAtSdhPathAlarmTypeVal(&numDefects);
    uint32 numFailures;
    const char **failureTypeStr = CliAtSdhPathFailureTypeStr(&numFailures);
    const uint32 *failureTypeVal = CliAtSdhPathFailureTypeVal(&numFailures);

    AtUnused(argc);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    /* Get list of path */
    paths = (AtChannel *)CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerEventsShow((AtChannel *)paths, numberPaths, silent, readMode,
                                          defectTypeStr, defectTypeVal, numDefects,
                                          failureTypeStr, failureTypeVal, numFailures);
    }

eBool CmdAtFailureProfileSdhPathStart(char argc, char **argv)
    {
    return Start(argc, argv, cAtTrue);
    }

eBool CmdAtFailureProfileSdhPathStop(char argc, char **argv)
    {
    return Start(argc, argv, cAtFalse);
    }

eBool CmdAtFailureProfilerSdhPathDefectInject(char argc, char **argv)
    {
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhPathAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhPathAlarmTypeVal(&numAlarms);
    return Inject(argc, argv, AtFailureProfilerDefectInject, numAlarms, alarmTypeStr, alarmTypeVal);
    }

eBool CmdAtFailureProfilerSdhPathFailureInject(char argc, char **argv)
    {
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhPathFailureTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhPathFailureTypeVal(&numAlarms);
    return Inject(argc, argv, AtFailureProfilerFailureInject, numAlarms, alarmTypeStr, alarmTypeVal);
    }

eBool CmdAtFailureProfilerSdhPathCurrentDefectsShow(char argc, char **argv)
    {
    AtChannel *paths;
    uint32 numberPaths;
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhPathAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhPathAlarmTypeVal(&numAlarms);

    AtUnused(argc);

    /* Get list of path */
    paths = (AtChannel *)CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerCurrentDefectsShow((AtChannel *)paths, numberPaths, alarmTypeStr, alarmTypeVal, numAlarms);
    }

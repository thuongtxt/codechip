/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : CliAtFailureProfilerPdhDe1.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : DS1/E1 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "CliAtFailureProfiler.h"
#include "../pdh/CliAtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel *De1sFromString(char *idString, uint32 *numChannels)
    {
    AtChannel *channelList = CliSharedChannelListGet(numChannels);
    *numChannels = De1ListFromString(idString, channelList, *numChannels);
    return channelList;
    }

static eBool Start(char argc, char **argv, eBool started)
    {
    uint32 numChannels;
    AtChannel *channels = De1sFromString(argv[0], &numChannels);

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerFailureProfilersStart(channels, numChannels, started);
    }

static eBool Inject(char argc, char **argv, CliAtFailureProfilerInjectHandler injectHandler)
    {
    uint32 numChannels;
    AtChannel *channels = De1sFromString(argv[0], &numChannels);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPdhDe1AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPdhDe1AlarmTypeVal(&numAlarms);

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerInject(channels, numChannels, argv[1], argv[2], alarmTypeStr, alarmTypeVal, numAlarms, injectHandler);
    }

eBool CmdAtFailureProfilerPdhDe1Show(char argc, char **argv)
    {
    AtChannel *channelList;
    uint32 numChannels;
    eAtHistoryReadingMode readMode = CliHistoryReadingModeGet(argv[1]);
    eBool silent = cAtFalse;
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPdhDe1AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPdhDe1AlarmTypeVal(&numAlarms);

    AtUnused(argc);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = De1sFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerEventsShow(channelList, numChannels, silent, readMode,
                                          alarmTypeStr, alarmTypeVal, numAlarms,
                                          alarmTypeStr, alarmTypeVal, numAlarms);
    }

eBool CmdAtFailureProfilerPdhDe1Start(char argc, char **argv)
    {
    return Start(argc, argv, cAtTrue);
    }

eBool CmdAtFailureProfilerPdhDe1Stop(char argc, char **argv)
    {
    return Start(argc, argv, cAtFalse);
    }

eBool CmdAtFailureProfilerPdhDe1DefectInject(char argc, char **argv)
    {
    return Inject(argc, argv, AtFailureProfilerDefectInject);
    }

eBool CmdAtFailureProfilerPdhDe1FailureInject(char argc, char **argv)
    {
    return Inject(argc, argv, AtFailureProfilerFailureInject);
    }

eBool CmdAtFailureProfilerPdhDe1CurrentDefectsShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtChannel *channels = De1sFromString(argv[0], &numChannels);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtPdhDe1AlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtPdhDe1AlarmTypeVal(&numAlarms);

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "Invalid DS1/E1 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerCurrentDefectsShow(channels, numChannels, alarmTypeStr, alarmTypeVal, numAlarms);
    }

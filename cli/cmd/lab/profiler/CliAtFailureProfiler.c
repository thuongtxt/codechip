/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : All module
 *
 * File        : CliAtFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : Failure profile
 *
 * Notes       : 
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "CliAtFailureProfiler.h"
#include "AtTokenizer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char const* cStateStr[]  = {"set", "clear"};
static const uint32 cStateVal[] = {cAtTrue, cAtFalse};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsInvalidTime(tAtOsalCurTime *osalTime)
    {
    if ((osalTime->sec  == cInvalidUint32) &&
        (osalTime->usec == cInvalidUint32))
        return cAtTrue;
    return cAtFalse;
    }

static const char *TimeToString(tAtOsalCurTime *osalTime, char *buffer, uint32 bufferSize)
    {
    if (IsInvalidTime(osalTime))
        return "xxx";

    AtSnprintf(buffer, bufferSize, "%u.%u", osalTime->sec, osalTime->usec);
    return buffer;
    }

static eBool IsInvalidNumber(uint32 number)
    {
    return (number == cInvalidUint32) ? cAtTrue : cAtFalse;
    }

static eBool ProfiledTimeInRange(uint32 failureStatus, uint32 profiledTime, uint32 holdOff, uint32 holdOn)
    {
    if (IsInvalidNumber(profiledTime))
        return cAtTrue;

    if (failureStatus)
        return mInRange(profiledTime, holdOff - 500, holdOff + 500);

    return mInRange(profiledTime, holdOn - 500, holdOn + 500);
    }

static AtModuleSur SurModule(void)
    {
    return (AtModuleSur)AtDeviceModuleGet(CliDevice(), cAtModuleSur);
    }

static eBool StatusFromString(char *statusString)
    {
    uint32 status = CliStringToEnum(statusString, cStateStr, cStateVal, mCount(cStateVal), NULL);
    return status ? cAtTrue : cAtFalse;
    }

eBool CliAtFailureProfilerCurrentDefectsShow(AtChannel *channels, uint32 numChannel,
                                             const char **alarmTypeStr, const uint32 *alarmTypeVal, uint32 numAlarms)
    {
    uint32 channel_i;
    tTab *tabPtr = NULL;
    uint32 row = 0;
    uint32 charBufferSize;
    char *charBuffer = CliSharedCharBufferGet(&charBufferSize);
    const char *pHeading[] = {"Channel.defectType", "raiseTime", "clearTime", "currentStatus"};

    /* Create table with titles */
    tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannel; channel_i++)
        {
        AtChannel channel = channels[channel_i];
        AtFailureProfiler profiler;
        uint32 alarm_i;

        profiler = AtChannelFailureProfilerGet(channel);
        if (profiler == NULL)
            continue;

        for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
            {
            uint32 column = 0;
            AtProfilerDefect defect = AtFailureProfilerDefectInfo(profiler, alarmTypeVal[alarm_i]);
            uint32 currentStatus;

            if (defect == NULL)
                continue;

            if (!TableAddRow(tabPtr, 1))
                {
                AtPrintc(cSevCritical, "ERROR: cannot add more rows\r\n");
                break;
                }

            /* Channel ID + alarm name */
            AtSnprintf(charBuffer, charBufferSize, "%s.%s",
                       CliChannelIdStringGet(channel),
                       alarmTypeStr[alarm_i]);
            StrToCell(tabPtr, row, column++, charBuffer);

            StrToCell(tabPtr, row, column++, TimeToString(AtProfilerDefectRaiseTime(defect), charBuffer, charBufferSize));
            StrToCell(tabPtr, row, column++, TimeToString(AtProfilerDefectClearTime(defect), charBuffer, charBufferSize));

            currentStatus = AtProfilerDefectCurrentStatus(defect);
            if (IsInvalidNumber(currentStatus))
                StrToCell(tabPtr, row, column++, "xxx");
            else
                ColorStrToCell(tabPtr, row, column++, currentStatus ? "set" : "clear", currentStatus ? cSevCritical : cSevInfo);

            /* Next row */
            row = row + 1;
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtFailureProfilerInject(AtChannel *channels, uint32 numChannels, char *alarmString, char *injectStatus,
                                 const char **alarmTypeStrings, const uint32 *alarmTypeVal, uint32 numAlarms,
                                 CliAtFailureProfilerInjectHandler injectHandler)
    {
    uint32 channel_i;
    eBool status = StatusFromString(injectStatus);
    eBool success = cAtTrue;
    uint32 injectMask = CliMaskFromString(alarmString, alarmTypeStrings, alarmTypeVal, numAlarms);

    if (injectMask == 0)
        {
        AtPrintc(cSevWarning, "WARNING: Nothing to inject, input mask may be wrong\r\n");
        return cAtTrue;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        uint32 statusMask = status ? injectMask : 0;
        AtFailureProfiler profiler = AtChannelFailureProfilerGet(channels[channel_i]);
        eAtRet ret = injectHandler(profiler, injectMask, statusMask);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Inject fail on %s with ret = %s\r\n",
                 CliChannelIdStringGet(channels[channel_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CliAtFailureProfilerEventsShow(AtChannel *channels, uint32 numChannel,
                                     eBool silent, eAtHistoryReadingMode readMode,
                                     const char **defectTypeStr, const uint32 *defectTypeVal, uint32 numDefects,
                                     const char **failureTypeStr, const uint32 *failureTypeVal, uint32 numFailures)
    {
    AtModuleSur surModule = SurModule();
    uint32 holdOff = AtModuleSurFailureHoldOffTimerGet(surModule);
    uint32 holdOn = AtModuleSurFailureHoldOnTimerGet(surModule);
    uint32 channel_i, event_i;
    tTab *tabPtr = NULL;
    uint32 row = 0;
    uint32 charBufferSize;
    char *charBuffer = CliSharedCharBufferGet(&charBufferSize);
    const char *pHeading[] = {"Channel", "defectType", "defectRaiseTime", "defectClearTime", "defectStatus",
                              "failureType", "failureReportTime", "failureStatus", "profiledTime(ms)", "conclude"};

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (channel_i = 0; channel_i < numChannel; channel_i++)
        {
        AtChannel channel = channels[channel_i];
        AtFailureProfiler profiler = AtChannelFailureProfilerGet(channel);
        AtList events;

        if (profiler == NULL)
            continue;

        AtFailureProfilerLock(profiler);
        events = AtFailureProfilerFailuresGet(profiler);

        for (event_i = 0; event_i < AtListLengthGet(events); event_i++)
            {
            uint32 defectType, currentSatus;
            uint32 failureStatus, failureType, profiledTimeMs;
            eBool pass = cAtFalse;
            uint32 column = 0;
            AtFailureProfilerEvent event = (AtFailureProfilerEvent)AtListObjectGet(events, event_i);
            AtProfilerDefect defect = AtFailureProfilerEventDefect(event);

            /* New row */
            TableAddRow(tabPtr, 1);
            StrToCell(tabPtr, row, column++, CliChannelIdStringGet(channel));

            /* Defect */
            defectType = AtProfilerDefectType(defect);
            StrToCell(tabPtr, row, column++, CliAlarmMaskToString(defectTypeStr, defectTypeVal, numDefects, defectType));
            StrToCell(tabPtr, row, column++, TimeToString(AtProfilerDefectRaiseTime(defect), charBuffer, charBufferSize));
            StrToCell(tabPtr, row, column++, TimeToString(AtProfilerDefectClearTime(defect), charBuffer, charBufferSize));
            currentSatus = AtProfilerDefectCurrentStatus(defect);
            if (IsInvalidNumber(currentSatus))
                StrToCell(tabPtr, row, column++, "xxx");
            else
                {
                currentSatus = currentSatus & defectType;
                ColorStrToCell(tabPtr, row, column++, currentSatus ? "set" : "clear", currentSatus ? cSevCritical : cSevInfo);
                }

            /* Failure */
            failureType = AtFailureProfilerEventType(event);
            StrToCell(tabPtr, row, column++, CliAlarmMaskToString(failureTypeStr, failureTypeVal, numFailures, failureType));
            StrToCell(tabPtr, row, column++, TimeToString(AtFailureProfilerEventReportedTime(event), charBuffer, charBufferSize));
            failureStatus = AtFailureProfilerEventStatus(event) & failureType;
            ColorStrToCell(tabPtr, row, column++, failureStatus ? "set" : "clear", failureStatus ? cSevCritical : cSevInfo);
            profiledTimeMs = AtFailureProfilerEventProfiledTimeMs(event);
            if (IsInvalidNumber(profiledTimeMs))
                StrToCell(tabPtr, row, column++, "xxx");
            else
                StrToCell(tabPtr, row, column++, CliNumber2String(profiledTimeMs, "%u"));

            /* Conclude */
            pass = ProfiledTimeInRange(failureStatus, profiledTimeMs, holdOff, holdOn);
            ColorStrToCell(tabPtr, row, column++, pass ? "pass" : "fail", pass ? cSevInfo : cSevCritical);
            row = row + 1;
            }

        if (readMode == cAtHistoryReadingModeReadToClear)
            AtFailureProfilerFailuresClear(profiler);

        AtFailureProfilerUnlock(profiler);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtFailureProfilerFailureProfilersStart(AtChannel *channels, uint32 numChannel, eBool started)
    {
    eAtRet ret;
    uint32 channel_i;
    eBool success = cAtTrue;

    for (channel_i = 0; channel_i < numChannel; channel_i++)
        {
        AtChannel channel = channels[channel_i];
        AtFailureProfiler profiler = AtChannelFailureProfilerGet(channel);

        if (started)
            ret = AtFailureProfilerStart(profiler);
        else
            ret = AtFailureProfilerStop(profiler);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: %s failure profiling on %s fail with ret = %s\r\n",
                 started ? "start" : "stop",
                 AtObjectToString((AtObject)channel),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

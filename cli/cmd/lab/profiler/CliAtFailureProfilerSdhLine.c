/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : CliAtFailureProfilerSdhLine.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : SDH Line CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtFailureProfiler.h"
#include "AtSdhLine.h"
#include "../sdh/CliAtModuleSdh.h"
#include "../sur/CliAtModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel *LinesFromString(char *pStrIdList, uint32 *numLines)
    {
    uint32 numChannels = 0;
    AtChannel *channels = CliSharedChannelListGet(&numChannels);
    numChannels = CliAtSdhLinesFromString(pStrIdList, channels, numChannels);
    if (numLines)
        *numLines = numChannels;
    return channels;
    }

static eBool Start(char argc, char **argv, eBool started)
    {
    uint32 numLines;
    AtChannel *lines = LinesFromString(argv[0], &numLines);

    AtUnused(argc);

    if (numLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of lines may be wrong, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    return CliAtFailureProfilerFailureProfilersStart(lines, numLines, started);
    }

static eBool Inject(char argc, char **argv,
                    CliAtFailureProfilerInjectHandler injectHandler,
                    uint32 numAlarms,
                    const char **alarmTypeStr,
                    const uint32 *alarmTypeVal)
    {
    uint32 numChannels;
    AtChannel *channelList = LinesFromString(argv[0], &numChannels);

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of lines may be wrong, expected: 1 or 1-3, ...\n");
        return cAtTrue;
        }

    return CliAtFailureProfilerInject(channelList, numChannels, argv[1], argv[2], alarmTypeStr, alarmTypeVal, numAlarms, injectHandler);
    }

eBool CmdAtFailureProfilerSdhLineShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtChannel *channelList = LinesFromString(argv[0], &numChannels);
    eAtHistoryReadingMode readMode = CliHistoryReadingModeGet(argv[1]);
    eBool silent = cAtFalse;
    uint32 numDefects;
    const char **defectTypeStr = CliAtSdhLineAlarmTypeStr(&numDefects);
    const uint32 *defectTypeVal = CliAtSdhLineAlarmTypeVal(&numDefects);
    uint32 numFailures;
    const char **failureTypeStr = CliAtSdhLineFailureTypeStr(&numFailures);
    const uint32 *failureTypeVal = CliAtSdhLineFailureTypeVal(&numFailures);

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SDH line list, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    return CliAtFailureProfilerEventsShow(channelList, numChannels, silent, readMode,
                                          defectTypeStr, defectTypeVal, numDefects,
                                          failureTypeStr, failureTypeVal, numFailures);
    }

eBool CmdAtFailureProfilerSdhLineStart(char argc, char **argv)
    {
    return Start(argc, argv, cAtTrue);
    }

eBool CmdAtFailureProfilerSdhLineStop(char argc, char **argv)
    {
    return Start(argc, argv, cAtFalse);
    }

eBool CmdAtFailureProfilerSdhLineDefectInject(char argc, char **argv)
    {
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhLineAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhLineAlarmTypeVal(&numAlarms);
    return Inject(argc, argv, AtFailureProfilerDefectInject, numAlarms, alarmTypeStr, alarmTypeVal);
    }

eBool CmdAtFailureProfilerSdhLineFailureInject(char argc, char **argv)
    {
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhLineFailureTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhLineFailureTypeVal(&numAlarms);
    return Inject(argc, argv, AtFailureProfilerFailureInject, numAlarms, alarmTypeStr, alarmTypeVal);
    }

eBool CmdAtFailureProfilerSdhLineCurrentDefectsShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtChannel *channels = LinesFromString(argv[0], &numChannels);
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhLineAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhLineAlarmTypeVal(&numAlarms);

    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: List of lines may be wrong, expected: 1 or 1-3, ...\n");
        return cAtTrue;
        }

    return CliAtFailureProfilerCurrentDefectsShow(channels, numChannels, alarmTypeStr, alarmTypeVal, numAlarms);
    }

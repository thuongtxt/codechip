/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CliThaDiag Module Name
 *
 * File        : CliThaDiag.c
 *
 * Created Date: Jun 10, 2015
 *
 * Description : This module implement function to diagnose the device during bring up.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtDiagnose.h"
#include "../pdh/CliAtModulePdh.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtPdhDe1FrameModeStr[] = {
                                      "unknown",
                                      "ds1_unframed",
                                      "ds1_sf",
                                      "ds1_esf",
                                      "e1_unframed",
                                      "e1_basic",
                                      "e1_crc"
                                        };

static const eAtPdhDe1FrameType cAtPdhDe1FrameModeVal[]  = {
                                                   cAtPdhDe1FrameUnknown,
                                                   cAtPdhDs1J1UnFrm      , /**< DS1/J1 Unframe */
                                                   cAtPdhDs1FrmSf        , /**< DS1 SF (D4) */
                                                   cAtPdhDs1FrmEsf       , /**< DS1 ESF */
                                                   cAtPdhE1UnFrm         , /**< E1 unframe */
                                                   cAtPdhE1Frm           , /**< E1 basic frame, FAS/NFAS framing */
                                                   cAtPdhE1MFCrc           /**< E1 Multi-Frame with CRC4 */
                                                    };

static const char *cAtPdhDe3FrameModeStr[] = {
                                      "ds3_unframed",
                                      "ds3_cbit_unchannelize",
                                      "ds3_cbit_28ds1",
                                      "ds3_cbit_21e1",
                                      "ds3_m13_28ds1",
                                      "ds3_m13_21e1",
                                      "e3_unframed",
                                      "e3_g832",
                                      "e3_g751",
                                      "e3_g751_16e1s"
                                      };

static const eAtPdhDe3FrameType cAtPdhDe3FrameModeVal[]  = {
                                                     cAtPdhDs3Unfrm,
                                                     cAtPdhDs3FrmCbitUnChn,
                                                     cAtPdhDs3FrmCbitChnl28Ds1s,
                                                     cAtPdhDs3FrmCbitChnl21E1s,
                                                     cAtPdhDs3FrmM13Chnl28Ds1s,
                                                     cAtPdhDs3FrmM13Chnl21E1s,
                                                     cAtPdhE3Unfrm,
                                                     cAtPdhE3Frmg832,
                                                     cAtPdhE3FrmG751,
                                                     cAtPdhE3FrmG751Chnl16E1s
                                                    };

static const char *cAtCepFrameModeStr[] = {
                                      "vc4",
                                      "vc3",
                                      "vc12",
                                      "vc11",
                                      };

static const eAtCepFrameType cAtCepFrameModeVal[]  = {
                                                     cCliDiagVc4Cep,
                                                     cCliDiagVc3Cep,
                                                     cCliDiagVc12Cep,
                                                     cCliDiagVc11Cep
                                                     };

static const char *cAtAuFrameModeStr[] = {
                                      "au3",
                                      "au4",
                                      };

static const eAtCepFrameType cAtAuFrameModeVal[]  = {
                                                     cCliDiagAu3,
                                                     cCliDiagAu4,
                                                     };

static const char *cCmdLoopbackModeStr[]={"release", "local", "remote"};
static const eAtLoopbackMode cCmdLoopbackModeVal[]={cAtLoopbackModeRelease, cAtLoopbackModeLocal, cAtLoopbackModeRemote };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool DiagDe1Pw(char argc, char **argv,
                       Bert_PdhLoop_ETHloop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, productCode;
    uint32 usSleepBetweenCheck = 0;
    uint16 frame = cAtPdhDe1FrameUnknown;
    eBool blResult;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));

    AtUnused(argc);
    /* Get Framer mode */
    mAtStrToEnum(cAtPdhDe1FrameModeStr, cAtPdhDe1FrameModeVal, argv[0], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: ds1_unframed, ds1_sf, ds1_esf, e1_unframed, e1_basic, e1_crc\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[1]);
    usSleepBetweenCheck = AtStrToDw(argv[2]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }
    /*disable jitterbuffer automatically center after init 1 minute*/
    productCode = AtDeviceProductCodeGet(Device());
    if ((productCode & cBit23_0) == (0x60210021 & cBit23_0)) AtHalWrite(hal, 0x243500, 0xa3);

    return setupMonitorFunc(frame, checkNum, usSleepBetweenCheck);
    }

static eBool DiagCepPw(char argc, char **argv,
                       Bert_PdhLoop_ETHloop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, productCode;
    uint32 usSleepBetweenCheck = 0;
    uint16 frame = 0;
    eBool blResult;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));

    AtUnused(argc);
    /* Get Framer mode */
    mAtStrToEnum(cAtCepFrameModeStr, cAtCepFrameModeVal, argv[0], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: vc3, vc11, vc12\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[1]);
    usSleepBetweenCheck = AtStrToDw(argv[2]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }
    /*disable jitterbuffer automatically center after init 1 minute*/
    productCode = AtDeviceProductCodeGet(Device());
    if ((productCode & cBit23_0) == (0x60210021 & cBit23_0)) AtHalWrite(hal, 0x243500, 0xa3);

    return setupMonitorFunc(frame, checkNum, usSleepBetweenCheck);
    }

static eBool DiagStmCepPw(char argc, char **argv,
                          Bert_StmVcLoop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, bufferSize, numberLine, i;
    uint32 usSleepBetweenCheck = 0;
    uint16 au, frame = 0;
    uint8 lineid = 0;
    uint32 *stmLineIds = CliSharedIdBufferGet(&bufferSize);
    eBool blResult;

    AtUnused(argc);
    numberLine = CliIdListFromString(argv[0], stmLineIds, bufferSize);
    if (stmLineIds == NULL  || !numberLine)
         {
         AtPrintc(cSevCritical, "ERROR: No STM line, the ID List may be wrong, expect 1,2-3,...\r\n");
         return cAtFalse;
         }
    /* Get AU Frame mode */
    mAtStrToEnum(cAtAuFrameModeStr, cAtAuFrameModeVal, argv[1], au, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: au3, au4\r\n");
        return cAtFalse;
        }
    /* Get VC Frame mode */
    mAtStrToEnum(cAtCepFrameModeStr, cAtCepFrameModeVal, argv[2], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: vc4, vc3, vc11, vc12\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[3]);
    usSleepBetweenCheck = AtStrToDw(argv[4]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }

    blResult = cAtTrue;
    for (i = 0; i < numberLine; i++)
        {
        AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
        lineid = (uint8)stmLineIds[i];
        if (lineid >= 1 && lineid <= AtModuleSdhMaxLinesGet(sdhModule))
            {
            lineid--;
            if (!setupMonitorFunc(lineid, au, frame, checkNum, usSleepBetweenCheck))
                blResult = cAtFalse;
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: Invalid ID = %d.\r\n", lineid);
            blResult = cAtFalse;
            }
        }

    return blResult;
    }

static eBool DiagStmDe1Pw(char argc, char **argv,
                          Bert_StmVcLoop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, bufferSize, numberLine, i;
    uint32 usSleepBetweenCheck = 0;
    uint16 au = 0, frame = 0;
    uint8 lineid = 0;
    eBool blResult;
    uint32 *stmLineIds = CliSharedIdBufferGet(&bufferSize);
    AtUnused(argc);


    numberLine = CliIdListFromString(argv[0], stmLineIds, bufferSize);
    if (stmLineIds == NULL  || !numberLine)
        {
        AtPrintc(cSevCritical, "ERROR: No STM line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Get Framer mode */
    mAtStrToEnum(cAtAuFrameModeStr, cAtAuFrameModeVal, argv[1], au, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: au3, au4\r\n");
        return cAtFalse;
        }
    /* Get Framer mode */
    mAtStrToEnum(cAtPdhDe1FrameModeStr, cAtPdhDe1FrameModeVal, argv[2], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: ds1_unframed, ds1_sf, ds1_esf, e1_unframed, e1_basic, e1_crc\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[3]);

    usSleepBetweenCheck = AtStrToDw(argv[4]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }

    blResult = cAtTrue;
    for (i = 0; i < numberLine; i++)
        {
        AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
        lineid = (uint8)stmLineIds[i];
        if (lineid >= 1 && lineid <= AtModuleSdhMaxLinesGet(sdhModule))
            {
            lineid--;
            if (!setupMonitorFunc(lineid, au, frame, checkNum, usSleepBetweenCheck))
                blResult = cAtFalse;
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: Invalid ID = %d.\r\n", lineid);
            blResult = cAtFalse;
            }
        }

    return blResult;
    }

static eBool DiagStmDe3Pw(char argc, char **argv,
                          Bert_StmVcLoop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, bufferSize, numberLine, i;
    uint32 usSleepBetweenCheck = 0;
    uint16 au = 0, frame = 0;
    uint8 lineid = 0;
    eBool blResult;
    uint32 *stmLineIds = CliSharedIdBufferGet(&bufferSize);
    AtUnused(argc);


    numberLine = CliIdListFromString(argv[0], stmLineIds, bufferSize);
    if (stmLineIds == NULL || !numberLine)
        {
        AtPrintc(cSevCritical, "ERROR: No STM line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Get Framer mode */
    mAtStrToEnum(cAtAuFrameModeStr, cAtAuFrameModeVal, argv[1], au, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: au3, au4\r\n");
        return cAtFalse;
        }
    /* Get Framer mode */
    mAtStrToEnum(cAtPdhDe3FrameModeStr, cAtPdhDe3FrameModeVal, argv[2], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[3]);

    usSleepBetweenCheck = AtStrToDw(argv[4]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }

    blResult = cAtTrue;
    for (i = 0; i < numberLine; i++)
        {
        AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
        lineid = (uint8)stmLineIds[i];
        if (lineid >= 1 && lineid <= AtModuleSdhMaxLinesGet(sdhModule))
            {
            lineid--;
            if (!setupMonitorFunc(lineid, au, frame, checkNum, usSleepBetweenCheck))
                blResult = cAtFalse;
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: Invalid ID = %d.\r\n", lineid);
            blResult = cAtFalse;
            }
        }

    return blResult;
    }

static eBool DiagStmDe3De1Pw(char argc, char **argv,
                          Bert_StmVcDe3De1Loop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, bufferSize, numberLine, i;
    uint32 usSleepBetweenCheck = 0;
    uint16 au = 0, de3frame = 0, frame = 0;
    eBool blResult;
    uint8 lineid = 0;
    uint32 *stmLineIds = CliSharedIdBufferGet(&bufferSize);
    AtUnused(argc);

    numberLine = CliIdListFromString(argv[0], stmLineIds, bufferSize);
    if (stmLineIds == NULL  || !numberLine)
        {
        AtPrintc(cSevCritical, "ERROR: No STM line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Get Framer mode */
    mAtStrToEnum(cAtAuFrameModeStr, cAtAuFrameModeVal, argv[1], au, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: au3, au4\r\n");
        return cAtFalse;
        }
    /* Get Framer mode */
    mAtStrToEnum(cAtPdhDe3FrameModeStr, cAtPdhDe3FrameModeVal, argv[2], de3frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n");
        return cAtFalse;
        }

    mAtStrToEnum(cAtPdhDe1FrameModeStr, cAtPdhDe1FrameModeVal, argv[3], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: ds1_unframed, ds1_sf, ds1_esf, e1_unframed, e1_basic, e1_crc\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[4]);

    usSleepBetweenCheck = AtStrToDw(argv[5]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }

    blResult = cAtTrue;
    for (i = 0; i < numberLine; i++)
        {
        AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
        lineid = (uint8)stmLineIds[i];
        if (lineid >= 1 && lineid <= AtModuleSdhMaxLinesGet(sdhModule))
            {
            lineid--;
            if (!setupMonitorFunc(lineid, au, de3frame, frame, checkNum, usSleepBetweenCheck))
                blResult = cAtFalse;
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: Invalid ID = %d.\r\n", lineid);
            blResult = cAtFalse;
            }
        }

    return blResult;
    }

static eBool DiagDe3Pw(char argc, char **argv,
                       Bert_PdhLoop_ETHloop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, productCode;
    uint32 usSleepBetweenCheck = 0;
    uint16 frame = cAtPdhDe1FrameUnknown;
    eBool blResult;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));

    AtUnused(argc);
    /* Get Framer mode */
    mAtStrToEnum(cAtPdhDe3FrameModeStr, cAtPdhDe3FrameModeVal, argv[0], frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: ds3_unframed,\
                                      ds3_cbit_unchannelize,\
                                      ds3_cbit_28ds1,\
                                      ds3_cbit_21e1,\
                                      ds3_m13_28ds1,\
                                      ds3_m13_21e1,\
                                      e3_unframed,\
                                      e3_g832,\
                                      e3_g751,\
                                      e3_g751_16e1s\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[1]);
    usSleepBetweenCheck = AtStrToDw(argv[2]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }
    /*disable jitterbuffer automatically center after init 1 minute*/
    productCode = AtDeviceProductCodeGet(Device());
    if ((productCode & cBit23_0) == (0x60210021 & cBit23_0)) AtHalWrite(hal, 0x243500, 0xa3);

    return setupMonitorFunc(frame, checkNum, usSleepBetweenCheck);
    }


static eBool DiagDe2De1Pw(char argc, char **argv,
                       Bert_M13Loop_ETHloop_Flow_Func setupMonitorFunc)
    {
    uint32 checkNum = 0, productCode;
    uint32 usSleepBetweenCheck = 0;
    uint16 de3frame = cAtPdhDe3FrameUnknown;
    uint16 de1frame = cAtPdhDe1FrameUnknown;
    eBool blResult;
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));

    AtUnused(argc);
    /* Get Framer mode */
    mAtStrToEnum(cAtPdhDe3FrameModeStr, cAtPdhDe3FrameModeVal, argv[0], de3frame, blResult);
    if(!blResult || !AtPdhDe3IsChannelizedFrameType(de3frame))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: \
                                      ds3_cbit_28ds1,\
                                      ds3_cbit_21e1,\
                                      ds3_m13_28ds1,\
                                      ds3_m13_21e1,\
                                      e3_g751_16e1s\r\n");
        return cAtFalse;
        }
    mAtStrToEnum(cAtPdhDe1FrameModeStr, cAtPdhDe1FrameModeVal, argv[1], de1frame, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid framing mode parameter.\r\n"
                                "Expected: ds1_unframed, ds1_sf, ds1_esf, e1_unframed, e1_basic, e1_crc\r\n");
        return cAtFalse;
        }
    checkNum = AtStrToDw(argv[2]);
    usSleepBetweenCheck = AtStrToDw(argv[3]);
    if (AtDeviceInit(Device()) != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Device Init.\r\n");
        return cAtFalse;
        }
    /*disable jitterbuffer automatically center after init 1 minute*/
    productCode = AtDeviceProductCodeGet(Device());
    if ((productCode & cBit23_0) == (0x60210021 & cBit23_0)) AtHalWrite(hal, 0x243500, 0xa3);

    return setupMonitorFunc(de3frame, de1frame, checkNum, usSleepBetweenCheck);
    }

eBool CmdDiagDe1Satop(char argc, char **argv)
    {
    eBool ret = DiagDe1Pw(argc, argv, Bert_LiuDe1Loop_Satop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagDe1Cesop(char argc, char **argv)
    {
    eBool ret = DiagDe1Pw(argc, argv, Bert_LiuDe1Loop_Cesop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagDe3Satop(char argc, char **argv)
    {
    eBool ret = DiagDe3Pw(argc, argv, Bert_LiuDe3Loop_Satop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagDe3De1Satop(char argc, char **argv)
    {
    eBool ret = DiagDe2De1Pw(argc, argv, Bert_LiuDe3Loop_M13satop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagDe3De1Cesop(char argc, char **argv)
    {
    eBool ret = DiagDe2De1Pw(argc, argv, Bert_LiuDe3Loop_M13Cesop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagEc1De3Satop(char argc, char **argv)
    {
    eBool ret = DiagDe3Pw(argc, argv, Bert_Ec1De3Loop_Satop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagEc1De3De1Satop(char argc, char **argv)
    {
    eBool ret = DiagDe2De1Pw(argc, argv, Bert_Ec1De3Loop_M13satop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagEc1De3De1Cesop(char argc, char **argv)
    {
    eBool ret = DiagDe2De1Pw(argc, argv, Bert_Ec1De3Loop_M13Cesop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagEc1Vc1xDe1Satop(char argc, char **argv)
    {
    eBool ret = DiagDe1Pw(argc, argv, Bert_Ec1Vc1xDe1Loop_Satop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagEc1Vc1xDe1Cesop(char argc, char **argv)
    {
    eBool ret = DiagDe1Pw(argc, argv, Bert_Ec1Vc1xDe1Loop_Cesop_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagEc1VcCep(char argc, char **argv)
    {
    eBool ret = DiagCepPw(argc, argv, Bert_Ec1VcLoop_Cep_ETHloop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmVcBert(char argc, char **argv)
    {
    eBool ret = DiagStmCepPw(argc, argv, Bert_StmVcLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmVc1xDe1Bert(char argc, char **argv)
    {
    eBool ret = DiagStmDe1Pw(argc, argv, Bert_StmVc1xDe1Loop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmDe3Bert(char argc, char **argv)
    {
    eBool ret = DiagStmDe3Pw(argc, argv, Bert_StmVc3De3Loop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmDe3De1Bert(char argc, char **argv)
    {
    eBool ret = DiagStmDe3De1Pw(argc, argv, Bert_StmVc3De3De1Loop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmVc1xDe1SatopBert(char argc, char **argv)
    {
    eBool ret = DiagStmDe1Pw(argc, argv, Bert_StmVc1xDe1PwLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmDe3SatopBert(char argc, char **argv)
    {
    eBool ret = DiagStmDe3Pw(argc, argv, Bert_StmVc3De3PwLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmDe3De1SatopBert(char argc, char **argv)
    {
    eBool ret = DiagStmDe3De1Pw(argc, argv, Bert_StmVc3De3De1PwLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmVc1xDe1CesopBert(char argc, char **argv)
    {
    eBool ret = DiagStmDe1Pw(argc, argv, Bert_StmVc1xDe1PwLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmDe3De1CesopBert(char argc, char **argv)
    {
    eBool ret = DiagStmDe3De1Pw(argc, argv, Bert_StmVc3De3De1PwLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmVcCepBert(char argc, char **argv)
    {
    eBool ret = DiagStmCepPw(argc, argv, Bert_StmVcPwLoop_Flow_Execute);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(ret));
    return ret;
    }

eBool CmdDiagStmVcLoop(char argc, char **argv)
    {
    eAtLoopbackMode loopbackMode = cAtLoopbackModeRelease;
    eBool convertSuccess;
    AtUnused(argc);

    mAtStrToEnum(cCmdLoopbackModeStr,
                 cCmdLoopbackModeVal,
                 argv[0],
                 loopbackMode,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <loopbackMode> , expected release|local|remote\r\n");
        return cAtFalse;
        }
    StmVcLoopModeSet((uint8)loopbackMode);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(cAtTrue));
    return cAtTrue;
    }

eBool CmdDiagStmPdhLoop(char argc, char **argv)
    {
    uint32 loopMode;
    AtUnused(argc);

    loopMode = CliPdhLoopbackModeFromString(argv[0]);
    if (loopMode == cAtPdhLoopbackModeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid loopback mode. Expected: %s\r\n", CliPdhValidLoopbackModes());
        return cAtFalse;
        }
    StmVcPdhLoopModeSet((uint8) loopMode);
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), mBoolToBin(cAtTrue));
    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : CliAtDiagProducts.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtDiagnose.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtOsalCurTime curTime, markTime;
#define mProfile_Mark {AtPrintc(cSevWarning, "Start profiling ...\n");AtOsalCurTimeGet(&curTime);}
#define mProfile_End  {AtOsalCurTimeGet(&markTime); AtPrintc(cSevWarning, "End profiling: %u ms\n", AtOsalDifferenceTimeInMs(&markTime, &curTime));}

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Tfi5LineToAug1Set(uint8 lineId)
    {
    AtSdhAug aug16, aug4;
    AtSdhLine stm;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    uint32 i;
    eAtRet retCode = cAtOk;

    /* Setup Tdm */
    stm = AtModuleSdhLineGet(sdhModule, lineId);
    retCode = AtSdhLineRateSet(stm, cAtSdhLineRateStm16);
    if (retCode != cAtOk)
        {
        mChannelLogReturnFalse(retCode, stm, "stm#%lu set rate failed \n", lineId);
        }
    aug16 = AtSdhLineAug16Get(stm, 0);
    if (!aug16)
        {
        mChannelLogReturnFalse(retCode, stm, "stm#%lu get aug16 failed \n", lineId);
        }
    retCode = AtSdhChannelMapTypeSet((AtSdhChannel)aug16, cAtSdhAugMapTypeAug16Map4xAug4s);
    if (retCode != cAtOk)
        {
        mChannelLogReturnFalse(retCode, aug16, "aug16 set aug4 failed \n");
        }
    for (i = 0; i < 4; i++)
        {
        aug4 = AtSdhLineAug4Get(stm, (uint8)i);
        retCode = AtSdhChannelMapTypeSet((AtSdhChannel)aug4, cAtSdhAugMapTypeAug4Map4xAug1s);
        if (retCode != cAtOk)
            {
            mChannelLogReturnFalse(retCode, aug4, "aug4 set aug1 failed \n");
            }
        }

    return cAtTrue;
    }

static AtChannel* Tfi5LineAu4Tug3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtSdhChannel aug1, vc4;
    AtSdhLine stm;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    uint32 currentChannelNum = 0;
    uint8 j, k;
    AtChannel *channelList = NULL, channel;
    eBool ret = cAtTrue;
    eAtRet retCode = cAtOk;
    uint8 lineId = StmLineIdGet();
    AtUnused(frame);

    *chnNum = 0;
    /* Setup Tdm */
    if (!Tfi5LineToAug1Set(lineId))
        return NULL;

    stm = (AtSdhLine)AtModuleSdhLineGet(sdhModule, (uint8)lineId);
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*48);
    for (j = 0; j < 16; j++)
        {
        aug1 = (AtSdhChannel)AtSdhLineAug1Get(stm, j);
        if (!aug1)
            {
            mChannelLog(stm, "aug1#d is NULL \n", j);
            ret = cAtFalse;
            continue;
            }
        retCode = AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
        if (retCode != cAtOk)
            {
            mChannelLog(aug1, "aug#%d set mapping to VC4 failed \n", j);
            ret = cAtFalse;
            continue;
            }
        vc4 = (AtSdhChannel)AtSdhLineVc4Get(stm, j);
        retCode = AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
        if (retCode != cAtOk)
            {
            mChannelLog(aug1, "vc4#%d set mapping to VC4 failed \n", j);
            ret = cAtFalse;
            continue;
            }
        retCode = AtChannelLoopbackSet((AtChannel)vc4, StmVcLoopModeGet());
        if (retCode != cAtOk)
            {
            mChannelLog(vc4, "set loopback failed \n", j);
            ret = cAtFalse;
            continue;
            }
        for (k = 0; k < 3; k++)
            {
            channel = (AtChannel)AtSdhLineTug3Get(stm, j, k);
            if (!channel)
                {
                mDevLog("its TUG3#%d.%d.%d is NULL \n", lineId, j, k);
                ret = cAtFalse;
                }
            else
                {
                channelList[currentChannelNum] = channel;
                currentChannelNum++;
                }
            }
        }

    if (ret == cAtFalse)
        {
        *chnNum = 0;
        AtOsalMemFree(channelList);
        return NULL;
        }

    *chnNum = currentChannelNum;
    return channelList;
    }

static AtChannel* Tfi5LineAu4Vc4Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtSdhChannel aug1, vc4;
    AtSdhLine stm;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    uint32 currentChannelNum = 0;
    uint8 j;
    AtChannel *channelList = NULL, channel;
    eBool ret = cAtTrue;
    eAtRet retCode = cAtOk;
    uint8 lineId = StmLineIdGet();
    AtUnused(frame);

    *chnNum = 0;
    /* Setup Tdm */
    if (!Tfi5LineToAug1Set(lineId))
        return NULL;

    stm = (AtSdhLine)AtModuleSdhLineGet(sdhModule, (uint8)lineId);
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*16);
    for (j = 0; j < 16; j++)
        {
        aug1 = (AtSdhChannel)AtSdhLineAug1Get(stm, j);
        if (!aug1)
            {
            mChannelLog(stm, "aug1#d is NULL \n", j);
            ret = cAtFalse;
            continue;
            }
        retCode = AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
        if (retCode != cAtOk)
            {
            mChannelLog(aug1, "aug#%d set mapping to VC4 failed \n", j);
            ret = cAtFalse;
            continue;
            }
        vc4 = (AtSdhChannel)AtSdhLineVc4Get(stm, j);
        retCode = AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4MapC4);
        if (retCode != cAtOk)
            {
            mChannelLog(aug1, "vc4#%d set mapping to VC4 failed \n", j);
            ret = cAtFalse;
            continue;
            }
        retCode = AtChannelLoopbackSet((AtChannel)vc4, StmVcLoopModeGet());
        if (retCode != cAtOk)
            {
            mChannelLog(vc4, "set loopback failed \n", j);
            ret = cAtFalse;
            continue;
            }
        channel = (AtChannel)vc4;
        channelList[currentChannelNum] = channel;
        currentChannelNum++;
        }

    if (ret == cAtFalse)
        {
        *chnNum = 0;
        AtOsalMemFree(channelList);
        return NULL;
        }

    *chnNum = currentChannelNum;
    return channelList;
    }

static AtChannel* Tfi5LineAu3Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtSdhChannel aug1;
    AtSdhLine stm;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    uint32 currentChannelNum = 0;
    uint8 j, k;
    AtChannel *channelList = NULL, channel;
    eBool ret = cAtTrue;
    eAtRet retCode = cAtOk;
    uint8 lineId = StmLineIdGet();
    AtUnused(frame);

    *chnNum = 0;
    /* Setup Tdm */
    if (!Tfi5LineToAug1Set(lineId))
        return NULL;

    stm = AtModuleSdhLineGet(sdhModule, lineId);
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*48);
    for (j = 0; j < 16; j++)
        {
        aug1 = (AtSdhChannel)AtSdhLineAug1Get(stm, j);
        if (!aug1)
            {
            mChannelLog(stm, "aug1#d is NULL \n", j);
            ret = cAtFalse;
            continue;
            }
        retCode = AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
        if (retCode != cAtOk)
            {
            mChannelLog(aug1, "aug#%d set mapping to 3xVC3 failed \n", j);
            ret = cAtFalse;
            continue;
            }
        for (k = 0; k < 3; k++)
            {
            channel = (AtChannel)AtSdhLineVc3Get(stm, j, k);
            retCode = AtChannelLoopbackSet((AtChannel)channel, StmVcLoopModeGet());
            if (retCode != cAtOk)
                {
                mChannelLog(channel, "set loopback failed \n");
                ret = cAtFalse;
                continue;
                }
            channelList[currentChannelNum] = channel;
            currentChannelNum++;
            }
        }


    if (ret == cAtFalse)
        {
        *chnNum = 0;
        AtOsalMemFree(channelList);
        return NULL;
        }

    *chnNum = currentChannelNum;
    return channelList;
    }

AtChannel* Ec1Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtChannel au3, channel;
    AtSdhLine ec1line;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    uint32 channelNum = AtModulePdhNumberOfDe3sGet(pdhModule);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    uint32 i, currentChannelNum = 0;
    AtChannel *channelList;
    eBool ret = cAtTrue;
    AtPdhSerialLine serLine;
    eAtPdhDe3SerialLineMode serMode;
    eAtRet retCode = cAtOk;

    AtUnused(frame);
    serMode = cAtPdhDe3SerialLineModeEc1;

    /* Setup Tdm */
    if (!channelNum)
        {
        mDevLog("num DE3 is zero\n");
        return NULL;
        }
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*channelNum);
    for (i = 0; i < channelNum; i++)
        {
        serLine = AtModulePdhDe3SerialLineGet(pdhModule, i);
        AtPdhSerialLineModeSet(serLine, serMode);
        ec1line = (AtSdhLine)AtModuleSdhLineGet(sdhModule, (uint8)i);
        retCode = AtChannelLoopbackSet((AtChannel)ec1line, cAtLoopbackModeLocal);
        if (retCode != cAtOk)
            {
            mChannelLog(ec1line, "ec1line#%lu loopback failed \n", i);
            ret = cAtFalse;
            continue;
            }
        au3 = (AtChannel)AtSdhLineAu3Get(ec1line, 0, 0);
        channel = (AtChannel)AtSdhLineVc3Get(ec1line, 0, 0);
        retCode = AtSdhChannelMapTypeSet((AtSdhChannel)channel, cAtSdhVcMapTypeVc3MapC3);/*set for CEP*/
        if (retCode != cAtOk)
            {
            mChannelLog(au3, "VC3#%lu set mapping to C3 failed \n", i);
            ret = cAtFalse;
            }
        else if (!channel)
            {
            mDevLog("VC3#%lu is NULL \n", i);
            ret = cAtFalse;
            }
        else
            {
            channelList[currentChannelNum] = channel;
            currentChannelNum++;
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        *chnNum = 0;
        return NULL;
        }

    *chnNum = currentChannelNum;
    return channelList;
    }

static eAtRet Vc3Map7xTug2Set(AtSdhChannel channel)
    {
    return AtSdhChannelMapTypeSet((AtSdhChannel)channel, cAtSdhVcMapTypeVc3Map7xTug2s);
    }

static eAtRet Tug3Map7xTug2Set(AtSdhChannel channel)
    {
    return AtSdhChannelMapTypeSet((AtSdhChannel)channel, cAtSdhTugMapTypeTug3Map7xTug2s);
    }

static AtChannel* GeneralVc3Setup_ChannelListGet(uint16 istu3, uint32 *chnNum,
                                                  ChannelSetup_ChannelListGet_Func upperLevelChannelListFunc)
    {
    AtChannel tug3, tu3, channel;
    uint32 vc3channelNum;
    uint32 i, currentChannelNum = 0;
    AtChannel *channelList, *vc3ChannelList;
    eBool ret = cAtTrue;
    eAtRet retCode = cAtOk;

    vc3ChannelList = upperLevelChannelListFunc(0, &vc3channelNum);
    /* Setup Tdm */
    if (!vc3channelNum)
        {
        *chnNum = 0;
        mDevLog("num DE3 is zero\n");
        return NULL;
        }

    if (!istu3)
        {
        *chnNum = vc3channelNum;
        return vc3ChannelList;
        }

    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*vc3channelNum*28);
    for (i = 0; i < vc3channelNum; i++)
        {
        tug3 = vc3ChannelList[i];
        retCode = AtSdhChannelMapTypeSet((AtSdhChannel)tug3, cAtSdhTugMapTypeTug3MapVc3);
        if (retCode != cAtOk)
            {
            mChannelLog(tug3, "VC3#%lu set mapping to 7xtug2s failed \n", i);
            ret = cAtFalse;
            }
        else
            {
            tu3 = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)tug3, 0);
            channel = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)tu3, 0);
            channelList[currentChannelNum] = channel;
            currentChannelNum++;
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        currentChannelNum = 0;
        channelList = NULL;
        }
    AtOsalMemFree(vc3ChannelList);
    *chnNum = currentChannelNum;
    return channelList;
    }

static AtChannel* GeneralVc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum,
                                                  ChannelSetup_ChannelListGet_Func upperLevelChannelListFunc,
                                                  DiagSdhChannel7xTug2MapTypeSet sdhUpperLevelMapTypeSetFunc)
    {
    AtChannel vc3, tug2, tu1x, channel;
    uint32 vc3channelNum;
    uint32 i, currentChannelNum = 0;
    AtChannel *channelList, *vc3ChannelList;
    eBool ret = cAtTrue;
    eAtRet retCode = cAtOk;

    vc3ChannelList = upperLevelChannelListFunc(0, &vc3channelNum);
    /* Setup Tdm */
    if (!vc3channelNum)
        {
        *chnNum = 0;
        mDevLog("num DE3 is zero\n");
        return NULL;
        }
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*vc3channelNum*28);
    for (i = 0; i < vc3channelNum; i++)
        {
        vc3 = vc3ChannelList[i];
        retCode = sdhUpperLevelMapTypeSetFunc((AtSdhChannel)vc3);
        if (retCode != cAtOk)
            {
            mChannelLog(vc3, "VC3#%lu set mapping to 7xtug2s failed \n", i);
            ret = cAtFalse;
            }
        else
            {
            uint8 j, k;
            eAtSdhTugMapType tug2Type = isVc12 ? cAtSdhTugMapTypeTug2Map3xTu12s:cAtSdhTugMapTypeTug2Map4xTu11s;
            for (j = 0; j < AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)vc3); j++)
                {
                tug2 = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)vc3, j);
                retCode = AtSdhChannelMapTypeSet((AtSdhChannel)tug2, tug2Type);
                if (retCode != cAtOk)
                    {
                    mChannelLog(tug2, "tug2#%lu set mapping failed \n", j);
                    ret = cAtFalse;
                    }
                else
                    {
                    for (k = 0; k < AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)tug2); k++)
                        {
                        tu1x = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)tug2, k);
                        /*retCode = AtSdhChannelMapTypeSet((AtSdhChannel)tu1x, tuType); @driver does not support*/
                        channel = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)tu1x, 0);
                        retCode = AtSdhChannelMapTypeSet((AtSdhChannel)channel, cAtSdhVcMapTypeVc1xMapC1x);/*set for CEP*/
                        if (retCode != cAtOk)
                            {
                            mChannelLog(channel, "vc1x#%lu set mapping to C1x failed \n", i);
                            ret = cAtFalse;
                            }
                        else if (!channel)
                            {
                            mChannelLog(tug2, "tug1x#%lu get subchannel to NULL \n", k);
                            ret = cAtFalse;
                            }
                        else
                            {
                            channelList[currentChannelNum] = channel;
                            currentChannelNum++;
                            }
                        }
                    }
                }

            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        currentChannelNum = 0;
        channelList = NULL;
        }
    AtOsalMemFree(vc3ChannelList);
    *chnNum = currentChannelNum;
    return channelList;
    }

AtChannel* Ec1Vc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum)
    {
    return GeneralVc1xSetup_ChannelListGet(isVc12, chnNum, Ec1Vc3Setup_ChannelListGet, Vc3Map7xTug2Set);
    }

AtChannel* StmAu3Vc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum)
    {
    return GeneralVc1xSetup_ChannelListGet(isVc12, chnNum, Tfi5LineAu3Vc3Setup_ChannelListGet, Vc3Map7xTug2Set);
    }

AtChannel* StmAu4Vc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum)
    {
    return GeneralVc1xSetup_ChannelListGet(isVc12, chnNum, Tfi5LineAu4Tug3Setup_ChannelListGet, Tug3Map7xTug2Set);
    }

AtChannel* StmAu3Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtUnused(frame);
    return GeneralVc3Setup_ChannelListGet(cAtFalse, chnNum, Tfi5LineAu3Vc3Setup_ChannelListGet);
    }

AtChannel* StmAu4Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtUnused(frame);
    return GeneralVc3Setup_ChannelListGet(cAtTrue, chnNum, Tfi5LineAu4Tug3Setup_ChannelListGet);
    }

AtChannel* StmAu4Vc4Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtUnused(frame);
    return Tfi5LineAu4Vc4Setup_ChannelListGet(frame, chnNum);
    }

AtChannel* Ec1De3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtChannel vc3, channel;
    uint32 channelNum;
    uint32 i, currentChannelNum = 0;
    AtChannel *channelList, *vc3ChannelList;
    eBool ret = cAtTrue;
    eAtRet retCode = cAtOk;

    vc3ChannelList = Ec1Vc3Setup_ChannelListGet(0, &channelNum);
    /* Setup Tdm */
    if (!channelNum)
        {
        mDevLog("num DE3 is zero\n");
        return NULL;
        }
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*channelNum);
    for (i = 0; i < channelNum; i++)
        {
        vc3 = vc3ChannelList[i];
        retCode = AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3MapDe3);
        if (retCode != cAtOk)
            {
            mChannelLog(vc3, "vc3#%lu set map de3 failed \n", i);
            ret = cAtFalse;
            }
        else
            {
            channel = (AtChannel)AtSdhChannelMapChannelGet((AtSdhChannel)vc3);
            if (!channel)
                {
                mDevLog("de3#%lu is NULL \n", i);
                ret = cAtFalse;
                }
            else if (!SetupOnePdhChannel((AtPdhChannel)channel, frame, cAtPdhLoopbackModeRelease))
                {
                ret = cAtFalse;
                }
            else
                {
                channelList[currentChannelNum] = channel;
                currentChannelNum++;
                }
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        currentChannelNum = 0;
        channelList = NULL;
        }
    AtOsalMemFree(vc3ChannelList);
    *chnNum = currentChannelNum;
    return channelList;
    }

static AtChannel* GeneralVcPdhSetup_ChannelListGet(uint16 frame, uint32 *chnNum,
                                                     ChannelSetup_ChannelListGet_Func vcChannelList_func,
                                                     eAtModuleSdhRet sdhmapSet(AtSdhChannel, uint8), uint8 mapType,
                                                     SetupOnePdhChannel_Func pdhChannelSetup_func)
    {
    AtChannel vc, channel;
    uint32 channelNum = 0;
    uint32 i, currentChannelNum = 0;
    AtChannel *channelList, *vcChannelList;
    eBool ret = cAtTrue;
    uint16 isVc12 = AtPdhDe1FrameTypeIsE1(frame) ? cAtTrue:cAtFalse;
    eAtRet retCode = cAtOk;


    vcChannelList = vcChannelList_func(isVc12, &channelNum);
    /* Setup Tdm */
    if (!channelNum)
        {
        *chnNum = 0;
        mDevLog("num vc is zero\n");
        return NULL;
        }
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*channelNum);
    for (i = 0; i < channelNum; i++)
        {
        vc = vcChannelList[i];
        retCode = sdhmapSet((AtSdhChannel)vc, mapType);
        if (retCode != cAtOk)
            {
            mChannelLog(vc, "vc#%lu set map de1 failed \n", i);
            ret = cAtFalse;
            }
        else
            {
            channel = (AtChannel)AtSdhChannelMapChannelGet((AtSdhChannel)vc);
            if (!channel)
                {
                mDevLog("de#%lu is NULL \n", i);
                ret = cAtFalse;
                }
            else if ((!pdhChannelSetup_func)
                    || (!pdhChannelSetup_func((AtPdhChannel)channel, frame)))
                {
                ret = cAtFalse;
                }
            else
                {
                channelList[currentChannelNum] = channel;
                currentChannelNum++;
                }
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        currentChannelNum = 0;
        channelList = NULL;
        }
    AtOsalMemFree(vcChannelList);
    *chnNum = currentChannelNum;
    return channelList;
    }

static uint32 De1FrameToMaxxDs0Mask(uint16 frameType)
    {
    if (!AtPdhDe1FrameTypeIsE1(frameType))
        return cBit23_0;
    else if (frameType == cAtPdhE1UnFrm)
        return cBit31_0;
    else if (frameType == cAtPdhE1Frm)
        return cBit31_1;
    else if (frameType == cAtPdhE1MFCrc)
        return cBit31_1;
    else
        return cBit31_0;
    }


static AtChannel* GeneralDe1NxDs0Setup_ChannelListGet(uint16 frame, AtChannel *de1List, uint32 numDe1, uint32 *chnNum)
    {
    AtChannel de1, nxDs0;
    uint32 channelNum = numDe1;
    uint32 i, currentChannelNum = 0;
    AtChannel *channelList;
    eBool ret = cAtTrue;

    /* Setup Tdm */
    if (!channelNum)
        {
        *chnNum = 0;
        mDevLog("num de1 is zero\n");
        return NULL;
        }
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*channelNum);
    for (i = 0; i < channelNum; i++)
        {
        de1 = de1List[i];
        nxDs0 = (AtChannel)NxDS0SetAndCheck((AtPdhDe1)de1, De1FrameToMaxxDs0Mask(frame));
        if (nxDs0)
            {
            channelList[currentChannelNum] = nxDs0;
            currentChannelNum++;
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        currentChannelNum = 0;
        channelList = NULL;
        }
    *chnNum = currentChannelNum;
    return channelList;
    }

static AtChannel* GeneralVcPdhNxDs0Setup_ChannelListGet(uint16 frame, uint32 *chnNum,
                                                     ChannelSetup_ChannelListGet_Func vcChannelList_func,
                                                     eAtModuleSdhRet sdhmapSet(AtSdhChannel, uint8), uint8 mapType,
                                                     SetupOnePdhChannel_Func pdhChannelSetup_func)
    {
    uint32 channelNum = 0;
    AtChannel *channelList, *de1ChannelList;

    de1ChannelList = GeneralVcPdhSetup_ChannelListGet(frame, &channelNum,
                                                       vcChannelList_func,
                                                       sdhmapSet, mapType,
                                                       pdhChannelSetup_func);
    /* Setup Tdm */
    if (!channelNum)
        {
        *chnNum = 0;
        mDevLog("num de1 is zero\n");
        return NULL;
        }

    channelList = GeneralDe1NxDs0Setup_ChannelListGet(frame, de1ChannelList, channelNum, chnNum);
    AtOsalMemFree(de1ChannelList);

    return channelList;
    }

AtChannel* Ec1Vc1xDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhSetup_ChannelListGet(frame, chnNum,
                                              Ec1Vc1xSetup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc1xMapDe1,
                                              SetupOneEc1VcPdhChannel);
    }

AtChannel* StmAu3Vc1xDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhSetup_ChannelListGet(frame, chnNum,
                                              StmAu3Vc1xSetup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc1xMapDe1,
                                              SetupOneStmVcPdhChannel);
    }

AtChannel* StmAu4Vc1xDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhSetup_ChannelListGet(frame, chnNum,
                                              StmAu4Vc1xSetup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc1xMapDe1,
                                              SetupOneStmVcPdhChannel);
    }

AtChannel* StmAu3Vc1xDe1Ds0Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhNxDs0Setup_ChannelListGet(frame, chnNum,
                                              StmAu3Vc1xSetup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc1xMapDe1,
                                              SetupOneStmVcPdhChannel);
    }

AtChannel* StmAu4Vc1xDe1Ds0Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhNxDs0Setup_ChannelListGet(frame, chnNum,
                                              StmAu4Vc1xSetup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc1xMapDe1,
                                              SetupOneStmVcPdhChannel);
    }

AtChannel* StmAu4Vc3De3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhSetup_ChannelListGet(frame, chnNum,
                                              StmAu4Vc3Setup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc3MapDe3,
                                              SetupOneStmVcPdhChannel);
    }

AtChannel* StmAu3Vc3De3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    return GeneralVcPdhSetup_ChannelListGet(frame, chnNum,
                                              StmAu3Vc3Setup_ChannelListGet,
                                              AtSdhChannelMapTypeSet, cAtSdhVcMapTypeVc3MapDe3,
                                              SetupOneStmVcPdhChannel);
    }

AtChannel* StmAu4Vc3De3De1Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum)
    {
    uint32 de3Num;
    AtChannel *list = StmAu4Vc3De3Setup_ChannelListGet(de3frame, &de3Num);
    return De2De1Setup_ChannelListGet(frame, list, de3Num, chnNum);
    }

AtChannel* StmAu3Vc3De3De1Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum)
    {
    uint32 de3Num;
    AtChannel *list = StmAu3Vc3De3Setup_ChannelListGet(de3frame, &de3Num);
    return De2De1Setup_ChannelListGet(frame, list, de3Num, chnNum);
    }

AtChannel* StmAu4Vc3De3De1Ds0Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum)
    {
    uint32 de3Num, de1Num;
    AtChannel *de3List = StmAu4Vc3De3Setup_ChannelListGet(de3frame, &de3Num);
    AtChannel *de1List = De2De1Setup_ChannelListGet(frame, de3List, de3Num, &de1Num);
    AtChannel *nxDs0List = GeneralDe1NxDs0Setup_ChannelListGet(frame, de1List, de1Num, chnNum);
    AtOsalMemFree(de3List);
    AtOsalMemFree(de1List);
    return nxDs0List;
    }

AtChannel* StmAu3Vc3De3De1Ds0Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum)
    {
    uint32 de3Num, de1Num;
    AtChannel *de3List = StmAu3Vc3De3Setup_ChannelListGet(de3frame, &de3Num);
    AtChannel *de1List = De2De1Setup_ChannelListGet(frame, de3List, de3Num, &de1Num);
    AtChannel *nxDs0List = GeneralDe1NxDs0Setup_ChannelListGet(frame, de1List, de1Num, chnNum);
    AtOsalMemFree(de3List);
    AtOsalMemFree(de1List);
    return nxDs0List;
    }

AtChannel* LiuDe3Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtChannel channel;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    uint32 channelNum = AtModulePdhNumberOfDe3sGet(pdhModule);
    uint32 i;
    AtChannel *channelList;
    eBool ret = cAtTrue;
    AtPdhSerialLine serLine;
    eAtPdhDe3SerialLineMode serMode;

    if (AtPdhDe3FrameTypeIsE3(frame))
        serMode = cAtPdhDe3SerialLineModeE3;
    else
        serMode = cAtPdhDe3SerialLineModeDs3;

    /* Setup Tdm */
    if (!channelNum)
        {
        mDevLog("num DE3 is zero\n");
        return NULL;
        }
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*channelNum);
    for (i = 0; i < channelNum; i++)
        {
        serLine = AtModulePdhDe3SerialLineGet(pdhModule, i);
        AtPdhSerialLineModeSet(serLine, serMode);
        channel = (AtChannel)AtModulePdhDe3Get(pdhModule, i);
        if (!channel)
            {
            mDevLog("de3#%lu is NULL \n", i);
            ret = cAtFalse;
            }
        else if (!SetupOnePdhChannel((AtPdhChannel)channel, frame, cAtPdhLoopbackModeLocalLine))
            {
            ret = cAtFalse;
            }
        else
            {
            channelList[i] = channel;
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        *chnNum = 0;
        return NULL;
        }

    *chnNum = channelNum;
    return channelList;
    }

AtChannel* De2De1Setup_ChannelListGet(uint16 de1Frame, AtChannel *de3List, uint32 de3Num, uint32 *de1Num)
    {
    /*Assume the entire DE3 in the de3 list had been configured*/
    uint32 i, j, k, totalDe1Num = 0, de2SubChannelNum = 0, de1SubChannelNum = 0, currentIndex = 0;
    AtChannel *channelList;
    AtPdhChannel de3, de2, de1;
    eBool ret = cAtTrue;

    /*dry run*/
    for (i = 0; i < de3Num; i++)
        {
        de3 = (AtPdhChannel)de3List[i];
        de2SubChannelNum = AtPdhChannelNumberOfSubChannelsGet(de3);
        for (j = 0; j < de2SubChannelNum; j++)
            {
            de2 = AtPdhChannelSubChannelGet(de3, (uint8)j);
            de1SubChannelNum = AtPdhChannelNumberOfSubChannelsGet(de2);
            totalDe1Num += de1SubChannelNum;
            }
        }

    if (!totalDe1Num)
        {
        *de1Num = 0;
        return NULL;
        }
    else
        {
        channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel) * totalDe1Num);
        for (i = 0; i < de3Num; i++)
            {
            de3 = (AtPdhChannel)de3List[i];
            de2SubChannelNum = AtPdhChannelNumberOfSubChannelsGet(de3);
            for (j = 0; j < de2SubChannelNum; j++)
                {
                de2 = AtPdhChannelSubChannelGet(de3, (uint8)j);
                de1SubChannelNum = AtPdhChannelNumberOfSubChannelsGet(de2);
                for (k = 0; k < de1SubChannelNum; k++)
                    {
                    de1 = AtPdhChannelSubChannelGet(de2, (uint8)k);
                    if (!de1)
                        {
                        mChannelLog(de2, "de1#%lu is NULL", k);
                        ret = cAtFalse;
                        }
                    else if (!SetupOnePdhChannel(de1, de1Frame, cAtPdhLoopbackModeRelease))
                        {
                        ret = cAtFalse;
                        }
                    else
                        {
                        channelList[currentIndex] = (AtChannel)de1;
                        currentIndex++;
                        }
                    }
                }
            }
        if (ret == cAtFalse)
            {
            AtOsalMemFree(channelList);
            *de1Num = 0;
            return NULL;
            }
        else
            {
            *de1Num = currentIndex;
            return channelList;
            }
        }
    }

AtChannel* LiuDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum)
    {
    AtChannel channel;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    uint32 channelNum = AtModulePdhNumberOfDe1sGet(pdhModule);
    uint32 i;
    AtChannel *channelList;
    eBool ret = cAtTrue;

    /* Setup Tdm */
    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*channelNum);
    for (i = 0; i < channelNum; i++)
        {
        channel = (AtChannel)AtModulePdhDe1Get(pdhModule, i);
        if (!channel)
            {
            mDevLog("de3#%lu is NULL", i);
            ret = cAtFalse;
            }
        else if (!SetupOnePdhChannel((AtPdhChannel)channel, frame, cAtPdhLoopbackModeLocalLine))
            {
            ret = cAtFalse;
            }
        else
            {
            channelList[i] = channel;
            }
        }

    if (ret == cAtFalse)
        {
        AtOsalMemFree(channelList);
        *chnNum = 0;
        return NULL;
        }

    *chnNum = channelNum;
    return channelList;
    }

AtChannel* LiuDe1NxDs0Setup_ChannelListGet(uint16 frame, AtChannel *de1List, uint32 numDe1, uint32 *chnNum)
    {
    AtChannel de1;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    uint32 numPw = AtModulePwMaxPwsGet(pwModule);
    AtPdhNxDS0 nxDs0;
    uint32 nxDs0Num, mask[31], ds0PerDe1Num, startShift, n, k;

    uint32 i, channelNum = 0;
    AtChannel *channelList;
    eBool ret = cAtTrue;

    /* calculate nxDs0Num */
    if (numPw <= numDe1)
        {
        nxDs0Num = 1;
        if (AtPdhDe1FrameTypeIsE1(frame))
            mask[0] = cBit31_1;
        else
            mask[0] = cBit23_0;
        }
    else
        {
        if (AtPdhDe1FrameTypeIsE1(frame))
            {
            ds0PerDe1Num = 31;
            startShift = 1;
            }
        else
            {
            ds0PerDe1Num = 24;
            startShift = 0;
            }
        nxDs0Num = numPw / numDe1 + (numPw % numDe1 ? 1:0);
        if ((nxDs0Num * (numPw / numDe1)) > ds0PerDe1Num)
            {
            mDevLogReturnFalse(cAtError, "There not enough Ds0 for %lu pw and %lu de1\n", numPw, numDe1);
            *chnNum = 0;
            return NULL;
            }
        else
            {
            for (n = 0; n < nxDs0Num; n++)
                {
                mask[n] = 0;
                for (k = startShift; k < startShift + (numPw/numDe1); k++)
                    mask[n] |= (cBit0 << k);
                startShift += (numPw/numDe1);
                }
            }
        }

    channelList = (AtChannel *)AtOsalMemAlloc(sizeof(AtChannel)*numPw);
    /* Setup channel list: NxDs0 bert only support single NxDs0 per DS1/E1 */
    for (n = 0; n < nxDs0Num; n++)
        {
        for (i = 0; i < numDe1; i++)
            {
            if (channelNum < numPw)
                {
                de1 = de1List[i];
                nxDs0 = NxDS0SetAndCheck((AtPdhDe1)de1, mask[n]);
                if (!nxDs0)
                    {
                    mChannelLog(de1, "nxds0 0x%08X is NULL", mask[n]);
                    ret = cAtFalse;
                    }
                else
                    {
                    channelList[channelNum++] = (AtChannel)nxDs0;
                    }
                }
            }
        }

    if (ret == cAtFalse)
        {
        *chnNum = 0;
        AtOsalMemFree(channelList);
        return NULL;
        }
    else
        {
        *chnNum = channelNum;
        return channelList;
        }
    }

void IdlePwPrint(uint32 i, uint32 maxI)
    {
    uint32 step = 100;

    if ((i%step) == 0)
        AtPrintc(cSevInfo, "Setup for PWs:%u-%u ...\n", i, (i + step - 1) < maxI ? ((i + step - 1)) : (maxI - 1));
    }


eBool Bert_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func tdmListfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc)
    {
    AtChannel channel;
    AtPw pw;
    AtEthPort port0 = NULL;
    uint32 i, channelNum = 0;
    AtChannel *channelList;
    eBool ret = cAtTrue;

    /*setup ETH port0*/
    if (setupEthFunc)
        {
        port0 = setupEthFunc(0);
        if (!port0)
            {
            mDevLog("Get ETH port#0 in failure");
            return cAtFalse;
            }
        }

    /*setup DE1*/
    if (tdmListfunc)
        {
        channelList = tdmListfunc(frame, &channelNum);
        if (!channelList)
            {
            mDevLogReturnFalse(cAtError, "TDM setup was ERROR\n");
            }

        /* setup pw */
        mProfile_Mark
        if (setupPwFunc)
            {
            for (i = 0; i < channelNum; i++)
                {
                channel = channelList[i];
                IdlePwPrint(i, channelNum);
                pw   = setupPwFunc(i, channel, port0);
                if (!pw)
                    {
                    mDevLog("Creature PW#%lu in failure\n", i);
                    ret = cAtFalse;
                    }
                }
            if (ret == cAtFalse)
                {
                AtOsalMemFree(channelList);
                return ret;
                }
            }
        mProfile_End

        /*bert and check*/
        ret = GeneralSlidingTdmChannelsToBertsAndCheck(checkNum, usSleepBetweenCheck, channelList, channelNum,
                                                tdmAlarmCheckFunc,
                                                pwAlarmCheckFunc,
                                                ethAlarmCheckFunc,
                                                bertSetupFunc);
        if (ret && (AtPdhChannelTypeGet((AtPdhChannel)channelList[0])) == cAtPdhChannelTypeDs1)
            ret = T1_Loopcode_Diag_Execute(frame, channelList, channelNum);

        AtOsalMemFree(channelList);
        }

    return ret;
    }

static eBool LoopcodeIsSupportedOnFrame(uint16 frame)
    {
    switch (frame)
        {
        case cAtPdhDs1J1UnFrm: return cAtTrue;
        case cAtPdhDs1FrmSf:   return cAtTrue;
        case cAtPdhDs1FrmEsf:  return cAtTrue;
        default:               return cAtFalse;
        }
    }

static eBool LoopcodeIsSupportedOnDevice(void)
    {
    uint32 productCode = AtDeviceProductCodeGet(Device());
    switch ((productCode & cBit23_0))
        {
        case (0x60210021 & cBit23_0): return cAtTrue;
        case (0x60210011 & cBit23_0): return cAtTrue;
        case (0x60210031 & cBit23_0): return cAtTrue;
        default:                      return cAtFalse;
        }
    }

static eBool Loopcode_Setup(AtChannel *channelList, uint32 numChannels)
    {
    uint32 channel_i;
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        if (AtPdhDe1LoopcodeEnable((AtPdhDe1)channelList[channel_i], cAtTrue) != cAtOk)
            return cAtFalse;
        }
    return cAtTrue;
    }

static eBool Loopcode_Execute(AtChannel *channelList, uint32 numChannels)
    {
    uint32 channel_i, test_i;
    eAtPdhDe1Loopcode txloopcode = cAtPdhDe1LoopcodeLineActivate;
    eAtPdhDe1Loopcode rxloopcode = cAtPdhDe1LoopcodeLineActivate;
    eBool isInband = cAtTrue;
    uint32 numberTests = isInband ? 2 : 4;
#if 0
    uint32 alarmR2c = 0;
    uint32 alarmMask = isInband ? cAtPdhDs1AlarmInbandLoopCodeChange:cAtPdhDs1AlarmOutbandLoopCodeChange;
#endif

    /* Clear all alarm first */
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        AtChannelAlarmInterruptClear(channelList[channel_i]);

    for (test_i = 0; test_i < numberTests; test_i++)
        {
        for (channel_i = 0; channel_i < numChannels; channel_i++)
            {
            if (AtPdhDe1LoopcodeSend((AtPdhDe1)channelList[channel_i], txloopcode) != cAtOk)
                return cAtFalse;
            }
        AtOsalUSleep(20000);
        for (channel_i = 0; channel_i < numChannels; channel_i++)
            {
            rxloopcode = AtPdhDe1LoopcodeReceive((AtPdhDe1)channelList[channel_i]);
            if (rxloopcode != txloopcode)
                {
                mDevLogReturnFalse(cAtError, "rxloopcode != txloopcode at %s\n", AtChannelIdString(channelList[channel_i]));
                return cAtFalse;
                }
#if 0
            alarmR2c = AtChannelAlarmInterruptClear(channelList[channel_i]);
            if (!(alarmR2c & alarmMask))
                {
                mDevLogReturnFalse(cAtError, "Dont get loopcode event change at %s\n", AtChannelIdString(channelList[channel_i]));
                return cAtFalse;
                }
            if ((alarmR2c ^ alarmMask))
                {
                mDevLogReturnFalse(cAtError, "Got alarmR2c %x  change at %s\n", alarmR2c, AtChannelIdString(channelList[channel_i]));
                return cAtFalse;
                }
#endif
            }

        /* Select new code */
        if (txloopcode == cAtPdhDe1LoopcodeLineActivate)
            txloopcode = cAtPdhDe1LoopcodeLineDeactivate;
        else if (txloopcode == cAtPdhDe1LoopcodeLineDeactivate)
            txloopcode = isInband ? cAtPdhDe1LoopcodeInbandDisable : cAtPdhDe1LoopcodePayloadActivate;
        }
    return cAtTrue;
    }

eBool T1_Loopcode_Diag_Execute(uint16 frame, AtChannel *de1Chns, uint32 numDe1Channels)
    {
    eBool retCode;

    /* Will not check if framer not supported */
    if (!(LoopcodeIsSupportedOnFrame(frame) && LoopcodeIsSupportedOnDevice()))
        return cAtTrue;

    retCode = Loopcode_Setup(de1Chns, numDe1Channels);
    if (retCode)
        Loopcode_Execute(de1Chns, numDe1Channels);

    return retCode;
    }

eBool Bert_M13_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func de3channelListfunc,
                        De2De1ChannelSetup_ChannelListGet_Func tdmListfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc)
    {
    AtChannel channel, *de3channelList, *channelList;
    uint32 i, de3channelNum = 0, channelNum = 0;
    AtPw pw;
    AtEthPort port0 = NULL;
    eBool ret = cAtTrue;

    /*setup ETH port0*/
    if (setupEthFunc)
        {
        port0 = setupEthFunc(0);
        if (!port0)
            {
            mDevLog("Get ETH port#0 in failure");
            return cAtFalse;
            }
        }

    /*setup DE1*/
    if (de3channelListfunc && tdmListfunc)
        {
        de3channelList = de3channelListfunc(de3frame, &de3channelNum);
        if (!de3channelList)
            {
            mDevLogReturnFalse(cAtError, "DE3 setup was ERROR\n");
            }
        else
            {
            channelList = tdmListfunc(frame, de3channelList, de3channelNum, &channelNum);
            if (!channelList)
                {
                AtOsalMemFree(de3channelList);
                mDevLogReturnFalse(cAtError, "DE1 setup was ERROR\n");
                }
            }

        /* setup pw */
        mProfile_Mark
        if (setupPwFunc)
            {
            for (i = 0; i < channelNum; i++)
                {
                channel = channelList[i];
                IdlePwPrint(i, channelNum);
                pw   = setupPwFunc(i, channel, port0);
                if (!pw)
                    {
                    mDevLog("Creature PW#%lu in failure\n", i);
                    ret = cAtFalse;
                    }
                }
            if (ret == cAtFalse)
                {
                AtOsalMemFree(de3channelList);
                AtOsalMemFree(channelList);
                return ret;
                }
            }
        mProfile_End

        /*bert and check*/
        ret = GeneralSlidingTdmChannelsToBertsAndCheck(checkNum, usSleepBetweenCheck, channelList, channelNum,
                                                tdmAlarmCheckFunc,
                                                pwAlarmCheckFunc,
                                                ethAlarmCheckFunc,
                                                bertSetupFunc);
        if (ret)
            ret = T1_Loopcode_Diag_Execute(frame, channelList, channelNum);

        AtOsalMemFree(de3channelList);
        AtOsalMemFree(channelList);
        }
    return ret;
    }

eBool Bert_Ds0_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func de1Listfunc,
                        NxDs0Setup_ChannelListGet_Func nxds0Listfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc)
    {
    AtChannel channel, *de1ChannelList, *channelList;
    AtPw pw;
    AtEthPort port0 = NULL;
    uint32 i, channelNum = 0;
    eBool ret = cAtTrue;

    /*setup ETH port0*/
    if (setupEthFunc)
        {
            port0 = setupEthFunc(0);
        if (!port0)
            {
            mDevLog("Get ETH port#0 in failure");
            return cAtFalse;
            }
        }

    /*setup DE1*/
    if (de1Listfunc && nxds0Listfunc)
        {
        de1ChannelList = de1Listfunc(frame, &channelNum);
        if (!de1ChannelList)
            {
            mDevLogReturnFalse(cAtError, "TDM De1 setup was ERROR\n");
            }

        channelList = nxds0Listfunc(frame, de1ChannelList, channelNum, &channelNum);
        if (!channelList)
            {
            mDevLogReturnFalse(cAtError, "TDM NxDs0 setup was ERROR\n");
            }

        /* setup pw */
        mProfile_Mark
        if (setupPwFunc)
            {
            for (i = 0; i < channelNum; i++)
                {
                channel = channelList[i];
                IdlePwPrint(i, channelNum);
                pw   = setupPwFunc(i, channel, port0);
                if (!pw)
                    {
                    mDevLog("Creature PW#%lu in failure\n", i);
                    ret = cAtFalse;
                    }
                }
            if (ret == cAtFalse)
                {
                AtOsalMemFree(de1ChannelList);
                AtOsalMemFree(channelList);
                return ret;
                }
            }
        mProfile_End

        /*bert and check*/
        ret = GeneralSlidingTdmChannelsToBertsAndCheck(checkNum, usSleepBetweenCheck, channelList, channelNum,
                                                tdmAlarmCheckFunc,
                                                pwAlarmCheckFunc,
                                                ethAlarmCheckFunc,
                                                bertSetupFunc);
        if (ret)
            ret = T1_Loopcode_Diag_Execute(frame, de1ChannelList, channelNum);
        AtOsalMemFree(de1ChannelList);
        AtOsalMemFree(channelList);
        }

    return ret;
    }

eBool Bert_M13Ds0_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func de3channelListfunc,
                        De2De1ChannelSetup_ChannelListGet_Func de1channelListfunc,
                        NxDs0Setup_ChannelListGet_Func nxds0Listfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc)
    {
    AtPw pw;
    AtEthPort port0 = NULL;
    uint32 i, de3channelNum = 0, de1channelNum = 0, channelNum = 0;
    AtChannel channel, *de3ChannelList, *de1ChannelList, *channelList;
    eBool ret = cAtTrue;

    /*setup ETH port0*/
    if (setupEthFunc)
        {
            port0 = setupEthFunc(0);
        if (!port0)
            {
            mDevLog("Get ETH port#0 in failure");
            return cAtFalse;
            }
        }

    /*setup DE1*/
    if (de3channelListfunc && de1channelListfunc && nxds0Listfunc)
        {
        de3ChannelList = de3channelListfunc(de3frame, &de3channelNum);
        if (!de3ChannelList)
            {
            mDevLogReturnFalse(cAtError, "DE3 setup was ERROR\n");
            }
        else
            {
            de1ChannelList = de1channelListfunc(frame, de3ChannelList, de3channelNum, &de1channelNum);
            if (!de1ChannelList)
                {
                AtOsalMemFree(de3ChannelList);
                mDevLogReturnFalse(cAtError, "DE1 setup was ERROR\n");
                }
            else
                {
                channelList = nxds0Listfunc(frame, de1ChannelList, de1channelNum, &channelNum);
                if (!channelList)
                    {
                    AtOsalMemFree(de3ChannelList);
                    AtOsalMemFree(de1ChannelList);
                    mDevLogReturnFalse(cAtError, "TDM NxDs0 setup was ERROR\n");
                    }
                }
            }

        /* setup pw */
        mProfile_Mark
        if (setupPwFunc)
            {
            for (i = 0; i < channelNum; i++)
                {
                channel = channelList[i];
                IdlePwPrint(i, channelNum);
                pw   = setupPwFunc(i, channel, port0);
                if (!pw)
                    {
                    mDevLog("Creature PW#%lu in failure\n", i);
                    ret = cAtFalse;
                    }
                }
            if (ret == cAtFalse)
                {
                AtOsalMemFree(de3ChannelList);
                AtOsalMemFree(de1ChannelList);
                AtOsalMemFree(channelList);
                return ret;
                }
            }
        mProfile_End

        /*bert and check*/
        if (bertSetupFunc)
            ret = GeneralSlidingTdmChannelsToBertsAndCheck(checkNum, usSleepBetweenCheck, channelList, channelNum,
                                                    tdmAlarmCheckFunc,
                                                    pwAlarmCheckFunc,
                                                    ethAlarmCheckFunc,
                                                    bertSetupFunc);
        /* Loop Code */
        if (ret)
            ret =  T1_Loopcode_Diag_Execute(frame, de1ChannelList, de1channelNum);

        AtOsalMemFree(de3ChannelList);
        AtOsalMemFree(de1ChannelList);
        AtOsalMemFree(channelList);
        }
    return ret;
    }

eBool Bert_LiuDe3Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                             SetupOne1GEthPort, EthPortAlarmCheck,
                             SetupOneSatopPw, PwAlarmCheck,
                             LiuDe3Setup_ChannelListGet, De3AlarmCheck,
                             SetupOneDe3Bert);
    }

eBool Bert_Ec1De3Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                 SetupOne1GEthPort, EthPortAlarmCheck,
                                 SetupOneSatopPw, PwAlarmCheck,
                                 Ec1De3Setup_ChannelListGet, SdhLineAu3Vc3De3AlarmCheck,
                                 SetupOneDe3Bert);
    }

eBool Bert_LiuDe3Loop_M13satop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_M13_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                           SetupOne1GEthPort, EthPortAlarmCheck,
                           SetupOneSatopPw, PwAlarmCheck,
                           LiuDe3Setup_ChannelListGet,
                           De2De1Setup_ChannelListGet,
                           De2De1AlarmCheck,
                           SetupOneDe1Bert);
    }

eBool Bert_Ec1De3Loop_M13satop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_M13_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                           SetupOne1GEthPort, EthPortAlarmCheck,
                           SetupOneSatopPw, PwAlarmCheck,
                           Ec1De3Setup_ChannelListGet,
                           De2De1Setup_ChannelListGet,
                           SdhLineAu3Vc3De3De2De1AlarmCheck,
                           SetupOneDe1Bert);
    }

eBool Bert_LiuDe1Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     SetupOne1GEthPort, EthPortAlarmCheck,
                                     SetupOneSatopPw, PwAlarmCheck,
                                     LiuDe1Setup_ChannelListGet, De1AlarmCheck,
                                     SetupOneDe1Bert);
    }

eBool Bert_LiuDe1Loop_Cesop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_Ds0_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                         SetupOne1GEthPort, EthPortAlarmCheck,
                         SetupOneCesopPw, PwAlarmCheck,
                         LiuDe1Setup_ChannelListGet, LiuDe1NxDs0Setup_ChannelListGet, De1AlarmCheck,
                         SetupOneNxDs0Bert);
    }

eBool Bert_LiuDe3Loop_M13Cesop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_M13Ds0_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                             SetupOne1GEthPort, EthPortAlarmCheck,
                             SetupOneCesopPw, PwAlarmCheck,
                             LiuDe3Setup_ChannelListGet, De2De1Setup_ChannelListGet, LiuDe1NxDs0Setup_ChannelListGet, De2De1AlarmCheck,
                             SetupOneNxDs0Bert);
    }

eBool Bert_Ec1De3Loop_M13Cesop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_M13Ds0_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                             SetupOne1GEthPort, EthPortAlarmCheck,
                             SetupOneCesopPw, PwAlarmCheck,
                             Ec1De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, LiuDe1NxDs0Setup_ChannelListGet, SdhLineAu3Vc3De3De2De1AlarmCheck,
                             SetupOneNxDs0Bert);
    }

eBool Bert_Ec1Vc1xDe1Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     SetupOne1GEthPort, EthPortAlarmCheck,
                                     SetupOneSatopPw, PwAlarmCheck,
                                     Ec1Vc1xDe1Setup_ChannelListGet, SdhLineAu3Vc3Vc1xDe1AlarmCheck,
                                     SetupOneDe1Bert);
    }

eBool Bert_Ec1Vc1xDe1Loop_Cesop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    return Bert_Ds0_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                SetupOne1GEthPort, EthPortAlarmCheck,
                                SetupOneCesopPw, PwAlarmCheck,
                                Ec1Vc1xDe1Setup_ChannelListGet, LiuDe1NxDs0Setup_ChannelListGet, SdhLineAu3Vc3Vc1xDe1AlarmCheck,
                                SetupOneNxDs0Bert);
    }

eBool Bert_Ec1VcLoop_Cep_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    if (frame == cCliDiagVc11Cep || frame == cCliDiagVc12Cep)
        return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                         SetupOne1GEthPort, EthPortAlarmCheck,
                                         SetupOneCepBasicPw, PwAlarmCheck,
                                         Ec1Vc1xSetup_ChannelListGet, SdhLineAu3Vc3Vc1xAlarmCheck,
                                         SetupOneVcBert);
    else /*VC3 CEP*/
        return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     SetupOne1GEthPort, EthPortAlarmCheck,
                                     SetupOneCepBasicPw, PwAlarmCheck,
                                     Ec1Vc3Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                     SetupOneVcBert);

    }

eBool Bert_StmVcLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        if (frame == cCliDiagVc11Cep)
            return Bert_Flow_Execute(cAtFalse, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu3Vc1xSetup_ChannelListGet, SdhLineAu3Vc3Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc12Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu3Vc1xSetup_ChannelListGet, SdhLineAu3Vc3Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc3Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu3Vc3Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                                      SetupOneVcTdmBert);
        else
            return cAtFalse;
    else /*Au4*/
        if (frame == cCliDiagVc11Cep)
            return Bert_Flow_Execute(cAtFalse, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu4Vc1xSetup_ChannelListGet, SdhLineAu4Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc12Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu4Vc1xSetup_ChannelListGet, SdhLineAu4Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc3Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu4Vc3Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc4Cep)
                    return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu4Vc4Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                                      SetupOneVcTdmBert);
        else
            return cAtFalse;
    }

eBool Bert_StmVcPwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        if (frame == cCliDiagVc11Cep)
            return Bert_Flow_Execute(cAtFalse, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu3Vc1xSetup_ChannelListGet, SdhLineAu3Vc3Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc12Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu3Vc1xSetup_ChannelListGet, SdhLineAu3Vc3Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc3Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                                     SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu3Vc3Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                                      SetupOneVcTdmBert);
        else
            return cAtFalse;
    else /*Au4*/
        if (frame == cCliDiagVc11Cep)
            return Bert_Flow_Execute(cAtFalse, checkNum, usSleepBetweenCheck,
                                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                                     SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu4Vc1xSetup_ChannelListGet, SdhLineAu4Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc12Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                                     SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu4Vc1xSetup_ChannelListGet, SdhLineAu4Vc1xAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc3Cep)
            return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                                     SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu4Vc3Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                                      SetupOneVcTdmBert);
        else if (frame == cCliDiagVc4Cep)
                    return Bert_Flow_Execute(cAtTrue, checkNum, usSleepBetweenCheck,
                                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                                     SetupOneCepBasicPw, PwAlarmCheck,
                                                      StmAu4Vc4Setup_ChannelListGet, SdhLineAuVcAlarmCheck,
                                                      SetupOneVcTdmBert);
        else
            return cAtFalse;
    }

eBool Bert_StmVc1xDe1Loop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu3Vc1xDe1Setup_ChannelListGet, SdhLineAu3Vc3Vc1xDe1AlarmCheck,
                                                      SetupOneDe1TdmBert);
        else
            return Bert_Ds0_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     NULL, NULL,/*eth*/
                                     NULL, NULL,/*pw*/
                                     StmAu3Vc1xDe1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu3Vc3Vc1xDe1AlarmCheck,
                                     SetupOneNxDs0TdmBert);
    else /*Au4*/
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,
                                                      NULL, NULL,
                                                      StmAu4Vc1xDe1Setup_ChannelListGet, SdhLineAu4Vc1xDe1AlarmCheck,
                                                      SetupOneDe1TdmBert);
        else
            return Bert_Ds0_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     NULL, NULL,
                                     NULL, NULL,
                                     StmAu4Vc1xDe1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu4Vc1xDe1AlarmCheck,
                                     SetupOneNxDs0TdmBert);
    }

eBool Bert_StmVc3De3Loop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu3Vc3De3Setup_ChannelListGet, ShdLineAu3Vc3De3AlarmCheck,
                                                      SetupOneDe3TdmBert);
    else /*Au4*/
        return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,
                                                      NULL, NULL,
                                                      StmAu4Vc3De3Setup_ChannelListGet, SdhLineAu4Vc3De3AlarmCheck,
                                                      SetupOneDe3TdmBert);
    }

eBool Bert_StmVc3De3De1Loop_Flow_Execute(uint8 lineid, uint16 auType, uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_M13_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,/*eth*/
                                                      NULL, NULL,/*pw*/
                                                      StmAu3Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, SdhLineAu3Vc3De3De2De1AlarmCheck,
                                                      SetupOneDe1TdmBert);
        else
            return Bert_M13Ds0_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                         NULL, NULL,/*eth*/
                                         NULL, NULL,/*pw*/
                                         StmAu3Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu3Vc3De3De2De1AlarmCheck,
                                         SetupOneNxDs0TdmBert);

    else /*Au4*/
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_M13_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,
                                                      NULL, NULL,
                                                      StmAu4Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, SdhLineAu4Vc3De3De2De1AlarmCheck,
                                                      SetupOneDe1TdmBert);
        else
            return Bert_M13Ds0_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                                      NULL, NULL,
                                                      NULL, NULL,
                                                      StmAu4Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu4Vc3De3De2De1AlarmCheck,
                                                      SetupOneNxDs0TdmBert);
    }


eBool Bert_StmVc1xDe1PwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneSatopPw, PwAlarmCheck,
                                                      StmAu3Vc1xDe1Setup_ChannelListGet, SdhLineAu3Vc3Vc1xDe1AlarmCheck,
                                                      SetupOneDe1Bert);
        else
            return Bert_Ds0_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                     SetupOneCesopPw, PwAlarmCheck,
                                     StmAu3Vc1xDe1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu3Vc3Vc1xDe1AlarmCheck,
                                     SetupOneNxDs0Bert);
    else /*Au4*/
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneSatopPw, PwAlarmCheck,
                                                      StmAu4Vc1xDe1Setup_ChannelListGet, SdhLineAu4Vc1xDe1AlarmCheck,
                                                      SetupOneDe1Bert);
        else
            return Bert_Ds0_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                     SetupOne10GEthPort, EthPortAlarmCheck,
                                     SetupOneCesopPw, PwAlarmCheck,
                                     StmAu4Vc1xDe1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu4Vc1xDe1AlarmCheck,
                                     SetupOneNxDs0Bert);
    }

eBool Bert_StmVc3De3PwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneSatopPw, PwAlarmCheck,
                                                      StmAu3Vc3De3Setup_ChannelListGet, ShdLineAu3Vc3De3AlarmCheck,
                                                      SetupOneDe3Bert);
    else /*Au4*/
        return Bert_Flow_Execute(frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneSatopPw, PwAlarmCheck,
                                                      StmAu4Vc3De3Setup_ChannelListGet, SdhLineAu4Vc3De3AlarmCheck,
                                                      SetupOneDe3Bert);
    }

eBool Bert_StmVc3De3De1PwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck)
    {
    StmLineIdSet(lineid);
    if (auType == cCliDiagAu3)
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_M13_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneSatopPw, PwAlarmCheck,
                                                      StmAu3Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, SdhLineAu3Vc3De3De2De1AlarmCheck,
                                                      SetupOneDe1Bert);
        else
            return Bert_M13Ds0_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                         SetupOne10GEthPort, EthPortAlarmCheck,
                                         SetupOneCesopPw, PwAlarmCheck,
                                         StmAu3Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu3Vc3De3De2De1AlarmCheck,
                                         SetupOneNxDs0Bert);

    else /*Au4*/
        if (AtPdhDe1IsUnframeMode(frame))
            return Bert_M13_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneSatopPw, PwAlarmCheck,
                                                      StmAu4Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, SdhLineAu4Vc3De3De2De1AlarmCheck,
                                                      SetupOneDe1Bert);
        else
            return Bert_M13Ds0_Flow_Execute(de3frame, frame, checkNum, usSleepBetweenCheck,
                                                      SetupOne10GEthPort, EthPortAlarmCheck,
                                                      SetupOneCesopPw, PwAlarmCheck,
                                                      StmAu4Vc3De3Setup_ChannelListGet, De2De1Setup_ChannelListGet, GeneralDe1NxDs0Setup_ChannelListGet, SdhLineAu4Vc3De3De2De1AlarmCheck,
                                                      SetupOneNxDs0Bert);
    }

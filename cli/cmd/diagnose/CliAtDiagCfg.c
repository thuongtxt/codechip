/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : CliDiagCfg.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtDiagnose.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 stmLineId = 0;
static uint8 stmvcloopMode = cAtLoopbackModeLocal;
static uint8 stmpdhloopMode = cAtPdhLoopbackModeRelease;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint8 StmLineIdGet(void)
    {
    return stmLineId;
    }

void StmLineIdSet(uint8 lineId)
    {
    stmLineId = lineId;
    }

void StmVcLoopModeSet(uint8 loopmode)
    {
    stmvcloopMode = loopmode;
    }

void StmVcPdhLoopModeSet(uint8 loopmode)
    {
    stmpdhloopMode = loopmode;
    }

uint8 StmVcLoopModeGet(void)
    {
    return stmvcloopMode;
    }

uint8 StmVcPdhLoopModeGet(void)
    {
    return stmpdhloopMode;
    }

AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

eBool FrameSetThenCheck(AtPdhChannel channel, uint16 frame)
    {
    /*set/get frame type*/
    uint16 auditFrame;
    uint32 tempRet = AtPdhChannelFrameTypeSet(channel, frame);
    if (tempRet == cAtOk)
        {
        auditFrame = AtPdhChannelFrameTypeGet(channel);
        if (frame != auditFrame)
            {
            mChannelLogReturnFalse(cAtError, channel, "Frame type HW failure: set=%u, get=%u\n", frame, auditFrame);
            }
        }
    else
        {
        mChannelLogReturnFalse(cAtError, channel, "Set frame type in failure\n");
        }

    return cAtTrue;
    }

AtPdhNxDS0 NxDS0SetAndCheck(AtPdhDe1 de1, uint32 NxDs0Mask)
    {
    eAtRet retCode;
    AtPdhNxDS0 auditNxDs0 = NULL, nDs0 = AtPdhDe1NxDs0Create((AtPdhDe1)de1, NxDs0Mask);
    if (!nDs0)
        mChannelLogReturnNull(cAtError, de1, "Create DS0 %08X in failure\n", NxDs0Mask)
    else
        {
        auditNxDs0 = AtPdhDe1NxDs0Get((AtPdhDe1)de1, NxDs0Mask);
        if (auditNxDs0 != nDs0)
            {
            retCode = AtPdhDe1NxDs0Delete((AtPdhDe1)de1, nDs0);
            mChannelLogReturnNull(retCode, de1, "Delete NxDs0 %08X in failure\n", NxDs0Mask)
            }
        return nDs0;
        }
    }

eBool SetupOnePdhChannel(AtPdhChannel channel, uint16 frame, eAtPdhLoopbackMode loopMode)
    {
    eAtRet tempRet = cAtOk;

    /*set framer*/
    if (!FrameSetThenCheck(channel, frame))
        return cAtFalse;

    /*loopback: localline*/
    tempRet = AtChannelLoopbackSet((AtChannel)channel, loopMode);
    mChannelLogReturnFalse(tempRet, channel, "LocalLine loop is failed\n");

    return cAtTrue;
    }

eBool SetupOneEc1VcPdhChannel(AtPdhChannel channel, uint16 frame)
    {
    eAtRet tempRet = cAtOk;
    eAtPdhLoopbackMode loopMode = cAtPdhLoopbackModeRelease;
    /*set framer*/
    if (!FrameSetThenCheck(channel, frame))
        return cAtFalse;

    /*loopback: localline*/
    tempRet = AtChannelLoopbackSet((AtChannel)channel, loopMode);
    mChannelLogReturnFalse(tempRet, channel, "LocalLine loop is failed\n");

    return cAtTrue;
    }

eBool SetupOneStmVcPdhChannel(AtPdhChannel channel, uint16 frame)
    {
    eAtRet tempRet = cAtOk;
    eAtPdhLoopbackMode loopMode = StmVcPdhLoopModeGet();
    /*set framer*/
    if (!FrameSetThenCheck(channel, frame))
        return cAtFalse;

    /*loopback: localline*/
    tempRet = AtChannelLoopbackSet((AtChannel)channel, loopMode);
    mChannelLogReturnFalse(tempRet, channel, "LocalLine loop is failed\n");

    return cAtTrue;
    }

void SetupOnePwMpls(AtPw pw)
    {
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
    }

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
void SetupOnePwMac(AtPw pw, AtEthPort ethPort)
    {
    uint8 mac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA}; /* Application has to know */
    tAtEthVlanTag cVlan;

    AtEthVlanTagConstruct(0, 0, (uint16)AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, ethPort);
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    }

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
eBool SetupOneCreatedPw(AtPw pw, AtChannel channel, AtEthPort ethPort)
    {
    eAtRet ret = cAtOk;
    ret = AtPwCircuitBind(pw, channel);
    mChannelLogReturnFalse(ret, channel, "Bind channel to PW#%lu in failure\n", AtChannelIdGet((AtChannel)pw))
    SetupOnePwMpls(pw);
    SetupOnePwMac(pw, ethPort);
    ret = AtChannelEnable((AtChannel)pw, cAtTrue);
    mChannelLogReturnFalse(ret, channel, "Enable PW#%lu in failure\n", AtChannelIdGet((AtChannel)pw))

    return cAtTrue;
    }

AtPw SetupOneSatopPw(uint32 pwId, AtChannel channel, AtEthPort ethPort)
    {
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtPw pw;

    pw   = (AtPw)AtModulePwSAToPCreate(pwModule, pwId);
    if (!pw)
        {
        mDevLog("Creature PW#%lu in failure\n", pwId);
        return NULL;
        }
    else if (SetupOneCreatedPw(pw, channel, ethPort))
        return pw;
    else
        return NULL;
    }

AtPw SetupOneCesopPw(uint32 pwId, AtChannel channel, AtEthPort ethPort)
    {
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtPw pw;

    pw   = (AtPw)AtModulePwCESoPCreate(pwModule, pwId, cAtPwCESoPModeBasic);
    if (!pw)
        {
        mDevLog("Creature PW#%lu in failure\n", pwId);
        return NULL;
        }
    else if (SetupOneCreatedPw(pw, channel, ethPort))
        return pw;
    else
        return NULL;
    }

AtPw SetupOneCepBasicPw(uint32 pwId, AtChannel channel, AtEthPort ethPort)
    {
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtPw pw;

    pw   = (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);
    if (!pw)
        {
        mDevLog("Creature PW#%lu in failure\n", pwId);
        return NULL;
        }
    else if (SetupOneCreatedPw(pw, channel, ethPort))
        return pw;
    else
        return NULL;
    }

static AtEthPort SetupOneEthPort(uint32 portId, eAtEthPortInterface interface)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
    AtEthPort port = AtModuleEthPortGet(ethModule, (uint8)portId);
    eAtLoopbackMode loopMode = cAtLoopbackModeLocal;
    uint8 srcmac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    uint8 ipv4[] = {172, 33, 34, 35};
    eAtRet retCode = cAtOk;

    retCode = AtEthPortInterfaceSet(port, interface);
    mChannelLogReturnNull(retCode, port, "Set interface #%lu in failure\n", interface)
    /*
    {
    eAtEthPortInterface realInf = (interface != cAtEthPortInterfaceXGMii) ? cAtEthPortInterfaceSgmii : interface;
    eAtEthPortSpeed speed = (realInf == cAtEthPortInterfaceXGMii) ? cAtEthPortSpeed10G : cAtEthPortSpeed1000M;
    retCode = AtEthPortSpeedSet(port, speed);
    mChannelLogReturnFalse(retCode, port, "Set speed in failure\n")
    }
    */
    retCode = AtEthPortSourceMacAddressSet(port, srcmac);
    mChannelLogReturnNull(retCode, port, "Set SA in failure\n")
    retCode = AtEthPortIpV4AddressSet(port, ipv4);
    mChannelLogReturnNull(retCode, port, "Set port IP in failure\n")
    retCode = AtChannelLoopbackSet((AtChannel)port, loopMode);
    mChannelLogReturnNull(retCode, port, "Set port local loopback in failure\n")

    return port;
    }

AtEthPort SetupOne1GEthPort(uint32 portId)
    {
    return SetupOneEthPort(portId, cAtEthPortInterfaceSgmii);
    }

AtEthPort SetupOne10GEthPort(uint32 portId)
    {
    return SetupOneEthPort(portId, cAtEthPortInterfaceXGMii);
    }

eBool OneBertConfigure(AtPrbsEngine bert, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    eAtRet retCode = cAtOk;
    uint32 i, bertId = AtPrbsEngineIdGet(bert);

    AtChannel channel = AtPrbsEngineChannelGet(bert);

    retCode = AtPrbsEngineGeneratingSideSet(bert, genSide);
    mChannelLogReturnFalse(retCode, channel, "Side Gen bert#%lu in failure\n", bertId);
    retCode = AtPrbsEngineMonitoringSideSet(bert, monSide);
    mChannelLogReturnFalse(retCode, channel, "Side MON bert#%lu in failure\n", bertId);
    retCode = AtPrbsEngineEnable(bert, cAtTrue);
    mChannelLogReturnFalse(retCode, channel, "Enable bert#%lu in failure\n", bertId);
    AtPrbsEngineAlarmHistoryClear(bert);
    for (i = 0; i < cAtPrbsEngineCounterNum; i++)
        AtPrbsEngineCounterClear(bert, (uint16)i);

    return cAtTrue;
    }

AtPrbsEngine BertAtModulePrbsTdmEngineCreate(AtModulePrbs self, uint32 engineId, AtChannel tdmChannel, eBertChannelType type)
        {
        if (type == cBertChannelTypeNxDs0)
            return AtModulePrbsNxDs0PrbsEngineCreate(self, engineId, (AtPdhNxDS0)tdmChannel);
        else if (type == cBertChannelTypeDe1)
            return AtModulePrbsDe1PrbsEngineCreate(self, engineId, (AtPdhDe1)tdmChannel);
        else if (type == cBertChannelTypeDe3)
            return AtModulePrbsDe3PrbsEngineCreate(self, engineId, (AtPdhDe3)tdmChannel);
        else if (type == cBertChannelTypeVc)
            return AtModulePrbsSdhVcPrbsEngineCreate(self, engineId, (AtSdhChannel)tdmChannel);
        else
            {
            mChannelLogReturnNull(cAtError, tdmChannel, "Invalid bert channel type %u\n", type);
            return NULL;
            }
        }

AtPrbsEngine SetupOneTdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide,
                                      AtModulePrbsTdmEngineCreate_Func createFunc, eBertChannelType type)
    {
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
    AtPrbsEngine auditBert = NULL, bert = NULL;
    bert = createFunc(prbsModule, bertId, channel, type);
    if (!bert)
        {
        AtModulePrbsEngineDelete(prbsModule, bertId);
        bert = createFunc(prbsModule, bertId, channel, type);
        }

    auditBert = AtModulePrbsEngineGet(prbsModule, bertId);
    if (!bert)
        mChannelLogReturnNull(cAtError, channel, "Create bert#%lu in failure\n", bertId);
    if (auditBert != bert)
        {
        AtModulePrbsEngineDelete(prbsModule, bertId);
        mChannelLogReturnNull(cAtError, channel, "Audit bert#%lu in failure\n", bertId);
        }

    if (!OneBertConfigure(bert, genSide, monSide))
        mChannelLogReturnNull(cAtError, channel, "Side Gen bert#%lu in failure\n", bertId);

    return bert;
    }

AtPrbsEngine SetupOneDe3Bert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    return SetupOneTdmBert(bertId, channel, genSide, monSide, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeDe3);
    }

AtPrbsEngine SetupOneDe1Bert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    return SetupOneTdmBert(bertId, channel, genSide, monSide, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeDe1);
    }

AtPrbsEngine SetupOneVcBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    return SetupOneTdmBert(bertId, channel, genSide, monSide, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeVc);
    }

AtPrbsEngine SetupOneNxDs0Bert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {

    return SetupOneTdmBert(bertId, channel, genSide, monSide, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeNxDs0);
    }

AtPrbsEngine SetupOneDe3TdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    AtUnused(genSide);
    AtUnused(monSide);
    return SetupOneTdmBert(bertId, channel, cAtPrbsSideTdm, cAtPrbsSideTdm, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeDe3);
    }

AtPrbsEngine SetupOneDe1TdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    AtUnused(genSide);
    AtUnused(monSide);
    return SetupOneTdmBert(bertId, channel, cAtPrbsSideTdm, cAtPrbsSideTdm, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeDe1);
    }

AtPrbsEngine SetupOneVcTdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    AtUnused(genSide);
    AtUnused(monSide);
    return SetupOneTdmBert(bertId, channel, cAtPrbsSideTdm, cAtPrbsSideTdm, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeVc);
    }

AtPrbsEngine SetupOneNxDs0TdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide)
    {
    AtUnused(genSide);
    AtUnused(monSide);
    return SetupOneTdmBert(bertId, channel, cAtPrbsSideTdm, cAtPrbsSideTdm, BertAtModulePrbsTdmEngineCreate, cBertChannelTypeNxDs0);
    }

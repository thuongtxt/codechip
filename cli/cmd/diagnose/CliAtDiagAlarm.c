/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : CliAtDiagCommon.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtDiagnose.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool volatile alarmLogEnable = cAtTrue;
/*--------------------------- Forward declarations ---------------------------*/


/*--------------------------- Implementation ---------------------------------*/
void DiagAlarmLogEnable(void)
    {
    alarmLogEnable = cAtTrue;
    }

void DiagAlarmLogDisable(void)
    {
    alarmLogEnable = cAtFalse;
    }

eBool DiagAlarmLogIsEnabled(void)
    {
    return alarmLogEnable;
    }

eBool SdhLineAlarmCheck(AtChannel channel)
    {
    uint32 alarm = AtChannelAlarmGet(channel);
    uint32 counter;
    eBool ret = cAtTrue;
    if (alarm & cAtSdhLineAlarmLos)
        {
        mChannelLogError(channel, "LOS \n");
        ret = cAtFalse;
        }
    if (alarm & cAtSdhLineAlarmLof)
        {
        mChannelLogError(channel, "LOF \n");
        ret = cAtFalse;
        }
    if (alarm & cAtSdhLineAlarmAis)
        {
        mChannelLogError(channel, "AIS \n");
        ret = cAtFalse;
        }
    counter = AtChannelCounterGet(channel, cAtSdhLineCounterTypeB1);
    if (counter)
        {
        mChannelLogError(channel, "B1 counter = %lu \n", counter);
        ret = cAtFalse;
        }
    counter = AtChannelCounterGet(channel, cAtSdhLineCounterTypeB2);
    if (counter)
        {
        mChannelLogError(channel, "B2 counter = %lu \n", counter);
        ret = cAtFalse;
        }
    return ret;
    }

eBool AuTuAlarmCheck(AtChannel channel)
    {
    uint32 alarm = AtChannelAlarmGet(channel);
    eBool ret = cAtFalse;
    if (alarm & cAtSdhPathAlarmAis)
        {
        mChannelLogError(channel, "AIS \n");
        ret = cAtFalse;
        }
    if (alarm & cAtSdhPathAlarmLop)
        {
        mChannelLogError(channel, "LOP \n");
        ret = cAtFalse;
        }
    return ret;
    }

eBool VcAlarmCheck(AtChannel channel)
    {
    uint32 alarm = AtChannelAlarmGet(channel);
    eBool ret = cAtFalse;
    if (alarm & cAtSdhPathAlarmLom)
        {
        mChannelLogError(channel, "H4 LOM \n");
        ret = cAtFalse;
        }
    if (alarm & cAtSdhPathAlarmPayloadUneq)
        {
        mChannelLogError(channel, "UNEQ \n");
        ret = cAtFalse;
        }
    if (alarm & cAtSdhPathAlarmPlm)
        {
        mChannelLogError(channel, "PLM \n");
        ret = cAtFalse;
        }
    return ret;
    }

eBool De1AlarmCheck(AtChannel channel)
    {
    /*check alarm*/
    uint32 alarm = AtChannelAlarmGet(channel);
    eBool ret = cAtTrue;
    if (alarm & cAtPdhDe1AlarmLos)
        {
        mChannelLogError(channel, "LOS \n");
        ret = cAtFalse;
        }
    if (alarm & cAtPdhDe1AlarmAis)
        {
        mChannelLogError(channel, "AIS \n");
        ret = cAtFalse;
        }
    if (alarm & cAtPdhDe1AlarmLof)
        {
        mChannelLogError(channel, "LOF \n");
        ret = cAtFalse;
        }

    return ret;
    }

eBool De3AlarmCheck(AtChannel channel)
    {
    /*check alarm*/
    uint32 alarm = AtChannelAlarmGet(channel);
    eBool ret = cAtTrue;
    if (alarm & cAtPdhDe3AlarmLos)
        {
        mChannelLogError(channel, "LOS \n");
        ret = cAtFalse;
        }
    if (alarm & cAtPdhDe3AlarmAis)
        {
        mChannelLogError(channel, "AIS \n");
        ret = cAtFalse;
        }
    if (alarm & cAtPdhDe3AlarmLof)
        {
        mChannelLogError(channel, "LOF \n");
        ret = cAtFalse;
        }

    return ret;
    }


eBool SdhLineAuVcAlarmCheck(AtChannel channel)
    {
    /*check alarm*/
    eBool ret = cAtTrue;
    AtChannel au, stm;
    AtModule sdhModule = AtDeviceModuleGet(Device(), cAtModuleSdh);

    if (!VcAlarmCheck(channel))
        ret = cAtFalse;

    return ret;/*TODO: remove this line code once sdh line loop is ready*/

    au = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)channel);
    if (!au)
        {
        mChannelLog(channel, "AU is NULL \n");
        ret = cAtFalse;
        }
    else if (!AuTuAlarmCheck(au))
        ret = cAtFalse;

    stm = (AtChannel)AtModuleSdhLineGet((AtModuleSdh)sdhModule, AtSdhChannelLineGet((AtSdhChannel)au));
    if (!stm)
        {
        mChannelLog(channel, "STM LINE is NULL \n");
        ret = cAtFalse;
        }
    else if (!SdhLineAlarmCheck(stm))
        ret = cAtFalse;

    return ret;
    }

eBool De3ParentAlarmCheck(AtChannel channel)
    {
    /*check alarm*/
    eBool ret = cAtTrue;
    AtChannel vc3, au3, ec1line;
    AtModule sdhModule = AtDeviceModuleGet(Device(), cAtModuleSdh);

    vc3 = (AtChannel)AtPdhChannelVcGet((AtPdhChannel)channel);
    if (!vc3)
        {
        mChannelLog(channel, "VC3 is NULL \n");
        ret = cAtFalse;
        }
    else if (!VcAlarmCheck(vc3))
        ret = cAtFalse;

    return ret; /*TODO: remove this line code once SDH line loopback is ready*/

    au3 = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)vc3);
    if (!au3)
        {
        mChannelLog(channel, "AU3 is NULL \n");
        ret = cAtFalse;
        }
    else if (!AuTuAlarmCheck(au3))
        ret = cAtFalse;

    ec1line = (AtChannel)AtModuleSdhLineGet((AtModuleSdh)sdhModule, AtSdhChannelLineGet((AtSdhChannel)au3));
    if (!ec1line)
        {
        mChannelLog(channel, "SDH LINE is NULL \n");
        ret = cAtFalse;
        }
    else if (!SdhLineAlarmCheck(ec1line))
        ret = cAtFalse;

    return ret;
    }

eBool SdhLineAu3Vc3De3AlarmCheck(AtChannel channel)
    {
    /*check alarm*/
    eBool ret = cAtTrue;

    if (!De3AlarmCheck(channel))
        ret = cAtFalse;
    return ret; /*TODO: remove this line code once SDH is ready*/
    if (!De3ParentAlarmCheck(channel))
        ret = cAtFalse;

    return ret;
    }

eBool De2AlarmCheck(AtChannel channel)
    {
    uint32 alarm = AtChannelAlarmGet(channel);
    eBool ret = cAtTrue;

    if (alarm & cAtPdhDe2AlarmLof)
        {
        ret = cAtFalse;
        mChannelLogError(channel, "LOF\n");
        }
    if (alarm & cAtPdhDe2AlarmAis)
        {
        ret = cAtFalse;
        mChannelLogError(channel, "AIS\n");
        }

    return ret;
    }

eBool De2De1AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de3, de2, de1 = (AtPdhChannel)channel;
    eBool ret = cAtTrue;

    if (!De1AlarmCheck(channel))
        ret = cAtFalse;

    de2 = AtPdhChannelParentChannelGet(de1);
    if (!de2)
        {
        mChannelLogError(de1, "its DE2 is NULL\n");
        ret = cAtFalse;
        }
    else if (!De2AlarmCheck((AtChannel)de2))
        ret = cAtFalse;

    de3 = AtPdhChannelParentChannelGet(de2);
    if (!de3)
        {
        mChannelLogError(de2, "its DE3 is NULL\n");
        ret = cAtFalse;
        }
    else if (!De3AlarmCheck((AtChannel)de3))
        ret = cAtFalse;

    return ret;
    }

eBool SdhLineAu4Vc3De3De2De1AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de3, de2, de1 = (AtPdhChannel)channel;
    eBool ret = cAtTrue;

    if (!De1AlarmCheck(channel))
        ret = cAtFalse;

    de2 = AtPdhChannelParentChannelGet(de1);
    if (!de2)
        {
        mChannelLogError(de1, "its DE2 is NULL\n");
        ret = cAtFalse;
        }
    else if (!De2AlarmCheck((AtChannel)de2))
        ret = cAtFalse;

    de3 = AtPdhChannelParentChannelGet(de2);
    if (!de3)
        {
        mChannelLogError(de2, "its DE3 is NULL\n");
        ret = cAtFalse;
        }
    else if (!SdhLineAu4Vc3De3AlarmCheck((AtChannel)de3))
        ret = cAtFalse;

    return ret;
    }

eBool SdhLineAu3Vc3De3De2De1AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de3, de2, de1 = (AtPdhChannel)channel;
    eBool ret = cAtTrue;

    if (!De1AlarmCheck(channel))
        ret = cAtFalse;

    de2 = AtPdhChannelParentChannelGet(de1);
    if (!de2)
        {
        mChannelLogError(de1, "its DE2 is NULL\n");
        ret = cAtFalse;
        }
    else if (!De2AlarmCheck((AtChannel)de2))
        ret = cAtFalse;

    de3 = AtPdhChannelParentChannelGet(de2);
    if (!de3)
        {
        mChannelLogError(de2, "its DE3 is NULL\n");
        ret = cAtFalse;
        }
    else if (!SdhLineAu3Vc3De3AlarmCheck((AtChannel)de3))
        ret = cAtFalse;

    return ret;
    }

eBool SdhLineAu3Vc3Vc1xAlarmCheck(AtChannel channel)
    {
    AtSdhChannel vc3, tug2, tu1x;
    eBool ret = cAtTrue;

    ret = VcAlarmCheck(channel);
    tu1x = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
    if (!AuTuAlarmCheck((AtChannel)tu1x))
        ret = cAtFalse;
    tug2 = AtSdhChannelParentChannelGet((AtSdhChannel)tu1x);
    vc3 = AtSdhChannelParentChannelGet(tug2);
    if (!vc3)
        {
        mChannelLogError(channel, "its Vc3 is NULL\n");
        ret = cAtFalse;
        }
    else if (!SdhLineAuVcAlarmCheck((AtChannel)vc3))
        ret = cAtFalse;

    return ret;
    }

eBool SdhLineAu4Vc1xAlarmCheck(AtChannel channel)
    {
    AtSdhChannel vc4, tug3, tug2, tu1x;
    eBool ret = cAtTrue;

    ret = VcAlarmCheck(channel);
    tu1x = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
    if (!AuTuAlarmCheck((AtChannel)tu1x))
        ret = cAtFalse;
    tug2 = AtSdhChannelParentChannelGet(tu1x);
    tug3 = AtSdhChannelParentChannelGet(tug2);
    if (!tug3)
        {
        mChannelLogError(channel, "its tug3 is NULL\n");
        ret = cAtFalse;
        }
    else
        {
        vc4 = AtSdhChannelParentChannelGet(tug3);
        if (!vc4)
            {
            mChannelLogError(channel, "its VC4 is NULL\n");
            ret = cAtFalse;
            }
        else if (!SdhLineAuVcAlarmCheck((AtChannel)vc4))
            ret = cAtFalse;
        }

    return ret;
    }

eBool SdhLineAu4Vc3AlarmCheck(AtChannel channel)
    {
    AtSdhChannel vc4, tug3, tu3;
    eBool ret = cAtTrue;

    ret = VcAlarmCheck(channel);
    tu3 = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
    if (!AuTuAlarmCheck((AtChannel)tu3))
        ret = cAtFalse;
    tug3 = AtSdhChannelParentChannelGet(tu3);
    vc4 = AtSdhChannelParentChannelGet(tug3);
    if (!vc4)
        {
        mChannelLogError(channel, "its VC4 is NULL\n");
        ret = cAtFalse;
        }
    else if (!SdhLineAuVcAlarmCheck((AtChannel)vc4))
        ret = cAtFalse;

    return ret;
    }

eBool SdhLineAu3Vc3Vc1xDe1AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de1 = (AtPdhChannel)channel;
    AtChannel vc1x;
    eBool ret = cAtTrue;

    if (!De1AlarmCheck(channel))
        ret = cAtFalse;
    return ret; /*TODO: remove this line code once SDH is ready*/

    vc1x = (AtChannel)AtPdhChannelVcGet(de1);
    if (!vc1x)
        {
        mChannelLogError(de1, "its VC1x is NULL\n");
        ret = cAtFalse;
        }
    else
        {
        ret = SdhLineAu3Vc3Vc1xAlarmCheck(vc1x);
        }

    return ret;
    }

eBool SdhLineAu4Vc1xDe1AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de1 = (AtPdhChannel)channel;
    AtChannel vc1x;
    eBool ret = cAtTrue;

    if (!De1AlarmCheck(channel))
        ret = cAtFalse;
    return ret; /*TODO: remove this line code once SDH is ready*/

    vc1x = (AtChannel)AtPdhChannelVcGet(de1);
    if (!vc1x)
        {
        mChannelLogError(de1, "its VC1x is NULL\n");
        ret = cAtFalse;
        }
    else
        {
        ret = SdhLineAu4Vc1xAlarmCheck(vc1x);
        }

    return ret;
    }

eBool ShdLineAu3Vc3De3AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de = (AtPdhChannel)channel;
    AtChannel vc;
    eBool ret = cAtTrue;

    if (!De3AlarmCheck(channel))
        ret = cAtFalse;
    return ret; /*TODO: remove this line code once SDH is ready*/
    vc = (AtChannel)AtPdhChannelVcGet(de);
    if (!vc)
        {
        mChannelLogError(de, "its VC is NULL\n");
        ret = cAtFalse;
        }
    else
        {
        ret = SdhLineAuVcAlarmCheck(vc);
        }

    return ret;
    }

eBool SdhLineAu4Vc3De3AlarmCheck(AtChannel channel)
    {
    AtPdhChannel de = (AtPdhChannel)channel;
    AtChannel vc;
    eBool ret = cAtTrue;

    if (!De3AlarmCheck(channel))
        ret = cAtFalse;
    return ret; /*TODO: remove this line code once SDH is ready*/
    vc = (AtChannel)AtPdhChannelVcGet(de);
    if (!vc)
        {
        mChannelLogError(de, "its VC is NULL\n");
        ret = cAtFalse;
        }
    else
        {
        ret = SdhLineAu4Vc3AlarmCheck(vc);
        }

    return ret;
    }

eBool PwAlarmCheck(AtChannel channel)
    {
    AtPw pw = (AtPw)channel;
    AtPwCounters counters;
    AtPwTdmCounters tdmCounters;
    AtPwCESoPCounters cesopCounters;
    AtPwCepCounters cepCounters;
    eAtRet retCode;
    eAtPwType pwType = AtPwTypeGet(pw);
    uint32 singleCounter;
    eBool ret = cAtTrue;
    /* Read counters */
    retCode = AtChannelAllCountersClear((AtChannel)pw, &counters);
    mChannelLogReturnFalse(retCode, pw, "Get counters in failure\n");

    /* Common counters */
    singleCounter = AtPwCountersTxPacketsGet(counters);
    if (!singleCounter)
        {
        mChannelLogError(pw, "PW TX packet is zero\n");
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxPacketsGet(counters);
    if (!singleCounter)
        {
        mChannelLogError(pw, "PW RX packet is zero\n");
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxDiscardedPacketsGet(counters);
    if (singleCounter)
        {
        mChannelLogError(pw, "PW RX Discard packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxMalformedPacketsGet(counters);
    if (singleCounter)
        {
        mChannelLogError(pw, "PW RX Malformed packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxLostPacketsGet(counters);
    if (singleCounter)
        {
        mChannelLogError(pw, "PW RX Lost packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxOutOfSeqDropPacketsGet(counters);
    if (singleCounter)
        {
        mChannelLogError(pw, "PW RX OutOfSeqDrop packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxStrayPacketsGet(counters);
    if (singleCounter)
        {
        mChannelLogError(pw, "PW RX Stray packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwCountersRxOamPacketsGet(counters);

    /* TDM counters */
    tdmCounters = (AtPwTdmCounters)counters;
    singleCounter = AtPwTdmCountersTxLbitPacketsGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM TX Lbit packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwTdmCountersTxRbitPacketsGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM TX Rbit packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwTdmCountersRxLbitPacketsGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM RX Lbit packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwTdmCountersRxRbitPacketsGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM RX Rbit packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwTdmCountersRxJitBufOverrunGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM RX JitBuffer Overrun packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwTdmCountersRxJitBufUnderrunGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM RX JitBuffer Underrun packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    singleCounter = AtPwTdmCountersRxLopsGet(tdmCounters);
    if (singleCounter)
        {
        mChannelLogError(pw, "TDM RX LOPS packet is %lu\n", singleCounter);
        ret = cAtFalse;
        }

    /* CESoP counters */
    if (pwType == cAtPwTypeCESoP)
        {
        cesopCounters = (AtPwCESoPCounters)counters;
        singleCounter = AtPwCESoPCountersTxMbitPacketsGet(cesopCounters);
        if (singleCounter)
            {
            mChannelLogError(pw, "PW CESop TX Mbit packet is %lu\n", singleCounter);
            ret = cAtFalse;
            }

        singleCounter = AtPwCESoPCountersRxMbitPacketsGet(cesopCounters);
        if (singleCounter)
            {
            mChannelLogError(pw, "PW CESop RX Mbit packet is %lu\n", singleCounter);
            ret = cAtFalse;
            }
        }

    /* CEP counters */
    if (pwType == cAtPwTypeCEP)
        {
        cepCounters = (AtPwCepCounters)counters;
        singleCounter = AtPwCepCountersTxNbitPacketsGet(cepCounters);
        singleCounter = AtPwCepCountersTxPbitPacketsGet(cepCounters);
        singleCounter = AtPwCepCountersRxNbitPacketsGet(cepCounters);
        singleCounter = AtPwCepCountersRxPbitPacketsGet(cepCounters);
        }

    return ret;
    }


eBool EthPortAlarmCheck(AtChannel channel)
    {
    AtEthPort port = (AtEthPort)channel;
    uint32 counter;
    eBool ret = cAtTrue;

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterTxPackets);
    if (counter == 0)
        {
        mChannelLogError(port, "ETH TxPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxPackets);
    if (counter == 0)
        {
        mChannelLogError(port, "ETH RxPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxDiscardedPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxDiscardedPackets %lu", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrBusPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrBusPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrEfmOamPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrEfmOamPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrEthHdrPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrEthHdrPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrFcsPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrFcsPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrFcsPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrFcsPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrIpv4Packets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrIpv4Packets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrIpv6Packets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrIpv6Packets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrL2tpv3Packets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrL2tpv3Packets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrMefPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrMefPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrMplsPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrMplsPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrPausePackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrPausePackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrPsnPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrPsnPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrPwLabelPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrPwLabelPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxErrUdpPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrUdpPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxMplsErrOuterLblPackets);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxMplsErrOuterLblPackets %lu\n", counter);
        ret = cAtFalse;
        }

    counter = AtChannelCounterClear((AtChannel)port, cAtEthPortCounterRxPhysicalError);
    if (counter != 0)
        {
        mChannelLogError(port, "ETH RxErrPhysicalError %lu\n", counter);
        ret = cAtFalse;
        }

    return ret;
    }


eBool BertAlarmCheck(AtPrbsEngine bert)
    {
    uint32 counter, alarm = AtPrbsEngineAlarmGet(bert);
    eBool ret = cAtTrue;

    if (alarm & cAtPrbsEngineAlarmTypeError)
        {
        mChannelLogError(AtPrbsEngineChannelGet(bert), "BERT#%lu mon status: ERROR\n", AtPrbsEngineIdGet(bert));
        ret = cAtFalse;
        }
    if (alarm & cAtPrbsEngineAlarmTypeLossSync)
        {
        mChannelLogError(AtPrbsEngineChannelGet(bert), "BERT#%lu mon status: LOSSYNC\n", AtPrbsEngineIdGet(bert));
        ret = cAtFalse;
        }

    AtPrbsEngineAllCountersLatch(bert);

    counter = AtPrbsEngineCounterClear(bert, cAtPrbsEngineCounterRxBitError);
    if (counter)
        {
        mChannelLogError(AtPrbsEngineChannelGet(bert), "BERT#%lu mon status: RxBitError %lu\n", AtPrbsEngineIdGet(bert), counter);
        ret = cAtFalse;
        }

    counter = AtPrbsEngineCounterClear(bert, cAtPrbsEngineCounterRxBit);
    if (counter == 0)
        {
        mChannelLogError(AtPrbsEngineChannelGet(bert), "BERT#%lu mon status: RxBit %lu\n", AtPrbsEngineIdGet(bert), counter);
        ret = cAtFalse;
        }

    counter = AtPrbsEngineCounterClear(bert, cAtPrbsEngineCounterTxBit);
    if (counter == 0)
        {
        mChannelLogError(AtPrbsEngineChannelGet(bert), "BERT#%lu mon status: TxBit %lu\n", AtPrbsEngineIdGet(bert), counter);
        ret = cAtFalse;
        }

    counter = AtPrbsEngineCounterClear(bert, cAtPrbsEngineCounterRxSync);
    if (counter == 0)
        {
        mChannelLogError(AtPrbsEngineChannelGet(bert), "BERT#%lu mon status: TxBit %lu\n", AtPrbsEngineIdGet(bert), counter);
        ret = cAtFalse;
        }

    return ret;
    }


eBool GeneralSlidingTdmChannelsToBertsAndCheck(uint32 checkNum, uint32 usSleepBetweenCheck, AtChannel channels[], uint32 channelNum,
                                               ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                                               ChannelAlarmCheck_Func pwAlarmCheckFunc,
                                               ChannelAlarmCheck_Func ethAlarmCheckFunc,
                                               ChannelToBertSetup_Func bertSetupFunc)
    {
    AtChannel channel, startChannel, endChannel;
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(Device(), cAtModulePrbs);
    uint32 bertNum = AtModulePrbsMaxNumEngines(prbsModule);
    AtPrbsEngine bert;
    uint32 channelIdx, bertId, i, j, k, l, sliceNum, remainNum, pdvDelay;
    eBool result, ret = cAtTrue;
    AtPw pw;
    AtEthPort ethPort;
    char channelString1[64], channelString2[64];

    if (!bertNum)
        mDevLogReturnFalse(cAtError, "Devie has no BERT to perform");

    /*Sleep for 1 minute while jitterbuffer is centering*/
    if (pwAlarmCheckFunc)
        {
        AtPrintc(cSevInfo, "wating for 10 seconds for jitter buffer centering ...\n");
        AtOsalSleep(10);
        }

    sliceNum = channelNum / bertNum;
    remainNum = channelNum % bertNum;
    for (j = 0; j <= sliceNum; j++)
        {
        k = (j < sliceNum) ? bertNum : remainNum;
        /*setup bert*/
        for (i = 0; i < k; i++)
            {
            bertId = i;
            channelIdx = bertId + j*bertNum;
            channel = channels[channelIdx];
            bert = bertSetupFunc(bertId, channel, cAtPrbsSideTdm, cAtPrbsSidePsn);
            if (!bert)
                return cAtFalse;
            }
        /*sleep for awhile then flush all garbage status*/
        if (k)
            {
            AtOsalUSleep(2*250000);
            DiagAlarmLogDisable();
            mDevLog("***START log for clearing garbage status ...***\n");
            for (i = 0; i < k; i++)
                {
                bertId = i;
                channelIdx = bertId + j*bertNum;
                channel = channels[channelIdx];
                pw = AtChannelBoundPwGet(channel);
                ethPort = AtPwEthPortGet(pw);
                bert = AtModulePrbsEngineGet(prbsModule, bertId);
                /*clear history alarm/counter */
                BertAlarmCheck(bert);
                if (tdmAlarmCheckFunc)
                    tdmAlarmCheckFunc(channel);
                if (pwAlarmCheckFunc)
                    pwAlarmCheckFunc((AtChannel)pw);
                if (ethAlarmCheckFunc)
                    ethAlarmCheckFunc((AtChannel)ethPort);
                }
            mDevLog("***END log of clearing garbage status***\n");
            DiagAlarmLogEnable();
            }
        /*check alarm and counter here*/
        for (l = 0; l < checkNum; l++)
            {
            if (k)
                {
                AtOsalUSleep(usSleepBetweenCheck);
                startChannel = channels[j*bertNum];
                endChannel = channels[j*bertNum + k - 1];
                AtSprintf(channelString1, "%s.%s", AtChannelTypeString(startChannel), AtChannelIdString(startChannel));
                AtSprintf(channelString2, "%s.%s", AtChannelTypeString(endChannel), AtChannelIdString(endChannel));
                AtPrintc(cSevInfo, "BERT Checking time[%u]: channels[%s-%s]: ...", l+1, channelString1, channelString2);
                mDevLog("***START log for checking time[%u] for channels[%s-%s] ...***\n", l+1,  channelString1, channelString2);
                result = cAtTrue;
                for (i = 0; i < k; i++)
                    {
                    bertId = i;
                    channelIdx = bertId + j*bertNum;
                    channel = channels[channelIdx];
                    bert = AtModulePrbsEngineGet(prbsModule, bertId);
                    pw = AtChannelBoundPwGet(channel);
                    ethPort = AtPwEthPortGet(pw);
                    pdvDelay = AtPwJitterBufferDelayGet(pw);

                    if (pwAlarmCheckFunc)
                        AtOsalUSleep(2*pdvDelay);/*sleep to avoid TxCounter is zero in E1 satop*/
                    else
                        AtOsalUSleep(16000);

                    if (tdmAlarmCheckFunc && !tdmAlarmCheckFunc(channel))
                        result = cAtFalse;
                    if (!BertAlarmCheck(bert))
                        result = cAtFalse;
                    if (pwAlarmCheckFunc && !pwAlarmCheckFunc((AtChannel)pw))
                        result = cAtFalse;
                    if (ethAlarmCheckFunc && !ethAlarmCheckFunc((AtChannel)ethPort))
                        result = cAtFalse;
                    }
                mDevLog("***END log for checking time[%u] for channels[%s-%s] ...***\n", l+1,  channelString1, channelString2);
                if (result)
                    AtPrintc(cSevInfo, "[OK]\n");
                else
                    {
                    AtPrintc(cSevCritical, "[FAIL]\n");
                    ret = cAtFalse;
                    }
                }
            }
        }

    return ret;
    }

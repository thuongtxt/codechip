/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : CliAtDiagnose.h
 * 
 * Created Date: Jun 29, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_DIAGNOSE_CLIATDIAGNOSE_H_
#define _CLI_CMD_DIAGNOSE_CLIATDIAGNOSE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtPdhChannel.h"
#include "AtPdhNxDs0.h"
#include "AtPw.h"
#include "AtPwCounters.h"
#include "AtEthPort.h"
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"
#include "AtPdhDe2.h"
#include "AtPdhSerialLine.h"
#include "AtEthPort.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtSdhTug.h"
#include "AtSdhTu.h"
#include "AtSdhAug.h"
#include "AtModuleXc.h"
#include "AtCrossConnect.h"
#include "AtTextUI.h"
#include "AtCliModule.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mDevLog(...) {if (DiagAlarmLogIsEnabled()) AtDeviceLog(Device(), cAtLogLevelInfo, __FILE__, __LINE__, ## __VA_ARGS__);}
#define mDevLogError(...) {if (DiagAlarmLogIsEnabled()) AtDeviceLog(Device(), cAtLogLevelCritical, __FILE__, __LINE__, ## __VA_ARGS__);}
#define mDevLogReturnFalse(retCode, ...) {if (retCode != cAtOk) {if (DiagAlarmLogIsEnabled()) AtDeviceLog(Device(), cAtLogLevelCritical, __FILE__, __LINE__, ## __VA_ARGS__); return cAtFalse;}}
#define mDevLogReturnNull(retCode, ...) {if (retCode != cAtOk) {AtDeviceLog(Device(), cAtLogLevelCritical, __FILE__, __LINE__, ## __VA_ARGS__); return NULL;}}
#define mChannelLog(channel, ...) {if (DiagAlarmLogIsEnabled()) AtChannelLog((AtChannel)channel, cAtLogLevelInfo, __FILE__, __LINE__, ## __VA_ARGS__);}
#define mChannelLogError(channel, ...) {if (DiagAlarmLogIsEnabled()) AtChannelLog((AtChannel)channel, cAtLogLevelCritical, __FILE__, __LINE__, ## __VA_ARGS__);}
#define mChannelLogReturnFalse(retCode, channel, ...) {if (retCode != cAtOk) {if (DiagAlarmLogIsEnabled()) AtChannelLog((AtChannel)channel, cAtLogLevelCritical, __FILE__, __LINE__, ## __VA_ARGS__); return cAtFalse;}}
#define mChannelLogReturnNull(retCode, channel, ...) {if (retCode != cAtOk) {if (DiagAlarmLogIsEnabled()) AtChannelLog((AtChannel)channel, cAtLogLevelCritical, __FILE__, __LINE__, ## __VA_ARGS__); return NULL;}}

/*--------------------------- Typedefs ---------------------------------------*/
typedef AtEthPort (*SetupOneEthPort_Func)(uint32 portId);
typedef AtPw (*SetupOnePw_Func)(uint32 pwId, AtChannel channel, AtEthPort ethPort);
typedef eBool (*ChannelAlarmCheck_Func)(AtChannel channel);
typedef AtPrbsEngine (*ChannelToBertSetup_Func)(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
typedef AtChannel* (*NxDs0Setup_ChannelListGet_Func)(uint16 frame, AtChannel *de1List, uint32 numDe1, uint32 *chnNum);
typedef AtChannel* (*ChannelSetup_ChannelListGet_Func)(uint16 frame, uint32 *chnNum);
typedef AtChannel* (*De2De1ChannelSetup_ChannelListGet_Func)(uint16 frame, AtChannel *de3List, uint32 de3Num, uint32 *chnNum);
typedef eBool (*Bert_PdhLoop_ETHloop_Flow_Func)(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
typedef eBool (*Bert_M13Loop_ETHloop_Flow_Func)(uint16 de3frame, uint16 de1frame, uint32 checkNum, uint32 usSleepBetweenCheck);
typedef eAtModuleSdhRet (*DiagSdhChannel7xTug2MapTypeSet)(AtSdhChannel self);
typedef eBool (*Bert_StmVcLoop_Flow_Func)(uint8 stmlineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
typedef eBool (*Bert_StmVcDe3De1Loop_Flow_Func)(uint8 stmlineid, uint16 auType, uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
typedef eBool (*SetupOnePdhChannel_Func)(AtPdhChannel channel, uint16 frame);

typedef enum eBertChannelType{
        cBertChannelTypeNxDs0,
        cBertChannelTypeDe1,
        cBertChannelTypeDe3,
        cBertChannelTypeVc
        }eBertChannelType;
typedef AtPrbsEngine (*AtModulePrbsTdmEngineCreate_Func)(AtModulePrbs self, uint32 engineId, AtChannel tdmChannel, eBertChannelType type);

typedef enum eAtCepFrameType
    {
     cCliDiagVc11Cep = 0,
     cCliDiagVc12Cep = 1,
     cCliDiagVc3Cep =2,
     cCliDiagVc4Cep =3
    } eAtCepFrameType;

typedef enum eAtAuFrameType
    {
     cCliDiagAu4 = 0,
     cCliDiagAu3 = 1,
    } eAtAuFrameType;
/*--------------------------- Forward declarations ---------------------------*/
extern void AtDeviceLog(AtDevice self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...);
extern void AtChannelLog(AtChannel self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...);

/*--------------------------- Entries ----------------------------------------*/
void DiagAlarmLogEnable(void);
void DiagAlarmLogDisable(void);
eBool DiagAlarmLogIsEnabled(void);
uint8 StmLineIdGet(void);
void StmLineIdSet(uint8 lineId);
void StmVcLoopModeSet(uint8 loopmode);
uint8 StmVcLoopModeGet(void);
void StmVcPdhLoopModeSet(uint8 loopmode);
uint8 StmVcPdhLoopModeGet(void);

/* Alarm check functions */
eBool SdhLineAlarmCheck(AtChannel channel);
eBool AuTuAlarmCheck(AtChannel channel);
eBool VcAlarmCheck(AtChannel channel);
eBool De1AlarmCheck(AtChannel channel);
eBool De3AlarmCheck(AtChannel channel);
eBool De3ParentAlarmCheck(AtChannel channel);
eBool De2AlarmCheck(AtChannel channel);
eBool De2De1AlarmCheck(AtChannel channel);
eBool SdhLineAuVcAlarmCheck(AtChannel channel);
eBool SdhLineAu3Vc3Vc1xAlarmCheck(AtChannel channel);
eBool SdhLineAu3Vc3Vc1xDe1AlarmCheck(AtChannel channel);
eBool ShdLineAu3Vc3De3AlarmCheck(AtChannel channel);
eBool SdhLineAu3Vc3De3AlarmCheck(AtChannel channel);
eBool SdhLineAu3Vc3De3De2De1AlarmCheck(AtChannel channel);
eBool SdhLineAu4Vc1xAlarmCheck(AtChannel channel);
eBool SdhLineAu4Vc3AlarmCheck(AtChannel channel);
eBool SdhLineAu4Vc1xDe1AlarmCheck(AtChannel channel);
eBool SdhLineAu4Vc3De3AlarmCheck(AtChannel channel);
eBool SdhLineAu4Vc3De3De2De1AlarmCheck(AtChannel channel);
eBool PwAlarmCheck(AtChannel channel);
eBool EthPortAlarmCheck(AtChannel port);
eBool BertAlarmCheck(AtPrbsEngine bert);

/* Configuration functions */
AtDevice Device(void);
eBool FrameSetThenCheck(AtPdhChannel channel, uint16 frame);
AtPdhNxDS0 NxDS0SetAndCheck(AtPdhDe1 de1, uint32 NxDs0Mask);
eBool SetupOnePdhChannel(AtPdhChannel channel, uint16 frame, eAtPdhLoopbackMode loopMode);
eBool SetupOneEc1VcPdhChannel(AtPdhChannel channel, uint16 frame);
eBool SetupOneStmVcPdhChannel(AtPdhChannel channel, uint16 frame);
void SetupOnePwMpls(AtPw pw);
void SetupOnePwMac(AtPw pw, AtEthPort ethPort);
eBool SetupOneCreatedPw(AtPw pw, AtChannel channel, AtEthPort ethPort);
AtPw SetupOneSatopPw(uint32 pwId, AtChannel channel, AtEthPort ethPort);
AtPw SetupOneCesopPw(uint32 pwId, AtChannel channel, AtEthPort ethPort);
AtPw SetupOneCepBasicPw(uint32 pwId, AtChannel channel, AtEthPort ethPort);
AtEthPort SetupOne1GEthPort(uint32 portId);
AtEthPort SetupOne10GEthPort(uint32 portId);
eBool OneBertConfigure(AtPrbsEngine bert, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine BertAtModulePrbsTdmEngineCreate(AtModulePrbs self, uint32 engineId, AtChannel tdmChannel, eBertChannelType type);
AtPrbsEngine SetupOneTdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide,
                             AtModulePrbsTdmEngineCreate_Func createFunc, eBertChannelType type);
AtPrbsEngine SetupOneDe3Bert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneDe1Bert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneVcBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneNxDs0Bert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneDe3TdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneDe1TdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneVcTdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);
AtPrbsEngine SetupOneNxDs0TdmBert(uint32 bertId, AtChannel channel, eAtPrbsSide genSide, eAtPrbsSide monSide);

/* Product specific configuration */
void IdlePwPrint(uint32 i, uint32 maxI);
AtChannel* StmAu3Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu3Vc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum);
AtChannel* StmAu3Vc1xDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu3Vc1xDe1Ds0Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu3Vc3De3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu3Vc3De3De1Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum);
AtChannel* StmAu3Vc3De3De1Ds0Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc4Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum);
AtChannel* StmAu4Vc1xDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc1xDe1Ds0Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc3De3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc3De3De1Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum);
AtChannel* StmAu4Vc3De3De1Ds0Setup_ChannelListGet(uint16 de3frame, uint16 frame, uint32 *chnNum);
AtChannel* Ec1Vc3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* Ec1Vc1xSetup_ChannelListGet(uint16 isVc12, uint32 *chnNum);
AtChannel* Ec1De3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* Ec1Vc1xDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* LiuDe3Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* De2De1Setup_ChannelListGet(uint16 de1Frame, AtChannel *de3List, uint32 de3Num, uint32 *de1Num);
AtChannel* LiuDe1Setup_ChannelListGet(uint16 frame, uint32 *chnNum);
AtChannel* LiuDe1NxDs0Setup_ChannelListGet(uint16 frame, AtChannel *de1List, uint32 numDe1, uint32 *chnNum);

eBool GeneralSlidingTdmChannelsToBertsAndCheck(uint32 checkNum, uint32 usSleepBetweenCheck, AtChannel channels[], uint32 channelNum,
                                               ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                                               ChannelAlarmCheck_Func pwAlarmCheckFunc,
                                               ChannelAlarmCheck_Func ethAlarmCheckFunc,
                                               ChannelToBertSetup_Func bertSetupFunc);
eBool Bert_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func tdmListfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc);

eBool Bert_M13_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func de3channelListfunc,
                        De2De1ChannelSetup_ChannelListGet_Func tdmListfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc);

eBool Bert_Ds0_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func de1Listfunc,
                        NxDs0Setup_ChannelListGet_Func nxds0Listfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc);

eBool Bert_M13Ds0_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck,
                        SetupOneEthPort_Func setupEthFunc,
                        ChannelAlarmCheck_Func ethAlarmCheckFunc,
                        SetupOnePw_Func      setupPwFunc,
                        ChannelAlarmCheck_Func pwAlarmCheckFunc,
                        ChannelSetup_ChannelListGet_Func de3channelListfunc,
                        De2De1ChannelSetup_ChannelListGet_Func de1channelListfunc,
                        NxDs0Setup_ChannelListGet_Func nxds0Listfunc,
                        ChannelAlarmCheck_Func tdmAlarmCheckFunc,
                        ChannelToBertSetup_Func bertSetupFunc);

eBool Bert_LiuDe3Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_Ec1De3Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_LiuDe3Loop_M13satop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_Ec1De3Loop_M13satop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_LiuDe1Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_LiuDe1Loop_Cesop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_LiuDe3Loop_M13Cesop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_Ec1De3Loop_M13Cesop_ETHloop_Flow_Execute(uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_Ec1Vc1xDe1Loop_Satop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_Ec1Vc1xDe1Loop_Cesop_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_Ec1VcLoop_Cep_ETHloop_Flow_Execute(uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVcLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVc1xDe1Loop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVc3De3Loop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVc3De3De1Loop_Flow_Execute(uint8 lineid, uint16 auType, uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVc1xDe1PwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVc3De3PwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVc3De3De1PwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 de3frame, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);
eBool Bert_StmVcPwLoop_Flow_Execute(uint8 lineid, uint16 auType, uint16 frame, uint32 checkNum, uint32 usSleepBetweenCheck);

eBool T1_Loopcode_Diag_Execute(uint16 frame, AtChannel *de1Chns, uint32 numDe1Channels);
#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_DIAGNOSE_CLIATDIAGNOSE_H_ */


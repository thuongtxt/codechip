/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : CliAtMdio.c
 *
 * Created Date: Jul 12, 2015
 *
 * Description : CLIs to control XAUI read/write
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "AtCli.h"
#include "AtDevice.h"
#include "AtMdio.h"
#include "AtTokenizer.h"
#include "../physical/CliAtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtSerdesController m_CurrentSerdes = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern void AtCliRegisterPrettyPrint(uint32 value);

/*--------------------------- Implementation ---------------------------------*/
static AtMdio Mdio(AtSerdesController serdes)
    {
    return AtSerdesControllerMdio(serdes);
    }

static AtSerdesController DefaultSerdes(void)
    {
    return CliSerdesControllerFromId(0);
    }

eBool CmdAtMdioRead(char argc, char **argv)
    {
    uint32 address;
    uint32 page;
    uint32 value;
    eBool isPretty = cAtFalse;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(argv[0], ".");
    AtMdio mdio;

    if (AtTokenizerNumStrings(tokenizer) != 2)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid address format: <page>.<address>\r\n");
        return cAtFalse;
        }

    /* Address */
    page    = AtStrToDw(AtTokenizerNextString(tokenizer));
    address = AtStrToDw(AtTokenizerNextString(tokenizer));

    /* Port and pretty options */
    if (argc > 1)
        {
        uint32 argv_i;

        for (argv_i = 1; argv_i < (uint32)argc; argv_i++)
            {
            if (AtStrcmp(argv[argv_i], "pretty") == 0)
                isPretty = cAtTrue;
            else
                {
                m_CurrentSerdes = CliSerdesControllerFromString(argv[argv_i]);
                if (m_CurrentSerdes == NULL)
                    {
                    AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
                    return cAtFalse;
                    }
                }
            }
        }
    else
        {
        if (m_CurrentSerdes == NULL)
            m_CurrentSerdes = DefaultSerdes();

        AtPrintc(cSevInfo, "Selected serdes %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        }

    mdio = Mdio(m_CurrentSerdes);
    if (mdio == NULL)
        {
        AtPrintc(cSevCritical, "MDIO is not supported on SERDES %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        return cAtFalse;
        }

    /* Read and display */
    value = AtMdioRead(mdio, page, address);
    if (isPretty)
        AtCliRegisterPrettyPrint(value);
    AtPrintc(cSevNormal, "Read address: %u.%u, value: 0x%x\r\n", page, address, value);

    AtTextUIIntegerResultSet(AtCliSharedTextUI(), value);

    return cAtTrue;
    }

eBool CmdAtMdioWrite(char argc, char **argv)
    {
    uint32 address;
    uint32 page;
    uint32 value;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(argv[0], ".");
    AtMdio mdio;
    eAtRet ret;

    if (AtTokenizerNumStrings(tokenizer) != 2)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid address format: <page>.<address>\r\n");
        return cAtFalse;
        }

    /* Address and value */
    page    = AtStrToDw(AtTokenizerNextString(tokenizer));
    address = AtStrToDw(AtTokenizerNextString(tokenizer));
    value   = AtStrToDw(argv[1]);

    /* Port */
    if (argc > 2)
        {
        m_CurrentSerdes = CliSerdesControllerFromString(argv[2]);
        if (m_CurrentSerdes == NULL)
            {
            AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
            return cAtFalse;
            }
        }
    else
        {
        /* Try to choose valid port in case user did not input port ID */
        if (m_CurrentSerdes == NULL)
            m_CurrentSerdes = DefaultSerdes();

        AtPrintc(cSevInfo, "Selected serdes %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        }

    /* Port must be selected */
    mdio = Mdio(m_CurrentSerdes);
    if (mdio == NULL)
        {
        AtPrintc(cSevCritical, "MDIO is not supported on SERDES %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        return cAtFalse;
        }

    /* Write */
    ret = AtMdioWrite(mdio, page, address, value);
    if (ret == cAtOk)
        {
        AtPrintc(cSevNormal, "Write address: %u.%u, value: 0x%x\r\n", page, address, value);
        return cAtTrue;
        }

    AtPrintc(cSevCritical, "ERROR: MDIO write fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtMdioPortSelect(char argc, char **argv)
    {
    uint32 portId;
    AtMdio mdio;

    if (argc == 0)
        {
        AtPrintc(cSevNormal, "Selected port: ");
        if (m_CurrentSerdes == NULL)
            AtPrintc(cSevNormal, "none\r\n");
        else
            {
            portId = AtMdioSelectedPort(Mdio(m_CurrentSerdes));
            if (portId == cBit31_0)
                AtPrintc(cSevNormal, "none\r\n");
            else
                AtPrintc(cSevNormal, "%u\r\n", portId + 1);
            }

        return cAtTrue;
        }

    if (AtStrcmp(argv[0], "none") == 0)
        {
        if (m_CurrentSerdes == NULL)
            return cAtTrue;

        AtMdioPortDeselect(Mdio(m_CurrentSerdes));
        return cAtTrue;
        }

    m_CurrentSerdes = CliSerdesControllerFromString(argv[0]);
    if (m_CurrentSerdes == NULL)
        {
        AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
        return cAtFalse;
        }

    mdio = Mdio(m_CurrentSerdes);
    if (mdio == NULL)
        {
        AtPrintc(cSevCritical, "MDIO is not supported on SERDES %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        return cAtFalse;
        }

    AtMdioPortSelect(mdio, AtSerdesControllerIdGet(m_CurrentSerdes));
    return cAtTrue;
    }

eBool CmdAtMdioDebug(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtMdioDebug(Mdio(m_CurrentSerdes));
    return cAtTrue;
    }

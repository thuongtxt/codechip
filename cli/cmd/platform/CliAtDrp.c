/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : CliAtDrp.c
 *
 * Created Date: Oct 27, 2015
 *
 * Description : DRP HAL CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "AtDrp.h"
#include "AtCli.h"
#include "AtDevice.h"
#include "AtTokenizer.h"
#include "../physical/CliAtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static AtSerdesController m_CurrentSerdes = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void AtCliRegisterPrettyPrint(uint32 value);

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController DefaultSerdes(void)
    {
    return CliSerdesControllerFromId(0);
    }

static AtDrp Drp(AtSerdesController serdes)
    {
    return AtSerdesControllerDrp(serdes);
    }
    
eBool CmdAtDrpRead(char argc, char **argv)
    {
    uint32 address;
    uint32 value;
    eBool isPretty = cAtFalse;
    uint32 portId = 0;
    AtDrp drp;

    /* Address */
    address = AtStrToDw(argv[0]);

    /* Port and pretty options */
    if (argc > 1)
        {
        uint32 argv_i;

        for (argv_i = 1; argv_i < (uint32)argc; argv_i++)
            {
            if (AtStrcmp(argv[argv_i], "pretty") == 0)
                isPretty = cAtTrue;
            else
                {
                m_CurrentSerdes = CliSerdesControllerFromString(argv[argv_i]);
                if (m_CurrentSerdes == NULL)
                    {
                    AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
                    return cAtFalse;
                    }
                }
            }
        }
    else
        {
        /* Try to choose valid port in case user did not input port ID */
        if (m_CurrentSerdes == NULL)
            m_CurrentSerdes = DefaultSerdes();

        AtPrintc(cSevInfo, "Selected serdes %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        }

    drp = Drp(m_CurrentSerdes);
    if (drp == NULL)
        {
        AtPrintc(cSevCritical, "DRP of serdes %s does not exist\r\n", AtSerdesControllerDescription(m_CurrentSerdes));
        return cAtFalse;
        }

    /* Apply port */
    portId = AtSerdesControllerIdGet(m_CurrentSerdes);
    AtDrpPortSelect(drp, portId);

    /* Read and display */
    value = AtDrpRead(drp, address);
    if (isPretty)
        AtCliRegisterPrettyPrint(value);
    AtPrintc(cSevNormal, "Read address: 0x%x, value: 0x%x\r\n", address, value);

    AtTextUIIntegerResultSet(AtCliSharedTextUI(), value);

    return cAtTrue;
    }

eBool CmdAtDrpWrite(char argc, char **argv)
    {
    uint32 address;
    uint32 value;
    uint32 portId = 0;
    AtDrp drp;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Address and value */
    address = AtStrToDw(argv[0]);
    value   = AtStrToDw(argv[1]);

    /* Port */
    if (argc > 2)
        {
        m_CurrentSerdes = CliSerdesControllerFromString(argv[2]);
        if (m_CurrentSerdes == NULL)
            {
            AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
            return cAtFalse;
            }
        }

    else
        {
        /* Try to choose valid port in case user did not input port ID */
        if (m_CurrentSerdes == NULL)
            m_CurrentSerdes = DefaultSerdes();

        AtPrintc(cSevInfo, "Selected serdes %s\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        }

    drp = Drp(m_CurrentSerdes);
    if (drp == NULL)
        {
        AtPrintc(cSevCritical, "DRP of serdes %s does not exist\r\n", AtSerdesControllerDescription(m_CurrentSerdes));
        return cAtFalse;
        }

    /* Apply port */
    portId = AtSerdesControllerIdGet(m_CurrentSerdes);
    AtDrpPortSelect(drp, portId);

    /* Write */
    AtDrpWrite(drp, address, value);
    AtPrintc(cSevNormal, "Write address: 0x%x, value: 0x%x\r\n", address, value);

    return cAtTrue;
    }

eBool CmdAtDrpPortSelect(char argc, char **argv)
    {
    AtDrp drp;

    if (argc == 0)
        {
        AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
        return cAtFalse;
        }

    if (AtStrcmp(argv[0], "none") == 0)
        {
        if (m_CurrentSerdes == NULL)
            return cAtTrue;

        AtDrpPortDeselect(Drp(m_CurrentSerdes));
        return cAtTrue;
        }

    m_CurrentSerdes = CliSerdesControllerFromString(argv[0]);
    if (m_CurrentSerdes == NULL)
        {
        AtPrintc(cSevCritical, "Invalid port. Expected [1-%u]\r\n", CliMaxNumSerdesController());
        return cAtFalse;
        }
        
    drp = Drp(m_CurrentSerdes);
    if (drp == NULL)
        {
        AtPrintc(cSevCritical, "DRP of serdes %s does not exist\r\n", CliSerdesControllerDescriptionWithIdString(m_CurrentSerdes));
        return cAtFalse;
        }

    AtDrpPortSelect(drp, AtSerdesControllerIdGet(m_CurrentSerdes));
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Platform
 * 
 * File        : CliAtLiu.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : CLI CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATLIU_H_
#define _CLIATLIU_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtLiuManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void CliAtLiuManagerSet(AtLiuManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATLIU_H_ */


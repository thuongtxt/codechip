/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LIU
 *
 * File        : CliAtLiu.c
 *
 * Created Date: Jul 22, 2014
 *
 * Description : LIU CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtList.h"
#include "AtCli.h"
#include "AtLiuController.h"
#include "CliAtLiu.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtLiuManager m_manager = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtLiuManager LiuManager(void)
    {
    return m_manager;
    }

static AtLiuController LiuController(uint32 liuId)
	{
	return AtLiuManagerLiuControllerGet(LiuManager(), (uint16)liuId);
	}

static uint32 LoopBackModeFromString(const char *paramIdStr)
    {
	eAtLiuDe1LoopMode param;
    eBool convertSuccess = cAtTrue;
    static const char *cAtLiuDe1LoopModeStr[] = {"dual",
                                                 "analog",
                                                 "remote",
                                                 "digital",
                                                 "noloop"
                                                 };

    static const uint32  cAtLiuDe1LoopModeVal[]  = {cAtLiuDe1DualLoop,
                                                    cAtLiuDe1AnalogLoop,
                                                    cAtLiuDe1RemoteLoop,
                                                    cAtLiuDe1DigitalLoop,
                                                    cAtLiuDe1NoLoop
                                                    };

    /* Try to get predefined parameter IDs */
    param = CliStringToEnum(paramIdStr, cAtLiuDe1LoopModeStr, cAtLiuDe1LoopModeVal, mCount(cAtLiuDe1LoopModeVal), &convertSuccess);
    if (convertSuccess)
        return param;

    /* Other IDs */
    return AtStrToDw(paramIdStr);
    }

static uint32 LiuModeFromString(const char *paramIdStr)
    {
	eAtLiuMode param;
    eBool convertSuccess = cAtTrue;
    static const char *cAtLiuModeStr[] = {"e1", "ds1"};
    static const uint32 cAtLiuModeVal[] = {cAtLiuE1Mode, cAtLiuDs1Mode};

    /* Try to get predefined parameter IDs */
    param = CliStringToEnum(paramIdStr, cAtLiuModeStr, cAtLiuModeVal, mCount(cAtLiuModeVal), &convertSuccess);
    if (convertSuccess)
        return param;

    /* Other IDs */
    return AtStrToDw(paramIdStr);
    }

static AtList LiuControllerListFromString(char *liuIdListString)
    {
    AtList validLius;
    uint32 *idBuf;
    uint32 bufferSize, numLius, i;

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numLius = CliIdListFromString(liuIdListString, idBuf, bufferSize)) == 0)
        return NULL;

    validLius = AtListCreate(AtLiuManagerMaxNumOfLiusGet(LiuManager()));
    if (validLius == NULL)
        return NULL;

    for (i = 0; i < numLius; i++)
        {
        AtLiuController liuController;

        liuController = LiuController((uint16)(idBuf[i] - 1));
        if (liuController)
            AtListObjectAdd(validLius, (AtObject)liuController);
        }

    /* Return NULL if nothing in this list */
    if (AtListLengthGet(validLius) == 0)
        {
        AtObjectDelete((AtObject)validLius);
        return NULL;
        }

    return validLius;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    eAtEpRet ret = cAtEpRetOk;
    uint16 liuPortIndex, numLius;
    AtList liuControllerList = LiuControllerListFromString(argv[0]);

    AtUnused(argc);
    numLius = (uint16)AtListLengthGet(liuControllerList);

    for (liuPortIndex = 0; liuPortIndex < numLius; liuPortIndex++)
        ret |= AtLiuControllerEnable((AtLiuController)AtListObjectGet(liuControllerList, liuPortIndex), enable);

    AtObjectDelete((AtObject)liuControllerList);
    return (ret == cAtEpRetOk) ? cAtTrue : cAtFalse;
    }

eBool CmdAtLiuDe1LoopbackSet(char argc, char **argv)
	{
	eAtEpRet ret = cAtEpRetOk;
	uint16 liuPortIndex, numLius;
    AtList liuControllerList = LiuControllerListFromString(argv[0]);
	AtUnused(argc);
    numLius = (uint16)AtListLengthGet(liuControllerList);

    for (liuPortIndex = 0; liuPortIndex < numLius; liuPortIndex++)
    	ret |= AtLiuControllerLoopbackSet((AtLiuController)AtListObjectGet(liuControllerList, liuPortIndex), LoopBackModeFromString(argv[1]));

    AtObjectDelete((AtObject)liuControllerList);
	return (ret == cAtEpRetOk) ? cAtTrue : cAtFalse;
	}

eBool CmdAtLiuWrite(char argc, char **argv)
    {
    uint32 regAddr  = AtStrToDw(argv[0]);
    uint32 regVal   = AtStrToDw(argv[1]);
	AtUnused(argc);
    AtPrintc(cSevNormal, "Write to address 0x%X, value = 0x%X\r\n", regAddr, regVal);
    AtLiuManagerWrite(LiuManager(), regAddr, regVal);
    return cAtTrue;
    }

eBool CmdAtLiuRead(char argc, char **argv)
    {
    uint32 regAddr  = AtStrToDw(argv[0]);
    uint32 regVal = AtLiuManagerRead(LiuManager(), regAddr);
	AtUnused(argc);
    AtPrintc(cSevNormal, "Read address 0x%X, value = 0x%X\r\n", regAddr, regVal);
    return cAtTrue;
    }

eBool CmdAtLiuMode(char argc, char **argv)
	{
	eAtEpRet ret = AtLiuManagerLiuModeSet(LiuManager(), LiuModeFromString(argv[0]));
	AtUnused(argc);
	return (ret == cAtEpRetOk) ? cAtTrue : cAtFalse;
	}

void CliAtLiuManagerSet(AtLiuManager manager)
    {
    m_manager = manager;
    }

eBool CmdAtLiuDe1Enable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtLiuDe1Disable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

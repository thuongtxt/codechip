/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtHal.c
 *
 * Created Date: Aug 3, 2015
 *
 * Description : Debug HAL
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "../../../driver/src/generic/man/AtDeviceInternal.h"
#include "../../../platform/hal/AtHalInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IndirectAccessEnable(char argc, char **argv, eBool enabled)
    {
    AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), 0);
    AtUnused(argc);
    AtUnused(argv);
    AtHalIndirectAccessEnable(hal, enabled);
    return cAtTrue;
    }

eBool CmdAtHalIndirectAccessEnable(char argc, char **argv)
    {
    return IndirectAccessEnable(argc, argv, cAtTrue);
    }

eBool CmdAtHalIndirectAccessDisable(char argc, char **argv)
    {
    return IndirectAccessEnable(argc, argv, cAtFalse);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : atcliplatform.c
 *
 * Created Date: Nov 11, 2012
 *
 * Description : Platform CLI
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "AtDevice.h"
#include "AtDebug.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaskTokenStr          "bit"
#define cMaskStrMaxLen         10
#define cMaskStrMinLen         4
#define cAtLongRegMaxSize      8
#define cMaxNumRepeatTime      1000000
#define cMaxAcceptableAddress  cBit25_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLongMask
    {
    uint32 mask[cAtLongRegMaxSize];
    uint32 shift[cAtLongRegMaxSize];
    uint8 isFullMask;
    }tAtLongMask;

typedef struct tAtCompareDumpInfo
    {
    uint32 startAddr;
    uint32 count;
    uint32 step;
    } tAtCompareDumpInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void AtDebugCpuRest(void);

/*--------------------------- Implementation ---------------------------------*/
static eBool MaskIsValid(const char *pMaskStr)
    {
    char strTmp[16];

    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, pMaskStr, 3);

    if((AtStrcmp(strTmp, cMaskTokenStr) != 0) ||
       (AtStrlen(pMaskStr) < cMaskStrMinLen)  ||
       (AtStrlen(pMaskStr) > cMaskStrMaxLen))
        return cAtFalse;

    return cAtTrue;
    }

static eBool BitPositionIsValid(const char *pBitPosition, uint16 *bitPosition, eBool isInLongReg)
    {
    uint32 position;

    position = (uint16)AtStrtoul(pBitPosition, (char**)NULL, 10);
    if (((AtStrcmp(pBitPosition, "0") != 0) && (position == 0)))
        return cAtFalse;

    if ((!isInLongReg) && (position > 31))
        return cAtFalse;

    /* In case of long register */
    if (position > cAtLongRegMaxSize * 32 - 1)
        return cAtFalse;

    if (bitPosition)
        *bitPosition = (uint16)position;

    return cAtTrue;
    }

/* Input and output value only in uint32 */
static eBool StartStopBitIsValid(uint16 startBit, uint16 stopBit)
    {
    if (startBit > stopBit)
        return cAtFalse;

    if (stopBit - startBit > 32)
        return cAtFalse;

    return cAtTrue;
    }

/* Calculate mask and shift inside a dword */
static void DwMaskShiftCalculate(uint32 startInDw, uint32 stopInDw, uint32 *mask, uint32 *shift)
    {
    if (mask != NULL)
        *mask = (cBit31_0 << startInDw) & (cBit31_0 >> (31 - stopInDw));

    if (shift != NULL)
        *shift = startInDw;
    }

static eBool BitPositionsGet(const char *pMaskStr, uint16 *startBit, uint16 *stopBit, eBool isInLongReg)
    {
    char strTmp[16];
    char *pStartBit;
    char *pStopBit;

    if (!MaskIsValid(pMaskStr))
        return cAtFalse;

    /* Initialize */
    *startBit = 0;
    *stopBit  = 0;
    AtOsalMemInit(strTmp, 0, sizeof(strTmp));
    AtStrncpy(strTmp, &pMaskStr[3], cMaskStrMaxLen);
    strTmp[cMaskStrMaxLen + 1] = '\0';

    /* Start bit and stop bit exist */
    pStopBit = AtStrtok(strTmp, "_");
    if (pStopBit == NULL)
        return cAtTrue;

    /* Stop bit */
    if (!BitPositionIsValid(pStopBit, stopBit, isInLongReg))
        {
        AtPrintc(cSevCritical, "Invalid mask\r\n");
        return cAtFalse;
        }

    /* Start bit */
    *startBit = *stopBit;
    pStartBit = AtStrtok(NULL, "_");
    if(pStartBit != NULL)
        {
        if (!BitPositionIsValid(pStartBit, startBit, isInLongReg))
            {
            AtPrintc(cSevCritical, "Invalid mask\r\n");
            return cAtFalse;
            }
        }

    /* Check if user input correct bit positions */
    if (!StartStopBitIsValid(*startBit, *stopBit))
        {
        AtPrintc(cSevCritical, "Invalid mask\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
Function Name   :   AtMaskStrConvert

Description     :   Convert mask string (cbit0, cbit1, cbit31_0 ...)
                    to mask value and shift value

Inputs          :   pMaskStr        - Mask string.

Outputs         :   pMaskVal        - Mask value
                    pShiftVal       - Shift value

Return Code     :   True            - If convert successfully
                    False           - If convert fail
------------------------------------------------------------------------------*/
static eBool AtMaskStrConvert(const char* pMaskStr, uint32 *pMaskVal, uint32 *pShiftVal)
    {
    uint16 startBit, stopBit;

    if (!MaskIsValid(pMaskStr))
        return cAtFalse;

    if (!BitPositionsGet(pMaskStr, &startBit, &stopBit, cAtFalse))
        return cAtFalse;

    DwMaskShiftCalculate(startBit, stopBit, pMaskVal, pShiftVal);

    return cAtTrue;
    }

/* Calculate long mask from user input start bit and stop bit */
static eBool LongMaskCalculate(uint16 startBit, uint16 stopBit, tAtLongMask *longMask)
    {
    uint32 i, startInDw, stopInDw;
    uint32 startDw = startBit / 32;
    uint32 stopDw  = stopBit / 32;

    AtOsalMemInit(longMask, 0, sizeof(tAtLongMask));

    startInDw = startBit % 32;
    for (i = startDw; i <= stopDw; i++)
        {
        if (i == stopDw)
            stopInDw = stopBit % 32;
        else
            stopInDw = 31;

        DwMaskShiftCalculate(startInDw, stopInDw, &longMask->mask[i], &longMask->shift[i]);

        /* Next */
        startInDw = 0;
        }

    return cAtTrue;
    }

/* Get mask from input string */
static eBool AtLongMaskGet(const char* pMaskStr, tAtLongMask *longMask)
    {
    uint16 startBit, stopBit;

    if (!MaskIsValid(pMaskStr))
        return cAtFalse;

    if (!BitPositionsGet(pMaskStr, &startBit, &stopBit, cAtTrue))
        return cAtFalse;

    return LongMaskCalculate(startBit, stopBit, longMask);
    }

static char *CorrectDwordString(const char* str, char *correctDwordString)
    {
    uint32 stringLen;
    uint8 hexPrefixLen = 0;
    static const uint8 cNumCharsInDword = 8UL;
    uint32 maxLen;

    /* if value bigger to 32 bit trim value */
    stringLen = AtStrlen(str);
    if ((str[0] == '0') && (str[1] == 'x' || str[1] == 'X'))
        hexPrefixLen = 2;

    maxLen = (uint32)(cNumCharsInDword + hexPrefixLen);
    if (stringLen > maxLen)
        stringLen = maxLen;
    AtStrncpy(correctDwordString, str, stringLen);

    return correctDwordString;
    }

void AtCliRegisterPrettyPrint(uint32 value)
    {
    static const uint8 cNumBitsInDword = 32;
    uint32 shiftBit = cBit31;
    uint8 postion;

    /* Print bit positions */
    AtPrintc(cSevNormal, "%s", "Bit# ");
    for (postion = 0; postion < cNumBitsInDword; postion++)
        AtPrintc(cSevNormal, "%d ", (cNumBitsInDword - postion) - 1);
    AtPrintc(cSevNormal, "\r\n");

    /* Print bit values */
    AtPrintc(cSevNormal, "     ");
    for (postion = cNumBitsInDword; postion > 0; postion--)
        {
        uint8 bitVal = (uint8)((value & shiftBit) >> (postion - 1));
        AtPrintc(bitVal ? cSevInfo : cSevNormal, (postion > 10) ? "%d  " : "%d ", bitVal);
        shiftBit = shiftBit >> cBit0;
        }

    AtPrintc(cSevNormal, "\r\n");
    }

static uint8 CoreId(uint32 cliCoreId)
    {
    return (uint8)(cliCoreId - 1);
    }

/*-----------------------------------------------------------------------------
Function Name:  CmdAtRead

Description  :  Read value to register

Input        :  argc        - The number of argument.
                argv        - The array of argument.

Output       :  None

Return Code  :  cAtTrue     - If read successfully
                cAtFalse        - If read fail
------------------------------------------------------------------------------*/
static eBool CmdAtReadByFunc(char argc, char **argv, AtHal specificHal, uint32 (*Read)(AtHal self, uint32 address))
    {
    AtHal  hal = specificHal;
    uint8  coreId;
    uint32 address;
    uint32 maskValue;
    uint32 shiftValue;
    uint32 value;
    uint32 fieldValue;
    eBool  isPretty = cAtFalse;

    /* Initial value */
    address   = 0;
    coreId    = 1;
    
    /* Not enough parameter */
    if(argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    /* Too many parameters */
    if (argc > 4)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Parse parameter address from command */
    StrToDw(argv[0], 'h', &address);

    /* Parse Bit Mask. If cannot parse, try to parse IP core */
    maskValue  = cBit31_0;
    shiftValue = 0;
    if (argc > 1)
        {
        uint8 arg_i;

        for (arg_i = 1; arg_i < argc; arg_i++)
            {
            if (AtStrcmp(argv[arg_i], "pretty") == 0)
                isPretty = cAtTrue;
            else if (AtStrstr(argv[arg_i], cMaskTokenStr))
                AtMaskStrConvert(argv[arg_i], &maskValue, &shiftValue);
            else
                coreId = (uint8)AtStrToDw(argv[arg_i]);
            }
        }

    /* Get HAL */
    if (hal == NULL)
        hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No HAL\r\n");
        return cAtFalse;
        }

    if (address > AtHalMaxAddress(hal))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Address is out of range, maximum address is: 0x%08x\r\n",
                 AtHalMaxAddress(hal));
        return cAtFalse;
        }

    /* Show value from address */
    value = Read(hal, address);
    mFieldGet(value, maskValue, shiftValue, uint32, &fieldValue);
    if (isPretty)
        AtCliRegisterPrettyPrint(fieldValue);
    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", fieldValue);

    /* Update text UI for caching result. This result will be used by TCL script */
    AtTextUIIntegerResultSet(AtCliSharedTextUI(), value);

    return cAtTrue;
    }

eBool CmdAtRead(char argc, char **argv)
    {
    return CmdAtReadByFunc(argc, argv, NULL, AtHalRead);
    }

eBool CmdAtDirectRead(char argc, char **argv)
    {
    return CmdAtReadByFunc(argc, argv, NULL, AtHalDirectRead);
    }

/*-----------------------------------------------------------------------------
Function Name:  CmdAtWrite

Description  :  Write value to register

Input        :  argc        - The number of argument.
                argv        - The array of argument.

Output       :  None

Return Code  :  cAtTrue     - If read successfully
                cAtFalse        - If read fail
------------------------------------------------------------------------------*/
static eBool CmdAtWriteByFunc(char  argc, char  **argv,
                              uint32 (*Read)(AtHal self, uint32 address),
                              void (*Write)(AtHal self, uint32 address, uint32 value))
    {
    AtHal  hal;
    uint8  coreId;
    uint32 address;
    uint32 maskValue;
    uint32 shiftValue;
    uint32 value;
    uint32 regValue = 0;
    char  dwordStringValue[32];

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 4)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Get address */
    StrToDw(argv[0], 'h', &address);

    /* Value */
    AtOsalMemInit(dwordStringValue, 0, sizeof(dwordStringValue));
    StrToDw(CorrectDwordString(argv[1], dwordStringValue), 'h', &value);

    /* Parse Bit Mask */
    maskValue  = cBit31_0;
    shiftValue = 0;
    coreId     = 1;
    if (argc > 2)
        {
        if (!AtMaskStrConvert(argv[2], &maskValue, &shiftValue))
            coreId = (uint8)AtStrToDw(argv[2]);
        else if (argc > 3)
            coreId = (uint8)AtStrToDw(argv[3]);
        }

    /* HAL */
    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No HAL\r\n");
        return cAtFalse;
        }

    if (address > AtHalMaxAddress(hal))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Address is out of range, maximum address is: 0x%08x\r\n",
                 AtHalMaxAddress(hal));
        return cAtFalse;
        }

    /* Insert field and apply to hardware */
    if (maskValue != cBit31_0)
        regValue = Read(hal, address);
    mFieldIns(&regValue, maskValue, shiftValue, value);
    Write(hal, address, regValue);
    AtPrintc(cSevNormal, "Write address: 0x%x, value: 0x%x\r\n", address, regValue);

    return cAtTrue;
    }

eBool CmdAtWrite(char argc, char **argv)
    {
    return CmdAtWriteByFunc(argc, argv, AtHalRead, AtHalWrite);
    }

eBool CmdAtDirectWrite(char argc, char **argv)
    {
    return CmdAtWriteByFunc(argc, argv, AtHalDirectRead, AtHalDirectWrite);
    }

static void DefaultLongMaskMake(tAtLongMask *mask)
    {
    uint8 i;
    AtOsalMemInit(mask, 0, sizeof(tAtLongMask));
    for (i = 0; i < cAtLongRegMaxSize; i++)
        mask->mask[i] = cBit31_0;

    mask->isFullMask = cAtTrue;
    }

/* Extract value from value array by using long mask */
static uint32 ValueFromLongRegGet(const tAtLongMask *mask, const uint32 *longRegs, uint16 numRealDw)
    {
    uint32 value = 0;
    uint16 i, j;

    if (numRealDw > cAtLongRegMaxSize)
        numRealDw = cAtLongRegMaxSize;

    /* Maximum value lays on 2 dwords */
    /* Scan all mask to get which mask != 0 */
    for(i = 0; i < numRealDw; i++)
        {
        if (mask->mask[i] == 0)
            continue;

        value = (longRegs[i] & mask->mask[i]) >> mask->shift[i];

        if ((j = (uint16)(i + 1)) >= numRealDw) /* last dword */
            break;

        if (mask->mask[j] == 0) /* value lays on one dword, done */
            break;

        /* Value lays on 2 dwords */
        value |= (longRegs[j] & mask->mask[j]) << (32 - mask->shift[i]);
        break;
        }

    return value;
    }

/* Convert long register value to string */
static const char* LongRegValue2String(uint32 *longValue, tAtLongMask *longMask, uint16 numRealDwords)
    {
    static char longRegValueString[32];
    uint32 value;

    if (longMask->isFullMask)
        return AtDebugLongRegValue2String(longValue, (uint8)numRealDwords);

    value = ValueFromLongRegGet(longMask, longValue, numRealDwords);
    AtSprintf(longRegValueString, "0x%08X", value);

    return longRegValueString;
    }

static eBool LongRegPrint(uint32 address, uint8 coreId, tAtLongMask *mask,
                          uint16 (*LongReadFunc)(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId))
    {
    uint32 longValue[cAtLongRegMaxSize];
    uint16 numReadDwords;

    AtOsalMemInit(longValue, 0, sizeof(longValue));
    numReadDwords = LongReadFunc(CliDevice(), address, longValue, cAtLongRegMaxSize, CoreId(coreId));

    if (numReadDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\r\n");
        return cAtFalse;
        }

    AtPrintc(cSevNormal, "Read address 0x%08x: %s\r\n", address, LongRegValue2String(longValue, mask, numReadDwords));
    return cAtTrue;
    }

static eBool LongRegPrettyPrint(uint32 address, uint8 coreId, tAtLongMask *mask,
                                uint16 (*LongReadFunc)(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId))
    {
    uint32 longValue[cAtLongRegMaxSize];
    uint16 numReadDwords;
    uint32 value;

    AtOsalMemInit(longValue, 0, sizeof(longValue));
    numReadDwords = LongReadFunc(CliDevice(), address, longValue, cAtLongRegMaxSize, CoreId(coreId));
    if (numReadDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\r\n");
        return cAtFalse;
        }

    if (mask->isFullMask)
        return AtRegisterPrettyLongRead(longValue, numReadDwords);

    value = ValueFromLongRegGet(mask, longValue, numReadDwords);
    AtCliRegisterPrettyPrint(value);
    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", value);

    return cAtTrue;
    }

static eBool LongRead(char argc, char **argv, uint16 (*LongReadFunc)(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId))
    {
    uint32 addr;
    uint8 coreId;
    tAtLongMask longMask;
    eBool isPretty = cAtFalse;

    coreId = 1;
    if(argc < 1 )
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 4)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Convert Mask Shift */
    DefaultLongMaskMake(&longMask);
    if (argc > 1)
        {
        uint8 arg_i;

        for (arg_i = 1; arg_i < argc; arg_i++)
            {
            if (AtStrcmp(argv[arg_i], "pretty") == 0)
                isPretty = cAtTrue;
            else if (AtStrstr(argv[arg_i], cMaskTokenStr))
                AtLongMaskGet(argv[arg_i], &longMask);
            else
                coreId = (uint8)AtStrToDw(argv[arg_i]);
            }
        }

    StrToDw(argv[0], 'h', &addr);

    if (isPretty)
        return LongRegPrettyPrint(addr, coreId, &longMask, LongReadFunc);
    else
        return LongRegPrint(addr, coreId, &longMask, LongReadFunc);
    }

static eBool DdrOffloadRegPrint(uint32 subDeviceId, uint32 address, uint8 coreId, tAtLongMask *mask,
                          uint16 (*DdrOffloadReadFunc)(AtDevice self, uint32 subDeviceId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId))
    {
    uint32 longValue[cAtLongRegMaxSize];
    uint16 numReadDwords;

    AtOsalMemInit(longValue, 0, sizeof(longValue));
    numReadDwords = DdrOffloadReadFunc(CliDevice(), subDeviceId, address, longValue, cAtLongRegMaxSize, CoreId(coreId));

    if (numReadDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\r\n");
        return cAtFalse;
        }

    AtPrintc(cSevNormal, "Read address 0x%08x: %s\r\n", address, LongRegValue2String(longValue, mask, numReadDwords));
    return cAtTrue;
    }

static eBool DdrOffloadRegPrettyPrint(uint32 subDeviceId, uint32 address, uint8 coreId, tAtLongMask *mask,
                                uint16 (*DdrOffloadReadFunc)(AtDevice self, uint32 subDeviceId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId))
    {
    uint32 longValue[cAtLongRegMaxSize];
    uint16 numReadDwords;
    uint32 value;

    AtOsalMemInit(longValue, 0, sizeof(longValue));
    numReadDwords = DdrOffloadReadFunc(CliDevice(), subDeviceId, address, longValue, cAtLongRegMaxSize, CoreId(coreId));
    if (numReadDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\r\n");
        return cAtFalse;
        }

    if (mask->isFullMask)
        return AtRegisterPrettyLongRead(longValue, numReadDwords);

    value = ValueFromLongRegGet(mask, longValue, numReadDwords);
    AtCliRegisterPrettyPrint(value);
    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", value);

    return cAtTrue;
    }

static eBool DdrOffloadRead(char argc, char **argv, uint16 (*DdrOffloadReadFunc)(AtDevice self, uint32 subDeviceId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId))
    {
    uint32 addr;
    uint8 coreId;
    tAtLongMask longMask;
    eBool isPretty = cAtFalse;
    uint32 subDeviceId = 0;
    AtDevice realDevice = NULL;
    uint8 Id, numSubDevice = 0;

    coreId = 1;
    if(argc < 2 )
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 5)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    Id = (uint8)AtStrToDw(argv[0]);
    realDevice = CliRealDevice();
    numSubDevice = AtDeviceNumSubDeviceGet(realDevice);
    if (Id == 0 || Id > numSubDevice)
        {
        AtPrintc(cSevCritical, "ERROR: Sub device ID is from 1 to %d\r\n", numSubDevice);
        return cAtFalse;
        }
    subDeviceId = (uint32)DeviceIdFromCmdParamId((uint8)Id);

    /* Convert Mask Shift */
    DefaultLongMaskMake(&longMask);
    if (argc > 2)
        {
        uint8 arg_i;

        for (arg_i = 2; arg_i < argc; arg_i++)
            {
            if (AtStrcmp(argv[arg_i], "pretty") == 0)
                isPretty = cAtTrue;
            else if (AtStrstr(argv[arg_i], cMaskTokenStr))
                AtLongMaskGet(argv[arg_i], &longMask);
            else
                coreId = (uint8)AtStrToDw(argv[arg_i]);
            }
        }

    StrToDw(argv[1], 'h', &addr);

    if (isPretty)
        return DdrOffloadRegPrettyPrint(subDeviceId, addr, coreId, &longMask, DdrOffloadReadFunc);
    else
        return DdrOffloadRegPrint(subDeviceId, addr, coreId, &longMask, DdrOffloadReadFunc);
    }

static uint32 CorrectAddressRange(char *startAddressStr, char *stopAddressStr,
                                  uint32 *startAddress, uint32 *stopAddress)
    {
    uint32 numRegisters;
    static const uint32 cMaxNumRegisters = 4096;

    *startAddress = 0;
    *stopAddress  = 0;

    /* Parse parameter address from command */
    StrToDw(startAddressStr, 'h', startAddress);
    StrToDw(stopAddressStr, 'h', stopAddress);
    if ((*startAddress) > (*stopAddress))
        return 0;

    numRegisters = ((*stopAddress) - (*startAddress)) + 1;
    if (numRegisters > cMaxNumRegisters)
        {
        numRegisters = cMaxNumRegisters;
        *stopAddress = *startAddress + numRegisters - 1;
        }

    return numRegisters;
    }

eBool CmdAtLongRegRead(char argc, char **argv)
    {
    return LongRead(argc, argv, AtDeviceLongReadOnCore);
    }

eBool CmdAtLongDump(char argc, char **argv)
    {
    uint32 fromAddr, toAddr;
    tAtLongMask  longMask;
    uint8 coreId = 1;
    eBool success = cAtTrue;

    if(argc < 1 )
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 4)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Convert Mask Shift */
    if (argc > 2)
        {
        if (!AtLongMaskGet(argv[2], &longMask))
            {
            DefaultLongMaskMake(&longMask);
            coreId = (uint8)AtStrToDw(argv[2]);
            }
        else if (argc > 3)
            coreId = (uint8)AtStrToDw(argv[3]);
        }
    else
        DefaultLongMaskMake(&longMask);

    /* Parse parameter address from command */
    if (CorrectAddressRange(argv[0], argv[1], &fromAddr, &toAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    while (fromAddr <= toAddr)
        {
        success |= LongRegPrint(fromAddr, coreId, &longMask, AtDeviceLongReadOnCore);
        if (fromAddr == cBit31_0)
            break;

        fromAddr++;
        }

    return success;
    }

/* Insert dword value to long registers array with corresponding mask */
static void LongRegValueInsert(tAtLongMask *mask, uint32 *longRegs, uint32 value, uint16 numRealDw)
    {
    uint16 i, j;

    if (numRealDw > cAtLongRegMaxSize)
        numRealDw = cAtLongRegMaxSize;

    for (i = 0; i < numRealDw; i++)
        {
        if (mask->mask[i] == 0)
            continue;

        mFieldIns(&longRegs[i], mask->mask[i], mask->shift[i], value);

        if ((j = (uint16)(i + 1)) >= numRealDw) /* Last dword */
            return;

        if (mask->mask[j] == 0) /* value lays on one dword, done */
            return;

        value >>= (32 - mask->shift[i]);
        mFieldIns(&longRegs[j], mask->mask[j], mask->shift[j], value);
        }
    }

/* Write one long register */
static eBool OneLongRegWrite(uint32 address, uint8 coreId, tAtLongMask *longMask, uint32 *values, eBool verbose)
    {
    uint16 numWrittenDwords;
    uint32 readValue[cAtLongRegMaxSize];

    uint32 *arrayToWrite = values;

    /* If write with mask, read registers, insert value and write back */
    if (!longMask->isFullMask)
        {
        numWrittenDwords = AtDeviceLongReadOnCore(CliDevice(), address, readValue, cAtLongRegMaxSize, CoreId(coreId));
        if (numWrittenDwords == 0)
            {
            AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\r\n");
            return cAtFalse;
            }

        LongRegValueInsert(longMask, readValue, values[0], numWrittenDwords);
        arrayToWrite = readValue;
        }

    numWrittenDwords = AtDeviceLongWriteOnCore(CliDevice(), address, arrayToWrite, cAtLongRegMaxSize, CoreId(coreId));
    if (numWrittenDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Can't write! This is not a long register or it is unknown\r\n");
        return cAtFalse;
        }

    if (verbose)
        AtPrintc(cSevNormal, "Write address: 0x%08x, value: %s\r\n", address, LongRegValue2String(arrayToWrite, longMask, numWrittenDwords));
    return cAtTrue;
    }

/* Write one DDR offload register */
static eBool OneDdrOffloadRegWrite(uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, uint8 coreId, tAtLongMask *longMask, uint32 *values, eBool verbose)
    {
    uint16 numWrittenDwords;
    uint32 readValue[cAtLongRegMaxSize];

    uint32 *arrayToWrite = values;

    /* If write with mask, read registers, insert value and write back */
    if (!longMask->isFullMask)
        {
        numWrittenDwords = AtDeviceReadOnDdrOffload(CliDevice(), subDeviceId, ddrAddress, readValue, cAtLongRegMaxSize, CoreId(coreId));
        if (numWrittenDwords == 0)
            {
            AtPrintc(cSevCritical, "ERROR: This is not a ddr offload register or it is unknown\r\n");
            return cAtFalse;
            }

        LongRegValueInsert(longMask, readValue, values[0], numWrittenDwords);
        arrayToWrite = readValue;
        }

    numWrittenDwords = AtDeviceWriteOnDdrOffload(CliDevice(), subDeviceId, ddrAddress, regAddress, arrayToWrite, cAtLongRegMaxSize, CoreId(coreId));
    if (numWrittenDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Can't write! This is not a ddr offload register or it is unknown\r\n");
        return cAtFalse;
        }

    if (verbose)
        AtPrintc(cSevNormal, "Write address: 0x%08x, value: %s\r\n", ddrAddress, LongRegValue2String(arrayToWrite, longMask, numWrittenDwords));
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
Function Name:  CmdAtLongRegWrite

Description  :  Write value to register

Input        :  argc        - The number of argument.
                argv        - The array of argument.

Output       :  None

Return Code  :  cAtTrue     - If read successfully
                cAtFalse    - If read fail
------------------------------------------------------------------------------*/
eBool CmdAtLongRegWrite(char argc, char** argv)
    {
    uint32            addr;
    uint32            pValue[cAtLongRegMaxSize];
    uint32            dDwNum;
    tAtLongMask       longMask;
    uint8             coreId;

    dDwNum     = 0;
    coreId     = 1; /* Default for core ID = 1 in this card */
    addr       = 0;
    AtOsalMemInit(pValue, 0, sizeof(pValue));

    if (argc < 2 )
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 4 )
        {
        AtPrintc(cSevCritical, "To much parameters\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        {
        if (!AtLongMaskGet(argv[2], &longMask))
            {
            DefaultLongMaskMake(&longMask);
            coreId = (uint8)AtStrToDw(argv[2]);
            }
        else if (argc > 3)
            coreId = (uint8)AtStrToDw(argv[3]);
        }
    else
        DefaultLongMaskMake(&longMask);

    StrToDw(argv[0], 'h', &addr);

    if (longMask.isFullMask)
        {
        if (AtStrToArrayDw(argv[1], pValue, &dDwNum) == cAtFalse )
            {
            AtPrintc(cSevCritical, "Invalid value\r\n");
            return cAtFalse;
            }
        }
    else
        StrToDw(argv[1], 'h', &(pValue[0]));

    return OneLongRegWrite(addr, coreId, &longMask, pValue, cAtTrue /* Verbose */);
    }

static eBool AddressIsValid(uint32 address)
    {
    return ((address <= cMaxAcceptableAddress) ? cAtTrue : cAtFalse);
    }

eBool CmdAtLongFill(char argc, char** argv)
    {
    uint32            fromAddr, toAddr;
    uint32            pValue[cAtLongRegMaxSize];
    uint32            dDwNum;
    tAtLongMask       longMask;
    uint8             coreId;
    eBool             ret = cAtTrue;

    dDwNum     = 0;
    coreId     = 1; /* Default for core ID = 1 in this card */
    AtOsalMemInit(pValue, 0, sizeof(pValue));

    if(argc < 3)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if(argc > 5)
        {
        AtPrintc(cSevCritical, "To much parameters\r\n");
        return cAtFalse;
        }

    if (argc > 3)
        {
        if (!AtLongMaskGet(argv[3], &longMask))
            {
            DefaultLongMaskMake(&longMask);
            coreId = (uint8)AtStrToDw(argv[3]);
            }
        else if (argc > 4)
            coreId = (uint8)AtStrToDw(argv[4]);
        }
    else
        DefaultLongMaskMake(&longMask);

    /* Get addresses and value */
    if (CorrectAddressRange(argv[0], argv[1], &fromAddr, &toAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    if ((!AddressIsValid(fromAddr)) || (!AddressIsValid(toAddr)))
        {
        AtPrintc(cSevWarning, "WARNING: Invalid address\r\n");
        return cAtTrue;
        }

    if (longMask.isFullMask)
        {
        if (AtStrToArrayDw(argv[2], pValue, &dDwNum) == cAtFalse )
            {
            AtPrintc(cSevCritical, "Invalid value\r\n");
            return cAtFalse;
            }
        }
    else
        StrToDw(argv[2], 'h', &(pValue[0]));

    while (fromAddr <= toAddr)
        {
        ret |= OneLongRegWrite(fromAddr, coreId, &longMask, pValue, cAtFalse /* No verbosed */);
        fromAddr++;
        }

    return ret;
    }

eBool CmdAtDump(char argc, char **argv)
    {
    AtHal  hal;
    uint8  coreId = 1;
    uint32 fromAddr = 0;
    uint32 toAddr = 0;
    uint32 maskValue = 0xffffffff;
    uint32 shiftValue = 0;
    eBool isPretty = cAtFalse;

    if(argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if(argc > 5)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Parse Bit Mask */
    if (argc > 2)
        {
        uint8 arg_i;

        for (arg_i = 2; arg_i < argc; arg_i++)
            {
            if (AtStrcmp(argv[arg_i], "pretty") == 0)
                isPretty = cAtTrue;
            else if (AtStrstr(argv[arg_i], cMaskTokenStr))
                AtMaskStrConvert(argv[arg_i], &maskValue, &shiftValue);
            else
                coreId = (uint8)AtStrToDw(argv[arg_i]);
            }
        }

    /* Parse parameter address from command */
    if (CorrectAddressRange(argv[0], argv[1], &fromAddr, &toAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        return cAtFalse;

    if ((fromAddr > AtHalMaxAddress(hal)) ||
        (toAddr   > AtHalMaxAddress(hal)))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Address is out of range, maximum address is: 0x%08x\r\n",
                 AtHalMaxAddress(hal));
        return cAtFalse;
        }

    if (isPretty)
        AtRegisterPrettyDump(hal, fromAddr, toAddr, maskValue, shiftValue);
    else
        AtDumpByHal(hal, fromAddr, toAddr, maskValue, shiftValue);

    return cAtTrue;
    }

eBool CmdAtFill(char argc, char **argv)
    {
    eBool  ret = cAtTrue;
    AtHal  hal;
    uint8  coreId = 1;
    uint32 address = 0;
    uint32 fromAddr = 0;
    uint32 toAddr = 0;
    uint32 maskValue = 0xffffffff;
    uint32 shiftValue = 0;
    uint32 value, regValue;
    char dwordStringValue[32];

    if(argc < 3)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if(argc > 5)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Get addresses */
    if (CorrectAddressRange(argv[0], argv[1], &fromAddr, &toAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    if ((!AddressIsValid(fromAddr)) || (!AddressIsValid(toAddr)))
        {
        AtPrintc(cSevWarning, "WARNING: Invalid address\r\n");
        return cAtTrue;
        }

    /* Values */
    AtOsalMemInit(dwordStringValue, 0, sizeof(dwordStringValue));
    StrToDw(CorrectDwordString(argv[2], dwordStringValue), 'h', &value);

    /* Parse Bit Mask */
    if (argc > 3)
        {
        /* Try to get bit mast, if cannot get it, consider it as IP Core ID */
        if (!AtMaskStrConvert(argv[3], &maskValue, &shiftValue))
            coreId = (uint8)AtStrToDw(argv[3]);
        else if (argc > 4)
            coreId = (uint8)AtStrToDw(argv[4]);
        }

    /* HAL */
    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No HAL\r\n");
        return cAtFalse;
        }

    if ((fromAddr > AtHalMaxAddress(hal)) ||
        (toAddr   > AtHalMaxAddress(hal)))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Address is out of range, maximum address is: 0x%08x\r\n",
                 AtHalMaxAddress(hal));
        return cAtFalse;
        }

    /* Fill */
    for (address = fromAddr; address <= toAddr; address++)
        {
        regValue = AtHalRead(hal, address);
        mFieldIns(&regValue, maskValue, shiftValue, value);
        AtHalWrite(hal, address, regValue);
        }

    return ret;
    }

eBool CmdAtMeasureRead(char argc, char **argv)
    {
    AtHal      hal;
    uint8      coreId = 1;
    uint32     address = 0;
    uint32     fromAddr = 0;
    uint32     toAddr = 0;
    uint32     repeat, repeatTimes;
    uint32     elapsedTime_us = 0;
    uint32     numRegs;
    uint32     totalTime_us = 0;

    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    uint32 maxTime_us = 0, minTime_us = (uint32)-1;

    AtOsalMemInit(&curTime, sizeof(tAtOsalCurTime), 0);
    AtOsalMemInit(&startTime, sizeof(tAtOsalCurTime), 0);

    if(argc != 3)
        {
        AtPrintc(cSevCritical, "Number of parameters wrong\n");
        return cAtFalse;
        }

    /* Parse parameter address from command */
    StrToDw(argv[0], 'h', &fromAddr);
    StrToDw(argv[1], 'h', &toAddr);
    StrToDw(argv[2], 'd', &repeatTimes);

    if ((repeatTimes > cMaxNumRepeatTime) ||
        (!AddressIsValid(fromAddr)) ||
        (!AddressIsValid(toAddr)))
        {
        AtPrintc(cSevCritical, "Invalid parameters, repeat time should less than %d\n", cMaxNumRepeatTime);
        return cAtFalse;
        }

    if (toAddr < fromAddr)
        return cAtFalse;

    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        return cAtFalse;

    numRegs = ((toAddr - fromAddr) + 1) * repeatTimes;
    for (repeat = 0; repeat < repeatTimes; repeat++)
        {
        for (address = fromAddr; address <= toAddr; address++)
            {
            AtOsalCurTimeGet(&startTime);
            AtHalRead(hal, address);
            AtOsalCurTimeGet(&curTime);

            elapsedTime_us = mTimeIntervalInUsGet(startTime, curTime);
            totalTime_us += elapsedTime_us;

            if (maxTime_us < elapsedTime_us)   maxTime_us = elapsedTime_us;
            if (elapsedTime_us < minTime_us)   minTime_us = elapsedTime_us;
            }
        }

    /* Average elapsed microsecond for reading one address */
    elapsedTime_us = totalTime_us / numRegs;
    AtPrintc(cSevInfo, "Read (%u regs): total %u us, average %u us, maximum %u us, minimum %u us\n", numRegs, totalTime_us, elapsedTime_us, maxTime_us, minTime_us);

    return cAtTrue;
    }

eBool CmdAtMeasureWrite(char argc, char **argv)
    {
    AtHal      hal;
    uint8      coreId = 1;
    uint32     address = 0;
    uint32     fromAddr = 0;
    uint32     toAddr = 0;
    uint32     repeat, repeatTimes;
    tAtOsalCurTime curTime, startTime;
    uint32 numRegs;
    uint32 elapsedTime_us;
    uint32     totalTime_us = 0;
    uint32 maxTime_us = 0, minTime_us = (uint32)-1;

    AtOsalMemInit(&curTime, sizeof(tAtOsalCurTime), 0);
    AtOsalMemInit(&startTime, sizeof(tAtOsalCurTime), 0);

    if(argc != 3)
        {
        AtPrintc(cSevCritical, "Number of parameters wrong\n");
        return cAtFalse;
        }

    /* Parse parameter address from command */
    StrToDw(argv[0], 'h', &fromAddr);
    StrToDw(argv[1], 'h', &toAddr);
    StrToDw(argv[2], 'd', &repeatTimes);

    if ((repeatTimes > cMaxNumRepeatTime) ||
        (!AddressIsValid(fromAddr)) ||
        (!AddressIsValid(toAddr)))
        {
        AtPrintc(cSevCritical, "Invalid parameters, repeat time should less than %d\n", cMaxNumRepeatTime);
        return cAtFalse;
        }

    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        return cAtFalse;

    if (toAddr < fromAddr)
        return cAtFalse;

    numRegs = ((toAddr - fromAddr) + 1) * repeatTimes;;
    for (repeat = 0; repeat < repeatTimes; repeat++)
        {
        for (address = fromAddr; address <= toAddr; address++)
            {
            AtOsalCurTimeGet(&startTime);
            AtHalWrite(hal, address, 0xABCD);
            AtOsalCurTimeGet(&curTime);

            elapsedTime_us = mTimeIntervalInUsGet(startTime, curTime);
            totalTime_us += elapsedTime_us;
            if (maxTime_us < elapsedTime_us)   maxTime_us = elapsedTime_us;
            if (elapsedTime_us < minTime_us)   minTime_us = elapsedTime_us;
            }
        }

    /* Average elapsed microsecond for reading one address */
    elapsedTime_us = totalTime_us / numRegs;
    AtPrintc(cSevInfo, "Write (%u regs): total %u us, average %u us, maximum %u us, minimum %u us\n", numRegs, totalTime_us, elapsedTime_us, maxTime_us, minTime_us);

    return cAtTrue;
    }

eBool CmdDiagHalMemTest(char argc, char **argv)
    {
    uint32 timeVal, testingTime = 1;
    uint8 core_i;
    eBool success = cAtTrue;

    if (argc > 0)
        testingTime = (uint32)AtStrToDw(argv[0]);

    /* Test HAL of all cores */
    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(CliDevice()); core_i++)
        {
        for (timeVal = 0; timeVal < testingTime; timeVal++)
            {
            AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), core_i);
            if (!AtHalDiagMemTest(hal))
                {
                success = cAtFalse;
                break;
                }
            }

        if (!success)
            break;
        }

    /* Let the user know */
    if (success)
        AtPrintc(cSevInfo, "HAL memory test success.\n");
    else
        AtPrintc(cSevCritical, "HAL memory test fail.\n");

    return success;
    }

eBool CmdAtHalDebug(char argc, char** argv)
    {
    uint8 core_i;
    uint8 numCores = AtDeviceNumIpCoresGet(CliDevice());
	AtUnused(argv);
	AtUnused(argc);

    for (core_i = 0; core_i < numCores; core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), core_i);
        if (numCores > 1)
            AtPrintc(cSevInfo, "* Core %d:\r\n", core_i + 1);
        AtHalDebug(hal);
        }

    return cAtTrue;
    }

eBool CmdAtHalDebugEnable(char argc, char** argv)
    {
    eBool parseResult;
    eBool debugEnable = cAtFalse;
    uint8 core_i;
    uint8 numCores = AtDeviceNumIpCoresGet(CliDevice());
	AtUnused(argc);

    /* Get debug enable option */
    mAtStrToBool(argv[0], debugEnable, parseResult);
    if (!parseResult)
        {
        AtPrintc(cSevCritical, "ERROR: Expected en/dis\r\n");
        return cAtFalse;
        }

    for (core_i = 0; core_i < numCores; core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), core_i);
        if (numCores > 1)
            AtPrintc(cSevInfo, "* Core %d:\r\n", core_i + 1);
        AtHalDebugEnable(hal, debugEnable);
        }

    return cAtTrue;
    }

/* The following source code is from the old SDK, it may need to be clean */
static void AtCompareDumpByHal(AtHal hal, tAtCompareDumpInfo *pDumpInfo, uint32 itemCount, uint32 maskVal, uint32 shiftVal)
    {
    char    *pStrLine;
    char    *pStr;
    uint32   maxCount;
    uint32   rowIndex;
    uint32   i;
    uint32   readValue;
    uint32   *addr;

    /* Find max row to print */
    maxCount = 0;
    for (i = 0; i < itemCount; i++)
        {
        if (maxCount < pDumpInfo[i].count)
            maxCount = pDumpInfo[i].count;
        }

    addr = (uint32 *)AtOsalMemAlloc((uint32)itemCount * sizeof(uint32));
    pStrLine = (char *)AtOsalMemAlloc((uint32)(itemCount * 2 * 11 + 10));

    if (addr == NULL || pStrLine == NULL)
        {
        AtPrintc(cSevCritical, "Cannot allocate memory\r\n");
        return;
        }

    AtOsalMemInit(addr, (uint32)itemCount * sizeof(uint32), 0);
    AtOsalMemInit(pStrLine, (uint32)(itemCount * 2 * 11), 0);

    pStrLine[0]= '\0';

    for (i = 0; i < itemCount; i++)
        AtStrcat(pStrLine, "Addr       Hex Value  ");

    AtPrintf("%s", pStrLine);
    AtPrintf("\r\n\n");

    for (i = 0; i < itemCount; i++)
        addr[i] = pDumpInfo[i].startAddr;

    rowIndex = 0;
    while (rowIndex < maxCount)
        {
        pStr = pStrLine;

        for (i = 0; i < itemCount; i++)
            {
            if (((addr[i] - pDumpInfo[i].startAddr) / pDumpInfo[i].step) < pDumpInfo[i].count)
                {
                readValue = (AtHalRead(hal, addr[i]) & maskVal) >> shiftVal;
                AtSprintf(pStr, "%08Xh: ", addr[i]);
                pStr += 11;
                AtSprintf(pStr, "%08X   ", readValue);
                pStr += 11;
                addr[i] += pDumpInfo[i].step;
                AtDebugCpuRest();
                }
            }

        *pStr = '\r'; pStr++;
        *pStr = '\n'; pStr++;
        *pStr = '\0';
        AtPrintf("%s", pStrLine);
        rowIndex++;
        }

    AtOsalMemFree(addr);
    AtOsalMemFree(pStrLine);
    }

eBool CmdAtCompareDump(char argc, char** argv)
    {
    uint32 i;
    uint32 rowCount;
    AtHal hal;
    uint32 numItem;
    char *pDupRegDumpInfoList;
    char *regDumpInfo;
    char *startAddress;
    char *step;
    tAtCompareDumpInfo *pDumpInfo;
    uint8  coreId = 1;
    AtList list = AtListCreate(0);
    uint32 maskValue = 0xffffffff;
    uint32 shiftValue = 0;
    static const uint32 cMaxNumRegisters = 4096;

    if(argc < 2)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if(argc > 4)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        {
        if (!AtMaskStrConvert(argv[2], &maskValue, &shiftValue))
            coreId = (uint8)AtStrToDw(argv[2]);
        else if (argc > 3)
            coreId = (uint8)AtStrToDw(argv[3]);
        }

    rowCount = (AtStrToDw(argv[0]) > cMaxNumRegisters) ? cMaxNumRegisters : AtStrToDw(argv[0]);

    /* Parse register dump information group  */
    pDupRegDumpInfoList = AtStrdup(argv[1]);
    AtListObjectAdd(list, (AtObject)(void *)AtStrtok(pDupRegDumpInfoList, cSeparateChar));
    while((regDumpInfo = AtStrtok(NULL, cSeparateChar)) != NULL)
        AtListObjectAdd(list, (AtObject)(void *)regDumpInfo);

    numItem = AtListLengthGet(list);
    pDumpInfo = (tAtCompareDumpInfo *) AtOsalMemAlloc(numItem * sizeof(tAtCompareDumpInfo));
    if (pDumpInfo == NULL)
        {
        AtPrintc(cSevCritical, "Cannot allocate memory\n");
        AtListFlush(list);
        AtObjectDelete((AtObject)list);
        AtOsalMemFree(pDupRegDumpInfoList);
        return cAtFalse;
        }

    for (i = 0; i < numItem; i++)
        {
        regDumpInfo = (char*)AtListObjectRemoveAtIndex(list, 0);

        /* Just for safety */
        if (regDumpInfo == NULL) break;

        /* Parse register dump information */
        startAddress = AtStrtok(regDumpInfo, cFormatCheck);
        step = AtStrtok(NULL, cFormatCheck);

        if ((step == NULL) || (AtStrtok(NULL, cFormatCheck) != NULL))
            {
            AtPrintc(cSevCritical, "Invalid compare dump string \n");
            AtListFlush(list);
            AtObjectDelete((AtObject)list);
            AtOsalMemFree(pDumpInfo);
            AtOsalMemFree(pDupRegDumpInfoList);
            return cAtFalse;
            }

        pDumpInfo[i].count = rowCount;
        pDumpInfo[i].startAddr = AtStrToDw(startAddress);
        pDumpInfo[i].step = AtStrToDw(step);
        }

    AtObjectDelete((AtObject)list);
    AtOsalMemFree(pDupRegDumpInfoList);
    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), CoreId(coreId)));
    if (hal == NULL)
        {
        AtOsalMemFree(pDumpInfo);
        return cAtFalse;
        }

    AtCompareDumpByHal(hal, pDumpInfo, numItem, maskValue, shiftValue);
    AtOsalMemFree(pDumpInfo);

    return cAtTrue;
    }

eBool CmdAtHalReset(char argc, char** argv)
    {
    uint8 core_i;
    AtUnused(argc);
    AtUnused(argv);

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(CliDevice()); core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), core_i);
        AtHalReset(hal);
        }

    return cAtTrue;
    }

eBool CmdBitFieldRegCheck(char argc , char **argv)
    {
    uint32 dPattern;
    uint32 dFieldVal;
    uint32 dRegVal;
    uint32 dFieldMask;
    uint32 dRegFieldAddr;
    uint32 dReg;
    int firstId = 0;
    int lastId = 0;
    eBool haveLast;
    uint32 iLengthMask;
    uint32 i;
    uint32 j;
    char  temStr[3];
    AtHal hal = NULL;

    AtUnused(argc);

    dPattern = 0xFFFFFFFF;
    haveLast = cAtFalse;
    AtOsalMemInit((void*)temStr, 3, 0);
    if(AtStrncmp("0x", argv[0], 2) == 0)
        {
        dFieldVal = (uint32)(AtStrtoul(argv[0], NULL, 16));
        }
    else
        {
        dFieldVal = (uint32)(AtStrtoul(argv[0], NULL, 10));
        }

    iLengthMask = AtStrlen((char*)argv[1]);
    j = 0;

    if((AtStrncmp("bit", argv[1], 3) != 0)
        ||(iLengthMask < 4)
        ||(iLengthMask > 8))
        {
        AtPrintc(cSevCritical, "Invalid Field Mask\n");
        return cAtFalse;
        }

    for(i = 3; i < iLengthMask; i++)
        {
        if(    (j == 3)
            || ((!AtIsDigit(argv[1][i])) && (argv[1][i] != '_'))
            || ((argv[1][i] == '_') && (i == (iLengthMask - 1))))
            {
            AtPrintc(cSevCritical, "Invalid Field Mask\n");
            return cAtFalse;
            }
        temStr[j] = argv[1][i];
        j++;
        if(argv[1][i] == '_')
            {
            firstId = (uint8)(AtStrtoul(temStr, NULL, 10));
            j = 0;
            haveLast = cAtTrue;
            AtOsalMemInit((void*)temStr, 3, 0);
            }
        }
    if(haveLast)
        {
        lastId = (uint8)(AtStrtoul(temStr, NULL, 10));
        }
    else
        {
        firstId = (uint8)(AtStrtoul(temStr, NULL, 10));
        if(firstId == 0)
            {
            lastId = -1;
            }
        }

    if((lastId >= firstId) || (lastId > 31) || (firstId > 31))
        {
        AtPrintc(cSevCritical, "Invalid Field Mask\n");
        return cAtFalse;
        }

    dRegFieldAddr = (uint32)(AtStrtoul(argv[2], NULL, 16));
    /* Get HAL */
    hal = AtIpCoreHalGet(AtDeviceIpCoreGet(CliDevice(), 0));
    if (hal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No HAL\r\n");
        return cAtFalse;
        }

    dReg = AtHalRead(hal, dRegFieldAddr);
    if(haveLast)
        {
        dFieldMask = ((dPattern >> lastId) << ((31 - firstId) + lastId)) >> (31 - firstId);
        mFieldGet(dReg, dFieldMask, lastId, uint32, &dRegVal);
        }
    else
        {
        dFieldMask = (cBit0 << firstId);
        mFieldGet(dReg, dFieldMask, firstId, uint32, &dRegVal);
        }
    if(dRegVal != dFieldVal)
        {
        AtPrintc(cSevCritical, "ERROR: Check BitField %s at 0x%0x, Expected val = 0x%x but Real Val = 0x%x\n", argv[1], dRegFieldAddr, dFieldVal, dRegVal);
        AtTextUIIntegerResultSet(AtCliSharedTextUI(), 0);
        }
    else
        {
        AtPrintc(cSevInfo, "OK: Check BitField %s at 0x%0x, Expected val = %x and Real Val = 0x%x\n", argv[1], dRegFieldAddr, dFieldVal, dRegVal);
        AtTextUIIntegerResultSet(AtCliSharedTextUI(), 1);
        }

    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
Function Name   : CmdBitFieldLongRegCheck

Description     : Check expected value of a field in a more than 32  bit register

Inputs          :


Outputs         : None

Return          : true    - if success
                  false   - if fail

Notes           : None
------------------------------------------------------------------------------*/
eBool CmdBitFieldLongRegCheck(char argc , char **argv)
    {
    uint32 dFieldVal;
    uint32 dRegVal;
    uint32 dRegFieldAddr;
    tAtLongMask longMask;
    uint32 longValue[cAtLongRegMaxSize];
    uint16 numReadDwords;
    uint8 coreId = 1;

    AtUnused(argc);

    DefaultLongMaskMake(&longMask);
    if(AtStrncmp("0x", argv[0], 2) == 0)
        {
        dFieldVal = (uint32)(AtStrtoul(argv[0], NULL, 16));
        }
    else
        {
        dFieldVal = (uint32)(AtStrtoul(argv[0], NULL, 10));
        }

    AtLongMaskGet(argv[1], &longMask);

    dRegFieldAddr = (uint32)(AtStrtoul(argv[2], NULL, 16));

    /* Get HAL */
    AtOsalMemInit(longValue, 0, sizeof(longValue));
    numReadDwords = AtDeviceLongReadOnCore(CliDevice(), dRegFieldAddr, longValue, cAtLongRegMaxSize, CoreId(coreId));
    if (numReadDwords == 0)
        {
        AtPrintc(cSevCritical, "ERROR: This is not a long register or it is unknown\r\n");
        return cAtFalse;
        }
    dRegVal = ValueFromLongRegGet(&longMask, longValue, numReadDwords);

    if(dRegVal != dFieldVal)
        {
        AtPrintc(cSevCritical, "ERROR: Check BitField %s at 0x%0x, Expected val = 0x%x but Real Val = 0x%x\n", argv[1], dRegFieldAddr, dFieldVal, dRegVal);
        AtTextUIIntegerResultSet(AtCliSharedTextUI(), 0);
        }
    else
        {
        AtPrintc(cSevInfo, "OK: Check BitField %s at 0x%0x, Expected val = 0x%x and Real Val = 0x%x\n", argv[1], dRegFieldAddr, dFieldVal, dRegVal);
        AtTextUIIntegerResultSet(AtCliSharedTextUI(), 1);
        }

    return cAtTrue;
    }

static uint32 RegisterListFromString(char* listString, uint32 *regList, uint32 bufferSize)
    {
    return CliIdListFromString(listString, regList, bufferSize);
    }

eBool CmdAtHalRegisterTest(char argc, char** argv)
    {
    uint32 time, testingTime = 1;
    uint8 core_i;
    uint32 regNum;
    uint32 *regList = CliSharedIdBufferGet(&regNum);
    uint32 maskValue, shiftValue;
    eBool success = cAtTrue;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    if (argc > 3)
        {
        AtPrintc(cSevCritical, "ERROR: Too much parameter\r\n");
        return cAtFalse;
        }

    /* Get start address */
    regNum = RegisterListFromString(argv[0], regList, regNum);
    if (regNum == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Can not get register list. Expected hexa format\r\n");
        return cAtFalse;
        }

    /* Parse the next arguments */
    maskValue  = cBit31_0;
    shiftValue = 0;

    if (argc > 1)
        {
        uint8 arg_i;

        for (arg_i = 1; arg_i < argc; arg_i++)
            {
            if (AtStrstr(argv[arg_i], cMaskTokenStr))
                AtMaskStrConvert(argv[arg_i], &maskValue, &shiftValue);
            else
                testingTime = AtStrToDw(argv[arg_i]);
            }
        }

    /* Just make sure that there is at least one testing time */
    if (testingTime == 0)
        testingTime = 1;

    /* Test registers on all cores */
    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(CliDevice()); core_i++)
        {
        for (time = 0; time < testingTime; time++)
            {
            AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), core_i);
            if (!AtHalDiagRegistersTest(hal, regList, regNum, maskValue))
                {
                success = cAtFalse;
                break;
                }
            }

        if (!success)
            break;
        }

    /* Let the user know */
    if (success)
        AtPrintc(cSevInfo, "Register test success.\n");
    else
        AtPrintc(cSevCritical, "Register test fail.\n");

    return success;
    }

eBool CmdAtDeviceDdrOffloadRegRead(char argc, char **argv)
    {
    return DdrOffloadRead(argc, argv, AtDeviceReadOnDdrOffload);
    }

eBool CmdAtDeviceDdrOffloadRegWrite(char argc, char** argv)
    {
    uint32            addr, regAddress;
    uint32            pValue[cAtLongRegMaxSize];
    uint32            dDwNum;
    tAtLongMask       longMask;
    uint8             coreId;
    uint32            subDeviceId;
    uint8 Id, numSubDevice;
    AtDevice realDevice;

    dDwNum     = 0;
    coreId     = 1; /* Default for core ID = 1 in this card */
    addr       = 0;
    AtOsalMemInit(pValue, 0, sizeof(pValue));

    if (argc < 4 )
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 6 )
        {
        AtPrintc(cSevCritical, "To much parameters\r\n");
        return cAtFalse;
        }

    if (argc > 4)
        {
        if (!AtLongMaskGet(argv[4], &longMask))
            {
            DefaultLongMaskMake(&longMask);
            coreId = (uint8)AtStrToDw(argv[4]);
            }
        else if (argc > 5)
            coreId = (uint8)AtStrToDw(argv[5]);
        }
    else
        DefaultLongMaskMake(&longMask);

    Id = (uint8)AtStrToDw(argv[0]);
    realDevice = CliRealDevice();
    numSubDevice = AtDeviceNumSubDeviceGet(realDevice);
    if (Id == 0 || Id > numSubDevice)
        {
        AtPrintc(cSevCritical, "ERROR: Sub device ID is from 1 to %d\r\n", numSubDevice);
        return cAtFalse;
        }
    subDeviceId = (uint32)DeviceIdFromCmdParamId((uint8)Id);

    StrToDw(argv[1], 'h', &addr);
    StrToDw(argv[2], 'h', &regAddress);

    if (longMask.isFullMask)
        {
        if (AtStrToArrayDw(argv[3], pValue, &dDwNum) == cAtFalse )
            {
            AtPrintc(cSevCritical, "Invalid value\r\n");
            return cAtFalse;
            }
        }
    else
        StrToDw(argv[3], 'h', &(pValue[0]));

    return OneDdrOffloadRegWrite(subDeviceId, addr, regAddress, coreId, &longMask, pValue, cAtTrue /* Verbose */);
    }

eBool CmdAtDeviceDdrOffloadRegDump(char argc, char **argv)
    {
    uint32 fromAddr, toAddr;
    tAtLongMask  longMask;
    uint8 coreId = 1;
    eBool success = cAtTrue;
    uint32 subDeviceId = 0;
    uint8 Id, numSubDevice;
    AtDevice realDevice;

    if(argc < 3 )
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc > 5)
        {
        AtPrintc(cSevCritical, "Too much parameters\r\n");
        return cAtFalse;
        }

    /* Convert Mask Shift */
    if (argc > 3)
        {
        if (!AtLongMaskGet(argv[3], &longMask))
            {
            DefaultLongMaskMake(&longMask);
            coreId = (uint8)AtStrToDw(argv[3]);
            }
        else if (argc > 4)
            coreId = (uint8)AtStrToDw(argv[4]);
        }
    else
        DefaultLongMaskMake(&longMask);

    Id = (uint8)AtStrToDw(argv[0]);
    realDevice = CliRealDevice();
    numSubDevice = AtDeviceNumSubDeviceGet(realDevice);
    if (Id == 0 || Id > numSubDevice)
        {
        AtPrintc(cSevCritical, "ERROR: Sub device ID is from 1 to %d\r\n", numSubDevice);
        return cAtFalse;
        }
    subDeviceId = (uint32)DeviceIdFromCmdParamId((uint8)Id);

    /* Parse parameter address from command */
    if (CorrectAddressRange(argv[1], argv[2], &fromAddr, &toAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    while (fromAddr <= toAddr)
        {
        success |= DdrOffloadRegPrint(subDeviceId, fromAddr, coreId, &longMask, AtDeviceReadOnDdrOffload);
        if (fromAddr == cBit31_0)
            break;

        fromAddr++;
        }

    return success;
    }

eBool CmdAtDeviceDdrOffloadRegFill(char argc, char** argv)
    {
    uint32            fromAddr, toAddr;
    uint32            fromRegAddr, toRegAddr;
    uint32            pValue[cAtLongRegMaxSize];
    uint32            dDwNum;
    tAtLongMask       longMask;
    uint8             coreId;
    eBool             ret = cAtTrue;
    uint32            subDeviceId = 0;
    uint8 Id, numSubDevice;
    AtDevice realDevice;

    dDwNum     = 0;
    coreId     = 1; /* Default for core ID = 1 in this card */
    AtOsalMemInit(pValue, 0, sizeof(pValue));

    if(argc < 6)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if(argc > 8)
        {
        AtPrintc(cSevCritical, "To much parameters\r\n");
        return cAtFalse;
        }

    if (argc > 6)
        {
        if (!AtLongMaskGet(argv[6], &longMask))
            {
            DefaultLongMaskMake(&longMask);
            coreId = (uint8)AtStrToDw(argv[6]);
            }
        else if (argc > 7)
            coreId = (uint8)AtStrToDw(argv[7]);
        }
    else
        DefaultLongMaskMake(&longMask);

    subDeviceId = AtStrToDw(argv[0]);
    Id = (uint8)AtStrToDw(argv[0]);
    realDevice = CliRealDevice();
    numSubDevice = AtDeviceNumSubDeviceGet(realDevice);
    if (Id == 0 || Id > numSubDevice)
        {
        AtPrintc(cSevCritical, "ERROR: Sub device ID is from 1 to %d\r\n", numSubDevice);
        return cAtFalse;
        }
    subDeviceId = (uint32)DeviceIdFromCmdParamId((uint8)Id);

    /* Get addresses and value */
    if (CorrectAddressRange(argv[1], argv[2], &fromAddr, &toAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    if (CorrectAddressRange(argv[3], argv[4], &fromRegAddr, &toRegAddr) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no register, start address must less than stop address\r\n");
        return cAtTrue;
        }

    if ((!AddressIsValid(fromAddr)) || (!AddressIsValid(toAddr)))
        {
        AtPrintc(cSevWarning, "WARNING: Invalid address\r\n");
        return cAtTrue;
        }
    if ((!AddressIsValid(fromRegAddr)) || (!AddressIsValid(toRegAddr)))
        {
        AtPrintc(cSevWarning, "WARNING: Invalid address\r\n");
        return cAtTrue;
        }

    if (longMask.isFullMask)
        {
        if (AtStrToArrayDw(argv[5], pValue, &dDwNum) == cAtFalse )
            {
            AtPrintc(cSevCritical, "Invalid value\r\n");
            return cAtFalse;
            }
        }
    else
        StrToDw(argv[5], 'h', &(pValue[0]));

    while ((fromAddr <= toAddr) && (fromRegAddr <= toRegAddr))
        {
        ret |= OneDdrOffloadRegWrite(subDeviceId, fromAddr, fromRegAddr, coreId, &longMask, pValue, cAtFalse /* No verbosed */);
        fromAddr++;
        fromRegAddr++;
        }

    return ret;
    }

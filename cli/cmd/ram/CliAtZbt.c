/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : CliAtZbt.c
 *
 * Created Date: Feb 21, 2013
 *
 * Description : ZBT CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleRam.h"

#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtZbtAddressBusTest(char argc, char **argv)
    {
    CmdAtRamBusTest(argc, argv, AtModuleRamZbtGet, cAtFalse);
    return cAtTrue;
    }

eBool CmdAtZbtDataBusTest(char argc, char **argv)
    {
    CmdAtRamBusTest(argc, argv, AtModuleRamZbtGet, cAtTrue);
    return cAtTrue;
    }

eBool CmdAtZbtMemoryTest(char argc, char **argv)
    {
    CmdAtRamMemoryTest(argc, argv, AtModuleRamZbtGet);
    return cAtTrue;
    }

eBool CmdAtZbtRead(char argc, char **argv)
    {
    return CmdAtRamRead(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtWrite(char argc, char **argv)
    {
    return CmdAtRamWrite(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtDebug(char argc, char **argv)
    {
    return CmdAtRamDebug(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtAccessEnable(char argc, char **argv)
    {
    return CmdAtRamAccessEnable(argc, argv, AtModuleRamZbtGet, cAtTrue);
    }

eBool CmdAtZbtAccessDisable(char argc, char **argv)
    {
    return CmdAtRamAccessEnable(argc, argv, AtModuleRamZbtGet, cAtFalse);
    }

eBool CmdAtZbtTest(char argc, char **argv)
    {
    CmdAtRamTest(argc, argv, AtModuleRamDdrGet);
    return cAtTrue;
    }

eBool CmdAtZbtHwTestStart(char argc, char **argv)
    {
    return CliAtRamHwTestStart(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtHwTestStop(char argc, char **argv)
    {
    return CliAtRamHwTestStop(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtHwTestStatusShow(char argc, char **argv)
    {
    return CliAtRamHwTestStatusShow(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtDebugAddressBusSizeSet(char argc, char **argv)
    {
    return CliAtRamDebugAddressBusSizeSet(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtDebugDataBusSizeSet(char argc, char **argv)
    {
    return CliAtRamDebugDataBusSizeSet(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtTestBurstSet(char argc, char **argv)
    {
    return CliAtRamTestBurstSet(argc, argv, AtModuleRamZbtGet);
    }

eBool CmdAtZbtTestHwAssistEnable(char argc, char **argv)
    {
    return CliAtRamTestHwAssistEnable(argc, argv, AtModuleRamZbtGet, cAtTrue);
    }

eBool CmdAtZbtTestHwAssistDisable(char argc, char **argv)
    {
    return CliAtRamTestHwAssistEnable(argc, argv, AtModuleRamZbtGet, cAtFalse);
    }

eBool CmdAtZbtShow(char argc, char **argv)
    {
    return CliAtRamShow(argc, argv, AtModuleRamZbtGet);
    }



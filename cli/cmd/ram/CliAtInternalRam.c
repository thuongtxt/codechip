/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : CliAtInternalRam.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : Internal RAM CLIs.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleRam.h"
#include "../diag/CliAtErrorGenerator.h"
#include "../../../driver/src/generic/ram/AtModuleRamInternal.h"
#include "../../../driver/src/implement/default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cCliAtRamErrorStr[] ={
                                         "ecc-correctable",
                                         "ecc-uncorrectable",
                                         "crc",
                                         "parity"
                                        };
static const uint32 cCliAtRamErrorValue[]={
                                           cAtRamAlarmEccCorrectable,
                                           cAtRamAlarmEccUnnorrectable,
                                           cAtRamAlarmCrcError,
                                           cAtRamAlarmParityError
                                          };

static const uint32 cAtModuleIdVal[] =
    {
     cThaModuleOcn,
     cThaModulePoh,
     cThaModuleCdr,
     cThaModuleMap,
     cThaModulePwe,
     cThaModuleCla,
     cThaModulePda,
     cAtModulePdh,
     cAtModuleSur
    };
static const char * cAtModuleIdStr[] =
    {
     "ocn",
     "poh",
     "cdr",
     "map",
     "pwe",
     "cla",
     "pda",
     "pdh",
     "sur"
    };

static const uint32 cCliAtRamErrorCntValue[]={
                                           cAtRamCounterTypeEccCorrectableErrors,
                                           cAtRamCounterTypeEccUncorrectableErrors,
                                           cAtRamCounterTypeCrcErrors
                                          };

static const char *cCliAtRamErrorTypeStr[] ={
                                         "ecc-correctable",
                                         "ecc-uncorrectable",
                                         "crc"
                                        };

static const uint32 cCliAtRamErrorTypeVal[]={
                                           cAtRamGeneratorErrorTypeEccCorrectable,
                                           cAtRamGeneratorErrorTypeEccUncorrectable,
                                           cAtRamGeneratorErrorTypeCrcError
                                          };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RamsFromString(char *pStrIdList, AtInternalRam *ramList, uint32 bufferSize)
    {
    uint32 *ramIdsList = CliSharedIdBufferGet(&bufferSize);
    uint32 ramIdsNum = CliIdListFromString(pStrIdList, ramIdsList, bufferSize);
    uint32 numRams = 0;
    uint32 ram_i;

    for (ram_i = 0; ram_i < ramIdsNum; ram_i++)
        {
        ramList[numRams] = AtModuleRamInternalRamGet(CliAtModuleRam(), CliId2DriverId(ramIdsList[ram_i]));

        if (ramList[numRams] && !AtInternalRamIsReserved(ramList[numRams]))
            numRams += 1;

        if (numRams >= bufferSize)
            break;
        }

    return numRams;
    }

static uint32 ModuleIdFromString(char* arg)
    {
    uint32 moduleId;
    for (moduleId = 0; moduleId < mCount(cAtModuleIdVal); moduleId++)
        {
        if (AtStrcmp(arg, cAtModuleIdStr[moduleId]) == 0)
            return cAtModuleIdVal[moduleId];
        }
    return cBit31_0;
    }

static uint32 RamsFromModule(char *pModuleStr, AtInternalRam *ramList, uint32 bufferSize)
    {
    uint32 moduleId = ModuleIdFromString(pModuleStr);
    uint32 minRamId = AtModuleRamStartInternalRamIdOfModuleGet(CliAtModuleRam(), moduleId);
    AtModule module = AtDeviceModuleGet(CliDevice(), moduleId);
    uint32 ramIdsNum = AtModuleNumInternalRamsGet(module);
    uint32 numRams = 0;
    uint32 ram_i;

    for (ram_i = 0; ram_i < ramIdsNum; ram_i++)
        {
        ramList[numRams] = AtModuleInternalRamGet(module, minRamId + ram_i, ram_i);

        if (ramList[numRams] && !AtInternalRamIsReserved(ramList[numRams]))
            numRams += 1;

        if (numRams >= bufferSize)
            break;
        }

    return numRams;
    }

static AtInternalRam* RamsFromArgument(char* arg, uint32 *numberRams, eBool supportModuleStrParser)
    {
    uint32 bufferSize;
    AtInternalRam *rams = (AtInternalRam *)((void**)CliSharedChannelListGet(&bufferSize));
    *numberRams = RamsFromString(arg, (AtInternalRam *)((void**)rams), bufferSize);

    if (*numberRams == 0 && supportModuleStrParser)
        {
        *numberRams = RamsFromModule(arg, (AtInternalRam *)((void**)rams), bufferSize);
        }

    return rams;
    }

static const char * Error2String(uint32 errors)
    {
    uint16 i;
    static char errorString[64];

    /* Initialize string */
    errorString[0] = '\0';
    if (errors == 0)
        AtSprintf(errorString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cCliAtRamErrorValue); i++)
            {
            if (errors & (cCliAtRamErrorValue[i]))
                AtSprintf(errorString, "%s%s|", errorString, cCliAtRamErrorStr[i]);
            }

        if (AtStrlen(errorString) == 0)
            return "None";

        /* Remove the last '|' */
        errorString[AtStrlen(errorString) - 1] = '\0';
        }

    return errorString;
    }

static uint32 ErrorMaskFromString(char *errorStr)
    {
    return CliMaskFromString(errorStr, cCliAtRamErrorStr, cCliAtRamErrorValue, mCount(cCliAtRamErrorValue));
    }

static eBool ErrorMonitor(char argc, char **argv,
                          eAtRet (*MonitorEnable)(AtInternalRam, eBool))
    {
    uint32 ram_i, numRams;
    AtInternalRam *ramIdsList;
    eBool success = cAtTrue;
    eBool enabled;

    AtUnused(argc);

    /* Get list of RAMs */
    if ((ramIdsList = RamsFromArgument(argv[0], &numRams, cAtFalse)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    mAtStrToBool(argv[1], enabled, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: expected en/dis\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        AtInternalRam ram = ramIdsList[ram_i];
        eAtRet ret = MonitorEnable(ram, enabled);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: %s parity fail on RAM %d with ret = %s\r\n",
                     enabled ? "Enable" : "Disable", AtInternalRamIdGet(ram) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool RamErrorCountersGet(char *pRamIdStr, uint32 (*CounterGetFunc)(AtInternalRam, uint16), eBool silent)
    {
    uint32 ram_i, numRams;
    AtInternalRam *ramIdsList;
    tTab *tabPtr = NULL;
    const char *pHeading[] = { "RamId", "Description", "ecc-correctable", "ecc-uncorrectable", "crc"  };

    /* Get list of RAMs */
    if ((ramIdsList = RamsFromArgument(pRamIdStr, &numRams, cAtFalse)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Fail to create table\r\n");
            return cAtFalse;
            }
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint8 counterType_i;
        uint16 counterType;
        uint32 counterValue;
        uint32 column = 0;
        AtInternalRam ram = ramIdsList[ram_i];
        StrToCell(tabPtr, ram_i, column++, CliNumber2String(AtInternalRamIdGet(ram) + 1, "%u"));
        StrToCell(tabPtr, ram_i, column++, AtInternalRamDescription(ram));

        for (counterType_i = 0; counterType_i < mCount(cCliAtRamErrorCntValue); counterType_i++)
            {
            counterType = (uint16)cCliAtRamErrorCntValue[counterType_i];
            if (AtInternalRamCounterTypeIsSupported(ram, counterType))
                {
                counterValue = CounterGetFunc(ram, counterType);
                CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, cAtCounterTypeError, cAtTrue);
                }
            else
                {
                StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
                }
            }
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool AttributeSet(char *pRamIdString,
                          eAtRet (*AttributeSetFunc)(AtErrorGenerator, uint32),
                          uint32 value)
    {
    uint32 ram_i, numRams;
    AtInternalRam *ramIdsList;
    eBool success = cAtTrue;

    /* Get list of RAMs */
    if ((ramIdsList = RamsFromArgument(pRamIdString, &numRams, cAtFalse)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        eAtRet ret;
        AtInternalRam ram = ramIdsList[ram_i];
        AtErrorGenerator errorGenerator = AtInternalRamErrorGeneratorGet(ram);
        if (errorGenerator != NULL)
            {
            ret = AttributeSetFunc(errorGenerator, value);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: AttributeSet-RAM %d with ret = %s\r\n", AtInternalRamIdGet(ram) + 1, AtRet2String(ret));
                success = cAtFalse;
                }
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: This attribute is not support for Internal-RAM %d\r\n", AtInternalRamIdGet(ram) + 1);
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool NoneAttributeSet(char *pRamIdString, eAtRet (*NoneAttributeSetFunc)(AtErrorGenerator))
    {
    uint32 ram_i, numRams;
    AtInternalRam *ramIdsList;
    eBool success = cAtTrue;

    /* Get list of RAMs */
    if ((ramIdsList = RamsFromArgument(pRamIdString, &numRams, cAtFalse)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        eAtRet ret;
        AtInternalRam ram = ramIdsList[ram_i];
        AtErrorGenerator errorGenerator = AtInternalRamErrorGeneratorGet(ram);
        if (errorGenerator != NULL)
            {
            ret = NoneAttributeSetFunc(errorGenerator);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: AttributeSet-RAM %d with ret = %s\r\n", AtInternalRamIdGet(ram) + 1, AtRet2String(ret));
                success = cAtFalse;
                }
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: This attribute is not support for Internal-RAM %d\r\n", AtInternalRamIdGet(ram) + 1);
            success = cAtFalse;
            }
        }

    return success;
    }

uint32 CliRamErrorTypeFromString(const char *string)
    {
    uint32 errorType;
    eBool convertSuccess;

    mAtStrToEnum(cCliAtRamErrorTypeStr,
                 cCliAtRamErrorTypeVal,
                 string,
                 errorType,
                 convertSuccess);
    return convertSuccess ? errorType : cAtRamGeneratorErrorTypeInvalid;
    }

char *CliRamErrorTypeToString(uint32 errorType)
    {
    static char errorTypeString[16];
    eBool convertSuccess;

    mAtEnumToStr(cCliAtRamErrorTypeStr, cCliAtRamErrorTypeVal, errorType, errorTypeString, convertSuccess);

    return convertSuccess ? errorTypeString : NULL;
    }

const char *CliRamErrorTypesValidDecs(void)
    {
    return "ecc-correctable, ecc-uncorrectable, crc";
    }

eBool CmdRamInternalRamShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 numRams;
    uint32 ram_i;
    const char *heading[] = {"RamId", "Description", "Parity Mon", "ECC Mon", "CRC Mon", "ErrorForced"};
    AtInternalRam *rams;

    AtUnused(argc);

    /* Get list of RAMs */
    if ((rams = RamsFromArgument(argv[0], &numRams, cAtTrue)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    if (numRams == 0)
        {
        AtPrintc(cSevInfo, "No internal RAMs to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numRams, mCount(heading), heading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint8 column = 0;
        uint32 errors;
        eBool boolValue;
        AtInternalRam ram = rams[ram_i];

        StrToCell(tabPtr, ram_i, column++, CliNumber2String(AtInternalRamIdGet(ram) + 1, "%u"));
        StrToCell(tabPtr, ram_i, column++, AtInternalRamDescription(ram));

        if (AtInternalRamParityMonitorIsSupported(ram))
            {
            boolValue = AtInternalRamParityMonitorIsEnabled(ram);
            ColorStrToCell(tabPtr, ram_i, column++, CliBoolToString(boolValue), boolValue ? cSevInfo : cSevCritical);
            }
        else
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);

        if (AtInternalRamEccMonitorIsSupported(ram))
            {
            boolValue = AtInternalRamEccMonitorIsEnabled(ram);
            ColorStrToCell(tabPtr, ram_i, column++, CliBoolToString(boolValue), boolValue ? cSevInfo : cSevCritical);
            }
        else
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);

        if (AtInternalRamCrcMonitorIsSupported(ram))
            {
            boolValue = AtInternalRamCrcMonitorIsEnabled(ram);
            ColorStrToCell(tabPtr, ram_i, column++, CliBoolToString(boolValue), boolValue ? cSevInfo : cSevCritical);
            }
        else
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);

        errors = AtInternalRamForcedErrorsGet(ram);
        StrToCell(tabPtr, ram_i, column++, Error2String(errors));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtPrintc(cSevInfo, "Total %u internal RAMs displayed.\r\n", numRams);
    return cAtTrue;
    }

eBool CmdRamInternalRamInterruptShow(char argc, char **argv)
    {
    tTab *tabPtr = NULL;
    uint32 numRams;
    uint32 ram_i;
    const char *heading[] = {"RamId", "Description", "ECC-Corr", "ECC-Uncorr", "CRC", "Parity"};
    eAtHistoryReadingMode readingMode;
    AtInternalRam *rams;
    uint32 (*ErrorGet)(AtInternalRam);
    eBool silent = cAtFalse;

    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of RAMs */
    if ((rams = RamsFromArgument(argv[0], &numRams, cAtTrue)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    if (numRams == 0)
        {
        AtPrintc(cSevInfo, "No internal RAMs to display\r\n");
        return cAtTrue;
        }

    /* Get reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
        tabPtr = TableAlloc(numRams, mCount(heading), heading);
        if (tabPtr == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    if (readingMode == cAtHistoryReadingModeReadOnly)
        ErrorGet = AtInternalRamErrorHistoryGet;
    else
        ErrorGet = AtInternalRamErrorHistoryClear;

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint8 column = 0;
        uint32 errors;
        AtInternalRam ram = rams[ram_i];
        uint32 j;

        StrToCell(tabPtr, ram_i, column++, CliNumber2String(AtInternalRamIdGet(ram) + 1, "%u"));
        StrToCell(tabPtr, ram_i, column++, AtInternalRamDescription(ram));

        errors = ErrorGet(ram);
        for (j = 0; j < mCount(cCliAtRamErrorValue); j++)
            {
            if (errors & cCliAtRamErrorValue[j])
                ColorStrToCell(tabPtr, ram_i, column++, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, ram_i, column++, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    if (!silent)
        AtPrintc(cSevInfo, "Total %u internal RAMs displayed.\r\n", numRams);

    return cAtTrue;
    }

eBool CmdRamInteralRamForceError(char argc, char **argv)
    {
    uint32 ram_i, numRams;
    AtInternalRam *ramIdsList;
    eBool success = cAtTrue;
    eAtRet (*ErrorHandle)(AtInternalRam self, uint32 errors);
    eBool force;
    uint32 errorValue;

    AtUnused(argc);

    /* Get list of RAMs */
    if ((ramIdsList = RamsFromArgument(argv[0], &numRams, cAtFalse)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    /* Alarm types */
    if (argv[1] != NULL)
        errorValue = ErrorMaskFromString(argv[1]);
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid alarm type\r\n");
        return cAtFalse;
        }

    /* Enable/disable */
    mAtStrToBool(argv[2], force, success);
    if(!success)
        {
        AtPrintc(cSevCritical, "ERROR: expect en/dis, not %s\r\n", argv[3]);
        return cAtFalse;
        }

    if (force)
        ErrorHandle = AtInternalRamErrorForce;
    else
        ErrorHandle = AtInternalRamErrorUnForce;

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        AtInternalRam ram = ramIdsList[ram_i];
        eAtRet ret = ErrorHandle(ram, errorValue);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: %s error on RAM %d fail with ret = %s\r\n",
                     force ? "Force" : "Unforce", AtInternalRamIdGet(ram) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdRamInteralRamParityMonitor(char argc, char **argv)
    {
    return ErrorMonitor(argc, argv, AtInternalRamParityMonitorEnable);
    }

eBool CmdRamInteralRamEccMonitor(char argc, char **argv)
    {
    return ErrorMonitor(argc, argv, AtInternalRamEccMonitorEnable);
    }

eBool CmdRamInteralRamCrcMonitor(char argc, char **argv)
    {
    return ErrorMonitor(argc, argv, AtInternalRamCrcMonitorEnable);
    }

eBool CmdRamInteralRamErrorCountersGet(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode = CliHistoryReadingModeGet(argv[1]);
    eBool silent = cAtFalse;
    AtUnused(argc);

    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid reading action mode. Expected: %s\r\n",
                 CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        return RamErrorCountersGet(argv[0], AtInternalRamCounterClear, silent);

    return RamErrorCountersGet(argv[0], AtInternalRamCounterGet, silent);
    }

eBool CmdRamInternalRamErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    uint32 errorType = CliRamErrorTypeFromString(argv[1]);
    AtUnused(argc);

    if(errorType == cAtRamGeneratorErrorTypeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Error Type. Expected: %s\r\n", CliRamErrorTypesValidDecs());
        return cAtFalse;
        }

    return AttributeSet(argv[0], AtErrorGeneratorErrorTypeSet, errorType);
    }

eBool CmdRamInternalRamErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], AtErrorGeneratorErrorNumSet, AtStrToDw(argv[1]));
    }

eBool CmdRamInternalRamErrorGeneratorStart(char argc, char **argv)
    {
    AtUnused(argc);
    return NoneAttributeSet(argv[0], AtErrorGeneratorStart);
    }

eBool CmdRamInternalRamErrorGeneratorStop(char argc, char **argv)
    {
    AtUnused(argc);
    return NoneAttributeSet(argv[0], AtErrorGeneratorStop);
    }

eBool CmdRamInternalRamErrorGeneratorShow(char argc, char **argv)
    {
    uint32 ram_i, numRams;
    AtInternalRam *ramIdsList;
    tTab *tabPtr;
    const char *pHeading[] =  { "RamId", "Description", "gen-mode", "error-type", "is-started", "error-num"};

    AtUnused(argc);

    /* Get list of RAMs */
    if ((ramIdsList = RamsFromArgument(argv[0], &numRams, cAtFalse)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No RAM ID, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint32 value;
        uint8 column = 0;
        AtInternalRam ram = ramIdsList[ram_i];
        AtErrorGenerator errorGenerator = AtInternalRamErrorGeneratorGet(ram);
        StrToCell(tabPtr, ram_i, column++, CliNumber2String(AtInternalRamIdGet(ram) + 1, "%u"));
        StrToCell(tabPtr, ram_i, column++, AtInternalRamDescription(ram));
        if (errorGenerator)
            {
            value = AtErrorGeneratorModeGet(errorGenerator);
            StrToCell(tabPtr, ram_i, column++, CliErrorGeneratorModeString(value));

            value = AtErrorGeneratorErrorTypeGet(errorGenerator);
            StrToCell(tabPtr, ram_i, column++, CliRamErrorTypeToString(value));

            StrToCell(tabPtr, ram_i, column++, CliBoolToString(AtErrorGeneratorIsStarted(errorGenerator)));

            value = AtErrorGeneratorErrorNumGet(errorGenerator);
            CliCounterPrintToCell(tabPtr, ram_i, column++, value, cAtCounterTypeGood, cAtTrue);
            }
        else
            {
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdRamInternalRamErrorGeneratorModeSet(char argc, char **argv)
    {
    eAtErrorGeneratorMode errorMode = CliErrorGeneratorModeFromString(argv[1]);
    
    AtUnused(argc);
    if (errorMode == cAtErrorGeneratorModeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid mode. Expected: %s\r\n", CliValidErrorGeneratorMode());
        return cAtFalse;
        }

    return AttributeSet(argv[0], (eAtRet (*)(AtErrorGenerator, uint32))AtErrorGeneratorModeSet, errorMode);
    }

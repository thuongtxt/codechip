# @group CliAtZbt "ZBT"
# @ingroup CliAtModuleRam "RAM module"

3 zbt test addressbus CmdAtZbtAddressBusTest 1 zbtIds=1
/*
    Syntax     : zbt test addressbus <zbtIds> [<time>/<time>s/<time>m/<time>h/<time>d]
    Parameter  : zbtIds   - List of ZBT IDs need to be tested     
                 duration - Duration.
                            + <time> : duration in milliseconds
                            + <time>s: duration in second
                            + <time>m: duration in minutes
                            + <time>h: duration in hours
                            + <time>d: duration in days  
    Description: Test address bus
    Example    : zbt test addressbus 1-2
*/

3 zbt test databus CmdAtZbtDataBusTest 1 zbtIds=1
/*
    Syntax     : zbt test databus <zbtIds> [<time>/<time>s/<time>m/<time>h/<time>d]
    Parameter  : zbtIds   - List of ZBT IDs need to be tested         
                 duration - Duration.
                            + <time> : duration in milliseconds
                            + <time>s: duration in second
                            + <time>m: duration in minutes
                            + <time>h: duration in hours
                            + <time>d: duration in days  
    Description: Test data bus
    Example    : zbt test databus 1-2
*/

3 zbt test memory CmdAtZbtMemoryTest 1 zbtIds=1
/*
    Syntax     : zbt test memory <zbtIds> [<time>/<time>s/<time>m/<time>h/<time>d]
    Parameter  : zbtIds   - List of ZBT IDs need to be tested              
                 duration - Duration.
                            + <time> : duration in milliseconds
                            + <time>s: duration in second
                            + <time>m: duration in minutes
                            + <time>h: duration in hours
                            + <time>d: duration in days  
    Description: Test memory
    Example    : zbt test memory 1-2
*/

2 zbt rd CmdAtZbtRead 2 zbtIds=1 localAddress=0x0
/*
    Syntax     : zbt rd <zbtIds> <localAddress>
    Parameter  : zbtIds        - ZBT ID
                 localAddress - Local address
    Description: Read ZBT
    Example    : zbt rd 0x1
*/

2 zbt wr CmdAtZbtWrite 3 zbtIds=1 localAddress=0x0 value=0x0
/*
    Syntax     : zbt wr <zbtIds> <localAddress> <value>
    Parameter  : zbtIds        - ZBT ID
                 localAddress - Local address
                 value        - Written value. Its format is 0x.<valueWithNoDot>
    Description: Write ZBT
    Example    : zbt wr 0x1 0x3
*/

2 debug zbt CmdAtZbtDebug 1 zbtIds=1
/*
    Syntax     : debug zbt <ddrIds>
    Parameter  : zbtIds - List of ZBT IDs need to show debug information      
    Description: Show debug information of ZBT
    Example    : debug zbt 1-5
*/

3 zbt access enable CmdAtZbtAccessEnable 1 zbtIds=1
/*
    Syntax     : zbt access enable <zbtIds>
    Parameter  : zbtIds - List of ZBT IDs need to be accessed      
    Description: Enable SW read/write ZBT memory
    Example    : zbt access enable 1-2
*/

3 zbt access disable CmdAtZbtAccessDisable 1 zbtIds=1
/*
    Syntax     : zbt access disable <zbtIds>
    Parameter  : zbtIds - List of ZBT IDs need to be disabled accessing      
    Description: Disable SW read/write ZBT memory
    Example    : zbt access disable 1-2
*/

2 zbt test CmdAtZbtTest -1 zbtIds=1
/*
    Syntax     : zbt test <zbtIds>
    Parameter  : zbtIds - List of ZBT IDs need to be tested
    Description: Full RAM testing
    Options    : durationMs  : Testing duration in milliseconds.
    Example    : zbt test 1-2
*/

3 zbt test start CmdAtZbtHwTestStart 1 zbtIds
/*
    Syntax     : zbt test start <zbtIds>
    Parameter  : zbtIds      - List of ZBT IDs
    Description: Start hardware base testing.
    Example    : zbt test start 1-2
*/

3 zbt test stop CmdAtZbtHwTestStop 1 zbtIds
/*
    Syntax     : zbt test stop <zbtIds>
    Parameter  : zbtIds      - List of ZBT IDs      
    Description: Stop hardware base testing.
    Example    : zbt test stop 1-2
*/

3 show zbt test CmdAtZbtHwTestStatusShow 1 zbtIds
/*
    Syntax     : show zbt test <zbtIds>
    Parameter  : zbtIds      - List of ZBT IDs      
    Description: Show ZBT hardware test status
    Example    : show zbt test 1-2
*/

4 debug zbt bussize address CmdAtZbtDebugAddressBusSizeSet 2 zbtIds busSize
/*
    Syntax     : debug zbt bussize address <zbtIds> <busSize>
    Parameter  : zbtIds      - List of ZBT IDs     
                 busSize     - Address bus size. Set this to 0 to revert back to 
                               default bus size.
    Description: Configure address bus size for testing and debugging purpose
    Example    : debug zbt bussize address 1-2 21
                 debug zbt bussize address 1-2 0
*/

4 debug zbt bussize data CmdAtZbtDebugDataBusSizeSet 2 zbtIds busSize
/*
    Syntax     : debug zbt bussize data <zbtIds> <busSize>
    Parameter  : zbtIds      - List of ZBT IDs     
                 busSize     - Data bus size. Set this to 0 to revert back to 
                               default bus size.
    Description: Configure data bus size for testing and debugging purpose
    Example    : debug zbt bussize data 1-2 21
                 debug zbt bussize data 1-2 0
*/

2 zbt burst CmdAtZbtTestBurstSet 2 zbtIds burst
/*
    Syntax     : zbt burst <zbtIds> <burst>
    Parameter  : zbtIds      - List of ZBT IDs     
                 burst       - Number of addresses hardware can do burst read/write
    Description: Configure number of addresses hardware can do burst read/write
    Example    : zbt burst 1-2 2
                 zbt burst 1-2 15 
*/

4 zbt test hwassist enable CmdAtZbtTestHwAssistEnable 1 zbtIds
/*
    Syntax     : zbt test hwassist enable <zbtIds>
    Parameter  : zbtIds      - List of ZBT IDs     
    Description: Enable hardware assist mode to speedup memory fill and compare 
                 operations. Note, not all of products can support this.
    Example    : zbt test hwassist enable 1-2
*/

4 zbt test hwassist disable CmdAtZbtTestHwAssistDisable 1 zbtIds
/*
    Syntax     : zbt test hwassist disable <zbtIds>
    Parameter  : zbtIds      - List of ZBT IDs     
    Description: Disable hardware assist mode. Memory fill and compare operations
                 are done by software. It would take long time to complete testing.
    Example    : zbt test hwassist disable 1-2
*/

2 show zbt CmdAtZbtShow 1 zbtIds
/*
    Syntax     : show zbt <zbtIds>
    Parameter  : zbtIds      - List of ZBT IDs     
    Description: Show ZBT information
    Example    : show zbt 1-2
                 show zbt 1-2
*/

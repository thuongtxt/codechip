/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : CliAtModuleRam.h
 * 
 * Created Date: Feb 21, 2013
 *
 * Description : RAM module CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULERAM_H_
#define _CLIATMODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"
#include "AtRam.h"
#include "AtInternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam CliAtModuleRam(void);

/* Helpers */
eBool CmdAtRamBusTest(char argc, char **argv,
                      AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                      atbool isDataBusTest);

eBool CmdAtRamMemoryTest(char argc, char **argv,
                         AtRam (*RamGet)(AtModuleRam self, uint8 ramId));

eBool CmdAtRamRead(char argc, char **argv,
                   AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CmdAtRamWrite(char argc, char **argv,
                          AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CmdAtRamAccessEnable(char argc, char **argv,
                           AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                           atbool enable);

eBool CmdAtRamTest(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamCountersShow(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamAlarmShow(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamCalibMeasure(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId));

eBool CliAtRamHwTestStart(char argc, char **argv,
                          AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamHwTestStop(char argc, char **argv,
                         AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamHwTestStatusShow(char argc, char **argv,
                               AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamHwTestErrorForce(char argc, char **argv,
                               AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamHwTestErrorUnforce(char argc, char **argv,
                                 AtRam (*RamGet)(AtModuleRam self, uint8 ramId));

eBool CmdAtRamDebug(char argc, char **argv,
                    AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamDebugAddressBusSizeSet(char argc, char **argv,
                                     AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamDebugDataBusSizeSet(char argc, char **argv,
                                  AtRam (*RamGet)(AtModuleRam self, uint8 ramId));

eBool CliAtRamTestBurstSet(char argc, char **argv,
                           AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
eBool CliAtRamTestHwAssistEnable(char argc, char **argv,
                                 AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                 eBool enable);
eBool CliAtRamShow(char argc, char **argv,
                   AtRam (*RamGet)(AtModuleRam self, uint8 ramId));

eBool CmdRamErrorGeneratorAttributeSet(char *pRamIdStr,
                                       AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                       eAtRet (*AttributeSetFunc)( AtErrorGenerator, uint32),
                                       uint32 value);

eBool CmdRamErrorGeneratorNoneAttributeSet(char *pRamIdStr,
                                       AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                       eAtRet (*NoneAttributeSetFunc)( AtErrorGenerator));

eBool CmdRamErrorGeneratorErrorGeneratorShow(char argc,
                                             char **argv,
                                             AtRam (*RamGet)(AtModuleRam self, uint8 ramId));
uint32 CliRamErrorTypeFromString(const char *string);
char *CliRamErrorTypeToString(uint32 errorType);
const char *CliRamErrorTypesValidDecs(void);

#endif /* _CLIATMODULERAM_H_ */


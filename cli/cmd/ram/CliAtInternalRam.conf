# @group CliAtInternalRam "Internal RAM"
# @ingroup CliAtModuleRam "RAM module"

3 show ram internal CmdRamInternalRamShow 1 internalRams=1
/*
    Syntax     : show ram internal <internalRams>
    Parameter  : internalRams - Internal RAM list, e.g. 1,3-15
    Description: Show internal ram configuration.
    Example    : show ram internal 1-100
*/

4 show ram internal history CmdRamInternalRamInterruptShow -1 internalRams=1 readingMode=r2c
/*
    Syntax     : show ram internal history <internalRams> <readingMode> [<silent>]
    Parameter  : internalRams - Internal RAM list, e.g. 1,3-15
                 readingMode  - Reading mode
                                + ro: read-only
                                + r2c: read-to-clear
                 silent       - If "silent" is specified, there would be no 
                                output with "r2c" mode
    Description: Show internal ram history status
    Example    : show ram internal history 1-100 ro
                 show ram internal history 1-200 r2c
                 show ram internal history 1-100 r2c silent
*/

4 ram internal force error CmdRamInteralRamForceError 3 internalRams=1 errors=parity enable=en
/*
    Syntax     : ram internal force error <internalRams> <errors> <enable>
    Parameter  : internalRams  - Internal RAM list.
                 errors        - Errors to be forced/unforced.
                                 + parity: Parity error.
                 enable        - Enable to force.
                                 + en: Force error
                                 + dis: Unforce error.
    Description: Force/unforce error on internal RAMs
    Example    : ram internal force error 1 parity en
*/

4 ram internal parity monitor CmdRamInteralRamParityMonitor 2 internalRams enable=en
/*
    Syntax     : ram internal parity monitor <internalRams> <enable>
    Parameter  : internalRams  - Internal RAM list.
                 enable        - Enable/disable parity monitoring.
    Description: Enable parity checking on internal RAMs.
    Example    : ram internal parity monitor 1 en 
*/

4 ram internal ecc monitor CmdRamInteralRamEccMonitor 2 internalRams enable=en
/*
    Syntax     : ram internal ecc monitor <internalRams> <enable>
    Parameter  : internalRams  - Internal RAM list.
                 enable        - Enable/disable ECC monitoring.
    Description: Enable ECC checking on internal RAMs.
    Example    : ram internal ecc monitor 1 en 
*/

4 ram internal crc monitor CmdRamInteralRamCrcMonitor 2 internalRams enable=en
/*
    Syntax     : ram internal crc monitor <internalRams> <enable>
    Parameter  : internalRams  - Internal RAM list.
                 enable        - Enable/disable CRC monitoring.
    Description: Enable CRC checking on internal RAMs.
    Example    : ram internal crc monitor 1 en 
*/

4 show ram internal counters CmdRamInteralRamErrorCountersGet -1 internalRams=1 readingMode=r2c silent 
/*
    Syntax     : show ram internal counters <internalRams> <ro/r2c> [silent]
    Parameter  : internalRams     - Internal RAM list.
                 readingMode      - Reading mode
                                    + ro: read-only      
                                    + r2c: read-to-clear
                 silent           - If "silent" is specified, there would be no 
                                    output with "r2c" mode
    Description: Show internal ram counters
    Example    : show ram internal counters 1-100 ro
                 show ram internal counters 1-200 r2c 
*/

5 ram internal error generator type CmdRamInternalRamErrorGeneratorErrorTypeSet 2 internalRams=1 type=crc
/*
    Syntax     : ram internal error generator type <internalRams> <ecc-correctable/ecc-uncorrectable/crc>
    Parameter  : internalRams   - Internal RAM list.
                 type           - Error type
                                  + ecc-correctable
                                  + ecc-uncorrectable
    Description: Set error generating type 
    Example    : ram internal error generator type 1 ecc-correctable
*/

5 ram internal error generator errornum CmdRamInternalRamErrorGeneratorErrorNumSet 2 internalRams=1 errornum=1
/*
    Syntax     : ram internal error generator errornum <internalRams> <errornum>
    Parameter  : internalRams      - Internal RAM list.
                 errornum          - Number of error is generated
    Description: Set number of error for oneshot force error 
    Example    : ram internal error generator errornum 1 3
*/

5 ram internal error generator start CmdRamInternalRamErrorGeneratorStart 1 internalRams=1
/*
    Syntax     : ram internal error generator start <internalRams>
    Parameter  : internalRams  - Internal RAM list.
    Description: Start error generator 
    Example    : ram internal error generator start 1
*/

5 ram internal error generator stop CmdRamInternalRamErrorGeneratorStop 1 internalRams=1
/*
    Syntax     : ram internal error generator stop <internalRams>
    Parameter  : internalRams  - Internal RAM list.
    Description: Stop error generator 
    Example    : ram internal error generator stop 1
*/

5 show ram internal error generator CmdRamInternalRamErrorGeneratorShow 1 internalRams=1
/*
    Syntax     : show ram internal error generator  <internalRams>
    Parameter  : internalRams  - Internal RAM list.
    Description: Show error generator 
    Example    : show ram internal error generator 1
*/

5 ram internal error generator mode CmdRamInternalRamErrorGeneratorModeSet 2 internalRams=1 mode=oneshot
/*
    Syntax     : ram internal error generator mode <internalRams> <oneshot/continuous>
    Parameter  : internalRams - Internal RAM list.
                 mode  - generator mode
                         + oneshot
                         + continuous
    Description: Set generator mode at TX direction
    Example    : ram internal error generator mode 1 oneshot
*/

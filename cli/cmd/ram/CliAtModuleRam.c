/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : CliAtModuleRam.c
 *
 * Created Date: Feb 21, 2013
 *
 * Description : RAM module CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleRam.h"
#include "AtRam.h"
#include "AtList.h"
#include "AtTokenizer.h"
#include "CliAtModuleRam.h"
#include "../diag/CliAtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#define cSecond2Ms 1000
#define cMinute2Ms 60 * cSecond2Ms
#define cHour2Ms   60 * cMinute2Ms
#define cDay2Ms    24 * cHour2Ms

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tRamTestInfo
    {
    AtRam ram;
    eAtRet initStatus;
    eAtRet testRetCode;
    }tRamTestInfo;

typedef eAtRet (*AttributeSetFunc)(AtRam self, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TestPatternAndReadValueShow(tRamTestInfo* ramsInfo, uint32 numRams,
                                        uint32 (*ErrorCountFunc)(AtRam),
                                        AtRamTestError (*IterateFunc)(AtRam))
    {
    tTab *tabPtr;
    uint32 numRows = 0, row_i;
    uint8 i;
    const char *heading[] = {"RAM", "Address", "Expected Value", "Actual Value", "Description"};
    static char buf[32];

    /* Determine number of rows need for this table */
    for (i = 0; i < numRams; i++)
        numRows = numRows + ErrorCountFunc(ramsInfo[i].ram);

    /* No row to display */
    if (numRows == 0)
        return;

    /* Create table */
    tabPtr = TableAlloc(numRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return;
        }

    /* Show result of all RAMs */
    row_i = 0;
    for (i = 0; i < numRams; i++)
        {
        AtRamTestError anError;

        while ((anError = IterateFunc(ramsInfo[i].ram)) != NULL)
            {
            AtSprintf(buf, "%d", AtRamIdGet(ramsInfo[i].ram) + 1);
            StrToCell(tabPtr, row_i, 0, buf);

            AtSprintf(buf, "0x%08x", AtRamTestErrorAddressGet(anError));
            StrToCell(tabPtr, row_i, 1, buf);

            AtSprintf(buf, "0x%08x", AtRamTestErrorExpectedValueGet(anError));
            StrToCell(tabPtr, row_i, 2, buf);

            AtSprintf(buf, "0x%08x", AtRamTestErrorActualValueGet(anError));
            StrToCell(tabPtr, row_i, 3, buf);

            StrToCell(tabPtr, row_i, 4, AtRamTestErrorDescriptionGet(anError));
            row_i = row_i + 1;
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    }

static const char* BusTestResultToString(uint32 errorCount, eAtRet busTestResult)
    {
    /* Well-known error code */
    if ((busTestResult == cAtOk) || (busTestResult == cAtErrorMemoryTestFail))
        return (errorCount == 0) ? "PASS" : "FAIL";

    /* Other error may happen */
    return AtRet2String(busTestResult);
    }

static eAtSevLevel ColorLevelFromBusTestResult(uint32 errorCount, eAtRet busTestResult)
    {
    if ((busTestResult == cAtOk) && (errorCount == 0))
        return cSevInfo;
    return cSevCritical;
    }

static void BusTestResultShow(tRamTestInfo* ramsInfo, uint32 numRams,
                              uint32 (*ErrorCountFunc)(AtRam),
                              AtRamTestError (*IterateFunc)(AtRam))
    {
    uint32 i;
    tTab *tabPtr;
    const char *heading[] = {"RAM", "InitStatus", "Result", "ErrorCount"};
    static char buf[32];
    uint32 errorCount;

    /* No RAM to show */
    if (numRams == 0)
        {
        AtPrintc(cSevInfo, "No RAM to display\r\n");
        return;
        }

    /* Create table */
    tabPtr = TableAlloc(numRams, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return;
        }

    /* Create table content */
    for (i = 0; i < numRams; i++)
        {
        AtRam ram = ramsInfo[i].ram;

        /* RAM ID */
        AtSprintf(buf, "%d", AtRamIdGet(ram) + 1);
        StrToCell(tabPtr, i, 0, buf);

        /* Init status */
        if (ramsInfo[i].initStatus == cAtOk)
            AtSprintf(buf, "%s", "OK");
        else
            AtSprintf(buf, "%s", AtRet2String(ramsInfo[i].initStatus));
        ColorStrToCell(tabPtr, i, 1, buf, (ramsInfo[i].initStatus == cAtOk)? cSevInfo : cSevCritical);

        if (ramsInfo[i].initStatus != cAtOk)
            continue;

        /* Test Result */
        errorCount = ErrorCountFunc(ram);
        ColorStrToCell(tabPtr, i, 2, BusTestResultToString(errorCount, ramsInfo[i].testRetCode), ColorLevelFromBusTestResult(errorCount, ramsInfo[i].testRetCode));

        /* Error count */
        if ((ramsInfo[i].testRetCode == cAtOk) || (ramsInfo[i].testRetCode == cAtErrorMemoryTestFail))
            AtSprintf(buf, "%d", errorCount);
        else
            AtSprintf(buf, "%s", "xxx");
        ColorStrToCell(tabPtr, i, 3, buf, ColorLevelFromBusTestResult(errorCount, ramsInfo[i].testRetCode));
        }

    /* Show it */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Try to show fail testing detail */
    TestPatternAndReadValueShow(ramsInfo, numRams, ErrorCountFunc, IterateFunc);
    }

static void MemTestResultShow(tRamTestInfo* ramsInfo, uint32 numRams)
    {
    uint32 i;
    tTab *tabPtr;
    const char *heading[] = {"Ram", "InitStatus", "Result", "ErrorCount", "FirstErrorAddress"};
    char buf[100];
    uint32 errorCount, firstErrorAddress;

    /* No RAMs to display */
    if (numRams == 0)
        {
        AtPrintc(cSevInfo, "No RAM to display\r\n");
        return;
        }

    /* Create table */
    tabPtr = TableAlloc(numRams, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return;
        }

    AtOsalMemInit(buf, 0, 100);

    /* Create table content */
    for (i = 0; i < numRams; i++)
        {
        AtRam ram = ramsInfo[i].ram;

        /* RAM ID */
        AtSprintf(buf, "%d", AtRamIdGet(ram) + 1);
        StrToCell(tabPtr, i, 0, buf);

        /* Init status */
        if (ramsInfo[i].initStatus == cAtOk)
            AtSprintf(buf, "%s", "OK");
        else
            AtSprintf(buf, "%s", AtRet2String(ramsInfo[i].initStatus));
        ColorStrToCell(tabPtr, i, 1, buf, (ramsInfo[i].initStatus == cAtOk)? cSevInfo : cSevCritical);

        if (ramsInfo[i].initStatus != cAtOk)
            continue;

        /* Result */
        if (ramsInfo[i].testRetCode == cAtOk)
            {
            errorCount = AtRamMemoryTestErrorCount(ram, &firstErrorAddress);
            AtSprintf(buf, "%s", (errorCount == 0)? "PASS" : "FAILED");
            ColorStrToCell(tabPtr, i, 2, buf, (errorCount == 0)? cSevInfo : cSevCritical);

            /* Error Count */
            AtSprintf(buf, "%u", errorCount);
            ColorStrToCell(tabPtr, i, 3, buf, (errorCount == 0)? cSevNormal : cSevCritical);
            if (errorCount == 0)
                continue;

            /* First Error Address */
            AtSprintf(buf, "0x%X (Bank: %u, Row: %u, Column: %u)", firstErrorAddress,
                      AtRamCellBankGet  (ram, firstErrorAddress),
                      AtRamCellRowGet   (ram, firstErrorAddress),
                      AtRamCellColumnGet(ram, firstErrorAddress));
            ColorStrToCell(tabPtr, i, 4, buf, cSevCritical);
            }
        else
            {
            ColorStrToCell(tabPtr, i, 2, AtRet2String(ramsInfo[i].testRetCode), cSevCritical);
            ColorStrToCell(tabPtr, i, 3, "xxx", cSevCritical);
            ColorStrToCell(tabPtr, i, 4, "xxx", cSevCritical);
            }
        }

    /* Show it */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    }

static tRamTestInfo* RamTestInfoAlloc(uint32 *ramIds, uint32 numInputRam,
                                      AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                      uint32 *numValidRam)
    {
    uint32 i, validRamCount;
    tRamTestInfo* ramsInfo;

    validRamCount = 0;
    for (i = 0; i < numInputRam; i++)
        {
        if (RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1)))
            validRamCount = validRamCount + 1;
        }

    if (validRamCount == 0)
        return NULL;

    ramsInfo = AtOsalMemAlloc(sizeof(tRamTestInfo) * validRamCount);
    if (ramsInfo == NULL)
        return NULL;

    AtOsalMemInit(ramsInfo, 0, validRamCount * sizeof(tRamTestInfo));
    *numValidRam = validRamCount;
    return ramsInfo;
    }

static eBool RamHwTestStart(char argc, char **argv,
                            eBool start,
                            AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numRams; i++)
        {
        /* Ignore RAM that does not exist */
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        eAtRet ret;
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[i]);
            continue;
            }

        if (start)
            ret = AtRamHwTestStart(ram);
        else
            ret = AtRamHwTestStop(ram);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot %s hardware base testing on RAM %u, ret = %s\r\n",
                     start ? "start" : "stop",
                     ramIds[i],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static uint32 DurationInMsFromString(char *durationString)
    {
    uint32 unitInMs = 1;
    char lastChar;
    uint32 lastCharIndex;

    if (AtStrlen(durationString) == 0)
        return 0;

    lastCharIndex = AtStrlen(durationString) - 1;
    lastChar = durationString[lastCharIndex];
    switch (lastChar)
        {
        case 's': unitInMs = cSecond2Ms; break;
        case 'm': unitInMs = cMinute2Ms; break;
        case 'h': unitInMs = cHour2Ms;   break;
        case 'd': unitInMs = cDay2Ms;    break;
        default:
            break;
        }

    if (unitInMs != 1)
        durationString[lastCharIndex] = '\0';

    return AtStrToDw(durationString) * unitInMs;
    }

eBool CmdAtRamBusTest(char argc, char **argv,
                      AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                      atbool isDataBusTest)
    {
    uint32 numRams, i, ramCount;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    uint32 durationMs = 0;
    tRamTestInfo* ramsInfo;

    tAtOsalCurTime stopTime;
    tAtOsalCurTime startTime;
    uint32 elapsedTime_us;

    AtOsalMemInit(&stopTime, sizeof(tAtOsalCurTime), 0);
    AtOsalMemInit(&startTime, sizeof(tAtOsalCurTime), 0);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    if (argc > 1)
        durationMs = DurationInMsFromString(argv[1]);

    /* Need some memories */
    ramsInfo = RamTestInfoAlloc(ramIds, numRams, RamGet, &ramCount);
    if (ramsInfo == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No valid RAM to test or allocate memory fail\r\n");
        return cAtTrue;
        }

    /* Test all RAMs */
    ramCount = 0;
    AtOsalCurTimeGet(&startTime);
    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        if (ram == NULL)
            continue;

        ramsInfo[ramCount].ram = ram;
        ramsInfo[ramCount].initStatus = AtRamInitStatusGet(ram);
        if (ramsInfo[ramCount].initStatus == cAtOk)
            {
            if (isDataBusTest)
                ramsInfo[ramCount].testRetCode = AtRamDataBusTestWithDuration(ram, durationMs);
            else
                ramsInfo[ramCount].testRetCode = AtRamAddressBusTestWithDuration(ram, durationMs);
            }

        ramCount++;
        }

    AtOsalCurTimeGet(&stopTime);
    BusTestResultShow(ramsInfo, ramCount,
                     (isDataBusTest) ? AtRamDataBusTestErrorCount : AtRamAddressBusTestErrorCount,
                     (isDataBusTest) ? AtRamDataBusTestNextError : AtRamAddressBusTestNextError);
    if (ramCount > 0)
        {
        elapsedTime_us = mTimeIntervalInUsGet(startTime, stopTime) / ramCount;
        AtPrintc(cSevInfo, "Average elapse time for each RAM: %s\n", CliTimeInUsToString(elapsedTime_us, NULL, 0));
        }

    AtOsalMemFree(ramsInfo);
    return cAtTrue;
    }

eBool CmdAtRamDebug(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
	AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));

        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: RAM %u does not exist\r\n", ramIds[i]);
            continue;
            }

        AtPrintc(cSevInfo, "* Ram: %u\r\n", ramIds[i]);
        AtRamDebug(ram);
        }

    return cAtTrue;
    }

eBool CmdAtRamMemoryTest(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i, ramCount;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    uint32 durationMs = 0;
    tRamTestInfo* ramsInfo;

    tAtOsalCurTime stopTime;
    tAtOsalCurTime startTime;
    uint32 elapsedTime_us;

    AtOsalMemInit(&stopTime, sizeof(tAtOsalCurTime), 0);
    AtOsalMemInit(&startTime, sizeof(tAtOsalCurTime), 0);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    if (argc > 1)
        durationMs = DurationInMsFromString(argv[1]);

    /* Allocate and initialize memory */
    ramsInfo = RamTestInfoAlloc(ramIds, numRams, RamGet, &ramCount);
    if (ramsInfo == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No valid RAM to test or allocate memory fail\r\n");
        return cAtTrue;
        }

    /* Test */
    AtOsalCurTimeGet(&startTime);
    ramCount = 0;
    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        if (ram == NULL)
            continue;

        ramsInfo[ramCount].ram = ram;
        ramsInfo[ramCount].initStatus = AtRamInitStatusGet(ram);
        if (ramsInfo[ramCount].initStatus == cAtOk)
            ramsInfo[ramCount].testRetCode = AtRamMemoryTestWithDuration(ram, durationMs);

        ramCount++;
        }
    AtOsalCurTimeGet(&stopTime);
    MemTestResultShow(ramsInfo, ramCount);
    if (ramCount > 0)
        {
        elapsedTime_us = mTimeIntervalInUsGet(startTime, stopTime) / ramCount;
        AtPrintc(cSevInfo, "Average elapse time for each RAM: %s\n", CliTimeInUsToString(elapsedTime_us, NULL, 0));
        }

    AtOsalMemFree(ramsInfo);
    return cAtTrue;
    }

eBool CmdAtRamRead(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    AtRam ram;
    uint8 ramId;
    uint32 address;
    uint32 value[10];
    uint32 numValue;
    char str[256];
    char buf[20];
    int i;
    eAtRet ret;
	AtUnused(argc);

    /* Get RAM */
    ramId = (uint8)AtStrtoul(argv[0], NULL, 10);
    if (ramId == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid RAM ID, expected 1..N\r\n");
        return cAtFalse;
        }
    ramId = (uint8)(ramId - 1);

    /* Get address */
    StrToDw(argv[1], 'h', &address);

    /* Get the RAM */
    ram = RamGet(CliAtModuleRam(), ramId);
    if (ram == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid RAM ID %s\r\n", argv[0]);
        return cAtFalse;
        }

    /* Check if RAM accessing is enabled */
    if (!AtRamAccessIsEnabled(ram))
        {
        AtPrintc(cSevCritical, "ERROR: RAM accessing has not enabled yet\r\n");
        return cAtFalse;
        }

    /* Read it */
    ret = AtRamCellRead(ram, address, value);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Read memory fail, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    /* Show value */
    AtOsalMemInit(buf, 0, 20);
    AtOsalMemInit(str, 0, 256);
    AtStrcat(str, "Read value: 0x");
    numValue = AtRamCellSizeGet(ram);
    for (i = (int)(numValue - 1); i > 0 ; i--)
        {
        AtSprintf(buf, "%08x.", value[i]);
        AtStrcat(str, buf);
        }

    AtSprintf(buf, "%08x", value[0]);
    AtStrcat(str, buf);
    AtPrintc(cSevNormal, "%s\r\n", str);

    return cAtTrue;
    }

static uint32 ValueGet(char *str, uint32 *value, uint32 numValue)
    {
    uint32 dword_i;
    uint32 numDwords = 0;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(str, ".");

    dword_i = AtTokenizerNumStrings(tokenizer) - 1;
    while (AtTokenizerHasNextString(tokenizer))
        {
        char *valueString = AtTokenizerNextString(tokenizer);
        if (dword_i >= numValue)
            continue;

        value[dword_i] = AtStrtoul(valueString, (char**)NULL, 16);
        numDwords = numDwords + 1;
        dword_i = dword_i - 1;
        }

    return numDwords;
    }

static uint32 CounterValueFromCounterType(AtRam self, eBool r2c, uint16 counterType)
    {
    if (r2c)
        return AtRamCounterClear(self, counterType);
    return AtRamCounterGet(self, counterType);
    }

static const char *AlarmToString(AtRam self, uint32 hwAlarmValue, uint32 alarmType)
    {
    if (!AtRamAlarmIsSupported(self, alarmType))
        return "N/A";

    if ((hwAlarmValue & cAtRamAlarmEccCorrectable) ||
        (hwAlarmValue & cAtRamAlarmEccUnnorrectable) ||
        (hwAlarmValue & cAtRamAlarmCrcError))
        return "set";
    return "clear";
    }

static eBool BusSizeSet(char argc, char **argv,
                        AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                        eAtRet (*BusSizeSetFunc)(AtRam self, uint32 busSize))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    uint32 busSize;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    busSize = AtStrToDw(argv[1]);

    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        eAtRet ret;

        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: RAM %u does not exist, ignore it\r\n", ramIds[i]);
            continue;
            }

        ret = BusSizeSetFunc(ram, busSize);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot change bus size on RAM %u, ret = %s\r\n",
                     ramIds[i],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool HwTestErrorForce(char argc, char **argv,
                              eBool forced,
                              AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to force/unforce error\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        eAtRet ret;

        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: RAM %u does not exist, ignore it\r\n", ramIds[i]);
            continue;
            }

        ret = AtRamHwTestErrorForce(ram, forced);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: %s error RAM %u fail, ret = %s\r\n",
                     forced ? "Force" : "Unforce",
                     ramIds[i],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool RamAttributeSet(char *ramIdsString,
                             uint32 value,
                             AttributeSetFunc func,
                             AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;

    /* Get RAMs */
    numRams = CliIdListFromString(ramIdsString, ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to configure\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        eAtRet ret;

        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: RAM %u does not exist, ignore it\r\n", ramIds[i]);
            continue;
            }

        ret = func(ram, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure RAM %u, ret = %s\r\n",
                     ramIds[i],
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool HelperCalibMeasure(uint32 *ramIds, uint32 numRams, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    tTab *tabPtr;
    const char *pHeading[] = {"RAM", "Calibration time (ms)"};
    uint32 i;

    tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliNumber2String(ramIds[i], "%u"));

        /* Invalid RAM */
        if (ram == NULL)
            {
            for (column = 1; column < mCount(pHeading); column++)
                ColorStrToCell(tabPtr, i, column, "Error", cSevCritical);
            continue;
            }

        StrToCell(tabPtr, i, column++, CliNumber2String(AtRamCalibMeasure(ram), "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliAtModuleRam(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static void ErrorReporting(AtInternalRam ram, uint32 errors, void* userContext)
    {
    AtUnused(userContext);
    AtUnused(errors);
    AtPrintc(cSevNormal, "\r\n%s [RAM] Parity error raised at ID %u, Desc \"%s\"",
             AtOsalDateTimeInUsGet(), (AtInternalRamIdGet(ram) + 1U), AtInternalRamDescription(ram));
    AtPrintc(cSevNormal, "\r\n");
    }

static eBool InterruptCapture(char argc, char **argv, eBool enable)
    {
    static tAtModuleRamEventListener listener;
    eAtRet ret;
    AtUnused(argv);
    AtUnused(argc);

    listener.ErrorNotify = ErrorReporting;
    if (enable)
        ret = AtModuleEventListenerAdd((AtModule)CliAtModuleRam(), &listener, NULL);
    else
        ret = AtModuleEventListenerRemove((AtModule)CliAtModuleRam(), &listener);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ShowLatchedErrors(AtList rams)
    {
    tTab *tabPtr;
    uint32 numRams = AtListLengthGet(rams);
    uint32 row = 0;
    uint32 ram_i;
    const char *pHeadings[] = {"RamId", ""};
    const char *rowTitles[] = {"LatchedErrorAddress", "LatchedErrorData", "LatchedExpectedData", "LatchedErrorDataBits"};

    if (AtListLengthGet(rams) == 0)
        return cAtTrue;

    /* Create table with titles */
    tabPtr = TableAlloc(4, mCount(pHeadings), pHeadings);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Print latched error types */
    for (row = 0; row < mCount(rowTitles); row++)
        StrToCell(tabPtr, row, 0, rowTitles[row]);

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        static const uint32 column = 1;
        AtRam ram = (AtRam)AtListObjectGet(rams, ram_i);
        const uint32 *data;
        uint32 numDwords;

        TableColumnHeaderSet(tabPtr, 1, CliNumber2String(AtRamIdGet(ram) + 1, "%u"));

        row = 0;
        StrToCell(tabPtr, row++, column, CliNumber2String(AtRamLatchedErrorAddress(ram), "%08x"));

        data = AtRamLatchedErrorData(ram, &numDwords);
        StrToCell(tabPtr, row++, column, CliDwordArrayToString(data, numDwords));

        data = AtRamLatchedExpectedData(ram, &numDwords);
        StrToCell(tabPtr, row++, column, CliDwordArrayToString(data, numDwords));

        data = AtRamLatchedErrorDataBits(ram, &numDwords);
        StrToCell(tabPtr, row++, column, CliDwordArrayToString(data, numDwords));

        AtPrintc(cSevWarning, "\r\n");
        AtPrintc(cSevWarning, "* Latched error information of %s (values are in hex):\r\n", AtObjectToString((AtObject)ram));
        TablePrint(tabPtr);
        }

    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtRamWrite(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    AtRam ram;
    uint8 ramId;
    uint32 address;
    uint32 value[10];
    eAtRet ret;
    uint32 numDwords;

	AtUnused(argc);

    /* Get RAM */
    ramId = (uint8)AtStrtoul(argv[0], NULL, 10);
    if (ramId == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid RAM ID, expected 1..N\r\n");
        return cAtFalse;
        }
    ramId = (uint8)(ramId - 1);

    /* Get address and value */
    StrToDw(argv[1], 'h', &address);

    /* Get the RAM */
    ram = RamGet(CliAtModuleRam(), ramId);
    if (ram == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid RAM ID %s\r\n", argv[0]);
        return cAtFalse;
        }

    /* Check if RAM accessing is enabled */
    if (!AtRamAccessIsEnabled(ram))
        {
        AtPrintc(cSevCritical, "ERROR: RAM accessing has not enabled yet\r\n");
        return cAtFalse;
        }

    /* Write value */
    AtOsalMemInit(value, 0, sizeof(uint32) * 10);
    numDwords = ValueGet(argv[2], value, AtRamCellSizeGet(ram));
    if (numDwords < AtRamCellSizeGet(ram))
        {
        AtPrintc(cSevCritical, "ERROR: Data must be %u dwords\r\n", AtRamCellSizeGet(ram));
        return cAtFalse;
        }

    ret = AtRamCellWrite(ram, address, value);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Write memory fail, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtRamAccessEnable(char argc, char **argv,
                           AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                           atbool enable)
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    for (i = 0; i < numRams; i++)
        {
        /* Ignore RAM that does not exist */
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        eAtRet ret;
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[i]);
            continue;
            }

        /* Access it */
        ret = AtRamAccessEnable(ram, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot access RAM %u, ret = %s\r\n", ramIds[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtRamTest(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, numValidRams, ram_i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    uint32 durationMs = 0;
    tRamTestInfo* ramsInfo;

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    if (argc > 1)
        durationMs = DurationInMsFromString(argv[1]);

    /* Allocate and initialize memory */
    ramsInfo = RamTestInfoAlloc(ramIds, numRams, RamGet, &numValidRams);
    if (ramsInfo == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No valid RAM to test or allocate memory fail\r\n");
        return cAtTrue;
        }

    numValidRams = 0;
    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            continue;


        ramsInfo[numValidRams].ram = ram;
        ramsInfo[numValidRams].initStatus = AtRamInitStatusGet(ram);
        if (ramsInfo[numValidRams].initStatus == cAtOk)
            ramsInfo[numValidRams].testRetCode |= AtRamTestWithDuration(ram, durationMs);

        numValidRams++;
        }

    BusTestResultShow(ramsInfo, numValidRams, AtRamDataBusTestErrorCount, AtRamDataBusTestNextError);
    BusTestResultShow(ramsInfo, numValidRams, AtRamAddressBusTestErrorCount, AtRamAddressBusTestNextError);
    MemTestResultShow(ramsInfo, numValidRams);

    AtOsalMemFree(ramsInfo);
    return cAtTrue;
    }

AtModuleRam CliAtModuleRam(void)
    {
    return (AtModuleRam)AtDeviceModuleGet(CliDevice(), cAtModuleRam);
    }

eBool CliAtRamCountersShow(char argc, char **argv,
                           AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    tTab *tabPtr = NULL;
    uint32 *ramIds;
    uint32 numRams, ram_i;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    const char *pHeading[] =
            {"RamId",
             "ErrorCorr",
             "ErrorUCorr",
             "ErrorCrc",
             "Read",
             "Write",
             "Error"};
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get RAMs */
    ramIds = CliSharedIdBufferGet(&numRams);
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint8 column = 0;
        uint32 counterValue;
        eBool isSupportedCounter = cAtTrue;
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[ram_i]);
            continue;
            }

        StrToCell(tabPtr, ram_i, column++, CliNumber2String(ramIds[ram_i], "%u"));

        isSupportedCounter = AtRamCounterIsSupported(ram, cAtRamCounterTypeEccCorrectableErrors);
        counterValue = CounterValueFromCounterType(ram, readingMode, cAtRamCounterTypeEccCorrectableErrors);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue);

        isSupportedCounter = AtRamCounterIsSupported(ram, cAtRamCounterTypeEccUncorrectableErrors);
        counterValue = CounterValueFromCounterType(ram, readingMode, cAtRamCounterTypeEccUncorrectableErrors);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue);

        isSupportedCounter = AtRamCounterIsSupported(ram, cAtRamCounterTypeCrcErrors);
        counterValue = CounterValueFromCounterType(ram, readingMode, cAtRamCounterTypeCrcErrors);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue);

        isSupportedCounter = AtRamCounterIsSupported(ram, cAtRamCounterTypeRead);
        counterValue = CounterValueFromCounterType(ram, readingMode, cAtRamCounterTypeRead);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue);

        isSupportedCounter = AtRamCounterIsSupported(ram, cAtRamCounterTypeWrite);
        counterValue = CounterValueFromCounterType(ram, readingMode, cAtRamCounterTypeWrite);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue);

        isSupportedCounter = AtRamCounterIsSupported(ram, cAtRamCounterTypeErrors);
        counterValue = CounterValueFromCounterType(ram, readingMode, cAtRamCounterTypeErrors);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtRamAlarmShow(char argc, char **argv,
                        AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    tTab *tabPtr;
    uint32 *ramIds;
    uint32 numRams, ram_i;
    const char *pHeading[] = {"RamId", "ErrorCorr", "ErrorUCorr", "ErrorCrc"};

    AtUnused(argc);
    /* Get RAMs */
    ramIds = CliSharedIdBufferGet(&numRams);
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to test\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint8 column = 0;
        uint32 alarmValue;
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[ram_i]);
            continue;
            }

        alarmValue = AtRamAlarmGet(ram);
        StrToCell(tabPtr, ram_i, column++, CliNumber2String(ramIds[ram_i], "%u"));
        ColorStrToCell(tabPtr, ram_i, column++, AlarmToString(ram, alarmValue, cAtRamAlarmEccCorrectable),  alarmValue ? cSevCritical : cSevInfo);
        ColorStrToCell(tabPtr, ram_i, column++, AlarmToString(ram, alarmValue, cAtRamAlarmEccUnnorrectable),alarmValue ? cSevCritical : cSevInfo);
        ColorStrToCell(tabPtr, ram_i, column++, AlarmToString(ram, alarmValue, cAtRamAlarmCrcError),        alarmValue ? cSevCritical : cSevInfo);
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtRamHwTestStart(char argc, char **argv,
                          AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    return RamHwTestStart(argc, argv, cAtTrue, RamGet);
    }

eBool CliAtRamHwTestStop(char argc, char **argv,
                         AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    return RamHwTestStart(argc, argv, cAtFalse, RamGet);
    }

eBool CliAtRamHwTestErrorForce(char argc, char **argv,
                               AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    return HwTestErrorForce(argc, argv, cAtTrue, RamGet);
    }

eBool CliAtRamHwTestErrorUnforce(char argc, char **argv,
                                 AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    return HwTestErrorForce(argc, argv, cAtFalse, RamGet);
    }

eBool CliAtRamHwTestStatusShow(char argc, char **argv,
                               AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    AtList failRams;
    tTab *tabPtr;
    uint32 *ramIds;
    uint32 numRams, ram_i;
    const char *pHeading[] =
            {"RamId",
             "InitStatus",
             "ErrorForced",
             "State",
             "Read",
             "Write",
             "Error",
             "Status"};

    AtUnused(argc);

    /* Get RAMs */
    ramIds = CliSharedIdBufferGet(&numRams);
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to show information\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    failRams = AtListCreate(0);
    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        eAtRet ret;
        uint8 column = 0;
        uint32 counterValue;
        eBool boolValue = cAtTrue;
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        const char *stringValue;

        StrToCell(tabPtr, ram_i, column++, CliNumber2String(ramIds[ram_i], "%u"));

        if (ram == NULL)
            {
            for (; column < mCount(pHeading); column++)
                ColorStrToCell(tabPtr, ram_i, column, "Error", cSevCritical);
            continue;
            }

        /* If calib fail, need to hide all of other fields */
        ret = AtRamInitStatusGet(ram);
        stringValue = (ret == cAtOk) ? "OK" : AtRet2String(ret);
        ColorStrToCell(tabPtr, ram_i, column++, stringValue, (ret == cAtOk) ? cSevInfo : cSevCritical);
        if (ret != cAtOk)
            {
            for (; column < mCount(pHeading); column++)
                StrToCell(tabPtr, ram_i, column, sAtNotCare);
            continue;
            }

        /* Error force */
        boolValue = AtRamHwTestErrorIsForced(ram);
        ColorStrToCell(tabPtr, ram_i, column++, CliBoolToString(boolValue), boolValue ? cSevCritical : cSevNormal);

        boolValue = AtRamHwTestIsStarted(ram);
        ColorStrToCell(tabPtr, ram_i, column++, boolValue ? "Started" : "Stopped", boolValue ? cSevInfo : cSevCritical);

        boolValue = AtRamCounterIsSupported(ram, cAtRamCounterTypeRead);
        counterValue = CounterValueFromCounterType(ram, cAtTrue, cAtRamCounterTypeRead);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, boolValue ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue);

        boolValue = AtRamCounterIsSupported(ram, cAtRamCounterTypeWrite);
        counterValue = CounterValueFromCounterType(ram, cAtTrue, cAtRamCounterTypeWrite);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, boolValue ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue);

        boolValue = AtRamCounterIsSupported(ram, cAtRamCounterTypeErrors);
        counterValue = CounterValueFromCounterType(ram, cAtTrue, cAtRamCounterTypeErrors);
        CliCounterPrintToCell(tabPtr, ram_i, column++, counterValue, boolValue ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue);

        /* If RAM is bad, put it to the fail list to display latched errors later */
        boolValue = AtRamIsGood(ram);
        ColorStrToCell(tabPtr, ram_i, column++, boolValue ? "good" : "bad", boolValue ? cSevInfo : cSevCritical);
        if ((!boolValue) && AtRamErrorLatchingSupported(ram))
            AtListObjectAdd(failRams, (AtObject)ram);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Show latched errors on RAMs that have PRBS test fail */
    ShowLatchedErrors(failRams);
    AtObjectDelete((AtObject)failRams);

    return cAtTrue;
    }

eBool CliAtRamDebugAddressBusSizeSet(char argc, char **argv,
                                     AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    return BusSizeSet(argc, argv, RamGet, AtRamDebugAddressBusSizeSet);
    }

eBool CliAtRamDebugDataBusSizeSet(char argc, char **argv,
                                  AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    return BusSizeSet(argc, argv, RamGet, AtRamDebugDataBusSizeSet);
    }

eBool CliAtRamTestBurstSet(char argc, char **argv,
                           AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    AtUnused(argc);
    return RamAttributeSet(argv[0], AtStrToDw(argv[1]), AtRamTestBurstSet, RamGet);
    }

eBool CliAtRamTestHwAssistEnable(char argc, char **argv,
                                 AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                 eBool enable)
    {
    AtUnused(argc);
    return RamAttributeSet(argv[0], enable, (AttributeSetFunc)AtRamTestHwAssistEnable, RamGet);
    }

eBool CliAtRamShow(char argc, char **argv,
                   AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;
    tTab *tabPtr;
    const char *pHeading[] = {"RAM", "AddrBusSize", "DataBusSize", "CellSize(bits)",
                              "Tested", "Good", "MaxBusrt", "Burst", "Accessed", "HwAssist"};

    AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numRams; i++)
        {
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[i] - 1));
        uint8 column = 0;
        eBool boolVal;

        StrToCell(tabPtr, i, column++, CliNumber2String(ramIds[i], "%u"));

        /* Invalid RAM */
        if (ram == NULL)
            {
            for (column = 1; column < mCount(pHeading); column++)
                ColorStrToCell(tabPtr, i, column, "Error", cSevCritical);
            continue;
            }

        StrToCell(tabPtr, i, column++, CliNumber2String(AtRamAddressBusSizeGet(ram), "%u"));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtRamDataBusSizeGet(ram), "%u"));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtRamCellSizeInBits(ram), "%u"));

        /* Tested status */
        boolVal = AtRamIsTested(ram);
        ColorStrToCell(tabPtr, i, column++, boolVal ? "yes" : "no", boolVal ? cSevInfo : cSevWarning);
        if (AtRamIsTested(ram))
            {
            boolVal = AtRamIsGood(ram);
            ColorStrToCell(tabPtr, i, column++, boolVal ? "good" : "bad", boolVal ? cSevInfo : cSevCritical);
            }
        else
            StrToCell(tabPtr, i, column++, "xxx");

        /* Burst */
        if (AtRamTestBurstIsSupported(ram))
            {
            StrToCell(tabPtr, i, column++, CliNumber2String(AtRamTestMaxBurst(ram), "%u"));
            StrToCell(tabPtr, i, column++, CliNumber2String(AtRamTestBurstGet(ram), "%u"));
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotSupported);
            StrToCell(tabPtr, i, column++, sAtNotSupported);
            }

        StrToCell(tabPtr, i, column++, CliBoolToString(AtRamAccessIsEnabled(ram)));

        boolVal = AtRamTestHwAssistIsEnabled(ram);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(boolVal), boolVal ? cSevInfo : cSevWarning);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CliAtRamCalibMeasure(char argc, char **argv, AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    uint32 measureTimes = 1;

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to display\r\n");
        return cAtTrue;
        }

    /* Get measure times */
    if (argc == 2)
        StrToDw(argv[1], 'd', &measureTimes);

    for (i = 0; i < measureTimes; i++)
        HelperCalibMeasure(ramIds, numRams, RamGet);

    return cAtTrue;
    }

eBool CmdAtModuleRamShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Attribute", "Value"};
    AtModuleRam ramModule = CliAtModuleRam();
    uint32 row = 0;
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(5, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)ramModule);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));

    row++;
    StrToCell(tabPtr, row, 0, "NumDDRs");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleRamNumDdrGet(ramModule), "%u"));

    row++;
    StrToCell(tabPtr, row, 0, "NumQDRs");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleRamNumQdrGet(ramModule), "%u"));

    row++;
    StrToCell(tabPtr, row, 0, "NumZBTs");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleRamNumZbtGet(ramModule), "%u"));

    row++;
    StrToCell(tabPtr, row, 0, "NumInternalRAMs");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleRamNumInternalRams(ramModule), "%u"));

    /* IMPORTANT: to add new rows, developer must update the above TableAlloc
     * calling by increasing number of rows to be created */

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdRamInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdRamInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdRamInterruptCaptureEnable(char argc, char **argv)
    {
    return InterruptCapture(argc, argv, cAtTrue);
    }

eBool CmdRamInterruptCaptureDisable(char argc, char **argv)
    {
    return InterruptCapture(argc, argv, cAtFalse);
    }

eBool CmdRamErrorGeneratorAttributeSet(char *pRamIdStr,
                                       AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                       eAtRet (*ErrorGenAttributeSetFunc)( AtErrorGenerator, uint32),
                                       uint32 value)
    {
    uint32 numRams, ram_i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;

    /* Get RAMs */
    numRams = CliIdListFromString(pRamIdStr, ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to display\r\n");
        return cAtTrue;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        eAtRet ret;
        AtErrorGenerator errorGenerator;
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[ram_i]);
            continue;
            }

        errorGenerator = AtRamErrorGeneratorGet(ram);
        if (errorGenerator != NULL)
            {
            ret = ErrorGenAttributeSetFunc(errorGenerator, value);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: AttributeSet-RAM %d with ret = %s\r\n", ramIds[ram_i], AtRet2String(ret));
                success = cAtFalse;
                }
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: This attribute is not support for Internal-RAM %d\r\n", ramIds[ram_i]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdRamErrorGeneratorNoneAttributeSet(char *pRamIdStr,
                                       AtRam (*RamGet)(AtModuleRam self, uint8 ramId),
                                       eAtRet (*ErrorGenNoneAttributeSetFunc)( AtErrorGenerator))
    {
    uint32 numRams, ram_i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;

    /* Get RAMs */
    numRams = CliIdListFromString(pRamIdStr, ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to display\r\n");
        return cAtTrue;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        eAtRet ret;
        AtErrorGenerator errorGenerator;
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[ram_i]);
            continue;
            }

        errorGenerator = AtRamErrorGeneratorGet(ram);
        if (errorGenerator != NULL)
            {
            ret = ErrorGenNoneAttributeSetFunc(errorGenerator);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: AttributeSet-RAM %d with ret = %s\r\n", ramIds[ram_i], AtRet2String(ret));
                success = cAtFalse;
                }
            }
        else
            {
            AtPrintc(cSevCritical, "ERROR: This attribute is not support for Internal-RAM %d\r\n", ramIds[ram_i]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdRamErrorGeneratorErrorGeneratorShow(char argc,
                                             char **argv,
                                             AtRam (*RamGet)(AtModuleRam self, uint8 ramId))
    {
    uint32 numRams, ram_i;
    uint32 *ramIds = CliSharedIdBufferGet(&numRams);
    eBool success = cAtTrue;
    tTab *tabPtr;
    const char *pHeading[] =  { "RamId", "gen-mode", "error-type", "is-started", "error-num"};

    AtUnused(argc);

    /* Get RAMs */
    numRams = CliIdListFromString(argv[0], ramIds, numRams);
    if (numRams == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No RAM to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numRams, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }

    for (ram_i = 0; ram_i < numRams; ram_i++)
        {
        uint32 value;
        AtErrorGenerator errorGenerator;
        uint8 column = 0;
        AtRam ram = RamGet(CliAtModuleRam(), (uint8)(ramIds[ram_i] - 1));
        if (ram == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ram %u does not exist\r\n", ramIds[ram_i]);
            success = cAtFalse;
            continue;
            }

        errorGenerator = AtRamErrorGeneratorGet(ram);
        StrToCell(tabPtr, ram_i, column++, CliNumber2String(AtRamIdGet(ram) + 1, "%u"));
        if (errorGenerator)
            {
            value = AtErrorGeneratorModeGet(errorGenerator);
            StrToCell(tabPtr, ram_i, column++, CliErrorGeneratorModeString(value));

            value = AtErrorGeneratorErrorTypeGet(errorGenerator);
            StrToCell(tabPtr, ram_i, column++, CliRamErrorTypeToString(value));

            StrToCell(tabPtr, ram_i, column++, CliBoolToString(AtErrorGeneratorIsStarted(errorGenerator)));

            value = AtErrorGeneratorErrorNumGet(errorGenerator);
            CliCounterPrintToCell(tabPtr, ram_i, column++, value, cAtCounterTypeGood, cAtTrue);
            }
        else
            {
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            StrToCell(tabPtr, ram_i, column++, sAtNotSupported);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CmdAtModuleRamDebug(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cAtModuleRam));
    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DDR
 *
 * File        : CliAtModuleRam.c
 *
 * Created Date: Feb 1, 2013
 *
 * Description : DDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleRam.h"

#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtDdrAddressBusTest(char argc, char **argv)
    {
    return CmdAtRamBusTest(argc, argv, AtModuleRamDdrGet, cAtFalse);
    }

eBool CmdAtDdrDataBusTest(char argc, char **argv)
    {
    return CmdAtRamBusTest(argc, argv, AtModuleRamDdrGet, cAtTrue);
    }

eBool CmdAtDdrMemoryTest(char argc, char **argv)
    {
    return CmdAtRamMemoryTest(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrRead(char argc, char **argv)
    {
    return CmdAtRamRead(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrWrite(char argc, char **argv)
    {
    return CmdAtRamWrite(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrDebug(char argc, char **argv)
    {
    return CmdAtRamDebug(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrAccessEnable(char argc, char **argv)
    {
    return CmdAtRamAccessEnable(argc, argv, AtModuleRamDdrGet, cAtTrue);
    }

eBool CmdAtDdrAccessDisable(char argc, char **argv)
    {
    return CmdAtRamAccessEnable(argc, argv, AtModuleRamDdrGet, cAtFalse);
    }

eBool CmdAtDdrTest(char argc, char **argv)
    {
    return CmdAtRamTest(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrMarginsDetect(char argc, char **argv)
    {
    /* TODO: Handle this */
    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevCritical, "ERROR: not implemented\r\n");
    return cAtFalse;
    }

eBool CmdAtDdrCountersShow(char argc, char **argv)
    {
    return CliAtRamCountersShow(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrAlarmShow(char argc, char **argv)
    {
    return CliAtRamAlarmShow(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrAlarmHistoryShow(char argc, char **argv)
    {
    /* TODO: Handle this */
    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevCritical, "ERROR: not implemented\r\n");
    return cAtFalse;
    }

eBool CmdAtDdrHwTestStart(char argc, char **argv)
    {
    return CliAtRamHwTestStart(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrHwTestStop(char argc, char **argv)
    {
    return CliAtRamHwTestStop(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrHwTestStatusShow(char argc, char **argv)
    {
    return CliAtRamHwTestStatusShow(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrDebugAddressBusSizeSet(char argc, char **argv)
    {
    return CliAtRamDebugAddressBusSizeSet(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrDebugDataBusSizeSet(char argc, char **argv)
    {
    return CliAtRamDebugDataBusSizeSet(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrTestBurstSet(char argc, char **argv)
    {
    return CliAtRamTestBurstSet(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrTestHwAssistEnable(char argc, char **argv)
    {
    return CliAtRamTestHwAssistEnable(argc, argv, AtModuleRamDdrGet, cAtTrue);
    }

eBool CmdAtDdrTestHwAssistDisable(char argc, char **argv)
    {
    return CliAtRamTestHwAssistEnable(argc, argv, AtModuleRamDdrGet, cAtFalse);
    }

eBool CmdAtDdrShow(char argc, char **argv)
    {
    return CliAtRamShow(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrCalibMeasure(char argc, char **argv)
    {
    return CliAtRamCalibMeasure(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrHwTestErrorForce(char argc, char **argv)
    {
    return CliAtRamHwTestErrorForce(argc, argv, AtModuleRamDdrGet);
    }

eBool CmdAtDdrHwTestErrorUnforce(char argc, char **argv)
    {
    return CliAtRamHwTestErrorUnforce(argc, argv, AtModuleRamDdrGet);
    }

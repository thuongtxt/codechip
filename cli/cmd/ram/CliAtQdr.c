/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : CliAtQdr.c
 *
 * Created Date: Jun 13, 2015
 *
 * Description : QDR CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleRam.h"
#include "AtDevice.h"
#include "../diag/CliAtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdAtQdrMarginsDetect(char argc, char **argv)
    {
    /* TODO: Handle this */
    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevCritical, "ERROR: not implemented\r\n");
    return cAtFalse;
    }

eBool CmdAtQdrCountersShow(char argc, char **argv)
    {
    return CliAtRamCountersShow(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrAlarmShow(char argc, char **argv)
    {
    return CliAtRamAlarmShow(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrAlarmHistoryShow(char argc, char **argv)
    {
    /* TODO: Handle this */
    AtUnused(argc);
    AtUnused(argv);
    AtPrintc(cSevCritical, "ERROR: not implemented\r\n");
    return cAtFalse;
    }

eBool CmdAtQdrAddressBusTest(char argc, char **argv)
    {
    return CmdAtRamBusTest(argc, argv, AtModuleRamQdrGet, cAtFalse);
    }

eBool CmdAtQdrDataBusTest(char argc, char **argv)
    {
    return CmdAtRamBusTest(argc, argv, AtModuleRamQdrGet, cAtTrue);
    }

eBool CmdAtQdrMemoryTest(char argc, char **argv)
    {
    return CmdAtRamMemoryTest(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrAccessEnable(char argc, char **argv)
    {
    return CmdAtRamAccessEnable(argc, argv, AtModuleRamQdrGet, cAtTrue);
    }

eBool CmdAtQdrAccessDisable(char argc, char **argv)
    {
    return CmdAtRamAccessEnable(argc, argv, AtModuleRamQdrGet, cAtFalse);
    }

eBool CmdAtQdrHwTestStart(char argc, char **argv)
    {
    return CliAtRamHwTestStart(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrHwTestStop(char argc, char **argv)
    {
    return CliAtRamHwTestStop(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrHwTestStatusShow(char argc, char **argv)
    {
    return CliAtRamHwTestStatusShow(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrDebugAddressBusSizeSet(char argc, char **argv)
    {
    return CliAtRamDebugAddressBusSizeSet(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrDebugDataBusSizeSet(char argc, char **argv)
    {
    return CliAtRamDebugDataBusSizeSet(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrDebug(char argc, char **argv)
    {
    return CmdAtRamDebug(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrHwTestErrorForce(char argc, char **argv)
    {
    return CliAtRamHwTestErrorForce(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrHwTestErrorUnforce(char argc, char **argv)
    {
    return CliAtRamHwTestErrorUnforce(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrRead(char argc, char **argv)
    {
    return CmdAtRamRead(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrWrite(char argc, char **argv)
    {
    return CmdAtRamWrite(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrTestBurstSet(char argc, char **argv)
    {
    return CliAtRamTestBurstSet(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrTestHwAssistEnable(char argc, char **argv)
    {
    return CliAtRamTestHwAssistEnable(argc, argv, AtModuleRamQdrGet, cAtTrue);
    }

eBool CmdAtQdrTestHwAssistDisable(char argc, char **argv)
    {
    return CliAtRamTestHwAssistEnable(argc, argv, AtModuleRamQdrGet, cAtFalse);
    }

eBool CmdAtQdrShow(char argc, char **argv)
    {
    return CliAtRamShow(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrCalibMeasure(char argc, char **argv)
    {
    return CliAtRamCalibMeasure(argc, argv, AtModuleRamQdrGet);
    }

eBool CmdAtQdrErrorGeneratorErrorTypeSet(char argc, char **argv)
    {
    uint32 errorType = CliRamErrorTypeFromString(argv[1]);
    AtUnused(argc);

    if(errorType == cAtRamGeneratorErrorTypeInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Error Type. Expected: %s\r\n", CliRamErrorTypesValidDecs());
        return cAtFalse;
        }

    return CmdRamErrorGeneratorAttributeSet(argv[0], AtModuleRamQdrGet, AtErrorGeneratorErrorTypeSet, errorType);
    }

eBool CmdAtQdrErrorGeneratorErrorNumSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CmdRamErrorGeneratorAttributeSet(argv[0], AtModuleRamQdrGet, AtErrorGeneratorErrorNumSet, AtStrToDw(argv[1]));
    }

eBool CmdAtQdrErrorGeneratorStart(char argc, char **argv)
    {
    AtUnused(argc);
    return CmdRamErrorGeneratorNoneAttributeSet(argv[0], AtModuleRamQdrGet, AtErrorGeneratorStart);
    }

eBool CmdAtQdrErrorGeneratorStop(char argc, char **argv)
    {
    AtUnused(argc);
    return CmdRamErrorGeneratorNoneAttributeSet(argv[0], AtModuleRamQdrGet, AtErrorGeneratorStop);
    }

eBool CmdAtQdrErrorGeneratorShow(char argc, char **argv)
    {
    return CmdRamErrorGeneratorErrorGeneratorShow(argc, argv, AtModuleRamQdrGet);
    }

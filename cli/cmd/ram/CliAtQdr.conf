# @group CliAtQdr "QDR"
# @ingroup CliAtModuleRam "RAM module"

3 qdr test addressbus CmdAtQdrAddressBusTest -1 qdrIds=1
/*
    Syntax     : qdr test addressbus <qdrIds> [<time>/<time>s/<time>m/<time>h/<time>d]
    Parameter  : qdrIds   - List of QDR IDs need to be tested      
                 duration - Duration.
                            + <time> : duration in milliseconds
                            + <time>s: duration in second
                            + <time>m: duration in minutes
                            + <time>h: duration in hours
                            + <time>d: duration in days
    Description: Test address bus
    Example    : qdr test addressbus 1-2
*/

3 qdr test databus CmdAtQdrDataBusTest -1 qdrIds=1
/*
    Syntax     : qdr test databus <qdrIds> [<time>/<time>s/<time>m/<time>h/<time>d]
    Parameter  : qdrIds - List of QDR IDs need to be tested          
                 duration - Duration.
                            + <time> : duration in milliseconds
                            + <time>s: duration in second
                            + <time>m: duration in minutes
                            + <time>h: duration in hours
                            + <time>d: duration in days
    Description: Test data bus
    Example    : qdr test databus 1-2
*/

3 qdr test memory CmdAtQdrMemoryTest -1 qdrIds=1
/*
    Syntax     : qdr test memory <qdrIds> [<time>/<time>s/<time>m/<time>h/<time>d]
    Parameter  : qdrIds - List of QDR IDs need to be tested
                 duration - Duration.
                            + <time> : duration in milliseconds
                            + <time>s: duration in second
                            + <time>m: duration in minutes
                            + <time>h: duration in hours
                            + <time>d: duration in days
    Description: Test memory
    Example    : qdr test memory 1-2
*/

3 qdr access enable CmdAtQdrAccessEnable 1 ddrIds=1
/*
    Syntax     : qdr access enable <ddrIds>
    Parameter  : qdrIds - List of QDR IDs need to be accessed
    Description: Enable SW read/write QDR memory
    Example    : qdr access enable 1-2
*/

3 qdr access disable CmdAtQdrAccessDisable 1 ddrIds=1
/*
    Syntax     : qdr access disable <ddrIds>
    Parameter  : qdrIds - List of QDR IDs need to be disabled accessing
    Description: Disable SW read/write QDR memory
    Example    : qdr access disable 1-2
*/

3 show qdr margins CmdAtQdrMarginsDetect -1 qdrIds=1
/*
    Syntax     : show qdr margins [<qdrIds>]
    Parameter  : qdrIds - List of QDR IDs      
    Description: Show QDR margins
    Example    : show qdr margins 1-2
*/

3 show qdr counters CmdAtQdrCountersShow -1 qdrIds readingMode
/*
    Syntax     : show qdr counters <qdrIds> <ro/r2c> [silent]
    Parameter  : qdrIds      - List of QDR IDs      
                 readingMode - [ro/r2c] Reading mode
                               + ro: read-only
                               + r2c: read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode      
    Description: Show QDR counters
    Example    : show qdr counters 1-2 r2c
                 show qdr counters 1-2 r2c silent
*/

3 show qdr alarm CmdAtQdrAlarmShow 1 qdrIds
/*
    Syntax     : show qdr alarm <qdrIds>
    Parameter  : qdrIds - List of QDR IDs      
    Description: Show QDR alarms
    Example    : show qdr alarm 1-2
*/

4 show qdr alarm history CmdAtQdrAlarmHistoryShow 2 qdrIds readingMode
/*
    Syntax     : show qdr alarm history <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs      
                 readingMode - [ro/r2c] Reading mode
                               + ro: read-only
                               + r2c: read-to-clear      
    Description: Show QDR alarms change history
    Example    : show qdr alarm history 1-2 ro
                 show qdr alarm history 1-2 r2c
*/

3 qdr test start CmdAtQdrHwTestStart 1 qdrIds
/*
    Syntax     : qdr test start <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs      
    Description: Start hardware base testing.
    Example    : qdr test start 1-2
*/

3 qdr test stop CmdAtQdrHwTestStop 1 qdrIds
/*
    Syntax     : qdr test stop <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs      
    Description: Stop hardware base testing.
    Example    : qdr test stop 1-2
*/

3 show qdr test CmdAtQdrHwTestStatusShow 1 qdrIds
/*
    Syntax     : show qdr test <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs      
    Description: Show QDR hardware test status
    Example    : show qdr test 1-2
*/

4 debug qdr bussize address CmdAtQdrDebugAddressBusSizeSet 2 qdrIds busSize
/*
    Syntax     : debug qdr bussize address <qdrIds> <busSize>
    Parameter  : qdrIds      - List of QDR IDs     
                 busSize     - Address bus size. Set this to 0 to revert back to 
                               default bus size.
    Description: Configure address bus size for testing and debugging purpose
    Example    : debug qdr bussize address 1-2 21
                 debug qdr bussize address 1-2 0
*/

4 debug qdr bussize data CmdAtQdrDebugDataBusSizeSet 2 qdrIds busSize
/*
    Syntax     : debug qdr bussize data <qdrIds> <busSize>
    Parameter  : qdrIds      - List of QDR IDs     
                 busSize     - Data bus size. Set this to 0 to revert back to 
                               default bus size.
    Description: Configure data bus size for testing and debugging purpose
    Example    : debug qdr bussize data 1-2 21
                 debug qdr bussize data 1-2 0
*/

2 debug qdr CmdAtQdrDebug 1 qdrList=1-6
/*
    Syntax     : debug qdr <qdrIds>
    Parameter  : qdrIds - List of QDR IDs need to show debug information      
    Description: Show debug information of QDR
    Example    : debug qdr 1-5
*/

3 qdr test force CmdAtQdrHwTestErrorForce 1 qdrList=1-6
/*
    Syntax     : qdr test force <qdrIds>
    Parameter  : qdrIds - List of QDR IDs.
    Description: Force error during hardware base testing is running
    Example    : qdr test force 1-5
*/

3 qdr test unforce CmdAtQdrHwTestErrorUnforce 1 qdrList=1-6
/*
    Syntax     : qdr test unforce <qdrIds>
    Parameter  : qdrIds - List of QDR IDs.
    Description: Unforce error during hardware base testing is running
    Example    : qdr test unforce 1-5
*/

2 qdr rd CmdAtQdrRead 2 qdrId=1 localAddress=0x0
/*
    Syntax     : qdr rd <qdrId> <localAddress>
    Parameter  : qdrId        - QDR ID
                 localAddress - Local address
    Description: Read QDR
    Example    : qdr rd 0x1
*/

2 qdr wr CmdAtQdrWrite 3 qdrId=1 localAddress=0x0 value=0x0
/*
    Syntax     : qdr wr <qdrId> <localAddress> <value>
    Parameter  : qdrId        - QDR ID
                 localAddress - Local address
                 value        - Written value. Its format is 0x.<valueWithNoDot>.
    Description: Write QDR
    Example    : qdr wr 0x1 0x3
*/

2 qdr burst CmdAtQdrTestBurstSet 2 qdrIds burst
/*
    Syntax     : qdr burst <qdrIds> <burst>
    Parameter  : qdrIds      - List of QDR IDs     
                 burst       - Number of addresses hardware can do burst read/write
    Description: Configure number of addresses hardware can do burst read/write
    Example    : qdr burst 1-2 2
                 qdr burst 1-2 15 
*/

4 qdr test hwassist enable CmdAtQdrTestHwAssistEnable 1 qdrIds
/*
    Syntax     : qdr test hwassist enable <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs     
    Description: Enable hardware assist mode to speedup memory fill and compare 
                 operations. Note, not all of products can support this.
    Example    : qdr test hwassist enable 1-2
*/

4 qdr test hwassist disable CmdAtQdrTestHwAssistDisable 1 qdrIds
/*
    Syntax     : qdr test hwassist disable <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs     
    Description: Disable hardware assist mode. Memory fill and compare operations
                 are done by software. It would take long time to complete testing.
    Example    : qdr test hwassist disable 1-2
*/

3 qdr calib measure CmdAtQdrCalibMeasure 2 qdrIds repeatTimes
/*
    Syntax     : qdr calib measure <qdrIds> <repeatTimes>
    Parameter  : qdrIds         - List of QDR IDs     
                 repeatTimes    - Repeat times
    Description: Measure Calibration of QDR
    Example    : qdr calib measure 1-2 5
*/

2 show qdr CmdAtQdrShow 1 qdrIds
/*
    Syntax     : show qdr <qdrIds>
    Parameter  : qdrIds      - List of QDR IDs     
    Description: Show QDR information
    Example    : show qdr 1-2
                 show qdr 1-2
*/

4 qdr error generator type CmdAtQdrErrorGeneratorErrorTypeSet 2 qdrIds=1 type=ecc-correctable
/*
    Syntax     : qdr error generator type <qdrIds> <ecc-correctable/ecc-uncorrectable>
    Parameter  : qdrIds - ram identifier
                 type   - Error type
                          + ecc-correctable
                          + ecc-uncorrectable
    Description: Set error generating type 
    Example    : qdr error generator type 1 ecc-correctable
*/

4 qdr error generator errornum CmdAtQdrErrorGeneratorErrorNumSet 2 qdrIds=1 errornum=1
/*
    Syntax     : qdr error generator errornum <qdrIds> <errornum>
    Parameter  : qdrIds     - ram identifier
                 errornum   - Number of error is generated
    Description: Set number of error for oneshot force error 
    Example    : qdr error generator errornum 1 3
*/

4 qdr error generator start CmdAtQdrErrorGeneratorStart 1 qdrIds=1
/*
    Syntax     : qdr error generator start <qdrIds>
    Parameter  : qdrIds     - ram identifier
    Description: Start error generator 
    Example    : qdr error generator start 1
*/

4 qdr error generator stop CmdAtQdrErrorGeneratorStop 1 qdrIds=1
/*
    Syntax     : qdr error generator stop <qdrIds>
    Parameter  : qdrIds     - ram identifier
    Description: Stop error generator 
    Example    : qdr error generator stop 1
*/

4 show qdr error generator CmdAtQdrErrorGeneratorShow 1 qdrIds=1
/*
    Syntax     : show qdr error generator  <qdrIds>
    Parameter  : qdrIds     - ram identifier
    Description: Show error generator 
    Example    : show qdr error generator 1
*/

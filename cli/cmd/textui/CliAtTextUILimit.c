/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : CliAtTextUILimit.c
 *
 * Created Date: May 7, 2016
 *
 * Description : CLIs to control processing limitation to give other threads
 *               chance to execute
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_numPwToRest = 10;
static uint32 m_numTdmChannelToRest = 10;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IntValueSet(char argc, char **argv, void (*ValueSet)(uint32 value))
    {
    uint32 restTimeMs = AtStrToDw(argv[0]);
    AtUnused(argc);
    ValueSet(restTimeMs);
    return cAtTrue;
    }

eBool CmdAtCliCpuRestTimeMsSet(char argc, char **argv)
    {
    return IntValueSet(argc, argv, AtCliCpuRestTimeMsSet);
    }

eBool CmdAtCliLimitShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Attribute", "Value"};
    uint32 row = 0;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(5, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* CPU resting time */
    StrToCell(tabPtr, row, 0, "CPU rest time");
    ColorStrToCell(tabPtr, row, 1, CliNumber2String(AtCliCpuRestTimeMsGet(), "%d (ms)"), AtCliCpuRestTimeMsGet() ? cSevWarning : cSevInfo);

    /* Number of PW to rest */
    row = row + 1;
    StrToCell(tabPtr, row, 0, "NumPws to rest");
    if (AtCliCpuRestTimeMsGet() == 0)
        StrToCell(tabPtr, row, 1, "xxx");
    else
        StrToCell(tabPtr, row, 1, CliNumber2String(AtCliLimitNumRestPwGet(), "%d"));

    /* Number of TDM channels to rest */
    row = row + 1;
    StrToCell(tabPtr, row, 0, "NumTdmChannels to rest");
    if (AtCliCpuRestTimeMsGet() == 0)
        StrToCell(tabPtr, row, 1, "xxx");
    else
        StrToCell(tabPtr, row, 1, CliNumber2String(AtCliLimitNumRestTdmChannelGet(), "%d"));

    /* Table resting time */
    row = row + 1;
    StrToCell(tabPtr, row, 0, "Table rest time");
    ColorStrToCell(tabPtr, row, 1, CliNumber2String(TableRestTimeMsGet(), "%d (ms)"), TableRestTimeMsGet() ? cSevWarning : cSevInfo);

    /* Number of printed line to rest */
    row = row + 1;
    StrToCell(tabPtr, row, 0, "NumPrintedLines to rest");
    if (TableRestTimeMsGet() == 0)
        StrToCell(tabPtr, row, 1, "xxx");
    else
        StrToCell(tabPtr, row, 1, CliNumber2String(TableNumRestLinesGet(), "%d"));

    /* NOTE: when new row is added, remember to update TableAlloc calling above */

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtCliLimitNumRestPwSet(char argc, char **argv)
    {
    return IntValueSet(argc, argv, AtCliLimitNumRestPwSet);
    }

eBool CmdAtCliLimitNumRestTdmChannelSet(char argc, char **argv)
    {
    return IntValueSet(argc, argv, AtCliLimitNumRestTdmChannelSet);
    }

eBool CmdTableNumRestLinesSet(char argc, char **argv)
    {
    return IntValueSet(argc, argv, TableNumRestLinesSet);
    }

eBool CmdTableRestTimeMsSet(char argc, char **argv)
    {
    return IntValueSet(argc, argv, TableRestTimeMsSet);
    }

void AtCliLimitNumRestPwSet(uint32 numPws)
    {
    m_numPwToRest = numPws;
    }

uint32 AtCliLimitNumRestPwGet(void)
    {
    return m_numPwToRest;
    }

uint32 AtCliLimitNumRestTdmChannelGet(void)
    {
    return m_numTdmChannelToRest;
    }

void AtCliLimitNumRestTdmChannelSet(uint32 numTdmChannels)
    {
    m_numTdmChannelToRest = numTdmChannels;
    }


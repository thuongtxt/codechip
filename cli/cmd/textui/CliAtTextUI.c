/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : CliAtTextUI.c
 *
 * Created Date: Nov 19, 2015
 *
 * Description : Text UI specific command
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "AtTokenizer.h"
#include "AtCliService.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtCliModeVal[] = {cAtCliModeAutotest,
                                       cAtCliModeTcl,
                                       cAtCliModeSilent};
static const char *cAtCliModeStr[] = {"autotest",
                                      "tcl",
                                      "silent"};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CliReplayEnable(char argc, char **argv, eBool enabled)
    {
    AtTextUIRemoteCliReplay(AtCliSharedTextUI(), enabled);

    AtUnused(argc);
    AtUnused(argv);

    return cAtTrue;
    }

static eBool Verbose(char argc, char **argv, eBool verbose)
    {
    AtUnused(argc);
    AtUnused(argv);
    AtTextUIVerbose(AtCliSharedTextUI(), verbose);
    return cAtTrue;
    }

static uint32 ModeFromString(char *string)
    {
    eAtCliMode allMode = 0;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(string, "|");
    while (AtTokenizerHasNextString(tokenizer))
        {
        eBool convertSuccess = cAtTrue;
        eAtCliMode mode;
        char *token = AtTokenizerNextString(tokenizer);

        mode = CliStringToEnum(token, cAtCliModeStr, cAtCliModeVal, mCount(cAtCliModeVal), &convertSuccess);
        if (convertSuccess)
            {
            allMode = allMode | mode;
            continue;
            }

        AtPrintc(cSevCritical, "ERROR: Please input valid TextUI mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtCliModeStr, mCount(cAtCliModeVal));
        AtPrintf("\r\n");
        return cInvalidUint32;
        }

    return allMode;
    }

static const char *Mode2String(eAtCliMode modes)
    {
    static char modeString[32];
    uint32 mode_i;

    if (modes == cAtCliModeNormal)
        return "normal";

    if (modes == cAtCliModeAll)
        return "all";

    modeString[0] = '\0';
    for (mode_i = 0; mode_i < mCount(cAtCliModeVal); mode_i++)
        {
        if ((modes & cAtCliModeVal[mode_i]) == 0)
            continue;

        if (AtStrlen(modeString) > 0)
            AtStrcat(modeString, "|");

        AtStrcat(modeString, cAtCliModeStr[mode_i]);
        }

    return modeString;
    }

static eBool ScriptVerboseEnable(char argc, char **argv, eBool verbose)
    {
    AtTextUI textUI = AtCliSharedTextUI();
    AtUnused(argc);
    AtUnused(argv);
    AtTextUIScriptVerbose(textUI, verbose);
    return cAtTrue;
    }

eBool CmdAtTextUIRemoteConnect(char argc, char **argv)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(argv[0], ":");
    char *ipAddress = NULL;
    uint16 port;

    AtUnused(argc);

    /* Only port is input, so just connect to localhost interface */
    if (AtTokenizerNumStrings(tokenizer) == 1)
        port = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));

    /* Both IP address and port are specified */
    else if(AtTokenizerNumStrings(tokenizer) == 2)
        {
        ipAddress = AtStrdup(AtTokenizerNextString(tokenizer));
        port = (uint16)AtStrToDw(AtTokenizerNextString(tokenizer));
        }

    /* Unknown address format */
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid network connection, expected: [<idAddress>:]<port>\r\n");
        return cAtFalse;
        }

    AtTextUIRemoteConnect(AtCliSharedTextUI(), ipAddress ? ipAddress : "127.0.0.1", port);
    AtOsalMemFree(ipAddress);
    if (AtTextUIRemoteConnection(AtCliSharedTextUI()))
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Cannot connect\r\n");

    return cAtFalse;
    }

eBool CmdAtTextUIRemoteDisconnect(char argc, char **argv)
    {
    AtTextUIRemoteDisconnect(AtCliSharedTextUI());

    AtUnused(argc);
    AtUnused(argv);

    return cAtTrue;
    }

eBool CmdAtTextUIRemoteCliReplayEnable(char argc, char **argv)
    {
    return CliReplayEnable(argc, argv, cAtTrue);
    }

eBool CmdAtTextUIRemoteCliReplayDisable(char argc, char **argv)
    {
    return CliReplayEnable(argc, argv, cAtFalse);
    }

eBool CmdAtTextUIShow(char argc, char **argv)
    {
    tTab *tabPtr;
    AtTextUI textUI = AtCliSharedTextUI();
    const char *pHeading[] = {"Attribute", "Value"};
    eBool enabled;
    AtConnection connection;
    uint32 numRows = 7, row_i = 0;

    AtUnused(argc);
    AtUnused(argv);

    tabPtr = TableAlloc(numRows, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Show remote connection */
    StrToCell(tabPtr, row_i, 0, "Remote connection");
    connection = AtTextUIRemoteConnection(textUI);
    if (connection)
        {
        char connectionDescription[31];
        AtSprintf(connectionDescription, "%s:%d", AtConnectionRemoteAddressGet(connection), AtConnectionConnectedPortGet(connection));
        ColorStrToCell(tabPtr, row_i, 1, connectionDescription, cSevInfo);
        }
    else
        StrToCell(tabPtr, row_i, 1, "None");
    row_i = row_i + 1;

    /* Show remote connection timeout */
    StrToCell(tabPtr, row_i, 0, "Remote connection timeout");
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtConnectionReceiveTimeoutGet(connection), "%d (seconds)"));
    row_i = row_i + 1;

    /* Show remote replaying */
    enabled = AtTextUIRemoteCliIsReplayed(textUI);
    StrToCell(tabPtr, row_i, 0, "Remote CLI replaying");
    ColorStrToCell(tabPtr, row_i, 1, enabled ? "en" : "dis", enabled ? cSevInfo : cSevNormal);
    row_i = row_i + 1;

    /* CPU resting time */
    StrToCell(tabPtr, row_i, 0, "CPU rest time");
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtCliCpuRestTimeMsGet(), "%d (ms)"));
    row_i = row_i + 1;

    /* TextUI mode */
    StrToCell(tabPtr, row_i, 0, "CLI Mode");
    StrToCell(tabPtr, row_i, 1, Mode2String(AtCliModeGet()));
    row_i = row_i + 1;

    /* CLI verbose */
    StrToCell(tabPtr, row_i, 0, "CLI verbosed");
    StrToCell(tabPtr, row_i, 1, CliBoolToString(AtTextUIIsVerbosed(textUI)));
    row_i = row_i + 1;

    /* Script verbose */
    StrToCell(tabPtr, row_i, 0, "Script verbosed");
    StrToCell(tabPtr, row_i, 1, CliBoolToString(AtTextUIScriptIsVerbosed(textUI)));
    row_i = row_i + 1;

    /* Below code will help when new rows are manually added */
    AtAssert(row_i == numRows);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtTextVerboseEnable(char argc, char **argv)
    {
    return Verbose(argc, argv, cAtTrue);
    }

eBool CmdAtTextVerboseDisable(char argc, char **argv)
    {
    return Verbose(argc, argv, cAtFalse);
    }

eBool CmdAtTextRemoteTimeoutSet(char argc, char **argv)
    {
    AtConnection connection;

    AtUnused(argc);

    connection = AtTextUIRemoteConnection(AtCliSharedTextUI());
    if (AtConnectionReceiveTimeoutSet(connection, AtStrToDw(argv[0])) == cAtConnectionOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Cannot change timeout, error happens\r\n");
    return cAtFalse;
    }

eBool CmdAtCliModeSet(char argc, char **argv)
    {
    AtUnused(argc);

    AtCliModeClr(cAtCliModeAll);

    if (AtStrcmp(argv[0], "normal") != 0)
        {
        uint32 mode = ModeFromString(argv[0]);
        if (mode == cInvalidUint32)
            return cAtFalse;
        AtCliModeSet(mode);
        }

    return cAtTrue;
    }

eBool CmdAtDebugTextUI(char argc, char **argv)
    {
    AtConnection connection;

    AtUnused(argc);
    AtUnused(argv);

    /* Show client connection */
    connection = AtCliClientConnection();
    AtPrintc(cSevInfo, "* Client connection: \r\n");
    AtConnectionDebug(connection);

    /* Show server connection */
    connection = AtCliServerConnection();
    AtPrintc(cSevInfo, "* Server connection: \r\n");
    AtConnectionDebug(connection);

    return cAtTrue;
    }

eBool CmdAtTextUIScriptVerboseEnable(char argc, char **argv)
    {
    return ScriptVerboseEnable(argc, argv, cAtTrue);
    }

eBool CmdAtTextUIScriptVerboseDisable(char argc, char **argv)
    {
    return ScriptVerboseEnable(argc, argv, cAtFalse);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : CliAtTextUIStdout.c
 *
 * Created Date: Apr 8, 2018
 *
 * Description : To capture stdout
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
#define cDefaultBufferSize 1024

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tStdoutBuffer
    {
    char *buffer;
    uint32 bufferSize;
    eBool autoFlush;
    eBool allowed;
    }tStdoutBuffer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tStdoutBuffer *m_buffer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tStdoutBuffer *BufferCreate(void)
    {
    tStdoutBuffer *buffer = AtOsalMemAlloc(sizeof(tStdoutBuffer));
    AtOsalMemInit(buffer, 0, sizeof(tStdoutBuffer));

    buffer->buffer = AtOsalMemAlloc(cDefaultBufferSize);
    AtOsalMemInit(buffer->buffer, 0, cDefaultBufferSize);
    buffer->bufferSize = cDefaultBufferSize;
    buffer->autoFlush = cAtTrue;
    buffer->allowed = cAtTrue;

    return buffer;
    }

static void BufferDelete(tStdoutBuffer *buffer)
    {
    if (buffer == NULL)
        return;

    AtOsalMemFree(buffer->buffer);
    AtOsalMemFree(buffer);
    }

static tStdoutBuffer *Buffer(void)
    {
    if (m_buffer == NULL)
        m_buffer = BufferCreate();
    return m_buffer;
    }

static uint32 BufferEnlarge(tStdoutBuffer *buffer, uint32 stringLenToHold)
    {
    uint32 numBlocks, numBytesToEnlarge;

    numBlocks = (stringLenToHold / cDefaultBufferSize);
    if (stringLenToHold % cDefaultBufferSize)
        numBlocks = numBlocks + 1;

    numBytesToEnlarge = numBlocks * cDefaultBufferSize;
    buffer->buffer = AtOsalMemRealloc(buffer->buffer, buffer->bufferSize, buffer->bufferSize + numBytesToEnlarge);
    buffer->bufferSize += numBytesToEnlarge;

    return (buffer->bufferSize - AtStrlen(buffer->buffer));
    }

static void WillPrint(AtStd self, void *userData, char *aString)
    {
    tStdoutBuffer *buffer = m_buffer;
    uint32 remained;
    uint32 strLen;

    if (buffer == NULL)
        return;

    AtUnused(self);
    AtUnused(userData);

    if (!buffer->allowed)
        return;

    remained = buffer->bufferSize - AtStrlen(buffer->buffer);
    strLen = AtStrlen(aString);

    if (remained < strLen)
        remained = BufferEnlarge(buffer, strLen);

    AtStrncat(buffer->buffer, aString, remained);
    remained = remained - strLen;
    }

static tAtStdListener *StdListener(void)
    {
    static tAtStdListener listener = {
                                     .WillPrint = WillPrint,
                                     .DidPrint = NULL
                                     };

    return &listener;
    }

static void TextUIWillStop(AtTextUI self, void *userData)
    {
    AtUnused(self);
    AtUnused(userData);
    BufferDelete(m_buffer);
    m_buffer = NULL;
    }

static void TextUIWillExecuteCli(AtTextUI self, const char *cli, void *userData)
    {
    tStdoutBuffer *buffer = userData;

    if (AtStrstr(cli, "stdout capture"))
        return;

    AtUnused(self);
    AtUnused(cli);

    if (buffer->autoFlush)
        buffer->buffer[0] = '\0';
    }

static tAtTextUIListerner* TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;
    m_listener.WillStop = TextUIWillStop;
    m_listener.WillExecuteCli = TextUIWillExecuteCli;
    return &m_listener;
    }

static eBool StdoutCaptureEnable(char argc, char **argv, eBool enabled)
    {
    tStdoutBuffer *buffer;

    AtUnused(argc);
    AtUnused(argv);

    if (enabled)
        {
        buffer = Buffer();
        AtStdListenerAdd(AtStdSharedStdGet(), StdListener(), buffer);
        AtTextUIListenerAdd(AtCliSharedTextUI(), TextUIListener(), buffer);
        }

    else
        {
        AtStdListenerRemove(AtStdSharedStdGet(), StdListener(), m_buffer);
        AtTextUIListenerRemove(AtCliSharedTextUI(), TextUIListener());

        BufferDelete(m_buffer);
        m_buffer = NULL;
        }

    return cAtTrue;
    }

static eBool AutoFlushEnable(char argc, char **argv, eBool enabled)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (m_buffer == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: stdout capture has not been enabled\r\n");
        return cAtFalse;
        }

    m_buffer->autoFlush = enabled;

    return cAtTrue;
    }

eBool CmdAtTextUIStdoutCaptureEnable(char argc, char **argv)
    {
    return StdoutCaptureEnable(argc, argv, cAtTrue);
    }

eBool CmdAtTextUIStdoutCaptureDisable(char argc, char **argv)
    {
    return StdoutCaptureEnable(argc, argv, cAtFalse);
    }

eBool CmdAtTextUIStdoutCaptureAutoFlushEnable(char argc, char **argv)
    {
    return AutoFlushEnable(argc, argv, cAtTrue);
    }

eBool CmdAtTextUIStdoutCaptureAutoFlushDisable(char argc, char **argv)
    {
    return AutoFlushEnable(argc, argv, cAtFalse);
    }

eBool CmdAtTextUIStdoutCaptureShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"Attribute", "Value"};
    uint32 numRows = 4, row_i = 0;

    AtUnused(argc);
    AtUnused(argv);

    if (m_buffer == NULL)
        {
        AtPrintc(cSevInfo, "Nothing to display, output capture has not been enabled\r\n");
        return cAtTrue;
        }

    /* Temporary disable as we will not capture this output */
    m_buffer->allowed = cAtFalse;

    tabPtr = TableAlloc(numRows, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, row_i, 0, "Enabled");
    StrToCell(tabPtr, row_i, 1, CliBoolToString(m_buffer ? cAtTrue : cAtFalse));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, "Auto flush");
    StrToCell(tabPtr, row_i, 1, CliBoolToString(m_buffer->autoFlush));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, "Buffer Size (bytes)");
    StrToCell(tabPtr, row_i, 1, CliNumber2String(m_buffer->bufferSize, "%d"));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, "Capture Output Size (bytes)");
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtStrlen(m_buffer->buffer), "%d"));
    row_i = row_i + 1;

    /* Below code will help when new rows are manually added */
    AtAssert(row_i == numRows);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Show this */
    if (AtStrlen(m_buffer->buffer))
        {
        AtPrintc(cSevInfo, "* Captured output content: \r\n");
        AtPrintf("%s\r\n", m_buffer->buffer);
        }

    /* Enable again */
    m_buffer->allowed = cAtTrue;

    return cAtTrue;
    }

eBool CmdAtTextUIStdoutCaptureFlush(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);

    if (m_buffer)
        m_buffer->buffer[0] = '\0';
    return cAtTrue;
    }

char *CliAtTextUICapturedOutput(void)
    {
    if (m_buffer)
        return m_buffer->buffer;
    return NULL;
    }

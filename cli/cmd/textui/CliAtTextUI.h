/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Text UI
 * 
 * File        : CliAtTextUI.h
 * 
 * Created Date: May 7, 2016
 *
 * Description : Text UI CLI
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATTEXTUI_H_
#define _CLIATTEXTUI_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtCliLimitNumRestPwSet(uint32 numPws);
uint32 AtCliLimitNumRestPwGet(void);
void AtCliLimitNumRestTdmChannelSet(uint32 numTdmChannels);
uint32 AtCliLimitNumRestTdmChannelGet(void);
char *CliAtTextUICapturedOutput(void);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATTEXTUI_H_ */

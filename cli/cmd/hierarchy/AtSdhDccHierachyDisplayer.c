/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtSdhDccHierachyDisplayer.c
 *
 * Created Date: Apr 19, 2017
 *
 * Description : DCC hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtSdhLine.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtSdhChannelHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_SharedSdhChannelDisplayer = NULL;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 nodeLength)
    {
    AtChannel attachedChannelLine = NULL;
    AtChannel attachedChannelSection = NULL;
    AtChannelHierarchyDisplayer defaultDisplayer = AtHdlcChannelHierarchyDisplayerGet(self->mode);

    attachedChannelLine = (AtChannel)AtSdhLineDccChannelGet((AtSdhLine)channel, cAtSdhLineDccLayerLine);
    attachedChannelSection = (AtChannel)AtSdhLineDccChannelGet((AtSdhLine)channel, cAtSdhLineDccLayerSection);

    if (attachedChannelSection)
        ChannelHierarchyRecursiveShow(defaultDisplayer, (AtChannel)attachedChannelSection, nodeLength, cAtTrue, cAtFalse);

    if (attachedChannelLine)
        ChannelHierarchyRecursiveShow(defaultDisplayer, (AtChannel)attachedChannelLine, nodeLength, cAtFalse, cAtTrue);

    return 0;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, mMethodsGet((AtChannelHierarchyDisplayer)self), sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        }

    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhChannelHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer AtSdhDccHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtSdhDccHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_SharedSdhChannelDisplayer == NULL)
        {
        m_SharedSdhChannelDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        AtSdhDccHierarchyDisplayerObjectInit(m_SharedSdhChannelDisplayer);
        }

    m_SharedSdhChannelDisplayer->mode = mode;
    return m_SharedSdhChannelDisplayer;
    }

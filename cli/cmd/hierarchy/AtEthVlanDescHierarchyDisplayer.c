/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtEthVlanDescHierarchyDisplayer.c
 *
 * Created Date: Aug 7, 2013
 *
 * Description : ETH VLAN hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthFlow.h"
#include "AtChannelHierarchyDisplayerInternal.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tAtEthVlanDescHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtEthVlanDescHierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtEthVlanDescHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_SharedVlanDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *VlanTag2Str(const tAtEthVlanTag *vlanTag)
    {
    static char vlanTagBuf[16];
    AtSprintf(vlanTagBuf, "%d.%d.%d", vlanTag->priority, vlanTag->cfi, vlanTag->vlanId);

    return vlanTagBuf;
    }

static const char *TraffficDescr2Str(const tAtEthVlanDesc *trafficDescr)
    {
    static char vlansStrBuf[32];

    if ((trafficDescr == NULL) || (trafficDescr->numberOfVlans == 0))
        return "None";

    /* If two VLANs exist, show the S-VLAN first */
    AtSprintf(vlansStrBuf, "%d#", trafficDescr->ethPortId + 1);
    if (trafficDescr->numberOfVlans == 2)
        {
        AtStrcat(vlansStrBuf, VlanTag2Str(&(trafficDescr->vlans[1])));
        AtStrcat(vlansStrBuf, ",");
        }

    /* Only C-VLAN exist */
    AtStrcat(vlansStrBuf, VlanTag2Str(&(trafficDescr->vlans[0])));

    return vlansStrBuf;
    }

static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
	AtUnused(branchLength);
	AtUnused(channel);
	AtUnused(self);
    AtPrintc(cSevNormal ,"\r\n");
    return 0;
    }

static const char *ToString(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    tAtEthVlanDesc* descs = (tAtEthVlanDesc*)((void*)channel);
    AtUnused(displayer);

    if (descs == NULL)
        return "none";

    return TraffficDescr2Str(descs);
    }

static eAtSevLevel StatusColorGet(AtChannelHierarchyDisplayer self, AtChannel channel)
    {
	AtUnused(channel);
	AtUnused(self);
    return cSevInfo;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, ToString);
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, StatusColorGet);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthVlanDescHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtEthVlanDescHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_SharedVlanDisplayer == NULL)
        {
        m_SharedVlanDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_SharedVlanDisplayer);
        }

    m_SharedVlanDisplayer->mode = mode;
    return m_SharedVlanDisplayer;
    }


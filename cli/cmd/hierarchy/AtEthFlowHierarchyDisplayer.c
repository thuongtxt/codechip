/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtEthFlowHierarchyDisplayer.c
 *
 * Created Date: Aug 7, 2013
 *
 * Description : Eth flow hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthFlow.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tAtEthFlowHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtEthFlowHierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtEthFlowHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_AtEthFlowHierarchyDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    AtEthFlow ethFlow = (AtEthFlow)channel;
    uint8 printedDummy = 0, vlanIndex;
    eBool isFirst, isLast;
    AtChannelHierarchyDisplayer subDisplayer;
    uint32 numVlan = AtEthFlowIngressNumVlansGet(ethFlow);

    if (numVlan == 0)
        return 0;

    /* Create displayer for sub-channel */
    subDisplayer = AtEthVlanDescHierarchyDisplayerGet(self->mode);
    subDisplayer->mode = self->mode;

    for (vlanIndex = 0; vlanIndex < numVlan; vlanIndex++)
        {
        tAtEthVlanDesc vlan;
        AtEthFlowIngressVlanAtIndex(ethFlow, vlanIndex, &vlan);
        isFirst = (vlanIndex == 0) ? cAtTrue : cAtFalse;
        isLast = (vlanIndex == numVlan) ? cAtTrue : cAtFalse;
        printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, (AtChannel)((void *)&vlan), branchLength, isFirst, isLast);
        }

    return printedDummy;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthFlowHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtEthFlowHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_AtEthFlowHierarchyDisplayer == NULL)
        {
        m_AtEthFlowHierarchyDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_AtEthFlowHierarchyDisplayer);
        }

    m_AtEthFlowHierarchyDisplayer->mode = mode;
    return m_AtEthFlowHierarchyDisplayer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtSonetChannelHierarchyDisplayer.c
 *
 * Created Date: Nov 3, 2015
 *
 * Description : Sonet channel hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannelHierarchyDisplayerInternal.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSdhDisplayer(self)((tAtSdhChannelHierarchyDisplayer*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSonetChannelHierarchyDisplayer
    {
    tAtSdhChannelHierarchyDisplayer super;
    }tAtSonetChannelHierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;

static tAtSonetChannelHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_SharedSonetChannelDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *LineString(AtSdhLine line)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate == cAtSdhLineRateStm0)  return "OC1";
    if (rate == cAtSdhLineRateStm1)  return "OC3";
    if (rate == cAtSdhLineRateStm4)  return "OC12";
    if (rate == cAtSdhLineRateStm16) return "OC48";
    if (rate == cAtSdhLineRateStm64) return "OC192";

    return "line";
    }

static const char *SonetChannelTypeString(AtChannel channel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)channel);

    if (channelType == cAtSdhChannelTypeLine)    return LineString((AtSdhLine)channel);

    /* AUG */
    if (channelType == cAtSdhChannelTypeAug64)   return "sts192";
    if (channelType == cAtSdhChannelTypeAug16)   return "sts48";
    if (channelType == cAtSdhChannelTypeAug4)    return "sts12";
    if (channelType == cAtSdhChannelTypeAug1)    return "sts3";

    /* AU */
    if (channelType == cAtSdhChannelTypeAu4_64c) return "sts192c";
    if (channelType == cAtSdhChannelTypeAu4_16c) return "sts48c";
    if (channelType == cAtSdhChannelTypeAu4_4c)  return "sts12c";
    if (channelType == cAtSdhChannelTypeAu4_nc)  return "stsNc";
    if (channelType == cAtSdhChannelTypeAu4)     return "sts3c";
    if (channelType == cAtSdhChannelTypeAu3)     return "sts1";


    if (channelType == cAtSdhChannelTypeTug2)    return "vtg";

    /* TU */
    if (channelType == cAtSdhChannelTypeTu12)    return "vt2";
    if (channelType == cAtSdhChannelTypeTu11)    return "vt15";

    /* VC */
    if (channelType == cAtSdhChannelTypeVc4_64c) return "sts192c_spe";
    if (channelType == cAtSdhChannelTypeVc4_16c) return "sts48c_spe";
    if (channelType == cAtSdhChannelTypeVc4_4c)  return "sts12c_spe";
    if (channelType == cAtSdhChannelTypeVc4)     return "sts3c_spe";
    if (channelType == cAtSdhChannelTypeVc3)     return "sts1_spe";
    if (channelType == cAtSdhChannelTypeVc12)    return "vt2_spe";
    if (channelType == cAtSdhChannelTypeVc11)    return "vt15_spe";
    if (channelType == cAtSdhChannelTypeVc4_nc)  return "stsNc_spe";

    return "unknown";
    }

static const char *SecondPoint(const char* string)
    {
    char* tmp;
    if ((tmp = AtStrchr(string, '.')) != NULL)
        return AtStrchr(tmp, '.');
    return NULL;
    }

static const char *SonetChannelIdStringGet(AtChannel channel)
    {
    static char idString[32];
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    if ((channelType == cAtSdhChannelTypeLine)   ||
        (channelType == cAtSdhChannelTypeAug64)  ||
        (channelType == cAtSdhChannelTypeAug16)  ||
        (channelType == cAtSdhChannelTypeAug4)   ||
        (channelType == cAtSdhChannelTypeAug1)   ||
        (channelType == cAtSdhChannelTypeAu4_64c)||
        (channelType == cAtSdhChannelTypeAu4_16c)||
        (channelType == cAtSdhChannelTypeAu4_4c) ||
        (channelType == cAtSdhChannelTypeAu4_nc) ||
        (channelType == cAtSdhChannelTypeAu4)    ||
        (channelType == cAtSdhChannelTypeVc4_64c)||
        (channelType == cAtSdhChannelTypeVc4_16c)||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeVc4_nc) ||
        (channelType == cAtSdhChannelTypeVc4))
        return AtChannelIdString(channel);

    if ((channelType == cAtSdhChannelTypeAu3)    ||
        (channelType == cAtSdhChannelTypeVc3))
        {
        AtSprintf(idString, "%u", AtSdhChannelSts1Get(sdhChannel) + 1);
        return idString;
        }

    AtSprintf(idString, "%u%s", AtSdhChannelSts1Get(sdhChannel) + 1, SecondPoint(AtChannelIdString(channel)));
    return idString;
    }

static const char *ToString(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    static char idString[32];
    AtUnused(displayer);

    if (channel == NULL)
        return "none";

    AtSprintf(idString, "%s.%s", SonetChannelTypeString(channel), SonetChannelIdStringGet(channel));

    return idString;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, mMethodsGet(self), sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, ToString);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSonetChannelHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtSonetChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel)
    {
    if (m_SharedSonetChannelDisplayer == NULL)
        {
        m_SharedSonetChannelDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_SharedSonetChannelDisplayer);
        }

    m_SharedSonetChannelDisplayer->mode = mode;
    m_SharedSonetChannelDisplayer->debugLevel = debugLevel;
    return m_SharedSonetChannelDisplayer;
    }

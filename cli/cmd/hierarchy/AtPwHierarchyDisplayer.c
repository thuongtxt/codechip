/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtPwHierarchyDisplayer.c
 *
 * Created Date: Jan 13, 2014
 *
 * Description : PW hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannelHierarchyDisplayerInternal.h"
#include "AtPw.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtPwHierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtPwHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_AtPwHierarchyDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    AtPw pw = (AtPw)channel;
    static char idString[32];
	AtUnused(displayer);

    if (channel == NULL)
        return "none";

    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        {
        if (AtPwCepModeGet((AtPwCep)pw) == cAtPwCepModeFractional)
            AtSprintf(idString, "%s fractional.%s", AtChannelTypeString(channel), AtChannelIdString(channel));
        else
            AtSprintf(idString, "%s basic.%s", AtChannelTypeString(channel), AtChannelIdString(channel));
        }
    else
        AtSprintf(idString, "%s.%s", AtChannelTypeString(channel), AtChannelIdString(channel));

    return idString;
    }

static eBool PwIsCepFractional(AtPw pw)
    {
    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtFalse;

    if (AtPwCepModeGet((AtPwCep)pw) != cAtPwCepModeFractional)
        return cAtFalse;

    return cAtTrue;
    }

static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    AtPw pw = (AtPw)channel;
    AtIterator iterator = AtPwCepEquippedChannelsIteratorCreate((AtPwCep)pw);
    AtChannel vc, lastVc;
    uint8 firstSub;
    uint8 printedDummy = 0;
    AtChannelHierarchyDisplayer subDisplayer;

    if (!PwIsCepFractional(pw) || (iterator == NULL) || AtIteratorCount(iterator) == 0)
        {
        AtObjectDelete((AtObject)iterator);
        return m_AtChannelHierarchyDisplayerMethods->SubChannelHierarchyShow(self, channel, branchLength);
        }

    vc = (AtChannel)AtIteratorNext(iterator);
    firstSub = 1;

    /* Create displayer for nxDs0 as a channel */
    subDisplayer = AtSdhChannelHierarchyDisplayerGet(self->mode, self->debugLevel);

    while (vc != NULL)
        {
        if ((lastVc = (AtChannel)AtIteratorNext(iterator)) == NULL)
            {
            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, (AtChannel)vc, branchLength, firstSub, cAtTrue);
            break;
            }
        else
            {
            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer,(AtChannel)vc, branchLength, firstSub, cAtFalse);
            vc = lastVc;
            }

        if (firstSub)
            firstSub = 0;
        }

    AtObjectDelete((AtObject)iterator);

    return printedDummy;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, ToString);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtPwHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_AtPwHierarchyDisplayer == NULL)
        {
        m_AtPwHierarchyDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_AtPwHierarchyDisplayer);
        }

    m_AtPwHierarchyDisplayer->mode = mode;
    return m_AtPwHierarchyDisplayer;
    }

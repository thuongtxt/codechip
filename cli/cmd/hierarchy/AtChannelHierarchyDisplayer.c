/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtChannelHierarchyDisplayer.c
 *
 * Created Date: Jun 4, 2013
 *
 * Description : Hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannelHierarchyDisplayerInternal.h"
#include "AtEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_methods;

static char m_branchs[1024];

static tAtChannelHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_DefaultDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    static char idString[128];
	AtUnused(displayer);

    if (channel == NULL)
        return "none";

    AtSprintf(idString, "%s.%s", AtChannelTypeString(channel), AtChannelIdString(channel));

    return idString;
    }

static uint32 PrintChannelWithColorStatus(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    eAtSevLevel color;
    const char *channelString = mMethodsGet(displayer)->ToString(displayer, channel);

    color = mMethodsGet(displayer)->StatusColorGet(displayer, channel);
    if (color == cSevInfo)
    	AtPrintc(displayer->branchColor, "%s_", displayer->branchStype);
    else
    	AtPrintc(displayer->branchColor, "%s*", displayer->branchStype); /* Mark * if channel alarm != 0 */

    AtPrintc(color, "%s", channelString);

    return AtStrlen(channelString) + AtStrlen(displayer->branchStype) + 1;
    }

static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 nodeLength)
    {
    /* Try with encap channel first */
    AtChannelHierarchyDisplayer defaultDisplayer = NULL;
    AtChannel attachedChannel = (AtChannel)AtChannelBoundEncapChannelGet(channel);

    if (attachedChannel && (AtEncapChannelEncapTypeGet((AtEncapChannel)attachedChannel) == cAtEncapHdlc))
        defaultDisplayer = AtHdlcChannelHierarchyDisplayerGet(self->mode);

    else if (attachedChannel == NULL)
        {
        attachedChannel = (AtChannel)AtChannelBoundPwGet(channel);
        defaultDisplayer = AtChannelHierarchyDisplayerGet(self->mode);
        }

    if (attachedChannel && defaultDisplayer)
        return ChannelHierarchyRecursiveShow(defaultDisplayer, attachedChannel, nodeLength, cAtTrue, cAtTrue);

    AtPrintc(cSevNormal ,"\r\n");
    return 0;
    }

static eAtSevLevel StatusColorGet(AtChannelHierarchyDisplayer self, AtChannel channel)
    {
    if (self->mode == cAtChannelHierarchyModeAlarm)
        return (AtChannelAlarmGet(channel)) ? cSevCritical : cSevInfo;

    if (self->mode == cAtChannelHierarchyModeHistoryR2C)
        return (AtChannelAlarmInterruptClear(channel)) ? cSevCritical : cSevInfo;

    if (self->mode == cAtChannelHierarchyModeHistoryRO)
        return (AtChannelAlarmInterruptGet(channel)) ? cSevCritical : cSevInfo;

    return cSevInfo;
    }

static void HierarchyBranchAdd(uint32 length)
    {
    uint16 i;
    for (i = 0; i < length - 1; i++)
        AtStrcat(m_branchs, " ");

    AtStrcat(m_branchs, "|");
    }

static void HierarchyBranchPrefixAdd(const char* prefix)
    {
    AtStrcat(m_branchs, prefix);
    }

static void HierarchyBranchRemove(uint32 length)
    {
    uint32 i = AtStrlen(m_branchs) - 1;

    while (length > 0)
        {
        m_branchs[i] = 0;
        length--;
        i--;
        }
    }

static void MethodsInit(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, SubChannelHierarchyShow);
        mMethodOverride(m_methods, PrintChannelWithColorStatus);
        mMethodOverride(m_methods, StatusColorGet);
        mMethodOverride(m_methods, ToString);
        }

    mMethodsGet(self) = &m_methods;
    }

eBool ChannelHierarchyRecursiveShow(AtChannelHierarchyDisplayer displayer, AtChannel channel,
                                    uint32 branchLength, eBool isFirstSub, eBool isLastSub)
    {
    uint32 nodeLength;
    eBool printedDummy = 0;

    /* Case of dummy displayer */
    if (channel == NULL)
        {
        AtPrintc(displayer->branchColor, "%s\r\n", m_branchs);
        return cAtFalse;
        }

    if (isFirstSub)
        HierarchyBranchAdd(branchLength);
    else
        AtPrintc(displayer->branchColor, "%s", m_branchs);

    if (isLastSub && (AtStrlen(m_branchs) > 0))
        m_branchs[AtStrlen(m_branchs) - 1] = ' ';

    /* Print displayer */
    nodeLength = mMethodsGet(displayer)->PrintChannelWithColorStatus(displayer, channel);

    /* It's child notes */
    printedDummy = mMethodsGet(displayer)->SubChannelHierarchyShow(displayer, channel, nodeLength);

    /* If this channel is the last sub branch of it's super node, delete it's branch */
    if (isLastSub)
        {
        /* Show a dummy channel to make the tree look better */
        if ((!isFirstSub) && (!printedDummy))
            {
            ChannelHierarchyRecursiveShow(displayer, NULL, branchLength, 0, 0);
            printedDummy = cAtTrue;
            }

        HierarchyBranchRemove(branchLength);
        }

    return printedDummy;
    }

AtChannelHierarchyDisplayer AtChannelHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, sizeof(tAtChannelHierarchyDisplayer));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    self->branchStype = cAtChannelHierarchyBranchStypeNormal;
    self->branchColor = cSevNormal;
    return self;
    }

void AtChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, const char* prefix)
    {
    AtOsalMemInit(m_branchs, 0, sizeof(m_branchs));
    HierarchyBranchPrefixAdd(prefix);
    ChannelHierarchyRecursiveShow(self, channel, 0, cAtFalse, cAtFalse);
    }

AtChannelHierarchyDisplayer AtChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_DefaultDisplayer == NULL)
        {
        m_DefaultDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        AtChannelHierarchyDisplayerObjectInit(m_DefaultDisplayer);
        }

    m_DefaultDisplayer->mode = mode;
    return m_DefaultDisplayer;
    }

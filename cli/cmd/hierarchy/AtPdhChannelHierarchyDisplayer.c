/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtPdhChannelHierarchyDisplayer.c
 *
 * Created Date: May 5, 2015
 *
 * Description : DE3 hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelHierarchyDisplayerMethods    m_AtChannelHierarchyDisplayerOverride;

/* Save super implementation */
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet ThaSdhChannel2HwMasterStsId(AtSdhChannel channel, eAtModule phyModule, uint8 *slice, uint8 *hwStsInSlice);

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)channel;
    uint8 numSubChannel = AtPdhChannelNumberOfSubChannelsGet(pdhChannel);
    AtChannel sdhChannel = (AtChannel)AtPdhChannelVcGet(pdhChannel);
    uint8 subChn_i, firstSub, lastSub;
    AtChannelHierarchyDisplayer subDisplayer;
    AtChannel subChannel;
    uint8 printedDummy;
    AtPw boundPw;

    if ((boundPw = AtChannelBoundPwGet(channel)) != NULL)
        {
        AtChannelHierarchyDisplayer pwDisplayer = AtPwHierarchyDisplayerGet(self->mode);
        return ChannelHierarchyRecursiveShow(pwDisplayer, (AtChannel)boundPw, branchLength, cAtTrue, cAtTrue);
        }

    if (sdhChannel && ((boundPw = AtChannelBoundPwGet(sdhChannel)) != NULL))
        {
        AtChannelHierarchyDisplayer vcDisplayer = AtSdhChannelHierarchyDisplayerGet(self->mode, self->debugLevel);
        return ChannelHierarchyRecursiveShow(vcDisplayer, (AtChannel)sdhChannel, branchLength, cAtTrue, cAtTrue);
        }

    if (numSubChannel > 0)
        {
        for (subChn_i = 0; subChn_i < numSubChannel; subChn_i++)
            {
            firstSub = (subChn_i == 0) ? 1 : 0;
            lastSub  = (subChn_i == numSubChannel - 1) ? 1 : 0;

            subChannel = (AtChannel)AtPdhChannelSubChannelGet((AtPdhChannel)channel, subChn_i);

            if ((AtPdhChannelTypeGet((AtPdhChannel)subChannel) == cAtPdhChannelTypeE1) ||
                (AtPdhChannelTypeGet((AtPdhChannel)subChannel) == cAtPdhChannelTypeDs1))
                subDisplayer = AtPdhDe1HierarchyDisplayerGet(self->mode, 0);
            else
                subDisplayer = AtPdhChannelHierarchyDisplayerGet(self->mode, 0);

            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, subChannel, branchLength, firstSub, lastSub);
            }

        return printedDummy;
        }

    return m_AtChannelHierarchyDisplayerMethods->SubChannelHierarchyShow(self, channel, branchLength);
    }

static uint32 PrintChannelWithColorStatus(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    static char buffer[8];
    uint32 normalLength = m_AtChannelHierarchyDisplayerMethods->PrintChannelWithColorStatus(displayer, channel);
    uint8 slice, hwSts;
    AtSdhChannel sdhChannel;

    if (displayer->debugLevel == 0)
        return normalLength;

    sdhChannel = (AtSdhChannel)AtPdhChannelVcGet((AtPdhChannel)channel);
    if (sdhChannel == NULL)
        return normalLength;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleSdh, &slice, &hwSts);
    AtSprintf(buffer, "(%u.%u)", slice, hwSts);
    AtPrintc(cSevDebug, "%-6s", buffer);

    return normalLength + 6;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, PrintChannelWithColorStatus);
        }

    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhChannelHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

AtChannelHierarchyDisplayer AtPdhChannelHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtPdhChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel)
    {
    static tAtPdhChannelHierarchyDisplayer object;
    static AtChannelHierarchyDisplayer sharedDisplayer = NULL;

    if (sharedDisplayer == NULL)
        {
        sharedDisplayer = (AtChannelHierarchyDisplayer)&object;
        AtPdhChannelHierarchyDisplayerObjectInit(sharedDisplayer);
        }

    sharedDisplayer->mode = mode;
    sharedDisplayer->debugLevel = debugLevel;
    return sharedDisplayer;
    }

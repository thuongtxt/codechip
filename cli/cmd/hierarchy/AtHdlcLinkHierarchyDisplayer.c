/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtHdlcLinkHierarchyDisplayer.c
 *
 * Created Date: Aug 7, 2013
 *
 * Description : HDLC hierarchy displayer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcLink.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tAtHdlcLinkHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtHdlcLinkHierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtHdlcLinkHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_AtHdlcLinkHierarchyDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    AtHdlcLink link = (AtHdlcLink)channel;
    AtChannel attachedFlow;
    uint8 printedDummy = 0;
    AtChannelHierarchyDisplayer subDisplayer;

    attachedFlow = (AtChannel)AtHdlcLinkBoundFlowGet(link);
    if (attachedFlow == NULL)
        {
        AtPrintc(cSevNormal ,"\r\n");
        return 0;
        }

    /* Create displayer for sub-channel */
    subDisplayer = AtEthFlowHierarchyDisplayerGet(self->mode);
    printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, attachedFlow, branchLength, cAtTrue, cAtTrue);

    return printedDummy;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHdlcLinkHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtHdlcLinkHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_AtHdlcLinkHierarchyDisplayer == NULL)
        {
        m_AtHdlcLinkHierarchyDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_AtHdlcLinkHierarchyDisplayer);
        }

    m_AtHdlcLinkHierarchyDisplayer->mode = mode;
    return m_AtHdlcLinkHierarchyDisplayer;
    }

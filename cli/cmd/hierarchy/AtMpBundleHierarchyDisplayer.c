/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtMpBundleHierarchyDisplayer.c
 *
 * Created Date: Jun 5, 2013
 *
 * Description :
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMpBundle.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tAtMpBundleHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtMpBundleHierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtMpBundleHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_SharedMpBundleDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    AtMpBundle bundle = (AtMpBundle)channel;
    AtChannel subChannel, lastSubChannel;
    uint8 printedDummy = 0;
    eBool firstSub;
    AtChannelHierarchyDisplayer subDisplayer;
    AtIterator iterator = AtMpBundleFlowIteratorCreate(bundle);

    if ((iterator == NULL) || AtIteratorCount(iterator) == 0)
        {
        AtObjectDelete((AtObject)iterator);
        return m_AtChannelHierarchyDisplayerMethods->SubChannelHierarchyShow(self, channel, branchLength);
        }

    subChannel = (AtChannel)AtIteratorNext(iterator);
    firstSub = 1;

    /* Create displayer for sub-channel */
    subDisplayer = AtEthFlowHierarchyDisplayerGet(self->mode);

    while (subChannel != NULL)
        {
        if ((lastSubChannel = (AtChannel)AtIteratorNext(iterator)) == NULL)
            {
            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, subChannel, branchLength, firstSub, cAtTrue);
            break;
            }
        else
            {
            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, subChannel, branchLength, firstSub, cAtFalse);
            subChannel = lastSubChannel;
            }

        if (firstSub)
            firstSub = 0;
        }

    AtObjectDelete((AtObject)iterator);

    return printedDummy;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMpBundleHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtMpBundleHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_SharedMpBundleDisplayer == NULL)
        {
        m_SharedMpBundleDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_SharedMpBundleDisplayer);
        }

    m_SharedMpBundleDisplayer->mode = mode;
    return m_SharedMpBundleDisplayer;
    }

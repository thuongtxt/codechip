/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtPdhDe1HierarchyDisplayer.c
 *
 * Created Date: Jun 5, 2013
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhDe1HierarchyDisplayer
    {
    tAtPdhChannelHierarchyDisplayer super;
    }tAtPdhDe1HierarchyDisplayer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;

/* Save super implementation */
static const tAtChannelHierarchyDisplayerMethods *m_AtChannelHierarchyDisplayerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    AtPdhDe1 de1 = (AtPdhDe1)channel;
    AtChannel nxDs0, lastNxDs0;
    uint8 printedDummy = 0;
    uint8 firstSub;
    AtChannelHierarchyDisplayer subDisplayer;
    AtIterator iterator = AtPdhDe1nxDs0IteratorCreate(de1);

    if ((iterator == NULL) || AtIteratorCount(iterator) == 0)
        {
        AtObjectDelete((AtObject)iterator);
        return m_AtChannelHierarchyDisplayerMethods->SubChannelHierarchyShow(self, channel, branchLength);
        }

    nxDs0 = (AtChannel)AtIteratorNext(iterator);
    firstSub = 1;

    /* Create displayer for nxDs0 as a channel */
    subDisplayer = AtChannelHierarchyDisplayerGet(self->mode);

    while (nxDs0)
        {
        if ((lastNxDs0 = (AtChannel)AtIteratorNext(iterator)) == NULL)
            {
            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer, (AtChannel)nxDs0, branchLength, firstSub, cAtTrue);
            break;
            }
        else
            {
            printedDummy = ChannelHierarchyRecursiveShow(subDisplayer,(AtChannel)nxDs0, branchLength, firstSub, cAtFalse);
            nxDs0 = lastNxDs0;
            }

        if (firstSub)
            firstSub = 0;
        }

    AtObjectDelete((AtObject)iterator);

    return printedDummy;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe1HierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtPdhDe1HierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel)
    {
    static tAtPdhDe1HierarchyDisplayer object;
    static AtChannelHierarchyDisplayer de1SharedDisplayer = NULL;

    if (de1SharedDisplayer == NULL)
        {
        de1SharedDisplayer = (AtChannelHierarchyDisplayer)&object;
        ObjectInit(de1SharedDisplayer);
        }

    de1SharedDisplayer->mode = mode;
    de1SharedDisplayer->debugLevel = debugLevel;

    return de1SharedDisplayer;
    }

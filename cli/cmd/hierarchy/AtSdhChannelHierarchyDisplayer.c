/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtSdhChannelHierarchyDisplayer.c
 *
 * Created Date: Jun 5, 2013
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtChannelHierarchyDisplayerInternal.h"
#include "AtModuleXc.h"
#include "AtDevice.h"
#include "AtCli.h"
#include "AtCrossConnect.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)((tAtSdhChannelHierarchyDisplayer*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtSdhChannelHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_SharedSdhChannelDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet ThaSdhChannel2HwMasterStsId(AtSdhChannel channel, eAtModule phyModule, uint8 *slice, uint8 *hwStsInSlice);
extern eAtRet Tha60210011ModuleXcTfi5StsFromOcnStsGet(AtModuleXc self, uint8 sliceId, uint8 stsId, uint8* lineId, uint8* tfi5Sts);
extern eBool AtSdhChannelIsHoVc(AtSdhChannel sdhChannel);

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelsShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength, eBool *dummyPrinted)
    {
    uint8 numSubChannel, subChn_i, firstSub, lastSub;
    AtChannel subChannel;
    *dummyPrinted = cAtFalse;

    numSubChannel = AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)channel);
    if (numSubChannel == 0)
        return 0;

    for (subChn_i = 0; subChn_i < numSubChannel; subChn_i++)
        {
        firstSub = (subChn_i == 0) ? 1 : 0;
        lastSub  = (subChn_i == numSubChannel - 1) ? 1 : 0;

        subChannel = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)channel, subChn_i);
        *dummyPrinted = ChannelHierarchyRecursiveShow(self, subChannel, branchLength, firstSub, lastSub);
        }

    return numSubChannel;
    }

static uint8 MapChannelsShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength, eBool *dummyPrinted)
    {
    AtChannel mapChannel = (AtChannel)AtSdhChannelMapChannelGet((AtSdhChannel)channel);
    AtChannelHierarchyDisplayer subDisplayer = NULL;
    eAtSdhChannelType sdhChannelType;
    *dummyPrinted = cAtFalse;

    if (mapChannel == NULL)
        return 0;

    sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)channel);

    if ((sdhChannelType == cAtSdhChannelTypeVc12) || (sdhChannelType == cAtSdhChannelTypeVc11))
        {
        subDisplayer = AtPdhDe1HierarchyDisplayerGet(self->mode, self->debugLevel);
        *dummyPrinted = ChannelHierarchyRecursiveShow(subDisplayer, (AtChannel)mapChannel, branchLength, cAtTrue, cAtTrue);
        return 1;
        }

    if (sdhChannelType == cAtSdhChannelTypeVc3)
        {
        subDisplayer = AtPdhChannelHierarchyDisplayerGet(self->mode, self->debugLevel);
        *dummyPrinted = ChannelHierarchyRecursiveShow(subDisplayer, (AtChannel)mapChannel, branchLength, cAtTrue, cAtTrue);
        return 1;
        }

    return 0;
    }

static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 branchLength)
    {
    eBool printedDummy = cAtFalse;
    AtPw boundPw;

    if (channel == NULL)
        return cAtFalse;

    if ((boundPw = AtChannelBoundPwGet(channel)) != NULL)
        {
        AtChannelHierarchyDisplayer pwDisplayer = AtPwHierarchyDisplayerGet(self->mode);
        return ChannelHierarchyRecursiveShow(pwDisplayer, (AtChannel)boundPw, branchLength, cAtTrue, cAtTrue);
        }

    if (MapChannelsShow(self, channel, branchLength, &printedDummy) > 0)
        return printedDummy;

    if (SubChannelsShow(self, channel, branchLength, &printedDummy) > 0)
        return printedDummy;

    return m_AtChannelHierarchyDisplayerMethods->SubChannelHierarchyShow(self, channel, branchLength);
    }

static AtModuleXc ModuleXc(AtChannel channel)
    {
    return (AtModuleXc)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModuleXc);
    }

static eBool SdhChannelNeedToShowDebugLevel2(AtSdhChannel channel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    if ((channelType == cAtSdhChannelTypeAu4)     ||
        (channelType == cAtSdhChannelTypeAu3)     ||
        AtSdhChannelIsHoVc(channel))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PrintChannelWithColorStatus(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    static char buffer1[8], buffer2[8];
    uint8 ocnSlice, ocnSts, line, tfi5Sts;
    uint32 normalLength = m_AtChannelHierarchyDisplayerMethods->PrintChannelWithColorStatus(displayer, channel);

    if (displayer->debugLevel == 0)
        return normalLength;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cAtModuleSdh, &ocnSlice, &ocnSts);
    if (SdhChannelNeedToShowDebugLevel2((AtSdhChannel)channel) && (displayer->debugLevel == 2))
        {
        Tha60210011ModuleXcTfi5StsFromOcnStsGet(ModuleXc(channel), ocnSlice, ocnSts, &line, &tfi5Sts);
        AtSprintf(buffer2, "(%u.%u)", line, tfi5Sts);
        AtPrintc(cSevMajor, "%s", buffer2);
        AtPrintc(cSevNormal, "-");
        normalLength = normalLength + 7;
        }

    AtSprintf(buffer1, "(%u.%u)", ocnSlice, ocnSts);
    AtPrintc(cSevDebug, "%-6s", buffer1);
    normalLength = normalLength + 6;

    return normalLength;
    }

static const char *LineString(AtSdhLine line)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate == cAtSdhLineRateStm0)  return "STM0";
    if (rate == cAtSdhLineRateStm1)  return "STM1";
    if (rate == cAtSdhLineRateStm4)  return "STM4";
    if (rate == cAtSdhLineRateStm16) return "STM16";
    if (rate == cAtSdhLineRateStm64) return "STM64";

    return "line";
    }

static const char *ChannelTypeString(AtChannel channel)
    {
    if (AtSdhChannelTypeGet((AtSdhChannel)channel) == cAtSdhChannelTypeLine)
        return LineString((AtSdhLine)channel);

    return AtChannelTypeString(channel);
    }

static const char *ToString(AtChannelHierarchyDisplayer displayer, AtChannel channel)
    {
    static char idString[32];
    AtUnused(displayer);

    if (channel == NULL)
        return "none";

    AtSprintf(idString, "%s.%s", ChannelTypeString(channel), AtChannelIdString(channel));

    return idString;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, PrintChannelWithColorStatus);
        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, ToString);
        }

    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhChannelHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

AtChannelHierarchyDisplayer AtSdhChannelHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtSdhChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode, uint8 debugLevel)
    {
    if (m_SharedSdhChannelDisplayer == NULL)
        {
        m_SharedSdhChannelDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        AtSdhChannelHierarchyDisplayerObjectInit(m_SharedSdhChannelDisplayer);
        }

    m_SharedSdhChannelDisplayer->mode = mode;
    m_SharedSdhChannelDisplayer->debugLevel = debugLevel;
    return m_SharedSdhChannelDisplayer;
    }

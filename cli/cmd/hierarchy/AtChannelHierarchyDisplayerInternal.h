/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtChannelHierarchyDisplayerInternal.h
 * 
 * Created Date: Jul 16, 2015
 *
 * Description : Hierarchy displayer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLI_CMD_HIERARCHY_ATCHANNELHIERARCHYDISPLAYERINTERNAL_H_
#define _CLI_CMD_HIERARCHY_ATCHANNELHIERARCHYDISPLAYERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannelHierarchyDisplayer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelHierarchyDisplayerMethods
    {
    uint8 (*SubChannelHierarchyShow)(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 nodeLength);
    eAtSevLevel (*StatusColorGet)(AtChannelHierarchyDisplayer self, AtChannel channel);
    const char* (*ToString)(AtChannelHierarchyDisplayer self, AtChannel channel);
    uint32 (*PrintChannelWithColorStatus)(AtChannelHierarchyDisplayer displayer, AtChannel channel);
    }tAtChannelHierarchyDisplayerMethods;

typedef struct tAtChannelHierarchyDisplayer
    {
    tAtObject super;
    const tAtChannelHierarchyDisplayerMethods *methods;

    eAtChannelHierarchyMode mode;
    const char* branchStype;
    eAtSevLevel branchColor;
    uint8 debugLevel;

    }tAtChannelHierarchyDisplayer;

typedef struct tAtPdhChannelHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtPdhChannelHierarchyDisplayer;

typedef struct tAtSdhChannelHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtSdhChannelHierarchyDisplayer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannelHierarchyDisplayer AtChannelHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self);
AtChannelHierarchyDisplayer AtPdhChannelHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self);
AtChannelHierarchyDisplayer AtSdhChannelHierarchyDisplayerObjectInit(AtChannelHierarchyDisplayer self);

#ifdef __cplusplus
}
#endif
#endif /* _CLI_CMD_HIERARCHY_ATCHANNELHIERARCHYDISPLAYERINTERNAL_H_ */


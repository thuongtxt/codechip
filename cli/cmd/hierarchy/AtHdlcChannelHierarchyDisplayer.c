/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtHdlcChannelHierarchyDisplayer.c
 *
 * Created Date: Apr 24, 2017
 *
 * Description : Show hierarchy HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"
#include "AtChannelHierarchyDisplayerInternal.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tAtHdlcHierarchyDisplayer
    {
    tAtChannelHierarchyDisplayer super;
    }tAtHdlcHierarchyDisplayer;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtChannelHierarchyDisplayerMethods m_AtChannelHierarchyDisplayerOverride;
static const tAtChannelHierarchyDisplayerMethods * m_AtChannelHierarchyDisplayerMethods = NULL;

static tAtHdlcHierarchyDisplayer m_Object;
static AtChannelHierarchyDisplayer m_AtHdlcHierarchyDisplayer = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SubChannelHierarchyShow(AtChannelHierarchyDisplayer self, AtChannel channel, uint32 nodeLength)
    {
    AtChannel attachedChannel = (AtChannel)AtChannelBoundPwGet(channel);

    if (attachedChannel == NULL)
        attachedChannel = (AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)channel);

    if (attachedChannel)
        {
        AtChannelHierarchyDisplayer defaultDisplayer = AtChannelHierarchyDisplayerGet(self->mode);
        return ChannelHierarchyRecursiveShow(defaultDisplayer, attachedChannel, nodeLength, cAtTrue, cAtTrue);
        }

    AtPrintc(cSevNormal ,"\r\n");
    return 0;
    }

static void OverrideAtChannelHierarchyDisplayer(AtChannelHierarchyDisplayer self)
    {
    if (!m_methodsInit)
        {
        m_AtChannelHierarchyDisplayerMethods = mMethodsGet((AtChannelHierarchyDisplayer)self);
        AtOsalMemCpy(&m_AtChannelHierarchyDisplayerOverride, m_AtChannelHierarchyDisplayerMethods, sizeof(m_AtChannelHierarchyDisplayerOverride));

        mMethodOverride(m_AtChannelHierarchyDisplayerOverride, SubChannelHierarchyShow);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet(self) = &m_AtChannelHierarchyDisplayerOverride;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHdlcHierarchyDisplayer);
    }

static void Override(AtChannelHierarchyDisplayer self)
    {
    OverrideAtChannelHierarchyDisplayer(self);
    }

static AtChannelHierarchyDisplayer ObjectInit(AtChannelHierarchyDisplayer self)
    {
    /* Initialize memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelHierarchyDisplayerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtChannelHierarchyDisplayer AtHdlcChannelHierarchyDisplayerGet(eAtChannelHierarchyMode mode)
    {
    if (m_AtHdlcHierarchyDisplayer == NULL)
        {
        m_AtHdlcHierarchyDisplayer = (AtChannelHierarchyDisplayer)&m_Object;
        ObjectInit(m_AtHdlcHierarchyDisplayer);
        }

    m_AtHdlcHierarchyDisplayer->mode = mode;
    return m_AtHdlcHierarchyDisplayer;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diag
 * 
 * File        : CliAtErrorGenerator.h
 * 
 * Created Date: Apr 6, 2016
 *
 * Description : Error generator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATERRORGENERATOR_H_
#define _CLIATERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtErrorGenerator.h"
#include "AtPdhChannel.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtPdhErrorGeneratorErrorType CliPdhErrorGeneratorErrorTypeFromString(const char *string);
const char *CliPdhErrorGeneratorErrorTypeString(eAtPdhErrorGeneratorErrorType errorType);
const char *CliValidPdhErrorGeneratorErrorType(void);

eAtErrorGeneratorMode CliErrorGeneratorModeFromString(const char *string);
const char *CliErrorGeneratorModeString(eAtErrorGeneratorMode mode);
const char *CliValidErrorGeneratorMode(void);

eAtEthPortErrorGeneratorErrorType CliEthPortErrorGeneratorErrorTypeFromString(const char *string);
const char *CliEthPortErrorGeneratorErrorTypeString(eAtEthPortErrorGeneratorErrorType errorType);
const char *CliValidEthPortErrorGeneratorErrorType(void);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATERRORGENERATOR_H_ */

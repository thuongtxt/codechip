/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : CliAtEthPort.c
 *
 * Created Date: Sep 28, 2015
 *
 * Description : ETH Port diagnostic
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../eth/CliAtModuleEth.h"
#include "../physical/CliAtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(calling, message)                                       \
    do                                                                         \
        {                                                                      \
        eAtRet ret_ = calling;                                                 \
        if (ret_ != cAtOk)                                                     \
            {                                                                  \
            AtPrintc(cSevCritical, "%s, ret=%s\r\n", message, AtRet2String(ret_)); \
            return ret_;                                                       \
            }                                                                  \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PrbsIsRunning(AtPrbsEngine prbsEngine)
    {
    uint32 txCounters, rxCounters;

    /* Clear and give hardware a moment to update counters */
    AtPrbsEngineCounterClear(prbsEngine, cAtPrbsEngineCounterTxFrame);
    AtPrbsEngineCounterClear(prbsEngine, cAtPrbsEngineCounterRxFrame);
    AtOsalUSleep(1000);
    txCounters = AtPrbsEngineCounterGet(prbsEngine, cAtPrbsEngineCounterTxFrame);
    rxCounters = AtPrbsEngineCounterGet(prbsEngine, cAtPrbsEngineCounterRxFrame);

    /* Let hardware run for a while, counters must also be updated */
    AtOsalUSleep(1000);
    if (AtPrbsEngineCounterGet(prbsEngine, cAtPrbsEngineCounterTxFrame) == txCounters)
        return cAtFalse;
    if (AtPrbsEngineCounterGet(prbsEngine, cAtPrbsEngineCounterRxFrame) == rxCounters)
        return cAtFalse;

    return cAtTrue;
    }

static eBool SerdesExpectGood(AtPrbsEngine prbsEngine, eBool expectGood)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;

    AtOsalCurTimeGet(&startTime);

    while (elapseTimeMs < cTimeoutMs)
        {
        static const uint32 cStablePeriodMs = 1000;
        uint32 stableElapseTime = 0;
        tAtOsalCurTime stableStartTime, stableCurTime;

        /* Make sure that  */
        AtOsalCurTimeGet(&stableStartTime);
        while (stableElapseTime < cStablePeriodMs)
            {
            eBool good = (AtPrbsEngineAlarmHistoryClear(prbsEngine) == 0) ? cAtTrue : cAtFalse;

            /* Expect good but it is not, break for retrying */
            if (expectGood && !good)
                break;

            /* Expect bad but it is not bad, break for retrying */
            if (!expectGood && good)
                break;

            /* Continue */
            AtOsalCurTimeGet(&stableCurTime);
            stableElapseTime = mTimeIntervalInMsGet(stableStartTime, stableCurTime);
            }

        /* OK, reach stable state */
        if (stableElapseTime >= cStablePeriodMs)
            return cAtTrue;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static void IOFlush(void)
    {
    AtFileFlush(AtStdStandardOutput(AtStdSharedStdGet()));
    }

static eAtRet SerdesDiagnosticRun(AtPrbsEngine prbsEngine)
    {
    eBool passed = cAtTrue;

    /* Enable PRBS */
    mSuccessAssert(AtPrbsEngineEnable(prbsEngine, cAtTrue), "ERROR: Cannot enable PRBS engine");

    /* The engine must run */
    AtPrintc(cSevNormal, "        Check PRBS running ... "); IOFlush();
    if (PrbsIsRunning(prbsEngine))
        AtPrintc(cSevInfo, "OK\r\n");
    else
        {
        AtPrintc(cSevCritical, "FAIL\r\n");
        passed = cAtFalse;
        }

    /* With no alarm */
    AtPrintc(cSevNormal, "        Check alarm ... "); IOFlush();
    if (SerdesExpectGood(prbsEngine, cAtTrue))
        AtPrintc(cSevInfo, "OK\r\n");
    else
        {
        AtPrintc(cSevCritical, "FAIL\r\n");
        passed = cAtFalse;
        }

    /* Error testing */
    AtPrintc(cSevNormal, "        Force error and check ... "); IOFlush();
    mSuccessAssert(AtPrbsEngineErrorForce(prbsEngine, cAtTrue), "ERROR: PRBS engine cannot forced error");
    if (SerdesExpectGood(prbsEngine, cAtFalse))
        AtPrintc(cSevInfo, "OK\r\n");
    else
        {
        AtPrintc(cSevCritical, "FAIL\r\n");
        passed = cAtFalse;
        }

    /* Bring it back to normal state */
    AtPrintc(cSevNormal, "        Bring to normal state ... "); IOFlush();
    AtPrbsEngineErrorForce(prbsEngine, cAtFalse);
    if (SerdesExpectGood(prbsEngine, cAtTrue))
        AtPrintc(cSevInfo, "OK\r\n");
    else
        {
        AtPrintc(cSevCritical, "FAIL\r\n");
        passed = cAtFalse;
        }

    return passed ? cAtOk : cAtErrorDiagnosticFail;
    }

static eAtRet SerdesDiagnostic(AtSerdesController controller)
    {
    AtPrbsEngine prbsEngine = AtSerdesControllerPrbsEngine(controller);
    uint32 serdesId = AtSerdesControllerIdGet(controller);
    eAtRet ret = cAtOk;

    /* To confirm the image can work with it-self by local loopback */
    AtPrintc(cSevInfo, "Testing port: %u\r\n", serdesId + 1);
    AtPrintc(cSevNormal, "    Loopback test: \r\n");
    ret |= AtSerdesControllerLoopbackSet(controller, cAtLoopbackModeLocal);
    ret |= SerdesDiagnosticRun(prbsEngine);

    /* To confirm the image can work with the remote side, assume that the remote
     * side makes remote loopback */
    AtPrintc(cSevNormal, "    None Loopback test: \r\n");
    ret |= AtSerdesControllerLoopbackSet(controller, cAtLoopbackModeRelease);
    ret |= SerdesDiagnosticRun(prbsEngine);

    /* Bring it back to default state */
    ret |= AtPrbsEngineEnable(prbsEngine, cAtFalse);

    return ret;
    }

eBool CmdAtEthPortSerdesDiagnostic(char argc, char **argv)
    {
    AtList serdesControllers = CliAtEthPortSerdesControllers(argv[0]);
    uint32 serdes_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    for (serdes_i = 0; serdes_i < AtListLengthGet(serdesControllers); serdes_i++)
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(serdesControllers, serdes_i);
        eAtRet ret;

        ret = SerdesDiagnostic(controller);
        if (ret != cAtOk)
            {
            success = cAtFalse;
            break;
            }
        }

    AtObjectDelete((AtObject)serdesControllers);

    return success;
    }

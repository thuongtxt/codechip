/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diag
 *
 * File        : CliAtErrorGenerator.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : Error generator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPdhErrorGeneratorErrorTypeStr[] ={"fbit-include", "fbit-exclude", "fbit-only", "invalid"};
static const uint32 cAtPdhErrorGeneratorErrorTypeValue[]={cAtPdhErrorGeneratorErrorTypeFullBitStream,
                                                          cAtPdhErrorGeneratorErrorTypePayload,
                                                          cAtPdhErrorGeneratorErrorTypeFbit,
                                                          cAtPdhErrorGeneratorErrorTypeInvalid};

static const char * cAtEthPortErrorGeneratorErrorTypeStr[] ={"fcs", "mac-pcs", "invalid"};
static const uint32 cAtEthPortErrorGeneratorErrorTypeValue[]={cAtEthPortErrorGeneratorErrorTypeFcs,
                                                              cAtEthPortErrorGeneratorErrorTypeMacPcs,
                                                              cAtPdhErrorGeneratorErrorTypeInvalid};

static const char * cAtErrorGeneratorModeStr[] ={"oneshot", "continuous", "invalid"};
static const uint32 cAtErrorGeneratorModeValue[]={cAtErrorGeneratorModeOneshot,
                                                  cAtErrorGeneratorModeContinuous,
                                                  cAtErrorGeneratorModeInvalid};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eAtPdhErrorGeneratorErrorType CliPdhErrorGeneratorErrorTypeFromString(const char *string)
    {
    eAtPdhErrorGeneratorErrorType errorType;
    eBool convertSuccess;

    mAtStrToEnum(cAtPdhErrorGeneratorErrorTypeStr,
                 cAtPdhErrorGeneratorErrorTypeValue,
                 string,
                 errorType,
                 convertSuccess);
    return convertSuccess ? errorType : cAtPdhErrorGeneratorErrorTypeInvalid;
    }

const char *CliPdhErrorGeneratorErrorTypeString(eAtPdhErrorGeneratorErrorType errorType)
    {
    return CliEnumToString(errorType, cAtPdhErrorGeneratorErrorTypeStr, cAtPdhErrorGeneratorErrorTypeValue,
                           mCount(cAtPdhErrorGeneratorErrorTypeValue), NULL);
    }

const char *CliValidPdhErrorGeneratorErrorType(void)
    {
    return "fbit-include, fbit-exclude, fbit-only";
    }

eAtEthPortErrorGeneratorErrorType CliEthPortErrorGeneratorErrorTypeFromString(const char *string)
    {
    eAtEthPortErrorGeneratorErrorType errorType;
    eBool convertSuccess;

    mAtStrToEnum(cAtEthPortErrorGeneratorErrorTypeStr,
                 cAtEthPortErrorGeneratorErrorTypeValue,
                 string,
                 errorType,
                 convertSuccess);
    return convertSuccess ? errorType : cAtEthPortErrorGeneratorErrorTypeInvalid;
    }

const char *CliEthPortErrorGeneratorErrorTypeString(eAtEthPortErrorGeneratorErrorType errorType)
    {
    return CliEnumToString(errorType, cAtEthPortErrorGeneratorErrorTypeStr, cAtEthPortErrorGeneratorErrorTypeValue,
                           mCount(cAtEthPortErrorGeneratorErrorTypeValue), NULL);
    }

const char *CliValidEthPortErrorGeneratorErrorType(void)
    {
    return "fcs, mac-pcs";
    }

eAtErrorGeneratorMode CliErrorGeneratorModeFromString(const char *string)
    {
    eAtErrorGeneratorMode mode;
    eBool convertSuccess;

    mAtStrToEnum(cAtErrorGeneratorModeStr,
                 cAtErrorGeneratorModeValue,
                 string,
                 mode,
                 convertSuccess);
    return convertSuccess ? mode : cAtErrorGeneratorModeInvalid;
    }

const char *CliErrorGeneratorModeString(eAtErrorGeneratorMode mode)
    {
    return CliEnumToString(mode, cAtErrorGeneratorModeStr, cAtErrorGeneratorModeValue,
                           mCount(cAtErrorGeneratorModeValue), NULL);
    }

const char *CliValidErrorGeneratorMode(void)
    {
    return "oneshot, continuous";
    }

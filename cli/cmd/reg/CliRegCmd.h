/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : CliAtSur.h
 *
 * Created Date: Mar 22, 2015
 *
 * Description : SUR engine CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef CLIREGCMD_H_
#define CLIREGCMD_H_

typedef unsigned long long uint64;
#define cAtHalRegLenMax    256
#define AT_REG_32_MUL      (cAtHalRegLenMax / 32) /* 6 * 32 -> 192 bits */
#define AT_REG_FIELD_MAX   (32) /* Maximum fields in register */
#define AT_REG_LEN_32  (0)
#define AT_REG_LEN_64  (1)
#define AT_REG_LEN_96  (2)
#define AT_REG_LEN_128 (3)
#define AT_REG_LEN_160 (4)
#define AT_REG_LEN_192 (5)
#define AT_REG_LEN(_mul) ((_mul) - 1)
#define AT_REG_FOR_VAR_NUM (5)
#define AT_REG_FIELD_RW (1)


typedef enum eAT_REG_BLOCKS
    {
    cAT_REG_BLK_GLB = 0,
    cAT_REG_BLK_GLBR = 1,
    cAT_REG_BLK_OCN = 2,
    cAT_REG_BLK_PDH = 3,
    cAT_REG_BLK_PDHDLK = 4,
    cAT_REG_BLK_MAP_HO = 5,
    cAT_REG_BLK_MAP = 6,
    cAT_REG_BLK_CDR_HO = 7,
    cAT_REG_BLK_CDR = 8,
    cAT_REG_BLK_PLA = 9,
    cAT_REG_BLK_PDA = 11,
    cAT_REG_BLK_CLA = 12,
    cAT_REG_BLK_PWE = 13,
    cAT_REG_BLK_PMC = 14,
    cAT_REG_BLK_ETH = 15,
    cAT_REG_BLK_PM  = 16,
    } eAT_REG_BLOCKS;

typedef enum eAtRegDevice
{
    cAtRegDev60210011,
    cAtRegDev60210031,
    cAtRegDev60210021
} eAtRegDevice;

/** @brief */
typedef struct tRegFieldBit
{
    uint32 pos_end;
    uint32 pos_start;
    uint64 value;
    uint8  type;
} tRegFieldBit;

typedef struct tRegValDetail
{
    const char* varName;
    uint16 varMin;
    uint16 varMax;
} tRegValDetail;

typedef struct tRegFormularInfo
{
    uint8 varNum;
    tRegValDetail varDetail[AT_REG_FOR_VAR_NUM];
} tRegFormularInfo;

/** @brief */
typedef struct tAtRegDes
{
    uint32 mul_32; /**< @ref: AT_REG_LEN_32... */
    uint32 addr_start; /**< Address start */
    uint32 addr_end; /**< Address end */
    uint32 regval[AT_REG_32_MUL];
    uint32 max_iterm; /**< Number field */
    const char** tittle;
    tRegFieldBit *field;
    uint64 *field_value[AT_REG_FIELD_MAX]; /* Max is 32 */
    const char* name;
    const char* formular;
    tRegFormularInfo forDetail;
} tAtRegDes;
/*--------------------------- Includes ---------------------------------------*/
eAtRet CliPmRegTableData(tTab *pTab);
#endif /* CLIREGCMD_H_ */
